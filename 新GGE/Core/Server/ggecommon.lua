-- @Author: 作者QQ381990860
-- @Date:   2021-08-13 19:44:17
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-08-13 19:53:31
-- @作者: baidwwy
-- @邮箱:  313738139@qq.com
-- @创建时间:   2015-01-27 03:51:34
-- @最后修改来自: baidwwy
-- @Last Modified time: 2021-06-23 16:24:47
require("ggefunction")
local _class 	= {}--保存父类
local ffi = require("ffi")
function class(...)
	local class_type 	= {}--创建的对象
	class_type.super 	= {...}
	class_type.初始化 	= false
	class_type.创建 	= function(...)
		local ctor = {}
			setmetatable(ctor,{ __index = _class[class_type] })--继承类属性和函数
			do--获得初始化属性
				local create;
				create = function(c,...)
					if #c.super > 0	then--继承属性
						for i,v in ipairs(c.super) do
						 	create(v,...)
						end
					end
					if c.初始化 then c.初始化(ctor,...) end
				end
				create(class_type,...)--递归所有父类
			end
			function ctor:运行父函数(id,name,...)
				if type(id) == 'number' then
					if _class[class_type.super[id]] and _class[class_type.super[id]][name] then
					    return _class[class_type.super[id]][name](self,...)
					end
				else
					if _class[id] and _class[id][name] then
					    return _class[id][name](self,...)
					end
				end
			end
		return ctor
	end
	_class[class_type] = {}--类函数实际保存
	local mt = {}
	mt.__newindex 	= _class[class_type]
	mt.__call		= function (t,...)
		return t.创建(...)
	end
	setmetatable(class_type,mt)
	if #class_type.super > 0 then--继承函数
		setmetatable(_class[class_type],{__index = function (t,k)
		    for i,v in ipairs(class_type.super) do
		        local ret = _class[v][k]
		        if ret then
		            return ret
		        end
		    end
		end})
	end
	return class_type
end
function ReadExcel(paths,stringkey)
    if paths=="NPC数据" then
         __gge.print(true,7,string.format("加载%s\t\t\t-->", paths))
    else 
        __gge.print(true,7,string.format("加载%s\t\t-->", paths))
    end
    
        path=ServerDirectory.."//data//"..paths..".data"
   
        -- local key={}
        -- for i=1,string.len(stringkey) do
        -- table.insert(key,string.byte(string.sub(stringkey,i,i)))
        -- end
          local  key = { 99, 77,66, 138, 55, 23, 254, 109, 165, 90, 19, 41, 145, 201, 58, 55, 37, 254, 185, 165, 169, 19, 171, 38, 1, 99, 9, 86, 12, 74, 1, 215, 88, 64, 56, 22, 56 };

    local file =  assert(io.open(path, "rb"))
    local DecryptData ={}
    local number =0
    while true do
        local bytes = file:read(1)

        if not bytes then break end
        for b in string.gfind(bytes, ".") do
            num=tonumber(string.byte(b))
            -- io.write(num,",")
            local a= number%#key
            DecryptData[number+1]=bit.bxor(tostring(num),key[a+1]) 
            number=number+1  
        end
    end
    file:close()
    local  indexes=0
    local bufToInt32 = function (dataTable)
      local num = 0;
      num = num  + math.floor(dataTable[indexes+4] * (2 ^ 24))
      num = num  + math.floor(dataTable[indexes+3] * (2 ^ 16))
      num = num  + math.floor(dataTable[indexes+2] * (2 ^ 8))
      num = num + dataTable[indexes+1];

        indexes=indexes+4
      return num;
    end
    local  bufToInt16=function(dataTable)
      local num = 0;
      num = num + math.floor(dataTable[indexes+2] * (2 ^ 8));
      num = num + dataTable[indexes+1];
        indexes=indexes+2
      return num;
    end
    
    local Row= bufToInt32(DecryptData)
    local Column=bufToInt32(DecryptData)
    local ReadASCIIString =function(dataTable)
            local len = bufToInt16(dataTable);
            local a={}
            for i=1,len do 
              table.insert(a,dataTable[indexes+1])
              indexes=indexes+1
            end
           
            return  string.char(unpack(a))
    end
    local FieldName={} 
    local GameData={}
    local GameDataName
    local funType={}

    for i=1,Row do
       for j=1,Column do
         local str = ReadASCIIString(DecryptData);
          -- print(str)
         if i==1 then 
             FieldName[j] = str;
         elseif i==3 then
             if str=="number" then
                   funType[j]=tonumber
             elseif str=="string" then
                   funType[j]=tostring
             elseif str=="array" then
                    funType[j]=toTablenumber
            elseif str=="table" then
                    funType[j]=toTable
             elseif str=="json" then
                funType[j]=cjson.decode
             end
             -- print(j)
            
             -- print(str)
         elseif i>3 and str~="" then 
         -- print(str)
              if j==1 then
              GameData[assert(funType[j](str))]={}
              GameDataName=assert(funType[j](str))
              else
               GameData[GameDataName][FieldName[j]] = assert(funType[j](str))
              end
         end
       end
    end
    __gge.print(false,7,"\t\t\t[")
    __gge.print(false,10,"成功")
    __gge.print(false,7,"]\n")
    __gge.print(false,7,"-------------------------------------------------------------------------\n")
   return GameData
end
function Split(szFullString, szSeparator)
        local nFindStartIndex = 1
        local nSplitIndex = 1
        local nSplitArray = {}
        while true do
           local nFindLastIndex = string.find(szFullString, szSeparator, nFindStartIndex)
           if not nFindLastIndex then
            nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, string.len(szFullString))
            break
           end
           nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, nFindLastIndex - 1)
           nFindStartIndex = nFindLastIndex + string.len(szSeparator)
           nSplitIndex = nSplitIndex + 1
        end
        return nSplitArray
end
function toTablenumber(value)
    value =assert(string.gsub(value,"^[%s{]*(.-)[%s}]*$", "%1"))
    local a = Split(value,",")
    local b={}
    for k,v in pairs(a) do
        b[k]=assert(tonumber(v))
    end 
     return b
end
function toTable(value)
    -- print(value)
    value =string.gsub(value,"^[%s{]*(.-)[%s}]*$", "%1")

     return Split(value,",")
end

--=============================================================================================
function classex(...)
	local class_type 	= {}--创建的对象
	class_type.super 	= {...}
	class_type.初始化 	= false
	class_type.销毁 		= false
	class_type.创建 	= function(...)
		local ctor = {}--对像真身
			setmetatable(ctor,{ __index = _class[class_type] })--继承类属性和函数
			do--获得初始化属性
				local create;
				create = function(c,...)
					if #c.super > 0	then--继承属性
						for i,v in ipairs(c.super) do
						 	create(v,...)
						end
					end
					if c.初始化 then c.初始化(ctor,...) end
				end
				create(class_type,...)--递归所有父类
			end
			function ctor:运行父函数(id,name,...)
				if _class[class_type.super[id]] and _class[class_type.super[id]][name] then
				    return _class[class_type.super[id]][name](self,...)
				end
			end
		local obj 	= {}--用于检测对象销毁
			obj.__gc = ffi.gc(ffi.new('char[1]'),function ()
				local destroy
				destroy = function(c)
					if #c.super > 0	then
						for i,v in ipairs(c.super) do
						 	destroy(v)
						end
					end
					if c.销毁 then c.销毁(ctor) end
				end
				destroy(class_type)--递归所有父类
				ctor = nil
			end)
		local cmt 	= getmetatable(ctor)--ctormetatable
		local omt 	= {}				--objmetatable
			for k,v in pairs(cmt) do--获得用户修改的mt
				omt[k] = v
			end
			omt.__index 	= ctor
			omt.__newindex 	= ctor
			setmetatable(obj,omt)
		return obj
	end
	_class[class_type] = {}--类函数实际保存
	local mt = {}
	mt.__newindex 	= _class[class_type]
	mt.__call		= function (t,...)
		return t.创建(...)
	end
	-- mt.__tostring 	= function ()
	-- 	return "ggeclass"
	-- end
	setmetatable(class_type,mt)
	if #class_type.super > 0 then--继承函数
		setmetatable(_class[class_type],{__index = function (t,k)
		    for i,v in ipairs(class_type.super) do
		        local ret = _class[v][k]
		        if ret then
		            return ret
		        end
		    end
		end})
	end
	return class_type
end


__颜色 = {
	深蓝 = 1,
	深绿 = 2,
	深青 = 3,
	深红 = 4,
	深紫 = 5,
	深黄 = 6,
	深白 = 7,
	深灰 = 8,

	蓝色 = 9,
	绿色 = 10,
	青色 = 11,
	红色 = 12,
	紫色 = 13,
	黄色 = 14,
	白色 = 15
}
__SO ={--错误类型
	'HP_SO_UNKNOWN'	,	-- 未知
	'HP_SO_ACCEPT'	,	-- 请求
	'HP_SO_CONNECT'	,	-- 连接
	'HP_SO_SEND'	,	-- 发送
	'HP_SO_RECEIVE'	,	-- 数据
	[100] = '非法封包1',--普通
	[101] = '非法封包2',--压缩
	[103] = '非法封包3',--指针
	[104] = '非法封包4',--GGE
	[105] = '非法封包5',--备用
	[106] = '数据丢失',
	[107] = '数据错误',
	[108] = '序号错误'
}
