

--游戏出错时会调用 __gge.traceback ,如果想让游戏继续运行,请删除__gge.traceback函数
__gge.safecall = function (func,...)
    local args = { ... };
	local ret = {xpcall(function() return func(unpack(args,1,table.maxn(args))) end, __gge.traceback)}

	if ret[1] then
	    return unpack(ret, 2,table.maxn(ret))
	end
	return false
end
local _print = print
function print( ... )
	local args = { ... };
	for i=1,table.maxn(args) do
		if type(args[i])=='string' and #args[i]%2 ~= 0 then
		    args[i] = args[i]..' '
		end
	end
	_print(unpack(args, 1, table.maxn(args)))
end

local errored--只显示一次
local function tracebackex()
	local ret = ""
	local level = 3
	ret = ret .. "stack traceback:\n"
	while true do
		--get stack info
		local info = debug.getinfo(level, "Sln")
		if not info then break end
		if info.what == "C" then                -- C function
			ret = ret .. string.format("\t[C]: in function '%s'\n", info.name or "")
		else           -- Lua function
			ret = ret .. string.format("\t%s:%d: in function '%s'\n", info.short_src, info.currentline, info.name or "")
		end
		--get local vars
		local i = 1
		while true do
			local name, value = debug.getlocal(level, i)
			if not name then break end
			if name~='self' and name~='(*temporary)' then
				value =  tostring(value)
				if #value >100 then
						value = value:sub(1,100).." ……"
				end
					ret = ret .. string.format("\t\t%s\t= %s\n", name,value)
			end

			i = i + 1
		end
		level = level + 1
	end
	return ret
end

if __gge.isdebug and #__gge.command >0 then
	__gge.traceback = function (msg)
		if not errored then
			print("-----------------------------------------------------------------")
			if msg then
					print(tostring(msg) .. "--按F4或双击此行可转到错误代码页--")
				print(">>>>>>>>>>>>>>>>>>>>>>>>>以下为错误跟踪<<<<<<<<<<<<<<<<<<<<<<<<<<")
			end
			-- print(debug.traceback('',2))
			-- print("-----------------------------------------------------------------")
			print(tracebackex())
			引擎.更新函数,引擎.渲染函数 = nil,nil
			if 引擎 then 引擎.关闭() end
			errored = true
		end
	end
else
	错误关闭数=0
	function __gge.traceback(msg)
		if not errored then
		    if tp==nil or (tp~=nil and tp.进程 ~= 6) then
				_gge.messagebox(tostring(msg),"游戏出错啦!",16)
				更新函数,渲染函数 = nil,nil
				循环函数 = nil
				if 引擎 then 引擎.关闭() end
				return false
		    end
			错误关闭数=错误关闭数+1
			if tp~=nil then
			    tp.窗口.聊天框类:添加文本("#gm@@/#R/"..tostring(msg).."#Y/请截图给#G/GM#Y/,谢谢".." 错误第"..错误关闭数.."次!")
			end
		    if 错误关闭数>70 then
				_gge.messagebox(tostring(msg).."并且错误数量到达70","游戏出错啦!",16)
				更新函数,渲染函数 = nil,nil
				循环函数 = nil
				引擎.关闭()
				if 引擎 then 引擎.关闭() end
		    end
		    return false
		end

	end
end
--运行一个函数,如果函数内有错误,游戏也不崩溃
__gge.pcall = function (fun,...)
	local ret = {pcall(fun,...)}
	if ret[1] and ret[2] then
		return unpack(ret, 2)
	end
end





