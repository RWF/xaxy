--======================================================================--
--该 文件由GGELUA创建
--
--作者：baidwwy  创建日期：2014-05-17 16:26:08
--======================================================================--
local __was = require "glow.was"

local WAS类 = class()


function WAS类:初始化(文件,长度,directory,file)
	self.was = __was()
	self.was:SetL(__gge.state)
	if self.was:OpenFile(文件,长度 or 0) then
		local head = self.was:GetHeaderInfo()
		self.方向数 = head.Group
		self.x = head.Key_X
		self.y = head.Key_Y
		self.帧数 = head.Frame
		self.宽度 =	head.Width
		self.高度 = head.Height
	else
		--print("打开失败->"..文件)
		error(string.format("文件[%s]打开[%s]失败请检查数据表配置是否正确",directory,file))
	end

end
function WAS类:置调色板(文件)
	self.was:SetPal(文件)
	return self
end
function WAS类:调色(...)
	self.was:ChangePal(...)
end
function WAS类:取纹理(fid)
	 if fid <self.方向数* self.帧数  then
		local 纹理 =  require ("gge纹理类").创建(self.was:GetPic(fid))
		local t = self.was:GetFrameInfo()
		t.纹理 = 纹理
			t.纹理 =  (t.Width ==0 and t.Height)  and require("gge纹理类")():空白纹理() or 纹理




		return t
	end 
	return {纹理=require("gge纹理类")():空白纹理(),Key_X=0,Key_Y=0}
end


function WAS类:取精灵(fid)
	local t = self:取纹理(fid)
	local 精灵 = require("gge精灵类").创建(t.纹理)
	t.精灵 = 精灵
	return t
end

return WAS类