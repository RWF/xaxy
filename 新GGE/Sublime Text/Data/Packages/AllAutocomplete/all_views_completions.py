# Extends Sublime Text autocompletion to find matches in all open
# files. By default, Sublime only considers words from the current file.

import sublime_plugin
import sublime
import re
import time
from os.path import basename

# limits to prevent bogging down the system
# 匹配最小单词长度
MIN_WORD_SIZE = 2
# 匹配最大单词长度
MAX_WORD_SIZE = 50

# 匹配窗口最大数量
MAX_VIEWS = 20
# 匹配字段最大数量
MAX_WORDS_PER_VIEW = 1500
# 每个窗口匹配最大用时
MAX_FIX_TIME_SECS_PER_VIEW = 0.01

def single_get_first(unicode1):
  str1 = unicode1.encode('gbk')
  try:
    ord(str1)
    return unicode1
  except:
    asc = str1[0] * 256 + str1[1]
    if asc >= 0xB0A1 and asc <= 0xB0C4:
      return 'a'
    if asc >= 0XB0C5 and asc <= 0XB2C0:
      return 'b'
    if asc >= 0xB2C1 and asc <= 0xB4ED:
      return 'c'
    if asc >= 0xB4EE and asc <= 0xB6E9:
      return 'd'
    if asc >= 0xB6EA and asc <= 0xB7A1:
      return 'e'
    if asc >= 0xB7A2 and asc <= 0xB8c0:
      return 'f'
    if asc >= 0xB8C1 and asc <= 0xB9FD:
      return 'g'
    if asc >= 0xB9FE and asc <= 0xBBF6:
      return 'h'
    if asc >= 0xBBF7 and asc <= 0xBFA5:
      return 'j'
    if asc >= 0xBFA6 and asc <= 0xC0AB:
      return 'k'
    if asc >= 0xC0AC and asc <= 0xC2E7:
      return 'l'
    if asc >= 0xC2E8 and asc <= 0xC4C2:
      return 'm'
    if asc >= 0xC4C3 and asc <= 0xC5B5:
      return 'n'
    if asc >= 0xC5B6 and asc <= 0xC5BD:
      return 'o'
    if asc >= 0xC5BE and asc <= 0xC6D9:
      return 'p'
    if asc >= 0xC6DA and asc <= 0xC8BA:
      return 'q'
    if asc >= 0xC8BB and asc <= 0xC8F5:
      return 'r'
    if asc >= 0xC8F6 and asc <= 0xCBF0:
      return 's'
    if asc >= 0xCBFA and asc <= 0xCDD9:
      return 't'
    if asc >= 0xCDDA and asc <= 0xCEF3:
      return 'w'
    if asc >= 0xCEF4 and asc <= 0xD188:
      return 'x'
    if asc >= 0xD1B9 and asc <= 0xD4D0:
      return 'y'
    if asc >= 0xD4D1 and asc <= 0xD7F9:
      return 'z'
    return ''

#取中文拼音首字母
def multi_get_letter(str_input):
  return_list = []
  for one_unicode in str_input:
    return_list.append(single_get_first(one_unicode))
  return  ''.join(return_list)

def plugin_loaded():
    global settings
    settings = sublime.load_settings('All Autocomplete.sublime-settings')

class AllAutocomplete(sublime_plugin.EventListener):

    def on_query_completions(self, view, prefix, locations):
        if is_disabled_in(view.scope_name(locations[0])):
            return []

        words = []

        # Limit number of views but always include the active view. This
        # view goes first to prioritize matches close to cursor position.
        other_views = [v for v in sublime.active_window().views() if v.id != view.id]
        views = [view] + other_views
        views = views[0:MAX_VIEWS]

        for v in views:
            if len(locations) > 0 and v.id == view.id:
                view_words = v.extract_completions(prefix, locations[0])
            else:
                view_words = v.extract_completions(prefix)

            #遍历含中文的字段
            a = []
            v.find_all('(--.*\n)?[\s\.]*([\u4E00-\u9FA5+_?\.?:?a-zA-Z0-9]+)', 0, "$2", a)
            view_words.extend(a)

            #遍历含中文的 self.XX self:XX
            b = []
            v.find_all('(self[\.:]*[\u4E00-\u9FA5+_?a-zA-Z0-9]+)', 0, "$1", b)
            view_words.extend(b)

            #去除重复字段
            view_words = list(set(view_words))
            #输出匹配到的字段数量
            #print('匹配字段数量',len(view_words))

            view_words = filter_words(view_words)
            view_words = fix_truncation(v, view_words)
            words += [(w, v) for w in view_words]

        words = without_duplicates(words)
        matches = []
        for w, v in words:
            trigger = w
            contents = w.replace('$', '\\$')
            if v.file_name():
                #修改联想
                trigger = multi_get_letter(contents)
                trigger += '\t %s ' % contents
            matches.append((trigger, contents))
        return matches


def is_disabled_in(scope):
    excluded_scopes = settings.get("exclude_from_completion", [])
    for excluded_scope in excluded_scopes:
        if scope.find(excluded_scope) != -1:
            return True
    return False

def filter_words(words):
    words = words[0:MAX_WORDS_PER_VIEW]
    return [w for w in words if MIN_WORD_SIZE <= len(w) <= MAX_WORD_SIZE]


# keeps first instance of every word and retains the original order
# (n^2 but should not be a problem as len(words) <= MAX_VIEWS*MAX_WORDS_PER_VIEW)
def without_duplicates(words):
    result = []
    used_words = []
    for w, v in words:
        if w not in used_words:
            used_words.append(w)
            result.append((w, v))
    return result


# Ugly workaround for truncation bug in Sublime when using view.extract_completions()
# in some types of files.
def fix_truncation(view, words):
    fixed_words = []
    start_time = time.time()
    for i, w in enumerate(words):
        #The word is truncated if and only if it cannot be found with a word boundary before and after

        # this fails to match strings with trailing non-alpha chars, like
        # 'foo?' or 'bar!', which are common for instance in Ruby.
        match = view.find(r'\b' + re.escape(w) + r'\b', 0)
        truncated = is_empty_match(match)
        if truncated:
            #Truncation is always by a single character, so we extend the word by one word character before a word boundary
            extended_words = []
            view.find_all(r'\b' + re.escape(w) + r'\w\b', 0, "$0", extended_words)
            if len(extended_words) > 0:
                fixed_words += extended_words
            else:
                # to compensate for the missing match problem mentioned above, just
                # use the old word if we didn't find any extended matches
                fixed_words.append(w)
        else:
            #Pass through non-truncated words
            fixed_words.append(w)

        # if too much time is spent in here, bail out,
        # and don't bother fixing the remaining words
        if time.time() - start_time > MAX_FIX_TIME_SECS_PER_VIEW:
            return fixed_words + words[i+1:]

    return fixed_words


if sublime.version() >= '3000':
    def is_empty_match(match):
        return match.empty()
else:
    plugin_loaded()
    def is_empty_match(match):
        return match is None
