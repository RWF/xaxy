-- @Author: 作者QQ414628710
-- @Date:   2022-01-15 20:06:48
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-19 22:56:02
local 商店道具 ={
  --飞儿
  [0]={"墨杆金钩","黄金钺","黄金剑","鱼骨双剑","云龙绸带","玄冰刺","劈水扇","幽路引魂","破甲战锤","玉竹金铃","赤炎环","破天宝刀","金错巨刃","琳琅盖","如意宫灯","夜灵珠","鹰眼法杖","玉腰弯弓","高速之星","绿靴","钢甲","金缕羽衣","羊角盔","媚狐头饰","乱牙咬"},
  --0级武器
  [1]={"折扇","红缨枪","牛皮鞭","曲柳杖","铁爪","松木锤","琉璃珠","双短剑","青铜短剑","柳叶刀","青铜斧","五色缎带","黄铜圈","硬木弓","细木棒","钝铁重剑","素纸灯","油纸伞"},
  --10级武器
  [2]={"铁骨扇","曲尖枪","牛筋鞭","红木杖","天狼爪","镔铁锤","水晶珠","镔铁双剑","铁齿剑","苗刀","开山斧","幻彩银纱","精钢日月圈","铁胆弓","金丝魔棒","桃印铁刃","竹骨灯","红罗伞"},
  --20级武器
  [3]={"精钢扇","锯齿矛","乌龙鞭","白椴杖","幽冥鬼爪","八棱金瓜","珍宝珠","龙凤双剑","吴越剑","夜魔弯刀","双面斧","金丝彩带","离情环","紫檀弓","玉如意","赭石巨剑","红灯笼","紫竹伞"},
  --30级武器
  [4]={"铁面扇","乌金三叉戟","钢结鞭","墨铁拐","青龙牙","狼牙锤","翡翠珠","竹节双剑","青锋剑","金背大砍刀","双弦钺","无极丝","金刺轮","宝雕长弓","点金棒","壁玉长铗","鲤鱼灯","锦绣椎"},
  --40级武器
  [5]={"百折扇","火焰枪","蛇骨鞭","玄铁牛角杖","勾魂爪","烈焰锤","莲华珠","狼牙双剑","龙泉剑","雁翅刀","精钢禅钺","天蚕丝带","风火圈","錾金宝弓","云龙棒","青铜古剑","芙蓉花灯","幽兰帐"},
   --长安防具
  [6]={"方巾","簪子","布衣","布裙","布鞋","面具","梅花簪子","鳞甲","五彩裙","马靴"},
   --长安饰品
  [7]={"银腰带","珍珠链","腰带","护身符"},
   --傲来防具
  [8]={"纶巾","珍珠头带","水晶腰带","侠客履","龙鳞羽衣","玉树腰带","锁子甲","骷髅吊坠","苍魂珠"},
   --建业防具
  [9]={"方巾","簪子","布衣","布裙","布鞋","护身符","腰带","布帽","玉钗","皮衣","丝绸长裙","牛皮靴","五色飞石","缎带"},
   --长安药店
  [10]={"天不老","紫石英","鹿茸","血色茶花","六道轮回","熊胆","凤凰尾","硫磺草","龙之心屑","火凤之睛","丁香水","月星子","仙狐涎","地狱灵芝","麝香","血珊瑚","餐风饮露","白露为霜","天龙水","孔雀红"},
   --长寿防具
  [11]={"九宫坠","白面狼牙","江湖夜雨","凤头钗","缨络丝带","神行靴","琥珀腰链","紧身衣","天香披肩"},
   --建业杂货店
  [12]={"摄妖香","洞冥草"},
   --罗道人
  [13]={"飞行符","白色导标旗","红色导标旗","黄色导标旗","蓝色导标旗","绿色导标旗","白色合成旗","红色合成旗","黄色合成旗","蓝色合成旗","绿色合成旗"},
   --建业药店
  [14]={"四叶花","紫丹罗","血色茶花","熊胆","丁香水","麝香","金创药","佛光舍利子","佛手","灵脂","曼佗罗花","白玉骨头","天青地白"},
  --云游道人
  [16]={"高级召唤兽内丹","高级魔兽要诀","红宝石","绿宝石","蓝宝石","黄宝石","光芒石","月亮石","太阳石","舍利子","红玛瑙","黑宝石","神秘石"},
  --宝石商人
  [17]={"清灵净瓶","玉葫灵髓","易经丹","召唤兽内丹","红宝石","绿宝石","蓝宝石","黄宝石","光芒石","月亮石","太阳石","舍利子","红玛瑙","黑宝石","神秘石"},
}
local 灵饰类型= {"手镯","佩饰","戒指","耳饰"}
local ShopControl =class()
function ShopControl:初始化( )
self.ShopGroup={}
  for n=0,14 do
    self.ShopGroup[n]={}
    for i=1,#商店道具[n] do
       self.ShopGroup[n][i]=AddItem(商店道具[n][i])
    end
  end
self.ShopGroup[15]={
  --AddItem("高级召唤兽内丹",nil,nil,nil,180000000),
  -- AddItem("高级魔兽要诀",nil,"随机技能",nil,28888888,nil,"折扣"),
  AddItem("红宝石",0,nil,nil,8000000),
  AddItem("绿宝石",0,nil,nil,8000000),
  AddItem("蓝宝石",0,nil,nil,8000000),
  AddItem("黄宝石",0,nil,nil,8000000),
  AddItem("光芒石",0,nil,nil,8000000),
  AddItem("月亮石",0,nil,nil,8000000),
  AddItem("太阳石",0,nil,nil,8000000),
  AddItem("舍利子",0,nil,nil,8000000),
  AddItem("红玛瑙",0,nil,nil,8000000),
  AddItem("黑宝石",0,nil,nil,8000000),
  AddItem("神秘石",0,nil,nil,8000000),
 }
 self.ShopGroup[16]={
  AddItem("召唤兽内丹",nil,"随机技能",nil,8000000),
  AddItem("易经丹",nil,nil,nil,45000000),
  AddItem("清灵净瓶",nil,nil,nil,25000000),
  AddItem("玉葫灵髓",nil,nil,nil,25000000),
  AddItem("红宝石",1,nil,nil,300000),
  AddItem("绿宝石",1,nil,nil,300000),
  AddItem("蓝宝石",1,nil,nil,300000),
  AddItem("黄宝石",1,nil,nil,300000),
  AddItem("光芒石",1,nil,nil,300000),
  AddItem("月亮石",1,nil,nil,300000),
  AddItem("太阳石",1,nil,nil,300000),
  AddItem("舍利子",1,nil,nil,300000),
  AddItem("红玛瑙",1,nil,nil,300000),
  AddItem("黑宝石",1,nil,nil,300000),
  AddItem("神秘石",1,nil,nil,300000),
  AddItem("钢甲",nil,nil,nil,3000000),
  AddItem("羊角盔",nil,nil,nil,3000000),
  AddItem("金缕羽衣",nil,nil,nil,3000000),
  AddItem("媚狐头饰",nil,nil,nil,3000000),
  AddItem("乱牙咬",nil,nil,nil,3000000),
 }
 self.ShopGroup[0]={
  AddItem("墨杆金钩",nil,nil,nil,3000000),
  AddItem("黄金钺",nil,nil,nil,3000000),
  AddItem("黄金剑",nil,nil,nil,3000000),
  AddItem("鱼骨双剑",nil,nil,nil,3000000),
  AddItem("云龙绸带",nil,nil,nil,3000000),
  AddItem("玄冰刺",nil,nil,nil,3000000),
  AddItem("劈水扇",nil,nil,nil,3000000),
  AddItem("幽路引魂",nil,nil,nil,3000000),
  AddItem("破甲战锤",nil,nil,nil,3000000),
  AddItem("玉竹金铃",nil,nil,nil,3000000),
  AddItem("赤炎环",nil,nil,nil,3000000),
  AddItem("破天宝刀",nil,nil,nil,3000000),
  AddItem("金错巨刃",nil,nil,nil,3000000),
  AddItem("琳琅盖",nil,nil,nil,3000000),
  AddItem("如意宫灯",nil,nil,nil,3000000),
  AddItem("夜灵珠",nil,nil,nil,3000000),
  AddItem("鹰眼法杖",nil,nil,nil,3000000),
  AddItem("玉腰弯弓",nil,nil,nil,3000000),
  AddItem("高速之星",nil,nil,nil,3000000),
  AddItem("绿靴",nil,nil,nil,3000000),
 }
  self.ShopGroup[17]={}---灵饰商店
  local 灵饰序号 = 0
  for i=1,6 do
      for o=1,4 do
       灵饰序号 = 灵饰序号 +1
       self.ShopGroup[17][灵饰序号]=AddItem("灵饰指南书",(i*2+4)*10,灵饰类型[o],nil,12000000*2^(i-1))
    end
  end
  self.ShopGroup[18]={}---灵饰商店
  for i=1,6 do
     self.ShopGroup[18][i]=AddItem("元灵晶石",(i*2+4)*10,nil,nil,6000000*2^(i-1))
  end
   --古董商人



      self.ShopGroup[19]={
      AddItem("未鉴定的古董",nil,nil,nil,88888),
      AddItem("考古铁铲",nil,nil,nil,999999)
      }

 self.ShopGroup[38] = {--春节商城
 AddItem("天机培元丹",nil,nil,nil,1000000,nil),
 AddItem("九霄清心丸",nil,nil,nil,1000000,nil),}
 self.ShopGroup[28] = {AddItem("秘制红罗羹",nil,nil,nil,100000)} --酒店老板
 self.ShopGroup[39] = {--春节商城
{名称="绿色合成旗",地图="长寿村",坐标={[1]={y=179,x=131},[2]={y=125,x=135},[3]={y=102,x=109},[4]={y=132,x=35},[5]={y=48,x=44},[6]={y=86,x=99},[7]={y=22,x=105}},次数=140,价格=60000,编号=1070,类型="合成旗",地图编号=1070},
{名称="红色合成旗",地图="傲来国",坐标={[2]={y=18,x=204},[5]={y=56,x=29},[3]={y=142,x=154},[7]={y=26,x=63},[1]={y=49,x=122},[4]={y=122,x=50},[6]={y=12,x=15}},次数=140,价格=60000,编号=1092,类型="合成旗",地图编号=1092},
{名称="黄色合成旗",地图="长安城",坐标={[2]={y=241,x=281},[5]={y=117,x=459},[3]={y=139,x=497},[7]={y=254,x=61},[1]={y=114,x=219},[4]={y=35,x=442},[6]={y=266,x=494}},次数=140,价格=60000,编号=1001,类型="合成旗",地图编号=1001},
{名称="蓝色合成旗",地图="大唐境外",坐标={[1]={y=65,x=10},[2]={y=55,x=49},[3]={y=21,x=18},[4]={y=13,x=63},[5]={y=14,x=222},[6]={y=11,x=534},[7]={y=45,x=627}},次数=140,价格=60000,编号=1173,类型="合成旗",地图编号=1173},
{名称="白色合成旗",地图="朱紫国",坐标={[1]={y=26,x=140},[2]={y=24,x=17},[3]={y=111,x=14},[4]={y=100,x=61},[5]={y=109,x=145},[6]={y=64,x=94},[7]={y=52,x=160}},次数=140,价格=60000,编号=1208,类型="合成旗",地图编号=1208},
}
end

function ShopControl:GetNpcShop(id, 序号)
  self.发送消息 = {}
  序号 = 序号 + 0
  self.ShopGroup[序号].银子 = 0
  self.ShopGroup[序号].组号 = 序号
  self.ShopGroup[序号].银子 = UserData[id].角色.道具.货币.银子
  SendMessage(UserData[id].连接id, 9001, self.ShopGroup[序号])
end

function ShopControl:数据处理(user,parameter,number,text,Value)  
  if number == 1 then
     self:GetShopData(user,parameter,text)
  elseif  number == 2 then
    self:buy(user,parameter,text)
   elseif  number == 3 then
    self:NPCbuy(user,parameter,text,Value)
  end
end
local  GetMoneyForm =function(user,text)
    local money,age
   if text =="银子" then -- 活动积分
        money=user.角色.道具.货币.银子
        age ="银子"
    elseif text =="活动积分" then -- 活动积分
        money=user.角色.活动积分
        age ="活动积分"
    elseif text =="比武积分" then -- 比武积分
       money=user.角色.比武积分
       age ="比武积分"
    elseif text =="副本积分" then -- 副本积分
       money=user.角色.副本积分
       age ="副本积分"
    elseif text =="地煞积分" then -- 神器积分
        money=user.角色.地煞积分
         age ="地煞积分"
    elseif text =="知了积分" then -- 知了积分
        age ="知了积分"
        money=user.角色.知了积分
    elseif text =="天罡积分" then -- 天罡积分
        age ="天罡积分"
        money=user.角色.天罡积分
    elseif text =="单人积分" then -- 单人积分
        age ="单人积分"
        money=user.角色.单人积分
    elseif text =="成就积分" then -- 成就积分
        age ="成就积分"
        money=user.角色.成就积分
    elseif text =="特殊积分" then -- 特殊积分
        age ="特殊积分"
        money=user.角色.特殊积分
    elseif text =="活跃度" then -- 活动积分
        age ="活跃度"
        money=user.角色.活跃度
    else
        money= f函数.读配置(ServerDirectory.."玩家信息/账号" .. user.账号 .. "/账号.txt", "账号信息", "仙玉") + 0
        age ="仙玉"
    end
    return money,age
end
function ShopControl:GetShopData(user,min,text)  
  if not merchandise[text] or min < 1 then
     print(string.format("玩家ID%d请求一个错误的商城组%s",user.id,text))
     return 
  elseif   min> #merchandise[text] then
    SendMessage(user.连接id,7,"#Y/已经是最后一页了") 
    return
  end
    local age ={}
    local delcount= (text=="锦衣" or text=="光环" or text=="脚印"or text=="祥瑞"or text=="定制"or text=="传音") and  20 or  32

    local del= text=="召唤兽" and 12 or delcount
    for i=1,del do
        if  merchandise[text][i+min-1] then
         age[i] =ShopData[merchandise[text][i+min-1]]
         age[i].number =merchandise[text][i+min-1]
        else 
         break
       end
    end
    age.银子 =GetMoneyForm(user,text)
    age.ID =text
    age.min =min
     if text=="活跃度" then
         SendMessage(user.连接id,20056,age)
     else 
        SendMessage(user.连接id,20034,age)
     end
  end
function ShopControl:buy(user,number,text)
   local  count =assert(tonumber(text),"商城传入的数量错误"..text)
   count= math.floor(count) ---防止小数点 网关已经加过检测 所以可以屏蔽
   if not ShopData[number] then
       __S服务:输出(string.format("ID%s为传入一个错误商品编号%s",user.id,number))
       return
   elseif count < 1 then
        return  
   end
     local  money,age =GetMoneyForm(user,ShopData[number].分类)
    local  Cyclevalue = ShopData[number].数量 and 1 or count
     local  TempCount =  ShopData[number].数量 and count or 1
    for i=1,Cyclevalue do 
       if  money < ShopData[number].价格*TempCount then
           SendMessage(user.连接id,7,string.format("#Y/您的%s不足",age))
           return  
       elseif  ShopData[number].分类 =="召唤兽" then
            if  #user.召唤兽.数据>= user.召唤兽.数据.召唤兽上限   then
                  SendMessage(user.连接id,7,"#Y/召唤兽已经达到上限")
                  return
            end
            if  RoleControl:扣除仙玉(user,ShopData[number].价格,"商城购买"..ShopData[number].名称) then
                 table.insert(user.召唤兽.数据,AddPet(ShopData[number].名称,ShopData[number].类型))
                 user.召唤兽:刷新属性(#user.召唤兽.数据)
            else
                 SendMessage(user.连接id,7,"#Y/你没有足够的仙玉！")
                return 
            end
       elseif  RoleControl:取可用道具格子(user,"包裹") == 0 then
           SendMessage(user.连接id, 7, "#Y/您身上似乎没有多余的道具存放空间")
           return
       else 
          local  RoseTempID  =RoleControl:取可用道具格子(user,"包裹")
           local  ItemTempID =ItemControl:取道具编号(user.id)
          user.角色.道具.包裹[RoseTempID] = ItemTempID
           if ShopData[number].技能 and  ShopData[number].技能=="随机技能" then
            user.物品[ItemTempID] = AddItem(ShopData[number].名称,ShopData[number].等级) 
           elseif ShopData[number].种类 and  ShopData[number].种类=="随机种类" then
            user.物品[ItemTempID] = AddItem(ShopData[number].名称,ShopData[number].等级) 
           elseif ShopData[number].等级 and  ShopData[number].等级==0 then
            user.物品[ItemTempID] = AddItem(ShopData[number].名称,math.random(1,8),ShopData[number].技能) 
           else 

            user.物品[ItemTempID] = table.copy(ShopData[number])

           end
            if user.物品[ItemTempID].Time then
                 user.物品[ItemTempID].Time=os.time()+3600*48
            end

            if ShopData[number].数量 then
                  user.物品[ItemTempID].数量  =count 
                  RoleControl["扣除"..age](RoleControl,user,ShopData[number].价格*count,"商城")
            else 
                  RoleControl["扣除"..age](RoleControl,user,ShopData[number].价格,"商城")
            end

            money=money- ShopData[number].价格
            SendMessage(user.连接id, 3007,{格子=RoseTempID,数据=user.物品[ItemTempID]})
       end
    end
    if ShopData[number].分类=="活跃度" then
       SendMessage(user.连接id,20057,GetMoneyForm(user,ShopData[number].分类)) 
    else
      SendMessage(user.连接id,20043,GetMoneyForm(user,ShopData[number].分类)) 
    end
    
    local Message = string.format("花费了%d点%s购买了%d个%s",ShopData[number].价格*count,age,count,ShopData[number].名称)
    SendMessage(user.连接id,7,"#Y/你"..Message)
    if ShopData[number].分类~="银子" then
      广播消息(9,"#jj/#G/"..user.角色.名称.."("..user.id..")#Y/在商城"..Message)
    end
end
function ShopControl:NPCbuy(user,number,text,Value)
  local  count =assert(tonumber(text),"商城传入的数量错误"..text)
  Value=Value+0
   count= math.floor(count) ---防止小数点 网关已经加过检测 所以可以屏蔽
   if not self.ShopGroup[Value][number] then
       __S服务:输出(string.format("ID%s为传入一个错误商品编号%s",user.id,number))
       return
   elseif count < 1 then
        return  
   end

  if Value == 15 then
    if count > 1 then
      SendMessage(user.连接id, 7, "#y/本商品每次只能购买1本")
      return 0
    elseif self.ShopGroup[Value][number].名称 == "高级魔兽要诀" and 道人数据.兽诀 <= 0 then
      SendMessage(user.连接id, 7, "#y/这种商品已经卖完了，请在整点的时候再来买")
      return 0
    elseif self.ShopGroup[Value][number].名称 == "高级召唤兽内丹" and 道人数据.内丹 <= 0 then
      SendMessage(user.连接id, 7, "#y/这种商品已经卖完了，请在整点的时候再来买")
      return 0
    elseif self.ShopGroup[Value][number].名称 ~= "高级魔兽要诀" and self.ShopGroup[Value][number].名称 ~= "无字天书" and 道人数据.宝石 <= 0 then
      SendMessage(user.连接id, 7, "#y/这种商品已经卖完了，请在整点的时候再来买")
      return 0
    end
  end

    local  Cyclevalue = self.ShopGroup[Value][number].数量 and 1 or count
    local  TempCount = self.ShopGroup[Value][number].数量 and count or 1
    for i=1,Cyclevalue do 
       if  user.角色.道具.货币.银子 < self.ShopGroup[Value][number].价格*TempCount then
           SendMessage(user.连接id,7,"#Y/您的银子不足")
           return  
       elseif  RoleControl:取可用道具格子(user,"包裹") == 0 then
           SendMessage(user.连接id, 7, "#Y/您身上似乎没有多余的道具存放空间")
           return
       else 
           local  RoseTempID  =RoleControl:取可用道具格子(user,"包裹")
           local  ItemTempID =ItemControl:取道具编号(user.id)
          user.角色.道具.包裹[RoseTempID] = ItemTempID

           if self.ShopGroup[Value][number].技能 and  self.ShopGroup[Value][number].技能=="随机技能" then
            user.物品[ItemTempID] = AddItem(self.ShopGroup[Value][number].名称,self.ShopGroup[Value][number].等级)
          elseif self.ShopGroup[Value][number].等级 and  self.ShopGroup[Value][number].等级==0 then
            user.物品[ItemTempID] = AddItem(self.ShopGroup[Value][number].名称,math.random(1,8),self.ShopGroup[Value][number].技能) 
 
           else 
            user.物品[ItemTempID] = table.copy(self.ShopGroup[Value][number])
           end
          if Value == 15 then
               if user.物品[ItemTempID].名称 == "高级魔兽要诀" then
                self.临时名称 = 取道人兽诀名称()
                广播消息(9, "#jj/#g/ " .. user.角色.名称 .. "#y/在云游道人试试手气处购买了一本#r/" .. self.临时名称 .. "#y/兽诀")
                user.物品[ItemTempID].技能 = self.临时名称
                道人数据.兽诀 = 道人数据.兽诀 - 1
              elseif user.物品[ItemTempID].名称=="高级召唤兽内丹" then
                   self.临时名称=取内丹("高级")
                    广播消息(9,"#jj/#g/ "..user.角色.名称.."#y/云游道人处购买了一颗#r/"..self.临时名称.."#y/内丹")
                         user.物品[ItemTempID].技能=self.临时名称
                      道人数据.内丹 = 道人数据.内丹 - 1
              else
                self.临时名称 = math.random(2, 8)
                广播消息(9, "#jj/#g/ " .. user.角色.名称 .. "#y/在云游道人处购买了一颗#r/" .. self.临时名称 .. "#y/级的" .. self.ShopGroup[Value][number].名称)
                user.物品[ItemTempID].等级 = self.临时名称
                道人数据.宝石 = 道人数据.宝石 - 1
              end
         end
        if user.物品[ItemTempID].名称 == "考古铁铲" then
              local 宝图地图 = {1501,1506,1070,1091,1092,1110,1173,1146,1142,1131,1140,1512,1513,1514,1174}
               user.物品[ItemTempID].地图编号=宝图地图[math.random(1,#宝图地图)]
                local 临时坐标= MapControl:Randomloadtion( user.物品[ItemTempID].地图编号)
                 user.物品[ItemTempID].x=临时坐标.x
                 user.物品[ItemTempID].y=临时坐标.y  
        end
            if self.ShopGroup[Value][number].数量 then
                  user.物品[ItemTempID].数量  =count  
                   RoleControl:扣除银子(user,self.ShopGroup[Value][number].价格*count,"商城")
            else 
               RoleControl:扣除银子(user,self.ShopGroup[Value][number].价格,"商城")
            end


            SendMessage(user.连接id, 3007,{格子=RoseTempID,数据=user.物品[ItemTempID]})
       end
    end
    SendMessage(user.连接id,9002,user.角色.道具.货币.银子)
    local Msg= string.format("#Y/您花费了%d点%s购买了%d个%s",self.ShopGroup[Value][number].价格*count,"银子",count,self.ShopGroup[Value][number].名称)

    SendMessage(user.连接id,7,Msg)
end
return ShopControl