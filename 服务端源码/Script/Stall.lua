local stall = class()
local 随机摆摊 ={"环装大甩卖","炼妖宠","C66月华","人不在代价","炼妖石点化","神兽大甩卖","便宜处理","寻69团队","地煞求固定队","杂货","乐器花环","宝图便宜","特殊令牌甩","魔兽要诀","免费打造","高级兽决秒价","人不在秒价","一梦十年","火爆新区","军火供应","炼妖胚子甩货"}
function stall:数据处理(id, 序号, 参数, 内容,编号)
	if 序号 == 1 then
		self:发送摊位信息(id)
	elseif 序号 == 2 then
		self:商品上架(id, 参数 + 0, 内容)
	elseif 序号 == 3 then
		self:更改招牌(id, 内容)
	elseif 序号 == 4 then
		SendMessage(UserData[id].连接id, 20002, ItemControl:索要道具1(id, "包裹"))
	elseif 序号 == 5 then
		SendMessage(UserData[id].连接id, 20005, UserData[id].召唤兽:获取数据())
	elseif 序号 == 6 then
		self:SellPet(id, 参数 + 0, 内容)
	elseif 序号 == 7 then
		self:发送玩家摊位信息(id, 参数)
	elseif 序号 == 8 then
		self:购买道具(id, 参数, 内容)
	elseif 序号 == 9 then
		if UserData[id].摊位id == nil then
			SendMessage(UserData[id].连接id, 7, "#y/这样的摊位不存在")
			return 0
		else
			self.摊位id = UserData[id].摊位id

			if 摊位数据[self.摊位id] == nil or 摊位数据[self.摊位id].状态 == false or UserData[self.摊位id] == nil then
				SendMessage(UserData[id].连接id, 7, "#y/这样的摊位不存在")

				return 0
			end

			SendMessage(UserData[id].连接id, 20011, self:索取摊位道具(self.摊位id))

			UserData[id].查看摊位 = os.time()
		end
	elseif 序号 == 10 then
		if UserData[id].摊位id == nil then
			SendMessage(UserData[id].连接id, 7, "#y/这样的摊位不存在")

			return 0
		else
			self.摊位id = UserData[id].摊位id
			if 摊位数据[self.摊位id] == nil or 摊位数据[self.摊位id].状态 == false or UserData[self.摊位id] == nil then
				SendMessage(UserData[id].连接id, 7, "#y/这样的摊位不存在")
				return 0
			end
			SendMessage(UserData[id].连接id, 20013, self:索取摊位bb(self.摊位id))
			UserData[id].查看摊位 = os.time()
		end
	elseif 序号 == 11 then
		self:BuyPet(id, 参数, 内容)
	elseif 序号 == 12 then
		摊位数据[id]=nil
		UserData[id].摆摊 = nil
		MapControl.MapData[UserData[id].地图].摆摊人数=MapControl.MapData[UserData[id].地图].摆摊人数-1
		SendMessage(UserData[id].连接id, 7, "#y/收摊回家玩老婆咯！")
		SendMessage(UserData[id].连接id, 20008,"")
		MapControl:更新摊位(id, "", UserData[id].地图)
	elseif 序号 == 13 then
		if UserData[id].摊位id == nil then
			SendMessage(UserData[id].连接id, 7, "#y/这样的摊位不存在")
			return 0
		else
			self.摊位id = UserData[id].摊位id
			if 摊位数据[self.摊位id] == nil or 摊位数据[self.摊位id].状态 == false or UserData[self.摊位id] == nil then
				SendMessage(UserData[id].连接id, 7, "#y/这样的摊位不存在")
				return 0
			end
			SendMessage(UserData[id].连接id, 20039, self:索取摊位制造(self.摊位id))
			UserData[id].查看摊位 = os.time()
		end
	elseif 序号 == 14 then
		SendMessage(UserData[id].连接id, 20040, RoleControl:获取制造数据(UserData[id]))
	elseif 序号 == 15 then
		self:制造上架(id, 参数 + 0, 内容,编号)
	elseif 序号 == 16 then
		self:购买制造(id, 参数, 内容)
	end
end

function stall:更改招牌(id, 名称)
	摊位数据[id].名称 = 敏感文本替换(名称)
	摊位数据[id].摆摊 = id

	SendMessage(UserData[id].连接id, 7, "#y/更改招牌成功")
	SendMessage(UserData[id].连接id, 20008, 摊位数据[id].名称)
	MapControl:更新摊位(id, 名称, UserData[id].地图)
end

function stall:SellPet(id, 编号, 价格)
	if UserData[id].召唤兽.数据[编号] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/你没有这样的召唤兽")

		return 0
	elseif UserData[id].召唤兽.数据[编号].加锁 then
		SendMessage(UserData[id].连接id, 7, "#y/加锁的召唤兽无法上架")
		return 0
	elseif 取表数量(UserData[id].召唤兽.数据[编号].装备)  > 0 then
				SendMessage(UserData[id].连接id, 7, "#y/佩戴装备的召唤兽无法摆摊")
				return 0
	else
		self.操作编号 = self:取bb状态(id, 编号)
		if self.操作编号 == 0 then
			if 价格 + 0 < 1 then
				SendMessage(UserData[id].连接id, 7, "#y/商品的最低价格不能低于1两银子")
				return 0
			end
			摊位数据[id].bb[编号] = {
				id = 编号,
				价格 = 价格
			}
			SendMessage(UserData[id].连接id, 20006, 摊位数据[id].bb[编号])
		else
			摊位数据[id].bb[编号] = nil

			SendMessage(UserData[id].连接id, 20007, 编号)
		end
		摊位数据[id].刷新 = os.time()
	end
end
function stall:制造上架(id, 编号, 价格,技能id)
    技能id = 分割文本(技能id, "*-*")
    if 技能id[1]+0== 99  and UserData[id].角色.辅助技能[技能id[2]+0].等级<50 then 
    	SendMessage(UserData[id].连接id, 7, "#y/制造等级在50级以上才可以摆摊！")
	elseif 技能id[1]+0~= 99 and UserData[id].角色.师门技能[技能id[1]+0].包含技能[技能id[2]+0] == nil   then
		SendMessage(UserData[id].连接id, 7, "#y/你没有学会这项技能")
		return 0
	elseif 技能id[1]+0~= 99 and UserData[id].角色.师门技能[技能id[1]+0].包含技能[技能id[2]+0].学会==false then
		SendMessage(UserData[id].连接id, 7, "#y/你没有学会这项技能")
		return 0
	else
		self.操作编号 = self:取制造状态(id, 编号)
		if self.操作编号 == 0 then
			if 价格 + 0 < 1 then
				SendMessage(UserData[id].连接id, 7, "#y/商品的最低价格不能低于1两银子")
				return 0
			end
			摊位数据[id].制造[编号] = {
				id = 编号,
				主技能id = 技能id[1]+0,
				包含技能id = 技能id[2]+0,
				价格 = 价格+0
			}
			SendMessage(UserData[id].连接id, 20041, 摊位数据[id].制造[编号])
		else
			摊位数据[id].制造[编号] = nil
			SendMessage(UserData[id].连接id, 20042, 编号)
		end
		摊位数据[id].刷新 = os.time()
	end
end
function stall:BuyPet(id, 编号, 数量)
	if UserData[id].摊位id == nil then
		SendMessage(UserData[id].连接id, 7, "#y/这样的摊位不存在")
		return 0
	else
		self.摊位id = UserData[id].摊位id
		if 摊位数据[self.摊位id] == nil or 摊位数据[self.摊位id].状态 == false or UserData[self.摊位id] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/这样的摊位不存在")
			return 0
		elseif UserData[id].查看摊位 <= 摊位数据[self.摊位id].刷新 then
			SendMessage(UserData[id].连接id, 7, "#y/该摊位商品发生了变化，请稍后再试")
			SendMessage(UserData[id].连接id, 20013, self:索取摊位bb(self.摊位id))
			UserData[id].查看摊位 = os.time()
			return 0
		end
		if 摊位数据[self.摊位id].bb[编号] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/这样的商品不存在")
			SendMessage(UserData[id].连接id, 20013, self:索取摊位bb(self.摊位id))
			return 0
		end
		self.bbid = 摊位数据[self.摊位id].bb[编号].id
		self.购买价格 = 摊位数据[self.摊位id].bb[编号].价格 + 0

		if UserData[self.摊位id].召唤兽.数据[self.bbid] == nil or UserData[self.摊位id].召唤兽.数据[self.bbid] == 0 then
			SendMessage(UserData[id].连接id, 7, "#y/这样的商品不存在")
			SendMessage(UserData[id].连接id, 20013, self:索取摊位bb(self.摊位id))
			return 0
		elseif #UserData[id].召唤兽.数据 >= UserData[id].召唤兽.数据.召唤兽上限 then
			SendMessage(UserData[id].连接id, 7, "#y/您当前无法携带更多的召唤兽了")
			return 0
		elseif 银子检查(id, self.购买价格) == false then
			SendMessage(UserData[id].连接id, 7, "#y/您没有那么多的银子")

			return 0
		end
		RoleControl:扣除银子(UserData[id], self.购买价格, 29)
		UserData[id].召唤兽.数据[#UserData[id].召唤兽.数据 + 1] = table.loadstring(UserData[self.摊位id].召唤兽:获取指定数据(self.bbid))
		SendMessage(UserData[id].连接id, 7, "#w/你花费了#r/" .. self.购买价格 .. "#w/两银子购买了#g/" .. UserData[id].召唤兽.数据[#UserData[id].召唤兽.数据].名称)
		RoleControl:添加消费日志(UserData[id],"[摊位系统-购买]花费" .. self.购买价格 .. "两银子购买了" .. UserData[id].召唤兽.数据[#UserData[id].召唤兽.数据].名称 .. "，类型为" .. UserData[id].召唤兽.数据[#UserData[id].召唤兽.数据].造型 .. "，出售者为" .. UserData[self.摊位id].账号)
		RoleControl:添加消费日志(UserData[self.摊位id],"[摊位系统-出售]获得" .. self.购买价格 .. "两银子购买了" .. UserData[id].召唤兽.数据[#UserData[id].召唤兽.数据].名称 .. "，类型为" .. UserData[id].召唤兽.数据[#UserData[id].召唤兽.数据].造型 .. "，购买者为" .. UserData[id].账号)
		RoleControl:添加银子(UserData[self.摊位id],self.购买价格, 29)
		SendMessage(UserData[self.摊位id].连接id, 7, "#w/出售#g/" .. UserData[id].召唤兽.数据[#UserData[id].召唤兽.数据].名称 .. "#w/获得了#r/" .. self.购买价格 .. "#w/两银子")
      


 

    摊位数据[self.摊位id].bb[编号]=nil
     
    self.参战数据=UserData[self.摊位id].召唤兽.数据.参战
    self.原始数据={}
    UserData[self.摊位id].召唤兽.数据.参战=0
    for n=1,#UserData[self.摊位id].召唤兽.数据 do
        self.原始数据[n]={bb=table.copy(UserData[self.摊位id].召唤兽.数据[n]),编号=n}
    end
    table.remove(self.原始数据,self.bbid)
    
    self.记录数据={}
    UserData[self.摊位id].召唤兽.数据[#self.原始数据+1]=nil
    for n=1,#self.原始数据 do
    	UserData[self.摊位id].召唤兽.数据[n]=table.copy(self.原始数据[n].bb)
		if self.原始数据[n].编号==self.参战数据 then
			UserData[self.摊位id].召唤兽.数据.参战=n
		end
		for i=1,12 do
			if 摊位数据[self.摊位id].bb[i]~=nil and 摊位数据[self.摊位id].bb[i].id==self.原始数据[n].编号 then
			self.记录数据[n]={id=n,价格=摊位数据[self.摊位id].bb[i].价格}
			end
		end

      end
     --self.原始数据={}
     摊位数据[self.摊位id].bb={}

     for n=1,12 do

        if self.记录数据[n]~=nil then

         摊位数据[self.摊位id].bb[n]={id=self.记录数据[n].id,价格=self.记录数据[n].价格}


          end


       end
      

              -- table.remove(UserData[self.摊位id].召唤兽.数据,self.bbid)
   		

		SendMessage(UserData[self.摊位id].连接id, 20014, "11")
		SendMessage(UserData[id].连接id, 20013, self:索取摊位bb(self.摊位id))
		SendMessage(UserData[id].连接id, 3040, "6")
		SendMessage(UserData[self.摊位id].连接id, 3040, "6")
		SendMessage(UserData[self.摊位id].连接id, 2005, UserData[self.摊位id].召唤兽:取头像数据())
		SendMessage(UserData[self.摊位id].连接id, 20015, 摊位数据[self.摊位id].bb)
		摊位数据[self.摊位id].刷新 = os.time()
	end
end
function stall:购买制造(id, 编号, 数量)
	if UserData[id].摊位id == nil then
		SendMessage(UserData[id].连接id, 7, "#y/这样的摊位不存在")
		return 0
	else
		self.摊位id = UserData[id].摊位id
		if 摊位数据[self.摊位id] == nil or 摊位数据[self.摊位id].状态 == false or UserData[self.摊位id] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/这样的摊位不存在")
			return 0
		elseif UserData[id].查看摊位 <= 摊位数据[self.摊位id].刷新 then
			SendMessage(UserData[id].连接id, 7, "#y/该摊位商品发生了变化，请稍后再试")
			SendMessage(UserData[id].连接id, 20039, self:索取摊位制造(self.摊位id))
			UserData[id].查看摊位 = os.time()
			return 0
		end
		if 摊位数据[self.摊位id].制造[编号] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/这样的商品不存在")
			SendMessage(UserData[id].连接id, 20039, self:索取摊位制造(self.摊位id))
			return 0
		end
		self.bbid = 摊位数据[self.摊位id].制造[编号].id
		self.购买价格 = 摊位数据[self.摊位id].制造[编号].价格 + 0
		if 摊位数据[self.摊位id].制造[编号].主技能id== 99 then
			local 熟练度  = {[10]="打造",[11]="裁缝",[12]="炼金"} 
			UserData[id]["购买"..熟练度[摊位数据[self.摊位id].制造[编号].包含技能id]]=UserData[self.摊位id].角色.辅助技能[摊位数据[self.摊位id].制造[编号].包含技能id].等级
            SendMessage(UserData[id].连接id, 7, "#w/你花费了#r/" .. self.购买价格 .. "#w/两银子购买了#R/"..UserData[self.摊位id].角色.辅助技能[摊位数据[self.摊位id].制造[编号].包含技能id].等级.."#W/级#g/" .. 熟练度[摊位数据[self.摊位id].制造[编号].包含技能id].."，现在可以进行打造了")
			ItemControl:索要打造道具(id, "包裹", "打造")
		else 
				if UserData[self.摊位id].角色.师门技能[摊位数据[self.摊位id].制造[编号].主技能id].包含技能[摊位数据[self.摊位id].制造[编号].包含技能id] == nil  then
					SendMessage(UserData[id].连接id, 7, "#y/这样的商品不存在")
					SendMessage(UserData[id].连接id, 20013, self:索取摊位制造(self.摊位id))
					return 0
				elseif UserData[self.摊位id].角色.当前活力 <UserData[self.摊位id].角色.师门技能[摊位数据[self.摊位id].制造[编号].主技能id].包含技能[摊位数据[self.摊位id].制造[编号].包含技能id].等级 then
					SendMessage(UserData[id].连接id, 7, "#y/玩家当前活力不足")
					return 0
				elseif 银子检查(id, self.购买价格) == false then
					SendMessage(UserData[id].连接id, 7, "#y/您没有那么多的银子")
					return 0
				end
				local 等级 = UserData[self.摊位id].角色.师门技能[摊位数据[self.摊位id].制造[编号].主技能id].包含技能[摊位数据[self.摊位id].制造[编号].包含技能id].等级
		        local 技能名称 = UserData[self.摊位id].角色.师门技能[摊位数据[self.摊位id].制造[编号].主技能id].包含技能[摊位数据[self.摊位id].制造[编号].包含技能id].名称
				RoleControl:扣除银子(UserData[id], self.购买价格, 29)

		        if 技能名称 == "堪察令" then
		        	ItemControl:GiveItem(id,"灵宝图鉴",等级)
		        elseif 技能名称 == "兵器谱"  then
		        	ItemControl:GiveItem(id,"神兵图鉴",等级)
		        else
		        	ItemControl:GiveItem(id,"强化符",等级,技能名称)
		        end
				SendMessage(UserData[id].连接id, 7, "#w/你花费了#r/" .. self.购买价格 .. "#w/两银子购买了#g/" .. 技能名称)
		end
				RoleControl:添加消费日志(UserData[id],"[摊位系统-购买]花费" .. self.购买价格 .. "两银子购买了" .. 技能名称 .. "，出售者为" .. UserData[self.摊位id].账号)
				RoleControl:添加消费日志(UserData[self.摊位id],"[摊位系统-出售]获得" .. self.购买价格 .. "两银子购买了" ..技能名称.."，购买者为" .. UserData[id].账号)
				RoleControl:添加银子(UserData[self.摊位id],self.购买价格, 29)
				SendMessage(UserData[self.摊位id].连接id, 7, "#w/出售#g/" .. 技能名称 .. "#w/获得了#r/" .. self.购买价格 .. "#w/两银子")
		        UserData[self.摊位id].角色.当前活力 = UserData[self.摊位id].角色.当前活力 - 等级

		SendMessage(UserData[id].连接id, 20039, self:索取摊位制造(self.摊位id))
		SendMessage(UserData[id].连接id, 3006, "66")
		摊位数据[self.摊位id].刷新 = os.time()
	end
end
function stall:购买道具(id, 编号, 数量)
	if UserData[id].摊位id == nil then
		SendMessage(UserData[id].连接id, 7, "#y/这样的摊位不存在")
		return 0
	else
		self.摊位id = UserData[id].摊位id
		if 摊位数据[self.摊位id] == nil or 摊位数据[self.摊位id].状态 == false or UserData[self.摊位id] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/这样的摊位不存在")
			return 0
		elseif UserData[id].查看摊位 <= 摊位数据[self.摊位id].刷新 then
			SendMessage(UserData[id].连接id, 7, "#y/该摊位商品发生了变化，请稍后再试")
			SendMessage(UserData[id].连接id, 20011, self:索取摊位道具(self.摊位id))
			UserData[id].查看摊位 = os.time()
			return 0
		end

		if 摊位数据[self.摊位id].道具[编号] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/这样的商品不存在")
			SendMessage(UserData[id].连接id, 20011, self:索取摊位道具(self.摊位id))
			UserData[id].查看摊位 = os.time()
			return 0
		else
			self.包裹id1 = 摊位数据[self.摊位id].道具[编号].id
			if UserData[self.摊位id].角色.道具.包裹[self.包裹id1] == nil then
				SendMessage(UserData[id].连接id, 7, "#y/这样的商品不存在")
				SendMessage(UserData[id].连接id, 20011, self:索取摊位道具(self.摊位id))
				return 0
			end
			self.道具id1 = UserData[self.摊位id].角色.道具.包裹[self.包裹id1]
			if UserData[self.摊位id].物品[self.道具id1] == nil or UserData[self.摊位id].物品[self.道具id1] == 0 then
				SendMessage(UserData[id].连接id, 7, "#y/这样的商品不存在")
				SendMessage(UserData[id].连接id, 20011, self:索取摊位道具(self.摊位id))
				UserData[id].查看摊位 = os.time()
				return 0
			end
			self.购买数量 = math.floor(数量 + 0)	
			if self.购买数量 <1 or self.购买数量 > 999 then
				SendMessage(UserData[id].连接id, 7, "#y/请输入正确的购买数量")
				return 0
			end
			if UserData[self.摊位id].物品[self.道具id1].数量 == nil then
				self.购买数量 = 1
			elseif self.购买数量 > 1 and UserData[self.摊位id].物品[self.道具id1].数量 <= self.购买数量 then
				self.购买数量 = UserData[self.摊位id].物品[self.道具id1].数量
			end
			self.扣除价格 = 摊位数据[self.摊位id].道具[编号].价格 * self.购买数量
			if 银子检查(id, self.扣除价格) == false then
				SendMessage(UserData[id].连接id, 7, "#y/你没有那么多的银子")

				return 0
			elseif RoleControl:取可用道具格子(UserData[id],"包裹") == 0 then
				SendMessage(UserData[id].连接id, 7, "#y/您当前的包裹空间不足")
				return 0
			end
			RoleControl:扣除银子(UserData[id], self.扣除价格, 24)
			self.可用id = RoleControl:取可用道具格子(UserData[id],"包裹")
			self.道具id = ItemControl:取道具编号(id)
			UserData[id].物品[self.道具id] = table.copy(UserData[self.摊位id].物品[self.道具id1])
			if UserData[self.摊位id].物品[self.道具id1].数量 ~= nil and UserData[self.摊位id].物品[self.道具id1].数量 >= 1 then
				UserData[id].物品[self.道具id].数量 = self.购买数量
				UserData[self.摊位id].物品[self.道具id1].数量 = UserData[self.摊位id].物品[self.道具id1].数量 - self.购买数量

				if UserData[self.摊位id].物品[self.道具id1].数量 < 1 then
					UserData[self.摊位id].角色.道具.包裹[self.包裹id1] = nil
					UserData[self.摊位id].物品[self.道具id1] = 0
					摊位数据[self.摊位id].道具[编号] = nil
				end
			else
				UserData[self.摊位id].角色.道具.包裹[self.包裹id1] = nil
				UserData[self.摊位id].物品[self.道具id1] = 0
				摊位数据[self.摊位id].道具[编号] = nil
			end
			UserData[id].角色.道具.包裹[self.可用id] = self.道具id
			RoleControl:添加消费日志(UserData[id],"[摊位系统-购买]花费" .. self.扣除价格 .. "两银子购买" .. UserData[id].物品[self.道具id].名称 .. "，出售者=" .. UserData[self.摊位id].账号)
			RoleControl:添加消费日志(UserData[self.摊位id],"[摊位系统-出售]获得" .. self.扣除价格 .. "两银子，出售了" .. UserData[id].物品[self.道具id].名称 .. "，购买者=" .. UserData[id].账号)
			RoleControl:添加银子(UserData[self.摊位id],self.扣除价格, 29)
			SendMessage(UserData[id].连接id, 7, "#w/您花费了#r/" .. self.扣除价格 .. "#w/两银子购买了#g/" .. UserData[id].物品[self.道具id].名称 .. "*" .. self.购买数量)
			SendMessage(UserData[self.摊位id].连接id, 7, "#w/出售#g/" .. UserData[id].物品[self.道具id].名称 .. "*" .. self.购买数量 .. "#w/获得#r/" .. self.扣除价格 .. "#w/两银子")
			摊位数据[self.摊位id].记录 = 摊位数据[self.摊位id].记录 .. os.date("[%Y年%m月%d日%X]") .. "#h/出售#g/" .. UserData[id].物品[self.道具id].名称 .. "*" .. self.购买数量 .. "#h/获得#r/" .. self.扣除价格 .. "#w/两银子" .. "\n"
			SendMessage(UserData[id].连接id, 3006, "66")
			SendMessage(UserData[self.摊位id].连接id, 3006, "66")
			SendMessage(UserData[id].连接id, 20011, self:索取摊位道具(self.摊位id))
			SendMessage(UserData[self.摊位id].连接id, 20012, "66")
			摊位数据[self.摊位id].刷新 = os.time()
			UserData[id].查看摊位 = os.time() + 1
		end
	end
end

function stall:商品上架(id, 编号, 价格)
	if UserData[id].角色.道具.包裹[编号] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/你没有这样的道具")
		return 0
	elseif UserData[id].物品[UserData[id].角色.道具.包裹[编号]].加锁 then
		SendMessage(UserData[id].连接id, 7, "#y/加锁的物品无法上架")

		return 0
	-- elseif UserData[id].物品[UserData[id].角色.道具.包裹[编号]].类型== "剧情道具" then
	-- 	SendMessage(UserData[id].连接id, 7, "#y/特殊物品无法上架")
	elseif UserData[id].物品[UserData[id].角色.道具.包裹[编号]].类型== "古董" then
		SendMessage(UserData[id].连接id, 7, "#y/古董无法上架")
	-- 	return 0
	else
		self.操作编号 = self:取道具状态(id, 编号)
		if self.操作编号 == 0 then
			if 价格 + 0 < 1 then
				SendMessage(UserData[id].连接id, 7, "#y/商品的最低价格不能低于1两银子")

				return 0
			end
			摊位数据[id].道具[编号] = {
				id = 编号,
				价格 = 价格
			}
			SendMessage(UserData[id].连接id, 20003, 摊位数据[id].道具[编号])
		else
			摊位数据[id].道具[self.操作编号] = nil
			SendMessage(UserData[id].连接id, 20004, self.操作编号)
		end
		摊位数据[id].刷新 = os.time()
	end
end

function stall:取道具状态(id, 编号)
	if 摊位数据[id] == nil then
		return 0
	else
		for n = 1, 20 do
			if 摊位数据[id].道具[n] ~= nil and 摊位数据[id].道具[n].id == 编号 then
				return n
			end
		end
	end

	return 0
end

function stall:取bb状态(id, 编号)
	if 摊位数据[id] == nil then
		return 0
	else
		for n = 1, 10 do
			if 摊位数据[id].bb[n] ~= nil and n == 编号 then
				return n
			end
		end
	end

	return 0
end
function stall:取制造状态(id, 编号)
	if 摊位数据[id] == nil then
		return 0
	else
		for n = 1, 10 do
			if 摊位数据[id].制造[n] ~= nil and n == 编号 then
				return n
			end
		end
	end
	return 0
end
function stall:发送玩家摊位信息(id, 玩家编号)
	if 摊位数据[玩家编号] == nil or 摊位数据[玩家编号].状态 == false or UserData[玩家编号] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/这个摊位不存在")

		return 0
	end
	摊位数据[玩家编号].次数 = 摊位数据[玩家编号].次数 + 1
	SendMessage(UserData[id].连接id, 20010, {
		id = 玩家编号,
		名称 = 摊位数据[玩家编号].名称,
		银子 = UserData[id].角色.道具.货币.银子,
		角色 = UserData[玩家编号].角色.名称
	})
	SendMessage(UserData[id].连接id, 20011, self:索取摊位道具(玩家编号))
	UserData[id].查看摊位 = os.time()
	UserData[id].摊位id = 玩家编号
end
function stall:索取摊位bb(玩家编号)
	if 摊位数据[玩家编号] == nil or 摊位数据[玩家编号].状态 == false or UserData[玩家编号] == nil then
		return {}
	end
	self.SendMessage = {}
	for n = 1, 10 do
		if 摊位数据[玩家编号].bb[n] ~= nil then
			self.SendMessage[n] = {
				bb = UserData[玩家编号].召唤兽:获取指定数据1(摊位数据[玩家编号].bb[n].id),
				单价 = 摊位数据[玩家编号].bb[n].价格
			}
		end
	end
	return self.SendMessage
end
function stall:索取摊位制造(玩家编号)
	if 摊位数据[玩家编号] == nil or 摊位数据[玩家编号].状态 == false or UserData[玩家编号] == nil then
		return {}
	end
	self.SendMessage = {}
	for n = 1, 5 do
		if 摊位数据[玩家编号].制造[n] ~= nil then
			if 摊位数据[玩家编号].制造[n].主技能id ==99  then
				self.SendMessage[n] = {

				制造 = UserData[玩家编号].角色.辅助技能[摊位数据[玩家编号].制造[n].包含技能id],
				单价 = 摊位数据[玩家编号].制造[n].价格
			    }
			else 
							self.SendMessage[n] = {

				制造 = UserData[玩家编号].角色.师门技能[摊位数据[玩家编号].制造[n].主技能id].包含技能[摊位数据[玩家编号].制造[n].包含技能id],
				单价 = 摊位数据[玩家编号].制造[n].价格
			}
			end

		end
	end
	return self.SendMessage
end
function stall:索取摊位道具(玩家编号)
	if 摊位数据[玩家编号] == nil or 摊位数据[玩家编号].状态 == false or UserData[玩家编号] == nil then
		return {}
	end
	self.SendMessage = {}
	for n = 1, 20 do
		if 摊位数据[玩家编号].道具[n] ~= nil then
			self.包裹id = 摊位数据[玩家编号].道具[n].id
			if UserData[玩家编号].角色.道具.包裹[self.包裹id] ~= nil then
				self.道具id = UserData[玩家编号].角色.道具.包裹[self.包裹id]
				if UserData[玩家编号].物品[self.道具id] ~= nil and UserData[玩家编号].物品[self.道具id] ~= 0 then
					self.SendMessage[n] = {
						道具 = UserData[玩家编号].物品[self.道具id],
						单价 = 摊位数据[玩家编号].道具[n].价格
					}
					-- if  then
					-- end
				end
			end
		end
	end
	return self.SendMessage
end
function stall:发送摊位信息(id)
	if MapControl:摊位重复(id, UserData[id].地图) then
		SendMessage(UserData[id].连接id, 7, "#y/这里已经有人在摆摊了，请换个地方吧")
		return 0
	elseif UserData[id].地图 ~= 1001 and UserData[id].地图 ~= 1070 and UserData[id].地图 ~= 1092 and UserData[id].地图 ~= 1501  and UserData[id].地图 ~= 1226 then
		SendMessage(UserData[id].连接id, 7, "#y/只有长安城、长寿村、傲来国、建邺城、宝象国才可以摆摊")
		return 0

	elseif UserData[id].角色.等级 < 69 and DebugMode==false then
		SendMessage(UserData[id].连接id, 7, "#y/玩家等级不足69级并且师门技能必须全满79否则无法摆摊，请不要多开摆摊")
		return 0
	elseif UserData[id].队伍 ~= 0 then
		SendMessage(UserData[id].连接id, 7, "#y/组队状态下无法摆摊")
		return 0

	end
	  local ff = function(地图,坐标)
                if 地图 ==1001 and  (取两点距离({x=373,y=49},坐标)<30 or 取两点距离({x=224,y=114},坐标)<50)  then
                    return true
                elseif 地图 ==1226 and   取两点距离({x=110,y=50},坐标)<30 then
                      return true
                elseif 地图 ==1070 and 取两点距离({x=132,y=129},坐标)<60 then

                    return true
                elseif 地图 ==1092 and 取两点距离({x=134,y=46},坐标)<40 then
                       return true
                elseif 地图 ==1501 and  取两点距离({x=64,y=111},坐标)<30 then
                      return true
                else 
                    return false
                end
        end 
          if ff(UserData[id].地图,{x=UserData[id].角色.地图数据.x/20,y=UserData[id].角色.地图数据.y/20})==false then
                SendMessage(UserData[id].连接id, 7, "#y/这个位置不能摆摊")
            	return 0
          end
	if 摊位数据[id] == nil then
		摊位数据[id] = {
			状态 = true,
			记录 = "",
			名称 = "火爆新区",
			次数 = 0,
			道具 = {},
			bb = {},
			制造 = {},
			刷新 = os.time()
		}
	end
	if UserData[id].假人 then
 	  摊位数据[id].名称 = 随机摆摊[math.random(1,#随机摆摊)]
    end
	摊位数据[id].状态 = true
	SendMessage(UserData[id].连接id, 20001, 摊位数据[id])
	SendMessage(UserData[id].连接id, 20002, ItemControl:索要道具1(id, "包裹"))
	SendMessage(UserData[id].连接id, 20008, 摊位数据[id].名称)
	MapControl:更新摊位(id, 摊位数据[id].名称, UserData[id].地图)
	UserData[id].摆摊 = id
	MapControl.MapData[UserData[id].地图].摆摊人数=MapControl.MapData[UserData[id].地图].摆摊人数+1
end
return stall
