function TaskControl:创建车迟斗法副本(id)

		if UserData[id].队伍 == 0 then
			SendMessage(UserData[id].连接id, 7, "#y/本副本需要队伍成员数达到五人才可开启")
		elseif #队伍数据[UserData[id].队伍].队员数据 < 5  and DebugMode == false then
			SendMessage(UserData[id].连接id, 7, "#y/本副本需要队伍成员数达到五人才可开启")
		else
			self.条件提示 = ""
			self.最低等级 = 175
            self.最高等级= 0
			for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
				if 活动数据.车迟[队伍数据[UserData[id].队伍].队员数据[n]] ==nil then
					 	活动数据.车迟[队伍数据[UserData[id].队伍].队员数据[n]]=1
				end
				if UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.等级 < 70 then
					self.条件提示 = "#y/" .. UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称 .. "等级未达到70级"
				elseif UserData[队伍数据[UserData[id].队伍].队员数据[n]].副本 ~= 0 then
					self.条件提示 = "#y/" .. UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称 .. "已经开启了其它副本"
				elseif 银子检查(队伍数据[UserData[id].队伍].队员数据[n],500000) == false then
					self.条件提示 = "#y/" .. UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称 .. "身上没有足够的银子需要50万"
				elseif 活动数据.车迟[队伍数据[UserData[id].队伍].队员数据[n]] < 1 then
					self.条件提示 = "#y/" .. UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称 .. "今日已经参加过这个副本了"
				end

				if  UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.等级 > self.最高等级 then
					self.最高等级 = UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.等级
				end

				if UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.等级 < self.最低等级 then
					self.最低等级 = UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.等级
				end
			end

			if self.最低等级 ~= self.最高等级 and math.abs(self.最高等级 - self.最低等级) >= 20  then
				self.条件提示 = "#y/队伍中存在等级差距达到20级的玩家"
			end
			if self.条件提示 ~= ""  then
				SendMessage(UserData[id].连接id, 7, self.条件提示)
			else
				for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
		            活动数据.车迟[队伍数据[UserData[id].队伍].队员数据[n]] =活动数据.车迟[队伍数据[UserData[id].队伍].队员数据[n]] -1
					RoleControl:扣除银子(UserData[队伍数据[UserData[id].队伍].队员数据[n]],500000, "车迟副本")

				end

				self:开启车迟副本(id)
			end
		end
end
function TaskControl:开启车迟副本(id)
	local 封妖数量 = 800
	-- if DebugMode then
	-- 	封妖数量 =1
	-- end
	local 临时id6 = tonumber(UserData[id].id .. "8010")
	任务数据[临时id6] = {
		类型 = "车迟",
		结束 = 7200,
		进度 = 1,
		上次 = 0,
		当前 = 0,
		id = id,
		数量 = 封妖数量,
		传送数据 = {
			y = 132,
			x = 17,
			地图 = 4131
		},
		数字id = {},
		起始 = os.time()
	}
	self.任务id组 = {}

	if UserData[id].队伍 == 0 then
		self.任务id组[#self.任务id组 + 1] = id
	else
		for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
			self.任务id组[#self.任务id组 + 1] = 队伍数据[UserData[id].队伍].队员数据[n]
		end
	end

	for n = 1, #self.任务id组 do
		self.任务临时id1 = RoleControl:生成任务id(UserData[self.任务id组[n]])
		UserData[self.任务id组[n]].角色.任务数据[self.任务临时id1] = 临时id6
		任务数据[临时id6].数字id[#任务数据[临时id6].数字id + 1] = UserData[self.任务id组[n]].id
		UserData[self.任务id组[n]].副本 = 临时id6
         self:刷新追踪任务信息(self.任务id组[n])
		SendMessage(UserData[self.任务id组[n]].连接id, 7, "#y/你成功开启了副本任务，请在120分钟内完成")
	end

	for n = 1, 30 do
		self:刷出道士(临时id6, id, tonumber(UserData[id].id .. "8011" .. n))
	end
end





function TaskControl:完成三妖大仙(id组, 类型)
	self.副本结束 = true

	if self.副本结束 then
		任务数据[UserData[id组[1]].副本].进度 = 6
		广播队伍消息(id组[1], 7, "#y/恭喜你们完成了车迟斗法副本")
		MapControl:Jump(id组[1], 1070, 127, 145,true)
	end
	任务数据[UserData[id组[1]].副本] = nil
	for n = 1, #id组 do
		if UserData[id组[n]] ~= nil then
			self.符合抓鬼id = true

			if self.符合抓鬼id then
				self.抓鬼奖励参数 = 25
				self.抓鬼奖励等级 = UserData[id组[n]].角色.等级
				self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * 50 * 2*self.抓鬼奖励等级 * self.抓鬼奖励参数)
				self.抓鬼奖励银子 = math.floor(self.抓鬼奖励等级 * self.抓鬼奖励等级 * 5)

				RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验, 6)
				RoleControl:添加银子(UserData[id组[n]],self.抓鬼奖励银子, "车迟")
				 UserData[id组[n]].角色.副本积分=UserData[id组[n]].角色.副本积分+90
        		SendMessage(UserData[id组[n]].连接id,7,"#Y/你获得了90点副本积分！")
				

				if self.副本结束 then
					RoleControl:添加经验(UserData[id组[n]],math.floor(self.抓鬼奖励经验 * 0.5), id组[n], 6)
					RoleControl:取消任务(UserData[id组[n]],UserData[id组[n]].副本)
					UserData[id组[n]].副本 = 0
					SendMessage(UserData[id组[n]].连接id, 7, "#y/你获得了星辉石")
					ItemControl:GiveItem(id组[n], "星辉石", math.random( 3))
					 ItemControl:GiveItem(id组[n],"钨金",math.random(10,16)*10)
                    SendMessage(UserData[id组[n]].连接id, 7, "#y/你获得了钨金")
       --              ItemControl:GiveItem(id组[n],"清灵净瓶",nil,nil,1)
				   -- SendMessage(UserData[id组[n]].连接id, 7, "#y/你获得了#r/清灵净瓶*1")


				   -- 	UserData[id组[n]].角色.副本积分 =UserData[id组[n]].角色.副本积分+10
  					-- SendMessage(UserData[id组[n]].连接id, 9, "#dq/#w/ 你获得了10点副本积分")
					self.奖励参数 = math.random(100)


			         if self.奖励参数 <= 15 then
                       ItemControl:GiveItem(id组[n], "神兜兜")
					广播消息(9, "#xt/#r/ " .. UserData[id组[n]].角色.名称 .. "#y/在车迟斗法战胜了妖怪，获得了天神奖励的#g/神兜兜   #80")
                  elseif self.奖励参数 <= 20 then
					ItemControl:GiveItem(id组[n],"清灵净瓶",nil,nil,1)
					广播消息(9, "#xt/#r/ " .. UserData[id组[n]].角色.名称 .. "#y/在车迟斗法战胜了妖怪，获得了天神奖励的#g/清灵净瓶   #80")
					elseif self.奖励参数 <= 35 then
						ItemControl:GiveItem(id组[n], "海马")
						SendMessage(UserData[id组[n]].连接id, 9, "#dq/#w/ 你获得了" .. "海马")
						广播消息(9, "#xt/#r/ " .. UserData[id组[n]].角色.名称 .. "#y/在车迟斗法战胜了妖怪，获得了天神奖励的#g/海马   #80")

					else
						ItemControl:GiveItem(id组[n], "上古锻造图策",145)
						SendMessage(UserData[id组[n]].连接id, 9, "#dq/#w/ 你获得了" .. "上古锻造图策")
						广播消息(9, "#xt/#r/ " .. UserData[id组[n]].角色.名称 .. "#y/在车迟斗法战胜了妖怪，获得了天神奖励的#g/上古锻造图策   #80")
					end
				end
				if UserData[id组[n]].召唤兽.数据.参战 ~= 0 then
					self.抓鬼奖励等级 = UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
					self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * self.抓鬼奖励等级 * 55 * self.抓鬼奖励参数 * 0.35 + 150)

					UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验, id组[n], UserData[id组[n]].召唤兽.数据.参战, 2)
				end
			end
		end
	end
end
function TaskControl:触发三妖大仙(id, 标识)
	if MapControl.MapData[UserData[id].地图].单位组[标识] == nil then
		return 0
	end

	self.任务id5 = MapControl.MapData[UserData[id].地图].单位组[标识].任务id

	if 任务数据[self.任务id5] == nil then
		广播队伍消息(id, 7, "#y/任务数据异常，代号9001")
	elseif 任务数据[self.任务id5].战斗 then
		广播队伍消息(id, 7, "#y/该npc已经在战斗中了")
	else
		任务数据[self.任务id5].战斗 = true

		FightGet:进入处理(id, 8012, "66", self.任务id5)
	end
end


function TaskControl:刷出道士(副本id, id, 编号)
	self.临时id2 = 编号
	self.临时坐标1 = MapControl:Randomloadtion(4131) 
	self.地图名称1 = "车迟副本"
	任务数据[self.临时id2] = {
		类型 = "车迟1",
		编号 = 1303,
		名称 = "有个道士",
		结束 = 7200,
		数字id = 0,
		造型 = "道士",
		战斗 = false,
		等级 = 0,
		id = id,
		起始 = os.time(),
		任务id = self.临时id2,
		地图编号 = 4131,
		地图名称 = self.地图名称1,
		方向 = math.random( 4) - 1,
		坐标 = self.临时坐标1,
		副本 = 副本id


	}
	local 题目序列 = math.random( #QuretionBank)  
	self.题目内容 = QuretionBank[题目序列].问题
	self.题目答案 = {}
    
	for n = 1, 3 do
		self.题目答案[n] = {
			正确 = false,
			答案 = QuretionBank[题目序列]["选择"..n],
			参数 = math.random( 99999)
		}
	end

	self.题目答案[3].正确 = true

	table.sort(self.题目答案, function (a, b)
		return b.参数 < a.参数
	end)

	self.正确序号 = 0

	for n = 1, 3 do
		if self.题目答案[n].正确 then
			self.正确序号 = n
		end
	end

	任务数据[self.临时id2].对话 = self.题目内容
	任务数据[self.临时id2].选项={self.题目答案[1].答案,self.题目答案[2].答案,self.题目答案[3].答案}
	任务数据[self.临时id2].正确答案 =  self.题目答案[self.正确序号].答案

	MapControl:添加单位(任务数据[self.临时id2])
end

function TaskControl:触发道士(id, 标识, 类型)
	if MapControl.MapData[UserData[id].地图].单位组[标识] == nil then
		return 0
	end

   local TempID = MapControl.MapData[UserData[id].地图].单位组[标识].任务id

	if 任务数据[TempID] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/这个单位不存在")

		return 0
	end
	
	if 任务数据[TempID].正确答案 == 类型 then
		self.抓鬼奖励等级 = UserData[id].角色.等级
		self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * 50 * self.抓鬼奖励等级)

		RoleControl:添加经验(UserData[id],self.抓鬼奖励经验, id, 6)

		任务数据[UserData[id].副本].数量 = 任务数据[UserData[id].副本].数量 - 8

		SendMessage(UserData[id].连接id, 7, "#y/道观建设进度增加了8点")
		MapControl:移除单位(任务数据[TempID].地图编号, TempID)

		任务数据[TempID] = nil

		self:刷出道士(UserData[id].副本, id, TempID)

		if 任务数据[UserData[id].副本].数量 <= 0 then
			for n, v in pairs(任务数据) do
				if 任务数据[n] ~= nil and UserData[id].副本 == 任务数据[n].副本 and 任务数据[n].类型 == "车迟1" then
					MapControl:移除单位(任务数据[n].地图编号, n)

					任务数据[n] = nil
				end
			end

			任务数据[UserData[id].副本].进度 = 2
			local 封妖数量 = 10
			任务数据[UserData[id].副本].数量 = 封妖数量
            -- if DebugMode then
            -- 	任务数据[UserData[id].副本].数量 =1
            -- end
			for n = 1, 封妖数量 do
				self.临时id2 = tonumber(UserData[id].id .. "8012" .. n)
				self.地图编号1 = 4131
				self.临时坐标1 = MapControl:Randomloadtion(self.地图编号1) 
				self.地图名称1 = "车迟副本"
				任务数据[self.临时id2] = {
					类型 = "车迟2",
					编号 = 1396,
					名称 = "贡品",
					结束 = 7200,
					数字id = 0,
					造型 = "泡泡",
					战斗 = false,
					等级 = 0,
					id = id,
					起始 = os.time(),
					任务id = self.临时id2,
					地图编号 = self.地图编号1,
					地图名称 = self.地图名称1,
					方向 = math.random( 4) - 1,
					坐标 = self.临时坐标1,
					副本 = UserData[id].副本
				}

				MapControl:添加单位(任务数据[self.临时id2])
			end
             self:刷新追踪任务信息(id)
			广播队伍消息(id, 7, "#y/您的副本进度已经更新，请查看任务列表")
		end
	else
		SendMessage(UserData[id].连接id, 7, "#y/你的回答不正确")
		MapControl:移除单位(任务数据[TempID].地图编号, TempID)

		任务数据[TempID] = nil

		self:刷出道士(UserData[id].副本, id, TempID)
	end
end



function TaskControl:完成贡品(id组, 任务id)
	for n = 1, #id组 do
		if UserData[id组[n]] ~= nil then
			self.符合抓鬼id = true

			if self.符合抓鬼id then
				self.抓鬼奖励参数 = 1.1
				self.抓鬼奖励等级 = UserData[id组[n]].角色.等级
				self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * 50 * self.抓鬼奖励等级 * self.抓鬼奖励参数)
				self.抓鬼奖励银子 = math.floor((self.抓鬼奖励等级 * self.抓鬼奖励等级 + math.random(2000, 2500)) * self.抓鬼奖励参数)

				RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验, id组[n], 6)
				RoleControl:添加银子(UserData[id组[n]],self.抓鬼奖励银子, "车迟")

				if UserData[id组[n]].召唤兽.数据.参战 ~= 0 then
					self.抓鬼奖励等级 = UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
					self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * 50 * self.抓鬼奖励等级 * self.抓鬼奖励参数 * 0.65 + 150)

					UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验, id组[n], UserData[id组[n]].召唤兽.数据.参战, 2)
				end
			end
		end
	end

	MapControl:移除单位(任务数据[任务id].地图编号, 任务id)

	任务数据[任务id] = nil
	任务数据[UserData[id组[1]].副本].数量 = 任务数据[UserData[id组[1]].副本].数量 - 1

	if 任务数据[UserData[id组[1]].副本].数量 <= 0 then
		任务数据[UserData[id组[1]].副本].进度 = 3
		任务数据[UserData[id组[1]].副本].传送数据 = {
			y = 38,
			x = 22,
			地图 = 4132
		}
		任务数据[UserData[id组[1]].副本].三清记录 = 3

		MapControl:Jump(id组[1], 4132, 22, 38,true)

		for n = 1, 3 do
			self.临时id2 = tonumber(UserData[id组[1]].id .. "8013" .. n)
			self.地图编号1 = 4132
			self.临时坐标1 = MapControl:Randomloadtion(self.地图编号1) 
			self.地图名称1 = "车迟副本"
			self.临时名称 = {
				"元始天尊",
				"道德天尊",
				"灵宝天尊"
			}
			任务数据[self.临时id2] = {
				数字id = 0,
				战斗 = false,
				变异 = true,
				等级 = 0,
				结束 = 7200,
				类型 = "车迟3",
				编号 = 1853,
				造型 = "大力金刚",
				id = id组[1],
				起始 = os.time(),
				任务id = self.临时id2,
				地图编号 = self.地图编号1,
				地图名称 = self.地图名称1,
				名称 = self.临时名称[n],
				方向 = math.random( 4) - 1,
				坐标 = self.临时坐标1,
				副本 = UserData[id组[1]].副本
			}

			MapControl:添加单位(任务数据[self.临时id2])
		end
       
		广播队伍消息(id组[1], 7, "#y/您的副本进度已经更新，请查看任务列表")


	end
		for n = 1, #id组 do
				if UserData[id组[n]] ~= nil then
				self:刷新追踪任务信息(id组[n])
				end
		end
end

function TaskControl:完成三清天尊(id组, 任务id)
	for n = 1, #id组 do
		if UserData[id组[n]] ~= nil then
			self.符合抓鬼id = true

			if self.符合抓鬼id then
				self.抓鬼奖励参数 = 2
				self.抓鬼奖励等级 = UserData[id组[n]].角色.等级
				self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * 90 * self.抓鬼奖励等级 * self.抓鬼奖励参数)
				self.抓鬼奖励银子 = math.floor(self.抓鬼奖励等级 * self.抓鬼奖励等级 * 5 + math.random(2000, 2500))

				RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验, id组[n], 6)
				RoleControl:添加银子(UserData[id组[n]],self.抓鬼奖励银子, "车迟")

				if UserData[id组[n]].召唤兽.数据.参战 ~= 0 then
					self.抓鬼奖励等级 = UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
					self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * 90 * self.抓鬼奖励等级 * self.抓鬼奖励参数 * 0.65 + 150)

					UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验, id组[n], UserData[id组[n]].召唤兽.数据.参战, 2)
				end
			end
		end
	end

	MapControl:移除单位(任务数据[任务id].地图编号, 任务id)

	任务数据[任务id] = nil
	任务数据[UserData[id组[1]].副本].三清记录 = 任务数据[UserData[id组[1]].副本].三清记录 - 1

	if 任务数据[UserData[id组[1]].副本].三清记录 <= 0 then
		任务数据[UserData[id组[1]].副本].进度 = 4
		任务数据[UserData[id组[1]].副本].传送数据 = {
			y = 131,
			x = 194,
			地图 = 4133
		}
		任务数据[UserData[id组[1]].副本].三清记录 = 2

		MapControl:Jump(id组[1], 4133, 194, 131,true)

		for n = 1, 2 do
			self.临时id2 = tonumber(UserData[id组[1]].id .. "8014" .. n)
			self.地图编号1 = 4133
			self.临时坐标1 = MapControl:Randomloadtion(self.地图编号1) 
			self.地图名称1 = "车迟副本"
			self.临时名称 = {
				"你不动",
				"我不动"
			}
			任务数据[self.临时id2] = {
				数字id = 0,
				战斗 = false,
				变异 = true,
				等级 = 0,
				结束 = 7200,
				类型 = "车迟4",
				编号 = 1420,
				造型 = "净瓶女娲",
				id = id组[1],
				起始 = os.time(),
				任务id = self.临时id2,
				地图编号 = self.地图编号1,
				地图名称 = self.地图名称1,
				名称 = self.临时名称[n],
				方向 = math.random( 4) - 1,
				坐标 = self.临时坐标1,
				副本 = UserData[id组[1]].副本
			}

			MapControl:添加单位(任务数据[self.临时id2])
		end

		广播队伍消息(id组[1], 7, "#y/您的副本进度已经更新，请查看任务列表")
		for n = 1, #id组 do
				if UserData[id组[n]] ~= nil then
				self:刷新追踪任务信息(id组[n])
				end
		end
	end
end

function TaskControl:完成双不动(id组, 任务id)
	for n = 1, #id组 do
		if UserData[id组[n]] ~= nil then
			self.符合抓鬼id = true

			if self.符合抓鬼id then
				self.抓鬼奖励参数 = 5
				self.抓鬼奖励等级 = UserData[id组[n]].角色.等级
				self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * 50 * self.抓鬼奖励等级 * self.抓鬼奖励参数)
				self.抓鬼奖励银子 = math.floor(self.抓鬼奖励等级 * self.抓鬼奖励等级 * 20 + math.random(2000, 2500))

				RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验, id组[n], 6)
				RoleControl:添加银子(UserData[id组[n]],self.抓鬼奖励银子, "车迟")

				if UserData[id组[n]].召唤兽.数据.参战 ~= 0 then
					self.抓鬼奖励等级 = UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
					self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * 50 * self.抓鬼奖励等级 * self.抓鬼奖励参数 * 0.65 + 150)

					UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验, id组[n], UserData[id组[n]].召唤兽.数据.参战, 2)
				end
			end
		end
	end

	MapControl:移除单位(任务数据[任务id].地图编号, 任务id)

	任务数据[任务id] = nil
	任务数据[UserData[id组[1]].副本].三清记录 = 任务数据[UserData[id组[1]].副本].三清记录 - 1

	if 任务数据[UserData[id组[1]].副本].三清记录 <= 0 then
		任务数据[UserData[id组[1]].副本].进度 = 5
		任务数据[UserData[id组[1]].副本].战斗对象 = math.random( 3)

		for n = 1, 3 do
			self.临时id2 = tonumber(UserData[id组[1]].id .. "8015" .. n)
			self.地图编号1 = 4133
			self.临时坐标1 = MapControl:Randomloadtion(self.地图编号1) 
			self.地图名称1 = "车迟副本"
			self.临时名称 = {
				"鹿力",
				"羊力",
				"虎力"
			}
			任务数据[self.临时id2] = {
				数字id = 0,
				战斗 = false,
				变异 = true,
				等级 = 0,
				结束 = 7200,
				类型 = "车迟5",
				编号 = 1416,
				造型 = "吸血鬼",
				id = id组[1],
				起始 = os.time(),
				任务id = self.临时id2,
				地图编号 = self.地图编号1,
				地图名称 = self.地图名称1,
				名称 = self.临时名称[n],
				方向 = math.random( 4) - 1,
				坐标 = self.临时坐标1,
				序列 = n,
				副本 = UserData[id组[1]].副本
			}

			MapControl:添加单位(任务数据[self.临时id2])
		end

		广播队伍消息(id组[1], 7, "#y/您的副本进度已经更新，请查看任务列表")
		for n = 1, #id组 do
				if UserData[id组[n]] ~= nil then
				self:刷新追踪任务信息(id组[n])
				end
		end
	end
end

function TaskControl:触发双不动(id, 标识)
	if MapControl.MapData[UserData[id].地图].单位组[标识] == nil then
		return 0
	end

	self.任务id5 = MapControl.MapData[UserData[id].地图].单位组[标识].任务id

	if 任务数据[self.任务id5] == nil then
		广播队伍消息(id, 7, "#y/任务数据异常，代号9001")
	elseif 任务数据[self.任务id5].战斗 then
		广播队伍消息(id, 7, "#y/该npc已经在战斗中了")
	else
		任务数据[self.任务id5].战斗 = true

		FightGet:进入处理(id, 8011, "66", self.任务id5)
	end
end



function TaskControl:触发贡品(id, 标识)
	if MapControl.MapData[UserData[id].地图].单位组[标识] == nil then
		return 0
	end

	self.任务id5 = MapControl.MapData[UserData[id].地图].单位组[标识].任务id

	if 任务数据[self.任务id5] == nil then
		广播队伍消息(id, 7, "#y/任务数据异常，代号9001")
	elseif 任务数据[self.任务id5].战斗 then
		广播队伍消息(id, 7, "#y/该npc已经在战斗中了")
	else
		任务数据[self.任务id5].战斗 = true

		FightGet:进入处理(id, 8009, "66", self.任务id5)
	end
end



function TaskControl:触发三清天尊(id, 标识)
	if MapControl.MapData[UserData[id].地图].单位组[标识] == nil then
		return 0
	end

	self.任务id5 = MapControl.MapData[UserData[id].地图].单位组[标识].任务id

	if 任务数据[self.任务id5] == nil then
		广播队伍消息(id, 7, "#y/任务数据异常，代号9001")
	elseif 任务数据[self.任务id5].战斗 then
		广播队伍消息(id, 7, "#y/该npc已经在战斗中了")
	else
		任务数据[self.任务id5].战斗 = true

		FightGet:进入处理(id, 8010, "66", self.任务id5)
	end
end
