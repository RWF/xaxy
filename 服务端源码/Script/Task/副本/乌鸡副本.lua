-- @Author: 作者QQ381990860
-- @Date:   2021-09-07 00:54:26
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-09-12 00:03:44
-- @Author: 作者QQ381990860
-- @Date:   2021-09-07 00:54:26
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-09-09 15:29:20

--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-05-08 00:22:16
--======================================================================--
function TaskControl:创建乌鸡国副本(id)
		if UserData[id].队伍 == 0 then
			SendMessage(UserData[id].连接id, 7, "#y/本副本需要队伍成员数达到五人才可开启")
		elseif #队伍数据[UserData[id].队伍].队员数据 < 5  and DebugMode == false then
			SendMessage(UserData[id].连接id, 7, "#y/本副本需要队伍成员数达到五人才可开启")
		else
			self.条件提示 = ""
			self.最低等级 = 175
            self.最高等级= 0
			for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
				if 活动数据.乌鸡[队伍数据[UserData[id].队伍].队员数据[n]] ==nil then
					 	活动数据.乌鸡[队伍数据[UserData[id].队伍].队员数据[n]]=1
				end
				if UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.等级 < 50 then
					self.条件提示 = "#y/" .. UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称 .. "等级未达到50级"
				elseif UserData[队伍数据[UserData[id].队伍].队员数据[n]].副本 ~= 0 then
					self.条件提示 = "#y/" .. UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称 .. "已经开启了其它副本"
				elseif 银子检查(队伍数据[UserData[id].队伍].队员数据[n],500000) == false then
					self.条件提示 = "#y/" .. UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称 .. "身上没有足够的银子需要50万"
				elseif 活动数据.乌鸡[队伍数据[UserData[id].队伍].队员数据[n]] < 1 then
					self.条件提示 = "#y/" .. UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称 .. "今日已经参加过这个副本了"
				end

				if  UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.等级 > self.最高等级 then
					self.最高等级 = UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.等级
				end

				if UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.等级 < self.最低等级 then
					self.最低等级 = UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.等级
				end
			end

			if self.最低等级 ~= self.最高等级 and math.abs(self.最高等级 - self.最低等级) >= 20  then
				self.条件提示 = "#y/队伍中存在等级差距达到20级的玩家"
			end
			if self.条件提示 ~= ""  then
				SendMessage(UserData[id].连接id, 7, self.条件提示)
			else
				for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
		            活动数据.乌鸡[队伍数据[UserData[id].队伍].队员数据[n]] =活动数据.乌鸡[队伍数据[UserData[id].队伍].队员数据[n]] -1
					RoleControl:扣除银子(UserData[队伍数据[UserData[id].队伍].队员数据[n]],500000, "乌鸡副本")

				end

				self:开启乌鸡国副本(id)
			end
		end
end
function TaskControl:开启乌鸡国副本(id)
	local 封妖数量 = 15

	local wjfbid = tonumber(UserData[id].id .. "8001")
	任务数据[wjfbid] = {
		类型 = "乌鸡",
		结束 = 7200,
		进度 = 1,
		上次 = 7200,
		当前 = 7200,
		id = id,
		数量 = 封妖数量,
		传送数据 = {
			y = 8,
			x = 118,
			地图 = 3131
		},
		数字id = {},
		起始 = os.time()
	}
	self.任务id组 = {}
	if DebugMode then
		任务数据[wjfbid].数量=1
	end
	if UserData[id].队伍 == 0 then
		self.任务id组[#self.任务id组 + 1] = id
	else
		for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
			self.任务id组[#self.任务id组 + 1] = 队伍数据[UserData[id].队伍].队员数据[n]
		end
	end

	for n = 1, #self.任务id组 do
		self.任务临时id1 = RoleControl:生成任务id(UserData[self.任务id组[n]])
		UserData[self.任务id组[n]].角色.任务数据[self.任务临时id1] = wjfbid
		任务数据[wjfbid].数字id[#任务数据[wjfbid].数字id + 1] = UserData[self.任务id组[n]].id
		UserData[self.任务id组[n]].副本 = wjfbid
		SendMessage(UserData[self.任务id组[n]].连接id, 7, "#y/你成功开启了副本任务，请在120分钟内完成")
		self:刷新追踪任务信息(self.任务id组[n])
	end

	for n = 1, 封妖数量 do
		self.临时id2 = tonumber(id .. "8002" .. n)
		self.地图编号1 = 3131
		self.临时坐标1 =  MapControl:Randomloadtion(self.地图编号1) 
		self.地图名称1 = "乌鸡国副本"
		任务数据[self.临时id2] = {
			数字id = 0,
			战斗 = false,
			名称 = "芭蕉木妖",
			变异 = true,
			等级 = 0,
			结束 = 7200,
			类型 = "乌鸡2",
			编号 = 1197,
			造型 = "树怪",
			id = id,
			起始 = os.time(),
			任务id = self.临时id2,
			地图编号 = self.地图编号1,
			地图名称 = self.地图名称1,
			方向 = math.random( 4) - 1,
			坐标 = self.临时坐标1,
			副本 = wjfbid
		}

		MapControl:添加单位(任务数据[self.临时id2])
	end
end

function TaskControl:触发芭蕉木妖(id, 标识)
	if MapControl.MapData[UserData[id].地图].单位组[标识] == nil then
		return 0
	end

	self.任务id5 = MapControl.MapData[UserData[id].地图].单位组[标识].任务id

	if 任务数据[self.任务id5] == nil then
		广播队伍消息(id, 7, "#y/任务数据异常，代号9001")
	elseif 任务数据[self.任务id5].战斗 then
		广播队伍消息(id, 7, "#y/该npc已经在战斗中了")
	else
		任务数据[self.任务id5].战斗 = true

		FightGet:进入处理(id, 8002, "66", self.任务id5)
	end
end
function TaskControl:完成芭蕉木妖(id组, 任务id)
	for n = 1, #id组 do
		if UserData[id组[n]] ~= nil then
				self.抓鬼奖励参数 = 1
				self.抓鬼奖励等级 = UserData[id组[n]].角色.等级
				self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * 100 * self.抓鬼奖励等级 * self.抓鬼奖励参数)
				RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验, id组[n], 6)
                UserData[id组[n]].角色.副本积分=UserData[id组[n]].角色.副本积分+1
        		SendMessage(UserData[id组[n]].连接id,7,"#Y/你获得了1点副本积分！")
				if UserData[id组[n]].召唤兽.数据.参战 ~= 0 then
					self.抓鬼奖励等级 = UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
					self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * 100 * self.抓鬼奖励等级 * self.抓鬼奖励参数*2)
					UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验, id组[n], UserData[id组[n]].召唤兽.数据.参战, 2)
				end
			if math.random(100)<=50 then
                local 奖励参数=math.random(120)
                local 奖励道具
                  if 奖励参数<=30 then
                  奖励道具="仙玉10点"
                 elseif 奖励参数<=40 then
                    奖励道具="超级金柳露"
                  elseif 奖励参数<=50 then
                    奖励道具="九转金丹"
                  elseif 奖励参数<=60 then
                    奖励道具="碎石锤"
                  elseif 奖励参数<=70 then
                    奖励道具="魔兽要诀"
                  elseif 奖励参数<=80 then
                    奖励道具="海马"
                  else
                    奖励道具="金柳露"
                  end
                   ItemControl:GiveItem(id组[n],奖励道具)
                   广播消息("#xt/".."#S/(乌鸡副本)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/击杀了芭蕉木妖，额外获得了#R/"..奖励道具.."#"..math.random(110))
			end
		end
	end

	MapControl:移除单位(任务数据[任务id].地图编号, 任务id)

	任务数据[任务id] = nil
	任务数据[UserData[id组[1]].副本].数量 = 任务数据[UserData[id组[1]].副本].数量 - 1

	if 任务数据[UserData[id组[1]].副本].数量 <= 0 then
		任务数据[UserData[id组[1]].副本].进度 = 2
		local 封妖数量 = 15

		任务数据[UserData[id组[1]].副本].数量 = 封妖数量
        if DebugMode then
			任务数据[UserData[id组[1]].副本].数量=1
		end
		for n = 1, 封妖数量 do
			self.临时id2 = tonumber(id组[1] .. 8003 .. n)
			self.地图编号1 = 3131
			self.临时坐标1 = MapControl:Randomloadtion(self.地图编号1) 
			self.地图名称1 = "乌鸡国副本"
			任务数据[self.临时id2] = {
				数字id = 0,
				战斗 = false,
				名称 = "热心仙人",
				变异 = true,
				等级 = 0,
				结束 = 7200,
				类型 = "乌鸡3",
				编号 = 1859,
				造型 = "赤脚大仙",
				id = id,
				起始 = os.time(),
				任务id = self.临时id2,
				地图编号 = self.地图编号1,
				地图名称 = self.地图名称1,
				方向 = math.random( 4) - 1,
				坐标 = self.临时坐标1,
				副本 = UserData[id组[1]].副本
			}

			MapControl:添加单位(任务数据[self.临时id2])
		end

		广播队伍消息(id组[1], 7, "#y/您的副本进度已经更新，请查看任务列表")
	end
	for n = 1, #id组 do
			if UserData[id组[n]] ~= nil then
			self:刷新追踪任务信息(id组[n])
			end
	end
end
function TaskControl:触发热心仙人(id, 任务id)
	if MapControl.MapData[UserData[id].地图].单位组[任务id] == nil then
		return 0
	end

	任务id = MapControl.MapData[UserData[id].地图].单位组[任务id].任务id
    if UserData[id].队伍==0 then
    	SendMessage(UserData[id].连接id,7,"#Y/请组队触发")
    	return
    end
	for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
		self.抓鬼奖励参数 = 0.5
		self.抓鬼奖励等级 = UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.等级
		self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * 100 * self.抓鬼奖励等级*0.5 * self.抓鬼奖励参数)
         UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.副本积分=UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.副本积分+1
        SendMessage(UserData[队伍数据[UserData[id].队伍].队员数据[n]].连接id,7,"#Y/你获得了1点副本积分！")
		RoleControl:添加经验(UserData[队伍数据[UserData[id].队伍].队员数据[n]],self.抓鬼奖励经验, 队伍数据[UserData[id].队伍].队员数据[n], 6)
		if UserData[队伍数据[UserData[id].队伍].队员数据[n]].召唤兽.数据.参战 ~= 0 then
			self.抓鬼奖励等级 = UserData[队伍数据[UserData[id].队伍].队员数据[n]].召唤兽.数据[UserData[队伍数据[UserData[id].队伍].队员数据[n]].召唤兽.数据.参战].等级
			self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * 200 * self.抓鬼奖励等级*0.5 * self.抓鬼奖励参数)

			UserData[队伍数据[UserData[id].队伍].队员数据[n]].召唤兽:添加经验(self.抓鬼奖励经验, 队伍数据[UserData[id].队伍].队员数据[n], UserData[队伍数据[UserData[id].队伍].队员数据[n]].召唤兽.数据.参战, 2)
		end
	end

	MapControl:移除单位(任务数据[任务id].地图编号, 任务id)

	任务数据[任务id] = nil
	任务数据[UserData[id].副本].数量 = 任务数据[UserData[id].副本].数量 - 1

	if 任务数据[UserData[id].副本].数量 <= 0 then
		任务数据[UserData[id].副本].进度 = 3

		广播队伍消息(id, 7, "#y/您的副本进度已经更新，请查看任务列表")

		任务数据[UserData[id].副本].三妖记录 = {
			false,
			false,
			false
		}
	end
	for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
			if UserData[队伍数据[UserData[id].队伍].队员数据[n]] ~= nil then
			self:刷新追踪任务信息(队伍数据[UserData[id].队伍].队员数据[n])
			end
	end
end


function TaskControl:触发鬼祟小妖(id, 标识)
	if MapControl.MapData[UserData[id].地图].单位组[标识] == nil then
		return 0
	end

	self.任务id5 = MapControl.MapData[UserData[id].地图].单位组[标识].任务id

	if 任务数据[self.任务id5] == nil then
		广播队伍消息(id, 7, "#y/任务数据异常，代号9001")
	elseif 任务数据[self.任务id5].战斗 then
		广播队伍消息(id, 7, "#y/该npc已经在战斗中了")
	else
		任务数据[self.任务id5].战斗 = true

		FightGet:进入处理(id, 8006, "66", self.任务id5)
	end
end



function TaskControl:完成鬼祟小妖(id组, 任务id)
	for n = 1, #id组 do
		if UserData[id组[n]] ~= nil then
			self.符合抓鬼id = true

			if self.符合抓鬼id then
				self.抓鬼奖励参数 = 1.5
				self.抓鬼奖励等级 = UserData[id组[n]].角色.等级
				self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * 120 * self.抓鬼奖励等级 * self.抓鬼奖励参数)

				RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验, id组[n], 6)
                UserData[id组[n]].角色.副本积分=UserData[id组[n]].角色.副本积分+1
        		SendMessage(UserData[id组[n]].连接id,7,"#Y/你获得了1点副本积分！")
				if UserData[id组[n]].召唤兽.数据.参战 ~= 0 then
					self.抓鬼奖励等级 = UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
					self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * 200 * self.抓鬼奖励等级 * self.抓鬼奖励参数 )
					UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验, id组[n], UserData[id组[n]].召唤兽.数据.参战, 2)
				end
			end
		end
	end

	MapControl:移除单位(任务数据[任务id].地图编号, 任务id)
	任务数据[任务id] = nil
	任务数据[UserData[id组[1]].副本].数量 = 任务数据[UserData[id组[1]].副本].数量 - 1
	if 任务数据[UserData[id组[1]].副本].数量 <= 20 then
		for n, v in pairs(任务数据) do
			if 任务数据[n] ~= nil and UserData[id组[1]].副本 == 任务数据[n].副本 and 任务数据[n].类型 == "乌鸡4" then
				MapControl:移除单位(任务数据[n].地图编号, n)
				任务数据[n] = nil
			end
		end

		任务数据[UserData[id组[1]].副本].进度 = 5
		任务数据[UserData[id组[1]].副本].真假 = math.random( 2)
		任务数据[UserData[id组[1]].副本].国王记录 = {
			false,
			false
		}

		广播队伍消息(id组[1], 7, "#y/您的副本进度已经更新，请查看任务列表")

	end
	for n = 1, #id组 do
		if UserData[id组[n]] ~= nil then
		self:刷新追踪任务信息(id组[n])
		end
	end
end

function TaskControl:完成国王挑战(id组, 类型)
	self.副本结束 = false

	if 类型 == 任务数据[UserData[id组[1]].副本].真假 then
		self.副本结束 = true
	else
		任务数据[UserData[id组[1]].副本].国王记录[类型] = true
		广播队伍消息(id组[1], 7, "#y/我说了我是真的国王，你们居然不相信")
	end


	for n = 1, #id组 do
		if UserData[id组[n]] ~= nil and self.副本结束 then
				self.抓鬼奖励参数 = 8
				self.抓鬼奖励等级 = UserData[id组[n]].角色.等级
				self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * 140 * self.抓鬼奖励等级*2 * self.抓鬼奖励参数)
				self.抓鬼奖励银子 = math.floor(self.抓鬼奖励等级 * self.抓鬼奖励等级 * 20)
				if UserData[id组[n]].召唤兽.数据.参战 ~= 0 then
					self.抓鬼奖励等级 = UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
					self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * self.抓鬼奖励等级 * 200 * self.抓鬼奖励参数)
					UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验, id组[n], UserData[id组[n]].召唤兽.数据.参战, 2)
				end
				RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验, 6)
				RoleControl:添加银子(UserData[id组[n]],self.抓鬼奖励银子,"乌鸡副本")
				RoleControl:取消任务(UserData[id组[n]],UserData[id组[n]].副本)
				RoleControl:添加活跃度(UserData[id组[n]],30)
				任务数据[UserData[id组[n]].副本] = nil
				UserData[id组[n]].副本 = 0
               	--ItemControl:GiveItem(id组[n],"修炼果")
			   	UserData[id组[n]].角色.副本积分 =UserData[id组[n]].角色.副本积分+20
					SendMessage(UserData[id组[n]].连接id, 9, "#dq/#w/ 你获得了20点副本积分")
				local cj = ""
				local 奖励参数 = math.random(150)
				if 奖励参数<=5 then
					cj=取等级卡片(9)
				elseif 奖励参数<=10 then
					cj=取高级兽诀名称()
				elseif 奖励参数<=15 then
					cj="仙玉50点"
				elseif 奖励参数<=20 then
					ItemControl:GiveItem(id组[n], "召唤兽内丹", nil,取内丹("低级"))
					广播消息("#hd/".."#S/(乌鸡国副本)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了乌鸡国副本，获得了玉皇大帝奖励的#g/炼妖石")
				elseif 奖励参数<=30 then
					cj=取随机五宝()
				elseif 奖励参数<=40 then
					cj="元宵"
				elseif 奖励参数<=50 then
					cj="仙玉40点"
				elseif 奖励参数<=60 then
					ItemControl:GiveItem(id组[n],"附魔宝珠",math.random(9,16)*10)
					广播消息("#hd/".."#S/(乌鸡国副本)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了乌鸡国副本，获得了玉皇大帝奖励的#g/附魔宝珠")
				elseif 奖励参数<=70 then
					ItemControl:GiveItem(id组[n],"九转金丹",math.random(3,5)*10)
					广播消息("#hd/".."#S/(乌鸡国副本)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了乌鸡国副本，获得了玉皇大帝奖励的#g/九转金丹")
				elseif 奖励参数<=80 then
					ItemControl:GiveItem(id组[n],"双倍经验丹")
					广播消息("#hd/".."#S/(乌鸡国副本)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了乌鸡国副本，获得了玉皇大帝奖励的#g/双倍经验丹")
				elseif 奖励参数<=90 then
					local 随机等级 =  math.random(6, 15)*10+5
					ItemControl:GiveItem(id组[n],"炼妖石",随机等级)
					广播消息("#hd/".."#S/(乌鸡国副本)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了乌鸡国副本，获得了玉皇大帝奖励的#g/炼妖石")
				elseif 奖励参数<=100 then
					local 随机等级 =  math.random(6, 15)*10+5
					ItemControl:GiveItem(id组[n],"上古锻造图策",随机等级)
					广播消息("#hd/".."#S/(乌鸡国副本)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了乌鸡国副本，获得了玉皇大帝奖励的#g/上古锻造图策")
				end

				if cj ~= "" then
					ItemControl:GiveItem(id组[n],cj)
					广播消息("#hd/".."#S/(乌鸡国副本)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了乌鸡国副本，获得了玉皇大帝奖励的#g/"..cj)
				end
			end
	end

	if self.副本结束 then

		MapControl:Jump(id组[1], 1001, 284, 83,true)
		广播队伍消息(id组[1], 7, "#y/恭喜你们完成了乌鸡国副本")

	end

end



function TaskControl:完成三妖挑战(id组, 类型)
	local 随机小怪 ={"黑熊精","兔子怪","蛤蟆精","羊头怪","狐狸精","雷鸟人","大蝙蝠"}
	for n = 1, #id组 do
		if UserData[id组[n]] ~= nil then
				self.抓鬼奖励参数 = 3
				self.抓鬼奖励等级 = UserData[id组[n]].角色.等级
				self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * 100 * self.抓鬼奖励等级 * self.抓鬼奖励参数)
				RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验, id组[n], 6)
				 UserData[id组[n]].角色.副本积分=UserData[id组[n]].角色.副本积分+10
        		SendMessage(UserData[id组[n]].连接id,7,"#Y/你获得了10点副本积分！")
				if UserData[id组[n]].召唤兽.数据.参战 ~= 0 then
					self.抓鬼奖励等级 = UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
					self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * 100 * self.抓鬼奖励等级 * self.抓鬼奖励参数)
					UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验, id组[n], UserData[id组[n]].召唤兽.数据.参战, 2)
				end
			if math.random(100)<=10 then
                 local 奖励参数=math.random(120)
                local 奖励道具
                  if 奖励参数<=30 then
                  奖励道具="净瓶玉露"
                 elseif 奖励参数<=40 then
                    奖励道具="超级金柳露"
                  elseif 奖励参数<=50 then
                    奖励道具="九转金丹"
                  elseif 奖励参数<=60 then
                    奖励道具="碎石锤"
                  elseif 奖励参数<=70 then
                    奖励道具="魔兽要诀"
                  elseif 奖励参数<=80 then
                    奖励道具="海马"
                  else
                    奖励道具="金柳露"
                  end
                   ItemControl:GiveItem(id组[n],奖励道具)
                   广播消息("#xt/".."#S/(乌鸡副本)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/击杀了三妖，额外获得了#R/"..奖励道具)
			end
		end
	end
	任务数据[UserData[id组[1]].副本].三妖记录[类型] = true
	if 任务数据[UserData[id组[1]].副本].三妖记录[1] and 任务数据[UserData[id组[1]].副本].三妖记录[2] and 任务数据[UserData[id组[1]].副本].三妖记录[3] then
		任务数据[UserData[id组[1]].副本].进度 = 4
		local 封妖数量 = 30
		任务数据[UserData[id组[1]].副本].数量 = 封妖数量
		if DebugMode  then
			任务数据[UserData[id组[1]].副本].数量=1
		end
		for n = 1, 封妖数量 do
			self.临时id2 = tonumber(UserData[id组[1]].id .. "8004" .. n)
			self.地图编号1 = 3132
			self.临时坐标1 = MapControl:Randomloadtion(self.地图编号1) 
			self.地图名称1 = "乌鸡国副本"
			任务数据[self.临时id2] = {
				数字id = 0,
				战斗 = false,
				名称 = "鬼祟小妖",
				变异 = true,
				等级 = 0,
				结束 = 7200,
				类型 = "乌鸡4",
				编号 = 1194,
				造型 = 随机小怪[math.random(#随机小怪)],
				id = id,
				起始 = os.time(),
				任务id = self.临时id2,
				地图编号 = self.地图编号1,
				地图名称 = self.地图名称1,
				方向 = math.random( 4) - 1,
				坐标 = self.临时坐标1,
				副本 = UserData[id组[1]].副本
			}
			MapControl:添加单位(任务数据[self.临时id2])
		end
         for n = 1, #id组 do
				if UserData[id组[n]] ~= nil then
				self:刷新追踪任务信息(id组[n])
				end
		end
		广播队伍消息(id组[1], 7, "#y/您的副本进度已经更新，请查看任务列表")
	end
end

