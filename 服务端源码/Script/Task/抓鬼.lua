-- @Author: 作者QQ381990860
-- @Date:	2021-09-07 00:54:26
-- @Last Modified by:	作者QQ381990860
-- @Last Modified time: 2021-09-09 21:05:34
-- @Author: 作者QQ381990860
-- @Date:	2021-09-07 00:54:26
-- @Last Modified by:	作者QQ381990860
-- @Last Modified time: 2021-09-09 13:33:33
function TaskControl:设置抓鬼任务(id)------------------------------完成--------------------
	if 取队伍符合等级(id,40)==false then
		广播队伍消息(id,7,"#Y/队伍平均等级达到40级才可领取此任务")
	else
		self.检查通过=""
		self.检查次数=200
		if UserData[id].队伍==0 then
			if RoleControl:GetTaskID(UserData[id],"抓鬼") ~= 0 then
				self.检查通过=UserData[id].角色.名称
			end
			if	活动数据.抓鬼[id]==nil then
				活动数据.抓鬼[id] = 400
			else
				self.检查次数 = 活动数据.抓鬼[id]
			end
		else
			for n=1,#队伍数据[UserData[id].队伍].队员数据 do
				if RoleControl:GetTaskID(UserData[队伍数据[UserData[id].队伍].队员数据[n]],"抓鬼")~=0 then
					self.检查通过=UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称..","..self.检查通过
				end
				if 活动数据.抓鬼[队伍数据[UserData[id].队伍].队员数据[n]]==nil then
					活动数据.抓鬼[队伍数据[UserData[id].队伍].队员数据[n]] = 400
				else
					self.检查次数 = 活动数据.抓鬼[队伍数据[UserData[id].队伍].队员数据[n]]
				end
			end
		end
		if self.检查通过~="" then
			广播队伍消息(id,7,"#Y/"..self.检查通过.."已经领取过此任务了")
		elseif self.检查次数 <1	then
			广播队伍消息(id,7,"#Y/队伍中有人抓鬼次数已经达到上限无法在进行抓鬼")
		else
			self:添加抓鬼任务(id)
		end
	end
end

function TaskControl:添加抓鬼任务(id)----------------------------完成
	local 抓鬼名称1={"子时","丑时","寅时","卵时","辰时","巳时","午时","未时","申时","酉时","戌时","亥时"}
	local 抓鬼名称2={"一刻","二刻","三刻","四刻","五刻","六刻",}
	local 抓鬼名称3 = {"诌鬼","假鬼","奸鬼","捣蛋鬼","冒失鬼","烟沙鬼","挖渣鬼","仔细鬼","讨吃鬼","醉死鬼","抠掏鬼","伶俐鬼","急突鬼","丢谎鬼","乜斜鬼","撩桥鬼","饿鬼","色鬼","穷鬼","刻山鬼","吸血鬼","惊鸿鬼","清明鬼"}
	local 抓鬼地图={1501,1092,1142,1193,1173,1146,1140,1070,1226,1209,1040}
	local 临时id=tonumber(id.."4"..os.time())
	local 抓鬼地图编号=抓鬼地图[math.random(#抓鬼地图)]
	local 临时坐标=	MapControl:Randomloadtion(抓鬼地图编号) 
	local 抓鬼造型={"牛头","马面","骷髅怪","僵尸","野鬼",}
	local 临时造型=抓鬼造型[math.random(#抓鬼造型)]
	local 临时名称=抓鬼名称1[math.random(#抓鬼名称1)]..抓鬼名称2[math.random(#抓鬼名称2)]..抓鬼名称3[math.random(#抓鬼名称3)]
	任务数据[临时id]={
		类型="抓鬼"
		,id=id
		,起始=os.time()
		,结束=1200
		,任务id=临时id
		,地图编号=抓鬼地图编号
		,地图名称=MapData[抓鬼地图编号].名称
		,名称=临时名称
		,造型=临时造型
		,方向=math.random(0,3)
		,坐标=临时坐标
		,战斗=false
		,数字id={}
		,客户id={}
		}
	self.任务id组={}
	if UserData[id].队伍==0 then
		self.任务id组[#self.任务id组+1]=id
	else
		for n=1,#队伍数据[UserData[id].队伍].队员数据 do
			self.任务id组[#self.任务id组+1]=队伍数据[UserData[id].队伍].队员数据[n]
		end
	end
	for n=1,#self.任务id组 do
		self.任务临时id1=RoleControl:生成任务id(UserData[self.任务id组[n]])
		UserData[self.任务id组[n]].角色.任务数据[self.任务临时id1]=临时id
		任务数据[临时id].数字id[#任务数据[临时id].数字id+1]=UserData[self.任务id组[n]].id
		任务数据[临时id].客户id[#任务数据[临时id].客户id+1]=self.任务id组[n]
		活动数据.抓鬼[self.任务id组[n]]=活动数据.抓鬼[self.任务id组[n]]-1
		SendMessage( UserData[self.任务id组[n]].连接id,9,"#xt/#Y/你当前在抓鬼任务中还可获得#R/".. 活动数据.抓鬼[self.任务id组[n]].."#Y/次奖励")
		self:刷新追踪任务信息(self.任务id组[n])
	end
	任务数据[临时id].标识 = MapControl:添加单位(任务数据[临时id],1)
	SendMessage(UserData[id].连接id,20,{"钟馗","钟馗",[[ #W/近日有#Y/]]..任务数据[临时id].名称.."#W/正在#Y/"..任务数据[临时id].地图名称.."（"..math.floor(任务数据[临时id].坐标.x)..","..math.floor(任务数据[临时id].坐标.y)..")#W/处作恶，请立即前往降服。"})

 end
function TaskControl:完成抓鬼任务(id组,任务id)-------------------
	for n=1,#id组 do
	if UserData[id组[n]]~=nil then
		self.符合抓鬼id=false
		for i=1,#任务数据[任务id].数字id do
		if UserData[id组[n]].id==任务数据[任务id].数字id[i] then
		self.符合抓鬼id=true
		end
		end
		if self.符合抓鬼id then
		RoleControl:取消任务(UserData[id组[n]],任务id)
		self.抓鬼奖励参数=UserData[id组[n]].角色.抓鬼次数*0.1+1
		self.抓鬼奖励等级=UserData[id组[n]].角色.等级
		self.等级误差=取经验差(任务数据[任务id].平均等级,self.抓鬼奖励等级)
		self.抓鬼奖励经验=math.floor(self.抓鬼奖励等级*self.抓鬼奖励等级*150*self.抓鬼奖励参数*self.等级误差)
		self.抓鬼奖励银子=math.floor((self.抓鬼奖励等级*self.抓鬼奖励等级+math.random(2000,2500))*self.抓鬼奖励参数*3)+10000
		RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验,"抓鬼")
		RoleControl:添加银子(UserData[id组[n]],self.抓鬼奖励银子,"抓鬼")
		RoleControl:添加仙玉(UserData[id组[n]],5,"抓鬼")
		RoleControl:添加活跃度(UserData[id组[n]],1)
		RoleControl:添加储备(UserData[id组[n]],math.floor(self.抓鬼奖励银子*0.5),"抓鬼")
		if UserData[id组[n]].召唤兽.数据.参战~=0 then
		self.抓鬼奖励等级=UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
		self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * self.抓鬼奖励等级 * 100 * self.抓鬼奖励参数)
		UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验,id组[n],UserData[id组[n]].召唤兽.数据.参战,2)
		end
		if UserData[id组[n]].角色.抓鬼次数==10 then
			local 随机奖励 = math.random(100)
			UserData[id组[n]].角色.抓鬼次数=1
			if 随机奖励<=80 then
				EquipmentControl:取随机装备(id组[n],math.random(6,8),false)
			elseif 随机奖励 <= 90 then
					ItemControl:GiveItem(id组[n],"炼妖石",math.floor(UserData[id组[n]].角色.等级/10)*10+5)
			elseif 随机奖励 <= 95 then
					ItemControl:GiveItem(id组[n],"上古锻造图策",math.floor(UserData[id组[n]].角色.等级/10)*10+5)

			end
		else
		UserData[id组[n]].角色.抓鬼次数=UserData[id组[n]].角色.抓鬼次数+1
		end

	 end
	end
	end
 MapControl:移除单位(任务数据[任务id].地图编号,任务id)
 任务数据[任务id]=nil
 end