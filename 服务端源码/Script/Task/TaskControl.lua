-- @Author: 作者QQ381990860
-- @Date:   2021-08-13 19:47:33
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-22 01:56:07
TaskControl = class()

require("Script/Task/任务说明")
require("Script/Task/副本/车迟副本")
require("Script/Task/副本/乌鸡副本")

require("Script/Task/封妖任务")
require("Script/Task/任务链")
require("Script/Task/嘉年华")
require("Script/Task/变身卡")
require("Script/Task/地煞")
require("Script/Task/坐骑")
require("Script/Task/大雁塔")
require("Script/Task/天罡")
require("Script/Task/官职")
require("Script/Task/宝图")
require("Script/Task/师门")
require("Script/Task/帮派任务")
require("Script/Task/抓鬼")
require("Script/Task/押镖")
require("Script/Task/无限轮回")
require("Script/Task/群雄逐鹿")
require("Script/Task/除暴安良")
require("Script/Task/游泳")
require("Script/Task/皇宫飞贼")
require("Script/Task/门派闯关")
require("Script/Task/鬼王")
require("Script/Task/法宝")
require("Script/Task/帮战怪物")
require("Script/Task/天罡星69")
require("Script/Task/神器任务")
require("Script/Task/蚩尤挑战")
function TaskControl:初始化()
	self.id编号 = 0
end

function TaskControl:数据处理(id, 序号, 内容, 参数)

	if 序号 == 26 then
		self:索取任务信息(id)
	elseif 序号 == 20003 then
		self:设置打图任务(id)
	elseif 序号 == 20004 then
		self.临时数据 = table.loadstring(内容)

		if self:删除任务道具(id, self.临时数据.格子, self.临时数据.id) then
			if 任务数据[self.临时数据.id].类型 == "官职" then
				self:完成官职物品任务(id, self.临时数据.id)
			elseif 任务数据[self.临时数据.id].类型 == "任务链" then
				self:完成任务链(id, self.临时数据.id)
      elseif 任务数据[self.临时数据.id].类型 == "宝宝任务链" then
        self:完成宝宝任务链(id, self.临时数据.id)
			elseif 任务数据[self.临时数据.id].类型 == "师门" then
				self:完成师门任务(id, self.临时数据.id)
			elseif 任务数据[self.临时数据.id].类型 == "神树" then
				self:完成灵气任务(id, self.临时数据.id)
			end
		end
	end
end
function TaskControl:刷新追踪任务信息(id)
if UserData[id] ==nil then
	return
else
 self.任务信息={}
 for n=1,#UserData[id].角色.任务数据 do
     if  UserData[id].角色.任务数据[n]~=nil and 任务数据[UserData[id].角色.任务数据[n]]~=nil then
        self.任务信息[#self.任务信息+1]=self:取任务信息(UserData[id].角色.任务数据[n],id)
      end
   end
  SendMessage(UserData[id].连接id,11015,self.任务信息)
 end
end
function TaskControl:Reward(任务id,id)
   if 任务数据[任务id]==nil or not UserData[id] then 任务数据[任务id]=nil return 0 end
   MapControl:移除单位(UserData[id].地图,任务id)
   local ExeclData=RefreshMonsterData[任务数据[任务id].类型]

   local  id组={}
   if UserData[id].队伍 == 0 then
        table.insert(id组,id)
   else
    for i,v in ipairs(队伍数据[UserData[id].队伍].队员数据) do
          table.insert(id组,v)
    end
   end
    for i,v in ipairs(id组) do
        for g,h in ipairs{"银子","经验","仙玉","储备"} do
            if ExeclData[h] then
                if type(ExeclData[h]) =="number" then
                    RoleControl["添加"..h](
                    RoleControl,
                    UserData[v],
                    ExeclData[h],
                    任务数据[任务id].类型)
                else

                 RoleControl["添加"..h](
                    RoleControl,
                    UserData[v],
                    UserData[v].角色.等级*UserData[v].角色.等级*ExeclData[h][1]+ExeclData[h][2],
                    任务数据[任务id].类型)
                    if h=="经验" and  UserData[v].召唤兽.数据.参战~=0 then
                        UserData[v].召唤兽:添加经验(UserData[v].角色.等级*UserData[v].角色.等级*ExeclData[h][1]*0.5+ExeclData[h][2],v,UserData[v].召唤兽.数据.参战,10)
                    end
                 end
            end
        end
        if ExeclData.添加积分 then
          for h,m in pairs(ExeclData.添加积分) do
            RoleControl[h](RoleControl,UserData[v],m,任务数据[任务id].类型)
          end
        end
         if ExeclData.物品 then
                local cj = ""
                for key,value in pairs(ExeclData.物品) do
                       if  value >= math.random(100)   then


                              cj=key

                           break
                       end
                end
                if cj ~= "" then
                    ItemControl:GiveItem(v,cj)
                    广播消息(string.format("#hd/#S/(%s)#R/%s#Y/成功战胜%s,获得了#g/%s".."#"..math.random(1,110),任务数据[任务id].类型,UserData[v].角色.名称,任务数据[任务id].类型,cj))
                end
        end

    end
  任务数据[任务id]=nil
end
function TaskControl:LoadRefreshMonster(data,state)
    local TempNmae =""

    for k,v in pairs(data.地图) do
        TempNmae = TempNmae.."#G/"..MapData[v].名称.."#W/、"
    end

    for i=1,math.random(data.数量[1],data.数量[2]) do
        local TempID=self:生成任务id()
        local Tempnum={}
        local MapID=data.地图[math.random(1,#data.地图)]
        if #data.名称==#data.模型 then
             Tempnum[1]= math.random(1,#data.名称)
             Tempnum[2]=  Tempnum[1]
        else
            Tempnum={math.random(1,#data.名称),math.random(1,#data.模型)}
        end

          任务数据[TempID]={
          类型=state,
          方向=math.random(4)-1,
          起始=os.time(),
          任务id=TempID,
          结束=data.时间,
          名称=data.名称[Tempnum[1]],
          造型=data.模型[Tempnum[2]],
          地图编号=MapID,
          坐标=MapControl:Randomloadtion(MapID) ,
          称谓=data.称谓,
          事件 ="刷新",
          战斗=false
         }
         if data.染色方案 then
            任务数据[TempID].染色方案 =data.染色方案
            任务数据[TempID].染色组 =table.copy(data.染色组)
        end
         if state=="150级BOSS" or state=="乌铁盒" or state=="黄铜箱"  or state=="白银匣" or state== "赤金宝箱"then
             任务数据[TempID].方向=0
         end
        MapControl:添加单位(任务数据[TempID])
    end
    if data.广播 then
         if state=="美猴王" then --自定义广播
            广播消息(string.format("#hd/#S(%s)#W/%s请前往",state,data.广播))
         else
              广播消息(string.format("#hd/#S(%s)#W/%s请前往%s参加",state,data.广播,TempNmae))
         end

    end
end

function TaskControl:GetLinkTask(user,tasktype)

  if LinkTask[tasktype].终身 and  user.角色[tasktype] then
      SendMessage(user.连接id,7,"#Y/这个"..tasktype.."你已经完成了,无法再领取")
      return
  end
    if LinkTask[tasktype].限制人数 then
         local Check =""
        if user.队伍==0 or #队伍数据[user.队伍].队员数据<LinkTask[tasktype].限制人数 then
            SendMessage(user.连接id,7,"#Y/"..tasktype.."任务最少需要"..LinkTask[tasktype].限制人数.."人组队参加")
                return 0
        elseif LinkTask[tasktype].等级差 then
              local MaxLv,MinLv,Prompt=0,175,""
                  for n = 1, #队伍数据[user.队伍].队员数据 do
                    if  UserData[队伍数据[user.队伍].队员数据[n]].角色.等级 > MaxLv then
                      MaxLv = UserData[队伍数据[user.队伍].队员数据[n]].角色.等级
                    end
                    if UserData[队伍数据[user.队伍].队员数据[n]].角色.等级 < MinLv then
                        MinLv = UserData[队伍数据[user.队伍].队员数据[n]].角色.等级
                    end
                 end
                  if MinLv ~= MaxLv and math.abs(MaxLv - MinLv) >= LinkTask[tasktype].等级差  then
                  Prompt = "#y/队伍中存在等级差距达到"..LinkTask[tasktype].等级差.."级的玩家"
                  end
                  if Prompt ~= ""  then
                      广播队伍消息(user.id, 7, Prompt)
                      return
                  end
        end
        if LinkTask[tasktype].限制等级  then
             if  not 取队伍符合等级(user.id, LinkTask[tasktype].限制等级[1])  then
                  广播队伍消息(user.id, 7, string.format("#y/领取本任务需要队伍所有成员等级达到%d级",LinkTask[tasktype].限制等级[1]))
                  return
             elseif not 取队伍高于等级(user.id, LinkTask[tasktype].限制等级[2]) then
                  广播队伍消息(user.id, 7, string.format("#y/领取本任务需要队伍所有成员等级低于%d级",LinkTask[tasktype].限制等级[2]))
                  return
             end
        end
        for n=1,#队伍数据[user.队伍].队员数据 do
            if RoleControl:GetTaskID(UserData[队伍数据[user.队伍].队员数据[n]],tasktype)~=0 then
                Check=Check.."，"..UserData[队伍数据[user.队伍].队员数据[n]].角色.名称
            end
        end
        if Check~="" then
            SendMessage(user.连接id,7,"#Y/"..Check.."已经领取过任务了")
            return
        end
    else
         if user.队伍~=0  then
            SendMessage(user.连接id,7,"#Y/"..tasktype.."任务最少不能组队参加")
            return
        elseif  RoleControl:GetTaskID(user,tasktype)~=0 then
            SendMessage(user.连接id,7,"#Y/你已经领取过任务了")
                return
        elseif LinkTask[tasktype].限制等级  then
            if user.角色.等级 < LinkTask[tasktype].限制等级[1]  then
                  SendMessage(user.连接id, 7, string.format("#y/领取本任务需要队伍所有成员等级达到%d级",LinkTask[tasktype].限制等级[1]))
                    return
            elseif user.角色.等级 > LinkTask[tasktype].限制等级[2]  then
                  SendMessage(user.连接id, 7, string.format("#y/领取本任务需要队伍所有成员等级低于%d级",LinkTask[tasktype].限制等级[2]))
                  return
            end
        end
    end
    if LinkTask[tasktype].次数 then
        local 检查通过 =""
        if 活动数据[tasktype]== nil then
            活动数据[tasktype]={}
        end
        if user.队伍~=0 then
            for n=1,#队伍数据[user.队伍].队员数据 do
                if  活动数据[tasktype][队伍数据[user.队伍].队员数据[n]]==nil then
                    活动数据[tasktype][队伍数据[user.队伍].队员数据[n]] = 0
                elseif 活动数据[tasktype][队伍数据[user.队伍].队员数据[n]] > LinkTask[tasktype].次数 then
                     检查通过=检查通过.."，"..UserData[队伍数据[user.队伍].队员数据[n]].角色.名称
                end
            end
        else
            if  活动数据[tasktype][user.id]==nil then
                活动数据[tasktype][user.id] = 0
            elseif 活动数据[tasktype][user.id]> LinkTask[tasktype].次数 then
                    检查通过=user.角色.名称
            end
        end
        if 检查通过~="" then
            SendMessage(user.连接id,7,"#Y/"..检查通过.."已经无法再领取任务请于明日中午12点刷新"..tasktype.."任务最多完成".. LinkTask[tasktype].次数)
            return
        end
    end
    TaskControl:AddLinkTask(user,tasktype)
end

function TaskControl:GetLinkData(TaskData,LinkTasks,course)
    local  course= TaskData.进程
    local  ADDMapAndNpc=function(TaskData,LinkTasks,course)
       if LinkTasks["进程"..course][2] =="随机人物" then
           TaskData.Map,TaskData.NPC =取任务人物和地图()
       else
          TaskData.Map,TaskData.NPC =tonumber(LinkTasks["进程"..course][3]),tonumber(LinkTasks["进程"..course][2])
       end
    end
    if LinkTasks["进程"..course][1] == "需要物品" then
        local TempItem = LinkTasks.需要物品[course];
        local RandomItem = nil;
     --  local RandomItem=LinkTasks.需要物品
     -- local TempItem = LinkTasks.需要物品[math.random(1, #LinkTasks.需要物品)]
     if  TempItem =="随机三药" then
         RandomItem = {"金创药","金香玉","小还丹","千年保心丹","风水混元丹","定神香","蛇蝎美人","九转回魂丹","佛光舍利子","五龙丹"}
     elseif TempItem =="随机烹饪" then
         RandomItem = {"包子","烤鸭","佛跳墙","珍露酒","女儿红","虎骨酒","豆斋果","臭豆腐","烤肉","桂花丸","翡翠豆腐","长寿面","梅花酒","百味酒","蛇胆酒","醉生梦死"}
     elseif TempItem =="随机宝石" then
          RandomItem = {"红宝石","绿宝石","蓝宝石","黄宝石","光芒石","月亮石","太阳石","舍利子","红玛瑙","黑宝石","神秘石","速度精魄灵石","气血精魄灵石", "灵力精魄灵石","躲避精魄灵石","防御精魄灵石","伤害精魄灵石"}
     end
     if RandomItem == nil then
        TaskData.Item = TempItem
    else
        TaskData.Item = RandomItem[math.random(#RandomItem)]
    end
        ADDMapAndNpc(TaskData,LinkTasks,course)
   elseif LinkTasks["进程"..course][1] == "需要找人" then
        ADDMapAndNpc(TaskData,LinkTasks,course)
   elseif LinkTasks["进程"..course][1] == "NPC战斗" then
         ADDMapAndNpc(TaskData,LinkTasks,course)
   elseif LinkTasks["进程"..course][1] == "刷新怪战斗" then
   elseif LinkTasks["进程"..course][1] == "遇怪战斗" then
     local TempMAP = LinkTasks.遇怪战斗[math.random(1, #LinkTasks.遇怪战斗)]
       if TempMAP=="随机地图" then
         TaskData.Map =取任务人物和地图()
       else
        TaskData.Map =tonumber(TempMAP)
       end

   end
end
function TaskControl:FulfilLinkTask(id, TaskID)
  if UserData[id].队伍~=0 then
    local TeamExamine={}
    for n=1,#队伍数据[UserData[id].队伍].队员数据 do
         TeamExamine[n]=false
         for i=1,#任务数据[TaskID].客户id do
            if 队伍数据[UserData[id].队伍].队员数据[n]==任务数据[TaskID].客户id[i] then
                TeamExamine[n]=true
               break
            end
          end
    end
    for i=1,#TeamExamine do
       if  TeamExamine[i]==false then
             广播队伍消息(id, 7, string.format("#y/%s角色没有%s任务不能完成",UserData[队伍数据[UserData[id].队伍].队员数据[i]].角色.名称,任务数据[TaskID].类型))
             return
       end
    end
  end
    local  tasks  = 任务数据[TaskID]
    local gift = nil; local hello = nil; local cur_npc = tasks.NPC;
    if LinkTask[tasks.类型]["进程"..tasks.进程+1] then--分奖励
        if  LinkTask[tasks.类型].每环奖励 then
             for n = 1, #tasks.客户id do
                 if UserData[tasks.客户id[n]] then
                    local TempLV = UserData[tasks.客户id[n]].角色.等级
                    local  ks = 0
                    for k,v in pairs(LinkTask[tasks.类型].每环奖励) do
                        if k =="添加物品" then
                            ks = ks+1
                            if  LinkTask[tasks.类型].物品几率 and  LinkTask[tasks.类型].物品几率[ks] and  math.random(100)<=LinkTask[tasks.类型].物品几率[ks]  then
                                local userID =tasks.客户id[n]
                                local RoleName = UserData[userID].角色.名称
                                if v =="仙玉抽奖" and  UserData[tasks.客户id[n]].队伍~=0 and UserData[tasks.客户id[n]].队长 then
                                     UserData[tasks.客户id[n]].抽奖 =true
                                    ItemControl:仙玉抽奖(tasks.客户id[n],math.random(200,207))
                                     广播消息(string.format("#hd/#S/(%s)#R/%s#Y/成功完成了%s,获得了一次仙玉抽奖的机会".."#"..math.random(1,110),tasks.类型,RoleName,tasks.类型))
                                elseif v =="随机召唤兽宝石" then
                                     local name =取召唤兽随机宝石()
                                     ItemControl:GiveItem(userID,name)
                                      广播消息(string.format("#hd/#S/(%s)#R/%s#Y/成功完成了%s,获得了#g/%s".."#"..math.random(1,110),tasks.类型,RoleName,tasks.类型,name))
                                elseif key=="随机1-2宝石" then-----自定义等级模板
                                        local name =取随机宝石()
                                        ItemControl:GiveItem(userID,name,math.random(1, 2))
                                        广播消息(string.format("#hd/#S/(%s)#R/%s#Y/成功完成了%s,获得了#g/%s".."#"..math.random(1,110),tasks.类型,RoleName,tasks.类型,name))
                                elseif v =="随机五宝" then
                                    local name =取随机五宝()
                                    ItemControl:GiveItem(userID,name)
                                     广播消息(string.format("#hd/#S/(%s)#R/%s#Y/成功完成了%s,获得了#g/%s".."#"..math.random(1,110),tasks.类型,RoleName,tasks.类型,name))
                                elseif v =="随机强化石" then
                                     local name =取随机强化石()
                                     ItemControl:GiveItem(userID,name)
                                      广播消息(string.format("#hd/#S/(%s)#R/%s#Y/成功完成了%s,获得了#g/%s".."#"..math.random(1,110),tasks.类型,RoleName,tasks.类型,name))
                                elseif v =="修炼果10个" then
                                      ItemControl:GiveItem(userID,"修炼果",nil,nil,10)
                                    广播消息(string.format("#hd/#S/(%s)#R/%s#Y/成功完成了%s,获得了10个#g/%s".."#"..math.random(1,110),tasks.类型,RoleName,tasks.类型,"修炼果"))
                                elseif v =="随机宝石" then
                                    local name =取随机宝石()
                                    ItemControl:GiveItem(userID,name)
                                     广播消息(string.format("#hd/#S/(%s)#R/%s#Y/成功完成了%s,获得了#g/%s".."#"..math.random(1,110),tasks.类型,RoleName,tasks.类型,name))
                                elseif v =="随机红包" then
                                     local name =取随机红包()
                                     ItemControl:GiveItem(userID,name)
                                      广播消息(string.format("#hd/#S/(%s)#R/%s#Y/成功完成了%s,获得了#g/%s".."#"..math.random(1,110),tasks.类型,RoleName,tasks.类型,name))

                                elseif  v ~="仙玉抽奖" then
                                    ItemControl:GiveItem(userID,v)
                                    广播消息(string.format("#hd/#S/(%s)#R/%s#Y/成功完成了%s,获得了#g/%s".."#"..math.random(1,110),tasks.类型,RoleName,tasks.类型,v))
                                end
                            end
                        elseif k =="添加银子" then
                            local Money= math.floor(TempLV * TempLV *v * (1 + tasks.进程 * 0.005))

                             RoleControl[k](RoleControl,UserData[tasks.客户id[n]],Money,tasks.类型)
                        elseif k =="添加经验" then
                            local Exp=math.floor((TempLV*TempLV*v+math.random(4000,5500))* (1 + tasks.进程 * 0.005))

                              RoleControl[k](RoleControl,UserData[tasks.客户id[n]],Exp,tasks.类型)
                        else
                             RoleControl[k](RoleControl,UserData[tasks.客户id[n]],v,tasks.类型)
                        end
                    end
                    if 活动数据[tasks.类型]==nil then
                      活动数据[tasks.类型]={}
                    end
                    if 活动数据[tasks.类型][tasks.客户id[n]] ==nil then
                      活动数据[tasks.类型][tasks.客户id[n]] = 0
                    end
                    活动数据[tasks.类型][tasks.客户id[n]]=活动数据[tasks.类型][tasks.客户id[n]]+1
                 end
            end
        end
        if LinkTask[tasks.类型]["进程"..tasks.进程][5] ~= nil then
            hello = LinkTask[tasks.类型]["进程"..tasks.进程][5];
        end
        if LinkTask[tasks.类型]["进程"..tasks.进程][6] ~= nil then
            gift = LinkTask[tasks.类型]["进程"..tasks.进程][6];
        end


        tasks.进程=tasks.进程+1
        self:GetLinkData(tasks,LinkTask[tasks.类型])
        for n = 1, #tasks.客户id do
             if UserData[tasks.客户id[n]] then
                    -- SendMessage(UserData[tasks.客户id[n]].连接id, 7, "#y/你的"..tasks.类型.."进度已经更新")
                    if gift ~= nil then
                        ItemControl:GiveItem(tasks.客户id[n],gift)
                    end
                    if hello ~= nil then
                        print(NpcData[cur_npc].名称..":"..hello)
                        SendMessage(UserData[tasks.客户id[n]].连接id,20,{[1]=NpcData[cur_npc].模型,[2]=NpcData[cur_npc].名称,[3]=hello,[6]=cur_npc})
                    end
                    self:刷新追踪任务信息(tasks.客户id[n])
            end
        end
    else--最终奖励
        for n = 1, #tasks.客户id do
             if UserData[tasks.客户id[n]] then
                 if  LinkTask[tasks.类型].最终奖励 then
                    local TempLV = UserData[tasks.客户id[n]].角色.等级
                    local  ks = 0
                    for k,v in pairs(LinkTask[tasks.类型].最终奖励) do
                        if k =="添加物品" then
                            ks = ks+1
                            if  LinkTask[tasks.类型].最终几率 and  LinkTask[tasks.类型].最终几率[ks] and  math.random(100)<= LinkTask[tasks.类型].最终几率[ks] then
                                local userID =tasks.客户id[n]
                                local RoleName = UserData[userID].角色.名称
                                 if v =="仙玉抽奖" and  UserData[tasks.客户id[n]].队伍~=0 and UserData[tasks.客户id[n]].队长 then
                                     UserData[tasks.客户id[n]].抽奖 =true
                                    ItemControl:仙玉抽奖(tasks.客户id[n],math.random(200,207))
                                     广播消息(string.format("#hd/#S/(%s)#R/%s#Y/成功完成了%s,获得了一次仙玉抽奖的机会".."#"..math.random(1,110),tasks.类型,RoleName,tasks.类型))

                                  else
                                    ItemControl:GiveItem(userID,v)
                                    广播消息(string.format("#hd/#S/(%s)#R/%s#Y/成功完成了%s,获得了#g/%s".."#"..math.random(1,110),tasks.类型,RoleName,tasks.类型,v))
                                end
                            end
                        elseif k =="添加银子" then
                            local Money= math.floor(TempLV * TempLV *v * (1 + tasks.进程 * 0.005))
                             RoleControl[k](RoleControl,UserData[tasks.客户id[n]],Money,tasks.类型)
                        elseif k =="添加经验" then
                            local Exp=math.floor((TempLV*TempLV*v+math.random(4000,5500))* (1 + tasks.进程 * 0.005))

                              RoleControl[k](RoleControl,UserData[tasks.客户id[n]],Exp,tasks.类型)
                        else
                             RoleControl[k](RoleControl,UserData[tasks.客户id[n]],v,tasks.类型)
                        end
                    end
                       if 活动数据[tasks.类型]==nil then
                      活动数据[tasks.类型]={}
                    end
                    if 活动数据[tasks.类型][tasks.客户id[n]] ==nil then
                      活动数据[tasks.类型][tasks.客户id[n]] = 0
                    end
                   活动数据[tasks.类型][tasks.客户id[n]]=活动数据[tasks.类型][tasks.客户id[n]]+1
                 end
                RoleControl:取消任务(UserData[tasks.客户id[n]],RoleControl:GetTaskID(UserData[tasks.客户id[n]],tasks.类型))
                SendMessage(UserData[tasks.客户id[n]].连接id, 7, "#y/恭喜你完成"..tasks.类型.."任务")
                  if LinkTask[tasks.类型].终身  then
                      UserData[tasks.客户id[n]].角色[tasks.类型]=1
                  end
             end
        end

        任务数据[TaskID]=nil
    end
end
function TaskControl:AddLinkTask(user,tasktype)
    local TempID = tonumber(self:生成任务id()..user.id..math.random(100,999))
    任务数据[TempID] = {
        战斗 = false,
        结束 = LinkTask[tasktype].限制时间,
        类型 = tasktype,
        进程 = 1,
        id = user.id,
        起始 = os.time(),
        任务id = TempID,
        客户id = {}
    }
    self:GetLinkData(任务数据[TempID], LinkTask[tasktype])

    self.任务id组 = {}
    if user.队伍 == 0 then
        self.任务id组[#self.任务id组 + 1] = user.id
    else
        for n = 1, #队伍数据[user.队伍].队员数据 do
            self.任务id组[#self.任务id组 + 1] = 队伍数据[user.队伍].队员数据[n]
            SendMessage(UserData[队伍数据[user.队伍].队员数据[n]].连接id, 7, "#y/你领取了"..tasktype.."任务")
        end
    end
    for n = 1, #self.任务id组 do
        self.任务临时id1 = RoleControl:生成任务id(UserData[self.任务id组[n]])
        UserData[self.任务id组[n]].角色.任务数据[self.任务临时id1] = TempID
        任务数据[TempID].客户id[#任务数据[TempID].客户id + 1] = self.任务id组[n]
        self:刷新追踪任务信息(self.任务id组[n])
    end
end
function TaskControl:CancelLinkTask(user,tasktype)
          local taskID = RoleControl:GetTaskID(user,tasktype)
           if taskID==0 then
                SendMessage(user.连接id,20,{"","","请先领取"..tasktype.."任务"})
           elseif user.队伍~=0 then
                 SendMessage(user.连接id,20,{"","","请解散队伍以后再过来取消"})
          else
                SendMessage(user.连接id,20,{"","","你取消了"..tasktype.."任务"})
                  RoleControl:取消任务(user,taskID)
                  任务数据[taskID]=nil
         end
end
function TaskControl:LinkTaskFight(id, TaskID)
  if UserData[id].队伍~=0 then
    local TeamExamine={}
    for n=1,#队伍数据[UserData[id].队伍].队员数据 do
         TeamExamine[n]=false
         for i=1,#任务数据[TaskID].客户id do
            if 队伍数据[UserData[id].队伍].队员数据[n]==任务数据[TaskID].客户id[i] then
                TeamExamine[n]=true
               break
            end
          end
    end
    for i=1,#TeamExamine do
       if  TeamExamine[i]==false then
             广播队伍消息(id, 7, string.format("#y/%s角色没有%s任务",UserData[队伍数据[UserData[id].队伍].队员数据[i]].角色.名称,任务数据[TaskID].类型))
             return
       end
    end
  end
    if LinkTask[任务数据[TaskID].类型].怪物["进程"..任务数据[TaskID].进程] then
        FightGet:LoadMonster(UserData[id],TaskID,LinkTask[任务数据[TaskID].类型].怪物["进程"..任务数据[TaskID].进程],2)
    else
        error("环任务数据表"..任务数据[TaskID].类型.."没有定义进程"..任务数据[TaskID].进程.."的怪物")
    end
end
function TaskControl:更新()
	for n, v in pairs(任务数据) do
		if 任务数据[n] ~= nil and 任务数据[n].类型 == "摄妖香" then
			任务数据[n].当前 = 1800 - (os.time() - 任务数据[n].起始)

			if 任务数据[n].上次 - 任务数据[n].当前 >= 60 then
				任务数据[n].上次 = 任务数据[n].当前
				 if UserData[任务数据[n].id]  then
				 self:刷新追踪任务信息(任务数据[n].id)
				 end
			end

			if UserData[任务数据[n].id] ~= nil then
				if 任务数据[n].当前 <= 0 then
					SendMessage(UserData[任务数据[n].id].连接id, 7, "#y/你的摄妖香效果过期了")
					RoleControl:取消任务(UserData[任务数据[n].id],n)
					UserData[任务数据[n].id].摄妖香 = false
					任务数据[n] = nil
				end
			else
				任务数据[n] = nil
			end
		elseif 任务数据[n] ~= nil and (任务数据[n].类型 == "双倍" or 任务数据[n].类型 == "变身卡"or 任务数据[n].类型 == "天机培元丹"or 任务数据[n].类型 == "九霄清心丸") then
			任务数据[n].当前 = 任务数据[n].结束 - (os.time() - 任务数据[n].起始)
			if 任务数据[n].上次 - 任务数据[n].当前 >= 60 then
				任务数据[n].上次 = 任务数据[n].当前
				 if UserData[任务数据[n].id]  then
				 	self:刷新追踪任务信息(任务数据[n].id)
				 end
			end
		elseif 任务数据[n] ~= nil and (任务数据[n].类型 == "平定安邦" or 任务数据[n].类型 == "押镖") then
			任务数据[n].当前 = 任务数据[n].结束 - (os.time() - 任务数据[n].起始)
			if 任务数据[n].上次 - 任务数据[n].当前 >= 60 then
				任务数据[n].上次 = 任务数据[n].当前
				 if UserData[任务数据[n].id]  then
				 	self:刷新追踪任务信息(任务数据[n].id)
				 end
			end
			if UserData[任务数据[n].id] ~= nil then
				if 任务数据[n].当前 <= 0 then
					SendMessage(UserData[任务数据[n].id].连接id, 7, "#y/你的"..任务数据[n].类型.."时间到了")
					RoleControl:取消任务(UserData[任务数据[n].id],n)
					UserData[任务数据[n].id].摄妖香 = false
					任务数据[n] = nil
				end
			elseif 任务数据[n].类型 == "平定安邦" then
				任务数据[n] = nil
			end
		elseif 任务数据[n] ~= nil and 任务数据[n].类型 == "车迟" then
			任务数据[n].当前 = 7200 - (os.time() - 任务数据[n].起始)

			if 任务数据[n].上次 - 任务数据[n].当前 >= 60 then
				任务数据[n].上次 = 任务数据[n].当前
				 if UserData[任务数据[n].id]  then
				 self:刷新追踪任务信息(任务数据[n].id)
				 end
			end

			if 任务数据[n].当前 <= 0 then
				for i = 1, #任务数据[n].数字id do
					if UserData[任务数据[n].数字id[i]] ~= nil then
						SendMessage(UserData[任务数据[n].数字id[i]].连接id, 7, "#y/你的副本任务失败了")
						if UserData[任务数据[n].数字id[i]].战斗 ~= 0 then
							FightGet.战斗盒子[UserData[任务数据[n].数字id[i]].战斗]:强制结束战斗()
						end

						if UserData[任务数据[n].数字id[i]].地图 == 4131 or UserData[任务数据[n].数字id[i]].地图 == 4132 or UserData[任务数据[n].数字id[i]].地图 == 4133 then
							MapControl:Jump(任务数据[n].数字id[i], 1070, 127, 145)
						end
						RoleControl:GetTaskID(UserData[任务数据[n].数字id[i]],n)
						UserData[任务数据[n].数字id[i]].副本 = 0
					end
				end

				任务数据[n] = nil
			end
		elseif 任务数据[n] ~= nil and 任务数据[n].类型 == "乌鸡" then
			任务数据[n].当前 = 7200 - (os.time() - 任务数据[n].起始)
			if 任务数据[n].上次 - 任务数据[n].当前 >= 60 then
				任务数据[n].上次 = 任务数据[n].当前
				 if UserData[任务数据[n].id] ~= nil then
				 self:刷新追踪任务信息(任务数据[n].id)
				 end
			end
			if 任务数据[n].当前 <= 0 then
				for i = 1, #任务数据[n].数字id do
					if UserData[任务数据[n].数字id[i]] ~= nil then
						SendMessage(UserData[任务数据[n].数字id[i]].连接id, 7, "#y/你的副本任务失败了")

						-- if UserData[任务数据[n].数字id[i]].战斗 ~= 0 then
						-- 	FightGet.战斗盒子[UserData[任务数据[n].数字id[i]].战斗]:强制结束战斗()
						-- end

						if UserData[任务数据[n].数字id[i]].地图 == 3131 or UserData[任务数据[n].数字id[i]].地图 == 3132 then
							MapControl:Jump(任务数据[n].数字id[i], 1001, 284, 83)
						end

						RoleControl:GetTaskID(UserData[任务数据[n].数字id[i]],n)

						UserData[任务数据[n].数字id[i]].副本 = 0
					end
				end

				任务数据[n] = nil
			end
		end
		if 任务数据[n] ~= nil and 任务数据[n].类型 ~= "摄妖香"    and 任务数据[n].类型 ~= "秘红罗羹制"and 任务数据[n].类型 ~= "平定安邦"and 任务数据[n].类型 ~= "押镖"and 任务数据[n].类型 ~= "打造" and 任务数据[n].类型 ~= "门派首席" then
			if 任务数据[n].类型 == "地煞" and os.time() - 任务数据[n].起始 >= 600 then
				任务数据[n].限制 = 30
			end
			if 任务数据[n] ~= nil and  任务数据[n].结束 and 任务数据[n].结束 <= os.time() - 任务数据[n].起始 then
				if 任务数据[n].战斗 == false then
					if 任务数据[n].类型 == "帮派玄武" or 任务数据[n].类型 == "摇钱树" or 任务数据[n].类型 == "宝图" or 任务数据[n].类型 == "抓鬼" then
						MapControl:移除单位(任务数据[n].地图编号, n)
						if UserData[任务数据[n].id] ~= nil then
							RoleControl:取消任务(UserData[任务数据[n].id],n)
						end
						任务数据[n] = nil
          elseif LinkTask[任务数据[n].类型] then
             任务数据[n] = nil
          elseif 任务数据[n].事件 =="刷新" then
            MapControl:移除单位(任务数据[n].地图编号, n)
            任务数据[n] = nil
					elseif 任务数据[n].类型 == "天罡" or 任务数据[n].类型 == "宝箱" or 任务数据[n].类型 == "地煞" or
						任务数据[n].类型 == "天罡星69" or 任务数据[n].类型 == "天罡星2" or 任务数据[n].类型 == "天罡星3" or 任务数据[n].类型 == "天罡星4" or 任务数据[n].类型 == "天罡星5" or 任务数据[n].类型 == "天罡星6" then
						MapControl:移除单位(任务数据[n].地图编号, n)
						任务数据[n] = nil
					elseif 任务数据[n].类型 == "游泳" then
						for i = 1, #任务数据[n].数字id do
							if UserData[任务数据[n].数字id[i]] ~= nil and RoleControl:GetTaskID(UserData[任务数据[n].数字id[i]],"游泳") ~= 0 then
								RoleControl:GetTaskID(UserData[任务数据[n].数字id[i]],n)
								SendMessage(UserData[任务数据[n].数字id[i]].连接id, 7, "#y/你本次的游泳大赛资格已经被取消")
							end
						end
							任务数据[n] = nil
					elseif 任务数据[n].类型 == "除暴安良" then
						if 任务数据[n].分类 ~= 5 then
						MapControl:移除单位(任务数据[n].地图编号, n)
						end
						if UserData[任务数据[n].id] ~= nil then
							RoleControl:取消任务(UserData[任务数据[n].id],n)
						end
						任务数据[n] = nil
					elseif 任务数据[n].类型 == "群雄逐鹿" then
						if UserData[任务数据[n].id] ~= nil then
							RoleControl:取消任务(UserData[任务数据[n].id],n)
						end
						任务数据[n] = nil
					end
				elseif 任务数据[n].类型 == "变身卡" and UserData[任务数据[n].id] ~= nil then
				     if UserData[任务数据[n].id]~=nil then
                    self:结束变身(任务数据[n].id)
                    任务数据[n]=nil
             end
				elseif 任务数据[n].类型 == "双倍" and UserData[任务数据[n].id] ~= nil then
						RoleControl:取消任务(UserData[任务数据[n].id],RoleControl:GetTaskID(UserData[任务数据[n].id],"双倍"))
						SendMessage(UserData[任务数据[n].id].连接id,7,"#Y/你的双倍效果已经消失")
                    	UserData[任务数据[n].id].角色.双倍 =false
                    	任务数据[n]=nil
        elseif 任务数据[n].类型 == "天机培元丹" and UserData[任务数据[n].id] ~= nil then
                        RoleControl:取消任务(UserData[任务数据[n].id],RoleControl:GetTaskID(UserData[任务数据[n].id],"天机培元丹"))
                        SendMessage(UserData[任务数据[n].id].连接id,7,"#Y/你的天机培元丹效果已经消失")
                        任务数据[n]=nil
        elseif 任务数据[n].类型 == "九霄清心丸" and UserData[任务数据[n].id] ~= nil then
                        RoleControl:取消任务(UserData[任务数据[n].id],RoleControl:GetTaskID(UserData[任务数据[n].id],"九霄清心丸"))
                        SendMessage(UserData[任务数据[n].id].连接id,7,"#Y/你的九霄清心丸效果已经消失")
                        任务数据[n]=nil
				end
      elseif 任务数据[n].类型 == "帮战怪物" and  帮派竞赛.开关==false then
                        MapControl:移除单位(任务数据[n].地图编号, n)
                        任务数据[n] = nil
      elseif 任务数据[n].类型 == "帮战宝箱" and  帮派竞赛.宝箱开关==false then

                        MapControl:移除单位(任务数据[n].地图编号, n)
                        任务数据[n] = nil
			end
		end
	end
end
function TaskControl:索取任务信息(id)
	self.任务信息 = {}

	for n = 1, #UserData[id].角色.任务数据 do
		if UserData[id].角色.任务数据[n] ~= nil and 任务数据[UserData[id].角色.任务数据[n]] ~= nil then
			self.任务信息[#self.任务信息 + 1] = self:取任务信息(UserData[id].角色.任务数据[n], id)
		end
	end

	SendMessage(UserData[id].连接id, 10001, self.任务信息)
end
function TaskControl:生成任务id()
	self.id编号 = self.id编号 + 1
	return self.id编号
end
function TaskControl:触发任务(id,标识)---------------
 local 临时任务id=MapControl.MapData[UserData[id].地图].单位组[标识].任务id
 local 类型 =任务数据[临时任务id].类型
  if MapControl.MapData[UserData[id].地图].单位组[标识]==nil then
   SendMessage(UserData[id].连接id,7,"#Y/任务数据异常，代号9002")
   return 0
  elseif 任务数据[临时任务id]==nil then
   SendMessage(UserData[id].连接id,7,"#Y/任务数据异常，代号9001")
   return 0
  elseif  任务数据[临时任务id].战斗 then
    if 类型=="帮战怪物" then
         return 0
    else
         SendMessage(UserData[id].连接id,7,"#Y/该npc已经在战斗中了")
          return 0
    end
  elseif UserData[id].地图 ~= 任务数据[临时任务id].地图编号 then
  		封禁账号(UserData[id],"触发任务")
        return
 elseif 类型=="师门守卫" then
	  if UserData[id].队伍==0  then
	   SendMessage(UserData[id].连接id,7,"#y/该活动必须组队完成")
	   return 0
	  elseif 取队伍符合等级(id,65)==false and DebugMode== false then
	   广播队伍消息(UserData[id].连接id,7,"#y/该活动需要队伍所有成员等级达到65级")
	   return 0
	  else
	   任务数据[临时任务id].战斗=true
	   MapControl:NPC更改战斗(任务数据[临时任务id].地图编号,临时任务id)

	   FightGet:进入处理(id,100043,"66",临时任务id)
	   end

 elseif 类型=="帮战怪物" then

       任务数据[临时任务id].战斗=true
       MapControl:NPC更改战斗(任务数据[临时任务id].地图编号,临时任务id)
       FightGet:进入处理(id,100068,"66",临时任务id)

 elseif 类型=="抓鬼"  or 类型 == "鬼王"then
	  if type(任务数据[临时任务id].数字id)~="table" then
	   广播队伍消息(id,7,"#Y/任务数据异常，代号9002")
	  else
	  self.符合id=false
		for n=1,#任务数据[临时任务id].数字id do
			if 任务数据[临时任务id].数字id[n]==UserData[id].id then
				self.符合id=true
			end
		end
		if self.符合id then
			任务数据[临时任务id].战斗=true
			MapControl:NPC更改战斗(任务数据[临时任务id].地图编号,临时任务id)
			if 类型=="抓鬼" then
			    FightGet:进入处理(id,100006,"66",临时任务id)
			elseif 类型=="鬼王" then
				FightGet:进入处理(id,100051,"66",临时任务id)
			end

		else
			广播队伍消息(id,7,"#Y/你没有领取这样的任务")
		end
	  end
    elseif 类型=="终极boss" then
    if UserData[id].队伍 == 0 then
        广播队伍消息(id, 7, "#y/挑战本仙人需要队伍成员或者助战数达到3人")
    elseif (#队伍数据[UserData[id].队伍].队员数据 < 3 and DebugMode == false)
        and (zzxtflag == false) then
        广播队伍消息(id, 7, "#y/挑战本仙人需要队伍成员或者助战数达到3人")
    elseif 取队伍符合等级(id, 130) == false and DebugMode == false then
        广播队伍消息(id, 7, "#y/挑战本仙人需要队伍所有成员等级达到130级")
    elseif 取队伍高于等级(id, 180) == false then
        广播队伍消息(id, 7, "#y/挑战本仙人需要队伍所有成员等级低于180级")
    else
        任务数据[临时任务id].战斗 = true
        MapControl:NPC更改战斗(任务数据[临时任务id].地图编号,临时任务id)
        FightGet:进入处理(id, 888888, "66", 临时任务id)
    end


 elseif 类型=="天罡" then
      if UserData[id].队伍==0  then
       SendMessage(UserData[id].连接id,7,"#Y/此战斗需要三人组队进行")
       elseif  #队伍数据[UserData[id].队伍].队员数据<3 and DebugMode == false then
       广播队伍消息(id,7,"#Y/此战斗需要三人组队进行")
      elseif 取队伍符合等级(id,任务数据[临时任务id].等级-30)==false  and DebugMode == false then
       广播队伍消息(id,7,"#Y/此战斗需要队伍所有成员等级达到"..(任务数据[临时任务id].等级-30).."级")
      elseif 取队伍高于等级(id,任务数据[临时任务id].等级+30)==false  and DebugMode == false then
       广播队伍消息(id,7,"#Y/此战斗不允许有成员等级达到"..(任务数据[临时任务id].等级+任务数据[临时任务id].限制).."级")
      else
       任务数据[临时任务id].战斗=true
       MapControl:NPC更改战斗(任务数据[临时任务id].地图编号,临时任务id)
       FightGet:进入处理(id,任务数据[临时任务id].战斗类型,"66",临时任务id)
      end


 elseif 类型=="飞贼" then
      local 符合id = false
		for n = 1, #任务数据[临时任务id].数字id do
			if 任务数据[临时任务id].数字id[n] == UserData[id].id then
				符合id = true
			end
		end
		if 符合id then
			任务数据[临时任务id].战斗 = true
			MapControl:NPC更改战斗(任务数据[临时任务id].地图编号,临时任务id)
			FightGet:进入处理(id, 100008, "66", 临时任务id)
		else
			广播队伍消息(id, 7, "#y/你没有领取这样的任务")
		end
 elseif 类型=="地煞" then
      if UserData[id].队伍==0  then
       SendMessage(UserData[id].连接id,7,"#Y/此战斗需要三人组队进行")
       elseif  #队伍数据[UserData[id].队伍].队员数据<3 and DebugMode == false then
       广播队伍消息(id,7,"#Y/此战斗需要三人组队进行")
      elseif 取队伍符合等级(id,任务数据[临时任务id].等级-30)==false  and DebugMode == false then
       广播队伍消息(id,7,"#Y/此战斗需要队伍所有成员等级达到"..(任务数据[临时任务id].等级-30).."级")
      elseif 取队伍高于等级(id,任务数据[临时任务id].等级+30)==false  and DebugMode == false then
       广播队伍消息(id,7,"#Y/此战斗不允许有成员等级达到"..(任务数据[临时任务id].等级+任务数据[临时任务id].限制).."级")
      else
       任务数据[临时任务id].战斗=true
       MapControl:NPC更改战斗(任务数据[临时任务id].地图编号,临时任务id)
       FightGet:进入处理(id,任务数据[临时任务id].战斗类型,"66",临时任务id)
      end
      elseif 类型=="天罡星69" then
    if UserData[id].队伍 == 0 then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍成员数达到2人")
    elseif #队伍数据[UserData[id].队伍].队员数据 < 2 and DebugMode == false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍成员数达到2人")
    elseif 取队伍符合等级(id, 65) == false and DebugMode== false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍所有成员等级达到65级")
    elseif 取队伍高于等级(id, 99) == false and DebugMode== false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍所有成员等级低于100级")
    else
        任务数据[临时任务id].战斗 = true
        MapControl:NPC更改战斗(任务数据[临时任务id].地图编号,临时任务id)
        FightGet:进入处理(id, 888890, "66", 临时任务id)
    end

     elseif 类型=="天罡星2" then
    if UserData[id].队伍 == 0 then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍成员数达到2人")
    elseif #队伍数据[UserData[id].队伍].队员数据 < 2 and DebugMode == false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍成员数达到2人")
    elseif 取队伍符合等级(id, 100) == false   and DebugMode == false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍所有成员等级达到100级")
        elseif 取队伍高于等级(id, 129) == false and DebugMode == false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍所有成员等级低于130级")

    else
        任务数据[临时任务id].战斗 = true
        MapControl:NPC更改战斗(任务数据[临时任务id].地图编号,临时任务id)
        FightGet:进入处理(id, 888891, "66", 临时任务id)
    end

     elseif 类型=="天罡星3" then
    if UserData[id].队伍 == 0 then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍成员数达到2人")
    elseif #队伍数据[UserData[id].队伍].队员数据 < 2 and DebugMode == false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍成员数达到2人")
    elseif 取队伍符合等级(id, 130) == false   and DebugMode == false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍所有成员等级达到130级")
        elseif 取队伍高于等级(id, 155) == false and DebugMode == false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍所有成员等级低于156级")
           --elseif 取队伍飞升状态(id)==false  then
           --广播队伍消息(id,7,"#y/此战斗需要队伍所有成员等级达到完成飞升挑战")
    --return 0
    else
        任务数据[临时任务id].战斗 = true
        MapControl:NPC更改战斗(任务数据[临时任务id].地图编号,临时任务id)
        FightGet:进入处理(id, 888892, "66", 临时任务id)
    end

     elseif 类型=="天罡星4" then
    if UserData[id].队伍 == 0 then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍成员数达到3人")
    elseif #队伍数据[UserData[id].队伍].队员数据 < 1 and DebugMode == false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍成员数达到3人")
    elseif 取队伍符合等级(id, 129) == false and DebugMode == false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍所有成员等级达到129级")
    elseif 取队伍高于等级(id, 155) == false  and DebugMode == false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍所有成员等级低于156级")
        elseif 取队伍飞升状态(id)==false  then
           广播队伍消息(id,7,"#y/此战斗需要队伍所有成员等级达到完成飞升挑战")
    return 0

    else
        任务数据[临时任务id].战斗 = true
        MapControl:NPC更改战斗(任务数据[临时任务id].地图编号,临时任务id)
        FightGet:进入处理(id, 888893, "66", 临时任务id)
    end

     elseif 类型=="天罡星5" then
    if UserData[id].队伍 == 0 then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍成员数达到3人")
    elseif #队伍数据[UserData[id].队伍].队员数据 < 3 and DebugMode == false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍成员数达到3人")
    elseif 取队伍符合等级(id, 156) == false and DebugMode == false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍所有成员等级达到156级")
    elseif 取队伍高于等级(id, 175) == false  and DebugMode == false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍所有成员等级低于180级")

    else
        任务数据[临时任务id].战斗 = true
        MapControl:NPC更改战斗(任务数据[临时任务id].地图编号,临时任务id)
        FightGet:进入处理(id, 888894, "66", 临时任务id)
    end

     elseif 类型=="天罡星6" then
    if UserData[id].队伍 == 0 then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍成员数达到3人")
    elseif #队伍数据[UserData[id].队伍].队员数据 < 3 and DebugMode == false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍成员数达到3人")
    elseif 取队伍符合等级(id, 170) == false and DebugMode == false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍所有成员等级达到170级")
    elseif 取队伍高于等级(id, 180) == false  and DebugMode == false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍所有成员等级低于180级")

    else
        任务数据[临时任务id].战斗 = true
        MapControl:NPC更改战斗(任务数据[临时任务id].地图编号,临时任务id)
        FightGet:进入处理(id, 888895, "66", 临时任务id)
    end
    end
 end
 function TaskControl:添加梦幻瓜子(id)
  local 临时id6 = tonumber(UserData[id].id .. "6")
  local 地图编号6 = UserData[id].地图
  local 临时坐标6 = {
    x = math.floor(UserData[id].角色.地图数据.x/20),
    y = math.floor(UserData[id].角色.地图数据.y/20)
  }
  local 地图名称6 = MapData[地图编号6].名称
  任务数据[临时id6] = {
    分类 = 1,
    名称 = "梦幻瓜子",
    方向 = 0,
    结束 = 7200,
    造型 = "摇钱树",
    类型 = "梦幻瓜子",
    战斗 = false,
    id = id,
    起始 = os.time(),
    任务id = 临时id6,
    地图编号 = 地图编号6,
    地图名称 = 地图名称6,
    次数 = math.random(3, 7),
    坐标 = 临时坐标6,
    数字id = UserData[id].id
  }

  MapControl:添加单位(任务数据[临时id6])
  UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])] = 临时id6
  SendMessage(UserData[id].连接id, 7, "#y/你种下了一颗梦幻瓜子")
 end
function TaskControl:添加摇钱树(id)
	local 临时id6 = tonumber(UserData[id].id .. "6")
	local 地图编号6 = UserData[id].地图
	local 临时坐标6 = {
		x = math.floor(UserData[id].角色.地图数据.x/20),
		y = math.floor(UserData[id].角色.地图数据.y/20)
	}
	local 地图名称6 = MapData[地图编号6].名称
	任务数据[临时id6] = {
		分类 = 1,
		名称 = "摇钱树",
		方向 = 0,
		结束 = 7200,
		造型 = "摇钱树",
		类型 = "摇钱树",
		战斗 = false,
		id = id,
		起始 = os.time(),
		任务id = 临时id6,
		地图编号 = 地图编号6,
		地图名称 = 地图名称6,
		次数 = math.random(3, 7),
		坐标 = 临时坐标6,
		数字id = UserData[id].id
	}

	MapControl:添加单位(任务数据[临时id6])
	UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])] = 临时id6
	SendMessage(UserData[id].连接id, 7, "#y/你种下了一颗摇钱树")
 end
function TaskControl:触发单人任务(id,标识)-----完成
 if MapControl.MapData[UserData[id].地图].单位组[标识]==nil then return 0 end
 local 任务id=MapControl.MapData[UserData[id].地图].单位组[标识].任务id

 local 类型 =任务数据[任务id].类型
 if 任务数据[任务id]==nil then
   SendMessage(UserData[id].连接id,7,"#Y/任务数据异常，代号9001")
  elseif  任务数据[任务id].数字id~=UserData[id].id then
   SendMessage(UserData[id].连接id,7,"#Y/你无法完成这样的任务")
  elseif  任务数据[任务id].战斗 then
   SendMessage(UserData[id].连接id,7,"#Y/任务数据异常，代号9002")
  elseif UserData[id].队伍 ~= 0 then
	SendMessage(UserData[id].连接id, 7, "#y/该任务不允许组队完成")
  elseif UserData[id].地图 ~= 任务数据[任务id].地图编号 then
		封禁账号(UserData[id],"触发单人任务")
	  return
  else
    if 类型 =="除暴安良" then
     FightGet:进入处理(id,100002,"66",任务id)
     elseif 类型 == "官职" then
     	FightGet:进入处理(id, 100010, "66", 任务id)
     elseif 类型=="宝图" then
        if 任务数据[任务id].分类==1 then
           FightGet:进入处理(id,100003,"66",任务id)
        else
          FightGet:进入处理(id,100049,"66",任务id)
        end
     end
      任务数据[任务id].战斗=true
     MapControl:NPC更改战斗(任务数据[任务id].地图编号,任务id)
   end
 end
function TaskControl:添加秘制红罗羹(id)
	if RoleControl:GetTaskID(UserData[id],"秘制红罗羹") ~= 0 then
		任务数据[RoleControl:GetTaskID(UserData[id],"秘制红罗羹")].次数 = 任务数据[RoleControl:GetTaskID(UserData[id],"秘制红罗羹")].次数 + 100
        self:刷新追踪任务信息(id)
		return 0
	end

	local 临时id6 = UserData[id].id .. "402"
	任务数据[临时id6] = {
		上次 = 0,
		类型 = "秘制红罗羹",
		当前 = 0,
		次数 = 100,
		id = id,
		起始 = os.time()
	}
	UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])] = 临时id6
	self:刷新追踪任务信息(id)
 end
function TaskControl:添加摄妖香(id)
	local 临时id6 = UserData[id].id .. "1"
	任务数据[临时id6] = {
		上次 = 1800,
		类型 = "摄妖香",
		当前 = 1800,
		id = id,
		起始 = os.time()
	}
	UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])] = 临时id6
	self:刷新追踪任务信息(id)
 end

function TaskControl:添加天机培元丹(id)
    local 临时id6 = UserData[id].id .. "3820"
    任务数据[临时id6] = {
        上次 = 3600*3,
        类型 = "天机培元丹",
        当前 = 3600*3,
        结束=3600*3,
        id = id,
        起始 = os.time()
    }
    UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])] = 临时id6
    self:刷新追踪任务信息(id)

 end
 function TaskControl:添加九霄清心丸(id)
    local 临时id6 = UserData[id].id .. "3821"
    任务数据[临时id6] = {
        上次 = 3600,
        类型 = "九霄清心丸",
        当前 = 3600,
        结束=3600,
        id = id,
        起始 = os.time()
    }
    UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])] = 临时id6
    self:刷新追踪任务信息(id)

 end
function TaskControl:添加双倍(id)
	local 临时id6 = UserData[id].id .. "3819"
	任务数据[临时id6] = {
		上次 = 3600,
		类型 = "双倍",
		当前 = 3600,
 		结束=3600,
		id = id,
		起始 = os.time()
	}
	UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])] = 临时id6
	UserData[id].角色.双倍=true
	self:刷新追踪任务信息(id)

 end

function TaskControl:领取双倍经验(id,时间)
	if  活动数据.双倍[id]==nil then
           	活动数据.双倍[id] = 3600*3
    end
    if 活动数据.双倍[id] < 时间 * 3600 then
       SendMessage(UserData[id].连接id, 7, "#y/你当前没有这么多的双倍时间")
       return
    else
    	活动数据.双倍[id]=活动数据.双倍[id]-时间 * 3600
    end
	if RoleControl:GetTaskID(UserData[id],"双倍") ~= 0 then
		任务数据[RoleControl:GetTaskID(UserData[id],"双倍")].结束 = 任务数据[RoleControl:GetTaskID(UserData[id],"双倍")].结束+时间 * 3600
	else
			local 临时id6 = UserData[id].id .. "3819"
			任务数据[临时id6] = {
				上次 = 3600*时间,
				类型 = "双倍",
				当前 = 3600*时间,
		 		结束=3600*时间,
				id = id,
				起始 = os.time()
			}
			UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])] = 临时id6
			UserData[id].角色.双倍=true
	end
		SendMessage(UserData[id].连接id, 20,{"御林军","马副将","领取了"..时间.."小时的双倍经验，赶紧去刷经验吧"})
		self:刷新追踪任务信息(id)
 end




function TaskControl:加载首席单位()
	local 首席弟子={
	 龙宫={地图=1116,方向=1,x=1893,y=1347}
	,女儿村={地图=1142,方向=0,x=490,y=608}
	,化生寺={地图=1002,方向=1,x=969,y=1309}
	,大唐官府={地图=1198,方向=0,x=1950,y=1349}
	,普陀山={地图=1140,方向=0,x=246,y=290}
	,地府={方向=0,x=788,y=1187}
	,五庄观={地图=1146,方向=1,x=811,y=949}
	,盘丝岭={方向=1,x=3632,y=548}
	,魔王寨={地图=1512,方向=1,x=1550,y=548}
	,狮驼岭={地图=1131,方向=1,x=2210,y=1610}
	,天宫={地图=1111,方向=0,x=3469,y=2429}
	,方寸山={地图=1135,方向=1,x=2000,y=940}
	,盘丝洞={地图=1513,方向=1,x=3632,y=548}
	,阴曹地府={地图=1122,方向=0,x=788,y=1187}
	,凌波城={地图=1150,方向=1,x=673,y=1359}
	,神木林={地图=1138,方向=0,x=903,y=2061}
	,无底洞={地图=1139,方向=0,x=1233,y=2525}
    ,花果山={地图=1251,方向=0,x=62*20,y=32*20}
    ,天机城={地图=1250,方向=1,x=116*20,y=49*20}
    ,女魃墓={地图=1249,方向=1,x=82*20,y=33*20}
	}
	local 首席弟子数据类={
	[1]="大唐官府",
	[2]="化生寺",
	[3]="女儿村",
	[4]="方寸山",
	[5]="天宫",
	[6]="龙宫",
	[7]="五庄观",
	[8]="普陀山",
	[9]="魔王寨",
	[10]="狮驼岭",
	[11]="盘丝洞",
	[12]="阴曹地府",
	[13]="凌波城",
	[14]="无底洞",
	[15]="神木林",
    [16]="花果山",
    [17]="天机城",
    [18]="女魃墓",
	}
	for n = 1, #首席资源数据 do
		self.门派名称 = 首席弟子数据类[n]
		self.闯关id = self:生成任务id()
		任务数据[self.闯关id] = {
			结束 = 0,
			数字id = 0,
			战斗 = false,
			场次 = 1,
			积分 = 0,
			类型 = "门派首席",
			等级 = 0,
			id = self.闯关id,
			称谓 = self.门派名称.."首席大弟子",
			起始 = os.time(),
			编号 = 首席资源数据[n].造型,
			任务id = self.闯关id,
			地图编号 = 首席弟子[self.门派名称].地图,
			地图名称 = self.门派名称,
			名称 = 首席资源数据[n].名称,
			造型 = 首席资源数据[n].造型,
			染色 = 首席资源数据[n].染色,
			染色方案 = 首席资源数据[n].染色方案,
			门派 = self.门派名称,
			方向 = 首席弟子[self.门派名称].方向,
			坐标 = {
				x = 首席弟子[self.门派名称].x/20,
				y = 首席弟子[self.门派名称].y/20
			},
			武器 = {
				名称 = 首席资源数据[n].武器数据.名称,
				等级 = 首席资源数据[n].武器数据.等级,
				强化 = 首席资源数据[n].武器数据.强化,
				类别 = 首席资源数据[n].武器数据.类别
			},
			门派编号 = n
		}

		MapControl:添加单位(任务数据[self.闯关id])
	end
 end
function TaskControl:更换首席弟子(id, 任务id)
	for n = 1, #UserData[id].角色.称谓 do
		if UserData[id].角色.称谓[n] == 任务数据[任务id].门派 .. "首席弟子" then
			SendMessage(UserData[id].连接id, 7, "#y/你已经领取过此称谓了")

			return 0
		end
	end
	if id ~= 首席资源数据[任务数据[任务id].门派编号].玩家id then
		SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有成为本门派首席弟子")
		return 0
	end
	MapControl:移除单位(任务数据[任务id].地图编号, 任务id)
	首席资源数据[任务数据[任务id].门派编号] = {
		奖励领取 = true,
		名称 = UserData[id].角色.名称,
		造型 = UserData[id].角色.造型,
		染色 = UserData[id].角色.染色,
		武器数据 = UserData[id].角色.武器数据,
		玩家id = UserData[id].id
	}
	任务数据[任务id].名称 = UserData[id].角色.名称
	任务数据[任务id].编号 = UserData[id].角色.造型
	任务数据[任务id].造型 = UserData[id].角色.造型
	任务数据[任务id].染色 = UserData[id].角色.染色
	任务数据[任务id].染色方案 = UserData[id].角色.染色方案
	任务数据[任务id].武器 = {
		名称 = 首席资源数据[任务数据[任务id].门派编号].武器数据.名称,
		等级 = 首席资源数据[任务数据[任务id].门派编号].武器数据.等级,
		强化 = 首席资源数据[任务数据[任务id].门派编号].武器数据.强化,
		类别 = 首席资源数据[任务数据[任务id].门派编号].武器数据.类别
	}
	MapControl:添加单位(任务数据[任务id])
	RoleControl:添加称谓(UserData[id], 任务数据[任务id].门派 .. "首席弟子")
	ItemControl:GiveItem(id, "珍珠",  math.random(15, 15) * 10)
	UserData[id].角色.门贡 = UserData[id].角色.门贡 + 20
	SendMessage(UserData[id].连接id, 7, "#y/你获得了20点门派贡献度")
	SendMessage(UserData[id].连接id, 7, "#y/你获得了珍珠")
	广播门派消息(UserData[id].角色.门派, "#" .. 门派代号[UserData[id].角色.门派] .. "/#r/" .. UserData[id].角色.名称 .. "#w/已经领取了首席弟子称谓，成为了新一代的本门派首席弟子。")
 end
function TaskControl:刷出师门守卫()
	self.随机门派 = 首席弟子门派类[math.random( #首席弟子门派类)]
	self.天关地图 = 门派传送[self.随机门派].z
	self.天关名称 = MapData[self.天关地图].名称
	门派入侵者数量 = 0

	for n = 1, math.random(8, 12) do
		门派入侵者数量 = 门派入侵者数量 + 1
		self.天关id = self:生成任务id()
		self.天关坐标 = MapControl:Randomloadtion(self.天关地图)
		任务数据[self.天关id] = {
			数字id = 0,
			战斗 = false,
			场次 = 1,
			结束 = 3500,
			变异 = true,
			名称 = "门派入侵者",
			类型 = "师门守卫",
			编号 = 1215,
			积分 = 0,
			造型 = "强盗",
			等级 = 0,
			id = self.天关id,
			起始 = os.time(),
			任务id = self.天关id,
			地图编号 = self.天关地图,
			地图名称 = self.天关名称,
			方向 = math.random( 4) - 1,
			坐标 = self.天关坐标,
			门派 = self.随机门派
		}

		MapControl:添加单位(任务数据[self.天关id])
	end

	-- 广播消息("#xt/#y/一群神秘入侵者正在#r/" .. self.随机门派 .. "#y/作恶，请本门派弟子速回本门派防卫。")
	广播门派消息(self.随机门派, "#" .. 门派代号[self.随机门派] .. "/#y/" .. "一群神秘入侵者正在本门内作恶，请所有弟子立即赶回本门防卫。")
	for n, v in pairs(UserData) do
		if UserData[n] ~= nil and UserData[n].角色.门派 == self.随机门派 then
			RoleControl:添加系统消息(UserData[n],"#w/你的门派正在遭受不明身份者入侵")
		end
	end
 end

function TaskControl:完成师门守卫(任务id, id组)
	for n = 1, #id组 do
		if UserData[id组[n]] ~= nil then
			self.符合抓鬼id = true

			if self.符合抓鬼id then
				self.抓鬼奖励参数 = 1
				self.抓鬼奖励等级 = UserData[id组[n]].角色.等级
				self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * self.抓鬼奖励等级 * 50) * 3
				self.奖励银子 = self.抓鬼奖励等级 * self.抓鬼奖励等级 * 10

				RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验,"师门守卫")
                RoleControl:添加银子(UserData[id组[n]],self.奖励银子, "师门守卫")
				if UserData[id组[n]].召唤兽.数据.参战 ~= 0 then
					self.抓鬼奖励等级 = UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
					self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * self.抓鬼奖励等级 * 12 * 0.35 + 150)

					UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验, id组[n], UserData[id组[n]].召唤兽.数据.参战, 2)
				end

				if 任务数据[任务id].门派 == UserData[id组[n]].角色.门派 then
					UserData[id组[n]].角色.门贡 = UserData[id组[n]].角色.门贡 + 3

					SendMessage(UserData[id组[n]].连接id, 7, "#y/你获得了3点门派贡献度")
				end
           self.奖励参数=math.random(3)
          if self.奖励参数<=1 then
					ItemControl:GiveItem(id组[n], "海马")
					SendMessage(UserData[id组[n]].连接id, 9, "#dq/#w/ 你获得了" .. "海马")

            elseif self.奖励参数<=2 then
					ItemControl:GiveItem(id组[n], "彩果",  1)
            elseif self.奖励参数<=3 then
				self.宝石名称 =取随机宝石()
				self.随机等级 = math.random(2, 6)
					ItemControl:GiveItem(id组[n], self.宝石名称, math.random( 2))
					SendMessage(UserData[id组[n]].连接id, 9, "#dq/#w/ 你获得了" .. self.宝石名称)
				end
			end
		end
	end

	MapControl:移除单位(UserData[id组[1]].地图, 任务id)

	门派入侵者数量 = 门派入侵者数量 - 1

	if 门派入侵者数量 == 0 then
		广播门派消息(任务数据[任务id].门派, "#w/本门内的神秘入侵者已经全部被消灭，本门派又恢复了往日的平静。")
	else
		广播门派消息(任务数据[任务id].门派, "#" .. 门派代号[self.随机门派] .. "/#w/门派内还有#r/" .. 门派入侵者数量 .. "#w/个神秘入侵者")
	end

	任务数据[任务id] = nil
 end
function TaskControl:关闭幻域迷宫()
	幻域迷宫开关 = false

	发送游戏公告("幻域迷宫活动已经关闭，感谢各位玩家的参与。")

	for n, v in pairs(任务数据) do
		if 任务数据[n] ~= nil and 任务数据[n].类型 == "迷宫" then
			MapControl:移除单位(任务数据[n].地图编号, n)

			任务数据[n] = nil
		end
	end
end
function TaskControl:开启幻域迷宫()
	幻域迷宫开关 = true
	幻域迷宫数据 = {
		排名奖励 = {}
	}
	for n = 1, 19 do
		self.加载数量 = 2
		if n == 1 then
			self.加载数量 = 1
		end
		self.加载地图 = n + 7000
		self.真假随机 = math.random( 2)
		for i = 1, self.加载数量 do
			self.临时坐标7000 =  MapControl:Randomloadtion(self.加载地图)
			self.临时id7000 = self:生成任务id()
			任务数据[self.临时id7000] = {
				类型 = "迷宫",
				编号 = 1224,
				名称 = "传送员",
				结束 = 3700,
				数字id = 0,
				战斗 = false,
				造型 = "天兵",
				id = 0,
				真假 = false,
				起始 = os.time(),
				任务id = self.临时id7000,
				地图编号 = self.加载地图,
				地图名称 = "幻域" .. n .. "层",
				方向 = math.random( 4) - 1,
				坐标 = self.临时坐标7000
			}

			if self.真假随机 == i or n == 1 then
				任务数据[self.临时id7000].真假 = true
			end

			MapControl:添加单位(任务数据[self.临时id7000])
		end
	end

	发送游戏公告("幻域迷宫活动已经开启，各位玩家可前往傲来国金毛猿处参加本活动")
end
function TaskControl:帮派扣费()
	for n, v in pairs(帮派数据) do
		if 帮派数据[n] ~= nil and n ~= "编号" then
			self.增加人气 = 0

			for q, w in pairs(帮派数据[n].成员名单) do
				if UserData[q] ~= nil then
					self.增加人气 = self.增加人气 + 1
				end
			end

			self.增加人气 = math.floor(self.增加人气 * 0.5) + 1
			self.基础费用 = 帮派数据[n].规模 * 8500 + 20000 + 帮派数据[n].书院 * 2500 + 帮派数据[n].仓库 * 6500
			self.最低费用 = 帮派数据[n].规模 * 8500 + 20000
			帮派数据[n].安定 = 帮派数据[n].安定 - 1
			帮派数据[n].人气 = 帮派数据[n].人气 + self.增加人气

			广播帮派消息(n, "#bp/#w/帮派的人气增加了" .. self.增加人气 .. "点")

			if self.基础费用 <= 帮派数据[n].资金 then
				self.资材增加 = 200 + 帮派数据[n].规模 * 20 + 帮派数据[n].仓库 * 20
				帮派数据[n].资金 = 帮派数据[n].资金 - self.基础费用

				广播帮派消息(n, "#bp/#w/帮派扣除了" .. self.基础费用 .. "两维护费用")
				广播帮派消息(n, "#bp/#w/帮派的资材增加了" .. self.资材增加 .. "点")
				广播帮派消息(n, "#bp/#w/帮派扣除了1点安定")

				帮派数据[n].资材 = 帮派数据[n].资材 + self.资材增加

				if 帮派数据[n].资材 > 帮派数据[n].仓库 * 100 + 100 then
					帮派数据[n].资材 = 帮派数据[n].仓库 * 100 + 100
				end
			elseif self.最低费用 <= 帮派数据[n].资金 then
				帮派数据[n].资金 = 帮派数据[n].资金 - self.最低费用

				广播帮派消息(n, "#bp/#w/帮派扣除了" .. self.最低费用 .. "两基础费用")
				广播帮派消息(n, "#bp/#w/因帮派资金无法满足维护帮派的需求，所以帮派资材无法增加")
				广播帮派消息(n, "#bp/#w/帮派扣除了1点安定")
			else
				帮派数据[n].安定 = 帮派数据[n].安定 - 10

				广播帮派消息(n, "#bp/#w/因帮派资金无法满足基础费用，导致安定度减少了10点。")
				广播帮派消息(n, "#bp/#w/因帮派资金无法满足维护帮派的需求，所以帮派资材无法增加")
				广播帮派消息(n, "#bp/#w/帮派扣除了1点安定")
			end
		end
	end
end
function TaskControl:开启三界书院()
	三界书院 = {
		答案 = "",
		开关 = true,
		结束 = 65,
		起始 = os.time(),
		间隔 = math.random(30, 90) * 60,
		名单 = {}
	}

    local 题目序列 = math.random(548, #QuretionBank)
	self.发言内容 = "#xt/#y/大家好，又到了考验大家知识水平的时间了。60秒内在世界频道中回答正确的玩家可以获得丰厚奖励噢。问题如下：#w/" .. QuretionBank[题目序列].问题 .. "？"
	三界书院.答案 = QuretionBank[题目序列].选择3
	广播消息(self.发言内容)
end
function TaskControl:刷出新手宝箱()
	self.天关地图 = 1501
	self.天关名称 = MapData[self.天关地图].名称
	self.宝箱名称 = {
		"经验宝箱",
		"储备宝箱",
		"储备宝箱",
		"储备宝箱",
		"经验宝箱",
		"经验宝箱",
		"经验宝箱",
		"储备宝箱",
		"经验宝箱",
		"经验宝箱",
		"储备宝箱",
		"银子宝箱"
	}

	for n = 1, math.random(3, 6) do
		self.天关id = self:生成任务id()
		self.天关坐标 = MapControl:Randomloadtion(self.天关地图)
		任务数据[self.天关id] = {
			类型 = 104,
			数字id = 0,
			结束 = 600,
			战斗 = false,
			场次 = 1,
			造型 = "进阶古代瑞兽",
			积分 = 0,
			等级 = 0,
			id = self.天关id,
			起始 = os.time(),
			任务id = self.天关id,
			地图编号 = self.天关地图,
			地图名称 = self.天关名称,
			名称 = self.宝箱名称[math.random( #self.宝箱名称)],
			方向 = math.random( 4) - 1,
			坐标 = self.天关坐标
		}

		MapControl:添加单位(任务数据[self.天关id])
	end

	self.天关地图 = 1506
	self.天关名称 = MapData[self.天关地图].名称

	for n = 1, math.random(3, 6) do
		self.天关id = self:生成任务id()
		self.天关坐标 = MapControl:Randomloadtion(self.天关地图)
		任务数据[self.天关id] = {
			类型 = 104,
			数字id = 0,
			结束 = 600,
			战斗 = false,
			场次 = 1,
			造型 = "宝箱",
			积分 = 0,
			等级 = 0,
			id = self.天关id,
			起始 = os.time(),
			任务id = self.天关id,
			地图编号 = self.天关地图,
			地图名称 = self.天关名称,
			名称 = self.宝箱名称[math.random( #self.宝箱名称)],
			方向 = math.random( 4) - 1,
			坐标 = self.天关坐标
		}

		MapControl:添加单位(任务数据[self.天关id])
	end

	self.天关地图 = 1092
	self.天关名称 = MapData[self.天关地图].名称

	for n = 1, math.random(3, 6) do
		self.天关id = self:生成任务id()
		self.天关坐标 = MapControl:Randomloadtion(self.天关地图)
		任务数据[self.天关id] = {
			类型 = 104,
			数字id = 0,
			结束 = 600,
			战斗 = false,
			场次 = 1,
			造型 = "宝箱",
			积分 = 0,
			等级 = 0,
			id = self.天关id,
			起始 = os.time(),
			任务id = self.天关id,
			地图编号 = self.天关地图,
			地图名称 = self.天关名称,
			名称 = self.宝箱名称[math.random( #self.宝箱名称)],
			方向 = math.random( 4) - 1,
			坐标 = self.天关坐标
		}

		MapControl:添加单位(任务数据[self.天关id])
	end

	广播消息("#xt/#y/有很多神秘宝箱从天而降，据说有人在#g/建邺城、东海湾、傲来国#y/这些地方看到过这些神秘宝箱。")
end
function TaskControl:触发新手宝箱(id, 任务id)
	if MapControl.MapData[UserData[id].地图].单位组[任务id] == nil then
		return 0
	end
	self.任务id6 = MapControl.MapData[UserData[id].地图].单位组[任务id].id
	if UserData[id].角色.等级 >= 100 then
		SendMessage(UserData[id].连接id, 7, "#y/只有等级低于100级的玩家才可开启此宝箱")
		return 0
	elseif UserData[id].角色.当前体力 < 10 then
		SendMessage(UserData[id].连接id, 7, "#y/你似乎没有那么多的体力")
		return 0
	elseif 任务数据[self.任务id6] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/错误的数据")
		return 0
	end

	self.宝箱名称 = 任务数据[self.任务id6].名称
	MapControl:移除单位(任务数据[self.任务id6].地图编号, self.任务id6)
	任务数据[self.任务id6] = nil
	UserData[id].角色.当前体力 = UserData[id].角色.当前体力 - 10
	SendMessage(UserData[id].连接id, 7, "#y/你消耗10点体力开启了一个#g/" .. self.宝箱名称)
	if self.宝箱名称 == "经验宝箱" then
		self.临时等级 = UserData[id].角色.等级
		self.临时经验 = self.临时等级 * 1050 + 5000
		RoleControl:添加经验(UserData[id],self.临时经验 * 5, id, 402)
	elseif self.宝箱名称 == "储备宝箱" then
		RoleControl:添加储备(UserData[id],UserData[id].角色.等级 * UserData[id].角色.等级 * 10, 14)
	elseif self.宝箱名称 == "银子宝箱" then
        RoleControl:添加银子(UserData[id],UserData[id].角色.等级 * UserData[id].角色.等级 * 10, "银子宝箱")
	end
end

return TaskControl