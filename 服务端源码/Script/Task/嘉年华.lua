-- @Author: 作者QQ381990860
-- @Date:   2021-09-07 00:54:26
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-09-09 15:29:20
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-12-23 21:00:26
--======================================================================--
function TaskControl:添加嘉年华任务(id)
	local 临时id3 = tonumber(UserData[id].id .. "4")
	任务数据[临时id3] = {
		战斗 = false,
		方向 = 0,
		名称 = 0,
		结束 = 3600,
		编号 = 0,
		类型 = "嘉年华",
		进程 = 1,
		造型 = 0,
		id = id,
		NPC=119303,
		起始 = os.time(),
		任务id = 临时id3,
		数字id = {},
		平均等级 =0,
		客户id = {}
	}
	self.任务id组 = {}
	if UserData[id].队伍 == 0 then
		self.任务id组[#self.任务id组 + 1] = id
	else
		for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
			self.任务id组[#self.任务id组 + 1] = 队伍数据[UserData[id].队伍].队员数据[n]
			SendMessage(UserData[队伍数据[UserData[id].队伍].队员数据[n]].连接id, 7, "#y/你领取了嘉年华任务")
			活动数据.嘉年华数据[队伍数据[UserData[id].队伍].队员数据[n]] = {false,false,false,false}
		end
	end

	for n = 1, #self.任务id组 do
		self.任务临时id1 = RoleControl:生成任务id(UserData[self.任务id组[n]])
		UserData[self.任务id组[n]].角色.任务数据[self.任务临时id1] = 临时id3
		任务数据[临时id3].数字id[#任务数据[临时id3].数字id + 1] = UserData[self.任务id组[n]].id
		任务数据[临时id3].客户id[#任务数据[临时id3].客户id + 1] = self.任务id组[n]
		self:刷新追踪任务信息(self.任务id组[n])
	end
 end
function TaskControl:完成嘉年华任务(id组, 任务id)
	self.临时经验4 = math.floor(任务数据[任务id].平均等级 * 任务数据[任务id].平均等级 * 30)
	self.临时金钱4 = math.floor(任务数据[任务id].平均等级 * 任务数据[任务id].平均等级 * 6 * 0.6) * 4
    任务数据[任务id].进程 = 任务数据[任务id].进程 + 1
	任务数据[任务id].NPC=111009
	for n = 1, #id组 do
		if 活动数据.嘉年华数据[id组[n]] ~= nil and 活动数据.嘉年华数据[id组[n]][1] == false then
			活动数据.嘉年华数据[id组[n]][1] = true
			RoleControl:添加银子(UserData[id组[n]],self.临时金钱4,"嘉年华")
			RoleControl:添加经验(UserData[id组[n]],self.临时经验4, "嘉年华")
			ItemControl:GiveItem(id组[n],取召唤兽随机宝石(), 1)
			SendMessage(UserData[id组[n]].连接id, 7, "#y/你的嘉年华任务进度已经更新")
			self:刷新追踪任务信息(id组[n])
		else
			SendMessage(UserData[id组[n]].连接id, 7, "#y/嘉年华奖励每日只可得到1次")
		end
	end

	
 end
function TaskControl:完成嘉年华任务2(id组, 任务id)
	self.临时经验4 = math.floor(任务数据[任务id].平均等级 * 任务数据[任务id].平均等级 * 30) * 4
	self.临时金钱4 = math.floor(任务数据[任务id].平均等级 * 任务数据[任务id].平均等级 * 6 * 0.6) * 6
		任务数据[任务id].进程 = 任务数据[任务id].进程 + 1
    任务数据[任务id].NPC=151301
	for n = 1, #id组 do
		if 活动数据.嘉年华数据[id组[n]] ~= nil and 活动数据.嘉年华数据[id组[n]][2] == false then
			活动数据.嘉年华数据[id组[n]][2] = true
			RoleControl:添加银子(UserData[id组[n]],self.临时金钱4,"嘉年华")
			RoleControl:添加经验(UserData[id组[n]],self.临时经验4, "嘉年华")
			SendMessage(UserData[id组[n]].连接id, 7, "#y/你的嘉年华任务进度已经更新")
                 local 卡片 ={"海星","狸","章鱼","大海龟","大蝙蝠","赌徒","海毛虫","护卫","巨蛙","强盗","山贼","树怪" ,"蛤蟆精","黑熊","狐狸精","花妖",
                         "老虎","羊头怪","骷髅怪","狼","牛妖","虾兵","小龙女","蟹将","野鬼","龟丞相","黑熊精","僵尸","马面","牛头","兔子怪","蜘蛛精","白熊",
                         "古代瑞兽","黑山老妖","蝴蝶仙子","雷鸟人","地狱战神", "风伯","天兵","进阶天兵","天将","凤凰" ,"雨师","蛟龙","蚌精",
                         "碧水夜叉","百足将军","锦毛貂精","镜妖","泪妖","千年蛇魅","如意仙子","鼠先锋","星灵仙子","巡游天神","野猪精",
                         "芙蓉仙子","犀牛将军人形","犀牛将军兽形","阴阳伞","巴蛇","龙龟","大力金刚","鬼将","红萼仙子","葫芦宝贝","画魂","机关鸟","金饶僧" ,"净瓶女娲",
                         "灵鹤","灵符女娲","律法女娲","琴仙","踏云兽","雾中仙",
                         "吸血鬼","噬天虎","炎魔神","幽灵" ,"幽莹娃娃","夜罗刹"
                        }
         ItemControl:GiveItem(id组[n],"怪物卡片",nil,卡片[math.random(1,#卡片)])
			SendMessage(UserData[id组[n]].连接id, 7, "#y/你获得了一张怪物卡片")
		else
			SendMessage(UserData[id组[n]].连接id, 7, "#y/嘉年华奖励每日只可得到1次")
		end
		self:刷新追踪任务信息(id组[n])
	end

 
 end
function TaskControl:完成嘉年华任务3(id组, 任务id)
	self.临时经验4 = math.floor(任务数据[任务id].平均等级 * 任务数据[任务id].平均等级 * 30) * 240
	self.临时金钱4 = math.floor(任务数据[任务id].平均等级 * 任务数据[任务id].平均等级 * 6 * 0.6) * 18
	任务数据[任务id] = nil
	for n = 1, #id组 do
		if 活动数据.嘉年华数据[id组[n]] ~= nil and 活动数据.嘉年华数据[id组[n]][3] == false then
			活动数据.嘉年华数据[id组[n]][3] = true
			RoleControl:添加银子(UserData[id组[n]],self.临时金钱4,"嘉年华")
			RoleControl:添加经验(UserData[id组[n]],self.临时经验4, "嘉年华")
			RoleControl:添加活跃度(UserData[id组[n]],20)
			SendMessage(UserData[id组[n]].连接id, 7, "#y/恭喜你完成了嘉年华任务")
			RoleControl:取消任务(UserData[id组[n]],任务id)
			if math.random(100) <= 70 then
				self.宝石名称 = 取随机宝石()
				self.随机等级 = math.random(8, 9)
				ItemControl:GiveItem(id组[n], self.宝石名称, self.随机等级)
				广播消息(9, "#hd/".."#S/(嘉年华)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/在嘉年华活动中战胜了女妖，获得了#r/" .. self.随机等级 .. "#y/级#y/" .. self.宝石名称 .. "奖励".."#"..math.random(110))
			else
				ItemControl:GiveItem(id组[n], "摇钱树树苗")
				SendMessage(UserData[id组[n]].连接id, 9, "#dq/#w/ 你获得了摇钱树树苗")
				广播消息(9, "#hd/".."#S/(嘉年华)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/在嘉年华活动中战胜了女妖，获得了#r/摇钱树树苗#y/奖励".."#"..math.random(110))
			end
		else
			SendMessage(UserData[id组[n]].连接id, 7, "#y/嘉年华奖励每日只可得到1次")
		end
		self:刷新追踪任务信息(id组[n])
	end
	

 end
function TaskControl:完成嘉年华任务1(id, 任务id)
	if RoleControl:GetTaskID(UserData[id],"嘉年华") == 0 and 任务数据[RoleControl:GetTaskID(UserData[id],"嘉年华")].进程 ~= 2 then
		return 0
	end

	local id组 = {}
	local 任务id = RoleControl:GetTaskID(UserData[id],"嘉年华")
	任务数据[任务id].进程 = 任务数据[任务id].进程 + 1
	任务数据[任务id].NPC=117314
	self.符合等级 = 0

	for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
		self.符合等级 = self.符合等级 + UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.等级
		id组[#id组 + 1] = 队伍数据[UserData[id].队伍].队员数据[n]
	end

	self.平均等级 = math.floor(self.符合等级 / #队伍数据[UserData[id].队伍].队员数据)
	self.临时经验4 = math.floor(self.平均等级 * self.平均等级 * 35) * 3
	self.临时金钱4 = math.floor(self.平均等级 * self.平均等级 * 6 * 0.6) * 5

	for n = 1, #id组 do
		if 活动数据.嘉年华数据[id组[n]] ~= nil and 活动数据.嘉年华数据[id组[n]][4] == false then
			活动数据.嘉年华数据[id组[n]][4] = true

			RoleControl:添加银子(UserData[id组[n]],self.临时金钱4,"嘉年华")
			RoleControl:添加经验(UserData[id组[n]],self.临时经验4, "嘉年华")
			SendMessage(UserData[id组[n]].连接id, 7, "#y/你的嘉年华任务进度已经更新")
		else
			SendMessage(UserData[id组[n]].连接id, 7, "#y/嘉年华奖励每日只可得到1次")
		end
	end
	self:刷新追踪任务信息(id组[n])
 end