function TaskControl:领取门派闯关任务(id)
	if UserData[id].队伍 == 0   then
		SendMessage(UserData[id].连接id, 7, "#y/参与此活动必须队伍人数达到1人")
		return 0
	elseif #队伍数据[UserData[id].队伍].队员数据 < 1  and DebugMode == false then
		SendMessage(UserData[id].连接id, 7, "#y/参与此活动必须队伍人数达到1人")
		return 0
	elseif 取队伍符合等级(id, 60) == false and DebugMode == false then
		广播队伍消息(id, 7, "#y/参与此活动需要队伍所有成员等级达到60级")
		return 0
	else
		self.任务重复 = false
		self.重复名称 = ""
		for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
			self.闯关临时id = 队伍数据[UserData[id].队伍].队员数据[n]

			if RoleControl:GetTaskID(UserData[self.闯关临时id],"门派闯关") ~= 0 then
				广播队伍消息(id, 7, "#y/" .. UserData[self.闯关临时id].角色.名称 .. "已经领取过此任务了")
				self.任务重复 = true
			end
		end
		if self.任务重复 == false then
			self:添加门派闯关任务(id)
		end
	end
 end
function TaskControl:添加门派闯关任务(id)
 self.闯关id=tonumber(UserData[id].id.."56")

  任务数据[self.闯关id]={
 类型="门派闯关"
 ,id=id
 ,起始=os.time()
 ,结束=2400
 ,编号=0
 ,任务id=self.闯关id
 ,地图编号=0
 ,地图名称=0
 ,名称=0
 ,坐标=0
 ,数字id={}
 ,客户id={}
 ,关卡数量={}
 ,当前关卡=0
 ,战斗=false
 }
 任务数据[self.闯关id].当前关卡=math.random(#门派闯关活动id)
 self.任务id组={}
 if UserData[id].队伍==0 then

   self.任务id组[#self.任务id组+1]=id
  else
   for n=1,#队伍数据[UserData[id].队伍].队员数据 do
     self.任务id组[#self.任务id组+1]=队伍数据[UserData[id].队伍].队员数据[n]
     end
   end
  for n=1,#self.任务id组 do
   self.任务临时id1=RoleControl:生成任务id(UserData[self.任务id组[n]])
   UserData[self.任务id组[n]].角色.任务数据[self.任务临时id1]=self.闯关id
     self:刷新追踪任务信息(self.任务id组[n])
    end
 self.对话内容=" #W/请你们立即前往#Y/"..任务数据[门派闯关活动id[任务数据[self.闯关id].当前关卡]].门派.."#W/接受考验，当前正在进行第#R/"..(#任务数据[self.闯关id].关卡数量+1).."#W/次考验。"
 SendMessage(UserData[id].连接id,20,{"御林军","门派闯关使",self.对话内容})
 end
function TaskControl:结束门派闯关活动()
  if 门派闯关活动id==nil then
     return
  end
 发送游戏公告("十八门派闯关活动已经结束，处于战斗中的玩家将被强制退出战斗，所有已领取任务的玩家会被自动取消任务。")
 门派闯关开关=false
 for n, v in pairs(FightGet.战斗盒子) do
   if FightGet.战斗盒子[n].战斗类型==100012 then
      FightGet.战斗盒子[n]:强制结束战斗()
      FightGet.战斗盒子[n]=nil
     end
   end
  for n, v in pairs(UserData) do
   if UserData[n]~=nil then

     if RoleControl:GetTaskID(UserData[n],"门派闯关")~=0 then
       RoleControl:取消任务(UserData[n],RoleControl:GetTaskID(UserData[n],"门派闯关"))
       任务数据[RoleControl:GetTaskID(UserData[n],"门派闯关")]=nil
       SendMessage(UserData[n].连接id,7,"#y/你的门派闯关任务已经取消了")
       end
     end
  end
  for n=1,#门派闯关活动id do
   MapControl:移除单位(任务数据[门派闯关活动id[n]].地图编号,门派闯关活动id[n])
   任务数据[门派闯关活动id[n]]=nil
    end
 end
function TaskControl:开启门派闯关活动()
  发送游戏公告("十八门派闯关活动已经开启，大量经验物品等你来拿。各位玩家可前往长安城门派闯关活动使者处参与本活动。")
  门派闯关开关=true
  门派闯关活动id={}
  	local 首席弟子数据类={
    "大唐官府",
    "化生寺",
    "女儿村",
    "方寸山",
    "天宫",
    "龙宫",
    "五庄观",
    "普陀山",
    "魔王寨",
	   "狮驼岭",
	   "盘丝洞",
	   "阴曹地府",
	   "凌波城",
	   "无底洞",
	   "神木林",
      "花果山",
      "天机城",
      "女魃墓",
	}

   local 首席弟子={
   龙宫={造型="龙太子",武器= "飞龙在天",地图=1116,方向=1,x=73,y=76}
  ,女儿村={造型="英女侠",武器= "祖龙对剑",地图=1142,方向=0,x=71,y=76}
  ,化生寺={造型="逍遥生",武器= "秋水人家",地图=1002,方向=1,x=32,y=76}
  ,大唐官府={造型="剑侠客",武器= "晓风残月",地图=1198,方向=1,x=140,y=46}
  ,普陀山={造型="玄彩娥",武器= "青藤玉树",地图=1140,方向=0,x=70,y=50}
  ,五庄观={造型="神天兵",武器= "混元金锤",地图=1146,方向=0,x=20,y=24}
  ,魔王寨={造型="巨魔王",武器= "斩妖泣血",地图=1512,方向=0,x=41,y=48}
  ,狮驼岭={造型="虎头怪",武器= "五丁开山",地图=1131,方向=3,x=108,y=14}
  ,天宫={造型="舞天姬",武器= "晃金仙绳",地图=1111,方向=0,x=187,y=128}
  ,方寸山={造型="飞燕女",武器= "九天金线",地图=1135,方向=1,x=65,y=132}
  ,盘丝洞={造型="狐美人",武器= "血之刺藤",地图=1513,方向=0,x=147,y=117}
  ,阴曹地府={造型="骨精灵",武器= "墨玉骷髅",地图=1122,方向=0,x=58,y=76}
  ,凌波城={造型="羽灵神",武器= "庄周梦蝶",地图=1150,方向=0,x=21,y=59}
  ,神木林={造型="巫蛮儿",武器= "裂云啸日",地图=1138,方向=0,x=38,y=116}
  ,无底洞={造型="鬼潇潇",武器= "雪羽穿云",地图=1139,方向=0,x=47,y=127}
  ,花果山={造型="桃夭夭",武器= "金风玉露",地图=1251,方向=0,x=107,y=42}
  ,天机城={造型="偃无师",武器= "秋水澄流",地图=1250,方向=1,x=60,y=93}
  ,女魃墓={造型="鬼潇潇",武器= "月影星痕",地图=1249,方向=1,x=19,y=100}
  }
  for n=1,#首席弟子数据类 do
   self.门派名称= 首席弟子数据类[n]
   self.闯关id=self:生成任务id()
   门派闯关活动id[#门派闯关活动id+1]=self.闯关id
    任务数据[self.闯关id]={
 类型="门派闯关开始"
 ,id=self.闯关id
 ,起始=os.time()
 ,结束=3600
 ,任务id=self.闯关id
 ,地图编号=首席弟子[self.门派名称].地图
 ,地图名称=self.门派名称
 ,名称=self.门派名称.."护法"
 ,造型=首席弟子[self.门派名称].造型
 ,门派=self.门派名称
 ,方向=首席弟子[self.门派名称].方向
 ,坐标={x=首席弟子[self.门派名称].x,y=首席弟子[self.门派名称].y}
 ,数字id=0
 ,武器={名称=首席弟子[self.门派名称].武器,等级=120,强化=1,类别=取武器类型(首席弟子[self.门派名称].武器)}
 ,战斗=false
 ,称谓="★门派护法★"
 ,场次=1
 ,积分=0
 ,等级=0
 }
   MapControl:添加单位(任务数据[self.闯关id])
    end
 end
function TaskControl:触发门派闯关任务(id, 标识)
  if UserData[id].队伍==0 and DebugMode == false then
   SendMessage(UserData[id].连接id,7,"#y/你的队伍呢？")
   return 0
    end
 self.任务id56=MapControl.MapData[UserData[id].地图].单位组[标识].id
 self.全部通过=true
 self.任务id561=RoleControl:GetTaskID(UserData[id],"门派闯关")
 for n=1,#队伍数据[UserData[id].队伍].队员数据 do
    if RoleControl:GetTaskID(UserData[队伍数据[UserData[id].队伍].队员数据[n]],"门派闯关")~=self.任务id561 then
     广播队伍消息(id,7,"#y/"..UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称.."当前任务与队长任务不相符")
     self.全部通过=false
      end
   end
 if self.全部通过 then
   FightGet:进入处理(id,100012,"66",self.任务id56)
   end
 end
function TaskControl:完成门派闯关任务(id组, 任务id)
 for n=1,#id组 do
   if UserData[id组[n]]~=nil then
      self.符合抓鬼id=true
       self.抓鬼奖励参数=1+#任务数据[RoleControl:GetTaskID(UserData[id组[n]],"门派闯关")].关卡数量*0.05
     if self.符合抓鬼id then
    		self.抓鬼奖励等级=UserData[id组[n]].角色.等级
    		self.抓鬼奖励经验=math.floor(self.抓鬼奖励等级*self.抓鬼奖励等级*45*self.抓鬼奖励参数)
    		self.抓鬼奖励银子=math.floor((self.抓鬼奖励等级*self.抓鬼奖励等级+math.random(2000,2500))*self.抓鬼奖励参数*3)
    		RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验,"门派闯关")
        RoleControl:添加银子(UserData[id组[n]],self.抓鬼奖励银子,"门派闯关")
    		if UserData[id组[n]].召唤兽.数据.参战~=0 then
    			self.抓鬼奖励等级=UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
    			self.抓鬼奖励经验=math.floor(self.抓鬼奖励等级*45*self.抓鬼奖励等级*self.抓鬼奖励参数*0.65+150)
    			UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验,id组[n],UserData[id组[n]].召唤兽.数据.参战,"门派闯关")
    		end
        local cj = ""
        local 奖励参数 = math.random(300)
        if 奖励参数 >=1 and 奖励参数 <=20 then
          cj= "超级金柳露"
        elseif 奖励参数 >=21 and 奖励参数 <=31 then
            cj="花豆"
        elseif 奖励参数 >=32 and 奖励参数 <=43 then
          cj="星辉石"
        elseif 奖励参数 >=44 and 奖励参数 <=60 then
          cj=取随机五宝()
        elseif 奖励参数 >=61 and 奖励参数 <=73 then
          cj="金柳露"
        elseif 奖励参数 >=74 and 奖励参数 <=87 then
          cj="彩果"
        elseif 奖励参数 >=88 and 奖励参数 <=90 then
            local 随机兽决 = 取高级兽诀名称()
              ItemControl:GiveItem(id组[n],"高级魔兽要诀",nil,随机兽决)
            广播消息("#hd/".."#S/(门派闯关)".."#R/ "..UserData[id组[n]].角色.名称.."#y/在门派闯关活动中表现优异，获得了唐王赏赐的#R/"..随机兽决.."#G/高级魔兽要诀".."#"..math.random(110))
        elseif 奖励参数 >=91 and 奖励参数 <=96 then
          cj=取随机宝石()
        elseif 奖励参数 >=97 and 奖励参数 <=100 then
        ItemControl:GiveItem(id组[n],"大米")
        广播消息("#hd/".."#S/(门派闯关)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/派闯关活动中表现优异，获得了唐王赏赐的#g/大米")
        elseif 奖励参数 >=101 and 奖励参数 <=111 then
          cj=取随机强化石()
        elseif 奖励参数 >=112 and 奖励参数 <=120 then
              cj="魔兽要诀"

               elseif 奖励参数 >=121 and 奖励参数 <=123 then
               local 随机兽决 = 取门派闯关兽诀名称()
              ItemControl:GiveItem(id组[n],"高级魔兽要诀",nil,随机兽决)
            广播消息("#hd/".."#S/(门派闯关)".."#R/ "..UserData[id组[n]].角色.名称.."#s/在门派闯关活动中表现优异，获得了唐王赏赐的#R/"..随机兽决.."#G/高级魔兽要诀".."#"..math.random(110))
        end
        if cj ~= "" then
          ItemControl:GiveItem(id组[n],cj)
            --广播消息("#hd/".."#S/(门派闯关)".."#R/ "..UserData[id组[n]].角色.名称.."#y/在门派闯关活动中表现优异，获得了唐王赏赐的#G/"..cj.."#"..math.random(110))
        end
     end
   end
 end

 self.闯关任务id=RoleControl:GetTaskID(UserData[id组[1]],"门派闯关")
 任务数据[self.闯关任务id].关卡数量[#任务数据[self.闯关任务id].关卡数量+1]=任务数据[self.闯关任务id].当前关卡

 if #任务数据[self.闯关任务id].关卡数量>=18 then
    广播队伍消息(id组[1],7,"#y/你们已经完成了本轮考验，请找使者重新领取任务")
    for n=1,#id组 do
      if UserData[id组[n]]~=nil then
        --RoleControl:添加活跃度(UserData[id组[n]],50)
       RoleControl:添加银子(UserData[id组[n]],math.floor(UserData[id组[n]].角色.等级*2000),"门派闯关")
       RoleControl:取消任务(UserData[id组[n]],self.闯关任务id)
      end
    end
    任务数据[self.闯关任务id]=nil
    return 0
  else
   self.复制数组={}
   for n=1,18 do
     self.允许复制=true

     for i=1,#任务数据[self.闯关任务id].关卡数量 do

       if 任务数据[self.闯关任务id].关卡数量[i]==n then

         self.允许复制=false

         end


       end

      if self.允许复制==true then
        self.复制数组[#self.复制数组+1]={序号=n,排序=math.random(1000)}
        end
     end
    table.sort(self.复制数组,function(a,b) return a.排序>b.排序 end )
    任务数据[self.闯关任务id].当前关卡=self.复制数组[math.random(#self.复制数组)].序号
     for n=1,#id组 do
     self:刷新追踪任务信息(id组[n])
     end
    广播队伍消息(id组[1],7,"#y/任务数据已经刷新，请及时查看")

   end

 --MapControl:移除单位(UserData[id组[1]].地图,任务id)
 --任务数据[任务id]=nil

 end