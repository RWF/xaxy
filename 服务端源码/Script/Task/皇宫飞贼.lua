
function TaskControl:设置皇宫飞贼任务(id)


	UserData[id].角色.飞贼次数 = UserData[id].角色.飞贼次数 + 1
	if  UserData[id].角色.飞贼次数  > 4 then
		UserData[id].角色.飞贼次数=1
	end
	self.临时次数 = UserData[id].角色.飞贼次数
	if self.临时次数 == 2 then
		self.临时名称 = "飞贼"
		self.临时造型 = "山贼"
		self.临时编号 = 1216
	elseif self.临时次数 == 3 then
		self.临时名称 = "销赃贼"
		self.临时造型 = "进阶吸血鬼"
		self.临时编号 = 1416
	elseif self.临时次数 == 4 then
		self.临时名称 = "盗贼首领"
		self.临时造型 = "进阶雨师"
		self.临时编号 = 1421
	else
		self.临时名称 = "毛贼"
		self.临时造型 = "强盗"
		self.临时编号 = 1215
	end

	self.飞贼地图范围 = {
		1501,
		1142,
		1092,
		1070,
		1193,
		1091
	}
	self.飞贼地图 = self.飞贼地图范围[math.random( #self.飞贼地图范围)]
	self.飞贼地图坐标 =   MapControl:Randomloadtion(self.飞贼地图) 
	self.飞贼地图名称 = MapData[self.飞贼地图].名称
	self.飞贼任务id = tonumber(UserData[id].id .. "15")
	任务数据[self.飞贼任务id] = {
		类型 = "飞贼",
		结束 = 3600,
		战斗 = false,
		id = id,
		起始 = os.time(),
		编号 = self.临时编号,
		任务id = self.飞贼任务id,
		地图编号 = self.飞贼地图,
		地图名称 = self.飞贼地图名称,
		名称 = self.临时名称,
		造型 = self.临时造型,
		次数 = self.临时次数,
		方向 = math.random( 4) - 1,
		坐标 = self.飞贼地图坐标,
		数字id = {},
		客户id = {}
	}
	self.任务id组 = {}

	if UserData[id].队伍 == 0 then
		self.任务id组[#self.任务id组 + 1] = id
	else
		for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
			self.任务id组[#self.任务id组 + 1] = 队伍数据[UserData[id].队伍].队员数据[n]
		end
	end

	for n = 1, #self.任务id组 do
		self.任务临时id1 = RoleControl:生成任务id(UserData[self.任务id组[n]])
		UserData[self.任务id组[n]].角色.任务数据[self.任务临时id1] = self.飞贼任务id
		任务数据[self.飞贼任务id].数字id[#任务数据[self.飞贼任务id].数字id + 1] = UserData[self.任务id组[n]].id
		任务数据[self.飞贼任务id].客户id[#任务数据[self.飞贼任务id].客户id + 1] = self.任务id组[n]
		self:刷新追踪任务信息(self.任务id组[n])
	end
	MapControl:添加单位(任务数据[self.飞贼任务id])
	self.对话内容 = " #W/偷拿皇宫的#Y/" .. 任务数据[self.飞贼任务id].名称 .. "#W/已经逃往#Y/" .. 任务数据[self.飞贼任务id].地图名称 .. "（" .. 任务数据[self.飞贼任务id].坐标.x .. "," .. 任务数据[self.飞贼任务id].坐标.y .. ")#W/处，请速往缉拿。"
	SendMessage(UserData[id].连接id, 20, {"御林军","御林军左统领",self.对话内容})
 end
function TaskControl:完成皇宫飞贼任务(id组, 任务id)
	for n = 1, #id组 do
		if UserData[id组[n]] ~= nil then
			self.符合抓鬼id = false

			for i = 1, #任务数据[任务id].数字id do
				if id组[n] == 任务数据[任务id].数字id[i] then
					self.符合抓鬼id = true
				end
			end
			if self.符合抓鬼id then
				RoleControl:取消任务(UserData[id组[n]],任务id)
				self.抓鬼奖励参数 = 任务数据[任务id].次数
				self.抓鬼奖励等级 = UserData[id组[n]].角色.等级
				self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * self.抓鬼奖励等级 * 60 * self.抓鬼奖励参数)
				RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验,"皇宫飞贼")
				if UserData[id组[n]].召唤兽.数据.参战 ~= 0 then
					self.抓鬼奖励等级 = UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
					self.抓鬼奖励经验 = math.floor(self.抓鬼奖励等级 * self.抓鬼奖励等级 * 60 * self.抓鬼奖励参数 * 0.35 + 150)
					UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验, id组[n], UserData[id组[n]].召唤兽.数据.参战, 2)
				end

				if 任务数据[任务id].次数 == 4 then

						UserData[id组[n]].角色.飞贼次数 = 0
						local cj = ""
						local 奖励参数 = math.random(140)
						if 奖励参数 >=1 and 奖励参数 <=20 then
						cj= "金柳露"
						elseif 奖励参数 >=21 and 奖励参数 <=31 then
						cj="超级金柳露"
						elseif 奖励参数 >=32 and 奖励参数 <=43 then
						cj="魔兽要诀"
						elseif 奖励参数 >=44 and 奖励参数 <=60 then
						cj="海马"
						elseif 奖励参数 >=61 and 奖励参数 <=73 then
						cj=取召唤兽随机宝石()
						elseif 奖励参数 >=74 and 奖励参数 <=83 then
						cj="碎石锤"
						elseif 奖励参数 >=84 and 奖励参数 <=90 then
						 ItemControl:GiveItem(id组[n],"珍珠",math.random(9,16)*10)
                    	 广播消息("#hd/".."#S/(皇宫飞贼)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了皇宫飞贼，获得了玉皇大帝奖励的#g/珍珠".."#"..math.random(110))
						elseif 奖励参数 >=91 and 奖励参数 <=96 then
						cj=取随机宝石()
						elseif 奖励参数 >=97 and 奖励参数 <=100 then
						cj="修炼果"
						end
						if cj ~= "" then
						ItemControl:GiveItem(id组[n],cj)
						广播消息(9, "#hd/".."#S/(皇宫飞贼)".."#r/ " .. UserData[id组[n]].角色.名称 .. "#y/成功抓捕了盗贼首领，获得了御林军左统领奖励的#g/"..cj.."#"..math.random(110))
						end
						 RoleControl:添加活跃度(UserData[id组[n]],5)

				else
					UserData[id组[n]].角色.飞贼次数 = 任务数据[任务id].次数
				end
			end
		end
	end

	MapControl:移除单位(任务数据[任务id].地图编号, 任务id)
	任务数据[任务id] = nil
	self:刷新追踪任务信息(id)
 end