function TaskControl:设置官职任务(id)
	if UserData[id].角色.等级 < 50 then
		SendMessage(UserData[id].连接id, 7, "#y/只有等级达到50级的玩家才可帮助朝廷")

		return 0
	elseif RoleControl:GetTaskID(UserData[id],"官职") ~= 0 then
		SendMessage(UserData[id].连接id, 7, "#y/你已经领取过此任务了")

		return 0
	elseif UserData[id].队伍 ~= 0 then
		SendMessage(UserData[id].连接id, 7, "#y/该任务不允许组队领取")

		return 0

	else

		-- if RoleControl:GetTaskID(UserData[id],"法宝") == 0 and math.random(100)<=5 and UserData[id].角色.等级>=60 then
		-- 	UserData[id].法宝 =true
		-- 	SendMessage(UserData[id].连接id,20,{"御林军","李将军","恭喜你激活了一个法宝任务,是否领取该任务呢?",{"领取法宝任务","我不需要法宝"}})

		-- else
			self.任务参数6 = math.random( 100)

			if self.任务参数6 <= 55 then
				self:设置官职飞贼任务(id)
			elseif self.任务参数6 <= 75 then
				self:设置官职押送任务(id)
			else
				self:设置官职物品任务(id)
			end
		--end
	end
 end
function TaskControl:完成官职飞贼任务(id, 任务id)
	if 任务数据[任务id] == nil then
		return 0
	end

	if 任务数据[任务id].次数 == 2 then
		任务数据[任务id].次数 = 1
		任务数据[任务id].战斗 = false

		MapControl:移除单位(任务数据[任务id].地图编号, 任务id)

		任务数据[任务id].坐标 =  MapControl:Randomloadtion(任务数据[任务id].地图编号) 

		MapControl:添加单位(任务数据[任务id])
		SendMessage(UserData[id].连接id, 7, "#y/飞贼已经逃往了另一个地点，请速去追捕")
	else
		MapControl:移除单位(UserData[id].地图, 任务id)
		RoleControl:取消任务(UserData[id],任务id)

		任务数据[任务id] = nil
		UserData[id].角色.官职贡献度 = UserData[id].角色.官职贡献度 + 1
		self.临时等级 = UserData[id].角色.等级

		RoleControl:添加经验(UserData[id],math.floor(self.临时等级 * self.临时等级 *20 * math.random(90, 110) / 100),"官职")
		RoleControl:添加储备(UserData[id],100000, "官职")
		RoleControl:添加银子(UserData[id],30000, "官职")
		RoleControl:添加活跃度(UserData[id],1)
		if UserData[id].角色.官职任务次数 >= 6 then
			UserData[id].角色.官职贡献度 = UserData[id].角色.官职贡献度 + 1

			SendMessage(UserData[id].连接id, 7, "#y/你获得了2点官职贡献度")

			if UserData[id].角色.官职任务次数 >= 10 then
				UserData[id].角色.官职任务次数 = 0
				if math.random(100) <= 60 then
					ItemControl:GiveItem(id, "藏宝图")
				else
					self.临时宝石 =取随机宝石()

					ItemControl:GiveItem(id, self.临时宝石, 1)
					广播消息("#hd/".."#S/(官职任务)".."#g/ " .. UserData[id].角色.名称 .. "#y/为朝廷尽心尽力、忠君爱国，为大唐繁荣盛世贡献出了自己的力量。获得了李将军赠与的#r/" .. self.临时宝石 .. "#y/以兹鼓励。".."#"..math.random(110))
				end
			end
		else
			SendMessage(UserData[id].连接id, 7, "#y/你获得了1点官职贡献度")
		end

		UserData[id].角色.官职任务次数 = UserData[id].角色.官职任务次数 + 1
	end
 end
function TaskControl:完成官职物品任务(id, 任务id)
	if RoleControl:GetTaskID(UserData[id],"官职") == 0 then
		return 0
	end

	任务id = 任务id + 0
	任务数据[RoleControl:GetTaskID(UserData[id],"官职")] = nil

	RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"官职"))

	UserData[id].角色.官职贡献度 = UserData[id].角色.官职贡献度 + 2
	self.临时等级 = UserData[id].角色.等级

	RoleControl:添加经验(UserData[id],math.floor(self.临时等级 * self.临时等级 *16 * math.random(90, 110) / 100), "官职")
    RoleControl:添加银子(UserData[id],100000, "官职任务")
	if UserData[id].角色.官职任务次数 >= 6 then
		UserData[id].角色.官职贡献度 = UserData[id].角色.官职贡献度 + 1

		SendMessage(UserData[id].连接id, 7, "#y/你获得了3点官职贡献度")

		if UserData[id].角色.官职任务次数 >= 10 then
			UserData[id].角色.官职任务次数 = 0

			if math.random(100) <= 60 then
				ItemControl:GiveItem(id, "藏宝图")

			else
				self.临时宝石 = 取随机宝石()
				ItemControl:GiveItem(id, self.临时宝石, 1)
				广播消息("#hd/".."#S/(官职任务)".."#g/ " .. UserData[id].角色.名称 .. "#y/为朝廷尽心尽力、忠君爱国，为大唐繁荣盛世贡献出了自己的力量。获得了李将军赠与的#r/" .. self.临时宝石 .. "#y/以兹鼓励。".."#"..math.random(110))
			end
		end
	else
		SendMessage(UserData[id].连接id, 7, "#y/你获得了2点官职贡献度")
	end

	UserData[id].角色.官职任务次数 = UserData[id].角色.官职任务次数 + 1
 end
function TaskControl:设置官职物品任务(id)
	local 临时id61 = tonumber(UserData[id].id .. "6")
	 local b = 0
    local 武器 = "包子"
	if math.random(100) <= 50 then
		 if math.random(10) <= 2 then
				local 三药 = {"金创药","金香玉","小还丹","千年保心丹","风水混元丹","定神香","蛇蝎美人","九转回魂丹","佛光舍利子","五龙丹"}
				 b= 3
				武器 = 三药[math.random(1,#三药)]
		else
			     b = 2
				local 二药 = {"天不老","紫石英","鹿茸","血色茶花","六道轮回","熊胆","凤凰尾","硫磺草","龙之心屑","火凤之睛","丁香水","月星子","仙狐涎","地狱灵芝","麝香","血珊瑚","餐风饮露","白露为霜","天龙水","孔雀红"}
				武器 = 二药[math.random(1,#二药)]
		end
	else
          b = math.random(5,8)*10
 		  武器 = 取随机物品(b)
	end

	任务数据[临时id61] = {
		地图编号 = 1173,
		类型 = "官职",
		编号 = 1218,
		分类 = 3,
		结束 = 3600,
		名称 = "军需官",
		地图名称 = "大唐境外",
		坐标 = 0,
		战斗 = false,
		id = id,
		起始 = os.time(),
		任务id = 临时id61,
		道具 = 武器,
		等级 = b,
		方向 = math.random( 4) - 1,
		数字id = UserData[id].id
	}
	self.对话内容 = " 近日宫中物资奇缺，请你帮忙找一个#G/" .. 任务数据[临时id61].道具 .. [[#W/给我，事后必有重谢。]]
	self.任务id = RoleControl:生成任务id(UserData[id])
	UserData[id].角色.任务数据[self.任务id] = 临时id61
    self:刷新追踪任务信息(id)
	SendMessage(UserData[id].连接id, 20, {"御林军","李将军",self.对话内容})
 end
function TaskControl:完成官职押送任务(id, 标识)
	local 任务id = MapControl.MapData[UserData[id].地图].单位组[标识].任务id

	if 任务数据[任务id] == nil then
		return 0
	end
	MapControl:移除单位(任务数据[任务id].地图编号, 任务id)
	RoleControl:取消任务(UserData[id],任务id)
	任务数据[任务id] = nil
	UserData[id].角色.官职贡献度 = UserData[id].角色.官职贡献度 + 1
	self.临时等级 = UserData[id].角色.等级
	RoleControl:添加经验(UserData[id],math.floor(self.临时等级 * self.临时等级 * 18 * math.random(60, 80) / 100), "官职")
	if UserData[id].角色.官职任务次数 >= 6 then
		UserData[id].角色.官职贡献度 = UserData[id].角色.官职贡献度 + 1
		SendMessage(UserData[id].连接id, 7, "#y/你获得了2点官职贡献度")
		if UserData[id].角色.官职任务次数 >= 10 then
			UserData[id].角色.官职任务次数 = 0
			if math.random(100) <= 60 then
				ItemControl:GiveItem(id, "藏宝图")
			else
				self.临时宝石 = 取随机宝石()

				ItemControl:GiveItem(id, self.临时宝石, 1)
				广播消息("#hd/".."#S/(官职任务)".."#g/ " .. UserData[id].角色.名称 .. "#y/为朝廷尽心尽力、忠君爱国，为大唐繁荣盛世贡献出了自己的力量。获得了李将军赠与的#r/" .. self.临时宝石 .. "#y/以兹鼓励。".."#"..math.random(110))
			end
		end
	else
		SendMessage(UserData[id].连接id, 7, "#y/你获得了1点官职贡献度")
	end

	UserData[id].角色.官职任务次数 = UserData[id].角色.官职任务次数 + 1
 end
function TaskControl:设置官职飞贼任务(id)
	self.飞贼地图 = {
		1501,
		1001,
		1070,
		1092
	}
	local 临时id6 = tonumber(UserData[id].id .. "6")
	local 地图编号6 = self.飞贼地图[math.random( #self.飞贼地图)]
	local 临时坐标6 =   MapControl:Randomloadtion(地图编号6) 
	local 地图名称6 = MapData[地图编号6].名称
	任务数据[临时id6] = {
		类型 = "官职",
		编号 = 1216,
		分类 = 1,
		次数 = 2,
		结束 = 3599,
		名称 = "飞贼",
		造型 = "山贼",
		战斗 = false,
		id = id,
		起始 = os.time(),
		任务id = 临时id6,
		地图编号 = 地图编号6,
		地图名称 = 地图名称6,
		方向 = math.random( 4) - 1,
		坐标 = 临时坐标6,
		数字id = UserData[id].id
	}

	MapControl:添加单位(任务数据[临时id6])
	UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])] = 临时id6
	SendMessage(UserData[id].连接id, 7, "#y/你领取了官职任务，请点击任务列表查看")
	  self:刷新追踪任务信息(id)
 end
function TaskControl:设置官职押送任务(id)
	local 临时id61 = tonumber(UserData[id].id .. "6")
	local 临时坐标61 =  MapControl:Randomloadtion(1173) 
	任务数据[临时id61] = {
		地图编号 = 1173,
		类型 = "官职",
		编号 = 1218,
		分类 = 2,
		名称 = "军需官",
		结束 = 3600,
		地图名称 = "大唐境外",
		造型 = "护卫",
		战斗 = false,
		id = id,
		起始 = os.time(),
		任务id = 临时id61,
		方向 = math.random( 4) - 1,
		坐标 = 临时坐标61,
		数字id = UserData[id].id
	}

	MapControl:添加单位(任务数据[临时id61])
	UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])] = 临时id61
	SendMessage(UserData[id].连接id, 7, "#y/你领取了官职任务，请点击任务列表查看")
	  self:刷新追踪任务信息(id)
 end