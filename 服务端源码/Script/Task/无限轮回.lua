
function TaskControl:完成无限轮回任务(id)
	self.任务积分 = 活动数据.无限轮回[id].积分
	if UserData[id].队伍==0 then
		SendMessage(UserData[id].连接id, 7, "#y/挑战失败,你离开了队伍将不再活动奖励!")
	  return 0
	end
	for n = 1, #队伍数据[UserData[id].队伍].队员数据, 1 do
		self.临时id = 队伍数据[UserData[id].队伍].队员数据[n]
		self.临时等级 = UserData[self.临时id].角色.等级
		self.基础经验 = self.临时等级 * 30
		RoleControl:添加经验(UserData[self.临时id],self.基础经验 * self.任务积分, "无限轮回")
		self.基础储备 = self.临时等级 * 15
		self.玩家名称 = UserData[self.临时id].角色.名称
		RoleControl:添加储备(UserData[self.临时id],self.基础储备 * self.任务积分, "无限轮回")

        RoleControl:添加活跃度(UserData[self.临时id],50)
		if  self.任务积分 >= 3000 then
			local sj = math.random(3)
			if sj == 1 then
					ItemControl:GiveItem(self.临时id,"玉葫灵髓")
                 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/玉葫灵髓".."#"..math.random(110))
            elseif  sj == 2 then
                ItemControl:GiveItem(self.临时id,"易经丹")
                 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/易经丹".."#"..math.random(110))
            elseif  sj == 3 then
				    local xtcj = {"玄天残卷·上卷","玄天残卷·中卷"}
                   local cj = xtcj[math.random(#xtcj)]
                  ItemControl:GiveItem(self.临时id,cj)
                 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/"..cj.."#"..math.random(110))
            end
		elseif  self.任务积分 >= 2800 then
			local sj = math.random(3)
			if sj == 1 then
                local 随机名称 = 取随机书铁()
                    local 随机等级 = math.random(14, 15)*10
                    ItemControl:GiveItem(self.临时id, 随机名称, 随机等级)
                    广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#小东/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))

            elseif  sj == 2 then
	            	 ItemControl:GiveItem(self.临时id,"二级未激活符石")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/三级未激活符石".."#"..math.random(110))
            elseif  sj == 3 then
                -- ItemControl:GiveItem(self.临时id,"神兜兜")
                --  广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/神兜兜".."#"..math.random(110))
				ItemControl:GiveItem(self.临时id,"战魄",160)
                 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/战魄".."#"..math.random(110))
            end
		elseif  self.任务积分 >= 2500 then
			local sj = math.random(3)
			if sj == 1 then
            	 ItemControl:GiveItem(self.临时id,"神兜兜")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/神兜兜".."#"..math.random(110))
            elseif  sj == 2 then
	            	 ItemControl:GiveItem(self.临时id,"二级未激活符石")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/二级未激活符石".."#"..math.random(110))
            elseif  sj == 3 then
                ItemControl:GiveItem(self.临时id,"钨金",160)
                 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/160#Y/级#r/钨金".."#"..math.random(110))
            end
		elseif  self.任务积分 >= 2000 then
			local sj = math.random(3)
			if sj == 1 then
            	 ItemControl:GiveItem(self.临时id,"陨铁")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/陨铁".."#"..math.random(110))
            elseif  sj == 2 then
	            	 ItemControl:GiveItem(self.临时id,"超级碎石锤")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/超级碎石锤".."#"..math.random(110))
            elseif  sj == 3 then
	            	 ItemControl:GiveItem(self.临时id,"清灵净瓶")
                 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/清灵净瓶".."#"..math.random(110))
            end
		elseif  self.任务积分 >= 800 then
			local sj = math.random(3)
			if sj == 1 then
            	 ItemControl:GiveItem(self.临时id,"摇钱树树苗")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/摇钱树树苗".."#"..math.random(110))
            elseif  sj == 2 then
	            	 ItemControl:GiveItem(self.临时id,"双倍经验丹")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/双倍经验丹".."#"..math.random(110))
            elseif  sj == 3 then
                 ItemControl:GiveItem(self.临时id,"摄灵珠")
                 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/摄灵珠".."#"..math.random(110))

            end
		elseif  self.任务积分 >= 700 then
			local sj = math.random(3)
			if sj == 1 then
                ItemControl:GiveItem(self.临时id,"附魔宝珠")
                 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/附魔宝珠".."#"..math.random(110))
            elseif  sj == 2 then
	            	 ItemControl:GiveItem(self.临时id,"碎石锤")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/碎石锤".."#"..math.random(110))
            elseif  sj == 3 then
	            	 ItemControl:GiveItem(self.临时id,"修炼果")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/修炼果".."#"..math.random(110))
            end

		elseif  self.任务积分 >= 600 then
			local sj = math.random(3)
			if sj == 1 then
            	 ItemControl:GiveItem(self.临时id,"法宝任务书")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/法宝任务书".."#"..math.random(110))
            elseif  sj == 2 then
                ItemControl:GiveItem(self.临时id,"星辉石",math.random(2,4))
                广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/星辉石".."#"..math.random(110))
            elseif  sj == 3 then
		            	 ItemControl:GiveItem(self.临时id,"超级净瓶玉露")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/超级净瓶玉露".."#"..math.random(110))
            end
		elseif  self.任务积分 >= 500 then
			local sj = math.random(3)
			if sj == 1 then
				 ItemControl:GiveItem(self.临时id,"海马")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/海马".."#"..math.random(110))

            elseif  sj == 2 then
				ItemControl:GiveItem(self.临时id,"星辉石",1)
			    广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/1级#r/星辉石".."#"..math.random(110))
            elseif  sj == 3 then
	 			ItemControl:GiveItem(self.临时id, "高级魔兽要诀", 取高级兽诀名称())
				SendMessage(UserData[self.临时id].连接id, 7, "#y/你获得了高级魔兽要诀")
				广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/高级魔兽要诀".."#"..math.random(110))
            end
		elseif  self.任务积分 >= 400 then
			local sj = math.random(3)
			if sj == 1 then
				 ItemControl:GiveItem(self.临时id,"九转金丹")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/九转金丹".."#"..math.random(110))
            elseif  sj == 2 then
            	 ItemControl:GiveItem(self.临时id,"符石卷轴")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/符石卷轴".."#"..math.random(110))
            elseif  sj == 3 then
            	 ItemControl:GiveItem(self.临时id,"一级未激活符石")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/一级未激活符石".."#"..math.random(110))
            end
		elseif  self.任务积分 >= 300 then
			local sj = math.random(3)
			if sj == 1 then
				local 宝石名称=取随机宝石()
				local 随机等级 = math.random(2,3)
				 ItemControl:GiveItem(self.临时id,宝石名称,1)
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/.."..随机等级.."#Y/级#R/"..宝石名称.."#"..math.random(110))
            elseif  sj == 2 then
                     local 随机名称 = 取随机书铁()
                    local 随机等级 = math.random(12, 14)*10
                    ItemControl:GiveItem(self.临时id, 随机名称, 随机等级)
                    广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
            elseif  sj == 3 then
				local 随机名称 = ItemControl:给予随机五宝(self.临时id)
				广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/" .. 随机名称.."#"..math.random(110))
            end
		elseif  self.任务积分 >= 200 then
			local sj = math.random(3)
			if sj == 1 then
				local 宝石名称= 取召唤兽随机宝石()
				 ItemControl:GiveItem(self.临时id,宝石名称,1)
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/1#Y/级#R/"..宝石名称.."#"..math.random(110))
            elseif  sj == 2 then
            	 ItemControl:GiveItem(self.临时id,"净瓶玉露")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/净瓶玉露".."#"..math.random(110))
            elseif  sj == 3 then
            	local 随机等级 =  math.random( 8)*10+5
             	 ItemControl:GiveItem(self.临时id,"炼妖石",随机等级)
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/炼妖石".."#"..math.random(110))
            end
		elseif  self.任务积分 >= 120 then
			local sj = math.random(3)
			if sj == 1 then
			 	local 随机名称 = 取随机书铁()
                    local 随机等级 = math.random(12, 13)*10
                    ItemControl:GiveItem(self.临时id, 随机名称, 随机等级)
                    广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
            elseif  sj == 2 then
            	 ItemControl:GiveItem(self.临时id,"月华露",(math.random(5)*10))
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/月华露".."#"..math.random(110))
            elseif  sj == 3 then
  	         	 ItemControl:GiveItem(self.临时id,"超级金柳露")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/超级金柳露".."#"..math.random(110))

            end
		elseif  self.任务积分 >= 40 then
			local sj = math.random(3)
			if sj == 1 then
				local 随机名称 = 取随机书铁()
				local 随机等级 = math.random(6, 8)*10
				ItemControl:GiveItem(self.临时id, 随机名称, 随机等级)
				广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/" .. 随机等级 .. "#Y/级#r/" .. 随机名称.."#"..math.random(110))
            elseif  sj == 2 then
            	 ItemControl:GiveItem(self.临时id,"彩果")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/彩果".."#"..math.random(110))
            elseif  sj == 3 then
            	  local 强化石 = 取随机强化石()
                   ItemControl:GiveItem(self.临时id,强化石)
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/"..强化石.."#"..math.random(110))
            end
		elseif  self.任务积分 >= 10 then
			local sj = math.random(3)
			if sj == 1 then
				local 宝石名称=取随机宝石()
				 ItemControl:GiveItem(self.临时id,宝石名称,1)
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/1#Y/级#R/"..宝石名称.."#"..math.random(110))
			elseif sj ==2 then
	         	 ItemControl:GiveItem(self.临时id,"藏宝图")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/藏宝图".."#"..math.random(110))
		    elseif sj ==3 then
	         	 ItemControl:GiveItem(self.临时id,"金柳露")
	         	 广播消息("#hd/".."#S/(无限轮回)".."#g/" .. self.玩家名称 .. "#y/完成无限轮回任务获得了#r/金柳露".."#"..math.random(110))
		    end
		end
	end
 end
function TaskControl:挑战无限轮回(id)
	if UserData[id].队伍 == 0 then
		SendMessage(UserData[id].连接id, 7, "#y/此活动必须组队完成")
		return 0
	elseif #队伍数据[UserData[id].队伍].队员数据 < 3  and DebugMode== false then
		SendMessage(UserData[id].连接id, 7, "#y/队伍中的成员数不能少于3人")
		return 0
	elseif 取队伍符合等级(id, 69) == false and DebugMode== false then
		广播队伍消息(id, 7, "#y/参加无限轮回活动需要队伍所有成员等级达到69级")
		return 0
	end
	self.重复名称 = ""
	for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
		if 活动数据.无限轮回[UserData[队伍数据[UserData[id].队伍].队员数据[n]].id] ~= nil then
			self.重复名称 = UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称
		end
	end
	if self.重复名称 ~= "" then
		广播队伍消息(id, 7, "#g/" .. self.重复名称 .. "#y/今日已经挑战过无限轮回了")
		return 0
	end
	for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
	 活动数据.无限轮回[UserData[队伍数据[UserData[id].队伍].队员数据[n]].id] ={积分=0,奖励=false,场次=1}
	end
    FightGet:进入处理(id, 100036, "66", 0)
 end