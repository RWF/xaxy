--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-06-10 16:28:23
--======================================================================--
function TaskControl:完成任务链(id, 任务id, 跳过)
	if 任务id == nil or 任务数据[任务id] == nil or 任务数据[任务id].奖励重置 == false then
		SendMessage(UserData[id].连接id, 7, "#y/任务链数据异常")
		return 0
	end
	任务数据[任务id].奖励重置 = false
	self.任务链经验 = math.floor(取角色等级(id) * 取角色等级(id) * 60 * (1 + 任务数据[任务id].次数 * 0.01))
	if 跳过 == nil then
		RoleControl:添加经验(UserData[id],self.任务链经验,"任务链")
		RoleControl:添加活跃度(UserData[id],1)
		RoleControl:添加单人积分(UserData[id],1)
	end
	-- local 随机等级 = math.floor(取角色等级(id) / 10)
	-- 随机等级 = math.random(随机等级-1, 随机等级 + 1)*10
	-- if 随机等级 > 150 then
	-- 	随机等级 = 150
	-- end
	随机等级= math.random(12,15)*10
	self.玩家名称 =UserData[id].角色.名称
	local 随机名称 = 取随机书铁()
	if 任务数据[任务id].次数 == 300 then
		  -- if RoleControl:取可用道具格子(user,"包裹") ==8 then
    --     SendMessage(user.连接id, 7, "#y/请在包裹栏留出8个空余的位置!")
    --     return true
    
		RoleControl:取消任务(UserData[id],任务id)
		任务数据[任务id] = nil
		SendMessage(UserData[id].连接id, 7, "#y/你完成了任务链")
		ItemControl:GiveItem(id, 随机名称, 随机等级)
		ItemControl:GiveItem(id,"修炼果",nil,nil,110)
		ItemControl:GiveItem(id,"神之符",nil,nil,3)
		ItemControl:GiveItem(id,"战魄",160)
		ItemControl:GiveItem(id,"战魄",160)
		ItemControl:GiveItem(id,"战魄",160)
		ItemControl:GiveItem(id,"陨铁",nil,nil,3)
		if math.random(1,100) ==66 then
			 local 临时兽决= 取特殊兽诀名称()
        ItemControl:GiveItem(user.id, "特殊魔兽要诀",nil, 临时兽决)
       -- SendMessage(user.连接id,20054,AddItem("高级魔兽要诀", nil, 临时兽决))
		广播消息("#hd/".."#S/(任务链)".."#g/" .. self.玩家名称 .. "#y/完成300次任务链任务获得了#G/" .. 随机等级 .. "#y/级#r/".. 随机名称.."#"..math.random(110))
		end
		
	elseif 任务数据[任务id].次数 == 200 then
		ItemControl:GiveItem(id, 随机名称, 随机等级)
		广播消息("#hd/".."#S/(任务链)".."#g/" .. self.玩家名称 .. "#y/完成200次任务链任务获得了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
		self:设置任务链(id, 任务id)
		ItemControl:GiveItem(id,"修炼果",nil,nil,30)
	elseif 任务数据[任务id].次数 == 100 then
		ItemControl:GiveItem(id, 随机名称, 随机等级)
		广播消息("#hd/".."#S/(任务链)".."#g/" .. self.玩家名称 .. "#y/完成100次任务链任务获得了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))

		self:设置任务链(id, 任务id)
		ItemControl:GiveItem(id,"修炼果",nil,nil,10)
	else
		self:设置任务链(id, 任务id)
	end
end
function TaskControl:设置任务链(id, 任务id)
	self.任务链参数 = math.random(4)
	任务数据[任务id].次数 = 任务数据[任务id].次数 + 1
	任务数据[任务id].id = 任务数据[任务id].id
	任务数据[任务id].奖励重置 = true
	任务数据[任务id].起始 = os.time()
	任务数据[任务id].结束 = 3600
	任务数据[任务id].地图名称,任务数据[任务id].NPC= 取任务人物和地图()


	local  任务链地图=MapData[任务数据[任务id].地图名称].名称
	local  任务链人物 =NpcData[任务数据[任务id].NPC].名称
	if self.任务链参数 == 1 then
		任务数据[任务id].分类 = 1
		self.对话内容 = " #W/前些日子#Y/" .. 任务链地图 .. "#W/的#Y/" .. 任务链人物 .. "#W/帮了我一个大忙，还请你帮我前去表示感谢。"
	elseif self.任务链参数 == 2 then
		任务数据[任务id].分类 = 2
		local cs = math.random(5)
	    if cs == 1 then
				local 三药 = {"金创药","金香玉","小还丹","千年保心丹","风水混元丹","定神香","蛇蝎美人","九转回魂丹","佛光舍利子","五龙丹"}
				 任务数据[任务id].道具  = 三药[math.random(1,#三药)]
		elseif cs == 2 then
				local 二药 = {"天不老","紫石英","鹿茸","血色茶花","六道轮回","熊胆","凤凰尾","硫磺草","龙之心屑","火凤之睛","丁香水","月星子","仙狐涎","地狱灵芝","麝香","血珊瑚","餐风饮露","白露为霜","天龙水","孔雀红"}
				 任务数据[任务id].道具 = 二药[math.random(1,#二药)]
		elseif cs == 3 then
			     任务数据[任务id].等级=math.random(5,8)*10
			     任务数据[任务id].道具 = 取随机物品(任务数据[任务id].等级)
	    elseif cs == 4 then
	    	local 随机环={"百合","桃花","兰花","红玫瑰","康乃馨","牡丹","木鱼","编钟","唢呐","笛子","萧","钹","竖琴","琵琶"}
	    	任务数据[任务id].道具= 随机环[math.random(1,#随机环)]
	    elseif cs == 5 then
	    	local 随机道具={"魔兽要诀","高级魔兽要诀","七彩石","易经丹","召唤兽内丹","高级召唤兽内丹","鬼谷子","神兜兜","附魔宝珠","不磨符"}
	    	任务数据[任务id].道具= 随机道具[math.random(1,#随机道具)]
		end
		self.对话内容 = " #W/听说#Y/" .. 任务链地图 .. "#W/的#Y/" .. 任务链人物 .. "#W/正在到处寻找#G/" .. 任务数据[任务id].道具 .. "#W/，请你前去帮他寻找个吧。"
	elseif self.任务链参数 == 3 then
		任务数据[任务id].分类 = 3
				local 召唤兽随机={
				{"大海龟","巨蛙","海毛虫","树怪","野猪","大蝙蝠","赌徒","山贼","强盗","狐狸精","黑熊","花妖","老虎","牛妖","小龙女","野鬼","狼","虾兵","蟹将",
                "龟丞相","牛头","马面","黑熊精","僵尸","雷鸟人","蝴蝶仙子","古代瑞兽","白熊","黑山老妖","天兵"},
                {"大海龟","巨蛙","海毛虫","树怪","野猪","大蝙蝠","赌徒","山贼","强盗","狐狸精","黑熊","花妖","老虎"},
                {"龟丞相","牛头","马面","黑熊精","僵尸","雷鸟人","蝴蝶仙子","古代瑞兽","白熊","黑山老妖","天兵"}
          		  }
				local sj =0
				if UserData[id].角色.等级 < 65  then
                      sj = 1
                elseif UserData[id].角色.等级 < 105 then
			    	  sj =2
			    else
			    	  sj =3
				end
		任务数据[任务id].召唤兽 =召唤兽随机[sj][math.random( #召唤兽随机[sj])]
		self.对话内容 = " #W/听说#Y/" .. 任务链地图 .. "#W/的#Y/" .. 任务链人物 .. "#W/最近迷恋上了炼妖，正在到处寻找#G/" .. 任务数据[任务id].召唤兽 .. "#W/，请你前去帮他寻找个吧。"
	elseif self.任务链参数 == 4 then
		任务数据[任务id].分类 = 4
		self.对话内容 = " #W/听闻#Y/" .. 任务链地图 .. "#W/的#Y/" .. 任务链人物 .. "#W/正在被心魔所困，还请前往协助铲除心魔。"
	end
	SendMessage(UserData[id].连接id,20,{"老书生","陆萧然",self.对话内容})
	self:刷新追踪任务信息(id)
end
function TaskControl:完成宝宝任务链(id, 任务id, 跳过)
	if 任务id == nil or 任务数据[任务id] == nil or 任务数据[任务id].奖励重置 == false then
		SendMessage(UserData[id].连接id, 7, "#y/宝宝任务链数据异常")
		return 0
	end
	任务数据[任务id].奖励重置 = false
	self.宝宝任务链经验 = math.floor(取角色等级(id) * 取角色等级(id) * 100 * (1 + 任务数据[任务id].次数 * 0.01))
	
		-- RoleControl:添加经验(UserData[id],self.宝宝任务链经验,"宝宝任务链")
		-- RoleControl:添加活跃度(UserData[id],1)
		 -- UserData[id].角色.神器积分=UserData[id].角色.神器积分+1
   --      SendMessage(UserData[id].连接id,7,"#y/你获得了1点神器积分！")

     -- 60 
      RoleControl:添加召唤兽修炼点数(UserData[id],60)
	
	local 随机等级 = math.floor(取角色等级(id) / 10)
	随机等级 = math.random(随机等级-1, 随机等级 + 1)*10
	if 随机等级 > 150 then
		随机等级 = 150
	end
	self.玩家名称 =UserData[id].角色.名称
	local 随机名称 = 取随机书铁()
	if 任务数据[任务id].次数 == 100 then
		RoleControl:取消任务(UserData[id],任务id)
		任务数据[任务id] = nil
		SendMessage(UserData[id].连接id, 7, "#y/你完成了宝宝任务链")
		ItemControl:GiveItem(id, 随机名称, 随机等级)
		RoleControl:添加银子(UserData[id],math.random(50000, 30000000),"宝宝任务链")
		ItemControl:GiveItem(id, "高级魔兽要诀", 取高级兽诀名称())
		SendMessage(UserData[id].连接id, 7, "#y/你获得了高级魔兽要诀")
		广播消息("#hd/".."#S/(宝宝任务链)".."#g/" .. self.玩家名称 .. "#y/完成100次宝宝任务链任务获得了#r/高级魔兽要诀".."#"..math.random(110))
            
		--ItemControl:GiveItem(id,"战魄",160)
		--ItemControl:GiveItem(id,"陨铁")
		广播消息("#hd/".."#S/(宝宝任务链)".."#g/" .. self.玩家名称 .. "#y/完成100次宝宝任务链任务获得了#G/" .. 随机等级 .. "#y/级#r/".. 随机名称.."#"..math.random(110))

	else
		self:设置宝宝任务链(id, 任务id)
	end
end
function TaskControl:设置宝宝任务链(id, 任务id)
	self.宝宝任务链参数 = math.random(3)
	任务数据[任务id].次数 = 任务数据[任务id].次数 + 1
	任务数据[任务id].id = 任务数据[任务id].id
	任务数据[任务id].奖励重置 = true
	任务数据[任务id].起始 = os.time()
	任务数据[任务id].结束 = 3600
	任务数据[任务id].地图名称,任务数据[任务id].NPC= 取任务人物和地图()


	local  宝宝任务链地图=MapData[任务数据[任务id].地图名称].名称
	local  宝宝任务链人物 =NpcData[任务数据[任务id].NPC].名称
	if self.宝宝任务链参数 == 1 then
		任务数据[任务id].分类 = 1
		self.对话内容 = " #W/前些日子#Y/" .. 宝宝任务链地图 .. "#W/的#Y/" .. 宝宝任务链人物 .. "#W/帮了我一个大忙，还请你帮我前去表示感谢。"
	elseif self.宝宝任务链参数 == 2 then
		任务数据[任务id].分类 = 2
		local cs = math.random(5)
	    if cs == 1 then
				local 三药 = {"金创药","金香玉","小还丹","千年保心丹","风水混元丹","定神香","蛇蝎美人","九转回魂丹","佛光舍利子","五龙丹"}
				 任务数据[任务id].道具  = 三药[math.random(1,#三药)]
		elseif cs == 2 then
				local 二药 = {"天不老","紫石英","鹿茸","血色茶花","六道轮回","熊胆","凤凰尾","硫磺草","龙之心屑","火凤之睛","丁香水","月星子","仙狐涎","地狱灵芝","麝香","血珊瑚","餐风饮露","白露为霜","天龙水","孔雀红"}
				 任务数据[任务id].道具 = 二药[math.random(1,#二药)]
		elseif cs == 3 then
			     任务数据[任务id].等级=math.random(5,8)*10
			     任务数据[任务id].道具 = 取随机物品(任务数据[任务id].等级)
	    elseif cs == 4 then
	    	local 随机环={"百合","桃花","兰花","红玫瑰","康乃馨","牡丹","木鱼","编钟","唢呐","笛子","萧","钹","竖琴","琵琶"}
	    	任务数据[任务id].道具= 随机环[math.random(1,#随机环)]
	    elseif cs == 5 then
	    	local 随机道具={"魔兽要诀","高级魔兽要诀","召唤兽内丹","高级召唤兽内丹","鬼谷子","附魔宝珠","七彩石","易经丹"}
	    	任务数据[任务id].道具= 随机道具[math.random(1,#随机道具)]
		end
		self.对话内容 = " #W/听说#Y/" .. 宝宝任务链地图 .. "#W/的#Y/" .. 宝宝任务链人物 .. "#W/正在到处寻找#G/" .. 任务数据[任务id].道具 .. "#W/，请你前去帮他寻找个吧。"
	elseif self.宝宝任务链参数 == 3 then
		任务数据[任务id].分类 = 3
				local 召唤兽随机={
				{"大海龟","巨蛙","树怪","野猪","狐狸精","花妖","强盗","山贼","老虎","海星","章鱼","骷髅怪","蜘蛛精","雷鸟人","狼","天兵"},
                {"黑山老妖","兔子怪","赌徒","古代瑞兽","天兵","天将","地狱战神","大海龟","巨蛙","树怪","野猪","狐狸精","花妖","强盗","山贼","老虎","海星","章鱼","骷髅怪","蜘蛛精","雷鸟人","狼","天兵"},
                {"凤凰","雷鸟人","蛟龙","大海龟","巨蛙","树怪","野猪","狐狸精","花妖","强盗","山贼","老虎","蝴蝶仙子","海星","章鱼","骷髅怪","蜘蛛精","雷鸟人","狼","天兵"}
          		  }
         
				local sj =0
				if UserData[id].角色.等级 < 65  then
                      sj = 1
                elseif UserData[id].角色.等级 < 105 then
			    	  sj =2
			    else
			    	  sj =3
				end
		任务数据[任务id].召唤兽 =召唤兽随机[sj][math.random( #召唤兽随机[sj])]
		self.对话内容 = " #W/听说#Y/" .. 宝宝任务链地图 .. "#W/的#Y/" .. 宝宝任务链人物 .. "#W/最近迷恋上了炼妖，正在到处寻找#G/" .. 任务数据[任务id].召唤兽 .. "#W/，请你前去帮他寻找个吧。"
	-- elseif self.宝宝任务链参数 == 4 then
	-- 	任务数据[任务id].分类 = 4
	-- 	self.对话内容 = " #W/听闻#Y/" .. 宝宝任务链地图 .. "#W/的#Y/" .. 宝宝任务链人物 .. "#W/正在被心魔所困，还请前往协助铲除心魔。"
	end
	SendMessage(UserData[id].连接id,20,{"太白金星","马真人",self.对话内容})
	self:刷新追踪任务信息(id)
end