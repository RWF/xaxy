--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-03-01 00:13:52
--======================================================================--
function TaskControl:刷新天罡星()
 local 地妖造型 = {"龙太子","虎头怪","剑侠客","神天兵","巨魔王","逍遥生","飞燕女","英女侠","狐美人","骨精灵","玄彩娥","舞天姬","鬼潇潇","桃夭夭","巫蛮儿","偃无师"}

 
 local 刷出地图={[70]=1040,[80]=1216,[90]=1041,[100]=1193,[110]=1070,[120]=1091,[130]=1135,[140]=1174,[150]=1201,[160]=1920,[170]=1221,[180]=1042}
 local 难度星={"★☆☆☆☆","★★☆☆☆","★★★☆☆","★★★★☆","★★★★★"}
 local 难度名称 = {"黄金甲之谜·","明火珠之影·","泪痕碗之念·","四神鼎之怨·","独弦琴之思·"}
 local 名称难度 = {"天慧神器","天暗神器","天机神器","天暴神器","天闲神器","天英神器","天空神器","天雄神器","天损神器","天寿神器","天孤神器","天立神器","天佑神器",
 "天威神器" ,"天杀神器","天勇神器","天败神器","天牢神器","天暗神器","天富神器"}
 local 随机锦衣={"五毒锦绣","鹿角湾湾","浪迹天涯","胡旋回雪","萌萌小厨","鎏金婚姻服","雀之恋骨"}




 local 发送名称 =""
 for n=1,12 do--地图
   发送名称 =发送名称.."#G/"..MapData[刷出地图[60+n*10]].名称.."#Y/、"
    for i=1,2 do  ---刷1-3个
       local 临时等级=60+n*10
       local 临时id=self:生成任务id()
       local 临时坐标=MapControl:Randomloadtion(刷出地图[临时等级]) 
       local 临时造型 = 地妖造型[math.random(1,#地妖造型)]
       local 随机难度 = math.random(5) ---难度
          任务数据[临时id]={
       类型="天罡"
       ,起始=os.time()
       ,结束=3600
       ,任务id=临时id
       ,地图编号=刷出地图[临时等级]
       ,地图名称=MapData[刷出地图[临时等级]].名称
       ,名称=难度名称[随机难度]..名称难度[math.random(1,#名称难度)].."("..临时等级..")"
       ,称谓=难度星[随机难度]
       ,造型=临时造型

       ,锦衣数据=随机锦衣[math.random(1,#随机锦衣)]

       ,武器=取假人武器(临时造型,临时等级)
       ,染色={b=3,a=5,c=5}
       ,染色方案 = 取属性(临时造型).染色方案
       ,方向=math.random(4)-1
       ,坐标=临时坐标
       ,数字id=0
       ,战斗=false
       ,战斗类型=100038
       ,等级=临时等级
       ,难度=随机难度
       ,限制=10
       }
       MapControl:添加单位(任务数据[临时id])
     end
 end
   广播消息("#hd/#Y/据说#S/天魁城,天罡城,天机城,天闲城,天勇城天罡星#G/已经出现在"..发送名称.."#Y/场景中了各路英雄快前去挑战#28")
 end
function TaskControl:完成天罡星(id组,任务id)
 if 任务数据[任务id]==nil then 任务数据[任务id]=nil return 0 end
 if 任务数据[任务id].等级==70 then
   self.增加经验=1000000
   self.增加储备=100000
   self.果子数量=2
   self.宝石等级=2
   self.任务链等级=math.random(9,12)

   self.石头等级=1
 elseif 任务数据[任务id].等级==100 then
   self.增加经验=2000000
   self.增加储备=250000
   self.果子数量=3
   self.宝石等级=3
   self.任务链等级=math.random(10,12)
   self.石头等级=2
  elseif 任务数据[任务id].等级==130 then
   self.增加经验=3000000
   self.增加储备=500000
   self.果子数量=4
   self.宝石等级=4
   self.任务链等级=math.random(10,13)
   self.石头等级=3
  elseif 任务数据[任务id].等级==160 then
   self.增加经验=4000000
   self.增加储备=550000
   self.果子数量=5
   self.宝石等级=5
   self.任务链等级=math.random(10,14)
   self.石头等级=4
   end
 self.地煞难度=任务数据[任务id].难度
 self.地煞等级=任务数据[任务id].等级
 self.增加经验=math.floor(self.地煞等级*self.地煞等级*self.地煞等级*(1+self.地煞难度))
 self.增加储备=math.floor(self.地煞等级*self.地煞等级*20*(1+self.地煞难度*0.2)+100000)
 MapControl:移除单位(UserData[id组[1]].地图,任务id)
 任务数据[任务id]=nil
 for n=1,#id组 do
   if UserData[id组[n]]~=nil then
      self.符合抓鬼id=true
       RoleControl:添加经验(UserData[id组[n]],self.增加经验,"天罡")
       RoleControl:添加储备(UserData[id组[n]],self.增加储备,"天罡")
       if UserData[id组[n]].召唤兽.数据.参战~=0 then
         UserData[id组[n]].召唤兽:添加经验(math.floor(self.增加经验*0.5),id组[n],UserData[id组[n]].召唤兽.数据.参战,10)
       end
        -- UserData[id组[n]].角色.天罡积分=UserData[id组[n]].角色.天罡积分+self.地煞难度
        -- SendMessage(UserData[id组[n]].连接id,7,"#y/你获得了"..(self.地煞难度).."点天罡积分！")
        -- UserData[id组[n]].角色.神器积分=UserData[id组[n]].角色.神器积分+1
        -- SendMessage(UserData[id组[n]].连接id,7,"#y/你获得了1点神器积分！")
         if self.地煞难度==1 then
                local cj = ""
                local 奖励参数 = math.random(150)   --- 20/100  0.2 20%
                if 奖励参数 >=1 and 奖励参数 <=15 then
                  cj= "炼兽真经"
                elseif 奖励参数 >=16 and 奖励参数 <=31 then --10%
                   self.宝石名称 = 取随机宝石()
                    self.随机等级 = math.random(7)
                   ItemControl:GiveItem(id组[n], self.宝石名称, self.随机等级)
                  广播消息(9, "#hd/".."#S/(神器1星)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/成功降服了神器星，获得了#小东/" .. self.随机等级 .. "#y/级#y/" .. self.宝石名称 .. "奖励".."#"..math.random(110))
                elseif 奖励参数 >=32 and 奖励参数 <=55 then
                self.宝石名称 = 取随机宝石()
                    self.随机等级 = math.random(7)
                   ItemControl:GiveItem(id组[n], self.宝石名称, self.随机等级)
                  广播消息(9, "#hd/".."#S/(神器1星)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/成功降服了神器星，获得了#小东/" .. self.随机等级 .. "#y/级#y/" .. self.宝石名称 .. "奖励".."#"..math.random(110))
                elseif 奖励参数 >=56 and 奖励参数 <=60 then
                 ItemControl:GiveItem(id组[n],"召唤兽内丹")
                  广播消息("#hd/".."#S/(神器1星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了#小东/召唤兽内丹".."#"..math.random(110))
                elseif 奖励参数 >=61 and 奖励参数 <=73 then
                     local 随机兽决 = 取高级兽诀名称()
                   ItemControl:GiveItem(id组[n],"高级魔兽要诀",nil,随机兽决)
                   广播消息("#hd/".."#S/(神器1星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了#小东/"..随机兽决.."#G/高级魔兽要诀".."#"..math.random(110))
                elseif 奖励参数 >=74 and 奖励参数 <=83 then
                ItemControl:GiveItem(id组[n],"附魔宝珠")
                广播消息("#hd/".."#S/(神器1星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了#小东/附魔宝珠".."#"..math.random(110))
                elseif 奖励参数 >=84 and 奖励参数 <=90 then
                  cj="二级未激活符石"
                elseif 奖励参数 >=91 and 奖励参数 <=96 then
                  cj=取召唤兽随机宝石()
                elseif 奖励参数 >=97 and 奖励参数 <=100 then
                  cj="修炼果"
                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(神器1星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/"..cj.."#"..math.random(110))
                end
         elseif self.地煞难度==2 then
                local cj = ""
                local 奖励参数 = math.random(140)
                if 奖励参数 >=1 and 奖励参数 <=8 then
                  cj= "神兜兜"
                  elseif 奖励参数 >=9 and 奖励参数 <=15 then --10%
                  cj="二级未激活符石"
                elseif 奖励参数 >=16 and 奖励参数 <=37 then --10%
                    self.宝石名称 = 取随机宝石()
                    self.随机等级 = math.random(10)
                   ItemControl:GiveItem(id组[n], self.宝石名称, self.随机等级)
                  广播消息(9, "#hd/".."#S/(神器2星)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/成功降服了神器星，获得了#小东/" .. self.随机等级 .. "#y/级#y/" .. self.宝石名称 .. "奖励".."#"..math.random(110))
                elseif 奖励参数 >=38 and 奖励参数 <=43 then
                ItemControl:GiveItem(id组[n],"高级召唤兽内丹")
                广播消息("#hd/".."#S/(神器2星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了#小东/高级召唤兽内丹".."#"..math.random(110))
                elseif 奖励参数 >=44 and 奖励参数 <=60 then
                  ItemControl:GiveItem(id组[n],"星辉石",math.random(3,6))
                  广播消息("#hd/".."#S/(神器2星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/星辉石".."#"..math.random(110))
                elseif 奖励参数 >=61 and 奖励参数 <=73 then
                    local 随机兽决 = 取高级兽诀名称()
                  ItemControl:GiveItem(id组[n],"高级魔兽要诀",nil,随机兽决)
                  广播消息("#hd/".."#S/(神器2星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了#小东/"..随机兽决.."#G/高级魔兽要诀".."#"..math.random(110))
                elseif 奖励参数 >=74 and 奖励参数 <=83 then
                cj="七彩石"
                elseif 奖励参数 >=84 and 奖励参数 <=90 then
                  cj="二级未激活符石"
                elseif 奖励参数 >=91 and 奖励参数 <=96 then
                  cj=取召唤兽随机宝石()
                elseif 奖励参数 >=97 and 奖励参数 <=100 then
                  cj="修炼果"
                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(神器2星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/"..cj.."#"..math.random(110))
                end
          elseif self.地煞难度==3 then
                local cj = ""
                local 奖励参数 = math.random(130)
                if 奖励参数 >=1 and 奖励参数 <=11 then
                  cj= "神兜兜"
                elseif 奖励参数 >=12 and 奖励参数 <=31 then
                  ItemControl:GiveItem(id组[n],"清灵净瓶")
                  广播消息("#hd/".."#S/(神器3星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/附魔宝珠".."#"..math.random(110))
                elseif 奖励参数 >=32 and 奖励参数 <=43 then
                    cj="七彩石"
                elseif 奖励参数 >=44 and 奖励参数 <=60 then
                     local 随机宝石 =  取随机宝石()
                  ItemControl:GiveItem(id组[n],随机宝石,math.random(10))
                  广播消息("#hd/".."#S/(神器3星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/"..随机宝石.."#"..math.random(110))
                elseif 奖励参数 >=61 and 奖励参数 <=65 then
                  local 随机兽决 = 取活动兽诀名称()
                  ItemControl:GiveItem(id组[n],"高级魔兽要诀",nil,随机兽决)
                  广播消息("#hd/".."#S/(神器3星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/"..随机兽决.."#G/高级魔兽要诀".."#"..math.random(110))
                elseif 奖励参数 >=66 and 奖励参数 <=83 then
                  ItemControl:GiveItem(id组[n],"星辉石",math.random(6,8))
                  广播消息("#hd/".."#S/(神器3星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/星辉石".."#"..math.random(110))
                elseif 奖励参数 >=84 and 奖励参数 <=90 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(12, 15)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(神器3星)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/成功降服了地煞星，获得了玉皇大帝奖励的#小东/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(神器3星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/"..cj.."#"..math.random(110))
                end
          elseif self.地煞难度==4 then
                local cj = ""
                local 奖励参数 = math.random(130)
                if 奖励参数 >=1 and 奖励参数 <=12 then
                  cj= "修炼果"
                elseif 奖励参数 >=13 and 奖励参数 <=31 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(12, 15)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(神器4星)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/成功降服了地煞星，获得了玉皇大帝奖励的#小东/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))

                elseif 奖励参数 >=32 and 奖励参数 <=43 then
                   cj= "神兜兜"
                elseif 奖励参数 >=44 and 奖励参数 <=60 then
                     local 随机宝石 =  取随机宝石()
                  ItemControl:GiveItem(id组[n],随机宝石,math.random(5,10))
                  广播消息("#hd/".."#S/(神器4星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/"..随机宝石.."#"..math.random(110))
                elseif 奖励参数 >=61 and 奖励参数 <=73 then
                  local 随机兽决 = 取高级兽诀名称()
                  ItemControl:GiveItem(id组[n],"高级魔兽要诀",nil,随机兽决)
                  广播消息("#hd/".."#S/(神器4星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/"..随机兽决.."#G/高级魔兽要诀".."#"..math.random(110))
                elseif 奖励参数 >=74 and 奖励参数 <=83 then
                  ItemControl:GiveItem(id组[n],"星辉石",math.random(8,9))
                  广播消息("#hd/".."#S/(神器4星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/星辉石".."#"..math.random(110))
                elseif 奖励参数 >=84 and 奖励参数 <=90 then
                    ItemControl:GiveItem(id组[n],"不磨符")
                  广播消息("#hd/".."#S/(神器4星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/不磨符".."#"..math.random(110))
                  elseif 奖励参数 >=91 and 奖励参数 <=96 then
                  ItemControl:GiveItem(id组[n],"高级召唤兽内丹")
                广播消息("#hd/".."#S/(神器4星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了#小东/高级召唤兽内丹".."#"..math.random(110))
                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(神器4星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/"..cj.."#"..math.random(110))
                end
          elseif self.地煞难度==5 then
                local cj = ""
                local 奖励参数 = math.random(130)
                if 奖励参数 >=1 and 奖励参数 <=12 then
                  cj= "修炼果"
                elseif 奖励参数 >=13 and 奖励参数 <=31 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(12, 15)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(神器5星)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/成功降服了地煞星，获得了玉皇大帝奖励的#小东/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))

                elseif 奖励参数 >=32 and 奖励参数 <=43 then
                   cj= "神兜兜"
                elseif 奖励参数 >=44 and 奖励参数 <=63 then
                     local 随机宝石 =  取随机宝石()
                  ItemControl:GiveItem(id组[n],随机宝石,math.random(5,13))
                  广播消息("#hd/".."#S/(神器5星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/"..随机宝石.."#"..math.random(110))
                elseif 奖励参数 >=64 and 奖励参数 <=75 then
                  local 随机兽决 = 取高级兽诀名称()
                  ItemControl:GiveItem(id组[n],"高级魔兽要诀",nil,随机兽决)
                  广播消息("#hd/".."#S/(神器5星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/"..随机兽决.."#G/高级魔兽要诀".."#"..math.random(110))
                elseif 奖励参数 >=76 and 奖励参数 <=85 then
                  ItemControl:GiveItem(id组[n],"星辉石",math.random(8,9))
                  广播消息("#hd/".."#S/(神器5星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/星辉石".."#"..math.random(110))
                elseif 奖励参数 >=86 and 奖励参数 <=90 then
                    ItemControl:GiveItem(id组[n],"武器幻色丹")
                  广播消息("#hd/".."#S/(神器5星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/武器幻色丹".."#"..math.random(110))
                   elseif 奖励参数 >=91 and 奖励参数 <=96 then
                  ItemControl:GiveItem(id组[n],"高级召唤兽内丹")
                广播消息("#hd/".."#S/(神器5星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了#小东/高级召唤兽内丹".."#"..math.random(110))
                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(神器5星)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了神器星，获得了玉皇大帝奖励的#小东/"..cj.."#"..math.random(110))
                end
           end
        end
   end
 end