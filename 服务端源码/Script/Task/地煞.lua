--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-06-25 02:01:13
--======================================================================--
function TaskControl:刷新地妖星()
 local 封妖编号=1506
 local 地妖造型 = {"龙太子","虎头怪","剑侠客","神天兵","巨魔王","逍遥生","飞燕女","英女侠","狐美人","骨精灵","玄彩娥","舞天姬","鬼潇潇","桃夭夭","巫蛮儿","偃无师"}
 local 刷出地图={[70]=1198,[80]=1138,[90]=1135,[100]=1002,[110]=1142,[120]=1111,[130]=1116,[140]=1140,[150]=1146,[160]=1512,[170]=1122,[180]=1131}
 local 难度星={"★☆☆☆☆","★★☆☆☆","★★★☆☆","★★★★☆","★★★★★","★★★★★★","★★★★★★★","★★★★★★★★"}
 local 难度名称 = {"初出茅庐","小有所成","伏虎斩妖","御风神行","履水吐焰","泽被星魂","暗夜星魂","嗜血霸星"}
 local 名称难度 = {"地兽星","地灵星","地佑星","地文星","地正星","地辟星","地阖星","地强星","地暗星","地猛星","地奇星","地彗星","地微星",
 "地暴星" ,"地佐星","地会星","地辅星","地猖星","地狂星","地默星"}
local 随机锦衣={"浪淘纱·月白","齐天大圣","浪淘纱","冰灵蝶翼·月笼","从军行·须眉","铃儿叮当","青花瓷·月白","落星织","碧华锦·凌雪","冰雪玉兔"}
 local 发送名称 =""
 for n=1,12 do
   发送名称 =发送名称.."#G/"..MapData[刷出地图[60+n*10]].名称.."#Y/、"
    for i=2,6 do
       local 临时等级=60+n*10
       local 临时id=self:生成任务id()
       local 临时坐标= MapControl:Randomloadtion(刷出地图[临时等级]) 
       local 临时造型 = 地妖造型[math.random(#地妖造型)]
       local 随机难度 = math.random(8)
      任务数据[临时id]={
       类型="地煞"
       ,起始=os.time()
       ,结束=3600
       ,任务id=临时id
       ,地图编号=刷出地图[临时等级]
       ,地图名称=MapData[刷出地图[临时等级]].名称
       ,名称=难度名称[随机难度]..名称难度[math.random(#名称难度)].."("..临时等级..")"
       ,称谓=难度星[随机难度]
       ,造型=临时造型
       -- ,锦衣数据=随机锦衣[math.random(#随机锦衣)]
       ,武器=取假人武器(临时造型,临时等级)
       ,染色={b=3,a=4,c=4}
       ,染色方案 = 取属性(临时造型).染色方案
       ,方向=math.random(4)-1
       ,坐标=临时坐标
       ,数字id=0
       ,战斗=false
       ,战斗类型=100029
       ,等级=临时等级
       ,难度=随机难度
       ,限制=12
       }
       MapControl:添加单位(任务数据[临时id])
     end
 end
   广播消息("#hd/#Y/据说#S/初出茅庐、小有所成、伏虎斩妖、御风神行、履水吐焰、泽被星魂、暗夜星魂、嗜血霸星地煞星#G/已经出现在"..发送名称.."#Y/场景中了各路英雄快前去挑战#1")
 end
function TaskControl:完成地妖星(id组,任务id)
 if 任务数据[任务id]==nil then 任务数据[任务id]=nil return 0 end
 if 任务数据[任务id].等级==70 then
   self.增加经验=1000000
   self.增加储备=100000
   self.果子数量=2
   self.宝石等级=2
   self.任务链等级=math.random(9,12)

   self.石头等级=1
 elseif 任务数据[任务id].等级==100 then
   self.增加经验=2000000
   self.增加储备=250000
   self.果子数量=3
   self.宝石等级=3
   self.任务链等级=math.random(10,12)
   self.石头等级=2
  elseif 任务数据[任务id].等级==130 then
   self.增加经验=3000000
   self.增加储备=500000
   self.果子数量=4
   self.宝石等级=4
   self.任务链等级=math.random(10,13)
   self.石头等级=3
  elseif 任务数据[任务id].等级==160 then
   self.增加经验=4000000
   self.增加储备=550000
   self.果子数量=5
   self.宝石等级=5
   self.任务链等级=math.random(10,14)
   self.石头等级=4
   end
 self.地煞难度=任务数据[任务id].难度
 self.地煞等级=任务数据[任务id].等级
 self.增加经验=math.floor(self.地煞等级*self.地煞等级*self.地煞等级*(1+self.地煞难度))
 self.增加储备=math.floor(self.地煞等级*self.地煞等级*20*(1+self.地煞难度*0.2)+100000)
 MapControl:移除单位(UserData[id组[1]].地图,任务id)
 任务数据[任务id]=nil
 for n=1,#id组 do
   if UserData[id组[n]]~=nil then
      self.符合抓鬼id=true
       RoleControl:添加经验(UserData[id组[n]],self.增加经验,"地煞")
       RoleControl:添加储备(UserData[id组[n]],self.增加储备,"地煞")
       RoleControl:添加活跃度(UserData[id组[n]],1)
       if UserData[id组[n]].召唤兽.数据.参战~=0 then
         UserData[id组[n]].召唤兽:添加经验(math.floor(self.增加经验*0.5),id组[n],UserData[id组[n]].召唤兽.数据.参战,10)
       end
        RoleControl:添加地煞积分(UserData[id组[n]],self.地煞难度)

         if self.地煞难度==1 then
                local cj = ""
                local 奖励参数 = math.random(140)   --- 20/100  0.2 20%
                if 奖励参数 >=1 and 奖励参数 <=10 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(10, 11)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(初出茅庐地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=11 and 奖励参数 <=21 then --10%
                  local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(10, 11)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(初出茅庐地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=22 and 奖励参数 <=32 then
                    cj="金银宝盒"
                elseif 奖励参数 >=33 and 奖励参数 <=43 then
                  cj="玲珑宝图"
                elseif 奖励参数 >=44 and 奖励参数 <=46 then
                  cj="金银宝盒"
                elseif 奖励参数 >=47 and 奖励参数 <=61 then
                  local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(10, 11)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(初出茅庐地煞星)".."#y/居然败倒在#G/".. UserData[id组[n]].角色.名称.. "#y/ 的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=62 and 奖励参数 <=72 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(10, 11)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(初出茅庐地煞星)".."#y/居然败倒在#G/".. UserData[id组[n]].角色.名称.. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=73 and 奖励参数 <=83 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(10, 11)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(初出茅庐地煞星)".."#y/居然败倒在#G/".. UserData[id组[n]].角色.名称.. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=84 and 奖励参数 <=94 then
                  cj="海马"
                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(初出茅庐地煞星)".."#y/居然败倒在#G/".. UserData[id组[n]].角色.名称.. "#y/的脚下,慌忙中落下了#G/"..cj.."#"..math.random(110))
                end
         elseif self.地煞难度==2 then
               local cj = ""
                local 奖励参数 = math.random(140)   --- 20/100  0.2 20%
                if 奖励参数 >=1 and 奖励参数 <=10 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(11, 12)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(小有所成地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=11 and 奖励参数 <=21 then --10%
                  local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(11, 12)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(小有所成地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110)) 
                elseif 奖励参数 >=22 and 奖励参数 <=32 then
                    cj="金银宝盒"
                elseif 奖励参数 >=33 and 奖励参数 <=43 then
                  cj="玲珑宝图"
                elseif 奖励参数 >=44 and 奖励参数 <=48 then
                  cj="修炼果"
                elseif 奖励参数 >=49 and 奖励参数 <=61 then
                  local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(11, 12)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(小有所成地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=62 and 奖励参数 <=72 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(11, 12)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(小有所成地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=73 and 奖励参数 <=83 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(11, 12)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(小有所成地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=84 and 奖励参数 <=94 then
                  cj="海马"
                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(小有所成地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/"..cj.."#"..math.random(110))
                end
          elseif self.地煞难度==3 then
                local cj = ""
                local 奖励参数 = math.random(130)   --- 20/100  0.2 20%
                if 奖励参数 >=1 and 奖励参数 <=10 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(12, 13)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(伏虎斩妖地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=11 and 奖励参数 <=21 then --10%
                  local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(12, 13)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(伏虎斩妖地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110)) 
                elseif 奖励参数 >=22 and 奖励参数 <=32 then
                    cj="金银宝盒"
                elseif 奖励参数 >=33 and 奖励参数 <=43 then
                  cj="玲珑宝图"
                elseif 奖励参数 >=44 and 奖励参数 <=49 then
                  cj="修炼果"
                elseif 奖励参数 >=50 and 奖励参数 <=61 then
                  local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(12, 13)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(伏虎斩妖地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=62 and 奖励参数 <=72 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(12, 13)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(伏虎斩妖地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=73 and 奖励参数 <=83 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(12, 13)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(伏虎斩妖地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=84 and 奖励参数 <=94 then
                  cj="海马"
                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(伏虎斩妖地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/"..cj.."#"..math.random(110))
                end
          elseif self.地煞难度==4 then
                local cj = ""
                local 奖励参数 = math.random(130)   --- 20/100  0.2 20%
                if 奖励参数 >=1 and 奖励参数 <=10 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 14)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(御风神行地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=11 and 奖励参数 <=21 then --10%
                  local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 14)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(御风神行地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110)) 
                elseif 奖励参数 >=22 and 奖励参数 <=32 then
                    cj="金银宝盒"
                elseif 奖励参数 >=33 and 奖励参数 <=43 then
                  cj="玲珑宝图"
                elseif 奖励参数 >=44 and 奖励参数 <=50 then
                  cj="修炼果"
                elseif 奖励参数 >=51 and 奖励参数 <=61 then
                  local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 14)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(御风神行地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=62 and 奖励参数 <=72 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 14)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(御风神行地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=73 and 奖励参数 <=83 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 14)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(御风神行地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=84 and 奖励参数 <=94 then
                  cj="海马"
                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(御风神行地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/"..cj.."#"..math.random(110))
                end
          elseif self.地煞难度==5 then
            RoleControl:添加成就积分(UserData[id组[n]],50,"地煞")
                local cj = ""
                local 奖励参数 = math.random(120)   --- 20/100  0.2 20%
                if 奖励参数 >=1 and 奖励参数 <=10 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 14)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                广播消息("#hd/".."#S/(履水吐焰地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=11 and 奖励参数 <=21 then --10%
                  local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 15)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                广播消息("#hd/".."#S/(履水吐焰地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110)) 
                elseif 奖励参数 >=22 and 奖励参数 <=32 then
                    cj="金银宝盒"
                elseif 奖励参数 >=33 and 奖励参数 <=43 then
                  cj="玲珑宝图"
                elseif 奖励参数 >=44 and 奖励参数 <=50 then
                  cj="修炼果"
                elseif 奖励参数 >=51 and 奖励参数 <=61 then
                  local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 15)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                广播消息("#hd/".."#S/(履水吐焰地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=62 and 奖励参数 <=72 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 14)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                广播消息("#hd/".."#S/(履水吐焰地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=73 and 奖励参数 <=83 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 14)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                广播消息("#hd/".."#S/(履水吐焰地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=84 and 奖励参数 <=94 then
                  cj="神兜兜"
                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(履水吐焰地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/"..cj.."#"..math.random(110))
                end
           elseif self.地煞难度==6 then
            RoleControl:添加成就积分(UserData[id组[n]],50,"地煞")
                local cj = ""
                local 奖励参数 = math.random(120)   --- 20/100  0.2 20%
                if 奖励参数 >=1 and 奖励参数 <=10 then
                    cj="修炼果"
                elseif 奖励参数 >=11 and 奖励参数 <=21 then --10%
                  local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(14, 15)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                广播消息("#hd/".."#S/(泽被星魂地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110)) 
                elseif 奖励参数 >=22 and 奖励参数 <=32 then
                    cj="金银宝盒"
                elseif 奖励参数 >=33 and 奖励参数 <=43 then
                  cj="玲珑宝图"
                elseif 奖励参数 >=44 and 奖励参数 <=50 then
                  cj="修炼果"
                elseif 奖励参数 >=51 and 奖励参数 <=61 then
                  local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 15)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                广播消息("#hd/".."#S/(泽被星魂地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=62 and 奖励参数 <=72 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 14)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                广播消息("#hd/".."#S/(泽被星魂地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=73 and 奖励参数 <=83 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 14)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                广播消息("#hd/".."#S/(泽被星魂地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=84 and 奖励参数 <=94 then
                  cj="神兜兜"
                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(泽被星魂地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/"..cj.."#"..math.random(110))
                end
                 elseif self.地煞难度==7 then
                  RoleControl:添加成就积分(UserData[id组[n]],50,"地煞")
                local cj = ""
                local 奖励参数 = math.random(120)   --- 20/100  0.2 20%
                if 奖励参数 >=1 and 奖励参数 <=10 then
                    cj="修炼果"
                elseif 奖励参数 >=11 and 奖励参数 <=21 then --10%
                  cj="修炼果"
                elseif 奖励参数 >=22 and 奖励参数 <=32 then
                    cj="灵饰洗炼石"
                elseif 奖励参数 >=33 and 奖励参数 <=43 then
                  cj="玲珑宝图"
                elseif 奖励参数 >=44 and 奖励参数 <=50 then
                  cj="修炼果"
                elseif 奖励参数 >=51 and 奖励参数 <=61 then
                  local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 15)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                广播消息("#hd/".."#S/(暗夜星魂地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=62 and 奖励参数 <=72 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 14)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                广播消息("#hd/".."#S/(暗夜星魂地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=73 and 奖励参数 <=83 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 14)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                广播消息("#hd/".."#S/(暗夜星魂地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=84 and 奖励参数 <=94 then
                  cj="神兜兜"
                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(暗夜星魂地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/"..cj.."#"..math.random(110))
                end
           
 elseif self.地煞难度==8 then
  RoleControl:添加成就积分(UserData[id组[n]],50,"地煞")
                local cj = ""
                local 奖励参数 = math.random(120)   --- 20/100  0.2 20%
                if 奖励参数 >=1 and 奖励参数 <=10 then
                    cj="修炼果"
                elseif 奖励参数 >=11 and 奖励参数 <=21 then --10%
                  cj="修炼果"
                elseif 奖励参数 >=22 and 奖励参数 <=32 then
               cj="灵饰洗炼石"
                elseif 奖励参数 >=33 and 奖励参数 <=43 then
                  cj="玲珑宝图"
                elseif 奖励参数 >=44 and 奖励参数 <=50 then
                  cj="修炼果"
                elseif 奖励参数 >=51 and 奖励参数 <=61 then
                  local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 15)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                广播消息("#hd/".."#S/(嗜血霸星地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=62 and 奖励参数 <=72 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 14)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                广播消息("#hd/".."#S/(嗜血霸星地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=73 and 奖励参数 <=83 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(13, 14)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                广播消息("#hd/".."#S/(嗜血霸星地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=84 and 奖励参数 <=94 then
                  cj="神兜兜"
                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(嗜血霸星地煞星)".."#y/居然败倒在#G/" .. UserData[id组[n]].角色.名称 .. "#y/的脚下,慌忙中落下了#G/"..cj.."#"..math.random(110))
                end
           end
        end
    end
 end