--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-10-26 23:15:42
--======================================================================--
function TaskControl:添加神器任务(id)
      local 临时id6= tonumber(UserData[id].id .. "4256")
     任务数据[临时id6]={
      类型="神器任务"
     ,id=id
     ,起始=os.time()
     ,结束=3600
     ,任务id=临时id6
     ,地图编号=1198
     ,NPC=119805
     ,进程=1
     }

     UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])]=临时id6
     SendMessage(UserData[id].连接id,7,"#Y/你领取了神器任务，请点击任务列表查看")
     self:刷新追踪任务信息(id)
end


function TaskControl:触发神器任务(id,任务id)
    if UserData[id].队伍~=0 then
         SendMessage(UserData[id].连接id,7,"#Y/请解散队伍以后再找我")
        return
    end
   if  任务数据[任务id].进程 ==3 then
            FightGet:进入处理(id, 100069, "66", RoleControl:GetTaskID(UserData[id],"神器任务"))
   elseif  任务数据[任务id].进程 ==4 then
        FightGet:进入处理(id, 100070, "66", RoleControl:GetTaskID(UserData[id],"神器任务"))
   elseif  任务数据[任务id].进程 ==6 then 

            FightGet:进入处理(id, 100071, "66", RoleControl:GetTaskID(UserData[id],"神器任务"))
   end
end
function TaskControl:完成神器任务(id,任务id)
    local 进程 = 任务数据[任务id].进程
     任务数据[任务id].进程=任务数据[任务id].进程+1
    if  进程 ==1 then 
        任务数据[任务id].道具="醉生梦死"
        任务数据[任务id].NPC=102801
        任务数据[任务id].地图编号=1028
        RoleControl:添加银子(UserData[id],100000,"神器任务")
        RoleControl:添加经验(UserData[id],2000000, "神器任务")
    elseif 进程 ==2 then
        任务数据[任务id].NPC=151201
        任务数据[任务id].地图编号=1512
        RoleControl:添加银子(UserData[id],200000,"神器任务")
        RoleControl:添加经验(UserData[id],5000000, "神器任务")
   elseif 进程 ==3 then
        任务数据[任务id].NPC=111103
        任务数据[任务id].地图编号=1111
        RoleControl:添加银子(UserData[id],200000,"神器任务")
        RoleControl:添加经验(UserData[id],5000000, "神器任务")
   elseif 进程 ==4 then
            任务数据[任务id].NPC=36
        任务数据[任务id].地图编号=1001
        RoleControl:添加银子(UserData[id],200000,"神器任务")
        RoleControl:添加经验(UserData[id],5000000, "神器任务")
    elseif 进程 ==5 then
                任务数据[任务id].NPC=121701
        任务数据[任务id].地图编号=1217
        RoleControl:添加银子(UserData[id],200000,"神器任务")
        RoleControl:添加经验(UserData[id],5000000, "神器任务")   
    elseif 进程 ==6 then 
       
        RoleControl:取消任务(UserData[id],任务id)
        任务数据[任务id]=nil
        RoleControl:添加神器(UserData[id])
         MapControl:Jump(id, 1001, 361, 36,true)
   
    end
        
        self:刷新追踪任务信息(id)
        
end

