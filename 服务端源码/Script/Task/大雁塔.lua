function TaskControl:完成大雁塔(id组, 层数)
	self.限制等级 = 50 + 20 * 层数
	self.限制奖励 = false

	if   取队伍高于等级(id组[1], self.限制等级) == false then
		self.限制奖励 = true
		广播队伍消息(id组[1], 7, "#y/队伍中有玩家等级达到了" .. self.限制等级 .. "级，您无法获得物品奖励")
	end

	self.显示名称 = ""
	self.最低等级 = 150
	self.最高等级 = 0
	for n = 1, #id组 do
		if self.最高等级 < UserData[id组[n]].角色.等级 then
			self.最高等级 = UserData[id组[n]].角色.等级
		end

		if UserData[id组[n]].角色.等级 < self.最低等级 then
			self.最低等级 = UserData[id组[n]].角色.等级
		end
	end
	self.奖励触发 = true
	if self.最高等级 - self.最低等级 >= 30 and self.最低等级 ~= 0 then
		self.奖励触发 = false
	end

	if 层数 <= 3 then
		self.限制奖励 = true
	end
	for n = 1, #id组 do
		if UserData[id组[n]] ~= nil then
			self.符合抓鬼id = true
			self.显示名称 = self.显示名称 .. UserData[id组[n]].角色.名称
			if n ~= #id组 then
				self.显示名称 = self.显示名称 .. "、"
			end
			if self.符合抓鬼id then
				self.添加经验 = 0
				self.添加银子 = 0

				self.添加经验 = 层数*1000000
				self.添加银子 = 层数*Config.大雁塔奖励
				RoleControl:添加储备(UserData[id组[n]],self.添加经验, "大雁塔")
				活动数据.大雁塔数据[层数][UserData[id组[n]].id] = 1
                if 层数 > 10 and UserData[id组[n]].角色.大雁塔 < 层数  then
				UserData[id组[n]].角色.大雁塔 =层数
			    end
				if 层数 > 4 then
					RoleControl:添加银子(UserData[id组[n]],self.添加银子,"大雁塔")


				end
                elseif 层数 > 10 then
				 RoleControl:添加活跃度(UserData[id组[n]],2)
				end
				if UserData[id组[n]].召唤兽.数据.参战 ~= 0 then
					UserData[id组[n]].召唤兽:添加经验(math.floor(self.添加经验 * 0.05), id组[n], UserData[id组[n]].召唤兽.数据.参战, 10)
				end
				if  self.限制奖励 == false  and math.random(100)<= 50  then
					if math.random(100) <= 15 then
						ItemControl:GiveItem(id组[n], "元宵")
						广播消息("#hd/".."#S/(大雁塔)".."#r/ " .. UserData[id组[n]].角色.名称 .. "#y/成功战胜了大雁塔" .. 层数 .. "层守卫，意外的获得了#g/元宵".."#"..math.random(110))
					elseif math.random(100) <= 30 then
						ItemControl:GiveItem(id组[n], "魔兽要诀")
						广播消息("#hd/".."#S/(大雁塔)".."#r/ " .. UserData[id组[n]].角色.名称 .. "#y/成功战胜了大雁塔" .. 层数 .. "层守卫，意外的获得了#g/魔兽要诀".."#"..math.random(110))
					elseif math.random(100) <= 45 then
						self.临时宝石=取随机宝石()
						ItemControl:GiveItem(id组[n],self.临时宝石)
						广播消息("#hd/".."#S/(大雁塔)".."#r/ " .. UserData[id组[n]].角色.名称 .. "#y/成功战胜了大雁塔" .. 层数 .. "层守卫，意外的获得了#g/" .. self.临时宝石.."#"..math.random(110))

						if math.random(100) <= 48 then
						ItemControl:GiveItem(id组[n], "超级金柳露")
						广播消息("#hd/".."#S/(大雁塔)".."#r/ " .. UserData[id组[n]].角色.名称 .. "#y/成功战胜了大雁塔" .. 层数 .. "层守卫，意外的获得了#g/超级金柳露".."#"..math.random(110))
					else
						self.强化石名称 = 取随机强化石()
						ItemControl:GiveItem(id组[n],self.强化石名称)
						SendMessage(UserData[id组[n]].连接id, 7, "#y/你获得了" .. self.强化石名称)
						广播消息("#hd/".."#S/(大雁塔)".."#g/ " .. UserData[id组[n]].角色.名称 .. "#y/成功战胜了大雁塔" .. 层数 .. "层守卫，意外的获得了#r/" .. self.强化石名称.."#"..math.random(110))
					end
				end
			end
		end
	end
 end
function TaskControl:挑战大雁塔(id, 层数)
	if UserData[id].队伍 == 0 then
		SendMessage(UserData[id].连接id, 7, "#y/此活动必须组队完成")

		return 0
	end
	self.重复名称 = ""
	self.重复名称1=""
	for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
		if 活动数据.大雁塔数据[层数][UserData[队伍数据[UserData[id].队伍].队员数据[n]].id] ~= nil then
			self.重复名称 = UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称
		end
		if 层数 > 1 and  活动数据.大雁塔数据[层数-1][UserData[队伍数据[UserData[id].队伍].队员数据[n]].id] ==nil then
			self.重复名称1 = UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称
		end
	end
	if self.重复名称 ~= "" then
		广播队伍消息(id, 7, "#g/" .. self.重复名称 .. "#y/今日已经挑战过本层守卫了")
		return 0
	end
	if self.重复名称1 ~= "" then
		广播队伍消息(id, 7, "#g/" .. self.重复名称1 .. "#y/未完成前一轮的挑战")
		return 0
	end
	FightGet:进入处理(id, 100019, 层数, 层数)
 end
