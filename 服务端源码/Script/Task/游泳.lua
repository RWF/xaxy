
function TaskControl:添加游泳任务(id)----------------
 self.临时id3=self:生成任务id()
  任务数据[self.临时id3]={
  类型="游泳"
 ,id=id
 ,起始=os.time()
 ,结束=7200
 ,NPC=109212
 ,地图编号=1092
 ,进程=1
 ,任务id=self.临时id3
 ,数字id={}
 ,战斗序列={}
 }
 for n=1,#队伍数据[UserData[id].队伍].队员数据 do
    self.任务临时id1=RoleControl:生成任务id(UserData[队伍数据[UserData[id].队伍].队员数据[n]])
    UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.任务数据[self.任务临时id1]=self.临时id3
    任务数据[self.临时id3].数字id[n]=队伍数据[UserData[id].队伍].队员数据[n]
    游泳比赛数据[队伍数据[UserData[id].队伍].队员数据[n]]=游泳比赛数据[队伍数据[UserData[id].队伍].队员数据[n]]+1
    self:刷新追踪任务信息(队伍数据[UserData[id].队伍].队员数据[n])
  end
  for n=1,15 do
   任务数据[self.临时id3].战斗序列[n]=false
  end
 广播队伍消息(id,7,"#Y/你参加了游戏大赛，请前往1号裁判处报道")
 end
function TaskControl:触发游泳比赛(id,标识)-------------
 local  游泳NPC={109212,151402,151403,111802,111801,112001,153201,111901,111803,111620,111619,111618,111621,150607,150608}
  local 游泳地图={1092,1514,1514,1118,1118,1120,1532,1119,1118,1116,1116,1116,1116,1506,1506}
  if UserData[id].队伍==0 then
   SendMessage(UserData[id].连接id,7,"#Y/请组队前来我这里报道")
   return 0
  elseif #队伍数据[UserData[id].队伍].队员数据<3 and DebugMode == false  then
   SendMessage(UserData[id].连接id,7,"#Y/队伍中的成员数低于3人，无法进行报道")
   return 0
  else
    --检查任务
   self.任务通过=true
    for n=1,#队伍数据[UserData[id].队伍].队员数据 do
      self.临时id=队伍数据[UserData[id].队伍].队员数据[n]
      if  RoleControl:GetTaskID(UserData[self.临时id],"游泳")==0 then
       self.任务通过=false
      elseif 游泳NPC[任务数据[RoleControl:GetTaskID(UserData[self.临时id],"游泳")].进程]~=任务数据[RoleControl:GetTaskID(UserData[self.临时id],"游泳")].NPC then
        self.任务通过=false
        end
      end
   if self.任务通过==false then
     SendMessage(UserData[id].连接id,7,"#Y/队伍中有玩家任务不一致，无法进行报道")
     return 0
     end
    --检查是否进行战斗
    if 任务数据[RoleControl:GetTaskID(UserData[id],"游泳")].战斗序列[任务数据[RoleControl:GetTaskID(UserData[id],"游泳")].进程]==false and math.random(100)<=70 then
       FightGet:进入处理(id,100039,"66",RoleControl:GetTaskID(UserData[id],"游泳"))
    else
      self.结束id=RoleControl:GetTaskID(UserData[id],"游泳")
     for n=1,#队伍数据[UserData[id].队伍].队员数据 do
       self.临时id=队伍数据[UserData[id].队伍].队员数据[n]
       self.临时等级=UserData[self.临时id].角色.等级
       self.经验奖励=math.floor(self.临时等级*self.临时等级*200*(1+任务数据[RoleControl:GetTaskID(UserData[self.临时id],"游泳")].进程/10))
       self.银子奖励=math.floor(self.临时等级*self.临时等级*6*(1+任务数据[RoleControl:GetTaskID(UserData[self.临时id],"游泳")].进程/10))
       RoleControl:添加经验(UserData[self.临时id],self.经验奖励,"游泳")
       RoleControl:添加储备(UserData[self.临时id],self.银子奖励,"游泳")
       RoleControl:添加仙玉(UserData[self.临时id],20,"游泳")
       if math.random(100) <=70 then
          local 奖励参数=math.random(100)
          local 奖励物品
            if 奖励参数<=5 then
              奖励物品 = "九转金丹"
            elseif 奖励参数<=10 then
                奖励物品 = "彩果"
            elseif 奖励参数<=20 then
                奖励物品 = "魔兽要诀"
            elseif 奖励参数<=30 then
               奖励物品 = "藏宝图"
            elseif 奖励参数<=40 then
              奖励物品 = "月华露"
            elseif 奖励参数<=55 then
                奖励物品 = "花豆"
            elseif 奖励参数<=60 then
              奖励物品 = "金柳露"
            else
              RoleControl:添加仙玉(UserData[self.临时id],10,"游泳")
              奖励物品 = "天眼通符"
            end
             ItemControl:GiveItem(self.临时id,奖励物品)
             广播消息("#hd/".."#S/(游泳活动)".."#R/ "..UserData[self.临时id].角色.名称.."#Y/在游泳大赛中激流勇进，获得了裁判奖励的的#g/"..奖励物品.."#"..math.random(110))
        end
        if 任务数据[RoleControl:GetTaskID(UserData[self.临时id],"游泳")].进程==15 then
          RoleControl:取消任务(UserData[self.临时id],RoleControl:GetTaskID(UserData[self.临时id],"游泳"))
          RoleControl:添加活跃度(UserData[self.临时id],10)
          SendMessage(UserData[self.临时id].连接id,7,"#Y/你完成了游泳大赛")
        else
        SendMessage(UserData[self.临时id].连接id,7,"#Y/请前往下一个裁判处报道")

        end
       
     end
        if 任务数据[self.结束id].进程==15 then
        任务数据[self.结束id]=nil
        else
        任务数据[self.结束id].进程=任务数据[self.结束id].进程+1
        任务数据[self.结束id].NPC=游泳NPC[任务数据[self.结束id].进程]
        任务数据[self.结束id].地图编号=游泳地图[任务数据[self.结束id].进程]
        end
        for n=1,#队伍数据[UserData[id].队伍].队员数据 do
          local 临时id=队伍数据[UserData[id].队伍].队员数据[n]
          self:刷新追踪任务信息(临时id)
        end
      end
    end
 end
