--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-10-17 02:35:35
--======================================================================--
function TaskControl:刷出帮战怪物()---------------------
  local 刷出地图={11351,11352,11353,11354,11355}
  local 临时怪物 ={"噬天虎"}
  local 临时名称 ={"拦路虎"}
  local 刷出数量 = 50
    for n=1,#帮派竞赛.帮战列表 do
        for i=1, 刷出数量 do
        local 临时数值=math.random(1,#临时怪物)
        local 天关id=self:生成任务id()
        任务数据[天关id]={
        类型="帮战怪物"
        ,id=天关id
        ,起始=os.time()
        ,结束=3600
        ,任务id=天关id
        ,地图编号=刷出地图[n]
        ,地图名称=MapData[刷出地图[n]].名称
        ,名称=临时名称[临时数值]
        ,造型=临时怪物[临时数值]
        ,方向=math.random(4)-1
        ,坐标=MapControl:Randomloadtion(刷出地图[n]) 
        ,数字id=0
        ,战斗=false
        }
        MapControl:添加单位(任务数据[天关id])
        end
   end

 end


function TaskControl:触发宝箱(id,标识)---------------
 local 临时任务id=MapControl.MapData[UserData[id].地图].单位组[标识].id
 local 类型 =任务数据[临时任务id].类型
  if MapControl.MapData[UserData[id].地图].单位组[标识]==nil then
   SendMessage(UserData[id].连接id,7,"#Y/任务数据异常，代号9002")
   return 0
  elseif 任务数据[临时任务id]==nil then
   SendMessage(UserData[id].连接id,7,"#Y/任务数据异常，代号9001")
   return 0
  else
     MapControl:移除单位(UserData[id].地图,临时任务id)
     任务数据[临时任务id]=nil

        local cj = ""
                local 奖励参数 = math.random(140)
                if 奖励参数 >=1 and 奖励参数 <=20 then
                  ItemControl:GiveItem(id,"附魔宝珠")
                广播消息("#hd/".."#S/(帮战奖励)".."#g/ "..UserData[id].角色.名称.."#Y/在帮战竞赛中所向披靡GM奖励#g/附魔宝珠".."#"..math.random(110))
                elseif 奖励参数 >=21 and 奖励参数 <=39 then
                    cj="玲珑宝图"
                elseif 奖励参数 >=40 and 奖励参数 <=43 then
                   cj="武器幻色丹"
                elseif 奖励参数 >=44 and 奖励参数 <=60 then
                  cj="星辉石"
                elseif 奖励参数 >=61 and 奖励参数 <=73 then
                  self.宝石名称 = 取随机宝石()
                  self.随机等级 = math.random(2, 9)
                ItemControl:GiveItem(id, self.宝石名称, self.随机等级)
               广播消息(9, "#hd/".."#S/(帮战奖励)".."#g/" .. UserData[id].角色.名称 .. "#y/在帮战竞赛中所向披靡GM奖励#g/" .. self.随机等级 .. "#y/级#y/" .. self.宝石名称 .. "奖励".."#"..math.random(110))
                elseif 奖励参数 >=74 and 奖励参数 <=83 then
                  cj="魔兽要诀"
                elseif 奖励参数 >=84 and 奖励参数 <=90 then
                  cj="九转金丹"
                elseif 奖励参数 >=91 and 奖励参数 <=96 then
                  cj=取召唤兽随机宝石()
                elseif 奖励参数 >=97 and 奖励参数 <=100 then
                  cj="七彩石"
                  elseif 奖励参数 >=101 and 奖励参数 <=110 then
                  local 随机兽决 = 取高级兽诀名称()
              ItemControl:GiveItem(id,"高级魔兽要诀",nil,随机兽决)
              广播消息("#hd/".."#S/(帮战奖励)".."#R/ "..UserData[id].角色.名称.."#y/在帮战竞赛中所向披靡GM奖励#g/"..随机兽决.."#G/高级魔兽要诀".."#"..math.random(110))

              elseif 奖励参数 >=110 and 奖励参数 <=120 then
              ItemControl:GiveItem(id,"召唤兽内丹")
              广播消息("#hd/".."#S/(帮战奖励)".."#g/ "..UserData[id].角色.名称.."#Y/在帮战竞赛中所向披靡GM奖励#g/召唤兽内丹")

               elseif 奖励参数 >=121 and 奖励参数 <=128 then
              local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(14, 15)*10
                  ItemControl:GiveItem(id, 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(帮战奖励)".."#R/" .. UserData[id].角色.名称 .. "#y/在帮战竞赛中所向披靡GM奖励#G/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                   elseif 奖励参数 >=128 and 奖励参数 <=132 then
                  ItemControl:GiveItem(id,"清灵净瓶")
                  广播消息("#hd/".."#S/(帮战奖励))".."#g/ "..UserData[id].角色.名称.."#Y/在帮战竞赛中所向披靡GM奖励#G/清灵净瓶".."#"..math.random(110))
                end


                if cj ~= "" then
                  ItemControl:GiveItem(id,cj)
                  广播消息("#hd/".."#S/(帮战奖励)".."#R/ "..UserData[id].角色.名称.."#Y/在帮战竞赛中所向披靡GM奖励#g/"..cj.."#"..math.random(110))
                end
  end
end
function TaskControl:刷出帮战宝箱()---------------------
  local 刷出数量 = 50

    for i=1, 刷出数量 do
        local 天关id=self:生成任务id()
        local 临时坐标=MapControl:Randomloadtion(1380) 
        任务数据[天关id]={
        类型="帮战宝箱"
        ,id=天关id
        ,起始=os.time()
        ,结束=600
        ,任务id=天关id
        ,地图编号=1380
        ,地图名称=MapData[1380].名称
        ,名称="宝箱"
        ,造型="宝箱"
        ,方向=math.random(4)-1
        ,坐标=临时坐标
        ,数字id=0
        ,战斗=false
        }
        MapControl:添加单位(任务数据[天关id])
    end
 end
function TaskControl:完成帮战怪物任务(id,任务id)-------------
 local 帮派积分 = 5
     local 胜利列表 = 帮派竞赛:取帮战列表编号(UserData[id].角色.帮派)
     帮派竞赛.帮战列表[胜利列表.组号][胜利列表.分类].积分 = 帮派竞赛.帮战列表[胜利列表.组号][胜利列表.分类].积分+ 帮派积分
    if UserData[id].队伍 == 0 then
        帮派数据[UserData[id].角色.帮派].成员名单[id].帮贡.获得=帮派数据[UserData[id].角色.帮派].成员名单[id].帮贡.获得+10
        帮派数据[UserData[id].角色.帮派].成员名单[id].帮贡.当前=帮派数据[UserData[id].角色.帮派].成员名单[id].帮贡.当前+10
        SendMessage(UserData[id].连接id,7,"#Y/你获得了10点帮贡")
        RoleControl:添加活跃度(UserData[id],1)
        UserData[id].角色.比武积分=UserData[id].角色.比武积分+1
        SendMessage(UserData[id].连接id, 7, "#y/你获得了" .. 帮派积分 .. "点帮派积分，当前总积分" .. 帮派竞赛.帮战列表[胜利列表.组号][胜利列表.分类].积分 .. "点")
    else
        for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
                帮派数据[UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[id].队伍].队员数据[n]].帮贡.获得=帮派数据[UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[id].队伍].队员数据[n]].帮贡.获得+10
                帮派数据[UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[id].队伍].队员数据[n]].帮贡.当前=帮派数据[UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[id].队伍].队员数据[n]].帮贡.当前+10
                SendMessage(UserData[队伍数据[UserData[id].队伍].队员数据[n]].连接id,7,"#Y/你获得了10点帮贡")
                RoleControl:添加活跃度(UserData[队伍数据[UserData[id].队伍].队员数据[n]],1)
                UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.比武积分=UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.比武积分+1
                SendMessage(UserData[队伍数据[UserData[id].队伍].队员数据[n]].连接id, 7, "#y/你获得了" .. 帮派积分 .. "点帮派积分，当前总积分" .. 帮派竞赛.帮战列表[胜利列表.组号][胜利列表.分类].积分 .. "点")
        end


  -- local cj = ""
  --       local 奖励参数 = math.random(100)
  --       if 奖励参数 >=1 and 奖励参数 <=20 then
  --         cj= "超修炼果"
  --       elseif 奖励参数 >=21 and 奖励参数 <=31 then
  --           cj="修炼果"
  --       elseif 奖励参数 >=32 and 奖励参数 <=43 then
  --         cj="修炼果"
  --       elseif 奖励参数 >=44 and 奖励参数 <=60 then
  --         cj="修炼果"
  --       elseif 奖励参数 >=61 and 奖励参数 <=73 then
  --         cj="修炼果"
  --       elseif 奖励参数 >=74 and 奖励参数 <=83 then
  --         cj="修炼果"
  --       elseif 奖励参数 >=84 and 奖励参数 <=90 then
  --         cj="修炼果"
  --       elseif 奖励参数 >=91 and 奖励参数 <=96 then
  --         cj=取随机宝石()
  --       elseif 奖励参数 >=97 and 奖励参数 <=100 then
  --         cj="修炼果"
  --       end
  --       if cj ~= "" then
  --         ItemControl:GiveItem(id,cj)
  --         广播消息("#hd/".."#S/(帮战怪物)".."#R/ "..UserData[id].角色.名称.."#Y/成功战胜帮战怪物,获得了#g/"..cj.."#"..math.random(110))
  --       end



  end

 MapControl:移除单位(UserData[id].地图,任务id)
 任务数据[任务id]=nil
 end
