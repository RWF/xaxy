-- @Author: baidwwy
-- @Date:   2020-03-28 01:03:21
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-08-21 23:51:37
function TaskControl:刷出天罡星69(编号)---------------------
  local 刷出地图={1070,1506,1514}
  local 刷新名称 = ""
  for n=1,3 do
  local 天关地图=刷出地图[math.random(1,#刷出地图)]
  local 随机造型={"云游火"}
    for n=1,math.random(3) do
      local 天关id=self:生成任务id()
      local 临时坐标=MapControl:Randomloadtion(天关地图) 
      任务数据[天关id]={
       类型="天罡星69"
     ,id=天关id
     ,起始=os.time()
     ,结束=1800
     ,任务id=天关id
     ,地图编号=天关地图
     ,地图名称=MapData[天关地图].名称
     ,名称="武王"
     ,称谓="武王(65-99)"
     ,造型=随机造型[math.random(1,#随机造型)]
     ,方向=math.random(4)-1
     ,坐标=临时坐标
     ,数字id=0
     ,战斗=false
     ,染色方案=20113
     ,染色组={5,0,0}
     }
     MapControl:添加单位(任务数据[天关id])
    end
    刷新名称 = 刷新名称..""..MapData[天关地图].名称.."."
 end
  广播消息("#hd/#g/蚩尤部落于涿鹿之战中败于黄帝，炎帝的部落联盟。部落族人四散逃入#S"..刷新名称.."#g/请各位侠士赶紧前往帮助化解#51")
 end
function TaskControl:完成天罡星69任务(任务id,id组)-------------
 for n=1,#id组 do
   if UserData[id组[n]]~=nil then
     self.符合抓鬼id=true
     if self.符合抓鬼id then
       self.抓鬼奖励参数=1
       self.抓鬼奖励等级=UserData[id组[n]].角色.等级
       self.抓鬼奖励经验=math.floor(self.抓鬼奖励等级*self.抓鬼奖励等级*120*self.抓鬼奖励参数+5000000)
       RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验,"天罡星69")
       RoleControl:添加银子(UserData[id组[n]],40000,"天罡星69")
       if UserData[id组[n]].召唤兽.数据.参战~=0 then
         self.抓鬼奖励等级=UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
         self.抓鬼奖励经验=math.floor(self.抓鬼奖励等级*self.抓鬼奖励等级*20*0.35+150)
         UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验,id组[n],UserData[id组[n]].召唤兽.数据.参战,2)
        --   UserData[id组[n]].角色.天罡积分=UserData[id组[n]].角色.天罡积分+5
        -- SendMessage(UserData[id组[n]].连接id,7,"#y/你获得了5点天罡积分！")
         local cj = ""
                local 奖励参数 = math.random(130)
                if 奖励参数 >=1 and 奖励参数 <=10 then
                  cj= "神兜兜"
                  elseif 奖励参数 >=11 and 奖励参数 <=21 then --10%
                  cj="修炼果"
                elseif 奖励参数 >=22 and 奖励参数 <=32 then --10%
                    self.宝石名称 = 取随机宝石()
                    self.随机等级 = math.random(6)
                   ItemControl:GiveItem(id组[n], self.宝石名称, self.随机等级)
                  广播消息(9, "#hd/".."#S/(武王)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/成功降服了武王，获得了#小东/" .. self.随机等级 .. "#y/级#y/" .. self.宝石名称 .. "奖励".."#"..math.random(110))
                elseif 奖励参数 >=33 and 奖励参数 <=43 then
                ItemControl:GiveItem(id组[n],"召唤兽内丹")
                广播消息("#hd/".."#S/(武王)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武王，获得了#小东/召唤兽内丹".."#"..math.random(110))
                elseif 奖励参数 >=44 and 奖励参数 <=53 then
                  ItemControl:GiveItem(id组[n],"星辉石",math.random(3,6))
                  广播消息("#hd/".."#S/(武王)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武王，获得了玉皇大帝奖励的#小东/星辉石".."#"..math.random(110))
                elseif 奖励参数 >=54 and 奖励参数 <=63 then
                    local 随机兽决 = 取高级兽诀名称()
                  ItemControl:GiveItem(id组[n],"高级魔兽要诀",nil,随机兽决)
                  广播消息("#hd/".."#S/(武王)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武王，获得了#小东/"..随机兽决.."#G/高级魔兽要诀".."#"..math.random(110))
                elseif 奖励参数 >=64 and 奖励参数 <=74 then
                cj=取召唤兽随机宝石()
                elseif 奖励参数 >=75 and 奖励参数 <=85 then
                  cj="聚气袋"
                elseif 奖励参数 >=86 and 奖励参数 <=88 then
                  cj="光照万象兽决碎片"
                elseif 奖励参数 >=89 and 奖励参数 <=95 then
                   cj="七彩石"

                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(武王)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武王，获得了玉皇大帝奖励的#小东/"..cj.."#"..math.random(110))
                end
         end
       end
     end
   end

 MapControl:移除单位(UserData[id组[1]].地图,任务id)
 任务数据[任务id]=nil
 end

 function TaskControl:刷出天罡星2(编号)---------------------
 local 刷出地图={1070,1506,1514}
  local 刷新名称 = ""
  for n=1,3 do
  local 天关地图=刷出地图[math.random(1,#刷出地图)]
  local 随机造型={"月影仙"}
    for n=1,math.random(3) do
      local 天关id=self:生成任务id()
      local 临时坐标=MapControl:Randomloadtion(天关地图) 
      任务数据[天关id]={
       类型="天罡星2"
     ,id=天关id
     ,起始=os.time()
     ,结束=1800
     ,任务id=天关id
     ,地图编号=天关地图
     ,地图名称=MapData[天关地图].名称
     ,名称="武圣"
     ,称谓="武圣(100-129)"
     ,造型=随机造型[math.random(1,#随机造型)]
     ,方向=math.random(4)-1
     ,坐标=临时坐标
     ,数字id=0
     ,战斗=false
     }
     MapControl:添加单位(任务数据[天关id])
    end
    刷新名称 = 刷新名称..""..MapData[天关地图].名称.."."
 end
 --广播消息("#hd/#g/蚩尤部落于涿鹿之战中败于黄帝，炎帝的部落联盟。部落族人四散流入#S"..刷新名称.."请各位侠士赶紧前往帮助化解")
 end
function TaskControl:完成天罡星2任务(任务id,id组)-------------
 for n=1,#id组 do
   if UserData[id组[n]]~=nil then
     self.符合抓鬼id=true
     if self.符合抓鬼id then
       self.抓鬼奖励参数=1
       self.抓鬼奖励等级=UserData[id组[n]].角色.等级
       self.抓鬼奖励经验=math.floor(self.抓鬼奖励等级*self.抓鬼奖励等级*120*self.抓鬼奖励参数+5000000)
       RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验,"天罡星2")
       RoleControl:添加银子(UserData[id组[n]],40000,"天罡星2")
       if UserData[id组[n]].召唤兽.数据.参战~=0 then
         self.抓鬼奖励等级=UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
         self.抓鬼奖励经验=math.floor(self.抓鬼奖励等级*self.抓鬼奖励等级*20*0.35+150)
         UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验,id组[n],UserData[id组[n]].召唤兽.数据.参战,2)
        --  UserData[id组[n]].角色.天罡积分=UserData[id组[n]].角色.天罡积分+5
        -- SendMessage(UserData[id组[n]].连接id,7,"#y/你获得了5点天罡积分！")
        local cj = ""
                local 奖励参数 = math.random(130)
                if 奖励参数 >=1 and 奖励参数 <=10 then
                  cj= "神兜兜"
                  elseif 奖励参数 >=11 and 奖励参数 <=21 then --10%
                  cj="修炼果"
                elseif 奖励参数 >=22 and 奖励参数 <=32 then --10%
                    self.宝石名称 = 取随机宝石()
                    self.随机等级 = math.random(9)
                   ItemControl:GiveItem(id组[n], self.宝石名称, self.随机等级)
                  广播消息(9, "#hd/".."#S/(武圣)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/成功降服了武圣，获得了#小东/" .. self.随机等级 .. "#y/级#y/" .. self.宝石名称 .. "奖励".."#"..math.random(110))
                elseif 奖励参数 >=33 and 奖励参数 <=43 then
                ItemControl:GiveItem(id组[n],"高级召唤兽内丹")
                广播消息("#hd/".."#S/(武圣)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武圣，获得了#小东/高级召唤兽内丹".."#"..math.random(110))
                elseif 奖励参数 >=44 and 奖励参数 <=53 then
                  ItemControl:GiveItem(id组[n],"星辉石",math.random(3,6))
                  广播消息("#hd/".."#S/(武圣)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武圣，获得了玉皇大帝奖励的#小东/星辉石".."#"..math.random(110))
                elseif 奖励参数 >=54 and 奖励参数 <=63 then
                    local 随机兽决 = 取高级兽诀名称()
                  ItemControl:GiveItem(id组[n],"高级魔兽要诀",nil,随机兽决)
                  广播消息("#hd/".."#S/(武圣)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武圣，获得了#小东/"..随机兽决.."#G/高级魔兽要诀".."#"..math.random(110))
                elseif 奖励参数 >=64 and 奖励参数 <=74 then
                cj=取召唤兽随机宝石()
                elseif 奖励参数 >=75 and 奖励参数 <=85 then
                  cj="聚气袋"
                elseif 奖励参数 >=86 and 奖励参数 <=88 then
                  cj="光照万象兽决碎片"
                elseif 奖励参数 >=89 and 奖励参数 <=95 then
                   cj="七彩石"

                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(武圣)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武圣，获得了玉皇大帝奖励的#小东/"..cj.."#"..math.random(110))
                end
         end
       end
     end
   end

 MapControl:移除单位(UserData[id组[1]].地图,任务id)
 任务数据[任务id]=nil
 end



 function TaskControl:刷出天罡星3(编号)---------------------
 local 刷出地图={1070,1506,1514}
  local 刷新名称 = ""
  for n=1,3 do
  local 天关地图=刷出地图[math.random(1,#刷出地图)]
  local 随机造型={"狐不归"}
    for n=1,math.random(3) do
      local 天关id=self:生成任务id()
      local 临时坐标=MapControl:Randomloadtion(天关地图) 
      任务数据[天关id]={
       类型="天罡星3"
     ,id=天关id
     ,起始=os.time()
     ,结束=1800
     ,任务id=天关id
     ,地图编号=天关地图
     ,地图名称=MapData[天关地图].名称
     ,名称="武仙"
     ,称谓="武仙(130-155)"
     ,造型=随机造型[math.random(1,#随机造型)]
     ,方向=math.random(4)-1
     ,坐标=临时坐标
     ,数字id=0
     ,战斗=false
     }
     MapControl:添加单位(任务数据[天关id])
    end
    刷新名称 = 刷新名称..""..MapData[天关地图].名称.."."
 end
 --广播消息("#hd/#g/蚩尤部落于涿鹿之战中败于黄帝，炎帝的部落联盟。部落族人四散流入#S"..刷新名称.."请各位侠士赶紧前往帮助化解")
 end
function TaskControl:完成天罡星3任务(任务id,id组)-------------
 for n=1,#id组 do
   if UserData[id组[n]]~=nil then
     self.符合抓鬼id=true
     if self.符合抓鬼id then
       self.抓鬼奖励参数=1
       self.抓鬼奖励等级=UserData[id组[n]].角色.等级
       self.抓鬼奖励经验=math.floor(self.抓鬼奖励等级*self.抓鬼奖励等级*120*self.抓鬼奖励参数+5000000)
       RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验,"天罡星3")
       RoleControl:添加银子(UserData[id组[n]],40000,"天罡星3")
       if UserData[id组[n]].召唤兽.数据.参战~=0 then
         self.抓鬼奖励等级=UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
         self.抓鬼奖励经验=math.floor(self.抓鬼奖励等级*self.抓鬼奖励等级*20*0.35+150)
         UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验,id组[n],UserData[id组[n]].召唤兽.数据.参战,2)
        --  UserData[id组[n]].角色.天罡积分=UserData[id组[n]].角色.天罡积分+5
        -- SendMessage(UserData[id组[n]].连接id,7,"#y/你获得了5点天罡积分！")
           local cj = ""
                local 奖励参数 = math.random(130)
                if 奖励参数 >=1 and 奖励参数 <=10 then
                  cj= "神兜兜"
                  elseif 奖励参数 >=11 and 奖励参数 <=21 then --10%
                  cj="修炼果"
                elseif 奖励参数 >=22 and 奖励参数 <=32 then --10%
                    self.宝石名称 = 取随机宝石()
                    self.随机等级 = math.random(9)
                   ItemControl:GiveItem(id组[n], self.宝石名称, self.随机等级)
                  广播消息(9, "#hd/".."#S/(武圣)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/成功降服了武仙，获得了#小东/" .. self.随机等级 .. "#y/级#y/" .. self.宝石名称 .. "奖励".."#"..math.random(110))
                elseif 奖励参数 >=33 and 奖励参数 <=43 then
                ItemControl:GiveItem(id组[n],"高级召唤兽内丹")
                广播消息("#hd/".."#S/(武仙)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武仙，获得了#小东/高级召唤兽内丹".."#"..math.random(110))
                elseif 奖励参数 >=44 and 奖励参数 <=53 then
                  ItemControl:GiveItem(id组[n],"星辉石",math.random(3,6))
                  广播消息("#hd/".."#S/(武仙)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武仙，获得了玉皇大帝奖励的#小东/星辉石".."#"..math.random(110))
                elseif 奖励参数 >=54 and 奖励参数 <=63 then
                    local 随机兽决 = 取高级兽诀名称()
                  ItemControl:GiveItem(id组[n],"高级魔兽要诀",nil,随机兽决)
                  广播消息("#hd/".."#S/(武仙)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武仙，获得了#小东/"..随机兽决.."#G/高级魔兽要诀".."#"..math.random(110))
                elseif 奖励参数 >=64 and 奖励参数 <=74 then
                cj=取召唤兽随机宝石()
                elseif 奖励参数 >=75 and 奖励参数 <=85 then
                  cj="聚气袋"
                elseif 奖励参数 >=86 and 奖励参数 <=88 then
                  cj="光照万象兽决碎片"
                elseif 奖励参数 >=89 and 奖励参数 <=95 then
                   cj="七彩石"

                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(武仙)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武仙，获得了玉皇大帝奖励的#小东/"..cj.."#"..math.random(110))
                end
         end
       end
     end
   end

 MapControl:移除单位(UserData[id组[1]].地图,任务id)
 任务数据[任务id]=nil
 end

 function TaskControl:刷出天罡星4(编号)---------------------
  local 刷出地图={1070,1506,1514}
  local 刷新名称 = ""
  for n=1,3 do
  local 天关地图=刷出地图[math.random(1,#刷出地图)]
  local 随机造型={"谛听"}
    for n=1,math.random(3) do
      local 天关id=self:生成任务id()
      local 临时坐标=MapControl:Randomloadtion(天关地图) 
      任务数据[天关id]={
       类型="天罡星4"
     ,id=天关id
     ,起始=os.time()
     ,结束=1800
     ,任务id=天关id
     ,地图编号=天关地图
     ,地图名称=MapData[天关地图].名称
     ,名称="武神"
     ,称谓="飞升(129-155)"
     ,造型=随机造型[math.random(1,#随机造型)]
     ,方向=math.random(4)-1
     ,坐标=临时坐标
     ,数字id=0
     ,战斗=false
     }
     MapControl:添加单位(任务数据[天关id])
    end
    刷新名称 = 刷新名称..""..MapData[天关地图].名称.."."
 end
  --广播消息("#hd/#g/蚩尤部落于涿鹿之战中败于黄帝，炎帝的部落联盟。部落族人四散流入#S"..刷新名称.."请各位侠士赶紧前往帮助化解")
 end
function TaskControl:完成天罡星4任务(任务id,id组)-------------
 for n=1,#id组 do
   if UserData[id组[n]]~=nil then
     self.符合抓鬼id=true
     if self.符合抓鬼id then
       self.抓鬼奖励参数=1
       self.抓鬼奖励等级=UserData[id组[n]].角色.等级
       self.抓鬼奖励经验=math.floor(self.抓鬼奖励等级*self.抓鬼奖励等级*120*self.抓鬼奖励参数+5000000)
       RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验,"天罡星4")
       RoleControl:添加银子(UserData[id组[n]],40000,"天罡星4")
       if UserData[id组[n]].召唤兽.数据.参战~=0 then
         self.抓鬼奖励等级=UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
         self.抓鬼奖励经验=math.floor(self.抓鬼奖励等级*self.抓鬼奖励等级*20*0.35+150)
         UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验,id组[n],UserData[id组[n]].召唤兽.数据.参战,2)
        --   UserData[id组[n]].角色.天罡积分=UserData[id组[n]].角色.天罡积分+5
        -- SendMessage(UserData[id组[n]].连接id,7,"#y/你获得了5点天罡积分！")
          local cj = ""
                local 奖励参数 = math.random(130)
                if 奖励参数 >=1 and 奖励参数 <=10 then
                  cj= "神兜兜"
                elseif 奖励参数 >=11 and 奖励参数 <=21 then
                  ItemControl:GiveItem(id组[n],"清灵净瓶")
                  广播消息("#hd/".."#S/(武神)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武神，获得了玉皇大帝奖励的#小东/清灵净瓶".."#"..math.random(110))
                elseif 奖励参数 >=22 and 奖励参数 <=32 then
                    cj="七彩石"
                elseif 奖励参数 >=33 and 奖励参数 <=43 then
                     cj="修炼果"
                elseif 奖励参数 >=44 and 奖励参数 <=54 then
                  local 随机兽决 = 取高级兽诀名称()
                  ItemControl:GiveItem(id组[n],"高级魔兽要诀",nil,随机兽决)
                  广播消息("#hd/".."#S/(武神)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武神，获得了玉皇大帝奖励的#小东/"..随机兽决.."#G/高级魔兽要诀".."#"..math.random(110))
                elseif 奖励参数 >=55 and 奖励参数 <=65 then
                  ItemControl:GiveItem(id组[n],"星辉石",math.random(6,8))
                  广播消息("#hd/".."#S/(武神)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武神，获得了玉皇大帝奖励的#小东/星辉石".."#"..math.random(110))
                elseif 奖励参数 >=66 and 奖励参数 <=76 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(14, 15)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(武神)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/成功降服了武神，获得了玉皇大帝奖励的#小东/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                 elseif 奖励参数 >=77 and 奖励参数 <=87 then
                  self.强化石名称 = 取随机强化石()
               ItemControl:GiveItem(id组[n],self.强化石名称)
              SendMessage(UserData[id组[n]].连接id, 7, "#y/你获得了" .. self.强化石名称)
               广播消息("#hd/".."#S/(武神)".."#g/ " .. UserData[id组[n]].角色.名称 .. "#y/成功降服了武神获得了玉皇大帝奖励的#小东/" .. self.强化石名称.."#"..math.random(110))
                 elseif 奖励参数 >=88 and 奖励参数 <=98 then
                 ItemControl:GiveItem(id组[n],"附魔宝珠")
                广播消息("#hd/".."#S/(武神)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武神，获得了玉皇大帝奖励的#g/附魔宝珠")
                 elseif 奖励参数 >=99 and 奖励参数 <=109 then
                  self.宝石名称 = 取随机宝石()
                  self.随机等级 = math.random(10)
                   ItemControl:GiveItem(id组[n], self.宝石名称, self.随机等级)
                  广播消息(9, "#hd/".."#S/(武神)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/成功降服了武神，获得了#小东/" .. self.随机等级 .. "#y/级#y/" .. self.宝石名称 .. "奖励".."#"..math.random(110))
                 elseif 奖励参数 >=110 and 奖励参数 <=118 then
                  ItemControl:GiveItem(id组[n],"高级召唤兽内丹")
                广播消息("#hd/".."#S/(武神)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武神，获得了#小东/高级召唤兽内丹".."#"..math.random(110))

                 elseif 奖励参数 >=118 and 奖励参数 <=120 then
                  cj="光照万象兽决碎片"
                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(武神)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武神，获得了玉皇大帝奖励的#小东/"..cj.."#"..math.random(110))
                end
         end
       end
     end
   end

 MapControl:移除单位(UserData[id组[1]].地图,任务id)
 任务数据[任务id]=nil
 end

 function TaskControl:刷出天罡星5(编号)---------------------
  local 刷出地图={1070,1506,1514}
  local 刷新名称 = ""
  for n=1,3 do
  local 天关地图=刷出地图[math.random(1,#刷出地图)]
  local 随机造型={"暗黑童子"}
    for n=1,math.random(2) do
      local 天关id=self:生成任务id()
      local 临时坐标=MapControl:Randomloadtion(天关地图) 
      任务数据[天关id]={
       类型="天罡星5"
     ,id=天关id
     ,起始=os.time()
     ,结束=1800
     ,任务id=天关id
     ,地图编号=天关地图
     ,地图名称=MapData[天关地图].名称
     ,名称="武尊"
     ,称谓="武尊(156-175)"
     ,造型=随机造型[math.random(1,#随机造型)]
     ,方向=math.random(4)-1
     ,坐标=临时坐标
     ,数字id=0
     ,战斗=false
     }
     MapControl:添加单位(任务数据[天关id])
    end
    刷新名称 = 刷新名称..""..MapData[天关地图].名称.."."
 end
  --广播消息("#hd/#g/蚩尤部落于涿鹿之战中败于黄帝，炎帝的部落联盟。部落族人四散流入#S"..刷新名称.."请各位侠士赶紧前往帮助化解")
 end
function TaskControl:完成天罡星5任务(任务id,id组)-------------
 for n=1,#id组 do
   if UserData[id组[n]]~=nil then
     self.符合抓鬼id=true
     if self.符合抓鬼id then
       self.抓鬼奖励参数=1
       self.抓鬼奖励等级=UserData[id组[n]].角色.等级
       self.抓鬼奖励经验=math.floor(self.抓鬼奖励等级*self.抓鬼奖励等级*120*self.抓鬼奖励参数+5000000)
       RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验,"天罡星5")
       RoleControl:添加银子(UserData[id组[n]],40000,"天罡星5")
       if UserData[id组[n]].召唤兽.数据.参战~=0 then
         self.抓鬼奖励等级=UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
         self.抓鬼奖励经验=math.floor(self.抓鬼奖励等级*self.抓鬼奖励等级*20*0.35+150)
         UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验,id组[n],UserData[id组[n]].召唤兽.数据.参战,2)
        --   UserData[id组[n]].角色.天罡积分=UserData[id组[n]].角色.天罡积分+5
        -- SendMessage(UserData[id组[n]].连接id,7,"#y/你获得了5点天罡积分！")
         local cj = ""
                local 奖励参数 = math.random(130)
                if 奖励参数 >=1 and 奖励参数 <=10 then
                  cj= "神兜兜"
                elseif 奖励参数 >=11 and 奖励参数 <=21 then
                  ItemControl:GiveItem(id组[n],"清灵净瓶")
                  广播消息("#hd/".."#S/(武尊)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武尊，获得了玉皇大帝奖励的#小东/清灵净瓶".."#"..math.random(110))
                elseif 奖励参数 >=22 and 奖励参数 <=32 then
                    cj="七彩石"
                elseif 奖励参数 >=33 and 奖励参数 <=43 then
                     cj="修炼果"
                elseif 奖励参数 >=44 and 奖励参数 <=54 then
                  local 随机兽决 = 取高级兽诀名称()
                  ItemControl:GiveItem(id组[n],"高级魔兽要诀",nil,随机兽决)
                  广播消息("#hd/".."#S/(武尊)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武尊，获得了玉皇大帝奖励的#小东/"..随机兽决.."#G/高级魔兽要诀".."#"..math.random(110))
                elseif 奖励参数 >=55 and 奖励参数 <=65 then
                  ItemControl:GiveItem(id组[n],"星辉石",math.random(6,8))
                  广播消息("#hd/".."#S/(武尊)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武尊，获得了玉皇大帝奖励的#小东/星辉石".."#"..math.random(110))
                elseif 奖励参数 >=66 and 奖励参数 <=76 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(12, 15)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(武尊)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/成功降服了武尊，获得了玉皇大帝奖励的#小东/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=77 and 奖励参数 <=87 then
                  ItemControl:GiveItem(id组[n],"高级召唤兽内丹")
                广播消息("#hd/".."#S/(武尊)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武尊，获得了#小东/高级召唤兽内丹".."#"..math.random(110))
                elseif 奖励参数 >=88 and 奖励参数 <=98 then
                     self.宝石名称 = 取随机宝石()
                    self.随机等级 = math.random(10)
                   ItemControl:GiveItem(id组[n], self.宝石名称, self.随机等级)
                  广播消息(9, "#hd/".."#S/(武尊)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/成功降服了武尊，获得了#小东/" .. self.随机等级 .. "#y/级#y/" .. self.宝石名称 .. "奖励".."#"..math.random(110))
                  elseif 奖励参数 >=99 and 奖励参数 <=109 then
                  self.强化石名称 = 取随机强化石()
            ItemControl:GiveItem(id组[n],self.强化石名称)
            SendMessage(UserData[id组[n]].连接id, 7, "#y/你获得了" .. self.强化石名称)
            广播消息("#hd/".."#S/(武尊)".."#g/ " .. UserData[id组[n]].角色.名称 .. "#y/成功降服了武尊获得了玉皇大帝奖励的#小东/" .. self.强化石名称.."#"..math.random(110))
               
               elseif 奖励参数 >=110 and 奖励参数 <=112 then
                  cj="光照万象兽决碎片"
                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(武尊)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武尊，获得了玉皇大帝奖励的#小东/"..cj.."#"..math.random(110))
                end
         end
       end
     end
   end

 MapControl:移除单位(UserData[id组[1]].地图,任务id)
 任务数据[任务id]=nil
 end

 function TaskControl:刷出天罡星6(编号)---------------------
  local 刷出地图={1070,1506,1514}
  local 刷新名称 = ""
  for n=1,3 do
  local 天关地图=刷出地图[math.random(1,#刷出地图)]
  local 随机造型={"无常鬼差白"}
    for n=1,math.random(2) do
      local 天关id=self:生成任务id()
      local 临时坐标=MapControl:Randomloadtion(天关地图) 
      任务数据[天关id]={
       类型="天罡星6"
     ,id=天关id
     ,起始=os.time()
     ,结束=1800
     ,任务id=天关id
     ,地图编号=天关地图
     ,地图名称=MapData[天关地图].名称
     ,名称="武帝"
     ,称谓="武帝(170-175)"
     ,造型=随机造型[math.random(1,#随机造型)]
     ,方向=math.random(4)-1
     ,坐标=临时坐标
     ,数字id=0
     ,战斗=false
     }
     MapControl:添加单位(任务数据[天关id])
    end
    刷新名称 = 刷新名称..""..MapData[天关地图].名称.."."
 end
  --广播消息("#hd/#g/蚩尤部落于涿鹿之战中败于黄帝，炎帝的部落联盟。部落族人四散流入#S"..刷新名称.."请各位侠士赶紧前往帮助化解")
 end
function TaskControl:完成天罡星6任务(任务id,id组)-------------
 for n=1,#id组 do
   if UserData[id组[n]]~=nil then
     self.符合抓鬼id=true
     if self.符合抓鬼id then
       self.抓鬼奖励参数=1
       self.抓鬼奖励等级=UserData[id组[n]].角色.等级
       self.抓鬼奖励经验=math.floor(self.抓鬼奖励等级*self.抓鬼奖励等级*120*self.抓鬼奖励参数+5000000)
       RoleControl:添加经验(UserData[id组[n]],self.抓鬼奖励经验,"天罡星6")
       RoleControl:添加银子(UserData[id组[n]],40000,"天罡星6")
       if UserData[id组[n]].召唤兽.数据.参战~=0 then
         self.抓鬼奖励等级=UserData[id组[n]].召唤兽.数据[UserData[id组[n]].召唤兽.数据.参战].等级
         self.抓鬼奖励经验=math.floor(self.抓鬼奖励等级*self.抓鬼奖励等级*50*0.35+5000000)
         UserData[id组[n]].召唤兽:添加经验(self.抓鬼奖励经验,id组[n],UserData[id组[n]].召唤兽.数据.参战,2)
        --   UserData[id组[n]].角色.天罡积分=UserData[id组[n]].角色.天罡积分+5
        -- SendMessage(UserData[id组[n]].连接id,7,"#y/你获得了5点天罡积分！")
         local cj = ""
                local 奖励参数 = math.random(130)
                if 奖励参数 >=1 and 奖励参数 <=10 then
                  cj= "神兜兜"
                elseif 奖励参数 >=11 and 奖励参数 <=21 then
                  ItemControl:GiveItem(id组[n],"清灵净瓶")
                  广播消息("#hd/".."#S/(武帝)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武帝，获得了玉皇大帝奖励的#小东/清灵净瓶".."#"..math.random(110))
                elseif 奖励参数 >=22 and 奖励参数 <=32 then
                    cj="七彩石"
                elseif 奖励参数 >=33 and 奖励参数 <=43 then
                     cj="修炼果"
                elseif 奖励参数 >=44 and 奖励参数 <=54 then
                  local 随机兽决 = 取高级兽诀名称()
                  ItemControl:GiveItem(id组[n],"高级魔兽要诀",nil,随机兽决)
                  广播消息("#hd/".."#S/(武帝)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武帝，获得了玉皇大帝奖励的#小东/"..随机兽决.."#G/高级魔兽要诀".."#"..math.random(110))
                elseif 奖励参数 >=55 and 奖励参数 <=65 then
                  ItemControl:GiveItem(id组[n],"星辉石",math.random(6,8))
                  广播消息("#hd/".."#S/(武帝)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武帝，获得了玉皇大帝奖励的#小东/星辉石".."#"..math.random(110))
                elseif 奖励参数 >=66 and 奖励参数 <=76 then
                   local 随机名称 = 取随机书铁()
                   local 随机等级 = math.random(12, 15)*10
                  ItemControl:GiveItem(id组[n], 随机名称, 随机等级)
                 广播消息("#hd/".."#S/(武帝)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/成功降服了武帝，获得了玉皇大帝奖励的#小东/" .. 随机等级 .. "#y/级#r/" .. 随机名称.."#"..math.random(110))
                elseif 奖励参数 >=77 and 奖励参数 <=87 then
                     cj="玲珑宝图"
                elseif 奖励参数 >=88 and 奖励参数 <=98 then
                     self.宝石名称 = 取随机宝石()
                    self.随机等级 = math.random(10)
                   ItemControl:GiveItem(id组[n], self.宝石名称, self.随机等级)
                  广播消息(9, "#hd/".."#S/(武帝)".."#g/" .. UserData[id组[n]].角色.名称 .. "#y/成功降服了武帝，获得了#小东/" .. self.随机等级 .. "#y/级#y/" .. self.宝石名称 .. "奖励".."#"..math.random(110))
                elseif 奖励参数 >=99 and 奖励参数 <=109 then
                 self.强化石名称 = 取随机强化石()
            ItemControl:GiveItem(id组[n],self.强化石名称)
            SendMessage(UserData[id组[n]].连接id, 7, "#y/你获得了" .. self.强化石名称)
             广播消息("#hd/".."#S/(武帝)".."#g/ " .. UserData[id组[n]].角色.名称 .. "#y/成功降服了武帝获得了玉皇大帝奖励的#小东/" .. self.强化石名称.."#"..math.random(110))
                
                elseif 奖励参数 >=110 and 奖励参数 <=112 then
                  cj="光照万象兽决碎片"

                end
                if cj ~= "" then
                  ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(武帝)".."#g/ "..UserData[id组[n]].角色.名称.."#Y/成功降服了武帝，获得了玉皇大帝奖励的#小东/"..cj.."#"..math.random(110))
                end
         end
       end
     end
   end

 MapControl:移除单位(UserData[id组[1]].地图,任务id)
 任务数据[任务id]=nil
 end