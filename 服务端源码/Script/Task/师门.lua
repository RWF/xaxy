function TaskControl:设置师门任务(id,num)
	UserData[id].角色.师门环数 = UserData[id].角色.师门环数 + 1
	local  lsid=tonumber(UserData[id].id .. "16")
	self.任务参数16 = math.random(2,5)
	if self.任务参数16 ==1 then
		self.师门参数 = math.random( #Q_人物随机)


		self.师门地图,self.师门人物 =取任务人物和地图()
		local lsid =tonumber(UserData[id].id.."16")
		任务数据[lsid] = {
			编号 = 0,
			分类 = 1,
			类型 = "师门",
			结束 = 9999999,
			id = id,
			NPC=num,
			起始 = os.time(),
			任务id = lsid,
			数字id = UserData[id].id,
			人物 = self.师门人物
		}
		self.对话内容 = " #W/ 请帮我将这封书信送给#Y/" .. self.师门地图 .. "#W/的#Y/" .. self.师门人物 .. "#W/。"
	elseif self.任务参数16 ==2 then
		local 召唤兽随机={
				{"大海龟","巨蛙","海毛虫","树怪","野猪","大蝙蝠","赌徒","山贼","强盗"},
                {"龟丞相","牛头","马面","黑熊精","僵尸","雷鸟人","蝴蝶仙子","古代瑞兽"}}
				local sj =0
				if  UserData[id].角色.等级  < 69  then
                        sj = 1
                else
			    	 sj =2
				end
				local xq= 召唤兽随机[sj][math.random( #召唤兽随机[sj])]
		任务数据[lsid] = {
			编号 = 0,
			分类 = 2,
			NPC=num,
			类型 = "师门",
			结束 = 9999999,
			id = id,
			起始 = os.time(),
			任务id = lsid,
			数字id = UserData[id].id,
			召唤兽 = xq
		}
		self.对话内容 = "最近师门太平静，都找不到惹事妖怪刷贡献了，为师很不爽，徒儿随便找个理由去帮我抓1只"..xq.."回来"
	elseif self.任务参数16 ==3 then
		local b =""
				if math.random(10) <= 2 then
						local 三药 = {"金创药","佛光舍利子"}
						 b = 三药[math.random(1,#三药)]
				else
						local 二药 = {"天不老","紫石英","鹿茸","血色茶花","六道轮回","熊胆","凤凰尾","硫磺草","龙之心屑","火凤之睛","丁香水","月星子","仙狐涎","地狱灵芝","麝香","血珊瑚","餐风饮露","白露为霜","天龙水","孔雀红"}
						 b = 二药[math.random(1,#二药)]
				end
		任务数据[lsid] = {
			编号 = 0,
			分类 = 3,
			NPC=num,
			类型 = "师门",
			结束 = 9999999,
			id = id,
			起始 = os.time(),
			任务id = lsid,
			数字id = UserData[id].id,
			道具 = b
		}
		self.对话内容 = "府上缺少药品#R/"..b.."#W/，你去帮为师买一个来吧。记得，要快去快回。"
    elseif self.任务参数16 ==4 then
		任务数据[lsid] = {
			编号 = 0,
			分类 = 4,
			NPC=num,
			类型 = "师门",
			次数 = 2,
			结束 = 9999999,
			id = id,
			起始 = os.time(),
			任务id = lsid,
			数字id = UserData[id].id,
			战斗 = math.random(10, 20)
		}
		self.对话内容 = " 最近常有妖魔鬼怪在本门内捣乱，还请你在这四周巡逻一番。"
	elseif self.任务参数16 ==5 then

	       local b = math.random(2)*10
		 local 武器 = 取随机物品(b)
		   任务数据[lsid] = {
			编号 = 0,
			分类 = 5,
			NPC=num,
			类型 = "师门",
			结束 = 9999999,
			id = id,
			起始 = os.time(),
			任务id = lsid,
			数字id = UserData[id].id,
			道具 = 武器,
			等级 = b
		}
		self.对话内容 ="府上缺少#R/"..武器.."#W/，你去帮为师买一个来吧。记得，要快去快回。"
	end

	UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])] = lsid
	SendMessage(UserData[id].连接id, 20, {NpcData[num].名称,NpcData[num].名称,self.对话内容})
	  self:刷新追踪任务信息(id)
 end
function TaskControl:完成师门任务(id)
	if RoleControl:GetTaskID(UserData[id],"师门") == 0 or 任务数据[RoleControl:GetTaskID(UserData[id],"师门")] == nil then
		return 0
	end
	RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"师门"))
	任务数据[RoleControl:GetTaskID(UserData[id],"师门")] = nil
	self.师门次数 = UserData[id].角色.师门环数
	if self.师门次数 >= 11 then
		return 0
	end
	self.师门id = RoleControl:GetTaskID(UserData[id],"师门")
	self.师门经验 = math.floor(UserData[id].角色.等级 * UserData[id].角色.等级 * 35 * (1 + self.师门次数 * 0.5))

	RoleControl:添加经验(UserData[id],self.师门经验,"师门")
    RoleControl:添加活跃度(UserData[id],1)
    RoleControl:添加单人积分(UserData[id],1)
	self.师门银子 = math.floor(UserData[id].角色.等级 * 110 * (1 + self.师门次数 * 0.85))
	RoleControl:添加银子(UserData[id],self.师门银子, "师门")
	if UserData[id].召唤兽.数据.参战 ~= 0 then
		self.师门奖励等级 = UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].等级
		self.师门奖励经验 = math.floor(self.师门经验 * 0.35 + 450)
		UserData[id].召唤兽:添加经验(self.师门奖励经验, id, UserData[id].召唤兽.数据.参战, 2)
	end
	if self.师门次数 == 7 then
		UserData[id].角色.门贡 = UserData[id].角色.门贡 + 1
		SendMessage(UserData[id].连接id, 7, "#y/你获得了1点门派贡献度")
		if math.random(100) <= 20 then

		end
	elseif self.师门次数 == 8 then
		UserData[id].角色.门贡 = UserData[id].角色.门贡 + 2
		SendMessage(UserData[id].连接id, 7, "#y/你获得了2点门派贡献度")
	elseif self.师门次数 == 9 then
		UserData[id].角色.门贡 = UserData[id].角色.门贡 + 3
		SendMessage(UserData[id].连接id, 7, "#y/你获得了3点门派贡献度")
	elseif self.师门次数 == 10 then
		UserData[id].角色.门贡 = UserData[id].角色.门贡 + 3
		SendMessage(UserData[id].连接id, 7, "#y/你获得了3点门派贡献度")
		UserData[id].角色.师门环数 = 0
		local 奖励参数 = math.random(100)
        if 奖励参数 <= 10 then
            ItemControl:GiveItem(id, "符石卷轴")
         elseif 奖励参数 <= 20 then
            ItemControl:GiveItem(id, "一级未激活符石")
		elseif 奖励参数 <= 30 then
			 ItemControl:GiveItem(id,"上古锻造图策",math.floor(UserData[id].角色.等级/10)*10+5)
		elseif 奖励参数 <= 40 then
			 ItemControl:GiveItem(id,"炼妖石",math.floor(UserData[id].角色.等级/10)*10+5)
		elseif 奖励参数 <= 50 then
			ItemControl:GiveItem(id, "藏宝图")
		end
	end
 end