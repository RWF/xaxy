-- @Author: baidwwy
-- @Date:   2020-02-03 03:54:05
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-25 23:53:54
function TaskControl:设置封妖任务(id, 数量)
    local 封妖编号 = math.random( #封妖地点)
    local 封妖数量 = math.random(3, 6)

    if 数量 ~= nil then
        封妖数量 = 1
    end

    for n = 1, 封妖数量 do
        self.临时id2 = self:生成任务id()
        self.临时造型2 = 封妖地点[封妖编号].怪物[math.random( #封妖地点[封妖编号].怪物)]
        self.地图编号1 = 封妖地点[封妖编号].地图
        self.临时坐标1 =  MapControl:Randomloadtion(self.地图编号1) 
        self.地图名称1 = MapData[self.地图编号1].名称
        任务数据[self.临时id2] = {
            类型 = "封妖",
            结束 = 3600,
            战斗 = false,
            id = id,
            起始 = os.time(),
            编号 = self.临时造型2,
            任务id = self.临时id2,
            地图编号 = self.地图编号1,
            地图名称 = self.地图名称1,
            名称 = "远古" .. self.临时造型2,
            造型 = self.临时造型2,
            方向 = math.random( 4) - 1,
            坐标 = self.临时坐标1,
            数字id = UserData[id].id,
            等级 = 封妖地点[封妖编号].等级
        }

        MapControl:添加单位(任务数据[self.临时id2])
    end
        广播消息("#xt/#y/ " .. UserData[id].角色.名称 .. "#w/在挖宝时不慎挖塌了妖怪家的房子，现在一群无家可归的妖怪正在#y/" .. self.地图名称1 .. "#w/寻衅闹事，各路英雄赶快前往平乱啊！")

end
function TaskControl:触发封妖任务(id, 标识)
    if MapControl.MapData[UserData[id].地图].单位组[标识] == nil then
        return 0
    end

    self.任务id4 = MapControl.MapData[UserData[id].地图].单位组[标识].任务id

    if 任务数据[self.任务id4] == nil then
        SendMessage(UserData[id].连接id, 7, "#y/任务数据异常，代号9001")
    elseif 任务数据[self.任务id4].战斗 then
        SendMessage(UserData[id].连接id, 7, "#y/该npc已经在战斗中了")
    elseif UserData[id].队伍 == 0 then
        SendMessage(UserData[id].连接id, 7, "#y/请与其他玩家组队前来挑战")
    elseif #队伍数据[UserData[id].队伍].队员数据 < 1 and DebugMode == false then
        SendMessage(UserData[id].连接id, 7, "#y/队伍人数未达到3人，无法发起挑战")
    elseif 取队伍符合等级(id, 任务数据[self.任务id4].等级) == false then
        广播队伍消息(id, 7, "#y/挑战本妖怪需要队伍所有成员等级达到" .. 任务数据[self.任务id4].等级 .. "级")
    else
        任务数据[self.任务id4].战斗 = true

        FightGet:进入处理(id, 100005, "66", self.任务id4)
    end
end
function TaskControl:完成封妖任务(任务id, id组)
    self.临时经验4 = math.floor(任务数据[任务id].等级 * 任务数据[任务id].等级 * 50) * 2
    --self.临时金钱4 = math.floor(任务数据[任务id].等级 * 任务数据[任务id].等级 * 6 * 2.5)
    for n = 1, #id组 do
      --RoleControl:添加银子(UserData[id组[n]],self.临时金钱4,"封妖")
        RoleControl:添加经验(UserData[id组[n]],self.临时经验4,"封妖")
               local cj = ""
               local 奖励参数 = math.random(130)
              if 奖励参数<=5 then
                 ItemControl:GiveItem(id组[n],"炼妖石",75)
                广播消息("#hd/".."#S/(梦魇夜叉)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了梦魇夜叉，获得了玉皇大帝奖励的#g/炼妖石")
              elseif 奖励参数<=10 then
                ItemControl:GiveItem(id组[n],"炼妖石",145)
                广播消息("#hd/".."#S/(梦魇夜叉)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了梦魇夜叉，获得了玉皇大帝奖励的#g/炼妖石")
              elseif 奖励参数<=15 then
               ItemControl:GiveItem(id组[n],"炼妖石",135)
                广播消息("#hd/".."#S/(梦魇夜叉)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了梦魇夜叉，获得了玉皇大帝奖励的#g/炼妖石")
              elseif 奖励参数<=20 then
                 ItemControl:GiveItem(id组[n],"炼妖石",125)
                广播消息("#hd/".."#S/(梦魇夜叉)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了梦魇夜叉，获得了玉皇大帝奖励的#g/炼妖石")
              elseif 奖励参数<=30 then
                  ItemControl:GiveItem(id组[n],"炼妖石",115)
                广播消息("#hd/".."#S/(梦魇夜叉)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了梦魇夜叉，获得了玉皇大帝奖励的#g/炼妖石")
              elseif 奖励参数<=40 then
               ItemControl:GiveItem(id组[n],"炼妖石",135)
                广播消息("#hd/".."#S/(梦魇夜叉)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了梦魇夜叉，获得了玉皇大帝奖励的#g/炼妖石")
              elseif 奖励参数<=50 then
                ItemControl:GiveItem(id组[n],"上古锻造图策",125)
                广播消息("#hd/".."#S/(梦魇夜叉)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了梦魇夜叉，获得了玉皇大帝奖励的#g/上古锻造图策")
              elseif 奖励参数<=60 then
               ItemControl:GiveItem(id组[n],"上古锻造图策",135)
               广播消息("#hd/".."#S/(梦魇夜叉)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了梦魇夜叉，获得了玉皇大帝奖励的#g/上古锻造图策")
              elseif 奖励参数<=70 then
                 ItemControl:GiveItem(id组[n],"炼妖石",math.floor(UserData[id组[n]].角色.等级/10)*10+5)
                 广播消息("#hd/".."#S/(梦魇夜叉)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了梦魇夜叉，获得了玉皇大帝奖励的#g/炼妖石")
              elseif 奖励参数<=80 then
                  ItemControl:GiveItem(id组[n],"炼妖石",math.floor(UserData[id组[n]].角色.等级/10)*10+5)
                  广播消息("#hd/".."#S/(梦魇夜叉)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了梦魇夜叉，获得了玉皇大帝奖励的#g/炼妖石")
              elseif 奖励参数<=90 then
                ItemControl:GiveItem(id组[n],"上古锻造图策",math.floor(UserData[id组[n]].角色.等级/10)*10+5)
                广播消息("#hd/".."#S/(梦魇夜叉)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了梦魇夜叉，获得了玉皇大帝奖励的#g/上古锻造图策")
              elseif 奖励参数<=100 then
                ItemControl:GiveItem(id组[n],"上古锻造图策",math.floor(UserData[id组[n]].角色.等级/10)*10+5)
                广播消息("#hd/".."#S/(梦魇夜叉)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了梦魇夜叉，获得了玉皇大帝奖励的#g/上古锻造图策")
              end

              if cj ~= "" then
               ItemControl:GiveItem(id组[n],cj)
                  广播消息("#hd/".."#S/(梦魇夜叉)".."#R/ "..UserData[id组[n]].角色.名称.."#Y/成功完成了梦魇夜叉，获得了玉皇大帝奖励的#g/"..cj)
                end


    end


    MapControl:移除单位(任务数据[任务id].地图编号, 任务id)
    任务数据[任务id] = nil
end