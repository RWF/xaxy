

function TaskControl:完成蚩尤挑战任务(id)
 
    if UserData[id].队伍==0 then
        SendMessage(UserData[id].连接id, 7, "#y/挑战失败,你离开了队伍将不再活动奖励!")
      return 0
    end
    for n = 1, #队伍数据[UserData[id].队伍].队员数据, 1 do
        self.临时id = 队伍数据[UserData[id].队伍].队员数据[n]
        self.临时等级 = UserData[self.临时id].角色.等级
        RoleControl:添加经验(UserData[self.临时id],10000000, "蚩尤挑战")
        self.玩家名称 = UserData[self.临时id].角色.名称
        RoleControl:添加储备(UserData[self.临时id],3000000, "蚩尤挑战")
        --RoleControl:添加仙玉(UserData[self.临时id],5000, "蚩尤挑战")
        RoleControl:添加活跃度(UserData[self.临时id],20)

                local cj = ""
                local 奖励参数 = math.random(100)   --- 20/100  0.2 20%
                if 奖励参数 >=1 and 奖励参数 <=10 then
                   cj ="玲珑宝图"
                elseif 奖励参数 >=11 and 奖励参数 <=16 then --10%
                    cj="高级魔兽要诀"
                elseif 奖励参数 >=16 and 奖励参数 <=26 then
                    local lsname = {"神之符","不磨符"}
                     cj =lsname[math.random(#lsname)]
                elseif 奖励参数 >=26 and 奖励参数 <=41 then
                  ItemControl:GiveItem(self.临时id, "附魔宝珠", 160)
                 广播消息("#hd/".."#S/(蚩尤挑战)".."#y/居然败倒在#G/".. self.玩家名称.. "#y/ 的脚下,慌忙中落下了#G/" .. 160 .. "#y/级#r/附魔宝珠#"..math.random(110))
                elseif 奖励参数 >=41 and 奖励参数 <=46 then
                 ItemControl:GiveItem(self.临时id, "陨铁", 160)
                 广播消息("#hd/".."#S/(蚩尤挑战)".."#y/居然败倒在#G/"..  self.玩家名称.. "#y/ 的脚下,慌忙中落下了#G/" .. 160 .. "#y/级#r/陨铁#"..math.random(110))
                elseif 奖励参数 >=46 and 奖励参数 <=66 then
    
                   local 随机等级 = math.random(16, 16)*10
                  ItemControl:GiveItem(self.临时id, "珍珠", 随机等级)
                 广播消息("#hd/".."#S/(蚩尤挑战)".."#y/居然败倒在#G/"..  self.玩家名称.. "#y/的脚下,慌忙中落下了#G/160#y/级#r/珍珠#"..math.random(110))
                end
                if cj ~= "" then
                  ItemControl:GiveItem(self.临时id,cj)
                  广播消息("#hd/".."#S/(蚩尤挑战)".."#y/居然败倒在#G/".. self.玩家名称.. "#y/的脚下,慌忙中落下了#G/"..cj.."#"..math.random(110))
                end

        -- if UserData[self.临时id].队长 then
        --    ItemControl:GiveItem(self.临时id,"蚩尤挑战")
        --     RoleControl:添加银子(UserData[self.临时id],1000000, "蚩尤挑战")
        -- end
    end
 end
function TaskControl:挑战蚩尤挑战(id)
    if UserData[id].队伍 == 0 then
        SendMessage(UserData[id].连接id, 7, "#y/此活动必须组队完成")
        return 0
    elseif #队伍数据[UserData[id].队伍].队员数据 < 3  and DebugMode== false then
        SendMessage(UserData[id].连接id, 7, "#y/队伍中的成员数不能少于3人")
        return 0
    elseif 取队伍符合等级(id, 69) == false and DebugMode== false then
        广播队伍消息(id, 7, "#y/参加蚩尤挑战活动需要队伍所有成员等级达到69级")
        return 0
    end


    self.重复名称 = ""
    for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
        if 活动数据.蚩尤挑战[UserData[队伍数据[UserData[id].队伍].队员数据[n]].id] ~= nil then
            self.重复名称 = UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称
        end
    end
    if self.重复名称 ~= "" then
        广播队伍消息(id, 7, "#g/" .. self.重复名称 .. "#y/今日已经挑战过蚩尤挑战了")
        return 0
    end
    for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
     活动数据.蚩尤挑战[UserData[队伍数据[UserData[id].队伍].队员数据[n]].id] ={积分=0,奖励=false,场次=1}
    end
    RoleControl:扣除银子(UserData[id],10000000, "挑战神器副本")
    FightGet:进入处理(id, 100031, "66", 0)
 end