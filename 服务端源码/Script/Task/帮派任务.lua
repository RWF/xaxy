function TaskControl:完成青龙任务(任务id, id)
	id = 任务数据[任务id].数字id
	self.临时等级 = UserData[id].角色.等级
	self.临时经验 = self.临时等级 * 1050 + 5000*4
	RoleControl:添加经验(UserData[id],self.临时经验,"帮派青龙")
	RoleControl:取消任务(UserData[id],任务id)
	任务数据[任务id] = nil
	SendMessage(UserData[id].连接id, 7, "#y/你完成了帮派建设任务")
	if UserData[id].角色.帮派 ~= nil and 帮派数据[UserData[id].角色.帮派] ~= nil and 帮派数据[UserData[id].角色.帮派].当前内政.名称 ~= "无" then
		帮派数据[UserData[id].角色.帮派].当前内政.总进度 = 帮派数据[UserData[id].角色.帮派].当前内政.总进度 + 1
		if 帮派数据[UserData[id].角色.帮派].当前内政.要求进度 <= 帮派数据[UserData[id].角色.帮派].当前内政.总进度 then
			帮派数据[UserData[id].角色.帮派].当前内政.总进度 = 0
			帮派数据[UserData[id].角色.帮派].当前内政.要求进度 = 0
			self.临时名称 = 帮派数据[UserData[id].角色.帮派].当前内政.名称
			if self.临时名称 == "聚义厅" then
				self.临时名称 = "规模"
			end
			帮派数据[UserData[id].角色.帮派][self.临时名称] = 帮派数据[UserData[id].角色.帮派][self.临时名称] + 1
			帮派数据[UserData[id].角色.帮派].当前内政.名称 = "无"
			广播帮派消息(UserData[id].角色.帮派, "#bp/#w/经过所有帮派成员的努力，帮派完成了建设任务！")
		end
	end

	self.增加帮贡 = 0

	if 帮派数据[UserData[id].角色.帮派].成员名单[id].青龙 <= 3 then
		self.增加帮贡 = 1
	elseif 帮派数据[UserData[id].角色.帮派].成员名单[id].青龙 <= 6 then
		self.增加帮贡 = 2
	elseif 帮派数据[UserData[id].角色.帮派].成员名单[id].青龙 <= 10 then
		self.增加帮贡 = 3

		if 帮派数据[UserData[id].角色.帮派].成员名单[id].青龙 == 10 then
			帮派数据[UserData[id].角色.帮派].成员名单[id].青龙 = 0
		end
	end

	self.临时帮派 = UserData[id].角色.帮派
	self.临时id = id
	帮派数据[self.临时帮派].成员名单[self.临时id].帮贡.获得 = 帮派数据[self.临时帮派].成员名单[self.临时id].帮贡.获得 + self.增加帮贡
	帮派数据[self.临时帮派].成员名单[self.临时id].帮贡.当前 = 帮派数据[self.临时帮派].成员名单[self.临时id].帮贡.当前 + self.增加帮贡

	SendMessage(UserData[id].连接id, 7, "#y/你获得了" .. self.增加帮贡 .. "点帮贡")
end


function TaskControl:添加玄武堂任务(id)
	self.飞贼地图 = {
		1506,
		1092,
		1070,
		1501,
		1142,
		1091
	}
	local 临时id6 = tonumber(UserData[id].id .. "701")
	local 地图编号6 = self.飞贼地图[math.random( #self.飞贼地图)]
	local 临时坐标6 = MapControl:Randomloadtion(地图编号6) 
	local 地图名称6 = MapData[地图编号6].名称
	任务数据[临时id6] = {
		类型 = "帮派玄武",
		编号 = 1216,
		分类 = 1,
		次数 = 2,
		结束 = 3600,
		名称 = "小贼",
		造型 = "强盗",
		战斗 = false,
		id = id,
		起始 = os.time(),
		任务id = 临时id6,
		地图编号 = 地图编号6,
		地图名称 = 地图名称6,
		方向 = math.random( 4) - 1,
		坐标 = 临时坐标6,
		数字id = UserData[id].id
	}

	MapControl:添加单位(任务数据[临时id6])

	UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])] = 临时id6
    self:刷新追踪任务信息(id)
	SendMessage(UserData[id].连接id, 7, "#y/你领取了玄武堂任务，请点击任务列表查看")
end
function TaskControl:设置青龙任务(id)
	if 帮派数据[UserData[id].角色.帮派].成员名单[id].青龙 == nil then
		帮派数据[UserData[id].角色.帮派].成员名单[id].青龙 = 0
	end

	帮派数据[UserData[id].角色.帮派].成员名单[id].青龙 = 帮派数据[UserData[id].角色.帮派].成员名单[id].青龙 + 1
	local 临时id6 = tonumber(UserData[id].id .. "6")
	self.临时物品 = {
		"小还丹",
		"金香玉",
		"千年保心丹",
		"佛光舍利子",
		"九转回魂丹",
		"蛇蝎美人",
		"十香返生丸",
		"风水混元丹",
		"五龙丹",
		"金创药",
		"金创药",
		"金创药",
		"烤鸭",
		"佛跳墙",
		"女儿红",
		"女儿红",
		"珍露酒",
		"桂花丸",
		"长寿面",
		"醉生梦死"
	}
	任务数据[临时id6] = {
		名称 = "",
		结束 = 3600,
		分类 = 1,
		次数 = 2,
		战斗 = false,
		地图名称 = 0,
		类型 = "帮派青龙",
		地图编号 = 0,
		编号 = 1216,
		造型 = "强盗",
		id = id,
		起始 = os.time(),
		任务id = 临时id6,
		道具 = self.临时物品[math.random( #self.临时物品)],
		方向 = math.random( 4) - 1,
		坐标 = 临时坐标6,
		数字id = UserData[id].id
	}
	UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])] = 临时id6
	self:刷新追踪任务信息(id)
	SendMessage(UserData[id].连接id, 7, "#y/你领取了帮派建设任务，请点击任务列表查看")
end

function TaskControl:完成玄武堂任务(任务id, id)
	self.临时id = 任务数据[任务id].数字id
	self.临时帮派 = UserData[self.临时id].角色.帮派

	RoleControl:取消任务(UserData[self.临时id],任务id)
	MapControl:移除单位(任务数据[任务id].地图编号, 任务id)

	任务数据[任务id] = nil

	if self.临时帮派 == nil or 帮派数据[self.临时帮派] == nil then
		SendMessage(UserData[self.临时id].连接id, 7, "#y/帮派数据异常，您无法获得帮贡奖励")
	else
		if 帮派数据[self.临时帮派].安定 >= 帮派数据[self.临时帮派].规模 * 100 + 500 then
			SendMessage(UserData[self.临时id].连接id, 7, "#y/本帮安定度已达上限，无法再增加安定度了")
		else
			帮派数据[self.临时帮派].安定 = 帮派数据[self.临时帮派].安定 + 1
		end
		帮派数据[self.临时帮派].繁荣 = 帮派数据[self.临时帮派].繁荣 + 1
		帮派数据[self.临时帮派].成员名单[self.临时id].帮贡.获得 = 帮派数据[self.临时帮派].成员名单[self.临时id].帮贡.获得 + 1
		帮派数据[self.临时帮派].成员名单[self.临时id].帮贡.当前 = 帮派数据[self.临时帮派].成员名单[self.临时id].帮贡.当前 + 1

		SendMessage(UserData[self.临时id].连接id, 7, "#y/你获得了1点帮贡")
	end
end
function TaskControl:触发玄武堂任务(id, 标识)
	if MapControl.MapData[UserData[id].地图].单位组[标识] == nil then
		return 0
	end

	local 任务id = MapControl.MapData[UserData[id].地图].单位组[标识].任务id

	if 任务数据[任务id] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/任务数据异常，代号9001")
	elseif 任务数据[任务id].数字id ~= UserData[id].id then
		SendMessage(UserData[id].连接id, 7, "#y/你无法完成这样的任务")
	elseif 任务数据[任务id].战斗 then
		SendMessage(UserData[id].连接id, 7, "#y/任务数据异常，代号9002")
	else
		任务数据[任务id].战斗 = true

		FightGet:进入处理(id, 100040, "66", 任务id)
	end
end