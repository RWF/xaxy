--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-10-04 06:34:52
--======================================================================--
function TaskControl:添加坐骑任务(id)
	local 临时id3 = tonumber(UserData[id].id .. "72")
	任务数据[临时id3] = {
		坐标0,
		地图编号 = 0,
		战斗 = false,
		方向 = 0,
		名称 = 0,
		结束 = 3600,
		编号 = 0,
		地图名称 = 0,
		类型 = "坐骑",
		进程 = 1,
		造型 = 0,
		NPC=107004,
		id = id,
		起始 = os.time(),
		任务id = 临时id3,
		数字id = id,
	}
 UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])]=临时id3
 SendMessage(UserData[id].连接id,7,"#Y/你领取了坐骑任务，请点击任务列表查看")
 self:刷新追踪任务信息(id)
 end
function TaskControl:完成坐骑任务(id,任务id)
	if #UserData[id].角色.坐骑数据 >= 5 then
			SendMessage(UserData[id].连接id, 7, "#y/你已经无法获得更多的坐骑了,请放生一个坐骑")
			return 0
	else
			RoleControl:添加随机坐骑(UserData[id])
			RoleControl:添加活跃度(UserData[id],1)
			 RoleControl:取消任务(UserData[id],任务id)
 			任务数据[任务id]=nil
	end
end
function TaskControl:完成坐骑任务2(id)

	    if RoleControl:GetTaskID(UserData[id],"坐骑") == 0  and 任务数据[RoleControl:GetTaskID(UserData[id],"坐骑")].进程~=2  then
	    		return 0
		end
		local 任务id = RoleControl:GetTaskID(UserData[id],"坐骑")
		SendMessage(UserData[id].连接id,20,{"天兵","顺风耳","#Y/好吧,我告诉你是水兵统领抢占资源"})
	任务数据[任务id].进程 = 任务数据[任务id].进程 + 1
	任务数据[任务id].NPC=111110
	
	self:刷新追踪任务信息(id)
 end
function TaskControl:完成坐骑任务3(任务id)
 id=任务数据[任务id].id
	SendMessage(UserData[id].连接id,20,{"将军","水兵统领","#Y/这一切都是周猎户指使的不关我的事呀"})
 任务数据[任务id].进程 = 任务数据[任务id].进程 + 1
 任务数据[任务id].NPC=152601
 self:刷新追踪任务信息(id)
 end
function TaskControl:完成坐骑任务4(任务id)
	 id=任务数据[任务id].id
	 if RoleControl:GetTaskID(UserData[id],"坐骑") == 0  and 任务数据[RoleControl:GetTaskID(UserData[id],"坐骑")].进程~=4  then
	    		return 0
	end
	SendMessage(UserData[id].连接id,7,"#y你完成了所有的坐骑任务,找百兽王领取坐骑吧")
 	任务数据[任务id].进程 = 任务数据[任务id].进程 + 1
 	任务数据[任务id].NPC=121601
 self:刷新追踪任务信息(id)
 end
function TaskControl:完成坐骑任务1(id, 任务id)
	if RoleControl:GetTaskID(UserData[id],"坐骑") == 0 and 任务数据[RoleControl:GetTaskID(UserData[id],"坐骑")].进程 ~= 1 then
		return 0
	end
	local 任务id = RoleControl:GetTaskID(UserData[id],"坐骑")
	任务数据[任务id].进程 = 任务数据[任务id].进程 + 1
	任务数据[任务id].NPC=111109
	local 三药 = {"金创药","金香玉","小还丹","千年保心丹","风水混元丹","定神香","蛇蝎美人","九转回魂丹","佛光舍利子","五龙丹"}
	任务数据[任务id].道具 = 三药[math.random(1,#三药)]
	self:刷新追踪任务信息(id)
 end