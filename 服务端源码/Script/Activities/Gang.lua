local 帮派竞赛 = class()
function 帮派竞赛:初始化()
  self.开关=false
  self.名单={}
  self.报名=true
  self.帮战列表={}
  self.迷宫名单={}
  self.迷宫开关=false
  self.宝箱开关=false
   self.迷宫准备开关=false
  self.宝箱名单={}
end

function 帮派竞赛:报名参赛(id)

     if self.报名 ==false then
        SendMessage(UserData[id].连接id, 7, "#y/报名时间已经截止！")
     elseif 帮派数据[UserData[id].角色.帮派].成员名单[id].职务== "帮主" or 帮派数据[UserData[id].角色.帮派].成员名单[id].职务== "副帮主" then
         if  self:重复名单(UserData[id].角色.帮派) then
            SendMessage(UserData[id].连接id, 7, "#y/你已经报名过，无需重复报名！")
         else
           self.名单[#self.名单+1]={名称=帮派数据[UserData[id].角色.帮派].名称,编号=UserData[id].角色.帮派}
           SendMessage(UserData[id].连接id, 7, "#y/报名参赛成功！")

        end
     else
         SendMessage(UserData[id].连接id, 7, "#y/只有帮主才可以报名参赛！")
     end

end
function 帮派竞赛:进入帮派迷宫(id)

    if self.迷宫准备开关  then
            local 列表= self:取迷宫列表编号(UserData[id].角色.帮派)
            if 列表==false then
            SendMessage(UserData[id].连接id, 7, "#y/你的帮派没有权限进入！")
            elseif     UserData[id].队伍 ~= 0 then
                 SendMessage(UserData[id].连接id, 7, "#y/请解散队伍才可以进入")
            elseif     UserData[id].迷宫 then
                SendMessage(UserData[id].连接id, 7, "#y/你已经参加过了，无法在进入")
            else
                if 列表==1 then
                    UserData[id].角色.称谓.特效=0xFF5794F8
                elseif 列表==2 then
                     UserData[id].角色.称谓.特效=0xFFFA0D0D
                elseif 列表==3 then
                    UserData[id].角色.称谓.特效=0xFFF0F966
                elseif 列表==4 then
                    UserData[id].角色.称谓.特效=0xFFF411F4
                else
                   UserData[id].角色.称谓.特效=0xFF10FBD9
                end
                SendMessage(UserData[id].连接id, 1026,UserData[id].角色.称谓)
                self.迷宫名单[列表].参加人数 =self.迷宫名单[列表].参加人数+1
                MapControl:Jump(id,1241, 120, 66)
                UserData[id].迷宫=true
            end
    else
        SendMessage(UserData[id].连接id, 7, "#y/活动时间还未开启")
    end


end
function 帮派竞赛:进入帮派宝箱(id)
    if self.宝箱开关  then

            local 列表= self:取宝箱列表编号(UserData[id].角色.帮派)
            if 列表==false then
            SendMessage(UserData[id].连接id, 7, "#y/你的帮派没有权限进入！")
            elseif     UserData[id].队伍 ~= 0 then
                 SendMessage(UserData[id].连接id, 7, "#y/请解散队伍才可以进入")
            else
                MapControl:Jump(id,1380, 105, 67)
            end
    else
        SendMessage(UserData[id].连接id, 7, "#y/活动时间还未开启")
    end


end
function 帮派竞赛:取宝箱列表编号(编号)

     if self.宝箱名单== 编号 then
         return i
     end

  return false
end
function 帮派竞赛:取迷宫列表编号(编号)
    for i=1,#self.迷宫名单 do
             if self.迷宫名单[i].编号 == 编号 then
                 return i
             end
    end
  return false
end
function 帮派竞赛:送出场地(id)


     local 列表= self:取帮战列表编号(UserData[id].角色.帮派)
            if 列表==false then
                 UserData[id].角色.称谓.特效=4285922956
                SendMessage(UserData[id].连接id, 1026,UserData[id].角色.称谓)
                MapControl:Jump(id, 1001, 378, 160, 1)
            elseif     UserData[id].队伍 ~= 0 then
                 SendMessage(UserData[id].连接id, 7, "#y/请解散队伍才可以离开")
            else
                UserData[id].角色.称谓.特效=4285922956
                SendMessage(UserData[id].连接id, 1026,UserData[id].角色.称谓)
                MapControl:Jump(id, 1001, 378, 160, 1)
                self.帮战列表[列表.组号][列表.分类].参加人数 =self.帮战列表[列表.组号][列表.分类].参加人数-1


            end

end
function 帮派竞赛:进入场地(id)
    if self.报名==false then

     local 列表= self:取帮战列表编号(UserData[id].角色.帮派)
            if 列表==false then
            SendMessage(UserData[id].连接id, 7, "#y/你的帮派没有在帮战名单内！")
            elseif     UserData[id].队伍 ~= 0 then
                 SendMessage(UserData[id].连接id, 7, "#y/请解散队伍才可以进入")
            else
                if 列表.分类==1 then
                    UserData[id].角色.称谓.特效=0xFF5794F8
                else
                    UserData[id].角色.称谓.特效=0xFFFA0D0D
                end
               SendMessage(UserData[id].连接id, 1026,UserData[id].角色.称谓)

                 self.帮战列表[列表.组号][列表.分类].参加人数 =self.帮战列表[列表.组号][列表.分类].参加人数+1
               
                MapControl:Jump(id,列表.地图, 16, 93)

            end
    else
        SendMessage(UserData[id].连接id, 7, "#y/比赛还未开始，无法进入场地")
    end
end
function 帮派竞赛:战斗处理(胜利id, 失败id)
    local 添加积分 =1
    local 列表= self:取帮战列表编号(UserData[失败id].角色.帮派)
    local 胜利列表 = self:取帮战列表编号(UserData[胜利id].角色.帮派)
   if UserData[失败id].队伍 == 0 then

        帮派数据[UserData[失败id].角色.帮派].成员名单[失败id].帮贡.获得=帮派数据[UserData[失败id].角色.帮派].成员名单[失败id].帮贡.获得+15
        帮派数据[UserData[失败id].角色.帮派].成员名单[失败id].帮贡.当前=帮派数据[UserData[失败id].角色.帮派].成员名单[失败id].帮贡.当前+15
        SendMessage(UserData[失败id].连接id,7,"#Y/你获得了15点帮贡")
        UserData[失败id].角色.称谓.特效=4285922956
        SendMessage(UserData[失败id].连接id, 1026,UserData[失败id].角色.称谓)
        RoleControl:添加活跃度(UserData[失败id],1)
        MapControl:Jump(失败id, 1001, 378, 160, 1)
    else
        添加积分= #队伍数据[UserData[失败id].队伍].队员数据
        for n = 1, #队伍数据[UserData[失败id].队伍].队员数据 do

        帮派数据[UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[失败id].队伍].队员数据[n]].帮贡.获得=帮派数据[UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[失败id].队伍].队员数据[n]].帮贡.获得+15
        帮派数据[UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[失败id].队伍].队员数据[n]].帮贡.当前=帮派数据[UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[失败id].队伍].队员数据[n]].帮贡.当前+15
        SendMessage(UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].连接id,7,"#Y/你获得了15点帮贡")
          UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].角色.称谓.特效=4285922956
        RoleControl:添加活跃度(UserData[队伍数据[UserData[失败id].队伍].队员数据[n]],1)
        end
        MapControl:Jump(失败id, 1001, 378, 160, 1)
    end
    self.帮战列表[列表.组号][列表.分类].参加人数 =self.帮战列表[列表.组号][列表.分类].参加人数-添加积分


    local 帮派积分 = 添加积分*5
     self.帮战列表[胜利列表.组号][胜利列表.分类].积分 = self.帮战列表[胜利列表.组号][胜利列表.分类].积分+ 帮派积分
     self.帮战列表[胜利列表.组号][胜利列表.分类].击杀人数 = self.帮战列表[胜利列表.组号][胜利列表.分类].击杀人数+ 添加积分
    if UserData[胜利id].队伍 == 0 then
        帮派数据[UserData[胜利id].角色.帮派].成员名单[胜利id].帮贡.获得=帮派数据[UserData[胜利id].角色.帮派].成员名单[胜利id].帮贡.获得+30
        帮派数据[UserData[胜利id].角色.帮派].成员名单[胜利id].帮贡.当前=帮派数据[UserData[胜利id].角色.帮派].成员名单[胜利id].帮贡.当前+30
        SendMessage(UserData[胜利id].连接id,7,"#Y/你获得了30点帮贡")
        RoleControl:添加活跃度(UserData[胜利id],1)
        SendMessage(UserData[胜利id].连接id, 7, "#y/你获得了" .. 帮派积分 .. "点帮派积分，当前总积分" .. self.帮战列表[胜利列表.组号][胜利列表.分类].积分 .. "点")
    else
        for n = 1, #队伍数据[UserData[胜利id].队伍].队员数据 do
                帮派数据[UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[胜利id].队伍].队员数据[n]].帮贡.获得=帮派数据[UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[胜利id].队伍].队员数据[n]].帮贡.获得+30
                帮派数据[UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[胜利id].队伍].队员数据[n]].帮贡.当前=帮派数据[UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[胜利id].队伍].队员数据[n]].帮贡.当前+30
                SendMessage(UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].连接id,7,"#Y/你获得了30点帮贡")
                RoleControl:添加活跃度(UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]],1)
                SendMessage(UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].连接id, 7, "#y/你获得了" .. 帮派积分 .. "点帮派积分，当前总积分" .. self.帮战列表[胜利列表.组号][胜利列表.分类].积分 .. "点")
        end
    end
end
function 帮派竞赛:迷宫战斗处理(胜利id, 失败id)
    local 添加积分 =1
    local 列表= self:取迷宫列表编号(UserData[失败id].角色.帮派)
    local 胜利列表 = self:取迷宫列表编号(UserData[胜利id].角色.帮派)
   if UserData[失败id].队伍 == 0 then

        帮派数据[UserData[失败id].角色.帮派].成员名单[失败id].帮贡.获得=帮派数据[UserData[失败id].角色.帮派].成员名单[失败id].帮贡.获得+50
        帮派数据[UserData[失败id].角色.帮派].成员名单[失败id].帮贡.当前=帮派数据[UserData[失败id].角色.帮派].成员名单[失败id].帮贡.当前+50
        SendMessage(UserData[失败id].连接id,7,"#Y/你获得了50点帮贡")
        UserData[失败id].角色.称谓.特效=4285922956
        SendMessage(UserData[失败id].连接id, 1026,UserData[失败id].角色.称谓)
        RoleControl:添加活跃度(UserData[失败id],1)
        UserData[失败id].迷宫=true
        MapControl:Jump(失败id, 1001, 378, 160, 1)
    else
        添加积分= #队伍数据[UserData[失败id].队伍].队员数据
        for n = 1, #队伍数据[UserData[失败id].队伍].队员数据 do

        帮派数据[UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[失败id].队伍].队员数据[n]].帮贡.获得=帮派数据[UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[失败id].队伍].队员数据[n]].帮贡.获得+50
        帮派数据[UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[失败id].队伍].队员数据[n]].帮贡.当前=帮派数据[UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[失败id].队伍].队员数据[n]].帮贡.当前+50
        SendMessage(UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].连接id,7,"#Y/你获得了50点帮贡")
          UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].角色.称谓.特效=4285922956
           RoleControl:添加活跃度(UserData[队伍数据[UserData[失败id].队伍].队员数据[n]],1)
           UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].迷宫=true
        end
        MapControl:Jump(失败id, 1001, 378, 160, 1)
    end
    self.迷宫名单[列表].参加人数 =self.帮战列表[列表].参加人数-添加积分



    if UserData[胜利id].队伍 == 0 then
        帮派数据[UserData[胜利id].角色.帮派].成员名单[胜利id].帮贡.获得=帮派数据[UserData[胜利id].角色.帮派].成员名单[胜利id].帮贡.获得+50
        帮派数据[UserData[胜利id].角色.帮派].成员名单[胜利id].帮贡.当前=帮派数据[UserData[胜利id].角色.帮派].成员名单[胜利id].帮贡.当前+50
        SendMessage(UserData[胜利id].连接id,7,"#Y/你获得了50点帮贡")
        RoleControl:添加活跃度(UserData[胜利id],1)

    else
        for n = 1, #队伍数据[UserData[胜利id].队伍].队员数据 do
                帮派数据[UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[胜利id].队伍].队员数据[n]].帮贡.获得=帮派数据[UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[胜利id].队伍].队员数据[n]].帮贡.获得+50
                帮派数据[UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[胜利id].队伍].队员数据[n]].帮贡.当前=帮派数据[UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].角色.帮派].成员名单[队伍数据[UserData[胜利id].队伍].队员数据[n]].帮贡.当前+50
                SendMessage(UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].连接id,7,"#Y/你获得了50点帮贡")
                RoleControl:添加活跃度(UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]],1)
        end
    end
end
function 帮派竞赛:取帮战列表编号(编号)

    for i=1,#self.帮战列表 do
        for n=1,#self.帮战列表[i] do
             if self.帮战列表[i][n].编号 == 编号 then
                 return {地图=tonumber("1135"..i),组号=i,分类=n}
             end
        end
    end
  return false
end
function 帮派竞赛:开启竞赛()
    广播消息("#xt/#y/帮战已经截止报名半小时后开启帮派竞赛，请帮派选手进入场地准备。")
    发送游戏公告("帮战已经截止报名半小时后开启帮派竞赛，请帮派选手进入场地准备。")
       广播消息("#xt/#G/帮派对战列表如下：")
        for i=1,math.modf(#self.名单/2) do
        
        
        self.帮战列表[i]={
        {编号=self.名单[i*2-1].编号,积分=0,名称=self.名单[i*2-1].名称,参加人数=0,击杀人数=0},
        {编号=self.名单[i*2].编号,积分=0,名称=self.名单[i*2].名称,参加人数=0,击杀人数=0},
        }
        广播消息("#xt/#R/"..self.帮战列表[i][1].名称.."#Y/ VS #R/"..self.帮战列表[i][2].名称)
        end

        self.报名=false
end
function 帮派竞赛:结束竞赛()
        self.开关=false
        self.名单={}
        self.报名=true
        local 临时名称=""
        for i=1,#self.帮战列表 do
           if self.帮战列表[i][1].积分 >  self.帮战列表[i][2].积分 then
            self.迷宫名单[#self.迷宫名单+1]={名称=self.帮战列表[i][1].名称,编号=self.帮战列表[i][1].编号,参加人数=0}
            临时名称=临时名称..self.帮战列表[i][1].名称.."、"
           elseif self.帮战列表[i][1].积分 == self.帮战列表[i][2].积分 and self.帮战列表[i][1].击杀人数> self.帮战列表[i][2].击杀人数 then
               self.迷宫名单[#self.迷宫名单+1]={名称=self.帮战列表[i][1].名称,编号=self.帮战列表[i][1].编号,参加人数=0}
               临时名称=临时名称..self.帮战列表[i][1].名称.."、"
           else
              self.迷宫名单[#self.迷宫名单+1]={名称=self.帮战列表[i][2].名称,编号=self.帮战列表[i][2].编号,参加人数=0}
              临时名称=临时名称..self.帮战列表[i][2].名称.."、"
           end

         MapControl:传送地图玩家(tonumber("1135"..i))
       end
       if 临时名称~="" then
          广播消息("#xt/#Y/帮派竞赛已经结束，恭喜以下帮派可以进入迷宫挑战环节是：#R/"..临时名称)
          发送游戏公告("帮派竞赛已经结束，恭喜以下帮派可以进入迷宫挑战环节是："..临时名称)
       end

        for n, v in pairs(FightGet.战斗盒子) do
           if FightGet.战斗盒子[n].战斗类型== 200004 or FightGet.战斗盒子[n].战斗类型== 100068 then
              FightGet.战斗盒子[n]:强制结束战斗()
              FightGet.战斗盒子[n]=nil
             end
        end


        self.帮战列表={}
        self.迷宫准备开关=true

end

function 帮派竞赛:结束迷宫()


        local 临时名称=""

        if #self.迷宫名单>0 then
            table.sort(self.迷宫名单,function(a,b) return a.参加人数>b.参加人数 end )
            临时名称=self.迷宫名单[1].名称
            self.宝箱名单=self.迷宫名单[1].编号
        end
       if 临时名称~="" then
          广播消息("#xt/#Y/帮派迷宫已经结束，恭喜以下帮派可以进入领取宝箱奖励环节是：#R/"..临时名称)
          发送游戏公告("帮派迷宫已经结束，恭喜以下帮派可以进入领取宝箱奖励环节是："..临时名称)
          广播消息("#xt/#Y/5分钟后帮战奖励地图会随机刷出宝箱")
       end

        for n, v in pairs(FightGet.战斗盒子) do
           if FightGet.战斗盒子[n].战斗类型== 200008 then
              FightGet.战斗盒子[n]:强制结束战斗()
              FightGet.战斗盒子[n]=nil
             end
        end
        MapControl:传送地图玩家(1241)
         self.迷宫开关=false
         self.宝箱开关=true
         self.迷宫名单={}


end

function 帮派竞赛:查看对战表(id)
    if self.报名==false then
            local 列表= self:取帮战列表编号(UserData[id].角色.帮派)
            if 列表==false then
            SendMessage(UserData[id].连接id, 7, "#y/你的帮派没有在帮战名单内！")
            else


            -- 对话内容="帮派对战列表信息如下：\n".."           "..self.帮战列表[列表.组号][1].名称.."          vs          "..self.帮战列表[列表.组号][2].名称.."\n比赛积分:           "..self.帮战列表[列表.组号][1].积分.."          vs          "..self.帮战列表[列表.组号][2].积分
            -- .."\n参加人数:           "..self.帮战列表[列表.组号][1].参加人数.."          vs          "..self.帮战列表[列表.组号][2].参加人数.."\n击杀人数:           "..self.帮战列表[列表.组号][1].击杀人数.."          vs          "..self.帮战列表[列表.组号][2].击杀人数


            SendMessage(UserData[id].连接id,1027,self.帮战列表[列表.组号])
                -- self.帮战列表[列表.组号]
            end

    else
        SendMessage(UserData[id].连接id, 7, "#y/帮战报名还未截止，无法查看对战信息")
    end
end
function 帮派竞赛:重复名单(编号)
    for n, v in pairs(self.名单) do
        if self.名单[n] ~= nil then
            if self.名单[n].编号 == 编号 then
                return true
            end
        end
    end
    return false
end
return 帮派竞赛