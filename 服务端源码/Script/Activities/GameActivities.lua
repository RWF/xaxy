-- @Author: 作者QQ381990860
-- @Date:   2021-09-07 00:54:26
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-09-08 10:38:40
local GameActivities = class()

function GameActivities:开启首席争入场()
	首席争霸赛进场开关 = true

	发送游戏公告("首席争霸赛活动已经开启入场，19点后将无法进入比赛地图。请各位玩家提前找本门派首席弟子进入比赛地图。")

	for n = 1, 18 do
		if 首席资源数据[n].玩家id + 0 ~= 0 and UserData[首席资源数据[n].玩家id] ~= nil then

			RoleControl:删除称谓(UserData[首席资源数据[n].玩家id], UserData[首席资源数据[n].玩家id].角色.门派 .. "首席弟子", 1)
			SendMessage(UserData[首席资源数据[n].玩家id].连接id, 7, "#y/你的首席弟子称谓已经被回收")
		end
	end
end

function GameActivities:炼丹更新()
  if 炼丹炉.时间 <= 0 then
    self.开奖数据 = self:炼丹开奖中()
  end
  炼丹炉.时间 = 炼丹炉.时间 - 1
  if 炼丹炉.时间 == 121 then
    self:炼丹开奖(self.开奖数据)
  end
  for i,v in pairs(炼丹查看) do
    if UserData[i] ~= nil and 炼丹炉.时间 ~= nil then
      SendMessage(UserData[i].连接id,3078,炼丹炉.时间)
    else
      炼丹查看[i] = nil
    end
  end
end

function GameActivities:炼丹开奖中()
  local 八卦 = {"乾","巽","坎","艮","坤","震","离","兑"}
  local 开奖 = {吉位=八卦[math.random(1,#八卦)]}
  开奖.八卦位 = 八卦[math.random(1,#八卦)]
  if 开奖.八卦位 == 开奖.吉位 then
    开奖.八卦位 = 八卦[math.random(1,#八卦)]
  end
  for i,v in pairs(炼丹查看) do
    if UserData[i] ~= nil then
      SendMessage(UserData[i].连接id,108.2,{吉位=开奖.吉位,八卦位=开奖.八卦位})
    else
      炼丹查看[i] = nil
    end
  end
  炼丹炉.时间 = 130
  return 开奖
end
function GameActivities:炼丹开奖(开奖)
  广播消息("#S(八卦炼丹)#Y本次八卦炉炼丹结束,本次炼丹吉位为:#G"..开奖.吉位.." #Y本次炼丹八卦位为:#G"..开奖.八卦位)
  for i,v in pairs(炼丹炉) do
    if i ~= "时间" then
      if 炼丹炉[i][开奖.吉位] ~= nil and 炼丹炉[i][开奖.八卦位] ~= nil and 开奖.吉位==开奖.八卦位 then
        local 奖励数据 = {}
        奖励数据[1],奖励数据[2],奖励数据[3] = self:取奖励(炼丹炉[i][开奖.吉位])
        if UserData[i] ~= nil then
          if 奖励数据[1] > 0 then
          	 ItemControl:GiveItem(i, "金砂丹",nil,nil,奖励数据[1])
            
          end
          if 奖励数据[2] > 0 then
          	ItemControl:GiveItem(i, "银砂丹",nil,nil,奖励数据[2])
          
          end
          if 奖励数据[3] > 0 then
          	ItemControl:GiveItem(i, "铜砂丹",nil,nil,奖励数据[3])
          end
            SendMessage(UserData[id].连接id,7,"#Y/恭喜你在炼丹中中得吉位和八卦位！")
          广播消息(string.format("#S(八卦炼丹)#Y恭喜:#G%s#Y在本次炼丹中幸运的猜中吉位以及八卦位获得了:#G%s颗#Y金丹 #G%s颗#Y银丹 #G%s颗#Y铜丹",UserData[i].角色.名称,奖励数据[1],奖励数据[2],奖励数据[3]))
        else
          if 商品存放[i] == nil then
            商品存放[i] = {[1]=奖励数据[1],[2]=奖励数据[2],[3]=奖励数据[3]}
          else
            商品存放[i] = {}
            商品存放[i][1] = 商品存放[i][1] + 奖励数据[1]
            商品存放[i][2] = 商品存放[i][2] + 奖励数据[2]
            商品存放[i][3] = 商品存放[i][3] + 奖励数据[3]
          end
        end
      elseif 炼丹炉[i][开奖.吉位] ~= nil then
        local 奖励数据 = {}
        奖励数据[1],奖励数据[2],奖励数据[3] = self:取奖励(炼丹炉[i][开奖.吉位]/2)
        奖励数据[1] = math.floor(奖励数据[1])
        奖励数据[2] = math.floor(奖励数据[2])
        奖励数据[3] = math.floor(奖励数据[3])
         if UserData[i] ~= nil then
          if 奖励数据[1] > 0 then
          	 ItemControl:GiveItem(i, "金砂丹",nil,nil,奖励数据[1])
            
          end
          if 奖励数据[2] > 0 then
          	ItemControl:GiveItem(i, "银砂丹",nil,nil,奖励数据[2])
          
          end
          if 奖励数据[3] > 0 then
          	ItemControl:GiveItem(i, "铜砂丹",nil,nil,奖励数据[3])
          end
            SendMessage(UserData[id].连接id,7,"#Y/恭喜你在炼丹中中得吉位和八卦位！")
          广播消息(string.format("#S(八卦炼丹)#Y恭喜:#G%s#Y在本次炼丹中幸运的猜中吉位以及八卦位获得了:#G%s颗#Y金丹 #G%s颗#Y银丹 #G%s颗#Y铜丹",UserData[i].角色.名称,奖励数据[1],奖励数据[2],奖励数据[3]))
        else
          if 商品存放[i] == nil then
            商品存放[i] = {[1]=奖励数据[1],[2]=奖励数据[2],[3]=奖励数据[3]}
          else
            商品存放[i] = {}
            商品存放[i][1] = 商品存放[i][1] + 奖励数据[1]
            商品存放[i][2] = 商品存放[i][2] + 奖励数据[2]
            商品存放[i][3] = 商品存放[i][3] + 奖励数据[3]
          end
        end
      end
    end
  end
  炼丹炉={}
  炼丹炉.时间 = 120
  for i,v in pairs(炼丹查看) do
    if UserData[i] ~= nil then
      SendMessage(UserData[i].连接id,108.1,{数据=炼丹炉[i],灵气=UserData[i].角色.炼丹灵气})
    else
      炼丹查看[i] = nil
    end
  end
end
function GameActivities:炼丹下注(内容)
  local 八卦 = {"乾","巽","坎","艮","坤","震","离","兑"}
  local id =内容.数字id
  local 序号 = 内容.编号
  local 数额 = 内容.数额

  if 炼丹炉.时间 <= 5 then
    SendMessage(UserData[id].连接id,7,"#Y/即将开始炼丹,停止灌注灵气！")
    return
  elseif 炼丹炉.时间 >= 115 then
    SendMessage(UserData[id].连接id,7,"#Y/炼丹才刚刚结束,请稍等再下注！")
    return
  elseif UserData[id].角色.炼丹灵气 < 数额 then
    SendMessage(UserData[id].连接id,7,"#Y/你没有这么多的灵气用于灌注！")
    return
  elseif 数额 < 10 then
    SendMessage(UserData[id].连接id,7,"#Y/炼丹炉单次灌注灵气不得少于10点！")
    return
  else
    if 炼丹炉[id] == nil then
      炼丹炉[id]={}
      炼丹炉[id][八卦[序号]] = 数额
    else
      if 炼丹炉[id][八卦[序号]] ~= nil then
        炼丹炉[id][八卦[序号]] = 炼丹炉[id][八卦[序号]] +数额
      else
        炼丹炉[id][八卦[序号]] = 数额
      end
    end
    UserData[id].角色.炼丹灵气 = UserData[id].角色.炼丹灵气 - 数额
    SendMessage(UserData[id].连接id,7,"#Y/灌注灵气成功,请等候丹炉炼丹吧！")
    SendMessage(UserData[id].连接id,108.1,{数据=炼丹炉[id],灵气=UserData[id].角色.炼丹灵气})
  end
end

function GameActivities:天梯匹配处理()

 
    for n,v in pairs(天梯匹配) do
	
	  if  UserData[n] ~= nil and 取队伍人数(n) < 3 and  v.匹配==1 then 
	    SendMessage(UserData[n].连接id, 7,"队伍人数低于3人，匹配失败") 
        
	  end
    if UserData[n] ~= nil and  v.匹配==1 and UserData[n].战斗 ==0 and UserData[n].观战模式 == false and 取队伍人数(n) >= 3   then --加一个队伍人数判定
     local 临时表 = {}
     local 玩家1=n ---玩家1
       for q, i in  pairs(天梯匹配)  do
	         if  玩家1~=q and  UserData[q] ~= nil and  天梯匹配[q].匹配==1 and 取队伍人数(q) < 3 then 
	            SendMessage(UserData[q].连接id, 7,"队伍人数低于3人，匹配失败")   
	        end
	   
        if  UserData[q] ~= nil and 玩家1~=q and  天梯匹配[q].匹配==1 and UserData[q].战斗 ==0 and UserData[q].观战模式 == false  and 取队伍人数(q) >= 3 then --加一个队伍人数判定
           临时表[#临时表+1]=q

         end
       end
     if #临时表>=1 then
       local 玩家2= 临时表[math.random(1,#临时表)]--玩家2
      FightGet:创建战斗(玩家1,200006,玩家2,1001)
      天梯匹配[玩家2].匹配=0
      天梯匹配[玩家1].匹配=0
      玩家表 = {[1]=玩家1,[2]=玩家2}
   
       end
    end
 end



end

function GameActivities:开启首席争霸赛()
	self.首席玩家 = {}
	for n = 1, 18 do
		self.首席玩家[首席弟子门派类[n]] = {}
	end
	for n, v in pairs(MapControl.MapData[5135].玩家) do
		if UserData[n] ~= nil then
			self.首席玩家[UserData[n].角色.门派][UserData[n].id] = math.floor(UserData[n].角色.等级 / 10)
			UserData[n].首席保护 = 0

			SendMessage(UserData[n].连接id, 7, "#y/你获得了" .. self.首席玩家[UserData[n].角色.门派][UserData[n].id] .. "点比赛积分")
		end
	end
	首席争霸赛战斗开关 = true
	首席争霸赛进场开关 = false
	self.统计门派 = 0

	for n = 1, 18 do
		self.参加人数 = 0
		self.参加id = 0

		for i, v in pairs(self.首席玩家[首席弟子门派类[n]]) do
			self.参加人数 = self.参加人数 + 1
			self.参加id = i
		end

		if self.参加人数 <= 0 then
			self:结束首席争霸门派(首席弟子门派类[n], 1)

			self.统计门派 = self.统计门派 + 1
		elseif self.参加人数 == 1 then
			self.统计门派 = self.统计门派 + 1

			self:结束首席争霸门派(首席弟子门派类[n], 2, self.参加id, n)
		end
	end

	发送游戏公告("首席争霸赛活动已经开放战斗功能，比武场内的玩家可通过Alt+A切入战斗。每次战斗胜利都会获得对应的比赛积分，比赛积分最多的玩家将成为本门派首席弟子。")

	if self.统计门派 >= 18 then
		发送游戏公告("首席争霸赛活动已经结束。已经获得本门派首席弟子资格的玩家可前往本门派首席弟子处领取首席弟子称谓。首席弟子称谓以及造型将在每天的首席争霸赛活动开启前失效。")

		首席争霸赛战斗开关 = false
	end
end


function GameActivities:首席争霸玩家退出(门派)
	self.参加人数 = 0
	self.参加id = 0

	for n, v in pairs(self.首席玩家[门派]) do
		if UserData[n] ~= nil then
			self.参加人数 = self.参加人数 + 1
			self.参加id = n
		end
	end

	if self.参加人数 == 0 then
		self:结束首席争霸门派(门派, 1)

		self.统计门派 = self.统计门派 + 1
	elseif self.参加人数 == 1 then
		self.统计门派 = self.统计门派

		self:结束首席争霸门派(门派, 4, self.参加id, self:取门派编号(门派))
	end
end

function GameActivities:比武大会战斗处理(胜利id, 失败id)
	if UserData[失败id].队伍 == 0 then
		self.战胜人数 = 1

		MapControl:Jump(失败id, 1001, 429, 164, 1)

		比武大会数据[失败id].奖励 = true
       RoleControl:添加活跃度(UserData[失败id],30)
		SendMessage(UserData[失败id].连接id, 7, "#y/你现在可以找兰虎领取奖励了")
	else
		self.战胜人数 = #队伍数据[UserData[失败id].队伍].队员数据

		for n = 1, #队伍数据[UserData[失败id].队伍].队员数据 do
			比武大会数据[队伍数据[UserData[失败id].队伍].队员数据[n]].奖励 = true

			SendMessage(UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].连接id, 7, "#y/你现在可以找兰虎领取奖励了")
		end

		MapControl:Jump(失败id, 1001, 429, 164, 1)
	end

	self.添加积分 = self.战胜人数 * 5

	if UserData[胜利id].队伍 == 0 then
		比武大会数据[胜利id].奖励 = true
		比武大会数据[胜利id].积分 = 比武大会数据[胜利id].积分 + self.添加积分
		UserData[胜利id].角色.比武积分 = UserData[胜利id].角色.比武积分 + self.添加积分
		UserData[胜利id].比武保护期 = os.time()
          RoleControl:添加活跃度(UserData[胜利id],30)
		SendMessage(UserData[胜利id].连接id, 7, "#y/你获得了" .. self.添加积分 .. "点比武积分，当前总积分" .. 比武大会数据[胜利id].积分 .. "点")
	else
		for n = 1, #队伍数据[UserData[胜利id].队伍].队员数据 do
			比武大会数据[队伍数据[UserData[胜利id].队伍].队员数据[n]].奖励 = true
			比武大会数据[队伍数据[UserData[胜利id].队伍].队员数据[n]].积分 = 比武大会数据[队伍数据[UserData[胜利id].队伍].队员数据[n]].积分 + self.添加积分
			UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].角色.比武积分 = UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].角色.比武积分 + self.添加积分
			UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].比武保护期 = os.time()
           RoleControl:添加活跃度(UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]],30)
			SendMessage(UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].连接id, 7, "#y/你获得了" .. self.添加积分 .. "点比武积分,当前总积分" .. 比武大会数据[队伍数据[UserData[胜利id].队伍].队员数据[n]].积分 .. "点")
		end
	end
end

function GameActivities:华山论剑奖励(胜利id, 失败id)
	--------------------胜利----------------------------
	if (UserData[胜利id].队伍 == 0 or  #队伍数据[UserData[胜利id].队伍].队员数据 < 3 or 取队伍符合等级(胜利id, 69) == false) and DebugMode== false  then
		
		SendMessage(UserData[胜利id].连接id, 7, "#y/队伍异常没有奖励")
	else
		for n = 1, #队伍数据[UserData[胜利id].队伍].队员数据 do
			 if 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]]==nil then
            天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]]={id=队伍数据[UserData[胜利id].队伍].队员数据[n],名称=UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].角色.名称,华山论剑积分=0,华山论剑累计积分=0,门派=UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].角色.门派,等级=UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].角色.等级,模式=0,匹配=0,连胜次数=0,临时次数=0}
          end 
		  if 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].临时次数 >50 then
             SendMessage(UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].连接id, 7,"已超过50次天梯将不能获得奖励！")
           return
		 end 
	   if UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].队长  then 
        
      ---------------------队长奖励--------------------------------		            
	天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].临时次数 = 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].临时次数 + 1
    天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].连胜次数=天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].连胜次数+1
	
    if 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].连胜次数 >=2 and 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].连胜次数 < 10 then
      天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑积分 = 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑积分 + 5 --- 40
	  天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑累计积分 = 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑累计积分 + 5 --- 40
    SendMessage(UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].连接id, 7,"恭喜你获得了5点华山论剑积分")
      发送游戏公告("#G无人可挡，玩家#R"..天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].名称.."#G在华山论剑中初获"..天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].连胜次数.."连斩！！！")
    elseif 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].连胜次数 >=10 and 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].连胜次数 < 20 then
     天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑积分 = 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑积分 + 10 --100
	 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑累计积分 = 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑累计积分 + 10 --100
   SendMessage(UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].连接id, 7,"恭喜你获得了10点华山论剑积分")
      发送游戏公告("#G无人可挡，玩家#R"..天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].名称.."#G在华山论剑中十步杀一人，已获"..天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].连胜次数.."连斩！！！")
    elseif 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].连胜次数 >=20 and 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].连胜次数 < 30 then
      天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑积分 = 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑积分 + 20 --200
	  天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑累计积分 = 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑累计积分 + 20 --200
    SendMessage(UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].连接id, 7,"恭喜你获得了20点华山论剑积分")
      发送游戏公告("#G无人可挡，玩家#R"..天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].名称.."#G在华山论剑中杀人如麻，已获"..天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].连胜次数.."连斩！！！")
    elseif 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].连胜次数 >=30 and 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].连胜次数 < 40 then
      天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑积分 = 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑积分 + 25 --250
	  天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑累计积分 = 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑累计积分 + 25 --250
    SendMessage(UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].连接id, 7,"恭喜你获得了25点华山论剑积分")
      发送游戏公告("#G无人可挡，玩家#R"..天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].名称.."#G在华山论剑中遇神杀神、遇魔杀魔，已获"..天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].连胜次数.."连战斩！！！")
	  elseif 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].连胜次数 >=40 and 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].连胜次数 <= 50 then
      天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑积分 = 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑积分 + 30 --300
	  天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑累计积分 = 天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑累计积分 + 30 --300
    SendMessage(UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].连接id, 7,"恭喜你获得了30点华山论剑积分")
      发送游戏公告("#G无人可挡，玩家#R"..天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].名称.."#G在华山论剑中遇神杀神、遇魔杀魔，已获"..天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].连胜次数.."连战斩！！！")
    end
    天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑积分=天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑积分+10 ---500+790
	天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑累计积分=天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].华山论剑累计积分+10 ---500+790
   SendMessage(UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].连接id, 7,"恭喜你获得了10点华山论剑积分")
     end
  
-----------------------------所有人奖励---------------------------------------		 
        RoleControl:添加仙玉(UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]],30,"华山论剑")
		--RoleControl:添加银子(UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]],200000, "华山论剑")
		
         local 奖励参数 = math.random(1,220)  --法宝任务书  -- 星辉石 1-3 --- 胜利才有2-6J星辉石 -- 随机1-5J宝石礼包叠加 --胜利随机5-8宝石礼包叠加 --胜利五宝礼盒--高级魔兽要诀
          if 奖励参数 <= 1 then
            ItemControl:GiveItem(队伍数据[UserData[胜利id].队伍].队员数据[n],"法宝任务书",1) 
          广播消息("#G玩家#R"..天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].名称.."#G在华山论剑中表现极佳，获得额外奖励#法宝任务书")
        elseif 奖励参数 <= 3 then
		
        ItemControl:GiveItem(队伍数据[UserData[胜利id].队伍].队员数据[n],"修炼果",1) 
        广播消息("#G玩家#R"..天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].名称.."#G在华山论剑中表现极佳，获得额外奖励#R星辉石1—3J礼包")
        elseif 奖励参数 <= 5 then
        ItemControl:GiveItem(队伍数据[UserData[胜利id].队伍].队员数据[n], "魔兽要诀",1)
        广播消息("#G玩家#R"..天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].名称.."#G在华山论剑中表现极佳，获得额外奖励#R星辉石2—6J礼包")
       
        elseif 奖励参数 <= 7 then
       ItemControl:GiveItem(队伍数据[UserData[胜利id].队伍].队员数据[n], "梦幻瓜子",1)
        广播消息("#G玩家#R"..天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].名称.."#G在华山论剑中表现极佳，获得额外奖励#R宝石1—5J礼包")
       
       
        elseif 奖励参数 <= 9 then
      ItemControl:GiveItem(队伍数据[UserData[胜利id].队伍].队员数据[n], "梦幻西瓜",1)
        广播消息("#G玩家#R"..天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].名称.."#G在华山论剑中表现极佳，获得额外奖励#R宝石5—8J礼包")
       
        elseif 奖励参数 <= 11 then
            ItemControl:GiveItem(队伍数据[UserData[胜利id].队伍].队员数据[n],"五宝礼盒",1)
            广播消息("#G玩家#R"..天梯匹配[队伍数据[UserData[胜利id].队伍].队员数据[n]].名称.."#G在华山论剑中表现极佳，获得额外奖励#R五宝礼盒")
         end
      
		 SendMessage(UserData[队伍数据[UserData[胜利id].队伍].队员数据[n]].连接id, 7,"恭喜你获得了胜利，请再次点击备战，进入备战状态")
		end
	end
	
	
	---------------------------------失败-------------------------------------
  if (UserData[失败id].队伍 == 0 or  #队伍数据[UserData[失败id].队伍].队员数据 < 3 or 取队伍符合等级(失败id, 69) == false) and DebugMode== false  then
		
		   SendMessage(UserData[失败id].连接id, 7, "#y/队伍异常没有奖励")
	 else
      for n = 1, #队伍数据[UserData[失败id].队伍].队员数据 do
	      if 天梯匹配[队伍数据[UserData[失败id].队伍].队员数据[n]]==nil then
            天梯匹配[队伍数据[UserData[失败id].队伍].队员数据[n]]={id=队伍数据[UserData[失败id].队伍].队员数据[n],名称=UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].角色.名称,华山论剑积分=0,华山论剑累计积分=0,门派=UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].角色.门派,等级=UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].角色.等级,模式=0,匹配=0,连胜次数=0,临时次数=0}
          end 
		  if 天梯匹配[队伍数据[UserData[失败id].队伍].队员数据[n]].临时次数 >50 then
             SendMessage(UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].连接id, 7,"已超过50次天梯将不能获得奖励！")
           return
		 end 
	  天梯匹配[队伍数据[UserData[失败id].队伍].队员数据[n]].连胜次数 = 0
	  天梯匹配[队伍数据[UserData[失败id].队伍].队员数据[n]].临时次数 = 天梯匹配[队伍数据[UserData[失败id].队伍].队员数据[n]].临时次数 + 1
-----------------------------所有人奖励---------------------------------------		 
        RoleControl:添加仙玉(UserData[队伍数据[UserData[失败id].队伍].队员数据[n]],5,"华山论剑")
		
		
         local 奖励参数 = math.random(1,220)  --法宝任务书  -- 星辉石 1-3 --- 胜利才有2-6J星辉石 -- 随机1-5J宝石礼包叠加 --胜利随机5-8宝石礼包叠加 --胜利五宝礼盒--高级魔兽要诀
          if 奖励参数 <= 1 then
            ItemControl:GiveItem(队伍数据[UserData[失败id].队伍].队员数据[n],"法宝任务书",1) 
          广播消息("#G玩家#R"..天梯匹配[队伍数据[UserData[失败id].队伍].队员数据[n]].名称.."#G在华山论剑中战斗失败,再接再厉,获得安慰奖#法宝任务书")
        elseif 奖励参数 <= 3 then
		
        ItemControl:GiveItem(队伍数据[UserData[失败id].队伍].队员数据[n],"修炼果",1) 
        广播消息("#G玩家#R"..天梯匹配[队伍数据[UserData[失败id].队伍].队员数据[n]].名称.."#G在华山论剑中战斗失败,再接再厉,获得安慰奖#R星辉石1—3J礼包")
        elseif 奖励参数 <= 5 then
        ItemControl:GiveItem(队伍数据[UserData[失败id].队伍].队员数据[n], "魔兽要诀",1)
        广播消息("#G玩家#R"..天梯匹配[队伍数据[UserData[失败id].队伍].队员数据[n]].名称.."#G在华山论剑中战斗失败,再接再厉,获得安慰奖#R星辉石2—6J礼包")
       
        elseif 奖励参数 <= 7 then
       ItemControl:GiveItem(队伍数据[UserData[失败id].队伍].队员数据[n], "梦幻瓜子",1)
        广播消息("#G玩家#R"..天梯匹配[队伍数据[UserData[失败id].队伍].队员数据[n]].名称.."#G在华山论剑中战斗失败,再接再厉,获得安慰奖#R宝石1—5J礼包")
       
       
        elseif 奖励参数 <= 9 then
      ItemControl:GiveItem(队伍数据[UserData[失败id].队伍].队员数据[n], "梦幻西瓜",1)
        广播消息("#G玩家#R"..天梯匹配[队伍数据[UserData[失败id].队伍].队员数据[n]].名称.."#G在华山论剑中战斗失败,再接再厉,获得安慰奖#R宝石5—8J礼包")
       
        elseif 奖励参数 <= 10 then
            ItemControl:GiveItem(队伍数据[UserData[失败id].队伍].队员数据[n],"五宝礼盒",1)
            广播消息("#G玩家#R"..天梯匹配[队伍数据[UserData[失败id].队伍].队员数据[n]].名称.."#G在华山论剑中战斗失败,再接再厉,获得安慰奖#R五宝礼盒")
         end
      
		 SendMessage(UserData[队伍数据[UserData[失败id].队伍].队员数据[n]].连接id, 7,"很遗憾战斗失败了,请再次点击备战,进入备战状态")
	 end		
		
end
end



function GameActivities:刷新华山论剑排行(id)
    
	

      local 发送信息 = {}
	   local 名单 = {}
	    if 天梯匹配[id]==nil then
          天梯匹配[id]={id=id,名称=UserData[id].角色.名称,华山论剑积分=0,华山论剑累计积分=0,门派=UserData[id].角色.门派,等级=UserData[id].角色.等级,匹配=0,连胜次数=0,临时次数=0}
        end 
	   for b, v in pairs(天梯匹配) do
          名单[#名单+1]=v
        end
       table.sort(名单,function(a,b) return a.华山论剑累计积分>b.华山论剑累计积分 end )
	      for i=1,10 do
	         if 名单[i] ~= nil then
            发送信息[i] = 名单[i]  
           end
        end
	    SendMessage(UserData[id].连接id,6017,发送信息)


end

function GameActivities:首席争霸战斗处理(胜利id, 失败id)
	self.临时门派 = UserData[胜利id].角色.门派
	self.首席玩家[self.临时门派][UserData[胜利id].id] = self.首席玩家[self.临时门派][UserData[胜利id].id] + self.首席玩家[self.临时门派][UserData[失败id].id]

	SendMessage(UserData[胜利id].连接id, 7, "#y/你获得了" .. self.首席玩家[self.临时门派][UserData[失败id].id] .. "点比赛积分")
	SendMessage(UserData[胜利id].连接id, 7, "#y/你当前的比赛积分为" .. self.首席玩家[self.临时门派][UserData[胜利id].id] .. "点")
	MapControl:Jump(失败id, 1001, 364, 36, 1)

	self.奖励经验 = UserData[失败id].角色.等级 * UserData[失败id].角色.等级 * 50

	RoleControl:添加经验(UserData[失败id],self.奖励经验, "首席争霸")
	SendMessage(UserData[失败id].连接id, 7, "#y/很遗憾，你失去了竞逐首席弟子的资格")

	self.首席玩家[self.临时门派][UserData[失败id].id] = nil
	self.参加人数 = 0

	for n, v in pairs(self.首席玩家[self.临时门派]) do
		if UserData[n] ~= nil then
			self.参加人数 = self.参加人数 + 1
		end
	end

	UserData[胜利id].首席保护 = os.time()

	if self.参加人数 == 1 then
		self.奖励经验 = math.floor(UserData[胜利id].角色.等级 * UserData[胜利id].角色.等级 * 50)

		RoleControl:添加经验(UserData[失败id],self.奖励经验,"首席争霸")

		首席资源数据[self:取门派编号(self.临时门派)].玩家id = UserData[胜利id].id

		广播消息(9, "#xt/#y/经过激烈的竞逐，#g/" .. UserData[胜利id].角色.名称 .. "#y/以#r/" .. self.首席玩家[self.临时门派][UserData[胜利id].id] .. "#y/点比赛积分成功夺得担任" .. self.临时门派 .. "的首席弟子资格。")
		SendMessage(UserData[胜利id].连接id, 7, "#y/你现在可以前往首席弟子处领取首席称谓了")
		RoleControl:添加系统消息(UserData[胜利id], "#h/恭喜你获得了担任本门派首席的资格，请在下次首席争霸赛开启前前往本门派首席大弟子处领取首席称谓")

		self.首席玩家[self.临时门派] = nil
		self.统计门派 = self.统计门派 + 1

		MapControl:Jump(胜利id, 1001, 364, 36, 1)
	end

	if self.统计门派 >= 18 then
		发送游戏公告("首席争霸赛活动已经结束。已经获得本门派首席弟子资格的玩家可前往本门派首席弟子处领取首席弟子称谓。首席弟子称谓以及造型将在每天的首席争霸赛活动开启前失效。")

		首席争霸赛战斗开关 = false
	end
end

function GameActivities:取门派编号(门派)
	for n = 1, 18 do
		if 首席弟子门派类[n] == 门派 then
			return n
		end
	end
end

function GameActivities:结束首席争霸门派(门派, 类型, id, 编号)
	if 类型 == 1 then
		广播消息(9, "#xt/#y/" .. 门派 .. "因无人参加本门派首席争霸赛，本门派首席弟子将不再刷新。")

		self.首席玩家[门派] = nil
	elseif 类型 == 2 then
		首席资源数据[编号].玩家id = UserData[id].id
         RoleControl:添加活跃度(UserData[id],30)
		广播消息(9, "#xt/#y/因无其他玩家参加本门派首席争霸赛，#g/" .. UserData[id].角色.名称 .. "#y/成功夺得担任" .. 门派 .. "的首席弟子资格。")
		SendMessage(UserData[id].连接id, 7, "#y/你现在可以前往首席弟子处领取首席称谓了")
		SendMessage(UserData[id].连接id, 7, "#y/因只有你一人参加了本次比赛，所以无法获得奖励")
		RoleControl:添加系统消息(UserData[id], "#h/恭喜你获得了担任本门派首席的资格，请在下次首席争霸赛开启前前往本门派首席大弟子处领取首席称谓")
		MapControl:Jump(id, 1001, 364, 36)

		self.首席玩家[门派] = nil
	elseif 类型 == 3 then
		for n, v in pairs(FightGet.战斗盒子) do
			if FightGet.战斗盒子[n] ~= nil then
				FightGet.战斗盒子[n]:强制结束战斗()
			end
		end

		for n = 1, 18 do


			if self.首席玩家[首席弟子门派类[n]] ~= nil then
				self.最高积分 = 0
				self.最高id = 0

				for i, v in pairs(self.首席玩家[首席弟子门派类[n]]) do
					if UserData[i] ~= nil then
						self.奖励经验 = math.floor(UserData[i].角色.等级 * UserData[i].角色.等级 * 50)

						RoleControl:添加经验(UserData[i],self.奖励经验, "首席争霸")
						MapControl:Jump(i, 1001, 364, 36)
						SendMessage(UserData[i].连接id, 7, "#y/由于比赛时间结束，你被强制传出比赛地图")

						if self.最高积分 < self.首席玩家[首席弟子门派类[n]][i] then
							self.最高积分 = self.首席玩家[首席弟子门派类[n]][i]
							self.最高id = i
						end
					end
				end

				if self.最高积分 ~= 0 then
					广播消息(9, "#xt/#y/经过激烈的竞逐，#g/" .. UserData[self.最高id].角色.名称 .. "#y/以#r/" .. self.首席玩家[首席弟子门派类[n]][UserData[self.最高id].id] .. "#y/点比赛积分成功夺得担任" .. 首席弟子门派类[n] .. "的首席弟子资格。")
					SendMessage(UserData[self.最高id].连接id, 7, "#y/你现在可以前往首席弟子处领取首席称谓了")
					RoleControl:添加系统消息(UserData[self.最高id], "#h/恭喜你获得了担任本门派首席的资格，请在下次首席争霸赛开启前前往本门派首席大弟子处领取首席称谓")

					首席资源数据[self:取门派编号(首席弟子门派类[n])].玩家id = UserData[self.最高id].id
				end
			end
		end

		发送游戏公告("首席争霸赛活动已经结束。已经获得本门派首席弟子资格的玩家可前往本门派首席弟子处领取首席弟子称谓。首席弟子称谓以及造型将在每天的首席争霸赛活动开启前失效。")

		首席争霸赛战斗开关 = false
	elseif 类型 == 4 then
		首席资源数据[编号].玩家id = UserData[id].id

		广播消息(9, "#xt/#y/因比赛场地内无其他玩家，#g/" .. UserData[id].角色.名称 .. "#y/成功夺得担任" .. 门派 .. "的首席弟子资格。")
		SendMessage(UserData[id].连接id, 7, "#y/你现在可以前往首席弟子处领取首席称谓了")
		RoleControl:添加系统消息(UserData[id], "#h/恭喜你获得了担任本门派首席的资格，请在下次首席争霸赛开启前前往本门派首席大弟子处领取首席称谓")

		self.奖励经验 = math.floor(UserData[id].角色.等级 * UserData[id].角色.等级 * 50)

		RoleControl:添加经验(UserData[id],self.奖励经验,"首席争霸")
		MapControl:Jump(id, 1001, 364, 36)

		self.首席玩家[门派] = nil
	end
end



return GameActivities
