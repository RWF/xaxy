--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-03-03 19:10:50
--======================================================================--
local 商会处理类 = class()
function 商会处理类:数据处理(id, 参数, 序号, 内容)
	if 序号 == 121 then
		if 物品店数据[id] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/你现在还没有物品商铺")

			return 0
		else
			物品店数据[id][1].店名 = 内容

			SendMessage(UserData[id].连接id, 7, "#y/物品店铺更名成功")
		end
	elseif 序号 == 122 then
		if 物品店数据[id] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/你现在还没有物品商铺")
			return 0
		else
			物品店数据[id][1].宣言 = 内容

			SendMessage(UserData[id].连接id, 7, "#y/宗旨更改成功")
		end
	elseif 序号 == 123 then
		if 物品店数据[id] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/你现在还没有物品商铺")
			return 0
		else
			SendMessage(UserData[id].连接id,20,{"商会总管","商会总管"," 扩张物品柜台需要消耗100点仙玉，请确认你是否要进行此操作？",{"确认扩张","我再想想"}})
		end
	elseif 序号 == 124 then
		self:登记商品处理(id, 参数, 内容)
	elseif 序号 == 12006 then
		self:上柜商品处理(id, 内容)
	elseif 序号 == 125 then
		self:取回商品处理(id, 参数, 内容)
	elseif 序号 == 126 then
		self:更改营业状态(id, 内容)
	elseif 序号 == 127 then
		内容 = 内容 + 0
		if 内容 == 1 then
			SendMessage(UserData[id].连接id, 7, "#y/这已经是第一个柜台了")
		else
			if 参数 == 1 then
				self:索取柜台商品信息(id, 内容 - 1)
			elseif 参数 == 2 then
				self:索取柜台唤兽信息(id, 内容 - 1)
			end
		end
	elseif 序号 == 128 then
		内容 = 内容 + 0
		if 参数 == 1 then
			if 内容 >= #物品店数据[id][1].店面 then
				SendMessage(UserData[id].连接id, 7, "#y/这已经是最后一个柜台了")
			else
				self:索取柜台商品信息(id, 内容 + 1)
			end
		elseif 参数 == 2 then
			if 内容 >= #唤兽店数据[id][1].店面 then
				SendMessage(UserData[id].连接id, 7, "#y/这已经是最后一个柜台了")
			else
				self:索取柜台唤兽信息(id, 内容 + 1)
			end
		end
	elseif 序号 == 129 then
		self:取出资金(id,参数)
	elseif 序号 == 130 then
		if 参数 == 1 then
			self:取查看商品信息(id, 内容 + 0, 1)
		elseif 参数 == 2 then
			self:取查看唤兽信息(id, 内容 + 0, 1)
		end
	elseif 序号 == 131 then
		self.换铺数据 = {
			类型 = 参数+0,
			商铺id = 内容+0
		}
		if self.换铺数据.类型 <= 1 then
			SendMessage(UserData[id].连接id, 7, "#y/这已经是第一个柜台了")
		else
			self:取查看商品信息(id, self.换铺数据.商铺id, self.换铺数据.类型 - 1)
		end
	elseif 序号 == 132 then
		self.换铺数据 = {
			类型 = 参数+0,
			商铺id = 内容+0
		}
		if self.换铺数据.类型 >= #物品店数据[self.换铺数据.商铺id][1].店面 then
			SendMessage(UserData[id].连接id, 7, "#y/这已经是最后一个柜台了")
		else
			self:取查看商品信息(id, self.换铺数据.商铺id, self.换铺数据.类型 + 1)
		end
	elseif 序号 == 133 then
		self:登记唤兽处理(id, 参数, 内容)
	elseif 序号 == 134 then
		if 唤兽店数据[id] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/你现在还没有唤兽商铺")
			return 0
		else
			唤兽店数据[id][1].店名 = 内容

			SendMessage(UserData[id].连接id, 7, "#y/唤兽店铺更名成功")
		end
	elseif 序号 == 135 then
		if 唤兽店数据[id] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/你现在还没有唤兽商铺")
			return 0
		else
			唤兽店数据[id][1].宣言 = 内容
			SendMessage(UserData[id].连接id, 7, "#y/宗旨更改成功")
		end
	elseif 序号 == 136 then
		if 唤兽店数据[id] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/你现在还没有唤兽商铺")
			return 0
		else
			SendMessage(UserData[id].连接id,20,{"商会总管","商会总管"," 扩张唤兽柜台需要消耗100点仙玉，请确认你是否要进行此操作？",{"我要扩张","我再想想"}})
		end
	elseif 序号 == 137 then
		self:取回唤兽处理(id, 参数, 内容)
	elseif 序号 == 138 then
		self:更改营业状态2(id, 内容)
	elseif 序号 == 139 then
		self.换铺数据 = {
			类型 = 参数,
			商铺id = 内容+0
		}
		if self.换铺数据.类型 <= 1 then
			SendMessage(UserData[id].连接id, 7, "#y/这已经是第一个柜台了")
		else
			self:取查看唤兽信息(id, self.换铺数据.商铺id, self.换铺数据.类型 - 1)
		end
	elseif 序号 == 140 then
		self.换铺数据 = {
			类型 = 参数,
			商铺id = 内容+0
		}
		if self.换铺数据.类型 >= #唤兽店数据[self.换铺数据.商铺id][1].店面 then
			SendMessage(UserData[id].连接id, 7, "#y/这已经是最后一个柜台了")
		else
			self:取查看唤兽信息(id, self.换铺数据.商铺id, self.换铺数据.类型 + 1)
		end
	elseif 序号 == 141 then
		if 参数 == 1 then
			self:获取物品商铺信息(id)
		elseif 参数 == 2 then
			self:获取唤兽商铺信息(id)
		else
			self:获取商会商铺信息(id)
		end	   
	end
end

function 商会处理类:购买商品处理(id, 参数, 序号, 类型, 数量)
	self.临时数据2 = {
		类型 = 参数,
		商品id = 序号,
		商铺id = 类型+0,
		商品数量 = 数量+0
	}
	if UserData[id].入店时间 <= 物品店数据[self.临时数据2.商铺id][1].刷新时间 then
		SendMessage(UserData[id].连接id, 7, "#y/该店的商品信息已经发生变化，请重新查看")

		return 0
	elseif UserData[id].角色.等级 < 60 then
		SendMessage(UserData[id].连接id, 7, "#y/你的等级低于60级，无法购买商会中的商品")

		return 0
	elseif 物品店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/这个商品已经被其他玩家买走了")

		return 0
	elseif 银子检查(id, 物品店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id].价格*self.临时数据2.商品数量) == false then
		SendMessage(UserData[id].连接id, 7, "#y/你身上的现金不够哦")

		return 0
	elseif 物品店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id].id.数量 and  物品店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id].id.数量 < self.临时数据2.商品数量 then
		SendMessage(UserData[id].连接id, 7, "#y/商品库存不足")
		self:取查看商品信息(id, self.临时数据2.商铺id, self.临时数据2.类型)
		return 0
	else

		self.临时格子1 = RoleControl:取可用道具格子(UserData[id],"包裹")

		if self.临时格子1 == 0 then
			SendMessage(UserData[id].连接id, 7, "#y/你身上没有足够的空间")

			return 0
		else
			self.临时道具id = ItemControl:取道具编号(id)
			UserData[id].物品[self.临时道具id] = table.copy(物品店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id].id)

			UserData[id].角色.道具.包裹[self.临时格子1] = self.临时道具id

			if UserData[id].物品[self.临时道具id].数量 ~= nil then
				UserData[id].物品[self.临时道具id].数量 = self.临时数据2.商品数量
			end

			RoleControl:扣除银子(UserData[id], 物品店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id].价格*self.临时数据2.商品数量, "商会")

			物品店数据[self.临时数据2.商铺id][1].日常运营资金 = 物品店数据[self.临时数据2.商铺id][1].日常运营资金 + math.floor(物品店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id].价格*self.临时数据2.商品数量 * 0.9)

			if 物品店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id].id.数量 == nil then
				物品店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id] = nil
			else
				物品店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id].id.数量 = 物品店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id].id.数量 - self.临时数据2.商品数量

				if 物品店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id].id.数量 < 1 then
					物品店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id] = nil
				end
			end

			SendMessage(UserData[id].连接id, 7, "#y/购买商品成功")
			self:取查看商品信息(id, self.临时数据2.商铺id, self.临时数据2.类型)
		end
	end
end
function 商会处理类:购买唤兽处理(id, 参数, 序号, 类型, 数量)
	self.临时数据2 = {
		类型 = 参数,
		商品id = 序号,
		商铺id = 类型+0,
	}

	if UserData[id].入店时间 <= 唤兽店数据[self.临时数据2.商铺id][1].刷新时间 then
		SendMessage(UserData[id].连接id, 7, "#y/该店的召唤兽信息已经发生变化")
		self:取查看唤兽信息(id, self.临时数据2.商铺id, self.临时数据2.类型)
		return 0
	elseif UserData[id].角色.等级 < 60 then
		SendMessage(UserData[id].连接id, 7, "#y/你的等级低于60级，无法购买商会中的召唤兽")

		return 0
	elseif 唤兽店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/这个召唤兽已经被其他玩家买走了")
		return 0
	elseif 银子检查(id, 唤兽店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id].价格) == false then
		SendMessage(UserData[id].连接id, 7, "#y/你身上的现金不够哦")

		return 0
	else

		if #UserData[id].召唤兽.数据 >= UserData[id].召唤兽.数据.召唤兽上限 then
		    SendMessage(UserData[id].连接id, 7, "#y/你当前可携带的召唤兽数量已达上限")
			return 0
		else
			RoleControl:扣除银子(UserData[id],唤兽店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id].价格, "商会")
			唤兽店数据[self.临时数据2.商铺id][1].日常运营资金 = 唤兽店数据[self.临时数据2.商铺id][1].日常运营资金 + math.floor(唤兽店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id].价格 * 0.9)
			UserData[id].召唤兽.数据[#UserData[id].召唤兽.数据+1] = table.copy(唤兽店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型][self.临时数据2.商品id].id)
			table.remove(唤兽店数据[self.临时数据2.商铺id][1].店面[self.临时数据2.类型], self.临时数据2.商品id)
			SendMessage(UserData[id].连接id, 7, "#y/购买召唤兽成功")
			self:取查看唤兽信息(id, self.临时数据2.商铺id, self.临时数据2.类型)
			UserData[id].入店时间=os.time()+1
			唤兽店数据[self.临时数据2.商铺id][1].刷新时间=os.time()

		end
	end
end

function 商会处理类:取回商品处理(id, 格子, 类型)
	self.临时数据 = {
		格子 = 格子,
		类型 = tonumber(类型)
	}

	if 物品店数据[id][1].店面[self.临时数据.类型][self.临时数据.格子] ~= nil then
		if 物品店数据[id][1].店面[self.临时数据.类型][self.临时数据.格子].状态 then
			SendMessage(UserData[id].连接id, 7, "#y/出售中的商品无法取回")

			return 0
		end

		self.临时格子 = RoleControl:取可用道具格子(UserData[id],"包裹")

		if self.临时格子 == 0 then
			SendMessage(UserData[id].连接id, 7, "#y/你的身上空间不足")
		else
			self.道具id = ItemControl:取道具编号(id)
			UserData[id].物品[self.道具id] = table.copy(物品店数据[id][1].店面[self.临时数据.类型][self.临时数据.格子].id)
			UserData[id].角色.道具.包裹[self.临时格子] = self.道具id
			物品店数据[id][1].店面[self.临时数据.类型][self.临时数据.格子] = nil
			SendMessage(UserData[id].连接id, 11004, ItemControl:索要道具1(id, "包裹"))
			SendMessage(UserData[id].连接id, 7, "#y/取回商品成功")
			self:索取柜台商品信息(id, self.临时数据.类型)
		end
	else
		SendMessage(UserData[id].连接id, 7, "#y/这个道具并不存在")
	end
end

function 商会处理类:取回唤兽处理(id, 格子, 类型)
	self.临时数据 = {
		格子 = 格子,
		类型 = 类型 + 0
	}

	if 唤兽店数据[id][1].店面[self.临时数据.类型][self.临时数据.格子] ~= nil then
		if 唤兽店数据[id][1].店面[self.临时数据.类型][self.临时数据.格子].状态 then
			SendMessage(UserData[id].连接id, 7, "#y/出售中的召唤兽无法取回")
			return 0
		end
		if #UserData[id].召唤兽.数据 >= UserData[id].召唤兽.数据.召唤兽上限 then
		    SendMessage(UserData[id].连接id, 7, "#y/你当前可携带的召唤兽数量已达上限")
		else
		    																			
			UserData[id].召唤兽.数据[#UserData[id].召唤兽.数据+1] = table.copy(唤兽店数据[id][1].店面[self.临时数据.类型][self.临时数据.格子].id)
			table.remove(唤兽店数据[id][1].店面[self.临时数据.类型], self.临时数据.格子)
			SendMessage(UserData[id].连接id, 10004, UserData[id].召唤兽:获取数据())
			SendMessage(UserData[id].连接id, 7, "#y/取回召唤兽成功")
			self:索取柜台唤兽信息(id, self.临时数据.类型)
		end
	else
		SendMessage(UserData[id].连接id, 7, "#y/这个召唤兽并不存在")
	end
end



function 商会处理类:上柜商品处理(id, 参数, 序号, 内容, 类型)
	self.临时数据 = {
		格子 = 参数,
		类型 = 序号+0,
		价格 = math.floor(内容)
	}
	if 类型==1 then
		if 物品店数据[id][1].店面[self.临时数据.类型][self.临时数据.格子] ~= nil then
			物品店数据[id][1].刷新时间 = os.time()

			if 物品店数据[id][1].店面[self.临时数据.类型][self.临时数据.格子].状态 then
				物品店数据[id][1].店面[self.临时数据.类型][self.临时数据.格子].状态 = false
			else
				物品店数据[id][1].店面[self.临时数据.类型][self.临时数据.格子].状态 = true
				物品店数据[id][1].店面[self.临时数据.类型][self.临时数据.格子].价格 = self.临时数据.价格
			end

			SendMessage(UserData[id].连接id, 7, "#y/商品状态更改成功")
			self:索取柜台商品信息(id, self.临时数据.类型)
		else
			SendMessage(UserData[id].连接id, 7, "#y/这个道具并不存在")
		end
	else
		if 唤兽店数据[id][1].店面[self.临时数据.类型][self.临时数据.格子] ~= nil then
			唤兽店数据[id][1].刷新时间 = os.time()

			if 唤兽店数据[id][1].店面[self.临时数据.类型][self.临时数据.格子].状态 then
				唤兽店数据[id][1].店面[self.临时数据.类型][self.临时数据.格子].状态 = false
			else
				唤兽店数据[id][1].店面[self.临时数据.类型][self.临时数据.格子].状态 = true
				唤兽店数据[id][1].店面[self.临时数据.类型][self.临时数据.格子].价格 = self.临时数据.价格
			end

			SendMessage(UserData[id].连接id, 7, "#y/召唤兽状态更改成功")
			self:索取柜台唤兽信息(id, self.临时数据.类型)
		else
			SendMessage(UserData[id].连接id, 7, "#y/这个召唤兽并不存在")
		end
	end
end


function 商会处理类:登记商品处理(id, 格子, 类型)
	self.临时数据 = {
		类型 = 类型+0,
		格子 = 格子
	}
	self.空余位置 = 0
	for n = 1, 20 do
		if self.空余位置 == 0 and 物品店数据[id][1].店面[self.临时数据.类型][n] == nil then
			self.空余位置 = n
		end
	end

	if self.空余位置 == 0 then
		SendMessage(UserData[id].连接id, 7, "#y/该柜台已经无法摆放更多的商品了")
		return 0
	elseif UserData[id].物品[UserData[id].角色.道具.包裹[self.临时数据.格子]].加锁 then
		SendMessage(UserData[id].连接id, 7, "#y/请先解锁")
		return 0
	elseif UserData[id].物品[UserData[id].角色.道具.包裹[self.临时数据.格子]].Time then
		SendMessage(UserData[id].连接id, 7, "#y/限时道具无法登记")
		return 0
	elseif UserData[id].物品[UserData[id].角色.道具.包裹[self.临时数据.格子]].类型 == "剧情道具" and 可交易剧情道具(UserData[id].物品[UserData[id].角色.道具.包裹[self.临时数据.格子]].名称)==false then
		SendMessage(UserData[id].连接id, 7, "#y/剧情道具无法登记")
		return 0
	elseif UserData[id].物品[UserData[id].角色.道具.包裹[self.临时数据.格子]].类型 == "古董" then
		SendMessage(UserData[id].连接id, 7, "#y/古董无法登记")
		return 0
	elseif UserData[id].物品[UserData[id].角色.道具.包裹[self.临时数据.格子]].数量 and UserData[id].物品[UserData[id].角色.道具.包裹[self.临时数据.格子]].数量>999 then
         错误日志=错误日志.."\n"..os.date("[%Y年%m月%d日%X]")..":ID"..id .."非法数据，商会"..UserData[id].物品[UserData[id].角色.道具.包裹[self.临时数据.格子]].名称.."数量为"..UserData[id].物品[UserData[id].角色.道具.包裹[self.临时数据.格子]].数量
         错误数目=错误数目+1
         封禁账号(UserData[id],"商会数量超过")
        return 0
	else
		物品店数据[id][1].店面[self.临时数据.类型][self.空余位置] = {
			状态 = false,
			价格 = 0,
			id = table.copy(UserData[id].物品[UserData[id].角色.道具.包裹[self.临时数据.格子]])
		}
		UserData[id].物品[UserData[id].角色.道具.包裹[self.临时数据.格子]] = nil
		UserData[id].角色.道具.包裹[self.临时数据.格子] = nil
		SendMessage(UserData[id].连接id, 7, "#y/道具成功添加至柜台")
		SendMessage(UserData[id].连接id, 11004, ItemControl:索要道具1(id, "包裹"))
		self:索取柜台商品信息(id, self.临时数据.类型)
	end
end

function 商会处理类:登记唤兽处理(id, 格子, 类型)
	self.临时数据 = {
		类型 = 类型+0,
		格子 = 格子+0
	}
	self.空余位置 = 0
	for n = 1, 10 do
		if self.空余位置 == 0 and 唤兽店数据[id][1].店面[self.临时数据.类型][n] == nil then
			self.空余位置 = n
		end
	end

	if self.空余位置 == 0 then
		SendMessage(UserData[id].连接id, 7, "#y/该柜台已经无法摆放更多的召唤兽了")
		return 0
	elseif not UserData[id].召唤兽.数据[self.临时数据.格子] then
		SendMessage(UserData[id].连接id, 7, "#y/你没有这样的召唤兽")
		return 0
	elseif UserData[id].召唤兽.数据.参战==self.临时数据.格子 then
		SendMessage(UserData[id].连接id, 7, "#y/参战的召唤兽无法登记")
		return 0
	elseif UserData[id].召唤兽.数据[self.临时数据.格子].类型 == "神兽" then
		SendMessage(UserData[id].连接id, 7, "#y/神兽无法登记")
		return 0
	elseif UserData[id].召唤兽.数据[self.临时数据.格子].加锁 then
		SendMessage(UserData[id].连接id, 7, "#y/请先解锁")
		return 0
	elseif 取表数量(UserData[id].召唤兽.数据[self.临时数据.格子].装备)  > 0 then
		return 0
	else

		唤兽店数据[id][1].店面[self.临时数据.类型][self.空余位置] = {
			状态 = false,
			价格 = 0,
			id = table.copy(UserData[id].召唤兽.数据[self.临时数据.格子])
		}		
		table.remove(UserData[id].召唤兽.数据, self.临时数据.格子)
		SendMessage(UserData[id].连接id, 7, "#y/该召唤兽已经成功添加至柜台")
		SendMessage(UserData[id].连接id, 10004, UserData[id].召唤兽:获取数据())

		self:索取柜台唤兽信息(id, self.临时数据.类型)
	end
end



function 商会处理类:扩张柜台(id)
	if #物品店数据[id][1].店面 >= 10 then
		SendMessage(UserData[id].连接id, 7, "#y/柜台数量最多只能为10间")

		return 0
	elseif RoleControl:扣除仙玉(UserData[id],100,"物品柜台") == false then
		SendMessage(UserData[id].连接id, 7, "#y/你没有那么多的仙玉")

		return 0
	else
		物品店数据[id][1].店面[#物品店数据[id][1].店面 + 1] = {}
		SendMessage(UserData[id].连接id, 7, "#y/扩张柜台成功")
		SendMessage(UserData[id].连接id, 11002, #物品店数据[id][1].店面)
		self:发送管理物品商铺(id)
	end
end

function 商会处理类:扩张唤兽柜台(id)
	if #唤兽店数据[id][1].店面 >= 10 then
		SendMessage(UserData[id].连接id, 7, "#y/唤兽柜台数量最多只能为10间")

		return 0
	elseif RoleControl:扣除仙玉(UserData[id],100,"唤兽柜台") == false then
		SendMessage(UserData[id].连接id, 7, "#y/你没有那么多的仙玉")

		return 0
	else
		唤兽店数据[id][1].店面[#唤兽店数据[id][1].店面 + 1] = {}
		SendMessage(UserData[id].连接id, 7, "#y/扩张唤兽柜台成功")
		SendMessage(UserData[id].连接id, 11002, #唤兽店数据[id][1].店面)
		self:发送管理唤兽商铺(id)
	end
end



function 商会处理类:取出资金(id,参数)
	if 参数 == 1 then
		if 物品店数据[id] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有物品商铺吧")

			return 0
		elseif 物品店数据[id][1].日常运营资金 <= 0 then
			SendMessage(UserData[id].连接id, 7, "#y/你当前没有可取出的资金")

			return 0
		else
			RoleControl:添加银子(UserData[id], 物品店数据[id][1].日常运营资金, "商会")
			物品店数据[id][1].日常运营资金 = 0
			SendMessage(UserData[id].连接id, 7, "#y/取出资金成功")
		    SendMessage(UserData[id].连接id, 11008, UserData[id].角色.道具.货币.银子)

		end
	else
		if 唤兽店数据[id] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有唤兽商铺吧")

			return 0
		elseif 唤兽店数据[id][1].日常运营资金 <= 0 then
			SendMessage(UserData[id].连接id, 7, "#y/你当前没有可取出的资金")

			return 0
		else
			RoleControl:添加银子(UserData[id], 唤兽店数据[id][1].日常运营资金, "商会")
			唤兽店数据[id][1].日常运营资金 = 0
			SendMessage(UserData[id].连接id, 7, "#y/取出资金成功")
			SendMessage(UserData[id].连接id, 11003, UserData[id].角色.道具.货币.银子)
		end
	end
end




function 商会处理类:更改营业状态(id)
	if 物品店数据[id] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有商铺吧")
		return 0
	elseif 物品店数据[id][1].营业 then
		物品店数据[id][1].刷新时间 = os.time()
		物品店数据[id][1].营业 = false

		SendMessage(UserData[id].连接id, 7, "#y/你的这间商铺已经暂停营业了")
		SendMessage(UserData[id].连接id, 11006, "1")
	else
		物品店数据[id][1].营业 = true

		SendMessage(UserData[id].连接id, 7, "#y/你的这间商铺已经开始营业了")
		SendMessage(UserData[id].连接id, 11007, "1")
	end
end

function 商会处理类:更改营业状态2(id)
	if 唤兽店数据[id] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有商铺吧")
		return 0
	elseif 唤兽店数据[id][1].营业 then
		唤兽店数据[id][1].刷新时间 = os.time()
		唤兽店数据[id][1].营业 = false

		SendMessage(UserData[id].连接id, 7, "#y/你的这间商铺已经暂停营业了")
		SendMessage(UserData[id].连接id, 9999, "1")
	else
		唤兽店数据[id][1].营业 = true

		SendMessage(UserData[id].连接id, 7, "#y/你的这间商铺已经开始营业了")
		SendMessage(UserData[id].连接id, 10000, "1")
	end
end



function 商会处理类:购买物品商铺(id)
	if 物品店数据[id] ~= nil then
		SendMessage(UserData[id].连接id, 7, "#y/目前每个玩家只允许购买一个物品商铺")

		return 0
	elseif UserData[id].角色.等级 < 65 then
		SendMessage(UserData[id].连接id, 7, "#y/等级未达到65级的玩家无法在我这里购买物品商铺")

		return 0
	elseif 银子检查(id,10000000) ==false then
		SendMessage(UserData[id].连接id, 7, "#y/你没有那么多的银子")


		return 0
	else
		RoleControl:扣除银子(UserData[id],10000000,"购买店铺")
		物品店数据[id] = {
			{
				营业 = false,
				基础运营资金 = 0,
				日志记录 = "",
				宣言 = "",
				店名 = "请取个店名",
				类型 = "物品",
				基础营业费用 = 0,
				日常运营资金 = 0,
				店面 = {{}},
				创店日期 = os.date("%Y-%m-%d"),
				店主名称 = UserData[id].角色.名称,
				店主id = id,
				刷新时间 = os.time()
			}
		}
		商会数据[#商会数据+1]={铺主id=id,类型="物品"}
		SendMessage(UserData[id].连接id, 7, "#y/购买物品商铺成功，你现在可以管理你的商铺了")
	end
end

function 商会处理类:购买唤兽商铺(id)
	if 唤兽店数据[id] ~= nil then
		SendMessage(UserData[id].连接id, 7, "#y/目前每个玩家只允许购买一个唤兽商铺")

		return 0
	elseif UserData[id].角色.等级 < 65 then
		SendMessage(UserData[id].连接id, 7, "#y/等级未达到65级的玩家无法在我这里购买唤兽商铺")

		return 0
	elseif 银子检查(id,10000000) ==false then
		SendMessage(UserData[id].连接id, 7, "#y/你没有那么多的银子")

		return 0
	else
		RoleControl:扣除银子(UserData[id],10000000,"购买店铺")
		唤兽店数据[id] = {
			{
				营业 = false,
				基础运营资金 = 0,
				日志记录 = "",
				宣言 = "",
				店名 = "请取个店名",
				类型 = "唤兽",
				基础营业费用 = 0,
				日常运营资金 = 0,
				店面 = {{}},
				创店日期 = os.date("%Y-%m-%d"),
				店主名称 = UserData[id].角色.名称,
				店主id = id,
				刷新时间 = os.time()
			}
		}
		商会数据[#商会数据+1]={铺主id=id,类型="唤兽"}
		SendMessage(UserData[id].连接id, 7, "#y/购买唤兽商铺成功，你现在可以管理你的商铺了")
	end
end




function 商会处理类:取查看商品信息(id, 出售id, 序号)
	self.发送信息1 = {}
	for n = 1, 20 do
		if 物品店数据[出售id][1].店面[序号][n] ~= nil and 物品店数据[出售id][1].店面[序号][n].状态 then
			self.发送信息1[n] = {
				道具 = 物品店数据[出售id][1].店面[序号][n].id,
				状态 = 物品店数据[出售id][1].店面[序号][n].状态,
				价格 = 物品店数据[出售id][1].店面[序号][n].价格
			}
		end
	end

	self.发送信息1.类型 = 序号
	self.发送信息1.店名 = 物品店数据[出售id][1].店名
	self.发送信息1.现金 = UserData[id].角色.道具.货币.银子
	self.发送信息1.店主名称 = 物品店数据[出售id][1].店主名称
	self.发送信息1.店主id = 物品店数据[出售id][1].店主id
	self.发送信息1.创店日期 = 物品店数据[出售id][1].创店日期
	self.发送信息1.店面 = #物品店数据[出售id][1].店面
	self.发送信息1.商铺id = 出售id
	UserData[id].入店时间 = os.time()

	SendMessage(UserData[id].连接id, 11010, self.发送信息1)
end

function 商会处理类:取查看唤兽信息(id, 出售id, 序号)
	self.发送信息1 = {}
	self.发送信息1.列表={}
	for n = 1, 10 do
		if 唤兽店数据[出售id][1].店面[序号][n] ~= nil and 唤兽店数据[出售id][1].店面[序号][n].状态 then
			self.发送信息1.列表[n] = {
				道具 = 唤兽店数据[出售id][1].店面[序号][n].id,
				状态 = 唤兽店数据[出售id][1].店面[序号][n].状态,
				价格 = 唤兽店数据[出售id][1].店面[序号][n].价格
			}
		else
		    self.发送信息1.列表[n] = {}
		end
	end

	self.发送信息1.类型 = 序号
	self.发送信息1.店名 = 唤兽店数据[出售id][1].店名
	self.发送信息1.现金 = UserData[id].角色.道具.货币.银子
	self.发送信息1.店主名称 = 唤兽店数据[出售id][1].店主名称
	self.发送信息1.店主id = 唤兽店数据[出售id][1].店主id
	self.发送信息1.创店日期 = 唤兽店数据[出售id][1].创店日期
	self.发送信息1.店面 = #唤兽店数据[出售id][1].店面
	self.发送信息1.商铺id = 出售id
	UserData[id].入店时间 = os.time()
	SendMessage(UserData[id].连接id, 9998, self.发送信息1)
end


function 商会处理类:获取商会商铺信息(id)
    self.发送内容 = {}
    for n=1,#商会数据 do
        if 商会数据[n].类型 == "物品" then
            if 物品店数据[商会数据[n].铺主id] ~= nil and 物品店数据[商会数据[n].铺主id][1].营业 then
                self.发送内容[#self.发送内容 + 1] = {
                    店名 = 物品店数据[商会数据[n].铺主id][1].店名,
                    宗旨 = 物品店数据[商会数据[n].铺主id][1].宣言,
                    店主名称 = 物品店数据[商会数据[n].铺主id][1].店主名称,
                    店主id = 物品店数据[商会数据[n].铺主id][1].店主id,
                    创店日期 = 物品店数据[商会数据[n].铺主id][1].创店日期,
                    店面 = #物品店数据[商会数据[n].铺主id][1].店面,
                    种类 = 商会数据[n].类型,
                    商铺id = 商会数据[n].铺主id
                }
            end
        else
            if 唤兽店数据[商会数据[n].铺主id] ~= nil and 唤兽店数据[商会数据[n].铺主id][1].营业 then
                self.发送内容[#self.发送内容 + 1] = {
                    店名 = 唤兽店数据[商会数据[n].铺主id][1].店名,
                    宗旨 = 唤兽店数据[商会数据[n].铺主id][1].宣言,
                    店主名称 = 唤兽店数据[商会数据[n].铺主id][1].店主名称,
                    店主id = 唤兽店数据[商会数据[n].铺主id][1].店主id,
                    创店日期 = 唤兽店数据[商会数据[n].铺主id][1].创店日期,
                    店面 = #唤兽店数据[商会数据[n].铺主id][1].店面,
                    种类 = 商会数据[n].类型,
                    商铺id = 商会数据[n].铺主id
                }
            end
        end
    end
    SendMessage(UserData[id].连接id, 11009, self.发送内容)
end

function 商会处理类:获取物品商铺信息(id)
    self.发送内容 = {}
    for n=1,#商会数据 do
        if 商会数据[n].类型 == "物品" then
            if 物品店数据[商会数据[n].铺主id] ~= nil and 物品店数据[商会数据[n].铺主id][1].营业 then
                self.发送内容[#self.发送内容 + 1] = {
                    店名 = 物品店数据[商会数据[n].铺主id][1].店名,
                    宗旨 = 物品店数据[商会数据[n].铺主id][1].宣言,
                    店主名称 = 物品店数据[商会数据[n].铺主id][1].店主名称,
                    店主id = 物品店数据[商会数据[n].铺主id][1].店主id,
                    创店日期 = 物品店数据[商会数据[n].铺主id][1].创店日期,
                    店面 = #物品店数据[商会数据[n].铺主id][1].店面,
                    种类 = 商会数据[n].类型,
                    商铺id = 商会数据[n].铺主id
                }
            end
        end
    end
    self.发送内容.店类 = "物品"
    SendMessage(UserData[id].连接id, 11009, self.发送内容)
end

function 商会处理类:获取唤兽商铺信息(id)
    self.发送内容 = {}
    for n=1,#商会数据 do
        if 商会数据[n].类型 == "唤兽" then
            if 唤兽店数据[商会数据[n].铺主id] ~= nil and 唤兽店数据[商会数据[n].铺主id][1].营业 then
                self.发送内容[#self.发送内容 + 1] = {
                    店名 = 唤兽店数据[商会数据[n].铺主id][1].店名,
                    宗旨 = 唤兽店数据[商会数据[n].铺主id][1].宣言,
                    店主名称 = 唤兽店数据[商会数据[n].铺主id][1].店主名称,
                    店主id = 唤兽店数据[商会数据[n].铺主id][1].店主id,
                    创店日期 = 唤兽店数据[商会数据[n].铺主id][1].创店日期,
                    店面 = #唤兽店数据[商会数据[n].铺主id][1].店面,
                    种类 = 商会数据[n].类型,
                    商铺id = 商会数据[n].铺主id
                }
            end
        end
    end
    self.发送内容.店类 = "唤兽"
    SendMessage(UserData[id].连接id, 11009, self.发送内容)
end


function 商会处理类:发送管理物品商铺(id)
	if 物品店数据[id] ~= nil then
		self.发送信息 = table.copy(物品店数据[id][1])
		self.发送信息.店面 = #self.发送信息.店面
		self.发送信息.现金 = UserData[id].角色.道具.货币.银子

		SendMessage(UserData[id].连接id, 11001, self.发送信息)

		if 物品店数据[id][1].营业 == false then
			SendMessage(UserData[id].连接id, 7, "#y/请注意，您的物品商铺已处于暂停营业状态")
		end

		SendMessage(UserData[id].连接id, 11004, ItemControl:索要道具1(id, "包裹"))
		self:索取柜台商品信息(id, 1)
	else
		SendMessage(UserData[id].连接id, 7, "#y/你在我这里没有物品商铺")
	end
end

function 商会处理类:发送管理唤兽商铺(id)
	if 唤兽店数据[id] ~= nil then
		self.发送信息 = table.copy(唤兽店数据[id][1])
		self.发送信息.店面 = #self.发送信息.店面
		self.发送信息.现金 = UserData[id].角色.道具.货币.银子

		SendMessage(UserData[id].连接id, 10002, self.发送信息)

		if 唤兽店数据[id][1].营业 == false then
			SendMessage(UserData[id].连接id, 7, "#y/请注意，您的唤兽商铺已处于暂停营业状态")
		end
		SendMessage(UserData[id].连接id, 10004, UserData[id].召唤兽:获取数据())
		self:索取柜台唤兽信息(id, 1)
	else
		SendMessage(UserData[id].连接id, 7, "#y/你在我这里没有唤兽商铺")
	end
end


function 商会处理类:索取柜台商品信息(id, 序号)
	self.发送信息 = {}

	for n = 1, 20 do
		if 物品店数据[id][1].店面[序号][n] ~= nil then
			if 物品店数据[id][1].店面[序号][n].id ~= nil then
				self.发送信息[n] = {
					道具 = 物品店数据[id][1].店面[序号][n].id,
					状态 = 物品店数据[id][1].店面[序号][n].状态,
					价格 = 物品店数据[id][1].店面[序号][n].价格
				}
			else
				物品店数据[id][1].店面[序号][n] = nil
			end
		end
	end

	self.发送信息.类型 = 序号

	SendMessage(UserData[id].连接id, 11005, self.发送信息)
end

function 商会处理类:索取柜台唤兽信息(id, 序号)
	self.发送信息 = {}

	for n = 1, 10 do
		if 唤兽店数据[id][1].店面[序号][n] ~= nil then
			if 唤兽店数据[id][1].店面[序号][n].id ~= nil then
				self.发送信息[n] = {
					道具 = 唤兽店数据[id][1].店面[序号][n].id,
					状态 = 唤兽店数据[id][1].店面[序号][n].状态,
					价格 = 唤兽店数据[id][1].店面[序号][n].价格
				}
			else
				唤兽店数据[id][1].店面[序号][n] = nil
			end
		end
	end

	self.发送信息.类型 = 序号
	SendMessage(UserData[id].连接id, 10003, self.发送信息)
end

return 商会处理类
