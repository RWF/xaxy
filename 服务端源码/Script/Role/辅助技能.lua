-- @Author: 作者QQ414628710
-- @Date:   2022-01-15 20:06:48
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-18 00:45:16
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-11-01 14:10:14
--======================================================================--
function RoleControl:学习技能信息(user,sj,lj)
  local lsx = user.角色[sj]
   if lj ~=nil then
      lsx.类别 = "生活技能"
  else
    lsx.类别 = sj
   end
  lsx.银两 = user.角色.道具.货币.银子
  lsx.存银 = user.角色.道具.货币.存银
  lsx.储备 = user.角色.道具.货币.储备
  lsx.经验 = user.角色.当前经验
  return lsx
 end
function RoleControl:学习强化技能处理(user, 数据)
 self.本次需求 = 计算技能数据(user.角色.强化技能[数据].等级+1)
 local bg= 0
  if user.角色.强化技能[数据].等级>=100 then

    bg =math.floor(0.7*user.角色.强化技能[数据].等级)
  end

   if user.角色.当前经验 >= self.本次需求.经验 and (user.角色.道具.货币.银子 >= self.本次需求.金钱 or user.角色.道具.货币.储备 >= self.本次需求.金钱) then

      if bg>0 then
        if user.角色.帮派==nil then
          SendMessage(user.连接id,7,"#Y/请加入帮派后才能继续学习")
          return
        elseif  帮派数据[user.角色.帮派].成员名单[user.id].帮贡.当前 < bg then
             SendMessage(user.连接id, 7, "#y/你没有那么多的帮贡")
                   return
                elseif 帮派数据[user.角色.帮派].资材 < 2 then
          SendMessage(user.连接id, 7, "#y/你帮派没有那么多的资材")
          return 0
        end
        帮派数据[user.角色.帮派].资材 = 帮派数据[user.角色.帮派].资材 - 2
        帮派数据[user.角色.帮派].成员名单[user.id].帮贡.当前 = 帮派数据[user.角色.帮派].成员名单[user.id].帮贡.当前 - bg
        SendMessage(user.连接id, 7, "#y/你减少了"..bg.."点帮贡")
        user.角色.强化技能[数据].等级 = self:计算升级技能(user,user.角色.强化技能[数据].等级,self.本次需求)
      else
        user.角色.强化技能[数据].等级 = self:计算升级技能(user,user.角色.强化技能[数据].等级,self.本次需求)
      end
  else
  SendMessage(user.连接id,7,"#Y/金钱或经验不足,无法升级!")
  return
  end
 self:刷新装备属性(user)
 SendMessage(user.连接id,2019,self:学习技能信息(user,"强化技能"))
 SendMessage(user.连接id,3030,1)
 SendMessage(user.连接id,1003,self:获取角色气血数据(user))
end

function RoleControl:学习辅助技能处理(user, 数据)
	self.本次需求 = 计算技能数据(user.角色.辅助技能[数据].等级+1)
	local bg= 0
	if user.角色.辅助技能[数据].等级>=100 then

		bg =math.floor(0.7*user.角色.辅助技能[数据].等级)
	end

	if user.角色.当前经验 >= self.本次需求.经验 and (user.角色.道具.货币.银子 >= self.本次需求.金钱 or user.角色.道具.货币.储备 >= self.本次需求.金钱) then
		if bg>0 then
			if user.角色.帮派==nil then
				SendMessage(user.连接id,7,"#Y/请加入帮派后才能继续学习")
				return
			elseif  帮派数据[user.角色.帮派].成员名单[user.id].帮贡.当前 < bg then
				SendMessage(user.连接id, 7, "#y/你没有那么多的帮贡")
				return
			elseif 帮派数据[user.角色.帮派].资材 < 2 then
				SendMessage(user.连接id, 7, "#y/你帮派没有那么多的资材")
				return 0
			end
			帮派数据[user.角色.帮派].资材 = 帮派数据[user.角色.帮派].资材 - 2
			帮派数据[user.角色.帮派].成员名单[user.id].帮贡.当前 = 帮派数据[user.角色.帮派].成员名单[user.id].帮贡.当前 - bg
			SendMessage(user.连接id, 7, "#y/你减少了"..bg.."点帮贡")
			user.角色.辅助技能[数据].等级 = self:计算升级技能(user,user.角色.辅助技能[数据].等级,self.本次需求)
		else
			user.角色.辅助技能[数据].等级 = self:计算升级技能(user,user.角色.辅助技能[数据].等级,self.本次需求)
		end
	else
		SendMessage(user.连接id,7,"#Y/金钱或经验不足,无法升级!")
		return
	end
	self:刷新装备属性(user)
	if 数据>6 then
		SendMessage(user.连接id,2019,self:学习技能信息(user,"辅助技能",1))
	else
		SendMessage(user.连接id,2019,self:学习技能信息(user,"辅助技能"))
	end

	SendMessage(user.连接id,1003,self:获取角色气血数据(user))
end
