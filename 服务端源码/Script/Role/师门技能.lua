--======================================================================--
-- @作者: QQ381990860
-- @创建时间:	2019-12-03 02:17:19
-- @Last Modified time: 2021-06-27 07:40:29
--======================================================================--
local floor = math.floor

function RoleControl:临时技能使用(user,主技能id,包含技能id)
	if user.角色.师门技能[主技能id].包含技能[包含技能id] then
	local 技能名称 =user.角色.师门技能[主技能id].包含技能[包含技能id].名称
			SendMessage(user.连接id,2033,ItemControl:索要道具4(user.id,"包裹",技能名称,主技能id.."*-*"..包含技能id))
	else
	SendMessage(user.连接id,7,"#y/技能数据错误请重新尝试")
	end
end
function RoleControl:获取制造数据(user)
	local 发送信息 = {}
	for i=1,#user.角色.师门技能 do
		for n=1,#user.角色.师门技能[i].包含技能 do
			if user.角色.师门技能[i].包含技能[n].种类 == 12	then
			 发送信息[#发送信息+1]=user.角色.师门技能[i].包含技能[n]
			 发送信息[#发送信息].主技能id =i
			 发送信息[#发送信息].包含技能id =n
			end
		end
	end
	local 熟练度	= {[10]="打造熟练度",[11]="裁缝熟练度",[12]="炼金熟练度"} 
	for i=10,12 do
	 
			 发送信息[#发送信息+1]=user.角色.辅助技能[i]
			 发送信息[#发送信息].主技能id =99
			 发送信息[#发送信息].包含技能id =i
			 发送信息[#发送信息].熟练度=user.角色[熟练度[i]]
			 
	end
	
	return 发送信息
end
function RoleControl:刷新技能学习(user)
	local 发送消息={
	储备=user.角色.道具.货币.储备,
	金钱=user.角色.道具.货币.银子,
	存银=user.角色.道具.货币.存银,
	当前经验=user.角色.当前经验,
	师门技能=user.角色.师门技能,
	门派=user.角色.门派,
	}
	return 发送消息
	end
function RoleControl:计算升级技能(user,技能,需求)--------------------------------------
	if user.角色.道具.货币.储备	>= 需求.金钱 then
	self:扣除银子(user,floor(需求.金钱),"技能学习", 1)
	elseif user.角色.道具.货币.银子	>= 需求.金钱 then
	 self:扣除银子(user,floor(需求.金钱),"技能学习")
	elseif user.角色.道具.货币.存银	>= 需求.金钱 then
		user.角色.道具.货币.存银 = floor(user.角色.道具.货币.存银 - 需求.金钱)
	else
	 SendMessage(user.连接id,7,"#Y/金钱或经验不足,无法升级!")
		return
	end
 self:扣除经验(user,floor(需求.经验))
	技能 = 技能 + 1
	return 技能
 end
function RoleControl:升级技能学习(user,选中)--------------------------------------
	self.本次需求 = 计算技能数据(user.角色.师门技能[选中].等级+1)
	if user.角色.当前经验 >= self.本次需求.经验 and (user.角色.道具.货币.银子 >= self.本次需求.金钱 or user.角色.道具.货币.储备 >= self.本次需求.金钱) then
		if user.角色.等级+9 >= user.角色.师门技能[选中].等级 then
			user.角色.师门技能[选中].等级 = self:计算升级技能(user,user.角色.师门技能[选中].等级,self.本次需求)
			self:升级技能(user,user.角色.师门技能[选中])
			for n=1,#user.角色.师门技能[选中].包含技能 do
				if user.角色.师门技能[选中].包含技能[n].学会 then
					user.角色.师门技能[选中].包含技能[n].等级 = user.角色.师门技能[选中].等级
					for w=1,#user.角色.人物技能 do
						if user.角色.人物技能[w].名称 == user.角色.师门技能[选中].包含技能[n].名称 then
							user.角色.人物技能[w].等级 = user.角色.师门技能[选中].包含技能[n].等级
							break
						end
					end
				end
			end
		else
			SendMessage(user.连接id,7,"#Y/提高人物的等级才能够升级")
		end
	else
		SendMessage(user.连接id,7,"#Y/金钱或经验不足,无法升级!")
	end
	RoleControl:刷新战斗属性(user)
	SendMessage(user.连接id,2027,self:刷新技能学习(user))
	SendMessage(user.连接id,1003,self:获取角色气血数据(user))
end

function RoleControl:升级技能(user,jn) --------------------------------------------- 升级获得技能
	-- 化生
	if jn.等级 == nil then
		jn.等级 = 1
	end
	if jn.名称 == "小乘佛法" then
		user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "金刚伏魔" then
		user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)
		if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
			self:学会技能(user,2,"佛法无边")
		end
	elseif jn.名称 == "诵经" then
		if jn.等级 >= 1 then
			self:学会技能(user,3,"唧唧歪歪")
		end
	elseif jn.名称 == "佛光普照" then
		if jn.等级 >= 20 then
			self:学会技能(user,4,"韦陀护法")
		end
		if jn.等级 >= 35 then
			self:学会技能(user,4,"金刚护体")
		self:学会技能(user,4,"拈花妙指")
		end
		if jn.等级 >= 30 then
			self:学会技能(user,4,"达摩护体")
			self:学会技能(user,4,"一苇渡江")
			self:学会技能(user,4,"金刚护法")
		end
	elseif jn.名称 == "大慈大悲" then
		if jn.等级 >= 40 then
			self:学会技能(user,5,"我佛慈悲")
		end
		user.角色.技能属性.防御 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "歧黄之术" then
	if jn.等级 >= 15 then
		self:学会技能(user,6,"解毒")
	end
	if jn.等级 >= 25 then
		self:学会技能(user,6,"活血")
	end
	if jn.等级 >= 35 then
		self:学会技能(user,6,"推气过宫")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,6,"舍生取义")
	end
	elseif jn.名称 == "渡世步" then
	if jn.等级 >= 1 then
		self:学会技能(user,7,"佛门普渡")
	end
	user.角色.技能属性.躲避 = floor(jn.等级 * 2.5)
	-- 大唐
	elseif jn.名称 == "为官之道" then
	if jn.等级 >= 15 then
		self:学会技能(user,1,"杀气诀")
	end
	elseif jn.名称 == "无双一击" then
	if jn.等级 >= 25 then
		self:学会技能(user,2,"后发制人")
	end
	user.角色.技能属性.命中 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "神兵鉴赏" then
	if jn.等级 >= 5 then
		self:学会技能(user,3,"兵器谱")
	end
	elseif jn.名称 == "疾风步" then
	if jn.等级 >= 1 then
		self:学会技能(user,4,"千里神行")
	end
	user.角色.技能属性.躲避 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "十方无敌" then
	if jn.等级 >= 30 then
		self:学会技能(user,5,"横扫千军")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,5,"破釜沉舟")
	end
	user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "紫薇之术" then
	user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "文韬武略" then
		if jn.等级 >= 35 then
		self:学会技能(user,7,"嗜血")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,7,"安神诀")
	end
	user.角色.技能属性.防御 = floor(jn.等级 * 2.5)
	-- 龙宫
	elseif jn.名称 == "九龙诀" then
	if jn.等级 >= 15 then
		self:学会技能(user,1,"清心")
	end
	if jn.等级 >= 30 then
		self:学会技能(user,1,"解封")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,1,"二龙戏珠")
	end
	user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "破浪诀" then
	user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,2,"神龙摆尾")
	end
	elseif jn.名称 == "呼风唤雨" then
	if jn.等级 >= 15 then
		self:学会技能(user,3,"龙卷雨击")
	end
	elseif jn.名称 == "龙腾术" then
		if jn.等级 >= 50 then
			self:学会技能(user,4,"龙腾")
		end
	elseif jn.名称 == "逆鳞术" then
		if jn.等级 >= 30 then
			self:学会技能(user,5,"逆鳞")
		end
		user.角色.技能属性.防御 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "游龙术" then
		if jn.等级 >= 1 then
			self:学会技能(user,6,"水遁")
		end
		if jn.等级 >= 20 then
			self:学会技能(user,6,"乘风破浪")
		end
		user.角色.技能属性.躲避 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "龙附术" then
		if jn.等级 >= 25 then
			self:学会技能(user,"龙啸九天")
		end
		if jn.等级 >= 30 then
			self:学会技能(user,7,"龙吟")
		end
		if jn.等级 >= 35 then
			self:学会技能(user,7,"龙附")
		end
	-- 方寸
	elseif jn.名称 == "黄庭经" then
		if jn.等级 >= 25 then
			self:学会技能(user,1,"三星灭魔")
		end
	elseif jn.名称 == "磐龙灭法" then
	user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "霹雳咒" then
	if jn.等级 >= 1 then
		self:学会技能(user,3,"五雷咒")
	end
	if jn.等级 >= 20 then
		self:学会技能(user,3,"落雷符")
	end
	if jn.等级 >= 25 then
		self:学会技能(user,3,"失魂符")
		self:学会技能(user,3,"离魂符")
	end
	if jn.等级 >= 40 then
		self:学会技能(user,3,"失心符")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,3,"碎甲符")
	end
	elseif jn.名称 == "符之术" then
	if jn.等级 >= 10 then
		self:学会技能(user,4,"催眠符")
	 -- self:学会技能(user,4,"兵解符")
	end
	-- if jn.等级 >= 15 then
	--	self:学会技能(user,4,"落魄符")
	-- end
	if jn.等级 >= 20 then
		self:学会技能(user,4,"追魂符")
		self:学会技能(user,4,"失忆符")
	end
	if jn.等级 >= 21 then
		self:学会技能(user,4,"飞行符")
	end
	if jn.等级 >= 30 then
		self:学会技能(user,4,"定身符")
	end
	elseif jn.名称 == "归元心法" then
	if jn.等级 >= 1 then
		self:学会技能(user,5,"归元咒")
	end
	if jn.等级 >= 100 then
		self:学会技能(user,5,"凝神术")
	end
	user.角色.技能属性.防御 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "神道无念" then
	if jn.等级 >= 30 then
		self:学会技能(user,6,"乾天罡气")
	end
	if jn.等级 >= 35 then
		self:学会技能(user,6,"神兵护法")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,6,"分身术")
	end
	user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "斜月步" then
	if jn.等级 >= 1 then
		self:学会技能(user,7,"乙木仙遁")
	end
	user.角色.技能属性.躲避 = floor(jn.等级 * 2.5)
	-- 地府
	elseif jn.名称 == "灵通术" then
	if jn.等级 >= 5 then
		self:学会技能(user,1,"堪察令")
	end
	if jn.等级 >= 30 then
		self:学会技能(user,1,"寡欲令")
	end
	elseif jn.名称 == "幽冥术" then
	if jn.等级 >= 25 then
		self:学会技能(user,3,"阎罗令")
	end
	 if jn.等级 >= 75 then
		self:学会技能(user,3,"锢魂术")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,3,"黄泉之息")
	end
	elseif jn.名称 == "六道轮回" then
	user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)
	 if jn.等级 >= 50 then
		self:学会技能(user,2,"魂飞魄散")
	end
	elseif jn.名称 == "拘魂诀" then
	if jn.等级 >= 20 then
		self:学会技能(user,4,"判官令")
	end
	if jn.等级 >= 35 then
		self:学会技能(user,4,"尸气漫天")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,4,"还阳术")
	end
	elseif jn.名称 == "九幽阴魂" then
	user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)
		if jn.等级 >= 30 then
		self:学会技能(user,5,"幽冥鬼眼")
	end
	elseif jn.名称 == "尸腐恶" then
	if jn.等级 >= 20 then
		self:学会技能(user,6,"尸腐毒")
		self:学会技能(user,6,"修罗隐身")
	end
	user.角色.技能属性.防御 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "无常步" then
	if jn.等级 >= 1 then
		self:学会技能(user,7,"杳无音讯")
	end
	user.角色.技能属性.躲避 = floor(jn.等级 * 2.5)
	-- 天宫
	elseif jn.名称 == "天罡气" then
	if jn.等级 >= 30 then
		self:学会技能(user,1,"天神护体")
	end
	if jn.等级 >= 40 then
		self:学会技能(user,1,"天神护法")
	end
	if jn.等级 >= 35 then
		self:学会技能(user,1,"五雷轰顶")
		self:学会技能(user,1,"浩然正气")
	end
	if jn.等级 >= 50	then
		self:学会技能(user,1,"雷霆万钧")
	end
	user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "傲世诀" then
	if jn.等级 >= 25 then
		self:学会技能(user,2,"天雷斩")
	end
	elseif jn.名称 == "宁气诀" then
		if jn.等级 >= 20 then
		self:学会技能(user,4,"宁心")
	end
	user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "清明自在" then
			if jn.等级 >= 35 then
		self:学会技能(user,3,"知己知彼")
	end
	user.角色.技能属性.气血 = floor(jn.等级 * 3)
	elseif jn.名称 == "乾坤塔" then
		if jn.等级 >= 30 then
		self:学会技能(user,5,"镇妖")
	end


	if jn.等级 >= 50 then
		self:学会技能(user,5,"错乱")
	end
	user.角色.技能属性.防御 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "混天术" then
	if jn.等级 >= 40 then
		self:学会技能(user,6,"百万神兵")
	end
	 if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,6,"金刚镯")
	end
	elseif jn.名称 == "云霄步" then
	if jn.等级 >= 1 then
		self:学会技能(user,7,"腾云驾雾")
	end
	user.角色.技能属性.躲避 = floor(jn.等级 * 2.5)
	-- 魔王
 elseif jn.名称 == "牛逼神功" then
	if jn.等级 >= 30 then
		self:学会技能(user,1,"魔王护持")
	end


	elseif jn.名称 == "火云术" then
	if jn.等级 >= 30 then
		self:学会技能(user,3,"飞砂走石")
	end
	if jn.等级 >= 20 then
		self:学会技能(user,3,"三昧真火")
	end
	elseif jn.名称 == "裂石步" then
	if jn.等级 >= 1 then
		self:学会技能(user,7,"牛屎遁")
	end
	user.角色.技能属性.躲避 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "震天诀" then
	user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "火牛阵" then
	if jn.等级 >= 30 then
		self:学会技能(user,4,"牛劲")
	end
	 if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,4,"火甲术")
	end
	user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "牛虱阵" then
	-- if jn.等级 >= 25 then
	--	self:学会技能(user,5,"无敌牛虱")
	--	self:学会技能(user,5,"无敌牛妖")
	-- end
	 if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,5,"摇头摆尾")
	end
	elseif jn.名称 == "回身击" then
	user.角色.技能属性.防御 = floor(jn.等级 * 2.5)
		if jn.等级 >= 30 then
		self:学会技能(user,6,"魔王回首")
	end
	elseif jn.名称 == "裂石步" then
	user.角色.技能属性.速度 = floor(jn.等级 * 2.5)
	--普陀
	elseif jn.名称 == "灵性" then
		if jn.等级 >= 60 then
		self:学会技能(user,1,"自在心法")
	end
	user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "护法金刚" then
	user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "金刚经" then
	if jn.等级 >= 15 then
		self:学会技能(user,5,"普渡众生")
	end
	if jn.等级 >= 35 then
		self:学会技能(user,5,"莲华妙法")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,5,"灵动九天")
	end
	elseif jn.名称 == "观音咒" then
	if jn.等级 >= 30 then
		self:学会技能(user,3,"紧箍咒")
	end
	if jn.等级 >= 20 then
		self:学会技能(user,3,"杨柳甘露")
	end
	elseif jn.名称 == "五行学说" then
	if jn.等级 >= 10 then
		self:学会技能(user,4,"日光华")
	end
	if jn.等级 >= 10 then
		self:学会技能(user,4,"靛沧海")
	end
	if jn.等级 >= 10 then
		self:学会技能(user,4,"巨岩破")
	end
	if jn.等级 >= 10 then
		self:学会技能(user,4,"苍茫树")
	end
	if jn.等级 >= 10 then
		self:学会技能(user,4,"地裂火")
	end
	elseif jn.名称 == "五行扭转" then
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,6,"颠倒五行")
	end
	user.角色.技能属性.防御 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "莲花宝座" then
	if jn.等级 >= 1 then
		self:学会技能(user,7,"坐莲")
	end
	user.角色.技能属性.躲避 = floor(jn.等级 * 2.5)
	-- 五庄观
	elseif jn.名称 == "周易学" then
	if jn.等级 >= 25 then
		self:学会技能(user,1,"驱魔")
	end
	if	jn.等级 >= 20 then
		self:学会技能(user,1,"驱尸")
	end
	user.角色.技能属性.魔法 = floor(jn.等级 * 3)
	elseif jn.名称 == "潇湘仙雨" then
	if jn.等级 >= 25 then
		self:学会技能(user,2,"烟雨剑法")
	end
	if jn.等级 >= 20 then
		self:学会技能(user,2,"飘渺式")
	end
	user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "乾坤袖" then
	if jn.等级 >= 20 then
		self:学会技能(user,3,"日月乾坤")
	end
		if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,3,"天地同寿")
		self:学会技能(user,3,"乾坤妙法")
	end
	elseif jn.名称 == "修仙术" then
	if jn.等级 >= 30 then
		self:学会技能(user,4,"炼气化神")
	end
	if jn.等级 >= 30 then
		self:学会技能(user,4,"生命之泉")
	end
	if jn.等级 >= 35 then
		self:学会技能(user,4,"一气化三清")
	end
	elseif jn.名称 == "混元道果" then
	user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "明性修身" then
	if jn.等级 >= 30 then
		self:学会技能(user,6,"三花聚顶")
	end
	user.角色.技能属性.防御 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "七星遁" then
	if jn.等级 >= 1 then
		self:学会技能(user,7,"斗转星移")
	end
	user.角色.技能属性.躲避 = floor(jn.等级 * 2.5)
	-- 狮驼岭
	elseif jn.名称 == "魔兽神功" then
	if jn.等级 >= 20 then
		self:学会技能(user,1,"变身")
	end

	elseif jn.名称 == "生死搏" then
	if jn.等级 >= 20 then
		self:学会技能(user,2,"象形")
	end
	if jn.等级 >= 30 then
		self:学会技能(user,2,"鹰击")
	end
	if jn.等级 >= 20 then
		self:学会技能(user,2,"狮搏")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,2,"天魔解体")
	end
	user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "训兽诀" then
	user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)
		if jn.等级 >= 15 then
		self:学会技能(user,3,"威慑")
	 end
	elseif jn.名称 == "阴阳二气诀" then
		if jn.等级 >= 40 then
		self:学会技能(user,4,"定心术")
	 end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,4,"魔息术")
	end
	elseif jn.名称 == "狂兽诀" then
	if jn.等级 >= 30 then
		self:学会技能(user,5,"连环击")
	end
	if jn.等级 >= 35 then
		self:学会技能(user,5,"神力无穷")
	end

	elseif jn.名称 == "大鹏展翅" then
	if jn.等级 >= 1 then
		self:学会技能(user,6,"振翅千里")
	end
	user.角色.技能属性.躲避 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "魔兽反噬" then
	user.角色.技能属性.防御 = floor(jn.等级 * 2.5)
		if jn.等级 >= 20 then
		self:学会技能(user,7,"极度疯狂")
	end
	-- 盘丝洞
	elseif jn.名称 == "蛛丝阵法" then
		--	if jn.等级 >= 25 then
		--	self:学会技能(user,1,"夺命蛛丝")
		-- end
	if jn.等级 >= 30 then
		self:学会技能(user,1,"盘丝舞")
	end
	user.角色.技能属性.防御 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "秋波暗送" then
	if jn.等级 >= 20 then
		self:学会技能(user,3,"勾魂")
	end
	if jn.等级 >= 20 then
		self:学会技能(user,3,"摄魄")
	end
	user.角色.技能属性.命中 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "迷情大法" then
	if jn.等级 >= 30 then
		self:学会技能(user,2,"含情脉脉")
	end
	if jn.等级 >= 45 then
		self:学会技能(user,2,"魔音摄魂")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,2,"瘴气")
	end
	elseif jn.名称 == "天外魔音" then

	user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)

	elseif jn.名称 == "盘丝大法" then
	if jn.等级 >= 20 then
		self:学会技能(user,5,"盘丝阵")
		self:学会技能(user,5,"复苏")
	end
	user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "盘丝步" then
	if jn.等级 >= 15 then
		self:学会技能(user,6,"天罗地网")
	end
	if jn.等级 >= 1 then
		self:学会技能(user,6,"天蚕丝")
	end
	 if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,6,"幻镜术")
	end
	user.角色.技能属性.躲避 = floor(jn.等级 * 2.5)
	-- 凌波城
	elseif jn.名称 == "九转玄功" then
	if jn.等级 >= 25 then
		self:学会技能(user,2,"不动如山")
	end
	elseif jn.名称 == "武神显圣" then
	if jn.等级 >= 30 then
		self:学会技能(user,3,"碎星诀")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,3,"镇魂诀")
	end
	elseif jn.名称 == "气吞山河" then
	if jn.等级 >= 25 then
		self:学会技能(user,5,"裂石")
	end
	if jn.等级 >= 25 then
		self:学会技能(user,5,"浪涌")
	end
	if jn.等级 >= 35 then
		self:学会技能(user,5,"断岳势")
	end
	if jn.等级 >= 45 then
		self:学会技能(user,5,"天崩地裂")
	end
	if jn.等级 >= 45 then
		self:学会技能(user,5,"翻江搅海")
	end
	if jn.等级 >= 35 then
		self:学会技能(user,5,"惊涛怒")
	end
	user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "啸傲" then
	user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "诛魔" then
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,6,"腾雷")
	end
	user.角色.技能属性.防御 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "法天象地" then
	if jn.等级 >= 1 then
		self:学会技能(user,7,"纵地金光")
	end
	user.角色.技能属性.命中 = floor(jn.等级 * 2.5)
	-- 神木林
	elseif jn.名称 == "瞬息万变" then
	if jn.等级 >= 35 then
		self:学会技能(user,1,"落叶萧萧")
	end
	elseif jn.名称 == "万灵诸念" then
	if jn.等级 >= 20 then
		self:学会技能(user,2,"荆棘舞")
	end
	if jn.等级 >= 20 then
		self:学会技能(user,2,"尘土刃")
	end
	if jn.等级 >= 20 then
		self:学会技能(user,2,"冰川怒")
	end
	user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "巫咒" then
	if jn.等级 >= 40 then
		self:学会技能(user,3,"雾杀")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,3,"血雨")
	end

	user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "万物轮转" then
	if jn.等级 >= 35 then
		self:学会技能(user,4,"星月之惠")
	end
	user.角色.技能属性.防御 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "天人庇护" then
	if jn.等级 >= 50 then
		self:学会技能(user,5,"炎护")
	end
	if jn.等级 >= 1 then
		self:学会技能(user,5,"叶隐")
	end
	user.角色.技能属性.躲避 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "神木恩泽" then
	 if jn.等级 >= 35 then
		self:学会技能(user,6,"神木呓语")
	end
	elseif jn.名称 == "驭灵咒" then
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,7,"蜜润")
	end
	user.角色.技能属性.魔法 = floor(jn.等级 * 3)
	-- 无底洞
	elseif jn.名称 == "枯骨心法" then
	if jn.等级 >= 20 then
		self:学会技能(user,1,"移魂化骨")
	end
	elseif jn.名称 == "阴风绝章" then
	if jn.等级 >= 25 then
		self:学会技能(user,2,"夺魄令")
	end
	if jn.等级 >= 30 then
		self:学会技能(user,2,"煞气诀")
	end
	if jn.等级 >= 50 then
		self:学会技能(user,2,"惊魂掌")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,2,"摧心术")
	end
	elseif jn.名称 == "鬼蛊灵蕴" then
	if jn.等级 >= 35 then
		self:学会技能(user,3,"夺命咒")
	end
	user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "燃灯灵宝" then
	if jn.等级 >= 35 then
		self:学会技能(user,4,"明光宝烛")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,4,"金身舍利")
	end
	user.角色.技能属性.防御 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "地冥妙法" then
	if jn.等级 >= 20 then
		self:学会技能(user,5,"地涌金莲")
	end
	elseif jn.名称 == "混元神功" then
	if jn.等级 >= 35 then
		self:学会技能(user,6,"元阳护体")
	end
	user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "秘影迷踪" then
	if jn.等级 >= 1 then
		self:学会技能(user,7,"遁地术")
	end
	user.角色.技能属性.速度 = floor(jn.等级 * 2.5)
	-- 女儿
	elseif jn.名称 == "毒经" then
	user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "倾国倾城" then
	if jn.等级 >= 20 then
		self:学会技能(user,2,"红袖添香")
	end
	if jn.等级 >= 10 then
		self:学会技能(user,2,"楚楚可怜")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,2,"一笑倾城")
	end
	elseif jn.名称 == "沉鱼落雁" then
	if jn.等级 >= 25 then
		self:学会技能(user,3,"满天花雨")
	end
	if jn.等级 >= 20 then
		self:学会技能(user,3,"雨落寒沙")
	end
	elseif jn.名称 == "闭月羞花" then
	if jn.等级 >= 30 then
		self:学会技能(user,4,"莲步轻舞")
		self:学会技能(user,4,"如花解语")
	end
	if jn.等级 >= 25 then
		self:学会技能(user,4,"娉婷嬝娜")
	end
	if jn.等级 >= 40 then
		self:学会技能(user,4,"似玉生香")
	end
	elseif jn.名称 == "香飘兰麝" then
	 if jn.等级 >= 35 then
		self:学会技能(user,5,"轻如鸿毛")
	end
	user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "玉质冰肌" then
	if jn.等级 >= 10 then
		self:学会技能(user,6,"百毒不侵")
	end
	user.角色.技能属性.防御 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "清歌妙舞" then
	if jn.等级 >= 1 then
		self:学会技能(user,7,"移形换影")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode)	then
		self:学会技能(user,7,"飞花摘叶")
	end
	user.角色.技能属性.速度 = floor(jn.等级 * 2.5)


-------------------------------------花果山---------------------------------------------


	elseif jn.名称 == "神通广大" then
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode)	then
		self:学会技能(user,1,"威震凌霄")
	end
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode)	then
		self:学会技能(user,1,"气慑天军")
	end

	elseif jn.名称 == "金刚之躯" then
	if jn.等级 >= 1 then
		self:学会技能(user,4,"铜头铁臂")
	end
	if jn.等级 >= 35 then
		self:学会技能(user,4,"担山赶月")
	end
	user.角色.技能属性.防御 = floor(jn.等级 * 2.5)

	elseif jn.名称 == "如意金箍" then
	if jn.等级 >= 1 then
		self:学会技能(user,2,"当头一棒")
	end
	if jn.等级 >= 1 then
		self:学会技能(user,2,"神针撼海")
	end
	if jn.等级 >= 1 then
		self:学会技能(user,2,"杀威铁棒")
	end
	if jn.等级 >= 1 then
		self:学会技能(user,2,"泼天乱棒")
	end
	user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "齐天逞胜" then
	-- if jn.等级 >= 1 then
	--	self:学会技能(user,3,"九幽除名")
	-- end
	if jn.等级 >= 15 then
		self:学会技能(user,3,"移星换斗")
	end
	if jn.等级 >= 1 then
		self:学会技能(user,3,"云暗天昏")
	end
	elseif jn.名称 == "灵猴九窍" then
	if jn.等级 >= 1 then
		self:学会技能(user,5,"无所遁形")
	end
	-- if jn.等级 >= 175 then ---化圣
	--	self:学会技能(user,5,"天地洞明")
	-- end
	-- if jn.等级 >= 25 then
	--	self:学会技能(user,5,"除光息焰")
	-- end
	user.角色.技能属性.命中 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "七十二变" then
	if jn.等级 >= 10 then
		self:学会技能(user,6,"呼子唤孙")
	end
	if jn.等级 >= 45 then
		self:学会技能(user,6,"八戒上身")
	end
	user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "滕云驾雾" then
	if jn.等级 >= 1 then
		self:学会技能(user,7,"筋斗云")
	end

	----------------------------新门派

	-----------------------女魃墓
	elseif jn.名称 == "天罚之焰" then
	if jn.等级 >= 10 then
		self:学会技能(user,2,"炽火流离")
	end
	-- if jn.等级 >= 25 then
	--	self:学会技能(user,2,"极天炼焰")
	-- end
	user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)
	elseif jn.名称 == "煌火无明" then
	if jn.等级 >= 15 then
		self:学会技能(user,3,"谜毒之缚")
	end
	-- if jn.等级 >= 10 then
	--	self:学会技能(user,3,"诡蝠之刑")
	-- end
		if jn.等级 >= 30 then
		self:学会技能(user,3,"怨怖之泣")
	end
	--	 if jn.等级 >= 40 then
	--	self:学会技能(user,3,"誓血之祭")
	-- end
	user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)

	elseif jn.名称 == "化神以灵" then
	if jn.等级 >= 1 then
		self:学会技能(user,4,"唤灵·魂火")
	end
	if jn.等级 >= 20 then
		self:学会技能(user,4,"唤魔·堕羽")
	end
	if jn.等级 >= 120 and( user.角色.飞升 or DebugMode)	then
		self:学会技能(user,4,"唤魔·毒魅")
	end
		if jn.等级 >= 10 then
		self:学会技能(user,4,"唤灵·焚魂")
	end
	 if jn.等级 >= 175 and user.角色.化圣 then---化圣
		self:学会技能(user,4,"天魔觉醒")
	end

	elseif jn.名称 == "弹指成烬" then
	if jn.等级 >= 25 then
		self:学会技能(user,5,"净世煌火")
	end
	if jn.等级 >= 35 then
		self:学会技能(user,5,"焚魔烈焰")
	end
	user.角色.技能属性.法术防御 = floor(jn.等级 * 2.5)

		elseif jn.名称 == "藻光灵狱" then
	if jn.等级 >= 35 then
		self:学会技能(user,6,"幽影灵魄")
	end
	user.角色.技能属性.防御 = floor(jn.等级 * 2.5)

		elseif jn.名称 == "离魂" then
	if jn.等级 >= 1 then
		self:学会技能(user,7,"魂兮归来")
	end
	user.角色.技能属性.速度 = floor(jn.等级 * 2.5)

------天机城
		elseif jn.名称 == "神工无形" then
	-- if jn.等级 >= 175 then---化圣
	--	self:学会技能(user,1,"一发而动")
	-- end
		user.角色.技能属性.速度 = floor(jn.等级 * 2.5)

		elseif jn.名称 == "攻玉以石" then
	if jn.等级 >= 25 then
		self:学会技能(user,2,"针锋相对")
	end
	--	 if jn.等级 >= 120 and (user.角色.飞升 or DebugMode)	then
	--	self:学会技能(user,2,"攻守易位")
	-- end


		elseif jn.名称 == "擎天之械" then
	if jn.等级 >= 25 then
		self:学会技能(user,3,"锋芒毕露")
	end


		elseif jn.名称 == "千机奇巧" then
	if jn.等级 >= 45 then
		self:学会技能(user,4,"诱袭")
	end
		if jn.等级 >= 1 then
		self:学会技能(user,4,"破击")
	end
	user.角色.技能属性.防御 = floor(jn.等级 * 2.5)

		elseif jn.名称 == "匠心不移" then
	if jn.等级 >= 120 and (user.角色.飞升 or DebugMode) then
		self:学会技能(user,5,"匠心·削铁")
	end
		if jn.等级 >= 35 then
		self:学会技能(user,5,"匠心·固甲")
	end
			if jn.等级 >= 25 then
		self:学会技能(user,5,"匠心·蓄锐")
	end
	user.角色.技能属性.伤害 = floor(jn.等级 * 2.5)


	elseif jn.名称 == "运思如电" then
	if jn.等级 >= 1 then
		self:学会技能(user,6,"天马行空")
	end
	user.角色.技能属性.躲避 = floor(jn.等级 * 2.5)

	elseif jn.名称 == "探奥索隐" then
	if jn.等级 >= 35 then
		self:学会技能(user,7,"鬼斧神工")
	end
		--	 if jn.等级 >= 25 then
		--	self:学会技能(user,7,"移山填海")
		-- end
	user.角色.技能属性.灵力 = floor(jn.等级 * 2.5)
	end

	self:刷新战斗属性(user)
 end
 
function RoleControl:学会技能(user,id,gz)-----------------------------------------------------
	if user.角色.师门技能[id] ~= nil then
		for s=1,#user.角色.师门技能[id].包含技能 do
			if user.角色.师门技能[id].包含技能[s].名称 == gz and not self:有无技能(user,gz) then
				user.角色.师门技能[id].包含技能[s].学会 = true
				user.角色.师门技能[id].包含技能[s].等级 = user.角色.师门技能[id].等级
				local x = table.tostring(user.角色.师门技能[id].包含技能[s])
				table.insert(user.角色.人物技能,table.loadstring(x))
				SendMessage(user.连接id,7,"#Y/你学会了门派技能#r/"..gz)
			end
		end
	end
end

function RoleControl:有无技能(user,名称)-------------------------------------------------------
	for n=1,#user.角色.人物技能 do
		if user.角色.人物技能[n].名称 == 名称 then
			return true
		end
	end
	return false
end
