--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-09-22 01:38:47
--======================================================================--
local floor = math.floor

function RoleControl:兑换乾元丹数据(user)
	local SendMessage={
			当前银子 = user.角色.道具.货币.银子,
			当前经验 = user.角色.当前经验,
			乾元丹=user.角色.奇经八脉,
			状态="乾元丹"
			}
	return SendMessage
end
function RoleControl:兑换乾元丹(user)
	if user.角色.奇经八脉.乾元丹 > 9 then
        SendMessage(user.连接id, 7, "#y/你已经无法在获得乾元丹了!")
		return
	elseif user.角色.奇经八脉.乾元丹 >= user.角色.奇经八脉.可换乾元丹 then
        SendMessage(user.连接id, 7, "#y/当前等级无法在获取新的乾元丹!")
		return
	end
	local js = {}
	if user.角色.奇经八脉.乾元丹 == 0 then
		js={jy=220000000,jq=40000000}
	elseif user.角色.奇经八脉.乾元丹 == 1 then
        js={jy=270000000,jq=55000000}
	elseif user.角色.奇经八脉.乾元丹 == 2 then
        js={jy=340000000,jq=70000000}
	elseif user.角色.奇经八脉.乾元丹 == 3 then
		js={jy=420000000,jq=80000000}
	elseif user.角色.奇经八脉.乾元丹 == 4 then
		js={jy=510000000,jq=100000000}
	elseif user.角色.奇经八脉.乾元丹 == 5 then
		js={jy=620000000,jq=120000000}
	elseif user.角色.奇经八脉.乾元丹 == 6 then
		js={jy=750000000,jq=150000000}
	elseif user.角色.奇经八脉.乾元丹 == 7 then
		js={jy=750000000,jq=150000000}
	elseif user.角色.奇经八脉.乾元丹 == 8 then
		js={jy=750000000,jq=150000000}
	end
	if user.角色.当前经验<js.jy then
        SendMessage(user.连接id, 7, "#y/当前经验不足无法进行兑换!")
        return
	elseif user.角色.道具.货币.银子 < js.jq then
        SendMessage(user.连接id, 7,  "#y/当前金钱不足无法进行兑换!")
        return
	else
		self:扣除经验(user,js.jy, "学习乾元丹")
		self:扣除银子(user,js.jq, "学习乾元丹")
		user.角色.奇经八脉.乾元丹=user.角色.奇经八脉.乾元丹+1
		user.角色.奇经八脉.剩余乾元丹=user.角色.奇经八脉.剩余乾元丹+1
		SendMessage(user.连接id, 7, "#y/兑换成功,贡献你获得一颗乾元丹!")
		SendMessage(user.连接id, 2017, RoleControl:兑换乾元丹数据(user))
	end
end

function RoleControl:学习奇经八脉(user,选中, 内容)
  if user.角色.奇经八脉.剩余乾元丹 < 1 then
    SendMessage(user.连接id, 7, "#y/你没有那么多乾元丹")
    return 0
  end
      if 选中 >=19 then
       user.角色.奇经八脉.额外能力 = true
       SendMessage(user.连接id,7,"#y/恭喜你学会了#r/".."额外能力")
      end
     user.角色.奇经八脉[内容] = true
     user.角色.奇经八脉.剩余乾元丹 =user.角色.奇经八脉.剩余乾元丹 - 1

    if 内容 == "翩鸿一击" or 内容 =="由己渡人" or 内容 =="长驱直入" or 内容 =="天命剑法" or  内容 =="百爪狂杀"or 内容 == "飞符炼魂" or 内容 == "顺势而为"
      or 内容 == "钟馗论道" or 内容 == "碎玉弄影" or 内容 == "鸿渐于陆" or 内容 == "风卷残云" or 内容 == "凋零之歌" or 内容 == "妙悟" or 内容 == "诸天看护"
      or 内容 == "落花成泥"  or 内容 == "妖风四起" or 内容 =="同舟共济" or 内容 =="烈焰真诀"or  内容 =="魔焰滔天" or  内容 =="肝胆相照" or  内容 =="背水一战"
      or  内容 =="风雷韵动"  or  内容 =="画地为牢" or  内容 =="五行制化" or  内容 =="波澜不惊"or  内容 =="莲心剑意"or  内容 =="莲花心音"or  内容 =="真君显灵"
      or  内容 =="天神怒斩"or  内容 =="清风望月" or  内容 =="心随意动" or  内容 =="雷浪穿云" or  内容 =="亢龙归海"  then
  
      table.insert(user.角色.乾元丹技能,置技能(内容))
    end
    user.角色.奇经八脉.技能树 = 技能树(选中) or 1
    SendMessage(user.连接id,7,"#y/恭喜你学会了#r/"..内容)
    self:刷新战斗属性(user)

   SendMessage(user.连接id,2069,self:索取奇经八脉(user))
 end
function RoleControl:索取奇经八脉(user)
  local SendMessage={
 名称=user.角色.名称,
 id=user.角色.id,
 造型=user.角色.造型,
 染色=user.角色.染色,
 等级=user.角色.等级,
 门派=user.角色.门派,
 奇经八脉=user.角色.奇经八脉,

 }
  return SendMessage
 end