--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-12-06 08:14:54
--======================================================================--
function RoleControl:添加好友数据(user,对方id)
 if UserData[对方id] ==nil then
  SendMessage(user.连接id,7,"#Y/对方数据不存在,可能已经下线!")
  return
  elseif 对方id== user.角色.id then
      SendMessage(user.连接id,7,"#Y/你不能将自己加为好友!")
  return
 end

  for i=1,#user.角色.好友数据 do
     if user.角色.好友数据[i].id == 对方id then
      SendMessage(user.连接id,7,"#Y/他已经是你的好友了,不需要再次添加")
      return
     end
  end
   user.角色.好友数据[#user.角色.好友数据+1]={
   id = 对方id,
   造型=UserData[对方id].角色.造型,
   名称 = UserData[对方id].角色.名称,
   等级 =UserData[对方id].角色.等级,
   门派 =UserData[对方id].角色.门派,
   帮派 = UserData[对方id].角色.帮派 ,
   当前称谓 = UserData[对方id].角色.称谓.当前,
   好友度 = 0,
   关系 = "好友",
  }

  SendMessage(user.连接id,7,"#Y/你添加了#G/"..UserData[对方id].角色.名称.."#Y/为好友")
  SendMessage(UserData[对方id].连接id,7,""..user.角色.名称.."#Y/添加你为好友")
  SendMessage(user.连接id,2078,user.角色.好友数据)
 end

 function RoleControl:更新好友数据(user,对方id)
 if UserData[对方id] ==nil then
  SendMessage(user.连接id,7,"#Y/对方数据已经下线，无法更新数据!")
  return
 end

  for i=1,#user.角色.好友数据 do
     if user.角色.好友数据[i].id == 对方id then
          user.角色.好友数据[i]={
          id = 对方id,
          造型=UserData[对方id].角色.造型,
          名称 = UserData[对方id].角色.名称,
          等级 =UserData[对方id].角色.等级,
          门派 =UserData[对方id].角色.门派,
          帮派 = UserData[对方id].角色.帮派 ,
          当前称谓 = UserData[对方id].角色.称谓.当前,
          好友度 = 0,
          关系 = "好友",
          }

        SendMessage(user.连接id,7,"#Y/数据更新成功!")
        SendMessage(user.连接id,1028,user.角色.好友数据[i])
        SendMessage(user.连接id,2078,user.角色.好友数据)
      return
     end
  end



 end


function RoleControl:deleteFriend(user, id)
    for i=#user.角色.好友数据,1,-1 do
     if user.角色.好友数据[i].id == id then
        table.remove(user.角色.好友数据, i)
       break
     end
    end
    SendMessage(user.连接id,2078,user.角色.好友数据)
end
  
function RoleControl:添加系统消息(user, 内容)
  if 系统消息数据[user.id] == nil then
    系统消息数据[user.id] = {}
  end

  系统消息数据[user.id][#系统消息数据[user.id] + 1] = {时间=os.date("[%Y年%m月%d日%X]"),内容=内容}

  SendMessage(user.连接id, 13, "77")
 end