-- @Author: 作者QQ381990860
-- @Date:   2021-09-12 16:36:56
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-09-21 16:26:19
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-06-11 18:31:59
--======================================================================--

------------------------------银子-----------------------------------------------
function RoleControl:添加银子(user,数额, 类型)

  if user ==nil then
    return
  end
    if type(数额)~= "number" or 数额 < 0 then
      print(数额, 类型)
      error(string.format("id=%d,数额=%s,类型=%s添加银子错误",user.id,数额, 类型))
      封禁账号(user,"添加银子"..类型)
      return
    end
        数额= math.floor(数额)
      local 临时数额 = 数额
      local 初始余额 = user.角色.道具.货币.银子
      -- if user.角色.法宝效果.聚宝盆 and 类型 ~= 29 and 类型 ~= "仙玉抽奖"then
      --    临时数额=临时数额+ user.角色.法宝效果.聚宝盆*10
      --  end
      user.角色.道具.货币.银子 = user.角色.道具.货币.银子 + 临时数额
      user.角色.获得银子 = user.角色.获得银子 + 临时数额
    if 类型 ~= 29 and 类型 ~= "仙玉抽奖" and 类型 ~= "黑市拍卖"then
      SendMessage(user.连接id, 7, "#y/你获得了" .. 数额 .. "两银子")
       SendMessage(user.连接id, 9, "#xt/#w/你获得了" .. 数额 .. "两银子")
    end
    if MoneyLog[user.id]==nil then
       MoneyLog[user.id]=0
    end
    MoneyLog[user.id]=MoneyLog[user.id]  +数额
    self:添加消费日志(user,"余额" .. 初始余额 .. "，获得银子：" .. 临时数额 .. ",来源代号" .. 类型 .. "，银子余额" .. user.角色.道具.货币.银子)
    SendMessage(user.连接id, 3008, {Type="银两",Value=user.角色.道具.货币.银子})

   end
function RoleControl:扣除银子(user,数额, 类型, 储备)

    if 数额 < 0 then
      封禁账号(user,"扣除银子")
      广播消息(9, "#xt/#g/ " .. user.角色.名称 .. "使用非法软件,已经封号")
      return 0
    end
    if type(user.角色.道具.货币.银子)~= "number" then
      封禁账号(user,"扣除银子")
      return
    end
    数额=math.floor(数额)
    if 储备 == nil then
      local 初始余额 = user.角色.道具.货币.银子
      user.角色.道具.货币.银子 = user.角色.道具.货币.银子 - 数额
      self:添加消费日志(user,"余额" .. 初始余额 .. "，失去银子：" .. 数额 .. ",来源代号" .. 类型 .. "，银子余额" .. user.角色.道具.货币.银子)
      if 类型 ~= "技能学习" and 类型 ~= "修炼" and 类型 ~= "商城" then
        SendMessage(user.连接id, 7, "#y/你失去了" .. 数额 .. "两银子")
      end
    elseif 数额 <= user.角色.道具.货币.储备 then
      初始余额 = user.角色.道具.货币.储备
      user.角色.道具.货币.储备 = user.角色.道具.货币.储备 - 数额
      self:添加消费日志(user,"余额" .. 初始余额 .. "，失去银子：" .. 数额 .. ",来源代号" .. 类型 .. "，储备余额" .. user.角色.道具.货币.储备)
      if 类型 ~= "技能学习" and 类型 ~= "修炼" then
        SendMessage(user.连接id, 7, "#y/你失去了" .. 数额 .. "两储备银子")
      end
    else
      self.扣除余额 = 数额 - user.角色.道具.货币.储备
      self:添加消费日志(user,"余额" .. user.角色.道具.货币.储备 .. "，失去银子：" .. user.角色.道具.货币.储备 .. ",来源代号" .. 类型 .. "，储备余额" .. user.角色.道具.货币.储备)
      if 类型 ~= 10 and 类型 ~= 11 then
        SendMessage(user.连接id, 7, "#y/你失去了" .. user.角色.道具.货币.储备 .. "两储备金")
      end
      user.角色.道具.货币.储备 = 0
      user.角色.道具.货币.银子 = user.角色.道具.货币.银子 - self.扣除余额
      self:添加消费日志(user,"余额" .. user.角色.道具.货币.银子 .. "，失去银子：" .. self.扣除余额 .. ",来源代号" .. 类型 .. "，银子余额" .. user.角色.道具.货币.银子)
      if 类型 ~= 10  then
        SendMessage(user.连接id, 7, "#y/你失去了" .. self.扣除余额 .. "两银子")
      end
    end
     SendMessage(user.连接id, 3008, {Type="银两",Value=user.角色.道具.货币.银子})
   end
------------------------------经验-----------------------------------------------
function RoleControl:扣除经验(user,数额, 类型)
    user.角色.当前经验 = user.角色.当前经验 - 数额
   end
function RoleControl:添加经验(user,数额, 类型)
  if type(数额)~="number" then
       error(类型.."添加经验类型数据错误")
       return
  elseif 类型==nil then
            error(user.id.."添加经验没有数据类型错误")
       return
  end
    local 临时数额 = 数额* ServerConfig.经验获得率
    if user.角色.法宝效果.通灵宝玉 then
         临时数额= 临时数额+ math.floor( 临时数额*user.角色.法宝效果.通灵宝玉/100)
    end
    if user.角色.锦衣数据.定制 and 类型~="GM充值" then
       临时数额 =  math.floor(临时数额*1.2)
    end
    if user.角色.双倍 and  LinkTask[类型]==nil and 类型~="师门" and 类型~="GM充值"and 类型~="仙玉抽奖" and 类型~="门派闯关"   and 类型~="游泳"
      and 类型~="皇宫飞贼" and 类型~="嘉年华" and 类型~="活跃度" and 类型~="古董"  and 类型~="群雄逐鹿" and 类型~="蚩尤挑战" and 类型~="无限轮回" then
       临时数额 =  临时数额*2
    end
    user.角色.当前经验 = user.角色.当前经验 +   math.floor(临时数额)
    user.角色.获得经验 = user.角色.获得经验 +  math.floor(临时数额)
    SendMessage(user.连接id, 9, "#xt/#w/获得" ..  临时数额 .. "点经验")
    SendMessage(user.连接id, 1003, self:获取角色气血数据(user))
    self:添加消费日志(user,"获得经验：" ..  临时数额 .. ",来源代号" .. 类型)
   end
------------------------------仙玉-----------------------------------------------
  function RoleControl:扣除仙玉(user,数额,类型)
    if 数额 <= 0 then
      return true
    end
    user.仙玉 = f函数.读配置(ServerDirectory.."玩家信息/账号" .. user.账号 .. "/账号.txt", "账号信息", "仙玉") + 0
    if user.仙玉 <= 0 then
      SendMessage(user.连接id, 7, "#y/你没有那么多的仙玉")
      return false
    end
    if user.仙玉 < 数额 then
      SendMessage(user.连接id, 7, "#y/你没有那么多的仙玉")
      return false
    else
      user.仙玉 = user.仙玉 - 数额
      self:添加系统消息(user, "#w/您消耗了#y/" .. 数额 .. "#w/点仙玉，当前可用仙玉数额为#r/" .. user.仙玉 .. "#w/点")
      f函数.写配置(ServerDirectory.."玩家信息/账号" .. user.账号 .. "/账号.txt", "账号信息", "仙玉", user.仙玉)
      self:添加消费日志(user,"扣除"..数额.."仙玉来源"..类型)
      return true
    end
     SendMessage(user.连接id, 3008, {Type="仙玉",Value=user.仙玉})
   end
   function RoleControl:添加仙玉(user,数额,类型)----------------------
      if 数额 <= 0 then
        return
      end
      user.仙玉 = f函数.读配置(ServerDirectory.."玩家信息/账号" .. user.账号 .. "/账号.txt", "账号信息", "仙玉") + 0
      user.仙玉=user.仙玉+数额
      f函数.写配置(ServerDirectory..[[玩家信息\账号]]..user.账号..[[\账号.txt]],"账号信息","仙玉",user.仙玉)
      user.仙玉=f函数.读配置(ServerDirectory..[[玩家信息\账号]]..user.账号..[[\账号.txt]],"账号信息","仙玉")+0
      self:添加系统消息(user, "#w/您获得了#y/" .. 数额 .. "#w/点仙玉，当前可用仙玉数额为#r/" .. user.仙玉 .. "#w/点")
      SendMessage(user.连接id,7,"#y/你获得了"..数额.."仙玉")
      self:添加消费日志(user,"增加"..数额.."仙玉来源"..类型)
         SendMessage(user.连接id, 3008, {Type="仙玉",Value=user.仙玉})
   end
------------------------------储备-----------------------------------------------
function RoleControl:添加储备(user,数额, 类型)
    数额= math.floor(数额)
    local 临时数额 = 数额
    local 初始余额 = user.角色.道具.货币.储备
    user.角色.道具.货币.储备 = user.角色.道具.货币.储备 + 临时数额
    SendMessage(user.连接id, 7, "#y/你获得了" .. 数额 .. "储备金")
    SendMessage(user.连接id, 9, "#xt/#w/你获得了" .. 数额 .. "储备金")
    self:添加消费日志(user,"余额" .. 初始余额 .. "，获得储备：" .. 临时数额 .. ",来源代号" .. 类型 .. "，储备余额" .. user.角色.道具.货币.储备)
      SendMessage(user.连接id, 3008, {Type="储备",Value=user.角色.道具.货币.储备})
end
------------------------------活力体力-----------------------------------------------
 function RoleControl:添加体力(user,数额)
      user.角色.当前体力 = user.角色.当前体力 + 数额
      if user.角色.当前体力 > user.角色.体力上限 then
        user.角色.当前体力 =user.角色.体力上限
      end
      SendMessage(user.连接id, 7,string.format("#y/你的体力增加了%d点",数额))
   end
 function RoleControl:扣除体力(user,数额)
      if user.角色.当前体力 < 数额 then
        return true
      else
        user.角色.当前体力=user.角色.当前体力-数额
      end
      SendMessage(user.连接id, 7, string.format("#y/你消耗了%d点体力",数额))
end
function RoleControl:添加活力(user,数额)
         user.角色.当前活力 = user.角色.当前活力 + 数额
      if user.角色.当前活力 > user.角色.活力上限 then
        user.角色.当前活力 =user.角色.活力上限
      end
      SendMessage(user.连接id, 7,string.format("#y/你的活力增加了%d点",数额))
   end
------------------------------愤怒-----------------------------------------------
function RoleControl:添加愤怒(user,数额)--------------------
  user.角色.愤怒=user.角色.愤怒+数额
  if user.角色.愤怒>150 then user.角色.愤怒=150 end
 SendMessage(user.连接id,3030,1)
 SendMessage(user.连接id,1003,self:获取角色气血数据(user))
 end

------------------------------血魔--------------------
function RoleControl:恢复血魔(user,类型,数额)--------------------
	if 类型==1 then --恢复至满状态
		user.角色.当前气血=user.角色.最大气血
		user.角色.气血上限=user.角色.最大气血
		user.角色.当前魔法=user.角色.魔法上限
	elseif 类型==2 then --恢复气血
		user.角色.当前气血=user.角色.当前气血+数额
		if user.角色.当前气血>user.角色.气血上限 then
			user.角色.当前气血=user.角色.气血上限
		end
	elseif 类型==3 then --恢复魔法
		user.角色.当前魔法=user.角色.当前魔法+数额
		if user.角色.当前魔法>user.角色.魔法上限 then 
			user.角色.当前魔法=user.角色.魔法上限 
		end
	elseif 类型==4 then
		user.角色.气血上限 =user.角色.气血上限+数额
		if user.角色.气血上限>user.角色.最大气血 then
			user.角色.气血上限=user.角色.最大气血
		end
    end
	SendMessage(user.连接id,3030,1)
	SendMessage(user.连接id,1003,self:获取角色气血数据(user))
end

function RoleControl:添加活跃度(user,数额)
  if 活动数据.活跃度[user.id] then
    if 活动数据.活跃度[user.id].积分  >=1200 then
       return
    elseif 活动数据.活跃度[user.id].积分+数额  >=1200 then
      活动数据.活跃度[user.id].积分=1200
    else
        活动数据.活跃度[user.id].积分= 活动数据.活跃度[user.id].积分+数额
    end
  else
   活动数据.活跃度[user.id]={积分=数额,[1]=0,[2]=0,[3]=0}
  end
    user.角色.活跃度=user.角色.活跃度+数额
    SendMessage(user.连接id, 7, "#y/你获得了#r/"..数额.."#y/点活跃度,今日还可获得活跃度为:#r/".. math.floor(1200-活动数据.活跃度[user.id].积分))
end

function RoleControl:添加帮贡(user,数额)
        if user.角色.帮派 then
          帮派数据[user.角色.帮派].繁荣=帮派数据[user.角色.帮派].繁荣+数额/3
          帮派数据[user.角色.帮派].成员名单[user.id].帮贡.获得=帮派数据[user.角色.帮派].成员名单[user.id].帮贡.获得+数额
          帮派数据[user.角色.帮派].成员名单[user.id].帮贡.当前=帮派数据[user.角色.帮派].成员名单[user.id].帮贡.当前+数额
          SendMessage(user.连接id,7,string.format("#Y/你获得了%d点帮贡",数额))
          SendMessage(user.连接id,7,string.format("#Y/你帮派的繁荣增加了%d点",数额/3))
        end
end



---扣除
function RoleControl:扣除活跃度(user,数额)
    user.角色.活跃度=user.角色.活跃度-数额



 -- if 活动数据.活跃度[user.id] then
 --    if 活动数据.活跃度[user.id].积分  >=1200 then
 --       return
 --    elseif 活动数据.活跃度[user.id].积分+数额  >=1200 then
 --      活动数据.活跃度[user.id].积分=1200
 --    else
 --        活动数据.活跃度[user.id].积分= 活动数据.活跃度[user.id].积分+数额
 --    end
 --  else
 --   活动数据.活跃度[user.id]={积分=数额,[1]=0,[2]=0,[3]=0}
 --  end
 --    user.角色.活跃度=user.角色.活跃度+数额
 --    SendMessage(user.连接id, 7, "#y/你获得了#r/"..数额.."#y/点活跃度,今日还可获得活跃度为:#r/".. math.floor(1200-活动数据.活跃度[user.id].积分))

    
end


function RoleControl:扣除活动积分(user,数额)
    user.角色.活动积分=user.角色.活动积分-数额
end 
function RoleControl:扣除比武积分(user,数额)
    user.角色.比武积分=user.角色.比武积分-数额
end
function RoleControl:扣除副本积分(user,数额)
    user.角色.副本积分=user.角色.副本积分-数额
end

function RoleControl:扣除地煞积分(user,数额)
    user.角色.地煞积分=user.角色.地煞积分-数额
end
function RoleControl:扣除知了积分(user,数额)
    user.角色.知了积分=user.角色.知了积分-数额
end
function RoleControl:扣除天罡积分(user,数额)
    user.角色.天罡积分=user.角色.天罡积分-数额
end

function RoleControl:扣除成就积分(user,数额)
    user.角色.成就积分=user.角色.成就积分-数额
end
function RoleControl:扣除单人积分(user,数额)
    user.角色.单人积分=user.角色.单人积分-数额
end
function RoleControl:扣除特殊积分(user,数额)
    user.角色.特殊积分=user.角色.特殊积分-数额
end
-------添加

function RoleControl:添加地煞积分(user,数额)
    user.角色.地煞积分=user.角色.地煞积分+数额
        SendMessage(user.连接id, 7, "#y/你获得了" .. 数额 .. "点地煞积分")
    SendMessage(user.连接id, 9, "#xt/#w/你获得了" .. 数额 .. "点地煞积分")
end
function RoleControl:添加法宝灵气(user,数额)
    user.角色.法宝灵气=user.角色.法宝灵气+数额
        SendMessage(user.连接id, 7, "#y/你获得了" .. 数额 .. "点法宝灵气")
    SendMessage(user.连接id, 9, "#xt/#w/你获得了" .. 数额 .. "点法宝灵气")
end
function RoleControl:添加知了积分(user,数额)
    user.角色.知了积分=user.角色.知了积分+数额
        SendMessage(user.连接id, 7, "#y/你获得了" .. 数额 .. "点知了积分")
    SendMessage(user.连接id, 9, "#xt/#w/你获得了" .. 数额 .. "点知了积分")
end
function RoleControl:添加天罡积分(user,数额)
    user.角色.天罡积分=user.角色.天罡积分+数额
        SendMessage(user.连接id, 7, "#y/你获得了" .. 数额 .. "点天罡积分")
    SendMessage(user.连接id, 9, "#xt/#w/你获得了" .. 数额 .. "点天罡积分")
end

function RoleControl:添加成就积分(user,数额)
    user.角色.成就积分=user.角色.成就积分+数额
        SendMessage(user.连接id, 7, "#y/你获得了" .. 数额 .. "点成就积分")
    SendMessage(user.连接id, 9, "#xt/#w/你获得了" .. 数额 .. "点成就积分")
end
function RoleControl:添加单人积分(user,数额)
    user.角色.单人积分=user.角色.单人积分+数额
        SendMessage(user.连接id, 7, "#y/你获得了" .. 数额 .. "点单人积分")
    SendMessage(user.连接id, 9, "#xt/#w/你获得了" .. 数额 .. "点单人积分")
end
function RoleControl:添加特殊积分(user,数额)
    user.角色.特殊积分=user.角色.特殊积分+数额
        SendMessage(user.连接id, 7, "#y/你获得了" .. 数额 .. "点特殊积分")
    SendMessage(user.连接id, 9, "#xt/#w/你获得了" .. 数额 .. "点特殊积分")
end
function RoleControl:添加比武积分(user,数额)
    user.角色.比武积分=user.角色.比武积分+数额
        SendMessage(user.连接id, 7, "#y/你获得了" .. 数额 .. "点比武积分")
    SendMessage(user.连接id, 9, "#xt/#w/你获得了" .. 数额 .. "点比武积分")
end
function RoleControl:添加副本积分(user,数额)
    user.角色.副本积分=user.角色.副本积分+数额
    SendMessage(user.连接id, 7, "#y/你获得了" .. 数额 .. "点副本积分")
    SendMessage(user.连接id, 9, "#xt/#w/你获得了" .. 数额 .. "点副本积分")
end
function RoleControl:添加活动积分(user,数额)
        user.角色.活动积分=user.角色.活动积分+数额
        SendMessage(user.连接id,7,string.format("#Y/你获得了%d点活动积分！该积分可在大转盘处抽奖",数额))
end
function RoleControl:添加剧情点(user,数额)
        user.角色.剧情技能点=user.角色.剧情技能点+数额
       SendMessage(user.连接id, 7, "#y/你获得了" .. 数额 .. "点剧情技能点")
       SendMessage(user.连接id, 9, "#xt/#w/你获得了" .. 数额 .. "点剧情技能点")
      SendMessage(user.连接id,20053,{剧情技能=user.角色.剧情技能,剧情点=user.角色.剧情技能点})
end
function RoleControl:添加修炼点(user,数额)
        if user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].上限 <= user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].等级 then
            SendMessage(user.连接id, 7, "#y/你当前的这项修炼等级已达上限，无法再在增加")
            return true
        elseif user.角色.等级 < 65 then
             SendMessage(user.连接id,7,"#Y/等级达到65级才能获得")
             return true
        else
            RoleControl:添加召唤兽修炼经验(user,数额)
        end
end
function RoleControl:添加召唤兽修炼点数(user,数额)
    user.角色.召唤兽修炼点数=user.角色.召唤兽修炼点数+数额
    SendMessage(user.连接id,9,"#xt/"..string.format("#Y/你获得了%d点召唤兽修炼点数",数额))
end

function RoleControl:扣除新年积分(user,数额)
    user.角色.新年积分=user.角色.新年积分-数额
end
function RoleControl:添加新年积分(user,数额)
    user.角色.新年积分=user.角色.新年积分+数额
        SendMessage(user.连接id, 7, "#y/你获得了" .. 数额 .. "点新年积分")
    SendMessage(user.连接id, 9, "#xt/#w/你获得了" .. 数额 .. "点新年积分")
end


function RoleControl:扣除长安保卫战积分(user,数额)
    user.角色.长安保卫战积分=user.角色.长安保卫战积分-数额
end
function RoleControl:添加长安保卫战积分(user,数额)
    user.角色.长安保卫战积分=user.角色.长安保卫战积分+数额
        SendMessage(user.连接id, 7, "#y/你获得了" .. 数额 .. "点长安保卫战积分")
    SendMessage(user.连接id, 9, "#xt/#w/你获得了" .. 数额 .. "点长安保卫战积分")
end

function RoleControl:扣除长安保卫战累计积分(user,数额)
    user.角色.长安保卫战累计积分=user.角色.长安保卫战累计积分-数额
end
function RoleControl:添加长安保卫战累计积分(user,数额)
    user.角色.长安保卫战累计积分=user.角色.长安保卫战累计积分+数额
        SendMessage(user.连接id, 7, "#y/你获得了" .. 数额 .. "点长安保卫战累计积分")
    SendMessage(user.连接id, 9, "#xt/#w/你获得了" .. 数额 .. "点长安保卫战累计积分")
end
