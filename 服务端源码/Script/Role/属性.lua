-- @Author: 作者QQ381990860
-- @Date:   2021-09-07 00:54:26
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-09-08 22:24:08
-- @Author: 作者QQ381990860
-- @Date:   2021-09-07 00:54:26
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-09-08 10:30:00
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-06-30 17:42:34
--======================================================================--
function RoleControl:刷新装备属性(user,Update)
	user.角色.敏捷 = user.角色.敏捷 - user.角色.装备属性.敏捷
	user.角色.力量 = user.角色.力量 - user.角色.装备属性.力量
	user.角色.魔力 = user.角色.魔力 - user.角色.装备属性.魔力
	user.角色.耐力 = user.角色.耐力 - user.角色.装备属性.耐力
	user.角色.体质 = user.角色.体质 - user.角色.装备属性.体质
	user.角色.特技数据 = {}
	user.角色.追加技能 = {}
	user.角色.附加技能 = {}
	user.角色.变身技能 = {}
 	user.角色.锦衣数据 ={}
	user.角色.愤怒特效 = nil
	self.临时追加 = {}
    self.临时变身 = {}
	self.临时附加 = {}
	user.角色.装备属性 = {
	    躲闪=0,
	    力量=0,
	    魔力=0,
	    耐力=0,
	    体质=0,
      	愤怒=0,
		命中 = 0,
		气血回复效果 = 0,
		固定伤害 = 0,
		气血 = 0,
		法防 = 0,
		抵抗封印等级 = 0,
		法术防御 = 0,
		灵力 = 0,
		魔法 = 0,
		法术伤害结果 = 0,
		速度 = 0,
		格挡值 = 0,
		狂暴等级 = 0,
		穿刺等级 = 0,
		防御 = 0,
		治疗能力 = 0,
		抗物理暴击等级 = 0,
		敏捷 = 0,
		法术暴击等级 = 0,
		封印命中等级 = 0,
		伤害 = 0,
		物理暴击等级 = 0,
		法术伤害 = 0,
		抗法术暴击等级 = 0
	}

	user.角色.法宝效果={}
	for n = 1, #全局变量.基础属性 do
		user.角色[全局变量.基础属性[n]] = user.角色[全局变量.基础属性[n]] - user.角色.装备三围[全局变量.基础属性[n]]
		user.角色.装备三围[全局变量.基础属性[n]] = 0
	end
	user.角色.武器数据 = {
		名称 = "",
		强化 = 0,
		等级 = 0,
		类别 = ""
	}
    --灵饰
    local 特性={}


	for n = 31, 34 do
		if user.角色.灵饰[n] ~= 0 then
      if  user.物品[user.角色.灵饰[n]].Time and user.物品[user.角色.灵饰[n]].Time <= os.time() then 
          UserData[user.id].物品[user.角色.灵饰[n]] =nil
          user.角色.灵饰[n]=0
          SendMessage(user.连接id, 7, "#y/你的道具已经过期被系统回收")   
      else
   
    			  self.幻化类型 = user.物品[user.角色.灵饰[n]].幻化属性.基础.类型
    				user.角色.装备属性[self.幻化类型] = user.角色.装备属性[self.幻化类型] + user.物品[user.角色.灵饰[n]].幻化属性.基础.数值
    				user.角色.装备属性[self.幻化类型] = user.角色.装备属性[self.幻化类型] + user.物品[user.角色.灵饰[n]].幻化属性.基础.强化
      			for i = 1, #user.物品[user.角色.灵饰[n]].幻化属性.附加 do
      				self.幻化类型 = user.物品[user.角色.灵饰[n]].幻化属性.附加[i].类型
      					user.角色.装备属性[self.幻化类型] = user.角色.装备属性[self.幻化类型] + user.物品[user.角色.灵饰[n]].幻化属性.附加[i].数值
      					user.角色.装备属性[self.幻化类型] = user.角色.装备属性[self.幻化类型] + user.物品[user.角色.灵饰[n]].幻化属性.附加[i].强化
      			end
            if  user.物品[user.角色.灵饰[n]].特性 then
                user.物品[user.角色.灵饰[n]].特性.套装 =nil
                特性[n]={user.物品[user.角色.灵饰[n]].特性.技能,user.物品[user.角色.灵饰[n]].特性.等级}
                local TempStatus = PropertyData[user.物品[user.角色.灵饰[n]].特性.技能]

                local TempStatus1= TempStatus[user.物品[user.角色.灵饰[n]].类型]
                if TempStatus.类型=="五行" or TempStatus.类型=="属性" or TempStatus.类型=="宿敌" then
                    user.角色.装备属性[TempStatus1.pro] =user.角色.装备属性[TempStatus1.pro]+user.物品[user.角色.灵饰[n]].特性.等级*TempStatus1.rate
                elseif  TempStatus.类型=="趣味" then
                     for i,v in ipairs{"躲闪","力量","魔力","耐力","体质","命中","气血回复效果","固定伤害","气血","法防","抵抗封印等级","法术防御",
                                        "灵力","魔法","法术伤害结果","速度","格挡值","狂暴等级","穿刺等级","防御","治疗能力","抗物理暴击等级","敏捷",
                                        "法术暴击等级","封印命中等级","伤害","物理暴击等级","法术伤害","抗法术暴击等级"} do
                         user.角色.装备属性[v]=user.角色.装备属性[v]+user.物品[user.角色.灵饰[n]].特性.等级*TempStatus1.rate
                     end
                end
            end
      end
		end
	end

    if 特性[31] and 特性[32] and 特性[33] and 特性[34] then
        if 特性[31][1] == 特性[32][1] and 特性[32][1]==特性[33][1] and 特性[33][1]==特性[34][1] then
            local min = 0
            local 总数 =特性[31][2]+特性[32][2]+特性[33][2]+特性[34][2]
            if  总数<=8 then
                min=1
            elseif 总数 <=16 then
                min=2
            elseif 总数 <=24 then
                min=3
            elseif 总数 <=28 then
                min=4
            else
               min=5
            end
            for i=31,34 do
                user.物品[user.角色.灵饰[i]].特性.套装 =min

            end
            if PropertyData[特性[31][1]].类型=="属性" then
                 user.角色.装备属性[PropertyData[特性[31][1]].套装.pro1]=user.角色.装备属性[PropertyData[特性[31][1]].套装.pro1]+ PropertyData[特性[31][1]]["套装"..min]
            end
        end
    end

	if user.角色.灵饰[35] ~= 0 then
		user.角色.锦衣数据.定制=user.物品[user.角色.灵饰[35]].锦衣.."_"..user.物品[user.角色.灵饰[35]].造型

	end
    ---法宝
	for n = 35, 38 do
		if user.角色.法宝[n] ~= 0 then
			local 名称 = user.物品[user.角色.法宝[n]].名称
			local 等级 =  user.物品[user.角色.法宝[n]].层数
      if  user.物品[user.角色.法宝[n]].Time and user.物品[user.角色.法宝[n]].Time <= os.time() then 
          UserData[user.id].物品[user.角色.法宝[n]] =nil
          user.角色.法宝[n]=0
          SendMessage(user.连接id, 7, "#y/你的道具已经过期被系统回收")   
      else
          if user.物品[user.角色.法宝[n]].灵气 > 0 then
            --tab切换
         
      			if 名称 == "碧玉葫芦" then
      			    user.角色.装备属性.治疗能力=user.角色.装备属性.治疗能力+((等级+1)*50)
      			elseif 名称 == "归元圣印" then
      			    user.角色.装备属性.治疗能力=user.角色.装备属性.治疗能力+((等级+1)*50)
      			 elseif 名称 == "流影云笛" then
      			    user.角色.装备属性.法术伤害=user.角色.装备属性.法术伤害+((等级+1)*30)
      			elseif 名称 == "飞剑" then
      				 user.角色.装备属性.命中=user.角色.装备属性.命中+((等级+1)*30)
      			elseif 名称 == "拭剑石" then
      				 user.角色.装备属性.伤害=user.角色.装备属性.伤害+((等级+1)*10)
      			elseif 名称 == "落星飞鸿" then
      				 user.角色.装备属性.伤害=user.角色.装备属性.伤害+((等级+1)*30)
      			elseif 名称 == "七杀" then
      				 user.角色.装备属性.伤害=user.角色.装备属性.伤害+((等级+1)*30)
      		 	elseif 名称 == "风袋" then
      				 user.角色.装备属性.速度=user.角色.装备属性.速度+((等级+1)*30)
      			elseif 名称 == "凌波仙符" then
      				 user.角色.装备属性.速度=user.角色.装备属性.速度+((等级+1)*30)
      			elseif 名称 == "附灵玉" then
      				 user.角色.装备属性.灵力=user.角色.装备属性.灵力+((等级+1)*30)
      			elseif 名称 == "金甲仙衣" then
      				 user.角色.法宝效果.金甲仙衣=(等级+1)/20
      			elseif 名称 == "降魔斗篷" then
      				 user.角色.法宝效果.降魔斗篷=(等级+1)/20
            elseif 名称 == "蟠龙玉璧" then
               user.角色.法宝效果.蟠龙玉璧=(等级+1)/10
      			elseif 名称 == "鬼脸面具" then
      				  user.角色.法宝效果.鬼脸面具 = true
      			elseif 名称 == "嗜血幡" then
      				user.角色.法宝效果.嗜血幡 = (等级+1)
      			elseif 名称 == "雷兽" then
                       user.角色.法宝效果.雷兽 = (等级+1)*1.5
                  elseif 名称 == "迷魂灯" then
                       user.角色.法宝效果.迷魂灯 = (等级+1)*1.5
                  elseif 名称 == "天师符" then
                       user.角色.法宝效果.天师符 = (等级+1)*1.5
                  elseif 名称 == "织女扇" then
                       user.角色.法宝效果.织女扇 = (等级+1)*1.5
      	        elseif 名称 == "定风珠" then
                       user.角色.法宝效果.定风珠 = (等级+1)*1.5
                 	 elseif 名称 == "幽灵珠" then
                       user.角色.法宝效果.幽灵珠 = (等级+1)*1.5
                  elseif 名称 == "金刚杵" then
                       user.角色.法宝效果.金刚杵 = (等级+1)*50
                  elseif 名称 == "普渡" then
                       user.角色.法宝效果.普渡 = (等级+1)
                  elseif 名称 == "九幽" then
                       user.角色.法宝效果.九幽 = (等级+1)*user.角色.等级*0.7+user.角色.装备属性.治疗能力
                  elseif 名称 == "通灵宝玉" then
                       user.角色.法宝效果.通灵宝玉 = (等级+1)
                  elseif 名称 == "聚宝盆" then
                       user.角色.法宝效果.聚宝盆 = (等级+1)

                  elseif 名称 == "五火神焰印" then
                        user.角色.装备属性.法术暴击等级=user.角色.装备属性.法术暴击等级+math.floor(((等级+1))*50*user.角色.等级/1000)
      			elseif 名称 == "千斗金樽" then
      				user.角色.装备属性.物理暴击等级=user.角色.装备属性.物理暴击等级+math.floor(((等级+1))*80*user.角色.等级/1000)
      			 elseif 名称 == "宿幕星河" then
      				user.角色.装备属性.法术暴击等级=user.角色.装备属性.法术暴击等级+math.floor(((等级+1))*80*user.角色.等级/1000)
      			elseif 名称 == "罗汉珠" then
      				 user.角色.法宝效果.罗汉珠=(等级+1)*4
      		 	elseif 名称 == "慈悲" then
      			    user.角色.装备属性.治疗能力=user.角色.装备属性.治疗能力+((等级+1)*30)
      			    user.角色.法宝效果.慈悲=(等级+1)*2
            elseif 名称 == "救命毫毛" then
            user.角色.法宝效果.救命毫毛=(等级+1)*3
            elseif 名称 == "宝烛" then
            user.角色.法宝效果.宝烛 = (等级+1)*20
            elseif 名称 == "天煞" then
            user.角色.法宝效果.天煞 = (等级+1)*20
            elseif 名称 == "伏魔天书" then
            user.角色.法宝效果.伏魔天书 = (等级+1)*50
            elseif 名称 == "镇海珠" then
            user.角色.法宝效果.镇海珠 = (等级+1)*50
            elseif 名称 == "翡翠芝兰" then
            user.角色.法宝效果.翡翠芝兰 = (等级+1)*50
            elseif 名称 == "月影" then
            user.角色.法宝效果.月影 = (等级+1)*3
            elseif 名称 == "斩魔" then
            user.角色.法宝效果.斩魔 = (等级+1)*60
            elseif 名称 == "河图洛书"then
            user.角色.法宝效果.河图洛书 = (等级+1)*1.8
            elseif 名称 == "炽焰丹珠"then
            user.角色.法宝效果.河图洛书 = (等级+1)*2
            elseif 名称 == "九黎战鼓" then
              user.角色.法宝效果.九黎战鼓 = (等级+1)*7
            elseif 名称 == "盘龙壁" then
            user.角色.法宝效果.盘龙壁 = (等级+1)*10
            elseif 名称 == "神行飞剑" then
            user.角色.法宝效果.神行飞剑 = (等级+1)*4
            elseif 名称 == "汇灵盏" then
            user.角色.法宝效果.汇灵盏 = (等级+1)*2
            elseif 名称 == "重明战鼓" then
            user.角色.法宝效果.重明战鼓 = (等级+1)*4
            elseif 名称 == "梦云幻甲" then
            user.角色.法宝效果.梦云幻甲 = (等级+1)*7
             elseif 名称 == "兽王令" then
            user.角色.法宝效果.兽王令 = (等级+1)*8

      			end
          end
       end
		end
	end
    --锦衣
    for n = 40, 45 do
      if user.角色.锦衣[n] ~= 0 then
          if  user.物品[user.角色.锦衣[n]].Time and user.物品[user.角色.锦衣[n]].Time <= os.time() then 
          UserData[user.id].物品[user.角色.锦衣[n]] =nil
       
          user.角色.锦衣[n]=0
          SendMessage(user.连接id, 7, "#y/你的道具已经过期被系统回收")   
          else
            local  Name =user.物品[user.角色.锦衣[n]].名称
              if n==45 then
                 user.角色.锦衣数据.光环=Name
                elseif n ==44 then
                   user.角色.锦衣数据.战斗锦衣=Name
               elseif n ==43 then
                 user.角色.锦衣数据.脚印=Name
               elseif n ==42 then
                 user.角色.锦衣数据.锦衣=Name
              end
              for k,v in pairs(ItemData[Name].属性) do
                  user.角色.装备属性[k]=user.角色.装备属性[k]+v
              end
            end
      end
    end

	for n = 21, 26 do
  
  	   if user.角色.装备数据[n] ~= nil and user.物品[user.角色.装备数据[n]] ~= nil and user.物品[user.角色.装备数据[n]].耐久度 > 0 then
             if  user.物品[user.角色.装备数据[n]].Time and user.物品[user.角色.装备数据[n]].Time <= os.time() then 
          UserData[user.id].物品[user.角色.装备数据[n]] =nil
          UserData[user.id].角色.装备数据[n]=nil
          SendMessage(user.连接id, 7, "#y/你的道具已经过期被系统回收")
     else 
    			if user.物品[user.角色.装备数据[n]].装备属性 ~= nil then
    				self.幻化类型 = user.物品[user.角色.装备数据[n]].装备属性.基础.类型
    				user.角色.装备属性[self.幻化类型] = user.角色.装备属性[self.幻化类型] + user.物品[user.角色.装备数据[n]].装备属性.基础.数值
    				user.角色.装备属性[self.幻化类型] = user.角色.装备属性[self.幻化类型] + user.物品[user.角色.装备数据[n]].装备属性.基础.强化
    				for i = 1, #user.物品[user.角色.装备数据[n]].装备属性.附加 do
    					self.幻化类型 = user.物品[user.角色.装备数据[n]].装备属性.附加[i].类型
    					user.角色.装备属性[self.幻化类型] = user.角色.装备属性[self.幻化类型] + user.物品[user.角色.装备数据[n]].装备属性.附加[i].数值
    					user.角色.装备属性[self.幻化类型] = user.角色.装备属性[self.幻化类型] + user.物品[user.角色.装备数据[n]].装备属性.附加[i].强化
    				end
    			end
    			if user.物品[user.角色.装备数据[n]].套装 ~= nil then
    				self.套装名称 = user.物品[user.角色.装备数据[n]].套装.名称

    				if user.物品[user.角色.装备数据[n]].套装.类型 == "追加法术" then
    					if self.临时追加[self.套装名称] == nil then
    						self.临时追加[self.套装名称] = 1
    					else
    						self.临时追加[self.套装名称] = self.临时追加[self.套装名称] + 1
    					end
            elseif user.物品[user.角色.装备数据[n]].套装.类型 == "变身术" then
              if self.临时变身[self.套装名称] == nil then
                self.临时变身[self.套装名称] = 1
              else
                self.临时变身[self.套装名称] = self.临时变身[self.套装名称] + 1
              end
    				elseif self.临时附加[self.套装名称] == nil then
    					self.临时附加[self.套装名称] = 1
    				else
    					self.临时附加[self.套装名称] = self.临时附加[self.套装名称] + 1
    				end
    			end
    			if user.物品[user.角色.装备数据[n]].熔炼~=nil then
    				for i=1,#user.物品[user.角色.装备数据[n]].熔炼 do

    						user.角色.装备属性[user.物品[user.角色.装备数据[n]].熔炼[i].类型]=user.角色.装备属性[user.物品[user.角色.装备数据[n]].熔炼[i].类型]+user.物品[user.角色.装备数据[n]].熔炼[i].属性
    				end
    			end
    			if user.物品[user.角色.装备数据[n]].附魔~=nil then
    				if user.物品[user.角色.装备数据[n]].附魔.时间 <= os.time() then
    					user.物品[user.角色.装备数据[n]].附魔 = nil
    				else
    				user.角色.装备属性[user.物品[user.角色.装备数据[n]].附魔.类型]=user.角色.装备属性[user.物品[user.角色.装备数据[n]].附魔.类型]+user.物品[user.角色.装备数据[n]].附魔.属性

    				end
    			end
          if user.物品[user.角色.装备数据[n]].特殊属性 then
              user.角色.装备属性[user.物品[user.角色.装备数据[n]].特殊属性.类型]=user.角色.装备属性[user.物品[user.角色.装备数据[n]].特殊属性.类型]+user.物品[user.角色.装备数据[n]].特殊属性.属性
          end
    			if user.物品[user.角色.装备数据[n]].特效 ~= nil then
    				for i = 1, #user.物品[user.角色.装备数据[n]].特效 do
    					if user.物品[user.角色.装备数据[n]].特效[i] == "愤怒" then
    						user.角色.愤怒特效 = true
    					end
    				end
    			end
    			if user.物品[user.角色.装备数据[n]].特技 ~= nil then
    				self.特技重复 = false
    				for i = 1, #user.角色.特技数据 do
    					if user.角色.特技数据[i].名称 == user.物品[user.角色.装备数据[n]].特技 then
    						self.特技重复 = true
    					end
    				end

                    user.角色.特技数据[#user.角色.特技数据 + 1] =  置技能(user.物品[user.角色.装备数据[n]].特技)
    			end
    			if user.物品[user.角色.装备数据[n]].三围属性 == nil then
    				user.物品[user.角色.装备数据[n]].三围属性 = {}
    			end
    			if user.物品[user.角色.装备数据[n]].临时效果 ~= nil then
            for i=#user.物品[user.角色.装备数据[n]].临时效果,1,-1 do
            
                if user.物品[user.角色.装备数据[n]].临时效果[i].时间 <= os.time() then
                   table.remove(user.物品[user.角色.装备数据[n]].临时效果, i)
                else
                  user.角色.装备属性[user.物品[user.角色.装备数据[n]].临时效果[i].类型] = user.角色.装备属性[user.物品[user.角色.装备数据[n]].临时效果[i].类型] + user.物品[user.角色.装备数据[n]].临时效果[i].数值
                end
            end
    			end
    			for i = 1, #user.物品[user.角色.装备数据[n]].三围属性 do
    				self.临时数值 = user.物品[user.角色.装备数据[n]].三围属性[i].数值
    				self.临时类型 = user.物品[user.角色.装备数据[n]].三围属性[i].类型
    				user.角色.装备三围[self.临时类型] = user.角色.装备三围[self.临时类型] + self.临时数值
                if user.物品[user.角色.装备数据[n]].升星  and user.物品[user.角色.装备数据[n]].升星> 0 then 
                 user.角色.装备三围[self.临时类型] = user.角色.装备三围[self.临时类型] + user.物品[user.角色.装备数据[n]].升星*10  --装备升星1点
              end
    			end
    			for i = 1, #user.物品[user.角色.装备数据[n]].锻造数据 do
    				user.角色.装备属性[user.物品[user.角色.装备数据[n]].锻造数据[i].类型] = user.角色.装备属性[user.物品[user.角色.装备数据[n]].锻造数据[i].类型] + user.物品[user.角色.装备数据[n]].锻造数据[i].数值
    			end
          --符石
          if user.物品[user.角色.装备数据[n]].符石~= nil and #user.物品[user.角色.装备数据[n]].符石>0 then
              if user.物品[user.角色.装备数据[n]].符石.组合~=nil then
                local 符石名称 = user.物品[user.角色.装备数据[n]].符石.组合.名称
                local 符石等级 = user.物品[user.角色.装备数据[n]].符石.组合.等级
                if 符石名称 == "万丈霞光" then
                        if 符石等级 ==1  then
                          user.角色.装备属性.气血回复效果=user.角色.装备属性.气血回复效果+50
                        elseif  符石等级 ==2  then
                           user.角色.装备属性.气血回复效果=user.角色.装备属性.气血回复效果+80
                        elseif  符石等级 ==3  then
                            user.角色.装备属性.气血回复效果=user.角色.装备属性.气血回复效果+120
                       elseif  符石等级 ==4  then
                            user.角色.装备属性.气血回复效果=user.角色.装备属性.气血回复效果+200
                        end
                elseif 符石名称 == "望穿秋水" then
                  user.角色.装备属性.灵力=user.角色.装备属性.灵力+60
                elseif 符石名称 == "无懈可击" then
                  user.角色.装备属性.防御=user.角色.装备属性.防御+30
                elseif 符石名称 == "万里横行" then
                  user.角色.装备属性.伤害=user.角色.装备属性.伤害+40
                elseif 符石名称 == "日落西山" then
                   user.角色.装备属性.速度=user.角色.装备属性.速度+40
                 elseif 符石名称 == "高山流水" then
                        if 符石等级 ==1  then
                          user.角色.装备属性.法术伤害=user.角色.装备属性.法术伤害+math.floor(user.角色.等级/3+300)
                        elseif  符石等级 ==2  then
                           user.角色.装备属性.法术伤害=user.角色.装备属性.法术伤害+math.floor(user.角色.等级/2+300)
                        elseif  符石等级 ==3  then
                            user.角色.装备属性.法术伤害=user.角色.装备属性.法术伤害+math.floor(user.角色.等级+300)
                        end
                 elseif 符石名称 == "百无禁忌" then
                        if 符石等级 ==1  then
                          user.角色.装备属性.抵抗封印等级=user.角色.装备属性.抵抗封印等级+math.floor(4*100*user.角色.等级/1000)
                        elseif  符石等级 ==2  then
                           user.角色.装备属性.抵抗封印等级=user.角色.装备属性.抵抗封印等级+math.floor(8*100*user.角色.等级/1000)
                        elseif  符石等级 ==3  then
                            user.角色.装备属性.抵抗封印等级=user.角色.装备属性.抵抗封印等级+math.floor(12*100*user.角色.等级/1000)
                        end
                 end
              end
             for i=1,#user.物品[user.角色.装备数据[n]].符石 do
                 if user.物品[user.角色.装备数据[n]].符石[i]~=nil and  user.物品[user.角色.装备数据[n]].符石[i].耐久>0 then
                          local Name = user.物品[user.角色.装备数据[n]].符石[i].名称

                          for i,v in ipairs({"气血","魔法","命中","伤害","防御","速度","灵力","敏捷","法术防御","法术伤害","固定伤害","力量","体质","耐力","魔力"}) do

                              if ItemData[Name].属性[v] then
                                   user.角色.装备属性[v] = user.角色.装备属性[v] +  ItemData[Name].属性[v]
                              end
                          end
                 end
             end
          end
                --装备
          if n == 23 then
              if user.物品[user.角色.装备数据[n]].升星  and user.物品[user.角色.装备数据[n]].升星> 0 then 

                    user.角色.装备属性.伤害 = user.角色.装备属性.伤害+math.floor(user.物品[user.角色.装备数据[n]].伤害 * (user.物品[user.角色.装备数据[n]].升星/10))
                     user.角色.装备属性.命中 = user.角色.装备属性.命中+math.floor(user.物品[user.角色.装备数据[n]].命中 * (user.物品[user.角色.装备数据[n]].升星/10))
              end
              user.角色.装备属性.伤害 = user.角色.装备属性.伤害 + user.物品[user.角色.装备数据[n]].伤害
              user.角色.装备属性.命中 = user.角色.装备属性.命中 + user.物品[user.角色.装备数据[n]].命中
              user.角色.武器数据 = {
                  强化 = 0,
                  名称 = user.物品[user.角色.装备数据[n]].名称,
                  类别 = 取武器类型(user.物品[user.角色.装备数据[n]].名称),
                  等级 = user.物品[user.角色.装备数据[n]].等级
              }
              if user.物品[user.角色.装备数据[n]].强化 == nil then
                  user.角色.武器数据.强化 = nil
              else
                  user.角色.武器数据.强化 = 1
              end
              if user.物品[user.角色.装备数据[n]].染色 then
                user.角色.武器数据.染色 = table.copy(user.物品[user.角色.装备数据[n]].染色)
                 user.角色.装备属性[user.物品[user.角色.装备数据[n]].染色.类型]=user.角色.装备属性[user.物品[user.角色.装备数据[n]].染色.类型]+user.物品[user.角色.装备数据[n]].染色.属性
              end
              -- SendMessage(user.连接id, 2010, self:获取地图数据(user))
              -- MapControl:MapSend(user.id, 1016, self:获取地图数据(user), user.地图)

          elseif n == 21 then
              if user.物品[user.角色.装备数据[n]].升星  and user.物品[user.角色.装备数据[n]].升星> 0 then 
                    user.角色.装备属性.魔法 = user.角色.装备属性.魔法+math.floor(user.物品[user.角色.装备数据[n]].魔法 * (user.物品[user.角色.装备数据[n]].升星/10))
                     user.角色.装备属性.防御 = user.角色.装备属性.防御+math.floor(user.物品[user.角色.装备数据[n]].防御 * (user.物品[user.角色.装备数据[n]].升星/10))
              end
              user.角色.装备属性.魔法 = user.角色.装备属性.魔法 + user.物品[user.角色.装备数据[n]].魔法
              user.角色.装备属性.防御 = user.角色.装备属性.防御 + user.物品[user.角色.装备数据[n]].防御

          elseif n == 25 then
                if user.物品[user.角色.装备数据[n]].升星 and user.物品[user.角色.装备数据[n]].升星> 0 then 
                     user.角色.装备属性.气血 = user.角色.装备属性.气血+math.floor(user.物品[user.角色.装备数据[n]].气血 * (user.物品[user.角色.装备数据[n]].升星/10))
                      user.角色.装备属性.防御 =user.角色.装备属性.防御 + math.floor(user.物品[user.角色.装备数据[n]].防御 * (user.物品[user.角色.装备数据[n]].升星/10))
              end
              user.角色.装备属性.气血 = user.角色.装备属性.气血 + user.物品[user.角色.装备数据[n]].气血
              user.角色.装备属性.防御 = user.角色.装备属性.防御 + user.物品[user.角色.装备数据[n]].防御

          elseif n == 22 then
                if user.物品[user.角色.装备数据[n]].升星 and user.物品[user.角色.装备数据[n]].升星> 0 then 
                     user.角色.装备属性.灵力 =  user.角色.装备属性.灵力+math.floor(user.物品[user.角色.装备数据[n]].灵力 * (user.物品[user.角色.装备数据[n]].升星/10))
              end
              user.角色.装备属性.灵力 = user.角色.装备属性.灵力 + user.物品[user.角色.装备数据[n]].灵力
              user.角色.装备属性.法防 = user.角色.装备属性.灵力
          elseif n == 26 then
                          if user.物品[user.角色.装备数据[n]].升星 and user.物品[user.角色.装备数据[n]].升星> 0 then 
                    user.角色.装备属性.敏捷 = user.角色.装备属性.敏捷+math.floor(user.物品[user.角色.装备数据[n]].敏捷 * (user.物品[user.角色.装备数据[n]].升星/10))
                     user.角色.装备属性.防御 =  user.角色.装备属性.防御+math.floor(user.物品[user.角色.装备数据[n]].防御 * (user.物品[user.角色.装备数据[n]].升星/10))
              end
              user.角色.装备属性.敏捷 = user.角色.装备属性.敏捷 + user.物品[user.角色.装备数据[n]].敏捷
              user.角色.装备属性.防御 = user.角色.装备属性.防御 + user.物品[user.角色.装备数据[n]].防御

          elseif n == 24 then
                 if user.物品[user.角色.装备数据[n]].升星 and user.物品[user.角色.装备数据[n]].升星> 0  then 
                  user.角色.装备属性.防御 =user.角色.装备属性.防御+ math.floor(user.物品[user.角色.装备数据[n]].防御 * (user.物品[user.角色.装备数据[n]].升星/10))
              end
              user.角色.装备属性.防御 = user.角色.装备属性.防御 + user.物品[user.角色.装备数据[n]].防御

          end
        end
  		 end
  
	end

	for n, v in pairs(self.临时追加) do
		if self.临时追加[n] >= 3 then
			user.角色.追加技能[#user.角色.追加技能 + 1] = n
		end
	end
  for n, v in pairs(self.临时变身) do
    if self.临时变身[n] >= 3 then
      user.角色.变身技能[#user.角色.变身技能 + 1] = n
    end
  end
	for n, v in pairs(self.临时附加) do
		if self.临时附加[n] >= 3 then
			user.角色.附加技能[#user.角色.附加技能 + 1] = n
		end
	end

	for n = 1, #全局变量.基础属性 do
		user.角色[全局变量.基础属性[n]] = user.角色[全局变量.基础属性[n]] + user.角色.装备三围[全局变量.基础属性[n]]
	end
	user.角色.力量=  user.角色.力量+user.角色.装备属性.力量
	user.角色.魔力=  user.角色.魔力+user.角色.装备属性.魔力
	user.角色.耐力=  user.角色.耐力+user.角色.装备属性.耐力
	user.角色.体质=  user.角色.体质+user.角色.装备属性.体质
	user.角色.敏捷 = user.角色.敏捷 + user.角色.装备属性.敏捷
    user.角色.装备属性.固定伤害 =user.角色.辅助技能[4].等级*2+user.角色.装备属性.固定伤害+user.角色.强化技能[6].等级
    user.角色.装备属性.治疗能力 =user.角色.装备属性.治疗能力+user.角色.强化技能[7].等级
   if user.角色.奇经八脉.咒法 then
     user.角色.装备属性.法术伤害=user.角色.装备属性.法术伤害+60
   end
   if user.角色.奇经八脉.锤炼 then
     user.角色.装备属性.伤害=math.floor(user.角色.装备属性.伤害*1.03)
   end
   if user.角色.神器 then
      for i=1,5 do
                        
          user.角色.装备属性[user.角色.神器.属性[i].类型]=user.角色.装备属性[user.角色.神器.属性[i].类型]+user.角色.神器.属性[i].数额
           
      end

   end

    if Update   then
         SendMessage(user.连接id, 2010, self:获取地图数据(user))
         MapControl:MapSend(user.id, 1016, self:获取地图数据(user), user.地图)
    end
	RoleControl:刷新战斗属性(user,1)
 end
function RoleControl:索取装备信息(user)
    local fhxx={}
    fhxx.造型=user.角色.造型
    fhxx.名称=user.角色.名称
    fhxx.等级=user.角色.等级
    fhxx.id=user.角色.id
    fhxx.武器数据=user.角色.武器数据
    for n = 21, 26 do
        if user.角色.装备数据[n] ~= nil and user.物品[user.角色.装备数据[n]] ~= nil then
            fhxx[n]=user.物品[user.角色.装备数据[n]]
        end
    end
    return fhxx
 end
function RoleControl:刷新战斗属性(user,恢复)
  local 坐骑力量,坐骑敏捷,坐骑魔力,坐骑耐力,坐骑体质=0,0,0,0,0
   if user.角色.坐骑.编号 ~= 0  then
   坐骑体质=math.floor(user.角色.坐骑数据[user.角色.坐骑.编号].体质*user.角色.坐骑数据[user.角色.坐骑.编号].成长/25)
   坐骑力量=math.floor(user.角色.坐骑数据[user.角色.坐骑.编号].力量*user.角色.坐骑数据[user.角色.坐骑.编号].成长/21)
   坐骑魔力=math.floor(user.角色.坐骑数据[user.角色.坐骑.编号].魔力*user.角色.坐骑数据[user.角色.坐骑.编号].成长/31)
   坐骑耐力=math.floor(user.角色.坐骑数据[user.角色.坐骑.编号].耐力*user.角色.坐骑数据[user.角色.坐骑.编号].成长/24)
   坐骑敏捷=math.floor(user.角色.坐骑数据[user.角色.坐骑.编号].敏捷*user.角色.坐骑数据[user.角色.坐骑.编号].成长/33)
  end
 if user.角色.种族=="人" then
   user.角色.伤害=math.floor((user.角色.力量+坐骑力量)*0.7)+34
   user.角色.命中=math.floor((user.角色.力量+坐骑力量)*2)+30
   user.角色.灵力=math.floor((user.角色.力量+坐骑力量)*0.2+(user.角色.魔力+坐骑魔力)*0.9+(user.角色.体质+坐骑体质)*0.3+(user.角色.耐力+坐骑耐力)*0.2)
   user.角色.速度=math.floor((user.角色.敏捷+坐骑敏捷)*0.7+(user.角色.力量+坐骑力量)*0.1+(user.角色.体质+坐骑体质)*0.1+(user.角色.耐力+坐骑耐力)*0.1)
   user.角色.躲闪=math.floor(user.角色.敏捷+坐骑敏捷)
   user.角色.魔法上限=math.floor((user.角色.魔力+坐骑魔力)*3)+80
   user.角色.最大气血=math.floor((user.角色.体质+坐骑体质)*5)+100
   user.角色.防御=math.floor((user.角色.耐力+坐骑耐力)*1.5)
 elseif user.角色.种族=="魔" then
   user.角色.伤害=math.floor((user.角色.力量+坐骑力量)*0.8)+34
   user.角色.命中=math.floor((user.角色.力量+坐骑力量)*2.3)+27
   user.角色.防御=math.floor((user.角色.耐力+坐骑耐力)*1.3)
   user.角色.灵力=math.floor((user.角色.力量+坐骑力量)*0.2+(user.角色.魔力+坐骑魔力)*0.9+(user.角色.体质+坐骑体质)*0.3+(user.角色.耐力+坐骑耐力)*0.2)
   user.角色.速度=math.floor((user.角色.敏捷+坐骑敏捷)*0.7+(user.角色.力量+坐骑力量)*0.1+(user.角色.体质+坐骑体质)*0.1+(user.角色.耐力+坐骑耐力)*0.1)
   user.角色.躲闪=math.floor(user.角色.敏捷+坐骑敏捷)
   user.角色.魔法上限=math.floor((user.角色.魔力+坐骑魔力)*2.5)+80
   user.角色.最大气血=math.floor((user.角色.体质+坐骑体质)*6)+100
 elseif user.角色.种族=="仙" then
   user.角色.伤害=math.floor((user.角色.力量+坐骑力量)*0.6)+40
   user.角色.命中=math.floor((user.角色.力量+坐骑力量)*1.7)+30
   user.角色.防御=math.floor((user.角色.耐力+坐骑耐力)*1.6)
   user.角色.速度=math.floor((user.角色.敏捷+坐骑敏捷)*0.7+(user.角色.力量+坐骑力量)*0.1+(user.角色.体质+坐骑体质)*0.1+(user.角色.耐力+坐骑耐力)*0.1)
   user.角色.灵力=math.floor((user.角色.力量+坐骑力量)*0.2+(user.角色.魔力+坐骑魔力)*0.9+(user.角色.体质+坐骑体质)*0.3+(user.角色.耐力+坐骑耐力)*0.2)
   user.角色.躲闪=math.floor(user.角色.敏捷+坐骑敏捷)
   user.角色.魔法上限=math.floor((user.角色.魔力+坐骑魔力)*3.5)+80
   user.角色.最大气血=math.floor((user.角色.体质+坐骑体质)*4.5)+100
 end
  if user.角色.奇经八脉.狂狷 and user.角色.耐力>10 then

     user.角色.伤害=math.floor(user.角色.伤害+(user.角色.力量/user.角色.耐力)*16)
  end
    if user.角色.奇经八脉.神附 then
     user.角色.伤害=math.floor(user.角色.伤害+(user.角色.力量*0.08))
  end
    if user.角色.奇经八脉.海沸 then
     user.角色.伤害=math.floor(user.角色.伤害+(user.角色.力量*0.08))
  end
  if user.角色.奇经八脉.夜行 then
     user.角色.伤害=math.floor(user.角色.伤害+40)
     user.角色.速度=math.floor(user.角色.速度+40)
  end
   if user.角色.奇经八脉.倩影 then
     user.角色.速度=user.角色.速度+50
  end

  user.角色.躲闪=user.角色.装备属性.躲闪+user.角色.躲闪+user.角色.技能属性.躲避
  user.角色.命中 =  math.floor(user.角色.命中 + user.角色.装备属性.命中  +user.角色.强化技能[1].等级*3+user.角色.技能属性.命中)

  user.角色.防御 =  math.floor(user.角色.防御 + user.角色.装备属性.防御 + 5 +user.角色.强化技能[5].等级*2 +user.角色.技能属性.防御)
  user.角色.法防 = math.floor(user.角色.装备属性.法防+user.角色.装备属性.法术防御 + user.角色.魔力 * 0.35 + user.角色.灵力 * 0.25+user.角色.技能属性.法术防御)
  user.角色.灵力 = math.floor(user.角色.灵力 + user.角色.装备属性.灵力 +user.角色.技能属性.灵力+ user.角色.强化技能[3].等级*1)
  user.角色.速度 = math.floor(user.角色.速度 + user.角色.装备属性.速度 + user.角色.强化技能[4].等级*1 +user.角色.技能属性.速度)
  user.角色.魔法上限 = user.角色.魔法上限 + user.角色.装备属性.魔法+user.角色.技能属性.魔法
  user.角色.最大气血 = user.角色.最大气血 + user.角色.装备属性.气血+user.角色.技能属性.气血+user.角色.辅助技能[3].等级*4
  user.角色.最大气血 = math.floor(user.角色.最大气血 * (1 + 0.01*user.角色.辅助技能[1].等级))
  user.角色.魔法上限 = math.floor(user.角色.魔法上限 * (1 + 0.01 *user.角色.辅助技能[2].等级))
  user.角色.体力上限 =	user.角色.等级 * 5 + 10+user.角色.辅助技能[7].等级*4
  user.角色.活力上限 =  user.角色.等级 * 5 + 10+user.角色.辅助技能[8].等级*4
   if user.角色.奇经八脉.护佑 then
     user.角色.防御=user.角色.防御+50
     user.角色.法防=user.角色.法防+50
  end
  if user.角色.变身.属性.类型 ~=0  and user.角色.变身.造型~=nil then ---1类型 2 属性 3 数值
      if user.角色.变身.属性.类型 == 1 then
       user.角色[user.角色.变身.属性.属性] =user.角色[user.角色.变身.属性.属性]+user.角色.变身.属性.数值
      elseif user.角色.变身.属性.类型 == 2 then
      user.角色[user.角色.变身.属性.属性] =math.floor(user.角色[user.角色.变身.属性.属性]+user.角色[user.角色.变身.属性.属性]*user.角色.变身.属性.数值)
      elseif user.角色.变身.属性.类型 == 3 then
       user.角色[user.角色.变身.属性.属性] =math.floor(user.角色[user.角色.变身.属性.属性]-user.角色[user.角色.变身.属性.属性]*user.角色.变身.属性.数值)
      end
   end
    user.角色.伤害 = math.floor(user.角色.伤害 + user.角色.装备属性.伤害 +user.角色.强化技能[2].等级*1.5 + user.角色.命中 * 0.35 +user.角色.技能属性.伤害)
        
  if LabelData[user.角色.称谓.当前] and LabelData[user.角色.称谓.当前].属性  then
     for k,v in pairs(LabelData[user.角色.称谓.当前].属性) do
       user.角色[k]=user.角色[k]+v
     end
  end

  if user.角色.锦衣数据.定制 then
     user.角色.最大气血=user.角色.最大气血+ 1000
  end
  user.角色.气血上限 = user.角色.最大气血
    if user.角色.飞升  then
     user.角色.最大气血= user.角色.最大气血+500
  end

  user.角色.气血上限 = user.角色.最大气血
  if user.角色.渡劫  then
     user.角色.最大气血= user.角色.最大气血+1000
  end

  user.角色.气血上限 = user.角色.最大气血
  if user.角色.气血上限 == nil then
    user.角色.气血上限 = user.角色.最大气血
  end
  if user.角色.当前气血 == nil then
    user.角色.当前气血 = user.角色.气血上限
  end
  if user.角色.当前魔法 == nil then
    user.角色.当前魔法 = user.角色.魔法上限
  end
  if user.角色.最大气血 < user.角色.气血上限 then
    user.角色.气血上限 = user.角色.最大气血
  end
  if user.角色.气血上限 < user.角色.当前气血 then
    user.角色.当前气血 = user.角色.气血上限
  end
  if user.角色.魔法上限 < user.角色.当前魔法 then
    user.角色.当前魔法 = user.角色.魔法上限
  end

  if 恢复 == nil then
    user.角色.气血上限 = user.角色.最大气血
    user.角色.当前气血 = user.角色.最大气血
    user.角色.当前魔法 = user.角色.魔法上限
  end
  if user ~= nil then
    SendMessage(user.连接id, 1003, self:获取角色气血数据(user))
  end
   SendMessage(user.连接id, 3030, "1")  
 end

function RoleControl:加点事件(user, 数据)
  数据= 分割文本(数据,"*-*")
 if user.角色.潜能 <= 0 then
    SendMessage(user.连接id, 7, "#y/你没有那么多可分配的属性点")
    return 0
  elseif 数据[1]+0 < 0 then
    封禁账号(user,"CE修改")
    return
  elseif 数据[2]+0 < 0 then
        封禁账号(user,"CE修改")
    return
  elseif 数据[3]+0 < 0 then
        封禁账号(user,"CE修改")
    return
  elseif 数据[4]+0 < 0 then
      封禁账号(user,"CE修改")
    return
  elseif 数据[5]+0 < 0 then
        封禁账号(user,"CE修改")
        return
  elseif user.角色.潜能<数据[1]+数据[2]+数据[3]+数据[4]+数据[5] then
   SendMessage(user.连接id,7,"#y/你没有那么多可分配的属性点")
  else

 user.角色.力量= user.角色.力量+数据[1]
 user.角色.体质= user.角色.体质+数据[2]
 user.角色.魔力= user.角色.魔力+数据[3]
 user.角色.耐力= user.角色.耐力+数据[4]
 user.角色.敏捷= user.角色.敏捷+数据[5]
 user.角色.潜能 =  user.角色.潜能 - (数据[1]+数据[2]+数据[3]+数据[4]+数据[5])
   SendMessage(user.连接id,7,"#y/属性点分配成功")
  self:刷新战斗属性(user,1)

   end
 end