--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-12-12 00:05:58
--======================================================================--
function RoleControl:添加随机坐骑(user)
  if user.角色.种族=="人" then
   self.随机坐骑={"汗血宝马","宝贝葫芦","欢喜羊羊","神气小龟"}
  elseif user.角色.种族=="魔" then
   self.随机坐骑={"魔力斗兽","宝贝葫芦","披甲战狼","神气小龟"}
   elseif user.角色.种族=="仙" then
   self.随机坐骑={"云魅仙鹿","宝贝葫芦","闲云野鹤","神气小龟"}
   end
   local 公共坐骑 = {"宝贝葫芦"}
 if user.角色.造型 == "偃无师" or user.角色.造型 == "鬼潇潇"or user.角色.造型 == "桃夭夭" then
 	self:创建坐骑(user,公共坐骑[math.random(1,#公共坐骑)])
  else
  	self:创建坐骑(user,self.随机坐骑[math.random(1,#self.随机坐骑)])
 end
   

 end
function RoleControl:添加祥瑞(user, 类型)
		user.角色.祥瑞数据[#user.角色.祥瑞数据 + 1] = {}
		user.角色.祥瑞数据[#user.角色.祥瑞数据] ={
			类型 = 类型,
			名称=类型,
			编号 = #user.角色.祥瑞数据}

	SendMessage(user.连接id, 7, "#y/你获得了新祥瑞#r/" .. 类型)
end
function RoleControl:创建坐骑(user, 类型)
        user.角色.坐骑数据[#user.角色.坐骑数据 + 1] = {}
        user.角色.坐骑数据[#user.角色.坐骑数据] ={
        敏捷 = 0,
        体质 = 0,
        当前经验 = 0,
        环境度 = 100,
        魔力 = 0,
        耐力 = 0,
        好感度 = 100,
        饱食度 = 100,
        力量 = 0,
        潜能 = 0,
        等级 = 0,
        摄灵珠 = 0,
        升级经验 = 400,
        类型 = 类型,
        名称=类型,
        编号 = #user.角色.坐骑数据
        }
        for n = 1, 50 do
        self.临时类型 = 全局变量.基础属性[math.random( #全局变量.基础属性)]
        user.角色.坐骑数据[#user.角色.坐骑数据][self.临时类型] = user.角色.坐骑数据[#user.角色.坐骑数据][self.临时类型] + 1
        end

        local cz = {0.95,1.1,1.2,1.3,1.35}
        local cz1 = math.random(100)
        if cz1 < 30 then
        self.临时成长 = cz[1]+math.random(20)/1000
        elseif cz1 > 30  and cz1 < 60 then
        self.临时成长 = cz[2]+math.random(20)/1000
        elseif cz1 > 60  and cz1 < 80 then
        self.临时成长 = cz[3]+math.random(20)/1000
        elseif cz1 > 80  and cz1 < 95 then
        self.临时成长 = cz[4]+math.random(20)/1000
        elseif cz1 > 95  and cz1 < 100 then
        self.临时成长 = cz[5]+math.random(20)/1000
        end

        if self.临时成长 == nil  or self.临时成长 ==0 then
        self.临时成长 = cz[1]
        end
        user.角色.坐骑数据[#user.角色.坐骑数据].成长 = self.临时成长
        SendMessage(user.连接id, 7, "#y/你获得了新坐骑#r/" .. 类型)
        
end
function RoleControl:坐骑骑乘处理(user, 编号)
	if 编号 == user.角色.坐骑.编号 then
		user.角色.坐骑 = {编号=0}
        self:刷新战斗属性(user,1)
		SendMessage(user.连接id, 2010, self:获取地图数据(user)) 
		MapControl:MapSend(user.id, 1016, self:获取地图数据(user), user.地图)

	
		SendMessage(user.连接id, 20032, self:取坐骑数据(user))
	else
        if  user.角色.坐骑数据[编号] ==nil then
            SendMessage(user.连接id, 7, "#y/当前坐骑不存在，请刷新坐骑数据")
            return 
        end
        if 可坐骑(user.角色.造型,user.角色.种族,user.角色.坐骑数据[编号].类型) then

			user.角色.坐骑={编号 = 编号,类型 = user.角色.坐骑数据[编号].类型}
			if user.角色.坐骑数据[编号].染色组 then
				user.角色.坐骑.染色组 =  table.copy(user.角色.坐骑数据[编号].染色组)
			end
	         if user.角色.坐骑数据[编号].饰品 then
	         	user.角色.坐骑.饰品 = user.角色.坐骑数据[编号].饰品
	         	if user.角色.坐骑数据[编号].装备.染色组 then
	         	   user.角色.坐骑.饰品染色组 = table.copy(user.角色.坐骑数据[编号].装备.染色组)
	         	end
	         end
			self:刷新战斗属性(user,1)
			SendMessage(user.连接id, 2010, self:获取地图数据(user))
			MapControl:MapSend(user.id, 1016, self:获取地图数据(user), user.地图)

			
			SendMessage(user.连接id, 20032, self:取坐骑数据(user))
	   else
	   	SendMessage(user.连接id, 7, "#Y/这坐骑该角色无法骑乘")
	   end
	end
end
function RoleControl:取坐骑数据(user)
	local 发送信息 ={}
 发送信息.坐骑数据=user.角色.坐骑数据
 发送信息.坐骑= user.角色.坐骑
 return  发送信息
end
function RoleControl:坐骑驯养处理(user, 编号)
	if user.角色.坐骑数据[编号].等级 >= user.角色.等级 + 15 then
		SendMessage(user.连接id, 7, "#y/你的坐骑等级已经超过人物15级了")
		return 0
	elseif user.角色.当前经验 < 2000000 then
		SendMessage(user.连接id, 7, "#y/每次驯养都需要消耗200万人物经验")
		return 0
	elseif user.角色.当前体力 < 10 then
		SendMessage(user.连接id, 7, "#y/每次驯养都需要消耗10点体力")
		return 0
	else
		user.角色.当前体力 = user.角色.当前体力 - 10
		user.角色.当前经验 = user.角色.当前经验 - 2000000
		user.角色.坐骑数据[编号].当前经验 = user.角色.坐骑数据[编号].当前经验 + 1500000

		while user.角色.坐骑数据[编号].升级经验 <= user.角色.坐骑数据[编号].当前经验 do
			user.角色.坐骑数据[编号].当前经验 = user.角色.坐骑数据[编号].当前经验 - user.角色.坐骑数据[编号].升级经验
			user.角色.坐骑数据[编号].等级 = user.角色.坐骑数据[编号].等级 + 1
			user.角色.坐骑数据[编号].升级经验 = 取升级经验(user.角色.坐骑数据[编号].等级) * 10
			for n = 1, 5 do
				user.角色.坐骑数据[编号][全局变量.基础属性[n]] = user.角色.坐骑数据[编号][全局变量.基础属性[n]] + 1
			end
			user.角色.坐骑数据[编号].潜能 = user.角色.坐骑数据[编号].潜能 + 5
			RoleControl:刷新战斗属性(user,1)
		end

		SendMessage(user.连接id, 7, "#y/驯养成功")
		SendMessage(user.连接id, 7, "#y/你的体力减少了10点")
		SendMessage(user.连接id, 7, "#y/你的人物经验减少了200万")
		SendMessage(user.连接id, 7, "#y/你的这只坐骑获得了150万经验")
		SendMessage(user.连接id, 20032, self:取坐骑数据(user))

	end
end
function RoleControl:坐骑加点处理(user, 编号, 数据)
  数据= 分割文本(数据,"*-*")
 if user.角色.坐骑数据[编号].潜能 <= 0 then
		SendMessage(user.连接id, 7, "#y/你的这只坐骑当前没有可分配的属性点")
		return 0
  elseif 数据[1]+0 < 0 then
      封禁账号(user,"CE修改")
    return
  elseif 数据[2]+0 < 0 then
        封禁账号(user,"CE修改")
    return
  elseif 数据[3]+0 < 0 then
        封禁账号(user,"CE修改")
    return
  elseif 数据[4]+0 < 0 then
      封禁账号(user,"CE修改")
    return
  elseif 数据[5]+0 < 0 then
        封禁账号(user,"CE修改")
        return
  elseif user.角色.坐骑数据[编号].潜能<数据[1]+数据[2]+数据[3]+数据[4]+数据[5] then
   		SendMessage(user.连接id,7,"#y/你没有那么多可分配的属性点")
  else
	 user.角色.坐骑数据[编号].力量= user.角色.坐骑数据[编号].力量+数据[1]
	 user.角色.坐骑数据[编号].体质= user.角色.坐骑数据[编号].体质+数据[2]
	 user.角色.坐骑数据[编号].魔力= user.角色.坐骑数据[编号].魔力+数据[3]
	 user.角色.坐骑数据[编号].耐力= user.角色.坐骑数据[编号].耐力+数据[4]
	 user.角色.坐骑数据[编号].敏捷= user.角色.坐骑数据[编号].敏捷+数据[5]
	 user.角色.坐骑数据[编号].潜能 =  user.角色.坐骑数据[编号].潜能 - (数据[1]+数据[2]+数据[3]+数据[4]+数据[5])
   SendMessage(user.连接id,7,"#y/属性点分配成功")
  	RoleControl:刷新战斗属性(user,1)
 	SendMessage(user.连接id, 20032, self:取坐骑数据(user))
 
	end
end
function RoleControl:坐骑放生(user,编号)
    if user.角色.坐骑.编号~=0 then
    	SendMessage(user.连接id,7,"#y/请下坐骑在执行放生操作")
    	return
    else
	table.remove(user.角色.坐骑数据,编号)
	SendMessage(user.连接id,7,"#y/坐骑放生成功")
	SendMessage(user.连接id, 20032, self:取坐骑数据(user))
	end
end
function RoleControl:坐骑染色(user,染色1,染色2,染色3)
	if user.角色.坐骑.编号 == 0 then
	   SendMessage(user.连接id,7,"#y/请将需要染色的坐骑选中骑乘状态")
	elseif user.角色.坐骑数据[user.角色.坐骑.编号] ==nil then
		SendMessage(user.连接id,7,"#y/坐骑数据错误")
	else
     if (染色1>0 or 染色2>0 or 染色3>0 ) and user.角色.坐骑数据[user.角色.坐骑.编号].染色组==nil then
         user.角色.坐骑数据[user.角色.坐骑.编号].染色组={0,0,0}
     end
     local xh= 0
	if   染色1 ~= user.角色.坐骑数据[user.角色.坐骑.编号].染色组[1] then
		xh =xh +10
	end
	if 染色2 ~= user.角色.坐骑数据[user.角色.坐骑.编号].染色组[2] then
		xh =xh +10
	end
	if 染色3 ~= user.角色.坐骑数据[user.角色.坐骑.编号].染色组[3] then
		xh =xh +10
	end
	self.彩果计算 = {}
	self.彩果满足 = false
	 if xh>0 then
	   for n=1,80 do
	     if xh>0 then
	       if xh>0 and  user.角色.道具.包裹[n]~=nil and user.物品[user.角色.道具.包裹[n]].名称=="颜灵果" then
	         if user.物品[user.角色.道具.包裹[n]].数量==nil then
	            xh=xh-1
	            self.彩果计算[#self.彩果计算+1]={格子=n,数量=1}
	          elseif user.物品[user.角色.道具.包裹[n]].数量>=xh then
	            self.彩果计算[#self.彩果计算+1]={格子=n,数量=xh}
	            xh=0
	          else
	           self.彩果计算[#self.彩果计算+1]={格子=n,数量=user.物品[user.角色.道具.包裹[n]].数量}
	           xh=xh-user.物品[user.角色.道具.包裹[n]].数量
	           end
	           if xh==0 then
	            self.彩果满足=true
	           end
	         end
	       end
	     end
	   end

   if xh>0  then
    SendMessage(user.连接id,7,"#Y/您还需要#R/"..xh.."#Y/个颜灵果才能染色")
   		return 0
   else
    if #self.彩果计算>0 then
      for n=1,#self.彩果计算 do
       self.临时格子=self.彩果计算[n].格子
       self.临时id=user.角色.道具.包裹[self.临时格子]
       if user.物品[self.临时id].数量==nil then
          user.物品[self.临时id]=0
          user.角色.道具.包裹[self.临时格子]=nil
        else
          user.物品[self.临时id].数量=user.物品[self.临时id].数量-self.彩果计算[n].数量
          if user.物品[self.临时id].数量<1 then
           user.物品[self.临时id]=0
           user.角色.道具.包裹[self.临时格子]=nil
            end
         end
        end
      end
   end
        user.角色.坐骑数据[user.角色.坐骑.编号].染色组={染色1,染色2,染色3}
        user.角色.坐骑.染色组 =  table.copy(user.角色.坐骑数据[user.角色.坐骑.编号].染色组)
        SendMessage(user.连接id,7,"#y/坐骑染色成功")
        SendMessage(user.连接id, 2010, self:获取地图数据(user))
		MapControl:MapSend(user.id, 1016, self:获取地图数据(user), user.地图)
		SendMessage(user.连接id, 20049,user.角色.坐骑数据[user.角色.坐骑.编号])
	end
end
function RoleControl:坐骑饰品染色(user,染色1,染色2,染色3)
	if user.角色.坐骑.编号 == 0 then
	   SendMessage(user.连接id,7,"#y/请将需要染色的坐骑选中骑乘状态")
	elseif user.角色.坐骑数据[user.角色.坐骑.编号] ==nil then
		SendMessage(user.连接id,7,"#y/坐骑数据错误")
	elseif user.角色.坐骑数据[user.角色.坐骑.编号].饰品 ==nil then
		SendMessage(user.连接id,7,"#y/坐骑数据错误")
	elseif user.物品[user.角色.坐骑数据[user.角色.坐骑.编号].装备编号] ==nil then
		SendMessage(user.连接id,7,"#y/坐骑数据错误")
	else
	    local 道具编号 = user.角色.坐骑数据[user.角色.坐骑.编号].装备编号
		if (染色1>0 or 染色2>0 or 染色3>0) and user.物品[道具编号].染色组 ==nil then
		 user.物品[道具编号].染色组={0,0,0}
		end
		local xh= 0
		if   染色1 ~= user.物品[道具编号].染色组[1] then
		xh =xh +10
		end
		if 染色2 ~= user.物品[道具编号].染色组[2] then
		xh =xh +10
		end
		if 染色3 ~= user.物品[道具编号].染色组[3] then
		xh =xh +10
		end
		self.彩果计算 = {}
		self.彩果满足 = false
		if xh>0 then
			for n=1,20 do
				if xh>0 then
					if xh>0 and  user.角色.道具.包裹[n]~=nil and user.物品[user.角色.道具.包裹[n]].名称=="颜灵果" then
						if user.物品[user.角色.道具.包裹[n]].数量==nil then
						xh=xh-1
						self.彩果计算[#self.彩果计算+1]={格子=n,数量=1}
						elseif user.物品[user.角色.道具.包裹[n]].数量>=xh then
						self.彩果计算[#self.彩果计算+1]={格子=n,数量=xh}
						xh=0
						else
						self.彩果计算[#self.彩果计算+1]={格子=n,数量=user.物品[user.角色.道具.包裹[n]].数量}
						xh=xh-user.物品[user.角色.道具.包裹[n]].数量
						end
						if xh==0 then
						self.彩果满足=true
						end
					end
				end
			end
		end
	   if xh>0  then
	    SendMessage(user.连接id,7,"#Y/您还需要#R/"..xh.."#Y/个颜灵果才能染色")
	   		return 0
	   else
		    if #self.彩果计算>0 then
		      for n=1,#self.彩果计算 do
			       self.临时格子=self.彩果计算[n].格子
			       self.临时id=user.角色.道具.包裹[self.临时格子]
					if user.物品[self.临时id].数量==nil then
						user.物品[self.临时id]=0
						user.角色.道具.包裹[self.临时格子]=nil
					else
						user.物品[self.临时id].数量=user.物品[self.临时id].数量-self.彩果计算[n].数量
						if user.物品[self.临时id].数量<1 then
						user.物品[self.临时id]=0
						user.角色.道具.包裹[self.临时格子]=nil
						end
					end
		       end
		     end
	   end
        user.物品[道具编号].染色组={染色1,染色2,染色3}
        user.角色.坐骑数据[user.角色.坐骑.编号].装备=  user.物品[道具编号]
        user.角色.坐骑.饰品染色组 =  table.copy(user.物品[道具编号].染色组)
        SendMessage(user.连接id,7,"#y/坐骑饰品染色成功")
        SendMessage(user.连接id, 2010, self:获取地图数据(user))
		MapControl:MapSend(user.id, 1016, self:获取地图数据(user), user.地图)
		SendMessage(user.连接id, 20050,user.角色.坐骑数据[user.角色.坐骑.编号])
	end
end
function RoleControl:坐骑改名(user,编号,名称)
	if   名称 and 名称~= "" then
	   	user.角色.坐骑数据[编号].名称 = 名称
	SendMessage(user.连接id,7,"#y/坐骑改名成功")
	SendMessage(user.连接id, 20032, self:取坐骑数据(user))
	end

end