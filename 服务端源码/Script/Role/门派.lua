-- @Author: 作者QQ414628710
-- @Date:   2022-01-15 20:06:48
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-23 04:03:34
local 神器=
{
大唐官府={"轩辕剑","藏锋敛锐","惊锋"},
化生寺={"墨魂笔","风起云墨","挥毫"},
女儿村={"泪痕碗","盏中晴雪","泪光盈盈"},
方寸山={"黄金甲","披坚执锐","金汤之固"},
天宫={"独弦琴","弦外之音","裂帛"},
普陀山={"华光玉","璇华","玉魄"},
龙宫={"清泽谱","沧浪赋","定风波"},
五庄观={"星斗盘","斗转参横","静笃"},
狮驼岭={"噬魂齿","蛮血","狂战"},
魔王寨={"明火珠","业焰明光","流火"},
阴曹地府={"四神鼎","亡灵泣语","魂魇"},
盘丝洞={"昆仑镜","镜花水月","澄明"},
神木林={"月光草","凭虚御风","钟灵"},
凌波城={"天罡印","威服天下","酣战"},
无底洞={ "玲珑结","情思悠悠","相思"},
女魃墓={ "莫愁铃","惊梦","泪雨零铃"},
天机城={ "千机锁","迭锁","千机巧变"},
花果山={ "鸿蒙石","万物滋长","开辟"},

}
local floor = math.floor
function RoleControl:脱离门派(user)----------------------------------------------------------
  if user.角色.道具.货币.银子 > Config.退出门派  then
      if user.角色.法宝[35]~=0 or user.角色.法宝[36]~=0 or user.角色.法宝[37]~=0 or user.角色.法宝[38]~=0 then
         SendMessage(user.连接id, 7, "#y/请将所有法宝脱下在确定退出门派!")
      else
        RoleControl:扣除银子(user,Config.退出门派, "脱离门派")
        user.角色.门派 = "无"

        user.角色.保留技能等级={}
        for i=1,# user.角色.师门技能 do
          user.角色.保留技能等级[i]= user.角色.师门技能[i].等级
        end
        user.角色.师门技能 = {}
        user.角色.默认法术=nil
        user.角色.人物技能 = {}
        user.角色.乾元丹技能 = {}
        user.角色.技能属性={气血=0,躲避=0,力量=0,魔力=0,耐力=0,体质=0,命中=0,法术防御=0,魔法=0,灵力=0,速度=0,治疗能力=0,穿刺等级=0,防御=0,敏捷=0,伤害=0}
        user.角色.快捷技能={}
        local x1,x2,x3,x4=user.角色.奇经八脉.乾元丹,user.角色.奇经八脉.乾元丹,user.角色.奇经八脉.可换乾元丹,user.角色.奇经八脉.附加乾元丹
        user.角色.奇经八脉={剩余乾元丹=x1,乾元丹=x2,可换乾元丹=x3,附加乾元丹=x4,技能树={1,2,3}}
      
       if user.角色.神器 then
          user.角色.领取神器 =true
         user.角色.神器 =nil 
         
       end
        
        SendMessage(user.连接id, 7, "#y/你现在已经变成无门派人士了")
        SendMessage(user.连接id, 7, "#y/你的门派技能已经被清空了")
        SendMessage(user.连接id, 9957, "1")
        for n=1,#user.角色.称谓 do
          if user.角色.称谓[n]==user.角色.门派.."首席弟子"  then
            if user.角色.称谓.当前==user.角色.门派.."首席弟子" then
              user.角色.称谓.当前=""
            end
            self:删除称谓(user,user.角色.门派.."首席弟子",1)
            SendMessage(user数据[self.userid].连接id,7,"#y/你已经不是首席弟子了,称谓被删除")
          end
        end
       RoleControl:刷新战斗属性(user,1)
      end
    else
      SendMessage(user.连接id, 7, "#y/你没那么多的银子")
    end
 end
function RoleControl:加入门派(user,类型,名称)

	if user.角色.门派~="无" then
		SendMessage(user.连接id,7,"#Y/您已经加入过门派了，无法再加入另一个门派")
		return 0
	elseif user.角色.等级<10 then
		SendMessage(user.连接id,7,"#Y/等级达到10级后才可进入门派")
		return 0
	else
        user.角色.人物技能 = {};
		user.角色.门派=类型
		local 列表 = 取门派技能(user.角色.门派)
		for n=1,#列表 do
			user.角色.师门技能[n]=置技能(列表[n])
			if user.角色.保留技能等级 then 
				user.角色.师门技能[n].等级=user.角色.保留技能等级[n]
			else 
				user.角色.师门技能[n].等级 =  DebugMode  and 175 or 1
			end
			local w = 取包含技能(user.角色.师门技能[n].名称)
			user.角色.师门技能[n].包含技能 = {}
			for s=1,#w do
				if w[s] ~= nil then
					user.角色.师门技能[n].包含技能[s]=置技能(w[s])
					user.角色.师门技能[n].包含技能[s].等级 = user.角色.师门技能[n].等级
				end
			end
			self:升级技能(user,user.角色.师门技能[n])
		end
		user.角色.保留技能等级=nil
		if user.角色.领取神器 then
			user.角色.领取神器=nil
			RoleControl:添加神器(user)
		end
		self:刷新战斗属性(user)
		SendMessage(user.连接id,7,"#Y/您已经成为#G/"..类型.."#Y/弟子")
		广播门派消息(user.角色.门派, "#" .. 门派代号[user.角色.门派] .. "/#g/" .. user.角色.名称 .. "#w/仰慕本门派已久，今日终于拜入本门派。请各位弟子对其多多支持。")
		SendMessage(user.连接id,20,{名称,名称,"师傅领进门，修行靠个人。该怎么样，还是得看你自己。"})
    end
end


function RoleControl:兑换潜能果(user)
  if user.角色.潜能果.潜能果 > 200 then
        SendMessage(user.连接id, 7, "#y/你已经无法在获得潜能果了!")
     return
  elseif user.角色.潜能果.潜能果 >= user.角色.潜能果.可换潜能果 then
      SendMessage(user.连接id, 7, "#y/当前等级无法在获取新的潜能果!")
       return
  end

   local js =100000000+user.角色.潜能果.潜能果*15000000
  if user.角色.当前经验<js then
         SendMessage(user.连接id, 7, "#y/当前经验不足无法进行兑换!")
        return
  else
    self:扣除经验(user,js, "学习潜能果")
   user.角色.潜能果.潜能果=user.角色.潜能果.潜能果+1
   user.角色.潜能=user.角色.潜能+1
  SendMessage(user.连接id, 7, "#y/兑换成功,贡献你获得一颗潜能果!")
  SendMessage(user.连接id, 2017,{潜能果=user.角色.潜能果,当前经验=user.角色.当前经验,状态="潜能果"})
  end
 end

function RoleControl:获取神器数据(user)
  if user.角色.神器 then 
    SendMessage(user.连接id,3000,{类型 ="神器",数据 =user.角色.神器})
  else 
     SendMessage(user.连接id, 7, "#y/当前还未获得神器,请先去袁天罡处领取任务")
  end
end

function RoleControl:添加神器(user)
   if  user.角色.门派  =="无" then
    SendMessage(user.连接id, 7, "#y/当前未加入门派，无法获得门派神器")
    return
  end
 

  user.角色.神器={
  名称 =神器[user.角色.门派][1],
  技能=神器[user.角色.门派][math.random(2,3)],
  灵气 = 100,
  属性={},
   }
   
local 主属性 ={"命中","气血","法防","灵力","魔法","速度","防御","伤害"}
local 副属性 ={"法术伤害" ,"法术防御" ,"治疗能力" ,"气血回复效果" ,"固定伤害" ,"法术伤害结果" ,"格挡值" ,"狂暴等级" ,"穿刺等级" ,"抗物理暴击等级" ,"法术暴击等级" ,"封印命中等级" ,"物理暴击等级" ,"抗法术暴击等级" ,"抵抗封印等级"}

 for i=1,5 do
     local 临时属性,临时数值
      if  math.random(100) > 50 then
      临时属性=主属性[math.random(1,#主属性)]
      临时数值=math.random(0,50)
      else
      临时属性=副属性[math.random(1,#副属性)]
      临时数值=math.random(0,25)
      end
      user.角色.神器.属性[i]={类型=临时属性,数额=临时数值}
 end
     RoleControl:刷新装备属性(user)
     SendMessage(user.连接id, 7, "#y/恭喜你获得了"..user.角色.门派.."门派神器#G/"..user.角色.神器.名称)
 
end





function RoleControl:神器洗炼属性(user)
     if not user.角色.神器 then
       return 

    end
    if 银子检查(user.id,10000000) then
      RoleControl:扣除银子(user,10000000, "神器洗炼属性")
      local 主属性 ={"命中","气血","法防","灵力","魔法","速度","防御","伤害"}
      local 副属性 ={"法术伤害" ,"法术防御" ,"治疗能力" ,"气血回复效果" ,"固定伤害" ,"法术伤害结果" ,"格挡值" ,"狂暴等级" ,"穿刺等级" ,"抗物理暴击等级" ,"法术暴击等级" ,"封印命中等级" ,"物理暴击等级" ,"抗法术暴击等级" ,"抵抗封印等级"}
       for i=1,5 do
           local 临时属性,临时数值
            if  math.random(100) > 50 then
            临时属性=主属性[math.random(1,#主属性)]
            临时数值=math.random(0,50)
            else
            临时属性=副属性[math.random(1,#副属性)]
            临时数值=math.random(0,25)
            end
            user.角色.神器.属性[i]={类型=临时属性,数额=临时数值}
       end
          SendMessage(user.连接id,7,"#Y/你的神器获得了新的属性")
         RoleControl:刷新装备属性(user)
         self:获取神器数据(user)
     else 
      SendMessage(user.连接id,7,"#Y/你当前的银子不足")
     end
end
function RoleControl:神器洗炼技能(user)
   if not user.角色.神器 then
       return 
   end
    if  self:扣除仙玉(user,10000,"神器灵气") then
         user.角色.神器.技能=神器[user.角色.门派][math.random(2,3)]
         RoleControl:刷新装备属性(user)
         SendMessage(user.连接id,7,"#Y/恭喜你洗出了#G/"..user.角色.神器.技能)
    else
         SendMessage(user.连接id,7,"#Y/你当前的仙玉不足10000")
        return 
    end
    self:获取神器数据(user)
end
function RoleControl:神器补充灵气(user)
   if not user.角色.神器 then
       return 
   end
   if 银子检查(user.id,10000000) then
      RoleControl:扣除银子(user,10000000, "神器洗炼属性")
      user.角色.神器.灵气 =user.角色.神器.灵气+200
      SendMessage(user.连接id,7,"#Y/你的神器增加了200点灵气")
      self:获取神器数据(user)
   else 
      SendMessage(user.连接id,7,"#Y/你当前的银子不足")
   end

   
end
