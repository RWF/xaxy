--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-12-06 08:08:57
--======================================================================--
function RoleControl:删除称谓(user, 称谓, 系统)
  if 称谓 == user.角色.门派 .. "首席弟子" and 系统 == nil and user ~= nil then
    SendMessage(user.连接id, 7, "#y/该称谓不允许被删除")
    return 0
  end
  local num = 0
  for n = 1, #user.角色.称谓 do
    if user.角色.称谓[n] == 称谓 then
      num = n
    end
  end
  if num == 0 then
    if user ~= nil then
      SendMessage(user.连接id, 7, "#y/你没有这样的称谓")
    end
  else
    table.remove(user.角色.称谓, num)

    if 称谓 == user.角色.称谓.当前 then
      user.角色.称谓.当前 = ""
      if user ~= nil then
        SendMessage(user.连接id, 2012, {称谓=user.角色.称谓.当前,effects=user.角色.称谓.effects})
        MapControl:更改称谓(user.地图, user, user.角色.称谓.当前)
      end
    end
    if user ~= nil then
      SendMessage(user.连接id, 7, "#y/删除称谓成功")
      SendMessage(user.连接id, 2018, user.角色.称谓)
    end
  end
 end
function RoleControl:隐藏称谓(user, 称谓)
  if user.角色.称谓.当前 == 称谓 then
    user.角色.称谓.当前 = ""
    SendMessage(user.连接id, 7, "#y/隐藏称谓成功")
    SendMessage(user.连接id, 2018, user.角色.称谓)
    SendMessage(user.连接id, 2012, {称谓=user.角色.称谓.当前,effects=user.角色.称谓.effects})
    MapControl:更改称谓(user.地图, user, user.角色.称谓.当前)
  else
    SendMessage(user.连接id, 7, "#y/此称谓都没有显示出来怎么隐藏？")
  end
 end
function RoleControl:取指定称谓(user,称谓)
  for n=1,#user.角色.称谓 do
    if user.角色.称谓[n]==称谓 then
     return true
      end
   end
 return false
 end
function RoleControl:buyEffects(user, Effects)
   if Effects and Effects ~="" and LabelData[Effects]  then
    for i=1,#user.角色.称谓特效 do
       if user.角色.称谓特效[i]== Effects then
           SendMessage(user.连接id, 7, "#y/你已经购买过这个特效了")
         return  
       end
    end
       if RoleControl:扣除仙玉(user,LabelData[Effects].无文字,"购买称谓特效"..Effects) then
         table.insert(user.角色.称谓特效, Effects)
         SendMessage(user.连接id, 7, "#y/购买#G/"..Effects.."#Y/特效成功！")
          SendMessage(user.连接id, 2035, {user.角色.称谓,user.仙玉,user.角色.称谓特效})
       else 
          SendMessage(user.连接id, 7, "#y/你的仙玉不足#G/"..LabelData[Effects].无文字.."#Y/点无法购买")
       end
          
   end
end

function RoleControl:改变称谓(user, 称谓,effects)
  local num = 0
  for n = 1, #user.角色.称谓 do
    if user.角色.称谓[n] == 称谓 then
      num = n
    end
  end
  if num == 0 then
    SendMessage(user.连接id, 7, "#y/你没有这样的称谓")
  else
    user.角色.称谓.当前 = 称谓
    if effects == "不显示特效" then
    user.角色.称谓.effects=nil
    else  
      user.角色.称谓.effects=effects
    end

    SendMessage(user.连接id, 7, "#y/更改称谓成功")
    SendMessage(user.连接id, 2018, user.角色.称谓)
    SendMessage(user.连接id, 2012, {称谓=user.角色.称谓.当前,effects=user.角色.称谓.effects})
   

   
    MapControl:更改称谓(user.地图,user, user.角色.称谓.当前)
  end
 end
function RoleControl:添加称谓(user, 称谓)
  if user==nil then
     return
  end
  for n = 1, #user.角色.称谓 do
    if user.角色.称谓[n] == 称谓 then
      SendMessage(user.连接id, 7, "#y/此称谓你已经拥有了")
      return 0
    end
  end
  if #user.角色.称谓 >= 7 then
    SendMessage(user.连接id, 7, "#y/角色拥有的称谓数量不能超过7个")
    return 0
  end
  user.角色.称谓[#user.角色.称谓 + 1] = 称谓
   user.角色.称谓.当前 = 称谓
  SendMessage(user.连接id, 7, "#y/你获得了新称谓#r/" .. 称谓)
 end
function RoleControl:回收称谓(user,内容)
  if user ==nil then
    return
  end
  for i=#user.角色.称谓,1,-1 do
    if user.角色.称谓[i] == 内容 then
       table.remove(user.角色.称谓,i)
    end
  end
   if 内容 == user.角色.称谓.当前 then
    user.角色.称谓.当前 = ""
    if user ~= nil then
        SendMessage(user.连接id, 2012, {称谓=user.角色.称谓.当前,effects=user.角色.称谓.effects})

        MapControl:更改称谓(user.地图, user, user.角色.称谓.当前)
    end
  end
 end
function RoleControl:检查称谓(user,内容)
  for i=1,#user.角色.称谓 do
    if user.角色.称谓[i] == 内容 then
       return true
    end
  end
   return false
 end
