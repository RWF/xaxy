local GetChildrenModel=function()

    local ChildrenModel={"小魔头","小精灵","小仙女","小神灵","小毛头","小丫丫"}

  return ChildrenModel[math.random(#ChildrenModel)]
end
local function ChildrenUpdateProperty (Children,recover)

 if Children.饰品 then
    Children.特殊资质={
          攻资=math.floor(Children.攻资*0.1),
          防资=math.floor(Children.防资*0.1),
          体资=math.floor(Children.体资*0.1),
          法资=math.floor(Children.法资*0.1),
          速资=math.floor(Children.速资*0.1),
          躲资=math.floor(Children.躲资*0.1)}
 end
 Children.伤害=math.floor(Children.等级*(Children.攻资+Children.特殊资质.攻资)*(14+10*Children.成长)/7500+Children.成长*Children.力量+20)+Children.装备属性.伤害
 Children.灵力=math.floor(Children.等级*(Children.法资+Children.特殊资质.法资+1640)*(Children.成长+1)/7500+0.7*Children.魔力+20+Children.力量*0.4+Children.耐力*0.2+Children.体质*0.3)+Children.装备属性.灵力
 Children.防御=math.floor(Children.等级*(Children.防资+Children.特殊资质.防资)/433+Children.成长*Children.耐力*4/3)+Children.装备属性.防御
 Children.速度=math.floor(Children.敏捷*Children.速资/1000)+Children.装备属性.速度
 Children.躲闪=math.floor(Children.敏捷*Children.躲资/1000)+Children.装备属性.躲闪
 Children.气血上限=math.floor(Children.等级*(Children.体资+Children.特殊资质.体资)/1000+Children.成长*Children.体质*10)+10+Children.装备属性.气血
 Children.最大气血=math.floor(Children.等级*(Children.体资+Children.特殊资质.体资)/1000+Children.成长*Children.体质*10)+10+Children.装备属性.气血
 Children.魔法上限=math.floor(Children.等级*(Children.法资+Children.特殊资质.法资)/2000+Children.成长*Children.魔力*2.5)+Children.装备属性.魔法


  for i=1,#Children.技能 do
      if Children.技能[i].名称=="千锤百炼" then
        Children.最大气血= math.min(Children.等级*5,540)+Children.最大气血
         Children.气血上限 =Children.最大气血
      end
      
  end
     if recover or  not Children.当前气血 or not Children.当前魔法  then
       Children.当前气血=Children.气血上限
       Children.当前魔法=Children.魔法上限
     end



 end
function RoleControl:DeleteChildren(user,number)
    if user.角色.子女列表[number] then 

        table.remove(user.角色.子女列表, number)
        user.角色.子女列表.参战 =0
        self:GetChildrenData(user)
        SendMessage(user.连接id,7,"#Y/孩子抛弃成功")
    else 
        SendMessage(user.连接id,7,"#Y/你貌似没有这个孩子")
    end 
end
function RoleControl:Children设置参战(user,number)
    if user.角色.子女列表[number] then 

         user.角色.子女列表.参战 =number
        self:GetChildrenData(user)
        SendMessage(user.连接id,7,string.format("#Y/%s设置为参战状态,战斗中使用逍遥镜就可以召唤出孩子" ,user.角色.子女列表[number].名称))
    else 
        SendMessage(user.连接id,7,"#Y/你貌似没有这个孩子")
    end 
end
function RoleControl:Children设置成长之路(user,number,text)
    text=string.split(text, "*")
    local sum =0
    for i=1,#text do
        text[i]=assert((tonumber(text[i])), "孩子成长之路设置数值不是数字")
        sum = sum+ text[i]
    end
    if sum~=5 then
         SendMessage(user.连接id,7,"#Y/设置的总数必须等于5")
    elseif user.角色.子女列表[number] then 
        for i,v in ipairs{"体质","魔力","力量","耐力","敏捷"} do
           user.角色.子女列表[number].成长之路[v]=text[i]
        end
       SendMessage(user.连接id,7,string.format("#Y/%s成长之路属性设置成功",user.角色.子女列表[number].名称))
    else 
        SendMessage(user.连接id,7,"#Y/你貌似没有这个孩子")
    end 
end

function RoleControl:GetChildrenHP(user)

  if user.角色.子女列表.参战 == 0 then
    return 0
  elseif user.角色.子女列表[user.角色.子女列表.参战] == nil then
    return 0
  end
  self.发送信息 = {
    造型 = user.角色.子女列表[user.角色.子女列表.参战].造型,
    当前气血 = user.角色.子女列表[user.角色.子女列表.参战].当前气血,
    气血上限 = user.角色.子女列表[user.角色.子女列表.参战].气血上限,
    当前魔法 = user.角色.子女列表[user.角色.子女列表.参战].当前魔法,
    魔法上限 = user.角色.子女列表[user.角色.子女列表.参战].魔法上限,
    当前经验 = user.角色.子女列表[user.角色.子女列表.参战].当前经验,
    升级经验 = user.角色.子女列表[user.角色.子女列表.参战].升级经验
  }
  return self.发送信息
end
function RoleControl:NewChildren(user)

    local  Children={
    等级 = 0,
    潜能=0,
    体质 = 20,
    魔力 = 20,
    力量 = 20,
    耐力 = 20,
    敏捷 = 20,
    类型 = "孩子",
    寿命 = 10000,
    参战等级 = 0,
    忠诚 = 100,
    攻资= math.random(860, 1280),
    防资= math.random(860, 1280),
    体资= math.random(2000, 3000),
    法资= math.random(1500, 2000),
    速资= math.random(860, 1280),
    躲资= math.random(860, 1280),
    成长 = math.random(75, 120)/100,
    装备属性={速度=0,伤害=0,防御=0,气血=0,魔法=0,灵力=0,体质=0,力量=0,魔力=0,耐力=0,敏捷=0,躲闪=0},
    特殊资质={攻资=0,防资=0,体资=0,法资=0,速资=0,躲资=0},
    五行=生成五行(),
    当前经验=0,
    升级经验=15,
    装备={},
    成长之路={体质=0,力量=5,魔力=0,耐力=0,敏捷=0},
    附加追加技能 = {},
    追加技能 ={},
    技能={},
    饰品=false,
    元宵=0,
    炼兽真经=0,
    道具数据={}
     }
    Children.造型=GetChildrenModel()
    Children.名称=Children.造型
    ChildrenUpdateProperty(Children)
    table.insert(user.角色.子女列表,Children)
    SendMessage(user.连接id,7,string.format("#Y/恭喜你领养了一个%s",Children.名称))
end

function RoleControl:GetChildrenData(user)
    SendMessage(user.连接id, 20033, user.角色.子女列表)

end
function RoleControl:Children重置属性点(user,number)
  
  user.角色.子女列表[number].力量 = user.角色.子女列表[number].等级 + 20
  user.角色.子女列表[number].魔力 = user.角色.子女列表[number].等级 + 20
  user.角色.子女列表[number].体质 = user.角色.子女列表[number].等级 + 20
  user.角色.子女列表[number].敏捷 = user.角色.子女列表[number].等级 + 20
  user.角色.子女列表[number].耐力 = user.角色.子女列表[number].等级 + 20
    for k,v in pairs(user.角色.子女列表[number].成长之路) do
          if v > 0 then 
            user.角色.子女列表[number][k] =user.角色.子女列表[number][k]+v*user.角色.子女列表[number].等级
          end  
    end
   ChildrenUpdateProperty (user.角色.子女列表[number],true)  
   self:GetChildrenData(user)
end
function RoleControl:ChildrenWornEquip(user,格子,num,text)
  

  if not  user.角色.子女列表[num] then
        SendMessage(user.连接id,7,"#y/您的子女貌似不存在")
  return 0
  elseif text==nil or  text=="" then
     return
  end
  if text=="脱" then
     self.可用格子=RoleControl:取可用道具格子(user,"包裹")
     if self.可用格子==0 then
       SendMessage(user.连接id,7,"#y/你已经无法携带更多的道具了")
       return 0
     end
     user.角色.道具["包裹"][self.可用格子]=user.角色.子女列表[num].装备[格子]
     user.角色.子女列表[num].装备[格子]=nil
     SendMessage(user.连接id, 3007,{格子=self.可用格子,数据=user.物品[user.角色.道具["包裹"][self.可用格子]]})
  else 
     local 临时id=user.角色.道具["包裹"][格子]
      if user.物品[临时id]==nil or user.物品[临时id]==0 then
        SendMessage(user.连接id,7,"#y/你似乎并没有这样的道具")
        return 0
      elseif  user.角色.子女列表[num].等级<user.物品[临时id].等级 then
        SendMessage(user.连接id,7,"#y/您的子女当前等级无法佩戴此装备")
        return 0
      end
         if user.物品[临时id].种类=="项圈" then
        self.格子类型=2
      elseif user.物品[临时id].种类=="铠甲" then
        self.格子类型=3
      else
        self.格子类型=1
      end
      self.已佩戴id=nil
      if user.角色.子女列表[num].装备[self.格子类型]~=nil then
      self.已佩戴id=user.角色.子女列表[num].装备[self.格子类型]
      end
      user.角色.子女列表[num].装备[self.格子类型]=临时id
      user.角色.道具["包裹"][格子]=self.已佩戴id
      SendMessage(user.连接id,7,"#y/子女佩戴装备成功")
  end
  

  self:ChildrenUpdateEquip(user,num)     
   SendMessage(user.连接id, 3007,{格子=格子,数据=user.物品[user.角色.道具.包裹[格子]]})
   self:GetChildrenData(user)
end
function RoleControl:ChildrenUpdateEquip(user,num)

  user.角色.子女列表[num].追加技能 = {}
  user.角色.子女列表[num].附加追加技能 = {}
  self.临时追加 = {}
  self.临时附加 = {}
  for n = 1, #全局变量.基础属性 do
    user.角色.子女列表[num][全局变量.基础属性[n]] = user.角色.子女列表[num][全局变量.基础属性[n]] - user.角色.子女列表[num].装备属性[全局变量.基础属性[n]]
  end
  user.角色.子女列表[num].装备属性 = {速度 = 0,体质 = 0,魔力 = 0,气血 = 0,敏捷 = 0,耐力 = 0,魔法 = 0,灵力 = 0,伤害 = 0,力量 = 0,防御 = 0,躲闪 = 0}
  user.角色.子女列表[num].道具数据 = {}
  for n = 1, 3 do
    if user.角色.子女列表[num].装备[n] ~= nil then
      user.角色.子女列表[num].道具数据[n] = user.物品[user.角色.子女列表[num].装备[n]]
      if user.角色.子女列表[num].道具数据[n] and user.角色.子女列表[num].道具数据[n].套装效果 then
        self.套装名称 = user.角色.子女列表[num].道具数据[n].套装效果.名称
        if user.角色.子女列表[num].道具数据[n].套装效果.类型 == "追加法术" then
           self.临时追加[self.套装名称] =self.临时追加[self.套装名称]and self.临时追加[self.套装名称] + 1 or 1
        else
            self.临时附加[self.套装名称] =self.临时附加[self.套装名称]and self.临时附加[self.套装名称] + 1 or 1
        end

      end

      user.角色.子女列表[num].装备属性[user.物品[user.角色.子女列表[num].装备[n]].主属性.名称] = user.角色.子女列表[num].装备属性[user.物品[user.角色.子女列表[num].装备[n]].主属性.名称] + user.物品[user.角色.子女列表[num].装备[n]].主属性.数值
        for i = 1, #user.物品[user.角色.子女列表[num].装备[n]].锻造数据 do
        user.角色.子女列表[num].装备属性[user.物品[user.角色.子女列表[num].装备[n]].锻造数据[i].类型] = user.角色.子女列表[num].装备属性[user.物品[user.角色.子女列表[num].装备[n]].锻造数据[i].类型] + user.物品[user.角色.子女列表[num].装备[n]].锻造数据[i].数值
      end
      if user.物品[user.角色.子女列表[num].装备[n]].附加属性 ~= nil then
        user.角色.子女列表[num].装备属性[user.物品[user.角色.子女列表[num].装备[n]].附加属性.名称] = user.角色.子女列表[num].装备属性[user.物品[user.角色.子女列表[num].装备[n]].附加属性.名称] + user.物品[user.角色.子女列表[num].装备[n]].附加属性.数值
      end

      if user.物品[user.角色.子女列表[num].装备[n]].单加 ~= nil then
        user.角色.子女列表[num].装备属性[user.物品[user.角色.子女列表[num].装备[n]].单加.名称] = user.角色.子女列表[num].装备属性[user.物品[user.角色.子女列表[num].装备[n]].单加.名称] + user.物品[user.角色.子女列表[num].装备[n]].单加.数值
      end

      if user.物品[user.角色.子女列表[num].装备[n]].双加 ~= nil then
        user.角色.子女列表[num].装备属性[user.物品[user.角色.子女列表[num].装备[n]].双加.名称] = user.角色.子女列表[num].装备属性[user.物品[user.角色.子女列表[num].装备[n]].双加.名称] + user.物品[user.角色.子女列表[num].装备[n]].双加.数值
      end
    else
      user.角色.子女列表[num].道具数据[n] = nil
    end
  end

  for n, v in pairs(self.临时追加) do
    if self.临时追加[n] >= 3 then
      user.角色.子女列表[num].追加技能[#user.角色.子女列表[num].追加技能+1] = n
    end
  end
  for n, v in pairs(self.临时附加) do
    if self.临时附加[n] >= 3 then
      user.角色.子女列表[num].附加追加技能[#user.角色.子女列表[num].附加追加技能+1] = {名称=n}

    end
  end
  for n = 1, #全局变量.基础属性 do
    user.角色.子女列表[num][全局变量.基础属性[n]] = user.角色.子女列表[num][全局变量.基础属性[n]] + user.角色.子女列表[num].装备属性[全局变量.基础属性[n]]
  end
   ChildrenUpdateProperty (user.角色.子女列表[num],true)  

end


function RoleControl:ChildrenAddExperience(user,number,amount)
  if  user.角色.子女列表[number].等级 >= user.角色.等级 + 5    then
    SendMessage(user.连接id, 9, "#xt/#w/你的子女等级已经高出人物5级，无法再获得经验")
    return 
  else
     user.角色.子女列表[number].当前经验 =  user.角色.子女列表[number].当前经验 + amount
    SendMessage(user.连接id, 9, "#xt/#w/ " ..  user.角色.子女列表[number].名称 .. "获得" .. amount .. "点经验")
    while user.角色.子女列表[number].当前经验 > user.角色.子女列表[number].升级经验 do
         user.角色.子女列表[number].当前经验 =  user.角色.子女列表[number].当前经验 -  user.角色.子女列表[number].升级经验
        user.角色.子女列表[number].等级 =  user.角色.子女列表[number].等级 + 1

        for k,v in pairs(user.角色.子女列表[number].成长之路) do
              if v > 0 then 
                user.角色.子女列表[number][k] =user.角色.子女列表[number][k]+v
              end  
        end
        user.角色.子女列表[number].忠诚 = 100
        for i = 1, #全局变量.基础属性 do
         user.角色.子女列表[number][全局变量.基础属性[i]] =  user.角色.子女列表[number][全局变量.基础属性[i]] + 1
        end
        user.角色.子女列表[number].升级经验 = 取宠物经验(user.角色.子女列表[number].等级)*10
        SendMessage(user.连接id, 9, "#xt/#y/ " ..  user.角色.子女列表[number].名称 .. "等级提升至#r/" ..  user.角色.子女列表[number].等级 .. "#y/级")
    end
    
  end
    
end

function RoleControl:ChildrenUseItems(user,ItemNum,number)

    if not user or user.战斗 ~= 0 or user.摆摊 then
        return 
    end
    local ItemID = user.角色.道具.包裹[ItemNum]
    local ItemTemp = user.物品[ItemID]
    if not ItemTemp  or not  ItemData[ItemTemp.名称] then
        SendMessage(user.连接id, 7, "#y/你没有这样的道具")
        return
    elseif number==0 then
        SendMessage(user.连接id, 7, "#y/请你选中一个子女才能使用")
        return true
    elseif user.角色.子女列表[number] == nil then
        SendMessage(user.连接id, 7, "#y/这个子女貌似不存在")
        return true
    end
    if ItemTemp.名称 == "超级金柳露" or ItemTemp.名称 == "超级净瓶玉露" then
        self:ChildrenAddExperience(user,number,1000)
    elseif ItemTemp.名称 == "金柳露" or ItemTemp.名称 == "净瓶玉露" then
        self:ChildrenAddExperience(user,number,350)
    elseif ItemTemp.名称 == "皇帝内经" or ItemTemp.名称 == "蚩尤武诀" or  ItemTemp.名称 == "还魂秘术" or ItemTemp.名称 == "奇异果" or ItemTemp.名称 == "六艺修行秘笈" or ItemTemp.名称 == "魔兽要诀" or ItemTemp.名称 == "高级魔兽要诀" or ItemTemp.名称 == "特殊魔兽要诀"  then
           local 临时技能=""
            if ItemTemp.类型 == "炼妖" then
                    临时技能=ItemTemp.技能
            elseif ItemTemp.名称 == "皇帝内经" then
                  临时技能="治疗" 
            elseif ItemTemp.名称 == "蚩尤武诀" then
                    临时技能="蚩尤之搏"
            elseif  ItemTemp.名称 == "还魂秘术" then 
                    临时技能="还魂咒"
            elseif ItemTemp.名称 == "奇异果" then
                  临时技能="潜力激发"
            elseif ItemTemp.名称 == "六艺修行秘笈" then
                 local  六艺={"峰回路转","仙人指路","张弛有道","开门见山","赴汤蹈火","千锤百炼","四面埋伏"}
                临时技能=六艺[math.random(#六艺)]
            end
                     
                          local 技能格子=0
                          local 基础几率= 10-#user.角色.子女列表[number].技能
 
                          if math.random(100)<=基础几率  then
                             if #user.角色.子女列表[number].技能<10 then
                                技能格子=#user.角色.子女列表[number].技能+1
                             else
                               技能格子=math.random(1,#user.角色.子女列表[number].技能)
                             end
                          elseif #user.角色.子女列表[number].技能<1 then
                              技能格子=#user.角色.子女列表[number].技能+1
                          else
                              技能格子=math.random(1,#user.角色.子女列表[number].技能)
                          end
                          local 重复技能=0
                          for n=1,#user.角色.子女列表[number].技能 do
                           if user.角色.子女列表[number].技能[n].名称==临时技能   then
                            重复技能=n
                           end
                          end
              

                      if 重复技能==0  then
                       user.角色.子女列表[number].技能[技能格子]=置技能(临时技能)
                      end
                      SendMessage(user.连接id,7,"#y/你的这只孩子学会了新技能#r/"..临时技能)
                      user.角色.子女列表[number].默认法术=nil
    elseif ItemTemp.名称 == "圣兽丹" and (not user.角色.子女列表[number].饰品 or  not user.角色.子女列表[number].进阶 )then
        if not user.角色.子女列表[number].进阶 then
            if user.角色.子女列表[number].成长 >=1.27 then
                user.角色.子女列表[number].进阶= true
                user.角色.子女列表[number].造型= "进阶"..user.角色.子女列表[number].造型
                SendMessage(user.连接id, 7, string.format("#Y/你的%s已经成功进阶",user.角色.子女列表[number].名称))
            else
                 SendMessage(user.连接id, 7, "#y/你的子女成长不足1.27不能进阶")
                 return 
            end
        else 
              user.角色.子女列表[number].饰品= true
        end 
    elseif ItemTemp.名称 == "红木短剑" or ItemTemp.名称 == "竹马" or ItemTemp.名称 == "玉灵果" or ItemTemp.名称 == "山海经" or ItemTemp.名称 == "搜神记" or ItemTemp.名称 == "论语" then
        if 200 + math.floor(user.角色.子女列表[number].等级 / 10) <= user.角色.子女列表[number].元宵 then
            SendMessage(user.连接id, 7, "#y/你的这只子女已经无法服用更多的"..ItemTemp.名称)
            return true
        end
        user.角色.子女列表[number].元宵 = user.角色.子女列表[number].元宵 + 1
       local 添加数额 = 0
        for i,v in ipairs{"玉灵果","红木短剑","山海经","搜神记","竹马","论语"} do
            if ItemTemp.名称 ==v  then
                    if 全局变量.资质标识[i] == "攻资" or 全局变量.资质标识[i] == "速资" or 全局变量.资质标识[i] == "躲资" or 全局变量.资质标识[i] == "防资" then
                        if user.角色.子女列表[number][全局变量.资质标识[i]] < 1300 then
                            添加数额 = 10
                        elseif user.角色.子女列表[number][全局变量.资质标识[i]] < 1400 then
                            添加数额 = 8
                        elseif user.角色.子女列表[number][全局变量.资质标识[i]] < 1500 then
                            添加数额 = 4
                        else
                            添加数额 = 3
                        end
                    elseif 全局变量.资质标识[i] == "体资" then
                        if user.角色.子女列表[number][全局变量.资质标识[i]] < 3000 then
                            添加数额 = 15
                        elseif user.角色.子女列表[number][全局变量.资质标识[i]] < 4000 then
                            添加数额 = 10
                        elseif user.角色.子女列表[number][全局变量.资质标识[i]] < 5000 then
                            添加数额 = 8
                        else
                            添加数额 = 5
                        end
                    elseif 全局变量.资质标识[i] == "法资" then
                        if user.角色.子女列表[number][全局变量.资质标识[i]] < 2000 then
                            添加数额 = 10
                        elseif user.角色.子女列表[number][全局变量.资质标识[i]] < 2500 then
                            添加数额 = 8
                        elseif user.角色.子女列表[number][全局变量.资质标识[i]] < 2800 then
                            添加数额 = 4
                        else
                            添加数额 = 3
                        end
                    end
                    user.角色.子女列表[number][全局变量.资质标识[i]] = user.角色.子女列表[number][全局变量.资质标识[i]] + 添加数额
                    SendMessage(user.连接id, 7, "#y/你的这只子女" .. 全局变量.资质标识[i] .. "增加了" .. 添加数额 .. "点")
                    SendMessage(user.连接id, 7, "#y/你的这只子女还能使用" .. (200-user.角色.子女列表[number].元宵) .. "个"..v)
                break 
            end
        end
    elseif ItemTemp.名称 == "瑶池蟠桃" then
            if  user.角色.子女列表[number].炼兽真经 >=200 then
                SendMessage(user.连接id, 7, "#y/你的子女已经无法服用更多的瑶池蟠桃了")
                return true
            elseif user.角色.子女列表[number].成长 >= 1.6 then
                SendMessage(user.连接id, 7, "#y/你的这只子女成长已经超过1.6无法在使用瑶池蟠桃了")
                return true
            end
            user.角色.子女列表[number].炼兽真经 = user.角色.子女列表[number].炼兽真经 + 1
            local 随机成长 = math.random(3)/1000
            user.角色.子女列表[number].成长 = user.角色.子女列表[number].成长 + 随机成长
            SendMessage(user.连接id, 7, "#y/你的这只子女成长提升了#R/"..随机成长.."#Y/点")
            SendMessage(user.连接id, 7, "#y/你的这只子女还能服用了" .. (200-user.角色.子女列表[number].炼兽真经) .. "个瑶池蟠桃")
    elseif  string.find(ItemTemp.名称, "武学")~=nil   then 

        if  user.角色.子女列表[number].门派 then
            SendMessage(user.连接id,7,string.format("#y/你的孩子已经学习了%s门派的技能,无法再学习其他门派的技能#r/",user.角色.子女列表[number].门派))
            return
        else
             local TempText=string.sub(ItemTemp.名称,1,string.find(ItemTemp.名称, "武学")-1)
             local  门派技能={
                狮驼岭={[1]="极度疯狂",[2]="威慑"},
                盘丝洞={[1]="勾魂",[2]="姐妹同心"},
                魔王寨={[1]="三昧真火",[2]="飞砂走石"},
                地府={[1]="幽冥鬼眼",[2]="修罗隐身"},
                大唐官府={[1]="后发制人",[2]="杀气诀"},
                化生寺={[1]="活血",[2]="金刚护体"},
                女儿村={[1]="楚楚可怜",[2]="百毒不侵"},
                方寸山={[1]="定身符",[2]="五雷咒"},
                龙宫={[1]="龙卷雨击",[2]="龙腾"},
                天宫={[1]="天雷斩",[2]="五雷轰顶"},
                五庄观={[1]="烟雨剑法",[2]="炼气化神"},
                普陀山={[1]="杨柳甘露",[2]="日光华"},
                神木林={[1]="雾杀",[2]="蜜润"},
                凌波城={[1]="裂石",[2]="不动如山"},
                无底洞={[1]="金身舍利",[2]="地涌金莲"}}
                for i=1,2 do
                    table.insert(user.角色.子女列表[number].技能,置技能(门派技能[TempText][i])) 
                    SendMessage(user.连接id,7,"#y/你的这只孩子学会了新技能#r/"..门派技能[TempText][i])
                end   
                user.角色.子女列表[number].门派=TempText

        end
          
    else
         SendMessage(user.连接id, 7, "#y/这个道具不能使用")
        return 
    end
      SendMessage(user.连接id, 7, "#y/你使用了"..ItemTemp.名称)

      ItemControl:RemoveItems(user,ItemNum,"孩子")

     ChildrenUpdateProperty (user.角色.子女列表[number],true)             
     self:GetChildrenData(user)
end
