-- @Author: 作者QQ381990860
-- @Date:   2021-11-27 17:21:26
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-23 04:17:10
 --======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-06-21 10:07:31
--======================================================================--
 RoleControl = class()
require("Script/Role/任务")
require("Script/Role/修炼")
require("Script/Role/奇经八脉")
require("Script/Role/好友")
require("Script/Role/属性")
require("Script/Role/师门技能")
require("Script/Role/快捷技能")
require("Script/Role/称谓")
require("Script/Role/辅助技能")
require("Script/Role/扣除添加数据")
require("Script/Role/门派")
require("Script/Role/坐骑")
require("Script/Role/创建角色")
require("Script/Role/祈福")
require("Script/Role/Children")
require("Script/QQ2124872/元神")
local floor = math.floor

function RoleControl:进入游戏处理(user)

       if user.角色.礼包.新手 == 0  then
            ItemControl:GiveItem(user.id,"新手玩家礼包")
            SendMessage(user.连接id,20054,AddItem("新手玩家礼包"))
            TaskControl:GetLinkTask(user,"商人的鬼魂")
            table.insert(user.角色.人物技能,{名称="牛刀小试",等级=1,种类=6,学会=true})
            SendMessage(user.连接id,7,"#Y/你学会了技能#r/牛刀小试")
            user.角色.礼包.新手= 1
      end
        if RoleControl:GetTaskID(user,"乌鸡") ~= 0 then
        user.副本 = RoleControl:GetTaskID(user,"乌鸡")
      elseif RoleControl:GetTaskID(user,"车迟") ~= 0 then
        user.副本 = RoleControl:GetTaskID(user,"车迟")
      end
      if RoleControl:GetTaskID(user,"变身卡") == 0 then
          user.角色.变身={造型=nil,技能=nil,属性={数值=0,类型=0,属性=""}}
      end
      if RoleControl:GetTaskID(user,"双倍") == 0 then
        user.角色.双倍 = false
      end
     for n=1,#user.角色.称谓 do
       if user.角色.称谓[n]==user.角色.门派.."首席弟子" and user.角色.id~=首席资源数据[GameActivities:取门派编号(user.角色.门派)].玩家id+0  then
          if user.角色.称谓.当前==user.角色.门派.."首席弟子" then
            user.角色.称谓.当前=""
           end
            RoleControl:删除称谓(user,user.角色.门派.."首席弟子",1)
            SendMessage(user.连接id,7,"#y/你已经不是首席弟子了,称谓被删除")
       end
     end



    if user.角色.帮派 ~= nil then
      if 帮派数据[user.角色.帮派] == nil then
      user.角色.帮派 = nil
      end
      if user.角色.帮派 ~= nil and 帮派数据[user.角色.帮派] and 帮派数据[user.角色.帮派].成员名单[user.id] == nil then
      RoleControl:添加系统消息(user,"#h/你已经被请离了#y/" .. 帮派数据[user.角色.帮派].名称 .. "#h/帮派，你现在是无帮人士了")

      local tempname = 帮派数据[user.角色.帮派].名称.."的"
      for k,v in pairs{"副帮主","左护法","右护法","长老","帮众"} do
         if RoleControl:检查称谓(user,tempname..v) then 
           RoleControl:回收称谓(user,tempname..v)
          break
         end 
      end
       
      user.角色.帮派 = nil
      end
    end
    if MapData[user.角色.地图数据.编号].宽<user.角色.地图数据.x/20 or MapData[user.角色.地图数据.编号].高<user.角色.地图数据.y/20 then 
          local  TempLoadtiom=  MapControl:Randomloadtion(user.角色.地图数据.编号)
        user.角色.地图数据.x=TempLoadtiom.x*20
        user.角色.地图数据.y=TempLoadtiom.y*20
    end 
    user.角色.称谓.特效=4285922956
    self:发送欢迎信息(user)

    

end
function RoleControl:删除角色(user,id)
  if user.id~= id+0 then
     SendMessage(user.连接id, 7, "#Y/你输入的ID不对请重新输入！")
  else
      local 写入信息=table.loadstring( ReadFile([[玩家信息/账号]]..user.角色.账号..[[/操作记录.txt]]))

   for i=#写入信息,1,-1 do
        if 写入信息[i] == user.id then
          table.remove(写入信息, i)
          break
      end
   end


      WriteFile([[玩家信息/账号]]..user.角色.账号..[[/操作记录.txt]],table.tostring(写入信息))
       SendMessage(user.连接id,99998,"您已经与这个世界隔离了")
       Network:退出处理(user.id, "删除成功")
  end
 end

function RoleControl:获取开运数据(user)
  self.SendMessage={
  银子 = user.角色.道具.货币.银子,
  体力 = user.角色.当前体力
 }
 for n=1,20 do
     if user.角色.道具.包裹[n]~=nil and user.物品[user.角色.道具.包裹[n]]~=nil then
         self.SendMessage[n]=user.物品[user.角色.道具.包裹[n]]
       end
     end
  return self.SendMessage
 end
function RoleControl:取是否佩戴装备(user)
  local sl =0
  for n = 21, 26 do
    if user.角色.装备数据[n] ~= nil then
      sl=sl+1
    end
  end
  for n = 31, 35 do
    if user.角色.灵饰[n] ~= 0 then
      sl=sl+1
    end
  end
  for n = 35, 38 do
    if user.角色.法宝[n] ~= 0 then
      sl=sl+1
    end
  end
 if sl ==0  then
    return false
 else
     return true
 end
 end
function RoleControl:改变造型(user,造型)
  if user.角色.门派 ~= "无" then
    SendMessage(user.连接id,7,"#y/你退出当前的门派,在进行造型转变")
    return
  elseif self:取是否佩戴装备(user) then
    SendMessage(user.连接id,7,"#y/请脱下装备.法宝.灵饰,锦衣在进行转变")
    return
  elseif  user.角色.坐骑.编号~=0 then
     SendMessage(user.连接id,7,"#y/请下骑坐骑,在进行转变")
    return
  elseif  user.角色.造型 == 造型 then
     SendMessage(user.连接id,7,"#y/请选择不一样的造型")
    return
  elseif  银子检查(user.id, 1000000)==false then
        SendMessage(user.连接id,7,"#y/当前银子不足100W两!")
    return
  else
    self:扣除银子(user,1000000,"改变造型")
     local  qusx = 取属性(造型)
    user.角色.造型 =造型
    user.角色.性别= qusx.性别
    user.角色.种族 =qusx.种族
    user.角色.染色方案 =qusx.染色方案
    user.角色.染色 = {c = 0,a = 0,b = 0},
       SendMessage(user.连接id, 2010, RoleControl:获取地图数据(user))
       MapControl:MapSend(user.id, 1016, self:获取地图数据(user), user.地图)
       SendMessage(user.连接id,7,"#y/改变造型成功，你当前的造型为：#r/"..造型)
  end
 end
function RoleControl:升级事件(user)
	MapControl:升级事件(user.id)
	user.角色.等级 = user.角色.等级 + 1
	user.角色.当前经验 = user.角色.当前经验 - user.角色.升级经验
	user.角色.升级经验 = 取升级经验(user.角色.等级)
	user.角色.潜能 = user.角色.潜能 + 5
	user.角色.体力上限 = user.角色.体力上限 + 5
	user.角色.活力上限 = user.角色.活力上限 + 5
	if user.角色.等级==69  then
		TaskControl:GetLinkTask(user,"玄奘的身世")
	elseif user.角色.等级==75 then
		TaskControl:GetLinkTask(user,"含冤小白龙")

    elseif user.角色.等级==111 then
       TaskControl:GetLinkTask(user,"三打白骨精")
	end
	if user.角色.等级 >=69 and user.角色.等级< 89 then
		user.角色.奇经八脉.可换乾元丹  = 1
		user.角色.潜能果.可换潜能果  = 50
		if user.角色.等级 >69 then
			for n = 1, #全局变量.修炼名称 do
				user.角色.人物修炼[全局变量.修炼名称[n]].上限 =13
			end
		end
	elseif user.角色.等级 >=89  and user.角色.等级< 109 then
		user.角色.潜能果.可换潜能果  = 100
		user.角色.奇经八脉.可换乾元丹  = 2
	elseif user.角色.等级 >=109 and user.角色.等级<129 then
		user.角色.奇经八脉.可换乾元丹  = 3
		if user.角色.等级 >=109 then
            for n = 1, #全局变量.修炼名称 do
				user.角色.人物修炼[全局变量.修炼名称[n]].上限 =17
			end
		end  
	elseif user.角色.等级 >=129 and user.角色.等级< 155 then
		user.角色.奇经八脉.可换乾元丹  =4
		if user.角色.等级 ==129  then
			for n = 1, #全局变量.修炼名称 do
				user.角色.人物修炼[全局变量.修炼名称[n]].上限 =20
			end
		end
	elseif user.角色.等级 >=155 and user.角色.等级< 159 then
		user.角色.奇经八脉.可换乾元丹  = 5
		user.角色.潜能果.可换潜能果  = 150
	elseif user.角色.等级 >=159 and user.角色.等级< 164 then
		user.角色.奇经八脉.可换乾元丹  = 6
    elseif user.角色.等级 >=164 and user.角色.等级< 168 then
		user.角色.奇经八脉.可换乾元丹  = 7
    elseif user.角色.等级 >=168 and user.角色.等级< 171 then
		user.角色.奇经八脉.可换乾元丹  = 8
		user.角色.潜能果.可换潜能果  = 200
	elseif user.角色.等级 >=171 then
		user.角色.奇经八脉.可换乾元丹  = 9
	end

	for n = 1, #全局变量.基础属性 do
		user.角色[全局变量.基础属性[n]] = user.角色[全局变量.基础属性[n]] + 1
	end
	RoleControl:刷新战斗属性(user)
 
	SendMessage(user.连接id,1003,self:获取角色气血数据(user))
	SendMessage(user.连接id,2004,"#y/1")
end

function RoleControl:角色改名(user,名称)
  local 名称数据 = table.loadstring( ReadFile("数据信息/名称数据.txt"))
  if   Network:名称检查(名称,user.连接id) then
    if   self:扣除仙玉(user,400,"角色更改名称")   then
      for n = 1, #名称数据 do
        if 名称数据[n].账号 == user.角色.账号 then
           名称数据[n].名称 = 名称
           WriteFile("数据信息/名称数据.txt", table.tostring(名称数据))
          break
        end
      end
      广播消息("#xt/#r/ "..user.角色.名称.."#y/排了三天三夜的队，终于成功改名为:#r/"..名称.."#32")
      user.角色.名称=名称
      SendMessage(user.连接id, 7, "#y/恭喜你改名成功!重新启动客户端生效")
      MapControl:MapSend(user.id, 1016, self:获取地图数据(user), user.地图)
    end
  else
    SendMessage(user.连接id,7,"#y/你没有足够的仙玉")
    return 0
  end
 end
function RoleControl:获取角色数据(user)
  local SenMsg = {
    名称 = user.角色.名称,
    id = user.id,
    种族=user.角色.种族,
    门贡 = user.角色.门贡,
    帮贡 = user.角色.帮贡,
    人气 = user.角色.人气,
    愤怒 = user.角色.愤怒,
    门派 = user.角色.门派,
    宠物=user.角色.宠物,
    染色方案=user.角色.染色方案,
    装备属性=user.角色.装备属性,
    奇经八脉 =user.角色.奇经八脉,
    辅助技能=user.角色.辅助技能,
    强化技能=user.角色.强化技能,
    剧情技能=user.角色.剧情技能,
    剧情技能点=user.角色.剧情技能点,
    特技数据=user.角色.特技数据,
    人物技能=user.角色.人物技能,
     乾元丹技能=user.角色.乾元丹技能,
    技能属性=user.角色.技能属性,
    当前气血 = user.角色.当前气血,
    气血上限 = user.角色.气血上限,
    最大气血 = user.角色.最大气血,
    当前魔法 = user.角色.当前魔法,
    法防=user.角色.法防,
    魔法上限 = user.角色.魔法上限,
    当前体力 = user.角色.当前体力,
    体力上限 = user.角色.体力上限,
    当前活力 = user.角色.当前活力,
    活力上限 = user.角色.活力上限,
    坐骑 = user.角色.坐骑,
    伤害 = user.角色.伤害,
    愤怒特效 = user.角色.愤怒特效,
    命中 = user.角色.命中,
    防御 = user.角色.防御,
    灵力 = user.角色.灵力,
    速度 = user.角色.速度,
    躲闪 = user.角色.躲闪,
    躲避 = user.角色.躲闪,
    装备属性 = user.角色.装备属性,
    潜能 = user.角色.潜能,
    染色 = user.角色.染色,
    当前经验 = user.角色.当前经验,
    等级 = user.角色.等级,
    升级经验 = user.角色.升级经验,
    造型 = user.角色.造型,
    武器数据 = user.角色.武器数据,
    地图数据 = user.角色.地图数据,
    锦衣数据 = user.角色.锦衣数据,
    师门技能 = user.角色.师门技能,
    默认法术 = user.角色.默认法术,
    称谓 = user.角色.称谓.当前,
    名称颜色=user.角色.称谓.特效,
    人物修炼= user.角色.人物修炼,
    召唤兽修炼=user.角色.召唤兽修炼,
    人物修炼经验={
         攻击 = 计算修炼等级经验(user.角色.人物修炼.攻击.等级,user.角色.人物修炼.攻击.上限),
         防御 = 计算修炼等级经验(user.角色.人物修炼.防御.等级,user.角色.人物修炼.防御.上限),
         法术 = 计算修炼等级经验(user.角色.人物修炼.法术.等级,user.角色.人物修炼.法术.上限),
         法抗 = 计算修炼等级经验(user.角色.人物修炼.法抗.等级,user.角色.人物修炼.法抗.上限),

         },
     召唤兽修炼经验={
         攻击 = 计算修炼等级经验(user.角色.召唤兽修炼.攻击.等级,user.角色.召唤兽修炼.攻击.上限),
         防御 = 计算修炼等级经验(user.角色.召唤兽修炼.防御.等级,user.角色.召唤兽修炼.防御.上限),
         法术 = 计算修炼等级经验(user.角色.召唤兽修炼.法术.等级,user.角色.召唤兽修炼.法术.上限),
         法抗 = 计算修炼等级经验(user.角色.召唤兽修炼.法抗.等级,user.角色.召唤兽修炼.法抗.上限),
        
         }}

  for n = 1, #全局变量.基础属性 do
    SenMsg[全局变量.基础属性[n]] = user.角色[全局变量.基础属性[n]]
  end
  if not user.屏蔽变身  then
     SenMsg.变身 = user.角色.变身.造型
  end

  if  user.召唤兽.数据.观看~=0 then
    if user.召唤兽.数据[user.召唤兽.数据.观看] then
              SenMsg.观看召唤兽 ={
        名称=user.召唤兽.数据[user.召唤兽.数据.观看].名称,
        造型=user.召唤兽.数据[user.召唤兽.数据.观看].造型,
        染色方案=user.召唤兽.数据[user.召唤兽.数据.观看].染色方案,
        染色组=user.召唤兽.数据[user.召唤兽.数据.观看].染色组,
        饰品=user.召唤兽.数据[user.召唤兽.数据.观看].饰品,
      }
    else
      user.召唤兽.数据.观看=0
    end

  end
  if user.角色.帮派 ~= nil and 帮派数据[user.角色.帮派] ~= nil and 帮派数据[user.角色.帮派].成员名单[user.id] ~= nil then
    SenMsg.帮派 = 帮派数据[user.角色.帮派].名称
    SenMsg.帮贡 = 帮派数据[user.角色.帮派].成员名单[user.id].帮贡.当前
  else
    SenMsg.帮派 = "无"
    SenMsg.帮贡 = 0
  end

  return SenMsg
 end
 
function RoleControl:获取战斗角色数据(user)
  local SendMessage = {
    名称 = user.角色.名称,
    id = user.id,
    愤怒 = user.角色.愤怒,
    门派 = user.角色.门派,
    宠物=user.角色.宠物,
    战斗锦衣=user.角色.锦衣数据.战斗锦衣,
    染色方案=user.角色.染色方案,
    装备属性=user.角色.装备属性,
    奇经八脉 =user.角色.奇经八脉,
    特技数据=user.角色.特技数据,
    当前气血 = user.角色.当前气血,
    气血上限 = user.角色.气血上限,
    最大气血 = user.角色.最大气血,
    当前魔法 = user.角色.当前魔法,
    法防=user.角色.法防,
    魔法上限 = user.角色.魔法上限,
    伤害 = user.角色.伤害,
     追加技能=user.角色.追加技能,
     变身技能=user.角色.变身技能,
     附加技能=user.角色.附加技能,
    法宝  =user.角色.法宝,
    主动技能={},
    乾元丹技能={},
    愤怒特效 = user.角色.愤怒特效,
    命中 = user.角色.命中,
    防御 = user.角色.防御,
    灵力 = user.角色.灵力,
    速度 = user.角色.速度,
    躲闪 = user.角色.躲闪,
    躲避 = user.角色.躲闪,
    装备属性 = user.角色.装备属性,
    染色 = user.角色.染色,
    等级 = user.角色.等级,
    造型 = user.角色.造型,
    武器数据 = user.角色.武器数据,
    默认法术 = user.角色.默认法术,
    修炼数据={},}
	for n = 1, #全局变量.基础属性 do
		SendMessage[全局变量.基础属性[n]] = user.角色[全局变量.基础属性[n]]
	end
	if not user.屏蔽变身  then
		SendMessage.变身 = user.角色.变身.造型
	end
	if SendMessage.战斗锦衣 and user.屏蔽战斗锦衣 then
		SendMessage.战斗锦衣 =nil
	end
	if user.角色.人物技能~=nil then
		for i=1,#user.角色.人物技能 do
			if user.角色.人物技能[i].种类~= 0 and user.角色.人物技能[i].种类~= 12 then
				if user.角色.人物技能[i].种类== 10  then
					SendMessage.主动技能[#SendMessage.主动技能+1]={名称=user.角色.人物技能[i].名称,等级=user.角色.等级,种类=user.角色.人物技能[i].种类}
				else
					SendMessage.主动技能[#SendMessage.主动技能+1]={名称=user.角色.人物技能[i].名称,等级=user.角色.人物技能[i].等级,种类=user.角色.人物技能[i].种类}
				end
			end
		end
	end
	if user.角色.乾元丹技能~=nil then
		for i=1,#user.角色.乾元丹技能 do
			if user.角色.乾元丹技能[i].种类~= 0 and user.角色.乾元丹技能[i].种类~= 12 then
				if user.角色.乾元丹技能[i].种类== 10  then
					SendMessage.主动技能[#SendMessage.主动技能+1]={名称=user.角色.乾元丹技能[i].名称,等级=user.角色.等级,种类=user.角色.乾元丹技能[i].种类}
				else
					SendMessage.主动技能[#SendMessage.主动技能+1]={名称=user.角色.乾元丹技能[i].名称,等级=user.角色.乾元丹技能[i].等级,种类=user.角色.乾元丹技能[i].种类}
				end
			end
		end
	end

	for n=1,#全局变量.修炼名称 do
		SendMessage.修炼数据[全局变量.修炼名称[n]]=user.角色.人物修炼[全局变量.修炼名称[n]].等级
	end
	return SendMessage
 end

 
function RoleControl:升级请求(user)

	local v = 0
	if user.角色.门派=="无" and user.角色.等级>=10 then
		SendMessage(user.连接id,20,{NpcData[12].模型,NpcData[12].名称,NpcData[12].对话[1],NpcData[12].选项,NpcData[12].事件})
		SendMessage(user.连接id,7,"#Y/请先找到门派传送员处选个门派吧")
		return 0

	elseif user.角色.等级 >=ServerConfig.限制等级  then 
		SendMessage(user.连接id,7,"#Y/当前服务器的限制等级为"..ServerConfig.限制等级.."无法继续升级")
		return 0
	elseif user.角色.等级>=69 and user.角色.等级 < 89 then
  
	elseif user.角色.等级>=89  and user.角色.等级 < 109 then
	elseif user.角色.等级>=109 and user.角色.等级 < 129 then
	elseif user.角色.等级>=129  and user.角色.等级 < 145 then
	elseif  user.角色.等级>=145 and not user.角色.飞升  and  DebugMode==false then
		SendMessage(user.连接id,7,"#Y/你的等级当前已经达到上限，需要到月宫吴刚处飞升才能提升155")
		return 0
	elseif  user.角色.等级>=155 and not user.角色.渡劫  and  DebugMode==false then
		SendMessage(user.连接id,7,"#Y/你的等级当前已经达到上限，需要到门派师父处领取渡劫任务")
		return 0
	elseif  user.角色.等级>=175 then
        SendMessage(user.连接id,7,"#Y/你的等级当前已经达到上限")
		return 0
	end
  for i=1,#user.角色.师门技能 do
    if user.角色.师门技能[i].等级 >= user.角色.等级-10 then
       v= v+1
    end
  end
    if user.角色.当前经验<user.角色.升级经验 then
      SendMessage(user.连接id,7,"#Y/你当前的经验不足以提升角色等级")
      return 0
    elseif  v <4 and user.角色.等级>=20 and DebugMode==false then
      SendMessage(user.连接id,7,"#Y/你必须满足4项师门技能达到"..(user.角色.等级-10).."的条件才可以升级")
    else
      self:升级事件(user)
      SendMessage(user.连接id,7,"#Y/等级提升成功")
    end
 end
function RoleControl:道具异常处理(user)-----------------------------------------------完成
 for i=1,20 do
    if user.角色.道具.包裹[i]~=nil and user.物品[user.角色.道具.包裹[i]]==nil then
      user.角色.道具.包裹[i]=nil
      end
    end
 for i=1,20 do
    if user.角色.道具.行囊[i]~=nil and user.物品[user.角色.道具.行囊[i]]==nil then
      user.角色.道具.行囊[i]=nil
      end
    end
     for i=1,20 do
    if user.角色.道具.法宝[i]~=nil and user.物品[user.角色.道具.法宝[i]]==nil then
      user.角色.道具.法宝[i]=nil
      end
    end
  for i=1,20 do
    if user.角色.道具.锦衣[i]~=nil and user.物品[user.角色.道具.锦衣[i]]==nil then
      user.角色.道具.锦衣[i]=nil
      end
    end
 end
function RoleControl:获取角色气血数据(user)----
	self.SendMessage = {
		当前气血 = user.角色.当前气血,
		气血上限 = user.角色.气血上限,
		最大气血 = user.角色.最大气血,
		当前魔法 = user.角色.当前魔法,
		魔法上限 = user.角色.魔法上限,
		当前经验 = user.角色.当前经验,
		升级经验 = user.角色.升级经验,
		愤怒 = user.角色.愤怒
		}
	return self.SendMessage
end
 
 
function RoleControl:扣除活力(user,类型, 数额)
  if 类型 == 1 then
    user.角色.当前活力 = math.floor(user.角色.当前活力 - user.角色.当前活力 * 数额)
  elseif 类型 == 2 then
    user.角色.当前活力 = math.floor(user.角色.当前活力 - 数额)
  end
 end
function RoleControl:获取地图数据(user)
 local  MSG = {
    名称 = user.角色.名称,
    id = user.id,
    武器数据 = user.角色.武器数据,
    锦衣数据 = user.角色.锦衣数据,
    地图数据 = user.角色.地图数据,
    造型 = user.角色.造型,
    染色 = user.角色.染色,
    染色方案 = user.角色.染色方案,
    等级 = user.角色.等级,
    门派 = user.角色.门派,
    队伍 = user.队伍,
    战斗开关 = user.战斗开关,
    坐骑 = user.角色.坐骑,
    称谓 = user.角色.称谓.当前,
    名称颜色 = user.角色.称谓.特效,
    effects = user.角色.称谓.effects,
    战斗 = user.战斗,
    动作 = user.动作,
   }
    if not user.屏蔽变身  then
     MSG.变身 = user.角色.变身.造型
  end
  if MSG.锦衣数据.定制 and   user.屏蔽定制 then
      MSG.锦衣数据.定制 =nil
  end
    if MSG.锦衣数据.战斗锦衣 and   user.屏蔽战斗锦衣 then
      MSG.锦衣数据.战斗锦衣 =nil
  end
   if user.队长 then
       MSG.队长 ={开关=true,
       动画= 队伍数据[user.队伍].队标}
   else
       MSG.队长 ={开关=false}
   end
  if  user.召唤兽.数据.观看~=0 then
     if user.召唤兽.数据[user.召唤兽.数据.观看] then
       MSG.观看召唤兽 ={
        名称=user.召唤兽.数据[user.召唤兽.数据.观看].名称,
        造型=user.召唤兽.数据[user.召唤兽.数据.观看].造型,
        染色方案=user.召唤兽.数据[user.召唤兽.数据.观看].染色方案,
        染色组=user.召唤兽.数据[user.召唤兽.数据.观看].染色组,
        饰品=user.召唤兽.数据[user.召唤兽.数据.观看].饰品,
      }
    else
      user.召唤兽.数据.观看=0
    end
  end
  if user.摆摊 == nil then
    MSG.摊位名称 = ""
  else
    MSG.摊位名称 = 摊位数据[user.id].名称
    MSG.摊位关注 = 摊位数据[user.id].关注
  end
  return MSG
 end
function RoleControl:重置属性点(user)
  for n = 1, #全局变量.基础属性 do
    user.角色[全局变量.基础属性[n]] = 10 + user.角色.等级
  end

  user.角色.潜能 = user.角色.等级 * 5+user.角色.潜能果.潜能果+5
  user.角色.装备三围 = {体质 = 0,力量 = 0,敏捷 = 0,耐力 = 0,魔力 = 0}
  self:刷新装备属性(user)
  RoleControl:刷新战斗属性(user)
 end
function RoleControl:获取角色染色数据(user)
  self.SendMessage = {
    染色方案 = user.角色.染色方案,
    染色 = user.角色.染色,
    造型 = user.角色.造型,
    武器数据 = user.角色.武器数据
  }

  return self.SendMessage
 end
function RoleControl:取可用格子数量(user,类型)
  local fornnumber =类型=="包裹" and user.角色.道具.Pages * 20 or  20
  local 临时数量 = 0

  for n = 1, fornnumber do
    if user.角色.道具[类型][n] == nil then
      临时数量 = 临时数量 + 1
    end
  end

  return 临时数量
 end
 function RoleControl:取可用Pages(user,TempPages)
 
  local TempPagess =(TempPages-1)*20+1

  for n = TempPagess, TempPages*20 do
    if user.角色.道具["包裹"][n] == nil then
      return n
    end
  end

  return 0
end
function RoleControl:取可用道具格子(user,类型)

  local fornnumber =类型=="包裹" and user.角色.道具.Pages * 20 or  20

  for n = 1, fornnumber do
    if user.角色.道具[类型][n] == nil then
      return n
    end
  end

  return 0
 end
function RoleControl:添加消费日志(user,内容)
  user.消费日志 = user.消费日志 .. os.date("%Y-%m-%d") .. " " .. 内容 .. "\n"
 end
function RoleControl:发送欢迎信息(user)
  if user.角色.离线时间 ~= nil then
     local 显示时间 = os.date("%Y年%m月%d日%X",user.角色.离线时间)
      local tx =""
    local ss=tostring(user.id)
    for i = 1, string.len(ss) do
        tx=tx.."#lhtxl"..string.sub(ss,i,i).."/"
    end



    tx = "#dq/#y/玩家#G/" .. user.角色.名称 .."("..tx.."#G/)#w/" .."#y/上线了，欢迎您#72"
    广播消息(tx)--称谓
     --self:添加系统消息(user,"#G/您好，欢迎回到梦幻西游这个大家庭！现实中辛劳已毕，请您好好享受梦幻西游给您带来的轻松愉悦的游戏体验!#y/如有疑问请联系GM哦..#小东/温馨提醒：#S/物理加速器查到必封永久,#r/请自重" )
    -- self:添加系统消息(user,"#G/您好，欢迎回到梦幻西游这个大家庭！5.1劳动节快乐!#y/如有疑问请联系GM哦..#小东/温馨提醒：#S/物理加速器查到必封永久,#r/请自重" )
     if user.角色.锦衣数据.定制  then
         发送游戏公告("#Y/天纵神武，英俊潇洒的".."#R/"..user.角色.名称.."#Y/登陆游戏，#g/欢迎大佬纵横三界.体验乐趣。");

     end
    SendMessage(user.连接id, 9, "#xt/#y/您上次的登录时间为#r/" .. 显示时间 .. "#y/。若非您本人登录，可能您的账号密码已经泄露，请及时修改自己的账号密码。")
  end
  user.角色.离线时间 = os.time()
  user.角色.离线ip = self.玩家ip
  if user.游戏管理 ~= 0 and user.角色.离线ip ~= nil then

  end
  SendMessage(user.连接id, 11, 游戏时辰.当前)

  SendMessage(user.连接id, 9, Config.欢迎内容)
  if user.角色.法防 == nil then
    self:刷新装备属性(user)
    RoleControl:刷新战斗属性(user,1)
  end
  TaskControl:刷新追踪任务信息(user.id)
 end
function RoleControl:取飞行限制(user)
  if user ==nil then
    return 
  end
  -- if user.队伍~=0 and user.队长==false then
  --     SendMessage(user.连接id,7,"#Y/只有队长才可使用此道具")
  --      return false
  -- end
  if user.地图 == 1511 or user.地图 == 11351  or user.地图 == 11352  or user.地图 == 11353  or user.地图 == 11354   or user.地图 == 11355  or user.地图 == 1241 then
    SendMessage(user.连接id, 7, "#y/本地图不允许使用飞行道具，请找指定传回长安城")
    return false
  elseif self:GetTaskID(user,"游泳")~=0 then
    SendMessage(user.连接id,7,"#Y/游泳大赛过程中不可使用此功能")
    return false
  elseif self:GetTaskID(user,"押镖")~=0 then
    SendMessage(user.连接id,7,"#Y/押镖过程中无法飞行!")
    return false
  else
    return true
  end
 end

function RoleControl:接受天罚(user)
  if user.战斗 ~= 0 then
    return 0
  else
    if user.队伍 ~= 0 then
      TeamControl:离开队伍(user.队伍, user.id)
      SendMessage(user.连接id, 7, "#y/你被强制离开了队伍")
    end

    广播消息(9, "#xt/#g/ " .. user.角色.名称 .. "#y/终日无所事事，只晓得以杀人为乐。为维护三界秩序，天庭特派执法天兵对其进行惩戒，希望其他玩家引以为戒")
    FightGet:进入处理(user.id, 100032, "66", 11)
  end
end

function RoleControl:辅助技能等级上限(user)
  if user.角色.帮派 == nil or 帮派数据[user.角色.帮派] == nil then
    return 150
  else
    return 150 + 帮派数据[user.角色.帮派].书院 * 2 + 10
  end
end
function RoleControl:飞升处理(user)
  if user.角色.飞升 then
    -- Nothing
  else
    user.角色.飞升 = true
    self.临时等级 = user.角色.等级
    user.角色.等级 = user.角色.等级 - 15
    RoleControl:添加成就积分(user,1)
    RoleControl:添加仙玉(user,1666,"飞身")
     SendMessage(user.连接id, 2072, {标题="飞入仙境",文本="恭喜你完成了飞升挑战"})
    self:重置属性点(user)
    user.召唤兽:飞升处理(user.id)
    self:添加系统消息(user, "#y/恭喜你完成了飞升挑战，您的等级由原来的#r/" .. self.临时等级 .. "#y/级下降至#r/" .. user.角色.等级 .. "#y/级。您的属性点已经被重置。")

    if user.角色.种族 == "人" then
      self.临时称谓 = "三届贤者"
    elseif user.角色.种族 == "魔" then
      self.临时称谓 = "混世魔王"
    elseif user.角色.种族 == "仙" then
      self.临时称谓 = "太乙金仙"
    else
      self.临时称谓 = "极乐天人"
    end
    self:添加称谓(user, self.临时称谓)
    广播消息("#xt/#g/" .. user.角色.名称 .. "#y/在月宫吴刚处完成了飞升挑战，用自己的实力证明了自己是三界的强者。获得了玉皇大帝特赐的#r/" .. self.临时称谓 .. "#y/称谓")
    self.修炼关键词 = "人物修炼"
    for n = 1, #全局变量.修炼名称 do
      if user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 == 20 then
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = 25
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = 15
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 == 19 then
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = 24
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = 15
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 == 18 then
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = 24
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = 14
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 == 17 then
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = 23
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = 14
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 == 16 then
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = 23
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = 13
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 == 15 then
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = 22
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = 13
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 == 14 then
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = 22
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = 12
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 == 13 then
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = 21
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = 12
      end
    end
    self.修炼关键词 = "召唤兽修炼"
    for n = 1, #全局变量.修炼名称 do
      if user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 == 20 then
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = 25
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = 15
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 == 19 then
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = 24
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = 15
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 == 18 then
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = 24
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = 14
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 == 17 then
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = 23
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = 14
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 == 16 then
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = 23
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = 13
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 == 15 then
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = 22
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = 13
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 == 14 then
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = 22
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = 12
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 == 13 then
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = 21
        user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = 12
      end
    end
    for n=1,#user.角色.师门技能 do
      self:升级技能(user,user.角色.师门技能[n])
    end

  end
end



function RoleControl:渡劫处理(user)

  if user.角色.渡劫 then
  else
    user.角色.渡劫=true
    self:添加系统消息(user,"#y/恭喜你完成了渡劫挑战，您的等级由原来的#r/".."155".."#y/级提升至#r/175#y/级。您的属性点已经被重置。")

    -- 计算修炼

    if user.角色.种族=="人" then
      self.临时称谓="一举成名天下惊"
    elseif  user.角色.种族=="魔" then
      self.临时称谓="苍茫三界主沉浮"
    elseif  user.角色.种族=="仙" then
      self.临时称谓="独步西游若等闲"
    else
      self.临时称谓="傻逼"
    end

    self:添加称谓(user,self.临时称谓)
    广播消息("#xt/#g/"..user.角色.名称.."#y/在天宫渡劫使者处完成了渡劫挑战#77，用自己的实力证明了自己是三界的强者。获得了玉皇大帝特赐的#r/"..self.临时称谓.."#y/称谓")
     发送游戏公告(user.角色.名称.."在天宫渡劫使者处完成了渡劫挑战，用自己的实力证明了自己是三界的强者。。")
      self.修炼关键词="人物修炼"

    for n=1,#全局变量.修炼名称 do

      if user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级==25 then
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限=30
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级=20
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级==19 then
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限=24
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级=15

       elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级==18 then
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限=24
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级=14
       elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级==17 then
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限=23
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级=14
       elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级==16 then
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限=23
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级=13
       elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级==15 then
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限=22
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级=13
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级==14 then
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限=22
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级=12
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级==13 then
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限=21
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级=12
        end
    end
     self.修炼关键词="召唤兽修炼"
    for n=1,#全局变量.修炼名称 do
      if user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级==25 then
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限=30
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级=20
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级==19 then
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限=24
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级=15

       elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级==18 then
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限=24
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级=14
       elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级==17 then
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限=23
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级=14
       elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级==16 then
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限=23
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级=13
       elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级==15 then
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限=22
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级=13
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级==14 then
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限=22
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级=12
      elseif user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级==13 then
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].上限=21
       user.角色[self.修炼关键词][全局变量.修炼名称[n]].等级=12

        end
       end

    end

 end



function RoleControl:玩家切磋(user, 挑战id)
  if UserData[挑战id] == nil then
    SendMessage(user.连接id, 7, "#y/对方并不在线")

    return 0
  elseif UserData[挑战id].摆摊 ~= nil then
    SendMessage(user.连接id, 7, "#y/对方正处于摆摊状态，无法发起战斗")

    return 0
  elseif user.地图 ~= UserData[挑战id].地图 then
    SendMessage(user.连接id, 7, "#y/对方与你不在同一地图内")
    return 0
  elseif user.队伍 ~= 0 and user.队长 == false then
    SendMessage(user.连接id, 7, "#y/只有队长才能发起战斗")
    return 0
  elseif user.队伍 ~= 0 and user.队伍 == UserData[挑战id].队伍 then
    SendMessage(user.连接id, 7, "#y/您不能对自己的队友发起攻击")
    return 0
  elseif user.战斗 ~= 0 or UserData[挑战id].战斗 ~= 0 and UserData[挑战id].观战模式 == false then
    if user.战斗 == 0 and UserData[挑战id].战斗 ~= 0 and user.队伍 == 0 then
      user.战斗 = UserData[挑战id].战斗
      user.观战模式 = true
      FightGet.战斗盒子[UserData[挑战id].战斗]:观战数据发送(user.id, 挑战id)
    else
      SendMessage(user.连接id, 7, "#y/对方正处于战斗中，请稍后再试")
      return 0
    end
  elseif user.地图 == 6001 or user.地图 == 6002 or user.地图 == 6003 then
    if 比武大会开始开关 == false then 
      SendMessage(user.连接id, 7, "#y/当前不是比赛时间，无法对其发起攻击")
      return 0
    elseif os.time() - UserData[挑战id].比武保护期 <= 30 then
      SendMessage(user.连接id, 7, "#y/对方处于30秒的保护期，暂时无法对其发起攻击")
      return 0
    else
      FightGet:创建战斗(user.id, 200004, 挑战id, 0)
    end

  elseif user.地图 == 11351 or user.地图 == 11352   or user.地图 == 11353 or user.地图 == 11354 or user.地图 == 11355 then

      if 帮派竞赛.开关==false  then
      SendMessage(user.连接id, 7, "#y/当前不是比赛时间，无法对其发起攻击")
      return 0
    -- elseif os.time() - UserData[挑战id].首席保护 <= 30 then
    --   SendMessage(user.连接id, 7, "#y/对方处于保护期，暂时无法对其发起攻击")
    --   return 0
      elseif  UserData[挑战id].角色.称谓.特效 == user.角色.称谓.特效 then
        SendMessage(user.连接id, 7, "#y/相同阵营的玩家不能发起攻击")
      else
      FightGet:创建战斗(user.id, 200005, 挑战id, 0)
    end

  elseif user.地图 ==1241 then

      if 帮派竞赛.迷宫开关==false  then
      SendMessage(user.连接id, 7, "#y/当前不是比赛时间，无法对其发起攻击")
      return 0
      elseif  UserData[挑战id].角色.称谓.特效 == user.角色.称谓.特效 then
        SendMessage(user.连接id, 7, "#y/相同阵营的玩家不能发起攻击")
      else
      FightGet:创建战斗(user.id, 200008, 挑战id, 0)
    end

  elseif user.地图 == 5135 then
    if 首席争霸赛战斗开关 == false then
      SendMessage(user.连接id, 7, "#y/当前不是比赛时间，无法对其发起攻击")
      return 0
    elseif os.time() - UserData[挑战id].首席保护 <= 30 then
      SendMessage(user.连接id, 7, "#y/对方处于保护期，暂时无法对其发起攻击")

      return 0
    else
      FightGet:创建战斗(user.id, 200002, 挑战id, 0)
    end


-- elseif user.地图 == 1208 then
--     FightGet:创建战斗(user.id,200001,挑战id,0)
--      user.战斗对象 = 挑战id
--                 广播消息(9,"#xt/#S/(华山论剑)#y/狭路相逢勇者胜，一剑西来空城雪。#g/" .. user.角色.名称 .. "(" .. user.id .. "）" .. "#y/对#g/" .. UserData[user.战斗对象].角色.名称 .. "(" .. UserData[user.战斗对象].id .. ")#y/发起了pk战斗")

    elseif user.地图 == 1001 then
      if 取两点距离({x=8274,y=3426},{x=user.角色.地图数据.x,y=user.角色.地图数据.y})>400 or 取两点距离({x=8274,y=3426},{x=UserData[挑战id].角色.地图数据.x,y=UserData[挑战id].角色.地图数据.y})>400 then
        SendMessage(user.连接id,7,"#y/只有长安城擂台才可以进行切磋")
        return 0
      else
        FightGet:创建战斗(user.id,200001,挑战id,0)
      end

  elseif user.角色.pk开关 == false then
        SendMessage(user.连接id,7,"#y/只有长安城擂台才可以进行切磋")

  else
        if UserData[挑战id].角色.pk开关 then
          if UserData[挑战id].战斗 ~= 0 and UserData[挑战id].观战模式 == false then
            SendMessage(user.连接id, 7, "#y/对方正在战斗中，请稍后再试")
            return 0
          elseif UserData[挑战id].地图 ~= user.地图 then
            SendMessage(user.连接id, 7, "#y/你与对方不处于同一地图内，无法发动攻击")
            return 0
          elseif user.地图 == 1001 or user.地图 == 5131 or user.地图 == 1092 then
                SendMessage(user.连接id, 7, "#y/本地图不允许发起pk战斗")
                return 0
          else
                if UserData[挑战id].战斗 ~= 0 and UserData[挑战id].观战模式 then
                FightGet.战斗盒子[UserData[挑战id].战斗]:退出观战(挑战id)
                UserData[挑战id].战斗 = 0
                UserData[挑战id].观战模式 = false
                end
                user.战斗对象 = 挑战id
                广播消息(9, "#xt/#y/生死看淡，不服就干。#g/" .. user.角色.名称 .. "(" .. user.id .. "）" .. "#y/对#g/" .. UserData[user.战斗对象].角色.名称 .. "(" .. UserData[user.战斗对象].id .. ")#y/发起了pk战斗")
                FightGet:创建战斗(user.id, 200003, 挑战id, 0)
                RoleControl:添加系统消息(UserData[UserData[user.战斗对象].id], "#h/" .. user.角色.名称 .. "[" .. user.id .. "]对你发起了pk战斗。")
                return 0
          end
        elseif os.time() < UserData[挑战id].角色.pk保护 then
          SendMessage(user.连接id, 7, "#y/对方正处于保护期，无法对其发动攻击")
          return 0
        else
          user.战斗对象 = 挑战id
          local 对话内容 = "你将花费200点人气对#Y/" .. UserData[挑战id].角色.等级 .. "#W/级的#G/" .. UserData[挑战id].角色.名称 .."#W/发起战斗，若对方等级低于或高于你20级将额外扣除100点人气。请确认是否要发起战斗"
          SendMessage(user.连接id,20,{user.角色.造型,user.角色.名称,对话内容,{"生死已看淡不服就是干","我再考虑会"}})
        end
  end

end

function RoleControl:技能降级(user)
  self.条件达成 = false
  self.扣除编号 = 0
  self.扣除等级 = 0
  for n = 1, #user.角色.师门技能 do
    if self.扣除等级 < user.角色.师门技能[n].等级 then
      self.扣除编号 = n
      self.扣除等级 = user.角色.师门技能[n].等级
    end
  end

  if self.扣除编号 ~= 0 then
    user.角色.师门技能[self.扣除编号].等级 = user.角色.师门技能[self.扣除编号].等级 - 1

    SendMessage(user.连接id, 9, "#xt/#y/你因为死亡导致师门技能" .. user.角色.师门技能[self.扣除编号].名称 .. "#y/等级下降1级")
  end
end

function RoleControl:清空包裹(user)
  for n = 1, 80 do
    if user.角色.道具.包裹[n] ~= nil then
      user.物品[user.角色.道具.包裹[n]] = nil
      user.角色.道具.包裹[n] = nil
    end
  end

  SendMessage(user.连接id, 3006, "88")
 end
function RoleControl:创建副本(user,副本)
   if  TaskControl["创建"..副本.."副本"] then
    TaskControl["创建"..副本.."副本"](TaskControl,user.id)
   else     
       SendMessage(user.连接id, 7, "#y/这个副本还没开发请留意公告")
   end
  
end
function RoleControl:进入副本(user)
  if user.副本 == 0 then
      SendMessage(user.连接id, 7, "#y/你似乎还没有开启副本任务")
    else
      self.允许传送 = true

      if user.队伍 ~= 0 then
        for n = 1, #队伍数据[user.队伍].队员数据 do
          if UserData[队伍数据[user.队伍].队员数据[n]].副本 ~= user.副本 then
            self.允许传送 = false
          end
        end
      end

      if self.允许传送 then
        MapControl:Jump(user.id, 任务数据[user.副本].传送数据.地图, 任务数据[user.副本].传送数据.x, 任务数据[user.副本].传送数据.y)
      else
        SendMessage(user.连接id, 7, "#y/有队员与队长副本不一致，无法从我这里传送")
      end
    end
end
function RoleControl:清除专用特效(user)
  for n = 21, 26 do
    if user.角色.装备数据[n] ~= nil and user.物品[user.角色.装备数据[n]] ~= nil then
      user.物品[user.角色.装备数据[n]].专用 = nil
    end
  end
end
function RoleControl:学习剧情点1(user,序号)
  if user.角色.剧情技能点<1 then
    SendMessage(user.连接id, 7, "#y/你当前的剧情技能点不够")
  elseif user.角色.剧情技能[序号+0].等级 >9 then
    SendMessage(user.连接id, 7, "#y/你当前的剧情技能已经学满,无需要学习了")
  else
    user.角色.剧情技能点=user.角色.剧情技能点-1
    user.角色.剧情技能[序号+0].等级=user.角色.剧情技能[序号+0].等级+1
    SendMessage(user.连接id,20053,{剧情技能=user.角色.剧情技能,剧情点=user.角色.剧情技能点})
    SendMessage(user.连接id, 7, "#y/学习成功")
  end

end

function RoleControl:学习剧情点(user)
  local TempMoney = 10000000
 if 银子检查(user.id,TempMoney)  then
       if user.角色.当前经验 >=  100000000 then
        self:扣除经验(user,100000000, "剧情")
        self:扣除银子(user,TempMoney,"剧情点")

        user.角色.剧情技能点=user.角色.剧情技能点+1
        SendMessage(user.连接id, 7, "#y/你获得了1点剧情技能点")
         SendMessage(user.连接id,20053,{剧情技能=user.角色.剧情技能,剧情点=user.角色.剧情技能点})
       else
         SendMessage(user.连接id, 7, "#y/你的经验不够，请检查是否有1亿")
       end
      else
        SendMessage(user.连接id, 7, "#y/你的银子不够，请检查是否有1亿")
      end
end
function RoleControl:重置经脉(user)
     if 银子检查(user.id,5000000)  then
        self:扣除银子(user,5000000,"洗乾元丹")
        user.角色.默认法术=nil
        user.角色.乾元丹技能 = {}
        local x1,x2,x3,x4=user.角色.奇经八脉.乾元丹,user.角色.奇经八脉.乾元丹,user.角色.奇经八脉.可换乾元丹,user.角色.奇经八脉.附加乾元丹
        user.角色.奇经八脉={剩余乾元丹=x1,乾元丹=x2,可换乾元丹=x3,附加乾元丹=x4,技能树={1,2,3}}
        RoleControl:刷新战斗属性(user,1)
        SendMessage(user.连接id, 7, "#y/你的奇经八脉已经重置成功")
        SendMessage(user.连接id,2069,self:索取奇经八脉(user))
      else
        SendMessage(user.连接id, 7, "#y/你的银子不够，请检查是否有500万")
      end
end
function RoleControl:活跃度奖励(user,类型)
  if 活动数据.活跃度[user.id] then
        if 活动数据.活跃度[user.id][类型]==0 then
          self:添加经验(user.角色.等级*user.角色.等级*100*类型+500000,"活跃度")
          self:添加银子(user,5000*类型*user.角色.等级,"活跃度")
          活动数据.活跃度[user.id][类型]=1
        else
          SendMessage(user.连接id, 7, "#y/你已经领取过该奖励了")
        end
  end
end
function RoleControl:获取签到数据(user)
   local 当月天数 = os.date("%d",os.time({year=os.date("%Y"),month=os.date("%m")+1,day=0})) --当月天数
   local 月份 =tonumber(os.date("%m"))
   local 几号 =tonumber(os.date("%d"))

   if 签到数据[user.id] ~= nil then
     if 签到数据[user.id][月份] == nil then
      签到数据[user.id]={[月份]={}}
      签到数据[user.id][月份]={
      当月天数=当月天数,
      几号=几号,
      月份=月份,
      累计签到=0
      }
      for i=1,当月天数 do
        if 签到数据[user.id][月份][i] == nil then
           签到数据[user.id][月份][i] =false
          end
      end
     end
   else
     签到数据[user.id]={[月份]={当月天数=当月天数,
      几号=几号,
      月份=月份,
      累计签到=0}}
      for i=1,当月天数 do
        if 签到数据[user.id][月份][i] == nil then
           签到数据[user.id][月份][i] =false
          end
      end
   end
   if 签到数据[user.id][月份].几号 ~= 几号 then
    签到数据[user.id][月份].几号 = 几号
   end
   SendMessage(user.连接id,20055,签到数据[user.id][月份])
end
function RoleControl:签到处理(user)
  local 月份 =tonumber(os.date("%m", os.time()))
   local 几号 = tonumber(os.date("%d", os.time()))
  local 累计次数 = 签到数据[user.id][月份].累计签到

  if 签到数据[user.id][月份][几号]  then
     SendMessage(user.连接id,7,"#y/今日已经签到过了!")
  elseif 活动数据.活跃度[user.id]    then
    if 活动数据.活跃度[user.id].积分 <400 then
      SendMessage(user.连接id,7,"#y/你今日的活跃度未达到400无法进行签到!")
      return
    end
    签到数据[user.id][月份][几号] = true
    签到数据[user.id][月份].累计签到 = 累计次数 + 1
    累计次数 = 累计次数 + 1
    self:添加经验(user.角色.等级*user.角色.等级*100,"签到")

    if 累计次数 == 7 then
      ItemControl:GiveItem(user.id,"双倍经验丹")
    elseif 累计次数 == 14 then
    ItemControl:GiveItem(user.id,"修炼果")
    elseif 累计次数 == 21 then
     ItemControl:GiveItem(user.id,"高级魔兽要诀",nil,取高级兽诀名称())
    elseif 累计次数 == 28 then
      ItemControl:GiveItem(user.id,"神兜兜")
      if math.random(100)<= 30  then
         ItemControl:GiveItem(user.id,"浪淘纱")
      end
    elseif 累计次数 == 30 then
     ItemControl:GiveItem(user.id,"青花瓷")
    end
    SendMessage(user.连接id,20055,签到数据[user.id][月份])
  else
    SendMessage(user.连接id,7,"#y/你今日的活跃度未达到400无法进行签到!")
  end
end

function RoleControl:获取活跃度(user)
  local fs ={}
  fs.活跃度=user.角色.活跃度
  if 活动数据.活跃度[user.id] then
     fs.获得活跃度=活动数据.活跃度[user.id].积分
  else
    fs.获得活跃度=0
  end
  return fs
end
function RoleControl:耐久处理(user,类型) --1=物理伤害 2=防具挨打 3=法术伤害
  local  耐磨=false
  if 类型==1 then
   if user.角色.装备数据[23]~=nil and user.物品[user.角色.装备数据[23]]~=0 and user.物品[user.角色.装备数据[23]]~=nil  then
         if user.物品[user.角色.装备数据[23]].特效 then
             for i,v in ipairs(user.物品[user.角色.装备数据[23]].特效)  do
                if v =="永不磨损" then
                  耐磨=true
                end
              end
          end
              if user.物品[user.角色.装备数据[23]].符石~=nil then
                 for p=1,5 do
                  if user.物品[user.角色.装备数据[23]].符石[p] then
                    if  user.物品[user.角色.装备数据[23]].符石[p].耐久> 0 then
                      user.物品[user.角色.装备数据[23]].符石[p].耐久 =user.物品[user.角色.装备数据[23]].符石[p].耐久-0.03
                    else
                        SendMessage(user.连接id,7,"#y/你的"..user.物品[user.角色.装备数据[23]].名称.."中的#r/"..user.物品[user.角色.装备数据[23]].符石[p].名称.."#y/因耐久度过低已无法使用")
                        user.物品[user.角色.装备数据[23]].符石[p] =nil
                        user.物品[user.角色.装备数据[23]].符石.组合=nil
                    end
                  end
                end
              end
     if 耐磨==false then
        user.物品[user.角色.装备数据[23]].耐久度=user.物品[user.角色.装备数据[23]].耐久度-0.05
      if user.物品[user.角色.装备数据[23]].耐久度<=0 then
        user.物品[user.角色.装备数据[23]].耐久度=0
        self:刷新装备属性(user)
        SendMessage(user.连接id,7,"#y/你的#r/"..user.物品[user.角色.装备数据[23]].名称.."#y/因耐久度过低已无法使用")
          end
        end
     end
  elseif 类型==2 then
    for n=21,26 do
     if user.角色.装备数据[n]~=nil and user.物品[user.角色.装备数据[n]]~=0  and user.物品[user.角色.装备数据[n]]~=nil and n~=23  then
      if user.物品[user.角色.装备数据[n]].特效 then
         for i,v in ipairs(user.物品[user.角色.装备数据[n]].特效)  do
            if v =="永不磨损" then
              耐磨=true
            end
          end
        end
      if user.物品[user.角色.装备数据[n]].符石~=nil then
          for p=1,5 do
           -- for p, v in pairs(user.物品[user.角色.装备数据[n]].符石) do
            if user.物品[user.角色.装备数据[n]].符石[p] then
              if  user.物品[user.角色.装备数据[n]].符石[p].耐久> 0 then
                user.物品[user.角色.装备数据[n]].符石[p].耐久 =user.物品[user.角色.装备数据[n]].符石[p].耐久-0.3
              else
                 SendMessage(user.连接id,7,"#y/你的"..user.物品[user.角色.装备数据[n]].名称.."中的#r/"..user.物品[user.角色.装备数据[n]].符石[p].名称.."#y/因耐久度过低已无法使用")
                 user.物品[user.角色.装备数据[n]].符石[p]=nil
                 user.物品[user.角色.装备数据[n]].符石.组合=nil
              end
            end
          end
      end
    if 耐磨==false then
       user.物品[user.角色.装备数据[n]].耐久度=user.物品[user.角色.装备数据[n]].耐久度-0.0325
     if user.物品[user.角色.装备数据[n]].耐久度<=0 then
        user.物品[user.角色.装备数据[n]].耐久度=0
        self:刷新装备属性(user)
        SendMessage(user.连接id,7,"#y/你的#r/"..user.物品[user.角色.装备数据[n]].名称.."#y/因耐久度过低已无法使用")
          end
        end
       end
      end
    end
 end
function RoleControl:制作灵饰图鉴(user,等级)
  if user.角色.当前活力 < 等级 then
     SendMessage(user.连接id,7,"#y/你的当前的活力不足"..等级.."点,无法制作图鉴")
   else
    user.角色.当前活力=user.角色.当前活力-等级
    ItemControl:GiveItem(user.id,"灵饰图鉴",等级)
  end
end

function RoleControl:死亡处理(类型, user, 战斗类型, 消息)
  if 消息 == nil then
    return 0
  end
  self:添加系统消息(user, "#h/你被" .. 消息 .. "杀死了")
  if user.角色.等级 <= 64 and 战斗类型 ~= 200003 and 战斗类型 ~= 100032 then
    return 0
  end
  local Multiplying =1 --死亡倍率
  local DeductMoney=math.floor(user.角色.道具.货币.银子 * 0.05)   --扣除银子
  if 类型 == 1 then
    Multiplying = 2
  end

  if 战斗类型 == 200003 then
    Multiplying = 5
    DeductMoney = math.floor(user.角色.道具.货币.银子 * 0.35)
  elseif 战斗类型 == 100032 then
    Multiplying = 10
    DeductMoney = math.floor(user.角色.道具.货币.银子 * 0.5)
  end
  local ExcludingExp=math.floor((user.角色.等级 * user.角色.等级 * user.角色.等级 + 500) * Multiplying)--扣除经验

  local 组合语句 = ""
  local 降级名称 = ""

  if 银子检查(user.id, DeductMoney) == false then
    SendMessage(user.连接id, 9, "#xt/#y/你因为死亡损失了" .. user.角色.道具.货币.银子 .. "两银子")
    user.角色.道具.货币.银子 = 0
    组合语句 = "#w/你因为死亡损失了#r/" .. user.角色.道具.货币.银子 .. "#w/两银子"
    self:技能降级(user)
  else
    self:扣除银子(user,DeductMoney, 19)
    SendMessage(user.连接id, 9, "#xt/#y/你因为死亡损失了" .. DeductMoney .. "两银子")
    组合语句 = "#w/你因为死亡损失了#r/" .. DeductMoney .. "#w/两银子"
  end
  组合语句 = 组合语句 .. "\n"
  if user.角色.当前经验 < ExcludingExp then
    SendMessage(user.连接id, 9, "#xt/#y/你因为死亡损失了" .. user.角色.当前经验 .. "点经验")
    组合语句 = 组合语句 .. "#w/你因为死亡损失了#r/" .. user.角色.当前经验 .. "#w/点经验"
    user.角色.当前经验 = 0
    self:技能降级(user)
  else
    self:扣除经验(user,ExcludingExp, 19)
    组合语句 = 组合语句 .. "#w/你因为死亡损失了#r/" .. ExcludingExp .. "#w/点经验"
    SendMessage(user.连接id, 9, "#xt/#y/你因为死亡损失了" .. ExcludingExp .. "点经验")
  end
  self:添加系统消息(user, 组合语句)
  if 战斗类型 == 200003 or 战斗类型 == 100032 then
    self:技能降级(user)
    self:技能降级(user)
    self:技能降级(user)
    self:技能降级(user)
    self:技能降级(user)
    if user.队伍 ~= 0 then
      TeamControl:离开队伍(user.队伍, user.id, 1)
    end
    user.角色.pk保护 = os.time() + 7200
    user.角色.pk时间 = 0
    MapControl:Jump(user.id, 1125, 28, 15, 1)
  end

  if 降级名称 ~= "" then
    self:添加系统消息(user, 降级名称)
  end
  if 战斗类型 == 100032 then
   降级名称 = ""
    self:技能降级(user)
    self:技能降级(user)
    self:技能降级(user)
    self:技能降级(user)
    self:技能降级(user)
    if 降级名称 ~= "" then
      self:添加系统消息(user, 降级名称)
    end
    user.角色.阴德 = 10
    天罚数据表[user.id] = 0
    local 掉落序列 = {}
    for n = 21, 26 do
      if user.角色.装备数据[n] ~= nil then
        掉落序列[#掉落序列 + 1] = n
      end
    end

    if #掉落序列 ~= 0 then
      local 序号 = 掉落序列[math.random( #掉落序列)]
      self:添加系统消息(user, "#w/你因为死亡损失了#r/" .. user.物品[user.角色.装备数据[序号]].名称 .. "#w/装备")
      user.物品[user.角色.装备数据[序号]] = nil
      user.角色.装备数据[序号] = nil
      self:刷新装备属性(user)
      if 序号 == 21 then
        SendMessage(user.连接id, 2010, RoleControl:获取地图数据(user))
        MapControl:MapSend(user.id, 1016, RoleControl:获取地图数据(user), user.地图)
      end
    end
  end
end

function RoleControl:存档(user)
	user.角色.离线ip = user.ip
	if user.账号 == nil or file_exists("玩家信息/账号" .. user.账号) == "0" then
		return 0
	end
	WriteFile("玩家信息/账号" .. user.账号 ..[[/]]..user.id.. [[/角色.txt]], table.tostring(user.角色))
	if user.召唤兽 == nil or user.物品 == nil then
		return 0
	end
	if user.助战 ~= nil then
		user.助战:存档(user.id)
		print("玩家助战存档成功")
	end
	user.召唤兽:存档(user.id)
	WriteFile("玩家信息/账号" .. user.账号 .. [[/]]..user.id.."/道具.txt", table.tostring(user.物品))
	WriteFile("玩家信息/账号" .. user.账号 ..[[/]]..user.id.."/日志/" .. user.日志编号 .. ".txt", user.消费日志)

  if string.len(user.消费日志) >= 10240 then
    user.日志编号 = user.日志编号 + 1 .. ""
    user.消费日志 = "日志创建"

    WriteFile("玩家信息/账号" .. user.账号 ..[[/]]..user.id.. "/日志/" .. user.日志编号 .. ".txt", "日志创建\n")
    WriteFile("玩家信息/账号" .. user.账号 .. [[/]]..user.id.."/日志编号.txt", user.日志编号)
  end
 end





 function RoleControl:黑市拍卖(user)

  local 道具id=0
  local 格子id=0
  -- if user.古玩拍卖~=nil and os.time()-user.古玩拍卖<=5 then
  -- SendMessage(user.连接id, 20,{user.角色.造型,user.角色.名称,"你的操作过于频繁，请稍后再试"})
   
  --  return
  if user.角色.等级<60 then
    SendMessage(user.连接id, 20,{user.角色.造型,user.角色.名称,"只有等级达到60级的角色才可参加古玩拍卖"})
   return
  elseif 古玩数据[user.id]~=nil and 古玩数据[user.id]>=100 then
    SendMessage(user.连接id, 20,{user.角色.造型,user.角色.名称,"你本日已经参加过100次拍卖了，请明日再来"})
   return
    end
if 古玩数据[user.id]==nil then 古玩数据[user.id]=0 end
 古玩数据[user.id]=古玩数据[user.id]+1
  -- user.古玩拍卖=os.time()
  for n=1,80 do
   if user.角色.道具.包裹[n]~=nil and user.物品[user.角色.道具.包裹[n]].名称=="未鉴定的古董" then --从道具栏中检测是否存在古董
     道具id=user.角色.道具.包裹[n]
     格子id=n
     end
    end
 if 道具id==0 then
  SendMessage(user.连接id, 20,{user.角色.造型,user.角色.名称,"请先在古玩店老板处购买未鉴定的古董"})
   return
   end
 user.物品[道具id].数量=user.物品[道具id].数量-1
 if user.物品[道具id].数量<=0 then
   user.物品[道具id]=nil
   user.角色.道具.包裹[格子id]=nil
   end

 SendMessage(user.连接id,3040,"1")
 --计算类型
 local 参数=math.random(1,100)
 local 计算流程={类型=0,动作={},银子=0}
 if 参数<=30 then --赝品

   计算流程.类型=1
   RoleControl:添加消费日志(user,"黑石拍卖为赝品")
  
  elseif 参数<=60 then --强盗
    计算流程.类型=2

    RoleControl:添加消费日志(user,"黑石拍卖遇到强盗")
  elseif 参数<=90 then --8888
    计算流程.类型=3
    计算流程.银子=8888
  
      RoleControl:添加银子(user,8888, "黑市拍卖")
  else
    local 银子=math.random(8888,88888)
    local 连击=math.random(1,11)  --最高连11下 
    local 初始银子=银子
    计算流程.类型=4
    计算流程.动作[1]=银子
    if math.random(100)<=5 then
     连击=11
      end
    for n=1,连击 do
     local 条件=math.random(100)
     银子=0
     if 条件<=15 then
       银子=银子+1000
     elseif 条件<=20  then
       银子=银子+math.floor(n*math.random(3000,8000))
      elseif 条件<=50  then
       银子=银子+math.floor(n*math.random(2000,20000))
      elseif 条件<=60  then
       银子=银子+math.floor(n*math.random(10000,20000))
      elseif 条件<=80  then
       银子=银子+math.floor(n*math.random(20000,30000))
      else
       银子=银子+math.floor(n*math.random(20000,50000))  ---20%最高
       end
      if math.random(1,100)<=5 then---有一定几率双倍  
          银子=银子*2
         end 
      初始银子=初始银子+银子
      计算流程.动作[#计算流程.动作+1]=初始银子
      end
      计算流程.银子=初始银子
   
      RoleControl:添加银子(user,初始银子, "黑市拍卖")
      if 初始银子>=1000000 then
       广播消息(string.format("#xt/#S(黑市拍卖会)#R/%s#Y带着自己的宝贝在参加黑市拍卖会时，居然被拍出了#G/%s#Y两银子的天价",user.角色.名称,初始银子))
        end
   end

  SendMessage(user.连接id,3076,计算流程)
  end


return RoleControl
