--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-12-12 00:42:16
--======================================================================--
function RoleControl:修炼学习(user,编号,倍数)
  local 类型
   if 编号 == 1 then
    类型 = "攻击"
  elseif 编号 == 2 then
     类型 = "法术"
  elseif 编号 == 3 then
     类型 = "防御"
  elseif 编号 == 4 then
     类型 = "法抗"
  else
    类型=nil
   end
        if 类型== nil then
      SendMessage(user.连接id, 7, "#y/数据错误请重新尝试")
            return 0
      elseif user.角色.人物修炼[类型].上限 <= user.角色.人物修炼[类型].等级 then
      SendMessage(user.连接id, 7, "#y/你当前的这项修炼等级已达上限，无法再增加修炼经验")
      return 0

    elseif 取角色银子(user.角色.id) < 200000*倍数 then
      SendMessage(user.连接id, 7, "#y/你没有那么多的银子")
      return 0
    elseif user.角色.当前经验 < 1000000*倍数 then
      SendMessage(user.连接id, 7, "#y/你没有那么多的经验")
      return 0
    else
      self:扣除经验(user,1000000*倍数,"修炼")
        self:扣除银子(user,200000*倍数,"修炼")
      self:添加人物修炼经验(user,10*倍数,类型)
      self:取人物修炼(user)
    end
 end
function RoleControl:取人物修炼(user)
    local sj = {
      [1]={user.角色.人物修炼.攻击.等级,user.角色.人物修炼.攻击.经验,计算修炼等级经验(user.角色.人物修炼.攻击.等级,user.角色.人物修炼.攻击.上限),user.角色.人物修炼.攻击.上限},
      [2]={user.角色.人物修炼.法术.等级,user.角色.人物修炼.法术.经验,计算修炼等级经验(user.角色.人物修炼.法术.等级,user.角色.人物修炼.法术.上限),user.角色.人物修炼.法术.上限},
      [3]={user.角色.人物修炼.防御.等级,user.角色.人物修炼.防御.经验,计算修炼等级经验(user.角色.人物修炼.防御.等级,user.角色.人物修炼.防御.上限),user.角色.人物修炼.防御.上限},
      [4]={user.角色.人物修炼.法抗.等级,user.角色.人物修炼.法抗.经验,计算修炼等级经验(user.角色.人物修炼.法抗.等级,user.角色.人物修炼.法抗.上限),user.角色.人物修炼.法抗.上限},
  
         银子=user.角色.道具.货币.银子,
         当前经验=user.角色.当前经验}
         SendMessage(user.连接id, 2077,sj)
    end

function RoleControl:添加召唤兽修炼经验(user,数额)
  if user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].上限 <= user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].等级 then
    return 0
  else
    SendMessage(user.连接id, 9, "#dq/#w/ 你的召唤兽" .. user.角色.召唤兽修炼.当前 .. "修炼增加了" .. 数额 .. "点经验")
    user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].经验 = user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].经验 + 数额
    self.升级经验 = 计算修炼等级经验(user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].等级, user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].上限)
    if self.升级经验 <= user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].经验 then
      user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].等级 = user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].等级 + 1
      user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].经验 = user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].经验 - self.升级经验
      SendMessage(user.连接id, 9, "#dq/#w/ 你的召唤兽" .. user.角色.召唤兽修炼.当前 .. "修炼等级提升至" .. user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].等级 .. "级")
    end
  end
 end

function RoleControl:添加人物修炼经验(user,数额,类型)
  if 类型 ==nil then
    类型=user.角色.人物修炼.当前
  end
  if user.角色.人物修炼[类型].上限 <= user.角色.人物修炼[类型].等级 then
    return 0
  else
    SendMessage(user.连接id, 9, "#dq/#w/ 你的人物" .. 类型 .. "修炼增加了" .. 数额 .. "点经验")

    user.角色.人物修炼[类型].经验 = user.角色.人物修炼[类型].经验 + 数额
    self.升级经验 = 计算修炼等级经验(user.角色.人物修炼[类型].等级, user.角色.人物修炼[类型].上限)

    if self.升级经验 <= user.角色.人物修炼[类型].经验 then
      user.角色.人物修炼[类型].等级 = user.角色.人物修炼[类型].等级 + 1
      user.角色.人物修炼[类型].经验 = user.角色.人物修炼[类型].经验 - self.升级经验

      SendMessage(user.连接id, 9, "#dq/#w/ 你的人物" .. 类型 .. "修炼等级提升至" .. user.角色.人物修炼[类型].等级 .. "级")
    end
  end
 end
function RoleControl:设置当前修炼(user,人物修,召唤兽修)
  if 人物修 ~= "无" then
    user.角色.人物修炼.当前 = 人物修
    SendMessage(user.连接id, 7,"#Y/您当前的人物修炼类型已经更换为#G/" .. user.角色.人物修炼.当前 .. "#Y/修炼")
    SendMessage(user.连接id, 3030,"刷新")
  end
  if 召唤兽修 ~= "无" then
    user.角色.召唤兽修炼.当前 = 召唤兽修
    SendMessage(user.连接id, 7,"#Y/您当前的召唤兽修炼类型已经更换为#G/" .. user.角色.召唤兽修炼.当前 .. "#Y/修炼")
    SendMessage(user.连接id, 3030,"刷新")
  end


 end
