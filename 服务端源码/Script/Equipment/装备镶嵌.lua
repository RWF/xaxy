function EquipmentControl:镶嵌装备(id,格子1,格子2)

if UserData[id].物品[UserData[id].角色.道具.包裹[格子1]]==nil or  UserData[id].物品[UserData[id].角色.道具.包裹[格子2]]==nil then
      SendMessage(UserData[id].连接id,7,"#y/道具不存在,请重新刷新后尝试")
      return 0
end


 if UserData[id].物品[UserData[id].角色.道具.包裹[格子1]].类型=="宝石"  then
   self.宝石格子=格子1
   self.装备格子=格子2
  else
   self.宝石格子=格子2
   self.装备格子=格子1
  end
   self.道具id=UserData[id].角色.道具.包裹[self.装备格子]
   self.宝石id=UserData[id].角色.道具.包裹[self.宝石格子]
   if UserData[id].物品[UserData[id].角色.道具.包裹[self.宝石格子]].名称=="星辉石" then
      if UserData[id].物品[self.道具id].类型~="戒指" and UserData[id].物品[self.道具id].类型~="手镯" and UserData[id].物品[self.道具id].类型~="耳饰" and UserData[id].物品[self.道具id].类型~="佩饰"   then
       SendMessage(UserData[id].连接id,7,"#y/只有灵饰才可以使用星辉石强化")
       return 0
      elseif   UserData[id].物品[self.道具id].幻化等级>=UserData[id].物品[UserData[id].角色.道具.包裹[self.宝石格子]].等级 then
       SendMessage(UserData[id].连接id,7,"#y/该灵饰需要等级达到#r/"..UserData[id].物品[self.道具id].幻化等级.."#y/的星辉石才可强化")
       return 0
      elseif   UserData[id].物品[self.道具id].幻化等级>=15 then
       SendMessage(UserData[id].连接id,7,"#y/每件灵饰只能用星辉石强化15次")
       return 0
      else
        for n=1,#UserData[id].物品[self.道具id].幻化属性.附加 do
          self.临时类型=UserData[id].物品[self.道具id].幻化属性.附加[n].类型
          UserData[id].物品[self.道具id].幻化属性.附加[n].强化=UserData[id].物品[self.道具id].幻化属性.附加[n].强化+灵饰属性.基础[self.临时类型][0]
        end
         UserData[id].物品[self.道具id].幻化等级=UserData[id].物品[self.道具id].幻化等级+1
          SendMessage(UserData[id].连接id,7,"#y/使用星辉石强化灵饰成功")
          UserData[id].物品[self.宝石id]=0
          UserData[id].角色.道具.包裹[self.宝石格子]=nil
          ItemControl:索要打造道具(id, "包裹","镶嵌")
          SendMessage(UserData[id].连接id,3006,"66")
          return 0
      end
  end

  if UserData[id].物品[self.道具id].锻造数据==nil then
    SendMessage(UserData[id].连接id,7,"#y/只有装备和灵饰才可以镶嵌")
    return
  end

  if UserData[id].物品[self.道具id].锻造数据.等级 and UserData[id].物品[self.道具id].锻造数据.等级>=UserData[id].物品[UserData[id].角色.道具.包裹[self.宝石格子]].等级 then
     SendMessage(UserData[id].连接id,7,"#y/您的这块宝石无法镶嵌到此锻炼等级的装备上")
     ItemControl:索要打造道具(id, "包裹","镶嵌")
     return 0
  else
      self.装备部位=self:获取装备部位(id,self.道具id)
      self.宝石名称=UserData[id].物品[self.宝石id].名称
      self.道具类型=UserData[id].物品[self.道具id].类型
      local 宝石={
      太阳石={部位1="武器",部位2="头盔"},
      月亮石={部位1="铠甲",部位2="头盔"},
      红玛瑙={部位1="武器",部位2="头盔"},
      舍利子={部位1="铠甲",部位2="项链"},
      光芒石={部位1="铠甲",部位2="腰带"},
      黑宝石={部位1="腰带",部位2="鞋子"},
      红宝石={部位1="项链",部位2="鞋子"},

      黄宝石={部位1="项链",部位2="鞋子"},
      蓝宝石={部位1="项链",部位2="鞋子"},
      绿宝石={部位1="项链",部位2="鞋子"},
      神秘石={部位1="武器",部位2="鞋子"},
      速度精魄灵石={部位1="召唤兽装备",部位2="召唤兽装备"},
      气血精魄灵石={部位1="召唤兽装备",部位2="召唤兽装备"},
      灵力精魄灵石={部位1="召唤兽装备",部位2="召唤兽装备"},
      躲避精魄灵石={部位1="召唤兽装备",部位2="召唤兽装备"},
      防御精魄灵石={部位1="召唤兽装备",部位2="召唤兽装备"},
      伤害精魄灵石={部位1="召唤兽装备",部位2="召唤兽装备"}}
      if   宝石[self.宝石名称]==nil then 
         SendMessage(UserData[id].连接id,7,"#y/这种宝石无法镶嵌这样的装备")
         return 0
      elseif 宝石[self.宝石名称].部位1~=self.装备部位 and 宝石[self.宝石名称].部位2~=self.装备部位 then
        SendMessage(UserData[id].连接id,7,"#y/这种宝石无法镶嵌这样的装备")
        ItemControl:索要打造道具(id, "包裹","镶嵌")
        return 0
      else
        self.重复镶嵌=0
        for n=1,#UserData[id].物品[self.道具id].锻造数据 do
          if UserData[id].物品[self.道具id].锻造数据[n].名称~=self.宝石名称 then
            self.重复镶嵌=self.重复镶嵌+1
          end
        end
        if self.重复镶嵌==2 then
          SendMessage(UserData[id].连接id,7,"#y/一件装备只能镶嵌两个不同类型的宝石")
          ItemControl:索要打造道具(id, "包裹","镶嵌")
          return 0
        else
          if self.宝石名称=="红玛瑙" then
            self.添加文本="命中"
            if self.道具类型=="武器" then
            UserData[id].物品[self.道具id][self.添加文本]=UserData[id].物品[self.道具id][self.添加文本]+25
            self:添加锻造数据(id,self.道具id,self.添加文本,0,self.宝石名称)
            elseif self.道具类型=="头盔" then
            self:添加锻造数据(id,self.道具id,self.添加文本,25,self.宝石名称)
            end
          elseif self.宝石名称=="太阳石" then
            self.添加文本="伤害"
            if self.道具类型=="武器" then
            UserData[id].物品[self.道具id][self.添加文本]=UserData[id].物品[self.道具id][self.添加文本]+8
            self:添加锻造数据(id,self.道具id,self.添加文本,0,self.宝石名称)
            elseif self.道具类型=="头盔" then
            self:添加锻造数据(id,self.道具id,self.添加文本,8,self.宝石名称)
            end
          elseif self.宝石名称=="舍利子" then
            self.添加文本="灵力"
            if self.道具类型=="项链" then
            UserData[id].物品[self.道具id][self.添加文本]=UserData[id].物品[self.道具id][self.添加文本]+6
            self:添加锻造数据(id,self.道具id,self.添加文本,0,self.宝石名称)
            elseif self.道具类型=="衣服" then
            self:添加锻造数据(id,self.道具id,self.添加文本,6,self.宝石名称)
            end
          elseif self.宝石名称=="神秘石" then
            self.添加文本="躲闪"
            self:添加锻造数据(id,self.道具id,self.添加文本,20,self.宝石名称)
          elseif self.宝石名称=="黑宝石" then
            self.添加文本="速度"
            if self.道具类型=="鞋子" then
            self:添加锻造数据(id,self.道具id,self.添加文本,8,self.宝石名称)
            elseif self.道具类型=="腰带" then
            self:添加锻造数据(id,self.道具id,self.添加文本,8,self.宝石名称)
            end
          elseif self.宝石名称=="光芒石" then
            self.添加文本="气血"
            if self.道具类型=="腰带" then
            UserData[id].物品[self.道具id][self.添加文本]=UserData[id].物品[self.道具id][self.添加文本]+40
            self:添加锻造数据(id,self.道具id,self.添加文本,0,self.宝石名称)
            elseif self.道具类型=="衣服" then
            self:添加锻造数据(id,self.道具id,self.添加文本,40,self.宝石名称)
            end
          elseif self.宝石名称=="月亮石" then
            self.添加文本="防御"
            if self.道具类型=="头盔" then
            UserData[id].物品[self.道具id][self.添加文本]=UserData[id].物品[self.道具id][self.添加文本]+12
            self:添加锻造数据(id,self.道具id,self.添加文本,0,self.宝石名称)
            elseif self.道具类型=="衣服" then
            UserData[id].物品[self.道具id][self.添加文本]=UserData[id].物品[self.道具id][self.添加文本]+12
            self:添加锻造数据(id,self.道具id,self.添加文本,0,self.宝石名称)
            end
          elseif self.宝石名称=="速度精魄灵石" then
            self.添加文本="速度"
            if self.道具类型=="召唤兽装备" and UserData[id].物品[self.道具id].种类=="项圈" then
              self:添加锻造数据(id,self.道具id,self.添加文本,6,self.宝石名称)
            else
              SendMessage(UserData[id].连接id,7,"#y/这种宝石只能镶嵌在项圈上")
              ItemControl:索要打造道具(id, "包裹","镶嵌")
              return 0
            end
          elseif self.宝石名称=="气血精魄灵石" then
            self.添加文本="气血"
            if self.道具类型=="召唤兽装备" and UserData[id].物品[self.道具id].种类=="铠甲"then
            self:添加锻造数据(id,self.道具id,self.添加文本,30,self.宝石名称)
            else
              SendMessage(UserData[id].连接id,7,"#y/这种宝石只能镶嵌在铠甲上")
              ItemControl:索要打造道具(id, "包裹","镶嵌")
              return 0
            end
          elseif self.宝石名称=="灵力精魄灵石" then
            self.添加文本="灵力"
            if self.道具类型=="召唤兽装备" and UserData[id].物品[self.道具id].种类=="护腕"then
            self:添加锻造数据(id,self.道具id,self.添加文本,4,self.宝石名称)
           else
              SendMessage(UserData[id].连接id,7,"#y/这种宝石只能镶嵌在护腕上")
              ItemControl:索要打造道具(id, "包裹","镶嵌")
              return 0
            end
          elseif self.宝石名称=="躲避精魄灵石" then
            self.添加文本="躲闪"
            if self.道具类型=="召唤兽装备" and UserData[id].物品[self.道具id].种类=="项圈" then
              self:添加锻造数据(id,self.道具id,self.添加文本,20,self.宝石名称)
            else
              SendMessage(UserData[id].连接id,7,"#y/这种宝石只能镶嵌在项圈上")
              ItemControl:索要打造道具(id, "包裹","镶嵌")
              return 0
            end
          elseif self.宝石名称=="防御精魄灵石" then
            self.添加文本="防御"
            if self.道具类型=="召唤兽装备" and UserData[id].物品[self.道具id].种类=="铠甲"then
            self:添加锻造数据(id,self.道具id,self.添加文本,8,self.宝石名称)
            else
              SendMessage(UserData[id].连接id,7,"#y/这种宝石只能镶嵌在铠甲上")
              ItemControl:索要打造道具(id, "包裹","镶嵌")
              return 0
            end
          elseif self.宝石名称=="伤害精魄灵石" then
            self.添加文本="伤害"
            if self.道具类型=="召唤兽装备" and UserData[id].物品[self.道具id].种类=="护腕"then
            self:添加锻造数据(id,self.道具id,self.添加文本,10,self.宝石名称)
           else
              SendMessage(UserData[id].连接id,7,"#y/这种宝石只能镶嵌在护腕上")
              ItemControl:索要打造道具(id, "包裹","镶嵌")
              return 0
            end
          end
           SendMessage(UserData[id].连接id,7,"#y/锻造装备成功")
           UserData[id].物品[self.宝石id]=0
           UserData[id].角色.道具.包裹[self.宝石格子]=nil
           ItemControl:索要打造道具(id, "包裹","镶嵌")
           SendMessage(UserData[id].连接id,3006,"66")
        end
     end
 end
 end
function EquipmentControl:添加锻造数据(id,道具id,内容,数值,宝石)

 self.重复宝石=0

 for n=1,#UserData[id].物品[道具id].锻造数据 do

   if UserData[id].物品[道具id].锻造数据[n].名称==宝石 then

      self.重复宝石=n
     end
   end

 if self.重复宝石==0 then

   UserData[id].物品[道具id].锻造数据[#UserData[id].物品[道具id].锻造数据+1]={名称=宝石,数值=数值,类型=内容}

  else

   UserData[id].物品[道具id].锻造数据[self.重复宝石].数值=UserData[id].物品[道具id].锻造数据[self.重复宝石].数值+数值



   end

 if UserData[id].物品[道具id].锻造数据.等级==nil then

    UserData[id].物品[道具id].锻造数据.等级=1

  else

    UserData[id].物品[道具id].锻造数据.等级=UserData[id].物品[道具id].锻造数据.等级+1


   end


 end