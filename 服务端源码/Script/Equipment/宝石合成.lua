--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-10-11 21:12:35
--======================================================================--
function EquipmentControl:宝石合成(id, 格子1, 格子2)
  self.宝石id1 = UserData[id].角色.道具.包裹[格子1]
  self.宝石id2 = UserData[id].角色.道具.包裹[格子2]
  if UserData[id].物品[self.宝石id1]==nil or UserData[id].物品[self.宝石id2]==nil then
        SendMessage(UserData[id].连接id, 7, "#y/物品不存在")
         ItemControl:索要打造道具(id, "包裹","合成")
    return
  elseif  UserData[id].物品[self.宝石id1].名称 == "碎石锤" or UserData[id].物品[self.宝石id2].类型== "碎石锤" or UserData[id].物品[self.宝石id1].名称 == "超级碎石锤" or UserData[id].物品[self.宝石id2].类型== "超级碎石锤"  then
      if UserData[id].物品[self.宝石id1].名称~=UserData[id].物品[self.宝石id2].名称 then
          SendMessage(UserData[id].连接id, 7, "#y/只有相同的碎石锤才能合成")
          ItemControl:索要打造道具(id, "包裹","合成")
          return
      elseif UserData[id].物品[self.宝石id1].等级~=UserData[id].物品[self.宝石id2].等级 then
          SendMessage(UserData[id].连接id, 7, "#y/只有相同等级的碎石锤才能合成")
          ItemControl:索要打造道具(id, "包裹","合成")
          return
      else

          local 合成几率 = 100 - UserData[id].物品[self.宝石id1].等级 * 10+UserData[id].角色.剧情技能[1].等级*3
          UserData[id].物品[self.宝石id1].等级 = UserData[id].物品[self.宝石id1].等级 + 1
          UserData[id].物品[self.宝石id2] = nil
          UserData[id].角色.道具.包裹[格子2] = nil
          if math.random(100) >= 合成几率 then
            UserData[id].物品[self.宝石id1].等级 = UserData[id].物品[self.宝石id1].等级 - 1
            SendMessage(UserData[id].连接id, 7, "#y/合成失败，你损失了一颗" .. UserData[id].物品[self.宝石id1].名称)
          else 
            SendMessage(UserData[id].连接id, 7, "#y/你获得了一颗#r/" .. UserData[id].物品[self.宝石id1].等级 .. "#y/级" .. UserData[id].物品[self.宝石id1].名称)
          end
          
          SendMessage(UserData[id].连接id, 3006, "66")
          ItemControl:索要打造道具(id, "包裹","合成")

      end
    elseif  UserData[id].物品[self.宝石id1].名称 == "钟灵石" or UserData[id].物品[self.宝石id2].类型== "钟灵石"  then
      if UserData[id].物品[self.宝石id1].技能~=UserData[id].物品[self.宝石id2].技能 then
          SendMessage(UserData[id].连接id, 7, "#y/只有相同的特性的钟灵石才能合成")
          ItemControl:索要打造道具(id, "包裹","合成")
          return
      elseif UserData[id].物品[self.宝石id1].等级~=UserData[id].物品[self.宝石id2].等级 then
          SendMessage(UserData[id].连接id, 7, "#y/只有相同等级的钟灵石才能合成")
          ItemControl:索要打造道具(id, "包裹","合成")
          return
      else
          UserData[id].物品[self.宝石id1].等级 = UserData[id].物品[self.宝石id1].等级 + 1
          UserData[id].物品[self.宝石id2] = nil
          UserData[id].角色.道具.包裹[格子2] = nil
          SendMessage(UserData[id].连接id, 7, "#y/你获得了#r/" .. UserData[id].物品[self.宝石id1].等级 .. "#y/级" .. UserData[id].物品[self.宝石id1].名称)
          SendMessage(UserData[id].连接id, 3006, "66")
          ItemControl:索要打造道具(id, "包裹","合成")
      end
  elseif  UserData[id].物品[self.宝石id1].类型 ~= "宝石" or UserData[id].物品[self.宝石id2].类型 ~= "宝石" then
    SendMessage(UserData[id].连接id, 7, "#y/只有宝石才能合成")
     ItemControl:索要打造道具(id, "包裹","合成")
    return
  elseif  UserData[id].物品[self.宝石id1].名称 ~= UserData[id].物品[self.宝石id2].名称 then
    SendMessage(UserData[id].连接id, 7, "#y/只有相同的名称的宝石才能合成")
     ItemControl:索要打造道具(id, "包裹","合成")
    return
  elseif  UserData[id].物品[self.宝石id1].等级 ~= UserData[id].物品[self.宝石id2].等级 then
    SendMessage(UserData[id].连接id, 7, "#y/只有相同的等级的宝石才能合成")
     ItemControl:索要打造道具(id, "包裹","合成")
    return
  else
    if UserData[id].物品[self.宝石id1].等级 ==nil then
         SendMessage(UserData[id].连接id, 7, "#y/数据等级错误，请联系GM处理")
      return
    end
    local 合成几率 = 100 - UserData[id].物品[self.宝石id1].等级 * 5+UserData[id].角色.剧情技能[1].等级*3
    UserData[id].物品[self.宝石id1].等级 = UserData[id].物品[self.宝石id1].等级 + 1
    UserData[id].物品[self.宝石id2] = nil
    UserData[id].角色.道具.包裹[格子2] = nil
    if UserData[id].物品[self.宝石id1].等级 <= 5 then
      合成几率 = 100
    end
    if math.random(100) >= 合成几率 then
      UserData[id].物品[self.宝石id1].等级 = UserData[id].物品[self.宝石id1].等级 - 1
      SendMessage(UserData[id].连接id, 7, "#y/合成失败，你损失了一颗" .. UserData[id].物品[self.宝石id1].名称)
    else 
      SendMessage(UserData[id].连接id, 7, "#y/你获得了一颗#r/" .. UserData[id].物品[self.宝石id1].等级 .. "#y/级" .. UserData[id].物品[self.宝石id1].名称)
    end
    
    SendMessage(UserData[id].连接id, 3006, "66")
    ItemControl:索要打造道具(id, "包裹","合成")
  end
 end