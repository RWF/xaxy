function EquipmentControl:灵饰打造(id,格子1,格子2,类型)-------------------------------------------完成--

 self.道具id={
 [1]=UserData[id].角色.道具.包裹[格子1]
 ,[2]=UserData[id].角色.道具.包裹[格子2]
 }
 self.书铁id={}
 if UserData[id].物品[self.道具id[1]].名称==UserData[id].物品[self.道具id[2]].名称 then
   SendMessage(UserData[id].连接id,7,"#Y/这种材料无法使用")
   return 0
 elseif UserData[id].物品[self.道具id[2]].名称=="灵饰指南书" and UserData[id].物品[self.道具id[1]].等级<UserData[id].物品[self.道具id[2]].等级 then
   SendMessage(UserData[id].连接id,7,"#Y/你的这块元灵晶石等级太低了")
   return 0
 elseif UserData[id].物品[self.道具id[1]].名称=="灵饰指南书" and UserData[id].物品[self.道具id[1]].等级>UserData[id].物品[self.道具id[2]].等级 then
   SendMessage(UserData[id].连接id,7,"#Y/你的这块元灵晶石等级太低了")
   return 0
  else
   if UserData[id].物品[self.道具id[2]].名称=="灵饰指南书" then
      self.书铁id[1]=self.道具id[2]
      self.书铁id[2]=self.道具id[1]
    else
      self.书铁id[1]=self.道具id[1]
      self.书铁id[2]=self.道具id[1]
   end
     self.扣除金钱=UserData[id].物品[self.道具id[1]].等级*UserData[id].物品[self.道具id[1]].等级*100
     if UserData[id].物品[self.道具id[1]].等级>=60 and 类型~="强化打造" then
       SendMessage(UserData[id].连接id,7,"#Y/此种装备必须采用强化打造方式制造")
          return 0
       end
      if 类型=="强化打造" then
          self.扣除金钱=self.扣除金钱*3
          if RoleControl:GetTaskID(UserData[id],"打造")~=0 then
          SendMessage(UserData[id].连接id,7,"#Y/你已经有一个打造任务尚未完成")
          return 0
          elseif UserData[id].物品[self.道具id[1]].等级<60 then
          SendMessage(UserData[id].连接id,7,"#Y/60级以下的书铁无法进行强化打造")
          return 0
          end
       end
       self.消耗体力=math.floor(UserData[id].物品[self.道具id[1]].等级*2)
      if 银子检查(id,self.扣除金钱)==false then
      SendMessage(UserData[id].连接id,7,"#Y/你没那么多的银子")
      return 0
      elseif UserData[id].角色.当前体力<self.消耗体力 then
      SendMessage(UserData[id].连接id,7,"#Y/你没那么多的体力")
      return 0
      end
      UserData[id].角色.当前体力=UserData[id].角色.当前体力-self.消耗体力
     RoleControl:扣除银子(UserData[id],self.扣除金钱, "打造")
      self.临时鉴定=false
     if 类型=="强化打造" then
        self.随机强化石={"青龙石","玄武石","朱雀石","白虎石"}
        self.临时等级=UserData[id].物品[self.道具id[1]].等级
        self.临时数量=0
        if self.临时等级==60 then
         self.临时数量=14
        elseif self.临时等级==70 then
         self.临时数量=18
         elseif self.临时等级==80 then
         self.临时数量=24
        elseif self.临时等级==90 then
         self.临时数量=32
        elseif self.临时等级==100 then
         self.临时数量=40
        elseif self.临时等级==110 then
         self.临时数量=48
        elseif self.临时等级==120 then
          self.临时数量=60
        elseif self.临时等级==130 then
          self.临时数量=80
        elseif self.临时等级==140 then
          self.临时数量=100
        elseif self.临时等级==150 then
          self.临时数量=150
        elseif self.临时等级==160 then
          self.临时数量=200
         end
         local 临时id6=tonumber(UserData[id].id.."501")
        任务数据[临时id6]={
        类型="打造"
        ,NPC=30
        ,当前=0
        ,上次=0
        ,id=id
        ,起始=os.time()
        ,分类=UserData[id].物品[self.书铁id[1]].种类
        ,名称= 取灵饰名称(UserData[id].物品[self.书铁id[1]].种类,UserData[id].物品[self.书铁id[1]].等级)
        ,等级=UserData[id].物品[self.书铁id[1]].等级
        ,道具=self.随机强化石[math.random(4)]
        ,数量=self.临时数量
        ,灵饰=true
        }
       UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])]=临时id6
        UserData[id].物品[self.道具id[1]]=nil
        UserData[id].物品[self.道具id[2]]=nil
        UserData[id].角色.道具.包裹[格子2]=nil
        UserData[id].角色.道具.包裹[格子1]=nil
        SendMessage(UserData[id].连接id,7,"#Y/你获得了打造任务，请点击任务列表查看")
        TaskControl:刷新追踪任务信息(id)
        ItemControl:索要打造道具(id,"包裹","打造")
        SendMessage(UserData[id].连接id,3006,"66")
     else
        self.临时id=ItemControl:取道具编号(id)
        ItemControl:灵饰处理(id,self.临时id,UserData[id].物品[self.书铁id[1]].等级,0,UserData[id].物品[self.书铁id[1]].种类)


      local 制造装备={戒指={[60]="枫华戒",[80]="芙蓉戒",[100]="金麟绕",[120]="悦碧水",[140]="九曜光华",[150]="武神戒",[160]="太虚渺云"}
               ,手镯={[60]="香木镯",[80]="翡玉镯",[100]="墨影扣",[120]="花映月",[140]="金水菩提",[160]="浮雪幻音"}
               ,耳饰={[60]="翠叶环",[80]="明月珰",[100]="玉蝶翩",[120]="点星芒",[140]="凤羽流苏",[160]="焰云霞珠"}
               ,佩饰={[60]="芝兰佩",[80]="逸云佩",[100]="莲音玦",[120]="相思染",[140]="玄龙苍珀",[160]="碧海青天"}}

        UserData[id].物品[self.临时id].名称=制造装备[UserData[id].物品[self.书铁id[1]].种类][UserData[id].物品[self.书铁id[1]].等级]
        UserData[id].物品[self.临时id].制造者=UserData[id].角色.名称
        UserData[id].物品[self.临时id].等级=UserData[id].物品[self.书铁id[1]].等级
        UserData[id].物品[self.临时id].类型=UserData[id].物品[self.书铁id[1]].种类
        UserData[id].物品[self.临时id].耐久=500
        UserData[id].物品[self.道具id[1]]=nil
        UserData[id].物品[self.道具id[2]]=nil
        UserData[id].角色.道具.包裹[格子2]=nil
        UserData[id].角色.道具.包裹[格子1]=self.临时id
        SendMessage(UserData[id].连接id,7,"#Y/制造成功！")
        ItemControl:索要打造道具(id,"包裹","打造")
        SendMessage(UserData[id].连接id,3006,"66")
       end
     end
 end