function EquipmentControl:熔炼装备(id,格子1,格子2)
  local 数据={[1]={id=格子1},[2]={id=格子2}}
    if  UserData[id].物品[UserData[id].角色.道具.包裹[数据[1].id]].名称=="钨金" then
   self.宝石格子=数据[1].id
   self.装备格子=数据[2].id
   else
   self.宝石格子=数据[2].id
   self.装备格子=数据[1].id
   end
   if UserData[id].物品[UserData[id].角色.道具.包裹[self.宝石格子]].名称=="钨金" then
     self.道具id=UserData[id].角色.道具.包裹[self.装备格子]
     self.宝石id=UserData[id].角色.道具.包裹[self.宝石格子]
     self.宝石名称=UserData[id].物品[self.宝石id].名称
     self.道具类型=UserData[id].物品[self.道具id].类型
     self.装备等级 = UserData[id].物品[self.道具id].等级
      if UserData[id].物品[self.宝石id].等级 < self.装备等级  then
      SendMessage(UserData[id].连接id,7,"#y/当前钨金的等级小于装备等级,不能熔炼!请换一个高级的钨金")
      ItemControl:索要打造道具(id, "包裹","熔炼")
      return
      elseif  UserData[id].物品[self.道具id].耐久度 <=5 then
      SendMessage(UserData[id].连接id,7,"#y/当前耐久不够,不能熔炼!")
      ItemControl:索要打造道具(id, "包裹","熔炼")
      return
       elseif  UserData[id].物品[self.道具id].制造者 and  UserData[id].物品[self.道具id].制造者=="新手礼包" then
      SendMessage(UserData[id].连接id,7,"#y/新手装备,不能熔炼!")
      ItemControl:索要打造道具(id, "包裹","熔炼")
      return

      end

      UserData[id].物品[self.道具id].熔炼={}
     self.熔炼属性值=math.random(math.floor(self.装备等级/10*3))
     self.熔炼强化值 =0.1

     local 熔炼值={}

    if self.道具类型=="武器"  then
        if math.random(100) <=50 then
             熔炼值[#熔炼值+1]={Type="伤害",value=math.floor(self.装备等级/10*30+10)*self.熔炼强化值}
        end
        熔炼值[#熔炼值+1]={Type="命中",value=math.floor(self.装备等级/10*35+10)*self.熔炼强化值}
    elseif  self.道具类型=="头盔" then
       if  math.random(100) <=50 then
          熔炼值[#熔炼值+1]={Type="防御",value=math.floor(self.装备等级/10*5+5)*self.熔炼强化值}
       end
        熔炼值[#熔炼值+1]={Type="魔法",value=math.floor(self.装备等级/10*10+5)*self.熔炼强化值}
    elseif self.道具类型=="项链" then
        熔炼值[#熔炼值+1]={Type="灵力",value=math.floor(self.装备等级/10*12+5)*self.熔炼强化值}
    elseif  self.道具类型=="衣服" then
       熔炼值[#熔炼值+1]={Type="防御",value=math.floor(self.装备等级/10*15+25)*self.熔炼强化值}
       
    elseif  self.道具类型=="鞋子" then
        if math.random(100) <=50 then
                熔炼值[#熔炼值+1]={Type="敏捷",value=math.floor(self.装备等级/10*3+5)*self.熔炼强化值}
        end
        熔炼值[#熔炼值+1]={Type="防御",value=math.floor(self.装备等级/10*5+5)*self.熔炼强化值}
    elseif  self.道具类型=="腰带" then
        熔炼值[#熔炼值+1]={Type="防御",value=math.floor(self.装备等级/10*5+5)*self.熔炼强化值}
        if math.random(100) <=50 then
          熔炼值[#熔炼值+1]={Type="气血",value=math.floor(self.装备等级/10*20+10)*self.熔炼强化值}
        end
   end
   
   for i=1,#熔炼值 do
     UserData[id].物品[self.道具id].熔炼[i]={类型=熔炼值[i].Type}
      if math.random(100) <=50 then
          UserData[id].物品[self.道具id].熔炼[i].属性=-(math.random(熔炼值[i].value))
      else
        UserData[id].物品[self.道具id].熔炼[i].属性=math.random(熔炼值[i].value)
      end
   end
   if #UserData[id].物品[self.道具id].三围属性>0 then
       for i=1,#UserData[id].物品[self.道具id].三围属性 do
          if UserData[id].物品[self.道具id].三围属性[i].数值<self.熔炼属性值 and math.random(100) <=30 then
                UserData[id].物品[self.道具id].熔炼[#UserData[id].物品[self.道具id].熔炼+1]={类型=UserData[id].物品[self.道具id].三围属性[i].类型}
                if math.random(100) <=50 then
                   UserData[id].物品[self.道具id].熔炼[#UserData[id].物品[self.道具id].熔炼].属性=math.random(self.熔炼属性值-UserData[id].物品[self.道具id].三围属性[i].数值)
                else 
                   UserData[id].物品[self.道具id].熔炼[#UserData[id].物品[self.道具id].熔炼].属性=-(math.random(self.熔炼属性值-UserData[id].物品[self.道具id].三围属性[i].数值))
                end
               
          end
       end
   end



      SendMessage(UserData[id].连接id,7,"#y/使用钨金熔炼装备成功")
      UserData[id].物品[self.道具id].耐久度=UserData[id].物品[self.道具id].耐久度-5
      SendMessage(UserData[id].连接id,7,"#y/装备扣除5点耐久")
      UserData[id].物品[self.宝石id]=0
      UserData[id].角色.道具.包裹[self.宝石格子]=nil

      SendMessage(UserData[id].连接id,3006,"66")
  else
        SendMessage(UserData[id].连接id,7,"#y/只有钨金才能熔炼")
end
           ItemControl:索要打造道具(id, "包裹","熔炼")
end