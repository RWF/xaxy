-- @Author: 作者QQ381990860
-- @Date:   2021-09-07 00:54:26
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-09-09 17:21:52

 EquipmentControl = class()
local 武器类型={[1]="剑",[2]="扇",[3]="锤",[4]="枪",[5]="刀",[6]="斧",[7]="环",[8]="双",[9]="飘",[10]="棒",[11]="爪",[12]="鞭",[13]="伞",[14]="巨剑",[15]="宝珠",[16]="法杖",[17]="弓弩",[18]="灯笼"}
local 制造装备={
 武器={
 枪={[10]="曲尖枪",[20]="锯齿矛",[30]="乌金三叉戟",[40]="火焰枪",[50]="墨杆金钩",[60]="玄铁矛",[70]="金蛇信",[80]="丈八点钢矛"},
 斧={[10]="开山斧",[20]="双面斧",[30]="双弦钺",[40]="精钢禅钺",[50]="黄金钺",[60]="乌金鬼头镰",[70]="狂魔镰",[80]="恶龙之齿"},
 剑={[10]="铁齿剑",[20]="吴越剑",[30]="青锋剑",[40]="龙泉剑",[50]="黄金剑",[60]="游龙剑",[70]="北斗七星剑",[80]="碧玉剑"},
 双={[10]="镔铁双剑",[20]="龙凤双剑",[30]="竹节双剑",[40]="狼牙双剑",[50]="鱼骨双剑",[60]="赤焰双剑",[70]="墨玉双剑",[80]="梅花双剑"},
 飘={[10]="幻彩银纱",[20]="金丝彩带",[30]="无极丝",[40]="天蚕丝带",[50]="云龙绸带",[60]="七彩罗刹",[70]="缚神绫",[80]="九天仙绫"},
 爪={[10]="天狼爪",[20]="幽冥鬼爪",[30]="青龙牙",[40]="勾魂爪",[50]="玄冰刺",[60]="青刚刺",[70]="华光刺",[80]="龙鳞刺"},
 扇={[10]="铁骨扇",[20]="精钢扇",[30]="铁面扇",[40]="百折扇",[50]="劈水扇",[60]="神火扇",[70]="阴风扇",[80]="风云雷电"},
 棒={[10]="金丝魔棒",[20]="玉如意",[30]="点金棒",[40]="云龙棒",[50]="幽路引魂",[60]="满天星",[70]="水晶棒",[80]="日月光华"},
 锤={[10]="镔铁锤",[20]="八棱金瓜",[30]="狼牙锤",[40]="烈焰锤",[50]="破甲战锤",[60]="震天锤",[70]="巨灵神锤",[80]="天崩地裂"},
 鞭={[10]="牛筋鞭",[20]="乌龙鞭",[30]="钢结鞭",[40]="蛇骨鞭",[50]="玉竹金铃",[60]="青藤柳叶鞭",[70]="雷鸣嗜血鞭",[80]="混元金钩"},
 环={[10]="精钢日月圈",[20]="离情环",[30]="金刺轮",[40]="风火圈",[50]="赤炎环",[60]="蛇形月",[70]="子母双月",[80]="斜月狼牙"},
 刀={[10]="苗刀",[20]="夜魔弯刀",[30]="金背大砍刀",[40]="雁翅刀",[50]="破天宝刀",[60]="狼牙刀",[70]="龙鳞宝刀",[80]="黑炎魔刀"},
 巨剑={[10]="桃印铁刃",[20]="赭石巨剑",[30]="壁玉长铗",[40]="青铜古剑",[50]="金错巨刃",[60]="惊涛雪",[70]="醉浮生",[80]="沉戟天戊"},
 伞={[10]="红罗伞",[20]="紫竹伞",[30]="锦绣椎",[40]="幽兰帐",[50]="琳琅盖",[60]="孔雀羽",[70]="金刚伞",[80]="落梅伞"},
 灯笼={[10]="竹骨灯",[20]="红灯笼",[30]="鲤鱼灯",[40]="芙蓉花灯",[50]="如意宫灯",[60]="玲珑盏",[70]="玉兔盏",[80]="冰心盏"},
 宝珠={[10]="水晶珠",[20]="珍宝珠",[30]="翡翠珠",[40]="莲华珠",[50]="夜灵珠",[60]="如意宝珠",[70]="沧海明珠",[80]="无量玉璧"},
 法杖={[10]="红木杖",[20]="白椴杖",[30]="墨铁拐",[40]="玄铁牛角杖",[50]="鹰眼法杖",[60]="腾云杖",[70]="引魂杖",[80]="碧玺杖"},
 弓弩={[10]="铁胆弓",[20]="紫檀弓",[30]="宝雕长弓",[40]="錾金宝弓",[50]="玉腰弯弓",[60]="连珠神弓",[70]="游鱼戏珠",[80]="灵犀望月"},
 },
 项链={[10]="五色飞石",[20]="珍珠链",[30]="骷髅吊坠",[40]="江湖夜雨",[50]="高速之星",[60]="风月宝链",[70]="碧水青龙",[80]="万里卷云"},
 鞋子={[10]="牛皮靴",[20]="马靴",[30]="侠客履",[40]="神行靴",[50]="绿靴",[60]="追星踏月",[70]="九州履",[80]="万里追云履"},
 男衣={[10]="皮衣",[20]="鳞甲",[30]="锁子甲",[40]="紧身衣",[50]="钢甲",[60]="夜魔披风",[70]="龙骨甲",[80]="死亡斗篷"},
 女衣={[10]="丝绸长裙",[20]="五彩裙",[30]="龙鳞羽衣",[40]="天香披肩",[50]="金缕羽衣",[60]="霓裳羽衣",[70]="流云素裙",[80]="七宝天衣"},
 头盔={[10]="布帽",[20]="面具",[30]="纶巾",[40]="缨络丝带",[50]="羊角盔",[60]="水晶帽",[70]="乾坤帽",[80]="黑魔冠"},
 发钗={[10]="玉钗",[20]="梅花簪子",[30]="珍珠头带",[40]="凤头钗",[50]="媚狐头饰",[60]="玉女发冠",[70]="魔女发冠",[80]="七彩花环"},
 腰带={[10]="缎带",[20]="银腰带",[30]="水晶腰带",[40]="琥珀腰链",[50]="乱牙咬",[60]="双魂引",[70]="百窜云",[80]="圣王坠"},
 }

  local 随机防具组={
  [60]={鞋子="追星踏月",腰带="双魂引",项链="风月宝链", 头盔={头盔="水晶帽",发钗="玉女发冠"},衣服={男衣="夜魔披风",女衣="霓裳羽衣"}},
  [90]={鞋子="踏雪无痕",腰带="幻彩玉带",项链="七彩玲珑", 头盔={头盔="白玉龙冠",发钗="凤翅金翎"},衣服={男衣="神谕披风",女衣="飞天羽衣"}},
  [100]={鞋子="平步青云",腰带="珠翠玉环",项链="黄玉琉佩", 头盔={头盔="水晶夔帽",发钗="寒雉霜蚕"},衣服={男衣="珊瑚玉衣",女衣="穰花翠裙"}},
  [110]={鞋子="追云逐电",腰带="金蟾含珠",项链="鸾飞凤舞", 头盔={头盔="翡翠曜冠",发钗="曜月嵌星"},衣服={男衣="金蚕披风",女衣="金蚕丝裙"}},
  [120]={鞋子="乾坤天罡履",腰带="乾坤紫玉带",项链="衔珠金凤佩",头盔={头盔="金丝黑玉冠",发钗="郁金流苏簪"},衣服={男衣="乾坤护心甲",女衣="紫香金乌裙"}},
  [130]={鞋子="七星逐月靴",腰带="琉璃寒玉带",项链="七璜珠玉佩", 头盔={头盔="白玉琉璃冠",发钗="玉翼附蝉翎"},衣服={男衣="蝉翼金丝甲",女衣="碧霞彩云衣"}},
  [140]={鞋子="碧霞流云履",腰带="蝉翼鱼佩带",项链="鎏金点翠佩", 头盔={头盔="兽鬼珐琅面",发钗="鸾羽九凤冠"},衣服={男衣="金丝鱼鳞甲",女衣="金丝蝉翼衫"}},
  [150]={鞋子="金丝逐日履",腰带="磐龙凤翔带",项链="紫金碧玺佩", 头盔={头盔="紫金磐龙冠",发钗="金珰紫焰冠"},衣服={男衣="紫金磐龙甲",女衣="五彩凤翅衣"}},
  [160]={鞋子="辟尘分光履",腰带="紫霄云芒带",项链="落霞陨星坠", 头盔={头盔="浑天玄火盔",发钗="乾元鸣凤冕"},衣服={男衣="混元一气甲",女衣="鎏金浣月衣"}},}
   local 随机武器组={
      刀={{"狼牙刀"},{"冷月","屠龙","血刃"},{"晓风残月","偃月青龙","斩妖泣血"},{"业火三灾"},{"鸣鸿"}},
      剑={{"游龙剑"},{"鱼肠","倚天","湛卢"},{"魏武青虹","四法青云","灵犀神剑"},{"霜冷九州"},{"擒龙"}},
      鞭={{"青藤柳叶鞭"},{"百花","吹雪","龙筋",},{"仙人指路","血之刺藤","游龙惊鸿",},{"牧云清歌"},{"霜陨"}},
      锤={{"震天锤"},{"八卦","鬼牙","雷神",},{"鬼王蚀日","混元金锤","九瓣莲花",},{"狂澜碎岳"},{"碎寂"}},
      斧={{"乌金鬼头镰"},{"破魄","肃魂","无敌",},{"护法灭魔","五丁开山","元神禁锢",},{"碧血干戚"},{"裂天"}},
      环={{"蛇形月"},{"乾坤","如意","月光双环",},{"金玉双环","九天金线","别情离恨",},{"无关风月"},{"朝夕"}},
      棒={{"满天星"},{"沧海","红莲","盘龙",},{"降魔玉杵","墨玉骷髅","青藤玉树",},{"丝萝乔木"},{"醍醐"}},
      飘={{"七彩罗刹"},{"碧波","彩虹","流云",},{"此最相思","晃金仙绳","秋水落霞",},{"揽月摘星"},{"九霄"}},
      枪={{"玄铁矛"},{"暗夜","梨花","霹雳",},{"五虎断魂","飞龙在天","刑天之逆",},{"天龙破城"},{"弑皇"}},
      扇={{"神火扇"},{"玉龙","秋风","太极",},{"画龙点睛","逍遥江湖","秋水人家",},{"浩气长舒"},{"星瀚"}},
      双={{"赤焰双剑"},{"灵蛇","阴阳","月光双剑",},{"金龙双剪","连理双树","祖龙对剑",},{"紫电青霜"},{"浮犀"}},
      爪={{"青刚刺"},{"毒牙","撕天","胭脂",},{"贵霜之牙","九阴勾魂","雪蚕之刺",},{"忘川三途"},{"离钩"}},
      法杖={{"腾云杖"},{"业焰","玉辉","鹿鸣",},{"庄周梦蝶","凤翼流珠","雪蟒霜寒",},{"碧海潮生"},{"弦月"}},
      弓弩={{"连珠神弓"},{"非攻","幽篁","百鬼",},{"冥火薄天","龙鸣寒水","太极流光",},{"九霄风雷"},{"若木"}},
      宝珠={{"如意宝珠"},{"离火","飞星","月华",},{"回风舞雪","紫金葫芦","裂云啸日",},{"云雷万里"},{"赤明"}},
      巨剑={{"惊涛雪"},{"鸦九","昆吾","弦歌",},{"墨骨枯麟","腾蛇郁刃","秋水澄流",},{"百辟镇魂"},{"长息"}},
      灯笼={{"玲珑盏"},{"蟠龙","云鹤","风荷",},{"金风玉露","凰火燎原","月露清愁",},{"夭桃秾李"},{"荒尘"}},
      伞={{"孔雀羽"},{"鬼骨","云梦","枕霞",},{"碧火琉璃","雪羽穿云","月影星痕",},{"浮生归梦"},{"晴雪"}},
 }
require("Script/Equipment/宝石合成")
require("Script/Equipment/灵饰打造")
require("Script/Equipment/符石")
require("Script/Equipment/装备修理")
require("Script/Equipment/装备熔炼")
require("Script/Equipment/装备鉴定")
require("Script/Equipment/装备镶嵌")
require("Script/Equipment/打造")
function EquipmentControl:初始化(id)

end
function EquipmentControl:数据处理(id,序号,消息,消息1,类型,编号)

  if 序号==25001 then
      self:索要道具(id,"包裹",消息)
    elseif 序号==4006 then
      if 编号=="打造" then
        self:打造装备(id,消息,消息1,类型)
      elseif 编号 =="镶嵌" then
        self:镶嵌装备(id,消息,消息1)
      elseif 编号 == "修理" then
        self:修理装备(id,消息,消息1)
      elseif 编号 == "熔炼" then
        self:熔炼装备(id,消息,消息1)
      elseif 编号 == "合成" then
        self:宝石合成(id,消息,消息1)
      end

    elseif 序号==4007 then --符石开孔
      if 消息1== "开运" then
         self:符石开孔(id,消息)
      elseif 消息1== "修理" then
        self:装备修理(id,消息)
     elseif 消息1== "鉴定" then
        self:装备鉴定(id,消息)
      end
    elseif 序号==4008 then  --符石合成
    elseif 序号==4009 then
    self:添加符石(id,类型,消息)
    end
 end
function EquipmentControl:取灵饰礼包(id,等级)
local 临时类别={"耳饰","戒指","佩饰","手镯"}
local 制造装备={
戒指={[60]="枫华戒",[80]="芙蓉戒",[100]="金麟绕",[120]="悦碧水",[140]="九曜光华",[150]="武神戒",[160]="太虚渺云"},
手镯={[60]="香木镯",[80]="翡玉镯",[100]="墨影扣",[120]="花映月",[140]="金水菩提",[160]="浮雪幻音"},
耳饰={[60]="翠叶环",[80]="明月珰",[100]="玉蝶翩",[120]="点星芒",[140]="凤羽流苏",[160]="焰云霞珠"},
佩饰={[60]="芝兰佩",[80]="逸云佩",[100]="莲音玦",[120]="相思染",[140]="玄龙苍珀",[160]="碧海青天"}
}
 for n=1,4 do
    self.临时格子=RoleControl:取可用道具格子(UserData[id],"包裹")
    local 临时名称=临时类别[n]
   if self.临时格子==0 then
      SendMessage(UserData[id].连接id,7,"#Y/您当前的包裹空间已满，无法获得新道具")
   else
       self.临时id= ItemControl:取道具编号(id)
       ItemControl:灵饰处理(id,self.临时id,等级,0,临时名称)
        UserData[id].物品[self.临时id].制造者="灵饰礼包"
        UserData[id].物品[self.临时id].名称=制造装备[临时名称][等级]
        UserData[id].物品[self.临时id].等级=等级
        UserData[id].物品[self.临时id].类型=临时名称
        UserData[id].物品[self.临时id].耐久=500
        UserData[id].物品[self.临时id].鉴定=true
        UserData[id].角色.道具.包裹[self.临时格子]=self.临时id
          SendMessage(UserData[id].连接id, 3007,{格子= self.临时格子,数据=UserData[id].物品[self.临时id]})
        SendMessage(UserData[id].连接id,9,"#xt/#w/你得到了"..UserData[id].物品[self.临时id].名称)
   end

end

end
function EquipmentControl:生成装备(id,类型,名称,等级,上限,下限,鉴定,强化,制造者,附加,道具id,属性,限时,元身)---------完成--

 if 道具id==nil then
      self.临时id=ItemControl:取道具编号(id)
   else
     self.临时id=道具id
  end
 self.属性上限=上限+Config.装备强度
 self.属性下限=下限+Config.装备强度
 if 等级 == 160 then
   self.属性上限=self.属性上限+0.15
   self.属性下限=self.属性下限+0.15
 end
 self.附加=附加
 UserData[id].物品[self.临时id]={}
 UserData[id].物品[self.临时id].三围属性={}
 if 类型=="武器" then
     self.临时伤害=math.random(math.floor((等级/10*30+10)*self.属性下限),math.floor((等级/10*30+10)*self.属性上限))
     self.临时命中=math.random(math.floor((等级/10*35+10)*self.属性下限),math.floor((等级/10*35+10)*self.属性上限))
     UserData[id].物品[self.临时id].伤害=self.临时伤害
     UserData[id].物品[self.临时id].命中=self.临时命中

     if self.附加~=0 then
        if  元身 then 
           UserData[id].物品[self.临时id].三围属性={}
          for i=1,#元身 do

            UserData[id].物品[self.临时id].三围属性[i]={类型=元身[i][1],数值=math.random(10,50)} 
             if 元身[i][2] ==2 then
                UserData[id].物品[self.临时id].三围属性[i].数值 = -(UserData[id].物品[self.临时id].三围属性[i].数值)
             end
          end
        else 
         self:生成三围(id,self.临时id,等级,强化,属性)
        end
         self.特效范围={"无级别限制","简易","珍宝","易修理","永不磨损","精致","必中","绝杀"}
         self:生成特效(id,self.临时id,self.特效范围)
      end
    elseif 类型=="衣服" or 类型=="男衣" or 类型=="女衣" then
     self.临时伤害=math.random(math.floor((等级/10*15+25)*self.属性下限),math.floor((等级/10*15+25)*self.属性上限))
     UserData[id].物品[self.临时id].防御=self.临时伤害
     if self.附加~=0 then
        if  元身 then 
           UserData[id].物品[self.临时id].三围属性={}
          for i=1,#元身 do
               UserData[id].物品[self.临时id].三围属性[i]={类型=元身[i][1],数值=math.random(10,50)}
             if 元身[i][2] ==2 then
                UserData[id].物品[self.临时id].三围属性[i].数值 = -(UserData[id].物品[self.临时id].三围属性[i].数值)
             end
          end
        else 
         self:生成三围(id,self.临时id,等级,强化,属性)
        end
         self.特效范围={"无级别限制","简易","珍宝","易修理","永不磨损","精致","专注","神农","再生"}
         self:生成特效(id,self.临时id,self.特效范围)
          end
  elseif 类型=="项链" then
      self.临时伤害=math.random(math.floor((等级/10*12+5)*self.属性下限),math.floor((等级/10*12+5)*self.属性上限))
     UserData[id].物品[self.临时id].灵力=self.临时伤害
      if self.附加~=0 then
         self.特效范围={"无级别限制","简易","珍宝","易修理","永不磨损","精致","专注","再生"}
         self:生成特效(id,self.临时id,self.特效范围)
          end
    elseif 类型=="鞋子" then
     self.临时伤害=math.random(math.floor((等级/10*5+5)*self.属性下限),math.floor((等级/10*5+5)*self.属性上限))
     self.临时命中=math.random(math.floor((等级/10*3+5)*self.属性下限),math.floor((等级/10*3+5)*self.属性上限))
     UserData[id].物品[self.临时id].防御=self.临时伤害
     UserData[id].物品[self.临时id].敏捷=self.临时命中
       if self.附加~=0 then
         self.特效范围={"无级别限制","简易","珍宝","易修理","永不磨损","精致","专注","神农","再生"}
         self:生成特效(id,self.临时id,self.特效范围)
          end
    elseif 类型=="腰带" then
     self.临时伤害=math.random(math.floor((等级/10*5+5)*self.属性下限),math.floor((等级/10*5+5)*self.属性上限))
     self.临时命中=math.random(math.floor((等级/10*20+10)*self.属性下限),math.floor((等级/10*20+10)*self.属性上限))
     UserData[id].物品[self.临时id].防御=self.临时伤害
     UserData[id].物品[self.临时id].气血=self.临时命中

       if self.附加~=0 then
         self.特效范围={"无级别限制","简易","珍宝","易修理","永不磨损","精致","愤怒","暴怒","神农"}
         self:生成特效(id,self.临时id,self.特效范围)
          end
    elseif 类型=="头盔" or 类型=="发钗" then
     self.临时伤害=math.random(math.floor((等级/10*5+5)*self.属性下限),math.floor((等级/10*5+5)*self.属性上限))
     self.临时命中=math.random(math.floor((等级/10*10+5)*self.属性下限),math.floor((等级/10*10+5)*self.属性上限))
     UserData[id].物品[self.临时id].防御=self.临时伤害
     UserData[id].物品[self.临时id].魔法=self.临时命中
       if self.附加~=0 then
         self.特效范围={"无级别限制","简易","珍宝","易修理","永不磨损","精致","专注","神农","再生"}
         self:生成特效(id,self.临时id,self.特效范围)
          end
     end
 if self.附加~=0 then
     self:生成特技(id,self.临时id)
  end

 if 等级 == 160 then
          local 临时属性,临时数值
    
          local 副属性 ={"法术伤害" ,"法术防御" ,"治疗能力" ,"气血回复效果" ,"固定伤害" ,"法术伤害结果" ,"格挡值" ,"狂暴等级" ,"穿刺等级" ,"抗物理暴击等级" ,"法术暴击等级" ,"封印命中等级" ,"物理暴击等级" ,"抗法术暴击等级" ,"抵抗封印等级"}

          临时属性=副属性[math.random(1,#副属性)]
          临时数值=math.random(math.floor(等级*0.1),math.floor(等级*0.3))
     
          UserData[id].物品[self.临时id].特殊属性 = {类型=临时属性,属性=临时数值}
 end



 UserData[id].物品[self.临时id].五行=生成五行()
 UserData[id].物品[self.临时id].名称=名称
 UserData[id].物品[self.临时id].等级=等级
 UserData[id].物品[self.临时id].耐久度=500
 UserData[id].物品[self.临时id].类型=类型
 UserData[id].物品[self.临时id].锻造数据={}
 UserData[id].物品[self.临时id].符石={最大孔数=math.floor(等级/35+1),最小孔数=0}
 UserData[id].物品[self.临时id].鉴定=鉴定
 UserData[id].物品[self.临时id].叠加 = false
if  限时 then
 UserData[id].物品[self.临时id].Time = os.time()+限时
end
 if 制造者~=0 then UserData[id].物品[self.临时id].制造者=制造者 end
 if 强化~=nil then UserData[id].物品[self.临时id].强化=1  end
 return self.临时id
 end


 function EquipmentControl:改变造型(id,格子,类型)-------------------------------------------------------完成--
 
    if UserData[id].角色.道具["包裹"][格子] == nil or UserData[id].物品[UserData[id].角色.道具["包裹"][格子]] == nil then
        SendMessage(UserData[id].连接id,7,"#Y/物品不存在，请重新选择")
     return 
   end
       local TempItem=UserData[id].物品[UserData[id].角色.道具["包裹"][格子]]
       local  TempName =TempItem.名称
    if  WeaponModel[TempName]["武器类型"..类型]==nil then
              SendMessage(UserData[id].连接id,7,"#Y/"..TempName.."没有这个造型的拓印数据")
     return 
    end
     if  RoleControl:扣除仙玉(UserData[id],3000,"光武拓印") then
          TempItem.名称=WeaponModel[TempName]["武器类型"..类型]
         TempItem.专用 =id
         SendMessage(UserData[id].连接id,7,"#Y/"..TempName.."已经成功转换"..TempItem.名称)
    else
       SendMessage(UserData[id].连接id,7,"#Y/你没有足够的仙玉！")
      return 
    end
         
 end
 function EquipmentControl:改变双加(id,格子,文本1,文本2)-------------------------------------------------------完成--
 
    if UserData[id].角色.道具["包裹"][格子] == nil or UserData[id].物品[UserData[id].角色.道具["包裹"][格子]] == nil then
        SendMessage(UserData[id].连接id,7,"#Y/物品不存在，请重新选择")
     return 
    elseif UserData[id].物品[UserData[id].角色.道具["包裹"][格子]].三围属性 ==nil then
              SendMessage(UserData[id].连接id,7,"#Y/物品数据错误，请重新选择")
        return 
      elseif   文本1==文本2 then 
                      SendMessage(UserData[id].连接id,7,"#Y/不能选择相同属性")
        return 
   end
       local TempItem=UserData[id].物品[UserData[id].角色.道具["包裹"][格子]]
 
  
    if  RoleControl:扣除仙玉(UserData[id],1000,"改变双加") then
         
          for i=1,#TempItem.三围属性 do
             if i==1 then
             TempItem.三围属性[i].类型=文本1
             else 
              TempItem.三围属性[i].类型=文本2

             end
          end
        SendMessage(UserData[id].连接id,7,"#Y/你的"..TempItem.名称.."改变双加成功")
    else
       SendMessage(UserData[id].连接id,7,"#Y/你没有足够的仙玉！")
      return 
    end
         
 end

function EquipmentControl:生成特效(玩家id,id,范围)-------------------------------------------------------完成--
  if math.random(1000)<= 100 then
    UserData[玩家id].物品[id].特效={}
    UserData[玩家id].物品[id].特效[1]=范围[math.random(1,#范围)]
    if math.random(1000)<=50 then
      self.临时特效=范围[math.random(1,#范围)]
      if self.临时特效~=UserData[玩家id].物品[id].特效[1] then
        UserData[玩家id].物品[id].特效[2]=self.临时特效
        end
      end
   end
 end
function EquipmentControl:生成特技(玩家id,id)------------------------------------------------------------完成--
 self.特技名称={"破血狂攻","弱点击破","凝气诀","凝神诀","冥王爆杀","慈航普渡","诅咒之伤","气疗术","命疗术","气归术","命归术",
 "水清诀","冰清诀","玉清诀","晶清诀","四海升平","破血狂攻","罗汉金钟","圣灵之甲","魔兽之印","野兽之力","放下屠刀","太极护法","光辉之甲","流云诀","笑里藏刀","破碎无双"}
 if math.random(1000)<=30 then
     UserData[玩家id].物品[id].特技=self.特技名称[math.random(1,#self.特技名称)]
     end

 end
   function EquipmentControl:生成三围1(玩家id,id,等级,强化,属性)--------------------------------------------------完成--
 if math.random(100)<=100 or 属性 then
     self.随机类型=全局变量.基础属性[math.random(1,#全局变量.基础属性)]
     self.随机数值=等级/10*2+3
     self.随机数值=math.random(math.floor(self.随机数值*0.5),self.随机数值)
     if 强化~=0 then
         self.随机数值=self.随机数值+10
       end
     UserData[玩家id].物品[id].三围属性[1]={类型=self.随机类型,数值=self.随机数值}
     if math.random(100)<=100 or 属性 then
         self.随机类型=全局变量.基础属性[math.random(1,#全局变量.基础属性)]
         while self.随机类型==UserData[玩家id].物品[id].三围属性[1].类型 do

             self.随机类型=全局变量.基础属性[math.random(1,#全局变量.基础属性)]

             end
         self.随机数值=等级/10*2+3
         self.随机数值=math.random(math.floor(self.随机数值*0.5),self.随机数值)
         if 强化~=0 then
             self.随机数值=self.随机数值+10
           end
         UserData[玩家id].物品[id].三围属性[2]={类型=self.随机类型,数值=self.随机数值}
         end
     end
 end
 function EquipmentControl:生成装备1(id,类型,名称,等级,上限,下限,鉴定,强化,制造者,附加,道具id,属性)---------完成--
 
 if 道具id==nil then
      self.临时id=ItemControl:取道具编号(id)
   else
     self.临时id=道具id
  end
 self.属性上限=上限
 self.属性下限=下限
 if 等级 == 160 then
   self.属性上限=self.属性上限+0.15
   self.属性下限=self.属性下限+0.15
 end
 self.附加=附加
 UserData[id].物品[self.临时id]={}
 UserData[id].物品[self.临时id].三围属性={}
 if 类型=="武器" then
     self.临时伤害=math.random(math.floor((等级/10*30+10)*self.属性下限),math.floor((等级/10*30+10)*self.属性上限))
     self.临时命中=math.random(math.floor((等级/10*35+10)*self.属性下限),math.floor((等级/10*35+10)*self.属性上限))
     UserData[id].物品[self.临时id].伤害=self.临时伤害
     UserData[id].物品[self.临时id].命中=self.临时命中

     if self.附加~=0 then
        if  元身 then 
           UserData[id].物品[self.临时id].三围属性={}
          for i=1,#元身 do

            UserData[id].物品[self.临时id].三围属性[i]={类型=元身[i][1],数值=math.random(10,50)} 
             if 元身[i][2] ==2 then
                UserData[id].物品[self.临时id].三围属性[i].数值 = -(UserData[id].物品[self.临时id].三围属性[i].数值)
             end
          end
        else 
         self:生成三围1(id,self.临时id,等级,强化,属性)
        end
         self.特效范围={"无级别限制","简易","珍宝","易修理","永不磨损","精致","必中","绝杀"}
         self:生成特效(id,self.临时id,self.特效范围)
      end
    elseif 类型=="衣服" or 类型=="男衣" or 类型=="女衣" then
     self.临时伤害=math.random(math.floor((等级/10*15+25)*self.属性下限),math.floor((等级/10*15+25)*self.属性上限))
     UserData[id].物品[self.临时id].防御=self.临时伤害
     if self.附加~=0 then
        if  元身 then 
           UserData[id].物品[self.临时id].三围属性={}
          for i=1,#元身 do
               UserData[id].物品[self.临时id].三围属性[i]={类型=元身[i][1],数值=math.random(10,50)}
             if 元身[i][2] ==2 then
                UserData[id].物品[self.临时id].三围属性[i].数值 = -(UserData[id].物品[self.临时id].三围属性[i].数值)
             end
          end
        else 
         self:生成三围1(id,self.临时id,等级,强化,属性)
        end
         self.特效范围={"无级别限制","简易","珍宝","易修理","永不磨损","精致","专注","神农","再生"}
         self:生成特效(id,self.临时id,self.特效范围)
          end
  elseif 类型=="项链" then
      self.临时伤害=math.random(math.floor((等级/10*12+5)*self.属性下限),math.floor((等级/10*12+5)*self.属性上限))
     UserData[id].物品[self.临时id].灵力=self.临时伤害
      if self.附加~=0 then
         self.特效范围={"无级别限制","简易","珍宝","易修理","永不磨损","精致","专注","再生"}
         self:生成特效(id,self.临时id,self.特效范围)
          end
    elseif 类型=="鞋子" then
     self.临时伤害=math.random(math.floor((等级/10*5+5)*self.属性下限),math.floor((等级/10*5+5)*self.属性上限))
     self.临时命中=math.random(math.floor((等级/10*3+5)*self.属性下限),math.floor((等级/10*3+5)*self.属性上限))
     UserData[id].物品[self.临时id].防御=self.临时伤害
     UserData[id].物品[self.临时id].敏捷=self.临时命中
       if self.附加~=0 then
         self.特效范围={"无级别限制","简易","珍宝","易修理","永不磨损","精致","专注","神农","再生"}
         self:生成特效(id,self.临时id,self.特效范围)
          end
    elseif 类型=="腰带" then
     self.临时伤害=math.random(math.floor((等级/10*5+5)*self.属性下限),math.floor((等级/10*5+5)*self.属性上限))
     self.临时命中=math.random(math.floor((等级/10*20+10)*self.属性下限),math.floor((等级/10*20+10)*self.属性上限))
     UserData[id].物品[self.临时id].防御=self.临时伤害
     UserData[id].物品[self.临时id].气血=self.临时命中

       if self.附加~=0 then
         self.特效范围={"无级别限制","简易","珍宝","易修理","永不磨损","精致","愤怒","暴怒","神农"}
         self:生成特效(id,self.临时id,self.特效范围)
          end
    elseif 类型=="头盔" or 类型=="发钗" then
     self.临时伤害=math.random(math.floor((等级/10*5+5)*self.属性下限),math.floor((等级/10*5+5)*self.属性上限))
     self.临时命中=math.random(math.floor((等级/10*10+5)*self.属性下限),math.floor((等级/10*10+5)*self.属性上限))
     UserData[id].物品[self.临时id].防御=self.临时伤害
     UserData[id].物品[self.临时id].魔法=self.临时命中
       if self.附加~=0 then
         self.特效范围={"无级别限制","简易","珍宝","易修理","永不磨损","精致","专注","神农","再生"}
         self:生成特效(id,self.临时id,self.特效范围)
          end
     end
 if self.附加~=0 then
     self:生成特技(id,self.临时id)
  end

 if 等级 == 160 then
          local 临时属性,临时数值
    
          local 副属性 ={"法术伤害" ,"法术防御" ,"治疗能力" ,"气血回复效果" ,"固定伤害" ,"法术伤害结果" ,"格挡值" ,"狂暴等级" ,"穿刺等级" ,"抗物理暴击等级" ,"法术暴击等级" ,"封印命中等级" ,"物理暴击等级" ,"抗法术暴击等级" ,"抵抗封印等级"}

          临时属性=副属性[math.random(1,#副属性)]
          临时数值=math.random(math.floor(等级*0.1),math.floor(等级*0.3))
     
          UserData[id].物品[self.临时id].特殊属性 = {类型=临时属性,属性=临时数值}
 end



 UserData[id].物品[self.临时id].五行=生成五行()
 UserData[id].物品[self.临时id].名称=名称
 UserData[id].物品[self.临时id].等级=等级
 UserData[id].物品[self.临时id].耐久度=500
 UserData[id].物品[self.临时id].类型=类型
 UserData[id].物品[self.临时id].锻造数据={}
 UserData[id].物品[self.临时id].符石={最大孔数=math.floor(等级/35+1),最小孔数=0}
 UserData[id].物品[self.临时id].鉴定=鉴定
 UserData[id].物品[self.临时id].叠加 = false
if  限时 then
 UserData[id].物品[self.临时id].Time = os.time()+限时
end
 if 制造者~=0 then UserData[id].物品[self.临时id].制造者=制造者 end
 if 强化~=nil then UserData[id].物品[self.临时id].强化=1  end
 return self.临时id
 end

function EquipmentControl:生成三围(玩家id,id,等级,强化,属性)--------------------------------------------------完成--
 if math.random(100)<=50 or 属性 then
     self.随机类型=全局变量.基础属性[math.random(1,#全局变量.基础属性)]
     self.随机数值=等级/10*2+3
     self.随机数值=math.random(math.floor(self.随机数值*0.5),self.随机数值)
     if 强化~=0 then
         self.随机数值=self.随机数值+10
       end
     UserData[玩家id].物品[id].三围属性[1]={类型=self.随机类型,数值=self.随机数值}
     if math.random(100)<=50 or 属性 then
         self.随机类型=全局变量.基础属性[math.random(1,#全局变量.基础属性)]
         while self.随机类型==UserData[玩家id].物品[id].三围属性[1].类型 do

             self.随机类型=全局变量.基础属性[math.random(1,#全局变量.基础属性)]

             end
         self.随机数值=等级/10*2+3
         self.随机数值=math.random(math.floor(self.随机数值*0.5),self.随机数值)
         if 强化~=0 then
             self.随机数值=self.随机数值+10
           end
         UserData[玩家id].物品[id].三围属性[2]={类型=self.随机类型,数值=self.随机数值}
         end
     end
 end
function EquipmentControl:获取装备部位(id,格子)
 if UserData[id].物品[格子].类型=="衣服"  then
   return "铠甲"
  elseif UserData[id].物品[格子].类型=="头盔" or UserData[id].物品[格子].类型=="发钗" then
     return "头盔"
  else
   return UserData[id].物品[格子].类型
   end
 end
function EquipmentControl:获取名称(分类,类型,等级,方式)
  if 分类=="武器"  and 等级>=90 then
    if 等级>=90 and 等级<120 then
      return 随机武器组[类型][2][math.random(3)]
    elseif 等级>=120 and 等级<150 then
      if 方式==1 then
        return 随机武器组[类型][2][math.random(3)]
      else
        return 随机武器组[类型][3][math.random(3)]
      end
    elseif 等级==150 then
      if 方式==1 then
        return 随机武器组[类型][2][math.random(3)]
      else
        return 随机武器组[类型][4][1]
       end
    elseif 等级==160 then
      if 方式==1 then
        return 随机武器组[类型][2][math.random(3)]
      else
        return 随机武器组[类型][5][1]
       end
    end
  elseif (分类=="衣服" or 分类=="头盔") and 等级>=90  then
    if 方式==2  then
        return 随机防具组[等级][分类][类型]
    else
        return 随机防具组[90][分类][类型]
    end
  else
     if 等级>=90 then
       if 方式==2  then
          return 随机防具组[等级][分类]
       else
          return 随机防具组[90][分类]
       end
     else
         if 分类 == "武器" then
            return 制造装备.武器[类型][等级]
         else
            return 制造装备[分类][等级]
         end
      end
  end
 end
function EquipmentControl:分类判断(分类)
 if 分类=="男衣" or 分类=="女衣" then
     return "衣服"
    elseif 分类=="头盔" or 分类=="发钗" then
     return "头盔"
    elseif 分类=="鞋子"then
     return "鞋子"
    elseif 分类=="项链"then
     return "项链"
    elseif 分类=="腰带"then
     return "腰带"
    else
     return "武器"
   end
  end
function EquipmentControl:索要道具(id,内容,类型)
 self.发送信息={}
 for n=1,20 do
    if UserData[id].角色.道具[内容][n]~=nil then
      self.发送信息[n]=UserData[id].物品[UserData[id].角色.道具[内容][n]]
      end
   end
 self.发送信息.类型=内容
 self.发送信息.类别=类型
 self.发送信息.银两=UserData[id].角色.道具.货币.银子
 self.发送信息.体力=UserData[id].角色.当前体力
 SendMessage(UserData[id].连接id,3005,self.发送信息)
 end



 function EquipmentControl:定制灵饰(id,等级,类型,主类型,主属性,超级简易,附加属性,限时)-----------------完成--
   等级 =等级+0
   主属性=主属性+0
   local 临时格子=RoleControl:取可用道具格子(UserData[id],"包裹")
   if 临时格子==0 then
       发送网关消息(string.format("%d该玩家当前的包裹空间已满，无法获得新道具",id))
       return
   else
        local 幻化id=ItemControl:取道具编号(id)
        local  名称 =取灵饰名称(类型,等级)
        UserData[id].物品[幻化id]=
        {
         幻化属性={ 附加={},基础={类型=主类型,数值=主属性,强化=0},},
         名称= 名称,
         鉴定=true,
         制造者="GM制造",
         等级=等级,
         类型=类型,
         耐久=500,
         幻化等级=0
        }
        if 超级简易=="1" then

            UserData[id].物品[幻化id].特效={[1]="超级简易"}

        end

        if 附加属性~="=@=@=@="  then
              local ls=分割文本(附加属性,"@")
              for k,v in pairs(ls) do
                local ks=分割文本(v,"=")
                if ks[2]~="" then
                  UserData[id].物品[幻化id].幻化属性.附加[k]={类型=ks[1],数值=ks[2]+0,强化=0}
                end
              end
        end
        if 限时 then
          UserData[id].物品[幻化id].Time=限时+os.time()
          UserData[id].物品[幻化id].专用=id
        end
        UserData[id].角色.道具.包裹[临时格子]=幻化id
         SendMessage(UserData[id].连接id,9,"#xt/#w/你得到了"..名称)
        SendMessage(UserData[id].连接id,7,"#y/你得到了"..名称)
        SendMessage(UserData[id].连接id,3006,"66")
    end

 end

function EquipmentControl:定制召唤兽装备(id,等级,类型,主属性,主数值,附加类型,附加属性,三围)

   等级 =等级+0
   主数值=主数值+0
   local 临时格子=RoleControl:取可用道具格子(UserData[id],"包裹")
   if 临时格子==0 then
       发送网关消息(string.format("%d该玩家当前的包裹空间已满，无法获得新道具",id))
       return
   else
        self.临时名称=self:取bb装备(等级,类型)
        local 幻化id=ItemControl:取道具编号(id)
        UserData[id].物品[幻化id]=
        {
          等级 =等级,
          主属性={名称=主属性,数值=主数值},
          类型="召唤兽装备",
          种类=类型,
          名称=self.临时名称,
          耐久度=500,
          制造者="GM强化打造",
          锻造数据 ={},
        }

      if 附加属性~="" then
        UserData[id].物品[幻化id].附加属性={名称=附加类型,数值=附加属性}
      end
      if 三围~="=@="  then
          self.临时三围=分割文本(三围,"@")
          self.单加属性=分割文本(self.临时三围[1],"=")

          if self.单加属性[2]~="" then
       
              UserData[id].物品[幻化id].单加={名称=self.单加属性[1],数值=self.单加属性[2]+0}
          end

          if self.临时三围[2]~= "=" then
           self.单加属性=分割文本(self.临时三围[2],"=")
        
           if self.单加属性[2]~="" then
             UserData[id].物品[幻化id].双加={名称=self.单加属性[1],数值=self.单加属性[2]+0}
           end
          end
      end
  

        UserData[id].角色.道具.包裹[临时格子]=幻化id
         SendMessage(UserData[id].连接id,9,"#xt/#w/你得到了"..self.临时名称)
        SendMessage(UserData[id].连接id,7,"#y/你得到了"..self.临时名称)
        SendMessage(UserData[id].连接id,3006,"66")
    end
 end



function EquipmentControl:定制装备(id,等级,类型,上限,下限,特效,特技,套装类型,套装名称,三围)
 等级 =等级+0
 上限=上限+0
 下限=下限+0
  local 装备礼包数据={}
   local 武器编号
  if 等级< 90 then
    武器编号= 1
  elseif 等级>= 90 and 等级<= 110 then
    武器编号= 2
  elseif 等级>= 120 and 等级<= 140 then
    武器编号 = 3
  elseif 等级== 150  then
    武器编号 = 4
  elseif 等级== 160 then
    武器编号 = 5
  end
  local cywq =取属性(UserData[id].角色.造型)
 if math.random(100)<=50 then
   self.临时武器=cywq.武器[1]
  else
   self.临时武器=cywq.武器[2]
   end

 if UserData[id].角色.性别=="男" then
   self.临时头盔="头盔"
   self.临时衣服="男衣"
  else
   self.临时头盔="发钗"
   self.临时衣服="女衣"
   end

  装备礼包数据={
  武器={名称=随机武器组[self.临时武器][武器编号][math.random(1,#随机武器组[self.临时武器][武器编号])]}
 ,鞋子={名称=随机防具组[等级].鞋子}
 ,腰带={名称=随机防具组[等级].腰带}
 ,项链={名称=随机防具组[等级].项链}
 ,衣服={名称=随机防具组[等级].衣服[self.临时衣服]}
 ,头盔={名称=随机防具组[等级].头盔[self.临时头盔]}
 }
    self.临时格子=RoleControl:取可用道具格子(UserData[id],"包裹")
   if self.临时格子==0 then
       发送网关消息(string.format("%d该玩家当前的包裹空间已满，无法获得新道具",id))
   else
        self.临时id=self:生成装备(id,类型,装备礼包数据[类型].名称,等级,下限,上限,true,true,"GM定制",0,nil)
        RoleControl:添加消费日志(UserData[id],"GM定制:"..self.临时id)
        UserData[id].角色.道具.包裹[self.临时格子]=self.临时id

       if 特效 ~="" then
        UserData[id].物品[self.临时id].特效={[1]=特效}
       end
       if 特技 ~=""then
         UserData[id].物品[self.临时id].特技=特技
       end
       if 套装类型 ~=""and 套装名称~="" then
              if 套装类型 == "1" then
              UserData[id].物品[self.临时id].套装={类型="追加法术",名称=套装名称}
              elseif 套装类型 == "2" then
              UserData[id].物品[self.临时id].套装={类型="附加状态",名称=套装名称}
             elseif 套装类型 == "3" then
              UserData[id].物品[self.临时id].套装={类型="变身术",名称=套装名称}
              end
       end
      if 三围~="=@="  then
          self.临时三围=分割文本(三围,"@")
          self.单加属性=分割文本(self.临时三围[1],"=")
          if self.单加属性[2]~="" then
              UserData[id].物品[self.临时id].三围属性={[1]={类型=self.单加属性[1],数值=self.单加属性[2]+0}}
          end

          if self.临时三围[2]~= "=" then
          self.单加属性=分割文本(self.临时三围[2],"=")
           if self.单加属性[2]~="" then
             UserData[id].物品[self.临时id].三围属性[2]={类型=self.单加属性[1],数值=self.单加属性[2]+0}
           end
          end
      end

        __S服务:发送(连接id,990,1,UserData[id].角色.名称.."添加"..等级.."级的"..装备礼包数据[类型].名称.."成功")

        SendMessage(UserData[id].连接id,9,"#xt/#w/你得到了"..装备礼包数据[类型].名称)
        SendMessage(UserData[id].连接id,7,"#y/你得到了"..装备礼包数据[类型].名称)
    end

 end

function EquipmentControl:取单件装备礼包(id,等级)
  local 装备礼包数据={}
   local 武器编号
  if 等级< 90 then
    武器编号= 1
  elseif 等级>= 90 and 等级<= 110 then
    武器编号= 2
  elseif 等级>= 120 and 等级<= 140 then
    武器编号 = 3
  elseif 等级== 150  then
    武器编号 = 4
  elseif 等级== 160 then
    武器编号 = 5
  end
  local cywq =取属性(UserData[id].角色.造型)
 if math.random(100)<=50 then
   self.临时武器=cywq.武器[1]
  else
   self.临时武器=cywq.武器[2]
   end

 if UserData[id].角色.性别=="男" then
   self.临时头盔="头盔"
   self.临时衣服="男衣"
  else
   self.临时头盔="发钗"
   self.临时衣服="女衣"
   end

  装备礼包数据={
  {类型="武器",名称=随机武器组[self.临时武器][武器编号][math.random(1,#随机武器组[self.临时武器][武器编号])]}
 ,{类型="鞋子",名称=随机防具组[等级].鞋子}
 ,{类型="腰带",名称=随机防具组[等级].腰带}
 ,{类型="项链",名称=随机防具组[等级].项链}
 ,{类型="衣服",名称=随机防具组[等级].衣服[self.临时衣服]}
 ,{类型="头盔",名称=随机防具组[等级].头盔[self.临时头盔]}
 }
    self.临时格子=RoleControl:取可用道具格子(UserData[id],"包裹")
    local 随机 =math.random(6)
   if self.临时格子==0 then
      SendMessage(UserData[id].连接id,7,"#Y/您当前的包裹空间已满，无法获得新道具")
   else
        self.临时id=self:生成装备(id,装备礼包数据[随机].类型,装备礼包数据[随机].名称,等级,1.25,1.4,true,true,"系统制造",nil,nil)

        RoleControl:添加消费日志(UserData[id],"装备礼包:"..self.临时id)
        UserData[id].角色.道具.包裹[self.临时格子]=self.临时id
        self:添加无级别特效(id,self.临时id)
        SendMessage(UserData[id].连接id,9,"#xt/#w/你得到了"..装备礼包数据[随机].名称)
    end

 end

function EquipmentControl:取装备礼包(id,等级,新手,属性,专用,限时)
  local 装备礼包数据={}
   local 武器编号
  if 等级< 90 then
    武器编号= 1
  elseif 等级>= 90 and 等级<= 110 then
    武器编号= 2
  elseif 等级>= 120 and 等级<= 140 then
    武器编号 = 3
  elseif 等级== 150  then
    武器编号 = 4
  elseif 等级== 160 then
    武器编号 = 5
  end
  local cywq =取属性(UserData[id].角色.造型)
 if math.random(100)<=50 then
   self.临时武器=cywq.武器[1]
  else
   self.临时武器=cywq.武器[2]
   end

 if UserData[id].角色.性别=="男" then
   self.临时头盔="头盔"
   self.临时衣服="男衣"
  else
   self.临时头盔="发钗"
   self.临时衣服="女衣"
   end

  装备礼包数据={
  {类型="武器",名称=随机武器组[self.临时武器][武器编号][math.random(1,#随机武器组[self.临时武器][武器编号])]}
 ,{类型="鞋子",名称=随机防具组[等级].鞋子}
 ,{类型="腰带",名称=随机防具组[等级].腰带}
 ,{类型="项链",名称=随机防具组[等级].项链}
 ,{类型="衣服",名称=随机防具组[等级].衣服[self.临时衣服]}
 ,{类型="头盔",名称=随机防具组[等级].头盔[self.临时头盔]}
 }
  for n=1,6 do
    self.临时格子=RoleControl:取可用道具格子(UserData[id],"包裹")
   if self.临时格子==0 then
      SendMessage(UserData[id].连接id,7,"#Y/您当前的包裹空间已满，无法获得新道具")
   else
       if  新手==nil and 专用  then

       self.临时id=self:生成装备(id,装备礼包数据[n].类型,装备礼包数据[n].名称,等级,1.05,1.2,true,1,"商城礼包",nil,nil,属性,限时)
       --self.临时id=self:生成装备(id,装备礼包数据[n].类型,装备礼包数据[n].名称,等级,1.25,1.4,true,1,"商城礼包",nil,nil,属性,限时)
       elseif 专用==nil then
       self.临时id=self:生成装备(id,装备礼包数据[n].类型,装备礼包数据[n].名称,等级,1.4,1.4,true,1,"GM累冲奖励",nil,nil,属性,限时)
       else
        --self.临时id=self:生成装备(装备礼包数据[n].类型,装备礼包数据[n].名称,等级,1.05,1.2,true,nil,"商城礼包",nil,nil,属性,限时)
        self.临时id=self:生成装备(id,装备礼包数据[n].类型,装备礼包数据[n].名称,等级,0.9,1.0,nil,nil,"新手礼包",0,nil,属性,限时)
       end
       if 专用 then
          UserData[id].物品[self.临时id].专用=id
       end

        RoleControl:添加消费日志(UserData[id],"装备礼包:"..self.临时id)
        UserData[id].角色.道具.包裹[self.临时格子]=self.临时id
        self:添加无级别特效(id,self.临时id)
        SendMessage(UserData[id].连接id,9,"#xt/#w/你得到了"..装备礼包数据[n].名称)
    end
   end
 end
 function EquipmentControl:添加无级别特效(id,道具id)
  if UserData[id].物品[道具id].特效==nil then
   UserData[id].物品[道具id].特效={}
   UserData[id].物品[道具id].特效[1]="无级别限制"
  else
    self.无级别重复=false
    for n=1,#UserData[id].物品[道具id].特效 do
     if UserData[id].物品[道具id].特效[n]=="无级别限制" then
        self.无级别重复=true
       end
      end
     if self.无级别重复==false then
     UserData[id].物品[道具id].特效[#UserData[id].物品[道具id].特效+1]="无级别限制"
     end
    end
 end
 function EquipmentControl:添加永不磨损特效(id,道具id)
  if UserData[id].物品[道具id].特效==nil then
   UserData[id].物品[道具id].特效={}
   UserData[id].物品[道具id].特效[1]="永不磨损"
  else
    self.无级别重复=false
    for n=1,#UserData[id].物品[道具id].特效 do
     if UserData[id].物品[道具id].特效[n]=="永不磨损" then
        self.无级别重复=true
       end
      end
     if self.无级别重复==false then
     UserData[id].物品[道具id].特效[#UserData[id].物品[道具id].特效+1]="永不磨损"
     end
    end
 end
  function EquipmentControl:添加愤怒特效(id,道具id)
  if UserData[id].物品[道具id].特效==nil then
   UserData[id].物品[道具id].特效={}
   UserData[id].物品[道具id].特效[1]="愤怒"
  else
    self.无级别重复=false
    for n=1,#UserData[id].物品[道具id].特效 do
     if UserData[id].物品[道具id].特效[n]=="愤怒" then
        self.无级别重复=true
       end
      end
     if self.无级别重复==false then
     UserData[id].物品[道具id].特效[#UserData[id].物品[道具id].特效+1]="愤怒"
     end
    end
 end
function EquipmentControl:取随机装备名称(等级)
  if math.random(100)<=50 then
      self.临时类型=武器类型[math.random(1,#武器类型)]
      self.临时名称=制造装备.武器[self.临时类型][等级]
      self.临时类型="武器"
    else
      self.临时随机数=math.random(5)
      if self.临时随机数==1 then
         self.临时类型="鞋子"
         self.临时名称=制造装备[self.临时类型][等级]
      elseif self.临时随机数==2 then
        if math.random(100)<=50 then
          self.临时类型="男衣"
        else
          self.临时类型="女衣"
          end
        self.临时名称=制造装备[self.临时类型][等级]
        self.临时类型="衣服"
      elseif self.临时随机数==3 then
        if math.random(100)<=50 then
          self.临时类型="头盔"
        else
          self.临时类型="发钗"
          end
        self.临时名称=制造装备[self.临时类型][等级]
        self.临时类型="头盔"
        elseif self.临时随机数==4 then
        self.临时类型="项链"
        self.临时名称=制造装备[self.临时类型][等级]
         elseif self.临时随机数==5 then
        self.临时类型="腰带"
        self.临时名称=制造装备[self.临时类型][等级]
        end
     end
  self.临时数据={名称=self.临时名称,类型=self.临时类型}
  return self.临时数据
 end
function EquipmentControl:设置传说物品(id)
  --修炼
  local 任务id=RoleControl:GetTaskID(UserData[id],"任务链")
  if 任务id~=0 and 任务数据[任务id]~=nil and 任务数据[任务id].传说==nil then
    任务数据[任务id].传说=1
    if 任务数据[任务id].等级==nil then
      ItemControl:GiveItem(id,任务数据[任务id].道具)
    else
      self:取传说装备(id,任务数据[任务id].等级,任务数据[任务id].道具)
    end

  end
end
function EquipmentControl:取传说装备(id,lv,名称)
  self.临时格子=RoleControl:取可用道具格子(UserData[id],"包裹")
 if self.临时格子==0 then
    SendMessage(UserData[id].连接id,7,"#y/您当前的包裹空间已满，无法获得新道具")
  else
    local lslex = self:分类判断(ItemData[名称].类型)
    UserData[id].角色.道具.包裹[self.临时格子]=self:生成装备(id,lslex,名称,lv,1.15,1,nil,nil,0,nil,ItemControl:取道具编号(id))
    SendMessage(UserData[id].连接id,9,"#xt/#w/你获得了传说中的#R/"..名称)
   end
end

function EquipmentControl:取随机装备(id,lv,jd)
 if lv >8 then lv =8 end
 local lslx,lsmc
 if math.random(100)<=50 then --武器
   lslx=武器类型[math.random(1,#武器类型)]
   lsmc=制造装备.武器[lslx][lv*10]
   lslx="武器"
 else
    self.临时参数=math.random(100)
    if self.临时参数<=14 then
      lslx="项链"
      lsmc=制造装备[lslx][lv*10]
   elseif self.临时参数<=28 then
      lslx="男衣"
      lsmc=制造装备[lslx][lv*10]
      lslx="衣服"
   elseif self.临时参数<=42 then
      lslx="女衣"
      lsmc=制造装备[lslx][lv*10]
      lslx="衣服"
   elseif self.临时参数<=56 then
      lslx="头盔"
      lsmc=制造装备[lslx][lv*10]
       lslx="头盔"
    elseif self.临时参数<=70 then
      lslx="发钗"
      lsmc=制造装备[lslx][lv*10]
       lslx="头盔"
    elseif self.临时参数<=84 then
      lslx="腰带"
      lsmc=制造装备[lslx][lv*10]
    else
      lslx="鞋子"
      lsmc=制造装备[lslx][lv*10]
      end
   end
  self.临时格子=RoleControl:取可用道具格子(UserData[id],"包裹")
 if self.临时格子==0 then
    SendMessage(UserData[id].连接id,7,"#y/您当前的包裹空间已满，无法获得新道具")
  else
    UserData[id].角色.道具.包裹[self.临时格子]=self:生成装备(id,lslx,lsmc,lv*10,1.15,1,jd,nil,0,nil,ItemControl:取道具编号(id))
    SendMessage(UserData[id].连接id,7,"#Y/你获得了#r/"..UserData[id].物品[UserData[id].角色.道具.包裹[self.临时格子]].名称)
   end
  end



function EquipmentControl:添加特效(user,TempItem,特效)

                        if TempItem.特效==nil then
                            TempItem.特效={}
                            TempItem.特效[1]=特效
                        else
                            local 无级别重复=false
                            for n=1,#TempItem.特效 do
                                if TempItem.特效[n]==特效 then
                                无级别重复=true
                                end
                            end
                            if 无级别重复==false then
                                 TempItem.特效[#TempItem.特效+1]=特效
                            end
                        end
                SendMessage(user.连接id,7,"#y/附加成功！")
end
return EquipmentControl
