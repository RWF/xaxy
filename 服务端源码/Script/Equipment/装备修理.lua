function EquipmentControl:修理装备(id,格子1,格子2)
  local 数据={[1]={id=格子1},[2]={id=格子2}}
  if  UserData[id].物品[UserData[id].角色.道具.包裹[数据[1].id]].名称=="珍珠" then
   self.宝石格子=数据[1].id
   self.装备格子=数据[2].id
   else
   self.宝石格子=数据[2].id
   self.装备格子=数据[1].id
   end
    if UserData[id].物品[UserData[id].角色.道具.包裹[self.宝石格子]].名称=="珍珠" then
      if UserData[id].物品[UserData[id].角色.道具.包裹[self.宝石格子]].等级<UserData[id].物品[UserData[id].角色.道具.包裹[self.装备格子]].等级 then
      SendMessage(UserData[id].连接id,7,"#y/您的这个珍珠等级太低了")
      return 0
      elseif UserData[id].物品[UserData[id].角色.道具.包裹[self.装备格子]].耐久度==nil then
      SendMessage(UserData[id].连接id,7,"#y/只有装备才可使用珍珠")
      return 0
      elseif UserData[id].物品[UserData[id].角色.道具.包裹[self.装备格子]].耐久度>=800 then
      SendMessage(UserData[id].连接id,7,"#y/只有耐久度小于800的装备才可使用珍珠")
      return 0
      else
      UserData[id].物品[UserData[id].角色.道具.包裹[self.装备格子]].耐久度=UserData[id].物品[UserData[id].角色.道具.包裹[self.装备格子]].耐久度+100
      SendMessage(UserData[id].连接id,7,"#y/镶嵌成功！装备耐久度增加了100点")
      UserData[id].物品[UserData[id].角色.道具.包裹[self.宝石格子]]=nil
      UserData[id].角色.道具.包裹[self.宝石格子]=nil
      ItemControl:索要打造道具(id, "包裹","修理")
      SendMessage(UserData[id].连接id,3006,"66")
      end
  else
          SendMessage(UserData[id].连接id,7,"#y/只有珍珠才能修理!")
  end
      ItemControl:索要打造道具(id, "包裹", "修理")
end
function EquipmentControl:装备修理(id,道具id)-----------------------------------------------------完成--
   self.临时id1=UserData[id].角色.道具["包裹"][道具id]
    if UserData[id].物品[self.临时id1]==nil or UserData[id].物品[self.临时id1]==0 then
    SendMessage(UserData[id].连接id,7,"#y/你似乎并没有这样的道具")
   elseif UserData[id].物品[self.临时id1].耐久度 == nil then
    SendMessage(UserData[id].连接id, 7, "#y/只有装备才可以修理噢")
   elseif UserData[id].物品[self.临时id1].耐久度 >= 500 then
    SendMessage(UserData[id].连接id, 7, "#y/您的这件装备无须修理")
   elseif UserData[id].物品[self.临时id1].修理失败 ~= nil and UserData[id].物品[self.临时id1].修理失败 >= 3 then
    SendMessage(UserData[id].连接id, 7, "#y/您的这件装备已经无法修理了")
    else
        self.花费金钱 = math.floor(UserData[id].物品[self.临时id1].等级 / 500 * 35000 * (500 - UserData[id].物品[self.临时id1].耐久度))
        if 银子检查(id, self.花费金钱) == false then
          SendMessage(UserData[id].连接id, 7, "#y/你没那么多的银子")
        elseif UserData[id].物品[self.临时id1].修理失败 and UserData[id].物品[self.临时id1].修理失败>2 then
          SendMessage(UserData[id].连接id, 7, "#Y/该装备已经修理失败3次无法进行修理！")
        else
           RoleControl:扣除银子(UserData[id], self.花费金钱, 18)
            self.失败几率 = 20

            if UserData[id].物品[self.临时id1].特效 == "易修理" then
              self.失败几率 = 10
            end

            if math.random(100) <= self.失败几率 then
              SendMessage(UserData[id].连接id, 7, "#y/你的装备在修理时出现了点意外")
              if UserData[id].物品[self.临时id1].修理失败 == nil then
                UserData[id].物品[self.临时id1].修理失败 = 1
              else
                UserData[id].物品[self.临时id1].修理失败 = UserData[id].物品[self.临时id1].修理失败 + 1
              end
            else
                   UserData[id].物品[self.临时id1].耐久度 = 500
                 SendMessage(UserData[id].连接id, 7, "#Y/装备修理成功！")
            end

        end
    end

      SendMessage(UserData[id].连接id,3006,"66")
      SendMessage(UserData[id].连接id, 3019, ItemControl:索要道具3(id,"包裹"))
  end