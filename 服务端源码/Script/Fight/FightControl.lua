-- @Author: 作者QQ381990860
-- @Date:   2021-11-27 17:21:23
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-05-22 19:10:19
local FightControl = class()
local SJZF ={"天覆阵","虎翼阵","鸟翔阵","地载阵","风扬阵","龙飞阵","云垂阵","蛇蟠阵","雷绝阵","鹰啸阵"}
local 随机阵法= SJZF[math.random(1,#SJZF)]
local 幻化战斗属性 = {"抵抗封印等级","法术伤害","法术伤害结果","法术暴击等级","抗法术暴击等级",
"治疗能力","抗物理暴击等级","格挡值","气血回复效果","封印命中等级","固定伤害","狂暴等级","穿刺等级","物理暴击等级"}

local 解除名称={"生命之泉" ,"普渡众生" ,"火甲术","颠倒五行","乾坤妙法","天地同寿","四面埋伏","灵动九天","神龙摆尾",
		"佛法无边","幽冥鬼眼","一苇渡江","金刚护体","金刚护法","不动如山","韦陀护法","明光宝烛","镇魂诀","金身舍利","蜜润","红袖添香",
		"达摩护体" ,"定心术","潜力激发","杀气诀","魔王回首","牛劲" ,"安神诀" ,"极度疯狂" ,"分身术","百毒不侵" ,"楚楚可怜","盘丝阵" ,"移魂化骨",
		"天神护法" ,"天神护体","逆鳞" ,"炎护" ,"碎星诀","乘风破浪","凝神术","匠心·削铁","匠心·固甲","无所遁形","铜头铁臂","气慑天军","无畏布施"}

function FightControl:初始化(玩家id,序号,任务id,地图)
	self.结束条件=false
	self.中断结束=false
	self.死亡惩罚=true
	self.观战玩家={}
	self.加载起始=0
	self.加载等待=0
	self.战斗开始=false
	self.回合数=0
	self.战斗类型=序号
	self.任务id=任务id
	self.战斗失败=false
	self.观战玩家={}
	self.中断计算=false
	self.参战单位={}
	self.参战玩家={}
	self.进入战斗玩家id=玩家id
	self.战斗发言数据={}
	self.飞升序号=0
	self.渡劫序号=0
	self.战斗计时=os.time()
	self.加载等待=7
	self.加载数量=0
	self.回合进程="加载回合"
	self.等待时间={初始=60,延迟=5}
	self.队伍数量={[1]=0,[2]=0}
	self.队伍位置={[1]={},[2]={}}
	self.观战方=玩家id
	self.对战方=任务id
	self.防卡战斗={回合=0,时间=os.time(),执行=false}
	self.pk战斗=false
	self.结束等待=0
	self.助战参战 = {}
	self.进程时间 = os.time()
	if UserData[玩家id].队伍==0 then
		self.发起id=玩家id
	else
		self.发起id=UserData[玩家id].队伍
	end
	self.队伍区分={[1]=self.发起id,[2]=0}
	--加载玩家单位
	if self.战斗类型~=200001 and self.战斗类型~=200002 and self.战斗类型~=200003 and self.战斗类型~=200004 and self.战斗类型~=200005 and self.战斗类型~=200006	and self.战斗类型~=200008 then --pk类的战斗
		if UserData[玩家id].队伍==0 then
			self:加载单个玩家(玩家id,1)
		else
			for n=1,#队伍数据[UserData[玩家id].队伍].队员数据 do
				self:加载单个玩家(队伍数据[UserData[玩家id].队伍].队员数据[n],n)
			end
		end
		self:加载敌方单位(地图)
	else
		self.pk战斗=true
		if self.战斗类型~=200003 then
			self.死亡惩罚=false
		end
		if UserData[玩家id].队伍==0 then
			self:加载单个玩家(玩家id,1)
		else
			for n=1,#队伍数据[UserData[玩家id].队伍].队员数据 do
				self:加载单个玩家(队伍数据[UserData[玩家id].队伍].队员数据[n],n)
			end
		end
		if UserData[任务id].队伍==0 then
			self.挑战id=任务id
		else
			self.挑战id=UserData[任务id].队伍
		end
		self.队伍区分[2]=self.挑战id
		if UserData[任务id].队伍==0 then
			self:加载单个玩家(任务id,1)
		else
			for n=1,#队伍数据[UserData[任务id].队伍].队员数据 do
				self:加载单个玩家(队伍数据[UserData[任务id].队伍].队员数据[n],n)
			end
		end
	end
	self:重置战斗属性()
	for n=1,#self.参战玩家 do---加载单位结束
		SendMessage(UserData[self.参战玩家[n].玩家id].连接id,6001,self.参战玩家[n].队伍id)
		SendMessage(UserData[self.参战玩家[n].玩家id].连接id,6002,#self.参战单位)
	end
	self.加载起始=os.time()
	for n=1,#self.参战玩家 do
		MapControl:更改战斗(self.参战玩家[n].玩家id,true)
	end
end

function FightControl:更新()
	if self.防卡战斗.回合==self.回合数 and os.time()-self.防卡战斗.时间 >= 360 and self.防卡战斗.执行==false then
		self.防卡战斗.执行=true
		self:强制结束战斗()
	end
	if self.回合进程=="加载回合" then
		if os.time()-self.加载起始 >= self.加载等待 then
			self:结束加载回合()
		end
	elseif self.回合进程=="命令回合" then
		if os.time()-self.等待起始 >= self.等待结束 then
			self:命令回合处理()
		end
	elseif self.回合进程=="执行回合" then
		if os.time()-self.等待起始>=self.等待结束 then
			self:执行回合处理()
		end
	elseif self.回合进程=="结束回合1" then
		self.回合进程="结束回合2"
		self:发送结束信息()
	end
end
 
function FightControl:结束加载回合()
	self.回合进程="加载回合结束"
	self.进程时间 = os.time()
	for n=1,#self.参战玩家 do
		if self.参战玩家[n].加载==false then
		end
	end
	self:战斗开始前处理()
 end
 
function FightControl:战斗开始前处理()
 self.等待结束=0
 self.战斗流程={信息提示={}}
 for n=1,#self.参战单位 do
	if self.参战单位[n].隐身~=0 then
	self:隐身处理(n)
	end
	if self.参战单位[n].齐天大圣 then
		self.参战单位[n].齐天大圣=nil
		self.战斗流程[#self.战斗流程+1]={流程=1,攻击方=n,执行=false,类型="齐天大圣"}
		self.等待结束=self.等待结束+5
	end
	if self.参战单位[n].奇经八脉.充沛	then
	local 临时气血=math.floor(self.参战单位[n].最大气血*0.1)
		self.参战单位[n].最大气血 =	self.参战单位[n].最大气血 +临时气血
		self.参战单位[n].气血上限=self.参战单位[n].气血上限+临时气血
		self.参战单位[n].当前气血 = self.参战单位[n].当前气血+临时气血
	end

	if self.参战单位[n].奇经八脉.纯净	then
	local 临时气血=self:取技能等级(self.参战单位[n],"星月之惠")*2
		self.参战单位[n].最大气血 =	self.参战单位[n].最大气血 +临时气血
		self.参战单位[n].气血上限=self.参战单位[n].气血上限+临时气血
		self.参战单位[n].当前气血 = self.参战单位[n].当前气血+临时气血
	end
	if self.参战单位[n].奇经八脉.魔心	then
		self.参战单位[n].魔法上限 =	math.floor(self.参战单位[n].魔法上限*1.1)
	end

		if self.参战单位[n].变身技能~=nil and math.random(0,100)<=100	then	---变身几率
		if	#self.参战单位[n].变身技能>0 then
			for i=1,#self.参战单位[n].变身技能 do
					local 临时变身卡数据 =CardData[self.参战单位[n].变身技能[i]]
				
					if 临时变身卡数据.技能~="无" then
						self:添加技能属性(n,{[1]={名称=临时变身卡数据.技能}})
					end
					if 临时变身卡数据.类型 then ---1类型 2 属性 3 数值
						if 临时变身卡数据.类型 == 1 then
						self.参战单位[n][临时变身卡数据.三维] =self.参战单位[n][临时变身卡数据.三维]+临时变身卡数据.数值
						elseif 临时变身卡数据.类型 == 2 then
						self.参战单位[n][临时变身卡数据.三维] =math.floor(self.参战单位[n][临时变身卡数据.三维]+self.参战单位[n][临时变身卡数据.三维]*临时变身卡数据.数值)
						-- elseif 临时变身卡数据.类型 == 3 then
						-- self.参战单位[n][临时变身卡数据.三维] =math.floor(self.参战单位[n][临时变身卡数据.三维]-self.参战单位[n][临时变身卡数据.三维]*临时变身卡数据.数值)
						end
					end
					self.战斗流程[#self.战斗流程+1]={流程=1,攻击方=n,执行=false,类型="变身",参数=self.参战单位[n].变身技能[i]}
				self.等待结束=self.等待结束+5
				break 
			end
		end
	end
	if self.参战单位[n].附加技能~=nil and #self.参战单位[n].附加技能>0 then
		for i=1,#self.参战单位[n].附加技能 do
		self.参战单位[n].命令数据={下达=false,类型="技能",目标=n,附加=0,参数=self.参战单位[n].附加技能[i]}
		self:技能计算(n,self.参战单位[n].附加技能[i])
		self.参战单位[n].下达=true
		end
	end
	end

	
	if #self.战斗流程==0 then
		self:发送命令回合()
	else
		self.回合进程="执行回合"
		self.进程时间 = os.time()
		self.等待起始=os.time()
		self.循环开关=false
		self.接收数量=#self.参战玩家
		for n=1,#self.参战玩家 do
			UserData[self.参战玩家[n].玩家id].战斗时间 =os.time()
			SendMessage(UserData[self.参战玩家[n].玩家id].连接id,6005,{流程=self.战斗流程,时间=UserData[self.参战玩家[n].玩家id].战斗时间})
		end
	end
 end
 
function FightControl:瞬法处理(编号)
	if self.参战单位[编号]==nil then
		return 0
	end
		self.相关id=self.参战单位[编号].队伍id
		self.临时参数=self.参战单位[编号].法术状态组["瞬法"].技能
		self.技能数量=self:取技能目标数(self.参战单位[编号],self.临时参数)
		self.临时目标=self:取随机单位(编号,self.相关id,1,1)
		
		self.参战单位[编号].命令数据.目标=self.临时目标[1]
		self.参战单位[编号].命令数据.参数=self.临时参数
		self:技能计算(编号,self.参战单位[编号].法术状态组["瞬法"].技能)
		self.参战单位[编号].法术状态组["瞬法"]=nil

end


function FightControl:峰回路转处理(编号)
 if self.参战单位[编号]==nil then
	return 0
	end
	self.相关id=self.参战单位[编号].队伍id
	self.临时目标=self:取随机单位(编号,self.相关id,2,10)
	self.参战单位[编号].法术状态组["峰回路转"]=nil
	self.参战单位[编号].命令数据.目标=self.临时目标[1]
	self.参战单位[编号].命令数据.参数="峰回路转"
	self:技能计算(编号,"峰回路转")
end


function FightControl:瞬击处理(编号)
 if self.参战单位[编号]==nil then
	return 0
	end
	self.相关id=self.参战单位[编号].队伍id
	self.临时目标=self:取随机单位(编号,self.相关id,1,1)
	if self.参战单位[编号].法术状态组["瞬击"]	then
		self.参战单位[编号].法术状态组["瞬击"]=nil
	elseif self.参战单位[编号].法术状态组["逍遥游"]	then
		self.参战单位[编号].法术状态组["逍遥游"]=nil
	elseif self.参战单位[编号].法术状态组["开门见山"]	then
		self.参战单位[编号].法术状态组["开门见山"]=nil
	end
	self.参战单位[编号].命令数据.目标=self.临时目标[1]
	self:攻击流程计算(编号,1)
end


function FightControl:隐身处理(编号)
 if self.参战单位[编号]==nil then
	return 0
	end
 self.战斗流程[#self.战斗流程+1]={流程=4,执行=false,攻击方=编号,挨打方=编号,结束=false,类型="技能",封印结果=true,参数="隐身"}
 self.等待结束=self.等待结束+4

 self.参战单位[编号].法术状态组["隐身"]={回合=self.参战单位[编号].隐身}
 end

function FightControl:发送命令回合()
	self.战斗开始=true
	for n=1,#self.参战玩家 do
		self.命令单位={}
		for i=1,#self.参战单位 do
			self.参战单位[i].命令数据={下达=false,类型="",目标="",附加=0,参数=0}
			if self.参战单位[i].战斗类型 ~= "召唤类" and self.参战单位[i].玩家id==self.参战玩家[n].玩家id and self.参战单位[i].单位消失==nil then
				self.命令单位[#self.命令单位+1]={编号=i,名称=self.参战单位[i].名称,自动=self.参战单位[i].自动,助战=self.参战单位[i].助战}
			end
		end
		if UserData[self.参战玩家[n].玩家id] ~= nil	then
			SendMessage(UserData[self.参战玩家[n].玩家id].连接id,6004,self.命令单位)
			for i=1,#self.战斗发言数据 do
				SendMessage(UserData[self.参战玩家[n].玩家id].连接id,6013,{id=self.战斗发言数据[i].id,消息=self.战斗发言数据[i].内容})
			end
		end
		self.战斗发言数据={}
	end
	self.回合进程="命令回合"
	self.进程时间 = os.time()
	self.等待起始=os.time()
	self.等待结束=self.等待时间.初始+self.等待时间.延迟
	self.循环开关=false
	self.接收数量=#self.参战玩家
	for n=1,#self.参战玩家 do
		if self.参战玩家[n].逃跑 then
			self.接收数量=self.接收数量-1
		end
	end
end


function FightControl:执行回合计算()
 self.战斗流程={信息提示={}}
 for n=1,#self.执行单位 do
	if self:取存活状态(self.执行单位[n]) then --存活则进行结算
		if self.参战单位[self.执行单位[n]].法宝效果.宝烛 and self.参战单位[self.执行单位[n]].当前气血 <=	self.参战单位[self.执行单位[n]].气血上限*0.7 then
			self.战斗流程[#self.战斗流程+1]={类型="技能",执行=false,结束=false,流程=100,挨打方=self.执行单位[n],伤害=self.参战单位[self.执行单位[n]].法宝效果.宝烛,伤害类型="加血",死亡=0}
			self.参战单位[self.执行单位[n]].当前气血=self.参战单位[self.执行单位[n]].当前气血+self.参战单位[self.执行单位[n]].法宝效果.宝烛
			self.等待结束=self.等待结束+2
			if self.参战单位[self.执行单位[n]].当前气血>self.参战单位[self.执行单位[n]].气血上限 then
			self.参战单位[self.执行单位[n]].当前气血=self.参战单位[self.执行单位[n]].气血上限
			end
		elseif self.参战单位[self.执行单位[n]].攻之械 >=3 and not self.参战单位[self.执行单位[n]].偃甲	then
			self.战斗流程[#self.战斗流程+1]={流程=1,攻击方=self.执行单位[n],执行=false,类型="攻之械"}
			self.等待结束=self.等待结束+5
			self.参战单位[self.执行单位[n]].偃甲 =true
		elseif self.参战单位[self.执行单位[n]].法宝效果.天煞 then
			self.参战单位[self.执行单位[n]].当前魔法=self.参战单位[self.执行单位[n]].当前魔法+self.参战单位[self.执行单位[n]].法宝效果.天煞
			if self.参战单位[self.执行单位[n]].当前魔法>self.参战单位[self.执行单位[n]].魔法上限 then
			self.参战单位[self.执行单位[n]].当前魔法=self.参战单位[self.执行单位[n]].魔法上限
			end
		end

	for v,q in pairs(self.参战单位[self.执行单位[n]].法术状态组) do
		if not self.参战单位[self.执行单位[n]].法术状态组.魔音摄魂 and (v=="普渡众生" or v=="生命之泉"or v=="九幽" ) then
				self.战斗流程[#self.战斗流程+1]={类型="技能",执行=false,结束=false,流程=100,挨打方=self.执行单位[n],伤害=q.伤害,伤害类型="加血",死亡=0}
				self.参战单位[self.执行单位[n]].当前气血=self.参战单位[self.执行单位[n]].当前气血+q.伤害
				self.等待结束=self.等待结束+2
				if self.参战单位[self.执行单位[n]].当前气血>self.参战单位[self.执行单位[n]].气血上限 then
				self.参战单位[self.执行单位[n]].当前气血=self.参战单位[self.执行单位[n]].气血上限
				end
		elseif v=="尸腐毒" or v=="紧箍咒" or v=="雾杀" then
			self.战斗流程[#self.战斗流程+1]={类型="技能",执行=false,结束=false,流程=100,挨打方=self.执行单位[n],伤害=q.伤害,伤害类型="掉血",死亡=0}
			self.参战单位[self.执行单位[n]].当前气血=self.参战单位[self.执行单位[n]].当前气血-q.伤害
			self.等待结束=self.等待结束+2
			if self.参战单位[self.执行单位[n]].当前气血<=0 then
			self.参战单位[self.执行单位[n]].当前气血=0
			self.战斗流程[#self.战斗流程].死亡=self:取死亡状态(1,self.参战单位[self.执行单位[n]])
			end
			self:催眠状态解除("掉血",self.参战单位[self.执行单位[n]])
		elseif v=="炼气化神" or v=="魔息术" or v=="赤焰"	then
			self.参战单位[self.执行单位[n]].当前魔法=self.参战单位[self.执行单位[n]].当前魔法+q.魔法
			if self.参战单位[self.执行单位[n]].当前魔法>self.参战单位[self.执行单位[n]].魔法上限 then
			self.参战单位[self.执行单位[n]].当前魔法=self.参战单位[self.执行单位[n]].魔法上限
			end
		elseif v=="乾坤玄火塔" then
			self.参战单位[self.执行单位[n]].愤怒=self.参战单位[self.执行单位[n]].愤怒+q.伤害
			if self.参战单位[self.执行单位[n]].愤怒>150 then
			self.参战单位[self.执行单位[n]].愤怒=150
			end
		elseif v=="无尘扇" then
			self.参战单位[self.执行单位[n]].愤怒=self.参战单位[self.执行单位[n]].愤怒-q.愤怒
			if self.参战单位[self.执行单位[n]].愤怒<0 then
			self.参战单位[self.执行单位[n]].愤怒=0
			end
		elseif v=="毒"	then
			self.扣除气血=math.floor(self.参战单位[self.执行单位[n]].当前气血*0.1)
			if self.扣除气血>q.等级*50 then
			self.扣除气血=q.等级*50
			end
			if self.参战单位[self.执行单位[n]].当前气血>self.扣除气血 then
				self.战斗流程[#self.战斗流程+1]={类型="技能",执行=false,结束=false,流程=100,挨打方=self.执行单位[n],伤害=self.扣除气血,伤害类型="掉血",死亡=0}
				self.参战单位[self.执行单位[n]].当前气血=self.参战单位[self.执行单位[n]].当前气血-self.扣除气血
				self.等待结束=self.等待结束+2
				if self.参战单位[self.执行单位[n]].当前气血<=0 then
				self.参战单位[self.执行单位[n]].当前气血=0
				self.战斗流程[#self.战斗流程].死亡=self:取死亡状态(1,self.参战单位[self.执行单位[n]])
				end
			end
			self:催眠状态解除("掉血",self.参战单位[self.执行单位[n]])
		elseif v=="横扫千军" then
			self.参战单位[self.执行单位[n]].命令数据.类型="已执行"
		elseif v=="血雨" then
			self.参战单位[self.执行单位[n]].命令数据.类型="已执行"
		elseif v=="天崩地裂" then
			self.参战单位[self.执行单位[n]].命令数据.类型="已执行"
		elseif v=="鹰击" then
			if self.参战单位[self.执行单位[n]].命令数据.类型~="防御" then
				self.参战单位[self.执行单位[n]].命令数据.类型="已执行"
			end
		elseif v=="发瘟匣" then
			self.参战单位[self.执行单位[n]].命令数据.目标=0
		elseif v=="无魂傀儡" then
			self.参战单位[self.执行单位[n]].命令数据.类型="攻击"
		elseif v=="失心钹" then
			self.参战单位[self.执行单位[n]].命令数据.类型="攻击"
			self.临时目标=self:取随机单位(self.执行单位[n],self.参战单位[self.执行单位[n]].队伍id,2,1)
				if self.临时目标==0 then
				self.参战单位[self.执行单位[n]].命令数据.目标=0
				else
				self.参战单位[self.执行单位[n]].命令数据.目标=self.临时目标[1]
				end
		elseif v=="翻江搅海" then
			if self.参战单位[self.执行单位[n]].命令数据.类型~="防御" then
			self.参战单位[self.执行单位[n]].命令数据.类型="已执行"
			end
		elseif v=="后发制人" then
			if self:取存活状态(q.目标)	then
				self.参战单位[self.执行单位[n]].命令数据.目标=q.目标
				else
						self.相关id=self.参战单位[self.执行单位[n]].队伍id
						self.临时目标=self:取随机单位(self.执行单位[n],self.相关id,1,1)
						if self.临时目标==0 then
						self.参战单位[self.执行单位[n]].命令数据.目标=0
						else
						self.参战单位[self.执行单位[n]].命令数据.目标=self.临时目标[1]
						end
				end
				self.参战单位[self.执行单位[n]].命令数据.类型="技能"
				self.参战单位[self.执行单位[n]].命令数据.参数="后发制人"
				self:单体物理法术计算(self.执行单位[n])
				self.参战单位[self.执行单位[n]].命令数据.类型="已执行"
				self:解除封印状态(self.参战单位[self.执行单位[n]],v)
				self.参战单位[self.执行单位[n]].法术状态组[v]=nil
		elseif v=="锋芒毕露" or	v == "诱袭"then
			if self:取存活状态(q.目标)	then
					self.参战单位[self.执行单位[n]].命令数据.目标=q.目标
				else
						self.相关id=self.参战单位[self.执行单位[n]].队伍id
						self.临时目标=self:取随机单位(self.执行单位[n],self.相关id,1,1)
						if self.临时目标==0 then
						self.参战单位[self.执行单位[n]].命令数据.目标=0
						else
						self.参战单位[self.执行单位[n]].命令数据.目标=self.临时目标[1]
						end
				end
				self.参战单位[self.执行单位[n]].命令数据.类型="攻击"
					self:攻击流程计算(self.执行单位[n])
				self.参战单位[self.执行单位[n]].命令数据.类型="已执行"


		end
	end
	else
		--检查后发
		if self.参战单位[self.执行单位[n]].法术状态组["后发制人"] then
		self.战斗流程[#self.战斗流程+1]={类型="技能",执行=false,结束=false,流程=104,挨打方=self.执行单位[n],参数="后发制人"}
		self:解除封印状态(self.参战单位[self.执行单位[n]],"后发制人")
		self.参战单位[self.执行单位[n]].法术状态组["后发制人"]=nil
		self.参战单位[self.执行单位[n]].命令数据.类型="已执行"
		-- elseif self.参战单位[self.执行单位[n]].法术状态组["诱袭"] or self.参战单位[self.执行单位[n]].法术状态组["锋芒毕露"] then
		--		self.战斗流程[#self.战斗流程+1]={类型="技能",执行=false,结束=false,流程=104,挨打方=self.执行单位[n],参数="后发制人"}
		--	self.参战单位[self.执行单位[n]].命令数据.类型="已执行"
		end
	end
 end
 if self.战斗类型==100005 and self.回合数>=3 then
	--寻找狸
	self.寻找编号=0
	for n=1,#self.参战单位 do
		if self.参战单位[n].队伍id==0 and self.参战单位[n].名称=="狸" then
		self.寻找编号=n
		end
	end
	if self.寻找编号~=0 and self:取存活状态(self.寻找编号)	then
		self.战斗流程[#self.战斗流程+1]={类型="技能",执行=false,结束=false,流程=100,挨打方=self.寻找编号,伤害=self.参战单位[self.寻找编号].当前气血,伤害类型="掉血",死亡=0}
		self.参战单位[self.寻找编号].当前气血=0
		self.等待结束=self.等待结束+2
		if self.参战单位[self.寻找编号].当前气血<=0 then
		self.参战单位[self.寻找编号].当前气血=0
		self.战斗流程[#self.战斗流程].死亡=self:取死亡状态(1,self.参战单位[self.寻找编号])
		end
	end
	end
 for n=1,#self.执行单位 do
	if self.中断结束 then --行动状态统一判断
	elseif self:取存活状态(self.执行单位[n])==false or self.参战单位[self.执行单位[n]].法术状态组["破釜沉舟"] or self.参战单位[self.执行单位[n]].法术状态组["横扫千军"] or self.参战单位[self.执行单位[n]].法术状态组["催眠符"] or self.参战单位[self.执行单位[n]].法术状态组["楚楚可怜"] or self.参战单位[self.执行单位[n]].法术状态组["天崩地裂"] or self.参战单位[self.执行单位[n]].法术状态组["煞气诀"] or self.参战单位[self.执行单位[n]].法术状态组["血雨"] then --存活状态判断

	else
		if self.参战单位[self.执行单位[n]].命令数据.类型=="攻击" and not self.参战单位[self.执行单位[n]].攻击封印 then

			if self.参战单位[self.执行单位[n]].理直气壮 and math.random(0,100)<=40	then
				

				self.参战单位[self.执行单位[n]].命令数据.类型="技能"
				self.参战单位[self.执行单位[n]].命令数据.参数="理直气壮"
				self:单体物理法术计算(self.执行单位[n])
			elseif math.random(100)<=self.参战单位[self.执行单位[n]].连击 then
				self.参战单位[self.执行单位[n]].命令数据.类型="技能"
				self.参战单位[self.执行单位[n]].命令数据.参数="连击"
				self:单体物理法术计算(self.执行单位[n])
			else
					self:攻击流程计算(self.执行单位[n])
			end
				if	math.random(100) <=35	and self:取存活状态(self.执行单位[n]) and self:取存活状态(self.参战单位[self.执行单位[n]].命令数据.目标)	and self.参战单位[self.执行单位[n]].追加技能~=nil and #self.参战单位[self.执行单位[n]].追加技能>0 and	not self.参战单位[self.执行单位[n]].法术状态组.隐身	then
				self.使用技能=self.参战单位[self.执行单位[n]].追加技能[math.random(1,#self.参战单位[self.执行单位[n]].追加技能)]
				self.参战单位[self.执行单位[n]].命令数据={下达=false,类型="技能",目标=self.参战单位[self.执行单位[n]].命令数据.目标,附加=0,参数=self.使用技能}
				self:技能计算(self.执行单位[n],self.使用技能)
			end
			if self.参战单位[self.执行单位[n]].法宝效果.嗜血幡	then
				if math.random(100)<self.参战单位[self.执行单位[n]].法宝效果.嗜血幡*5	then
				local lsmb=self:取随机单位(self.执行单位[n],self.参战单位[self.执行单位[n]].队伍id,1,1)
				if lsmb==0 then
				self.参战单位[self.执行单位[n]].命令数据.目标=0
				else
				self.参战单位[self.执行单位[n]].命令数据.目标=lsmb[1]
				end
				if	self.参战单位[self.执行单位[n]].命令数据.目标~= 0 then
				self:攻击流程计算(self.执行单位[n])
					if	math.random(100) <=35	and self:取存活状态(self.执行单位[n]) and self:取存活状态(self.参战单位[self.执行单位[n]].命令数据.目标)	and self.参战单位[self.执行单位[n]].追加技能~=nil and #self.参战单位[self.执行单位[n]].追加技能>0 and	not self.参战单位[self.执行单位[n]].法术状态组.隐身	then
						self.使用技能=self.参战单位[self.执行单位[n]].追加技能[math.random(1,#self.参战单位[self.执行单位[n]].追加技能)]
						self.参战单位[self.执行单位[n]].命令数据={下达=false,类型="技能",目标=self.参战单位[self.执行单位[n]].命令数据.目标,附加=0,参数=self.使用技能}
						self:技能计算(self.执行单位[n],self.使用技能)
					end
				end
				end
			end
			if	self.参战单位[self.执行单位[n]].追击 then
				if self:取存活状态(self.执行单位[n]) and self:取存活状态(self.参战单位[self.执行单位[n]].命令数据.目标)==false then
					if	math.random(100) <=35 and self.参战单位[self.执行单位[n]].追加技能~=nil and #self.参战单位[self.执行单位[n]].追加技能>0 and	not self.参战单位[self.执行单位[n]].法术状态组.隐身 then
					self.使用技能=self.参战单位[self.执行单位[n]].追加技能[math.random(1,#self.参战单位[self.执行单位[n]].追加技能)]
					self.参战单位[self.执行单位[n]].命令数据={下达=false,类型="技能",目标=self.参战单位[self.执行单位[n]].命令数据.目标,附加=0,参数=self.使用技能}
					self:技能计算(self.执行单位[n],self.使用技能)
					end
					self:攻击流程计算(self.执行单位[n],1)
				end
			end

			if self.参战单位[self.执行单位[n]].怒击 then
				if self.苍鸾怒击==true	then
					self:攻击流程计算(self.执行单位[n],1)
					if	math.random(100) <=35	and self:取存活状态(self.执行单位[n]) and self:取存活状态(self.参战单位[self.执行单位[n]].命令数据.目标)	and self.参战单位[self.执行单位[n]].追加技能~=nil and #self.参战单位[self.执行单位[n]].追加技能>0 and	not self.参战单位[self.执行单位[n]].法术状态组.隐身	then
					self.使用技能=self.参战单位[self.执行单位[n]].追加技能[math.random(1,#self.参战单位[self.执行单位[n]].追加技能)]
					self.参战单位[self.执行单位[n]].命令数据={下达=false,类型="技能",目标=self.参战单位[self.执行单位[n]].命令数据.目标,附加=0,参数=self.使用技能}
					self:技能计算(self.执行单位[n],self.使用技能)
					end
				end
			end
		elseif self.参战单位[self.执行单位[n]].命令数据.类型=="捕捉" then
			self:捕捉流程计算(self.执行单位[n])
		elseif self.参战单位[self.执行单位[n]].命令数据.类型=="逃跑" and self.战斗类型~=200003 then
			self:逃跑流程计算(self.执行单位[n])
		elseif self.参战单位[self.执行单位[n]].命令数据.类型=="召唤" and self:取存活状态(self.执行单位[n]) then
			self:召唤流程计算(self.执行单位[n])
		elseif self.参战单位[self.执行单位[n]].命令数据.类型=="道具" then
			self:道具流程计算(self.执行单位[n])
		elseif self.参战单位[self.执行单位[n]].命令数据.类型=="法宝" and not self.参战单位[self.执行单位[n]].法宝封印 then
			self:法宝流程计算(self.执行单位[n])
		elseif self.参战单位[self.执行单位[n]].命令数据.类型=="特技" and not self.参战单位[self.执行单位[n]].特技封印 then
			self.临时参数=self.参战单位[self.执行单位[n]].命令数据.参数
			if self.临时参数=="破碎无双" or self.临时参数=="弱点击破" or self.临时参数=="破血狂攻" or self.临时参数=="连环击" or self.临时参数=="断岳势" or self.临时参数=="腾雷" then --单体法术设置
				self:单体物理法术计算(self.执行单位[n],1)
			elseif	self.临时参数=="凝神诀" or self.临时参数=="凝气诀" or self.临时参数=="气疗术" or self.临时参数=="命疗术" or self.临时参数=="命归术" or self.临时参数=="气归术" or self.临时参数=="冰清诀" or self.临时参数=="水清诀"	then --单体法术设置
			self:单体恢复法术计算(self.执行单位[n],1)
			elseif	self.临时参数=="四海升平" or self.临时参数=="晶清诀" or self.临时参数=="玉清诀" or self.临时参数=="圣灵之甲" or self.临时参数=="魔兽之印" or self.临时参数=="罗汉金钟"	then --单体法术设置
			self:群体恢复法术计算(self.执行单位[n],1)
			elseif	self.临时参数=="冥王爆杀" or self.临时参数=="诅咒之伤"	then --单体法术设置
			self:单体法术计算(self.执行单位[n],1)
			elseif	self.临时参数=="野兽之力" or self.临时参数=="光辉之甲" or self.临时参数=="太极护法"	then --单体法术设置
			self:单体封印法术计算(self.执行单位[n],1)
			elseif	self.临时参数=="慈航普渡"	then --单体法术设置
			self:群体复活法术计算(self.执行单位[n],1)
			elseif self.临时参数=="放下屠刀" or	self.临时参数=="流云诀"or self.临时参数=="笑里藏刀" then
				self:单体状态法术计算(self.执行单位[n],1)
			end
		elseif self.参战单位[self.执行单位[n]].命令数据.类型=="技能" and not self.参战单位[self.执行单位[n]].技能封印 and not self.参战单位[self.执行单位[n]].法术状态组.象形限制 and	not self.参战单位[self.执行单位[n]].法术状态组.隐身 then
				if self.参战单位[self.执行单位[n]].命令数据.参数=="摇头摆尾" then
					if math.random(100)<=50 then
					self.参战单位[self.执行单位[n]].命令数据.参数="飞砂走石"
					else
					self.参战单位[self.执行单位[n]].命令数据.参数="三昧真火"
					end
				self.参战单位[self.执行单位[n]].命令数据.摇头摆尾=true
				end
			self:技能计算(self.执行单位[n],self.参战单位[self.执行单位[n]].命令数据.参数)
		end
	end
	if self.参战单位[self.执行单位[n]].奇经八脉.养生 and not self.参战单位[self.执行单位[n]].法术状态组["生命之泉"] and	self.参战单位[self.执行单位[n]].当前气血<=self.参战单位[self.执行单位[n]].最大气血*0.3 then
		self.参战单位[self.执行单位[n]].命令数据={类型="技能",参数="生命之泉",目标=self.参战单位[self.执行单位[n]]}
		self:群体恢复法术计算(self.执行单位[n])
	end
	end

 end
function FightControl:状态数据更新()
 self.发送信息={}
 self.当前封印人数=0
	for n=1,#self.参战单位 do
	--添加愤怒
	if self.参战单位[n].愤怒~=nil and self:取存活状态(n) then
		if self.参战单位[n].愤怒>150 then
		self.参战单位[n].愤怒 =150
		end
	end
	if self.参战单位[n].再生~=0 and self:取存活状态(n) then
		self.参战单位[n].当前气血=self.参战单位[n].当前气血+self.参战单位[n].再生
		if self.参战单位[n].当前气血>self.参战单位[n].气血上限 then
		self.参战单位[n].当前气血=self.参战单位[n].气血上限
		end
	end
	if self.参战单位[n].冥想~=0 and self:取存活状态(n) then
		self.参战单位[n].当前魔法=self.参战单位[n].当前魔法+self.参战单位[n].冥想
		if self.参战单位[n].当前魔法>self.参战单位[n].魔法上限 then
		self.参战单位[n].当前魔法=self.参战单位[n].魔法上限
		end
	end
	for k,v in pairs(self.参战单位[n].法术状态组) do
		if v.回合 ==nil then
		v.回合 =1
		end
			v.回合=v.回合-1
			if k=="分身术" then
			v.躲避=false
			end
			if v.回合<=0 then
			self.发送信息[#self.发送信息+1]={id=n,名称=k,气血=self.参战单位[n].气血上限}
			v=nil
			self:解除封印状态(self.参战单位[n],k)
			end
		end
	end
	if #self.发送信息>0 then
	for n=1,#self.参战玩家 do
		if	UserData[self.参战玩家[n].玩家id] then
		SendMessage(UserData[self.参战玩家[n].玩家id].连接id,6006,self.发送信息)
		end
	end
	end
 end
function FightControl:取技能等级(攻击方,技能名称)
	if 装备特技[技能名称]~=nil then 
		return 0 
	end
	for n=1,#攻击方.主动技能 do
		if 攻击方.主动技能[n].名称==技能名称 then
			return 攻击方.主动技能[n].等级
		end
	end
	if 攻击方.追加技能==nil then 
		return 0 
	end
	for n=1,#攻击方.追加技能 do
		if 攻击方.追加技能[n]==技能名称 then
			return 攻击方.等级+5
		end
	end

	if 攻击方.附加技能==nil then 
		return 0 
	end
	for n=1,#攻击方.附加技能 do
		if 攻击方.附加技能[n]==技能名称 then
			return 攻击方.等级+5
		end
	end
	if 攻击方.变身技能==nil then 
		return 0 
	end
	for n=1,#攻击方.变身技能 do
		if 攻击方.变身技能[n]==技能名称 then
			return 攻击方.等级+5
		end
	end
	return 0
end

function FightControl:取日月乾坤结束(n)
	for n=1,15 do
		if self.战斗类型==100000+n then
			return true
		end
		return false
	end
end
function FightControl:执行回合处理()
	self:状态数据更新()
	self.死亡人数=0
	for n=1,#self.参战单位 do
		if self.参战单位[n].队伍id==self.队伍区分[1] then
			if self:取存活状态(n)==false or (self.参战单位[n].队伍id==0 and	self.参战单位[n].法术状态组.日月乾坤 and self:取日月乾坤结束(n)) then
				self.死亡人数=self.死亡人数+1
			end
		end
	end
	if self.死亡人数==self.队伍数量[1] then
		self:结束战斗(self.队伍区分[2])
		return 0
	end
	self.死亡人数=0
	for n=1,#self.参战单位 do
		if self.参战单位[n].队伍id==self.队伍区分[2] then
			if self:取存活状态(n)==false or (self.参战单位[n].队伍id==0 and	self.参战单位[n].法术状态组.日月乾坤 and self:取日月乾坤结束(n)) then
				self.死亡人数=self.死亡人数+1
			end
		end
	end
	if self.死亡人数==self.队伍数量[2]	then
		self:结束战斗(self.队伍区分[1])
		return 0
	end
	self.回合数=self.回合数+1
	self.防卡战斗.回合=self.回合数
	self.防卡战斗.时间=os.time()
	self:发送命令回合()
 end
function FightControl:命令回合处理()
	self.回合进程="计算回合"
	self.进程时间 = os.time()
 --计算所有单位命令是否已经下达
	for n=1,#self.参战单位 do
		if self.参战单位[n].命令数据.下达==false then
			if self.参战单位[n].助战 then
				if UserData[self.参战单位[n].玩家id].助战.数据[self.参战单位[n].助战编号].上次行动 == nil then
					self:添加攻击命令(n)
				else
					self.参战单位[n].命令数据 = table.copy(UserData[self.参战单位[n].玩家id].助战.数据[self.参战单位[n].助战编号].上次行动)
					if self.参战单位[n].命令数据.参数 == "鹰击" or self.参战单位[n].命令数据.参数 == "狮搏" then
						if not self.参战单位[n].法术状态组.变身 then
							self.参战单位[n].命令数据.参数 = "变身"
							self.参战单位[n].命令数据.目标 = n
						end
					end
				end
			else
				self:添加攻击命令(n)
			end
		end
	end
	self.临时单位={}
	for n=1,#self.参战单位 do
		self.临时单位[n]={id=n,sd=self.参战单位[n].速度}
		self.参战单位[n].行动=false
	end
	table.sort(self.临时单位,function(a,b) return a.sd>b.sd end )
	self.执行单位={}
	for n=1,#self.临时单位 do
		self.执行单位[n]=self.临时单位[n].id
	end
	self.等待结束=0
	self:执行回合计算()
	self.回合进程="执行回合"
	self.进程时间 = os.time()
	self.等待起始=os.time()
	self.循环开关=false
	self.结束等待=0
	self.接收数量=#self.参战玩家
	for n=1,#self.参战玩家 do
		if self.参战玩家[n].逃跑 then
			self.接收数量=self.接收数量-1
		end
	end
	for n=1,#self.观战玩家 do
		if self.观战玩家[n]~=0 and UserData[self.观战玩家[n]]~=nil then
			UserData[self.观战玩家[n]].战斗时间 =os.time()
			SendMessage(UserData[self.观战玩家[n]].连接id,6005,{流程=self.战斗流程,时间=UserData[self.观战玩家[n]].战斗时间})
		end
	end
	for n=1,#self.参战玩家 do
		if UserData[self.参战玩家[n].玩家id] then
			UserData[self.参战玩家[n].玩家id].战斗时间 =os.time()
			SendMessage(UserData[self.参战玩家[n].玩家id].连接id,6005,{流程=self.战斗流程,时间=UserData[self.参战玩家[n].玩家id].战斗时间})
			for i=1,#self.参战单位 do
				if self.参战单位[i].当前气血==nil then 
					self.参战单位[i].当前气血=1 
				end
				if self.参战单位[i].助战 == nil and self.参战单位[i].玩家id == self.参战玩家[n].玩家id and self.参战单位[i].助战参战 == nil then
					if self.参战单位[i].战斗类型=="角色" and UserData[self.参战玩家[n].玩家id]~=nil	then
						UserData[self.参战玩家[n].玩家id].角色.当前气血=self.参战单位[i].当前气血
						UserData[self.参战玩家[n].玩家id].角色.气血上限=self.参战单位[i].气血上限
						UserData[self.参战玩家[n].玩家id].角色.最大气血=self.参战单位[i].最大气血
						UserData[self.参战玩家[n].玩家id].角色.魔法上限=self.参战单位[i].魔法上限
						UserData[self.参战玩家[n].玩家id].角色.当前魔法=self.参战单位[i].当前魔法
						UserData[self.参战玩家[n].玩家id].角色.愤怒=self.参战单位[i].愤怒
						SendMessage(UserData[self.参战玩家[n].玩家id].连接id,1003,RoleControl:获取角色气血数据(UserData[self.参战玩家[n].玩家id]))
					elseif UserData[self.参战单位[n].玩家id]~=nil and self.参战单位[i].战斗类型~="召唤类"   then
						if UserData[self.参战玩家[n].玩家id].召唤兽.数据.参战~=0 then
							self.召唤兽id=UserData[self.参战玩家[n].玩家id].召唤兽.数据.参战
							UserData[self.参战玩家[n].玩家id].召唤兽.数据[self.召唤兽id].当前气血=self.参战单位[i].当前气血
							UserData[self.参战玩家[n].玩家id].召唤兽.数据[self.召唤兽id].气血上限=self.参战单位[i].气血上限
							UserData[self.参战玩家[n].玩家id].召唤兽.数据[self.召唤兽id].当前魔法=self.参战单位[i].当前魔法
							UserData[self.参战玩家[n].玩家id].召唤兽.数据[self.召唤兽id].魔法上限=self.参战单位[i].魔法上限
						end

						-- if self.参战单位[i].助战参战 == nil then
							self.发送信息={
							当前气血=self.参战单位[i].当前气血,
							气血上限=self.参战单位[i].气血上限,
							当前魔法=self.参战单位[i].当前魔法,
							魔法上限=self.参战单位[i].魔法上限}
							SendMessage(UserData[self.参战玩家[n].玩家id].连接id,2015,self.发送信息)
						-- end
					end
				end
			end
		end
	end
end


function FightControl:添加攻击命令(编号)
	self.相关id=self.参战单位[编号].队伍id

	if self.相关id ~=0	then
		self.参战单位[编号].命令数据.下达=true
		self.参战单位[编号].命令数据.类型="攻击"
		self.临时目标=self:取随机单位(编号,self.相关id,1,1)
		if self.临时目标==0 then
			self.参战单位[编号].命令数据.目标=0
		else
			self.参战单位[编号].命令数据.目标=self.临时目标[1]
		end
		if self.参战单位[编号].助战 then
			self.参战单位[编号].自动 = true
			if self.参战单位[编号].命令数据.类型 == "逃跑" then
				self.参战单位[编号].命令数据.类型 = "攻击"
			end
			UserData[self.参战单位[编号].玩家id].助战.数据[self.参战单位[编号].助战编号].上次行动 = self.参战单位[编号].命令数据
			UserData[self.参战单位[编号].玩家id].助战.数据[self.参战单位[编号].助战编号].自动 = self.参战单位[编号].自动
		end
	else
		if self.战斗类型 == 888 and self.参战单位[编号].战斗类型 ~="召唤类" then
			return
		end
		self.参战单位[编号].命令数据.下达=true
		self.参战单位[编号].命令数据.类型="攻击"
		self.临时目标=self:取随机单位(编号,self.相关id,1,1)
		if self.临时目标==0 then
			self.参战单位[编号].命令数据.目标=0
		else
			self.参战单位[编号].命令数据.目标=self.临时目标[1]
		end
		if self.参战单位[编号].特技数据~= nil and #self.参战单位[编号].特技数据>0 and math.random(100)<=30 then
			self.临时技能=self.参战单位[编号].特技数据[math.random(1,#self.参战单位[编号].特技数据)].名称
			self.参战单位[编号].命令数据.下达=true
			self.参战单位[编号].命令数据.类型="特技"
			self.参战单位[编号].命令数据.参数=self.临时技能
			if 装备特技[self.临时技能]==nil then
				self.参战单位[编号].命令数据.类型=""
				return 0
			end
			if 装备特技[self.临时技能].对象==1 then
				self.参战单位[编号].命令数据.目标=编号
			elseif 装备特技[self.临时技能].对象==2 then
				self.临时目标=self:取随机单位(编号,self.相关id,2,1)
				if self.临时目标==0 then
					self.参战单位[编号].命令数据.目标=0
				else
					self.参战单位[编号].命令数据.目标=self.临时目标[1]
				end
			elseif 装备特技[self.临时技能].对象==3 then
				self.临时目标=self:取随机单位(编号,self.相关id,1,1)
				if self.临时目标==0 then
					self.参战单位[编号].命令数据.目标=0
				else
					self.参战单位[编号].命令数据.目标=self.临时目标[1]
					--self.参战单位[编号].命令数据.目标=1
				end
			end
		elseif #self.参战单位[编号].主动技能>0	and math.random(100)<=80	then
			local 主动技能编号 = math.random(1,#self.参战单位[编号].主动技能)
			self.临时技能=self.参战单位[编号].主动技能[主动技能编号].名称
			for i=1,#self.参战单位[编号].主动技能 do
				if self.参战单位[编号].主动技能[i].名称=="变身" and	not self.参战单位[编号].法术状态组["变身"]	then
					self.临时技能 ="变身"
					主动技能编号 = i
				end
			end

			for i=1,#解除名称 do
				if self.临时技能 ==解除名称[i] and self.参战单位[编号].法术状态组[解除名称[i]]	then
					self.参战单位[编号].命令数据.下达=true
					self.参战单位[编号].命令数据.类型="攻击"
					self.临时目标=self:取随机单位(编号,self.相关id,1,1)
					if self.临时目标==0 then
						self.参战单位[编号].命令数据.目标=0
					else
						self.参战单位[编号].命令数据.目标=self.临时目标[1]
					end
					return
				end
			end
			self.参战单位[编号].命令数据.下达=true
			self.参战单位[编号].命令数据.类型="技能"
			self.参战单位[编号].命令数据.参数=self.临时技能
			if self.参战单位[编号].主动技能[主动技能编号].种类==2 or self.参战单位[编号].主动技能[主动技能编号].种类==5 then
				self.参战单位[编号].命令数据.目标=编号
			elseif self.参战单位[编号].主动技能[主动技能编号].种类==3 or self.参战单位[编号].主动技能[主动技能编号].种类 ==6 then
				self.临时目标=self:取随机单位(编号,self.相关id,2,1)
				if self.临时目标==0 then
					self.参战单位[编号].命令数据.目标=0
				else
					self.参战单位[编号].命令数据.目标=self.临时目标[1]
				end
			elseif self.参战单位[编号].主动技能[主动技能编号].种类 ==4 then
				self.临时目标=self:取随机单位(编号,self.相关id,1,1)
				if self.临时目标==0 then
					self.参战单位[编号].命令数据.目标=0
				else
					self.参战单位[编号].命令数据.目标=self.临时目标[1]
				end
			end
		end
	end
end


function FightControl:加载敌方单位(单位组)
 if self.战斗类型==100001 then
	self:创建野外单位()
	else
	self:加载指定单位(单位组)
	end
 end
function FightControl:加载指定单位(单位组)
	self.加载数值={"伤害","灵力","速度",}
	local sjsz ={"力量","魔力","躲闪","体质","耐力","敏捷"}
	self.加载人数=#单位组
 for n=1,self.加载人数 do
	self.参战单位[#self.参战单位+1]={}
	for i=1,#self.加载数值 do
		self.参战单位[#self.参战单位][self.加载数值[i]]=math.floor(单位组[n][self.加载数值[i]])
	end
	self.参战单位[#self.参战单位].当前气血=math.floor(单位组[n].气血)
	self.参战单位[#self.参战单位].气血上限=math.floor(单位组[n].气血)
	self.参战单位[#self.参战单位].当前魔法=8000
	self.参战单位[#self.参战单位].命中=self.参战单位[#self.参战单位].伤害
	self.参战单位[#self.参战单位].魔法上限=8000
	self.参战单位[#self.参战单位].名称=单位组[n].名称
	self.参战单位[#self.参战单位].等级=单位组[n].等级
	self.参战单位[#self.参战单位].防御=单位组[n].防御 or 单位组[n].等级
	self.参战单位[#self.参战单位].类型=单位组[n].类型
	self.参战单位[#self.参战单位].造型=单位组[n].造型
	self.参战单位[#self.参战单位].编号=#self.参战单位
	self.参战单位[#self.参战单位].法防=self.参战单位[#self.参战单位].法防 or math.floor(单位组[n].灵力* 0.25)
	self.参战单位[#self.参战单位].技能=单位组[n].技能
	self.参战单位[#self.参战单位].战斗类型=单位组[n].战斗类型
	self.参战单位[#self.参战单位].武器数据=单位组[n].武器数据
	self.参战单位[#self.参战单位].染色=单位组[n].染色
	self.参战单位[#self.参战单位].染色组=单位组[n].染色组
	self.参战单位[#self.参战单位].染色方案=单位组[n].染色方案
	self.参战单位[#self.参战单位].饰品=单位组[n].饰品
	self.参战单位[#self.参战单位].阵法=单位组[n].阵法 
	self.参战单位[#self.参战单位].物伤减免=单位组[n].物伤减免 or 1
	self.参战单位[#self.参战单位].法伤减免=单位组[n].法伤减免 or 1
	self.参战单位[#self.参战单位].修炼数据={}
	if 单位组[n].喊话 then
		table.insert(self.战斗发言数据,{内容=单位组[n].喊话,id =#self.参战单位})
	end


		for i=1,#sjsz do
	self.参战单位[#self.参战单位][sjsz[i]]=math.random(单位组[n].等级*1.5,单位组[n].等级*2)
	end
	for n=1,#全局变量.修炼名称 do
		self.参战单位[#self.参战单位].修炼数据[全局变量.修炼名称[n]]=0
		end
	self.参战单位[#self.参战单位].主动技能={}
	self.参战单位[#self.参战单位].特技数据={}
	if 单位组[n].主动技能 ~=nil then
		for i=1,#单位组[n].主动技能 do
		local 剧情技能=置技能(单位组[n].主动技能[i])
		剧情技能.等级 =单位组[n].等级
		table.insert(self.参战单位[#self.参战单位].主动技能,剧情技能)
		end
	end
	if 单位组[n].特技数据 ~=nil then
		for i=1,#单位组[n].特技数据 do
		self.参战单位[#self.参战单位].特技数据[i]={名称=单位组[n].特技数据[i],等级=单位组[n].等级}
		end
	end
	self.参战单位[#self.参战单位].技能={}
	if 单位组[n].技能 then
		for i=1,#单位组[n].技能 do
			self.参战单位[#self.参战单位].技能[i]={名称=单位组[n].技能[i]}
		end
	end

	self.参战单位[#self.参战单位].位置=n
	self.队伍区分[2]=0
	self.参战单位[#self.参战单位].队伍=0
	self.参战单位[#self.参战单位].怪物=true
	self.参战单位[#self.参战单位].队伍id=0
	self.参战单位[#self.参战单位].索引=n
	self.队伍数量[2]=self.队伍数量[2]+1
	if self.战斗类型==56 then
		self.参战单位[#self.参战单位].战斗类型="角色"
		self.参战单位[#self.参战单位].染色={a=0,b=0,c=0}
		self.参战单位[#self.参战单位].武器数据={强化=1,等级=120,名称=任务数据[self.任务id].武器.名称,类别= 取武器类型(任务数据[self.任务id].武器.名称)}
	elseif self.战斗类型==100029 then
		if 任务数据[self.任务id].难度 > 2 then
		self.参战单位[#self.参战单位].阵法=随机阵法
		end
	elseif self.战斗类型==100006 then --抓鬼
		if	self.参战单位[#self.参战单位].造型 == "僵尸" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="我僵尸出了名的血多,想打到我没那么容易!#2"}
		elseif	self.参战单位[#self.参战单位].造型 == "马面" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="我最怕物理伤害的门派了，法术伤害可伤不了我!#2"}
		elseif self.参战单位[#self.参战单位].造型 == "牛头" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="我最怕物理法术的门派了，物理伤害可伤不了我!#2"}
		elseif self.参战单位[#self.参战单位].造型 == "骷髅怪" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="躲避能力可是一流的，想打到我没那么容易!#2"}
		elseif self.参战单位[#self.参战单位].造型 == "野鬼" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="我不怕物理伤害和魔法伤害，双抗高!#2"}
		end
	elseif self.战斗类型==100064 then --师父挑战

		self.参战单位[#self.参战单位].阵法=随机阵法
		if	self.参战单位[#self.参战单位].造型 == "大大王" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="我们这里神仙都不敢来的。"}
		elseif	self.参战单位[#self.参战单位].造型 == "白晶晶" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="盘丝岭从不相信眼泪。"}
		elseif self.参战单位[#self.参战单位].造型 == "二郎神" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="能给我杨戬当徒儿的，都是人中龙凤。"}
		elseif self.参战单位[#self.参战单位].造型 == "巫奎虎" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="我神木林一派擅长操控自然之灵，天地万物均可化为己用，但谨记必须对神灵心存敬畏，方能运用自如。"}
		elseif self.参战单位[#self.参战单位].造型 == "地涌夫人" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="请叫我女王大人。"}
		elseif	self.参战单位[#self.参战单位].造型 == "孙婆婆" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="村里人口虽然不多，却个个都是貌美如花的绝世高手。"}
		elseif self.参战单位[#self.参战单位].造型 == "东海龙王" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="龙宫里有数不尽的宝贝，有机缘之人方能得到。"}
		elseif self.参战单位[#self.参战单位].造型 == "空度禅师" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="人生本在是非场，一生难免会有过。修真正道先修心，悟玄讲道渡世人。"}
		elseif self.参战单位[#self.参战单位].造型 == "程咬金" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="一屋不扫何以天下？修身与治国平天下同等重要。"}
		elseif self.参战单位[#self.参战单位].造型 == "地藏王" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="地狱不空，誓不成佛。"}
		elseif self.参战单位[#self.参战单位].造型 == "菩提祖师" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="天地玄黄修道德，宇宙洪荒炼元神；虎龙啸聚风云鼎，乌兔周旋卯酉晨。"}
		elseif self.参战单位[#self.参战单位].造型 == "李靖" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="天庭冷落，真想再回到人间。"}
		elseif self.参战单位[#self.参战单位].造型 == "牛魔王" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="你们谁瞧见了本王的避水金睛兽？"}
		elseif self.参战单位[#self.参战单位].造型 == "观音姐姐" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="佛祖有真经三藏，乃是修真之经，正善之门，可劝人为善。"}
		elseif self.参战单位[#self.参战单位].造型 == "镇元子" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="我观中的人参果树乃是混沌初分，鸿蒙初判，天地未开之际产成的灵根。"}
		end
	elseif self.战斗类型==888 then
		self.参战单位[#self.参战单位].阵法=随机阵法

		elseif self.战斗类型==100038 then
		self.参战单位[#self.参战单位].阵法=随机阵法
		if	self.参战单位[#self.参战单位].造型 == "凤凰" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="我们这里神仙都不敢来的#32"}
			elseif self.参战单位[#self.参战单位].造型 == "修罗傀儡妖" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="敢挑战我的都是人中龙凤#32"}
		end
		elseif self.战斗类型==100044 then
		self.参战单位[#self.参战单位].阵法=随机阵法
		if	self.参战单位[#self.参战单位].造型 == "蚩尤" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="挑战渡劫第一关#32"}
			elseif self.参战单位[#self.参战单位].造型 == "混沌兽" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="预备#32"}
		end
			elseif self.战斗类型==100045 then
		self.参战单位[#self.参战单位].阵法=随机阵法
		if	self.参战单位[#self.参战单位].造型 == "进阶蚩尤" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="挑战渡劫第二关#89"}
			elseif self.参战单位[#self.参战单位].造型 == "蜃气妖" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="赐教#89"}
		end
		elseif self.战斗类型==100046 then
		self.参战单位[#self.参战单位].阵法=随机阵法
		if	self.参战单位[#self.参战单位].造型 == "进阶蚩尤" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="挑战渡劫第三关#89"}
			elseif self.参战单位[#self.参战单位].造型 == "进阶幽灵" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="有点实力#26"}
			elseif self.参战单位[#self.参战单位].造型 == "进阶雨师" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="不错不错#2"}
			elseif self.参战单位[#self.参战单位].造型 == "进阶吸血鬼" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="在我眼里都是蝼蚁#15"}
			elseif self.参战单位[#self.参战单位].造型 == "进阶蛟龙" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="废什么话看招#80"}
		end
		elseif self.战斗类型==888889 then
		self.参战单位[#self.参战单位].阵法=随机阵法
		if	self.参战单位[#self.参战单位].造型 == "猪八戒" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="看俺老猪厉害#89"}
			elseif self.参战单位[#self.参战单位].造型 == "沙僧" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#Y师弟低调点#26"}
			elseif self.参战单位[#self.参战单位].造型 == "猴子" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#S老孙翻一个跟头就十万八千里#2"}
			elseif self.参战单位[#self.参战单位].造型 == "二郎神" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#g我的狗呢？#55"}
			elseif self.参战单位[#self.参战单位].造型 == "牛魔王" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="废什么话看招#80"}
		end
			elseif self.战斗类型==100005 then
		self.参战单位[#self.参战单位].阵法=随机阵法
		if	self.参战单位[#self.参战单位].造型 == "夜罗刹" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="有本事就把喽啰换成同一种类型#28"}
			elseif self.参战单位[#self.参战单位].造型 == "画魂" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#Y击杀我可能有至宝#26"}
			elseif self.参战单位[#self.参战单位].造型 == "龙龟" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#S把我打成变异有惊喜哦#2"}
			elseif self.参战单位[#self.参战单位].造型 == "幽莹娃娃" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#g有本事就把喽啰换成同一种类型#28"}
		end
 elseif self.战斗类型==888890 then
		self.参战单位[#self.参战单位].阵法=随机阵法
		if	self.参战单位[#self.参战单位].造型 == "化圣剑侠客" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#y不自量力#28"}
			elseif self.参战单位[#self.参战单位].造型 == "云游火" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#Y这一局胜负已定#80"}
			elseif self.参战单位[#self.参战单位].造型 == "巴蛇" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#S让我们来猎杀那些陷入装逼中的人吧#2"}
			elseif self.参战单位[#self.参战单位].造型 == "噬天虎" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#g是时候表演真正的装逼技术了#28"}
		end
 elseif self.战斗类型==888891 then
		self.参战单位[#self.参战单位].阵法=随机阵法
		if	self.参战单位[#self.参战单位].造型 == "化圣神天兵" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#y不自量力#28"}
			elseif self.参战单位[#self.参战单位].造型 == "云游火" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#Y这一局胜负已定#80"}
			elseif self.参战单位[#self.参战单位].造型 == "进阶巴蛇" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#S让我们来猎杀那些陷入装逼中的人吧#2"}
			elseif self.参战单位[#self.参战单位].造型 == "进阶噬天虎" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#g是时候表演真正的装逼技术了#28"}
		end
	elseif self.战斗类型==888892 then
		self.参战单位[#self.参战单位].阵法=随机阵法
		if	self.参战单位[#self.参战单位].造型 == "进阶鬼将" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="来战#28"}
			elseif self.参战单位[#self.参战单位].造型 == "剑侠客" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#Y我们这里神仙都不敢来的#80"}
			elseif self.参战单位[#self.参战单位].造型 == "虎头怪" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#S若天压我,劈开这天#2"}
			elseif self.参战单位[#self.参战单位].造型 == "巨魔王" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#g若地拘我,踏碎这地#28"}
		end
	elseif self.战斗类型==888893 then
		self.参战单位[#self.参战单位].阵法=随机阵法
		if	self.参战单位[#self.参战单位].造型 == "进阶毗舍童子" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="来战#28"}
			elseif self.参战单位[#self.参战单位].造型 == "舞天姬" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#Y我们这里神仙都不敢来的#80"}
			elseif self.参战单位[#self.参战单位].造型 == "龙太子" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#S若天压我,劈开这天#2"}
			elseif self.参战单位[#self.参战单位].造型 == "虎头怪" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#g若地拘我,踏碎这地#28"}
		end
	elseif self.战斗类型==888894 then
		self.参战单位[#self.参战单位].阵法=随机阵法
		if	self.参战单位[#self.参战单位].造型 == "进阶真陀护法" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="来战#28"}
			elseif self.参战单位[#self.参战单位].造型 == "鬼潇潇" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#Y我们这里神仙都不敢来的#80"}
			elseif self.参战单位[#self.参战单位].造型 == "桃夭夭" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#S若天压我,劈开这天#2"}
			elseif self.参战单位[#self.参战单位].造型 == "玄彩娥" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#g若地拘我,踏碎这地#28"}
		end
	elseif self.战斗类型==888895 then
		self.参战单位[#self.参战单位].阵法=随机阵法
		if	self.参战单位[#self.参战单位].造型 == "饰品_大力金刚" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="来战#28"}
			elseif self.参战单位[#self.参战单位].造型 == "巨魔王" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#Y我们这里神仙都不敢来的#80"}
			elseif self.参战单位[#self.参战单位].造型 == "剑侠客" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#S若天压我,劈开这天#2"}
			elseif self.参战单位[#self.参战单位].造型 == "剑侠客" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#g若地拘我,踏碎这地#28"}
		end
 
	
	elseif self.战斗类型==2022 then
		self.参战单位[#self.参战单位].阵法=随机阵法
		if	self.参战单位[#self.参战单位].造型 == "化圣剑侠客" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="我是你无法逾越的人#28"}
		end
		elseif self.战斗类型==2023 then
		self.参战单位[#self.参战单位].阵法=随机阵法
		if	self.参战单位[#self.参战单位].造型 == "化圣龙太子" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#G我是你无法逾越的仙#28"}
		end
			elseif self.战斗类型==2024 then
		self.参战单位[#self.参战单位].阵法=随机阵法
		if	self.参战单位[#self.参战单位].造型 == "化圣虎头怪" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#y我是你无法逾越的魔#28"}
		end
			elseif self.战斗类型==2025 then
		self.参战单位[#self.参战单位].阵法=随机阵法
		if	self.参战单位[#self.参战单位].造型 == "化圣骨精灵" then
			self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="#Y我是你无法逾越的魔#28"}
		end
		end
	end
 end
function FightControl:创建野外单位()
 self.地图名称=MapData[UserData[self.发起id].地图].名称
 self.地图编号 = UserData[self.发起id].地图
 self.地图等级=MapData[UserData[self.发起id].地图].等级
 self.地图经验=MapData[UserData[self.发起id].地图].经验
 self.地图物品=MapData[UserData[self.发起id].地图].物品/10
	local bs =MapData[UserData[self.发起id].地图].怪物
	local 数量 = 0
	if UserData[self.发起id].队伍==0 then
	数量 = 数量 + math.random(3)
	else
	数量 = math.random(#队伍数据[UserData[self.发起id].队伍].队员数据+1,#队伍数据[UserData[self.发起id].队伍].队员数据*2+1)
	if 数量>10 then 数量=10 end
 end
	local 初始化野怪表 = {}
	local	l= #bs
	for w=1,数量 do
	if math.random(15) == 1 then
		初始化野怪表[w] = bs[math.random(1,l)] + 1
	elseif math.random(25) == 1 then
		初始化野怪表[w] = bs[math.random(1,l)]	+ 2
	elseif math.random(40) == 1 then
		初始化野怪表[w] = bs[math.random(1,l)]	+ 3
	elseif math.random(10) == 1 then
		初始化野怪表[w]=-501312
			
	else
		初始化野怪表[w] = bs[math.random(1,l)]

	end
	end

	for i=1,#初始化野怪表 do
	初始化野怪表[i] = 取敌人信息(初始化野怪表[i])
	end
 self.队伍区分[2]=0
 for n=1,#初始化野怪表 do
	self.怪物组=初始化野怪表
	self.参战单位[#self.参战单位+1]={}
	self.参战单位[#self.参战单位] = 创建敌人(初始化野怪表[n])
		if self.参战单位[#self.参战单位].种类 == 2 then
		self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="其实我一个小小的宝宝,你们别抓我呀!#52"}
		elseif self.参战单位[#self.参战单位].种类 == 3 then
	self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="诶啊..我..我..我什么变态了,不对是换色了,你们别抓我呀!#52"}
		elseif	self.参战单位[#self.参战单位].名称==self.参战单位[#self.参战单位].造型.."精灵" then
		self.战斗发言数据[#self.战斗发言数据+1]={id=#self.参战单位,内容="我是一只小精灵,小啊小精灵!#52"}
		end
	self.参战单位[#self.参战单位].位置=n
	self.参战单位[#self.参战单位].队伍=0
	self.参战单位[#self.参战单位].编号=#self.参战单位
	self.参战单位[#self.参战单位].队伍id=0
	self.队伍数量[2]=self.队伍数量[2]+1
	end
 end
function FightControl:数据处理(id,序号,内容,参数)
	if 序号==231 then---发送参战单位信息
		for i=1,#self.参战单位 do
			if self.参战单位[i].当前气血 > 0 then
				self.参战单位[i].单位消失 = nil
			end
			SendMessage(UserData[id].连接id,6003,self.参战单位[i])
		end
	elseif 序号==232	then
		if self.回合进程=="加载回合" then
			self.临时玩家id=self:取参战玩家id(id)
			self.参战玩家[self.临时玩家id].加载=true
			self.加载数量=self.加载数量+1
			if self.加载数量==#self.参战玩家 then
				self:结束加载回合()
			end
		elseif self.回合进程=="命令回合" and self.掉线玩家 then
			local 命令单位={}
			for i=1,#self.参战单位 do
				if self.参战单位[i].玩家id==self.掉线玩家 and self.参战单位[i].战斗类型~= "召唤类" and self.参战单位[i].单位消失==nil then
					--命令单位[#命令单位+1]=i
					
					命令单位[#命令单位+1]={编号=i,名称=self.参战单位[i].名称,自动=self.参战单位[i].自动,助战=self.参战单位[i].助战}
				end
			end
			SendMessage(UserData[self.掉线玩家].连接id,6004,命令单位)
			self.掉线玩家=nil
		end
	elseif 序号==235 and self.回合进程=="命令回合" then
		local 命令信息=分割文本(内容,"*-*")
		local 信息={}
		for n=1,#命令信息 do
			self.分割信息=分割文本(命令信息[n],"@-@")
			if self.分割信息[1]~="" then
				信息[#信息+1]={编号=self.分割信息[1]+0,目标=self.分割信息[2]+0,类型=self.分割信息[3],参数=self.分割信息[4],附加=self.分割信息[5]}
			end
		end
		for n=1,#信息 do
			if 信息[n].目标 ~= 0 or 信息[n].类型 == "逃跑" or 信息[n].类型 == "防御" or not self.参战单位[信息[n].编号].法术状态组.横扫千军 or not self.参战单位[信息[n].编号].法术状态组.破釜沉舟 or not self.参战单位[信息[n].编号].法术状态组.天崩地裂 or not self.参战单位[信息[n].编号].法术状态组.血雨 then
				self.参战单位[信息[n].编号].命令数据={类型=信息[n].类型,目标=信息[n].目标,附加=信息[n].附加,参数=信息[n].参数,下达=true}
				if self.参战单位[信息[n].编号].法术状态组.楚楚可怜 then
					self.参战单位[信息[n].编号].命令数据={类型="取消",目标=0,附加=信息[n].附加,参数=信息[n].参数,下达=true}
				elseif 信息[n].类型=="技能" and self:SkillExists(信息[n].编号,信息[n].参数)==false then 
					SendMessage(UserData[self.参战单位[信息[n].编号].数字id].连接id,7,"#y/使用法术#r/"..信息[n].参数.."#y/数据错误")
					SendMessage(UserData[self.参战单位[信息[n].编号].数字id].连接id,9,"#dq/#y/使用法术#r/"..信息[n].参数.."#y/数据错误")
					self.参战单位[信息[n].编号].命令数据={类型="取消",目标=0,附加=信息[n].附加,参数=信息[n].参数,下达=true}
				end
			end
			if self.参战单位[信息[n].编号].助战 then
				if self.参战单位[信息[n].编号].命令数据.类型 == "逃跑" then
					self.参战单位[信息[n].编号].命令数据.类型 = "攻击"
					常规提示(self.参战单位[信息[n].编号].玩家id,"#Y助战无法设置逃跑，已自动转为普通攻击.")
				end
				UserData[self.参战单位[信息[n].编号].玩家id].助战.数据[self.参战单位[信息[n].编号].助战编号].上次行动 = table.copy(self.参战单位[信息[n].编号].命令数据)
				if self.参战单位[信息[n].编号].命令数据.参数 == "鹰击" or self.参战单位[信息[n].编号].命令数据.参数 == "狮搏" then
					if not self.参战单位[信息[n].编号].法术状态组.变身 then
						self.参战单位[信息[n].编号].命令数据.参数 = "变身"
						self.参战单位[信息[n].编号].命令数据.目标 = 信息[n].编号
					end
				end
			end
		end
		self.接收数量=self.接收数量-1
		if self.接收数量<=0 then
			self:命令回合处理()
		end
	elseif 序号==234 then
		if UserData[id].战斗时间 ==tonumber(内容) then
			if self.回合进程=="执行回合"	then
				self.接收数量=self.接收数量-1
				if self.接收数量<=0 then
					self:执行回合处理()
				end
			end
		else
			封禁账号(UserData[id],"跳过战斗")
			return
		end
	elseif 序号==238 then
	self:设置默认法术(id,内容,参数)
	elseif 序号==236 then
	SendMessage(UserData[id].连接id,6008,ItemControl:索要道具1(id,"包裹")) --获取道具数据
	elseif 序号==237 then
	self:获取召唤兽数据(id)
	elseif 序号==240 then
		SendMessage(UserData[id].连接id,6011,ItemControl:索要道具1(id,"法宝")) --获取法宝数据
	elseif 序号==239 then
		for n=1,#self.观战玩家 do
			if self.观战玩家[n]==id then
				UserData[id].战斗=0
				UserData[id].观战状态=false
				SendMessage(UserData[self.观战玩家[n]].连接id,6007,"66")
				self.观战玩家[n]=0
			end
		end
	elseif 序号==241 then
		for n,v in pairs(self.参战单位) do
			if v.助战 then
				self.参战单位[n].自动 = true
				UserData[self.参战单位[n].玩家id].助战.数据[self.参战单位[n].助战编号].自动 = self.参战单位[n].自动
			end
		end
	elseif 序号==242 then
		local 内容 = table.loadstring(内容)
		if self.参战单位[内容.编号] ~= nil then
			if self.参战单位[内容.编号].自动 then
				self.参战单位[内容.编号].自动 = false
				常规提示(id,"#Y你的助战 #G"..self.参战单位[内容.编号].名称.." #Y已取消托管。")
			else
				self.参战单位[内容.编号].自动 = true
				常规提示(id,"#Y你的助战 #G"..self.参战单位[内容.编号].名称.." #Y开始托管。")
			end
			UserData[self.参战单位[内容.编号].玩家id].助战.数据[self.参战单位[内容.编号].助战编号].自动 = self.参战单位[内容.编号].自动
			SendMessage(UserData[id].连接id,6018,{编号=内容.编号,自动=self.参战单位[内容.编号].自动})
		end
	end
end

function FightControl:SkillExists(id,Sklil)
	for k,v in pairs(self.参战单位[id].主动技能) do
	if v.名称 == Sklil then 
		return	true
	end
	end
	return false 
end
function FightControl:获取召唤兽数据(id)
 self.符合id=0
 for n=1,#self.参战单位 do
	if self.参战单位[n].战斗类型=="角色" and self.参战单位[n].玩家id==id then
		self.符合id=n
	end
end
 if self.符合id~=0 then
	self.发送信息={}
	for n=1,#UserData[id].召唤兽.数据 do
	self.发送信息[n]={编号=n,等级=UserData[id].召唤兽.数据[n].等级,名称=UserData[id].召唤兽.数据[n].名称,当前气血=UserData[id].召唤兽.数据[n].当前气血,气血上限=UserData[id].召唤兽.数据[n].气血上限,当前魔法=UserData[id].召唤兽.数据[n].当前魔法,魔法上限=UserData[id].召唤兽.数据[n].魔法上限,忠诚=UserData[id].召唤兽.数据[n].忠诚}
	self.发送信息[n].参战=false
	for i=1,#self.参战单位[self.符合id].参战召唤兽 do
		if n==self.参战单位[self.符合id].参战召唤兽[i] then
		self.发送信息[n].参战=true
			end
		end
		end
	SendMessage(UserData[id].连接id,6009,self.发送信息)
	end
 end

function FightControl:设置默认法术(id,参数,编号)
 编号=编号+0
 if 参数=="0" then 参数=nil end
 self.参战单位[编号].默认法术=参数
 if self.参战单位[编号].战斗类型=="角色" then
	UserData[id].角色.默认法术=参数
	else
	UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].默认法术=参数
	end
 end
function FightControl:取参战玩家id(id)
	for n=1,#self.参战玩家 do
	if self.参战玩家[n].玩家id==id then
		return n
	end
	end
	return 0
 end
function FightControl:退出观战(id)
 for n=1,#self.观战玩家 do
	if self.观战玩家[n]==id then
		SendMessage(UserData[self.观战玩家[n]].连接id,6007,"66")
		self.观战玩家[n]=0
		end
	end
	end
function FightControl:观战数据发送(id,目标id)
 self.观战玩家[#self.观战玩家+1]=id
 for n=1,#self.参战玩家 do
	if self.参战玩家[n].玩家id==目标id then
		SendMessage(UserData[id].连接id,6001,self.参战玩家[n].队伍id)
		SendMessage(UserData[id].连接id,6010,#self.参战单位)
		end
	end
	end
function FightControl:重连数据发送(id)
 for n=1,#self.参战玩家 do
	if self.参战玩家[n].玩家id==id then
		SendMessage(UserData[self.参战玩家[n].玩家id].连接id,6001,self.参战玩家[n].队伍id)
		if self.战斗类型 == 999 then
			SendMessage(UserData[self.参战玩家[n].玩家id].连接id,6012,#self.参战单位)
			else
			SendMessage(UserData[self.参战玩家[n].玩家id].连接id,6002,#self.参战单位)
		end
		end
	end
	self.掉线玩家=id
 end
function FightControl:重置战斗属性()
	for n=1,#self.参战单位 do
	self.参战单位[n].必杀=5
	self.参战单位[n].法宝效果={}
	self.参战单位[n].奇经八脉={}
	self.参战单位[n].神器=""
	self.参战单位[n].战意点数=0
	self.参战单位[n].攻之械=0
	self.参战单位[n].暴击伤害=0
	self.参战单位[n].识药=0
	self.参战单位[n].吸血=0
	self.参战单位[n].法术吸血=0
	self.参战单位[n].阴伤=0
	self.参战单位[n].驱散=0	---小法	内丹
	self.参战单位[n].连击=0
	self.参战单位[n].法术暴击= 0
	self.参战单位[n].额外法术伤害=0
	self.参战单位[n].理直气壮=false
	self.参战单位[n].反震=0
	self.参战单位[n].反击=0
	self.参战单位[n].法暴=0
	self.参战单位[n].法连=0
	self.参战单位[n].法波=0
	self.参战单位[n].魔心=1
	self.参战单位[n].神佑=0
	self.参战单位[n].复活=0
	self.参战单位[n].冥想=0
	self.参战单位[n].慧根=1
	self.参战单位[n].再生=0
	self.参战单位[n].毒=0
	self.参战单位[n].驱鬼=0
	self.参战单位[n].火吸=0
	self.参战单位[n].水吸=0
	self.参战单位[n].雷吸=0
	self.参战单位[n].土吸=0
	self.参战单位[n].隐身=0
	self.参战单位[n].物伤减免=1
	self.参战单位[n].法伤减免=1
	self.参战单位[n].武器伤害=0
	self.参战单位[n].封印状态=false
	self.参战单位[n].法术状态组={}
	if self.参战单位[n].特技数据 ==nil then
	self.参战单位[n].特技数据={}
	end
	for i=1,#幻化战斗属性 do
	self.参战单位[n][幻化战斗属性[i]]=0
	end
	if self.参战单位[n].战斗类型~="角色" then
		self:添加技能属性(n,self.参战单位[n].技能)
		if self.参战单位[n].附加追加技能~=nil and #self.参战单位[n].附加追加技能>0 then
			self:添加技能属性(n,self.参战单位[n].附加追加技能)
		end
		if self.参战单位[n].内丹 ~= nil then
			self:添加内丹属性(n,self.参战单位[n].内丹)
			self:添加特性属性(n,self.参战单位[n].特性,self.参战单位[n].特性几率)
		end
	elseif self.参战单位[n].战斗类型=="角色" and self.参战单位[n].队伍~=0 then
		if DebugMode then
		--	table.insert(self.参战单位[n].主动技能,{名称="落叶萧萧",等级=500,种类=4})
		--	self.参战单位[n].法术伤害结果 =10000000
		--	self.参战单位[n].速度 =100000
		end
		for i=1,#幻化战斗属性 do
			self.参战单位[n][幻化战斗属性[i]]=self.参战单位[n][幻化战斗属性[i]]+ UserData[self.参战单位[n].玩家id].角色.装备属性[幻化战斗属性[i]]
		end
		if self.参战单位[n].变身造型技能~=0 then
			self:添加技能属性(n,{self.参战单位[n].变身造型技能})
		end
		--加载幻化属性

		self.参战单位[n].特技数据=UserData[self.参战单位[n].玩家id].角色.特技数据
		if UserData[self.参战单位[n].玩家id].角色.装备数据[23]~=nil and UserData[self.参战单位[n].玩家id].物品[UserData[self.参战单位[n].玩家id].角色.装备数据[23]]~=nil and UserData[self.参战单位[n].玩家id].物品[UserData[self.参战单位[n].玩家id].角色.装备数据[23]]~=0	then
			self.参战单位[n].武器伤害=UserData[self.参战单位[n].玩家id].物品[UserData[self.参战单位[n].玩家id].角色.装备数据[23]].伤害
			if self.参战单位[n].武器伤害==nil then
				self.参战单位[n].武器伤害=0
			end
		end
		self.参战单位[n].法宝效果=UserData[self.参战单位[n].玩家id].角色.法宝效果
		self.参战单位[n].奇经八脉=UserData[self.参战单位[n].玩家id].角色.奇经八脉


		for v = 35, 38 do
			if self.参战单位[n].法宝 ~= nil and self.参战单位[n].法宝[v] ~= nil and self.参战单位[n].法宝[v]~=0 then
				local TempItem = UserData[self.参战单位[n].玩家id].物品[self.参战单位[n].法宝[v]]
				if TempItem.灵气>=1 then
					TempItem.灵气=TempItem.灵气-1
					SendMessage(UserData[self.参战单位[n].玩家id].连接id,9,"#dq/#y/当的#G/"..TempItem.名称.."#Y/灵气为#R/"..TempItem.灵气) 
				else
					SendMessage(UserData[self.参战单位[n].玩家id].连接id,9,"#dq/#y/当的#G/"..TempItem.名称.."#Y/灵气不足无法生效,请及时补充灵气") 
				end
			end
		end

	

		if UserData[self.参战单位[n].玩家id].角色.神器 then
			if UserData[self.参战单位[n].玩家id].角色.神器.灵气>1 then
				self.参战单位[n].神器=UserData[self.参战单位[n].玩家id].角色.神器.技能
				UserData[self.参战单位[n].玩家id].角色.神器.灵气=UserData[self.参战单位[n].玩家id].角色.神器.灵气-1
				SendMessage(UserData[self.参战单位[n].玩家id].连接id,9,"#dq/#y/当的#G/"..UserData[self.参战单位[n].玩家id].角色.神器.名称.."#Y/灵气为#R/"..UserData[self.参战单位[n].玩家id].角色.神器.灵气) 
			else
				SendMessage(UserData[self.参战单位[n].玩家id].连接id,9,"#dq/#y/当的#G/"..UserData[self.参战单位[n].玩家id].角色.神器.名称.."#Y/灵气不足无法生效,请及时补充灵气") 
			end
		
		end 

		if self.参战单位[n].门派 == "凌波城" then
			self.参战单位[n].必杀 = self.参战单位[n].必杀 +5
			self.参战单位[n].战意点数=2
		elseif self.参战单位[n].门派 == "阴曹地府" then
		self.参战单位[n].夜战=true
		elseif self.参战单位[n].门派 == "方寸山" then
		self.参战单位[n].驱鬼=2
		elseif self.参战单位[n].门派 == "天机城" then
			self.参战单位[n].攻之械=1
		end
		if self.参战单位[n].神器 =="定风波" then
			self.参战单位[n].法暴 = self.参战单位[n].法暴 +15
		elseif self.参战单位[n].神器 =="酣战" then
			self.参战单位[n].战意点数=self.参战单位[n].战意点数+3
		elseif self.参战单位[n].神器 =="狂战" then
			self.参战单位[n].伤害=math.floor(self.参战单位[n].伤害*1.15)
			self.参战单位[n].防御=math.floor(self.参战单位[n].防御*0.95)
		elseif self.参战单位[n].神器 =="万物滋长" and (math.random(100)<= 20 or DebugMode) then
			self.参战单位[n].伤害=math.floor(self.参战单位[n].伤害*1.3)
			self.参战单位[n].齐天大圣=true
		elseif self.参战单位[n].神器 =="盏中晴雪=" and math.random(100)<= 10 then	
				self.参战单位[n].速度 =self.参战单位[n].速度*1.2
				self.参战单位[n].防御 =self.参战单位[n].防御*0.9
				self.参战单位[n].灵力 =self.参战单位[n].灵力*0.9
		elseif self.参战单位[n].神器 =="镜花水月" then
			self.参战单位[n].防御=math.floor(self.参战单位[n].防御*0.85)
			self.参战单位[n].当前气血=math.floor(self.参战单位[n].当前气血*0.85)
			self.参战单位[n].封印命中等级=self.参战单位[n].封印命中等级+self.参战单位[n].等级
		end
		
		if self.参战单位[n].门派 == "凌波城" then
			if self.参战单位[n].助战 == nil and UserData[self.参战单位[n].玩家id] ~= nil and UserData[self.参战单位[n].玩家id].角色.元神 ~= nil and UserData[self.参战单位[n].玩家id].角色.元神 > 0 then
				local 元神加成下限 = SpiritData[UserData[self.参战单位[n].玩家id].角色.门派][UserData[self.参战单位[n].玩家id].角色.元神.."介效果2下限"]
				local 元神加成上限 = SpiritData[UserData[self.参战单位[n].玩家id].角色.门派][UserData[self.参战单位[n].玩家id].角色.元神.."介效果2上限"]
				if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
					self.参战单位[n].战意点数 = self.参战单位[n].战意点数 + 取随机数(元神加成下限,元神加成上限)
				end
			end
		end
		
		local user =UserData[self.参战单位[n].玩家id]

		if user.角色.灵饰[31] ~= 0 then
		if user.物品[user.角色.灵饰[31]].特性	and user.物品[user.角色.灵饰[31]].特性.套装 then
		--if PropertyData[user.物品[user.角色.灵饰[31]].特性.技能].类型 =="战斗效果" or PropertyData[user.物品[user.角色.灵饰[31]].特性.技能].类型 =="特技" then

			if	user.物品[user.角色.灵饰[31]].特性.技能 == "心源" and math.random(100)<=2 then

				self.参战单位[n].伤害=math.floor(self.参战单位[n].伤害* (PropertyData[user.物品[user.角色.灵饰[31]].特性.技能]["套装"..user.物品[user.角色.灵饰[31]].特性.套装]/100+1))
				self.参战单位[n].灵力=math.floor(self.参战单位[n].灵力* (PropertyData[user.物品[user.角色.灵饰[31]].特性.技能]["套装"..user.物品[user.角色.灵饰[31]].特性.套装]/100+1))
			end
		end
		end

	for i=21,26 do
			if user.物品[user.角色.装备数据[i]]~=nil and user.物品[user.角色.装备数据[i]]~=0 then
			if user.物品[user.角色.装备数据[i]].符石~= nil then
				if user.物品[user.角色.装备数据[i]].符石.组合~=nil then
				local 临时等级= user.物品[user.角色.装备数据[i]].符石.组合.等级
				if user.物品[user.角色.装备数据[i]].符石.组合.名称=="无心插柳" then
					if 临时等级 == 1 then
						self.参战单位[n].溅射=0.15
					elseif	临时等级== 2 then
						self.参战单位[n].溅射=0.2
					elseif	临时等级== 3 then
						self.参战单位[n].溅射=0.25
					elseif	临时等级==4 then
						self.参战单位[n].溅射=0.3
					end
				elseif user.物品[user.角色.装备数据[i]].符石.组合.名称=="百步穿杨" then
					if 临时等级 == 1 then
						self.参战单位[n].百步穿杨=200
					elseif	临时等级== 2 then
						self.参战单位[n].百步穿杨=450
					elseif	临时等级== 3 then
						self.参战单位[n].百步穿杨=600
					elseif	临时等级==4 then
						self.参战单位[n].百步穿杨=800
					end
				elseif user.物品[user.角色.装备数据[i]].符石.组合.名称=="隔山打牛" then
					if 临时等级 == 1 then
						self.参战单位[n].隔山打牛=80
					elseif	临时等级== 2 then
						self.参战单位[n].隔山打牛=120
					elseif	临时等级== 3 then
						self.参战单位[n].隔山打牛=170
					elseif	临时等级==4 then
						self.参战单位[n].隔山打牛=200
					end
				elseif user.物品[user.角色.装备数据[i]].符石.组合.名称=="心随我动" then
					if 临时等级 == 1 then
						self.参战单位[n].心随我动=250
					elseif	临时等级== 2 then
						self.参战单位[n].心随我动=400
					elseif	临时等级== 3 then
						self.参战单位[n].心随我动=700
					elseif	临时等级==4 then
						self.参战单位[n].心随我动=900
					end
				elseif user.物品[user.角色.装备数据[i]].符石.组合.名称=="云随风舞" then
					if 临时等级 == 1 then
						self.参战单位[n].云随风舞=200
					elseif	临时等级== 2 then
						self.参战单位[n].云随风舞=400
					elseif	临时等级== 3 then
						self.参战单位[n].云随风舞=700
					elseif	临时等级==4 then
						self.参战单位[n].云随风舞=800
					end
				elseif user.物品[user.角色.装备数据[i]].符石.组合.名称=="网罗乾坤" then
					if 临时等级 == 1 then
						self.参战单位[n].网罗乾坤=math.floor(self.参战单位[n].等级/2)
					elseif	临时等级== 2 then
						self.参战单位[n].网罗乾坤=math.floor(self.参战单位[n].等级/1.5)
					elseif	临时等级== 3 then
						self.参战单位[n].网罗乾坤=self.参战单位[n].等级
					end
				elseif user.物品[user.角色.装备数据[i]].符石.组合.名称=="石破天惊" then
					if 临时等级 == 1 then
						self.参战单位[n].石破天惊=math.floor(self.参战单位[n].等级/2)
					elseif	临时等级== 2 then
						self.参战单位[n].石破天惊=math.floor(self.参战单位[n].等级/1.5)
					elseif	临时等级== 3 then
						self.参战单位[n].石破天惊=self.参战单位[n].等级
					end
				elseif user.物品[user.角色.装备数据[i]].符石.组合.名称=="天雷地火" then
					if 临时等级 == 1 then
						self.参战单位[n].天雷地火=math.floor(self.参战单位[n].等级/2)
					elseif	临时等级== 2 then
						self.参战单位[n].天雷地火=math.floor(self.参战单位[n].等级/1.5)
					elseif	临时等级== 3 then
						self.参战单位[n].天雷地火=self.参战单位[n].等级
					end
				elseif user.物品[user.角色.装备数据[i]].符石.组合.名称=="烟雨飘摇" then
					if 临时等级 == 1 then
						self.参战单位[n].烟雨飘摇=math.floor(self.参战单位[n].等级/2)
					elseif	临时等级== 2 then
						self.参战单位[n].烟雨飘摇=math.floor(self.参战单位[n].等级/1.5)
					elseif	临时等级== 3 then
						self.参战单位[n].烟雨飘摇=self.参战单位[n].等级
					end
				elseif user.物品[user.角色.装备数据[i]].符石.组合.名称=="索命无常" then
					if 临时等级 == 1 then
						self.参战单位[n].索命无常=math.floor(self.参战单位[n].等级/2)
					elseif	临时等级== 2 then
						self.参战单位[n].索命无常=math.floor(self.参战单位[n].等级/1.5)
					elseif	临时等级== 3 then
						self.参战单位[n].索命无常=self.参战单位[n].等级
					end
				elseif user.物品[user.角色.装备数据[i]].符石.组合.名称=="行云流水" then
					if 临时等级 == 1 then
						self.参战单位[n].行云流水=math.floor(self.参战单位[n].等级/2)
					elseif	临时等级== 2 then
						self.参战单位[n].行云流水=math.floor(self.参战单位[n].等级/1.5)
					elseif	临时等级== 3 then
						self.参战单位[n].行云流水=self.参战单位[n].等级
					end
				elseif user.物品[user.角色.装备数据[i]].符石.组合.名称=="福泽天下" then
					if 临时等级 == 1 then
						self.参战单位[n].福泽天下=math.floor(self.参战单位[n].等级/2)
					elseif	临时等级== 2 then
						self.参战单位[n].福泽天下=math.floor(self.参战单位[n].等级/1.5)
					elseif	临时等级== 3 then
						self.参战单位[n].福泽天下=self.参战单位[n].等级
					end
				elseif user.物品[user.角色.装备数据[i]].符石.组合.名称=="暗度陈仓" then
							self.参战单位[n].物伤减免 = self.参战单位[n].物伤减免-0.03
				elseif user.物品[user.角色.装备数据[i]].符石.组合.名称=="化敌为友" then
									self.参战单位[n].法伤减免=self.参战单位[n].法伤减免-0.03
						end
				end
			end
			end
	end
	end
	if self.参战单位[n].队伍~=0 and 昼夜参数==1 and not self.参战单位[n].夜战 then
		self.参战单位[n].伤害=math.floor(self.参战单位[n].伤害*0.85)
		self.参战单位[n].防御=math.floor(self.参战单位[n].防御*0.85)
		self.参战单位[n].法防=math.floor(self.参战单位[n].法防*0.85)--+self.参战单位[n].法术防御
	end
	end
end
---------------------
function FightControl:添加进场特性属性(编号,特性,几率)
	if 特性 =="灵法" then
	self.参战单位[编号].法术状态组["灵法"]={}
	local 灵法概率 = 0
	if 几率 == 1 then
		self.参战单位[编号].法术状态组["灵法"].灵力 =math.floor(self.参战单位[编号].灵力*0.3)
		self.参战单位[编号].法术状态组["灵法"].防御 = math.floor(self.参战单位[编号].防御*10/100)
		灵法概率=33
	elseif 几率 == 2 then
		self.参战单位[编号].法术状态组["灵法"].灵力 =math.floor(self.参战单位[编号].灵力*0.5)
		self.参战单位[编号].法术状态组["灵法"].防御 = math.floor(self.参战单位[编号].防御*5/100)
		灵法概率=50
	elseif 几率 == 3 then
		self.参战单位[编号].法术状态组["灵法"].灵力 =math.floor(self.参战单位[编号].灵力*0.66)
		self.参战单位[编号].法术状态组["灵法"].防御 = math.floor(self.参战单位[编号].防御*5/100)
		灵法概率=66
	elseif 几率 == 4 then
		self.参战单位[编号].法术状态组["灵法"].灵力 =math.floor(self.参战单位[编号].灵力*0.8)
		self.参战单位[编号].法术状态组["灵法"].防御 = math.floor(self.参战单位[编号].防御*5/100)
		灵法概率=83
	elseif 几率 == 5 then
		self.参战单位[编号].法术状态组["灵法"].灵力 =(self.参战单位[编号].灵力*1)
		self.参战单位[编号].法术状态组["灵法"].防御 = math.floor(self.参战单位[编号].防御*5/100)
		灵法概率=100
	end
		if 灵法概率~= 0 and 灵法概率 >= math.random(100) then
		self.参战单位[编号].法术状态组["灵法"].回合=4
		self.参战单位[编号].灵力 =self.参战单位[编号].灵力+self.参战单位[编号].法术状态组["灵法"].灵力
		self.参战单位[编号].防御 =self.参战单位[编号].防御-self.参战单位[编号].法术状态组["灵法"].防御
		self.参战单位[编号].法术状态组["灵法"].当前气血 = math.floor(self.参战单位[编号].当前气血*0.3)
		self.参战单位[编号].当前气血 =self.参战单位[编号].当前气血-self.参战单位[编号].法术状态组["灵法"].当前气血
	end
	elseif 特性 =="灵断" then
	
	local 灵断概率 = 0
	if 几率 == 1 then
		灵断概率=33
	elseif 几率 == 2 then
		灵断概率=50
	elseif 几率 == 3 then
		灵断概率=66
	elseif 几率 == 4 then
		灵断概率=83
	elseif 几率 == 5 then
		灵断概率=100
	end
		if 灵断概率~= 0 and 灵断概率 >= math.random(100)	then
			self.参战单位[编号].法术状态组["灵断"]={}
			self.参战单位[编号].法术状态组["灵断"].灵力 = math.floor(self.参战单位[编号].灵力*50/100)
			self.参战单位[编号].法术状态组["灵断"].回合=4
			self.参战单位[编号].灵力 =self.参战单位[编号].灵力+self.参战单位[编号].法术状态组["灵断"].灵力
			self.参战单位[编号].法术状态组["灵断"].当前气血 = math.floor(self.参战单位[编号].当前气血*0.3)
			self.参战单位[编号].当前气血 =self.参战单位[编号].当前气血-self.参战单位[编号].法术状态组["灵断"].当前气血
	end
	elseif 特性 =="怒吼" then
	self.参战单位[编号].法术状态组["怒吼"]={}
		self.参战单位[编号].法术状态组["怒吼"].回合=4
	if 几率 == 1 then
		self.参战单位[编号].法术状态组["怒吼"].伤害 =50
		self.参战单位[编号].法术状态组["怒吼"].灵力 = math.floor(self.参战单位[编号].灵力*6/100)
	elseif 几率 == 2 then
		self.参战单位[编号].法术状态组["怒吼"].伤害 =71
		self.参战单位[编号].法术状态组["怒吼"].灵力 = math.floor(self.参战单位[编号].灵力*6/100)
	elseif 几率 == 3 then
		self.参战单位[编号].法术状态组["怒吼"].伤害 =109
		self.参战单位[编号].法术状态组["怒吼"].灵力 = math.floor(self.参战单位[编号].灵力*10/100)
	elseif 几率 == 4 then
		self.参战单位[编号].法术状态组["怒吼"].伤害 =160
		self.参战单位[编号].法术状态组["怒吼"].灵力 = math.floor(self.参战单位[编号].灵力*10/100)
	elseif 几率 == 5 then
		self.参战单位[编号].法术状态组["怒吼"].伤害 =180
		self.参战单位[编号].法术状态组["怒吼"].灵力 = math.floor(self.参战单位[编号].灵力*10/100)
	end
		self.参战单位[编号].灵力 = self.参战单位[编号].灵力- self.参战单位[编号].法术状态组["怒吼"].灵力
		self.参战单位[编号].伤害 =self.参战单位[编号].伤害+ self.参战单位[编号].法术状态组["怒吼"].伤害
	elseif 特性 =="御风" then
	self.参战单位[编号].法术状态组["御风"]={}
			self.参战单位[编号].法术状态组["御风"].回合=4
	if 几率 == 1 then
		self.参战单位[编号].法术状态组["御风"].速度 =math.floor(self.参战单位[编号].速度*6/100)
		self.参战单位[编号].法术状态组["御风"].防御 =math.floor(self.参战单位[编号].防御*20/100)
	elseif 几率 == 2 then
		self.参战单位[编号].法术状态组["御风"].速度 =math.floor(self.参战单位[编号].速度*10/100)
		self.参战单位[编号].法术状态组["御风"].防御 =math.floor(self.参战单位[编号].防御*20/100)
	elseif 几率 == 3 then
		self.参战单位[编号].法术状态组["御风"].速度 =math.floor(self.参战单位[编号].速度*14/100)
		self.参战单位[编号].法术状态组["御风"].防御 =math.floor(self.参战单位[编号].防御*10/100)
	elseif 几率 == 4 then
			self.参战单位[编号].法术状态组["御风"].速度 =math.floor(self.参战单位[编号].速度*17/100)
		self.参战单位[编号].法术状态组["御风"].防御 =math.floor(self.参战单位[编号].防御*10/100)
	elseif 几率 == 5 then
		self.参战单位[编号].法术状态组["御风"].速度 =math.floor(self.参战单位[编号].速度*21/100)
		self.参战单位[编号].法术状态组["御风"].防御 =math.floor(self.参战单位[编号].防御*10/100)
	end
			self.参战单位[编号].防御 = self.参战单位[编号].防御- self.参战单位[编号].法术状态组["御风"].防御
		self.参战单位[编号].速度 =self.参战单位[编号].速度+ self.参战单位[编号].法术状态组["御风"].速度
	elseif 特性 =="灵刃" then
	self.参战单位[编号].法术状态组["灵刃"]={}
	self.参战单位[编号].法术状态组["灵刃"].回合=4
	if 几率 == 1 then
		self.参战单位[编号].法术状态组["灵刃"].伤害 =math.floor(self.参战单位[编号].伤害*15/100)
		self.参战单位[编号].法术状态组["灵刃"].灵力 =math.floor(self.参战单位[编号].灵力*10/100)
				self.参战单位[编号].法术状态组["灵刃"].防御 =math.floor(self.参战单位[编号].防御*10/100)
	elseif 几率 == 2 then
		self.参战单位[编号].法术状态组["灵刃"].伤害 =math.floor(self.参战单位[编号].伤害*25/100)
		self.参战单位[编号].法术状态组["灵刃"].灵力 =math.floor(self.参战单位[编号].灵力*10/100)
				self.参战单位[编号].法术状态组["灵刃"].防御 =math.floor(self.参战单位[编号].防御*10/100)
	elseif 几率 == 3 then
			self.参战单位[编号].法术状态组["灵刃"].伤害 =math.floor(self.参战单位[编号].伤害*33/100)
		self.参战单位[编号].法术状态组["灵刃"].灵力 =math.floor(self.参战单位[编号].灵力*5/100)
				self.参战单位[编号].法术状态组["灵刃"].防御 =math.floor(self.参战单位[编号].防御*5/100)
	elseif 几率 == 4 then
			self.参战单位[编号].法术状态组["灵刃"].伤害 =math.floor(self.参战单位[编号].伤害*43/100)
		self.参战单位[编号].法术状态组["灵刃"].灵力 =math.floor(self.参战单位[编号].灵力*5/100)
				self.参战单位[编号].法术状态组["灵刃"].防御 =math.floor(self.参战单位[编号].防御*5/100)
	elseif 几率 == 5 then
			self.参战单位[编号].法术状态组["灵刃"].伤害 =math.floor(self.参战单位[编号].伤害*100/100)
		self.参战单位[编号].法术状态组["灵刃"].灵力 =math.floor(self.参战单位[编号].灵力*5/100)
				self.参战单位[编号].法术状态组["灵刃"].防御 =math.floor(self.参战单位[编号].防御*5/100)
	end
				self.参战单位[编号].法术状态组["灵刃"].当前气血 = math.floor(self.参战单位[编号].当前气血*0.3)
		self.参战单位[编号].当前气血 =self.参战单位[编号].当前气血-self.参战单位[编号].法术状态组["灵刃"].当前气血
			self.参战单位[编号].防御 = self.参战单位[编号].防御- self.参战单位[编号].法术状态组["灵刃"].防御
		self.参战单位[编号].灵力 =self.参战单位[编号].灵力- math.floor (self.参战单位[编号].法术状态组["灵刃"].灵力*0.5)
		self.参战单位[编号].伤害 =self.参战单位[编号].伤害+ self.参战单位[编号].法术状态组["灵刃"].伤害

	elseif 特性 =="瞬法" then
	local 瞬法几率 =33
	if 几率 == 1 then
		瞬法几率 =33
	elseif 几率 == 2 then
		瞬法几率=50
	elseif 几率 == 3 then
		瞬法几率=66
	elseif 几率 == 4 then
		瞬法几率= 83
	elseif 几率 == 5 then
		瞬法几率= 100
	end
	for i=1,#self.参战单位[编号].主动技能	do
		if self.参战单位[编号].主动技能[i].名称 =="天降灵葫" and	math.random(100) <= 瞬法几率 then
		self.参战单位[编号].法术状态组["瞬法"]={}
		self.参战单位[编号].法术状态组["瞬法"].技能= "天降灵葫"

		elseif self.参战单位[编号].主动技能[i].名称 =="八凶法阵" and	math.random(100) <= 瞬法几率 then
			self.参战单位[编号].法术状态组["瞬法"]={}
		self.参战单位[编号].法术状态组["瞬法"].技能= "八凶法阵"
		elseif self.参战单位[编号].主动技能[i].名称 =="泰山压顶" and	math.random(100) <= 瞬法几率 then
		self.参战单位[编号].法术状态组["瞬法"]={}
		self.参战单位[编号].法术状态组["瞬法"].技能= "泰山压顶"
		elseif self.参战单位[编号].主动技能[i].名称 == "水漫金山" and	math.random(100) <= 瞬法几率 then
		self.参战单位[编号].法术状态组["瞬法"]={}
		self.参战单位[编号].法术状态组["瞬法"].技能="水漫金山"
		elseif self.参战单位[编号].主动技能[i].名称 == "地狱烈火" and	math.random(100) <= 瞬法几率 then
		self.参战单位[编号].法术状态组["瞬法"]={}
		self.参战单位[编号].法术状态组["瞬法"].技能="地狱烈火"
		elseif self.参战单位[编号].主动技能[i].名称 =="奔雷咒"	and	math.random(100) <= 瞬法几率 then
		self.参战单位[编号].法术状态组["瞬法"]={}
		self.参战单位[编号].法术状态组["瞬法"].技能="奔雷咒"
		elseif self.参战单位[编号].主动技能[i].名称 =="雷击" and	math.random(100) <= 瞬法几率 then
		self.参战单位[编号].法术状态组["瞬法"]={}
		self.参战单位[编号].法术状态组["瞬法"].技能="雷击"
		elseif self.参战单位[编号].主动技能[i].名称 =="水攻" and	math.random(100) <= 瞬法几率 then
		self.参战单位[编号].法术状态组["瞬法"]={}
		self.参战单位[编号].法术状态组["瞬法"].技能="水攻"
		elseif self.参战单位[编号].主动技能[i].名称 == "烈火" and	math.random(100) <= 瞬法几率 then
		self.参战单位[编号].法术状态组["瞬法"]={}
		self.参战单位[编号].法术状态组["瞬法"].技能="烈火"
		elseif self.参战单位[编号].主动技能[i].名称 == "落岩" and	math.random(100) <= 瞬法几率 then
		self.参战单位[编号].法术状态组["瞬法"]={}
		self.参战单位[编号].法术状态组["瞬法"].技能="落岩"
		end
	end
	elseif 特性 =="瞬击" then
	local 随机几率 =33
	if 几率 == 1 then
		随机几率 =33
	elseif 几率 == 2 then
		随机几率=50
	elseif 几率 == 3 then
		随机几率=66
	elseif 几率 == 4 then
		随机几率= 83
	elseif 几率 == 5 then
		随机几率= 100
	end
		if	math.random(100) <= 随机几率 then
			self.参战单位[编号].法术状态组["瞬击"]={}
		end
	end
 end
function FightControl:添加特性属性(编号,特性,几率)
	if 特性 =="力破" then
	if 几率 == 1 then
		self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +80
	elseif 几率 == 2 then
		self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +120
	elseif 几率 == 3 then
		self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +160
	elseif 几率 == 4 then
		self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +200
	elseif 几率 == 5 then
		self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +240
	end
	elseif 特性 =="巧劲" then
	if 几率 == 1 then
		self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +2
		self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*23/100)
	elseif 几率 == 2 then
		self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +5
		self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*20/100)
	elseif 几率 == 3 then
		self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +8
		self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*17/100)
	elseif 几率 == 4 then
		self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +12
		self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*15/100)
	elseif 几率 == 5 then
		self.参战单位[编号].伤害 = self.参战单位[编号].伤害 +25
		self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*12/100)
	end
	elseif 特性 =="识药" then
	if 几率 == 1 then
		self.参战单位[编号].识药 = 1.2
		self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*20/100)
	elseif 几率 == 2 then
			self.参战单位[编号].识药 = 2.4
		self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*10/100)
	elseif 几率 == 3 then
			self.参战单位[编号].识药 = 3.6
		self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*10/100)
	elseif 几率 == 4 then
			self.参战单位[编号].识药 = 4.8
		self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*10/100)
	elseif 几率 == 5 then
			self.参战单位[编号].识药 = 6.0
		self.参战单位[编号].防御 = self.参战单位[编号].防御 - math.floor(self.参战单位[编号].防御*10/100)
	end
	elseif 特性 =="抗物" then
	if 几率 == 1 then
		self.参战单位[编号].灵力 =self.参战单位[编号].灵力-math.floor(self.参战单位[编号].灵力*15/100)
		self.参战单位[编号].防御 = self.参战单位[编号].防御 +6
	elseif 几率 == 2 then
			self.参战单位[编号].灵力 =self.参战单位[编号].灵力-math.floor(self.参战单位[编号].灵力*18/100)
		self.参战单位[编号].防御 = self.参战单位[编号].防御 +9
	elseif 几率 == 3 then
			self.参战单位[编号].灵力 =self.参战单位[编号].灵力-math.floor(self.参战单位[编号].灵力*22/100)
		self.参战单位[编号].防御 = self.参战单位[编号].防御 +14
	elseif 几率 == 4 then
			self.参战单位[编号].灵力 =self.参战单位[编号].灵力-math.floor(self.参战单位[编号].灵力*26/100)
		self.参战单位[编号].防御 = self.参战单位[编号].防御 +23
	elseif 几率 == 5 then
			self.参战单位[编号].灵力 =self.参战单位[编号].灵力-math.floor(self.参战单位[编号].灵力*30/100)
		self.参战单位[编号].防御 = self.参战单位[编号].防御 +52
	end
	elseif 特性 =="抗法" then
	if 几率 == 1 then
		self.参战单位[编号].防御 =self.参战单位[编号].防御-math.floor(self.参战单位[编号].防御*15/100)
		self.参战单位[编号].灵力 = self.参战单位[编号].灵力 +6
	elseif 几率 == 2 then
			self.参战单位[编号].防御 =self.参战单位[编号].防御-math.floor(self.参战单位[编号].防御*18/100)
		self.参战单位[编号].灵力 = self.参战单位[编号].灵力 +9
	elseif 几率 == 3 then
			self.参战单位[编号].防御 =self.参战单位[编号].防御-math.floor(self.参战单位[编号].防御*22/100)
		self.参战单位[编号].灵力 = self.参战单位[编号].灵力 +14
	elseif 几率 == 4 then
			self.参战单位[编号].防御 =self.参战单位[编号].防御-math.floor(self.参战单位[编号].防御*26/100)
		self.参战单位[编号].灵力 = self.参战单位[编号].灵力 +23
	elseif 几率 == 5 then
			self.参战单位[编号].防御 =self.参战单位[编号].防御-math.floor(self.参战单位[编号].防御*30/100)
		self.参战单位[编号].灵力 = self.参战单位[编号].灵力 +52
	end
	end
 end
function FightControl:添加内丹属性(编号,内丹1)
	local 内丹={}
	for n=1,#内丹1 do
	内丹[n]=内丹1[n]
	end
	for i=1,#内丹 do
		local 等级 = 内丹[i].等级
		local 技能 = 内丹[i].技能
		if 技能 == "深思" then
		if self:重复技能判断(编号,"高级冥思") then
			self.参战单位[编号].冥想= 等级 *5 +self.参战单位[编号].冥想
		end
		end
		if 技能 == "淬毒" then
		if self:重复技能判断(编号,"毒") or self:重复技能判断(编号,"高级毒") then
			self.参战单位[编号].毒= math.floor(等级 *5 /100)+self.参战单位[编号].毒
		end
		end
		if 技能 == "连环" then
		if self:重复技能判断(编号,"连击") or self:重复技能判断(编号,"高级连击") then
			self.参战单位[编号].连击=math.floor(等级*0.8)+self.参战单位[编号].连击
		end
		end
		if 技能 == "灵光" then
			self.参战单位[编号].法术伤害= math.floor(等级 *self.参战单位[编号].魔力*0.05)+self.参战单位[编号].法术伤害
		end
		if 技能 == "舍身击" then
			self.参战单位[编号].伤害= math.floor(等级*12+20)+self.参战单位[编号].伤害
		end
		if 技能 == "圣洁" then
		if self:重复技能判断(编号,"驱鬼") or self:重复技能判断(编号,"高级驱鬼") then
			self.参战单位[编号].驱鬼= 等级 *2+self.参战单位[编号].驱鬼
		end
		end
		if 技能 == "狙刺" then
			self.参战单位[编号].法术伤害= math.floor((等级 *15 +self.参战单位[编号].等级/3)*0.7)+self.参战单位[编号].法术伤害
		end
		if 技能 == "狂怒" then
			self.参战单位[编号].暴击伤害= 等级*20 +self.参战单位[编号].暴击伤害
		end
		if 技能 == "阴伤" then
			self.参战单位[编号].阴伤= 等级*50 +self.参战单位[编号].阴伤
		end
		if 技能 == "擅咒" then
		self.参战单位[编号].额外法术伤害= self.参战单位[编号].额外法术伤害 + 等级*12
		end
		if 技能 == "钢化" then
		if	self:重复技能判断(编号,"高级防御") or	self:重复技能判断(编号,"防御") then
				self.参战单位[编号].防御=self.参战单位[编号].防御+等级*20
		end
		end
		if 技能 == "生死决" then
		local	临时属性	= self.参战单位[编号].伤害*(等级/100)
		self.参战单位[编号].伤害= self.参战单位[编号].伤害 +临时属性
		self.参战单位[编号].防御= self.参战单位[编号].防御 - 临时属性
		end
		if 技能 == "碎甲刃" then
		self.参战单位[编号].伤害= self.参战单位[编号].伤害 + self.参战单位[编号].力量*(等级*0.15)
		end
		if 技能 == "撞击" then
		self.参战单位[编号].伤害= self.参战单位[编号].伤害 + 等级*2
		end
		if 技能 == "催心浪" then
		if self.参战单位[编号].法波 ~=0 then
		self.参战单位[编号].法波.下限=self.参战单位[编号].法波.下限 + 等级*2
	end
		end
		if 技能 == "通灵法" then
		self.参战单位[编号].额外法术伤害 =self.参战单位[编号].额外法术伤害+ math.floor(self.参战单位[编号].额外法术伤害 * (等级*4/100))
		end
		if 技能 == "隐匿击" then
		if	self:重复技能判断(编号,"高级隐身") or	self:重复技能判断(编号,"隐身") then
		self.参战单位[编号].伤害 =	self.参战单位[编号].伤害 + math.floor(self.参战单位[编号].伤害*(等级*0.02))
		end
		end
		if 技能 == "灵身" then
			self.参战单位[编号].法暴 = self.参战单位[编号].法暴+(2+等级)
		end
		if 技能 == "腾挪劲" then
			self.参战单位[编号].防御 = self.参战单位[编号].防御+ 等级*3
		end
		if 技能 == "电魂闪" then
			self.参战单位[编号].驱散 = self.参战单位[编号].驱散+等级*8	----小法
		end
		if 技能 == "坚甲" then
		if	self:重复技能判断(编号,"高级反震") or	self:重复技能判断(编号,"反震") then
				self.参战单位[编号].反震=self.参战单位[编号].反震+等级*2
		end
		end
		if 技能 == "双星爆" then
		if	self:重复技能判断(编号,"高级法术连击") or	self:重复技能判断(编号,"法术连击") then
				self.参战单位[编号].法连=self.参战单位[编号].法连+等级
				self.参战单位[编号].额外法术伤害 =self.参战单位[编号].额外法术伤害+ math.floor(self.参战单位[编号].额外法术伤害 * (等级*2/100))
		end
		end
	end
 end
 
function FightControl:添加技能属性(编号,技能组1,召唤)

	local 技能组={}
	for n=1,#技能组1 do
		技能组[n]=技能组1[n].名称
	end
	self.参战单位[编号].已加技能={}
	-- if self.参战单位[编号].造型=="齐天大圣" then
	--	技能组[#技能组+1]="大闹天宫"
	-- end
	for n=1,#技能组 do
	repeat
		
		if	SkillData[技能组[n]].类型 ~=7	then
			self.参战单位[编号].主动技能[#self.参战单位[编号].主动技能+1]={名称=技能组[n],等级=self.参战单位[编号].等级,种类=SkillData[技能组[n]].类型}
				break
		end
		if self:重复技能判断(编号,技能组[n]) then
			break
		end
		if 技能组[n]=="潜力激发"	then
			self.参战单位[编号].伤害=self.参战单位[编号].伤害+self.参战单位[编号].等级
			self.参战单位[编号].防御=self.参战单位[编号].防御+math.floor(self.参战单位[编号].等级*0.8)
			self.参战单位[编号].速度=self.参战单位[编号].速度+math.floor(self.参战单位[编号].等级/4)
		elseif 技能组[n]=="必杀" or 技能组[n]=="高级必杀"	then
			if 技能组[n]=="必杀" then
			if self:重复技能判断(编号,"高级必杀")==false then
			self.参战单位[编号].必杀=10+self.参战单位[编号].必杀
			end
			else
			self.参战单位[编号].必杀=15+self.参战单位[编号].必杀
			end

		elseif 技能组[n]=="独行" or 技能组[n]=="高级独行"	then
			if 技能组[n]=="独行" then
				if self:重复技能判断(编号,"高级独行")==false then
				self.参战单位[编号].抵抗封印等级=math.floor(self.参战单位[编号].抵抗封印等级*0.25)
				end
			else
			self.参战单位[编号].抵抗封印等级=math.floor(self.参战单位[编号].抵抗封印等级*0.5)
			end
		elseif 技能组[n]=="强力" or 技能组[n]=="高级强力"	then
			if 技能组[n]=="强力" then
			if self:重复技能判断(编号,"高级强力")==false then
			self.参战单位[编号].伤害=self.参战单位[编号].伤害+math.floor(self.参战单位[编号].等级*0.52)
			end
			else
			self.参战单位[编号].伤害=self.参战单位[编号].伤害+math.floor(self.参战单位[编号].等级*0.715)
			end
		elseif 技能组[n]=="须弥真言" then
			self.参战单位[编号].灵力=self.参战单位[编号].灵力+math.floor(self.参战单位[编号].魔力*0.7)
	elseif 技能组[n]=="津津有味" or 技能组[n]=="水击三千" then
		self.参战单位[编号].必杀=5+self.参战单位[编号].必杀
		elseif 技能组[n]=="灵能激发"	then
			self.参战单位[编号].灵力=self.参战单位[编号].灵力+math.floor(self.参战单位[编号].魔力*0.2)
		elseif 技能组[n]=="开门见山"	then
			self.参战单位[编号].法术状态组["开门见山"]={}
		elseif 技能组[n]=="峰回路转"	then
			self.参战单位[编号].法术状态组["峰回路转"]={}
		elseif 技能组[n]=="出其不意"	then
			self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.15)
		elseif 技能组[n]=="凭风借力"	then
			self.参战单位[编号].暴击伤害=math.floor(self.参战单位[编号].伤害*0.25)
		elseif 技能组[n]=="龙魂"	then
			self.参战单位[编号].灵力=self.参战单位[编号].灵力+math.floor(self.参战单位[编号].魔力*0.4)
		elseif 技能组[n]=="防御" or 技能组[n]=="高级防御"	then
			if 技能组[n]=="防御" then
				if self:重复技能判断(编号,"高级防御")==false then
					self.参战单位[编号].防御=self.参战单位[编号].防御+math.floor(self.参战单位[编号].等级*0.6)
					self.参战单位[编号].灵力=self.参战单位[编号].灵力-math.floor(self.参战单位[编号].等级*0.1)
				end
				else
				self.参战单位[编号].防御=self.参战单位[编号].防御+math.floor(self.参战单位[编号].等级*0.8)
				self.参战单位[编号].灵力=self.参战单位[编号].灵力-math.floor(self.参战单位[编号].等级*0.1)
			end
		elseif 技能组[n]=="精神集中" or 技能组[n]=="高级精神集中"	then
			if 技能组[n]=="精神集中" then
					if self:重复技能判断(编号,"高级精神集中")==false then
						self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*0.7)
						self.参战单位[编号].神迹=2
					end
			else
					self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*0.8)
					self.参战单位[编号].神迹=2
			end
		elseif 技能组[n]=="幸运" or 技能组[n]=="高级幸运"	then
			self.参战单位[编号].幸运=true
		elseif 技能组[n]=="偷袭" or 技能组[n]=="高级偷袭"	then
			self.参战单位[编号].偷袭=true
		elseif 技能组[n]=="感知" or 技能组[n]=="高级感知"	then
			self.参战单位[编号].感知=true
		elseif	技能组[n]=="高级神迹"	then
			self.参战单位[编号].神迹=2
		elseif 技能组[n]=="夜战" or 技能组[n]=="高级夜战"	then
			self.参战单位[编号].夜战=true
		elseif 技能组[n]=="大快朵颐"	then
			self.参战单位[编号].连击=10+self.参战单位[编号].连击
			self.参战单位[编号].必杀=10+self.参战单位[编号].必杀
			
		elseif 技能组[n]=="连击" or 技能组[n]=="高级连击"	then
			if 技能组[n]=="连击" then
			if self:重复技能判断(编号,"高级连击")==false then
				self.参战单位[编号].连击=45+self.参战单位[编号].连击
				if self.参战单位[编号].连击减伤==nil then
				self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*0.8)
				self.参战单位[编号].连击减伤=1
				end
				end
			else
			self.参战单位[编号].连击=55+self.参战单位[编号].连击
			if self.参战单位[编号].连击减伤==nil then
				self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*0.8)
				self.参战单位[编号].连击减伤=1
				end
				end
		elseif 技能组[n]=="理直气壮"	then
			self.参战单位[编号].理直气壮=true
			self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*0.8)
		elseif	技能组[n]=="嗜血追击"	then
			self.参战单位[编号].追击=true
		elseif	技能组[n]=="苍鸾怒击"	then
		self.参战单位[编号].怒击=true
		elseif	技能组[n]=="从天而降"	then
			self.参战单位[编号].从天而降开关=true
		elseif 技能组[n]=="隐身" or 技能组[n]=="高级隐身"	then
			if 技能组[n]=="隐身" then
			if self:重复技能判断(编号,"高级隐身")==false then
				self.参战单位[编号].隐身=4
				self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*0.75)
			end
			else
			self.参战单位[编号].隐身=5
			self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*0.85)
			end
		elseif	技能组[n]=="神出鬼没"	then
			self.参战单位[编号].隐身=math.random(2, 3)
			self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.1)
		elseif 技能组[n]=="敏捷" or 技能组[n]=="高级敏捷"	then
			if 技能组[n]=="敏捷" then
			if self:重复技能判断(编号,"高级敏捷")==false then
				self.参战单位[编号].速度=self.参战单位[编号].速度+math.floor(self.参战单位[编号].速度*0.2)
				end
			else
			self.参战单位[编号].速度=self.参战单位[编号].速度+math.floor(self.参战单位[编号].速度*0.1)
				end
		elseif 技能组[n]=="逍遥游"	then
			self.参战单位[编号].速度=self.参战单位[编号].速度+math.floor(self.参战单位[编号].速度*0.2)
			self.参战单位[编号].防御=self.参战单位[编号].防御+math.floor(self.参战单位[编号].等级*0.6)
					self.参战单位[编号].灵力=self.参战单位[编号].灵力-math.floor(self.参战单位[编号].等级*0.1)

		elseif 技能组[n]=="浮云神马"	then
			self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.1)

			for n=1,#self.参战单位 do
			if self.参战单位[n].玩家id and self.参战单位[n].玩家id==self.参战单位[编号].玩家id	then
			
				self.参战单位[n].速度=math.floor(self.参战单位[n].速度*1.1)
				
	
				table.insert(self.战斗发言数据,
					{
					内容=string.format("#G/%s#W/为#R/%s#W/增加了10%%的速度#91/", self.参战单位[编号].名称,self.参战单位[n].名称),
					id =编号})
				break
			end
			end
		elseif 技能组[n]=="吸血" or 技能组[n]=="高级吸血"	then
			if 技能组[n]=="吸血" then
			if self:重复技能判断(编号,"高级吸血")==false then
				self.参战单位[编号].吸血=0.15
			end
			else
			self.参战单位[编号].吸血=0.3
			end

		elseif 技能组[n]=="张弛有道" then
			self.参战单位[编号].法术吸血=0.3
		elseif 技能组[n]=="毒" or 技能组[n]=="高级毒"	then
			if 技能组[n]=="毒" then
			if self:重复技能判断(编号,"高级毒")==false then
				self.参战单位[编号].毒=10
			end
			else
			self.参战单位[编号].毒=20
			end
		elseif 技能组[n]=="反震" or 技能组[n]=="高级反震"	then
			if 技能组[n]=="反震" then
			if self:重复技能判断(编号,"高级反震")==false then
				self.参战单位[编号].反震=25
			end
			else
			self.参战单位[编号].反震=30
			end
			elseif 技能组[n]=="无赦魔决" then
		self.参战单位[编号].无赦魔决 =1
		elseif 技能组[n]=="风起龙游" then
		self.参战单位[编号].风起龙游 =1
		elseif 技能组[n]=="反击" or 技能组[n]=="高级反击"	then
			if 技能组[n]=="反击" then
			if self:重复技能判断(编号,"高级反击")==false then
				self.参战单位[编号].反击=15
			end
			else
			self.参战单位[编号].反击=30
			end
		elseif 技能组[n]=="神佑复生" or 技能组[n]=="高级神佑复生" and	self:重复技能判断(编号,"高级鬼魂术")==false and self:重复技能判断(编号,"鬼魂术")==false	then
			if 技能组[n]=="神佑复生" then
				if self:重复技能判断(编号,"高级神佑复生")==false then
				self.参战单位[编号].神佑=15
				end
			else
			self.参战单位[编号].神佑=30
			end
		elseif 技能组[n]=="鬼魂术" or 技能组[n]=="高级鬼魂术"	then
			if 技能组[n]=="鬼魂术" then
			if self:重复技能判断(编号,"高级鬼魂术")==false then
				self.参战单位[编号].复活=4
			end
			else
			self.参战单位[编号].复活=5
			end
		elseif 技能组[n]=="驱鬼" or 技能组[n]=="高级驱鬼"	then
			if 技能组[n]=="驱鬼" then
			if self:重复技能判断(编号,"高级驱鬼")==false then
				self.参战单位[编号].驱鬼=1.5
				end
			else
			self.参战单位[编号].驱鬼=2
				end
		elseif 技能组[n]=="再生" or 技能组[n]=="高级再生"	then
			if 技能组[n]=="再生" then
			if self:重复技能判断(编号,"高级再生")==false then
				self.参战单位[编号].再生=math.floor(self.参战单位[编号].等级*0.5)
			end
			else
			self.参战单位[编号].再生=self.参战单位[编号].等级
			end
		elseif 技能组[n]=="冥思" or 技能组[n]=="高级冥思"	then
			if 技能组[n]=="冥思" then
			if self:重复技能判断(编号,"高级冥思")==false then
				self.参战单位[编号].冥想=math.floor(self.参战单位[编号].等级*0.25)
				end
			else
			self.参战单位[编号].冥想=math.floor(self.参战单位[编号].等级*0.5)
				end
		elseif 技能组[n]=="赴汤蹈火"	then
		self.参战单位[编号].法伤减免=0.9
		elseif 技能组[n]=="千锤百炼" then
		self.参战单位[编号].物伤减免=0.85
		elseif 技能组[n]=="慧根" or 技能组[n]=="高级慧根"	then
			if 技能组[n]=="慧根" then
			if self:重复技能判断(编号,"高级慧根")==false then
				self.参战单位[编号].慧根=0.75
				end
			else
			self.参战单位[编号].慧根=0.5
				end
		elseif 技能组[n]=="法术暴击" or 技能组[n]=="高级法术暴击"	then
			if 技能组[n]=="法术暴击" then

			if self:重复技能判断(编号,"高级法术暴击")==false then

				self.参战单位[编号].法暴=15
				end
			else
			self.参战单位[编号].法暴=25
				end
		elseif 技能组[n]=="法术连击" or 技能组[n]=="高级法术连击"	then
			if 技能组[n]=="法术连击" then
			if self:重复技能判断(编号,"高级法术连击")==false then
				self.参战单位[编号].法连=25
				end
			else
			self.参战单位[编号].法连=40
				end
		elseif 技能组[n]=="法术波动" or 技能组[n]=="高级法术波动"	then
		if 技能组[n]=="法术波动" then
		if self:重复技能判断(编号,"高级法术波动")==false then
			self.参战单位[编号].法波={下限=80,上限=110}
			end
		else
		self.参战单位[编号].法波={下限=90,上限=125}
			end
		elseif 技能组[n]=="魔之心" or 技能组[n]=="高级魔之心"	then
			if 技能组[n]=="魔之心" then
				if self:重复技能判断(编号,"高级魔之心")==false then
				self.参战单位[编号].魔心=1.1
				end
			else
				self.参战单位[编号].魔心=1.2
			end
			elseif 技能组[n]=="北冥之渊" then
			self:添加封印状态(self.参战单位[编号],self.参战单位[编号],技能组[n])
		end
	
		if 召唤 then

			if 技能组[n]=="高级进击必杀" or 技能组[n]=="进击必杀"	then
				if 技能组[n]=="进击必杀" then
					if self:重复技能判断(编号,"高级进击必杀")==false then
						self:添加封印状态(self.参战单位[编号],self.参战单位[编号],技能组[n])
					end
				else
					if self:重复技能判断(编号,"进击必杀")==false then
						self:添加封印状态(self.参战单位[编号],self.参战单位[编号],技能组[n])
					end
				end
		
			elseif 技能组[n]=="高级进击法暴" or 技能组[n]=="进击法暴"	then
				if 技能组[n]=="进击法暴" then
					if self:重复技能判断(编号,"高级进击法暴")==false then
						self:添加封印状态(self.参战单位[编号],self.参战单位[编号],技能组[n])
					end
				else
					if self:重复技能判断(编号,"进击法暴")==false then
						self:添加封印状态(self.参战单位[编号],self.参战单位[编号],技能组[n])
					end
				end
				elseif 技能组[n]=="逍遥游" then
			self.参战单位[编号].伤害 =math.floor(self.参战单位[编号].伤害*1.5)
			
			elseif 技能组[n]=="高级遗志" or 技能组[n]=="遗志"	then
				if 技能组[n]=="遗志" then
					if self:重复技能判断(编号,"高级遗志")==false then
						self.参战单位[编号].伤害 =math.floor(self.参战单位[编号].伤害*0.6)
					end
				else
				if self:重复技能判断(编号,"遗志")==false then
					self.参战单位[编号].伤害 =math.floor(self.参战单位[编号].伤害*0.6)
					end
				end
				self.参战单位[编号].法术状态组[技能组[n]]={}	
			end
		
	end

			self.参战单位[编号].已加技能[#self.参战单位[编号].已加技能+1]=技能组[n]
			break
	until true

	end
 end

function FightControl:重复技能判断(编号,名称)
	for n=1,#self.参战单位[编号].已加技能 do
	if self.参战单位[编号].已加技能[n]==名称 then
		return true
	end
	end
	return false
 end
 
 
function FightControl:加载单个玩家(id,位置)
	print("加载玩家:"..id..",位置："..位置)
	--加载单位
	self.参战玩家[#self.参战玩家+1]={玩家id=id,队伍id=self:取队伍id(id),加载=false,数字id=UserData[id].id,逃跑=false}
	self.参战单位[#self.参战单位+1]=RoleControl:获取战斗角色数据(UserData[id])
	self.参战单位[#self.参战单位].位置=位置
	self.参战单位[#self.参战单位].队伍id=self:取队伍id(id)
	self.参战单位[#self.参战单位].数字id=UserData[id].id
	self.参战单位[#self.参战单位].玩家id=id
	self.参战单位[#self.参战单位].编号=#self.参战单位
	self.参战单位[#self.参战单位].战斗类型="角色"
	self.参战单位[#self.参战单位].参战召唤兽={}
	self.参战单位[#self.参战单位].召唤类={}
	self.参战单位[#self.参战单位].变身造型技能=0
	if UserData[id].角色.变身.造型~="" and UserData[id].角色.变身.造型~=nil then
		self.参战单位[#self.参战单位].变身造型技能=UserData[id].角色.变身.技能 or 0
	end

	self:设置队伍区分(id,true)
	self:增加阵法属性(self:取队伍id(id),位置,#self.参战单位)

	--加载召唤兽
	if UserData[id].召唤兽.数据.参战==0 or UserData[id].召唤兽.数据.参战==nil	then
		-- 什么也不干
	elseif type(UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].当前气血)~= "number" then
		UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].当前气血=0
		UserData[id].召唤兽.数据.参战=0
		SendMessage(UserData[id].连接id,2005,UserData[id].召唤兽:取头像数据())
		SendMessage(UserData[id].连接id,7,"#y/你的召唤兽因气血异常而不愿意参战")
	elseif UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].当前气血<=0 then
		UserData[id].召唤兽.数据.参战=0
		SendMessage(UserData[id].连接id,2005,UserData[id].召唤兽:取头像数据())
		SendMessage(UserData[id].连接id,7,"#y/你的召唤兽因气血过低而不愿意参战")
	elseif UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].忠诚<=70 and math.random(100)<=50 then
		UserData[id].召唤兽.数据.参战=0
		SendMessage(UserData[id].连接id,2005,UserData[id].召唤兽:取头像数据())
		SendMessage(UserData[id].连接id,7,"#y/你的召唤兽因忠诚度过低而不愿意参战")
	elseif UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].寿命<=50	then
		UserData[id].召唤兽.数据.参战=0
		SendMessage(UserData[id].连接id,2005,UserData[id].召唤兽:取头像数据())
		SendMessage(UserData[id].连接id,7,"#y/你的召唤兽因寿命过低而无法参战")
	elseif UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].禁止使用	then
		UserData[id].召唤兽.数据.参战=0
		SendMessage(UserData[id].连接id,2005,UserData[id].召唤兽:取头像数据())
		SendMessage(UserData[id].连接id,7,"#y/你的召唤兽已经被禁止使用")
	elseif UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战]~=nil then
		if UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].类型~="神兽" then
			UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].寿命=UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].寿命-0.5
			UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].忠诚=UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].忠诚-0.1
		end
		self.参战单位[#self.参战单位].参战召唤兽[#self.参战单位[#self.参战单位].参战召唤兽+1]=UserData[id].召唤兽.数据.参战
		self.参战单位[#self.参战单位+1]=table.loadstring(UserData[id].召唤兽:获取指定数据(UserData[id].召唤兽.数据.参战))
		self.参战单位[#self.参战单位].位置=位置+5
		self.参战单位[#self.参战单位].命中=self.参战单位[#self.参战单位].伤害
		self.参战单位[#self.参战单位].法防=math.floor(self.参战单位[#self.参战单位].魔力*0.5+self.参战单位[#self.参战单位].灵力*0.3)
		self.参战单位[#self.参战单位].队伍id=self:取队伍id(id)
		self.参战单位[#self.参战单位].数字id=UserData[id].id
		self.参战单位[#self.参战单位].召唤兽id=UserData[id].召唤兽.数据.参战
		self.参战单位[#self.参战单位].玩家id=id
		self.参战单位[#self.参战单位].战斗类型="召唤兽"
		self.参战单位[#self.参战单位].主动技能={}
		self.参战单位[#self.参战单位].技能=UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].技能
		self.参战单位[#self.参战单位].追加技能=UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].追加技能
		self.参战单位[#self.参战单位].编号=#self.参战单位
		self.参战单位[#self.参战单位].伤害=math.floor(self.参战单位[#self.参战单位].伤害*1.1)
		if UserData[id].角色.法宝效果.九黎战鼓 then
			self.参战单位[#self.参战单位].伤害=self.参战单位[#self.参战单位].伤害+UserData[id].角色.法宝效果.九黎战鼓
		end
		if UserData[id].角色.法宝效果.盘龙壁 then
			self.参战单位[#self.参战单位].防御=self.参战单位[#self.参战单位].防御+UserData[id].角色.法宝效果.盘龙壁
		end
		if UserData[id].角色.法宝效果.神行飞剑 then
			self.参战单位[#self.参战单位].速度=self.参战单位[#self.参战单位].速度+UserData[id].角色.法宝效果.神行飞剑
		end
		if UserData[id].角色.法宝效果.汇灵盏 then
			self.参战单位[#self.参战单位].灵力=self.参战单位[#self.参战单位].灵力+UserData[id].角色.法宝效果.汇灵盏
		end
		if UserData[id].角色.法宝效果.重明战鼓	then
			self.参战单位[#self.参战单位].伤害=self.参战单位[#self.参战单位].伤害+UserData[id].角色.法宝效果.重明战鼓
			self.参战单位[#self.参战单位].灵力=self.参战单位[#self.参战单位].灵力+UserData[id].角色.法宝效果.重明战鼓
		end
		if UserData[id].角色.法宝效果.梦云幻甲 then
			self.参战单位[#self.参战单位].防御=self.参战单位[#self.参战单位].防御+UserData[id].角色.法宝效果.梦云幻甲
			self.参战单位[#self.参战单位].法防=self.参战单位[#self.参战单位].法防+UserData[id].角色.法宝效果.梦云幻甲
		end
		if UserData[id].角色.法宝效果.兽王令 then
			self.参战单位[#self.参战单位].伤害=self.参战单位[#self.参战单位].伤害+UserData[id].角色.法宝效果.兽王令
			self.参战单位[#self.参战单位].速度=self.参战单位[#self.参战单位].速度+UserData[id].角色.法宝效果.兽王令
			self.参战单位[#self.参战单位].灵力=self.参战单位[#self.参战单位].灵力+UserData[id].角色.法宝效果.兽王令
			self.参战单位[#self.参战单位].防御=self.参战单位[#self.参战单位].防御+UserData[id].角色.法宝效果.兽王令
			self.参战单位[#self.参战单位].法防=self.参战单位[#self.参战单位].法防+UserData[id].角色.法宝效果.兽王令
		end



		self.参战单位[#self.参战单位].修炼数据={}
		for n=1,#全局变量.修炼名称 do
			self.参战单位[#self.参战单位].修炼数据[全局变量.修炼名称[n]]=UserData[id].角色.召唤兽修炼[全局变量.修炼名称[n]].等级
		end
		self:设置队伍区分(id)
		self:增加阵法属性(self:取队伍id(id),位置+5,#self.参战单位)
	end
	-- 如果没有组队或者你是队长同时人数小于5，增加助战
	if UserData[id].队伍 == 0 or (队伍数据[self:取队伍id(id)] ~= nil and #队伍数据[self:取队伍id(id)].队员数据 < 5 and UserData[id].队长) then
		local 参战助战 = {}
		if UserData[id].助战.数据 ~= nil then
			for i = 1,#UserData[id].助战.数据 do
				if UserData[id].助战.数据[i].参战 and #参战助战 < 4 then
					参战助战[#参战助战+1] = {主人=id,识别码=UserData[id].助战.数据[i].识别码}
				end
			end
		end
		local 位置起始 = 2
		if UserData[id].队伍 ~= 0 then
			位置起始 = #队伍数据[self:取队伍id(id)].队员数据 + 1
		end
		for n = 位置起始,5 do
			if 参战助战[n-1] ~= nil then
				self.助战参战[#self.助战参战+1]=参战助战[n-1]
				self.参战单位[#self.参战单位+1]=UserData[id].助战:获取战斗数据(参战助战[n-1].识别码)
				self.参战单位[#self.参战单位].位置=n
				self.参战单位[#self.参战单位].助战=true
				self.参战单位[#self.参战单位].助战编号=UserData[id].助战:取助战编号(参战助战[n-1].识别码)
				self.参战单位[#self.参战单位].自动=UserData[id].助战.数据[self.参战单位[#self.参战单位].助战编号].自动
				self.参战单位[#self.参战单位].队伍id=self:取队伍id(id)
				self.参战单位[#self.参战单位].数字id=UserData[id].id
				self.参战单位[#self.参战单位].玩家id=id
				self.参战单位[#self.参战单位].编号=#self.参战单位
				self.参战单位[#self.参战单位].战斗类型="角色"
				self.参战单位[#self.参战单位].参战召唤兽={}
				self.参战单位[#self.参战单位].召唤类={}
				self.参战单位[#self.参战单位].变身造型技能=0
				self:设置队伍区分(id)
				self:增加阵法属性(self:取队伍id(id),n,#self.参战单位)
				local cw = 取助战宠物(UserData[id].召唤兽.数据,self.参战单位[#self.参战单位].助战编号)
				if cw ~= nil then
					self.参战单位[#self.参战单位].参战召唤兽[#self.参战单位[#self.参战单位].参战召唤兽+1]=cw.id
					self.参战单位[#self.参战单位+1]=cw.数据
					self.参战单位[#self.参战单位].位置=n+5
					self.参战单位[#self.参战单位].命中=cw.数据.伤害
					self.参战单位[#self.参战单位].法防=math.floor(cw.数据.魔力*0.5+cw.数据.灵力*0.3)
					self.参战单位[#self.参战单位].队伍id=self:取队伍id(id)
					self.参战单位[#self.参战单位].数字id=UserData[id].id
					self.参战单位[#self.参战单位].召唤兽id=cw.id
					self.参战单位[#self.参战单位].玩家id=id
					self.参战单位[#self.参战单位].战斗类型="召唤兽"
					self.参战单位[#self.参战单位].主动技能={}
					self.参战单位[#self.参战单位].技能=cw.数据.技能
					self.参战单位[#self.参战单位].追加技能=cw.数据.追加技能
					self.参战单位[#self.参战单位].编号=#self.参战单位
					self.参战单位[#self.参战单位].伤害=math.floor(cw.数据.伤害*1.1)
					if UserData[id].角色.法宝效果.九黎战鼓 then
						self.参战单位[#self.参战单位].伤害=self.参战单位[#self.参战单位].伤害+UserData[id].角色.法宝效果.九黎战鼓
					end
					if UserData[id].角色.法宝效果.盘龙壁 then
						self.参战单位[#self.参战单位].防御=self.参战单位[#self.参战单位].防御+UserData[id].角色.法宝效果.盘龙壁
					end
					if UserData[id].角色.法宝效果.神行飞剑 then
						self.参战单位[#self.参战单位].速度=self.参战单位[#self.参战单位].速度+UserData[id].角色.法宝效果.神行飞剑
					end
					if UserData[id].角色.法宝效果.汇灵盏 then
						self.参战单位[#self.参战单位].灵力=self.参战单位[#self.参战单位].灵力+UserData[id].角色.法宝效果.汇灵盏
					end
					if UserData[id].角色.法宝效果.重明战鼓	then
						self.参战单位[#self.参战单位].伤害=self.参战单位[#self.参战单位].伤害+UserData[id].角色.法宝效果.重明战鼓
						self.参战单位[#self.参战单位].灵力=self.参战单位[#self.参战单位].灵力+UserData[id].角色.法宝效果.重明战鼓
					end
					if UserData[id].角色.法宝效果.梦云幻甲 then
						self.参战单位[#self.参战单位].防御=self.参战单位[#self.参战单位].防御+UserData[id].角色.法宝效果.梦云幻甲
						self.参战单位[#self.参战单位].法防=self.参战单位[#self.参战单位].法防+UserData[id].角色.法宝效果.梦云幻甲
					end
					if UserData[id].角色.法宝效果.兽王令 then
						self.参战单位[#self.参战单位].伤害=self.参战单位[#self.参战单位].伤害+UserData[id].角色.法宝效果.兽王令
						self.参战单位[#self.参战单位].速度=self.参战单位[#self.参战单位].速度+UserData[id].角色.法宝效果.兽王令
						self.参战单位[#self.参战单位].灵力=self.参战单位[#self.参战单位].灵力+UserData[id].角色.法宝效果.兽王令
						self.参战单位[#self.参战单位].防御=self.参战单位[#self.参战单位].防御+UserData[id].角色.法宝效果.兽王令
						self.参战单位[#self.参战单位].法防=self.参战单位[#self.参战单位].法防+UserData[id].角色.法宝效果.兽王令
					end

					self.参战单位[#self.参战单位].修炼数据={}
					for n=1,#全局变量.修炼名称 do
						self.参战单位[#self.参战单位].修炼数据[全局变量.修炼名称[n]]=UserData[id].角色.召唤兽修炼[全局变量.修炼名称[n]].等级
					end
					self:设置队伍区分(id)
					self:增加阵法属性(self:取队伍id(id),位置+5,#self.参战单位)
				end
			end
		end
	end

	
end

function 取助战宠物(数据,助战编号)
	for i=1,#数据 do
	    if 数据[i].助战参战 == 助战编号 then
	    	return {数据=数据[i],id=i}
	    end
	end
	return nil
end

function FightControl:取参战编号(id,战斗类型)
	for n=1,#self.参战单位 do
		if self.参战单位[n].战斗类型==战斗类型 and self.参战单位[n].玩家id==id and self.参战单位[n].助战编号 == nil then
			return n
		end
	end
end


function FightControl:增加阵法属性(队伍id,位置,编号)
 if 队伍数据[队伍id]~=nil and (#队伍数据[队伍id].队员数据 + #UserData[self.进入战斗玩家id].助战.数据) >=5 then
	self.参战单位[编号].阵法=队伍数据[队伍id].阵法
	if 位置>5 then
		return 0
	end
	if self.参战单位[编号].阵法=="天覆阵" then
	self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.2)
	self.参战单位[编号].灵力=math.floor(self.参战单位[编号].灵力*1.2)
	self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*0.8)
	elseif self.参战单位[编号].阵法=="地载阵" then
		if 位置<=4 then
		self.参战单位[编号].防御=math.floor(self.参战单位[编号].防御*1.15)
		self.参战单位[编号].法防=math.floor(self.参战单位[编号].法防*1.15)
		self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.15)
		else
		self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.15)
		end
	elseif self.参战单位[编号].阵法=="鸟翔阵" then
		self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.2)
	elseif self.参战单位[编号].阵法=="风扬阵" then
		if 位置==1 then
		self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.2)
		self.参战单位[编号].灵力=math.floor(self.参战单位[编号].灵力*1.2)
		elseif 位置==2 or 位置==3 then
		self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.1)
		self.参战单位[编号].灵力=math.floor(self.参战单位[编号].灵力*1.1)
		elseif 位置==4 then
		self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.1)
		elseif 位置==5 then
			self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.1)
		end
	elseif self.参战单位[编号].阵法=="云垂阵" then
		if 位置==1 then
		self.参战单位[编号].防御=math.floor(self.参战单位[编号].防御*1.4)
		self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*0.7)
		elseif 位置==2	then
			self.参战单位[编号].防御=math.floor(self.参战单位[编号].防御*1.2)
			self.参战单位[编号].法防=math.floor(self.参战单位[编号].法防*1.1)
		elseif	位置==3 then
		self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.1)
		self.参战单位[编号].灵力=math.floor(self.参战单位[编号].灵力*1.1)
		elseif 位置==4 then
		self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.15)
		elseif 位置==5 then
			self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.15)
		end
	elseif self.参战单位[编号].阵法=="龙飞阵" then
		if 位置==1 then
			self.参战单位[编号].法防=math.floor(self.参战单位[编号].法防*1.2)
		elseif 位置==2	then
			self.参战单位[编号].防御=math.floor(self.参战单位[编号].防御*1.2)
		elseif	位置==3 then
		self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*0.7)
		self.参战单位[编号].灵力=math.floor(self.参战单位[编号].灵力*1.3)
		elseif 位置==4 then
		self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.15)
		elseif 位置==5 then
		self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.2)
		self.参战单位[编号].灵力=math.floor(self.参战单位[编号].灵力*1.2)
		end
		elseif self.参战单位[编号].阵法=="虎翼阵" then
		self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.1)
		elseif self.参战单位[编号].阵法=="蛇蟠阵" then
		self.参战单位[编号].法防=math.floor(self.参战单位[编号].法防*1.2)
	elseif self.参战单位[编号].阵法=="鹰啸阵" then
		if 位置==1 then
			self.参战单位[编号].防御=math.floor(self.参战单位[编号].防御*1.1)
		elseif 位置==2	then
			self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.15)
		elseif	位置==3 then
		self.参战单位[编号].速度=math.floor(self.参战单位[编号].速度*1.15)
		elseif 位置==4 then
		self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.15)
		self.参战单位[编号].灵力=math.floor(self.参战单位[编号].灵力*1.15)
		elseif 位置==5 then
		self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.1)
		self.参战单位[编号].灵力=math.floor(self.参战单位[编号].灵力*1.1)
		end
	elseif self.参战单位[编号].阵法=="雷绝阵" then
			if 位置==1 then
			self.参战单位[编号].雷绝阵=true
		elseif 位置==2	then
			self.参战单位[编号].雷绝阵=true
		elseif	位置==3 then
		self.参战单位[编号].雷绝阵=true
		elseif 位置==4 then
		self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.1)
		self.参战单位[编号].灵力=math.floor(self.参战单位[编号].灵力*1.1)
		elseif 位置==5 then
		self.参战单位[编号].伤害=math.floor(self.参战单位[编号].伤害*1.1)
		self.参战单位[编号].灵力=math.floor(self.参战单位[编号].灵力*1.1)
		end
	end
	end
	end
function FightControl:设置队伍区分(id,玩家)
	if UserData[id].队伍==0 then
	self.临时区分id=id
	else
	self.临时区分id=UserData[id].队伍
	end
	if self.队伍区分[1]==self.临时区分id then
	self.队伍数量[1]=self.队伍数量[1]+1
		if 玩家 then
		table.insert(self.队伍位置[1],1)
		end
	
	else
	self.队伍数量[2]=self.队伍数量[2]+1
	self.队伍区分[2]=self.临时区分id
		if 玩家 then
		table.insert(self.队伍位置[2],1)
		end
	end
 end

 function FightControl:技能召唤流程(编号,技能)
	if self.参战单位[编号].召唤类==nil then 
		return 0 
	end
	self.寻找id={}
	for n=1,#self.参战单位 do
		if self.参战单位[n].战斗类型=="召唤类" and	self.参战单位[n].队伍id==self.参战单位[编号].队伍id and	self.参战单位[n].单位消失==nil then
			table.insert(self.寻找id, n)
		end
	end
	local 召唤数量 = 1
	local 召唤类数量 =#self.寻找id
	local 属性集合 ={}
	local 位置数={}
	do
		local TempID =self.参战单位[编号].玩家id
		local 临时区分id=0
		if UserData[TempID].队伍==0 then
			临时区分id=TempID
		else
			临时区分id=UserData[TempID].队伍
		end
		if self.队伍区分[1]==临时区分id then
				位置数= self.队伍位置[1]
		else
			self.队伍区分[2]=临时区分id
			位置数= self.队伍位置[2]
		end
	end


	local 技能等级 = self:取技能等级(self.参战单位[编号],技能)
	if 技能=="唤灵·魂火" then
		属性集合={技能={},气血上限=技能等级*10,伤害=技能等级*3,灵力=技能等级,速度=技能等级,名称="怨灵",等级=技能等级,防御=技能等级,造型="进阶混沌兽",染色组={1,0},染色方案=2042,饰品=true}
		召唤数量= 2+ math.floor(技能等级/125)
	elseif 技能=="呼子唤孙"	then
		属性集合={技能={},气血上限=技能等级*12,伤害=技能等级*5,灵力=技能等级,速度=技能等级,名称="巨力神猿",等级=技能等级,防御=技能等级,造型="巨力神猿"}
	elseif 技能=="唤魔·堕羽"	then
		local 寻找怨灵 ={}
		local 寻找幻魔 ={}
			for i=1,#self.寻找id do
				if self.参战单位[self.寻找id[i]].名称 == "怨灵" and self.参战单位[self.寻找id[i]].玩家id==self.参战单位[编号].玩家id then
					table.insert(寻找怨灵,self.寻找id[i])
				elseif self.参战单位[self.寻找id[i]].名称 == "幻魔" then
					table.insert(寻找幻魔,self.寻找id[i])
				end
			end
			if #寻找怨灵 < 2 then
				self:添加提示(self.参战单位[编号].数字id,"当前你所召唤的怨灵数量不足2个,施法失败")
				return 
			elseif #寻找幻魔 >=2	then 
				self:添加提示(self.参战单位[编号].数字id,"场上已经存在2个幻魔,施法失败")
				return 
			else 
				属性集合={技能={},气血上限=技能等级*20,伤害=技能等级*7,灵力=技能等级*4,速度=技能等级,名称="幻魔",等级=技能等级,防御=技能等级*2,造型="凤凰",染色组={1,0},染色方案=2042,饰品=true}
				--让幻灵消失
				--和法伤挂钩
				local 召还ID ={}
				for i=1,2 do
					self.参战单位[寻找怨灵[i]].单位消失 = 1
					位置数[self.参战单位[ 寻找怨灵[i]].位置] =nil
					table.insert(召还ID,寻找怨灵[i])
				end

				召唤类数量 =召唤类数量-2
				self.战斗流程[#self.战斗流程+1]={流程=1,挨打方=table.copy(召还ID),执行=false,类型="召还"}
				self.等待结束=self.等待结束+5
			end
	elseif 技能=="唤魔·毒魅"	then
	
		local 寻找幻魔 ={}
		for i=1,#self.寻找id do
			if self.参战单位[self.寻找id[i]].名称 == "幻魔" then
				table.insert(寻找幻魔,self.寻找id[i])
			end
		end
		local 寻找id=0
		for n=1,#self.参战单位 do
			if self.参战单位[n].玩家id==self.参战单位[编号].玩家id and self.参战单位[n].战斗类型=="召唤兽"and not self.参战单位[n].单位消失 then
				寻找id=n
			end
		end
		if	寻找id==0 then
		self:添加提示(self.参战单位[编号].数字id,"施法失败,你还没有参战的召唤兽")
		return
		elseif #寻找幻魔 >=2	then 
		self:添加提示(self.参战单位[编号].数字id,"场上已经存在2个幻魔,施法失败")
			return 
		else 
			属性集合={技能={},气血上限=技能等级*20,伤害=技能等级*7,灵力=技能等级*4,速度=技能等级,名称="幻魔",等级=技能等级,防御=技能等级*2,造型="进阶巴蛇",染色组={1,0},染色方案=2042,饰品=false}

			if self.参战单位[编号].神器=="泪雨零铃" then
				属性集合.伤害=属性集合.伤害*1.2
				属性集合.技能={"高级神佑复生"}
			end

			self.参战单位[寻找id].单位消失 = 1
			self.战斗流程[#self.战斗流程+1]={流程=1,挨打方={寻找id},执行=false,类型="召还"}
			self.等待结束=self.等待结束+5
		end
	elseif 技能=="唤灵·焚魂"	then
		属性集合={技能={},气血上限=技能等级*10,伤害=技能等级*3,灵力=技能等级,速度=技能等级,名称="怨灵",等级=技能等级,防御=技能等级,造型="进阶混沌兽",染色组={1,0},染色方案=2042,饰品=true}
		召唤数量=6

		local 寻找id=0
		for n=1,#self.参战单位 do
			if self.参战单位[n].玩家id==self.参战单位[编号].玩家id and self.参战单位[n].战斗类型=="召唤兽"and not self.参战单位[n].单位消失 then
				寻找id=n
			end
		end
		if	寻找id==0 then
			self:添加提示(self.参战单位[编号].数字id,"施法失败,你还没有参照的召唤兽")
			return
		end
		self.参战单位[寻找id].单位消失 = 1
		self.战斗流程[#self.战斗流程+1]={流程=1,挨打方={寻找id},执行=false,类型="召还"}
		self.等待结束=self.等待结束+5
	elseif 技能=="天魔觉醒"	then
		return
	end
 

 --位置设置
 --销毁设置
 
	if 召唤类数量+召唤数量 >6 then
		self:添加提示(self.参战单位[编号].数字id,"我方的召唤数量已经超出上限")
		return 0
	elseif self:技能消耗(self.参战单位[编号],1) ==false then
		self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
		return 0
	end

	local TempData ={}
	for i=1,召唤数量 do
		self.创建id=#self.参战单位+1
		self:设置队伍区分(self.参战单位[编号].玩家id)

		self.参战单位[self.创建id]={}
		self.参战单位[self.创建id].命令数据={下达=false,类型="",目标="",附加=0,参数=0}
		self.参战单位[self.创建id].战斗类型="召唤类"
		table.insert(self.参战单位[编号].召唤类,self.创建id) 
		self.参战单位[self.创建id].队伍id=self.参战单位[编号].队伍id
		self.参战单位[self.创建id].数字id=self.参战单位[编号].数字id
		self.参战单位[self.创建id].玩家id=self.参战单位[编号].玩家id
	local 位置数量 =0
	for k,v in pairs(位置数) do
		位置数量=位置数量+1
	end
	
		if 位置数量 >=5 then
		for i=11,16 do
				if not 位置数[i] then
					self.参战单位[self.创建id].位置 = i
					位置数[i] =1
					break
				end
			end
		--self.参战单位[self.创建id].位置=#self.寻找id+i+10 --需要设置
		else 
			for i=1,5 do
				if not 位置数[i] then
					self.参战单位[self.创建id].位置 = i
					位置数[i] =1
					break
				end
			end
		end	

		local 女魃墓元神加成 = 1
		if self.参战单位[编号].门派 == "女魃墓" then
			if self.参战单位[编号].助战 == nil and UserData[self.参战单位[编号].玩家id] ~= nil and UserData[self.参战单位[编号].玩家id].角色.元神 ~= nil and UserData[self.参战单位[编号].玩家id].角色.元神 > 0 then
				local 元神加成下限 = SpiritData[UserData[self.参战单位[编号].玩家id].角色.门派][UserData[self.参战单位[编号].玩家id].角色.元神.."介效果1下限"]
				local 元神加成上限 = SpiritData[UserData[self.参战单位[编号].玩家id].角色.门派][UserData[self.参战单位[编号].玩家id].角色.元神.."介效果1上限"]
				if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
					女魃墓元神加成 = 女魃墓元神加成 + 取随机数(元神加成下限,元神加成上限)/100
				end
			end
		end


	for i,v in pairs(属性集合) do
		self.参战单位[self.创建id][i] = v
	end

		self.参战单位[self.创建id].法防= math.floor(self.参战单位[self.创建id].灵力* 0.3 * 女魃墓元神加成)
		self.参战单位[self.创建id].命中=math.floor(self.参战单位[self.创建id].伤害 * 女魃墓元神加成)
		self.参战单位[self.创建id].当前气血=math.floor(self.参战单位[self.创建id].气血上限 * 女魃墓元神加成)
		self.参战单位[self.创建id].最大气血=math.floor(self.参战单位[self.创建id].气血上限 * 女魃墓元神加成)
		self.参战单位[self.创建id].力量=math.floor(100 * 女魃墓元神加成)
		self.参战单位[self.创建id].魔力=math.floor(100 * 女魃墓元神加成)
		self.参战单位[self.创建id].躲闪=math.floor(100 * 女魃墓元神加成)
		self.参战单位[self.创建id].体质=math.floor(100 * 女魃墓元神加成)
		self.参战单位[self.创建id].耐力=math.floor(100 * 女魃墓元神加成)
		self.参战单位[self.创建id].敏捷=math.floor(100 * 女魃墓元神加成)
		self.参战单位[self.创建id].特技数据={}
		self.参战单位[self.创建id].当前魔法=math.floor(8000 * 女魃墓元神加成)
		self.参战单位[self.创建id].魔法上限=math.floor(8000 * 女魃墓元神加成)
		self.参战单位[self.创建id].编号=self.创建id
		self.参战单位[self.创建id].必杀=5
		self.参战单位[self.创建id].法宝效果={}
		self.参战单位[self.创建id].奇经八脉={}
		self.参战单位[self.创建id].神器=""
		self.参战单位[self.创建id].战意点数=0
		self.参战单位[self.创建id].攻之械=0
		self.参战单位[self.创建id].暴击伤害=0

		self.参战单位[self.创建id].识药=0
		self.参战单位[self.创建id].吸血=0
		self.参战单位[self.创建id].法术吸血=0

		self.参战单位[self.创建id].阴伤=0
		self.参战单位[self.创建id].驱散=0	---小法	内丹
		self.参战单位[self.创建id].连击=0
		self.参战单位[self.创建id].额外法术伤害=0
		self.参战单位[self.创建id].理直气壮=false
		self.参战单位[self.创建id].反震=0
		self.参战单位[self.创建id].反击=0
		self.参战单位[self.创建id].法暴=0
		self.参战单位[self.创建id].法连=0
		self.参战单位[self.创建id].法波=0
		self.参战单位[self.创建id].魔心=1
		self.参战单位[self.创建id].神佑=0
		self.参战单位[self.创建id].复活=0
		self.参战单位[self.创建id].冥想=0
		self.参战单位[self.创建id].风起龙游=0
		self.参战单位[self.创建id].无赦魔决=0
		self.参战单位[self.创建id].慧根=1
		self.参战单位[self.创建id].再生=0
		self.参战单位[self.创建id].毒=0
		self.参战单位[self.创建id].驱鬼=0
		self.参战单位[self.创建id].火吸=0
		self.参战单位[self.创建id].水吸=0
		self.参战单位[self.创建id].雷吸=0
		self.参战单位[self.创建id].土吸=0
		self.参战单位[self.创建id].隐身=0
		self.参战单位[self.创建id].物伤减免=1
		self.参战单位[self.创建id].法伤减免=1
		self.参战单位[self.创建id].武器伤害=0
		self.参战单位[self.创建id].修炼数据={}
		for n=1,#全局变量.修炼名称 do
			self.参战单位[self.创建id].修炼数据[全局变量.修炼名称[n]]=0
		end
		self.参战单位[self.创建id].封印状态=false
		self.参战单位[self.创建id].命令数据={下达=false,类型="",目标="",附加=0,参数=0}
		self.参战单位[self.创建id].法术状态组={}
		self.参战单位[self.创建id].主动技能={}
		
		for i=1,#幻化战斗属性 do
			if	self.参战单位[self.创建id][幻化战斗属性[i]]==nil then
				self.参战单位[self.创建id][幻化战斗属性[i]]=0
			end
		end
		table.insert(TempData,self.参战单位[self.创建id])
	end
	self.战斗流程[#self.战斗流程+1]={流程=1,攻击方=编号,参数=技能,执行=false,名称=self.参战单位[self.创建id].名称,数据=table.copy(TempData),类型="召唤类"}
	self.等待结束=self.等待结束+5
 end 
--------------
function FightControl:召唤流程计算(编号,孩子)

 if self.参战单位[编号].参战召唤兽==nil then return 0 end
 self.召唤id=self.参战单位[编号].玩家id
 self.寻找id=0
	for n=1,#self.参战单位 do
		if self.参战单位[n].玩家id==self.召唤id and (self.参战单位[n].战斗类型=="召唤兽" or self.参战单位[n].战斗类型=="孩子") then
			self.寻找id=n
		end
	end
	if not 孩子 then
		for n=1,#self.参战单位[编号].参战召唤兽 do
		if	self.参战单位[编号].参战召唤兽[n]==self.参战单位[编号].命令数据.目标 then
			self:添加提示(self.参战单位[编号].数字id,"您的这只召唤兽不能重复出战")
			return 0
			end
		end
		if UserData[self.召唤id].召唤兽.数据[self.参战单位[编号].命令数据.目标].忠诚<=60 then
			self:添加提示(self.参战单位[编号].数字id,"您的这只召唤兽因忠诚度过低而不愿参战")
			return 0
		elseif UserData[self.召唤id].召唤兽.数据[self.参战单位[编号].命令数据.目标].寿命<=50 then
			self:添加提示(self.参战单位[编号].数字id,"您的这只召唤兽因寿命过低而不愿参战")
			return 0
			elseif UserData[self.召唤id].召唤兽.数据[self.参战单位[编号].命令数据.目标].禁止使用 then
			self:添加提示(self.参战单位[编号].数字id,"您的这只召唤兽已被禁止使用")
			return 0
			elseif UserData[self.召唤id].召唤兽.数据[self.参战单位[编号].命令数据.目标].等级>UserData[self.召唤id].角色.等级+10 then

			self:添加提示(self.参战单位[编号].数字id,"您的这只召唤兽因等级高于角色10级无法参战")
			return 0
			elseif UserData[self.召唤id].召唤兽.数据[self.参战单位[编号].命令数据.目标].等级>UserData[self.召唤id].角色.等级+10 then

			self:添加提示(self.参战单位[编号].数字id,"您的这只召唤兽因等级高于角色10级无法参战")
			return 0
			elseif	UserData[self.召唤id].召唤兽.数据[self.参战单位[编号].命令数据.目标].参战等级>UserData[self.召唤id].角色.等级+10 then

			self:添加提示(self.参战单位[编号].数字id,"您的角色当前无法驾驭这样的召唤兽")
			return 0

			end
	else
		if self.参战单位[编号].参战子女 then
			self:添加提示(self.参战单位[编号].数字id,"您的孩子已经出战过了")
			return 0
		end
	end
 if self.寻找id==0 then
	self.创建id=#self.参战单位+1
	self:设置队伍区分(self.召唤id)
	else
	self.创建id=self.寻找id
	end
 self.参战单位[self.创建id]={}
	self.参战单位[self.创建id].命令数据={下达=false,类型="",目标="",附加=0,参数=0}
	if not 孩子	then
		UserData[self.召唤id].召唤兽.数据.参战=self.参战单位[编号].命令数据.目标
		self.参战单位[self.创建id]=table.loadstring(UserData[self.召唤id].召唤兽:获取指定数据(UserData[self.召唤id].召唤兽.数据.参战))
	UserData[self.召唤id].召唤兽.数据[UserData[self.召唤id].召唤兽.数据.参战].忠诚=UserData[self.召唤id].召唤兽.数据[UserData[self.召唤id].召唤兽.数据.参战].忠诚-0.1
	UserData[self.召唤id].召唤兽.数据[UserData[self.召唤id].召唤兽.数据.参战].寿命=UserData[self.召唤id].召唤兽.数据[UserData[self.召唤id].召唤兽.数据.参战].寿命-0.5
	self.参战单位[编号].参战召唤兽[#self.参战单位[编号].参战召唤兽+1]=self.参战单位[编号].命令数据.目标
	self.参战单位[self.创建id].战斗类型="召唤兽"
	else
	self.参战单位[编号].参战子女 =true
	self.参战单位[self.创建id]=table.copy(UserData[self.召唤id].角色.子女列表[UserData[self.召唤id].角色.子女列表.参战])
	self.参战单位[self.创建id].战斗类型="孩子"
	end


	self.参战单位[self.创建id].位置=self.参战单位[编号].位置+5
	self.参战单位[self.创建id].队伍id=self.参战单位[编号].队伍id
	self.参战单位[self.创建id].数字id=self.参战单位[编号].数字id
	self.参战单位[self.创建id].玩家id=self.参战单位[编号].玩家id
	self.参战单位[self.创建id].编号=self.创建id

	if self.参战单位[self.创建id].当前气血<=0 then
		self.参战单位[self.创建id].当前气血=1
	end
	self.参战单位[self.创建id].命中=self.参战单位[self.创建id].伤害
	self.参战单位[self.创建id].必杀=5
	self.参战单位[self.创建id].法宝效果={}
	self.参战单位[self.创建id].奇经八脉={}
	self.参战单位[self.创建id].神器=""
	self.参战单位[self.创建id].战意点数=0
	self.参战单位[self.创建id].攻之械=0
		self.参战单位[self.创建id].暴击伤害=0
	self.参战单位[self.创建id].识药=0
	self.参战单位[self.创建id].吸血=0
	self.参战单位[self.创建id].法术吸血=0

	self.参战单位[self.创建id].阴伤=0
	self.参战单位[self.创建id].驱散=0	---小法	内丹
	self.参战单位[self.创建id].连击=0
	self.参战单位[self.创建id].额外法术伤害=0
	self.参战单位[self.创建id].理直气壮=false
	self.参战单位[self.创建id].反震=0
	self.参战单位[self.创建id].反击=0
	self.参战单位[self.创建id].法暴=0
	self.参战单位[self.创建id].法连=0
	self.参战单位[self.创建id].法波=0
	self.参战单位[self.创建id].魔心=1
	self.参战单位[self.创建id].神佑=0
	self.参战单位[self.创建id].无赦魔决=0
	self.参战单位[self.创建id].风起龙游=0
	self.参战单位[self.创建id].复活=0
	self.参战单位[self.创建id].冥想=0
	self.参战单位[self.创建id].慧根=1
	self.参战单位[self.创建id].再生=0
	self.参战单位[self.创建id].毒=0
	self.参战单位[self.创建id].驱鬼=0
	self.参战单位[self.创建id].火吸=0
	self.参战单位[self.创建id].水吸=0
	self.参战单位[self.创建id].雷吸=0
	self.参战单位[self.创建id].土吸=0
	self.参战单位[self.创建id].隐身=0
	self.参战单位[self.创建id].物伤减免=1
	self.参战单位[self.创建id].法伤减免=1
	self.参战单位[self.创建id].武器伤害=0
	self.参战单位[self.创建id].封印状态=false
	self.参战单位[self.创建id].命令数据={下达=false,类型="",目标="",附加=0,参数=0}
	self.参战单位[self.创建id].法术状态组={}

	for i=1,#幻化战斗属性 do
			if	self.参战单位[self.创建id][幻化战斗属性[i]]==nil then
		self.参战单位[self.创建id][幻化战斗属性[i]]=0
		end
		end
	self.参战单位[self.创建id].主动技能={}
	self:添加技能属性(self.创建id,self.参战单位[self.创建id].技能,true)
	if not 孩子 then
		self:添加内丹属性(self.创建id,self.参战单位[self.创建id].内丹)
		self:添加特性属性(self.创建id,self.参战单位[self.创建id].特性,self.参战单位[self.创建id].特性几率)
		self:添加进场特性属性(self.创建id,self.参战单位[self.创建id].特性,self.参战单位[self.创建id].特性几率)
	end
		self.战斗流程[#self.战斗流程+1]={流程=1,进行=false,id=self.创建id,名称=self.参战单位[self.创建id].名称,数据=table.copy(self.参战单位[self.创建id]),类型="召唤"}
		local 法术状态名称={"灵法","灵断","护佑","御风","怒吼","阳护","灵刃","逍遥游","瞬击","瞬法","开门见山","高级进击必杀" ,"进击必杀" ,"高级进击法暴" ,"进击法暴" ,"高级遗志" ,"遗志" }

	for i=1,#法术状态名称 do
	if self.参战单位[self.创建id].法术状态组[法术状态名称[i]] then
		self.战斗流程[#self.战斗流程+1]={流程=108,执行=false,攻击方=self.创建id,挨打方=self.创建id,结束=false,类型="技能",参数=法术状态名称[i]}

	end
	end

	

	self.等待结束=self.等待结束+2
	self.参战单位[self.创建id].法防=math.floor(self.参战单位[self.创建id].魔力*0.5)
	self.参战单位[self.创建id].伤害=math.floor(self.参战单位[self.创建id].伤害*1.1)

	self.参战单位[self.创建id].修炼数据={}
	for n=1,#全局变量.修炼名称 do

		self.参战单位[self.创建id].修炼数据[全局变量.修炼名称[n]]=UserData[self.召唤id].角色.召唤兽修炼[全局变量.修炼名称[n]].等级
		end
	if self.参战单位[self.创建id].法术状态组["瞬法"] and self.参战单位[self.创建id].法术状态组["瞬法"].技能 then
	self:瞬法处理(self.创建id)
	elseif self.参战单位[self.创建id].法术状态组["瞬击"] or self.参战单位[self.创建id].法术状态组["开门见山"] or self.参战单位[self.创建id].法术状态组["逍遥游"]	then
	self:瞬击处理(self.创建id)
	end
	
	if self.参战单位[self.创建id].法术状态组["峰回路转"] and self.回合数>4	then
		self:峰回路转处理(self.创建id)
	end
		if self.参战单位[self.创建id].隐身~=0 then
	self:隐身处理(self.创建id)
	end
	if 孩子 then
		SendMessage(UserData[self.召唤id].连接id,2005,RoleControl:GetChildrenHP(UserData[self.召唤id]))
	else
		SendMessage(UserData[self.召唤id].连接id,2005,UserData[self.召唤id].召唤兽:取头像数据())
	end

 end

function FightControl:攻击流程计算(编号,伤害,连击)
	if self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false or self:取隐身判断(编号,self.参战单位[编号].命令数据.目标)==false then
	self.相关id=self.参战单位[编号].队伍id
	self.临时目标=self:取随机单位(编号,self.相关id,1,1)
		if self.临时目标==0 then
		self.参战单位[编号].命令数据.目标=0
		else
		self.参战单位[编号].命令数据.目标=self.临时目标[1]
		end
	end
	if self.参战单位[编号].命令数据.目标==0 then --找不到可攻击的目标
		return 0
	end
	self.战斗流程[#self.战斗流程+1]={流程=1,执行=false,结束=false,类型="攻击",反震=false,反击=false,吸血=false,挨打动作="被击中"}--流程=1为从攻击
	self.战斗流程[#self.战斗流程].攻击方=编号
	self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
	self.躲避=false
	self.等待结束=self.等待结束+6
	self.临时躲避=self.参战单位[self.参战单位[编号].命令数据.目标].躲闪+self.参战单位[编号].命中
	self.临时比较=self.参战单位[编号].命中/self.临时躲避*100
	if self.临时比较>=50 then
		self.临时比较=50
	end
	-------开始躲避
	self.参战单位[编号].从天而降 =nil

	if self.参战单位[编号].奇经八脉.六道无量 then
		self.躲避=false
	elseif self.临时比较>math.random(1000) then
		self.躲避=true
	else
		self.躲避=false
		if self.参战单位[编号].从天而降开关 then
			if math.random(100)< 60	then
				self.参战单位[编号].从天而降 =true
			else
				self.躲避=false
			end
		end
	end

	self.战斗流程[#self.战斗流程].躲避=self.躲避

	-----------------躲避判断
	if self.躲避==false then
		self.临时伤害=self:取物理伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
		self.保护=false
		self.必杀=self:取是否必杀(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
		if self.必杀 then
			if self.参战单位[编号].神器 =="威服天下" then
				self.临时伤害=math.floor(self.临时伤害*2.26)+self.参战单位[编号].暴击伤害
			else 
				self.临时伤害=math.floor(self.临时伤害*2)+self.参战单位[编号].暴击伤害
			end
			self.战斗流程[#self.战斗流程].必杀动作=1
			self.苍鸾怒击=true
		end
		if self.参战单位[编号].队伍id == self.参战单位[self.参战单位[编号].命令数据.目标].队伍id and not self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.催眠符 then
			self.临时伤害=1
		end
		if self.参战单位[self.参战单位[编号].命令数据.目标].命令数据.类型=="防御" and not self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.催眠符 then
			if not self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.天地同寿 then
				self.临时伤害=math.floor(self.临时伤害*0.5)
			end
			self.战斗流程[#self.战斗流程].挨打动作="防御"
			if self.临时伤害<1 then
				self.临时伤害=1
			end
		end
		if self.参战单位[self.参战单位[编号].命令数据.目标].命令数据.类型=="防御" and not self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.煞气诀 then
			if not self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.天地同寿 then
				self.临时伤害=math.floor(self.临时伤害*0.5)
			end
				self.战斗流程[#self.战斗流程].挨打动作="防御"
			if self.临时伤害<1 then
				self.临时伤害=1
			end
		end
		self.保护=self:取保护单位(self.参战单位[编号].命令数据.目标)
		self.吸血=self:取是否吸血(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
		if self.参战单位[编号].奇经八脉.化血 and not self.吸血 and math.random(100) <= 30 then
			self.战斗流程[#self.战斗流程].吸血伤害=self:取物理伤害结果(math.floor(self.临时伤害*0.16),self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],1,"加血")
			self.战斗流程[#self.战斗流程].吸血=true
		end
		-----------------吸血伤害计算
		if self.吸血 then
			self.战斗流程[#self.战斗流程].吸血伤害=self:取物理伤害结果(math.floor(self.临时伤害*self.参战单位[编号].吸血),self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],1,"加血")
			self.战斗流程[#self.战斗流程].吸血=true
		end
	------------------保护流程
		if self.保护==false then
			if self.参战单位[self.参战单位[编号].命令数据.目标].法宝效果.蟠龙玉璧 and math.random(100) < 30 then
				self.临时伤害=self:取物理伤害结果(self.临时伤害,self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],(1-self.参战单位[self.参战单位[编号].命令数据.目标].法宝效果.蟠龙玉璧),"掉血")
				self.战斗流程[#self.战斗流程].金甲仙衣 = true
			elseif self.参战单位[self.参战单位[编号].命令数据.目标].法宝效果.金甲仙衣 and math.random(100) < 30 then
				self.临时伤害=self:取物理伤害结果(self.临时伤害,self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],(1-self.参战单位[self.参战单位[编号].命令数据.目标].法宝效果.金甲仙衣),"掉血")
				self.战斗流程[#self.战斗流程].金甲仙衣 = true
			else
				self.临时伤害=self:取物理伤害结果(self.临时伤害,self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],1,"掉血")
			end
			self.战斗流程[#self.战斗流程].挨打数据={伤害=self.临时伤害.伤害,类型=self.临时伤害.类型,死亡=self.临时伤害.死亡}
			if math.random(100)<=self.参战单位[编号].毒 and self.参战单位[self.参战单位[编号].命令数据.目标].复活==0 and not self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.百毒不侵 then
				self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组["毒"]={回合=5,等级=self.参战单位[编号].等级}

				self.战斗流程[#self.战斗流程].毒=true
			end
			if	self:取存活状态(self.参战单位[编号].命令数据.目标) and	self:取存活状态(编号) then
				self.反震=self:取是否反震(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
				if self.反震 then
				self.临时伤害=self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打数据.伤害,self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],0.5,"掉血",1)
				self.战斗流程[#self.战斗流程].反震数据={伤害=self.临时伤害.伤害,类型=self.临时伤害.类型,死亡=self.临时伤害.死亡}
				self.等待结束=self.等待结束+4
				end
			end
			if	self:取存活状态(self.参战单位[编号].命令数据.目标) and	self:取存活状态(编号) then
				self.反击=self:取是否反击(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
				if self.反击 then
				self.临时伤害=self:取物理伤害(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],1)
				self.临时伤害=self:取物理伤害结果(self.临时伤害,self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],0.5,"掉血",1)
				self.战斗流程[#self.战斗流程].反击数据={伤害=self.临时伤害.伤害,类型=self.临时伤害.类型,死亡=self.临时伤害.死亡}
				self.等待结束=self.等待结束+4
				end
			end
		else
			self.临时伤害1=self:取物理伤害结果(self.临时伤害,self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],0.3,"掉血")
			self.战斗流程[#self.战斗流程].挨打数据={伤害=self.临时伤害1.伤害,类型=self.临时伤害1.类型,死亡=self.临时伤害1.死亡}
			self.临时伤害1=self:取物理伤害结果(self.临时伤害,self.参战单位[编号],self.参战单位[self.保护],0.7,"掉血",1)
			self.战斗流程[#self.战斗流程].保护数据={编号=self.保护,伤害=self.临时伤害1.伤害,类型=self.临时伤害1.类型,死亡=self.临时伤害1.死亡}
			self.等待结束=self.等待结束+5
		end

		if self.参战单位[self.参战单位[编号].命令数据.目标].奇经八脉.飞龙 and not	self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.神龙摆尾 and	math.random(100) <=30	then
			self.参战单位[self.参战单位[编号].命令数据.目标].命令数据={类型="技能",参数="神龙摆尾",目标=self.参战单位[编号].命令数据.目标}
			self:群体状态法术计算(self.参战单位[编号].命令数据.目标)
		end
		if self.参战单位[self.参战单位[编号].命令数据.目标].奇经八脉.不舍 and	math.random(100) <=30	then
			self.参战单位[self.参战单位[编号].命令数据.目标].命令数据={类型="技能",参数="归元咒",目标=self.参战单位[编号].命令数据.目标}
			self:单体恢复法术计算(self.参战单位[编号].命令数据.目标)
		end


		if self.参战单位[编号].溅射	then
		self.相关id=self.参战单位[编号].队伍id
		self.临时目标=self:取随机单位(编号,self.相关id,1,3)
			if self.临时目标==0 then
				return 0
			end
			for n=#self.临时目标,1,-1 do
				if self.临时目标[n]== self.参战单位[编号].命令数据.目标 then
				table.remove(self.临时目标,n)
				break
				end
			end
			self.战斗流程[#self.战斗流程+1]={流程=5,执行=false,结束=false,类型="攻击",参数="溅射"}--流程=30为从非物理技能开始
			self.战斗流程[#self.战斗流程].攻击方=编号
			self.战斗流程[#self.战斗流程].挨打方={}
			self.等待结束=self.等待结束+5
			for n=1,#self.临时目标 do
				self.等待结束=self.等待结束+2
				self.战斗流程[#self.战斗流程].挨打方[n]={反震=false}
				self.战斗流程[#self.战斗流程].挨打方[n].编号=self.临时目标[n]
				self.战斗流程[#self.战斗流程].挨打方[n].伤害= self:取物理伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
				if self.参战单位[编号].奇经八脉.风刃	then
				self.临时伤害= self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.临时目标[n]],self.参战单位[编号].溅射,"掉血",nil,self.参战单位[编号].等级*3)
				else
				self.临时伤害= self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.临时目标[n]],self.参战单位[编号].溅射,"掉血")
				end

				self.战斗流程[#self.战斗流程].挨打方[n].伤害=self.临时伤害.伤害
				self.战斗流程[#self.战斗流程].挨打方[n].类型=self.临时伤害.类型
				self.战斗流程[#self.战斗流程].挨打方[n].死亡=self.临时伤害.死亡
			end
		end
		-----------------判读挨打结束状态
	end
	--------------------躲避判断结束
	if	self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.火甲术 and self:取存活状态(self.参战单位[编号].命令数据.目标) and	self:取存活状态(编号) then --火甲术
		self:单体法术计算(self.参战单位[编号].命令数据.目标,nil,"三昧真火",编号)
	end
end


function FightControl:技能计算(攻击方,技能名称)
	if  技能名称=="象形" or 技能名称=="大闹天宫" or 技能名称=="横扫千军" or 技能名称=="烟雨剑法" or 技能名称=="连环击"or 技能名称=="善恶有报"or 技能名称=="裂石"or 技能名称=="天崩地裂"or 技能名称=="断岳势"or 技能名称=="腾雷" or 技能名称=="满天花雨"or 技能名称=="力劈华山"or 技能名称=="壁垒击破"or 技能名称=="惊心一剑" or 技能名称=="知己知彼"or 技能名称=="翩鸿一击"or 技能名称=="长驱直入"or 技能名称=="天命剑法" or 技能名称=="哼哼哈兮" or 技能名称=="当头一棒"or 技能名称=="杀威铁棒"or 技能名称=="泼天乱棒" or 技能名称=="诱袭" or 技能名称=="死亡召唤" or 技能名称=="蚩尤之搏" then --单体法术设置
		self:单体物理法术计算(攻击方)
	elseif 技能名称=="夺命蛛丝"or 技能名称=="天魔解体"or 技能名称=="黄泉之息"or 技能名称=="夜舞倾城" or 技能名称=="水攻" or 技能名称=="雷击" or 技能名称=="烈火" or 技能名称=="落岩" or 技能名称=="龙腾" or 技能名称=="三昧真火" or 技能名称=="五雷轰顶" or 技能名称=="判官令" or 技能名称=="血雨" or 技能名称=="上古灵符"or 技能名称=="月光" or 技能名称=="上古灵符"then --单体法术设置
		self:单体法术计算(攻击方)
		if self.参战单位[攻击方].法连>=math.random(100) and 技能名称~="力劈华山" then
			self:单体法术计算(攻击方)
		end
		if self.参战单位[攻击方].法术状态组.佛法无边 and math.random(1000)<=self.参战单位[攻击方].法术状态组.佛法无边.几率 then
			self:单体法术计算(攻击方)
		end
	elseif 技能名称=="荆棘舞" or 技能名称=="尘土刃" or 技能名称=="冰川怒"	then
		self:单体法术计算(攻击方)
		if self.参战单位[攻击方].法宝效果.月影 and	math.random(200)<=self.参战单位[攻击方].法宝效果.月影 then
			self:单体法术计算(攻击方)
		end
	elseif 技能名称=="唤灵·魂火" or	技能名称=="唤魔·堕羽" or 技能名称=="唤魔·毒魅" or 技能名称=="唤灵·焚魂" or 技能名称=="天魔觉醒" or 技能名称=="呼子唤孙" then
		self:技能召唤流程(攻击方,技能名称)
	elseif 技能名称=="光照万象"	then
		local 回合限制 =Config.光照回合
 
		if	self.回合数 <回合限制	and self.参战单位[攻击方].数字id and	UserData[self.参战单位[攻击方].数字id]	then
		SendMessage(UserData[self.参战单位[攻击方].数字id].连接id, 9,"#xt/#y/".."当前回合数为：#r/"..self.回合数.."#y/暂时无法使用该技能！")
		SendMessage(UserData[self.参战单位[攻击方].数字id].连接id, 7,"#xt/#y/".."当前回合数为：#r/"..self.回合数.."#y/暂时无法使用该技能！")
		return
		end
		self.临时目标=self:取随机单位(攻击方,self.参战单位[攻击方].队伍id,1,1)

		if self.临时目标 ==0 then
		return
		end
		for i=1,#self.参战单位[攻击方].主动技能 do
		if self.参战单位[攻击方].主动技能[i].名称~="光照万象"	then
					self.参战单位[攻击方].命令数据={下达=false,类型="技能",目标=self.临时目标,附加=0,参数=self.参战单位[攻击方].主动技能[i].名称}

		self:技能计算(攻击方,self.参战单位[攻击方].主动技能[i].名称)
		end

		end
	elseif 技能名称=="雷霆万钧"or 技能名称=="九天玄火" or 技能名称=="落雷符"or 技能名称=="阎罗令" or 技能名称=="天罗地网" or 技能名称=="唧唧歪歪" or 技能名称== "巨岩破" or 技能名称=="苍茫树" or 技能名称=="靛沧海" or 技能名称=="地裂火"or 技能名称=="日光华"or 技能名称=="夺命咒"or 技能名称=="破击"or 技能名称=="云暗天昏" or 技能名称=="焚魔烈焰" or 技能名称=="二龙戏珠"or 技能名称=="龙卷雨击"or 技能名称=="飞砂走石"or 技能名称=="泰山压顶"or 技能名称=="水漫金山"or 技能名称=="奔雷咒"or 技能名称=="地狱烈火"or 技能名称=="龙吟"or 技能名称=="雨落寒沙" or 技能名称=="扶摇万里"or 技能名称=="神来气旺"or 技能名称=="天降灵葫" or 技能名称=="八凶法阵"or 技能名称=="叱咤风云"or 技能名称=="流沙轻音"or 技能名称=="食指大动" or 技能名称=="五雷咒"or 技能名称=="落叶萧萧"or 技能名称=="五雷咒" then
			self:群体法术计算(攻击方)
			if self.参战单位[攻击方].法术状态组.佛法无边 and math.random(1000)<=self.参战单位[攻击方].法术状态组.佛法无边.几率 then
				self:群体法术计算(攻击方)
			end

			if	self.参战单位[攻击方].法连 >= math.random(100) then
				self:群体法术计算(攻击方)
				if	技能名称 == "叱咤风云"	then
					local xx=0
					while true do
						xx=xx+5
						if math.floor(self.参战单位[攻击方].法连 - xx)>=math.random(100)	then 
							self:群体法术计算(攻击方)
						else
							break 
						end
					end 
				end
			end
	elseif 技能名称=="落叶萧萧"	then
			self:群体法术计算(攻击方)
			if self.参战单位[攻击方].法宝效果.月影 and	math.random(200)<=self.参战单位[攻击方].法宝效果.月影 then
			self:群体法术计算(攻击方)
			end
	elseif 技能名称=="飘渺式" or 技能名称=="天雷斩" or	技能名称=="牛刀小试" or 技能名称=="鹰击" or	技能名称=="破釜沉舟" or 技能名称=="浪涌" or
	技能名称=="惊涛怒" or 技能名称=="翻江搅海" or 技能名称=="神针撼海" or 技能名称=="剑荡四方" or 技能名称=="进阶力劈华山" or 技能名称=="进阶善恶有报" or 技能名称=="锋芒毕露"or 技能名称=="针锋相对"or 技能名称=="百爪狂杀" or 技能名称=="狮搏"	then --单体法术设置
			self:群体物理法术计算(攻击方)
	elseif	技能名称=="瘴气"or	技能名称=="紧箍咒"or 技能名称=="魔息术"or 技能名称=="炼气化神"or
	技能名称=="威慑"or 技能名称=="离魂符"or 技能名称=="追魂符"or 技能名称=="失心符"or 技能名称=="催眠符"or 技能名称=="含情脉脉" or
	技能名称=="似玉生香"or 技能名称=="如花解语"or 技能名称=="莲步轻舞"or 技能名称=="日月乾坤" or
	技能名称=="百万神兵"or 技能名称=="镇妖"or 技能名称=="定身符"or 技能名称=="失魂符"or 技能名称=="失忆符"or 技能名称=="夺魄令"or
	技能名称=="煞气诀" or	技能名称=="错乱" or 技能名称=="魔音摄魂"or 技能名称=="复苏" or 技能名称=="炽火流离" or 技能名称=="勾魂"or 技能名称=="摄魄"
 or 技能名称 == "娉婷嬝娜" or 技能名称=="锢魂术"	then --封印法术设置
		self:单体封印法术计算(攻击方)
	elseif 技能名称=="尸腐毒" or 技能名称=="雾杀"or	技能名称=="紧箍咒"	then --单体法术设置
		self:单体持续伤害法术计算(攻击方)
	elseif	技能名称=="舍生取义" or	技能名称=="清心" or 技能名称=="解毒" or
	技能名称=="归元咒" or	技能名称=="自在心法" or	技能名称=="净世煌火" or	技能名称=="活血" or	技能名称=="治疗" or	技能名称=="仙人指路" or 技能名称=="移星换斗" or 技能名称=="星月之惠" or 技能名称=="三花聚顶" or
	技能名称=="乾天罡气"	then --单体法术设置
		self:单体恢复法术计算(攻击方)
			if self.参战单位[攻击方].法术状态组.佛法无边 and math.random(1000)<=self.参战单位[攻击方].法术状态组.佛法无边.几率 then
			self:单体恢复法术计算(攻击方)
			end
	elseif 技能名称=="飞花摘叶"or 技能名称=="一笑倾城" or 技能名称=="怨怖之泣" then
	self:群体封印法术计算(攻击方)
	elseif	技能名称=="推气过宫" or 技能名称=="生命之泉" or 技能名称=="匠心·蓄锐" or 技能名称=="峰回路转" or 技能名称=="地涌金莲"or 技能名称=="普渡众生"	then --单体法术设置
		self:群体恢复法术计算(攻击方)
	-----------群体状态法术
	elseif	技能名称=="火甲术"or 技能名称=="颠倒五行"or 技能名称=="乾坤妙法"or 技能名称=="天地同寿"or 技能名称=="灵动九天"
 or 技能名称=="金刚镯"or 技能名称=="神龙摆尾"or 技能名称=="碎甲符"or 技能名称=="佛法无边"or 技能名称=="幽冥鬼眼"
 or 技能名称=="一苇渡江"or 技能名称=="金刚护体"or 技能名称=="金刚护法" or 技能名称=="匠心·削铁"or 技能名称=="匠心·固甲" 
 or 技能名称=="不动如山"or 技能名称=="韦陀护法"or 技能名称=="明光宝烛"or 技能名称=="镇魂诀"
 or 技能名称=="金身舍利"or 技能名称=="蜜润"or 技能名称=="红袖添香"or 技能名称=="摧心术"or 技能名称 =="惊魂掌" or	技能名称 =="谜毒之缚"	
 or 技能名称 =="达摩护体" then
		self:群体状态法术计算(攻击方)
	elseif	技能名称=="定心术"or 技能名称=="杀气诀"or 技能名称=="无畏布施" or 技能名称=="无所遁形" or 技能名称=="四面埋伏" or 技能名称=="潜力激发" or 技能名称=="魔王回首"or 技能名称=="牛劲"or 技能名称=="安神诀" or
	技能名称=="变身" or 技能名称=="极度疯狂"or 技能名称=="分身术"or 技能名称=="百毒不侵" or 技能名称=="楚楚可怜" or 技能名称=="解封" or
	技能名称=="盘丝阵" or 技能名称=="移魂化骨"or 技能名称=="天神护法"or 技能名称=="天神护体"or	技能名称=="后发制人" or
	技能名称=="逆鳞" or 技能名称=="炎护"or 技能名称=="气慑天军"or 技能名称=="威震凌霄" or 技能名称=="铜头铁臂" or 技能名称=="碎星诀"or 技能名称=="乘风破浪" or 技能名称=="宁心" or 技能名称== "魂飞魄散" or 技能名称=="驱魔"
	or 技能名称=="寡欲令" or 技能名称=="驱尸" or 技能名称=="法术防御" or 技能名称=="八戒上身" or 技能名称=="波澜不惊" or 技能名称== "凝神术" then
			self:单体状态法术计算(攻击方)
	elseif 技能名称=="还阳术" or	技能名称=="我佛慈悲"or	技能名称=="莲花心音" or	技能名称=="还魂咒" or	技能名称=="杨柳甘露" or 技能名称=="由己渡人" then --单体法术设置
			self:单体复活法术计算(攻击方)
	elseif	技能名称=="修罗隐身"	then
			self:修罗隐身计算(攻击方)
	end
 end

 
function FightControl:捕捉流程计算(编号)
	if self.战斗类型~=100001 then
	self:添加提示(self.参战单位[编号].数字id,"当前战斗类型无法使用捕捉指令")
	return 0
	elseif self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false then
	self:添加提示(self.参战单位[编号].数字id,"目标当前无法捕捉")
	return 0
	end
 self.消耗魔法=15+self.参战单位[self.参战单位[编号].命令数据.目标].等级

	local Name =self.参战单位[self.参战单位[编号].命令数据.目标].造型
 if self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false then
	self:添加提示(self.参战单位[编号].数字id,"目标当前无法捕捉")
	return 0
	elseif Config.bb捕捉限制 =="有" and self.参战单位[编号].等级< BBData[Name].等级 then
	self:添加提示(self.参战单位[编号].数字id,"捕捉该目标需要人物等级达到"..BBData[Name].等级.."级")
	return 0
	elseif	self.参战单位[编号].当前魔法<self.消耗魔法 then
	self:添加提示(self.参战单位[编号].数字id,"你没那么多的魔法")
	return 0
	elseif	#UserData[self.参战单位[编号].玩家id].召唤兽.数据>=UserData[self.参战单位[编号].玩家id].召唤兽.数据.召唤兽上限 then
	self:添加提示(self.参战单位[编号].数字id,"你当前可携带的召唤兽数量已达上限")
	return 0
	end
 self.等待结束=self.等待结束+5
 self.初始几率=35
 self.初始几率=self.初始几率+math.floor(self.参战单位[self.参战单位[编号].命令数据.目标].当前气血/self.参战单位[self.参战单位[编号].命令数据.目标].气血上限*100)
 if self.初始几率>80 then self.初始几率=80 end
 if math.random(100)<=self.初始几率 then
	self.捕捉结果=true
	self.参战单位[self.参战单位[编号].命令数据.目标].单位消失=1
	local lx = ""
	if self.参战单位[self.参战单位[编号].命令数据.目标].种类 == 0 then
		lx = "野怪"
	elseif self.参战单位[self.参战单位[编号].命令数据.目标].种类 == 1 then
		lx = "野怪"
	elseif self.参战单位[self.参战单位[编号].命令数据.目标].种类 == 2 then
		lx = "宝宝"
	elseif self.参战单位[self.参战单位[编号].命令数据.目标].种类 == 3 then
		lx = "变异"
	end

	UserData[self.参战单位[编号].玩家id].召唤兽:创建召唤兽(self.参战单位[self.参战单位[编号].命令数据.目标].造型,lx,nil,
	self.参战单位[self.参战单位[编号].命令数据.目标].等级,self.参战单位[self.参战单位[编号].命令数据.目标].技能,self.参战单位[self.参战单位[编号].命令数据.目标].染色方案,
	self.参战单位[self.参战单位[编号].命令数据.目标].染色组)
	-- UserData[self.参战单位[编号].玩家id].召唤兽.数据.技能={}
	-- for n=1,#self.参战单位[self.参战单位[编号].命令数据.目标].技能 do
	--	UserData[self.参战单位[编号].玩家id].召唤兽.数据.技能[n]=self.参战单位[self.参战单位[编号].命令数据.目标].技能[n]
	--	end
	else
	self.捕捉结果=false
	end
	self.参战单位[编号].当前魔法=self.参战单位[编号].当前魔法-self.消耗魔法
	self.战斗流程[#self.战斗流程+1]={流程=1,执行=false,结果=self.捕捉结果,结束=false,类型="捕捉"}
	self.战斗流程[#self.战斗流程].攻击方=编号
	self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
 end
function FightControl:逃跑流程计算(编号)
	local 逃跑结果 =false
	local 逃跑单位 = 0
	local 逃跑类型 = "召唤兽"
	local 逃跑几率=60
	if DebugMode then
		逃跑几率=100
	end
	if math.random(100) <= 逃跑几率 then
		逃跑结果 = true
		if self.参战单位[编号].战斗类型 == "角色" then

			for n=1,#self.参战玩家 do
				if self.参战单位[编号].助战 == nil and self.参战玩家[n].玩家id==self.参战单位[编号].玩家id then
					self.参战玩家[n].逃跑=true
				end
			end

			逃跑类型 = "角色"
			self.参战单位[编号].单位消失=1
			if UserData[self.参战单位[编号].玩家id].队伍~=0 and self.参战单位[编号].助战 == nil then
				TeamControl:离开队伍(UserData[self.参战单位[编号].玩家id].队伍,self.参战单位[编号].玩家id)
			end
			self.接收数量=self.接收数量-1
			if #self.参战玩家==1 then --直接结束战斗
				self.中断结束=true
			else
				MapControl:更改战斗(self.参战单位[编号].玩家id,false)
				UserData[self.参战单位[编号].玩家id].战斗=0
				UserData[self.参战单位[编号].玩家id].遇怪时间={起始=os.time(),间隔=math.random(20,20)}
			end
			for n=1,#self.参战单位 do
				if self.参战单位[n].玩家id == self.参战单位[编号].玩家id then
					if self.参战单位[编号].助战 ~= nil then
						逃跑单位 = 编号
					else
						self.参战单位[n].单位消失=1
						if self.参战单位[n].战斗类型 ~="召唤类" then
							逃跑单位=n
						end
					end
				end
			end
		else
			self.参战单位[编号].单位消失=1
		end
	end
	self.战斗流程[#self.战斗流程+1]={流程=1,执行=false,结果=逃跑结果,攻击方=编号,结束=false,类型="逃跑",逃跑单位=逃跑单位,id=self.参战单位[编号].玩家id,逃跑类型=逃跑类型}
	self.等待结束=self.等待结束+5
 end
 
function FightControl:道具流程计算(编号)
 self.道具玩家id=self.参战单位[编号].玩家id
 self.参战单位[编号].命令数据.参数=self.参战单位[编号].命令数据.参数+0
	if UserData[self.道具玩家id].角色.道具.包裹[self.参战单位[编号].命令数据.参数]==nil	then
	self:添加提示(self.参战单位[编号].数字id,"您没有这样的道具")
	return 0
	end
 self.道具使用id=UserData[self.道具玩家id].角色.道具.包裹[self.参战单位[编号].命令数据.参数]
 self.道具执行流程=0
 self.额外百分比 = 1+(UserData[self.道具玩家id].角色.剧情技能[2].等级*5/100)
 self.名称=UserData[self.道具玩家id].物品[self.道具使用id].名称
 if UserData[self.道具玩家id].物品[self.道具使用id].类型=="烹饪" then
	if self:取存活状态(self.参战单位[编号].命令数据.目标)==false then
	self:添加提示(self.参战单位[编号].数字id,"无法对对方使用这样的道具")
	return 0
	end
	if self.参战单位[编号].战斗类型~="角色" then
	self:添加提示(self.参战单位[编号].数字id,"无法对召唤兽使用这样的道具")
	return 0
	end
		self.品质=UserData[self.道具玩家id].物品[self.道具使用id].品质

	if self.名称=="醉生梦死"and self.参战单位[编号].战斗类型=="角色"	then
	self.参战单位[self.参战单位[编号].命令数据.目标].愤怒=(self.参战单位[self.参战单位[编号].命令数据.目标].愤怒+self.品质)
		self.道具执行流程={类型="酒",数额=self.品质}
	elseif self.名称=="珍露酒" and self.参战单位[编号].战斗类型=="角色"	then
		self.参战单位[self.参战单位[编号].命令数据.目标].愤怒=self.参战单位[self.参战单位[编号].命令数据.目标].愤怒+self.品质*0.4+10
		self.道具执行流程={类型="酒",数额=self.品质*0.4+10}
	elseif self.名称=="女儿红" and self.参战单位[编号].战斗类型=="角色" then
		self.参战单位[self.参战单位[编号].命令数据.目标].愤怒=self.参战单位[self.参战单位[编号].命令数据.目标].愤怒+20
			self.道具执行流程={类型="酒",数额=20}
	elseif self.名称=="梅花酒" and self.参战单位[编号].战斗类型=="角色" then
		self.参战单位[self.参战单位[编号].命令数据.目标].愤怒=self.参战单位[self.参战单位[编号].命令数据.目标].愤怒+self.品质*0.6
		self.道具执行流程={类型="酒",数额=self.品质*0.6}
	elseif self.名称=="虎骨酒"and self.参战单位[编号].战斗类型=="角色"	then
		self.参战单位[self.参战单位[编号].命令数据.目标].愤怒=self.参战单位[self.参战单位[编号].命令数据.目标].愤怒+20
		self.道具执行流程={类型="酒",数额=20}
	elseif self.名称=="百味酒"and self.参战单位[编号].战斗类型=="角色"	then
		self.参战单位[self.参战单位[编号].命令数据.目标].愤怒=self.参战单位[self.参战单位[编号].命令数据.目标].愤怒+self.品质*0.6
		self.道具执行流程={类型="酒",数额=self.品质*0.6}
	elseif self.名称=="蛇胆酒"and self.参战单位[编号].战斗类型=="角色"	then
			self.参战单位[self.参战单位[编号].命令数据.目标].愤怒=self.参战单位[self.参战单位[编号].命令数据.目标].愤怒+self.品质*1
			self.道具执行流程={类型="酒",数额=self.品质*1}
	end
	if self.参战单位[self.参战单位[编号].命令数据.目标].愤怒 > 150 then
		self.参战单位[self.参战单位[编号].命令数据.目标].愤怒 =150
	end

	self:添加提示(self.参战单位[编号].数字id,"你使用了"..UserData[self.道具玩家id].物品[self.道具使用id].名称)
			UserData[self.道具玩家id].物品[self.道具使用id]=nil
		UserData[self.道具玩家id].角色.道具.包裹[self.参战单位[编号].命令数据.参数]=nil
 elseif	UserData[self.道具玩家id].物品[self.道具使用id].类型=="药品" then
	if ItemData[self.名称].等级~=3 then
		if self:取存活状态(self.参战单位[编号].命令数据.目标)==false then
		self:添加提示(self.参战单位[编号].数字id,"无法对对方使用这样的道具")
		return 0
		else

			if ItemData[self.名称].属性.气血~=nil then
			self:恢复血魔(self.参战单位[编号].命令数据.目标,2,ItemData[self.名称].属性.气血*self.额外百分比)
			self.道具执行流程={类型="加血",数额=ItemData[self.名称].属性.气血*self.额外百分比}
			end
			if ItemData[self.名称].属性.魔法~=nil then
			self:恢复血魔(self.参战单位[编号].命令数据.目标,3,ItemData[self.名称].属性.魔法*self.额外百分比)
			self.道具执行流程={类型="加蓝",数额=ItemData[self.名称].属性.魔法*self.额外百分比}
			end
			if ItemData[self.名称].属性.伤势~=nil then
			self:恢复血魔(self.参战单位[编号].命令数据.目标,4,ItemData[self.名称].属性.伤势*self.额外百分比)
			self.道具执行流程={类型="伤势",数额=ItemData[self.名称].属性.伤势*self.额外百分比}
			end
			if UserData[self.道具玩家id].物品[self.道具使用id].数量 ==1 then
			UserData[self.道具玩家id].物品[self.道具使用id]=nil
			UserData[self.道具玩家id].角色.道具.包裹[self.参战单位[编号].命令数据.参数]=nil
			else
				UserData[self.道具玩家id].物品[self.道具使用id].数量 = UserData[self.道具玩家id].物品[self.道具使用id].数量 -1
			end

		end
	elseif ItemData[self.名称].等级==3	then
		if self.名称~="佛光舍利子" and self.名称~="九转回魂丹" then
			if self:取存活状态(self.参战单位[编号].命令数据.目标)==false then
			self:添加提示(self.参战单位[编号].数字id,"无法对对方使用这样的道具")
			return 0
		elseif self:取复活状态(self.参战单位[编号].命令数据.目标) then
			self:添加提示(self.参战单位[编号].数字id,"该单位目前有死亡召唤状态无法被复活")
		return 0
		else
			self.品质=UserData[self.道具玩家id].物品[self.道具使用id].品质
			if self.名称=="金创药" then
			self:恢复血魔(self.参战单位[编号].命令数据.目标,2,400*self.额外百分比)
			self.道具执行流程={类型="加血",数额=400*self.额外百分比}
			elseif self.名称=="千年保心丹" then
			self:恢复血魔(self.参战单位[编号].命令数据.目标,2,(self.品质*4+200)*self.额外百分比)
			self:恢复血魔(self.参战单位[编号].命令数据.目标,4,(self.品质*4+100)*self.额外百分比)
			self.道具执行流程={类型="加血",数额=(self.品质*4+200)*self.额外百分比}
			elseif self.名称=="金香玉" then
			self:恢复血魔(self.参战单位[编号].命令数据.目标,2,(self.品质*12+150)*self.额外百分比)
			self.道具执行流程={类型="加血",数额=(self.品质*12+150)*self.额外百分比}
			elseif self.名称=="小还丹" then
			self:恢复血魔(self.参战单位[编号].命令数据.目标,2,(self.品质*8+100)*self.额外百分比)
			self:恢复血魔(self.参战单位[编号].命令数据.目标,4,(self.品质+80)*self.额外百分比)
			self.道具执行流程={类型="加血",数额=(self.品质*8+100)*self.额外百分比}
			elseif self.名称=="风水混元丹" then
			self:恢复血魔(self.参战单位[编号].命令数据.目标,3,(self.品质*3+50)*self.额外百分比)
			self.道具执行流程={类型="加蓝",数额=(self.品质*3+50)*self.额外百分比}
			elseif self.名称=="红雪散" then
			self:恢复血魔(self.参战单位[编号].命令数据.目标,2,self.品质*4*self.额外百分比)
			self.道具执行流程={类型="加血",数额=self.品质*4}
			elseif self.名称=="五龙丹" then
				self:恢复血魔(self.参战单位[编号].命令数据.目标,2,self.品质*3*self.额外百分比)
			self.道具执行流程={类型="加血",数额=self.品质*3*self.额外百分比}
				self.参战单位[self.参战单位[编号].命令数据.目标].攻击封印=nil
				self.参战单位[self.参战单位[编号].命令数据.目标].技能封印=nil
				self.参战单位[self.参战单位[编号].命令数据.目标].特技封印=nil
				self.参战单位[self.参战单位[编号].命令数据.目标].封印状态=nil
			elseif self.名称=="蛇蝎美人" then
			self:恢复血魔(self.参战单位[编号].命令数据.目标,3,(self.品质*5+100)*self.额外百分比)
			self.道具执行流程={类型="加蓝",数额=(self.品质*5+100)*self.额外百分比}
			elseif self.名称=="十香返生丸" then
			self:恢复血魔(self.参战单位[编号].命令数据.目标,3,(self.品质*3+50)*self.额外百分比)
			self.道具执行流程={类型="加蓝",数额=(self.品质*3+50)*self.额外百分比}
			elseif self.名称=="定神香" then
			self:恢复血魔(self.参战单位[编号].命令数据.目标,3,(self.品质*5+50)*self.额外百分比)
			self.道具执行流程={类型="加蓝",数额=(self.品质*5+50)*self.额外百分比}
			end
			UserData[self.道具玩家id].物品[self.道具使用id]=nil
			UserData[self.道具玩家id].角色.道具.包裹[self.参战单位[编号].命令数据.参数]=nil
		end
		elseif self.名称=="佛光舍利子" or self.名称=="九转回魂丹" then
		if self:取存活状态(self.参战单位[编号].命令数据.目标) then
			self:添加提示(self.参战单位[编号].数字id,"无法对对方使用这样的道具")
			return 0
		else
			if self.名称=="佛光舍利子" then
				self:恢复血魔(self.参战单位[编号].命令数据.目标,2,UserData[self.道具玩家id].物品[self.道具使用id].品质*3)
				self.道具执行流程={类型="复活",数额=UserData[self.道具玩家id].物品[self.道具使用id].品质*3,复活=true}
			else
			self:恢复血魔(self.参战单位[编号].命令数据.目标,2,UserData[self.道具玩家id].物品[self.道具使用id].品质*5)
			self.道具执行流程={类型="复活",数额=UserData[self.道具玩家id].物品[self.道具使用id].品质*5,复活=true}
			end
			UserData[self.道具玩家id].物品[self.道具使用id]=nil
			UserData[self.道具玩家id].角色.道具.包裹[self.参战单位[编号].命令数据.参数]=nil
		end
		end
	end
 elseif	self.名称=="逍遥镜" then
	if	UserData[self.道具玩家id].角色.子女列表.参战 ==0 then
		self:添加提示(self.参战单位[编号].数字id,"你还没有设置参战的子女")
		else
		self:召唤流程计算(编号,true)
	end

	return
 end
 if self.道具执行流程~=0 then
	self.战斗流程[#self.战斗流程+1]={流程=105,执行=false,结束=false,类型="技能",参数=self.道具执行流程.类型,名称=self.名称}--流程=30为从非物理技能开始
	self.战斗流程[#self.战斗流程].攻击方=编号
	self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
	self.战斗流程[#self.战斗流程].数额=self.道具执行流程.数额+self.参战单位[self.参战单位[编号].命令数据.目标].气血回复效果
	self.战斗流程[#self.战斗流程].复活=self.道具执行流程.复活
	self.等待结束=self.等待结束+5
 end
 end
function FightControl:法宝流程计算(编号)
 self.道具玩家id=self.参战单位[编号].玩家id
 self.参战单位[编号].命令数据.参数=self.参战单位[编号].命令数据.参数+0
	if UserData[self.道具玩家id].角色.道具.法宝[self.参战单位[编号].命令数据.参数]==nil	then
	self:添加提示(self.参战单位[编号].数字id,"您没有这样的法宝")
	return 0
	end
	self.道具使用id=UserData[self.道具玩家id].角色.道具.法宝[self.参战单位[编号].命令数据.参数]
 local 名称=UserData[self.道具玩家id].物品[self.道具使用id].名称
	if UserData[self.道具玩家id].角色.等级 < ItemData[名称].属性.佩戴等级 then
		self:添加提示(self.参战单位[编号].数字id,"当前等级无法使用这样的法宝！")
	return 0
 elseif	ItemData[名称].属性.门派 and	UserData[self.道具玩家id].角色.门派 ~= ItemData[名称].属性.门派	then
		self:添加提示(self.参战单位[编号].数字id,"当前门派无法使用这样的法宝！")
	return 0
	end

 self.道具执行流程=0
 local 等级= UserData[self.道具玩家id].物品[self.道具使用id].层数
 local 几率 = false
	if	UserData[self.道具玩家id].物品[self.道具使用id].灵气 <1 then 
	self:添加提示(self.参战单位[编号].数字id,UserData[self.道具玩家id].物品[self.道具使用id].名称.."的灵气不足1点")
	return 
	end 
	if 名称 =="干将莫邪" or 名称 =="乾坤玄火塔" or 名称 =="混元伞" or 名称 =="赤焰" or 名称 =="无尘扇" or 名称 =="舞雪冰蝶"or 名称 =="摄魂" or 名称 =="奇门五行令" or 名称 =="五彩娃娃" or 名称 =="苍白纸人"
 or	名称 =="神木宝鼎"	then
		self.战斗流程[#self.战斗流程+1]={流程=4,执行=false,结束=false,类型="技能",参数=名称}--流程=30为从非物理技能开始
		self.战斗流程[#self.战斗流程].攻击方=编号
		self.战斗流程[#self.战斗流程].挨打方={[1]={编号=self.参战单位[编号].命令数据.目标}}
		self.等待结束=self.等待结束+5
		self.参战单位[编号].法术状态组[名称]={}
		if 名称 =="干将莫邪" then
		self.参战单位[编号].法术状态组[名称].回合=3
		self.参战单位[编号].法术状态组[名称].伤害=(等级+1)*40
		self.参战单位[编号].伤害=self.参战单位[编号].伤害+self.参战单位[编号].法术状态组[名称].伤害
		elseif 名称 =="乾坤玄火塔" then
		self.参战单位[编号].法术状态组[名称].回合=4
		self.参战单位[编号].法术状态组[名称].伤害=(等级+1)
		elseif 名称 =="混元伞" then
		self.参战单位[编号].法术状态组[名称].回合=3
		elseif 名称 =="赤焰" then
			self.参战单位[编号].法术状态组[名称].回合=3
		self.参战单位[编号].法术状态组[名称].魔法=等级*20
		self.参战单位[编号].当前魔法=self.参战单位[编号].当前魔法+self.参战单位[编号].法术状态组[名称].魔法
		if self.参战单位[编号].当前魔法>self.参战单位[编号].魔法上限 then
		self.参战单位[编号].当前魔法=self.参战单位[编号].魔法上限
		end
		elseif 名称 =="无尘扇" then
			self.参战单位[编号].法术状态组[名称].回合=3
		self.参战单位[编号].法术状态组[名称].愤怒=等级*2
		self.参战单位[编号].愤怒=self.参战单位[编号].愤怒+self.参战单位[编号].法术状态组[名称].愤怒
		if self.参战单位[编号].愤怒<0 then self.参战单位[编号].愤怒=0 end
		elseif 名称 =="舞雪冰蝶" then
		self.参战单位[编号].法术状态组[名称].回合=3
		self.参战单位[编号].法术状态组[名称].命中=(等级+1)*40
		self.参战单位[编号].命中=self.参战单位[编号].命中-self.参战单位[编号].法术状态组[名称].命中
		elseif 名称 =="摄魂" then
		self.参战单位[编号].法术状态组[名称].回合=3
		self.参战单位[编号].法术状态组[名称].防御=(等级+1)*40
		self.参战单位[编号].法术状态组[名称].法防=(等级+1)*40
		self.参战单位[编号].防御=self.参战单位[编号].防御-self.参战单位[编号].法术状态组[名称].防御
		self.参战单位[编号].法防=self.参战单位[编号].法防-self.参战单位[编号].法术状态组[名称].法防
		elseif 名称 =="奇门五行令" then
		self.参战单位[编号].法术状态组[名称].回合=3
		elseif 名称 =="苍白纸人" then
		self.参战单位[编号].法术状态组[名称].回合=3
		self.参战单位[编号].法术状态组[名称].伤害=(等级+1)*60
		elseif 名称 =="五彩娃娃" then
		self.参战单位[编号].法术状态组[名称].回合=3
		self.参战单位[编号].法术状态组[名称].伤害=(等级+1)*30
		elseif 名称 =="神木宝鼎" then
		self.参战单位[编号].法术状态组[名称].回合=3
		self.参战单位[编号].法术状态组[名称].伤害=(等级+1)/10
		end
	elseif 名称=="无字经" or	名称=="发瘟匣" or	名称=="无魂傀儡" or	名称=="失心钹" then
		if 名称=="失心钹" then
			if self.参战单位[self.参战单位[编号].命令数据.目标].战斗类型 ~= "召唤兽"	then
					self:添加提示(self.参战单位[编号].数字id,"只能对玩家召唤兽使用！")
				return
			end
		end
		self.战斗流程[#self.战斗流程+1]={流程=4,执行=false,结束=false,类型="技能",参数=名称}
		self.战斗流程[#self.战斗流程].攻击方=编号
		self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
		if math.random(100) <=等级*3	then
		self.战斗流程[#self.战斗流程].封印结果=true
		self.参战单位[编号].法术状态组[名称]={回合=3}
			if 名称=="无字经"	then
				self.参战单位[编号].法宝封印=true
				self.参战单位[编号].封印开关=true
			elseif 名称=="发瘟匣"	then
					self.参战单位[编号].技能封印=true
				self.参战单位[编号].封印开关=true
			end
		end
	elseif 名称 =="金钱镖" or 名称 =="落雨金钱"	then
		local ls = 0

		if	名称 =="金钱镖"	then
			ls = 2000
		else
		ls = 4000
		end
		if UserData[self.道具玩家id].角色.道具.货币.银子 < ls	then
			self:添加提示(self.参战单位[编号].数字id,"当前银子不足无法使用法宝！")
			return
		else
			UserData[self.道具玩家id].角色.道具.货币.银子=UserData[self.道具玩家id].角色.道具.货币.银子 -ls
			self:添加提示(self.参战单位[编号].数字id,"扣除金钱"..ls)
		end
		

		self:单体法术计算(编号,nil,名称)
	elseif 名称 =="缩地尺" then
		self:强制结束战斗()
		UserData[self.道具玩家id].物品[self.道具使用id].灵气=UserData[self.道具玩家id].物品[self.道具使用id].灵气-9
	elseif 名称 =="惊魂铃" or 名称 =="鬼泣"	then
		if	self.参战单位[self.参战单位[编号].命令数据.目标].战斗类型 == "召唤兽" then
		if self.参战单位[self.参战单位[编号].命令数据.目标].参战等级 < 136 and 名称 == "惊魂铃" then
			if math.random(100)<= 等级*3 then
				self.参战单位[self.参战单位[编号].命令数据.目标].单位消失 = 1
				几率=true
			end
			self.战斗流程[#self.战斗流程+1]={流程=4,执行=false,结束=false,类型="技能",参数=名称}--流程=30为从非物理技能开始
			self.战斗流程[#self.战斗流程].攻击方=编号
			self.战斗流程[#self.战斗流程].挨打方={[1]={编号=self.参战单位[编号].命令数据.目标,逃跑=true}}

		else
			self:添加提示(self.参战单位[编号].数字id,"只能对参战等级在136以下的召唤兽使用！")
			return
		end
		else
		self:添加提示(self.参战单位[编号].数字id,"只能对玩家召唤兽使用！")
		return
		end
 end

	UserData[self.道具玩家id].物品[self.道具使用id].灵气=UserData[self.道具玩家id].物品[self.道具使用id].灵气-1
	self:添加提示(self.参战单位[编号].数字id,UserData[self.道具玩家id].物品[self.道具使用id].名称.."当前灵气为"..UserData[self.道具玩家id].物品[self.道具使用id].灵气)
 end
------------------------------
function FightControl:单体状态法术计算(编号,特技)
 if self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false or self:取隐身判断(编号,self.参战单位[编号].命令数据.目标)==false then
	self.相关id=self.参战单位[编号].队伍id
	self.临时目标=self:取随机单位(编号,self.相关id,1,1)
	if self.临时目标==0 then
	self.参战单位[编号].命令数据.目标=0
	else
	self.参战单位[编号].命令数据.目标=self.临时目标[1]
	end
	end
	self.流程类型=特技 and "特技" or "技能"
 if self.参战单位[编号].命令数据.目标==0 then --找不到可攻击的目标
	return 0
	elseif 特技==nil and self:技能消耗(self.参战单位[编号]) ==false then
		if self.临时参数=="移魂化骨" then
		self:添加提示(self.参战单位[编号].数字id,"你的当前气血不足以施放此技能")
		else
		self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
		end
	return 0
	elseif 特技~=nil then
	if self:特技消耗(self.参战单位[编号],self.临时参数) ==false then
	self:添加提示(self.参战单位[编号].数字id,"你没那么多的愤怒")
	return 0
		end
	end
	self.临时参数=self.参战单位[编号].命令数据.参数
	if self.临时参数=="后发制人" or self.临时参数=="诱袭" or	self.临时参数=="锋芒毕露" then
	self.临时目标=self.参战单位[编号].命令数据.目标
	self.参战单位[编号].命令数据.目标=编号

	end
 self.战斗流程[#self.战斗流程+1]={流程=4,执行=false,结束=false,类型=self.流程类型,参数=self.临时参数}
 self.战斗流程[#self.战斗流程].攻击方=编号
 self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
 self.战斗流程[#self.战斗流程].封印结果=true
 self.等待结束=self.等待结束+5
	if self.临时参数=="移魂化骨" then
	self.战斗流程[#self.战斗流程].自身伤害=math.floor(self.参战单位[编号].当前气血*0.1)
	self.参战单位[编号].当前气血=self.参战单位[编号].当前气血-self.战斗流程[#self.战斗流程].自身伤害
	end
	if self.临时参数=="笑里藏刀" then
		if self.参战单位[self.参战单位[编号].命令数据.目标].战斗类型=="角色" then
			self.参战单位[self.参战单位[编号].命令数据.目标].愤怒=self.参战单位[self.参战单位[编号].命令数据.目标].愤怒 -70
			if self.参战单位[self.参战单位[编号].命令数据.目标].愤怒 < 0 then
				self.参战单位[self.参战单位[编号].命令数据.目标].愤怒 = 0
			end
		end
	else
		self:添加封印状态(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],self.临时参数,self.临时目标)
	end
		if self.临时参数=="八戒上身" then
			self.战斗流程[#self.战斗流程+1]={流程=1,攻击方=self.参战单位[编号].命令数据.目标,执行=false,类型="八戒上身"}
			self.等待结束=self.等待结束+5
		end
 end
function FightControl:群体状态法术计算(编号)
	self.相关id=self.参战单位[编号].队伍id
	self.临时参数=self.参战单位[编号].命令数据.参数
	self.技能数量=self:取技能目标数(self.参战单位[编号],self.临时参数)
	if	self.临时参数=="摧心术" or self.临时参数=="谜毒之缚"	then
		self.临时目标=self:取随机单位(编号,self.相关id,1,self.技能数量)
	else
		self.临时目标=self:取随机单位(编号,self.相关id,2,self.技能数量)
	end
	self.目标存在=false
	if self.临时目标==0 then return 0 end
	for n=1,#self.临时目标 do
	if self.临时目标[n]== self.参战单位[编号].命令数据.目标 then
	self.目标存在=true
		end
	end
	if self.目标存在==false then
	if self:取存活状态(self.参战单位[编号].命令数据.目标) then
	self.临时目标[math.random(1,#self.临时目标)]=self.参战单位[编号].命令数据.目标
	end
	end
 if #self.临时目标==0 then --找不到可攻击的目标
	return 0
	elseif self:技能消耗(self.参战单位[编号],#self.临时目标) ==false then
	self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
	return 0
	end
	if (self.临时参数=="匠心·削铁" or self.临时参数=="匠心·固甲") and self.参战单位[编号].攻之械 <3 then
	self.参战单位[编号].攻之械=self.参战单位[编号].攻之械+1
	self.参战单位[编号].伤害=self.参战单位[编号].伤害*1.1
	if self.参战单位[编号].数字id and	UserData[self.参战单位[编号].数字id] then
		SendMessage(UserData[self.参战单位[编号].数字id].连接id, 9,"#xt/#y/当前你的攻之械为："..self.参战单位[编号].攻之械)
		if self.参战单位[编号].攻之械==3 then
			SendMessage(UserData[self.参战单位[编号].数字id].连接id, 9,"#xt/#y/你的攻之械已经达到3点下回合将变身为偃甲状态")
		end
	end
 end

	

 self.战斗流程[#self.战斗流程+1]={流程=4,执行=false,结束=false,类型="技能",参数=self.临时参数}--流程=30为从非物理技能开始
 self.战斗流程[#self.战斗流程].攻击方=编号
 self.战斗流程[#self.战斗流程].挨打方={}
 self.等待结束=self.等待结束+5
 for n=1,#self.临时目标 do
	self.战斗流程[#self.战斗流程].挨打方[n]={}
	self.战斗流程[#self.战斗流程].挨打方[n].编号=self.临时目标[n]
	self:添加封印状态(self.参战单位[self.临时目标[n]],self.参战单位[编号],self.临时参数,self.临时目标[n])
	end

 end

function FightControl:单体物理法术计算(编号,特技)
	self.开关=1
 if self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false or self:取隐身判断(编号,self.参战单位[编号].命令数据.目标)==false then
	--二次刷新目标
	self.相关id=self.参战单位[编号].队伍id
	self.临时目标=self:取随机单位(编号,self.相关id,1,1)
		if self.临时目标==0 then
		self.参战单位[编号].命令数据.目标=0
		else
		self.参战单位[编号].命令数据.目标=self.临时目标[1]
		end
 end
	if self.参战单位[编号].命令数据.目标==0 then --找不到可攻击的目标
	return 0
	end
 self.临时参数=self.参战单位[编号].命令数据.参数
 if	self.临时参数=="连环击" or	self.临时参数=="象形"	then
		if not self.参战单位[编号].法术状态组.变身 then
		self:添加提示(self.参战单位[编号].数字id,"你需要在变身状态下才可使用此技能")
		return 0
		end
	elseif self.临时参数=="知己知彼" then
	SendMessage(UserData[self.参战单位[编号].数字id].连接id, 9,"#xt/#y/"..self.参战单位[self.参战单位[编号].命令数据.目标].名称.."当前的气血为："..self.参战单位[self.参战单位[编号].命令数据.目标].当前气血.."/"..self.参战单位[self.参战单位[编号].命令数据.目标].气血上限)
	SendMessage(UserData[self.参战单位[编号].数字id].连接id, 9,"#xt/#y/"..self.参战单位[self.参战单位[编号].命令数据.目标].名称.."当前的魔法为："..self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法.."/"..self.参战单位[self.参战单位[编号].命令数据.目标].魔法上限)
	SendMessage(UserData[self.参战单位[编号].数字id].连接id, 9,"#xt/#y/"..self.参战单位[self.参战单位[编号].命令数据.目标].名称.."当前的伤害为："..self.参战单位[self.参战单位[编号].命令数据.目标].伤害)
	SendMessage(UserData[self.参战单位[编号].数字id].连接id, 9,"#xt/#y/"..self.参战单位[self.参战单位[编号].命令数据.目标].名称.."当前的防御为："..self.参战单位[self.参战单位[编号].命令数据.目标].防御)
	SendMessage(UserData[self.参战单位[编号].数字id].连接id, 9,"#xt/#y/"..self.参战单位[self.参战单位[编号].命令数据.目标].名称.."当前的法术防御为："..self.参战单位[self.参战单位[编号].命令数据.目标].法防)
	SendMessage(UserData[self.参战单位[编号].数字id].连接id, 9,"#xt/#y/"..self.参战单位[self.参战单位[编号].命令数据.目标].名称.."当前的灵力为："..self.参战单位[self.参战单位[编号].命令数据.目标].灵力)
	SendMessage(UserData[self.参战单位[编号].数字id].连接id, 9,"#xt/#y/"..self.参战单位[self.参战单位[编号].命令数据.目标].名称.."当前的速度为："..self.参战单位[self.参战单位[编号].命令数据.目标].速度)
	return 0
 elseif self.临时参数=="天崩地裂" then
		if self.参战单位[编号].战意点数 < 3 then
		self:添加提示(self.参战单位[编号].数字id,"你的当前战意不足以施放此技能，你当前战意为："..self.参战单位[编号].战意点数)
		return 0
		else
		self.参战单位[编号].战意点数 =self.参战单位[编号].战意点数 -3
		end
 elseif self.临时参数=="裂石" then
		if self.参战单位[编号].战意点数 < 7 then
		self.参战单位[编号].战意点数 =self.参战单位[编号].战意点数 +1
		self:添加提示(self.参战单位[编号].数字id,"你当前战意为："..self.参战单位[编号].战意点数)
		end
 elseif self.临时参数=="断岳势" or	self.临时参数=="腾雷"	then
		if self.参战单位[编号].战意点数 < 1 then
		self:添加提示(self.参战单位[编号].数字id,"你的当前战意不足以施放此技能，你当前战意为："..self.参战单位[编号].战意点数)
		return 0
		else
		self.参战单位[编号].战意点数 =self.参战单位[编号].战意点数 -1
		end
 end
self.流程类型=特技 and "特技" or "技能"
 if self.临时参数~="连击" and	self.临时参数~="理直气壮"	then
	if 特技==nil and self:技能消耗(self.参战单位[编号],1) ==false then
		if self.临时参数=="横扫千军" then
		self:添加提示(self.参战单位[编号].数字id,"你的当前气血不足以施放此技能")
		else
		self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
		end
		return 0
	elseif 特技~=nil then
		if self:特技消耗(self.参战单位[编号],self.临时参数) ==false then
		self:添加提示(self.参战单位[编号].数字id,"你没那么多的愤怒")
		return 0
		end
	end
 end
 self.战斗流程[#self.战斗流程+1]={流程=1,执行=false,结束=false,类型="攻击",参数=self.临时参数,攻击方=编号}--流程=1为从物理技能开始
 self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
 if self.临时参数=="横扫千军" then
	if self.参战单位[编号].奇经八脉.无敌 then
		self.攻击次数=4
		self.战斗流程[#self.战斗流程].消耗=math.floor(self.参战单位[编号].当前气血*0.2)
	else
		self.攻击次数=3
		self.战斗流程[#self.战斗流程].消耗=math.floor(self.参战单位[编号].当前气血*0.1)
	end
	if self.参战单位[编号].助战 == nil and UserData[self.参战单位[编号].玩家id] ~= nil and UserData[self.参战单位[编号].玩家id].角色.元神 ~= nil and UserData[self.参战单位[编号].玩家id].角色.元神 > 0 then
		local 元神加成下限 = SpiritData[UserData[self.参战单位[编号].玩家id].角色.门派][UserData[self.参战单位[编号].玩家id].角色.元神.."介效果2下限"]
		local 元神加成上限 = SpiritData[UserData[self.参战单位[编号].玩家id].角色.门派][UserData[self.参战单位[编号].玩家id].角色.元神.."介效果2上限"]
		if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
			self.攻击次数 = self.攻击次数 + 取随机数(元神加成下限,元神加成上限)
		end
	end
	if self.参战单位[编号].神器 =="惊锋" then
		self.战斗流程[#self.战斗流程].消耗=self.战斗流程[#self.战斗流程].消耗*2
	end
	self.参战单位[编号].当前气血=self.参战单位[编号].当前气血-self.战斗流程[#self.战斗流程].消耗
	elseif self.临时参数=="后发制人" then
	if self.参战单位[编号].奇经八脉.勇武 then
			self.战斗流程[#self.战斗流程].增加=math.floor(self.参战单位[编号].气血上限*0.15)
			self.参战单位[编号].当前气血=self.参战单位[编号].当前气血+self.战斗流程[#self.战斗流程].增加
			if self.参战单位[编号].当前气血>	self.参战单位[编号].气血上限 then
				self.参战单位[编号].当前气血= self.参战单位[编号].气血上限
			end
		self.参战单位[编号].愤怒=self.参战单位[编号].愤怒+10
		if self.参战单位[编号].愤怒 > 150 then
			self.参战单位[编号].愤怒 = 150
		end
	else
		self.战斗流程[#self.战斗流程].消耗=math.floor(self.参战单位[编号].当前气血*0.05)
		self.参战单位[编号].当前气血=self.参战单位[编号].当前气血-self.战斗流程[#self.战斗流程].消耗
	end

	self.攻击次数=1
 elseif self.临时参数=="天崩地裂"or self.临时参数=="理直气壮"	then
	self.攻击次数=3
 elseif self.临时参数=="烟雨剑法" or self.临时参数=="破血狂攻" or self.临时参数=="断岳势" or self.临时参数=="连击" then
	self.攻击次数=2
 elseif self.临时参数=="连环击" then
	self.攻击次数=self:取技能目标数(self.参战单位[编号],self.临时参数)
 elseif self.临时参数=="血雨"	then
	self.攻击次数=1
	self.战斗流程[#self.战斗流程].消耗=math.floor(self.参战单位[编号].当前气血*0.1)
	self.参战单位[编号].当前气血=self.参战单位[编号].当前气血-self.战斗流程[#self.战斗流程].消耗
 elseif self.临时参数=="天命剑法" then
	self.攻击次数=math.random(2,5)
		self.战斗流程[#self.战斗流程].消耗=math.floor(self.参战单位[编号].当前气血*0.02)
	self.参战单位[编号].当前气血=self.参战单位[编号].当前气血-self.战斗流程[#self.战斗流程].消耗
	elseif self.临时参数=="哼哼哈兮" then
		self.攻击次数=math.random(1,3)
	else
		self.攻击次数=1
	end
	self.战斗流程[#self.战斗流程].动作={}
	for n=1,self.攻击次数 do
		if self:取存活状态(编号)==false or self:取存活状态(self.参战单位[编号].命令数据.目标)==false then
			if self.临时参数=="横扫千军" then
				if self.参战单位[编号].奇经八脉.连破 and math.random(100) < 18 then
					self.战斗流程[#self.战斗流程].免休 = true
				else
					if self.参战单位[编号].助战 == nil and UserData[self.参战单位[编号].玩家id] ~= nil and UserData[self.参战单位[编号].玩家id].角色.元神 ~= nil and UserData[self.参战单位[编号].玩家id].角色.元神 > 0 then
						local 元神加成下限 = SpiritData[UserData[self.参战单位[编号].玩家id].角色.门派][UserData[self.参战单位[编号].玩家id].角色.元神.."介效果3下限"]
						local 元神加成上限 = SpiritData[UserData[self.参战单位[编号].玩家id].角色.门派][UserData[self.参战单位[编号].玩家id].角色.元神.."介效果3上限"]
						if (元神加成下限 ~= 0 or 元神加成上限 ~= 0) and 取随机数() <= 取随机数(元神加成下限,元神加成上限) then
							self.战斗流程[#self.战斗流程].免休 = true
						else
							self:添加封印状态(self.参战单位[编号],self.参战单位[编号],self.临时参数)
						end
					else
						self:添加封印状态(self.参战单位[编号],self.参战单位[编号],self.临时参数)
					end
				end
			elseif	self.临时参数=="天命剑法" then
				self:添加封印状态(self.参战单位[编号],self.参战单位[编号],"横扫千军")
			elseif self.临时参数=="连环击" then
				self:添加封印状态(self.参战单位[编号],self.参战单位[编号],"鹰击")
				self:解除封印状态(self.参战单位[编号],"变身")
			elseif self.临时参数=="血雨" then
				self:添加封印状态(self.参战单位[编号],self.参战单位[编号],self.临时参数)
			elseif self.临时参数=="象形" then
				self:添加封印状态(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],self.临时参数,目标)
				self:解除封印状态(self.参战单位[编号],"变身")
				self.参战单位[编号].法术状态组.象形限制={回合=2}
			end
			return 0
		end

	self.等待结束=self.等待结束+5
	self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作+1]={必杀动作="",挨打动作="被击中",防御动作="",反震动作="",反震=false,死亡=0,反震死亡=0}
	self.临时伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数,n)
	--计算是否必杀
	self.必杀=self:取是否必杀(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
	self.吸血=self:取是否吸血(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
	if self.参战单位[编号].奇经八脉.化血 and not self.吸血 and math.random(100) <= 30 then
		self.战斗流程[#self.战斗流程].吸血伤害=self:取物理伤害结果(math.floor(self.临时伤害*0.16),self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],1,"加血")
		self.战斗流程[#self.战斗流程].吸血=true
	end
	-----------------吸血伤害计算
	if self.吸血	and	(self.临时参数=="连击" or	self.临时参数=="理直气壮" or	self.临时参数=="力劈华山" or	self.临时参数=="善恶有报" or	self.临时参数=="壁垒击破" or	self.临时参数=="夜舞倾城" or	self.临时参数=="哼哼哈兮" ) then
	self.战斗流程[#self.战斗流程].吸血伤害=self:取物理伤害结果(math.floor(self.临时伤害*self.参战单位[编号].吸血),self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],1,"加血")
	self.战斗流程[#self.战斗流程].吸血=true
	end
	if self.必杀 then
		if (self.临时参数=="连击" or	self.临时参数=="理直气壮") and self.开关==1 then
			self.苍鸾怒击=true
			self.开关=2
		end
		if self.参战单位[编号].神器 =="威服天下" then
			self.临时伤害=math.floor(self.临时伤害*2.26)+self.参战单位[编号].暴击伤害
		else 
			self.临时伤害=math.floor(self.临时伤害*2)+self.参战单位[编号].暴击伤害
		end
		
		self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].必杀动作="必杀"
	end
	if self.参战单位[self.参战单位[编号].命令数据.目标].命令数据.类型=="防御" and not self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.催眠符 then
		if not self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.天地同寿 then
			self.临时伤害=math.floor(self.临时伤害*0.5)
		end
		self.战斗流程[#self.战斗流程].挨打动作="防御"
		self.战斗流程[#self.战斗流程].防御动作=1
		if self.临时伤害<1 then
			self.临时伤害=1
		end
	end
	self.伤害类型="掉血"
	if self.临时参数=="善恶有报" or	self.临时参数=="进阶善恶有报"	then
		if math.random(100)<=30 then
		self.伤害类型="加血"
		self.临时伤害=math.floor(self.临时伤害*0.35)
		end
	end
	self.保护=self:取保护单位(self.参战单位[编号].命令数据.目标)
	if self.保护==false then
		self.临时伤害=self:取物理伤害结果(self.临时伤害,self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],1,self.伤害类型)
	else
		self.临时伤害2=self.临时伤害
		self.临时伤害=self:取物理伤害结果(self.临时伤害,self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],0.3,self.伤害类型)
		self.临时伤害1=self:取物理伤害结果(self.临时伤害2,self.参战单位[编号],self.参战单位[self.保护],0.7,self.伤害类型)
		self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].保护数据={编号=self.保护,伤害=self.临时伤害1.伤害,类型=self.临时伤害1.类型,死亡=self.临时伤害1.死亡}
		self.等待结束=self.等待结束+5
	end
	self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].伤害类型=self.临时伤害.类型
	self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].普通伤害=self.临时伤害.伤害
	self.战斗流程[#self.战斗流程].动作[#self.战斗流程[#self.战斗流程].动作].死亡=self.临时伤害.死亡
	if self.临时参数=="破碎无双" then
		self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法-math.floor(self.临时伤害.伤害*0.5)
		if self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法<0 then
		self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=0
		end
	elseif self.临时参数=="惊心一剑"	then
		self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法-math.floor(self.参战单位[self.参战单位[编号].命令数据.目标].等级)
		if self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法<0 then
		self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=0
		end
	elseif self.临时参数=="泼天乱棒"	then
	self.相关id=self.参战单位[编号].队伍id
	self.临时目标=self:取随机单位(编号,self.相关id,1,3)
		if self.临时目标==0 then
			return 0
		end
		for n=#self.临时目标,1,-1 do
			if self.临时目标[n]== self.参战单位[编号].命令数据.目标 then
			table.remove(self.临时目标,n)
			break
			end
		end
		self.战斗流程[#self.战斗流程+1]={流程=5,执行=false,结束=false,类型="攻击",参数="溅射"}--流程=30为从非物理技能开始
		self.战斗流程[#self.战斗流程].攻击方=编号
		self.战斗流程[#self.战斗流程].挨打方={}
		self.等待结束=self.等待结束+5
		for n=1,#self.临时目标 do
			self.等待结束=self.等待结束+2
			self.战斗流程[#self.战斗流程].挨打方[n]={反震=false}
			self.战斗流程[#self.战斗流程].挨打方[n].编号=self.临时目标[n]
			self.战斗流程[#self.战斗流程].挨打方[n].伤害= self:取物理伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
			self.临时伤害= self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.临时目标[n]],0.5,"掉血")
			self.战斗流程[#self.战斗流程].挨打方[n].伤害=self.临时伤害.伤害
			self.战斗流程[#self.战斗流程].挨打方[n].类型=self.临时伤害.类型
			self.战斗流程[#self.战斗流程].挨打方[n].死亡=self.临时伤害.死亡
		end


	end
	end
	if self.临时参数=="横扫千军" then
		if self.参战单位[编号].奇经八脉.连破 and math.random(100) < 18 then
			self.战斗流程[#self.战斗流程].免休 = true
		else
			if self.参战单位[编号].助战 == nil and UserData[self.参战单位[编号].玩家id] ~= nil and UserData[self.参战单位[编号].玩家id].角色.元神 ~= nil and UserData[self.参战单位[编号].玩家id].角色.元神 > 0 then
				local 元神加成下限 = SpiritData[UserData[self.参战单位[编号].玩家id].角色.门派][UserData[self.参战单位[编号].玩家id].角色.元神.."介效果3下限"]
				local 元神加成上限 = SpiritData[UserData[self.参战单位[编号].玩家id].角色.门派][UserData[self.参战单位[编号].玩家id].角色.元神.."介效果3上限"]
				if (元神加成下限 ~= 0 or 元神加成上限 ~= 0) and 取随机数() <= 取随机数(元神加成下限,元神加成上限) then
					self.战斗流程[#self.战斗流程].免休 = true
				else
					self:添加封印状态(self.参战单位[编号],self.参战单位[编号],self.临时参数)
				end
			else
				self:添加封印状态(self.参战单位[编号],self.参战单位[编号],self.临时参数)
			end
		end
	elseif self.临时参数=="死亡召唤"	then
				self.战斗流程[#self.战斗流程].封印结果 =self:取封印结果(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数)
				if self.战斗流程[#self.战斗流程].封印结果 then
				self:添加封印状态(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],self.临时参数,目标)
				end
	elseif	self.临时参数=="天命剑法" then
	self:添加封印状态(self.参战单位[编号],self.参战单位[编号],"横扫千军")
	elseif self.临时参数=="连环击" then
	self:解除封印状态(self.参战单位[编号],"变身")
	self:添加封印状态(self.参战单位[编号],self.参战单位[编号],"鹰击")
	elseif self.临时参数=="诱袭" then
		self:添加封印状态(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],self.临时参数,目标)
	elseif self.临时参数=="血雨" then
	self:添加封印状态(self.参战单位[编号],self.参战单位[编号],self.临时参数)
	elseif self.临时参数=="象形" then
	self:添加封印状态(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],self.临时参数,目标)
	self:解除封印状态(self.参战单位[编号],"变身")
	self.参战单位[编号].法术状态组.象形限制={回合=2}
	end
 end

 function FightControl:群体物理法术计算(编号)
	self.保护=false
	self.相关id=self.参战单位[编号].队伍id
	self.临时参数=self.参战单位[编号].命令数据.参数
	self.技能数量=self:取技能目标数(self.参战单位[编号],self.临时参数)
	self.临时目标=self:取随机单位(编号,self.相关id,1,self.技能数量)
	self.目标存在=false
	if self.临时参数=="鹰击" or self.临时参数=="狮搏" then
		if not self.参战单位[编号].法术状态组.变身 then
			self:添加提示(self.参战单位[编号].数字id,"你需要在变身状态下才可使用此技能")
			return 0
		end
	elseif	self.临时参数=="翻江搅海" then
		if self.参战单位[编号].战意点数 < 3 then
			self:添加提示(self.参战单位[编号].数字id,"你的当前战意不足以施放此技能，你当前战意为："..self.参战单位[编号].战意点数)
			return 0
		else
			self.参战单位[编号].战意点数 =self.参战单位[编号].战意点数 -3
		end
	elseif self.临时参数 == "浪涌" then
	self:添加提示(self.参战单位[编号].数字id,"你当前战意为："..self.参战单位[编号].战意点数)
	elseif self.临时参数 == "裂石" then
	self:添加提示(self.参战单位[编号].数字id,"你当前战意为："..self.参战单位[编号].战意点数)
	elseif self.临时参数 == "断岳势" then
	self:添加提示(self.参战单位[编号].数字id,"你当前战意为："..self.参战单位[编号].战意点数)
	elseif self.临时参数 == "腾雷" then
	self:添加提示(self.参战单位[编号].数字id,"你当前战意为："..self.参战单位[编号].战意点数)
	elseif self.临时参数 == "惊涛怒" then
		if self.参战单位[编号].战意点数 < 1 then
			self:添加提示(self.参战单位[编号].数字id,"你的当前战意不足以施放此技能，你当前战意为："..self.参战单位[编号].战意点数)
			return 0
		else
			self.参战单位[编号].战意点数 =self.参战单位[编号].战意点数 -1
			self:添加提示(self.参战单位[编号].数字id,"你当前战意为："..self.参战单位[编号].战意点数)
		end
	end


	if self.临时目标==0 then
	return 0
	end

	for n=1,#self.临时目标 do
	if self.临时目标[n]== self.参战单位[编号].命令数据.目标 then

		self.临时目标[1],self.临时目标[n]=self.临时目标[n],self.临时目标[1]
	self.目标存在=true
	end
	end

	if self.目标存在==false then
	if self:取存活状态(self.参战单位[编号].命令数据.目标) and self:取隐身判断(编号,self.参战单位[编号].命令数据.目标) then
		self.临时目标[1]=self.参战单位[编号].命令数据.目标
	end
	end
	if #self.临时目标==0 then --找不到可攻击的目标
	return 0
	elseif self:技能消耗(self.参战单位[编号],#self.临时目标) ==false then
		if self.临时参数=="破釜沉舟" or	self.临时参数=="剑荡四方" then
		self:添加提示(self.参战单位[编号].数字id,"你的当前气血不足以施放此技能")
		else
		self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
		end
		return 0
	end
	if (self.临时参数=="针锋相对" or self.临时参数=="锋芒毕露") and self.参战单位[编号].攻之械 <3 then
	self.参战单位[编号].攻之械=self.参战单位[编号].攻之械+1
	self.参战单位[编号].伤害=self.参战单位[编号].伤害*1.1
	if self.参战单位[编号].数字id and	UserData[self.参战单位[编号].数字id] then
		SendMessage(UserData[self.参战单位[编号].数字id].连接id, 9,"#xt/#y/当前你的攻之械为："..self.参战单位[编号].攻之械)
		if self.参战单位[编号].攻之械==3 then
			SendMessage(UserData[self.参战单位[编号].数字id].连接id, 9,"#xt/#y/你的攻之械已经达到3点下回合将变身为偃甲状态")
		end
	end
 end
	self.战斗流程[#self.战斗流程+1]={流程=1,执行=false,结束=false,类型="攻击",参数=self.临时参数}
	self.战斗流程[#self.战斗流程].攻击方=编号
	self.战斗流程[#self.战斗流程].挨打方={}
	self.等待结束=self.等待结束+5
	if self.临时参数=="破釜沉舟" or	self.临时参数=="剑荡四方"	then
		self.战斗流程[#self.战斗流程].消耗=math.floor(self.参战单位[编号].当前气血*0.1)
		self.参战单位[编号].当前气血=self.参战单位[编号].当前气血-self.战斗流程[#self.战斗流程].消耗
	end
	for n=1,#self.临时目标 do
		self.等待结束=self.等待结束+2
		self.战斗流程[#self.战斗流程].挨打方[n]={反震=false,反震死亡=false,必杀动作="",防御动作="",反震动作=""}
		self.战斗流程[#self.战斗流程].挨打方[n].编号=self.临时目标[n]
		self.战斗流程[#self.战斗流程].挨打方[n].伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.临时目标[n]],self.临时参数,#self.临时目标)
		self.战斗流程[#self.战斗流程].挨打方[n].类型="掉血"
		self.战斗流程[#self.战斗流程].挨打方[n].死亡=0
		--计算是否必杀
		self.必杀=self:取是否必杀(self.参战单位[编号],self.参战单位[self.临时目标[n]])
		self.吸血=self:取是否吸血(self.参战单位[编号],self.参战单位[self.临时目标[n]])
		if self.参战单位[编号].奇经八脉.化血 and not self.吸血 and math.random(100) <= 30 then
			self.战斗流程[#self.战斗流程].挨打方[n].吸血伤害=self:取物理伤害结果(math.floor(self.临时伤害*0.16),self.参战单位[self.临时目标[n]],self.参战单位[编号],1,"加血")
			self.战斗流程[#self.战斗流程].挨打方[n].吸血=true
		end
		-----------------吸血伤害计算
		if self.吸血	then
			self.战斗流程[#self.战斗流程].挨打方[n].吸血伤害=self:取物理伤害结果(math.floor(self.临时伤害*self.参战单位[编号].吸血),self.参战单位[self.临时目标[n]],self.参战单位[编号],1,"加血")
			self.战斗流程[#self.战斗流程].挨打方[n].吸血=true
		end
		if self.必杀 then
			if self.参战单位[编号].神器 =="威服天下" then
				self.战斗流程[#self.战斗流程].挨打方[n].伤害=math.floor(self.战斗流程[#self.战斗流程].挨打方[n].伤害*2.26)+self.参战单位[编号].暴击伤害
			else 
				self.战斗流程[#self.战斗流程].挨打方[n].伤害=math.floor(self.战斗流程[#self.战斗流程].挨打方[n].伤害*2)+self.参战单位[编号].暴击伤害
			end
			self.战斗流程[#self.战斗流程].挨打方[n].必杀动作="必杀"
		end
		if self.参战单位[self.临时目标[n]].命令数据.类型=="防御" and not self.参战单位[self.临时目标[n]].法术状态组.催眠符 then
				if self.参战单位[self.临时目标[n]].法术状态组.天地同寿~=nil and not self.参战单位[self.临时目标[n]].法术状态组.天地同寿 then
				self.战斗流程[#self.战斗流程].挨打方[n].伤害=math.floor(self.战斗流程[#self.战斗流程].挨打方[n].伤害*0.5)
			end
			self.战斗流程[#self.战斗流程].挨打方[n].防御动作=1
		end
		self.保护=self:取保护单位(self.临时目标[n])
		if self.保护==false then
			self.临时伤害= self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.临时目标[n]],1,"掉血")
		else
			self.临时伤害= self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.临时目标[n]],0.3,"掉血")
			self.临时伤害1= self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.保护],0.7,"掉血")
			self.战斗流程[#self.战斗流程].挨打方[n].保护数据={编号=self.保护,伤害=self.临时伤害1.伤害,类型=self.临时伤害1.类型,死亡=self.临时伤害1.死亡}
			self.等待结束=self.等待结束+5
		end
		self.战斗流程[#self.战斗流程].挨打方[n].伤害=self.临时伤害.伤害
		self.战斗流程[#self.战斗流程].挨打方[n].类型=self.临时伤害.类型
		self.战斗流程[#self.战斗流程].挨打方[n].死亡=self.临时伤害.死亡
		--计算是否反震
		self.反震=self:取是否反震(self.参战单位[编号],self.参战单位[self.临时目标[n]])
		if self.反震 and self:取存活状态(self.临时目标[n]) and self.保护==false then
			self.战斗流程[#self.战斗流程].挨打方[n].反震=true
			self.战斗流程[#self.战斗流程].挨打方[n].反震动作="反震"
			self.战斗流程[#self.战斗流程].挨打方[n].反震伤害=math.floor(self.战斗流程[#self.战斗流程].挨打方[n].伤害*0.25)
			self.临时伤害=self:取物理伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].反震伤害,self.参战单位[self.临时目标[n]],self.参战单位[编号],1,"掉血",1)
			self.战斗流程[#self.战斗流程].挨打方[n].反震死亡=self.临时伤害.死亡
			self.战斗流程[#self.战斗流程].挨打方[n].反震伤害=self.临时伤害.伤害
		end
		if self.临时参数=="锋芒毕露" then
			self:添加封印状态(self.参战单位[self.临时目标[n]],self.参战单位[编号],self.临时参数,编号)
		end
	end
	if self.临时参数=="鹰击" then
		if self.参战单位[编号].助战 == nil and UserData[self.参战单位[编号].玩家id] ~= nil and UserData[self.参战单位[编号].玩家id].角色.元神 ~= nil and UserData[self.参战单位[编号].玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[self.参战单位[编号].玩家id].角色.门派][UserData[self.参战单位[编号].玩家id].角色.元神.."介效果2下限"]
			local 元神加成上限 = SpiritData[UserData[self.参战单位[编号].玩家id].角色.门派][UserData[self.参战单位[编号].玩家id].角色.元神.."介效果2上限"]
			if 取随机数() <= 取随机数(元神加成下限,元神加成上限) then
				self.战斗流程[#self.战斗流程].免休 = true
			else
				self:添加封印状态(self.参战单位[编号],self.参战单位[编号],self.临时参数)
			end
		else
			self:添加封印状态(self.参战单位[编号],self.参战单位[编号],self.临时参数)
		end
	elseif self.临时参数=="破釜沉舟" then
		if self.参战单位[编号].神器 =="藏锋敛锐" and	math.random(100) <30 then 
		else
			self:添加封印状态(self.参战单位[编号],self.参战单位[编号],self.临时参数)
		end
	end
end
 --
function FightControl:单体恢复法术计算(编号,特技)
	if self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false then
	--二次刷新目标
	self.相关id=self.参战单位[编号].队伍id
	self.临时目标=self:取随机单位(编号,self.相关id,2,1)
	if self.临时目标==0 then
		self.参战单位[编号].命令数据.目标=0
	else
	self.参战单位[编号].命令数据.目标=self.临时目标[1]
		end
	end
self.流程类型=特技 and "特技" or "技能"
 self.临时参数=self.参战单位[编号].命令数据.参数
 if self.参战单位[编号].命令数据.目标==0 then --找不到可攻击的目标
	return 0
	elseif 特技==nil and self:技能消耗(self.参战单位[编号]) ==false then
	if self.临时参数=="三花聚顶" or self.临时参数=="乾天罡气" or self.临时参数=="移星换斗" then
	self:添加提示(self.参战单位[编号].数字id,"你的当前气血不足以施放此技能")
	else
	self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
	end
	elseif self.临时参数=="自在心法" and not self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组["普渡众生"] then
	self:添加提示(self.参战单位[编号].数字id,"目标当前没有普渡众生状态")
		return 0
	elseif self.临时参数=="净世煌火" then 
		local 寻找怨灵 ={}
		for n=1,#self.参战单位 do
			if	self.参战单位[n].战斗类型=="召唤类" and self.参战单位[n].名称 == "怨灵" and self.参战单位[n].玩家id==self.参战单位[编号].玩家id	and	self.参战单位[n].队伍id==self.参战单位[编号].队伍id and	self.参战单位[n].单位消失==nil then
				table.insert(寻找怨灵, n)
			end
		end
		if #寻找怨灵 < 1 then
		self:添加提示(self.参战单位[编号].数字id,"当前你所召唤的怨灵数量不足1个,施法失败")
			return 
		else 
			local 召还ID ={}
			local 位置数={}
			do
				local TempID =self.参战单位[编号].玩家id
				local 临时区分id=0
				if UserData[TempID].队伍==0 then
					临时区分id=TempID
				else
					临时区分id=UserData[TempID].队伍
				end
				if self.队伍区分[1]==临时区分id then
						位置数= self.队伍位置[1]
				else
					self.队伍区分[2]=临时区分id
					位置数= self.队伍位置[2]
				end
			end
			for i=1,1 do
			self.参战单位[寻找怨灵[i]].单位消失 = 1
			位置数[self.参战单位[ 寻找怨灵[i]].位置] =nil
			table.insert(召还ID,寻找怨灵[i])
			end
			self.战斗流程[#self.战斗流程+1]={流程=1,挨打方=table.copy(召还ID),执行=false,类型="召还"}
			self.等待结束=self.等待结束+5
		end
	elseif 特技~=nil then
	if self:特技消耗(self.参战单位[编号],self.临时参数) ==false then
	self:添加提示(self.参战单位[编号].数字id,"你没那么多的愤怒")
	return 0
		end
	end
	if self.临时参数=="三花聚顶" or self.临时参数== "乾天罡气" or self.临时参数== "移星换斗" then
	self.等待结束=self.等待结束+5
	self.临时参数=self.参战单位[编号].命令数据.参数
	self.战斗流程[#self.战斗流程+1]={流程=4,执行=false,结束=false,类型=self.流程类型,参数=self.临时参数,封印结果=true}--流程=30为从非物理技能开始
	self.战斗流程[#self.战斗流程].攻击方=编号
	self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
	self.战斗流程[#self.战斗流程].法术伤害=30
	self.战斗流程[#self.战斗流程].伤害类型="掉血"
	self.战斗流程[#self.战斗流程].法术死亡=0
	self.参战单位[编号].当前气血=self.参战单位[编号].当前气血 - 30
	self.参战单位[编号].当前魔法=self.参战单位[编号].当前魔法 +self:取技能伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数)
		if self.参战单位[编号].当前魔法> self.参战单位[编号].魔法上限 then
			self.参战单位[编号].当前魔法=self.参战单位[编号].魔法上限
		end
	else
		self.等待结束=self.等待结束+5
	self.临时参数=self.参战单位[编号].命令数据.参数
	self.战斗流程[#self.战斗流程+1]={流程=4,执行=false,结束=false,类型=self.流程类型,参数=self.临时参数,封印结果=true}--流程=30为从非物理技能开始
	self.战斗流程[#self.战斗流程].攻击方=编号
	self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
	self.战斗流程[#self.战斗流程].法术伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数)
	self.战斗流程[#self.战斗流程].伤害类型="加血"
	self.临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].法术伤害,self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],1,self.战斗流程[#self.战斗流程].伤害类型)
	self.战斗流程[#self.战斗流程].法术伤害=self.临时伤害.伤害
	self.战斗流程[#self.战斗流程].伤害类型=self.临时伤害.类型
	self.战斗流程[#self.战斗流程].法术死亡=0
	end
	if self.临时参数=="普渡众生" then
	self:添加封印状态(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],self.临时参数)
	elseif self.临时参数=="冰清诀" then
	self:解除异常状态(self.参战单位[self.参战单位[编号].命令数据.目标])

	elseif self.临时参数=="仙人指路" and math.random(100)<15 then
	self:解除异常状态(self.参战单位[self.参战单位[编号].命令数据.目标])
	elseif self.临时参数=="水清诀" then
	self:解除异常状态(self.参战单位[self.参战单位[编号].命令数据.目标])
	elseif self.临时参数=="凝气诀" then
	self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法+math.floor(self.参战单位[self.参战单位[编号].命令数据.目标].魔法上限*0.1+150)
	if self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法>self.参战单位[self.参战单位[编号].命令数据.目标].魔法上限 then
	self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=self.参战单位[self.参战单位[编号].命令数据.目标].魔法上限
	end
	elseif self.临时参数=="凝神诀" then
	self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法+math.floor(self.参战单位[self.参战单位[编号].命令数据.目标].魔法上限*0.15+250)
	if self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法>self.参战单位[self.参战单位[编号].命令数据.目标].魔法上限 then
	self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=self.参战单位[self.参战单位[编号].命令数据.目标].魔法上限
	end
	elseif self.临时参数=="解毒" then
	self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.毒=nil
	self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.尸腐毒=nil
	elseif self.临时参数=="清心" then
	self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.毒=nil
	self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.尸腐毒=nil
	self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.雾杀=nil
	elseif self.临时参数=="驱尸" then
	self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.毒=nil
	self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组.尸腐毒=nil
	elseif self.临时参数=="舍生取义" then
	self.参战单位[编号].防御=math.floor(self.参战单位[编号].防御*0.95)
	self.参战单位[编号].法防=math.floor(self.参战单位[编号].法防*0.97)
	end
 end
function FightControl:群体恢复法术计算(编号,特技)
	self.相关id=self.参战单位[编号].队伍id
	self.临时参数=self.参战单位[编号].命令数据.参数
	self.技能数量=self:取技能目标数(self.参战单位[编号],self.临时参数)
	if self.临时参数=="推气过宫" or self.临时参数=="地涌金莲" or self.临时参数=="匠心·蓄锐"	then
		self.临时目标=self:取随机单位1(编号,self.相关id,2,self.技能数量,self.临时参数)
	else
		self.临时目标=self:取随机单位(编号,self.相关id,2,self.技能数量,self.临时参数)
	end


 
	self.目标存在=false
	if self.临时目标==0 then return 0 end
	for n=1,#self.临时目标 do
	if self.临时目标[n]== self.参战单位[编号].命令数据.目标 then
	self.目标存在=true
		end
	end
	if self.目标存在==false then
	if self:取存活状态(self.参战单位[编号].命令数据.目标) then
	if self.临时参数=="推气过宫" or self.临时参数=="地涌金莲" or self.临时参数=="匠心·蓄锐" or self.临时参数=="峰回路转" then
		self.临时目标[#self.临时目标]=self.参战单位[编号].命令数据.目标
	else
		self.临时目标[math.random(1,#self.临时目标)]=self.参战单位[编号].命令数据.目标
		end
	end
	end
 self.流程类型=特技 and "特技" or "技能"
 if #self.临时目标==0 then --找不到可攻击的目标
	return 0
	elseif 特技==nil	and self:技能消耗(self.参战单位[编号],#self.临时目标) ==false then
	self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
	return 0
	elseif 特技~=nil then
	if self:特技消耗(self.参战单位[编号],self.临时参数) ==false then
	self:添加提示(self.参战单位[编号].数字id,"你没那么多的愤怒")
	return 0
		end
	end


 self.战斗流程[#self.战斗流程+1]={流程=4,执行=false,结束=false,类型=self.流程类型,参数=self.临时参数}--流程=30为从非物理技能开始
 self.战斗流程[#self.战斗流程].攻击方=编号
 self.战斗流程[#self.战斗流程].挨打方={}
 self.等待结束=self.等待结束+5
 if self.临时参数=="匠心·蓄锐" and self.参战单位[编号].攻之械 <3 then
	self.参战单位[编号].攻之械=self.参战单位[编号].攻之械+1
	self.参战单位[编号].伤害=self.参战单位[编号].伤害*1.1
	if self.参战单位[编号].数字id and	UserData[self.参战单位[编号].数字id] then
	if self.参战单位[编号].数字id and	UserData[self.参战单位[编号].数字id] then
		SendMessage(UserData[self.参战单位[编号].数字id].连接id, 9,"#xt/#y/当前你的攻之械为："..self.参战单位[编号].攻之械)
		if self.参战单位[编号].攻之械==3 then
			SendMessage(UserData[self.参战单位[编号].数字id].连接id, 9,"#xt/#y/你的攻之械已经达到3点下回合将变身为偃甲状态")
		end
	end
	end
 end
	local 慈悲翻倍 =false
	if self.临时参数=="推气过宫" and self.参战单位[编号].法宝效果.慈悲 and math.random(100)<self.参战单位[编号].法宝效果.慈悲 then
		慈悲翻倍=true
	end

	
 for n=1,#self.临时目标 do
	self.战斗流程[#self.战斗流程].挨打方[n]={}
	self.战斗流程[#self.战斗流程].挨打方[n].编号=self.临时目标[n]
	if self.临时参数=="九幽" then
		self.战斗流程[#self.战斗流程].挨打方[n].伤害=self.参战单位[编号].法宝效果.九幽
	else
		self.战斗流程[#self.战斗流程].挨打方[n].伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.临时目标[n]],self.临时参数,#self.临时目标)
	end
	if 慈悲翻倍 then
	self.战斗流程[#self.战斗流程].挨打方[n].伤害=self.战斗流程[#self.战斗流程].挨打方[n].伤害*2
	end
	self.战斗流程[#self.战斗流程].挨打方[n].类型="加血"
	self.战斗流程[#self.战斗流程].挨打方[n].死亡=0
	self.临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[self.临时目标[n]],1,self.战斗流程[#self.战斗流程].挨打方[n].类型)
	self.战斗流程[#self.战斗流程].挨打方[n].伤害=self.临时伤害.伤害
	self.战斗流程[#self.战斗流程].挨打方[n].类型=self.临时伤害.类型
	self.战斗流程[#self.战斗流程].挨打方[n].死亡=self.临时伤害.死亡
	--计算是否神佑
	if self.临时参数=="生命之泉" or self.临时参数=="魔兽之印" or self.临时参数=="圣灵之甲" or self.临时参数=="罗汉金钟"or self.临时参数=="普渡众生" or self.临时参数== "九幽" then
		self:添加封印状态(self.参战单位[self.临时目标[n]],self.参战单位[编号],self.临时参数)
		elseif self.临时参数=="玉清诀" or self.临时参数=="晶清诀" then
		self:解除异常状态(self.参战单位[self.临时目标[n]])
		end
	end
 end
 --
function FightControl:单体法术计算(编号,特技,特定,目标)
	local 临时参数,临时目标
	if 特定==nil then
	临时参数=self.参战单位[编号].命令数据.参数
	else
	临时参数=特定
	end
	if 目标~=nil then
	临时目标=目标
	else
	临时目标= self.参战单位[编号].命令数据.目标
	end
	if 临时目标==0 or self:取存活状态(临时目标)==false or self:取隐身判断(编号,临时目标)==false then
	if 目标~=nil then
		return 0
	end
	local 临时目标1=self:取随机单位(编号,self.参战单位[编号].队伍id,1,1)
	if 临时目标1==0 then
	临时目标=0
	else
		临时目标=临时目标1[1]
	end
	end
	self.流程类型=特技 and "特技" or "技能"
	if 临时目标==0 then --找不到可攻击的目标
	return 0
	elseif 特定==nil and 特技==nil and self:技能消耗(self.参战单位[编号]) ==false then
	self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
	return 0
	elseif 特技~=nil then
	if self:特技消耗(self.参战单位[编号],临时参数) ==false then
		self:添加提示(self.参战单位[编号].数字id,"你没那么多的愤怒")
		return 0
	end
	end
	if	临时参数=="上古灵符" then
	local sglf ={"冰冻","流沙","心火","怒雷"}
	临时参数 =sglf[math.random(1,#sglf)]
	self.战斗流程[#self.战斗流程+1]={流程=1,执行=false,结束=false,类型=self.流程类型,参数=临时参数}--流程=30为从非物理技能开始
	self.战斗流程[#self.战斗流程].攻击方=编号
	self.战斗流程[#self.战斗流程].挨打方={{法术暴击=false,编号=临时目标,伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[临时目标],"上古灵符"),类型="掉血",死亡=0,}}
	self.等待结束=self.等待结束+5





	if 临时参数 == "怒雷" then
		self.战斗流程[#self.战斗流程].挨打方[1].伤害=self.战斗流程[#self.战斗流程].挨打方[1].伤害*2
	elseif 临时参数 == "冰冻" then
		self.战斗流程[#self.战斗流程].封印结果=self:取封印结果(self.参战单位[编号],self.参战单位[临时目标],临时参数)
		if self.战斗流程[#self.战斗流程].封印结果 then
		self:添加封印状态(self.参战单位[临时目标],self.参战单位[编号],临时参数,self.临时目标)
		end
	elseif 临时参数 == "流沙" then
		if self.参战单位[临时目标].战斗类型=="角色" and self.参战单位[临时目标].愤怒	then
			self.参战单位[临时目标].愤怒=self.参战单位[临时目标].愤怒-15
			if self.参战单位[临时目标].愤怒<0 then self.参战单位[临时目标].愤怒=0 end
		end
	elseif 临时参数 == "心火" then
		self.参战单位[临时目标].当前魔法=self.参战单位[临时目标].当前魔法-math.floor(self.参战单位[临时目标].等级)
		if self.参战单位[临时目标].当前魔法<0 then self.参战单位[临时目标].当前魔法=0 end
	end
 else
	self.战斗流程[#self.战斗流程+1]={流程=1,执行=false,结束=false,类型=self.流程类型,参数=临时参数}--流程=30为从非物理技能开始
	self.战斗流程[#self.战斗流程].攻击方=编号
	self.战斗流程[#self.战斗流程].挨打方={{法术暴击=false,编号=临时目标,伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[临时目标],临时参数),类型="掉血",死亡=0,}}
	if self:取法术暴击(临时参数) then
		if math.random(100) <= math.floor((self.参战单位[编号].法暴+(self.参战单位[编号].法术暴击等级-self.参战单位[临时目标].抗法术暴击等级)*(1000/self.参战单位[编号].等级)/100)) then

		self.战斗流程[#self.战斗流程].挨打方[1].伤害 =math.floor(self.战斗流程[#self.战斗流程].挨打方[1].伤害* 1.5)
		self.战斗流程[#self.战斗流程].挨打方[1].法术暴击=true
		end
	end
	self.等待结束=self.等待结束+5
	end

	--先计算法术吸收
	if 临时参数=="水攻" then
	if math.random(100)<=self.参战单位[临时目标].水吸 or (self.参战单位[临时目标].法术状态组.颠倒五行 and math.random(1000)<=self.参战单位[临时目标].法术状态组.颠倒五行.几率) then
		self.战斗流程[#self.战斗流程].挨打方[1].伤害=math.floor(self.战斗流程[#self.战斗流程].挨打方[1].伤害*0.25)
		if self.战斗流程[#self.战斗流程].挨打方[1].伤害<=1 then self.战斗流程[#self.战斗流程].挨打方[1].伤害=1 end
		self.战斗流程[#self.战斗流程].挨打方[1].类型="加血"
		end
	elseif 临时参数=="烈火" then
		if math.random(100)<=self.参战单位[临时目标].火吸 or (self.参战单位[临时目标].法术状态组.颠倒五行 and math.random(1000)<=self.参战单位[临时目标].法术状态组.颠倒五行.几率) then
			self.战斗流程[#self.战斗流程].挨打方[1].伤害=math.floor(self.战斗流程[#self.战斗流程].挨打方[1].伤害*0.25)
			if self.战斗流程[#self.战斗流程].挨打方[1].伤害<=1 then 
			self.战斗流程[#self.战斗流程].挨打方[1].伤害=1 
			end
			self.战斗流程[#self.战斗流程].挨打方[1].类型="加血"
		end
	elseif 临时参数=="落岩"	then
		if math.random(100)<=self.参战单位[临时目标].土吸 or (self.参战单位[临时目标].法术状态组.颠倒五行 and math.random(1000)<=self.参战单位[临时目标].法术状态组.颠倒五行.几率) then
		self.战斗流程[#self.战斗流程].挨打方[1].伤害=math.floor(self.战斗流程[#self.战斗流程].挨打方[1].伤害*0.25)
		if self.战斗流程[#self.战斗流程].挨打方[1].伤害<=1 then self.战斗流程[#self.战斗流程].挨打方[1].伤害=1 end
		self.战斗流程[#self.战斗流程].挨打方[1].类型="加血"
		end
	elseif 临时参数=="雷击"	then
		if math.random(100)<=self.参战单位[临时目标].雷吸 or (self.参战单位[临时目标].法术状态组.颠倒五行 and math.random(1000)<=self.参战单位[临时目标].法术状态组.颠倒五行.几率) then
		self.战斗流程[#self.战斗流程].挨打方[1].伤害=math.floor(self.战斗流程[#self.战斗流程].挨打方[1].伤害*0.25)
		if self.战斗流程[#self.战斗流程].挨打方[1].伤害<=1 then self.战斗流程[#self.战斗流程].挨打方[1].伤害=1 end
		self.战斗流程[#self.战斗流程].挨打方[1].类型="加血"
		end
	elseif 临时参数=="血雨"	then
	self.战斗流程[#self.战斗流程].消耗=math.floor(self.参战单位[编号].当前气血*0.1)
	self.参战单位[编号].当前气血=self.参战单位[编号].当前气血-self.战斗流程[#self.战斗流程].消耗
	end
	if self.参战单位[临时目标].法宝效果.降魔斗篷 and math.random(100) < 30 then
		self.临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].挨打方[1].伤害,self.参战单位[编号],self.参战单位[临时目标],(1-self.参战单位[临时目标].法宝效果.降魔斗篷),self.战斗流程[#self.战斗流程].挨打方[1].类型)
		self.战斗流程[#self.战斗流程].挨打方[1].降魔斗篷 = true
	else
	self.临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].挨打方[1].伤害,self.参战单位[编号],self.参战单位[临时目标],1,self.战斗流程[#self.战斗流程].挨打方[1].类型)
	end
	if self:取是否法术吸血(self.参战单位[编号],self.参战单位[临时目标]) then
		self.战斗流程[#self.战斗流程].法术吸血=self:取法术伤害结果(math.floor(self.临时伤害.伤害*self.参战单位[编号].法术吸血),self.参战单位[临时目标],self.参战单位[编号],1,"加血")
	end
	self.战斗流程[#self.战斗流程].挨打方[1].伤害=self.临时伤害.伤害
	self.战斗流程[#self.战斗流程].挨打方[1].类型=self.临时伤害.类型
	self.战斗流程[#self.战斗流程].挨打方[1].死亡=self.临时伤害.死亡
	if 临时参数=="月光" then
		if	self:取存活状态(临时目标) then
		self.战斗流程[#self.战斗流程+1]={流程=1,执行=false,结束=false,类型=self.流程类型,参数=临时参数}--流程=30为从非物理技能开始
		self.战斗流程[#self.战斗流程].攻击方=编号
		self.战斗流程[#self.战斗流程].挨打方={{法术暴击=false,编号=临时目标,伤害=math.floor(self:取技能伤害(self.参战单位[编号],self.参战单位[临时目标],临时参数)*0.5),类型="掉血",死亡=0,}}
		if self:取法术暴击(临时参数) then
			if math.random(100) <= math.floor((self.参战单位[编号].法暴+(self.参战单位[编号].法术暴击等级-self.参战单位[临时目标].抗法术暴击等级)*(1000/self.参战单位[编号].等级)/100)) then
			self.战斗流程[#self.战斗流程].挨打方[1].伤害 =math.floor(self.战斗流程[#self.战斗流程].挨打方[1].伤害* 1.5)
			self.战斗流程[#self.战斗流程].挨打方[1].法术暴击=true
			end
		end
		self.临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].挨打方[1].伤害,self.参战单位[编号],self.参战单位[临时目标],1,self.战斗流程[#self.战斗流程].挨打方[1].类型)
		self.战斗流程[#self.战斗流程].挨打方[1].伤害=self.临时伤害.伤害
		self.战斗流程[#self.战斗流程].挨打方[1].类型=self.临时伤害.类型
		self.战斗流程[#self.战斗流程].挨打方[1].死亡=self.临时伤害.死亡
		self.等待结束=self.等待结束+5
	end
		if	self:取存活状态(临时目标) then
		self.战斗流程[#self.战斗流程+1]={流程=1,执行=false,结束=false,类型=self.流程类型,参数=临时参数}--流程=30为从非物理技能开始
		self.战斗流程[#self.战斗流程].攻击方=编号
		self.战斗流程[#self.战斗流程].挨打方={{法术暴击=false,编号=临时目标,伤害=math.floor(self:取技能伤害(self.参战单位[编号],self.参战单位[临时目标],临时参数)*0.25),类型="掉血",死亡=0,}}
		if self:取法术暴击(临时参数) then
			if math.random(100) <=	math.floor((self.参战单位[编号].法暴+(self.参战单位[编号].法术暴击等级-self.参战单位[临时目标].抗法术暴击等级)*(1000/self.参战单位[编号].等级)/100)) then
			self.战斗流程[#self.战斗流程].挨打方[1].伤害 =math.floor(self.战斗流程[#self.战斗流程].挨打方[1].伤害* 1.5)
			self.战斗流程[#self.战斗流程].挨打方[1].法术暴击=true
			end
		end
			self.临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].挨打方[1].伤害,self.参战单位[编号],self.参战单位[临时目标],1,self.战斗流程[#self.战斗流程].挨打方[1].类型)
		self.战斗流程[#self.战斗流程].挨打方[1].伤害=self.临时伤害.伤害
		self.战斗流程[#self.战斗流程].挨打方[1].类型=self.临时伤害.类型
		self.战斗流程[#self.战斗流程].挨打方[1].死亡=self.临时伤害.死亡
		self.等待结束=self.等待结束+5
	end
		if	self:取存活状态(临时目标) then
		self.战斗流程[#self.战斗流程+1]={流程=1,执行=false,结束=false,类型=self.流程类型,参数=临时参数}--流程=30为从非物理技能开始
		self.战斗流程[#self.战斗流程].攻击方=编号
		self.战斗流程[#self.战斗流程].挨打方={{法术暴击=false,编号=临时目标,伤害=math.floor(self:取技能伤害(self.参战单位[编号],self.参战单位[临时目标],临时参数)*0.125),类型="掉血",死亡=0,}}
		if self:取法术暴击(临时参数) then
			if math.random(100) <=	math.floor((self.参战单位[编号].法暴+(self.参战单位[编号].法术暴击等级-self.参战单位[临时目标].抗法术暴击等级)*(1000/self.参战单位[编号].等级)/100)) then
			self.战斗流程[#self.战斗流程].挨打方[1].伤害 =math.floor(self.战斗流程[#self.战斗流程].挨打方[1].伤害* 1.5)
			self.战斗流程[#self.战斗流程].挨打方[1].法术暴击=true
			end
		end
		self.临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].挨打方[1].伤害,self.参战单位[编号],self.参战单位[临时目标],1,self.战斗流程[#self.战斗流程].挨打方[1].类型)
		self.战斗流程[#self.战斗流程].挨打方[1].伤害=self.临时伤害.伤害
		self.战斗流程[#self.战斗流程].挨打方[1].类型=self.临时伤害.类型
		self.战斗流程[#self.战斗流程].挨打方[1].死亡=self.临时伤害.死亡
		self.等待结束=self.等待结束+5
	end
	end
 end
function FightControl:取法术暴击(技能)
	if 技能=="五雷轰顶"or 技能=="判官令"or 技能== "血雨"or 技能=="勾魂" or 技能=="炽火流离" or 技能=="摄魄" or 技能=="天罗地网"or 技能=="雨落寒沙" or
	技能=="苍茫树" or 技能=="地裂火" or 技能=="巨岩破" or 技能=="靛沧海"or 技能=="日光华" or 技能=="夺命咒" or 技能=="破击" or 技能=="云暗天昏"or 技能=="阎罗令" or 技能=="诅咒之伤" or 技能=="上古灵符"	then
		return false
	else
		return true
	end
end

function FightControl:群体法术计算(编号) ---------------群体法术完成
	local 临时参数=self.参战单位[编号].命令数据.参数
	local 临时目标=self:取随机单位(编号,self.参战单位[编号].队伍id,1,self:取技能目标数(self.参战单位[编号],临时参数))
	local czmb=false
	if 临时目标==0 then
	return 0
	end
	for n=1,#临时目标 do
	if 临时目标[n]== self.参战单位[编号].命令数据.目标 then
	czmb=true
		end
	end
	if czmb==false then
	if self:取存活状态(self.参战单位[编号].命令数据.目标)	and	self:取隐身判断(编号,self.参战单位[编号].命令数据.目标) then
	临时目标[math.random(1,#临时目标)]=self.参战单位[编号].命令数据.目标
	end
	end
 if #临时目标==0 then --找不到可攻击的目标
	return 0
	elseif self:技能消耗(self.参战单位[编号],#临时目标) ==false then
	self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
	return 0
	end
	if 临时参数 =="焚魔烈焰" then
	local 寻找怨灵 ={}
	for n=1,#self.参战单位 do
		if	self.参战单位[n].战斗类型=="召唤类" and self.参战单位[n].名称 == "怨灵" and self.参战单位[n].玩家id==self.参战单位[编号].玩家id	and	self.参战单位[n].队伍id==self.参战单位[编号].队伍id and	self.参战单位[n].单位消失==nil then
			table.insert(寻找怨灵, n)
		end
		end
		if #寻找怨灵 < 1 then
		self:添加提示(self.参战单位[编号].数字id,"当前你所召唤的怨灵数量不足1个,施法失败")
			return 
		else 
			local 召还ID ={}
			local 位置数={}
			do
				local TempID =self.参战单位[编号].玩家id
				local 临时区分id=0
				if UserData[TempID].队伍==0 then
					临时区分id=TempID
				else
					临时区分id=UserData[TempID].队伍
				end
				if self.队伍区分[1]==临时区分id then
						位置数= self.队伍位置[1]
				else
					self.队伍区分[2]=临时区分id
					位置数= self.队伍位置[2]
				end
			end
			for i=1,1 do
			self.参战单位[寻找怨灵[i]].单位消失 = 1
			位置数[self.参战单位[ 寻找怨灵[i]].位置] =nil
			table.insert(召还ID,寻找怨灵[i])
			end
			self.战斗流程[#self.战斗流程+1]={流程=1,挨打方=table.copy(召还ID),执行=false,类型="召还"}
			self.等待结束=self.等待结束+5
		end
	elseif 临时参数=="破击" and self.参战单位[编号].攻之械 <3 then
	self.参战单位[编号].攻之械=self.参战单位[编号].攻之械+1
	self.参战单位[编号].伤害=self.参战单位[编号].伤害*1.1
	if self.参战单位[编号].数字id and	UserData[self.参战单位[编号].数字id] then
		SendMessage(UserData[self.参战单位[编号].数字id].连接id, 9,"#xt/#y/当前你的攻之械为："..self.参战单位[编号].攻之械)
		if self.参战单位[编号].攻之械==3 then
			SendMessage(UserData[self.参战单位[编号].数字id].连接id, 9,"#xt/#y/你的攻之械已经达到3点下回合将变身为偃甲状态")
		end
	end

	end
 self.战斗流程[#self.战斗流程+1]={流程=1,执行=false,结束=false,类型="技能",参数=临时参数}--流程=30为从非物理技能开始
 self.战斗流程[#self.战斗流程].攻击方=编号

 self.战斗流程[#self.战斗流程].挨打方={}
 self.等待结束=self.等待结束+10
 for n=1,#临时目标 do
	self.战斗流程[#self.战斗流程].挨打方[n]={编号=临时目标[n],伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[临时目标[n]],临时参数,#临时目标)
	,类型="掉血",法术暴击=false,死亡=0}
		if self:取法术暴击(临时参数) then
		if math.random(100) <=	math.floor((self.参战单位[编号].法暴+(self.参战单位[编号].法术暴击等级-self.参战单位[临时目标[n]].抗法术暴击等级)*(1000/self.参战单位[编号].等级)/100)) then
		self.战斗流程[#self.战斗流程].挨打方[n].伤害 = math.floor(self.战斗流程[#self.战斗流程].挨打方[n].伤害* 1.5 ) ---法术暴击公式改这里
		self.战斗流程[#self.战斗流程].挨打方[n].法术暴击=true
		end
		end
	local 临时伤害
	if self.参战单位[临时目标[n]].法宝效果.降魔斗篷 and math.random(100) < 30 then
	临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[临时目标[n]],(1-self.参战单位[临时目标[n]].法宝效果.降魔斗篷),self.战斗流程[#self.战斗流程].挨打方[n].类型)
	self.战斗流程[#self.战斗流程].挨打方[n].降魔斗篷 = true
	else
	临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].挨打方[n].伤害,self.参战单位[编号],self.参战单位[临时目标[n]],1,self.战斗流程[#self.战斗流程].挨打方[n].类型)
	end

	self.战斗流程[#self.战斗流程].挨打方[n].伤害=临时伤害.伤害
	self.战斗流程[#self.战斗流程].挨打方[n].类型=临时伤害.类型
	self.战斗流程[#self.战斗流程].挨打方[n].死亡=临时伤害.死亡	

		if n==1 and	self:取是否法术吸血(self.参战单位[编号],self.参战单位[临时目标[n]]) then
		self.战斗流程[#self.战斗流程].法术吸血=self:取法术伤害结果(math.floor(self.临时伤害*self.参战单位[编号].法术吸血),self.参战单位[临时目标[n]],self.参战单位[编号],1,"加血")
		end
		if	临时参数 =="雨落寒沙" then
		if math.random(100)<=15 and self.参战单位[临时目标[n]].复活==0 and not self.参战单位[临时目标[n]].法术状态组.百毒不侵 then
			self.参战单位[临时目标[n]].法术状态组["毒"]={回合=5,等级=self.参战单位[编号].等级}
			self.战斗流程[#self.战斗流程].挨打方[n].毒=true
		end
		elseif self.参战单位[编号].奇经八脉.电芒 and 临时参数 == "雷霆万钧"	then
			self.参战单位[临时目标[n]].法术状态组["电芒"]={回合=4}
		self.战斗流程[#self.战斗流程].挨打方[n].电芒=true
		end
	end

 end
--
function FightControl:单体持续伤害法术计算(编号)
	if self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false or self:取隐身判断(编号,self.参战单位[编号].命令数据.目标)==false then
	self.相关id=self.参战单位[编号].队伍id
	self.临时目标=self:取随机单位(编号,self.相关id,1,1)
	if self.临时目标==0 then
		self.参战单位[编号].命令数据.目标=0
	else
	self.参战单位[编号].命令数据.目标=self.临时目标[1]
		end
	end

 if self.参战单位[编号].命令数据.目标==0 then --找不到可攻击的目标
	return 0
	elseif self:技能消耗(self.参战单位[编号]) ==false then
	self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
	return 0
	end
 self.临时参数=self.参战单位[编号].命令数据.参数
 self.战斗流程[#self.战斗流程+1]={流程=4,执行=false,结束=false,类型="技能",参数=self.临时参数,死亡=0}
 self.战斗流程[#self.战斗流程].攻击方=编号
 self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
 self.战斗流程[#self.战斗流程].封印结果=self:取封印结果(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数)
 self.等待结束=self.等待结束+5
 if self.战斗流程[#self.战斗流程].封印结果 then
	self.战斗流程[#self.战斗流程].伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数)
	self.临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].伤害,self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],1,"掉血",1)
	self.战斗流程[#self.战斗流程].伤害=self.临时伤害.伤害
	self.战斗流程[#self.战斗流程].死亡=self.临时伤害.死亡
		self:添加封印状态(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],self.临时参数)
		if self.临时参数=="尸腐毒" and self.参战单位[编号].法宝效果.九幽 then
		self.参战单位[编号].命令数据.参数 ="九幽"
		self.参战单位[编号].命令数据.目标=编号
		self:群体恢复法术计算(编号)
		end
	end
 end
function FightControl:群体封印法术计算(编号,特技)
	local 临时参数=self.参战单位[编号].命令数据.参数
	local 临时目标=self:取随机单位(编号,self.参战单位[编号].队伍id,1,self:取技能目标数(self.参战单位[编号],临时参数))
	local czmb=false
	if 临时目标==0 then
	return 0
	end
	for n=1,#临时目标 do
	if 临时目标[n]== self.参战单位[编号].命令数据.目标 then
	czmb=true
		end
	end
	if czmb==false then
	if self:取存活状态(self.参战单位[编号].命令数据.目标)	and	self:取隐身判断(编号,self.参战单位[编号].命令数据.目标) then
	临时目标[math.random(1,#临时目标)]=self.参战单位[编号].命令数据.目标
	end
	end
 if #临时目标==0 then --找不到可攻击的目标
	return 0
	elseif self:技能消耗(self.参战单位[编号],#临时目标) ==false then
	self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
	return 0
	end




 self.战斗流程[#self.战斗流程+1]={流程=4,执行=false,结束=false,类型="技能",参数=临时参数}--流程=30为从非物理技能开始
 self.战斗流程[#self.战斗流程].攻击方=编号
 self.战斗流程[#self.战斗流程].挨打方={}
 self.等待结束=self.等待结束+5



 for n=1,#临时目标 do
	self.战斗流程[#self.战斗流程].挨打方[n]={}
	self.战斗流程[#self.战斗流程].挨打方[n].编号=临时目标[n]
	if 临时参数=="飞花摘叶" then
			local 临时伤害 = self:取技能伤害(self.参战单位[编号],self.参战单位[临时目标[n]],临时参数)
			self.参战单位[临时目标[n]].当前魔法=self.参战单位[临时目标[n]].当前魔法-临时伤害
			if self.参战单位[临时目标[n]].当前魔法<=0 then
			self.参战单位[临时目标[n]].当前魔法=0
			end
			self.参战单位[编号].当前魔法=self.参战单位[编号].当前魔法+math.floor(临时伤害*0.5)
			self.战斗流程[#self.战斗流程].死亡=0
			if self.参战单位[编号].当前魔法>self.参战单位[编号].魔法上限 then
			self.参战单位[编号].当前魔法=self.参战单位[编号].魔法上限
			end
	elseif 临时参数=="怨怖之泣" then
 
	
		for y=1,#解除名称 do
			if self.参战单位[临时目标[n]].法术状态组[解除名称[y]] then
			self.参战单位[临时目标[n]].法术状态组[解除名称[y]].回合 =self.参战单位[临时目标[n]].法术状态组[解除名称[y]].回合-1
			end
		end
		
	else
		self.战斗流程[#self.战斗流程].挨打方[n].封印结果=self:取封印结果(self.参战单位[编号],self.参战单位[临时目标[n]],临时参数)
		if self.战斗流程[#self.战斗流程].挨打方[n].封印结果 then
		self:添加封印状态(self.参战单位[临时目标[n]],self.参战单位[编号],临时参数,临时目标[n])
		end
	end

 end
end
function FightControl:单体封印法术计算(编号,特技)
	if self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false or self:取隐身判断(编号,self.参战单位[编号].命令数据.目标)==false then
	--二次刷新目标
	self.相关id=self.参战单位[编号].队伍id
	self.临时目标=self:取随机单位(编号,self.相关id,1,1)
	if self.临时目标==0 then
		self.参战单位[编号].命令数据.目标=0
	else
	self.参战单位[编号].命令数据.目标=self.临时目标[1]
		end
	end
	self.流程类型=特技 and "特技" or "技能"
 if self.参战单位[编号].命令数据.目标==0 then --找不到可攻击的目标
	return 0
	elseif 特技==nil and self:技能消耗(self.参战单位[编号]) ==false then
	self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
	return 0
	elseif 特技~=nil then
	if self:特技消耗(self.参战单位[编号],self.临时参数) ==false then
	self:添加提示(self.参战单位[编号].数字id,"你没那么多的愤怒")
	return 0
		end
	end
 self.临时参数=self.参战单位[编号].命令数据.参数

 self.战斗流程[#self.战斗流程+1]={流程=4,执行=false,结束=false,类型=self.流程类型,参数=self.临时参数}
 self.战斗流程[#self.战斗流程].攻击方=编号
 self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
 self.战斗流程[#self.战斗流程].封印结果=self:取封印结果(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数)
	self.等待结束=self.等待结束+5
	if self.战斗流程[#self.战斗流程].封印结果 then
		if self.临时参数=="勾魂" or self.临时参数=="炽火流离" then
			self.战斗流程[#self.战斗流程].法术伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数)
			self.参战单位[self.参战单位[编号].命令数据.目标].当前气血=self.参战单位[self.参战单位[编号].命令数据.目标].当前气血-self.战斗流程[#self.战斗流程].法术伤害
			if self.参战单位[self.参战单位[编号].命令数据.目标].当前气血<=0 then
			self.参战单位[self.参战单位[编号].命令数据.目标].当前气血=0
			self.战斗流程[#self.战斗流程].死亡=self:取死亡状态(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标])
			else
			self.战斗流程[#self.战斗流程].死亡=0
			end
			self.战斗流程[#self.战斗流程].恢复伤害=math.floor(self.战斗流程[#self.战斗流程].法术伤害*0.5+1)
			self.参战单位[编号].当前气血=self.参战单位[编号].当前气血+self.战斗流程[#self.战斗流程].恢复伤害
			if self.参战单位[编号].当前气血>self.参战单位[编号].气血上限 then
			self.参战单位[编号].当前气血=self.参战单位[编号].气血上限
			end
		elseif self.临时参数=="摄魄" then
			local 临时伤害 = self:取技能伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数)
			self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法-临时伤害
			if self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法<=0 then
			self.参战单位[self.参战单位[编号].命令数据.目标].当前魔法=0
			end
			self.参战单位[编号].当前魔法=self.参战单位[编号].当前魔法+math.floor(临时伤害*0.5)
			self.战斗流程[#self.战斗流程].死亡=0
			if self.参战单位[编号].当前魔法>self.参战单位[编号].魔法上限 then
			self.参战单位[编号].当前魔法=self.参战单位[编号].魔法上限
			end
		else
		self:添加封印状态(self.参战单位[self.参战单位[编号].命令数据.目标],self.参战单位[编号],self.临时参数,self.临时目标)
		end
	end
 end
function FightControl:单体复活法术计算(编号)
 if self.参战单位[编号].命令数据.目标==0 then --找不到可攻击的目标
	return 0
	elseif self:取存活状态(self.参战单位[编号].命令数据.目标) then
	self:添加提示(self.参战单位[编号].数字id,"该单位目前无法被复活")
	return 0
	elseif self:取复活状态(self.参战单位[编号].命令数据.目标) then
	self:添加提示(self.参战单位[编号].数字id,"该单位目前有死亡召唤状态无法被复活")
	return 0
	elseif	self.参战单位[self.参战单位[编号].命令数据.目标].战斗类型~="角色" and self.参战单位[编号].命令数据.参数~="莲花心音"then
	self:添加提示(self.参战单位[编号].数字id,"该单位目前无法被复活。")
	return 0
	elseif self:技能消耗(self.参战单位[编号]) ==false then
	self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
	return 0
	end
	if self.临时参数=="莲花心音" and	self.参战单位[self.参战单位[编号].命令数据.目标].复活==0 then
	self:添加提示(self.参战单位[编号].数字id,"只能复活鬼魂术的宝宝")
	return 0

	end
	self.等待结束=self.等待结束+5
	self.临时参数=self.参战单位[编号].命令数据.参数
	self.战斗流程[#self.战斗流程+1]={流程=4,执行=false,结束=false,类型="技能",参数=self.临时参数,封印结果=true}--流程=30为从非物理技能开始

	if self.临时参数=="由己渡人" then
	self.战斗流程[#self.战斗流程].自身伤害=math.floor(self.参战单位[编号].当前气血*0.1)
	self.参战单位[编号].当前气血=self.参战单位[编号].当前气血-self.战斗流程[#self.战斗流程].自身伤害
	end
	self.战斗流程[#self.战斗流程].攻击方=编号
	self.战斗流程[#self.战斗流程].挨打方=self.参战单位[编号].命令数据.目标
	self.战斗流程[#self.战斗流程].法术伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],self.临时参数)
	self.战斗流程[#self.战斗流程].伤害类型="加血"
	self.临时伤害=self:取法术伤害结果(self.战斗流程[#self.战斗流程].法术伤害,self.参战单位[编号],self.参战单位[self.参战单位[编号].命令数据.目标],1,self.战斗流程[#self.战斗流程].伤害类型)
	self.战斗流程[#self.战斗流程].法术伤害=self.临时伤害.伤害
	self.战斗流程[#self.战斗流程].伤害类型=self.临时伤害.类型
	self.战斗流程[#self.战斗流程].法术死亡=0
 end
function FightControl:群体复活法术计算(编号)
	self.临时参数=self.参战单位[编号].命令数据.参数
 self.临时目标=self:取起群体复活单位(编号,self.参战单位[编号].队伍id,0,5)
 if self.临时目标==0 then --找不到可攻击的目标
	self:添加提示(self.参战单位[编号].数字id,"当前没有队友处于死亡状态")
	return 0
	elseif self:特技消耗(self.参战单位[编号],self.临时参数) ==false then
	self:添加提示(self.参战单位[编号].数字id,"你的当前愤怒不足以施放此技能")
	return 0
	end
	self.等待结束=self.等待结束+5
	self.临时参数=self.参战单位[编号].命令数据.参数
	self.战斗流程[#self.战斗流程+1]={流程=4,执行=false,结束=false,类型="特技",参数=self.临时参数}--流程=30为从非物理技能开始
	self.战斗流程[#self.战斗流程].攻击方=编号
	self.战斗流程[#self.战斗流程].挨打方={}
	self.参战单位[编号].当前魔法=0
	self.气血伤害值=math.floor(self.参战单位[编号].气血上限*0.1)
	if self.参战单位[编号].当前气血<=self.气血伤害值 then
		self.战斗流程[#self.战斗流程].消耗伤害=0
	else
		self.战斗流程[#self.战斗流程].消耗伤害=self.参战单位[编号].当前气血-self.气血伤害值
		self.参战单位[编号].当前气血=self.气血伤害值
	end
	for n=1,#self.临时目标 do
		self.战斗流程[#self.战斗流程].挨打方[n]={}
		self.战斗流程[#self.战斗流程].挨打方[n].编号=self.临时目标[n]
		self.战斗流程[#self.战斗流程].挨打方[n].伤害=self:取技能伤害(self.参战单位[编号],self.参战单位[self.临时目标[n]],self.临时参数)
		self.参战单位[self.临时目标[n]].当前气血=self.战斗流程[#self.战斗流程].挨打方[n].伤害
	end
 end
--
function FightControl:解除门派封印法术(门派,攻击方)
	if 门派=="女儿村" then
	self.解除名称={"如花解语","似玉生香","莲步轻舞","娉婷嬝娜","一笑倾城"}
	elseif 门派=="方寸山" then
	self.解除名称={"离魂符","失心符","追魂符","催眠符","定身符","失魂符","失忆符"}
	elseif 门派=="天宫" then
	self.解除名称={"百万神兵","镇妖"}
 elseif 门派=="无底洞" then
	self.解除名称={"夺魄令","煞气诀"}
	elseif 门派=="盘丝" then
	self.解除名称={"含情脉脉"}
	elseif 门派=="增益" then
	self.解除名称={"生命之泉" ,"普渡众生" ,"火甲术","颠倒五行","乾坤妙法","天地同寿","四面埋伏","灵动九天","神龙摆尾",
	"佛法无边","幽冥鬼眼","一苇渡江","金刚护体","金刚护法","不动如山","韦陀护法","明光宝烛","镇魂诀","金身舍利","蜜润","红袖添香",
	"达摩护体" ,"定心术","潜力激发","杀气诀","魔王回首","牛劲" ,"安神诀" ,"极度疯狂" ,"分身术","百毒不侵" ,"楚楚可怜","盘丝阵" ,"移魂化骨",
	"天神护法" ,"天神护体","逆鳞" ,"炎护" ,"碎星诀","乘风破浪","凝神术","匠心·削铁","匠心·固甲","无所遁形","铜头铁臂","气慑天军","无畏布施"}
	end
	for n=1,#self.解除名称 do
	if 攻击方.法术状态组[self.解除名称[n]] then
	攻击方.法术状态组[self.解除名称[n]]=nil
	self:解除封印状态(攻击方,self.解除名称[n])
	end
	end
 end
function FightControl:解除异常状态(攻击方)
	self.异常状态名称={"错乱","含情脉脉","尸腐毒","如花解语","似玉生香","莲步轻舞","离魂符","失心符","追魂符","催眠符","定身符","失魂符","失忆符","百万神兵","镇妖","日月乾坤","紧箍咒","雾杀","夺魄令","煞气诀","娉婷嬝娜","一笑倾城"}
 for n=1,#self.异常状态名称 do
	if 攻击方.法术状态组[self.异常状态名称[n]]then
	self:解除封印状态(攻击方,self.异常状态名称[n])
	end
	end
 end
function FightControl:催眠状态解除(类型,挨打方)
	if 类型=="掉血" and 挨打方.法术状态组.催眠符 then
	self:解除封印状态(挨打方,"催眠符")
	end
	if 类型=="掉血" and 挨打方.法术状态组.煞气诀 then
	self:解除封印状态(挨打方,"煞气诀")
	end
 end
function FightControl:解除封印状态(攻击方,技能名称)
	if 攻击方.法术状态组[技能名称] then
	if 技能名称=="含情脉脉" or 技能名称=="似玉生香" or 技能名称=="威慑" or 技能名称=="象形" or 技能名称=="冰冻"	then
	攻击方.攻击封印=nil
	攻击方.技能封印=nil
	攻击方.特技封印=nil
	攻击方.封印开关=false
	elseif 技能名称=="波澜不惊"	then
	攻击方.法术状态组[技能名称].伤害 = nil
		elseif 技能名称=="铜头铁臂"	then
	攻击方.法术状态组[技能名称].护盾 = nil
	攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="气慑天军" then
	攻击方.物伤减免 =1
	elseif 技能名称=="威震凌霄" then
	攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
	攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害

	elseif 技能名称=="由己渡人"	then
	攻击方.法术状态组[技能名称].伤害 = nil
	elseif 技能名称=="鬼魂术" then
	攻击方.当前气血=攻击方.气血上限
	elseif 技能名称=="红袖添香" then
	攻击方.速度=攻击方.速度-攻击方.法术状态组[技能名称].速度
	elseif 技能名称=="谜毒之缚" then
	攻击方.治疗能力=攻击方.治疗能力+攻击方.法术状态组[技能名称].治疗能力
	elseif 技能名称=="惊魂掌" then
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="日月乾坤"	then
	攻击方.攻击封印=nil
	攻击方.技能封印=nil
	攻击方.特技封印=nil
	攻击方.封印开关=false
	elseif 技能名称=="移魂化骨" then
	攻击方.吸血=攻击方.吸血-攻击方.法术状态组[技能名称].吸血
	elseif 技能名称=="达摩护体" then
	if 攻击方.战斗类型=="角色" then
		攻击方.最大气血=攻击方.最大气血-攻击方.法术状态组[技能名称].气血
		else
		攻击方.气血上限=攻击方.气血上限-攻击方.法术状态组[技能名称].气血
	end
	elseif 技能名称=="韦陀护法" then
	攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="百万神兵" then
	攻击方.攻击封印=nil
	攻击方.技能封印=nil
	攻击方.特技封印=nil
	攻击方.封印开关=false
	elseif 技能名称=="如花解语" then
	攻击方.攻击封印=nil
	攻击方.技能封印=nil
	攻击方.特技封印=nil
	攻击方.封印开关=false
	elseif 技能名称=="莲步轻舞"or 技能名称=="一笑倾城" then
	攻击方.攻击封印=nil
	攻击方.技能封印=nil
	攻击方.特技封印=nil
	攻击方.封印开关=false
	elseif 技能名称=="楚楚可怜" then
	攻击方.攻击封印=nil
	攻击方.技能封印=nil
	攻击方.特技封印=nil
	攻击方.封印开关=false
	elseif 技能名称=="娉婷嬝娜" then
	攻击方.攻击封印=nil
	攻击方.技能封印=nil
	攻击方.特技封印=nil
	攻击方.封印开关=false
	elseif 技能名称=="失忆符" or 技能名称=="镇妖" then
	攻击方.攻击封印=nil
	攻击方.技能封印=nil
	攻击方.特技封印=nil
	攻击方.封印开关=false
	elseif 技能名称=="失魂符" or 技能名称=="追魂符" or 技能名称=="夺魄令" or 技能名称=="错乱"	then
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	攻击方.攻击封印=nil
	攻击方.技能封印=nil
	攻击方.特技封印=nil
	攻击方.封印开关=false
	elseif 技能名称=="碎甲符" then
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	攻击方.法防=攻击方.法防+攻击方.法术状态组[技能名称].法防
	elseif 技能名称=="灵动九天" then
	攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
	elseif 技能名称=="蜜润" then
	攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
	elseif 技能名称=="干将莫邪" then
	攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="舞雪冰蝶" then
	攻击方.命中=攻击方.命中+攻击方.法术状态组[技能名称].命中
	elseif 技能名称=="摄魂" then
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	攻击方.法防=攻击方.法防+攻击方.法术状态组[技能名称].法防
	elseif 技能名称=="罗汉金钟" then
	攻击方.法伤减免=1
 elseif 技能名称=="太极护法" then
	攻击方.法伤减免=1
	elseif 技能名称=="圣灵之甲" then
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="光辉之甲" then
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="魔兽之印" then
	攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="野兽之力" then
	攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="流云诀" then
	攻击方.速度=攻击方.速度+攻击方.法术状态组[技能名称].速度
	elseif 技能名称=="无字经" then
		攻击方.法宝封印=nil
	攻击方.封印开关=false
	elseif 技能名称=="发瘟匣"	then
		攻击方.技能封印=nil
	攻击方.封印开关=false
	elseif 技能名称=="失心符" then
	攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
	攻击方.法防=攻击方.法防+攻击方.法术状态组[技能名称].法防
	攻击方.攻击封印=nil
	攻击方.技能封印=nil
	攻击方.特技封印=nil
	攻击方.封印开关=false
	elseif 技能名称=="安神诀" then
	攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
	攻击方.法防=攻击方.法防-攻击方.法术状态组[技能名称].法防
	elseif 技能名称=="灵法" then
	攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力*0.8
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="灵断" then
	攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
	elseif 技能名称=="怒吼" then
	攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
	攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="御风" then
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	攻击方.速度=攻击方.速度-攻击方.法术状态组[技能名称].速度
	elseif 技能名称=="灵刃" then
	攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
	攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="离魂符" then
	攻击方.躲闪=攻击方.躲闪+攻击方.法术状态组[技能名称].躲闪
	攻击方.攻击封印=nil
	攻击方.技能封印=nil
	攻击方.特技封印=nil
	攻击方.封印开关=false
	elseif 技能名称=="一苇渡江" then
	攻击方.速度=攻击方.速度+攻击方.法术状态组[技能名称].速度
	elseif 技能名称=="催眠符" then
	攻击方.封印开关=false
	elseif 技能名称=="煞气诀" then
	攻击方.封印开关=false
	elseif 技能名称=="盘丝阵" then
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
 elseif 技能名称=="定身符" then
	攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	攻击方.攻击封印=nil
	攻击方.技能封印=nil
	攻击方.特技封印=nil
	攻击方.封印开关=false
	elseif 技能名称=="天神护体" then
	攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
	攻击方.法防=攻击方.法防+攻击方.法术状态组[技能名称].法防
	elseif 技能名称=="天神护法" then
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="牛劲" then
	攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
	elseif 技能名称=="杀气诀" then
	攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害

	elseif 技能名称=="金刚护体" then
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="金身舍利" then
	攻击方.法防=攻击方.法防+攻击方.法术状态组[技能名称].法防
	elseif 技能名称=="明光宝烛" then
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="不动如山" then
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="法术防御" then
	攻击方.法伤减免=攻击方.法伤减免+0.4
	elseif 技能名称=="神龙摆尾" then
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="乘风破浪" then
	攻击方.躲闪=攻击方.躲闪-攻击方.法术状态组[技能名称].躲闪
	elseif 技能名称=="金刚护法" then
	攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="匠心·固甲" then
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="匠心·削铁" then
	攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害

	elseif 技能名称=="镇魂诀" then
	攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="碎星诀" then
	攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="定心术" then
	攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
	elseif 技能名称=="无畏布施" then
	攻击方.法防=攻击方.法防-攻击方.法术状态组[技能名称].法防
攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
		elseif 技能名称=="高级进击必杀" then
		攻击方.必杀= 攻击方.必杀-攻击方.法术状态组[技能名称].必杀
		elseif 技能名称=="北冥之渊" then
	攻击方.伤害= 攻击方.伤害-攻击方.法术状态组[技能名称].伤害
	攻击方.灵力= 攻击方.灵力-攻击方.法术状态组[技能名称].灵力

	elseif 技能名称=="进击必杀" then

	攻击方.必杀= 攻击方.必杀-攻击方.法术状态组[技能名称].必杀
		elseif 技能名称=="高级进击法暴" then

	攻击方.法暴= 攻击方.法暴-攻击方.法术状态组[技能名称].法暴
		elseif 技能名称=="进击法暴" then

	攻击方.法暴= 攻击方.法暴-攻击方.法术状态组[技能名称].法暴 

 elseif 技能名称=="无所遁形" then

	攻击方.穿刺等级=攻击方.穿刺等级-攻击方.法术状态组[技能名称].穿刺等级
	攻击方.物理暴击等级=攻击方.物理暴击等级-攻击方.法术状态组[技能名称].物理暴击等级

	elseif 技能名称=="四面埋伏" then
	攻击方.封印命中等级=攻击方.封印命中等级-攻击方.法术状态组[技能名称].封印命中等级
	elseif 技能名称=="潜力激发" then
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
	攻击方.速度=攻击方.速度-攻击方.法术状态组[技能名称].速度
	攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害

	elseif 技能名称=="变身" then
	攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="逆鳞" then
	攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="后发制人" then
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
	攻击方.速度=攻击方.速度-攻击方.法术状态组[技能名称].速度
	攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
	攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="瘴气" then
	攻击方.气血回复效果=攻击方.气血回复效果+攻击方.法术状态组[技能名称].气血回复效果
	elseif 技能名称=="横扫千军" then
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
	elseif 技能名称=="天崩地裂" then
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
	elseif 技能名称=="金刚镯"	then
	if 攻击方.武器伤害~=nil then
	攻击方.武器伤害=攻击方.武器伤害+攻击方.法术状态组[技能名称].伤害
	end
	end
	攻击方.法术状态组[技能名称]=nil
	end

 end
function FightControl:添加封印状态(攻击方,挨打方,技能名称,目标)
	self.临时等级=self:取技能等级(挨打方,技能名称)
	if 技能名称=="解封" then
		self:解除门派封印法术("女儿村",攻击方)
		return 0
	elseif 技能名称=="驱魔" then
		self:解除门派封印法术("方寸山",攻击方)
	elseif 技能名称=="魂飞魄散" or 技能名称=="八戒上身"	then
		self:解除门派封印法术("增益",攻击方)
		return 0
	elseif 技能名称=="驱尸" then
		return 0
	end
	if 攻击方.法术状态组[技能名称] and 技能名称~="分身术" then
		self:解除封印状态(攻击方,技能名称)
	end

	if 技能名称=="达摩护体" or 技能名称=="分身术" or 技能名称=="灵动九天" or 技能名称=="蜜润" or 技能名称=="颠倒五行" or 技能名称=="乾坤妙法" or 技能名称=="天地同寿 " or 技能名称=="碎甲符" or 技能名称=="野兽之力" or 技能名称=="流云诀" or 技能名称=="魔兽之印" or 技能名称=="光辉之甲" or 技能名称=="安神诀" or 技能名称=="圣灵之甲" or 技能名称=="神龙摆尾" or 技能名称=="太极护法" or 技能名称=="罗汉金钟" or 技能名称=="碎甲符" or 技能名称=="魔兽之印" or 技能名称=="光辉之甲" or 技能名称=="安神诀"then
		if 攻击方.法术状态组[技能名称] then
			return 0
		end
	end
	攻击方.法术状态组[技能名称]={}
	if 技能名称=="含情脉脉" or 技能名称=="似玉生香" or 技能名称=="威慑" then
		攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
		攻击方.攻击封印=true
		攻击方.技能封印=true
		攻击方.封印开关=true
	elseif 技能名称=="寡欲令"	then
		攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
		self:解除门派封印法术("盘丝",攻击方)
	elseif 技能名称=="炎护" then
		攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/25)+2
	elseif 技能名称=="波澜不惊"	then
		攻击方.法术状态组[技能名称]={回合=5,伤害 = 攻击方.等级*15+150}
		攻击方.法术状态组[技能名称].伤害=攻击方.法术状态组[技能名称].伤害*(1+攻击方.修炼数据.法术*0.03)+攻击方.治疗能力+挨打方.气血回复效果
	elseif 技能名称=="铜头铁臂" then
		攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/25)+2
		攻击方.法术状态组[技能名称].护盾 =math.floor(self.临时等级*8)
		攻击方.法术状态组[技能名称].伤害 =math.floor(self.临时等级)
		攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="气慑天军" then
		攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/25)+2
		攻击方.物伤减免 =0.8
	elseif 技能名称=="高级进击必杀" then
		攻击方.法术状态组[技能名称].回合=5
		攻击方.法术状态组[技能名称].必杀 = 20
		攻击方.必杀= 攻击方.必杀+攻击方.法术状态组[技能名称].必杀
	
	elseif 技能名称=="北冥之渊" then
		攻击方.法术状态组[技能名称].回合=2
		攻击方.法术状态组[技能名称].伤害 =攻击方.伤害*0.5
		攻击方.法术状态组[技能名称].灵力 =攻击方.灵力*0.5
		攻击方.伤害= 攻击方.伤害+攻击方.法术状态组[技能名称].伤害
		攻击方.灵力= 攻击方.灵力+攻击方.法术状态组[技能名称].灵力

	elseif 技能名称=="进击必杀" then
		攻击方.法术状态组[技能名称].回合=5
		攻击方.法术状态组[技能名称].必杀 = 10
		攻击方.必杀= 攻击方.必杀+攻击方.法术状态组[技能名称].必杀
	elseif 技能名称=="高级进击法暴" then
	攻击方.法术状态组[技能名称].回合=5
	攻击方.法术状态组[技能名称].法暴 = 15
	攻击方.法暴= 攻击方.法暴+攻击方.法术状态组[技能名称].法暴
		elseif 技能名称=="进击法暴" then
	攻击方.法术状态组[技能名称].回合=5
	攻击方.法术状态组[技能名称].法暴 = 7
	攻击方.法暴= 攻击方.法暴+攻击方.法术状态组[技能名称].法暴 


	elseif 技能名称=="威震凌霄" then
	攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/25)+1
	攻击方.法术状态组[技能名称].灵力 =math.floor(攻击方.灵力*0.3)
	攻击方.法术状态组[技能名称].伤害 =math.floor(攻击方.伤害*0.3)
	攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
	攻击方.伤害=攻击方.伤害-攻击方.法术状态组[技能名称].伤害
	
	elseif 技能名称=="达摩护体" then
	攻击方.法术状态组[技能名称].回合=4
	攻击方.法术状态组[技能名称].气血= math.floor(self.临时等级*15)
	if 攻击方.战斗类型 == "角色" then
	攻击方.最大气血=攻击方.最大气血+攻击方.法术状态组[技能名称].气血
	end
	elseif 技能名称=="死亡召唤"	then
	攻击方.法术状态组[技能名称].回合=10
		elseif 技能名称=="锢魂术"	then
	攻击方.法术状态组[技能名称].回合=5
	elseif 技能名称=="韦陀护法" then
	攻击方.法术状态组[技能名称].回合=5
	攻击方.法术状态组[技能名称].伤害=math.floor(self.临时等级*0.5)
	攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="火甲术"	then
	攻击方.法术状态组[技能名称].回合=3
	elseif 技能名称=="惊魂掌" then
	攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/25)+3
	攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*2)
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="移魂化骨" then
	攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/35)+2
	攻击方.法术状态组[技能名称].吸血=self.临时等级*1.2/1000
	攻击方.吸血=攻击方.吸血+攻击方.法术状态组[技能名称].吸血
	elseif 技能名称=="炼气化神"	then
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/25)
	攻击方.法术状态组[技能名称].魔法=math.floor(self.临时等级/2)
	攻击方.当前魔法=攻击方.当前魔法+攻击方.法术状态组[技能名称].魔法
	if 攻击方.当前魔法>攻击方.魔法上限 then
	攻击方.当前魔法=攻击方.魔法上限
	end
	elseif 技能名称=="魔息术"	then
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/35)
	攻击方.法术状态组[技能名称].魔法=math.floor(self.临时等级/5)
	攻击方.当前魔法=攻击方.当前魔法+攻击方.法术状态组[技能名称].魔法
	if 攻击方.当前魔法>攻击方.魔法上限 then
	攻击方.当前魔法=攻击方.魔法上限
	end
	elseif 技能名称=="金刚镯"	then
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
	攻击方.法术状态组[技能名称].伤害=math.floor(self.临时等级*2)
	if 攻击方.武器伤害~=nil then
	攻击方.武器伤害=攻击方.武器伤害-攻击方.法术状态组[技能名称].伤害
	end
	elseif 技能名称=="瘴气"	then
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
	攻击方.法术状态组[技能名称].气血回复效果=math.floor(self.临时等级*5)
	攻击方.气血回复效果=攻击方.气血回复效果-攻击方.法术状态组[技能名称].气血回复效果
	elseif 技能名称=="娉婷嬝娜" then
	攻击方.法术状态组[技能名称].前置=true
	攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)
	攻击方.特技封印=true
	攻击方.封印开关=true
	elseif 技能名称=="佛法无边"	then
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/40)
	攻击方.法术状态组[技能名称].几率=self.临时等级
	elseif 技能名称=="分身术"	then
	攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/30)+1
	if 挨打方.奇经八脉.化身 then
	攻击方.法术状态组[技能名称].回合=攻击方.法术状态组[技能名称].回合+1
	end
	攻击方.法术状态组[技能名称].躲避=false
 elseif 技能名称=="灵动九天"	then
	攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/40)
	攻击方.法术状态组[技能名称].灵力=self.临时等级
	if 挨打方.奇经八脉.灵动 then
		攻击方.法术状态组[技能名称].回合=攻击方.法术状态组[技能名称].回合+3
		攻击方.法术状态组[技能名称].灵力=攻击方.法术状态组[技能名称].灵力+30
	end
	攻击方.灵力=攻击方.灵力+self.临时等级
	elseif 技能名称=="红袖添香" then
	攻击方.法术状态组[技能名称].回合=4
	攻击方.法术状态组[技能名称].速度=math.floor(self.临时等级*0.83)
	攻击方.速度=攻击方.速度+攻击方.法术状态组[技能名称].速度


	elseif 技能名称=="谜毒之缚" then
	攻击方.法术状态组[技能名称].回合=4
	攻击方.法术状态组[技能名称].治疗能力=self.临时等级
	攻击方.治疗能力=攻击方.治疗能力-攻击方.法术状态组[技能名称].治疗能力

 elseif 技能名称=="蜜润"	then
	攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/40)+1
	攻击方.法术状态组[技能名称].灵力=self.临时等级
	if 挨打方.神器=="钟灵" then
		攻击方.法术状态组[技能名称].灵力 =math.floor(攻击方.法术状态组[技能名称].灵力*1.5)
	end
	攻击方.灵力=攻击方.灵力+self.临时等级
	elseif 技能名称=="颠倒五行"	then
	攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/30)+1
	攻击方.法术状态组[技能名称].几率=self.临时等级
	elseif 技能名称=="乾坤妙法"	then
	攻击方.法术状态组[技能名称].回合=1
	elseif 技能名称=="天地同寿"	then
	攻击方.法术状态组[技能名称].回合=1
 elseif 技能名称=="象形" or 技能名称=="冰冻" then
	攻击方.法术状态组[技能名称].回合=2
	攻击方.攻击封印=true
	攻击方.技能封印=true
	攻击方.封印开关=true
	elseif 技能名称=="碎甲符" then

	攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/50)+1
	攻击方.防御=攻击方.防御-math.floor(self.临时等级)
	攻击方.法防=攻击方.法防-math.floor(self.临时等级*0.35)
	攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级)
	攻击方.法术状态组[技能名称].法防=math.floor(self.临时等级*0.35)
	elseif 技能名称=="野兽之力" then
	攻击方.法术状态组[技能名称].回合=5
	攻击方.法术状态组[技能名称].伤害=math.floor(攻击方.伤害*0.1)
	攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="流云诀" then
	攻击方.法术状态组[技能名称].回合=5
	攻击方.法术状态组[技能名称].速度 =math.floor(攻击方.速度*0.1)
	攻击方.速度=攻击方.速度+攻击方.法术状态组[技能名称].速度
	elseif 技能名称=="魔兽之印" then

	攻击方.法术状态组[技能名称].回合=5
	攻击方.法术状态组[技能名称].伤害=math.floor(攻击方.伤害*0.05)
	攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="光辉之甲" then

	攻击方.法术状态组[技能名称].回合=5
	攻击方.法术状态组[技能名称].防御=math.floor(攻击方.防御*0.1)
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="安神诀" then

	攻击方.法术状态组[技能名称].回合=4
		攻击方.法术状态组[技能名称].灵力= math.floor(self.临时等级*0.55)
	攻击方.法术状态组[技能名称].法防=math.floor(self.临时等级*0.35)
	攻击方.法防=攻击方.法防+math.floor(self.临时等级*0.35)
	攻击方.灵力=攻击方.灵力+math.floor(self.临时等级*0.55)

	elseif 技能名称=="圣灵之甲" then
	攻击方.法术状态组[技能名称].回合=5
	攻击方.法术状态组[技能名称].防御=math.floor(攻击方.防御*0.05)
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="神龙摆尾" then
	攻击方.法术状态组[技能名称].回合=4
	攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*2)
	攻击方.防御=攻击方.防御+math.floor(self.临时等级*2)
 elseif 技能名称=="太极护法" then
	攻击方.法术状态组[技能名称].回合=5
	攻击方.法伤减免=攻击方.法伤减免-0.5
	elseif 技能名称=="罗汉金钟" then
	攻击方.法术状态组[技能名称].回合=4
	攻击方.法伤减免=攻击方.法伤减免-0.5
	elseif 技能名称=="幽冥鬼眼" then
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
	elseif 技能名称=="魔王回首" then
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
	elseif 技能名称=="极度疯狂" then
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
	elseif 技能名称=="百毒不侵" then
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
	if 攻击方.法术状态组["毒"] then
	攻击方.法术状态组["毒"]=nil
	end
	if 攻击方.法术状态组["尸腐毒"] then
	攻击方.法术状态组["尸腐毒"]=nil

	end
	elseif 技能名称=="宁心"	then
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
	self:解除门派封印法术("女儿村",攻击方)
	elseif 技能名称=="复苏"	then
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
	self:解除门派封印法术("天宫",攻击方)
	elseif 技能名称=="魔音摄魂"	then
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
	elseif 技能名称=="日月乾坤" then
	攻击方.法术状态组[技能名称].回合=2+math.floor(self.临时等级/35)
	攻击方.攻击封印=true
	攻击方.技能封印=true
	攻击方.特技封印=true
	攻击方.封印开关=true
	elseif 技能名称=="百万神兵" then
	攻击方.法术状态组[技能名称].前置=true
	攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)
	攻击方.攻击封印=true
	攻击方.封印开关=true
	elseif 技能名称=="如花解语" then
	攻击方.法术状态组[技能名称].前置=true
	攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)
	攻击方.攻击封印=true
	攻击方.封印开关=true
	elseif 技能名称=="莲步轻舞"or 技能名称=="一笑倾城" then
	攻击方.法术状态组[技能名称].前置=true
	攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)
	攻击方.技能封印=true
	攻击方.封印开关=true
	elseif 技能名称=="催眠符" then
	攻击方.法术状态组[技能名称].前置=true
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/35)
	攻击方.封印开关=true
	elseif 技能名称=="煞气诀" then
	攻击方.法术状态组[技能名称].前置=true
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/35)
	攻击方.封印开关=true
	elseif 技能名称=="失心符" then
	攻击方.法术状态组[技能名称].前置=true
	攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)
	攻击方.法术状态组[技能名称].灵力=math.floor(self.临时等级)
	攻击方.法术状态组[技能名称].法防=math.floor(self.临时等级*0.5)
	攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
	攻击方.法防=攻击方.法防-攻击方.法术状态组[技能名称].法防
	攻击方.技能封印=true
	攻击方.封印开关=true
 elseif 技能名称=="天神护体" then
	攻击方.法术状态组[技能名称].前置=true
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
	攻击方.法术状态组[技能名称].灵力=math.floor(self.临时等级)
	攻击方.法术状态组[技能名称].法防=math.floor(self.临时等级*0.5)
	攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
	攻击方.法防=攻击方.法防+攻击方.法术状态组[技能名称].法防
	elseif 技能名称=="天神护法" then
	攻击方.法术状态组[技能名称].前置=true
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
	攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级)
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="牛劲" then
	攻击方.法术状态组[技能名称].前置=true
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
	if 挨打方.奇经八脉.连营 then
		攻击方.法术状态组[技能名称].回合=攻击方.法术状态组[技能名称].回合+1
	end
	攻击方.法术状态组[技能名称].灵力=math.floor(self.临时等级)
	if 挨打方.奇经八脉.炙烤 then
		攻击方.法术状态组[技能名称].灵力=math.floor(攻击方.法术状态组[技能名称].灵力*1.1)
	end
	攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
	elseif 技能名称=="楚楚可怜" then
	攻击方.法术状态组[技能名称].回合=2+math.floor(self.临时等级/25)
	攻击方.技能封印=true
	攻击方.封印开关=true
	elseif 技能名称=="失忆符" or 技能名称=="镇妖" then
	攻击方.法术状态组[技能名称].前置=true
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/25)
	攻击方.特技封印=true
	攻击方.封印开关=true
 elseif 技能名称=="盘丝阵" then
	攻击方.法术状态组[技能名称].回合=2+math.floor(self.临时等级/25)
	攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*1.5)
	if 挨打方.奇经八脉.结阵 then
		攻击方.法术状态组[技能名称].防御=攻击方.法术状态组[技能名称].防御*2
		攻击方.法术状态组[技能名称].回合=攻击方.法术状态组[技能名称].回合-3
	end
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
 elseif 技能名称=="失魂符" then
	攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)+1
	攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*1.5)
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
	攻击方.技能封印=true
	攻击方.封印开关=true
 elseif 技能名称=="错乱" then
	攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)+1
	攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*1.5)
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
	攻击方.技能封印=true
	攻击方.封印开关=true
 elseif 技能名称=="夺魄令" then
	攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)+1
	攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*1.5)
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
	if 挨打方.奇经八脉.追魂 then
	攻击方.攻击封印=true
	end
	攻击方.技能封印=true
	攻击方.封印开关=true
	elseif 技能名称=="一苇渡江" then
	攻击方.法术状态组[技能名称].回合=2+math.floor(self.临时等级/25)+1
	攻击方.法术状态组[技能名称].速度=math.floor(self.临时等级)
	if 挨打方.奇经八脉.流刚 then
	攻击方.法术状态组[技能名称].速度= math.floor(攻击方.法术状态组[技能名称].速度*1.2)
	end
	攻击方.速度=攻击方.速度+攻击方.法术状态组[技能名称].速度
	elseif 技能名称=="追魂符" then
	攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)+1
	攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*1.5)
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
	攻击方.攻击封印=true
	攻击方.封印开关=true
	elseif 技能名称=="离魂符" then
	攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)+1
	攻击方.法术状态组[技能名称].躲闪=math.floor(self.临时等级)
	攻击方.躲闪=攻击方.躲闪-攻击方.法术状态组[技能名称].躲闪
	攻击方.技能封印=true
	攻击方.封印开关=true
 elseif 技能名称=="定身符" then
	攻击方.法术状态组[技能名称].回合=3+math.floor(self.临时等级/25)+1
	攻击方.法术状态组[技能名称].灵力=math.floor(self.临时等级*0.75)
	攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
	攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级*0.5)
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
	攻击方.攻击封印=true
	攻击方.封印开关=true
	elseif 技能名称=="尸腐毒" or 技能名称=="紧箍咒" or 技能名称=="雾杀"	then
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/25)+2
	攻击方.法术状态组[技能名称].伤害=self:取技能伤害(挨打方,攻击方,技能名称)
	攻击方.法术状态组[技能名称].攻击方=挨打方
	elseif 技能名称=="普渡众生" then
	攻击方.法术状态组[技能名称].回合=2+math.floor(self.临时等级/30)
	攻击方.法术状态组[技能名称].伤害=self:取技能伤害(挨打方,攻击方,技能名称)
	if 挨打方.奇经八脉.普渡 then
		攻击方.法术状态组[技能名称].伤害=math.floor(攻击方.法术状态组[技能名称].伤害*1.12)
	end
	elseif 技能名称=="九幽" then
	攻击方.法术状态组[技能名称].回合=5
	攻击方.法术状态组[技能名称].伤害=self:取技能伤害(挨打方,攻击方,技能名称)
	elseif	技能名称=="生命之泉" then
	攻击方.法术状态组[技能名称].回合=2+math.floor(self.临时等级/30)
	攻击方.法术状态组[技能名称].伤害=self:取技能伤害(挨打方,攻击方,技能名称)
	elseif 技能名称=="杀气诀" then
	攻击方.法术状态组[技能名称].回合=4
	攻击方.法术状态组[技能名称].伤害=math.floor(self.临时等级*1.5)
	攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="凝神术" then
	攻击方.法术状态组[技能名称].回合=4
	elseif 技能名称=="金刚护体" then
	攻击方.法术状态组[技能名称].回合=6
	攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级/25)+3
		if 挨打方.奇经八脉.流刚 then
	攻击方.法术状态组[技能名称].防御= math.floor(攻击方.法术状态组[技能名称].防御*1.2)
	end
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="明光宝烛" then
	攻击方.法术状态组[技能名称].回合=4
	攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级/25)+3
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="不动如山" then
	攻击方.法术状态组[技能名称].回合=4
	攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级/25)+3
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="法术防御" then
	攻击方.法伤减免=攻击方.法伤减免-0.4
	攻击方.法术状态组[技能名称].回合=4
	elseif 技能名称=="乘风破浪" then
	攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/25)+3
	攻击方.法术状态组[技能名称].躲闪=math.floor(self.临时等级*0.5)
	攻击方.躲闪=攻击方.躲闪+攻击方.法术状态组[技能名称].躲闪
	elseif 技能名称=="金刚护法" then
	攻击方.法术状态组[技能名称].回合=5
	攻击方.法术状态组[技能名称].伤害=math.floor(self.临时等级-10)
	if 挨打方.奇经八脉.流刚 then
	攻击方.法术状态组[技能名称].伤害= math.floor(攻击方.法术状态组[技能名称].伤害*1.2)
	end
	攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害

	elseif 技能名称=="匠心·固甲" then
	攻击方.法术状态组[技能名称].回合=4
	攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级/40)+3
	if 挨打方.攻之械 >=3 then
	攻击方.法术状态组[技能名称].防御=攻击方.法术状态组[技能名称].防御*1.5
	end
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	elseif 技能名称=="匠心·削铁" then
	攻击方.法术状态组[技能名称].回合=4
	攻击方.法术状态组[技能名称].伤害=math.floor(self.临时等级*0.7)
	if 挨打方.攻之械 >=3 then
	攻击方.法术状态组[技能名称].伤害=攻击方.法术状态组[技能名称].伤害*1.5
	end
	攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="镇魂诀" then
	攻击方.法术状态组[技能名称].回合=5
	攻击方.法术状态组[技能名称].伤害=math.floor(self.临时等级*1.5)
	攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="碎星诀" then
	if 挨打方.奇经八脉.战诀 then
	攻击方.战意点数= 攻击方.战意点数+1
	end
	攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/25)+3
	攻击方.法术状态组[技能名称].伤害=math.floor(self.临时等级)
	攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="定心术" then
	攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/25)+3
	攻击方.法术状态组[技能名称].灵力=math.floor(self.临时等级)
	攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
	elseif 技能名称=="无畏布施" then

		攻击方.法术状态组[技能名称].回合=3
	攻击方.法术状态组[技能名称].法防=math.floor(self.临时等级/2)+50
攻击方.法术状态组[技能名称].防御=math.floor(self.临时等级/2)+50
攻击方.法术状态组[技能名称].伤害=math.floor(self.临时等级/2)+50

	攻击方.法防=攻击方.法防+攻击方.法术状态组[技能名称].法防
攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害

	elseif 技能名称=="金身舍利" then
	攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/25)+3
	攻击方.法术状态组[技能名称].法防=math.floor(self.临时等级)
	攻击方.法防=攻击方.法防-攻击方.法术状态组[技能名称].法防
	elseif 技能名称=="四面埋伏" then
	攻击方.法术状态组[技能名称].回合=4
	攻击方.法术状态组[技能名称].封印命中等级=math.floor(self.临时等级*1.2)
	攻击方.封印命中等级=攻击方.封印命中等级+攻击方.法术状态组[技能名称].封印命中等级
		elseif 技能名称=="无所遁形" then
	攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/25)+2
	攻击方.法术状态组[技能名称].穿刺等级=math.floor(self.临时等级*1.2)
	攻击方.法术状态组[技能名称].物理暴击等级=math.floor(self.临时等级*1.2)
	攻击方.穿刺等级=攻击方.穿刺等级+攻击方.法术状态组[技能名称].穿刺等级
	攻击方.物理暴击等级=攻击方.物理暴击等级+攻击方.法术状态组[技能名称].物理暴击等级
	elseif 技能名称=="潜力激发" then
	攻击方.法术状态组[技能名称].回合=6
	攻击方.法术状态组[技能名称].防御=self.临时等级*1.2
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	攻击方.法术状态组[技能名称].速度=math.floor(self.临时等级/3)+10
	攻击方.速度=攻击方.速度+攻击方.法术状态组[技能名称].速度

	攻击方.法术状态组[技能名称].伤害=self.临时等级*1.7
	攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害

	elseif 技能名称=="变身" then
		攻击方.法术状态组[技能名称].回合=math.floor(self.临时等级/25)+2
		攻击方.法术状态组[技能名称].伤害=math.floor(self.临时等级+10)
		print(攻击方.法术状态组[技能名称].回合,self.临时等级)
	if 挨打方.奇经八脉.宁息 then
		攻击方.法术状态组[技能名称].回合=攻击方.法术状态组[技能名称].回合+2
	end
	攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="逆鳞" then
	攻击方.法术状态组[技能名称].回合=5
	攻击方.法术状态组[技能名称].伤害=math.floor(self.临时等级)
	攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害
	elseif 技能名称=="后发制人" then
	攻击方.法术状态组[技能名称].回合=2
	攻击方.法术状态组[技能名称].防御=math.floor(攻击方.防御*0.5)
	攻击方.防御=攻击方.防御+攻击方.法术状态组[技能名称].防御
	攻击方.法术状态组[技能名称].速度=math.floor(攻击方.速度*0.5)
	攻击方.速度=攻击方.速度+攻击方.法术状态组[技能名称].速度
	攻击方.法术状态组[技能名称].灵力=math.floor(攻击方.灵力*0.5)
	攻击方.灵力=攻击方.灵力+攻击方.法术状态组[技能名称].灵力
	攻击方.法术状态组[技能名称].伤害=math.floor(攻击方.伤害*0.5)
	攻击方.伤害=攻击方.伤害+攻击方.法术状态组[技能名称].伤害
	攻击方.法术状态组[技能名称].目标=目标
	elseif 技能名称=="锋芒毕露" or 技能名称=="诱袭" then
	攻击方.法术状态组[技能名称].回合=1+math.floor(self.临时等级/30)
	攻击方.法术状态组[技能名称].目标=目标
	elseif 技能名称=="横扫千军" then
	攻击方.法术状态组[技能名称].回合=2
	攻击方.法术状态组[技能名称].防御=math.floor(攻击方.防御*0.1)
	攻击方.防御=攻击方.防御-攻击方.法术状态组[技能名称].防御
	攻击方.法术状态组[技能名称].灵力=math.floor(攻击方.灵力*0.1)
	攻击方.灵力=攻击方.灵力-攻击方.法术状态组[技能名称].灵力
	elseif 技能名称=="天崩地裂" then
	攻击方.法术状态组[技能名称].回合=2
	攻击方.法术状态组[技能名称].防御=math.floor(攻击方.防御*0.35)
	攻击方.法术状态组[技能名称].灵力=math.floor(攻击方.灵力*0.35)
 elseif 技能名称=="破釜沉舟" then
	攻击方.法术状态组[技能名称].回合=2
 elseif 技能名称=="血雨" then
	攻击方.法术状态组[技能名称].回合=2
	elseif 技能名称=="鹰击" then
	攻击方.法术状态组[技能名称].回合=2
 elseif 技能名称=="连环击" then
	攻击方.法术状态组[技能名称].回合=2
	end
 end
 --
function FightControl:特技消耗(攻击方,特技名称)
	self.临时消耗=装备特技[特技名称].消耗
	if 攻击方.愤怒特效 then
	self.临时消耗=math.floor(self.临时消耗*0.8)
	end
	if 攻击方.战斗类型~= "角色" then
	return true
	elseif 攻击方.愤怒<self.临时消耗 then
	return false
	else
	攻击方.愤怒=攻击方.愤怒-self.临时消耗
		return true
	end
 end
function FightControl:魔法消耗(攻击方,数值,数量)
 self.临时消耗=math.floor(数值*数量*攻击方.慧根)
 if 攻击方.当前魔法<self.临时消耗 then
	return false
	elseif 攻击方.法宝效果.罗汉珠 and math.random(100)< 攻击方.法宝效果.罗汉珠 then
		return true
	else
	攻击方.当前魔法=攻击方.当前魔法-self.临时消耗
	return true
	end
 end

 
 
function FightControl:恢复血魔(编号,类型,数额)
	数额=self.参战单位[编号].气血回复效果+数额
	if 类型==2 then
	self.参战单位[编号].当前气血=self.参战单位[编号].当前气血+数额
	if self.参战单位[编号].当前气血>self.参战单位[编号].气血上限 then
	self.参战单位[编号].当前气血=self.参战单位[编号].气血上限
	end
	elseif 类型==3 then
	self.参战单位[编号].当前魔法=self.参战单位[编号].当前魔法+数额
	if self.参战单位[编号].当前魔法>self.参战单位[编号].魔法上限 then
	self.参战单位[编号].当前魔法=self.参战单位[编号].魔法上限
	end
	elseif 类型==4 then
	self.参战单位[编号].气血上限=self.参战单位[编号].气血上限+数额
	if self.参战单位[编号].气血上限>self.参战单位[编号].最大气血 then
	self.参战单位[编号].气血上限=self.参战单位[编号].最大气血
	end

	end
 end
 --
function FightControl:技能消耗(攻击方,数量)
	if 数量==nil then 
		数量=1 
	end
	self.技能名称=攻击方.命令数据.参数
	if 攻击方.队伍id==0 then return true end
		if (self.技能名称=="横扫千军" or self.技能名称=="破釜沉舟" )and 攻击方.当前气血>=攻击方.最大气血*0.5 then
			return true
		elseif self.技能名称=="天命剑法" and 攻击方.当前气血>=攻击方.最大气血*0.7 then
			return true
	elseif self.技能名称=="由己渡人" and 攻击方.当前气血>攻击方.最大气血*0.1 then
		return true
	elseif self.技能名称=="移魂化骨" and 攻击方.当前气血>=攻击方.最大气血*0.3 then
		return true
	elseif self.技能名称=="烟雨剑法" and 攻击方.当前气血>=攻击方.最大气血*0.7 then
		return self:魔法消耗(攻击方,50,1)
	elseif (self.技能名称=="后发制人" or self.技能名称=="剑荡四方") and 攻击方.当前气血>=攻击方.最大气血*0.1 then
		return true
	elseif (self.技能名称=="炼气化神"or self.技能名称=="三花聚顶"or self.技能名称=="魔息术" or self.技能名称 == "乾天罡气" or self.技能名称 == "移星换斗")and 攻击方.当前气血>30 then
	攻击方.当前气血=攻击方.当前气血-30
		return true
	elseif self.技能名称=="断岳势" then
		return self:魔法消耗(攻击方,50,1)
	elseif self.技能名称=="天崩地裂" then
		return self:魔法消耗(攻击方,50,1)
	elseif self.技能名称=="浪涌" then
		if 攻击方.战意点数 < 6 then
			攻击方.战意点数	= 攻击方.战意点数 + 1
		end
		return self:魔法消耗(攻击方,30,数量)
	elseif self.技能名称=="惊涛怒" or self.技能名称=="流沙轻音" or self.技能名称=="叱咤风云" or self.技能名称=="食指大动"then
		return self:魔法消耗(攻击方,30,数量)
	elseif self.技能名称=="翻江搅海" then
		return self:魔法消耗(攻击方,20,数量)
	elseif self.技能名称=="腾雷" then
		return self:魔法消耗(攻击方,60,数量)
	elseif self.技能名称=="镇魂诀" and 攻击方.当前气血>攻击方.气血上限*0.05	then
	--攻击方.当前气血=math.floor(攻击方.当前气血*0.95)
		return true
	elseif self.技能名称=="地涌金莲" and 攻击方.当前气血>攻击方.气血上限*0.05	then
	--攻击方.当前气血=math.floor(攻击方.当前气血*0.97)
		return self:魔法消耗(攻击方,50,1)
	elseif self.技能名称=="血雨" and 攻击方.当前气血>=攻击方.气血上限*0.5 then
		--攻击方.当前气血=math.floor(攻击方.当前气血*0.8)
		return true
	elseif self.技能名称=="我佛慈悲" then 
		if 攻击方.神器=="挥毫" then 
			return self:魔法消耗(攻击方,300,1)
		else 
			return self:魔法消耗(攻击方,150,1)
		end
	elseif	self.技能名称=="救死扶伤"or self.技能名称=="舍生取义"or self.技能名称=="还阳术"or
	self.技能名称=="佛法无边"or self.技能名称=="杨柳甘露" or self.技能名称=="还魂咒"or self.技能名称=="修罗隐身"or self.技能名称=="不动如山"	then
		return self:魔法消耗(攻击方,150,1)
	elseif self.技能名称=="金身舍利"or self.技能名称=="神龙摆尾"or self.技能名称=="摧心术"or self.技能名称=="明光宝烛"
 or self.技能名称=="上古灵符" or self.技能名称=="威震凌霄"or self.技能名称=="哼哼哈兮"or self.技能名称=="无畏布施" or self.技能名称=="气慑天军"or self.技能名称=="惊心一剑" or self.技能名称=="月光" or self.技能名称=="唤魔·毒魅" or self.技能名称=="匠心·削铁" or self.技能名称=="匠心·固甲" or self.技能名称=="唤灵·焚魂" or self.技能名称=="天魔觉醒"then
		return self:魔法消耗(攻击方,100,1)
	elseif self.技能名称=="分身术" or self.技能名称=="妙手回春"or self.技能名称=="知己知彼"or self.技能名称=="颠倒五行"or self.技能名称=="匠心·蓄锐"then
		return self:魔法消耗(攻击方,80,1)
	elseif self.技能名称=="活血"	then
		local expendMP= 攻击方.奇经八脉.仁心 and 170 or 70
		if 攻击方.神器=="挥毫" then
			expendMP =expendMP*2
		end
		return self:魔法消耗(攻击方,expendMP,1)
	elseif self.技能名称=="二龙戏珠" or self.技能名称=="诱袭" then
	return self:魔法消耗(攻击方,70,1)
	elseif self.技能名称=="云暗天昏" or self.技能名称=="无所遁形" or self.技能名称=="铜头铁臂" or self.技能名称=="当头一棒" or self.技能名称=="神针撼海" or self.技能名称=="杀威铁棒" or self.技能名称=="泼天乱棒"then
	return self:魔法消耗(攻击方,75,1) 
	elseif self.技能名称=="失心符"or self.技能名称=="失魂符"or self.技能名称=="飞花摘叶"or self.技能名称=="一笑倾城"or
	self.技能名称=="定身符"or self.技能名称=="错乱"or self.技能名称=="百万神兵"or self.技能名称=="解封"or
	self.技能名称=="火甲术"or self.技能名称=="复苏"or self.技能名称=="无穷妙道" or self.技能名称=="莲花心音" then
	return self:魔法消耗(攻击方,60,1)
	elseif self.技能名称=="追魂符"then
	return self:魔法消耗(攻击方,55,1)
	elseif self.技能名称=="失忆符"or self.技能名称=="似玉生香"or self.技能名称=="离魂符"or self.技能名称=="凝神术"or
	self.技能名称=="推拿"or self.技能名称=="天神护体"or self.技能名称=="五雷轰顶"or self.技能名称=="掌心雷"or
	self.技能名称=="天地同寿"or self.技能名称=="乾坤妙法"or self.技能名称=="紧箍咒"or self.技能名称=="苍茫树"or
	self.技能名称=="地裂火" or self.技能名称=="日光华" or self.技能名称=="巨岩破" or self.技能名称=="靛沧海"or
	self.技能名称=="寡欲令"or self.技能名称=="黄泉之息"or self.技能名称=="锢魂术"or self.技能名称=="摇头摆尾"or
	self.技能名称=="象形" or self.技能名称=="姐妹同心"or self.技能名称=="幻镜术"or self.技能名称=="裂石"or self.技能名称=="四面埋伏" or
	self.技能名称=="煞气诀"or self.技能名称=="惊魂掌"or self.技能名称=="雾杀"or self.技能名称=="星月之惠" or	self.技能名称=="潜力激发" or
	self.技能名称=="金刚镯" or self.技能名称=="壁垒击破" or self.技能名称=="八戒上身" or self.技能名称=="治疗" or self.技能名称=="蚩尤之搏" or self.技能名称=="翩鸿一击" or self.技能名称=="长驱直入" then
	return self:魔法消耗(攻击方,50,1)
	elseif self.技能名称=="催眠符" or self.技能名称=="镇妖"or self.技能名称=="驱魔"or self.技能名称=="含情脉脉" then
	return self:魔法消耗(攻击方,45,1)
	elseif self.技能名称=="杀气诀"or self.技能名称=="满天花雨"or self.技能名称=="落魄符"or self.技能名称=="金刚护法"or
	self.技能名称=="解毒"or self.技能名称=="莲步轻舞"or self.技能名称=="娉婷嬝娜"or self.技能名称=="逆鳞"or
	self.技能名称=="乘风破浪"or self.技能名称=="驱尸" or self.技能名称=="尸腐毒"or self.技能名称=="魂飞魄散"or
	self.技能名称=="无敌牛虱" or self.技能名称=="无敌牛妖"or self.技能名称=="定心术"or self.技能名称=="勾魂"or
	self.技能名称=="魔音摄魂"or self.技能名称=="盘丝阵" then
	return self:魔法消耗(攻击方,40,1)
	elseif self.技能名称=="韦陀护法"or self.技能名称=="如花解语"or self.技能名称=="日月乾坤"or self.技能名称=="摄魄"then
	return self:魔法消耗(攻击方,35,1)
	elseif self.技能名称=="反间之计"or self.技能名称=="红袖添香"or self.技能名称=="安神诀"or self.技能名称=="五雷咒"or
	self.技能名称=="达摩护体"or self.技能名称=="金刚护体"or self.技能名称=="百毒不侵"or self.技能名称=="一苇渡江"or
	self.技能名称=="楚楚可怜"or self.技能名称=="天神护法"or self.技能名称=="宁心"or self.技能名称=="天雷斩"or
	self.技能名称=="清心"or self.技能名称=="龙腾"or self.技能名称=="龙吟"or self.技能名称=="灵动九天"or
	self.技能名称=="自在心法"or self.技能名称=="魔王回首"or self.技能名称=="极度疯狂"or self.技能名称=="天魔解体"or
	self.技能名称=="碎星诀"or self.技能名称=="夺魄令"or self.技能名称=="荆棘舞"or self.技能名称=="尘土刃" or
	self.技能名称=="冰川怒"or self.技能名称=="炎护" or	self.技能名称=="死亡召唤" or	self.技能名称=="唤灵·魂火" or	self.技能名称=="呼子唤孙" or	self.技能名称=="炽火流离" or	self.技能名称=="怨怖之泣"or	self.技能名称=="谜毒之缚" then
	return self:魔法消耗(攻击方,30,1)

	elseif self.技能名称=="推气过宫"	then
	local expendMP= 攻击方.奇经八脉.仁心 and 180 or 30
	if 攻击方.神器=="挥毫" then
		expendMP =expendMP*2
	end
	return self:魔法消耗(攻击方,expendMP,1)
	elseif self.技能名称=="归元咒" or self.技能名称=="幽冥鬼眼"or self.技能名称=="牛劲"or
	self.技能名称=="威慑" or self.技能名称=="变身" or self.技能名称=="仙人指路"then
	return self:魔法消耗(攻击方,20,1)
	elseif	self.技能名称=="判官令" then
	if 攻击方.奇经八脉.判官 then
		return self:魔法消耗(攻击方,30,1)
	else
		return self:魔法消耗(攻击方,20,1)
	end
	elseif self.技能名称=="碎甲符" or self.技能名称=="普渡众生"or self.技能名称=="大闹天宫" or self.技能名称=="水攻" or
	self.技能名称=="落岩" or self.技能名称=="烈火" or self.技能名称=="雷击" or self.技能名称=="锋芒毕露" or self.技能名称=="针锋相对" or self.技能名称=="净世煌火" or self.技能名称=="焚魔烈焰" or self.技能名称=="唤魔·堕羽"	then
	return self:魔法消耗(攻击方,50,数量)
	elseif	self.技能名称=="九天玄火" then
	return self:魔法消耗(攻击方,45,1)
	elseif	self.技能名称=="雷霆万钧" or self.技能名称=="百爪狂杀" then
	return self:魔法消耗(攻击方,35,数量)
	elseif self.技能名称=="落雷符"or self.技能名称=="生命之泉"or self.技能名称=="飘渺式"or self.技能名称=="飞砂走石" or
	self.技能名称=="三昧真火"  or  self.技能名称=="鹰击" or self.技能名称=="狮搏"or self.技能名称=="连环击"or
	self.技能名称=="天罗地网"or self.技能名称=="瘴气"or self.技能名称=="落叶萧萧"or self.技能名称=="泰山压顶" or
	self.技能名称=="奔雷咒" or self.技能名称=="地狱烈火" or self.技能名称=="神来气旺" or self.技能名称=="水漫金山"or self.技能名称=="破击" or self.技能名称=="法术防御"or self.技能名称=="天降灵葫" or self.技能名称=="八凶法阵"then
	return self:魔法消耗(攻击方,30,数量)
	elseif self.技能名称=="蜜润"	then
	return self:魔法消耗(攻击方,25,数量)
	elseif self.技能名称=="唧唧歪歪"or self.技能名称=="龙卷雨击"or self.技能名称=="阎罗令"or self.技能名称=="夺命咒"then
	return self:魔法消耗(攻击方,20,数量)
	elseif self.技能名称=="扶摇万里"	then
	return self:魔法消耗(攻击方,12,数量)
	elseif self.技能名称=="雨落寒沙"	then
	return self:魔法消耗(攻击方,10,数量)
	elseif self.技能名称=="善恶有报" or	self.技能名称=="夜舞倾城" or	self.技能名称=="波澜不惊"	then
	return self:魔法消耗(攻击方,80,数量)
	elseif self.技能名称=="力劈华山" then
	return self:魔法消耗(攻击方,80,数量)
	elseif self.技能名称=="进阶力劈华山" or self.技能名称=="进阶善恶有报" or	self.技能名称=="牛刀小试" then
	return self:魔法消耗(攻击方,5,数量)
	elseif self.技能名称=="九幽" or self.技能名称=="金钱镖" or self.技能名称=="落雨金钱" or self.技能名称=="峰回路转" then
	return true
	else
	return false
	end
 end
---
function FightControl:取复活状态(编号)
	if	self.参战单位[编号].法术状态组.死亡召唤 or self.参战单位[编号].法术状态组.锢魂术 then
		return true
	else
		return false
	end
end
function FightControl:取技能目标数(攻击方,技能名称)
 if self.战斗开始==false then return 1 end --附加状态的人数
 if 装备特技[技能名称]==nil then
	self.临时等级=self:取技能等级(攻击方,技能名称)
 end
 self.临时人数=1
 if 技能名称=="推气过宫" or 技能名称=="幽冥鬼眼"	then

	self.临时人数=math.floor(self.临时等级/30)+1
	if (攻击方.名称=="胖和尚" or 攻击方.名称=="蜘蛛女王" ) and 攻击方.队伍==0 then
		self.临时人数=10
	end
	if ( 攻击方.名称=="香炉精" or 攻击方.名称=="噬天虎7"or 攻击方.名称=="鬼将6") and 攻击方.队伍==0 then
		self.临时人数=10
	end


	elseif 技能名称=="匠心·蓄锐" or 技能名称=="匠心·固甲" or 技能名称=="匠心·削铁" or 技能名称=="锋芒毕露" or 技能名称=="针锋相对" then 
		self.临时人数=攻击方.攻之械 >= 3 and math.min(math.floor(self.临时等级/30)+1, 5) or 1
		if 技能名称=="匠心·蓄锐" or 技能名称=="匠心·固甲" or 技能名称=="匠心·削铁" then
			if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
				local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1下限"]
				local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1上限"]
				if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
					self.临时人数 = self.临时人数 + 取随机数(元神加成下限,元神加成上限)
				end
			end
		end
	elseif 技能名称=="峰回路转" then
	self.临时人数=10
	elseif 技能名称=="鹰击" then
		self.临时人数=math.floor(self.临时等级/30)+1
		if 攻击方.奇经八脉.翼展 then
			self.临时人数=self.临时人数+1
		end
		if 攻击方.神器 =="蛮血" then
			self.临时人数=self.临时人数+3
		end
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果3下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果3上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时人数 = self.临时人数 + 取随机数(元神加成下限,元神加成上限)
			end
		end
	elseif 技能名称=="翻江搅海"	then
		self.临时人数=math.floor(self.临时等级/30)+2
		
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果3下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果3上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时人数 = self.临时人数 + 取随机数(元神加成下限,元神加成上限)
			end
		end
	elseif 技能名称=="狮搏"	then
	if 攻击方.奇经八脉.狮吼 then
		self.临时人数=2
	else
		self.临时人数=1
	end
	elseif 技能名称=="五雷咒" then
	self.临时人数=1
	if 攻击方.奇经八脉.奔雷 then
		self.临时人数=self.临时人数+1
	end
	elseif 技能名称=="落雷符" then
		self.临时人数=math.floor(self.临时等级/30)+1
		if self.临时人数>3 then 
			self.临时人数=3 
		end
		if self.pk战斗 then
			self.临时人数=1
		end
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时人数 = self.临时人数 + 取随机数(元神加成下限,元神加成上限)
			end
		end
	elseif 技能名称=="金刚护法" then

	self.临时人数=math.floor((self.临时等级-40)/30)+2

	if 攻击方.奇经八脉.映法 and math.random(100)<= 50 then
		self.临时人数 = 10
	end
	elseif 技能名称=="镇魂诀" then

	self.临时人数=5


	elseif 技能名称=="碎星诀" or 技能名称=="法术防御" then

	self.临时人数=1

	elseif 技能名称=="普渡众生"	then
		if 攻击方.法宝效果.普渡 then
			if math.random(100)<攻击方.法宝效果.普渡*3 then
				self.临时人数=2
			else
				self.临时人数=1
			end
		else
			self.临时人数=1
		end
		if 攻击方.奇经八脉.道衍 then
			self.临时人数=self.临时人数+2
		end


	elseif 技能名称=="地涌金莲" then
		self.临时人数=3
		if 攻击方.奇经八脉.金莲 then
			self.临时人数=self.临时人数+1
		end
		if 攻击方.神器 =="相思" then
			self.临时人数=self.临时人数+3
		end
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时人数 = self.临时人数 + 取随机数(元神加成下限,元神加成上限)
			end
		end
	elseif 技能名称=="金刚护体" or 技能名称=="一苇渡江"	then
		self.临时人数=math.floor((self.临时等级-50)/30)+2
		if 攻击方.奇经八脉.映法 and math.random(100)<= 50 then
			self.临时人数 = 10
		end
	elseif	技能名称=="不动如山" or 技能名称=="灵动九天" or 技能名称=="明光宝烛"	then
	self.临时人数=math.floor((self.临时等级-50)/30)+2
		if (攻击方.名称=="胖和尚" ) and 攻击方.队伍==0 then
		self.临时人数=10
	end
	elseif 技能名称=="蜜润"	then 
	self.临时人数=1
	if 攻击方.神器 == "钟灵" then
		self.临时人数=6
	end
	elseif	技能名称=="九幽"	then

	self.临时人数=10

	elseif 技能名称=="飞花摘叶" or 技能名称=="雨落寒沙" or 技能名称=="谜毒之缚" or 技能名称=="怨怖之泣" or 技能名称=="红袖添香" or 技能名称=="唧唧歪歪" or 技能名称=="龙卷雨击" or 技能名称=="阎罗令" or 技能名称=="生命之泉" then

		self.临时人数=math.floor(self.临时等级/25)+1
		if 技能名称=="龙卷雨击" then
			if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
				local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果3下限"]
				local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果3上限"]
				if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
					self.临时人数 = self.临时人数 + 取随机数(元神加成下限,元神加成上限)
				end
				local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2下限"]
				local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2上限"]
				if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
					if 攻击方.法连 == nil then
						攻击方.法连 = 0
					end
					攻击方.法连 = 攻击方.法连 + 取随机数(元神加成下限,元神加成上限)
				end
			end
		end
		if	(self.战斗类型==200001 or self.战斗类型==200002 or self.战斗类型==200003 or self.战斗类型==200004 or self.战斗类型==200005 or self.战斗类型==200006 or self.战斗类型==200008) and	(技能名称=="雨落寒沙" or 技能名称=="阎罗令") then
			self.临时人数=1
		end
	elseif 技能名称=="一笑倾城" then
	self.临时人数=math.random(4)
	elseif 技能名称=="焚魔烈焰"	then
	self.临时人数=3
		if 攻击方.神器=="惊梦" then
		self.临时人数=5
	end
	elseif 技能名称=="飞砂走石"	then

	self.临时人数=math.floor(self.临时等级/35)+1

	if 攻击方.奇经八脉.震怒 and math.random(100)<= 20 then
		self.临时人数=self.临时人数+1
	end
	if 攻击方.神器=="流火" and math.random(100)<= 30 then
		self.临时人数=self.临时人数+3
	end
	elseif 技能名称=="落叶萧萧"	then

		self.临时人数=math.floor(self.临时等级/35)+1
		if 攻击方.奇经八脉.法身 and math.random(100)<50 then
			self.临时人数=self.临时人数+1
		end
		if 攻击方.神器=="凭虚御风" then
			self.临时人数=self.临时人数+2
		end
		
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时人数 = self.临时人数 + 取随机数(元神加成下限,元神加成上限)
			end
		end
	elseif 技能名称=="天降灵葫"	then

	self.临时人数=4 

	
		
	elseif 技能名称=="苍茫树" or 技能名称=="地裂火" or 技能名称=="靛沧海" or 技能名称=="日光华" or 技能名称=="巨岩破" or 技能名称=="夺命咒"	then

	self.临时人数=math.floor(self.临时等级/40)+2
		if	(self.战斗类型==200001 or self.战斗类型==200002 or self.战斗类型==200003 or self.战斗类型==200004 or self.战斗类型==200005 or self.战斗类型==200006 or self.战斗类型==200008) then
		self.临时人数=1
		end
	elseif 技能名称=="天雷斩" or 技能名称=="天罗地网"	then

	self.临时人数=math.floor(self.临时等级/30)+1
	if self.临时人数>3 then self.临时人数=3 end
	if 技能名称=="天罗地网" and	攻击方.神器 =="澄明" then
		self.临时人数=self.临时人数+4
	end
	
		if	(self.战斗类型==200001 or self.战斗类型==200002 or self.战斗类型==200003 or self.战斗类型==200004 or self.战斗类型==200005 or self.战斗类型==200006 or self.战斗类型==200008) and 技能名称=="天罗地网" then
		self.临时人数=1
		end
	elseif 技能名称=="破釜沉舟" or 技能名称=="雷霆万钧" or 技能名称=="剑荡四方" or 技能名称=="破击" then
		self.临时人数=3
		if 技能名称 =="雷霆万钧" and 攻击方.神器 == "弦外之音" then
			self.临时人数 =6
		elseif 技能名称 =="破釜沉舟" and 攻击方.神器 == "藏锋敛锐" then 
			self.临时人数 =7 
		end
		if 技能名称 == "雷霆万钧" then
			if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
				local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2下限"]
				local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2上限"]
				if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
					self.临时人数 = self.临时人数 + 取随机数(元神加成下限,元神加成上限)
				end
				local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果3下限"]
				local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果3上限"]
				if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
					if 攻击方.法连 == nil then
						攻击方.法连 = 0
					end
					攻击方.法连 = 攻击方.法连 + 取随机数(元神加成下限,元神加成上限)
				end
			end
		end
		if 技能名称 =="破釜沉舟" then
			if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
				local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果4下限"]
				local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果4上限"]
				if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
					self.临时人数 = self.临时人数 + 取随机数(元神加成下限,元神加成上限)
				end
			end
		end
	elseif 技能名称=="二龙戏珠" or 技能名称=="摧心术"	then
	self.临时人数=2
	elseif 技能名称=="苍鸾怒击"	then
	self.临时人数=2
	elseif 技能名称=="飘渺式"	then
		self.临时人数=math.floor(self.临时等级/30)+1
		if self.临时人数>3 then 
			self.临时人数=3 
		end
		if self.pk战斗 then
			self.临时人数=1
		end
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时人数 = self.临时人数 + 取随机数(元神加成下限,元神加成上限)
			end
		end
 elseif 技能名称=="浪涌"	then
	self.临时人数=math.floor(self.临时等级/30)+1
	if self.临时人数>3 then self.临时人数=3 end
	if self.pk战斗 then
	self.临时人数=1
	end
	elseif 技能名称=="神针撼海"	then
		self.临时人数=math.min(math.floor(self.临时等级/35)+1,5) 
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时人数 = self.临时人数 + 取随机数(元神加成下限,元神加成上限)/100
			end
		end
 elseif 技能名称=="断岳势"	then
	self.临时人数=math.floor(self.临时等级/30)+1
	if self.临时人数>3 then
	self.临时人数=3
	end
	if self.pk战斗 then
	self.临时人数=1
	end
	elseif 技能名称=="腾雷"	then
	self.临时人数=1
	if self.pk战斗 then
	self.临时人数=1
	end
	elseif 技能名称=="百爪狂杀"	then
	self.临时人数=4
	elseif 技能名称=="惊涛怒"	then
	self.临时人数=math.floor(self.临时等级/30)+1
	if self.临时人数>3 then
	self.临时人数=3
	end
	elseif 技能名称=="云暗天昏"	then
	self.临时人数=math.floor(self.临时等级/35)+2
	elseif 技能名称=="连环击"	then
	self.临时人数=math.floor(self.临时等级/35)+2
	if 攻击方.奇经八脉.乱击 then
		self.临时人数=self.临时人数+1
	end
	elseif 技能名称=="龙吟" or 技能名称=="四海升平" or 技能名称=="玉清诀" or 技能名称=="晶清诀" or 技能名称=="魔兽之印" or 技能名称=="圣灵之甲" or 技能名称=="罗汉金钟"	then
	self.临时人数=10

	elseif 技能名称=="食指大动" then 
		self.临时人数=5

		elseif 技能名称=="神来气旺" then 
		self.临时人数=6

		elseif 技能名称=="进阶力劈华山" then 
		self.临时人数=4

		elseif 技能名称=="进阶善恶有报" then 
		self.临时人数=4
	
		elseif 技能名称=="扶摇万里" then
	self.临时人数=math.min(math.floor(self.临时等级/15)+1,10)
	elseif 技能名称=="流沙轻音" then
	self.临时人数=math.min(math.floor(self.临时等级/20)+1,5)
	elseif 技能名称=="泰山压顶" or 技能名称=="水漫金山" or 技能名称=="地狱烈火" or 技能名称=="奔雷咒" or 技能名称=="八凶法阵"or 技能名称=="叱咤风云"	then
	self.临时人数=math.floor(self.临时等级/30)+1
	if self.临时人数>3 then self.临时人数=3 end
	else
	self.临时人数=1
	end
 return self.临时人数
 end
function FightControl:取技能伤害(攻击方,挨打方,技能名称,人数)---------------------------------------完成
	self.临时伤害=0
	self.天地同寿=false
	if 技能名称=="水攻" or 技能名称=="落岩" or 技能名称=="烈火" or 技能名称=="雷击"	then
		self.临时伤害=self:取灵力差(攻击方,挨打方)*0.9+20+攻击方.等级*3		---等级*3+灵力差*1.2+20

	
	elseif 技能名称=="上古灵符" then
	self.临时伤害=攻击方.等级*1.6+攻击方.固定伤害+self:取灵力差(攻击方,挨打方)*0.3
		elseif 技能名称=="月光" then
	self.临时伤害=self:取灵力差(攻击方,挨打方)*0.9+20+攻击方.等级*2
	elseif 技能名称=="荆棘舞" then
	self.临时伤害=(self:取技能等级(攻击方,技能名称)*2+self:取灵力差(攻击方,挨打方)*1.25+攻击方.武器伤害*0.315+28.5)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)*(math.random(90,105)/100)
	self.天地同寿=true
	elseif 技能名称=="尘土刃" then
	self.临时伤害=(self:取技能等级(攻击方,技能名称)*2+self:取灵力差(攻击方,挨打方)*1.25+攻击方.武器伤害*0.315+28.5)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)*(math.random(90,105)/100)
	self.天地同寿=true
	elseif 技能名称=="冰川怒" then
	self.临时伤害=(self:取技能等级(攻击方,技能名称)*2+self:取灵力差(攻击方,挨打方)*1.25+攻击方.武器伤害*0.315+28.5)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)*(math.random(90,105)/100)
	self.天地同寿=true
	elseif 技能名称=="三昧真火" then
		self.临时伤害=(self:取技能等级(攻击方,技能名称)*2.5+self:取灵力差(攻击方,挨打方)+攻击方.武器伤害*0.315+28.5)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)*(math.random(90,105)/100)
		self.天地同寿=true
		if 攻击方.奇经八脉.魔心 and math.random(100) <=10 then
			self.临时伤害=self.临时伤害*2
		end
		if 攻击方.神器=="业焰明光" and math.random(100) <=50 then
			self.临时伤害=self.临时伤害*1.4
		end
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限)/100+1)
			end
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				if 攻击方.法连 == nil then
					攻击方.法连 = 0
				end
				攻击方.法连 = 攻击方.法连 + 取随机数(元神加成下限,元神加成上限)
			end
		end
	elseif 技能名称=="归元咒" then
	self.临时伤害=self:取技能等级(攻击方,技能名称)+30
	elseif 技能名称=="净世煌火" then
	self.临时伤害=self:取技能等级(攻击方,技能名称)*7+攻击方.灵力*0.25+30
	elseif 技能名称=="三花聚顶" or 技能名称 == "乾天罡气" or 技能名称 == "移星换斗" then
	self.临时伤害=self:取技能等级(攻击方,技能名称)
 elseif 技能名称=="天魔解体" then
	if 挨打方.队伍==0 or 挨打方.队伍==nil then
	self.临时伤害=1
	else
		if self:取技能等级(攻击方,技能名称)+5>=挨打方.等级 and math.random(100)<=30 then
		self.临时伤害=(攻击方.当前气血*0.85)
		else
		self.临时伤害=1
		end
	end
	elseif 技能名称=="五雷轰顶" then
	self.临时伤害=挨打方.当前气血*(math.random(2,20)/100)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)
	if self.临时伤害>攻击方.等级*攻击方.等级*0.5 then
		self.临时伤害=攻击方.等级*攻击方.等级*0.75
	end
	elseif 技能名称=="五雷咒" then
	self.临时伤害=(self:取技能等级(攻击方,技能名称)*1.5+self:取灵力差(攻击方,挨打方)*1.05+攻击方.武器伤害*0.3+22.5)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)
	elseif 技能名称=="落雷符" then
		self.临时伤害=(self:取技能等级(攻击方,技能名称)*2.5+self:取灵力差(攻击方,挨打方)+攻击方.武器伤害*0.3+22.5)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)*(10-人数+1)/10
		if 攻击方.奇经八脉.雷动 then
			self.临时伤害=self.临时伤害+100
		end
		if 攻击方.石破天惊 then
			self.临时伤害=self.临时伤害+攻击方.石破天惊
		end
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限)/100+1)
			end
		end
	elseif 技能名称=="自在心法" then
		self.临时伤害 =self:取技能等级(攻击方,技能名称)*2+挨打方.法术状态组["普渡众生"].伤害*挨打方.法术状态组["普渡众生"].回合
		挨打方.法术状态组["普渡众生"]=nil
	elseif 技能名称=="判官令" then
	self.临时伤害=(self:取技能等级(攻击方,技能名称)*3+75)+攻击方.固定伤害+攻击方.敏捷*0.7
	挨打方.当前魔法=挨打方.当前魔法-math.floor(self:取技能等级(攻击方,技能名称)*1.5)
	if 挨打方.当前魔法<0 then 挨打方.当前魔法=0 end

	if 攻击方.奇经八脉.判官 then
		self.临时伤害=self.临时伤害*1.3
	end
	if 攻击方.雷绝阵 then
		self.临时伤害= math.floor(self.临时伤害*1.2)
	end
	if	昼夜参数==1 then
		self.临时伤害= math.floor(self.临时伤害*1.3)
		if 攻击方.神器 =="亡灵泣语" then
		self.临时伤害=self.临时伤害*1.5
		end
	end
		if 挨打方.队伍~=0 then
		self.临时伤害=self.临时伤害*0.5
	end
	elseif 技能名称=="勾魂" then
	self.临时伤害=(self:取技能等级(攻击方,技能名称)*4+挨打方.当前气血*0.05)
	elseif 技能名称=="炽火流离" then
	self.临时伤害=(self:取技能等级(攻击方,技能名称)*1.2+self:取灵力差(攻击方,挨打方)*1+攻击方.武器伤害*0.3+22.5)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)
	elseif 技能名称=="飞花摘叶" then
		if self.pk战斗 then
			self.临时伤害=1
		else
			self.临时伤害=(self:取技能等级(攻击方,技能名称)*2)*1.5+攻击方.固定伤害
		end
		self.天地同寿=true
	elseif 技能名称=="血雨" then
	self.临时伤害=(self:取技能等级(攻击方,技能名称)*8+75)+攻击方.固定伤害+(攻击方.当前气血*0.2)+攻击方.敏捷*0.7
	elseif 技能名称=="雨落寒沙" then
		self.临时伤害=(self:取技能等级(攻击方,技能名称)*8+攻击方.固定伤害+攻击方.敏捷*0.7+100)*1.3+攻击方.武器伤害*0.24+80+攻击方.修炼数据.法术*20
		-- 攻击方.毒=20
		if 攻击方.奇经八脉.暗伤 then
			self.临时伤害=self.临时伤害+攻击方.武器伤害*0.18
		end
		if 攻击方.神器 =="泪光盈盈" then
			self.临时伤害=self.临时伤害*1.2
		end
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限)/100+1)
			end
		end
	if 挨打方.队伍~=0 then
		self.临时伤害=self.临时伤害*0.5
	end
		if 攻击方.雷绝阵 then
		self.临时伤害= math.floor(self.临时伤害*1.2)
	end
	elseif 技能名称=="摄魄" then
	self.临时伤害=(self:取技能等级(攻击方,技能名称)*3+挨打方.当前魔法*0.05)
	elseif 技能名称=="天罗地网" then
		self.临时伤害=(self:取技能等级(攻击方,技能名称)*8+攻击方.伤害/3)+攻击方.固定伤害+攻击方.敏捷*1.1+攻击方.武器伤害*0.24+80+攻击方.修炼数据.法术*20
		if 攻击方.网罗乾坤 then
			self.临时伤害=self.临时伤害+攻击方.网罗乾坤
		end
		if 攻击方.奇经八脉.粘附 then
			self.临时伤害=self.临时伤害+攻击方.武器伤害*0.18
		end
		if 攻击方.神器 =="澄明" then
			self.临时伤害=self.临时伤害*1.15
		end
		if 挨打方.队伍~=0 then
			self.临时伤害=self.临时伤害*0.5
		end
		if 攻击方.雷绝阵 then
			self.临时伤害= math.floor(self.临时伤害*1.2)
		end
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限)/100+1)
			end
		end
	elseif 技能名称=="苍茫树" or 技能名称=="地裂火" or 技能名称=="靛沧海" or 技能名称=="日光华" or 技能名称=="巨岩破"	then
	self.临时伤害=(self:取技能等级(攻击方,技能名称)*8+攻击方.固定伤害+攻击方.敏捷*0.7+100)*1.3+攻击方.武器伤害*0.24+80+攻击方.修炼数据.法术*20
	if 攻击方.行云流水 then
		self.临时伤害=self.临时伤害+攻击方.行云流水
	end
	if 攻击方.奇经八脉.借灵 then
			self.临时伤害=self.临时伤害+攻击方.武器伤害*0.24
	end
	if 攻击方.法宝效果.金刚杵 then
		self.临时伤害= self.临时伤害+攻击方.法宝效果.金刚杵
	end
	if 攻击方.奇经八脉.推衍 and 攻击方.敏捷 >= 攻击方.等级*2.2 then
	self.临时伤害= self.临时伤害*1.22
	end
	if 攻击方.雷绝阵 then
		self.临时伤害= math.floor(self.临时伤害*1.2)
	end
		if 攻击方.神器=="璇华" then
		self.临时伤害= math.floor(self.临时伤害*1.3)
	end
	if 挨打方.队伍~=0 then
		self.临时伤害=self.临时伤害*0.5
	end
	elseif	技能名称=="夺命咒"	then
	self.临时伤害=self:取技能等级(攻击方,技能名称)*7+攻击方.固定伤害+攻击方.敏捷*0.7+攻击方.武器伤害*0.24+80+攻击方.修炼数据.法术*20
	if 攻击方.奇经八脉.追魂 and 攻击方.敏捷 >=	攻击方.等级*2.2 then
		self.临时伤害=self:取技能等级(攻击方,"移魂化骨")+self.临时伤害
	end

	if 挨打方.队伍~=0 then
		self.临时伤害=self.临时伤害*0.5
	end
		if 攻击方.雷绝阵 then
		self.临时伤害= math.floor(self.临时伤害*1.2)
	end
	elseif	技能名称=="破击"	then
	self.临时伤害=self:取技能等级(攻击方,技能名称)*9+攻击方.固定伤害+攻击方.敏捷*0.7+攻击方.武器伤害*0.24+80+攻击方.修炼数据.法术*20
		if 挨打方.队伍~=0 then
			self.临时伤害=self.临时伤害*0.5
		end
		if 攻击方.雷绝阵 then
		self.临时伤害= math.floor(self.临时伤害*1.2)
	end
	elseif	技能名称=="云暗天昏"	then
	self.临时伤害=self:取技能等级(攻击方,技能名称)*6+攻击方.固定伤害+攻击方.敏捷*0.7+攻击方.武器伤害*0.24+80+攻击方.修炼数据.法术*20
		if 挨打方.队伍~=0 then
			self.临时伤害=self.临时伤害*0.5
		end
		if 攻击方.雷绝阵 then
		self.临时伤害= math.floor(self.临时伤害*1.2)
	end
	elseif 技能名称=="阎罗令" then
	self.临时伤害=self:取技能等级(攻击方,技能名称)*7+攻击方.固定伤害+攻击方.敏捷*0.7+攻击方.武器伤害*0.24+80+攻击方.修炼数据.法术*20
	if 攻击方.索命无常 then
	self.临时伤害=self.临时伤害+攻击方.索命无常
	end
	if 攻击方.雷绝阵 then
		self.临时伤害= math.floor(self.临时伤害*1.2)
	end
		if	昼夜参数==1 then
		self.临时伤害= math.floor(self.临时伤害*1.3)
		if 攻击方.神器 =="亡灵泣语" then
		self.临时伤害=self.临时伤害*1.5
		end
	end
		if 挨打方.队伍~=0 then
		self.临时伤害=self.临时伤害*0.5
	end
	elseif 技能名称=="紧箍咒" or 技能名称=="尸腐毒" or 技能名称=="雾杀"	then
		self.临时伤害=self:取技能等级(攻击方,技能名称)+10+攻击方.固定伤害+攻击方.敏捷*0.7
		if 技能名称=="尸腐毒" then
			if 攻击方.神器=="魂魇" and 攻击方.当前气血 < 攻击方.气血上限*0.3 then
				self.临时伤害=self.临时伤害*1.5
			end
			if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
				local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1下限"]
				local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1上限"]
				if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
					self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限)/100+1)
				end
			end
		end
	elseif 技能名称=="泰山压顶" or 技能名称=="水漫金山" or 技能名称=="地狱烈火" or 技能名称=="奔雷咒" or 技能名称=="天降灵葫" or 技能名称=="叱咤风云" then
	self.临时伤害=self:取灵力差(攻击方,挨打方)*0.6*(10-人数+1)/10+攻击方.等级*3
	elseif 技能名称=="流沙轻音" or 技能名称=="食指大动" then
	self.临时伤害=self:取灵力差(攻击方,挨打方)*0.7*(10-人数+1)/10+攻击方.等级*3
	elseif 技能名称=="八凶法阵" or	技能名称=="神来气旺" then
	self.临时伤害=self:取灵力差(攻击方,挨打方)*0.8*(10-人数+1)/10+攻击方.等级*3
	elseif 技能名称=="扶摇万里"	then
	self.临时伤害=self:取灵力差(攻击方,挨打方)*1.25*(10-人数+1)/10+攻击方.等级*3
	----龙宫
	elseif 技能名称=="龙卷雨击" then
		self.临时伤害=(self:取技能等级(攻击方,技能名称)*4.5+self:取灵力差(攻击方,挨打方)+攻击方.武器伤害*0.5)*(10-人数+1)/10*(math.random(90,110)/100)
		if 攻击方.法宝效果.镇海珠 then
			self.临时伤害= self.临时伤害+攻击方.法宝效果.镇海珠
		end
		if 攻击方.奇经八脉.云霄 then
			self.临时伤害 =self.临时伤害+100
		end
		if 攻击方.神器=="沧浪赋" then
			self.临时伤害 =self.临时伤害*1.3
		end
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限)/100+1)
			end
		end
	elseif 技能名称=="龙吟" then
	self.临时伤害=(self:取技能等级(攻击方,技能名称))+10
	self.天地同寿=true
	挨打方.当前魔法=挨打方.当前魔法-math.floor(self.临时伤害*0.8)
	if 挨打方.当前魔法<0 then 挨打方.当前魔法=0 end
			if 攻击方.法宝效果.镇海珠 then
		self.临时伤害= self.临时伤害+攻击方.法宝效果.镇海珠
	end
	elseif 技能名称=="龙腾" then
	self.临时伤害=(self:取技能等级(攻击方,技能名称)*2.8+self:取灵力差(攻击方,挨打方)+攻击方.武器伤害*0.3)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)*(math.random(90,105)/100)
		if 攻击方.法宝效果.镇海珠 then
		self.临时伤害= self.临时伤害+攻击方.法宝效果.镇海珠
	end
	elseif 技能名称=="二龙戏珠" then
	self.临时伤害=(self:取技能等级(攻击方,技能名称)*2.8+self:取灵力差(攻击方,挨打方)+攻击方.武器伤害*0.3)*0.8*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)*(math.random(90,105)/100)
		if 攻击方.法宝效果.镇海珠 then
		self.临时伤害= self.临时伤害+攻击方.法宝效果.镇海珠
	end
	elseif 技能名称=="雷霆万钧" then
		self.临时伤害=(self:取技能等级(攻击方,技能名称)*8+self:取灵力差(攻击方,挨打方)+攻击方.武器伤害*0.3+22.5)*(10-人数+1)/10*(math.random(90,110)/100)
		self.法术暴击=true
		self.法伤附加=true
		self.天地同寿=true
		if	攻击方.天雷地火 then
			self.临时伤害=self.临时伤害+攻击方.天雷地火
		end
		if 攻击方.法宝效果.伏魔天书 then
			self.临时伤害= self.临时伤害+攻击方.法宝效果.伏魔天书
		end
		if 挨打方.法术状态组.电芒 then
			self.临时伤害 =self.临时伤害*1.05
		end
		if 攻击方.神器 =="裂帛" then
			self.临时伤害 =self.临时伤害*1.2
		end
		
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限)/100+1)
			end
		end
	elseif 技能名称=="飞砂走石" then
		self.临时伤害=(self:取技能等级(攻击方,技能名称)*5+self:取灵力差(攻击方,挨打方)+攻击方.武器伤害*0.5)*(10-人数+1)/10*(math.random(90,110)*1.15/100)
		if 攻击方.奇经八脉.魔心 and math.random(100) <=10 then
			self.临时伤害=self.临时伤害*2
		end
		if 攻击方.神器=="流火" and math.random(100) <=30 then
			self.临时伤害=self.临时伤害*1.3
		end
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限)/100+1)
			end
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				if 攻击方.法连 == nil then
					攻击方.法连 = 0
				end
				攻击方.法连 = 攻击方.法连 + 取随机数(元神加成下限,元神加成上限)
			end
		end
	elseif 技能名称=="焚魔烈焰" then
	self.临时伤害=(self:取技能等级(攻击方,技能名称)*8+self:取灵力差(攻击方,挨打方)+攻击方.武器伤害*0.3)*(10-人数+1)/10*(math.random(90,110)/100)
	if 攻击方.神器=="惊梦" then
	self.临时伤害=self.临时伤害*1.2
	end
	elseif 技能名称=="落叶萧萧" then
		self.临时伤害=(self:取技能等级(攻击方,技能名称)*2+self:取灵力差(攻击方,挨打方)+攻击方.武器伤害*0.5)*(10-人数+1)/10*(math.random(90,110)*1.15/100)
		if 攻击方.神器=="凭虚御风" then
			self.临时伤害=self.临时伤害*1.05
		end
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限)/100+1)
			end
			
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果3下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果3上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				if 攻击方.法连 == nil then
					攻击方.法连 = 0
				end
				攻击方.法连 = 攻击方.法连 + 取随机数(元神加成下限,元神加成上限)
			end
		end
 elseif 技能名称=="夜舞倾城" then
	self.临时伤害=self:取物理伤害(攻击方,挨打方)*1.35
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="力劈华山" then
	local xs =0.5
	if 攻击方.伤害 > 挨打方.伤害	then
		xs=1.2
	end
	self.临时伤害=self:取物理伤害(攻击方,挨打方)*xs
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	if self:取是否必杀(攻击方,挨打方) then
		self.临时伤害=self.临时伤害*2
		end

		elseif 技能名称=="进阶力劈华山" then
	local xs =0.5
	if 攻击方.伤害 > 挨打方.伤害	then
		xs=1.2
	end
	self.临时伤害=self:取物理伤害(攻击方,挨打方)*xs
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	if self:取是否必杀(攻击方,挨打方) then
		self.临时伤害=self.临时伤害*2
		end

	elseif 技能名称=="后发制人"	then
	self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.3*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100))
	if 攻击方.奇经八脉.勇武 then
		self.临时伤害= self.临时伤害*1.4
	end
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)

	elseif 技能名称=="当头一棒"	then
		self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.6*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.2/100))
		self.临时伤害=self.临时伤害*(math.random(90,105)/100)
		if 攻击方.神器 == "开辟" then
			self.临时伤害=self.临时伤害*1.15
			if	挨打方.当前气血 >= 挨打方.气血上限*0.6 and	挨打方.当前气血 <= 挨打方.气血上限*0.8 then
			self.临时伤害=self.临时伤害*2
			end
		else 
			if	挨打方.当前气血 >= 挨打方.气血上限*0.6 and	挨打方.当前气血 <= 挨打方.气血上限*0.7 then
				self.临时伤害=self.临时伤害*2
			end
		end

	elseif 技能名称=="杀威铁棒"	then
	self.临时伤害=(self:取物理伤害(攻击方,挨打方)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.16/100))
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
			elseif 技能名称=="泼天乱棒"	then
	self.临时伤害=(self:取物理伤害(攻击方,挨打方)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.16/100))
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="翩鸿一击"	then
	self.临时伤害=self:取物理伤害(攻击方,挨打方)*1.6
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="长驱直入"	then
	self.临时伤害=self:取物理伤害(攻击方,挨打方)*1.6
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="弱点击破" then
	self.临时伤害=self:取物理伤害(攻击方,挨打方,0.35)
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)

	elseif 技能名称=="蚩尤之搏" then
	self.临时伤害=self:取物理伤害(攻击方,挨打方,0.35)
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)

	elseif 技能名称=="壁垒击破" then
	self.临时伤害=self:取物理伤害(攻击方,挨打方,0.35)
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="天命剑法" then
	self.临时伤害=self:取物理伤害(攻击方,挨打方)*(0.7 +人数*0.1)
	elseif 技能名称=="破血狂攻" then
	self.临时伤害=math.floor(self:取物理伤害(攻击方,挨打方)*1.25)
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="破碎无双" then
	self.临时伤害=math.floor(self:取物理伤害(攻击方,挨打方)*1.25)
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="惊心一剑" then
	self.临时伤害=math.floor(self:取物理伤害(攻击方,挨打方)*1.25)
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="善恶有报" then
	self.临时伤害=math.floor(self:取物理伤害(攻击方,挨打方)*2)
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="进阶善恶有报" then
	self.临时伤害=math.floor(self:取物理伤害(攻击方,挨打方)*2)
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="横扫千军"	then
		if 攻击方.奇经八脉.勇念 then
			self.临时伤害=(self:取物理伤害(攻击方,挨打方,0.9)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级))/100)* (0.85 +人数*0.1)*1.5
		else
			self.临时伤害=(self:取物理伤害(攻击方,挨打方)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级))/100)* (0.85 +人数*0.1)*1.5
		end
		self.临时伤害=self.临时伤害*(math.random(95,105)/100)
		if 攻击方.神器== "惊锋" then
			self.临时伤害= self.临时伤害 *1.2
		end
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限) / 100 + 1)
			end
		end
	elseif 技能名称=="天崩地裂"	then
	self.临时伤害=(self:取物理伤害(攻击方,挨打方)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级))/100)*(1 +人数*0.1)
	self.临时伤害=self.临时伤害*math.random(90,105)/100
	elseif 技能名称=="破釜沉舟"	then
		self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.1*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级))/100)
		self.临时伤害=self.临时伤害*math.random(90,105)/100
		if 攻击方.奇经八脉.破空 then
			self.临时伤害= self.临时伤害+挨打方.防御*0.3
		end
	elseif 技能名称=="连环击"	then
	self.临时伤害=(self:取物理伤害(攻击方,挨打方)*0.65*((取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)*(1-人数*0.1)))
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)

	elseif 技能名称=="烟雨剑法"	then
	self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.15*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100))
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	if 攻击方.烟雨飘摇 then
		self.临时伤害=self.临时伤害+攻击方.烟雨飘摇
	end
	elseif 技能名称=="断岳势"	then
	self.临时伤害=(self:取物理伤害(攻击方,挨打方)*0.85*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100))
	if 攻击方.奇经八脉.破击凌 then
	self.临时伤害= self.临时伤害*1.2
	end
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="腾雷"	then
	self.临时伤害=(self:取物理伤害(攻击方,挨打方)*0.85*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100))
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="大闹天宫"	then
	self.临时伤害=(self:取物理伤害(攻击方,挨打方)+攻击方.力量*0.45)*1.25
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="连击" or 技能名称=="死亡召唤" or 技能名称=="哼哼哈兮" or 技能名称=="理直气壮" then
	self.临时伤害=self:取物理伤害(攻击方,挨打方)
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="夺命蛛丝"	then
	if 挨打方.队伍==0 or 挨打方.队伍==nil then
	self.临时伤害=1
	else
		if 挨打方.战斗类型=="角色" then
		self.临时伤害=1
		else
		self.临时伤害=挨打方.气血上限
		end
	end
	elseif 技能名称=="飘渺式"	then
		self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.15*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.15/100))
		self.临时伤害=self.临时伤害*(math.random(90,105)/100)
		if 攻击方.烟雨飘摇 then
			self.临时伤害=self.临时伤害+攻击方.烟雨飘摇
		end
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限)/100+1)
			end
		end
	elseif 技能名称=="锋芒毕露"	then
		self.临时伤害=(self:取物理伤害(攻击方,挨打方)*0.5*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.15/100))
		self.临时伤害=self.临时伤害*(math.random(90,105)/100)
		if 攻击方.攻之械 >=3 then
	self.临时伤害=self.临时伤害*1.5
	end
	elseif 技能名称=="针锋相对"	then
		self.临时伤害=(self:取物理伤害(攻击方,挨打方)*0.7*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.15/100))
		self.临时伤害=self.临时伤害*(math.random(90,105)/100)
		if 攻击方.攻之械 >=3 then
		self.临时伤害=self.临时伤害*1.5
		end
		if 攻击方.神器 =="千机巧变" then
		self.临时伤害 =self.临时伤害*1.5
		end
	elseif 技能名称=="诱袭"	then
		self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.2*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.15/100))
		self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="浪涌"	then
		self.临时伤害=(self:取物理伤害(攻击方,挨打方)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.05/100))
			if 攻击方.奇经八脉.怒涛 then
	self.临时伤害= self.临时伤害*1.2
	end
		self.临时伤害=self.临时伤害*(math.random(90,105)/100)

	elseif 技能名称=="断岳势"	then
		self.临时伤害=(self:取物理伤害(攻击方,挨打方)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.15/100))
		if 攻击方.奇经八脉.破击凌 then
	self.临时伤害= self.临时伤害*1.2
	end
		self.临时伤害=self.临时伤害*(math.random(90,105)/100)

	elseif 技能名称=="惊涛怒"	then
	self.临时伤害=(self:取物理伤害(攻击方,挨打方)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100))*0.8
				if 攻击方.奇经八脉.怒涛 then
	self.临时伤害= self.临时伤害*1.2
	end
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="满天花雨"	then
		self.临时伤害=(self:取物理伤害(攻击方,挨打方)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.55/100))
		self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="狮搏"	then
	self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.3*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.15/100))
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	if 攻击方.奇经八脉.狮吼 then
		self.临时伤害 =self.临时伤害*1.15
	end
	elseif 技能名称=="裂石"	then
	self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.25*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.15/100))
	if 攻击方.奇经八脉.破击凌 then
	self.临时伤害= self.临时伤害*1.2
	end
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="象形"	then
	self.临时伤害=(self:取物理伤害(攻击方,挨打方)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.15/100))*0.85
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="天雷斩"	then
	self.临时伤害=(self:取物理伤害(攻击方,挨打方)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.15/100)*0.95)+攻击方.灵力*0.1
	if 挨打方.战斗类型~= "角色" then
		self.临时伤害=self.临时伤害*1.15
	end
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	if	攻击方.天雷地火 then
	self.临时伤害=self.临时伤害+攻击方.天雷地火
	end
		if 攻击方.奇经八脉.疾雷 then
		self.临时伤害=self.临时伤害+0.6*(攻击方.敏捷-攻击方.等级)
		end
	elseif 技能名称=="金钱镖" or 技能名称=="落雨金钱"	then
	self.临时伤害=self:取物理伤害(攻击方,挨打方)*0.6
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="鹰击" or 技能名称=="牛刀小试"	then
		self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.15*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100))
		self.临时伤害=self.临时伤害*(math.random(90,105)/100)
		if 攻击方.神器 =="蛮血" then
			self.临时伤害=self.临时伤害*1.15
		end
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限)/100+1)
			end
		end
	elseif 技能名称=="神针撼海"	then
		self.临时伤害=(self:取物理伤害(攻击方,挨打方)*0.95*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)*1.18/100))
		self.临时伤害=self.临时伤害*(math.random(90,105)/100)
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限)/100+1)
			end
		end
	elseif 技能名称=="百爪狂杀"	then
	self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.2)
	if 攻击方.奇经八脉.百炼 then
	self.临时伤害=self.临时伤害+攻击方.武器伤害
	end
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	if	昼夜参数==1 then
		if 攻击方.神器 =="亡灵泣语" then
		self.临时伤害=self.临时伤害*1.5
		end
	end
	elseif 技能名称=="剑荡四方"	then
	self.临时伤害=self:取物理伤害(攻击方,挨打方)*0.5
	self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="翻江搅海"	then
		self.临时伤害=(self:取物理伤害(攻击方,挨打方)*1.05*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100))
		if 攻击方.奇经八脉.怒涛 then
			self.临时伤害= self.临时伤害*1.2
		end
		self.临时伤害=self.临时伤害*(math.random(90,105)/100)
	elseif 技能名称=="星月之惠"	then
	self.临时伤害=self:取技能等级(攻击方,技能名称)*3+150
	self.临时伤害=self.临时伤害*(1+攻击方.修炼数据.法术*0.02)+攻击方.治疗能力+挨打方.气血回复效果
	if 攻击方.奇经八脉.月影	then
		self.临时伤害=self.临时伤害*1.2
	end
	elseif 技能名称=="舍生取义"	then
	self.临时伤害=(挨打方.气血上限-挨打方.当前气血)*(取百分比(self:取技能等级(攻击方,技能名称),攻击方.等级)/100)
	elseif 技能名称=="杨柳甘露"	then
		self.临时伤害=self:取技能等级(攻击方,技能名称)*3+150
		if 攻击方.神器 == "玉魄" and math.random(100) <=20 then
			self.临时伤害 =挨打方.气血上限
		end
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限)/100+1)
			end
		end
	elseif 技能名称=="由己渡人"	then
	self.临时伤害=self:取技能等级(攻击方,"地涌金莲")*6
	if 挨打方.法术状态组.由己渡人==nil then
		挨打方.法术状态组.由己渡人={伤害 =self:取技能等级(攻击方,"地涌金莲")*12,回合=5}
	end

	elseif 技能名称=="还阳术"	then
	self.临时伤害=self:取技能等级(攻击方,技能名称)*3+150
	if not 挨打方.法术状态组.还阳术 then
		self.临时伤害=self.临时伤害*5
		挨打方.防御=挨打方.防御-self:取技能等级(攻击方,技能名称)
		挨打方.伤害=挨打方.伤害+self:取技能等级(攻击方,技能名称)
		挨打方.法术状态组.还阳术={回合=9999999}
	end
	elseif 技能名称=="黄泉之息"	then
	self.临时伤害=self:取技能等级(攻击方,技能名称)*2+150
	if not 挨打方.法术状态组.黄泉之息 then
		self.临时伤害=self.临时伤害*5
		挨打方.速度=挨打方.速度-math.floor(self:取技能等级(攻击方,技能名称)/5)
		挨打方.法术状态组.黄泉之息={回合=9999999}
	end
	elseif 技能名称=="摧心术"	then
	if not 挨打方.法术状态组.摧心术 then
		挨打方.速度=挨打方.速度-math.floor(self:取技能等级(攻击方,技能名称)/5)
		挨打方.法术状态组.摧心术={回合=5}
	end
	----------------化生
	elseif 技能名称=="峰回路转"	then
	self.临时伤害=self:取技能等级(攻击方,技能名称)*7+200
	self.临时伤害=self.临时伤害*(1+攻击方.修炼数据.法术*0.02)+攻击方.治疗能力+挨打方.气血回复效果
	elseif 技能名称=="匠心·蓄锐"	then
	self.临时伤害=(self:取技能等级(攻击方,技能名称)*5+100)*(10-人数+1)/10
	self.临时伤害=self.临时伤害*(1+攻击方.修炼数据.法术*0.02)+攻击方.治疗能力+挨打方.气血回复效果
	if 攻击方.攻之械 >=3 then
	self.临时伤害=self.临时伤害*1.5
	end
	elseif 技能名称=="推气过宫"	then
		self.临时伤害=(self:取技能等级(攻击方,技能名称)*10+200)*(10-人数+1)/10
		self.临时伤害=self.临时伤害*(1+攻击方.修炼数据.法术*0.02)+攻击方.治疗能力+挨打方.气血回复效果
		if 攻击方.奇经八脉.佛显 then
			self.临时伤害=self.临时伤害*1.1
		end
		
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限)/100+1)
			end
		end
		
	if 攻击方.奇经八脉.归气 then
		self.临时伤害=self.临时伤害*1.08
	end
	if 攻击方.奇经八脉.止戈 then
		self.临时伤害=self.临时伤害+攻击方.武器伤害*0.18
	end
	if 攻击方.奇经八脉.心韧 and math.random(100)<=15 then
		self.临时伤害=self.临时伤害*2
	end
	if 攻击方.神器 =="挥毫" then
	self.临时伤害=self.临时伤害*1.35
	elseif 攻击方.神器 =="风起云墨" then
		挨打方.物伤减免 =0.92
		挨打方.法伤减免 =0.92
	end
	elseif 技能名称=="我佛慈悲"	then
		self.临时伤害=self:取技能等级(攻击方,技能名称)*3+150
		if 攻击方.法宝效果.慈悲 and math.random(100)<攻击方.法宝效果.慈悲 then
			self.临时伤害= self.临时伤害*2
		end
		if 攻击方.奇经八脉.归气 then
			self.临时伤害=self.临时伤害*1.08
		end
		if 攻击方.神器 =="挥毫" then
			self.临时伤害=self.临时伤害*1.35
		end
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限)/100+1)
			end
		end
	elseif 技能名称=="还魂咒"	then
	self.临时伤害=self:取技能等级(攻击方,技能名称)*2
	elseif 技能名称=="莲花心音"	then
		self.临时伤害=挨打方.气血上限*0.6

	elseif 技能名称=="治疗"	then
	self.临时伤害=self:取技能等级(攻击方,技能名称)*4+50
	self.临时伤害=self.临时伤害*(1+攻击方.修炼数据.法术*0.05)+攻击方.治疗能力+挨打方.气血回复效果
	elseif 技能名称=="仙人指路"	then
	self.临时伤害=self:取技能等级(攻击方,技能名称)*5+50
	self.临时伤害=self.临时伤害*(1+攻击方.修炼数据.法术*0.05)+攻击方.治疗能力+挨打方.气血回复效果
	elseif 技能名称=="活血"	then
	self.临时伤害=self:取技能等级(攻击方,技能名称)*15+150
	self.临时伤害=self.临时伤害*(1+攻击方.修炼数据.法术*0.05)+攻击方.治疗能力+挨打方.气血回复效果
	if 攻击方.法宝效果.慈悲 and math.random(100)<攻击方.法宝效果.慈悲 then
		self.临时伤害= self.临时伤害*2
	end
		if 攻击方.奇经八脉.归气 then
		self.临时伤害=self.临时伤害*1.08
	end
	if 攻击方.奇经八脉.佛誉 then
		self.临时伤害=self.临时伤害*1.2
	end
	if 攻击方.奇经八脉.止戈 then
		self.临时伤害=self.临时伤害+攻击方.武器伤害*0.18
	end
	if 攻击方.奇经八脉.仁心 and math.random(100) <= 20 then
	self.临时伤害=self.临时伤害*2
	end
	if 攻击方.神器 =="挥毫" then
	self.临时伤害=self.临时伤害*1.35
	elseif 攻击方.神器 =="风起云墨" then
		挨打方.物伤减免 =0.92
		挨打方.法伤减免 =0.92
	end
	elseif 技能名称=="唧唧歪歪" then
	self.临时伤害=(self:取技能等级(攻击方,技能名称)*2+self:取灵力差(攻击方,挨打方)+攻击方.武器伤害*0.3+22.5)*(10-人数+1)/10*(math.random(90,110)/100)
	if 攻击方.福泽天下 then
		self.临时伤害=self.临时伤害+攻击方.福泽天下
	end
	if 攻击方.奇经八脉.销武 then
		self.临时伤害=self.临时伤害+攻击方.等级
	end
	-------------------无底
	elseif 技能名称=="地涌金莲"	then
		self.临时伤害=(self:取技能等级(攻击方,技能名称)*10+200)*(10-人数+1)/10*(1+攻击方.修炼数据.法术*0.02)+攻击方.治疗能力+挨打方.气血回复效果
		if 攻击方.奇经八脉.自愈 then
			self.临时伤害=self.临时伤害*1.5
		end
		if 攻击方.奇经八脉.化莲 then
			self.临时伤害=self.临时伤害+攻击方.武器伤害*0.3
		end
		if 攻击方.神器 =="情思悠悠" then
			self.临时伤害=self.临时伤害*1.2
		end
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限)/100+1)
			end
		end
	elseif 技能名称=="普渡众生"	then
		if 攻击方.法宝效果.普渡 then
			self.临时伤害=(self:取技能等级(攻击方,技能名称)*6+50+攻击方.治疗能力)*(1+攻击方.修炼数据.法术*0.02)+挨打方.气血回复效果*(1+攻击方.法宝效果.普渡/10)
		else
			self.临时伤害=(self:取技能等级(攻击方,技能名称)*6+50+攻击方.治疗能力)*(1+攻击方.修炼数据.法术*0.02)+挨打方.气血回复效果
		end
		if 攻击方.奇经八脉.化戈 then
			self.临时伤害=self.临时伤害+攻击方.武器伤害*0.18
		end
		
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果1上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				self.临时伤害 = self.临时伤害 * (取随机数(元神加成下限,元神加成上限)/100+1)
			end
		end
	elseif 技能名称=="九幽"	then
	self.临时伤害=math.min(攻击方.法宝效果.九幽/1.5,攻击方.等级*3)
	if 攻击方.奇经八脉.幽光 then
	self.临时伤害=self.临时伤害+120
	end
	elseif 技能名称=="生命之泉"	then
	self.临时伤害=(self:取技能等级(攻击方,技能名称)*1.5+攻击方.治疗能力)*(1+攻击方.修炼数据.法术*0.02)+挨打方.气血回复效果
	if 攻击方.奇经八脉.体恤 and 攻击方.当前气血<=攻击方.最大气血*0.3 then
		self.临时伤害=self.临时伤害+150
	end
	elseif 技能名称=="气疗术" then
	self.临时伤害=挨打方.气血上限*0.03+200+攻击方.治疗能力+挨打方.气血回复效果
	elseif 技能名称=="命疗术" then
	self.临时伤害=挨打方.气血上限*0.09+600+攻击方.治疗能力+挨打方.气血回复效果
	elseif 技能名称=="气归术" or 技能名称=="冰清诀" or	技能名称=="四海升平" then
	self.临时伤害=挨打方.气血上限*0.25+攻击方.治疗能力+挨打方.气血回复效果
	elseif 技能名称=="命归术" then
	self.临时伤害=挨打方.气血上限*0.5+攻击方.治疗能力+挨打方.气血回复效果
	elseif 技能名称=="玉清诀" or 技能名称=="凝气诀" or 技能名称=="凝神诀" then
	self.临时伤害=1
	elseif 技能名称=="慈航普渡" then
	self.临时伤害=挨打方.气血上限
	elseif 技能名称=="晶清诀" then
	self.临时伤害=挨打方.气血上限*0.15+攻击方.治疗能力+挨打方.气血回复效果
	elseif 技能名称=="冥王爆杀" then
	self.临时伤害=攻击方.灵力*math.random(80,150)/100
	self.天地同寿=true
	elseif 技能名称=="诅咒之伤" then
	self.临时伤害=挨打方.当前气血*0.2
	if self.临时伤害>攻击方.等级*攻击方.等级 then
		self.临时伤害=攻击方.等级*攻击方.等级
	end
	end

 if 攻击方.法波~=0	then
	self.临时伤害=self.临时伤害*math.random(攻击方.法波.下限,攻击方.法波.上限)/100
	end
	if self.天地同寿 and 挨打方.法术状态组.天地同寿 and 挨打方.命令数据.类型=="防御" then
	self.临时伤害=1
	end
 if self.临时伤害<1 then self.临时伤害=1 end
	if self:取额外伤害法术(技能名称) then
	return math.floor(self.临时伤害+攻击方.额外法术伤害+攻击方.法术伤害结果)
	else
	return math.floor(self.临时伤害)
	end
 end
function FightControl:取额外伤害法术(技能)
	if	技能=="水攻" or 技能=="落岩" or 技能=="烈火" or 技能=="雷击" or 技能=="月光" or 技能=="荆棘舞" or 技能=="扶摇万里"or 技能=="神来气旺"or 技能=="尘土刃" or 技能=="冰川怒" or 技能=="三昧真火" or 技能=="五雷咒"
 or 技能=="落雷符"or 技能=="上古灵符" or 技能=="泰山压顶" or 技能=="水漫金山" or 技能=="地狱烈火" or 技能=="奔雷咒" or 技能=="天降灵葫" or 技能=="雷霆万钧" or 技能=="飞砂走石"or 技能=="焚魔烈焰"
 or 技能=="八凶法阵" or 技能=="龙卷雨击"or 技能=="龙腾" or 技能=="二龙戏珠" or	技能=="落叶萧萧"or	技能=="叱咤风云" or	技能=="流沙轻音" or	技能=="食指大动"	then
	return true
	else
	return	false
	end
end
-------------------------------



function FightControl:修罗隐身计算(编号)
	if self.参战单位[编号].命令数据.目标==0 or self:取存活状态(self.参战单位[编号].命令数据.目标)==false then
	self:添加提示(self.参战单位[编号].数字id,"目标已死亡")
	return 0
	end
	if self.参战单位[编号]==nil then
		return 0
	end
	if self.参战单位[编号].命令数据.目标==0 then --找不到可攻击的目标
	return 0
	elseif 特技==nil and self:技能消耗(self.参战单位[编号]) ==false then
	self:添加提示(self.参战单位[编号].数字id,"你的当前魔法不足以施放此技能")
	return 0
	end
	self.战斗流程[#self.战斗流程+1]={流程=4,执行=false,攻击方=编号,挨打方=self.参战单位[编号].命令数据.目标,结束=false,封印结果=true,类型="技能",参数="隐身"}
	self.等待结束=self.等待结束+5
	self.参战单位[self.参战单位[编号].命令数据.目标].法术状态组["隐身"]={回合=math.floor(self:取技能等级(self.参战单位[编号],"修罗隐身")/30)+2}
 end
function FightControl:取起群体复活单位(编号,相关id,类型,数量) --1为敌 2为队友
	self.单位组={}
	for n=1,#self.参战单位 do
		if self.参战单位[n].战斗类型=="角色" and self.参战单位[n].队伍id==相关id and self:取存活状态(n)==false and self:取复活状态(n)==false then
			self.单位组[#self.单位组+1]=n
		end
	end
	--这里遍历一次单位组中符合条件的单位
	if #self.单位组==0 then
		return 0
	else
		return self:取随机数组成员(self.单位组,数量)
	end
end

function FightControl:添加提示(id,内容)
	self.战斗流程.信息提示[#self.战斗流程.信息提示+1]={id=id,内容="#y/"..内容}
end


function FightControl:取保护单位(编号)
	for n=1,#self.参战单位 do
		if self.参战单位[n].队伍==self.参战单位[编号].队伍	and self.参战单位[n].命令数据.类型=="保护" and	self.参战单位[n].命令数据.目标==编号 and self:取存活状态(n) then
			self.参战单位[n].命令数据.类型=""
			return n
		end
	end
	return false
end

function FightControl:取物理伤害结果(数额,攻击方,挨打方,倍数,类型,神佑,额外)
	if 攻击方.神器 =="披坚执锐" and math.random(100) <=8 then
	数额=数额*0.1 
	end
	数额=math.floor(数额*倍数-挨打方.格挡值)
	if	攻击方.无赦魔决~=0 and 挨打方.神佑>15 then
		挨打方.神佑=挨打方.神佑-15
	end
	if 攻击方.风起龙游~=0 and 攻击方.速度 > 挨打方.速度 then
	数额=数额*1.2
	end
	if 攻击方.奇经八脉.六道无量 then
	数额=self:取技能等级(攻击方,"魂飞魄散")*3*0.6+数额
	end
	if 额外 then
	数额=数额+额外
	end
	if 挨打方.复活~=0 and 攻击方.驱鬼~=0 then
	数额 =数额*攻击方.驱鬼
	end
	if 挨打方.法术状态组.苍白纸人 then
	数额=数额-挨打方.法术状态组.苍白纸人.伤害
	end
	if 挨打方.法术状态组.五彩娃娃 then
	数额=数额-挨打方.法术状态组.五彩娃娃.伤害
	end
	if 挨打方.法术状态组.铜头铁臂 then
			if 挨打方.法术状态组.铜头铁臂.护盾<数额	then
				数额=数额- 挨打方.法术状态组.铜头铁臂.护盾
				self:解除封印状态(挨打方,"铜头铁臂")
			else
				
				挨打方.法术状态组.铜头铁臂.护盾=挨打方.法术状态组.铜头铁臂.护盾-数额
				数额 =1
			end
	end
	if 挨打方.法术状态组.炎护 then
	if self:魔法消耗(攻击方,数额,0.5) then
		数额 = 数额 *0.5
		挨打方.当前魔法 = 挨打方.当前魔法 -数额
		if 挨打方.当前魔法 <1 then
			挨打方.法术状态组.炎护=nil
		end
	else
			挨打方.法术状态组.炎护=nil
	end
	end
	if 数额<1 then 数额=1 end
	local 死亡方式=0
	if 类型=="加血" then
		挨打方.当前气血=挨打方.当前气血+数额
		if 挨打方.当前气血>挨打方.气血上限 then 挨打方.当前气血=挨打方.气血上限 end
	elseif 挨打方.法术状态组.波澜不惊 then

			if 挨打方.法术状态组.波澜不惊.伤害<数额	then
				数额=数额- 挨打方.法术状态组.波澜不惊.伤害
				挨打方.法术状态组.波澜不惊.回合=1
				挨打方.法术状态组.波澜不惊.伤害=0
				-- self:解除封印状态(挨打方,"波澜不惊")
			else
				类型="加血"
				挨打方.法术状态组.波澜不惊.伤害=挨打方.法术状态组.波澜不惊.伤害-数额
				挨打方.当前气血=挨打方.当前气血+数额
				if 挨打方.当前气血>挨打方.气血上限 then 挨打方.当前气血=挨打方.气血上限 end
			end

	elseif 挨打方.法术状态组.由己渡人 then
			if 挨打方.法术状态组.由己渡人.伤害<数额	then
				self:解除封印状态(挨打方,"由己渡人")
			else
				类型="加血"
				挨打方.法术状态组.由己渡人.伤害=挨打方.法术状态组.由己渡人.伤害-数额
				挨打方.当前气血=挨打方.当前气血+数额
				if 挨打方.当前气血>挨打方.气血上限 then 挨打方.当前气血=挨打方.气血上限 end
			end
	else
		if 攻击方.百步穿杨 and 20>math.random(100) then
			数额=数额+攻击方.百步穿杨
		end
		if 挨打方.心随我动 and 25>math.random(100) then
			数额=数额-挨打方.心随我动
		end
		if 攻击方.法宝效果.斩魔 then
			数额=数额+攻击方.法宝效果.斩魔
		end
		if 攻击方.从天而降	then
			数额=数额+数额*0.5
		end
		if 神佑==nil and self:取是否神佑(挨打方,数额) then
			类型="加血"
			数额=挨打方.气血上限
			挨打方.当前气血=挨打方.当前气血+数额
			if 挨打方.当前气血>挨打方.气血上限 then
				挨打方.当前气血=挨打方.气血上限
			end
		else
			数额 =math.floor(数额)
			挨打方.当前气血=挨打方.当前气血-数额
				if 挨打方.法术状态组.神木宝鼎 then
				挨打方.当前魔法=挨打方.当前魔法+math.floor(数额*(1+挨打方.法术状态组.神木宝鼎.伤害))
				if 挨打方.当前魔法>挨打方.魔法上限 then
				挨打方.当前魔法=挨打方.魔法上限
				end
				end
			if 挨打方.当前气血<=0 then
			挨打方.当前气血=0
			死亡方式=self:取死亡状态(攻击方,挨打方)
			end
			self:愤怒处理(挨打方,数额,死亡方式)
		end
	end
	if 数额<1 then 数额=1 end
	self:催眠状态解除(类型,挨打方)
	return {伤害=math.floor(数额),类型=类型,死亡=死亡方式}
 end
function FightControl:取法术伤害结果(数额,攻击方,挨打方,倍数,类型,神佑)
	if 攻击方.神器 =="金汤之固" and	攻击方.当前气血 <攻击方.气血上限*0.3 then
		数额=数额*0.5
	elseif 攻击方.神器 =="披坚执锐" and math.random(100) <=8 then
	数额=数额*0.1 
	end
	数额=math.floor(数额*倍数)
	if 挨打方.奇经八脉.回旋 then
	数额=数额-60
	end
	if 挨打方.复活~=0 and 攻击方.驱鬼~=0 then
	数额 =数额*攻击方.驱鬼
	end
	if 挨打方.法术状态组.苍白纸人 then
	数额=数额-挨打方.法术状态组.苍白纸人.伤害
	end
	if 挨打方.法术状态组.五彩娃娃 then
	数额=数额-挨打方.法术状态组.五彩娃娃.伤害
	end
	local 死亡方式=0
	if 挨打方.法术状态组.炎护 then
	if self:魔法消耗(攻击方,数额,0.5) then
		数额 = 数额 *0.5
		挨打方.当前魔法 = 挨打方.当前魔法 -数额
		if 挨打方.当前魔法 <1 then
			挨打方.法术状态组.炎护=nil
		end
	else
			挨打方.法术状态组.炎护=nil
	end
	end
	if 挨打方.法术状态组.铜头铁臂 then
			if 挨打方.法术状态组.铜头铁臂.护盾<数额	then
				数额=数额- 挨打方.法术状态组.铜头铁臂.护盾
				self:解除封印状态(挨打方,"铜头铁臂")
			else
				
				挨打方.法术状态组.铜头铁臂.护盾=挨打方.法术状态组.铜头铁臂.护盾-数额
				数额 =1
			end
	end
	if 数额< 1 then 数额=1 end
 if 类型=="加血" then
	挨打方.当前气血=挨打方.当前气血+数额
	if 挨打方.当前气血>挨打方.气血上限 then
	挨打方.当前气血=挨打方.气血上限
	end
 elseif 挨打方.法术状态组.波澜不惊 then
	if 挨打方.法术状态组.波澜不惊.伤害<数额	then
			数额=数额- 挨打方.法术状态组.波澜不惊.伤害
			if 挨打方.法术状态组.波澜不惊.伤害 <1 then
			-- self:解除封印状态(挨打方,"波澜不惊")
			挨打方.法术状态组.波澜不惊.回合=1
			挨打方.法术状态组.波澜不惊.伤害=0
			end
		end
	elseif 挨打方.法术状态组.由己渡人 then
	if 挨打方.法术状态组.由己渡人.伤害<数额	then
			数额=数额- 挨打方.法术状态组.由己渡人.伤害
			if 挨打方.法术状态组.由己渡人.伤害 <1 then
			挨打方.法术状态组.由己渡人=nil
			end
		end
	else
		if 挨打方.云随风舞 and 25>math.random(100) then
			数额=数额-挨打方.云随风舞
		end
	if 神佑==nil and self:取是否神佑(挨打方,数额) then
		类型="加血"
		数额=挨打方.气血上限
		挨打方.当前气血=挨打方.当前气血+数额
		if 挨打方.当前气血>挨打方.气血上限 then 挨打方.当前气血=挨打方.气血上限 end
	else
		
		挨打方.当前气血=挨打方.当前气血-数额
		if 挨打方.法术状态组.神木宝鼎 then
			挨打方.当前魔法=挨打方.当前魔法+math.floor(数额*(1+挨打方.法术状态组.神木宝鼎.伤害))
			if 挨打方.当前魔法>挨打方.魔法上限 then
			挨打方.当前魔法=挨打方.魔法上限
			end
		end
		if 挨打方.当前气血<=0 then
		挨打方.当前气血=0
		死亡方式=self:取死亡状态(攻击方,挨打方)
		end
		self:愤怒处理(挨打方,数额,死亡方式)
	end
	end


	if 类型=="加血" and 挨打方.法术状态组.魔音摄魂 then
	数额=1
	end
 if 数额<1 then 数额=1 end
	self:催眠状态解除(类型,挨打方)
	return {伤害=math.floor(数额),类型=类型,死亡=死亡方式}
 end
function FightControl:愤怒处理(攻击方,伤害,死亡方式)
	if 攻击方.愤怒==nil then
	return 0
	elseif 死亡方式>0 then
	if 攻击方.法宝效果.炽焰丹珠 then
		if 攻击方.愤怒 > 攻击方.法宝效果.炽焰丹珠	then
			攻击方.愤怒 = 攻击方.法宝效果.炽焰丹珠
		end
	else
		攻击方.愤怒=0
	end
	return 0
	else
		local 增加愤怒 = 150*(1-(攻击方.最大气血-伤害)/攻击方.气血上限)+1
		攻击方.愤怒=攻击方.愤怒+math.floor(增加愤怒)
		if 攻击方.愤怒>150 then
		攻击方.愤怒=150
		end
		if 攻击方.愤怒<0 then
		攻击方.愤怒=0
		end
	end
 end
function FightControl:取是否吸血(攻击方,挨打方)
 if 攻击方.吸血>0 and 挨打方.复活==0 then
	return true
	else
	return false
	end
	end
function FightControl:取死亡状态(攻击方,挨打方)
	if 攻击方~=nil and 攻击方~=1 and 攻击方.神器=="静笃" then
	攻击方.伤害= 攻击方.伤害+math.random(100,200)
	end
	if 挨打方.战斗类型=="角色" and 挨打方.队伍~=0 then

	if 攻击方~=nil and 攻击方~=1 and 攻击方.名称~=nil	then
		挨打方.死亡消息=攻击方.名称
	else
		挨打方.死亡消息="？？？"
	end
	if 攻击方~=nil and 攻击方~=1 and 攻击方.玩家id~=nil and 攻击方.玩家id~=0 then
		挨打方.死亡消息=挨打方.死亡消息.."["..攻击方.玩家id.."]"
	end
	return 1
	elseif 挨打方.复活~=0 and 攻击方.驱鬼==0 then
	挨打方.法术状态组.鬼魂术={回合=挨打方.复活}
	return 1
	else

	挨打方.单位消失=1
	return 2
	end
 end
function FightControl:取物理伤害(攻击方,挨打方,防御)
	self.附加值=0
	if 挨打方.物伤减免==nil then 
		挨打方.物伤减免=1 
	end
	if 攻击方.玩家id~=0 and 攻击方.战斗类型~="角色" and UserData[攻击方.玩家id]~= nil then
		self.附加值=攻击方.伤害*0.1
	end
	local 额外伤害 = 0
	if 攻击方.门派 == "凌波城" then
		if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
			local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2下限"]
			local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2上限"]
			if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
				local 转换参数 = 取随机数(元神加成下限,元神加成上限)
				额外伤害 = math.floor(攻击方.防御 * 转换参数 + 挨打方.防御 * 转换参数 * 0.4)
			end
		end
	end
	if 防御 then
		self.临时伤害=math.floor((攻击方.伤害 + 额外伤害 +self.附加值+攻击方.穿刺等级-(挨打方.防御*防御)))
	else
		self.临时伤害= math.floor((攻击方.伤害+额外伤害+self.附加值+攻击方.穿刺等级-(挨打方.防御)))
	end
	if self.临时伤害 < 1 then
		self.临时伤害 =1
	end
	self.临时伤害=self.临时伤害+self.临时伤害*self.临时伤害/4500
	self.临时伤害=self.临时伤害*((1+攻击方.修炼数据.攻击*0.04))+攻击方.修炼数据.攻击*5
	self.临时伤害=self.临时伤害*((1-挨打方.修炼数据.防御*0.03))
	self.临时伤害=self.临时伤害*(math.random(90,110)/100)*挨打方.物伤减免
 if self.临时伤害<1 then self.临时伤害=1 end
 if 攻击方.玩家id~=0 and 攻击方.战斗类型=="角色" and UserData[攻击方.玩家id]~=nil then
	RoleControl:耐久处理(UserData[攻击方.玩家id],1)
	end
 if 挨打方.玩家id~=0 and 挨打方.战斗类型=="角色" and UserData[挨打方.玩家id]~=nil then
	RoleControl:耐久处理(UserData[挨打方.玩家id],2)
	end
 return math.floor(self.临时伤害)
 end
function FightControl:取灵力差(攻击方,挨打方,伤害)
	if 攻击方.隔山打牛 and 20>math.random(100) then
	self.临时伤害=math.floor(((攻击方.灵力+攻击方.隔山打牛)*攻击方.魔心+攻击方.法术伤害-挨打方.法防))
	else
	self.临时伤害=math.floor((攻击方.灵力*攻击方.魔心+攻击方.法术伤害-挨打方.法防))
	end
	self.临时伤害=self.临时伤害+self.临时伤害*self.临时伤害/2000
	self.临时伤害=self.临时伤害*挨打方.法伤减免
	self.临时伤害=self.临时伤害*((1+攻击方.修炼数据.法术*0.05))
	self.临时伤害=self.临时伤害*((1-挨打方.修炼数据.法抗*0.025))+攻击方.法术伤害结果
	if 挨打方.队伍==0 then
	self.临时伤害=math.floor(self.临时伤害*1.35)
	end
	if 攻击方.法波~=0	then
	self.临时伤害=self.临时伤害*math.random(攻击方.法波.下限,攻击方.法波.上限)/100
	end
	return math.floor(self.临时伤害)
 end
 
function FightControl:取队伍id(id)
	if UserData[id].队伍 == 0 then
		return id
	else
		return UserData[id].队伍
	end
end

function FightControl:取隐身判断(攻击方,挨打方)
	if self.参战单位[挨打方].法术状态组.楚楚可怜 and not self.参战单位[攻击方].感知 then
	return false
	end
	if self.参战单位[挨打方].法术状态组.分身术 then
	if self.参战单位[挨打方].法术状态组.分身术.躲避==false	then
	self.参战单位[挨打方].法术状态组.分身术.躲避=true
	self.参战单位[挨打方].法术状态组.分身术.标记=攻击方
	return false
	elseif self.参战单位[挨打方].法术状态组.分身术.标记==攻击方 then
		return false
	end
	end
	if self.参战单位[挨打方].法术状态组.隐身 and not self.参战单位[攻击方].感知 and not self.参战单位[攻击方].法术状态组.幽冥鬼眼	then
	return false
	else
	return true
	end
 end

 
function FightControl:取存活状态(编号)
	if self.参战单位[编号]==nil or self.参战单位[编号].当前气血==nil then
		return false
	end
	if self.参战单位[编号].当前气血<=0 or self.参战单位[编号].单位消失==1 then
		return false
	else
		return true
	end
end

function FightControl:取是否神佑(挨打方,伤害)
	if 挨打方.复活~=0 then 
		return false 
	end
	if	挨打方.当前气血<=伤害 and (math.random(100)<=挨打方.神佑 or (挨打方.法宝效果.救命毫毛 and math.random(100) < 挨打方.法宝效果.救命毫毛)) then
		return true
	else
		return false
	end
end

function FightControl:取是否法术吸血(攻击方,挨打方)
	if 攻击方.法术吸血>0 and 挨打方.复活==0 then
		return true
	else
		return false
	end
end
function FightControl:取是否反击(攻击方,挨打方)
 if math.random(100)<=挨打方.反击 and not 攻击方.偷袭 then
	return true
 elseif 挨打方.法术状态组.魔王回首 and not 攻击方.偷袭 then
	return true
	elseif 挨打方.法术状态组.极度疯狂 and not 攻击方.偷袭 then
	return true
	else
	return false
	end
 end
function FightControl:取是否反震(攻击方,挨打方)
 if math.random(100)<=挨打方.反震 and not 攻击方.偷袭 then
	return true
	else
	return false
	end
 end
function FightControl:取是否必杀(攻击方,挨打方)
 if math.random(100)<=math.floor((攻击方.必杀+(攻击方.物理暴击等级-挨打方.抗物理暴击等级)*(1000/攻击方.等级)/100)) and (not 挨打方.幸运 or 攻击方.神器=="威服天下" ) then
	return true
	else
	return false
	end
 end
function FightControl:取随机单位1(编号,相关id,类型,数量,参数) --1为敌 2为队友 
	self.单位组={}
	for n=1,#self.参战单位 do
	if 类型==1 then
	if self.参战单位[n].队伍id~=相关id and self:取存活状态(n) and self:取隐身判断(编号,n) then self.单位组[#self.单位组+1]=n end
	elseif 类型==2 then
	if self.参战单位[n].队伍id==相关id and self:取存活状态(n) then self.单位组[#self.单位组+1]={编号=n,气血=self.参战单位[n].当前气血/self.参战单位[n].气血上限} end
	end
	end
	if #self.单位组==0 then
	return 0
	else
	return self:取随机数组成员1(self.单位组,数量)
	end
 end
function FightControl:取随机数组成员1(数组,数量)
 if #数组<=数量 then
	self.临时数组={}
	for n=1,#数组 do
		self.临时数组[n]=数组[n].编号
	end
	return self.临时数组
	else
	self.临时数组={}
	table.sort(数组,function(a,b) return a.气血<b.气血 end )
	for n=1,数量 do
		self.临时数组[n]=数组[n].编号
	end
	return self.临时数组
	end
end

function FightControl:取随机单位(编号,相关id,类型,数量) --1为敌 2为队友
	self.单位组={}
	for n=1,#self.参战单位 do
		if 类型==1 then
			if self.参战单位[n].队伍id~=相关id and self:取存活状态(n) and self:取隐身判断(编号,n) then
				self.单位组[#self.单位组+1]=n
			end
		elseif 类型==2 then
			if self.参战单位[n].队伍id==相关id and self:取存活状态(n) then
				self.单位组[#self.单位组+1]=n
			end
		elseif 类型==3 then
			if self.参战单位[n].队伍id==相关id and self:取存活状态(n) and n~=编号 then
				self.单位组[#self.单位组+1]=n
			end
		end
	end
	if #self.单位组==0 then
		return 0
	else
		return self:取随机数组成员(self.单位组,数量)
	end
end
 
function FightControl:取随机数组成员(数组,数量)
	if #数组<=数量 then
		return 数组
	else
		self.临时数组={}
		for n=1,#数组 do
			数组[n]={a=数组[n],b=math.random(10000)}
		end
		table.sort(数组,function(a,b) return a.b>b.b end )
		for n=1,数量 do
			self.临时数组[n]=数组[n].a
		end
		return self.临时数组
	end
end

function FightControl:取封印结果(攻击方,挨打方,技能名称)
	assert(挨打方,string.format("技能名称=%s导致封印错误卡战斗",技能名称))
	if 技能名称=="反间之计" and 挨打方.允许反间==false then
		return false
	elseif 技能名称=="瘴气" or	技能名称=="魔息术" or	技能名称=="炽火流离" or 技能名称=="颠倒五行" or 技能名称=="灵动九天" or 技能名称=="金刚镯" or 技能名称=="神龙摆尾" or 技能名称=="分身术" or 技能名称=="碎甲符" or 技能名称=="后发制人" or 技能名称=="野兽之力" or 技能名称=="光辉之甲" or 技能名称=="太极护法" or 技能名称=="炼气化神" or 技能名称=="安神诀" or 技能名称=="佛法无边" or 技能名称=="蜜润"	then
		return true
	elseif	技能名称=="天地同寿" or 技能名称=="乾坤妙法" or 技能名称=="火甲术" then
		if self:取技能等级(攻击方,技能名称)<挨打方.等级 then
			return false
		else
			return true
		end
	elseif 挨打方.封印开关 then
		return false
	end
	if 挨打方.不可封印 then
		return false
	end
	self.初始几率=0
	self.等级差=self:取技能等级(攻击方,技能名称)-挨打方.等级
	if 技能名称=="含情脉脉" or 技能名称=="摄魄" or 技能名称=="勾魂" then
		if 攻击方.法宝效果.迷魂灯 then
			self.初始几率=35+攻击方.法宝效果.迷魂灯
		else
			self.初始几率=35
		end
		if 挨打方.法术状态组.寡欲令 and 技能名称=="含情脉脉" then
			return false
		end
		if 技能名称=="摄魄" or 技能名称=="勾魂" then
			self.初始几率=self.初始几率+15
		end
		if 技能名称=="含情脉脉" then
			if 攻击方.助战 == nil and UserData[攻击方.玩家id] ~= nil and UserData[攻击方.玩家id].角色.元神 ~= nil and UserData[攻击方.玩家id].角色.元神 > 0 then
				local 元神加成下限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2下限"]
				local 元神加成上限 = SpiritData[UserData[攻击方.玩家id].角色.门派][UserData[攻击方.玩家id].角色.元神.."介效果2上限"]
				if 元神加成下限 ~= 0 or 元神加成上限 ~= 0 then
					self.初始几率 = self.初始几率 + 取随机数(元神加成下限,元神加成上限)
				end
			end
		end
	elseif 技能名称=="似玉生香"or 技能名称=="一笑倾城"	then
		self.初始几率=35
		if 挨打方.法术状态组.宁心 or 挨打方.法术状态组.解封 then
			return false
		end
	elseif 技能名称=="尸腐毒"	then
		if 挨打方.法术状态组.百毒不侵 or 挨打方.法术状态组.乾坤妙法	then
			return false
		else
			self.初始几率=75
		end
	elseif 技能名称=="雾杀"	then
		if 挨打方.法术状态组.乾坤妙法	then
			return false
		else
			self.初始几率=75
		end
	elseif 技能名称=="日月乾坤" then
		if 攻击方.法宝效果.定风珠 then
			self.初始几率=30+攻击方.法宝效果.定风珠
		else
			self.初始几率=30
		end
		if 攻击方.神器=="斗转参横" and 攻击方.法术状态组.生命之泉 then
			self.初始几率=self.初始几率+10
		end
	elseif 技能名称=="威慑" then
		self.初始几率=50
		if self.pk战斗 or self.战斗类型==100029 or self.战斗类型==100033 or self.战斗类型==100034 or self.战斗类型==100035 or	self.战斗类型==100037 or self.战斗类型==100038 or self.战斗类型==888888 then
			return false
		end
	elseif 技能名称=="百万神兵" or 技能名称=="镇妖" or 技能名称=="错乱"	then
		if 攻击方.法宝效果.雷兽 then
			self.初始几率=35+攻击方.法宝效果.雷兽
		else
			self.初始几率=35
		end
		if 挨打方.法术状态组.复苏 then
			return false
		end
	elseif 技能名称=="如花解语" or 技能名称=="娉婷嬝娜" or 技能名称=="莲步轻舞" then
		if 攻击方.法宝效果.织女扇 then
			self.初始几率=45+攻击方.法宝效果.织女扇
		else
			self.初始几率=45
		end
		if 挨打方.法术状态组.宁心 or 挨打方.法术状态组.解封 then
			return false
		end
	elseif 技能名称=="夺魄令" or 技能名称=="煞气诀" then
		if 攻击方.法宝效果.幽灵珠 then
			self.初始几率=35+攻击方.法宝效果.幽灵珠
		else
			self.初始几率=35
		end
	elseif 技能名称=="定身符" or 技能名称=="追魂符" or 技能名称=="失心符" or 技能名称=="离魂符" or 技能名称=="失忆符" or 技能名称=="失魂符" then
		if 攻击方.法宝效果.天师符 then
			self.初始几率=40+攻击方.法宝效果.天师符
		else
			self.初始几率=40
		end
	elseif 技能名称=="催眠符" then
		self.初始几率=35
		if 攻击方.奇经八脉.不倦 then
			self.初始几率=55
		end
	elseif 技能名称=="锢魂术" then
		self.初始几率=35
	elseif	技能名称=="冰冻" then
		self.初始几率=50
	elseif 技能名称=="天神护体" then
		return true
	elseif 技能名称=="变身" or 技能名称=="后发制人" or 技能名称=="杀气诀" or 技能名称=="逆鳞" or 技能名称=="楚楚可怜" or 技能名称=="乘风破浪" or 技能名称=="炼气化神" or 技能名称=="普渡众生" or 技能名称=="法术防御" then
		return true
	end
	if 挨打方.法术状态组.乾坤妙法 then 
		return false 
	end
	self.初始几率=self.等级差+self.初始几率+攻击方.修炼数据.法术+math.floor(攻击方.封印命中等级 *1000/攻击方.等级)/100 -math.floor(挨打方.抵抗封印等级 *1000/挨打方.等级)/100--+攻击方.修炼数据.法术
	if 挨打方.奇经八脉.神凝 then
		self.初始几率=self.初始几率-10
	end
	if self.初始几率>95 then 
		self.初始几率=95 
	end
	if self.初始几率<35 then 
		self.初始几率=35 
	end
	if 攻击方.封印必中 then
		self.初始几率=100
	end
	if 攻击方.法宝效果.河图洛书 then
		self.初始几率=self.初始几率+攻击方.法宝效果.河图洛书
	end
	if 挨打方.神迹==2 then
		return false
	end
	if self.初始几率>=math.random(100) and 挨打方.复活==0 then
		return true
	else
		return false
	end
end

function FightControl:死亡处理()
	local alive = {};
	for n=1,#self.参战单位 do
		if self.参战单位[n]~=nil and  self.参战单位[n].当前气血>0 and self.参战单位[n].战斗类型=="角色" then
			alive[self.参战单位[n].玩家id] = 1
		end
	end
	local dead = {};

	for n=1,#self.参战单位 do
		-- print_r(self.参战单位[n])
	if self.参战单位[n]~=nil and self.参战单位[n].队伍id~=0 and self.参战单位[n].当前气血<=0 then
		if	self.参战单位[n].战斗类型=="角色" then
			UserData[self.参战单位[n].玩家id].角色.当前气血=UserData[self.参战单位[n].玩家id].角色.气血上限
			UserData[self.参战单位[n].玩家id].角色.当前魔法=UserData[self.参战单位[n].玩家id].角色.魔法上限
			UserData[self.参战单位[n].玩家id].角色.愤怒=0
			SendMessage(UserData[self.参战单位[n].玩家id].连接id,1003,RoleControl:获取角色气血数据(UserData[self.参战单位[n].玩家id]))
			if self.死亡惩罚 and alive[self.参战单位[n].玩家id]~=1 and dead[self.参战单位[n].玩家id]~=1 then
				RoleControl:死亡处理(1,UserData[self.参战单位[n].玩家id],self.战斗类型,self.参战单位[n].死亡消息)
				dead[self.参战单位[n].玩家id] = 1
			end

		elseif UserData[self.参战单位[n].玩家id]~=nil and self.参战单位[n].战斗类型~="召唤类"  then
			if UserData[self.参战单位[n].玩家id].召唤兽.数据.参战~=0 then
				self.召唤兽id=UserData[self.参战单位[n].玩家id].召唤兽.数据.参战
				UserData[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].当前气血=UserData[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].气血上限
				UserData[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].当前魔法=UserData[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].魔法上限
				if self.死亡惩罚 then
				UserData[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].忠诚=UserData[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].忠诚-5
					if UserData[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].类型~="神兽" then
						UserData[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].寿命=UserData[self.参战单位[n].玩家id].召唤兽.数据[self.召唤兽id].寿命-25
					end
				end
				-- print("出现死亡: "..self.参战单位[n].造型..":"..self.参战单位[n].助战参战)
				-- if self.参战单位[n].助战参战 == nil then
					self.发送信息={
						当前气血=self.参战单位[n].气血上限,
						气血上限=self.参战单位[n].气血上限,
						当前魔法=self.参战单位[n].魔法上限,
						魔法上限=self.参战单位[n].魔法上限
					}
					SendMessage(UserData[self.参战单位[n].玩家id].连接id,2015,self.发送信息)
				-- end
			end


		end
	end
	end
end
function FightControl:强制结束战斗()
	self.死亡惩罚=false
	if self.任务di~=nil and 任务数据[self.任务id]~=nil then
		任务数据[self.任务id].战斗=false
	end
	self:发送结束信息()
 end
 
function FightControl:结束战斗(队伍id)
	self.回合进程="结束回合"
	self.进程时间 = os.time()
	if 任务数据[self.任务id]~=nil then
		任务数据[self.任务id].战斗=false
	end
	self.战斗计时=os.time()-self.战斗计时
	self:死亡处理()
	self:秘制红罗羹处理()
	self:胜利结算(队伍id)
	self:失败结算(队伍id)
end

function FightControl:秘制红罗羹处理()
	if self.战斗类型==100033 or self.战斗类型==100034 or self.战斗类型==100035 or self.战斗类型==100036 or self.战斗类型==100044 or self.战斗类型==100045 or self.战斗类型==100046 then
		return 0
	end
	for n=1,#self.参战玩家 do
		self.秘制红罗羹id=RoleControl:GetTaskID(UserData[self.参战玩家[n].玩家id],"秘制红罗羹")
		if self.秘制红罗羹id~=0 and 任务数据[self.秘制红罗羹id]~=nil then
			任务数据[self.秘制红罗羹id].次数=任务数据[self.秘制红罗羹id].次数-1
			if 任务数据[self.秘制红罗羹id].次数 <=0 then
				RoleControl:取消任务(UserData[self.参战玩家[n].玩家id],self.秘制红罗羹id)
				任务数据[self.秘制红罗羹id]=nil
				SendMessage(UserData[self.参战玩家[n].玩家id].连接id,7,"#y/你的秘制红罗羹效果到期了")
			else
				SendMessage(UserData[self.参战玩家[n].玩家id].连接id,7,"#y/你的秘制红罗羹还可在#r/"..任务数据[self.秘制红罗羹id].次数.."#y/场战斗中生效")
			end
			RoleControl:恢复血魔(UserData[self.参战玩家[n].玩家id],1)
			if UserData[self.参战玩家[n].玩家id].召唤兽.数据.参战~=0 then
				UserData[self.参战玩家[n].玩家id].召唤兽:恢复状态(3,UserData[self.参战玩家[n].玩家id].召唤兽.数据.参战)
				SendMessage(UserData[self.参战玩家[n].玩家id].连接id,2005,UserData[self.参战玩家[n].玩家id].召唤兽:取头像数据())
			end
		end
	end
end

function FightControl:发送结束信息()
	for n=1,#self.观战玩家 do
		if self.观战玩家[n]~=0 and UserData[self.观战玩家[n]] then
			UserData[self.观战玩家[n]].战斗=0
			UserData[self.观战玩家[n]].观战模式=false
			SendMessage(UserData[self.观战玩家[n]].连接id,6007,"66")
		end
	end
	for n=1,#self.参战玩家 do
		if UserData[self.参战玩家[n].玩家id] then
			UserData[self.参战玩家[n].玩家id].战斗=0
			MapControl:更改战斗(self.参战玩家[n].玩家id,false)
			SendMessage(UserData[self.参战玩家[n].玩家id].连接id,2005,UserData[self.参战玩家[n].玩家id].召唤兽:取头像数据())
			RoleControl:刷新装备属性(UserData[self.参战玩家[n].玩家id])
			SendMessage(UserData[self.参战玩家[n].玩家id].连接id,6007,"66")
			UserData[self.参战玩家[n].玩家id].遇怪时间={起始=os.time(),间隔=math.random(8,20)}
			if UserData[self.参战玩家[n].玩家id].下线标记 then
				Network:退出处理(self.参战玩家[n].玩家id)
			end
		end
	end
	if self.飞升序号~=0 then
		SendMessage(UserData[self.进入战斗玩家id].连接id,7,"#y/您当前正在进行飞升挑战，该挑战将连续进行三场战斗")
		FightGet:进入处理(self.进入战斗玩家id,self.飞升序号,"66",1)
	elseif self.渡劫序号~=0 then
		SendMessage(UserData[self.进入战斗玩家id].连接id,7,"#y/您当前正在进行渡劫挑战，该挑战将连续进行三场战斗")
		FightGet:进入处理(self.进入战斗玩家id,self.渡劫序号,"66",1)
	elseif self.战斗类型==100036 and self.战斗失败==false then
		if 活动数据.无限轮回[self.进入战斗玩家id].场次<20 then
			活动数据.无限轮回[self.进入战斗玩家id].场次=活动数据.无限轮回[self.进入战斗玩家id].场次+1
			self.基础积分=活动数据.无限轮回[self.进入战斗玩家id].场次*2
			活动数据.无限轮回[self.进入战斗玩家id].积分=活动数据.无限轮回[self.进入战斗玩家id].积分+self.基础积分*self.死亡人数+math.floor(UserData[self.进入战斗玩家id].角色.等级/10)
			广播队伍消息(self.进入战斗玩家id,7,"#y/您当前的积分为#r/"..活动数据.无限轮回[self.进入战斗玩家id].积分.."#y/积分越高获得的奖励也越高")
			广播队伍消息(self.进入战斗玩家id,7,"#y/当前正在进入第#r/"..活动数据.无限轮回[self.进入战斗玩家id].场次.."#y/次战斗")
			FightGet:进入处理(self.进入战斗玩家id,100036,"66",0)
		else
		TaskControl:完成无限轮回任务(self.进入战斗玩家id)
		end
	end
	self.结束条件=true
end
function FightControl:取符合id(id)
	self.返回id={}
	for n=1,#self.参战单位 do
		if self.参战单位[n].助战 == nil and self.参战单位[n].战斗类型=="角色" and self.参战单位[n].队伍id == id then
			self.返回id[#self.返回id+1]=self.参战单位[n].玩家id
		end
	end
	return self.返回id
 end
function FightControl:宝图遇怪奖励(id组)
	for n=1,#id组 do
	self.添加经验=math.random(2000,5000)
	RoleControl:添加银子(UserData[id组[n]],self.添加经验,"宝图")
	SendMessage(UserData[id组[n]].连接id,9,"#dq/#w/获得"..self.添加经验.."两银子")
	if math.random(100)<=20 then
		EquipmentControl:取随机装备(id组[n],math.random(3,8),true)
		end
	end
 end
function FightControl:添加野外经验(id组)
	self.临时经验=self.地图经验*(self.队伍数量[2]/10)
	self.临时经验=self.队伍数量[1]*0.2*self.临时经验
	local 等级下限,等级上限=取场景等级(self.地图编号)
	for n=1,#id组 do
	self.角色等级=UserData[id组[n]].角色.等级
	self.等级差=取经验差(self.地图等级,self.角色等级)
	if	RoleControl:GetTaskID(UserData[id组[n]],"平定安邦")~=0	then
		if UserData[id组[n]].角色.等级>=等级下限 and UserData[id组[n]].角色.等级<=等级上限 then
		ItemControl:GiveItem(id组[n],"心魔宝珠")
		end
	end
	if	RoleControl:GetTaskID(UserData[id组[n]],"任务链")~=0	then
			if	任务数据[RoleControl:GetTaskID(UserData[id组[n]],"任务链")].分类==2 then
				if UserData[id组[n]].角色.等级>=等级下限 and UserData[id组[n]].角色.等级<=等级上限 then
						if math.random(100)<=5 then
						--EquipmentControl:设置传说物品(id组[n])
						end
				end
			end
	end
	if self.角色等级>self.地图等级 then
	self.等级差=1
	end
	self.添加经验=math.floor(self.临时经验*self.等级差)
	RoleControl:添加经验(UserData[id组[n]],self.添加经验,"野外")
	if math.random(100)<=3 then
		EquipmentControl:取随机装备(id组[n],self.地图物品,false)
	end
	if UserData[id组[n]].召唤兽.数据.参战~=0 then
		self.召唤兽id=UserData[id组[n]].召唤兽.数据.参战
		self.角色等级=UserData[id组[n]].召唤兽.数据[self.召唤兽id].等级
		self.等级差=取经验差(self.地图等级,self.角色等级)
		self.添加经验=math.floor(self.临时经验*4*self.等级差)
		UserData[id组[n]].召唤兽:添加经验(self.添加经验*5,id组[n],self.召唤兽id,1)
		end
	end
 end
function FightControl:胜利结算(队伍id)
	if 队伍id==0 then
		return 0
	else
	self.胜利id组=self:取符合id(队伍id)
	if self.战斗类型==100001 then
		self:添加野外经验(self.胜利id组)
	elseif self.战斗类型==1 then --刷新
		TaskControl:Reward(self.任务id,self.参战玩家[1].玩家id)
	elseif self.战斗类型==2 then -- 环 
		TaskControl:FulfilLinkTask(self.参战玩家[1].玩家id, self.任务id)
	elseif self.战斗类型==100002 then
		TaskControl:完成除暴安良任务(self.任务id)
	elseif self.战斗类型==100050 then
		UserData[任务数据[self.任务id].数字id].押镖时间={起始 = os.time(),间隔=math.random(25, 40)}
	elseif self.战斗类型==100003 then
		TaskControl:完成打图任务(self.任务id)
	elseif self.战斗类型==100004 then
		self:宝图遇怪奖励(self.胜利id组)

	elseif self.战斗类型==100005 then
		TaskControl:完成封妖任务(self.任务id,self.胜利id组)
	elseif self.战斗类型==100006 then
		TaskControl:完成抓鬼任务(self.胜利id组,self.任务id)
	elseif self.战斗类型==100051 then
		TaskControl:完成鬼王任务(self.胜利id组,self.任务id)

	elseif self.战斗类型==100008 then
		TaskControl:完成皇宫飞贼任务(self.胜利id组,self.任务id)
	elseif self.战斗类型==100009 then
		TaskControl:回答科举题目(self.参战玩家[1].玩家id,-1)
	elseif self.战斗类型==100010 then
	TaskControl:完成官职飞贼任务(self.参战玩家[1].玩家id,self.任务id)

	elseif self.战斗类型==100012 then
		TaskControl:完成门派闯关任务(self.胜利id组,self.任务id)
	elseif self.战斗类型==100013 then
		TaskControl:完成任务链(任务数据[self.任务id].id,self.任务id)
	elseif self.战斗类型==100060 then
		TaskControl:完成坐骑任务3(self.任务id)
	elseif self.战斗类型==100061 then
		TaskControl:完成坐骑任务4(self.任务id)
	elseif self.战斗类型==100062 then
		TaskControl:完成法宝任务(self.任务id)
	elseif self.战斗类型==100069 or self.战斗类型==100070 or self.战斗类型==100071 then
		
		TaskControl:完成神器任务(self.胜利id组[1],self.任务id)
	elseif self.战斗类型==100064 then
		TaskControl:完成群雄逐鹿任务(self.胜利id组,self.任务id)
		elseif self.战斗类型==100068 then
		TaskControl:完成帮战怪物任务(self.胜利id组[1],self.任务id)
	elseif self.战斗类型==100014 then
	任务数据[self.任务id].次数=任务数据[self.任务id].次数-1
	UserData[任务数据[self.任务id].id].遇怪时间.起始=os.time()
	if 任务数据[self.任务id].次数<=0 then
		TaskControl:完成师门任务(任务数据[self.任务id].id)
		else
		SendMessage(UserData[任务数据[self.任务id].id].连接id,7,"#y/嘿嘿，换个地方我继续捣乱")
		任务数据[self.任务id].起始=os.time()
		任务数据[self.任务id].战斗=math.random(10,30)
		end
	elseif self.战斗类型==100015 then
		TaskControl:完成铃铛任务(self.胜利id组[1],self.任务id)
	elseif self.战斗类型==100016 then
		UserData[self.参战玩家[1].玩家id].角色.突破=0
		广播消息(9,"#xt/#r/天降祥云，勇者归来。恭喜#g/"..UserData[self.参战玩家[1].玩家id].角色.名称.."#r/在南极仙翁处完成了100级突破挑战，除了获得丰厚的奖励外，等级上限也上升至145级。")

	elseif self.战斗类型==100019 then
		TaskControl:完成大雁塔(self.胜利id组,self.任务id)
	elseif self.战斗类型==100026 then
		TaskControl:完成嘉年华任务(self.胜利id组,self.任务id)
	elseif self.战斗类型==100027 then
		TaskControl:完成嘉年华任务2(self.胜利id组,self.任务id)
	elseif self.战斗类型==100028 then
		TaskControl:完成嘉年华任务3(self.胜利id组,self.任务id)
	elseif self.战斗类型==200002 then
		if self.观战方==self.胜利id组[1] then
		GameActivities:首席争霸战斗处理(self.观战方,self.对战方)
		else
		GameActivities:首席争霸战斗处理(self.对战方,self.观战方)
		end
	elseif self.战斗类型==200005 then
		if self.观战方==self.胜利id组[1] then
		帮派竞赛:战斗处理(self.观战方,self.对战方)
		else
		帮派竞赛:战斗处理(self.对战方,self.观战方)
		end
	elseif self.战斗类型==200008 then
		if self.观战方==self.胜利id组[1] then
		帮派竞赛:迷宫战斗处理(self.观战方,self.对战方)
		else
		帮派竞赛:迷宫战斗处理(self.对战方,self.观战方)
		end
	elseif self.战斗类型==200004 then
		if self.观战方==self.胜利id组[1] then
		GameActivities:比武大会战斗处理(self.观战方,self.对战方)
		else
		GameActivities:比武大会战斗处理(self.对战方,self.观战方)
		end
	elseif self.战斗类型==8002 then
		TaskControl:完成芭蕉木妖(self.胜利id组,self.任务id)
	elseif self.战斗类型==8003 then
		TaskControl:完成三妖挑战(self.胜利id组,1)
	elseif self.战斗类型==8004 then
		TaskControl:完成三妖挑战(self.胜利id组,2)
	elseif self.战斗类型==8005 then
		TaskControl:完成三妖挑战(self.胜利id组,3)
	elseif self.战斗类型==8006 then
		TaskControl:完成鬼祟小妖(self.胜利id组,self.任务id)
	elseif self.战斗类型==8007 then
		TaskControl:完成国王挑战(self.胜利id组,1)
	elseif self.战斗类型==8008 then
		TaskControl:完成国王挑战(self.胜利id组,2)
	elseif self.战斗类型==8009 then
		TaskControl:完成贡品(self.胜利id组,self.任务id)
	elseif self.战斗类型==8010 then
		TaskControl:完成三清天尊(self.胜利id组,self.任务id)
	elseif self.战斗类型==8011 then
		TaskControl:完成双不动(self.胜利id组,self.任务id)
	elseif self.战斗类型==8012 then
		TaskControl:完成三妖大仙(self.胜利id组,self.任务id)
	elseif self.战斗类型==8012 then
		TaskControl:完成三妖大仙(self.胜利id组,self.任务id)
	elseif self.战斗类型==100033 then
		self.飞升序号=100034
	elseif self.战斗类型==100034 then
		self.飞升序号=100035
	elseif self.战斗类型==100035 then
		RoleControl:飞升处理(UserData[self.进入战斗玩家id])
	elseif self.战斗类型==100029 or self.战斗类型==100041 then
	TaskControl:完成地妖星(self.胜利id组,self.任务id)
	elseif self.战斗类型==100031 then
 TaskControl:完成蚩尤挑战任务(self.进入战斗玩家id,self.任务id)
	elseif self.战斗类型==100044 then
		self.渡劫序号=100045
	elseif self.战斗类型==100045 then
		self.渡劫序号=100046
	elseif self.战斗类型==100046 then
		RoleControl:渡劫处理(UserData[self.进入战斗玩家id])
	elseif self.战斗类型==100049 then
		TaskControl:完成贼王任务(self.任务id,self.胜利id组)
	elseif self.战斗类型==200003 then
		for n=1,#self.胜利id组 do
		if self.发起id==self.胜利id组[n] then
		if 天罚数据表[self.胜利id组[n]]==nil then

		天罚数据表[self.胜利id组[n]]=0
		end
		天罚数据表[self.胜利id组[n]]=天罚数据表[self.胜利id组[n]]+1
		RoleControl:添加系统消息(UserData[self.胜利id组[n]],"#h/你因为参加主动发起的pk战斗而被扣除了阴德值，阴德值过低会遭遇天罚。阴德值在每日中午12点刷新")
		end
		end
	elseif self.战斗类型==200006 then
		if self.观战方==self.胜利id组[1] then
			GameActivities:华山论剑奖励(self.观战方,self.对战方)
		else
			GameActivities:华山论剑奖励(self.对战方,self.观战方)
			end

		elseif self.战斗类型==888890 then
		TaskControl:完成天罡星69任务(self.任务id,self.胜利id组)
		elseif self.战斗类型==888891 then
		TaskControl:完成天罡星2任务(self.任务id,self.胜利id组)
		elseif self.战斗类型==888892 then
		TaskControl:完成天罡星3任务(self.任务id,self.胜利id组)
		elseif self.战斗类型==888893 then
		TaskControl:完成天罡星4任务(self.任务id,self.胜利id组)
		elseif self.战斗类型==888894 then
		TaskControl:完成天罡星5任务(self.任务id,self.胜利id组)
		elseif self.战斗类型==888895 then
		TaskControl:完成天罡星6任务(self.任务id,self.胜利id组)
	elseif self.战斗类型==100038	then
	TaskControl:完成天罡星(self.胜利id组,self.任务id)
	elseif self.战斗类型==100039 then
	任务数据[self.任务id].战斗序列[任务数据[self.任务id].进程]=true
	elseif self.战斗类型==100040 then
		TaskControl:完成玄武堂任务(self.任务id)

	elseif self.战斗类型==100043 then
		TaskControl:完成师门守卫(self.任务id,self.胜利id组)
	elseif self.战斗类型==100048 then
		TaskControl:完成嘉年华(self.任务id,self.胜利id组)
	elseif self.战斗类型==100059 then
		TaskControl:完成天佑星(self.胜利id组,self.任务id)
	end
	end
	self.回合进程="结束回合1"
	self.进程时间 = os.time()
end
 
function FightControl:失败结算(队伍id)
	if self.队伍区分[1]==队伍id then
		self.失败id=self.队伍区分[2]
	else
		self.失败id=self.队伍区分[1]
	end
	if self.失败id==0 then
		return 0
	end
	self.战斗失败=true
	if self.战斗类型==100009 then
		TaskControl:回答科举题目(self.参战玩家[1].玩家id,0)
	elseif self.战斗类型==100014 then
		任务数据[self.任务id].起始=os.time()
		任务数据[self.任务id].战斗=math.random(10,30)
	elseif self.战斗类型==100050 then
		for n = 1, 20 do
			if UserData[任务数据[self.任务id].数字id].角色.道具.包裹[n] ~= nil and UserData[任务数据[self.任务id].数字id].物品[UserData[任务数据[self.任务id].数字id].角色.道具.包裹[n]] ~= nil then
				if 任务数据[self.任务id].道具 == UserData[任务数据[self.任务id].数字id].物品[UserData[任务数据[self.任务id].数字id].角色.道具.包裹[n]].名称 then
						UserData[任务数据[self.任务id].数字id].物品[UserData[任务数据[self.任务id].数字id].角色.道具.包裹[n]] = nil
						UserData[任务数据[self.任务id].数字id].角色.道具.包裹[n] = nil
						break
				end
			end
		end
		RoleControl:取消任务(UserData[任务数据[self.任务id].数字id],RoleControl:GetTaskID(UserData[任务数据[self.任务id].数字id],"押镖"))
		SendMessage(UserData[任务数据[self.任务id].数字id].连接id, 7, "#y/任务失败")

	elseif	self.战斗类型==1 or self.战斗类型==100006 or self.战斗类型==100051 or self.战斗类型==100029 or self.战斗类型==100037 or self.战斗类型==100002 or self.战斗类型==100011 or self.战斗类型==100049 or self.战斗类型==100003 or self.战斗类型==888888 or self.战斗类型==888890 or self.战斗类型==888891 or self.战斗类型==100043 or self.战斗类型==100038 or self.战斗类型==100003 or self.战斗类型==100005 or self.战斗类型==100059 or self.战斗类型==888892 or self.战斗类型==100068 or self.战斗类型==888893 or self.战斗类型==888894 or self.战斗类型==888895 then
		任务数据[self.任务id].战斗=false
		MapControl:NPC取消战斗(任务数据[self.任务id].地图编号,self.任务id)
		self.组合名称=""
		for n=1,#self.参战玩家 do
			if UserData[self.参战玩家[n].玩家id] then
				self.组合名称=self.组合名称..UserData[self.参战玩家[n].玩家id].角色.名称
				if n~=#self.参战玩家 then
					self.组合名称=self.组合名称.."、"
				end
			end
		end
		广播消息("#xt/#y/据可靠消息#g/"..self.组合名称.."#y/不自量力的去挑战#r/"..任务数据[self.任务id].名称.."#y/，结果被打的爹妈都不认识了,加强自身修炼,强化技能,锻造装备,打造宠物.来日再战")
	elseif self.战斗类型==100036	then
		广播队伍消息(self.进入战斗玩家id,7,"#y/当前完成#r/"..活动数据.无限轮回[self.进入战斗玩家id].场次.."#y/次战斗")
		广播队伍消息(self.进入战斗玩家id,7,"#y/当前积分为#r/"..活动数据.无限轮回[self.进入战斗玩家id].积分.."#y/点积分")
		TaskControl:完成无限轮回任务(self.进入战斗玩家id,self.任务id)
	elseif	self.战斗类型==100041 or self.战斗类型==100063	then
		任务数据[self.任务id].战斗=false
		self.组合名称=""
		for n=1,#self.参战玩家 do
			self.组合名称=self.组合名称..UserData[self.参战玩家[n].玩家id].角色.名称
			if n~=#self.参战玩家 then
				self.组合名称=self.组合名称.."、"
			end
		end
	end
	self.回合进程="结束回合1"
	self.进程时间 = os.time()
end

return FightControl
