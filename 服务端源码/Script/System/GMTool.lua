-- @Author: 作者QQ381990860
-- @Date:   2021-08-13 19:47:33
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-24 01:28:41

local GMTool = class()

function GMTool:数据处理(num,arg)
  if __gge.isdebug then
    print("GMTool",num,arg)
  end
  管理日志=ReadFile("管理日志.txt")
  管理日志 =管理日志.."\n"..os.date("[%Y年%m月%d日%X]:")..arg
  WriteFile("管理日志.txt", 管理日志)
  if num==1000 then
       self:GM网关开关(arg) 
  elseif num == 1001 then
      self:GM玩家管理(table.loadstring(arg)) 
  elseif num == 1002 then
      self:GM修改参数(table.loadstring(arg))  
  elseif num == 1003 then
      self:GM赠送称谓(table.loadstring(arg))
  elseif num == 1004 then
      self:GM充值系统(table.loadstring(arg)) 
  elseif num == 1005 then
      self:GM定制装备(table.loadstring(arg)) 
  elseif num == 1006 then
      self:GM定制灵饰(table.loadstring(arg)) 
  elseif num == 1007 then
      self:GM定制召唤兽(table.loadstring(arg)) 
  elseif num == 1008 then
      self:GM地图系统(table.loadstring(arg))
  elseif num == 1009 then  
    self:GM自定义刷新怪(arg)
  elseif num == 1010 then 
      self:GM自定义物品(table.loadstring(arg))   
  elseif num == 1011 then --转发消息  
    self:GM转发消息(table.loadstring(arg))   
    elseif num == 1012 then --转发消息  
    self:CDK兑换(table.loadstring(arg)) 
    elseif num == 1013 then
      self:GM定制召唤兽装备(table.loadstring(arg))
  end                        
end
function GMTool:CDK兑换(data)
  local 仙玉比例 =Config.cdk仙玉
  if  UserData[data[2]] == nil then
         发送网关消息(string.format("%d角色不存在或未上线充值CDK失败",data[2]))
         管理日志=ReadFile("管理日志.txt")
        管理日志 =管理日志.."\n"..os.date("[%Y年%m月%d日%X]:")..arg
        WriteFile("管理日志.txt", string.format("%d角色不存在或未上线充值CDK失败",data[2]))
  else
      if data[1]=="普通抽奖" or data[1]=="中等抽奖" or   data[1]=="极品抽奖" then
                local cj = ""
                local 奖励参数 = math.random(1000)   
               for i=1,#Lottery[data[1]] do
                      if 奖励参数 >=Lottery[data[1]][i].几率[1] and 奖励参数 <=Lottery[data[1]][i].几率[2] then
                        cj= Lottery[data[1]][i].物品
                      end
                  if cj ~= "" then 
                    local 数量,等级=nil
                    if Lottery[data[1]][i].等级 then
                        等级=math.random(Lottery[data[1]][i].等级[1], Lottery[data[1]][i].等级[2])
                    end
                    if Lottery[data[1]][i].数量 then
                        数量=math.random(Lottery[data[1]][i].数量[1], Lottery[data[1]][i].数量[2])
                    end
                    ItemControl:GiveItem(data[2],cj,等级,Lottery[data[1]][i].技能,数量)
                    广播消息("#hd/".."#S/(CDK抽奖)".."#R/ "..UserData[data[2]].角色.名称.."#Y/喜从天降在新年CDK抽奖中，获得了GM奖励的#g/"..cj.."#"..math.random(110))
                      break
                  end
               end
          if Config.cdk抽奖仙玉 >0 then 
            RoleControl:添加仙玉(UserData[data[2]],Config.cdk抽奖仙玉,"cdk兑换")  
          end
      elseif data[1]=="银子1E" then
        RoleControl:添加银子(UserData[data[2]],100000000,"cdk兑换"..data[1])
      elseif data[1]=="银子5E" then
        RoleControl:添加银子(UserData[data[2]],500000000,"cdk兑换"..data[1])
      elseif data[1]=="银子10E" then
        RoleControl:添加银子(UserData[data[2]],1000000000,"cdk兑换"..data[1])
      elseif data[1]=="银子20E" then
        RoleControl:添加银子(UserData[data[2]],2000000000,"cdk兑换"..data[1])
      elseif data[1]=="银子50E" then
        RoleControl:添加银子(UserData[data[2]],5000000000,"cdk兑换"..data[1])
      elseif data[1]=="仙玉6元" then
         RoleControl:添加仙玉(UserData[data[2]],仙玉比例*6,"cdk兑换"..data[1])
         UserData[data[2]].角色.vip.充值=UserData[data[2]].角色.vip.充值+仙玉比例*6
      elseif data[1]=="仙玉50元" then
         RoleControl:添加仙玉(UserData[data[2]],仙玉比例*50,"cdk兑换"..data[1])
         UserData[data[2]].角色.vip.充值=UserData[data[2]].角色.vip.充值+仙玉比例*50
      elseif data[1]=="仙玉100元" then
         RoleControl:添加仙玉(UserData[data[2]],仙玉比例*100,"cdk兑换"..data[1])
         UserData[data[2]].角色.vip.充值=UserData[data[2]].角色.vip.充值+仙玉比例*100
      elseif data[1]=="仙玉500元" then
         RoleControl:添加仙玉(UserData[data[2]],仙玉比例*500,"cdk兑换"..data[1])
         UserData[data[2]].角色.vip.充值=UserData[data[2]].角色.vip.充值+仙玉比例*500
      elseif data[1]=="仙玉1000元" then
         RoleControl:添加仙玉(UserData[data[2]],仙玉比例*1000,"cdk兑换"..data[1])
         UserData[data[2]].角色.vip.充值=UserData[data[2]].角色.vip.充值+仙玉比例*1000
      end 
      发送网关消息(string.format("%d角色兑换CDK成功",data[2]))
  end 
end
function GMTool:GM转发消息(data)

  if  UserData[data[2]] == nil then
         发送网关消息(string.format("%d角色不存在或未上线",data[1]))
  else 
       SendMessage(UserData[data[2]].连接id,7,"#Y/"..data[1])
  end 
end
function GMTool:GM自定义物品(data)
  if   data[1] == "全服" then

     for k,v in pairs(UserData) do
           for i=3,6 do
            if data[i] =="" then
               data[i] =nil
            elseif i == 3 or i==5 or i==6 then
               data[i] =tonumber(data[i])
            end
          end
          if not v.假人  and v.角色.等级 >68 then
          ItemControl:GiveItem(k,data[2],data[3],data[4],data[5],data[6])
          end
          广播消息(9,"#xt/#g/ " .. v.角色.名称 .. "#y/获得了游戏管理员赠送的红包，打开一看竟是#r/" .. data[2] .. "。#S/玩家热情不断,GM福利嗨翻天".."#"..math.random(110))
      end
    发送网关消息(string.format("全服69级以上玩家发送%s成功",data[2]))
  else
    if  UserData[data[1]] == nil then
           发送网关消息(string.format("%d角色不存在或未上线",data[1]))
    elseif ItemData[data[2]] then
         for i=3,6 do
            if data[i] =="" then
               data[i] =nil
            elseif i == 3 or i==5 or i==6 then
               data[i] =tonumber(data[i])
            end
          end
          ItemControl:GiveItem(data[1],data[2],data[3],data[4],data[5],data[6])
          发送网关消息(string.format("发送%s成功",data[2]))
    else
      发送网关消息(string.format("数据表未定义%s物品",data[2]))
    end
  end
end
function GMTool:GM自定义刷新怪(data)
  if RefreshMonsterData[data] then
    TaskControl:LoadRefreshMonster(RefreshMonsterData[data],data)
    发送网关消息(string.format("%s刷新成功",data))
  else 
    发送网关消息(string.format("数据表未定义%s刷新怪任务",data))
  end 
end
function GMTool:GM修改参数(...)
    local num,text=unpack(...)
    if text =="调整经验" then
        ServerConfig.经验获得率= tonumber(num)
        发送网关消息("当前服务端的经验获得率改为"..num)
         广播消息("当前服务器的经验倍率为："..ServerConfig.经验获得率);
         发送游戏公告("当前服务器的经验倍率为："..ServerConfig.经验获得率);

    elseif text =="限制等级" then
      ServerConfig.限制等级= tonumber(num)
          发送网关消息("当前服务端的限制等级改为"..num)
         广播消息("当前服务器的限制等级为："..ServerConfig.限制等级);
         发送游戏公告("当前服务器的限制等级为："..ServerConfig.限制等级);
     
    end
end   
function GMTool:GM地图系统(...)
  local arg=...
  local MapNumber = tonumber(arg[2])
    if  arg[1] == "更新数据" then

    elseif arg[1] == "清除摆摊" then
      for n, v in pairs(MapControl.MapData[MapNumber].玩家) do
         if UserData[n] and UserData[n].摆摊 then
              摊位数据[n]=nil
              UserData[n].摆摊 = nil
              MapControl.MapData[MapNumber].摆摊人数=MapControl.MapData[MapNumber].摆摊人数-1
              SendMessage(UserData[n].连接id, 7, "#y/收摊回家玩老婆咯！")
              SendMessage(UserData[n].连接id, 20008,"")
              MapControl:更新摊位(n, "", MapNumber)
         end
      end
    elseif arg[1] == "清除假人" then
        假人移动 =false
        协程列表["假人移动"]=nil
        for n, v in pairs(MapControl.MapData[MapNumber].玩家) do
           if UserData[n] and UserData[n].假人 then
                  if UserData[n].摆摊 then
                      MapControl.MapData[MapNumber].摆摊人数=MapControl.MapData[MapNumber].摆摊人数-1
                  end
                  MapControl:移除玩家(UserData[n].地图, n)
                  MapControl.MapData[MapNumber].假人=MapControl.MapData[MapNumber].假人-1
                  UserData[n]=nil
           end
        end
         collectgarbage("collect")
    elseif arg[1] == "清除怪物" then
      for k,v in pairs(MapControl.MapData[MapNumber].单位组) do
           for n, s in pairs(MapControl.MapData[MapNumber].玩家) do
            if UserData[n] then
                SendMessage(UserData[n].连接id,1018,v.任务id)
             end
          end
          MapControl.MapData[MapNumber].单位组[k]=nil 
      end
    elseif arg[1] == "清除玩家" then
        for n, v in pairs(MapControl.MapData[MapNumber].玩家) do
         if UserData[n]  then
            SendMessage(UserData[n].连接id,99998,"GM清空地图处理")
         end
        end
    elseif arg[1] == "添加假人" then
      if not MapControl.MapData[MapNumber].坐标 then
        发送网关消息("这个地图不支持刷出假人,请尝试在主城刷新，如果有特别要求请联系作者")
         return 
      end
        local 玩家账号 =取目录下名称(ServerDirectory.."玩家信息")
        if arg[3] > #玩家账号 then
          arg[3]=#玩家账号
        end
        协程列表["假人生成"]=nil
        local co = coroutine.wrap(
          function(玩家账号,arg,MapNumber)
              local ls= 0;
              for i=1,arg do
                  Network:假人进入游戏处理({账号=玩家账号[i],编号=1,ip="127.0.0.1",id=0},MapNumber)
                  ls=ls+1
                  if ls ==30 then
                      ls =0
                       SendMessage(1,993,cjson.encode({
                                Role=tostring(取表数量(MapControl.MapData[MapNumber].玩家)),
                                Monster=tostring(#MapControl.MapData[MapNumber].单位组),
                                Model=tostring(MapControl.MapData[MapNumber].假人),
                                Stall=tostring(MapControl.MapData[MapNumber].摆摊人数),
                                Number=tostring(MapControl.MapData[MapNumber].编号) 
                                }))
                        coroutine.yield()
                  end
              end
              return true
          end

          )
        if not co(玩家账号,arg[3],MapNumber) then
              协程列表["假人生成"]= co
        end

         假人移动=true
    end
      SendMessage(1,993,cjson.encode({
      Role=tostring(取表数量(MapControl.MapData[MapNumber].玩家)),
      Monster=tostring(#MapControl.MapData[MapNumber].单位组),
      Model=tostring(MapControl.MapData[MapNumber].假人),
      Stall=tostring(MapControl.MapData[MapNumber].摆摊人数),
      Number=tostring(MapControl.MapData[MapNumber].编号) 
      }))
     发送网关消息(string.format("地图%s成功",arg[1]))
end

function GMTool:GM充值系统(...)

  local arg=...
    if  UserData[arg[1]] == nil then
         发送网关消息(string.format("%d角色不存在或未上线",arg[1]))
         return
    elseif arg[2]=="充值仙玉" then 
                   local 账号=id取账号(arg[1])
                   local 临时日志 =  ReadFile("玩家信息/账号" .. 账号 .. "/"..arg[1].."/充值记录.txt")
                    临时日志 = 临时日志 .. "\n仙玉充值：" .. os.date("%Y-%m-%d") .. "充值" .. arg[3] .. "点仙玉，网关操作" 
                    WriteFile("玩家信息/账号" .. 账号 .. "/"..arg[1].."/充值记录.txt", 临时日志)
                    RoleControl:添加仙玉(UserData[arg[1]],arg[3],"网关充值")
                    UserData[arg[1]].角色.vip.充值=UserData[arg[1]].角色.vip.充值+arg[3]
                    SendMessage(UserData[arg[1]].连接id, 7, "#y/您充值的#r/" .. arg[3] .. "#y/点仙玉已经到账")

    elseif arg[2]=="充值银子" then 
                     RoleControl:添加银子(UserData[arg[1]],arg[3],"GM充值")
                    SendMessage(UserData[arg[1]].连接id, 7, "#y/您充值的#r/" .. arg[3] .. "#y/点银子已经到账")
    elseif arg[2]=="充值储备" then 
                    RoleControl:添加储备(UserData[arg[1]],arg[3],"GM充值")
                    SendMessage(UserData[arg[1]].连接id, 7, "#y/您充值的#r/" .. arg[3] .. "#y/点储备已经到账")
    elseif arg[2]=="充值门贡" then 
                    UserData[arg[1]].角色.门贡=UserData[arg[1]].角色.门贡+arg[3]
                    SendMessage(UserData[arg[1]].连接id, 7, "#y/您充值的#r/" .. arg[3] .. "#y/点门贡已经到账")
    elseif arg[2]=="充值帮贡" then 
                    UserData[arg[1]].角色.帮贡=UserData[arg[1]].角色.帮贡+arg[3]
                    SendMessage(UserData[arg[1]].连接id, 7, "#y/您充值的#r/" .. arg[3] .. "#y/点帮贡已经到账")
    elseif arg[2]=="充值经验" then 
                  RoleControl:添加经验(UserData[arg[1]],arg[3], "GM充值")
                SendMessage(UserData[arg[1]].连接id, 7, "#y/您充值的#r/" .. arg[3] .. "#y/点经验已经到账")

   else
        RoleControl["添加"..arg[2]](RoleControl,UserData[arg[1]],arg[3])
                SendMessage(UserData[arg[1]].连接id, 7, "#y/您充值的#r/" .. arg[3] .. "#y/点"..arg[2].."已经到账")
   end


    发送网关消息(string.format("角色%d%s%s",arg[1],arg[2],arg[3]))
end
function GMTool:GM赠送称谓(...)
  local arg=...
     if  UserData[arg[1]] == nil then
         发送网关消息(string.format("%d角色不存在或未上线",arg[1]))
    elseif RoleControl:检查称谓(UserData[arg[1]],arg[2]) then
      发送网关消息(UserData[arg[1]].角色.名称.."已有称谓无法添加")
    else
      RoleControl:添加称谓(UserData[arg[1]],arg[2])
      SendMessage(UserData[arg[1]].连接id, 7, "#y/你获得GM赠送的"..arg[2].."称谓")
      发送网关消息(UserData[arg[1]].角色.名称.."赠送"..arg[2].."称谓成功")
    end
end
function GMTool:GM定制装备(...)
  local arg=...
     if  UserData[arg[1]] == nil then
        发送网关消息(string.format("%d角色不存在或未上线",arg[1]))
    else
        local 装备数据=分割文本(arg[2],"*-*")
     
        EquipmentControl:定制装备(arg[1],装备数据[1],装备数据[2],装备数据[3],装备数据[3],装备数据[4],装备数据[5],装备数据[6],装备数据[7],装备数据[8])
        发送网关消息(string.format("%d发送装备成功",arg[1]))          
    end
end
function GMTool:GM定制召唤兽装备(...)
  local arg=...
     if  UserData[arg[1]] == nil then
        发送网关消息(string.format("%d角色不存在或未上线",arg[1]))
    else
        local 装备数据=分割文本(arg[2],"*-*")
        EquipmentControl:定制召唤兽装备(arg[1],装备数据[1],装备数据[2],装备数据[3],装备数据[4],装备数据[5],装备数据[6],装备数据[7])
        发送网关消息(string.format("%d发送装备成功",arg[1]))          
    end
end


function GMTool:GM定制召唤兽(...)
  local arg=...
     if  UserData[arg[1]] == nil then
        发送网关消息(string.format("%d角色不存在或未上线",arg[1]))
    else
        local 数据=分割文本(arg[2],"*-*")
        if BBData[数据[2]] ==nil then 
            发送网关消息(string.format("召唤兽数据表未定义%s召唤兽",数据[2]))
            return 
        end
        UserData[arg[1]].召唤兽:定制召唤兽(数据)
        发送网关消息(string.format("%d召唤兽发送成功",arg[1]))   
        SendMessage(UserData[arg[1]].连接id, 7, "#y/你获得GM赠送的神级召唤兽")       
    end
end
function GMTool:GM定制灵饰(...)
  local arg=...
     if  UserData[arg[1]] == nil then
        发送网关消息(string.format("%d角色不存在或未上线",arg[1]))
    else
        local 装备数据=分割文本(arg[2],"*-*")
        EquipmentControl:定制灵饰(arg[1],装备数据[1],装备数据[2],装备数据[3],装备数据[4],装备数据[5],装备数据[6])
        发送网关消息(string.format("%d发送装备成功",arg[1]))          
    end
end
function GMTool:GM玩家管理(...)
  local arg=...
     if arg[2]=="解封(账号)" then
        if  file_exists("玩家信息/账号" .. arg[1] .. "/账号.txt") == false then
            发送网关消息(string.format("%s账号不存在",arg[1]))
        else 
            f函数.写配置(ServerDirectory.."玩家信息/账号" .. arg[1] .. "/账号.txt", "账号信息", "封禁", 0)
            发送网关消息(string.format("%s账号解封成功",arg[1]))
        end
      return 
     end

     arg[1] =arg[1]+0
     if  UserData[arg[1]] == nil then
         发送网关消息(string.format("%d角色不存在或未上线",arg[1]))
    elseif arg[2]=="强制下线" then 
                UserData[arg[1]] = nil

                发送网关消息(string.format("%d退出了游戏",arg[1]))
    elseif arg[2]=="取ID账号" then 
      发送网关消息(string.format("%d角色账号为%s",arg[1],id取账号(arg[1])))
    elseif arg[2]=="退出战斗" then 
            if UserData[arg[1]].战斗 == 0 then
                 发送网关消息(string.format("%d未处于战斗中",arg[1]))
            else
                FightGet.战斗盒子[UserData[arg[1]].战斗]:强制结束战斗()
                FightGet.战斗盒子[UserData[arg[1]].战斗] = nil
                发送网关消息(string.format("%d退出了战斗",arg[1]))
            end
    elseif arg[2]=="封禁账号" then 
        封禁账号(UserData[arg[1]],"GM封禁")
        发送网关消息(string.format("%d封禁成功",arg[1]))
    elseif arg[2]=="网关封禁账号" then 
        封禁账号(UserData[arg[1]],"系统封禁")
        发送网关消息(string.format("%d因拦截封包封禁成功",arg[1]))
    elseif arg[2]=="清除摆摊" then 
        摆摊处理类:数据处理(arg[1], 12)
    elseif arg[2]=="清空背包" then 
        RoleControl:清空包裹(UserData[arg[1]],arg[1])
        发送网关消息(string.format("%d清除背包成功",arg[1]))
    elseif arg[2]=="限制喊话" then 
        UserData[arg[1]].角色.限制喊话=true
        发送网关消息(string.format("%d限制了喊话功能",arg[1]))
    elseif arg[2]=="解除限制喊话" then 
        UserData[arg[1]].角色.限制喊话=false
        发送网关消息(string.format("%d解除了限制了喊话功能",arg[1]))
    elseif arg[2]=="删除角色" then
      RoleControl:删除角色(UserData[arg[1]].角色,arg[1])
       发送网关消息(string.format("%d角色删除成功",arg[1]))
    end 
end
function GMTool:GM网关开关(data)
  if data =="当前监听关闭" then
    当前监听=false
    发送网关消息(data)
  elseif data =="队伍监听关闭" then
    队伍监听=false
    发送网关消息(data)
  elseif data =="世界监听关闭" then
    世界监听=false
    发送网关消息(data)
  elseif data =="帮派监听关闭" then
    帮派监听=false
    发送网关消息(data)
  elseif data =="门派监听关闭" then
    门派监听=false
    发送网关消息(data)
  elseif data =="传闻监听关闭" then
    传闻监听=false
    发送网关消息(data)
  elseif data =="当前监听开启" then
    当前监听=true
    发送网关消息(data)
  elseif data =="队伍监听开启" then
    队伍监听=true
    发送网关消息(data)
  elseif data =="世界监听开启" then
    世界监听=true
    发送网关消息(data)
  elseif data =="帮派监听开启" then
    帮派监听=true
    发送网关消息(data)
  elseif data =="门派监听开启" then
    门派监听=true
    发送网关消息(data)
  elseif data =="传闻监听开启" then
    传闻监听=true
    发送网关消息(data)
  elseif data =="刷出天罡" then 
  TaskControl:刷新天罡星()
  TaskControl:刷出天罡星69()
      TaskControl:刷出天罡星2()
      TaskControl:刷出天罡星3()
      TaskControl:刷出天罡星4() 
      TaskControl:刷出天罡星5() 
      TaskControl:刷出天罡星6() 
      发送网关消息(string.format("%s成功",data))
  elseif data =="刷出地煞" then 
  TaskControl:刷新地妖星()   
      发送网关消息(string.format("%s成功",data))
  elseif data =="更新全部数据" then
     CardData=ReadExcel("变身卡数据",ServerConfig.key)
    MapData=ReadExcel("地图数据",ServerConfig.key)
    TransferData=ReadExcel("传送圈数据",ServerConfig.key)
    RefreshMonsterData=ReadExcel("刷新怪数据",ServerConfig.key)
    RefreshMonsterTime={}
  for k,v in pairs(RefreshMonsterData) do
       RefreshMonsterTime[k] ={v.刷新时间,v.日期}
  end 
    ModelData=ReadExcel("模型数据",ServerConfig.key)
    SkillData=ReadExcel("技能数据",ServerConfig.key)
    Config=ReadExcel("Config",ServerConfig.key)[f函数.读配置(ServerDirectory .. "config.ini", "mainconfig", "ft")+0]
    ItemData=ReadExcel("物品数据",ServerConfig.key)
    NpcData=ReadExcel("NPC数据",ServerConfig.key)
    QuretionBank =ReadExcel("题库数据",ServerConfig.key)
    BBData=ReadExcel("召唤兽数据",ServerConfig.key)
    MonsterData=ReadExcel("怪物数据",ServerConfig.key)
    ShopData =ReadExcel("商城数据",ServerConfig.key)
    LinkTask =ReadExcel("环任务数据",ServerConfig.key)
    WeaponModel=ReadExcel("光武拓印",ServerConfig.key)
    Reward =ReadExcel("首服奖励",ServerConfig.key)
    LinkTaskDialogue={}
    LinkTaskClear={}
    LinkTaskEvent1={}
    LinkTaskEvent2={}
    LinkTaskEvent3={}
    for k,v in pairs(LinkTask) do
        LinkTaskDialogue[v.事件]=k
        LinkTaskClear["取消"..k]=k
        LinkTaskEvent1["给予道具("..k..")"]=k
        LinkTaskEvent2["好的，我帮你。("..k..")"]=k
        LinkTaskEvent3["开始挑战("..k..")"]=k
    end
    LabelData=ReadExcel("称谓数据",ServerConfig.key)
    古玩数据={}

       local tempLottery=ReadExcel("抽奖数据",ServerConfig.key)
   Lottery={普通抽奖={},中等抽奖={},极品抽奖={}}
   for key,value in  pairs(tempLottery) do
     
      for k,v in  pairs(Lottery) do
          if value.分类 == k then 
            table.insert(Lottery[k],value)
            break;
          end
      end
   end 
    merchandise={银子={},法宝={},祥瑞={},仙玉={},传音={},孩子={},活动积分={},知了积分={},天罡积分={},单人积分={},成就积分={},特殊积分={},锦衣={},光环={},脚印={},活跃度={},比武积分={},副本积分={},地煞积分={},定制={},召唤兽={}}
   for key,value in  pairs(ShopData) do
      for k,v in  pairs(merchandise) do
          if value.分类 == k then 
            table.insert(merchandise[k],key)
            break;
          end
      end
      if value.分类 == "定制" then
        ShopData[key].锦衣=value.名称
        ShopData[key].造型=value.技能
        ShopData[key].技能=nil
        ShopData[key].名称="GM定制礼包"
        ShopData[key].类型="定制锦衣"
      elseif value.分类 == "召唤兽" then
        ShopData[key] = AddPet(value.名称,value.技能,value.价格)
      else
        ShopData[key] =AddItem(value.名称,value.等级,value.技能,nil,value.价格,nil,value.图标,nil,value.限时)
      end 
        ShopData[key].分类 = value.分类
   end
    发送网关消息(string.format("%s成功",data))
  elseif data =="更新变身卡数据" then
         CardData=ReadExcel("变身卡数据",ServerConfig.key)
         发送网关消息(string.format("%s成功",data))
  elseif data =="更新地图数据" then
       MapData=ReadExcel("地图数据",ServerConfig.key)
       发送网关消息(string.format("%s成功",data))
  elseif data =="更新传送圈数据" then
       TransferData=ReadExcel("传送圈数据",ServerConfig.key)
       发送网关消息(string.format("%s成功",data))
  elseif data =="更新刷新怪数据" then
      RefreshMonsterData=ReadExcel("刷新怪数据",ServerConfig.key)
      RefreshMonsterTime={}
  for k,v in pairs(RefreshMonsterData) do
       RefreshMonsterTime[k] ={v.刷新时间,v.日期}
  end 
      发送网关消息(string.format("%s成功",data)) 
  elseif data =="更新技能数据" then
        SkillData=ReadExcel("技能数据",ServerConfig.key)
         发送网关消息(string.format("%s成功",data)) 
  elseif data =="更新物品数据" then
        ItemData=ReadExcel("物品数据",ServerConfig.key)
        发送网关消息(string.format("%s成功",data))
  elseif data =="更新NPC数据" then
     NpcData=ReadExcel("NPC数据",ServerConfig.key)
     发送网关消息(string.format("%s成功",data))
  elseif data =="更新召唤兽数据" then
     BBData=ReadExcel("召唤兽数据",ServerConfig.key)
     发送网关消息(string.format("%s成功",data))
  elseif data =="更新商城数据" then
    ShopData =ReadExcel("商城数据",ServerConfig.key)
    merchandise={银子={},法宝={},祥瑞={},仙玉={},传音={},孩子={},活动积分={},知了积分={},天罡积分={},单人积分={},成就积分={},特殊积分={},锦衣={},光环={},脚印={},活跃度={},比武积分={},副本积分={},地煞积分={},定制={},召唤兽={}}
   for key,value in  pairs(ShopData) do
      for k,v in  pairs(merchandise) do
          if value.分类 == k then 
            table.insert(merchandise[k],key)
            break;
          end
      end
      if value.分类 == "定制" then
        ShopData[key].锦衣=value.名称
        ShopData[key].造型=value.技能
        ShopData[key].技能=nil
        ShopData[key].名称="GM定制礼包"
        ShopData[key].类型="定制锦衣"
      elseif value.分类 == "召唤兽" then
        ShopData[key] = AddPet(value.名称,value.技能,value.价格)
      else
        ShopData[key] =AddItem(value.名称,value.等级,value.技能,nil,value.价格,nil,value.图标,nil,value.限时)
      end 
        ShopData[key].分类 = value.分类
   end

  elseif data =="更新怪物数据" then
     MonsterData=ReadExcel("怪物数据",ServerConfig.key)
  elseif data =="刷出师门守卫" then
        师门守卫刷新 = math.random(3600, 7200) + os.time()
        TaskControl:刷出师门守卫()
            发送网关消息(string.format("%s成功",data))


 elseif data =="华山论剑开关" then
     华山论剑开关 = not 华山论剑开关
    if 华山论剑开关 then
       天梯匹配={}
       广播消息("#xt/#y/华山论剑活动已经开放，各位玩家可前往长安城华山论剑NPC参加活动")
      发送网关消息("华山论剑开启")
    else
        for n,p in  pairs (UserData) do
          if UserData[n]~=nil then 
            GameActivities:刷新华山论剑排行(n)
          end
         end
          广播消息("#xt/#y/华山论剑活动已经结束。")
          发送网关消息("华山论剑结束")
    end

  elseif data =="门派闯关开关" then
    if 门派闯关开关 then
       TaskControl:结束门派闯关活动()
        发送网关消息("门派闯关开启")
    else
        TaskControl:开启门派闯关活动()
        发送网关消息("门派闯关结束")  
    end
  elseif data =="皇宫飞贼开关" then
    飞贼开关 = not 飞贼开关
    if 飞贼开关 then
       广播消息("#xt/#y/皇宫飞贼活动已经开放，各位玩家可前往长安城御林军左统领处领取任务")
      发送网关消息("皇宫飞贼开启")
    else

      广播消息("#xt/#y/皇宫飞贼活动已经结束。玩家将无法领取新的任务，未完成的任务仍然可以继续完成。")
      发送网关消息("皇宫飞贼结束")
    end       
  elseif data =="首席争霸开关" then
    if  首席争霸赛战斗开关 then
       GameActivities:结束首席争霸门派(1, 3, 0, 0)    
       发送网关消息("首席争霸结束")
    else
       if 首席争霸赛进场开关 then 
          GameActivities:开启首席争霸赛()
      else 
           广播消息("#xt/#r/ 首席争霸赛活动已经开启入场，10分钟后将无法进入比赛地图。请各位玩家提前找本门派首席弟子进入比赛地图。")
            首席争霸赛进场开关 = true
            首席争霸赛战斗开关 = false
          发送网关消息("首席争霸进场开启需要在按一下开启战斗")
      end      
    end          
  elseif data =="比武大会开关" then 
     if  比武大会开始开关 then

          发送游戏公告("比武大会活动已结束。玩家将无法发起攻击。奖励将在服务器下一次重启时消除，请尽快前往兰虎处领取奖励")
          比武大会开始开关 = false
          发送网关消息("比武大会结束")
    else
       if 比武大会进场开关 then
            发送游戏公告("比武大会活动已开启。各位比武场内的玩家可以通过Alt+A对对方发起攻击。")
           比武大会进场开关 = false
            比武大会开始开关 = true
            MapControl:重置比武大会玩家()
            发送网关消息("比武大会PK开启")
       else 
           比武大会进场开关 = true
           发送游戏公告("比武大会活动即将开启，各位玩家现在可以通过长安城兰虎进入比武场。20分钟后将无法进入比武场。请参加活动的玩家提前进入比武场。")
            发送网关消息("比武大会进场开启需要在按一下开关开启战斗")
       end   
    end
  elseif data =="调试开关" then
  DebugMode=not DebugMode  
      发送网关消息("DebugMode状态为"..tostring(DebugMode))  

  elseif data =="每日金钱排行" then
     local TempMoney ={}
      for k,v in pairs(MoneyLog) do
        TempMoney[#TempMoney+1] ={ID=k,金额=v}
      end
      table.print(TempMoney)
      table.sort(TempMoney,function(a, b) 
        if a.金额 == b.金额 then
          return  a.ID > b.ID
        end
        return  a.金额 > b.金额 end)
      local stringMsg ="----------每日金钱排行(每日中午12点重新记录)---------"
     for i=1,#TempMoney do
       stringMsg=stringMsg.."\r\n".."ID: "..TempMoney[i].ID.."\t\t金额: "..TempMoney[i].金额
     end
    
       发送网关消息(stringMsg) 


      
  elseif data =="全局存档" then
		Save()    
      发送网关消息(string.format("%s成功",data))
  elseif data =="清空排行数据" then

    Ranking ={}
    local 数组 ={"属性排行","积分排行","数据排行"}
    local 排行属性 ={{"气血排行","防御排行","伤害排行","速度排行","命中排行","灵力排行","魔法排行","躲避排行"},
                    {"门贡排行","知了积分","长安保卫","活动积分","比武积分","副本积分","成就积分","单人积分"},
                    {"银子排行","储备排行","积累经验","经验排行","仙玉排行","累冲排行","积累银子","人物等级"}}

    for i=1,#数组 do
      Ranking[数组[i]]={}
      for o=1,8 do
        Ranking[数组[i]][排行属性[i][o]]={}
        for io=1,20 do
          Ranking[数组[i]][排行属性[i][o]][io]={名称="无",数值=0}
        end
         
      end
    end
      发送网关消息(string.format("%s成功",data)) 
  elseif data =="清除竞拍" then
  竞拍信息=nil    
      发送网关消息(string.format("%s成功",data))
  elseif data =="清除全服任务数据" then 
      清理任务信息()   
      发送网关消息(string.format("%s成功",data))
  elseif data =="重置活动次数" then
    活动数据={科举名单={},大雁塔数据={},嘉年华数据={},活跃度={},宝藏山名单={},平定安邦={},师门名单={},无限轮回={},群雄逐鹿={},蚩尤挑战={},抓鬼={},除暴安良={},押镖={},官职={},鬼王={},乌鸡={},车迟={},双倍={},每日答题={},打图={}}
  for n=1,100 do
       活动数据.大雁塔数据[n]={}
     end
    游泳比赛数据 = {}
    天罚数据表={}   
        发送网关消息(string.format("%s成功",data)) 
  elseif data =="维护通知" then 
        服务器关闭 = {计时 = 60,开关 = true,起始 = os.time()}
        发送游戏公告("各位玩家请注意，服务器将在1分钟后关闭，请所有玩家提前下线。")
        广播消息("#xt/#y/各位玩家请注意，服务器将在1分钟后关闭，请所有玩家提前下线。")
        Save()   
            发送网关消息(string.format("%s成功",data))
  elseif data =="帮战1开启" then

         帮派竞赛:开启竞赛()

          帮派竞赛.开关=true
          TaskControl:刷出帮战怪物()
          发送游戏公告("帮派竞赛开始，请各位帮战人员开战吧")
          广播消息("帮派竞赛开始，请各位帮战人员开战吧") 
              发送网关消息(string.format("%s成功",data))  
  elseif data =="帮战1结束" then
      帮派竞赛:结束竞赛()   
          发送网关消息(string.format("%s成功",data)) 
  elseif data =="帮战2开启" then
          帮派竞赛.迷宫开关=true
          帮派竞赛.准备迷宫开关=false
          发送游戏公告("帮派迷宫已经开始，请各位帮战人员开战吧")
          广播消息("帮派迷宫已经开始，请各位帮战人员开战吧")   
              发送网关消息(string.format("%s成功",data)) 
  elseif data =="帮战2结束" then 
   帮派竞赛:结束迷宫()   
       发送网关消息(string.format("%s成功",data))
  elseif data =="帮战3开启" then
     TaskControl:刷出帮战宝箱()
          发送游戏公告("宝箱已经散落到帮战奖励地图中，10分钟后将关闭，请获胜帮派取地图中捡取宝箱")
          广播消息("#xt/#y/宝箱已经散落到帮战奖励地图中，10分钟后将关闭，请获胜帮派取地图中捡取宝箱")
              发送网关消息(string.format("%s成功",data))
  elseif data =="帮战3结束" then
        帮派竞赛.宝箱开关=false
           MapControl:传送地图玩家(1380)  
               发送网关消息(string.format("%s成功",data))
  elseif data =="随机仙玉(50-500)" then
     local 随机银子 = math.random(10, 50)
     for k,v in pairs(UserData) do
       if not v.假人  and v.角色.等级 >68 then
       RoleControl:添加仙玉(v,随机银子,"GM奖励")
       广播消息(9,"#xt/#g/ " .. v.角色.名称 .. "#y/获得了游戏管理员赠送的新年红包，打开一看竟是#r/" .. 随机银子 .. "#y/仙玉。#S/快喊上你的朋友一起来组队吧！玩家热情不断,GM福利嗨翻天".."#"..math.random(110))
     end
    end
    发送网关消息(string.format("发送全服%s成功",data))
  elseif data =="随机储备" then
     local 随机银子 = math.random(90000, 50000000)
     for k,v in pairs(UserData) do
       if not v.假人  and v.角色.等级 >68 then
       RoleControl:添加储备(v,随机银子,"GM奖励")
       广播消息(9,"#xt/#g/ " .. v.角色.名称 .. "#y/获得了游戏管理员赠送的新年红包，打开一看竟是#r/" .. 随机银子 .. "#y/储备金。#S/快喊上你的朋友一起来组队吧！玩家热情不断,GM福利嗨翻天".."#"..math.random(110))
     end
    end
     发送网关消息(string.format("发送全服%s成功",data))
  elseif data =="随机银子(50W-200W)" then
      local 随机银子 = math.random(500000, 2000000)
     for k,v in pairs(UserData) do
       if not v.假人  and v.角色.等级 >68 then
        RoleControl:添加银子(v,随机银子,"GM奖励")
       广播消息(9,"#xt/#g/ " .. v.角色.名称 .. "#y/获得了游戏管理员赠送的新年红包，打开一看竟是#r/" .. 随机银子 .. "#y/银子。#S/快喊上你的朋友一起来组队吧！玩家热情不断,GM福利嗨翻天".."#"..math.random(110))
     end
   end
    发送网关消息(string.format("发送全服%s成功",data))
  elseif data =="随机兽决" then
    for k,v in pairs(UserData) do
       if not v.假人  and v.角色.等级 >68 then
       ItemControl:GiveItem(k,"魔兽要诀")
       广播消息(9, "#xt/#g/ " .. v.角色.名称 .. "#y/获得了游戏管理员赠送的新年红包，打开一看竟是#g/魔兽要诀".."#"..math.random(110))
      end
    end
    发送网关消息(string.format("发送全服%s成功",data))
  elseif data =="随机高级兽决" then
     for k,v in pairs(UserData) do
      if not v.假人  and v.角色.等级 >68 then
        local 随机兽决 = 取垃圾兽诀名称()
        ItemControl:GiveItem(k,"高级魔兽要诀",nil,随机兽决)
       广播消息(9, "#xt/#g/ " .. v.角色.名称 .. "#y/获得了游戏管理员赠送的开服红包，打开一看竟是#g/"..随机兽决.."#"..math.random(110))
     end
   end
    发送网关消息(string.format("发送全服%s成功",data))
  elseif data =="1E经验500万仙玉" then  
     for k,v in pairs(UserData) do
      if not v.假人  and v.角色.等级 >68 then
           RoleControl:添加仙玉(v,5000000,"GM奖励")
          RoleControl:添加经验(v,100000000,"GM奖励")
          广播消息(9,"#xt/#g/ " .. v.角色.名称 .. "#y/获得了游戏管理员赠送的开服红包，打开一看竟是#r/1E#y/银子和#r/500W#y/仙玉")
      end
     
     end
     发送网关消息(string.format("发送全服%s成功",data))
     
  end
end
return GMTool
