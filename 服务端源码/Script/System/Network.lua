-- @Author: 作者QQ381990860
-- @Date:   2021-09-12 16:36:56
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-23 01:45:09

local Network = class()

function Network:初始化()
  self.rwdh={
   [1]= {"飞燕女","英女侠","巫蛮儿","偃无师","逍遥生","剑侠客"},
   [2]={"狐美人","骨精灵","鬼潇潇","杀破狼","巨魔王","虎头怪"},
   [3]={"舞天姬","玄彩娥","桃夭夭","羽灵神","神天兵","龙太子"}
 }
end
function Network:账号处理(内容)
  local fun  = require("ffi函数")
  if 内容.密码 == nil  then
    self:强制断开1(内容.id,"#y/非法登陆!" )
    return 0
  elseif  file_exists("玩家信息/账号" .. 内容.账号 .. "/账号.txt") == false then
    self:强制断开1(内容.id,"#y/对不起你的账号不存在,请你注册账号先,谢谢!" )
    return 0
  elseif  f函数.读配置(ServerDirectory.."玩家信息/账号" .. 内容.账号 .. "/账号.txt", "账号信息", "密码")~=内容.密码 then
    self:强制断开1(内容.id,"#y/对不起你的密码不正确,请核对后在登陆,谢谢!" )
    return 0
  elseif  f函数.读配置(ServerDirectory.."玩家信息/账号" .. 内容.账号 .. "/账号.txt", "账号信息", "仙玉")< "0" then
    self:强制断开1(内容.id,"#y/您仙玉为负数，暂不能登录。" )
    return 0
  else
      self:取角色选择信息(内容.id,内容.账号)
  end
end

function Network:强制断开1(id,消息)
   SendMessage(id,99998,消息)
   SendMessage(id,99999,"强制断开1")
end

function Network:取角色选择信息(id,账号)

       if not file_exists([[玩家信息/账号]]..账号..[[/操作记录.txt]]) then 
         __S服务:输出(string.format("玩家账号%s操作记录文件不存在请及时删除",账号))
        return 
      end

  local 临时文件= ReadFile([[玩家信息/账号]]..账号..[[/操作记录.txt]])

  local 写入信息=table.loadstring(临时文件)
  self.SendMessage={}
  for n=1,#写入信息 do

    local 读取文件= ReadFile([[玩家信息/账号]]..账号..[[/]]..写入信息[n]..[[/角色.txt]])
   
    local 还原数据=table.loadstring(读取文件)
    if 还原数据 ==nil then
      SendMessage(id,7,"#y/系统异常请重启客户端")
      return
    end
    self.SendMessage[n]={名称=还原数据.名称,等级=还原数据.等级,染色方案=还原数据.染色方案,染色=还原数据.染色,造型=还原数据.造型,
    武器数据=还原数据.武器数据,锦衣数据=还原数据.锦衣数据,门派=还原数据.门派,id=还原数据.id}
  end
  SendMessage(id,6,self.SendMessage)
  self.SendMessage =nil
end
function Network:NewAccount(内容)
	local 角色id= f函数.读配置(ServerDirectory .. "config.ini", "mainconfig", "id") + 0
	local fun  = require("ffi函数")
	if 内容.名称 == nil then
		内容.名称 = "名字" .. 角色id + 1
	end
	if 内容.密码 == nil then
		self:强制断开1(内容.id,"#y/非法登陆!" )
		return 0
	end
	if file_exists("玩家信息/账号" .. 内容.账号 .. "/账号.txt") == false then
		self:强制断开1(内容.id,"#y/对不起你的账号不存在,请你注册账号先,谢谢!" )
		return 0
	elseif file_exists("玩家信息/账号" .. 内容.账号 .. "/账号.txt") and f函数.读配置(ServerDirectory.."玩家信息/账号" .. 内容.账号 .. "/账号.txt", "账号信息", "密码")~=内容.密码  then
		self:强制断开1(内容.id,"#y/对不起你的密码不正确,请核对后在登陆,谢谢!" )
		return 0
	elseif self.rwdh[内容.编号][内容.序号] == nil then
		SendMessage(内容.id,7, "#y/建立角色时出现严重错误，错误代号#r/2003，#y/请联系游戏管理员解决")
		return
	elseif self:名称检查(内容.名称, 内容.id, 内容.ip) == false then
		return 0
	else
		if not file_exists([[玩家信息/账号]]..内容.账号..[[/操作记录.txt]]) then 
			__S服务:输出(string.format("玩家账号%s操作记录文件不存在请及时删除",内容.账号))
			return 
		end
        local 临时文件= ReadFile([[玩家信息/账号]]..内容.账号..[[/操作记录.txt]])
        local 写入信息=table.loadstring(临时文件)
        if #写入信息>=6 then
			SendMessage(内容.id,7,"#Y/您无法再创建更多的角色了")
			return 0
        end
		角色id = 角色id + 1
		f函数.写配置(ServerDirectory .. "config.ini", "mainconfig", "id", 角色id)
		local 名称数据 = table.loadstring( ReadFile("数据信息/名称数据.txt"))
		名称数据[#名称数据 + 1] = {名称 = 内容.名称,账号 = 内容.账号,ip = ip}
		WriteFile("数据信息/名称数据.txt", table.tostring(名称数据))
  
		os.execute("mkdir "..ServerDirectory..[[玩家信息\账号]] .. 内容.账号..[[\]]..角色id)
		os.execute("mkdir "..ServerDirectory..[[玩家信息\账号]] .. 内容.账号 .. [[\]]..角色id..[[\日志]])

		WriteFile("玩家信息/账号" .. 内容.账号 ..[[/]]..角色id.."/日志编号.txt", "1")
		WriteFile("玩家信息/账号" .. 内容.账号 ..[[/]]..角色id.."/充值记录.txt", "日志创建")
		WriteFile("玩家信息/账号" .. 内容.账号 ..[[/]]..角色id.."/日志" .. "/1.txt", "日志创建\n")
		WriteFile("玩家信息/账号" .. 内容.账号 ..[[/]]..角色id.."/召唤兽.txt", table.tostring({}))
		WriteFile("玩家信息/账号" .. 内容.账号 ..[[/]]..角色id.."/道具.txt", table.tostring({}))
		WriteFile("玩家信息/账号" .. 内容.账号 ..[[/]]..角色id.."/助战.txt", table.tostring({}))
  
		临时角色= RoleControl:创建新角色(内容.账号, 内容.名称, self.rwdh[内容.编号][内容.序号], 内容.编号, 性别, 角色id)
     WriteFile("玩家信息/账号" .. 内容.账号 ..[[/]]..角色id.."/角色.txt", table.tostring(临时角色))

    local 临时文件= ReadFile([[玩家信息/账号]]..内容.账号..[[/操作记录.txt]])
    local 写入信息=table.loadstring(临时文件)
    local 进入内容={账号=内容.账号,密码=内容.密码,编号=#写入信息,id=内容.id,ip=内容.ip}

    self:进入游戏处理(进入内容)


  end
end
function Network:创建玩家账号(账号,密码,安全码,id,ip)
      local fun  = require("ffi函数")
         f函数.写配置(ServerDirectory..[[玩家信息\账号]]..账号..[[\账号.txt]],"账号信息","密码",密码)
         f函数.写配置(ServerDirectory..[[玩家信息\账号]]..账号..[[\账号.txt]],"账号信息","安全码",安全码)
         f函数.写配置(ServerDirectory..[[玩家信息\账号]]..账号..[[\账号.txt]],"账号信息","仙玉","0")
         f函数.写配置(ServerDirectory..[[玩家信息\账号]]..账号..[[\账号.txt]],"账号信息","点数","0")
         f函数.写配置(ServerDirectory..[[玩家信息\账号]]..账号..[[\账号.txt]],"账号信息","体验","0")
         f函数.写配置(ServerDirectory..[[玩家信息\账号]]..账号..[[\账号.txt]],"账号信息","管理","0")
         f函数.写配置(ServerDirectory..[[玩家信息\账号]]..账号..[[\账号.txt]],"账号信息","封禁","0")
         f函数.写配置(ServerDirectory..[[玩家信息\账号]]..账号..[[\账号.txt]],"账号信息","创建员",账号)
         f函数.写配置(ServerDirectory..[[玩家信息\账号]]..账号..[[\账号.txt]],"账号信息","ip",ip)
         f函数.写配置(ServerDirectory..[[玩家信息\账号]]..账号..[[\账号.txt]],"账号信息","注册时间",os.date("[%Y年%m月%d日%X]"))
         WriteFile([[玩家信息\账号]]..账号..[[\操作记录.txt]],table.tostring({}))
end
function Network:账号检查(账号)
  if string.find(账号, "/") ~= nil then
    return true
  elseif string.find(账号, "@") ~= nil then

    return true
  elseif string.find(账号, "#") ~= nil then

    return true
  elseif string.find(账号, "*") ~= nil then
  return true
  end

  return false
end

function Network:数据处理(序号, 内容, id, ip)

      if 序号 > 999 then
            GMTool:数据处理(序号,内容)
            return                                
      end
        self.处理信息1=cjson.decode(内容)
        -- if __gge.isdebug then
            print("序号["..序号.."],内容:"..内容)
        -- end
        local 内容 = self.处理信息1
        local 玩家数据 = UserData

        if 序号>3 and 序号~=9 and 序号~=102  and  not UserData[self.处理信息1.数字id] then
            self:强制断开1(内容.连接id,"#y/服务器已重启，为了您的账号安全，请重新登陆游戏!" )
           return 0
        end
        if 序号 == 0 then
            self:进入游戏处理(self.处理信息1)
        elseif 序号 == 1 then
            self:账号处理(self.处理信息1)
        elseif 序号 == 2 then
            self:NewAccount(self.处理信息1)
        elseif 序号 == 3 then
            self:退出处理(self.处理信息1)
        elseif 序号 == 4 then
            MapControl:数据处理(self.处理信息1)
        elseif 序号 == 5 then
            self:角色处理(self.处理信息1)
        elseif 序号 == 6 then
            DialogueControl:数据处理(self.处理信息1)
        elseif 序号 == 7 then
          RoleControl:升级技能学习(UserData[self.处理信息1.数字id], self.处理信息1.参数)
        elseif 序号 == 8 then
          RoleControl:学习生活技能处理(UserData[self.处理信息1.数字id],  self.处理信息1)
        elseif 序号 == 9 then
            if self.处理信息1.c==nil then
                 SendMessage(self.处理信息1.a,7,"#y/请联系游戏管理员解决1")
            elseif file_exists([[玩家信息\账号]]..self.处理信息1.c..[[\账号.txt]]) then
                 SendMessage(self.处理信息1.a,7, "#y/您要注册的账号已经存在，请换其他账号")
            elseif  string.len(self.处理信息1.c)<6 then
                 SendMessage(self.处理信息1.a,7,"#R/账号不能小于6位数")
            elseif   self.处理信息1.c == self.处理信息1.d then
                    SendMessage(self.处理信息1.a,7,"#R/账号和密码不能一样")
            elseif   self.处理信息1.d == self.处理信息1.e then
                 SendMessage(self.处理信息1.a,7,"#R/密码和安全码不能一样")
            elseif  string.len(self.处理信息1.d)<6 then
                 SendMessage(self.处理信息1.a,7,"#R/密码不能小于6位数")
            elseif string.len(self.处理信息1.e)<6 then
                SendMessage(self.处理信息1.a,7,"#R/安全码不能小于6位数")
            elseif self:账号检查(self.处理信息1.c) then
                SendMessage(self.处理信息1.a,7,"#y/请联系游戏管理员解决2")
            else
              if os.execute("mkdir "..ServerDirectory..[[玩家信息\账号]]..self.处理信息1.c)==0  then
                self:创建玩家账号(self.处理信息1.c,self.处理信息1.d,self.处理信息1.e,self.处理信息1.a, self.处理信息1.b)
               SendMessage(self.处理信息1.a,7, "#y/您的账号已经建立成功，请勿泄露自己的账号密码")

                SendMessage(self.处理信息1.a,25, {self.处理信息1.c,self.处理信息1.d})

              else
                SendMessage(self.处理信息1.a,7, "#y/无法建立账号数据，请联系管理员3")
              end
            end
        elseif 序号 == 10 then
            if self.处理信息1.序号==1 then
            RoleControl:学习强化技能处理(UserData[self.处理信息1.数字id], self.处理信息1.参数)
            elseif self.处理信息1.序号==2 then
            RoleControl:学习辅助技能处理(UserData[self.处理信息1.数字id], self.处理信息1.参数)
            elseif self.处理信息1.序号==3 then
              for i=1,10 do
               RoleControl:学习强化技能处理(UserData[self.处理信息1.数字id], self.处理信息1.参数)
              end
            
            elseif self.处理信息1.序号==4 then
                for i=1,10 do
                RoleControl:学习辅助技能处理(UserData[self.处理信息1.数字id], self.处理信息1.参数)  
              end      
            end
        elseif 序号 == 11 then
            TeamControl:数据处理(self.处理信息1)
        elseif 序号 == 12 then
            Chat:数据处理(self.处理信息1)
		elseif 序号 == 13 then
		    ItemControl:数据处理(self.处理信息1.数字id, self.处理信息1.序号, self.处理信息1.文本, self.处理信息1.参数,self.处理信息1.编号)
    	elseif 序号 == 14 then
          if UserData[self.处理信息1.数字id].摆摊 ~= nil then
              return 0
          end
           ShopControl:数据处理(UserData[self.处理信息1.数字id],self.处理信息1.参数 , self.处理信息1.序号 , self.处理信息1.文本, self.处理信息1.编号)
    		elseif 序号 == 15 then
    			if UserData[self.处理信息1.数字id].摆摊 ~= nil then
    				return 0
    			end
    			ItemControl:交换格子(self.处理信息1.数字id, self.处理信息1.序号, self.处理信息1.文本, self.处理信息1.参数)
    		elseif 序号 == 16 then -------重构
           ItemControl:道具使用(UserData[self.处理信息1.数字id], self.处理信息1.参数,self.处理信息1.序号, self.处理信息1.文本)
    		elseif 序号 == 17 then
    			if UserData[self.处理信息1.数字id].摆摊 ~= nil then
    				return 0
    			end
    			EquipmentControl:数据处理(self.处理信息1.数字id, 4006, self.处理信息1.参数 + 0, self.处理信息1.序号 + 0, self.处理信息1.文本,self.处理信息1.编号)
    		elseif 序号 == 18 then
    			if UserData[self.处理信息1.数字id].摆摊 ~= nil then
    				return 0
    			end
    			EquipmentControl:数据处理(self.处理信息1.数字id, 4032, self.处理信息1.参数 + 0, self.处理信息1.序号 + 0, self.处理信息1.文本)
    		elseif 序号 == 19 then
          

          UserData[self.处理信息1.数字id].召唤兽:取道具召唤兽()
    		elseif 序号 == 20 then

    			if UserData[self.处理信息1.数字id].摆摊 ~= nil then
    				return 0
    			end
    			ItemControl:仓库存放事件(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
    		elseif 序号 == 21 then
    			if UserData[self.处理信息1.数字id].摆摊 ~= nil then
    				return 0
    			end
    			ItemControl:仓库拿走事件(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
    		elseif 序号 == 22 then
    			SendMessage(UserData[self.处理信息1.数字id].连接id, 2006, UserData[self.处理信息1.数字id].召唤兽:获取数据())
    		elseif 序号 == 23 then
    			if UserData[self.处理信息1.数字id].摆摊 ~= nil then
    				return 0
    			end

    			if self.处理信息1.序号 == 1 then
    				UserData[self.处理信息1.数字id].召唤兽:放生召唤兽(self.处理信息1.数字id, self.处理信息1.参数)
    			elseif self.处理信息1.序号 == 2 then
    				UserData[self.处理信息1.数字id].召唤兽:参战召唤兽(self.处理信息1.数字id, self.处理信息1.参数)
    			elseif self.处理信息1.序号 == 3 then
    				UserData[self.处理信息1.数字id].召唤兽:改名召唤兽(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.文本)
    			elseif self.处理信息1.序号 == 4 then
    				UserData[self.处理信息1.数字id].召唤兽:加点召唤兽(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.文本)
    		    elseif self.处理信息1.序号 == 5 then
    				SendMessage(UserData[self.处理信息1.数字id].连接id,2070,UserData[self.处理信息1.数字id].召唤兽:获取数据())
    			elseif self.处理信息1.序号 == 6 then
    		        UserData[self.处理信息1.数字id].召唤兽:存取召唤兽(self.处理信息1.数字id,self.处理信息1.参数,self.处理信息1.文本)
    		     elseif self.处理信息1.序号 == 7 then
    		        UserData[self.处理信息1.数字id].召唤兽:驯养召唤兽(self.处理信息1.数字id,self.处理信息1.参数)
    		     elseif self.处理信息1.序号 == 8 then
    		        UserData[self.处理信息1.数字id].召唤兽:观看召唤兽(self.处理信息1.数字id,self.处理信息1.参数)
               elseif self.处理信息1.序号 == 9 then
                UserData[self.处理信息1.数字id].召唤兽:召唤兽改变造型(self.处理信息1.数字id,self.处理信息1.参数,self.处理信息1.文本,self.处理信息1.编号)

    			end
    		elseif 序号 == 24 then
    			FightGet:数据处理(self.处理信息1.数字id, self.处理信息1.序号, self.处理信息1.文本, self.处理信息1.参数, id, ip)
    		elseif 序号 == 25 then

    			TaskControl:数据处理(self.处理信息1.数字id, self.处理信息1.序号, self.处理信息1.文本, self.处理信息1.参数)
    		elseif 序号 == 26 then
    			if UserData[self.处理信息1.数字id].摆摊 ~= nil then
    				return 0
    			end

    			self.找到id = self.处理信息1.参数 + 0

    			if UserData[self.找到id] == nil then
    				SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/这个玩家并不存在")

    				return 0
    			elseif UserData[self.找到id].给予物品 == false then
    				SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/对方没有打开接受物品开关")

    				return 0
    			elseif UserData[self.找到id].角色.等级 < 60 then
    				SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/对方等级未达到60级无法给予物品")

    				return 0
    			elseif UserData[self.处理信息1.数字id].角色.等级 < 60 then
    				SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/你的等级未达到60级无法给予物品")

    				return 0
    			else
    				if UserData[self.处理信息1.数字id].角色.道具.货币.银子 < 0 or UserData[self.找到id].角色.道具.货币.银子 < 0 then
    					SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/错误的给予数据")
    					return 0
    				end
    				self.发送信息 = {
    					id = self.找到id,
    					道具 = ItemControl:索要玩家道具(self.处理信息1.数字id, "包裹"),
    					名称 = UserData[self.找到id].角色.名称,
    					等级 = UserData[self.找到id].角色.等级
    				}
    				SendMessage(UserData[self.处理信息1.数字id].连接id, 3039, self.发送信息)
    			end
    		elseif 序号 == 27 then

    			if UserData[self.处理信息1.数字id].摆摊 ~= nil then
    				return 0
    			end

    			ItemControl:交易条件判断(self.处理信息1.数字id, self.处理信息1.参数)
    		elseif 序号 == 28 then

    			if UserData[self.处理信息1.数字id].摆摊 ~= nil then
    				return 0
    			end
    			 RoleControl:玩家切磋(UserData[self.处理信息1.数字id],self.处理信息1.参数)
    		elseif 序号 == 29 then
    			if UserData[self.处理信息1.数字id].摆摊 ~= nil then
    				return 0
    			end
    			 ItemControl:给予玩家物品处理(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
        elseif 序号==30  then --请求快捷技能数据
            RoleControl:设置快捷技能(UserData[self.处理信息1.数字id],self.处理信息1.参数,self.处理信息1.序号,self.处理信息1.文本)
       elseif 序号 == 31 then

          if UserData[self.处理信息1.数字id].摆摊 ~= nil then
            return 0
          end

          self.处理信息1.参数 = self.处理信息1.参数 + 0
          if 交易数据[self.处理信息1.参数] == nil then
            SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#w/这样的交易并不存在")
          else
            self.发送信息 = {
              道具 = ItemControl:索要玩家道具(self.处理信息1.数字id, "包裹")
            }

            SendMessage(UserData[self.处理信息1.数字id].连接id, 3014, self.发送信息)
          end
   
    		elseif 序号 == 32 then
    			if UserData[self.处理信息1.数字id].摆摊 ~= nil then
    				return 0
    			end
    			self.处理信息1.参数 = self.处理信息1.参数 + 0

    			if 交易数据[self.处理信息1.参数] == nil then
    				SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#w/这样的交易并不存在")
    			else
    				SendMessage(UserData[self.处理信息1.数字id].连接id, 3015, UserData[self.处理信息1.数字id].召唤兽:获取数据())
    			end
    		elseif 序号 == 33 then
    			if UserData[self.处理信息1.数字id].摆摊 ~= nil then
    				return 0
    			end

    			self.处理信息1.参数 = self.处理信息1.参数 + 0

    			ItemControl:锁定交易(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
    		elseif 序号 == 34 then

    			if UserData[self.处理信息1.数字id].摆摊 ~= nil then
    				return 0
    			end

    			self.处理信息1.参数 = self.处理信息1.参数 + 0

    			ItemControl:完成交易(self.处理信息1.数字id, self.处理信息1.参数)
    		elseif 序号 == 35 then
    			if UserData[self.处理信息1.数字id].摆摊 ~= nil then
    				return 0
    			end
    			self.处理信息1.参数 = self.处理信息1.参数 + 0
    			self.交易id = UserData[self.处理信息1.数字id].交易

    			if 交易数据[self.交易id] == nil then
    				SendMessage(UserData[self.处理信息1.数字id].连接id, 3016, "11")
    			end
                if UserData[交易数据[self.交易id].发起方.id] ==nil or UserData[交易数据[self.交易id].接受方.id] ==nil then
                	 if UserData[交易数据[self.交易id].发起方.id] then
                	 	SendMessage(UserData[交易数据[self.交易id].发起方.id].连接id, 7, "#y/对方已经离开游戏并且取消了交易")
                	 	SendMessage(UserData[交易数据[self.交易id].发起方.id].连接id, 3016, "11")
                	 	UserData[交易数据[self.交易id].发起方.id].交易 = 0
                	 end
                	 if UserData[交易数据[self.交易id].接受方.id] then
                	 	SendMessage(UserData[交易数据[self.交易id].接受方.id].连接id, 7, "#y/对方已经离开游戏并且取消了交易")
                	 	SendMessage(UserData[交易数据[self.交易id].接受方.id].连接id, 3016, "11")
                	 	UserData[交易数据[self.交易id].接受方.id].交易 = 0
                	 end
                else
    			 SendMessage(UserData[交易数据[self.交易id].发起方.id].连接id, 7, "#y/" .. UserData[self.处理信息1.数字id].角色.名称 .. "取消了交易")
    		     SendMessage(UserData[交易数据[self.交易id].接受方.id].连接id, 7, "#y/" .. UserData[self.处理信息1.数字id].角色.名称 .. "取消了交易")
    			SendMessage(UserData[交易数据[self.交易id].发起方.id].连接id, 3016, "11")
    			SendMessage(UserData[交易数据[self.交易id].接受方.id].连接id, 3016, "11")
    			UserData[交易数据[self.交易id].接受方.id].交易 = 0
    			UserData[交易数据[self.交易id].发起方.id].交易 = 0
    		    end
    			交易数据[self.交易id] = false
    		  elseif 序号 == 36 then

          商会处理类:数据处理(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
        elseif 序号 == 37 then
          
          商会处理类:上柜商品处理(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本, tonumber(self.处理信息1.编号))
        elseif 序号 == 38 then

          商会处理类:购买商品处理(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本, self.处理信息1.编号)
    		elseif 序号 == 39 then
    			ItemControl:染色处理(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
    		elseif 序号 == 40 then
    			if UserData[self.处理信息1.数字id].摆摊 ~= nil then
    				return 0
    			end
    			ItemControl:城市快捷传送(self.处理信息1.数字id)
    		elseif 序号 == 41 then
    			宝箱解密类:数据处理(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
    		elseif 序号 == 42 then
          RoleControl:坐骑放生(UserData[self.处理信息1.数字id],self.处理信息1.参数)

    		elseif 序号 == 43 then
            if self.处理信息1.序号 == 1 then
                     SendMessage(UserData[self.处理信息1.数字id].连接id,20046,Ranking.数据排行)
    			 elseif self.处理信息1.序号 == 2 then
    			 	 SendMessage(UserData[self.处理信息1.数字id].连接id,20047,{数据=Ranking.积分排行,类型="积分排行"})
    			 elseif self.处理信息1.序号 == 3 then
    			 	 SendMessage(UserData[self.处理信息1.数字id].连接id,20047,{数据=Ranking.属性排行,类型="属性排行"})
    			 elseif self.处理信息1.序号 == 4 then
    			 	SendMessage(UserData[self.处理信息1.数字id].连接id,20047,{数据=Ranking.数据排行,类型="数据排行"})
                 end
    		elseif 序号 == 44 then
    			摆摊处理类:数据处理(self.处理信息1.数字id, self.处理信息1.序号 + 0, self.处理信息1.参数, self.处理信息1.文本, self.处理信息1.编号)
    		elseif 序号 == 45 then
    			self.处理信息1.参数 = self.处理信息1.参数 + 0
    			展示数据["[bb" .. self.处理信息1.数字id .. self.处理信息1.参数 .. "]"] = {
    				数据 = UserData[self.处理信息1.数字id].召唤兽:获取指定数据(self.处理信息1.参数),
    				时间 = os.time()
    			}

    			if bb数据列表[self.处理信息1.数字id] == nil then
    				bb数据列表[self.处理信息1.数字id] = {}
    			end

    			bb数据列表[self.处理信息1.数字id][#bb数据列表[self.处理信息1.数字id] + 1] = {
    				内容 = "[" .. "bb" .. self.处理信息1.数字id .. self.处理信息1.参数 .. "]",
    				名称 = UserData[self.处理信息1.数字id].召唤兽.数据[self.处理信息1.参数].造型
    			}
    			UserData[self.处理信息1.数字id].组合语句 = UserData[self.处理信息1.数字id].召唤兽.数据[self.处理信息1.参数 + 0].造型

    			SendMessage(UserData[self.处理信息1.数字id].连接id, 20028, "[" .. "bb" .. self.处理信息1.数字id .. self.处理信息1.参数 .. "]")
    		elseif 序号 == 46 then
    			SendMessage(UserData[self.处理信息1.数字id].连接id, 20029, 展示数据[self.处理信息1.参数])
    		elseif 序号 == 47 then
    			SendMessage(UserData[self.处理信息1.数字id].连接id, 20031, RoleControl:取坐骑数据(UserData[self.处理信息1.数字id]))
    		elseif 序号 == 48 then
            RoleControl:坐骑骑乘处理(UserData[self.处理信息1.数字id], self.处理信息1.参数)
    		elseif 序号 == 49 then
           RoleControl:坐骑驯养处理(UserData[self.处理信息1.数字id], self.处理信息1.参数)
    		elseif 序号 == 50 then
          RoleControl:坐骑加点处理(UserData[self.处理信息1.数字id], self.处理信息1.参数, self.处理信息1.文本)
    		elseif 序号 == 51 then
          RoleControl:角色改名(UserData[self.处理信息1.数字id], self.处理信息1.文本)
    		elseif 序号 == 52 then
    			 SendMessage(UserData[self.处理信息1.数字id].连接id,2062,UserData[self.处理信息1.数字id].角色.宠物)
    		elseif 序号 == 53 then
            UserData[self.处理信息1.数字id].角色.宠物.领取 =self.处理信息1.参数
            UserData[self.处理信息1.数字id].角色.宠物.名称=self.处理信息1.文本
            UserData[self.处理信息1.数字id].角色.宠物.模型=self.处理信息1.文本
        elseif 序号 == 54 then
            	if self.处理信息1.序号 + 0 ==1 then
                SendMessage(UserData[self.处理信息1.数字id].连接id,2078,UserData[self.处理信息1.数字id].角色.好友数据)
                elseif self.处理信息1.序号 + 0 ==2 then
                    RoleControl:添加好友数据(UserData[self.处理信息1.数字id], self.处理信息1.参数)
                end
        elseif 序号 == 55 then     
            SendMessage(UserData[self.处理信息1.数字id].连接id,2074,RoleControl:获取快捷技能数据(UserData[self.处理信息1.数字id]))
        elseif 序号 == 56 then
    			if  f函数.读配置(ServerDirectory.."玩家信息/账号" .. UserData[self.处理信息1.数字id].账号 .. "/账号.txt", "账号信息", "解锁密码") == nil or f函数.读配置(ServerDirectory.."玩家信息/账号" .. UserData[self.处理信息1.数字id].账号 .. "/账号.txt", "账号信息", "解锁密码") == "空" then
    			 f函数.写配置(ServerDirectory.."玩家信息/账号" .. UserData[self.处理信息1.数字id].账号 .. "/账号.txt", "账号信息", "解锁密码",self.处理信息1.文本)
    			  SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/恭喜你设置解锁密码成功,你的解锁密码是:#r/"..self.处理信息1.文本)
    			end
    	  elseif 序号 == 57 then
    			if  f函数.读配置(ServerDirectory.."玩家信息/账号" .. UserData[self.处理信息1.数字id].账号 .. "/账号.txt", "账号信息", "解锁密码") ==  self.处理信息1.文本 then
    			    UserData[self.处理信息1.数字id].加锁 =true
    			    SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/解锁成功,你现在可以解锁道具或者召唤兽")
    			else
    			  SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/解锁失败,你输入的密码不正确!请重新输入")
    			end
    	   elseif 序号 == 58 then
              RoleControl:坐骑改名(UserData[self.处理信息1.数字id],self.处理信息1.参数, self.处理信息1.文本)
    	   elseif 序号 == 59 then
         if  self.处理信息1.序号 ==1  then
          UserData[self.处理信息1.数字id].屏蔽交易 = not UserData[self.处理信息1.数字id].屏蔽交易
                      if UserData[self.处理信息1.数字id].屏蔽交易 then
              SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/你已经开启了屏蔽交易，别人无法对你进行交易")
            else
              SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/你关闭了屏蔽交易，交易正常")
            end
         elseif  self.处理信息1.序号 ==2  then
          UserData[self.处理信息1.数字id].屏蔽查看 = not UserData[self.处理信息1.数字id].屏蔽查看
            if UserData[self.处理信息1.数字id].屏蔽查看 then
              SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/你已经开启了屏蔽查看装备，别的玩家无法查看你的装备")
            else
              SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/你关闭了屏蔽查看装备，别的玩家可以查看你的装备")
            end
         elseif  self.处理信息1.序号 ==3  then
             UserData[self.处理信息1.数字id].屏蔽定制 = not UserData[self.处理信息1.数字id].屏蔽定制
                         if UserData[self.处理信息1.数字id].屏蔽定制 then
              SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/你已经开启了屏蔽定制，无法显示定制造型但是属性照样加成")
            else
              SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/你关闭了屏蔽定制，定制造型正常显示")
            end
            SendMessage(UserData[self.处理信息1.数字id].连接id, 2010, RoleControl:获取地图数据(UserData[self.处理信息1.数字id]))
            MapControl:MapSend(self.处理信息1.数字id, 1016,  RoleControl:获取地图数据(UserData[self.处理信息1.数字id]), UserData[self.处理信息1.数字id].地图)
         elseif self.处理信息1.序号 ==4 then
            UserData[self.处理信息1.数字id].屏蔽变身 = not UserData[self.处理信息1.数字id].屏蔽变身
            if UserData[self.处理信息1.数字id].屏蔽变身 then
              SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/你已经开启了屏蔽变身模式，在变身状态下可以不显示变身造型")
            else
              SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/你关闭了屏蔽变身模式，变身造型可以正常显示")
            end
              SendMessage(UserData[self.处理信息1.数字id].连接id, 24, UserData[self.处理信息1.数字id].屏蔽变身)

              SendMessage(UserData[self.处理信息1.数字id].连接id, 2010, RoleControl:获取地图数据(UserData[self.处理信息1.数字id]))
             
            MapControl:MapSend(self.处理信息1.数字id, 1016,  RoleControl:获取地图数据(UserData[self.处理信息1.数字id]), UserData[self.处理信息1.数字id].地图)
          end
    	   elseif 序号 == 60 then
            RoleControl:坐骑染色(UserData[self.处理信息1.数字id],self.处理信息1.参数+0,self.处理信息1.序号+0, self.处理信息1.文本+0)
    	   elseif 序号 == 61 then
           RoleControl:坐骑饰品染色(UserData[self.处理信息1.数字id], self.处理信息1.参数+0,self.处理信息1.序号+0, self.处理信息1.文本+0)
    	   elseif 序号 == 62 then
           if self.处理信息1.序号==1 then
                RoleControl:deleteFriend(UserData[self.处理信息1.数字id],self.处理信息1.参数)
           else 
              if UserData[self.处理信息1.参数]==nil then
                SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/玩家不在线无法查看装备")
              elseif UserData[self.处理信息1.参数].屏蔽查看 then 
                  SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/对方不允许你查看装备")
              else
                  SendMessage(UserData[self.处理信息1.数字id].连接id, 20052,RoleControl:索取装备信息(UserData[self.处理信息1.参数]) )
              end
           end
             
    	   elseif 序号 == 63 then
              UserData[self.处理信息1.数字id].屏蔽给予 = not UserData[self.处理信息1.数字id].屏蔽给予
              if UserData[self.处理信息1.数字id].屏蔽给予 then
              SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/你已经开启了屏蔽给予模式，不会接受玩家给予的道具")
              else
              SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/你关闭了屏蔽给予模式")
              end
      	  elseif 序号 == 64 then
            if UserData[self.处理信息1.数字id].摆摊 ~= nil then
            return 0
            end

      			if UserData[self.处理信息1.参数] ==nil then
      				SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/当前玩家不在线")
      			elseif MapControl:取距离范围(self.处理信息1.数字id, self.处理信息1.参数, 400) == false then
      					SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/你离这个玩家太远了")
      			elseif UserData[self.处理信息1.参数].角色.变身.造型==nil then
      				SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/当前玩家并没有变身")

      			elseif  UserData[self.处理信息1.数字id].角色.剧情技能[3].等级< UserData[self.处理信息1.参数].角色.变身.等级 then
      				SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/你当前的变化之术不足以破解他的变身")

      			elseif  UserData[self.处理信息1.数字id].角色.当前体力<300 then
      				SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/你的体力不足300")
      			else
      				 UserData[self.处理信息1.数字id].角色.当前体力=UserData[self.处理信息1.数字id].角色.当前体力-300
      				  TaskControl:结束变身(self.处理信息1.参数)
      				  SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "#y/成功识破他的变身")
      			end
      	  elseif 序号 == 65 then
            RoleControl:签到处理(UserData[self.处理信息1.数字id])
      	  elseif 序号 == 66 then
            RoleControl:获取签到数据(UserData[self.处理信息1.数字id])
          elseif 序号 == 67 then
            RoleControl:ChildrenUseItems(UserData[self.处理信息1.数字id],self.处理信息1.参数,self.处理信息1.序号)
           elseif 序号 == 68 then
              RoleControl:ChildrenWornEquip(UserData[self.处理信息1.数字id],self.处理信息1.参数,self.处理信息1.序号,self.处理信息1.文本)
          elseif 序号 == 69 then
               ItemControl:行囊空间互换(self.处理信息1.数字id,self.处理信息1.参数,self.处理信息1.序号)

          elseif 序号 == 70 then
               EquipmentControl:改变造型(self.处理信息1.数字id,self.处理信息1.参数,self.处理信息1.序号)    

          elseif 序号 == 71 then
               EquipmentControl:改变双加(self.处理信息1.数字id,self.处理信息1.参数,self.处理信息1.文本,self.处理信息1.编号)
        elseif 序号 == 72 then
              for i=1,10 do
                RoleControl:升级技能学习(UserData[self.处理信息1.数字id], self.处理信息1.参数)
              end
        elseif 序号==73 then --法术认证
            if 查看数据 then
              if 查看数据[self.处理信息1.文本].类型 == 2 then
                SendMessage(UserData[self.处理信息1.数字id].连接id, 28,查看数据[self.处理信息1.文本])
              else
                SendMessage(UserData[self.处理信息1.数字id].连接id, 27, 查看数据[self.处理信息1.文本])
              end
            else 
               SendMessage(UserData[self.处理信息1.数字id].连接id, 7, "数据已消失")
            end
        elseif 序号 == 75 then
          商会处理类:购买唤兽处理(self.处理信息1.数字id, self.处理信息1.参数, self.处理信息1.序号, self.处理信息1.文本)
        elseif 序号 == 76 then
          SendMessage(UserData[self.处理信息1.数字id].连接id, 29, 首服奖励)
      	 elseif 序号==96 then
      	      -- self.处理信息1.参数=self.处理信息1.参数+0
      	      -- if UserData[self.处理信息1.参数]~=nil then
      	      --    UserData[self.处理信息1.参数].连接id=self.处理信息1.连接id
      	      --    SendMessage(self.处理信息1.连接id,30,self.处理信息1.参数)
      	      --   end
      		elseif 序号 == 102  then
      			tchs()
                  return
        elseif 序号 == 100  then--网关占用
         elseif 序号 == 99  then--网关占用 
            print(1111111111)
        elseif 序号 == 103 then
            SendMessage(self.处理信息1.连接id,117,点歌列表)
        elseif 序号 == 104 then
            --Max 修改点歌限制时间为10秒
            if string.find(self.处理信息1.文本,"do local ret",1) ~= nil then
                文本 = table.loadstring(self.处理信息1.文本)
            end
            if os.time() - 点歌状态 >= 10 then
              点歌状态 = os.time()
              --Max 去掉点歌耗费仙玉
              if RoleControl:扣除仙玉(UserData[self.处理信息1.数字id],100,"点歌祝福")  then
                广播祝福(文本.歌曲,UserData[self.处理信息1.数字id].角色.名称.."为大家送祝福")
                --Max 删除点歌增加经验
                发送游戏公告("#R"..UserData[self.处理信息1.数字id].角色.名称.."#G为大家点播了一首".."#R"..文本.歌曲.."#G祝大家游戏愉快天天好心情！") --,所有在线玩家获得普天同庆经验增加#R100000
              end
            else
              常规提示(self.处理信息1.数字id,"#Y/请稍等一会在点歌")
            end
        elseif 序号==106 then --道具交换格子处理
            if UserData[self.处理信息1.数字id].摆摊~=nil then 
				return 0 
			end
			EquipmentControl:数据处理(self.处理信息1.数字id,4009,self.处理信息1.参数+0,self.处理信息1.序号+0,self.处理信息1.文本)
        elseif 序号==107 then --
        	UserData[self.处理信息1.数字id].召唤兽:染色处理(self.处理信息1.数字id,self.处理信息1.参数+0,self.处理信息1.序号+0,self.处理信息1.文本,self.处理信息1.编号+0)

        elseif 序号==108 then --
			--print(table.tostring(table.loadstring(self.处理信息1.文本)))
			local 参数 = self.处理信息1.参数
			if 参数 == 1 then
				SendMessage(self.处理信息1.连接id,20058,UserData[self.处理信息1.数字id].助战:取数据())
			elseif 参数 == 2 then
				UserData[self.处理信息1.数字id].最后操作 = {动作="购买助战"}
				local zzmod = {"飞燕女","英女侠","巫蛮儿","逍遥生","剑侠客","狐美人","骨精灵","杀破狼","巨魔王","虎头怪","舞天姬","玄彩娥","羽灵神","神天兵","龙太子","桃夭夭","偃无师","鬼潇潇"}
				SendMessage(self.处理信息1.连接id,20,{UserData[self.处理信息1.数字id].角色.模型,UserData[self.处理信息1.数字id].角色.名称,"购买助战需消耗100万银子，请选择您想要的助战外形。",zzmod,"特殊"})
			else
				--package.loaded["Script/QQ2124872/助战系统"] = ni
				--UserData[self.处理信息1.数字id].助战 = require("Script/QQ2124872/助战系统")(self.处理信息1.数字id)
				--UserData[self.处理信息1.数字id].助战:加载数据(UserData[self.处理信息1.数字id].账号,self.处理信息1.数字id)
				-- 参数,序号,文本
				UserData[self.处理信息1.数字id].助战:数据处理(参数,self.处理信息1.序号,self.处理信息1.文本)   --助战
			end
        elseif 序号==113 then --助战宠物 参战
            if string.find(self.处理信息1.文本,"do local ret",1) ~= nil then
                文本 = table.loadstring(self.处理信息1.文本)
                -- table.print(UserData[self.处理信息1.数字id])
                -- table.print(UserData[self.处理信息1.数字id].助战.数据[文本.助战编号])
            if UserData[self.处理信息1.数字id].召唤兽.数据[文本.宠物编号] == nil then
                SendMessage(self.处理信息1.连接id, 7, "#y/你没有这只召唤兽")
            elseif UserData[self.处理信息1.数字id].战斗 ~= 0 then
                SendMessage(self.处理信息1.连接id, 7, "#y/战斗中无法进行此操作")
                return 0
            elseif UserData[self.处理信息1.数字id].召唤兽.数据[文本.宠物编号].参战等级 > UserData[self.处理信息1.数字id].助战.数据[文本.助战编号].等级 and Config.bb参战限制=="有" then
                SendMessage(self.处理信息1.连接id, 7, "#y/你无法驾驭这只召唤兽")
            elseif UserData[self.处理信息1.数字id].召唤兽.数据[文本.宠物编号].等级 >= UserData[self.处理信息1.数字id].助战.数据[文本.助战编号].等级 + 11 then
                SendMessage(self.处理信息1.连接id, 7, "#y/你无法驾驭等级大于你10级的召唤兽")
            else
                for i = 1,#UserData[self.处理信息1.数字id].召唤兽.数据 do
                    if i == 文本.宠物编号  then
                        UserData[self.处理信息1.数字id].召唤兽.数据[i].助战参战 = 文本.助战编号
                    else
                        if UserData[self.处理信息1.数字id].召唤兽.数据[i].助战参战 == 文本.助战编号 then
                            UserData[self.处理信息1.数字id].召唤兽.数据[i].助战参战 = nil
                        end
                    end
                end
                SendMessage(self.处理信息1.连接id,20070,UserData[self.处理信息1.数字id].召唤兽:获取数据())
            end
            end
        elseif 序号==114 then --助战宠物 休息
            if string.find(self.处理信息1.文本,"do local ret",1) ~= nil then
                文本 = table.loadstring(self.处理信息1.文本)
                -- table.print(UserData[self.处理信息1.数字id].召唤兽.数据[文本.宠物编号])
                -- table.print(UserData[self.处理信息1.数字id].助战.数据[文本.助战编号])
                for i = 1,#UserData[self.处理信息1.数字id].召唤兽.数据 do
                    if i == 文本.宠物编号 and UserData[self.处理信息1.数字id].召唤兽.数据[i].助战参战 ==文本.助战编号  then
                        UserData[self.处理信息1.数字id].召唤兽.数据[i].助战参战 = nil
                    end
                end
                SendMessage(self.处理信息1.连接id,20070,UserData[self.处理信息1.数字id].召唤兽:获取数据())
            end
        elseif 序号==109 then --助战宠物
            if string.find(self.处理信息1.文本,"do local ret",1) ~= nil then
                文本 = table.loadstring(self.处理信息1.文本)
                -- //
                SendMessage(self.处理信息1.连接id,20070,UserData[self.处理信息1.数字id].召唤兽:获取数据())
            end
        elseif 序号==110 then --挂机系统
			挂机系统:数据处理(self.处理信息1.数字id,table.loadstring(self.处理信息1.文本))
        elseif 序号==111 then --元神系统
			RoleControl:刷新元神(self.处理信息1.数字id)
		elseif 序号==112 then
            RoleControl:增加元神突破(self.处理信息1.数字id)
		elseif 序号==600 then
            local 点卡=f函数.读配置(ServerDirectory..[[data\]]..玩家数据[内容.数字id].账号..[[\账号信息.txt]],"账号配置","点卡")
            if 点卡 == nil or 点卡 == "" or 点卡 == "空" then
              f函数.写配置(ServerDirectory..[[data\]]..玩家数据[内容.数字id].账号..[[\账号信息.txt]],"账号配置","点卡",0)
              点卡 = 0
            end
            发送数据(内容.连接id,92,{兑换比例=兑换比例,点卡=点卡})

            发送数据(内容.连接id,112,藏宝阁数据)
            if 观察藏宝阁数据 == nil then
              观察藏宝阁数据 = {}
            end
            观察藏宝阁数据[内容.数字id] = 1

          elseif 序号 == 601 then
            发送数据(内容.连接id, 113, ItemControl:索要道具1(内容.数字id, "包裹"))
          elseif 序号 == 602 then
            发送数据(内容.连接id, 114, 玩家数据[内容.数字id].召唤兽)
          elseif 序号 == 603 then
            -- table.print(玩家数据[内容.数字id].角色)
            发送数据(内容.连接id, 115, 玩家数据[内容.数字id].角色.道具.货币.银子)
          elseif 序号 == 605 then --上架道具
            文本 = table.loadstring(self.处理信息1.文本)
            self:藏宝阁上架(内容.数字id,文本.编号,文本.价格)
          elseif 序号 == 606 then --上架召唤兽
            文本 = table.loadstring(self.处理信息1.文本)
            self:藏宝阁上架召唤兽(内容.数字id,文本.编号,文本.价格)
          elseif 序号 == 607 then --上架召唤兽
            观察藏宝阁数据[内容.数字id] = nil
          elseif 序号 == 608 then --上架银子
            文本 = table.loadstring(self.处理信息1.文本)
            self:藏宝阁上架银两(内容.数字id,文本.编号,文本.价格)
          elseif 序号 == 609 then --上架银子
            文本 = table.loadstring(self.处理信息1.文本)
            self:藏宝阁购买处理(内容.数字id,文本.编号,文本.类型)
          elseif 序号 == 604 then --上架银子
            文本 = table.loadstring(self.处理信息1.文本)
            self:藏宝阁取回处理(内容.数字id,文本.编号,文本.类型)
          elseif 序号 == 610 then --上架角色
            文本 = table.loadstring(self.处理信息1.文本)
            玩家数据[内容.数字id].角色.藏宝阁出售 = 1
            print_r(玩家数据[内容.数字id].角色)
            藏宝阁数据.角色[#藏宝阁数据.角色+1] = {所有者=内容.数字id,价格=文本.编号,取回密码=文本.价格,结束时间=os.time()+604800,角色信息={账号=玩家数据[内容.数字id].账号,模型=玩家数据[内容.数字id].角色.造型,名称=玩家数据[内容.数字id].角色.名称,等级=玩家数据[内容.数字id].角色.等级,门派=玩家数据[内容.数字id].角色.门派}}
            --常规提示(内容.数字id,"#Y上架角色成功,即日起该角色将无法进入游戏,可以使用任意角色在XX处使用取回密码取回账号,请牢记你的取回密码:"..内容.价格)
            -- 玩家数据[内容.数字id].角色:存档()
            RoleControl:存档(玩家数据[内容.数字id])
            发送数据(内容.连接id,998,"#Y上架角色成功,即日起该角色将无法进入游戏,可以使用任意角色在XX处使用取回密码取回账号,请牢记你的取回密码！")
            __S服务:连接退出(内容.连接id)
            for i,v in pairs(观察藏宝阁数据) do
              if 玩家数据[i] ~= nil then
                发送数据(玩家数据[i].连接id,116 , 藏宝阁数据)
              else
                观察藏宝阁数据[i] = nil
              end
            end
          elseif 序号 == 620 then --查看角色
            local id = 内容.数字id
            local 编号 = 内容.编号
            local 名称 = 内容.名称
            if 藏宝阁数据.角色[编号] == nil or 名称 ~= 藏宝阁数据.角色[编号].角色信息.名称 then
              常规提示(id,"#Y数据错误请重新打开藏宝阁")
              return
            end
            local 发送角色信息= {}
            发送角色信息.角色数据 = table.loadstring(读入文件(程序目录..[[data/]]..藏宝阁数据.角色[编号].角色信息.账号..[[/]]..藏宝阁数据.角色[编号].所有者..[[/角色.txt]]))
            发送角色信息.召唤兽数据 = table.loadstring(读入文件(程序目录..[[data/]]..藏宝阁数据.角色[编号].角色信息.账号..[[/]]..藏宝阁数据.角色[编号].所有者..[[/召唤兽.txt]]))
            local 道具数据 = table.loadstring(读入文件(程序目录..[[data/]]..藏宝阁数据.角色[编号].角色信息.账号..[[/]]..藏宝阁数据.角色[编号].所有者..[[/道具.txt]]))
            发送角色信息.装备数据 = {}
            for i=1,6 do
              if 发送角色信息.角色数据.装备[i] ~= nil and 道具数据[发送角色信息.角色数据.装备[i]] ~= nil then
                发送角色信息.装备数据[i] = 道具数据[发送角色信息.角色数据.装备[i]]
              end
            end
            发送角色信息.灵饰数据 = {}
             for i=1,4 do
              if 发送角色信息.角色数据.灵饰[i] ~= nil and 道具数据[发送角色信息.角色数据.灵饰[i]] ~= nil then
                发送角色信息.灵饰数据[i] = 道具数据[发送角色信息.角色数据.灵饰[i]]
              end
            end
            发送数据(玩家数据[id].连接id,117,发送角色信息)
            发送角色信息 = nil
            道具数据 = nil
          elseif 序号==630 then
            local id = 内容.数字id
            local 目标id = 内容.编号
            local 密码 = 内容.价格
            for i=1,#藏宝阁数据.角色 do
              if 藏宝阁数据.角色[i].所有者 == 目标id or tonumber(藏宝阁数据.角色[i].所有者) == tonumber(目标id) then
                if 藏宝阁数据.角色[i].取回密码 ~= 密码 then
                  常规提示(id,"#Y密码错误请重新输入")
                  return
                else
                  local 角色信息 = table.loadstring(读入文件(程序目录..[[data/]]..藏宝阁数据.角色[i].角色信息.账号..[[/]]..藏宝阁数据.角色[i].所有者..[[/角色.txt]]))
                  角色信息.藏宝阁出售 = nil
                  写出文件(程序目录..[[data/]]..藏宝阁数据.角色[i].角色信息.账号..[[/]]..藏宝阁数据.角色[i].所有者..[[/角色.txt]],table.tostring(角色信息))
                  角色信息 = nil
                  table.remove(藏宝阁数据.角色,i)
                  for i,v in pairs(观察藏宝阁数据) do
                    if 玩家数据[i] ~= nil then
                      发送数据(玩家数据[i].连接id,116 , 藏宝阁数据)
                    else
                      观察藏宝阁数据[i] = nil
                    end
                  end
                  常规提示(id,"#Y恭喜角色成功取回,请原账号登录即可!!!")
                  return
                end
              end
            end
        end
end

function Network:藏宝阁取回处理(id,编号,类型)
  local 关键字 = "其他"
  if 类型 == 1 then
    关键字 = "装备"
  elseif 类型 == 3 then
    关键字 = "灵饰"
  elseif 类型 == 4 then
    关键字 = "召唤兽"
  elseif 类型 == 5 then
    关键字 = "银两"
  elseif 类型 == 6 then
    关键字 = "其他"
  end
  if 类型 == 7 then
    发送数据(玩家数据[id].连接id, 7, "#y/取回角色请前往XXXX取回")
    return 0
  elseif 藏宝阁数据[关键字][编号] == nil then
    发送数据(玩家数据[id].连接id, 7, "#y/数据已经刷新请重新打开藏宝阁")
    return 0
  elseif 藏宝阁数据[关键字][编号].结束时间 - os.time() <= 30 then
    发送数据(玩家数据[id].连接id, 7, "#y/该物品即将超时下架,无法进行取回处理")
    return 0
  elseif 藏宝阁数据[关键字][编号].所有者 ~= id then
    发送数据(玩家数据[id].连接id, 7, "#y/这不是你的商品你无法取回")
    return 0
  else
    if 关键字 ~= "银两" and 关键字 ~= "召唤兽" then
      self.临时格子=玩家数据[id].角色:取道具格子()
      if self.临时格子==0 then
       发送数据(玩家数据[id].连接id,7,"#y/您当前的包裹空间已满，无法取回")
        return 0
      end
      self.可用id = 玩家数据[id].道具:取新编号()
      玩家数据[id].道具.数据[self.可用id] = 藏宝阁数据[关键字][编号].物品
      玩家数据[id].角色.道具["包裹"][self.临时格子] = self.可用id
      table.remove(藏宝阁数据[关键字],编号)
      发送数据(玩家数据[id].连接id,7,"#y/取回物品成功")
    elseif 关键字 == "银两" then
      玩家数据[id].角色:添加银子(藏宝阁数据[关键字][编号].数额,"藏宝阁取回"..关键字,1)
      table.remove(藏宝阁数据[关键字],编号)
      发送数据(玩家数据[id].连接id,7,"#y/恭喜你取回物品成功")
    elseif 关键字 == "召唤兽" then
      if #玩家数据[id].召唤兽.数据 >= 7 then
        发送数据(玩家数据[id].连接id,7,"#y/您的召唤兽空间已满，无法取回")
          return 0
      else
        玩家数据[id].召唤兽.数据[#玩家数据[id].召唤兽.数据+1] = 藏宝阁数据[关键字][编号].召唤兽
        table.remove(藏宝阁数据[关键字],编号)
        发送数据(玩家数据[id].连接id,7,"#y/恭喜你取回物品成功")
      end
    end
    for i,v in pairs(观察藏宝阁数据) do
      if 玩家数据[i] ~= nil then
        发送数据(玩家数据[i].连接id,116 , 藏宝阁数据)
      else
        观察藏宝阁数据[i] = nil
      end
    end
  end
end

function Network:藏宝阁购买处理(id,编号,类型)
    local 玩家数据 = UserData;
    local 程序目录 = ServerDirectory;
  local 关键字 = "其他"
  if 类型 == 1 then
    关键字 = "装备"
  elseif 类型 == 3 then
    关键字 = "灵饰"
  elseif 类型 == 4 then
    关键字 = "召唤兽"
  elseif 类型 == 5 then
    关键字 = "银两"
  elseif 类型 == 6 then
    关键字 = "其他"
  elseif 类型 == 7 then
    关键字 = "角色"
  end
  if 藏宝阁数据[关键字][编号] == nil then
    发送数据(玩家数据[id].连接id, 7, "#y/数据已经刷新请重新打开藏宝阁")
    return 0
  elseif 藏宝阁数据[关键字][编号].结束时间 - os.time() <= 30 then
    发送数据(玩家数据[id].连接id, 7, "#y/该物品即将超时下架,无法进行购买处理")
    return 0
  else
    local 价格 = tonumber(藏宝阁数据[关键字][编号].价格)
    if 价格 == nil  then
        return
    end
    local 买家价格 = math.floor(价格)
    local 点卡=f函数.读配置(程序目录..[[data\]]..玩家数据[id].账号..[[\账号信息.txt]],"账号配置","点卡")
    if 点卡==nil or 点卡=="空" then
        f函数.写配置(程序目录..[[data\]]..玩家数据[id].账号..[[\账号信息.txt]],"账号配置","点卡","0")
        点卡=0
    end
    if 点卡+0<买家价格 then
      常规提示(id,"点卡不够哦!!请注意充值购买!!!")
      return
    end

    local 对方id = 藏宝阁数据[关键字][编号].所有者
    if 关键字 ~= "银两" and 关键字 ~= "召唤兽" and 关键字 ~= "角色" then
      self.临时格子=玩家数据[id].角色:取道具格子()
      if self.临时格子==0 then
        发送数据(玩家数据[id].连接id,7,"#y/您当前的包裹空间已满，无法购买")
        return 0
      end
      self.可用id = 玩家数据[id].道具:取新编号()
      玩家数据[id].道具.数据[self.可用id] = 藏宝阁数据[关键字][编号].物品
      玩家数据[id].角色.道具["包裹"][self.临时格子] = self.可用id
      藏宝阁记录 = 藏宝阁记录..玩家数据[id].角色.数据.名称.."通过藏宝阁购买ID:"..藏宝阁数据[关键字][编号].所有者.."物品["..藏宝阁数据[关键字][编号].物品.名称.."]花费"..价格.."银两\n"
      table.remove(藏宝阁数据[关键字],编号)
      发送数据(玩家数据[id].连接id,7,"#y/恭喜你购买物品成功")
    elseif 关键字 == "银两" then
      玩家数据[id].角色:添加银子(藏宝阁数据[关键字][编号].数额,"藏宝阁购买"..关键字,1)
      藏宝阁记录 = 藏宝阁记录..玩家数据[id].角色.数据.名称.."通过藏宝阁购买ID:"..藏宝阁数据[关键字][编号].所有者.."物品["..藏宝阁数据[关键字][编号].数额.."两银子]花费"..价格.."银两\n"
      table.remove(藏宝阁数据[关键字],编号)
      发送数据(玩家数据[id].连接id,7,"#y/恭喜你购买物品成功")
    elseif 关键字 == "召唤兽" then
      if #玩家数据[id].召唤兽.数据 >= 7 then
        发送数据(玩家数据[id].连接id,7,"#y/您的召唤兽空间已满，无法购买")
        return 0
      else
        玩家数据[id].召唤兽.数据[#玩家数据[id].召唤兽.数据+1] = 藏宝阁数据[关键字][编号].召唤兽
        藏宝阁记录 = 藏宝阁记录..玩家数据[id].角色.数据.名称.."通过藏宝阁购买ID:"..藏宝阁数据[关键字][编号].所有者.."物品["..藏宝阁数据[关键字][编号].召唤兽.名称.."]花费"..价格.."银两\n"
        table.remove(藏宝阁数据[关键字],编号)
        发送数据(玩家数据[id].连接id,7,"#y/恭喜你购买物品成功")
      end
    elseif 关键字 == "角色" then
      local 账号信息 = table.loadstring(读入文件(程序目录..[[data/]]..玩家数据[id].角色.数据.账号..[[/信息.txt]]))
      if #账号信息 >= 6 then
        发送数据(玩家数据[id].连接id,7,"#y/您的账号信息已满,无法购买新的角色")
        return
      elseif 玩家数据[id].角色.数据.账号 == 藏宝阁数据[关键字][编号].角色信息.账号 then
        发送数据(玩家数据[id].连接id,7,"#y/你不能购买自己的角色")
        return
      end
      账号信息[#账号信息+1] = 藏宝阁数据[关键字][编号].所有者
      写出文件(程序目录..[[data/]]..玩家数据[id].角色.数据.账号..[[/]].."/信息.txt",table.tostring(账号信息))
      账号信息 = {}
      local 账号 = 藏宝阁数据[关键字][编号].角色信息.账号
      账号信息 = table.loadstring(读入文件(程序目录..[[data/]]..账号..[[/信息.txt]]))
      for i=1,#账号信息 do
        if 账号信息[i] == 藏宝阁数据[关键字][编号].所有者 or tonumber(账号信息[i]) == 藏宝阁数据[关键字][编号].所有者 then
          table.remove(账号信息,i)
        end
      end
      写出文件(程序目录..[[data/]]..账号..[[/]].."/信息.txt",table.tostring(账号信息))
      local 角色信息=table.loadstring(读入文件(程序目录..[[data/]]..账号..[[/]]..藏宝阁数据[关键字][编号].所有者..[[/角色.txt]]))
      角色信息.账号 = 玩家数据[id].角色.数据.账号
      角色信息.藏宝阁出售 = nil
      写出文件(程序目录..[[data/]]..账号..[[/]]..藏宝阁数据[关键字][编号].所有者..[[/角色.txt]],table.tostring(角色信息))
      角色信息 = nil
      if 移动文件(账号,tostring(藏宝阁数据[关键字][编号].所有者),玩家数据[id].角色.数据.账号) == "0" then
        藏宝阁记录 = 藏宝阁记录..玩家数据[id].角色.数据.名称.."ID为:"..id.."通过藏宝阁购买ID:"..藏宝阁数据[关键字][编号].所有者.."角色花费"..价格.."银两,角色移除异常,请检查!\n"
      else
        藏宝阁记录 = 藏宝阁记录..玩家数据[id].角色.数据.名称.."ID为:"..id.."通过藏宝阁购买ID:"..藏宝阁数据[关键字][编号].所有者.."角色花费"..价格.."银两,角色转换成功!\n"
      end
      table.remove(藏宝阁数据[关键字],编号)
      发送数据(玩家数据[id].连接id,7,"#y/恭喜你购买物品成功")
    end
    玩家数据[id].角色:扣除点卡(买家价格,"藏宝阁购买",id)
    发送数据(玩家数据[id].连接id,7,"#y/支付:"..买家价格.."点点卡!")
    if 玩家数据[对方id] == nil then
      if 寄存数据[对方id] == nil then
        寄存数据[对方id] = {[1]={类型="点卡",数额=价格}}
      else
        寄存数据[对方id][#寄存数据[对方id]+1] = {类型="点卡",数额=价格}
      end
    else
        添加点卡(math.floor(价格),玩家数据[对方id].账号,对方id,"藏宝阁出售")
        发送数据(玩家数据[对方id].连接id,7,"#y/藏宝阁物品成功出售,并获得"..价格.."点点卡!!!")
    end
    for i,v in pairs(观察藏宝阁数据) do
      if 玩家数据[i] ~= nil then
        发送数据(玩家数据[i].连接id,116 , 藏宝阁数据)
      else
        观察藏宝阁数据[i] = nil
      end
    end
  end
end

function Network:藏宝阁上架召唤兽(id,编号,价格)
    local 玩家数据 = UserData;
  if 玩家数据[id].召唤兽.数据[编号] == nil then
    发送数据(玩家数据[id].连接id, 7, "#y/你没有这样的召唤兽")
    return 0
  -- elseif 玩家数据[id].召唤兽:是否有装备(编号)  then
  --   发送数据(玩家数据[id].连接id, 7, "#y/请将该召唤兽装备卸下后再进行交易")
  --   return 0
  elseif tonumber(价格) == nil or tonumber(价格) < 1 then
    发送数据(玩家数据[id].连接id, 7, "#y/请正确输入物品的价格")
    return 0
  -- elseif 玩家数据[id].召唤兽:是否有统御()  then
  --   发送数据(玩家数据[id].连接id, 7, "#y/请将所有召唤兽统御效果撤除后再进行交易")
  --   return 0
  elseif #玩家数据[id].召唤兽.数据[编号].技能 <  7 then
    发送数据(玩家数据[id].连接id, 7, "#y/召唤兽上架技能要求最少8个")
    return 0
  -- elseif 玩家数据[id].召唤兽.数据[编号].成长 <  1.3 then
  --   发送数据(玩家数据[id].连接id, 7, "#y/召唤兽上架技能要求最少12个")
  --   return 0
  else
    藏宝阁数据["召唤兽"][#藏宝阁数据["召唤兽"]+1] = {召唤兽=玩家数据[id].召唤兽.数据[编号],价格=价格,所有者=id,结束时间=os.time()+204800}
    if 玩家数据[id].召唤兽.数据[编号].认证码==玩家数据[id].角色.数据.参战宝宝.认证码 then
      玩家数据[id].角色.数据.参战宝宝={}
      发送数据(玩家数据[id].连接id,18,玩家数据[id].角色.数据.参战宝宝)
    end
    发送数据(玩家数据[id].连接id,45,编号)
    table.remove(玩家数据[id].召唤兽.数据,编号)
    发送数据(玩家数据[id].连接id, 7, "#y/上架成功")
    for i,v in pairs(观察藏宝阁数据) do
      if 玩家数据[i] ~= nil then
        发送数据(玩家数据[i].连接id,116 , 藏宝阁数据)
      else
        观察藏宝阁数据[i] = nil
      end
    end
  end
end


function Network:藏宝阁上架(id,编号,价格) --上架手续费 第二要限制出售类型 装备类 120级以上 其他
  local 玩家数据 = UserData
  local 物品id = 玩家数据[id].角色.道具["包裹"][编号]
  local 道具类型 = UserData[id].物品[物品id].类型
  local 道具名称 = UserData[id].物品[物品id].名称
  -- local 道具类型 = 玩家数据[id].道具.数据[物品id].分类
  local 类型 = "其他"

  if 玩家数据[id].角色.道具["包裹"][编号] == nil then
    发送数据(玩家数据[id].连接id, 7, "#y/你没有这样的道具")
    return 0
  elseif tonumber(价格) == nil or tonumber(价格) < 1 then
    发送数据(玩家数据[id].连接id, 7, "#y/请正确输入物品的价格")
    return 0
  elseif 道具名称 == "飞行符"
    or 道具名称 == "摄妖香"
     or 道具名称 == "珍珠"
      or 道具名称 == "天眼通符"
        or 道具名称 == "金柳露"
         or 道具名称 == "超级金柳露"
          or 道具名称 == "玄武石"
           or 道具名称 == "青龙石"
            or 道具名称 == "白虎石"
             or 道具名称 == "朱雀石"
              or 道具名称 == "海马"
               or 道具名称 == "秘制红罗羹"
                or 道具名称 == "秘制绿罗羹"
                 or 道具名称 == "洞冥草"
                  or 道具名称 == "月华露"
                   or 道具名称 == "修炼果"
                    or 道具名称 == "九转金丹"
                     or 道具名称 == "易经丹"
                      or 道具名称 == "玉葫灵髓"
                       or 道具名称 == "清灵净瓶"
                        or 道具名称 == "彩果"
                         or 道具名称 == "金银锦盒"
                          or 道具名称 == "空白强化符"
                           or 道具名称 == "碎星锤"
                            or 道具名称 == "神兵图鉴"
                             or 道具名称 == "灵宝图鉴"
                              or 道具名称 == "含沙射影"
                               or 道具名称 == "符石卷轴"
                                or 道具名称 == "召唤兽内丹"
                                 or 道具名称 == "元宵"
                                  or 道具名称 == "低级清灵净瓶"
                                   or 道具名称 == "中级清灵净瓶"
                                    or 道具名称 == "百炼精铁"
                                     or 道具名称 == "魔兽要诀"
                                      or 道具名称 == "九转金丹"  then
        发送数据(玩家数据[id].连接id, 7, "#y/请勿上架低价值的物品")
        return 0
  -- elseif 玩家数据[id].角色.道具["包裹"][编号].总类 == 1 and 玩家数据[id].角色.道具["包裹"][编号].子类 == 20 then
  --     发送数据(玩家数据[id].连接id, 7, "#y/请勿上架低价值的物品")
  --      return 0
  -- elseif 玩家数据[id].角色.道具["包裹"][编号].总类 == 1 and 玩家数据[id].角色.道具["包裹"][编号].子类 == 1 then
  --     发送数据(玩家数据[id].连接id, 7, "#y/请勿上架低价值的物品")
  --      return 0
  -- elseif 玩家数据[id].角色.道具["包裹"][编号].总类 == 55 and 玩家数据[id].角色.道具["包裹"][编号].子类 == 1 then
  --     发送数据(玩家数据[id].连接id, 7, "#y/请勿上架低价值的物品")
  --      return 0
  -- elseif 玩家数据[id].道具.数据[物品id].总类 == 2 and 道具类型 >=1 and 道具类型 <= 6 and 玩家数据[id].角色.道具["包裹"][编号].级别限制 < 140 then
  --     发送数据(玩家数据[id].连接id, 7, "#y/低于140的装备属于低价值物品,无法上架藏宝阁")
  --     return 0
  -- elseif 玩家数据[id].道具.数据[物品id].总类 == 2 and 道具类型 >=10 and 道具类型 <=13 and 玩家数据[id].角色.道具["包裹"][编号].级别限制 < 100 then
  --     发送数据(玩家数据[id].连接id, 7, "#y/低于100的灵饰属于低价值物品,无法上架藏宝阁")
  --     return 0
  else
    -- if 道具类型 ~= nil then
    --   if 玩家数据[id].道具.数据[物品id].总类 == 2 and 道具类型 >=1 and 道具类型 <= 6 then
    --     类型 = "装备"
    --   elseif 玩家数据[id].道具.数据[物品id].总类 == 2 and 道具类型 >=10 and 道具类型 <=13 then
    --     类型 = "灵饰"
    --   end
    -- end
    print_r(UserData[id].物品[物品id])
    藏宝阁数据[类型][#藏宝阁数据[类型]+1] = {物品=UserData[id].物品[物品id],价格=价格,所有者=id,结束时间=os.time()+172800}
    UserData[id].物品[物品id] = nil
    UserData[id].角色.道具.包裹[物品id] = nil
    -- 道具刷新(id)
    发送数据(玩家数据[id].连接id, 7, "#y/上架成功")
    for i,v in pairs(观察藏宝阁数据) do
      if 玩家数据[i] ~= nil then
        发送数据(玩家数据[i].连接id,116 , 藏宝阁数据)
      else
        观察藏宝阁数据[i] = nil
      end
    end
  end
end
function Network:藏宝阁上架银两(id,数量,价格)
  if tonumber(数量) == nil or tonumber(价格) ==nil then
    return
  end
  if UserData[id].角色.道具.货币.银子<数量 then
    发送数据(UserData[id].连接id,7,"#y/你没那么多银子")
  elseif tonumber(价格) == nil or tonumber(价格) < 1 then
    发送数据(UserData[id].连接id, 7, "#y/请正确输入价格")
    return 0
  else
    RoleControl:扣除银子(UserData[id],数量,"上架藏宝阁")
      -- UserData[id].角色:扣除银子(数量,0,0,"上架藏宝阁")
      藏宝阁数据["银两"][#藏宝阁数据["银两"]+1] = {数额=数量,价格=价格,所有者=id,结束时间=os.time()+86400}
      发送数据(UserData[id].连接id, 7, "#y/上架成功")
    for i,v in pairs(观察藏宝阁数据) do
      if UserData[i] ~= nil then
        发送数据(UserData[i].连接id,116, 藏宝阁数据)
      else
        观察藏宝阁数据[i] = nil
      end
    end
    end
end

function Network:退出处理(内容, 强制)
    内容 = 内容 + 0
    if UserData[内容]~= nil and  UserData[内容].摆摊 and 强制==nil  then
      SendMessage(UserData[内容].连接id,99999,"退出处理")
      return 0
    end
    if UserData[内容] ~= nil then
        if UserData[内容].观战模式 and UserData[内容].战斗 ~= 0 then
          FightGet.战斗盒子[UserData[内容].战斗]:退出观战(内容)
          UserData[内容].战斗 = 0
        end
        if UserData[内容].战斗 ~= 0 then
            UserData[内容].下线标记 = true
            if UserData[内容].队伍 ~= 0 then
              广播队伍消息(内容, 21, "#dw/#g/" .. UserData[内容].角色.名称 .. "#y/玩家已经掉线……")
            end
        else
            if UserData[内容].角色.帮派 ~= nil and 帮派数据[UserData[内容].角色.帮派].成员名单[内容] ~= nil then
             帮派数据[UserData[内容].角色.帮派].成员名单[内容].离线时间 = os.time()
            end
            if UserData[内容].队伍 ~= 0 then
             TeamControl:离开队伍(UserData[内容].队伍, 内容, 1)
            end
            MapControl:移除玩家(UserData[内容].地图, 内容)
            RoleControl:存档(UserData[内容])
            SendMessage(UserData[内容].连接id,99999,"退出处理1")
            UserData[内容]=nil
        end
    end
end
function Network:角色处理(内容)
        if 内容.序号 == 1 then
          SendMessage(UserData[内容.数字id].连接id,2016,RoleControl:获取角色数据(UserData[内容.数字id]))
        elseif 内容.序号 == 2 then
          RoleControl:升级请求(UserData[内容.数字id])
        elseif 内容.序号 == 3 then
           RoleControl:加点事件(UserData[内容.数字id], 内容.文本)
        elseif 内容.序号 == 4 then
          SendMessage(UserData[内容.数字id].连接id,2068,RoleControl:索取奇经八脉(UserData[内容.数字id]))
        elseif 内容.序号 == 5 then
          RoleControl:学习奇经八脉(UserData[内容.数字id],内容.参数, 内容.文本)
        elseif 内容.序号 == 6 then
          SendMessage(UserData[内容.数字id].连接id,2069,RoleControl:索取奇经八脉(UserData[内容.数字id]))
        elseif 内容.序号 ==7 then
           RoleControl:兑换乾元丹(UserData[内容.数字id])
        elseif 内容.序号 ==8 then
           RoleControl:修炼学习(UserData[内容.数字id],内容.参数+0,1)
        elseif 内容.序号 ==9 then
          RoleControl:修炼学习(UserData[内容.数字id],内容.参数+0,10)
        elseif 内容.序号 ==10 then
          RoleControl:兑换潜能果(UserData[内容.数字id])
        elseif 内容.序号 ==11 then
          RoleControl:临时技能使用(UserData[内容.数字id],内容.参数+0,内容.文本+0)
        elseif 内容.序号 ==12 then
          RoleControl:设置当前修炼(UserData[内容.数字id],内容.文本,内容.编号)
        elseif 内容.序号 == 13 then
          SendMessage(UserData[内容.数字id].连接id,2002,RoleControl:获取角色数据(UserData[内容.数字id]))
        elseif 内容.序号 == 14 then
          RoleControl:购买祈福(UserData[内容.数字id],内容.文本)
        elseif 内容.序号 == 15 then
          RoleControl:创建副本(UserData[内容.数字id],内容.文本)
        elseif 内容.序号 == 16 then
           RoleControl:进入副本(UserData[内容.数字id],内容.文本)
        elseif 内容.序号 == 17 then
           RoleControl:重置经脉(UserData[内容.数字id])
        elseif 内容.序号 == 18 then
          RoleControl:学习剧情点(UserData[内容.数字id])
        elseif 内容.序号 == 19 then
          RoleControl:学习剧情点1(UserData[内容.数字id], 内容.文本)
        elseif 内容.序号 == 20 then
            RoleControl:删除角色(UserData[内容.数字id], 内容.文本)
        elseif 内容.序号 == 21 then
          SendMessage(UserData[内容.数字id].连接id,2003,RoleControl:获取活跃度(UserData[内容.数字id]))
        elseif 内容.序号 == 22 then
          RoleControl:活跃度奖励(UserData[内容.数字id],内容.参数)
        elseif 内容.序号 == 23 then
          RoleControl:更新好友数据(UserData[内容.数字id],内容.参数)
        elseif 内容.序号 == 24 then
          local 好友id = 内容.参数+0
          if 消息数据[好友id]==nil then
          消息数据[好友id]={}
          end
          消息数据[好友id][#消息数据[好友id]+1]={名称=UserData[内容.数字id].角色.名称,内容=内容.文本,id=内容.数字id,时间=os.date("[%Y-%m-%d-%X]"),造型=UserData[内容.数字id].角色.造型,等级=UserData[内容.数字id].角色.等级,好友度=0}
          SendMessage(UserData[内容.数字id].连接id,1031,{信息=UserData[内容.数字id].角色.名称.."   "..os.date("[%Y-%m-%d-%X]"),id=好友id,造型=UserData[内容.数字id].角色.造型,内容=内容.文本})
          self:更新消息通知(好友id)
        elseif 内容.序号==25 then
          self:取消息数据(内容.数字id)
        elseif 内容.序号==26 then
          if self:取消息数据(内容.数字id)==false then
          SendMessage(UserData[内容.数字id].连接id,2078,UserData[内容.数字id].角色.好友数据)
          end
        elseif 内容.序号==27 then
          local 查找数据=self:查找角色(内容.文本,内容.编号)
          if 查找数据.名称 then
          SendMessage(UserData[内容.数字id].连接id,1033,查找数据)
          else
          SendMessage(UserData[内容.数字id].连接id,7,"#Y/这个角色并不存在或当前没有在线")
          return
          end
        elseif 内容.序号==28 then
          SendMessage(UserData[内容.数字id].连接id, 20032, RoleControl:取坐骑数据(UserData[内容.数字id]))
         elseif 内容.序号==29 then
            RoleControl:GetChildrenData(UserData[内容.数字id])
        elseif 内容.序号==30 then
            RoleControl:DeleteChildren(UserData[内容.数字id],内容.参数)
        elseif 内容.序号==31 then
            RoleControl:Children设置成长之路(UserData[内容.数字id],内容.参数,内容.文本)
        elseif 内容.序号==32 then 
             RoleControl:Children设置参战(UserData[内容.数字id],内容.参数)
        elseif 内容.序号==33 then 
             RoleControl:获取神器数据(UserData[内容.数字id])
        elseif 内容.序号==34 then 
             RoleControl:神器洗炼属性(UserData[内容.数字id])
        elseif 内容.序号==35 then 
          RoleControl:神器洗炼技能(UserData[内容.数字id])
        elseif 内容.序号==36 then 
          RoleControl:神器补充灵气(UserData[内容.数字id])
        elseif 内容.序号==37 then 
          RoleControl:黑市拍卖(UserData[内容.数字id])

        end
end
function Network:查找角色(名称,id)
  local 数据组={}
  if id~="" then id=id+0 end
  for i, v in pairs(UserData) do
    if UserData[i].角色.名称==名称 or i==id then
      数据组.名称=UserData[i].角色.名称
      数据组.id=i
      数据组.等级=UserData[i].角色.等级
      数据组.门派=UserData[i].角色.门派
    end
  end
  return 数据组
end
function Network:取消息数据(id)
  if UserData[id]~=nil and 消息数据[id]~=nil and #消息数据[id]>0 then
    SendMessage(UserData[id].连接id,1030,{信息=消息数据[id][1].名称.."   "..消息数据[id][1].时间,id=消息数据[id][1].id,内容=消息数据[id][1].内容,造型=消息数据[id][1].造型,名称=消息数据[id][1].名称,好友度=0,等级=消息数据[id][1].等级})
    table.remove(消息数据[id],1)
    if #消息数据[id]==0 then
      SendMessage(UserData[id].连接id,1029,"1")
    end
    return true
  else
    SendMessage(UserData[id].连接id,1029,"1")
    return false
  end
end
function Network:更新消息通知(id)
  if UserData[id]~=nil and 消息数据[id]~=nil and #消息数据[id]>0  then
    SendMessage(UserData[id].连接id,1032,"1")
  end
end
function Network:进入游戏处理(内容)
    self.账号信息 = 内容
    if self.账号信息.账号 ==nil  then
		return
    end
    local 是否封禁 = f函数.读配置(ServerDirectory.."玩家信息/账号" .. self.账号信息.账号 .. "/账号.txt", "账号信息", "封禁")
    if 是否封禁 ~= "0" then
        self:强制断开1(self.账号信息.id,"你的账号已经被管理员封禁，你已经被强制断开连接。")
      	return
    end
    if not file_exists([[玩家信息/账号]]..self.账号信息.账号..[[/操作记录.txt]]) then 
        __S服务:输出(string.format("玩家账号%s操作记录文件不存在请及时删除",self.账号信息.账号))
        return 
    end
    local 写入信息=table.loadstring( ReadFile([[玩家信息/账号]]..self.账号信息.账号..[[/操作记录.txt]]))
    local 选择id = 写入信息[self.账号信息.编号+0]
	if 选择id == nil then
	    return
	end
	local 临时角色 =  ReadFile("玩家信息/账号" .. self.账号信息.账号 ..[[/]]..选择id.. "/角色.txt")
	临时角色 = table.loadstring(临时角色)
	if UserData[临时角色.id] == nil or UserData[临时角色.id].假人  then
		UserData[临时角色.id] = {
			id = 临时角色.id,
			连接id =self.账号信息.id,
			仙玉 = f函数.读配置(ServerDirectory.."玩家信息/账号" .. self.账号信息.账号 .. "/账号.txt", "账号信息", "仙玉") + 0,
			--管理 = f函数.读配置(ServerDirectory.."玩家信息/账号" .. self.账号信息.账号 .. "/账号.txt", "账号信息", "管理"),
			管理 = false,
			ip=self.账号信息.ip,
			角色=加载角色数据(临时角色),
			日志编号= ReadFile("玩家信息/账号" .. self.账号信息.账号 .. [[/]]..选择id.."/日志编号.txt"),
			账号 = self.账号信息.账号,
			在线时间 = 0,小时 = 0,分 = 0,秒 = 0,
			遇怪时间 = {起始 = os.time(),间隔 = math.random(8, 20)},
			世界频道 = os.time(),传闻频道 = os.time(),队伍频道 = os.time(),门派频道 = os.time(),
			队伍 = 0,
			观战模式 = false,
			加锁 =false,
			队长 = false,
			当前频道 = os.time(),
			首席保护 = 0,
			请求数量 = 0,
			战斗对象 = 0,
			副本 = 0,
			付费 = false,
			下线标记 = false,
			状态 = 1,
			移动目标 = 0,
			npc = 0,
			比武保护期 = 0,
			任务id = 0,
			战斗 = 0,
			交易 = 0,
			召唤兽id = 0,
			道具id = 0,
			包裹类型 = 0,
			给予物品 = true,
			物品=加载item数据( ReadFile("玩家信息/账号" .. self.账号信息.账号 .. [[/]]..选择id.."/道具.txt"))
			}

		UserData[临时角色.id].消费日志 =  ReadFile("玩家信息/账号" .. self.账号信息.账号 .. [[/]]..选择id.."/日志/" .. UserData[临时角色.id].日志编号 .. ".txt")
		UserData[临时角色.id].地图 = UserData[临时角色.id].角色.地图数据.编号
		UserData[临时角色.id].助战 = require("Script/QQ2124872/助战系统")(临时角色.id)
		UserData[临时角色.id].召唤兽 = require("Script/Pet/PetControl")(临时角色.id)
		UserData[临时角色.id].召唤兽:加载数据( ReadFile("玩家信息/账号" .. self.账号信息.账号 .. [[/]]..选择id.."/召唤兽.txt"), 临时角色.id)
		UserData[临时角色.id].助战:加载数据(self.账号信息.账号,临时角色.id)
		if  UserData[临时角色.id].角色.坐骑.类型  then
			RoleControl:刷新战斗属性(UserData[临时角色.id],1)
		end
		RoleControl:进入游戏处理(UserData[临时角色.id])
		RoleControl:道具异常处理(UserData[临时角色.id],临时角色.id)
		SendMessage(self.账号信息.id, 1003, RoleControl:获取角色气血数据(UserData[临时角色.id]))
		SendMessage(self.账号信息.id,5,RoleControl:获取地图数据(UserData[临时角色.id]))
		SendMessage(self.账号信息.id,30,cjson.encode({id=临时角色.id,User=临时角色.账号,Name=UserData[临时角色.id].角色.名称}))
		SendMessage(self.账号信息.id, 2005, UserData[临时角色.id].召唤兽:取头像数据())
		MapControl:加入玩家(临时角色.id)
		UserData[临时角色.id].帮派进入编号 = UserData[临时角色.id].角色.帮派
     if UserData[临时角色.id].地图 == 5135 then
      MapControl:Jump(UserData[临时角色.id].id, 1001, 364, 36)
    elseif UserData[临时角色.id].地图 == 6001 or UserData[临时角色.id].地图 == 6002 or UserData[临时角色.id].地图 == 6003 then
      MapControl:Jump(UserData[临时角色.id].id, 1001, 429, 164)
    elseif UserData[临时角色.id].地图 == 3131 or UserData[临时角色.id].地图 == 3132 then
      MapControl:Jump(UserData[临时角色.id].id, 1001, 284, 83)
    elseif UserData[临时角色.id].地图 == 4131 or UserData[临时角色.id].地图 == 4132 or UserData[临时角色.id].地图 == 4133 then
      MapControl:Jump(UserData[临时角色.id].id, 1070, 127, 145)
    elseif UserData[临时角色.id].地图 == 1875 or UserData[临时角色.id].地图 == 1815 or UserData[临时角色.id].地图 == 1825 or UserData[临时角色.id].地图 == 1835 or UserData[临时角色.id].地图 == 1845 or UserData[临时角色.id].地图 == 1855 or UserData[临时角色.id].地图 == 1865 or UserData[临时角色.id].地图 == 1875 then
      MapControl:Jump(UserData[临时角色.id].id, 1001, 419, 8)
    end
      if  UserData[临时角色.id].地图 == 11351 or UserData[临时角色.id].地图 == 11352 or UserData[临时角色.id].地图 == 11353 or UserData[临时角色.id].地图 == 11354 or UserData[临时角色.id].地图 == 11355 then
         MapControl:Jump(UserData[临时角色.id].id, 1001, 364, 36)
      end
      if  UserData[临时角色.id].地图 == 1241  then
         UserData[临时角色.id].迷宫=true
         MapControl:Jump(UserData[临时角色.id].id, 1001, 364, 36)
      end
      if  帮派竞赛.宝箱开关==false and  UserData[临时角色.id].地图 == 1380  then
         MapControl:Jump(UserData[临时角色.id].id, 1001, 364, 36)
      end
       

		else
			if UserData[临时角色.id].观战模式 and UserData[临时角色.id].战斗 ~= 0 then
				FightGet.战斗盒子[UserData[临时角色.id].战斗]:退出观战(临时角色.id)
				UserData[临时角色.id].战斗 = 0
			end
			if UserData[临时角色.id].战斗 == 0 then
				if UserData[临时角色.id].摆摊 ~= nil then
					 UserData[临时角色.id].摆摊 = nil
              摊位数据[临时角色.id]=nil
          MapControl.MapData[UserData[临时角色.id].地图].摆摊人数=MapControl.MapData[UserData[临时角色.id].地图].摆摊人数-1
					MapControl:更新摊位(临时角色.id, "", UserData[临时角色.id].地图)
				end
				self.下线id = UserData[临时角色.id].连接id
        self:强制断开1(self.下线id,"您的账号已经在其它设备上登录，您被迫下线")
      
				UserData[临时角色.id].连接bid = self.账号信息.id
				UserData[临时角色.id].下线标记 = false
				UserData[临时角色.id].观战模式 = false
				UserData[临时角色.id].加锁 =false
				UserData[临时角色.id].连接id =self.账号信息.id
        
				MapControl:加入玩家(临时角色.id)
        SendMessage(self.账号信息.id, 1003, RoleControl:获取角色气血数据(UserData[临时角色.id]))
				SendMessage(self.账号信息.id,5,RoleControl:获取角色数据(UserData[临时角色.id]))
        SendMessage(self.账号信息.id,30,cjson.encode({id=临时角色.id,User=临时角色.账号,Name=UserData[临时角色.id].角色.名称}))
				SendMessage(self.账号信息.id,2005, UserData[临时角色.id].召唤兽:取头像数据())
				RoleControl:发送欢迎信息(UserData[临时角色.id])
            if UserData[临时角色.id].队长 then
              SendMessage(UserData[临时角色.id].连接id, 5002, 队伍数据[UserData[临时角色.id].队伍].队标)
            end
			else
				self.下线id = UserData[临时角色.id].连接id
				if FightGet.战斗盒子[UserData[临时角色.id].战斗] == nil then
           self:强制断开1(self.下线id,"战斗数据异常，请等待战斗结束后再尝试登录")
				else
          self:强制断开1(self.下线id,"您的账号已经在其它设备上登录，您被迫下线")
					UserData[临时角色.id].连接bid = self.账号信息.id
					UserData[临时角色.id].下线标记 = false
					UserData[临时角色.id].观战模式 = false
					UserData[临时角色.id].加锁 =false
					UserData[临时角色.id].连接id =self.账号信息.id
					MapControl:加入玩家(临时角色.id)
          SendMessage(self.账号信息.id, 1003, RoleControl:获取角色气血数据(UserData[临时角色.id]))
					SendMessage(self.账号信息.id,5,RoleControl:获取角色数据(UserData[临时角色.id]))
				  SendMessage(self.账号信息.id,30,cjson.encode({id=临时角色.id,User=临时角色.账号,Name=UserData[临时角色.id].角色.名称}))
					SendMessage(self.账号信息.id, 2005, UserData[临时角色.id].召唤兽:取头像数据())
					RoleControl:发送欢迎信息(UserData[临时角色.id])
					FightGet.战斗盒子[UserData[临时角色.id].战斗]:重连数据发送(临时角色.id)
					if UserData[临时角色.id].队伍 ~= 0 then
						TeamControl:发送队伍信息(UserData[临时角色.id].队伍, 5009, UserData[临时角色.id].连接id)
						广播队伍消息(临时角色.id, 21, "#dw/#g/" .. UserData[临时角色.id].角色.名称 .. "#y/玩家已经上线……")
						if UserData[临时角色.id].队长 then
							 SendMessage(UserData[临时角色.id].连接id, 5002, 队伍数据[UserData[临时角色.id].队伍].队标)
						end
					end
				end
			end
		end
    UserData[临时角色.id].屏蔽变身=false
     UserData[临时角色.id].屏蔽给予=false
     UserData[临时角色.id].屏蔽交易=false
     UserData[临时角色.id].屏蔽查看=false
     UserData[临时角色.id].屏蔽定制=false
end
function Network:假人进入游戏处理(内容,地图)
    self.账号信息 = 内容
    local 是否封禁 = f函数.读配置(ServerDirectory.."玩家信息/" .. self.账号信息.账号 .. "/账号.txt", "账号信息", "封禁")
   if 是否封禁 ~= "0" then
         self:强制断开1(self.账号信息.id,"你的账号已经被管理员封禁，你已经被强制断开连接。" )

    	 return
   end
      if not file_exists([[玩家信息/]]..内容.账号..[[/操作记录.txt]]) then 
         __S服务:输出(string.format("玩家账号%s操作记录文件不存在请及时删除",内容.账号))
        return 
      end
     local 写入信息=table.loadstring( ReadFile([[玩家信息/]]..self.账号信息.账号..[[/操作记录.txt]]))
     local 选择id = 写入信息[self.账号信息.编号+0]
	  if 选择id==nil then
	     return
	  end

	 local 临时角色 =  ReadFile("玩家信息/" .. self.账号信息.账号 ..[[/]]..选择id.. "/角色.txt")
	  临时角色 = table.loadstring(临时角色)
		if UserData[临时角色.id] == nil then
			UserData[临时角色.id] = {
				角色 = 临时角色,
				id = 临时角色.id,
				连接id =self.账号信息.id,-- self.账号信息.id,
        假人=true,
			}
  if type(UserData[临时角色.id].名称)~="string" then
    UserData[临时角色.id].名称="名字"..UserData[临时角色.id].id
  end
    if string.len(UserData[临时角色.id].名称)>15 or UserData[临时角色.id].名称=="" then
    UserData[临时角色.id].名称="名字"..UserData[临时角色.id].id
    elseif string.find(UserData[临时角色.id].名称,[[\]])~=nil  then
    UserData[临时角色.id].名称="名字"..UserData[临时角色.id].id
    elseif string.find(UserData[临时角色.id].名称,[[/]])~=nil  then
    UserData[临时角色.id].名称="名字"..UserData[临时角色.id].id
    elseif string.find(UserData[临时角色.id].名称,[[@]])~=nil  then
    UserData[临时角色.id].名称="名字"..UserData[临时角色.id].id
    elseif string.find(UserData[临时角色.id].名称,[[#]])~=nil  then
    UserData[临时角色.id].名称="名字"..UserData[临时角色.id].id
    elseif string.find(UserData[临时角色.id].名称,[[*]])~=nil  then
    UserData[临时角色.id].名称="名字"..UserData[临时角色.id].id
    elseif string.find(UserData[临时角色.id].名称,[[ ]])~=nil  then
    UserData[临时角色.id].名称="名字"..UserData[临时角色.id].id
    elseif string.find(UserData[临时角色.id].名称,"老G")~=nil  then
    UserData[临时角色.id].名称="名字"..UserData[临时角色.id].id
    elseif string.find(UserData[临时角色.id].名称,"老g")~=nil  then
    UserData[临时角色.id].名称="名字"..UserData[临时角色.id].id
    elseif string.find(UserData[临时角色.id].名称,"炸服")~=nil  then
    UserData[临时角色.id].名称="名字"..UserData[临时角色.id].id
    end
			UserData[临时角色.id].召唤兽 = require("Script/Pet/PetControl")(临时角色.id)
			UserData[临时角色.id].召唤兽:加载数据( ReadFile("玩家信息/" .. self.账号信息.账号 .. [[/]]..选择id.."/召唤兽.txt"), 临时角色.id)
			UserData[临时角色.id].账号 = self.账号信息.账号
			UserData[临时角色.id].队伍 = 0
			UserData[临时角色.id].加锁 =false
			UserData[临时角色.id].队长 = false
			UserData[临时角色.id].当前频道 = os.time()
			UserData[临时角色.id].遇怪时间 = {起始 = os.time(),间隔 = math.random(8, 20)}
			UserData[临时角色.id].地图 = UserData[临时角色.id].角色.地图数据.编号
			UserData[临时角色.id].副本 = 0
			UserData[临时角色.id].下线标记 = false
			UserData[临时角色.id].状态 = 1
			UserData[临时角色.id].战斗 = 0
			UserData[临时角色.id].交易 = 0
			UserData[临时角色.id].给予物品 = false
			UserData[临时角色.id].物品=加载item数据( ReadFile("玩家信息/" .. self.账号信息.账号 .. [[/]]..选择id.."/道具.txt"))
      RoleControl:发送欢迎信息(UserData[临时角色.id])
			MapControl:加入玩家(临时角色.id)
      if UserData[临时角色.id].角色.称谓.特效~=4285922956 then
          UserData[临时角色.id].角色.称谓.特效=4285922956
      end
       local 临时坐标= MapControl:Randomloadtion(地图)
			     MapControl:Jump(临时角色.id, 地图, 临时坐标.x, 临时坐标.y)

          local ff = function(地图,坐标)
                    if 地图 ==1001 and  (取两点距离({x=373,y=49},坐标)<30 or 取两点距离({x=224,y=114},坐标)<50)  then
                        return true
                    elseif 地图 ==1226 and   取两点距离({x=110,y=50},坐标)<30 then
                          return true
                    elseif 地图 ==1070 and 取两点距离({x=132,y=129},坐标)<60 then

                        return true
                    elseif 地图 ==1092 and 取两点距离({x=134,y=46},坐标)<40 then
                           return true
                    elseif 地图 ==1501 and  取两点距离({x=64,y=111},坐标)<30 then
                          return true
                    else 
                        return false
                    end
            end 
          if ff(地图,临时坐标) then
                
            	摆摊处理类:发送摊位信息(临时角色.id)
          end
           MapControl.MapData[地图].假人=MapControl.MapData[地图].假人+1
		end
end

function Network:名称检查(姓名, idd, ip)
	if string.len(姓名) < 4 then
		SendMessage(idd,7,"#y/角色名称长度过短")
		return false
	elseif string.len(姓名) > 15 then
		SendMessage(idd,7,"#y/角色名称长度过长")
		return false
	elseif 姓名 == "" then
			SendMessage(idd,7,"#y/角色名称不能为空")
		return false
	elseif string.find(姓名, "/") ~= nil then
		SendMessage(idd,7,"#y/角色名称含有特殊符号")
		return false
	elseif string.find(姓名, "@") ~= nil then
		SendMessage(idd,7,"#y/角色名称含有特殊符号")
		return false
	elseif string.find(姓名, "#") ~= nil then
		SendMessage(idd,7,"#y/角色名称含有特殊符号")
		return false
	elseif string.find(姓名, "*") ~= nil then
       SendMessage(idd,7,"#y/角色名称含有特殊符号")
		return false
	end

	for n = 1, string.len(姓名) do
		if __gge.crc32(string.sub(姓名, n, n)) == -378745019 or __gge.crc32(string.sub(姓名, n, n)) == -1327500718 then
		SendMessage(idd,7,"#y/角色名称含有特殊符号")

			return false
		end
	end

	for n = 1, #敏感词数据 do
		if string.find(姓名, 敏感词数据[n]) ~= nil then
			SendMessage(idd,7,"#y/角色名称存在不允许使用的字符")

			return false
		end
	end
   local 名称数据 = table.loadstring( ReadFile("数据信息/名称数据.txt"))
	for n = 1, #名称数据 do
		if 名称数据[n].名称 == 姓名 then
			SendMessage(idd,7,"#y/该角色名称已被其他玩家使用")
			return false
		end
	end

	return true
end
function Network:删除临时用户(id)
  self.临时客户[id]=nil
end
function Network:连接断开(id)
	local 客户id=连接id转玩家id(id)
	if 客户id~=0 then
     SendMessage(UserData[客户id].连接id,8,消息)
     登录处理类:玩家退出(id)
     __N连接数 = __N连接数-1
     __S服务:断开连接(id)
     return 0
  else
    __N连接数 = __N连接数-1
    SendMessage(id,8,消息)
   __S服务:断开连接(id)

   self.临时客户[id]=0
   return 0
   end
 __N连接数 = __N连接数-1
 __S服务:断开连接(id)
end
return Network