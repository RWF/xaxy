-- @Author: 作者QQ381990860
-- @Date:   2021-11-27 17:21:25
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-12-11 03:21:57
--======================================================================--
-- @作者: QQ381990860

PetControl = class()
require("Script/Pet/仓库")
require("Script/Pet/参战")
require("Script/Pet/属性")
require("Script/Pet/巫医")
require("Script/Pet/改名")
require("Script/Pet/驯养")
function PetControl:初始化(id)
  self.数据={参战=0,仓库={},召唤兽上限=3,观看=0,扩充=0}
  self.玩家id = id
  self.识别码 = nil
end

function PetControl:更新(dt)
end

function PetControl:加载数据(q, w, e)
  self.数据 = table.loadstring(q)
  if self.数据==nil  then
     self.数据={参战=0,仓库={},召唤兽上限=3,观看=0,扩充=0}
     return
  end
  if self.数据.参战 == nil then
    self.数据.参战 = 0
  end
  if self.数据.观看 == nil then
    self.数据.观看 = 0
  end
  if self.数据[self.数据.参战]==nil then
        self.数据.参战=0
  end
  if self.数据[self.数据.观看]==nil then
        self.数据.观看=0
  end
  if self.数据.参战 ~= 0 then
    if self.数据[self.数据.参战].等级 > UserData[self.玩家id].角色.等级 + 10 then
      self.数据.参战 = 0
    elseif UserData[self.玩家id].角色.等级 < self.数据[self.数据.参战].参战等级 then
      self.数据.参战 = 0
    end
  end
   if self.数据.仓库==nil then self.数据.仓库={} end
   if self.数据.扩充==nil then self.数据.扩充=0 end
  if self.数据.召唤兽上限==nil then self.数据.召唤兽上限=3 end
 for i,v in ipairs(self.数据) do
      if  type(v) ~="table" then 
         table.remove(self.数据, i)
      elseif v.造型=="小毛头" or v.造型=="小丫丫"or v.造型=="小神灵" or v.造型=="小仙女"  or v.造型=="小精灵"  or v.造型=="小魔头"  or v.造型=="花仙子" or v.造型=="水晶龙" or v.造型=="贪狼"
       or v.造型=="武神"  or v.造型=="音乐小熊"  or v.造型=="音乐小熊" or v.造型=="哮天犬" or v.造型=="进阶哮天犬"  or v.造型=="帝尊" or v.体质==nil or v.攻资==nil then
          table.remove(self.数据, i)
          SendMessage(UserData[self.玩家id].连接id,7,"#Y/老版的孩子已经删除,请使用新版孩子")


          self.数据.参战=0
          self.数据.观看 = 0
     end
 end

  for n = 1, #self.数据 do
     if self.数据[n].体质 < 0 or self.数据[n].力量 < 0 or self.数据[n].耐力 < 0 or  self.数据[n].魔力 < 0 or self.数据[n].敏捷 < 0 or self.数据[n].潜能 < 0 then
      self.数据[n].体质=1
      self.数据[n].力量 =1
      self.数据[n].耐力 =1
      self.数据[n].魔力 =1
      self.数据[n].敏捷 =1
      self.数据[n].潜能 =0

    end

    self.总属性点 = 0
    self.总属性点 = self.数据[n].体质 + self.数据[n].力量 + self.数据[n].耐力 + self.数据[n].魔力 + self.数据[n].敏捷 + self.数据[n].潜能
    if self.数据[n].清空默认 == nil then
      self.数据[n].默认法术 = nil
      self.数据[n].清空默认 = true
    end

    if self.数据[n].神兽 and self.数据[n].寿命 <= 50 then
      self.数据[n].寿命 = 5000
    end

    if self.总属性点 > 110 + self.数据[n].等级 * 10 then
      -- Nothing
    elseif self.数据[n].禁止使用 then
      self.数据[n].禁止使用 = false
    end

    if self.数据[n].等级 > UserData[self.玩家id].角色.等级 + 10 and (self.数据[n].类型=="宝宝" or self.数据[n].类型=="变异") then
      self:降级处理(n, self.玩家id)
    end
      self:刷新属性(n)
  end
end

function PetControl:飞升处理(id)
  for n = 1, #self.数据 do
    if self.数据[n] ~= nil and self.数据[n].等级 > UserData[id].角色.等级 + 10 then
      self:降级处理(n, id)
    end
  end
end


function PetControl:获取助战宠物(识别码)
  print("self.识别码:"..识别码)
  return self.数据
end

function PetControl:取道具召唤兽()
  local msg={}
  msg.参战=self.数据.参战
   for i=1,#self.数据 do
       msg[i]={
       染色方案=self.数据[i].染色方案,
       染色方案=self.数据[i].染色方案,
       染色组=self.数据[i].染色组,
       当前气血=self.数据[i].当前气血,
       气血上限=self.数据[i].气血上限,
       当前魔法=self.数据[i].当前魔法,
       魔法上限=self.数据[i].魔法上限,
       道具数据=self.数据[i].道具数据,
       饰品=self.数据[i].饰品,
       造型=self.数据[i].造型,
      名称=self.数据[i].名称,
        } 

   end

   SendMessage(UserData[self.玩家id].连接id, 3022,msg)
end
function PetControl:降级处理(编号, id)
  self.临时等级 = self.数据[编号].等级
  self.数据[编号].等级 = UserData[id].角色.等级 + 10
  self.数据[编号].升级经验 = math.floor(取升级经验(self.数据[编号].等级)* 0.5)
  self.数据[编号].当前经验 = 0

  RoleControl:添加系统消息(UserData[id], "#h/您的召唤兽#g/" .. self.数据[编号].名称 .. "#h/的等级由原来的#r/" .. self.临时等级 .. "#h/级下降至#r/" .. self.数据[编号].等级 .. "#h/级。")
  self:重置属性点(编号)
end

function PetControl:提级处理(编号, id)
  self.数据[编号].等级 = UserData[id].角色.等级 + 5
  self.数据[编号].升级经验 = math.floor(取升级经验(self.数据[编号].等级 )* 0.5)
  self.数据[编号].当前经验 = 0

  self:重置属性点(编号)
end

function PetControl:提级处理1(编号, id, 等级)
  self.数据[编号].等级 = 等级
  self.数据[编号].升级经验 = math.floor(取升级经验(self.数据[编号].等级) * 0.5)
  self.数据[编号].当前经验 = 0

  self:重置属性点(编号)
end

function PetControl:重置属性点(编号)
  if self.数据[编号].类型 == "变异" then
  self.数据[编号].力量 = self.数据[编号].等级 + 30
  self.数据[编号].魔力 = self.数据[编号].等级 + 30
  self.数据[编号].体质 = self.数据[编号].等级 + 30
  self.数据[编号].敏捷 = self.数据[编号].等级 + 30
  self.数据[编号].耐力 = self.数据[编号].等级 + 30
  elseif self.数据[编号].类型 == "神兽" then
  self.数据[编号].力量 = self.数据[编号].等级 + 40
  self.数据[编号].魔力 = self.数据[编号].等级 + 40
  self.数据[编号].体质 = self.数据[编号].等级 + 40
  self.数据[编号].敏捷 = self.数据[编号].等级 + 40
  self.数据[编号].耐力 = self.数据[编号].等级 + 40
 else
  self.数据[编号].力量 = self.数据[编号].等级 + 20
  self.数据[编号].魔力 = self.数据[编号].等级 + 20
  self.数据[编号].体质 = self.数据[编号].等级 + 20
  self.数据[编号].敏捷 = self.数据[编号].等级 + 20
  self.数据[编号].耐力 = self.数据[编号].等级 + 20
  end
  self.数据[编号].潜能 = self.数据[编号].等级 * 5+self.数据[编号].最高灵性*2
  self:刷新属性(编号, 恢复)
end

function PetControl:获取指定数据(编号)
  return table.tostring(self.数据[编号])
end

function PetControl:获取指定数据1(编号)
  return self.数据[编号]
end

function PetControl:获取数据()
  return self.数据
end

function PetControl:放生召唤兽(id, 编号)
  if self.数据[编号] == nil then
    SendMessage(UserData[id].连接id, 7, "#y/你没有这只召唤兽")
    return 0
  elseif UserData[id].战斗 ~= 0 then
    SendMessage(UserData[id].连接id, 7, "#y/战斗中无法进行此操作")
    return 0
  elseif self.数据[编号].加锁  then
      SendMessage(UserData[id].连接id, 7, "#y/加锁的召唤兽无法放生")
    return 0
  else
    self.数据.参战 = 0
    self.数据.观看 = 0
    RoleControl:添加消费日志(UserData[id],string.format("放生召唤兽:编号=%s,类型=%s,名称=%s,等级=%s,技能数=%s", 编号, self.数据[编号].造型, self.数据[编号].名称, self.数据[编号].等级, #self.数据[编号].技能))
    table.remove(self.数据, 编号)
    SendMessage(UserData[id].连接id, 2006, self:获取数据())
    SendMessage(UserData[id].连接id, 2005, self:取头像数据())
    SendMessage(UserData[id].连接id, 2010, RoleControl:获取地图数据(UserData[id]))
   MapControl:MapSend(id, 1016, RoleControl:获取地图数据(UserData[id]))
    SendMessage(UserData[id].连接id, 7, "#y/你的这只召唤兽消失的无影无踪")
  end
end
function PetControl:加点召唤兽(id,序号,数据)
  数据= 分割文本(数据,"*-*")
 if self.数据[序号]==nil then
   SendMessage(UserData[id].连接id,7,"#y/你没有这只召唤兽")
  elseif 数据[1]+0 < 0 then
    封禁账号(UserData[id],"CE修改")
    return
  elseif 数据[2]+0 < 0 then
        封禁账号(UserData[id],"CE修改")
    return
  elseif 数据[3]+0 < 0 then
        封禁账号(UserData[id],"CE修改")
    return
  elseif 数据[4]+0 < 0 then
      封禁账号(UserData[id],"CE修改")
    return
  elseif 数据[5]+0 < 0 then
        封禁账号(UserData[id],"CE修改")
        return
  elseif self.数据[序号].潜能<数据[1]+数据[2]+数据[3]+数据[4]+数据[5] then
   SendMessage(UserData[id].连接id,7,"#y/你的这只召唤兽没有那么多的可分配属性点")
  else

 self.数据[序号].力量= self.数据[序号].力量+数据[1]
 self.数据[序号].体质= self.数据[序号].体质+数据[2]
 self.数据[序号].魔力= self.数据[序号].魔力+数据[3]
 self.数据[序号].耐力= self.数据[序号].耐力+数据[4]
 self.数据[序号].敏捷= self.数据[序号].敏捷+数据[5]
 self.数据[序号].潜能 =  self.数据[序号].潜能 - (数据[1]+数据[2]+数据[3]+数据[4]+数据[5])
   SendMessage(UserData[id].连接id,7,"#y/召唤兽属性点分配成功")
   self:刷新属性(序号)
    SendMessage(UserData[id].连接id, 2005, self:取头像数据())
    SendMessage(UserData[id].连接id, 2006, self:获取数据(编号))
   end
 end

function PetControl:定制召唤兽(data)

   local bbid=self:创建召唤兽(data[2],data[3],nil,data[1]+0)
 
  local a={"特性","灵性","攻资","防资","体资","法资","速资","躲资","成长"}
  for i=1,#a do
    if data[i+3]~="" then
       if i==1 then
          self.数据[bbid].特性几率 =5
          self.数据[bbid][a[i]]=data[i+3]
        elseif i ==2 then
          self.数据[bbid].最高灵性= tonumber(data[i+3])
          self.数据[bbid][a[i]]= tonumber(data[i+3])
           self.数据[bbid].潜能 = self.数据[bbid].等级 * 5+self.数据[bbid].最高灵性*2
        else
            self.数据[bbid][a[i]]= tonumber(data[i+3])
       end

    end
  end
  if self.数据[bbid].灵性>=110 then
      self.数据[bbid].进阶属性.力量 =math.random(20,100)
      self.数据[bbid].进阶属性.魔力 =math.random(20,100)
      self.数据[bbid].进阶属性.体质 =math.random(20,100)
      self.数据[bbid].进阶属性.敏捷 =math.random(20,100)
      self.数据[bbid].进阶属性.耐力 =math.random(20,100)
  end
  if data[13]~="" then
      local 内丹数据 = string.split(data[13],"@")
      for i=1,6 do
         if 内丹数据[i] then
              self.数据[bbid].内丹[i]= {等级=1,技能=内丹数据[i]}
              self.数据[bbid].内丹.可用数量 =self.数据[bbid].内丹.可用数量-1
              if self.数据[bbid].内丹.可用数量<0 then
                  self.数据[bbid].内丹.可用数量=0
              end
         end
      end
  end
   if data[14]~="" then
    local  技能组 = string.split(data[14],"@")
     self.数据[bbid].技能={}
      for m=1,#技能组 do
          self.数据[bbid].技能[m] =置技能(技能组[m])
      end
  end
   
end

function PetControl:创建召唤兽(造型,类型,资质,等级,技能组,染色方案,染色组)

	self.数据[#self.数据+1]={} 
	self.数据[#self.数据]={名称=造型,造型=造型,类型 = 类型,炼兽真经=0,忠诚 = 100,灵性 = 0,特性 = "无",特性几率=0,进阶属性 = {力量=0,敏捷=0,耐力=0,魔力=0,体质=0},最高灵性 = 0,最高灵性显示 =false,五行=生成五行(),当前经验=0,升级经验=15,装备={},元宵=0,附加追加技能 = {},追加技能 ={},饰品=false,特殊资质={攻资=0,防资=0,体资=0,法资=0,速资=0,躲资=0},道具数据={},加锁=false}
	if 等级 == nil then
		self.数据[#self.数据].等级 = 0
	else
		self.数据[#self.数据].等级 = 等级
	end
	if 类型 ~= "野怪" then
		self.数据[#self.数据].潜能=5*self.数据[#self.数据].等级
	else
		self.数据[#self.数据].潜能=0
	end
	if 类型 == "孩子" or 类型 == "神兽" or 类型 == "变异神兽"  then
		self.数据[#self.数据].寿命 = 10000
		self.数据[#self.数据].内丹 = {总数=6,可用数量=6,}
		self.数据[#self.数据].参战等级 = 0
		self.数据[#self.数据].神兽升级=0
	else
		self.数据[#self.数据].寿命=100*math.random(20,100)
		self.数据[#self.数据].参战等级 = BBData[造型].等级 or 0
		self.数据[#self.数据].内丹 = {
		总数=math.floor(self.数据[#self.数据].参战等级 / 35)+1,
		可用数量 = math.floor(self.数据[#self.数据].参战等级 / 35)+1
      }
	end
  local 能力 = 1
  local 五维总值 = 0
  local 属性 = 0
    if 类型 == "野怪" then
      属性 = 10 + self.数据[#self.数据].等级 * 5*2 + math.random(-10,20)
    elseif 类型 == "宝宝" then
      属性 = 20 + self.数据[#self.数据].等级 * 5
    elseif 类型 == "变异" then
      属性 = 30+ self.数据[#self.数据].等级 * 5
    elseif 类型 == "神兽" or "变异神兽" then
      属性 = 40 + self.数据[#self.数据].等级 * 5
    elseif 类型 == "孩子" then
      属性 = 20 + self.数据[#self.数据].等级 * 5
    end
    五维总值 = 属性
     self.数据[#self.数据].技能={}
 if 类型 == "野怪"  then
   能力 = 1
    local s1,s2,s3,s4,s5,s6
   
    local sd = 1
    while true do
      s1 = math.random(math.floor(五维总值/10),math.floor(五维总值/3))
      s2 = math.random(math.floor(五维总值/10),math.floor(五维总值/3))
      s3 = math.random(math.floor(五维总值/10),math.floor(五维总值/3))
      s4 = math.random(math.floor(五维总值/10),math.floor(五维总值/3))
      s5 = math.random(math.floor(五维总值/10),math.floor(五维总值/3))
      sd =sd+1
      if(s1+s2+s3+s4+s5==五维总值) or  sd > 200 then
        self.数据[#self.数据].体质 = s1
        self.数据[#self.数据].魔力 = s2
        self.数据[#self.数据].力量 = s3
        self.数据[#self.数据].耐力 = s4
        self.数据[#self.数据].敏捷 = s5
        break
      end
    end
  self.数据[#self.数据].忠诚 = 80
 elseif 类型 == "宝宝" then
   能力 = 0.95
   self.数据[#self.数据].忠诚 = 80
    self.数据[#self.数据].忠诚 = 100
    self.数据[#self.数据].体质 = 20+ self.数据[#self.数据].等级
    self.数据[#self.数据].魔力 = 20+ self.数据[#self.数据].等级
    self.数据[#self.数据].力量 = 20+ self.数据[#self.数据].等级
    self.数据[#self.数据].耐力 = 20+ self.数据[#self.数据].等级
    self.数据[#self.数据].敏捷 = 20+ self.数据[#self.数据].等级
 elseif 类型 == "孩子" then
    能力 = 1.05
    五维总值 = 属性
    self.数据[#self.数据].忠诚 = 100
    self.数据[#self.数据].体质 = 20+ self.数据[#self.数据].等级
    self.数据[#self.数据].魔力 = 20+ self.数据[#self.数据].等级
    self.数据[#self.数据].力量 = 20+ self.数据[#self.数据].等级
    self.数据[#self.数据].耐力 = 20+ self.数据[#self.数据].等级
    self.数据[#self.数据].敏捷 = 20+ self.数据[#self.数据].等级
 elseif 类型 == "变异" then
    能力 = 1.05
    五维总值 = 属性
    self.数据[#self.数据].忠诚 = 100
    self.数据[#self.数据].体质 = 30+ self.数据[#self.数据].等级
    self.数据[#self.数据].魔力 = 30+ self.数据[#self.数据].等级
    self.数据[#self.数据].力量 = 30+ self.数据[#self.数据].等级
    self.数据[#self.数据].耐力 = 30+ self.数据[#self.数据].等级
    self.数据[#self.数据].敏捷 = 30+ self.数据[#self.数据].等级
  elseif 类型 == "神兽" then
   self.数据[#self.数据].忠诚 = 100
    self.数据[#self.数据].体质 = 40+ self.数据[#self.数据].等级
    self.数据[#self.数据].魔力 = 40+ self.数据[#self.数据].等级
    self.数据[#self.数据].力量 = 40+ self.数据[#self.数据].等级
    self.数据[#self.数据].耐力 = 40+ self.数据[#self.数据].等级
    self.数据[#self.数据].敏捷 = 40+ self.数据[#self.数据].等级
  elseif 类型 == "变异神兽" then
    self.数据[#self.数据].忠诚 = 100
    self.数据[#self.数据].体质 = 40+ self.数据[#self.数据].等级
    self.数据[#self.数据].魔力 = 40+ self.数据[#self.数据].等级
    self.数据[#self.数据].力量 = 40+ self.数据[#self.数据].等级
    self.数据[#self.数据].耐力 = 40+ self.数据[#self.数据].等级
    self.数据[#self.数据].敏捷 = 40+ self.数据[#self.数据].等级
  end
 if 资质 == nil then
     if 类型 == "变异神兽" or 类型 == "神兽" then
      self.数据[#self.数据].攻资= 1500
      self.数据[#self.数据].防资= 1500
      self.数据[#self.数据].体资= 5500
      self.数据[#self.数据].法资= 3000
      self.数据[#self.数据].速资= 1500
      self.数据[#self.数据].躲资= 1500
      self.数据[#self.数据].成长 = 1.35

     else 
        for k,v in pairs(全局变量.资质标识) do
          self.数据[#self.数据][v] = math.floor(math.random(BBData[造型][v]*0.75,BBData[造型][v]*能力))
          
        end
      end
   local cz1 = math.random(100)
  if cz1 < 50 then
    self.数据[#self.数据].成长 = BBData[造型].成长[1]-math.random(20,40)/1000
  elseif cz1 <= 60 then
    self.数据[#self.数据].成长 = BBData[造型].成长[1]-math.random(20)/1000
  elseif cz1 > 60  and cz1 <= 70 then
    self.数据[#self.数据].成长 = BBData[造型].成长[2]-math.random(20)/1000
  elseif cz1 > 70  and cz1 <= 85 then
    self.数据[#self.数据].成长 = BBData[造型].成长[3]-math.random(20)/1000
  elseif cz1 > 85  and cz1 <= 95 then
    self.数据[#self.数据].成长 = BBData[造型].成长[4]-math.random(20)/1000
  elseif cz1 > 95  and cz1 <= 100 then
    self.数据[#self.数据].成长 = BBData[造型].成长[5]-math.random(20)/1000
  end
  if self.数据[#self.数据].成长 == nil  or self.数据[#self.数据].成长 ==0 then
    self.数据[#self.数据].成长 = BBData[造型].成长[1]
  end
 else
  self.数据[#self.数据].攻资= 资质.攻资
  self.数据[#self.数据].防资= 资质.防资
  self.数据[#self.数据].体资= 资质.体资
  self.数据[#self.数据].法资= 资质.法资
  self.数据[#self.数据].速资= 资质.速资
  self.数据[#self.数据].躲资= 资质.躲资
  self.数据[#self.数据].成长 = 资质.成长
 end
  local jn =  BBData[造型].技能
  local jn0 ={}



if  技能组 then
  jn0=技能组
else
  if math.random(100) <= 80 then
    for q=1,#jn do
        if  math.random(100)<=math.max(70+(5-q*5),3)  then
            table.insert(jn0,jn[math.random(1,#jn)])
        end
    end
  end

  if BBData[造型].天生 then
    for q=1,#BBData[造型].天生 do
          table.insert(jn0,BBData[造型].天生[q])
    end
  end
end


  jn0 = 删除重复(jn0)

  for m=1,#jn0 do
    local w = jn0[m]
    self.数据[#self.数据].技能[m] =置技能(w)
  end
   --if 染色方案 ~=nil then
    self.数据[#self.数据].染色方案 =染色方案
  -- end
     --  if 染色组 ~=nil then
    self.数据[#self.数据].染色组 = 染色组

    self:刷新属性(#self.数据)
    return #self.数据
  end



function RoleControl:赠送召唤兽(id, 类型, 仙玉)
  if #UserData[id].召唤兽.数据 >= UserData[id].召唤兽.数据.召唤兽上限 then
    SendMessage(UserData[id].连接id, 7, "#y/你当前可携带的召唤兽数量已达上限")
  elseif self:扣除仙玉(UserData[id],仙玉,"赠送召唤兽") then
    SendMessage(UserData[id].连接id, 7, "#y/你得到神兽" .. 类型)
    UserData[id].召唤兽:创建召唤兽(类型, true, false, 3)
  end
end

function PetControl:取装备状态(编号)
  for n = 1, 3 do
    if self.数据[编号].装备[n] ~= nil then
      return true
    end
  end

  return false
end



function PetControl:保留成长(成长)
  self.成长数量 = string.len(tostring(成长))

  if self.成长数量 > 5 then
    self.临时成长 = ""

    for n = 1, 5 do
      self.临时成长 = self.临时成长 .. string.sub(成长, n, n)
    end

    return tonumber(self.临时成长)
  else
    return 成长
  end
end


function PetControl:取全体数值差(类型)
  if 类型 == 1 then
    self.返回数据 = {
      魔法 = 0,
      气血 = 0
    }

    for n = 1, #self.数据 do
      self.返回数据.气血 = self.返回数据.气血 + self.数据[n].气血上限 - self.数据[n].当前气血
      self.返回数据.魔法 = self.返回数据.魔法 + self.数据[n].魔法上限 - self.数据[n].当前魔法
    end

    return self.返回数据
  elseif 类型 == 2 then
    self.返回数据 = 0

    for n = 1, #self.数据 do
      self.返回数据 = self.返回数据 + 100 - self.数据[n].忠诚
    end

    return self.返回数据
  end
end



function PetControl:升级处理(玩家id, id)
  self.数据[id].当前经验 = self.数据[id].当前经验 - self.数据[id].升级经验
  self.数据[id].等级 = self.数据[id].等级 + 1
  self.数据[id].潜能 = self.数据[id].潜能 + 5
  self.数据[id].忠诚 = 100

  for i = 1, #全局变量.基础属性 do
    self.数据[id][全局变量.基础属性[i]] = self.数据[id][全局变量.基础属性[i]] + 1
  end

  self.数据[id].升级经验 = math.floor(取升级经验(self.数据[id].等级) * 0.5)

  self:刷新属性(id)
  self:恢复状态(3, id, 玩家id)
  SendMessage(UserData[玩家id].连接id, 9, "#xt/#y/ " .. self.数据[id].名称 .. "等级提升至#r/" .. self.数据[id].等级 .. "#y/级")
end

function PetControl:添加经验(数额, 玩家id, id, 类型)
  if 类型 ~= 10 then
    self.临时数额 = 数额 * ServerConfig.经验获得率
  else
    self.临时数额 = 数额
  end

  if self.数据[id].等级 >= UserData[玩家id].角色.等级 + 5 then
    SendMessage(UserData[玩家id].连接id, 9, "#xt/#w/你的召唤兽等级已经高出人物5级，无法再获得经验")
  else
    self.数据[id].当前经验 = self.数据[id].当前经验 + self.临时数额

    SendMessage(UserData[玩家id].连接id, 9, "#xt/#w/ " .. self.数据[id].名称 .. "获得" .. self.临时数额 .. "点经验")

    if self.数据[id].升级经验 <= self.数据[id].当前经验 then
      self:升级处理(玩家id, id)
    end
  end

  SendMessage(UserData[玩家id].连接id, 2005, UserData[玩家id].召唤兽:取头像数据())
end

function PetControl:存档(id)
	WriteFile("玩家信息/账号" .. UserData[id].账号 ..[[/]]..id.. "/召唤兽.txt", table.tostring(self.数据))
end

function PetControl:取头像数据()
  if self.数据.参战 == 0 then
    return 0
  elseif self.数据[self.数据.参战] == nil then
    return 0
  end
  local 发送信息= {
    造型 = self.数据[self.数据.参战].造型,
    当前气血 = self.数据[self.数据.参战].当前气血,
    气血上限 = self.数据[self.数据.参战].气血上限,
    当前魔法 = self.数据[self.数据.参战].当前魔法,
    魔法上限 = self.数据[self.数据.参战].魔法上限,
    当前经验 = self.数据[self.数据.参战].当前经验,
    升级经验 = self.数据[self.数据.参战].升级经验
  }

  return 发送信息
end
function PetControl:召唤兽改变造型(id,编号,造型,进阶)
  if self.数据[编号] then
        if ModelData[造型] then 
          local money = (进阶=="true") and BBData[string.sub(造型,5,string.len(造型) )].换造型  or BBData[造型].换造型

           if RoleControl:扣除仙玉(UserData[id],money,"召唤兽改变造型") then 
             self.数据[编号].饰品=false 
             self.数据[编号].染色方案 =nil
             self.数据[编号].染色组=nil 
             self.数据[编号].进阶 =true 
             self.数据[编号].造型 = 造型
             SendMessage(UserData[id].连接id, 7,  "#Y/恭喜你的召唤兽成功转换为"..造型.."造型")
           else  
               SendMessage(UserData[id].连接id, 7,  "#Y/你当前的仙玉不足"..money.."无法进行转换")
            end 
         else 
          SendMessage(UserData[id].连接id, 7,  "#Y/这个造型无法转换！")
        end

  else  
      SendMessage(UserData[id].连接id, 7,  "#Y/召唤兽不存在请从新刷新数据！")
  end
 
  SendMessage(UserData[id].连接id,2006,self:获取数据())
 end

function PetControl:观看召唤兽(id,数据)
  if self.数据.观看 == 数据 then
    self.数据.观看= 0
  else
    self.数据.观看 = 数据
  end
  SendMessage(UserData[id].连接id, 2010, RoleControl:获取地图数据(UserData[id]))


 MapControl:MapSend(id, 1016, RoleControl:获取地图数据(UserData[id]))
  SendMessage(UserData[id].连接id,2006,self:获取数据())
 end
function PetControl:染色处理(id,染色id,染色1,染色2,编号)
           -- if RoleControl:扣除仙玉(UserData[id],5000,"召唤兽染色") then
            if UserData[id].角色.道具.货币.银子>=100000000 then
             RoleControl:扣除银子(UserData[id],100000000,"召唤兽染色")
                self.数据[编号].染色方案 =染色id
                self.数据[编号].染色组={[1]=染色1,[2]=染色2}
             SendMessage(UserData[id].连接id,7,"#y/恭喜你的#R/"..self.数据[编号].名称.."#Y/染色成功")
                SendMessage(UserData[id].连接id, 2006, self:获取数据())
            else
                SendMessage(UserData[id].连接id,7,"#y/你没有足够的银子！")
            end
end

return PetControl
