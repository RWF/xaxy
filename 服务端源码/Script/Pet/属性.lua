--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-02-04 01:59:09
--======================================================================--
function PetControl:刷新装备属性(编号, id)
	self.数据[编号].追加技能 = {}
	self.数据[编号].附加追加技能 = {}
	self.临时追加 = {}
	self.临时附加 = {}
    self.临时变身 = {}
	for n = 1, #全局变量.基础属性 do
		self.数据[编号][全局变量.基础属性[n]] = self.数据[编号][全局变量.基础属性[n]] - self.数据[编号].装备属性[全局变量.基础属性[n]]
	end
	self.数据[编号].装备属性 = {速度 = 0,体质 = 0,魔力 = 0,气血 = 0,敏捷 = 0,耐力 = 0,魔法 = 0,灵力 = 0,伤害 = 0,力量 = 0,防御 = 0,躲闪 = 0}
	self.数据[编号].道具数据 = {}
	for n = 1, 3 do
		if self.数据[编号].装备[n] ~= nil then
			self.数据[编号].道具数据[n] = UserData[id].物品[self.数据[编号].装备[n]]
			if self.数据[编号].道具数据[n] and self.数据[编号].道具数据[n].套装效果 then
				self.套装名称 = self.数据[编号].道具数据[n].套装效果.名称

				if self.数据[编号].道具数据[n].套装效果.类型 == "追加法术" then
					if self.临时追加[self.套装名称] == nil then
						self.临时追加[self.套装名称] = 1
					else
						self.临时追加[self.套装名称] = self.临时追加[self.套装名称] + 1
					end
                elseif self.数据[编号].道具数据[n].套装效果.类型 == "变身术" then
                    if self.临时变身[self.套装名称] == nil then
                        self.临时变身[self.套装名称] = 1
                    else
                        self.临时变身[self.套装名称] = self.临时变身[self.套装名称] + 1
                    end
				else
                    if self.临时附加[self.套装名称] == nil then
    					self.临时附加[self.套装名称] = 1
    				else
    					self.临时附加[self.套装名称] = self.临时附加[self.套装名称] + 1
    				end
                end
			end

			self.数据[编号].装备属性[UserData[id].物品[self.数据[编号].装备[n]].主属性.名称] = self.数据[编号].装备属性[UserData[id].物品[self.数据[编号].装备[n]].主属性.名称] + UserData[id].物品[self.数据[编号].装备[n]].主属性.数值
  			for i = 1, #UserData[id].物品[self.数据[编号].装备[n]].锻造数据 do
				self.数据[编号].装备属性[UserData[id].物品[self.数据[编号].装备[n]].锻造数据[i].类型] = self.数据[编号].装备属性[UserData[id].物品[self.数据[编号].装备[n]].锻造数据[i].类型] + UserData[id].物品[self.数据[编号].装备[n]].锻造数据[i].数值
			end
			if UserData[id].物品[self.数据[编号].装备[n]].附加属性 ~= nil then
				self.数据[编号].装备属性[UserData[id].物品[self.数据[编号].装备[n]].附加属性.名称] = self.数据[编号].装备属性[UserData[id].物品[self.数据[编号].装备[n]].附加属性.名称] + UserData[id].物品[self.数据[编号].装备[n]].附加属性.数值
			end

			if UserData[id].物品[self.数据[编号].装备[n]].单加 ~= nil then
				self.数据[编号].装备属性[UserData[id].物品[self.数据[编号].装备[n]].单加.名称] = self.数据[编号].装备属性[UserData[id].物品[self.数据[编号].装备[n]].单加.名称] + UserData[id].物品[self.数据[编号].装备[n]].单加.数值
			end

			if UserData[id].物品[self.数据[编号].装备[n]].双加 ~= nil then
				self.数据[编号].装备属性[UserData[id].物品[self.数据[编号].装备[n]].双加.名称] = self.数据[编号].装备属性[UserData[id].物品[self.数据[编号].装备[n]].双加.名称] + UserData[id].物品[self.数据[编号].装备[n]].双加.数值
			end
		else
			self.数据[编号].道具数据[n] = nil
		end
	end

	for n, v in pairs(self.临时追加) do
		if self.临时追加[n] >= 3 then
			self.数据[编号].追加技能[#self.数据[编号].追加技能+1] = n

		end
	end
    for n, v in pairs(self.临时变身) do
        if self.临时变身[n] >= 3 then
            self.数据[编号].变身技能[#self.数据[编号].变身技能+1] = n

        end
    end

	for n, v in pairs(self.临时附加) do
		if self.临时附加[n] >= 3 then
			self.数据[编号].附加追加技能[#self.数据[编号].附加追加技能+1] = {名称=n}

		end
	end

	for n = 1, #全局变量.基础属性 do
		self.数据[编号][全局变量.基础属性[n]] = self.数据[编号][全局变量.基础属性[n]] + self.数据[编号].装备属性[全局变量.基础属性[n]]
	end

	self:刷新属性(编号, 恢复)
end

function PetControl:刷新属性(编号,恢复)
  if self.数据[编号].装备属性==nil then
   self.数据[编号].装备属性={速度=0,伤害=0,防御=0,气血=0,魔法=0,灵力=0,体质=0,力量=0,魔力=0,耐力=0,敏捷=0,躲闪=0}
   end
 if self.数据[编号].装备属性.躲闪==nil then
    self.数据[编号].装备属性.躲闪=0
 end
 if self.数据[编号].饰品 then
  self.数据[编号].特殊资质={
  攻资=math.floor(self.数据[编号].攻资*0.1),
  防资=math.floor(self.数据[编号].防资*0.1),
  体资=math.floor(self.数据[编号].体资*0.1),
  法资=math.floor(self.数据[编号].法资*0.1),
  速资=math.floor(self.数据[编号].速资*0.1),
  躲资=math.floor(self.数据[编号].躲资*0.1)}
 end

 self.临时等级=self.数据[编号].等级
 self.临时成长=self.数据[编号].成长


 self.数据[编号].伤害=math.floor(self.临时等级*(self.数据[编号].攻资+self.数据[编号].特殊资质.攻资)*(14+10*self.临时成长)/7500+self.临时成长*(self.数据[编号].力量+self.数据[编号].进阶属性.力量)+20)+self.数据[编号].装备属性.伤害
 self.数据[编号].灵力=math.floor(self.临时等级*(self.数据[编号].法资+self.数据[编号].特殊资质.法资+1640)*(self.临时成长+1)/7500+0.7*(self.数据[编号].魔力+self.数据[编号].进阶属性.魔力)+20+(self.数据[编号].力量+self.数据[编号].进阶属性.力量)*0.4+(self.数据[编号].耐力+self.数据[编号].进阶属性.耐力)*0.2+(self.数据[编号].体质+self.数据[编号].进阶属性.体质)*0.3)+self.数据[编号].装备属性.灵力
 self.数据[编号].防御=math.floor(self.临时等级*(self.数据[编号].防资+self.数据[编号].特殊资质.防资)/433+self.临时成长*(self.数据[编号].耐力+self.数据[编号].进阶属性.耐力)*4/3)+self.数据[编号].装备属性.防御
 self.数据[编号].速度=math.floor((self.数据[编号].敏捷+self.数据[编号].进阶属性.敏捷)*self.数据[编号].速资/1000)+self.数据[编号].装备属性.速度
 self.数据[编号].躲闪=math.floor((self.数据[编号].敏捷+self.数据[编号].进阶属性.敏捷)*self.数据[编号].躲资/1000)+self.数据[编号].装备属性.躲闪
 self.数据[编号].气血上限=math.floor(self.临时等级*(self.数据[编号].体资+self.数据[编号].特殊资质.体资)/1000+self.临时成长*(self.数据[编号].体质+self.数据[编号].进阶属性.体质)*10)+10+self.数据[编号].装备属性.气血
 self.数据[编号].最大气血=math.floor(self.临时等级*(self.数据[编号].体资+self.数据[编号].特殊资质.体资)/1000+self.临时成长*(self.数据[编号].体质+self.数据[编号].进阶属性.体质)*10)+10+self.数据[编号].装备属性.气血
 self.数据[编号].魔法上限=math.floor(self.临时等级*(self.数据[编号].法资+self.数据[编号].特殊资质.法资)/2000+self.临时成长*(self.数据[编号].魔力+self.数据[编号].进阶属性.魔力)*2.5)+self.数据[编号].装备属性.魔法

  for i=1,#self.数据[编号].内丹 do
      if self.数据[编号].内丹[i].技能=="迅敏" then
        self.数据[编号].伤害=(self.数据[编号].内丹[i].等级*8)+self.数据[编号].伤害
        self.数据[编号].速度=(self.数据[编号].内丹[i].等级*5)+self.数据[编号].速度
      end
      if self.数据[编号].内丹[i].技能=="静岳" then
        self.数据[编号].灵力=(self.数据[编号].内丹[i].等级*4)+self.数据[编号].灵力
        self.数据[编号].气血上限=(self.数据[编号].内丹[i].等级*40)+self.数据[编号].气血上限
      end
      if self.数据[编号].内丹[i].技能=="矫健" then
        self.数据[编号].气血上限=(self.数据[编号].内丹[i].等级*30)+self.数据[编号].气血上限
        self.数据[编号].速度=(self.数据[编号].内丹[i].等级*3)+self.数据[编号].速度
      end
      if self.数据[编号].内丹[i].技能=="玄武躯" then
        self.数据[编号].气血上限=self.数据[编号].内丹[i].等级*100+200+self.数据[编号].气血上限
      end
      if self.数据[编号].内丹[i].技能=="龙胄铠" then
        self.数据[编号].防御=self.数据[编号].内丹[i].等级*12+self.数据[编号].防御
      end
  end
  for i=1,#self.数据[编号].技能 do
      if self.数据[编号].技能[i].名称=="灵山禅语" then
          self.数据[编号].魔法上限=self.数据[编号].魔法上限+math.random(300,500)
      end
      if self.数据[编号].技能[i].名称=="净台妙谛" then
          self.数据[编号].气血上限=self.数据[编号].气血上限+self.数据[编号].体质*self.数据[编号].成长*2
      end
      if self.数据[编号].技能[i].名称=="溜之大吉" then
          self.数据[编号].速度=math.floor(self.数据[编号].速度*1.2)
      end

        if self.数据[编号].技能[i].名称=="欣欣向荣" and  self.数据[编号].五行 =="木" then
          self.数据[编号].气血上限=math.floor(self.数据[编号].气血上限*1.3) 
      end

      if self.数据[编号].技能[i].名称=="万物之灵（火）" and  self.数据[编号].五行 =="火" then
          self.数据[编号].灵力=math.floor(self.数据[编号].灵力*1.05) 
      end
         if self.数据[编号].技能[i].名称=="万物之灵（水）" and  self.数据[编号].五行 =="水" then
          self.数据[编号].灵力=math.floor(self.数据[编号].灵力*1.05) 
      end
            if self.数据[编号].技能[i].名称=="万物之灵（金）" and  self.数据[编号].五行 =="金" then
          self.数据[编号].灵力=math.floor(self.数据[编号].灵力*1.05) 
      end
            if self.数据[编号].技能[i].名称=="万物之灵（土）" and  self.数据[编号].五行 =="土" then
          self.数据[编号].灵力=math.floor(self.数据[编号].灵力*1.05) 
      end
        if self.数据[编号].技能[i].名称=="万物之灵（木）" and  self.数据[编号].五行 =="木" then
          self.数据[编号].灵力=math.floor(self.数据[编号].灵力*1.05) 
      end  

      if self.数据[编号].技能[i].名称=="力大无穷（火）" and  self.数据[编号].五行 =="火" then
          self.数据[编号].伤害=math.floor(self.数据[编号].伤害*1.05) 
      end
         if self.数据[编号].技能[i].名称=="力大无穷（水）" and  self.数据[编号].五行 =="水" then
          self.数据[编号].伤害=math.floor(self.数据[编号].伤害*1.05) 
      end
            if self.数据[编号].技能[i].名称=="力大无穷（金）" and  self.数据[编号].五行 =="金" then
          self.数据[编号].伤害=math.floor(self.数据[编号].伤害*1.05) 
      end
            if self.数据[编号].技能[i].名称=="力大无穷（土）" and  self.数据[编号].五行 =="土" then
          self.数据[编号].伤害=math.floor(self.数据[编号].伤害*1.05) 
      end
        if self.数据[编号].技能[i].名称=="力大无穷（木）" and  self.数据[编号].五行 =="木" then
          self.数据[编号].伤害=math.floor(self.数据[编号].伤害*1.05) 
      end    
      
     
  end







  if 恢复==nil then
   self.数据[编号].当前气血=self.数据[编号].气血上限
   self.数据[编号].当前魔法=self.数据[编号].魔法上限
 end

 end
