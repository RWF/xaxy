--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-11-01 14:23:09
--======================================================================--
function PetControl:存取召唤兽(id,编号,临时)
  if 临时 =="存入" then
    if #self.数据.仓库 > 7 then
    	  SendMessage(UserData[id].连接id,7,"#Y/你的仓库已经满了,无法在存取召唤兽!")
    	  return
    end
     self.数据.参战 = 0
     self.数据.观看 = 0
     RoleControl:添加消费日志(UserData[id],string.format("存放仓库:编号=%s,类型=%s,名称=%s,等级=%s,技能数=%s",编号, self.数据[编号].造型, self.数据[编号].名称, self.数据[编号].等级, #self.数据[编号].技能))
    table.insert(self.数据.仓库,self.数据[编号])
    table.remove(self.数据,编号)
    SendMessage(UserData[id].连接id,7,"#Y/你的这只召唤兽已经存入仓库")
    elseif 临时 =="取出" then
      if #self.数据 >= self.数据.召唤兽上限 then
            SendMessage(UserData[id].连接id,7,"#Y/你当前携带的宝宝已经超过上限无法取出!")
        return
      end
    table.insert(self.数据,self.数据.仓库[编号])
    table.remove(self.数据.仓库,编号)
    SendMessage(UserData[id].连接id,7,"#Y/你的召唤兽已经取出")
    end
    SendMessage(UserData[id].连接id,2071,self:获取数据())
    SendMessage(UserData[id].连接id,3040,"1")
    SendMessage(UserData[id].连接id, 2005, self:取头像数据())
      MapControl:MapSend(id, 1016, RoleControl:获取地图数据(UserData[id]))
 end
