-- @Author: 作者QQ414628710
-- @Date:   2021-11-27 17:21:26
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-12-12 22:32:14
--======================================================================--
function PetControl:参战召唤兽(id, 编号)
	if self.数据[编号] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/你没有这只召唤兽")
	elseif UserData[id].战斗 ~= 0 then
		SendMessage(UserData[id].连接id, 7, "#y/战斗中无法进行此操作")
		return 0
	elseif self.数据[编号].参战等级 > UserData[id].角色.等级 and Config.bb参战限制=="有" then
		SendMessage(UserData[id].连接id, 7, "#y/你无法驾驭这只召唤兽")
	elseif self.数据[编号].等级 >= UserData[id].角色.等级 + 11 then
		SendMessage(UserData[id].连接id, 7, "#y/你无法驾驭等级大于你10级的召唤兽")
	else
		if self.数据.参战 == 编号 then
			self.数据.参战 = 0

			SendMessage(UserData[id].连接id, 2008, 编号)
		else
			self.数据.参战 = 编号

			SendMessage(UserData[id].连接id, 2007, 编号)
		end

		SendMessage(UserData[id].连接id, 2005, self:取头像数据())
	end
end