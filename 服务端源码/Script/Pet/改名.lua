--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-10-05 21:14:41
--======================================================================--
function PetControl:改名召唤兽(id, 序号, 名称)
	self.临时数据 = {
		序号 = 序号,
		名称 = 名称
	}

	if self.数据[self.临时数据.序号] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/你没有这只召唤兽")
	elseif UserData[id].战斗 ~= 0 then
		SendMessage(UserData[id].连接id, 7, "#y/战斗中无法进行此操作")

		return 0
	elseif string.len(self.临时数据.名称) > 10 then
		SendMessage(UserData[id].连接id, 7, "#y/名称长度太长，请再想个短一点的名称吧")
	else
		self.数据[self.临时数据.序号].名称 = self.临时数据.名称

		SendMessage(UserData[id].连接id, 7, "#y/召唤兽改名成功")
		SendMessage(UserData[id].连接id, 2006, self:获取数据())
	end
end
