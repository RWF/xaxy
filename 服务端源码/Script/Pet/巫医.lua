--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2019-12-17 04:52:14
--======================================================================--
function PetControl:巫医治疗(id,类型)
 if 类型==1 then
      self.临时参数=self:取全体数值差(类型)
      self.扣除金钱=math.floor(self.临时参数.气血*0.8+self.临时参数.魔法*1.5)
      if 银子检查(id,self.扣除金钱)==false then
          wb = "#Y/本次操作需要扣除"..self.扣除金钱.."两银子"
         SendMessage(UserData[id].连接id,20,{"超级巫医","超级巫医",wb})
      else
       RoleControl:扣除银子(UserData[id],self.扣除金钱,7)
        self:恢复状态(1)
             local wb = "治疗好了，您的所有召唤兽已经恢复至最佳状态，共收取了"..self.扣除金钱.."两银子"
           SendMessage(UserData[id].连接id,20,{"超级巫医","超级巫医",wb})
      end
  elseif 类型==2 then
   self.临时参数=self:取全体数值差(类型)
   self.扣除金钱=math.floor(self.临时参数*60)
   if 银子检查(id,self.扣除金钱)==false then
           wb = "#Y/本次操作需要扣除"..self.扣除金钱.."两银子"
         SendMessage(UserData[id].连接id,20,{"超级巫医","超级巫医",wb})
    else
     RoleControl:扣除银子(UserData[id],self.扣除金钱,7)
      self:恢复状态(2)
      local wb = "治疗好了，您的所有召唤兽忠诚已经恢复至100，共收取了"..self.扣除金钱.."两银子"
         SendMessage(UserData[id].连接id,20,{"超级巫医","超级巫医",wb})
      end
  elseif 类型==3 then
   self.临时参数=self:取全体数值差(1)
   self.扣除金钱=math.floor(self.临时参数.气血*0.8+self.临时参数.魔法*1.5)
   self.临时参数=self:取全体数值差(2)
   self.扣除金钱=self.扣除金钱+math.floor(self.临时参数*60)
   if 银子检查(id,self.扣除金钱)==false then
           wb = "#Y/本次操作需要扣除"..self.扣除金钱.."两银子"
         SendMessage(UserData[id].连接id,20,{"超级巫医","超级巫医",wb})
    else
     RoleControl:扣除银子(UserData[id],self.扣除金钱,7)
      self:恢复状态(3)
          local wb = "治疗好了，您的所有召唤兽已经恢复至最佳状态，共收取了"..self.扣除金钱.."两银子"
         SendMessage(UserData[id].连接id,20,{"超级巫医","超级巫医",wb})
      end
   end
 end
function PetControl:恢复状态(类型, id)
	if 类型 == 1 then
		if id ~= nil then
			self.数据[id].当前气血 = self.数据[id].气血上限
			self.数据[id].当前魔法 = self.数据[id].魔法上限
		else
			for n = 1, #self.数据 do
				self.数据[n].当前气血 = self.数据[n].气血上限
				self.数据[n].当前魔法 = self.数据[n].魔法上限
			end
		end
	elseif 类型 == 2 then
		if id ~= nil then
			self.数据[id].忠诚 = 100
		else
			for n = 1, #self.数据 do
				self.数据[n].忠诚 = 100
			end
		end
	elseif 类型 == 3 then
		if id ~= nil then
			self.数据[id].当前气血 = self.数据[id].气血上限
			self.数据[id].当前魔法 = self.数据[id].魔法上限
			self.数据[id].忠诚 = 100
		else
			for n = 1, #self.数据 do
				self.数据[n].当前气血 = self.数据[n].气血上限
				self.数据[n].当前魔法 = self.数据[n].魔法上限
				self.数据[n].忠诚 = 100
			end
		end
	end

	SendMessage(UserData[self.玩家id].连接id, 2005, UserData[self.玩家id].召唤兽:取头像数据())
end