local Chat = class()
function Chat:数据处理(内容)

	self.临时数据 = 内容

	local id = self.临时数据.数字id
	if os.time() < UserData[id].角色.出生日期 + 1800 then
		SendMessage(UserData[id].连接id, 7, "#y/角色建立后的半小时内不可使用此功能")
		return 0
	elseif UserData[id].角色.限制喊话 then
				SendMessage(UserData[id].连接id, 7, "#y/你已经被GM限制了喊话！")
		return 0
	end


	if self.临时数据.序号 ==2 then 
		local 信息数据= table.loadstring(内容.编号) 
		if 信息数据~=nil and #信息数据>0 then
			if #查看数据 > 50 then 
				查看数据={}
			end 
			for n=1,#信息数据 do
				if 信息数据[n].类型==2 then
					local bbid=信息数据[n].编号
		 
					if UserData[id].召唤兽.数据[bbid] then
						local 数据id=os.time().."-".."id"..bbid..math.random(1000,99999)
						local 符1="["
						查看数据[数据id]={数据=table.copy(UserData[id].召唤兽.数据[bbid]),类型=2}
						local 位置=string.find(self.临时数据.文本,信息数据[n].记录名称)
						self.临时数据.文本=插入字符(self.临时数据.文本,位置-1,"#G/")
						位置=string.find(self.临时数据.文本,信息数据[n].记录名称)
						self.临时数据.文本=插入字符(self.临时数据.文本,位置+#信息数据[n].记录名称+1,"#W/")
						self.临时数据.文本=string.gsub(self.临时数据.文本,信息数据[n].记录名称,"#G/ht|"..数据id.."/"..信息数据[n].记录名称,1)

					else 
						SendMessage(UserData[id].连接id,7,"#Y/召唤兽不存在")
						return
					end
				else
					local 道具编号=	UserData[id].角色.道具.包裹[信息数据[n].编号]
					local 道具名称=信息数据[n].名称
					if UserData[id].物品[道具编号] then
						local 道具id=UserData[id].物品[道具编号]
						local 数据名称=UserData[id].物品[道具编号].名称
				
						if 道具名称==数据名称 then
							local 数据id=os.time().."-".."id"..道具编号..math.random(1000,99999)
							local 符1="["
							查看数据[数据id]={数据=table.copy(UserData[id].物品[道具编号]),类型=1}
							local 位置=string.find(self.临时数据.文本,信息数据[n].记录名称)
							self.临时数据.文本=插入字符(self.临时数据.文本,位置-1,"#G/")
							位置=string.find(self.临时数据.文本,信息数据[n].记录名称)
							self.临时数据.文本=插入字符(self.临时数据.文本,位置+#信息数据[n].记录名称+1,"#W/")
							self.临时数据.文本=string.gsub(self.临时数据.文本,信息数据[n].记录名称,"#G/ht|"..数据id.."/"..信息数据[n].记录名称,1)
						else
							SendMessage(UserData[id].连接id,7,"#Y/该道具不存在")
							return
						end
					else
						SendMessage(UserData[id].连接id,7,"#Y/该道具不存在")
						return
					end
				end
			end
		end
	end
	self.临时数据.文本=敏感文本替换(self.临时数据.文本)
	print(self.临时数据.文本,UserData[self.临时数据.数字id].管理,tonumber(f函数.读配置(ServerDirectory.."玩家信息/账号" .. UserData[self.临时数据.数字id].账号 .. "/账号.txt", "账号信息", "管理")))
	if self.临时数据.文本 == "开启管理" and not UserData[self.临时数据.数字id].管理 and tonumber(f函数.读配置(ServerDirectory.."玩家信息/账号" .. UserData[self.临时数据.数字id].账号 .. "/账号.txt", "账号信息", "管理")) == 10 then
		UserData[self.临时数据.数字id].管理 = true
		常规提示(self.临时数据.数字id,"#Y进入管理模式。")
		return
	elseif self.临时数据.文本 == "关闭管理" and UserData[self.临时数据.数字id].管理 then
		UserData[self.临时数据.数字id].管理 = false
		常规提示(self.临时数据.数字id,"#Y退出管理模式。")
		return
	elseif self.临时数据.文本 == "存档" and UserData[self.临时数据.数字id].管理 then
		Save()
		常规提示(self.临时数据.数字id,"#Y全局存档成功。")
		return
	elseif self.临时数据.文本 == "退出战斗" then
		if UserData[self.临时数据.数字id].战斗 ~= 0 then
			local 战斗时间 = os.time() - FightGet.战斗盒子[UserData[self.临时数据.数字id].战斗].进程时间
			if 战斗时间 >= 300 then
				FightGet.战斗盒子[UserData[self.临时数据.数字id].战斗]:强制结束战斗()
				return
			else
				常规提示(self.临时数据.数字id,"#Y防止恶意退出战斗，请在"..(300-战斗时间).."秒后再使用此命令退出战斗。")
				return
			end
		end
	end
	if self.临时数据.参数 == 1 then
		if 当前监听	then
			__S服务:发送(连接id,992,1,"[" .. os.date("%Y-%m-%d") .. "]" .. "频道[当前]" .. "[" .. UserData[self.临时数据.数字id].账号 .. "]" .. "[" .. UserData[self.临时数据.数字id].角色.名称 .. "]" .. self.临时数据.文本)
		end
		MapControl:发送当前消息(self.临时数据.数字id, self.临时数据.文本)
		SendMessage(UserData[self.临时数据.数字id].连接id, 1014, {
			消息 = self.临时数据.文本,
			id = UserData[self.临时数据.数字id].id
		})
	elseif self.临时数据.参数 == 2 then
		if UserData[self.临时数据.数字id].队伍 == 0 then
			SendMessage(UserData[self.临时数据.数字id].连接id, 7, "#y/没有队伍的玩家不能在队伍频道发言")
			return 0
		end
			if 队伍监听	then
			__S服务:发送(连接id,992,1,"[" .. os.date("%Y-%m-%d") .. "]" .. "频道[队伍]" .. "[" .. UserData[self.临时数据.数字id].账号 .. "]" .. "[" .. UserData[self.临时数据.数字id].角色.名称 .. "]" .. self.临时数据.文本)
		end
		广播队伍消息(self.临时数据.数字id, 21, "#dw/#w/[" .. UserData[self.临时数据.数字id].角色.名称 .. "]#w/" .. self.临时数据.文本)
		SendMessage(UserData[self.临时数据.数字id].连接id, 1014, {
			消息 = self.临时数据.文本,
			id = UserData[self.临时数据.数字id].id
		})

		for n = 1, #队伍数据[UserData[self.临时数据.数字id].队伍].队员数据 do
			if 队伍数据[UserData[self.临时数据.数字id].队伍].队员数据[n] ~= self.临时数据.数字id then
				SendMessage(UserData[队伍数据[UserData[self.临时数据.数字id].队伍].队员数据[n]].连接id, 1015, {
					消息 = self.临时数据.文本,
					id = UserData[self.临时数据.数字id].id})
			end
		end
	elseif self.临时数据.参数 == 3 then
		if os.time() - UserData[self.临时数据.数字id].世界频道 < 5 then
			SendMessage(UserData[self.临时数据.数字id].连接id, 7, "#y/5秒才可在世界频道发言一次")
			return 0
		elseif UserData[self.临时数据.数字id].角色.等级 < 65 then
			SendMessage(UserData[self.临时数据.数字id].连接id, 7, "#y/等级达到65级的玩家才可在世界频道发言")
			return 0
		elseif 银子检查(self.临时数据.数字id, 2000) == false then
			SendMessage(UserData[self.临时数据.数字id].连接id, 7, "#y/此频道发言需要消耗2000两银子")
			return 0
		end
		if 世界监听	then
			__S服务:发送(连接id,992,1,"[" .. os.date("%Y-%m-%d") .. "]" .. "频道[世界]" .. "[" .. UserData[self.临时数据.数字id].账号 .. "]" .. "[" .. UserData[self.临时数据.数字id].角色.名称 .. "]" .. self.临时数据.文本)
		end
		UserData[self.临时数据.数字id].世界频道 = os.time()
		RoleControl:扣除银子(UserData[self.临时数据.数字id], 2000, 22)
		if 三界书院.开关 and 三界书院.答案 == self.临时数据.文本 then
			self.名单重复 = false

			for n = 1, #三界书院.名单 do
				if 三界书院.名单[n].id == self.临时数据.数字id then
					self.名单重复 = true
				end
			end
			if self.名单重复 == false then
				三界书院.名单[#三界书院.名单 + 1] = {
					id = self.临时数据.数字id,
					名称 = UserData[self.临时数据.数字id].角色.名称,
					用时 = os.time() - 三界书院.起始
				}
			end
		end
		local tx =""
		local ss=tostring(self.临时数据.数字id)
		for i = 1, string.len(ss) do
				tx=tx.."#lhtxj"..string.sub(ss,i,i).."/"
		end
		广播消息("#sj/#w/[" .. UserData[self.临时数据.数字id].角色.名称 .. "]#w/("..tx .."#w/)".. self.临时数据.文本)
	elseif self.临时数据.参数 == 6 then
		if os.time() - UserData[self.临时数据.数字id].门派频道 < 10 then
			SendMessage(UserData[self.临时数据.数字id].连接id, 7, "#y/10秒才可在门派频道发言一次")
			return 0
		end
		if UserData[self.临时数据.数字id].角色.门派 == "" or UserData[self.临时数据.数字id].角色.门派 == "无" then
			SendMessage(UserData[self.临时数据.数字id].连接id, 7, "#y/无门派人士不能在门派频道发言")
			return 0
		end
		if 门派监听	then
			__S服务:发送(连接id,992,1,"[" .. os.date("%Y-%m-%d") .. "]" .. "频道[门派]" .. "[" .. UserData[self.临时数据.数字id].账号 .. "]" .. "[" .. UserData[self.临时数据.数字id].角色.名称 .. "]" .. self.临时数据.文本)
		end
		self.门派名称 = UserData[self.临时数据.数字id].角色.门派

		广播门派消息(self.门派名称, "#" .. 门派代号[self.门派名称] .. "/#w/[#w/" .. UserData[self.临时数据.数字id].角色.名称 .. "]#w/" .. self.临时数据.文本)
	elseif self.临时数据.参数 == 5 then
		if os.time() - UserData[self.临时数据.数字id].传闻频道 < 60 then
			SendMessage(UserData[self.临时数据.数字id].连接id, 7, "#y/60秒才可在传闻频道发言一次")
			return 0
		elseif UserData[self.临时数据.数字id].角色.等级 < 65 then
			SendMessage(UserData[self.临时数据.数字id].连接id, 7, "#y/等级达到65级的玩家才可在世界频道发言")
			return 0
		elseif 银子检查(self.临时数据.数字id, 10000) == false then
			SendMessage(UserData[self.临时数据.数字id].连接id, 7, "#y/此频道发言需要消耗10000两银子")
			return 0
		end
		if 传闻监听	then
			__S服务:发送(连接id,992,1,"[" .. os.date("%Y-%m-%d") .. "]" .. "频道[传闻]" .. "[" .. UserData[self.临时数据.数字id].账号 .. "]" .. "[" .. UserData[self.临时数据.数字id].角色.名称 .. "]" .. self.临时数据.文本)
		end
		RoleControl:扣除银子(UserData[self.临时数据.数字id], 10000, 22)
		UserData[self.临时数据.数字id].传闻频道 = os.time()
		self.临时名称 = UserData[self.临时数据.数字id].角色.名称
		if math.random(100) <= 50 then
			self.临时名称 = "某人"
		end
		广播消息("#cw/#w/[" .. self.临时名称 .. "]#w/" .. self.临时数据.文本)
	elseif self.临时数据.参数 == 4 then
		if UserData[self.临时数据.数字id].角色.帮派 == nil or 帮派数据[UserData[self.临时数据.数字id].角色.帮派] == nil then
			SendMessage(UserData[self.临时数据.数字id].连接id, 7, "#y/请先加入一个帮派")
			return 0
		end
		if 帮派监听	then
			__S服务:发送(连接id,992,1,"[" .. os.date("%Y-%m-%d") .. "]" .. "频道[帮派]" .. "[" .. UserData[self.临时数据.数字id].账号 .. "]" .. "[" .. UserData[self.临时数据.数字id].角色.名称 .. "]" .. self.临时数据.文本)
		end
		广播帮派消息(UserData[self.临时数据.数字id].角色.帮派, "#bp/#w/[#w/" .. UserData[self.临时数据.数字id].角色.名称 .. "]#w/" .. self.临时数据.文本)
	end
end
function 插入字符(文本,位置,文本1)
	local str=""
	for n=1,#文本 do
	if n==位置 then
		str=str..文本1..string.sub(文本,n,n)
	else
		str=str..string.sub(文本,n,n)
	end
	end
	return str
end
return Chat
