--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-12-12 00:05:58
--======================================================================--
function ItemControl:分解装备(id, 格子)--------------------完成--
	self.临时id1 = UserData[id].角色.道具.包裹[格子]
	if UserData[id].物品[self.临时id1] == nil or UserData[id].物品[self.临时id1] == 0 then
		SendMessage(UserData[id].连接id, 7, "#y/你似乎并没有这样的道具")
		return 0
	elseif UserData[id].物品[self.临时id1].类型 ~="武器" and UserData[id].物品[self.临时id1].类型 ~="头盔"and UserData[id].物品[self.临时id1].类型 ~="衣服"and UserData[id].物品[self.临时id1].类型 ~="项链" and UserData[id].物品[self.临时id1].类型 ~="鞋子" and UserData[id].物品[self.临时id1].类型 ~="腰带" and UserData[id].物品[self.临时id1].类型 ~="衣服"  then

		SendMessage(UserData[id].连接id, 7, "#y/只能分解装备!")
		return 0
    elseif UserData[id].物品[self.临时id1].等级 <60 then
    	SendMessage(UserData[id].连接id, 7, "#y/只能分解60级以上的装备!")
		return 0
	elseif UserData[id].物品[self.临时id1].加锁 then
		  	SendMessage(UserData[id].连接id, 7, "#y/加锁的装备无法进行分解!")
		return 0
		elseif UserData[id].物品[self.临时id1].Time then
		  	SendMessage(UserData[id].连接id, 7, "#y/有效期的装备无法进行分解!")
		return 0
	elseif UserData[id].物品[self.临时id1].专用 then
				SendMessage(UserData[id].连接id, 7, "#y/专用的装备无法进行分解!")
				return 0
	else
		local fjf =0
		local fj = {sl= math.floor(UserData[id].物品[self.临时id1].等级/10*4),tl= math.floor(UserData[id].物品[self.临时id1].等级/10*7)}
	   for n = 1, 20 do
			if UserData[id].角色.道具.包裹[n] ~= nil and UserData[id].物品[UserData[id].角色.道具.包裹[n]] ~= nil then
				if UserData[id].物品[UserData[id].角色.道具.包裹[n]].名称=="分解符" then
				    fjf = fjf + UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量
				end
			end
		end

		 if UserData[id].角色.当前体力 < fj.tl  then
		    SendMessage(UserData[id].连接id, 7, "#y/当前体力不足无法分解装备!")
		    return 0
		 elseif fjf <fj.sl  then
		 	 SendMessage(UserData[id].连接id, 7, "#y/当前所需要的分解符数量不足!")
		    return 0
		 else
              UserData[id].角色.当前体力=UserData[id].角色.当前体力-fj.tl
              SendMessage(UserData[id].连接id, 7, "#y/当前体力减少#r/"..fj.tl)
              self:处理分解符(id,fj.sl)


					self:GiveItem(id,"吸附石",math.floor(UserData[id].物品[self.临时id1].等级/20))

              -- end

			UserData[id].物品[self.临时id1] = nil
			UserData[id].角色.道具.包裹[格子] = nil
			SendMessage(UserData[id].连接id, 3006, "66")
			SendMessage(UserData[id].连接id, 7, "#y/分解成功！")
		 end


	end
 end
function ItemControl:处理分解符(id,sl)
	local fjf = sl
	   for n = 1, 80 do
		   	if  fjf > 0  then
				if UserData[id].角色.道具.包裹[n] ~= nil and UserData[id].物品[UserData[id].角色.道具.包裹[n]] ~= nil  then
					if UserData[id].物品[UserData[id].角色.道具.包裹[n]].名称=="分解符"  then
						if  UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量 <= fjf then
							fjf = fjf - UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量
	                        UserData[id].物品[UserData[id].角色.道具.包裹[n]]=nil
	                        UserData[id].角色.道具.包裹[n] = nil
						else
							UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量=UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量-fjf
							fjf = 0
						end
					end
				end
			else
				break
			end
		end
  end