-- @Author: 作者QQ381990860
-- @Date:   2021-09-07 00:54:26
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-09-10 10:58:25
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-09-07 18:19:08
--======================================================================--
ItemControl = class()
require("Script/Item/召唤兽道具")
require("Script/Item/消耗道具")
require("Script/Item/坐骑道具")
require("Script/Item/交易")
require("Script/Item/仓库处理")
require("Script/Item/大转盘")
require("Script/Item/月光宝盒")
require("Script/Item/法宝")
require("Script/Item/炼妖")
require("Script/Item/竞拍")
require("Script/Item/给予")
require("Script/Item/装备灵饰")
require("Script/Item/道具出售")
require("Script/Item/道具加锁")
require("Script/Item/锦衣")
require("Script/Item/分解符")
require("Script/Item/小窗口技能")
require("Script/Item/窗口道具")
require("Script/Item/染色处理")
require("Script/Item/仙玉抽奖")
require("Script/Item/银币抽奖")
function ItemControl:道具使用(user,格子,PitchOn,Text)
	if not user or user.战斗 ~= 0 or user.摆摊 then
		return 
	end
	local ItemID = user.角色.道具.包裹[格子]
    local ItemTemp = user.物品[ItemID]
	if not ItemTemp  or not  ItemData[ItemTemp.名称] then
		SendMessage(user.连接id, 7, "#y/你没有这样的道具")
		return
	end
   if self[ItemData[ItemTemp.名称].类型.."使用"] then 
   	        if  user.物品[ItemID].Time and user.物品[ItemID].Time <= os.time() then 
   	        		    user.物品[ItemID] =nil
		             	user.角色.道具.包裹[格子] =nil
		             	SendMessage(user.连接id, 7, "#y/你的道具已经过期被系统回收")
		    elseif  not self[ItemData[ItemTemp.名称].类型.."使用"](self,user,ItemTemp,PitchOn,Text,格子) then 
		          if ItemData[ItemTemp.名称].叠加 then
		            if not user.物品[ItemID].数量 then
		                 user.物品[ItemID].数量 =1                             
		            end                                    
		             user.物品[ItemID].数量 =user.物品[ItemID].数量 - 1
		          end
		          if not ItemData[ItemTemp.名称].叠加 or user.物品[ItemID].数量 < 1 then
		           		user.物品[ItemID] =nil
		             	user.角色.道具.包裹[格子] =nil
		          end              --扣除道具
		         
		    end 
			 SendMessage(user.连接id, 3007,{格子=格子,数据=user.物品[user.角色.道具.包裹[格子]]})
	else  
		SendMessage(user.连接id, 7, "#y/这个物品不可以直接使用")
	end

end
function ItemControl:RemoveItems(user,number,text,数量)

      if not user.角色.道具.包裹[number] then
      	 print("ID为："..user.id.."删除数据有问题请检查，编号"..number.."来源"..text)
      	 return 
      end
	  if ItemData[user.物品[user.角色.道具.包裹[number]].名称].叠加 then
        if not user.物品[user.角色.道具.包裹[number]] then
             user.物品[user.角色.道具.包裹[number]].数量 =1                             
        end                                    
         user.物品[user.角色.道具.包裹[number]].数量 =user.物品[user.角色.道具.包裹[number]].数量 -  (数量 or 1)
      end
      if not ItemData[user.物品[user.角色.道具.包裹[number]].名称].叠加 or user.物品[user.角色.道具.包裹[number]].数量 < 1 then
		     user.物品[user.角色.道具.包裹[number]] =nil 
		     user.角色.道具.包裹[number] =nil
      end 

     SendMessage(user.连接id, 3007,{格子=number,数据=user.物品[user.角色.道具.包裹[number]]})

end

function ItemControl:寻找道具(user,data)
	local  gz={}
	  for i = 1, #data do
           local lskg = false
           for n = 1, 80 do
             if user.角色.道具.包裹[n] ~= nil and lskg == false and user.物品[user.角色.道具.包裹[n]].名称 == data[i] then
               lskg = true
               gz[#gz + 1] = n
             end
           end
       end
       return gz
end


function ItemControl:数据处理(id, 序号, 内容, 参数,编号)
	if UserData[id]==nil or UserData[id].战斗 ~= 0 then
		return 0
	end

	if UserData[id].摆摊 ~= nil and 序号 ~= 1 then
		return 0
	end
	if 序号 == 1 then
		self:索要道具(id, 参数)
	elseif 序号 == 2 then
		self:佩戴装备(id, 参数,内容,编号)
	elseif 序号 == 3 then
      self:丢弃道具(id,{格子 = 参数,类型 = 内容})
	elseif 序号 == 4 then
		self:出售道具处理(id, 参数, 内容,编号)
	elseif 序号 == 5 then
		self:鉴定道具(id, 参数, 内容)
	elseif 序号 == 6 then
		 self:索要打造道具(id, "包裹", "打造")
	elseif 序号 == 7 then
		EquipmentControl:数据处理(id, 4006, 参数, 内容)
	elseif 序号 == 11 then
		SendMessage(UserData[id].连接id, 3009, self:索要道具1(id, 内容))
	elseif 序号 == 12 then
		SendMessage(UserData[id].连接id, 3010, self:索要仓库道具(id, 参数))
	elseif 序号 == 13 then
		self:出售全部道具(id,内容)
	elseif 序号 == 14 then
		self:空间互换(id, 参数,内容,编号)
	elseif 序号 == 22 then
		self:索要炼妖数据(id,内容)
	elseif 序号 == 23 then
		self:炼妖合宠(id, 参数, 内容)
	elseif 序号 == 24 then
		self:道具合宠(id, 参数, 内容,编号)
	elseif 序号 == 26 then
		self:佩戴bb装备处理(id, 参数, 内容,编号)
	elseif 序号 == 27 then
		self:卸下bb装备处理(id, 参数, 内容,编号)
	elseif 序号 == 28 then
		self:炼药处理(id, 参数, 内容)
	elseif 序号 == 29 then
		self:烹饪处理(id, 参数, 内容)
	elseif 序号 == 30 then
	self:索要道具(id, "包裹",参数)
	elseif 序号 == 32 then
		self:召唤兽快捷加血(id)
	elseif 序号 == 33 then
		self:召唤兽快捷加蓝(id)
	elseif 序号 == 34 then
		self:角色快捷加血(id)
	elseif 序号 == 35 then
		self:角色快捷加蓝(id)
	elseif 序号 == 36 then
		self.临时数据 = {id = tonumber(内容),格子 = 参数}
		if self.临时数据.id == 0 then
			self:点化装备处理(id, self.临时数据)
		elseif self:删除道具处理(id, self.临时数据) == false then
			return
		elseif self.临时数据.id == -701 then
			self.帮派id = UserData[id].角色.帮派

			if self.帮派id ~= nil and 帮派数据[self.帮派id] ~= nil then

		      local sl = UserData[id].物品[UserData[id].角色.道具.包裹[参数]].数量
		      		UserData[id].物品[UserData[id].角色.道具.包裹[参数]] = nil
					UserData[id].角色.道具.包裹[参数] = nil
				帮派数据[self.帮派id].资金 = 帮派数据[self.帮派id].资金 + 50000*sl
				if 帮派数据[self.帮派id].资金 > 帮派数据[self.帮派id].金库 * 5000000 + 3000000*sl then
					帮派数据[self.帮派id].资金 = 帮派数据[self.帮派id].金库 * 5000000 + 3000000*sl
				end
				帮派数据[self.帮派id].繁荣 = 帮派数据[self.帮派id].繁荣 + 1*sl
				帮派数据[self.帮派id].成员名单[id].帮贡.获得 = 帮派数据[self.帮派id].成员名单[id].帮贡.获得 + 5*sl
				帮派数据[self.帮派id].成员名单[id].帮贡.当前 = 帮派数据[self.帮派id].成员名单[id].帮贡.当前 + 5*sl

				SendMessage(UserData[id].连接id, 7, "#y/你获得了"..(sl*5).."点帮贡")
				广播帮派消息(UserData[id].角色.帮派, "#bp/#w/" .. UserData[id].角色.名称 .. "#w/在白虎堂总管处上交了#G/"..sl.."#w/个金银宝盒，为帮派增加了"..(sl*50000).."两的资金。")
			end

		elseif self.临时数据.id == -21 then
		   if  活动数据.平定安邦[id] ==nil then
           	    	活动数据.平定安邦[id] =10
          	end
           if   活动数据.平定安邦[id] <=0 then        
           	SendMessage(UserData[id].连接id, 7, "#y/今日上交次数已经达到上限,请明日在来")
           	return
           elseif 	UserData[id].物品[UserData[id].角色.道具.包裹[参数]].数量  < 20 then
           	SendMessage(UserData[id].连接id, 7, "#y/你的心魔宝珠不足20个无法兑换奖励")
           	return
           else

                 UserData[id].物品[UserData[id].角色.道具.包裹[参数]].数量 =UserData[id].物品[UserData[id].角色.道具.包裹[参数]].数量 -20
                 if UserData[id].物品[UserData[id].角色.道具.包裹[参数]].数量 < 1 then
                     UserData[id].物品[UserData[id].角色.道具.包裹[参数]] = nil
					UserData[id].角色.道具.包裹[参数] = nil
                 end
                        活动数据.平定安邦[id] =活动数据.平定安邦[id] -1
                        local 奖励经验=math.floor(UserData[id].角色.等级*UserData[id].角色.等级*16)*14
        				local 奖励银子=math.floor((UserData[id].角色.等级*UserData[id].角色.等级+math.random(2000,2500))*1.5)*90
                        RoleControl:添加经验(UserData[id],奖励经验,"心魔宝珠")
        				RoleControl:添加储备(UserData[id],奖励银子,"心魔宝珠")
           	end
	


		elseif LinkTask[任务数据[self.临时数据.id].类型] then
			TaskControl:FulfilLinkTask(id, self.临时数据.id)
		elseif 任务数据[self.临时数据.id].类型 == "官职" then
			TaskControl:完成官职物品任务(id, self.临时数据.id)
		elseif 任务数据[self.临时数据.id].类型 == "神器任务" then
			   TaskControl:完成神器任务(id, self.临时数据.id)
		elseif 任务数据[self.临时数据.id].类型 == "帮派青龙" then
			TaskControl:完成青龙任务(self.临时数据.id, id)
		elseif 任务数据[self.临时数据.id].类型 == "师门" then
			TaskControl:完成师门任务(id)
		elseif 任务数据[self.临时数据.id].类型 == "押镖" then
			RoleControl:处理押镖任务(UserData[id],self.临时数据.id)
			RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"押镖"))
			任务数据[self.临时数据.id] = nil
			广播消息(9, "#jj/#g/ " .. UserData[id].角色.名称 .. "#y/在完成押镖任务获得大量银子")
		elseif 任务数据[self.临时数据.id].类型 == "坐骑" then
			 TaskControl:完成坐骑任务2(id)

	   	elseif 任务数据[self.临时数据.id].类型 == "任务链" then
			 TaskControl:完成任务链(id,tonumber(UserData[id].id.."14"))
		elseif 任务数据[self.临时数据.id].类型 == "宝宝任务链" then
			 TaskControl:完成宝宝任务链(id,tonumber(UserData[id].id.."187"))
		elseif 任务数据[self.临时数据.id].类型 == "打造" then
			if 任务数据[self.临时数据.id].灵饰 then
				self.临时格子 = RoleControl:取可用道具格子(UserData[id],"包裹")
				if self.临时格子 == 0 then
					SendMessage(UserData[id].连接id, 7, "#y/你的包裹空间已满，无法获得新道具")
					return
				end
				self.临时id = self:取道具编号(id)
				self:灵饰处理(id, self.临时id, 任务数据[self.临时数据.id].等级, 1, 任务数据[self.临时数据.id].分类)
				UserData[id].物品[self.临时id].名称 = 任务数据[self.临时数据.id].名称
				UserData[id].物品[self.临时id].制造者 = UserData[id].角色.名称 .. "强化打造"
				UserData[id].物品[self.临时id].等级 = 任务数据[self.临时数据.id].等级
				UserData[id].物品[self.临时id].类型 = 任务数据[self.临时数据.id].分类
				UserData[id].物品[self.临时id].耐久 = 500
				UserData[id].物品[self.幻化id].特效={[1]="无级别限制"}
				UserData[id].角色.道具.包裹[self.临时格子] = self.临时id
				SendMessage(UserData[id].连接id, 7, "#y/你获得了#r/" .. 任务数据[self.临时数据.id].名称)

			else
				self.临时id = EquipmentControl:生成装备(id,任务数据[self.临时数据.id].分类, 任务数据[self.临时数据.id].名称, 任务数据[self.临时数据.id].等级, 1.6, 1.3, false, 1, UserData[id].角色.名称, nil, nil, nil, nil,任务数据[self.临时数据.id].元身)
				self.临时格子 = RoleControl:取可用道具格子(UserData[id],"包裹")
				SendMessage(UserData[id].连接id, 7, "#y/你获得了#r/" .. 任务数据[self.临时数据.id].名称)
				if self.临时格子 ~= 0 then
					UserData[id].角色.道具.包裹[self.临时格子] = self.临时id
				else
					SendMessage(UserData[id].连接id, 7, "#y/由于你的包裹空间已满，无法获得新道具#r/" .. 任务数据[self.临时数据.id].名称)
				end
			end
			SendMessage(UserData[id].连接id, 3006, "66")
            RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"打造"))
			任务数据[self.临时数据.id] = nil
		end
	
	elseif 序号 == 46 then
		self:修理装备处理(id, {格子 = 参数,id = 内容})

	elseif 序号 == 48 then
		self:存钱处理(id, 参数, 内容)
	elseif 序号 == 49 then
		self:取钱处理(id, 参数, 内容)
	elseif 序号 == 50 then
		ShopControl:GetNpcShop(id, 12)
	elseif 序号 == 51 then
		if 系统消息数据[id] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/你没有新的系统消息了")
		elseif #系统消息数据[id] >= 1 then
			SendMessage(UserData[id].连接id, 14, 系统消息数据[id][1])
			table.remove(系统消息数据[id], 1)

			if #系统消息数据[id] <= 0 then
				SendMessage(UserData[id].连接id, 12, "1")
			end
		else
			SendMessage(UserData[id].连接id, 7, "#y/你没有新的系统消息了")
		end
	elseif 序号 == 52 then
		if 系统消息数据[id] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/你没有新的系统消息了")
		elseif #系统消息数据[id] >= 1 then
			SendMessage(UserData[id].连接id, 14, 系统消息数据[id][1])
			table.remove(系统消息数据[id], 1)

			if #系统消息数据[id] <= 0 then
				SendMessage(UserData[id].连接id, 12, "1")
			end
		else
			SendMessage(UserData[id].连接id, 7, "#y/你没有新的系统消息了")
		end
	elseif 序号 == 53 then
		SendMessage(UserData[id].连接id, 2011, {UserData[id].角色.称谓,UserData[id].仙玉,UserData[id].角色.称谓特效})
	elseif 序号 == 54 then
		RoleControl:删除称谓(UserData[id],内容)
	elseif 序号 == 55 then
		RoleControl:隐藏称谓(UserData[id], 内容)
	elseif 序号 == 56 then
		RoleControl:改变称谓(UserData[id], 内容,编号)
	elseif 序号 == 57 then
		RoleControl:buyEffects(UserData[id], 内容)

	elseif 序号 == 58 then
			self:脱下装备(id, 参数,内容,编号)
	elseif 序号 == 59 then
			self:脱下灵饰(id, 参数,内容,编号)
	elseif 序号 == 61 then
		if UserData[id].角色.帮派 == nil then
			SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有加入一个帮派哟")

			return 0
		else
			self:获取帮派信息(id, UserData[id].角色.帮派)
		end
	elseif 序号 == 62 then
		self:创建帮派(id, 内容)
	elseif 序号 == 63 then
		if 取帮派踢人权限(id, UserData[id].角色.帮派) == false then
			SendMessage(UserData[id].连接id, 7, "#y/你没有修改权限")

			return 0
		else
			if 帮派数据[UserData[id].角色.帮派].资金 < 帮派数据[UserData[id].角色.帮派].规模 * 3000000 then
				SendMessage(UserData[id].连接id, 7, "#y/你帮派的资金不足")

				return 0
			end

			帮派数据[UserData[id].角色.帮派].宗旨 = 内容
			self.消耗资金 = 0

			if 帮派数据[UserData[id].角色.帮派].修改宗旨 == false then
				帮派数据[UserData[id].角色.帮派].修改宗旨 = true
			else
				self.消耗资金 = 500000
			end

			帮派数据[UserData[id].角色.帮派].资金 = 帮派数据[UserData[id].角色.帮派].资金 - self.消耗资金

			SendMessage(UserData[id].连接id, 7, "#y/修改帮派宗旨成功！")
			广播帮派消息(UserData[id].角色.帮派, "#bp/#g/" .. UserData[id].角色.名称 .. "#y/修改了帮派宗旨。本次操作消耗了#r/" .. self.消耗资金 .. "#y/两帮派资金。")
		end
	elseif 序号 == 64 then
		if UserData[id].角色.帮派 ~= nil then
			SendMessage(UserData[id].连接id, 7, "#y/请先脱离原有的帮派")

			return 0
		end

		if 帮派数据[参数] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/这个帮派并不存在")

			return 0
		end

		if 帮派数据[参数].申请名单[id] == nil then
			帮派数据[参数].申请名单[id] = {
				日期 = os.time(),
				日期文本 = os.date("[%Y年%m月%d日%X"),
				id = id,
				名称 = UserData[id].角色.名称,
				门派 = UserData[id].角色.门派,
				等级 = UserData[id].角色.等级
			}

			广播帮派消息(参数, "#bp/#g/" .. UserData[id].角色.名称 .. "#y/申请加入本帮，请尽快处理")
			SendMessage(UserData[id].连接id, 7, "#y/提交入帮申请成功")
		else
			SendMessage(UserData[id].连接id, 7, "#y/你已经在对方的申请名单里了，请耐心等待")
		end
	elseif 序号 == 65 then
		if UserData[id].角色.帮派 == nil then
			SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有帮派")

			return 0
		elseif 帮派数据[UserData[id].角色.帮派] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/帮派数据异常")

			return 0
		else
			self.发送信息 = {}

			for n, v in pairs(帮派数据[UserData[id].角色.帮派].申请名单) do
				if UserData[n] ~= nil then
					self.发送信息[#self.发送信息 + 1] = 帮派数据[UserData[id].角色.帮派].申请名单[n]
				end
			end
		end

		SendMessage(UserData[id].连接id, 20022, self.发送信息)
	elseif 序号 == 66 then
		参数 = 参数 + 0

		if 取帮派踢人权限(id, UserData[id].角色.帮派) == false then
			SendMessage(UserData[id].连接id, 7, "#y/你没有此项操作权限")

			return 0
		end

		帮派数据[UserData[id].角色.帮派].申请名单 = {}

		广播帮派消息(UserData[id].角色.帮派, "#bp/#g/" .. UserData[id].角色.名称 .. "#r/清空了帮派申请名单")
		SendMessage(UserData[id].连接id, 7, "#y/清空申请列表成功")

		self.发送信息 = {}

		for n, v in pairs(帮派数据[UserData[id].角色.帮派].申请名单) do
			if UserData[n] ~= nil then
				self.发送信息[#self.发送信息 + 1] = 帮派数据[UserData[id].角色.帮派].申请名单[n]
			end
		end

		SendMessage(UserData[id].连接id, 20022, self.发送信息)
	elseif 序号 == 67 then
		参数 = 参数 + 0

		if 取帮派加人权限(id, UserData[id].角色.帮派) == false then
			SendMessage(UserData[id].连接id, 7, "#y/你没有此项操作权限")

			return 0
		else
			广播帮派消息(UserData[id].角色.帮派, "#bp/#g/" .. UserData[id].角色.名称 .. "#r/拒绝了#g/" .. 帮派数据[UserData[id].角色.帮派].申请名单[参数].名称 .. "#r/的入帮申请")

			帮派数据[UserData[id].角色.帮派].申请名单[参数] = nil

			SendMessage(UserData[id].连接id, 7, "#y/你拒绝了对方的入帮申请")

			self.发送信息 = {}

			for n, v in pairs(帮派数据[UserData[id].角色.帮派].申请名单) do
				if UserData[n] ~= nil then
					self.发送信息[#self.发送信息 + 1] = 帮派数据[UserData[id].角色.帮派].申请名单[n]
				end
			end

			SendMessage(UserData[id].连接id, 20022, self.发送信息)
		end
	elseif 序号 == 68 then
		参数 = 参数 + 0

		if 取帮派加人权限(id, UserData[id].角色.帮派) == false then
			SendMessage(UserData[id].连接id, 7, "#y/你没有此项操作权限")

			return 0
		elseif UserData[参数] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/对方不在线，无法进行此操作")

			return 0
		elseif UserData[参数].角色.帮派 ~= nil then
			SendMessage(UserData[id].连接id, 7, "#y/对方已经加入了其它帮派")

			帮派数据[UserData[id].角色.帮派].申请名单[参数] = nil
			self.发送信息 = {}

			for n, v in pairs(帮派数据[UserData[id].角色.帮派].申请名单) do
				if UserData[n] ~= nil then
					self.发送信息[#self.发送信息 + 1] = 帮派数据[UserData[id].角色.帮派].申请名单[n]
				end
			end

			SendMessage(UserData[id].连接id, 20022, self.发送信息)

			return 0
		elseif 帮派数据[UserData[id].角色.帮派].厢房 * 20 + 20 <= self:取帮派人数(UserData[id].角色.帮派) then
			SendMessage(UserData[id].连接id, 7, "#y/本帮人数已达上限，无法接收新的帮众")

			return 0
		else
			广播帮派消息(UserData[id].角色.帮派, "#bp/#g/" .. UserData[id].角色.名称 .. "#r/批准了#g/" .. 帮派数据[UserData[id].角色.帮派].申请名单[参数].名称 .. "#r/的入帮申请")

			帮派数据[UserData[id].角色.帮派].申请名单[参数] = nil

			SendMessage(UserData[id].连接id, 7, "#y/你批准了对方的入帮申请")

			UserData[参数].角色.帮派 = UserData[id].角色.帮派
			帮派数据[UserData[id].角色.帮派].成员名单[参数] = {
				职务 = "帮众",
				id = 参数,
				名称 = UserData[参数].角色.名称,
				帮贡 = {
					当前 = 0,
					获得 = 0
				},
				入帮时间 = os.time(),
				离线时间 = os.time()
			}



		
		RoleControl:添加称谓(UserData[参数],帮派数据[UserData[id].角色.帮派].名称.."的帮众")

			SendMessage(UserData[参数].连接id, 7, "#y/你成功加入了#r/" .. 帮派数据[UserData[id].角色.帮派].名称 .. "#y/帮派")

			self.发送信息 = {}

			for n, v in pairs(帮派数据[UserData[id].角色.帮派].申请名单) do
				if UserData[n] ~= nil then
					self.发送信息[#self.发送信息 + 1] = 帮派数据[UserData[id].角色.帮派].申请名单[n]
				end
			end

			SendMessage(UserData[id].连接id, 20022, self.发送信息)
		end
	elseif 序号 == 69 then
		if UserData[id].角色.帮派 == nil then
			SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有加入一个帮派哟")

			return 0
		elseif 帮派数据[UserData[id].角色.帮派].成员名单[id].职务 ~= "帮众" then
			SendMessage(UserData[id].连接id, 7, "#y/请先解除您的帮派职务")

			return 0
		else

		 local tempname = 帮派数据[UserData[id].角色.帮派].名称.."的"
	      for k,v in pairs{"副帮主","左护法","右护法","长老","帮众"} do
	         if RoleControl:检查称谓(UserData[id],tempname..v) then 
	           RoleControl:回收称谓(UserData[id],tempname..v)
	          break
	         end 
	      end

			帮派数据[UserData[id].角色.帮派].成员名单[id] = nil

			广播帮派消息(UserData[id].角色.帮派, "#bp/#g/" .. UserData[id].角色.名称 .. "#w/退出了本帮")
			UserData[id].角色.帮派 = nil
			SendMessage(UserData[id].连接id, 20023, "1")
			SendMessage(UserData[id].连接id, 7, "#y/脱离帮派成功")
		end
	elseif 序号 == 70 then
		if UserData[id].角色.帮派 == nil then
			SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有加入一个帮派哟")
			return 0
		elseif 取帮派踢人权限(id, UserData[id].角色.帮派) == false then
			SendMessage(UserData[id].连接id, 7, "#y/你没有此种操作的权限")

			return 0
		elseif 帮派数据[UserData[id].角色.帮派].成员名单[参数].职务 ~= "帮众" then
			SendMessage(UserData[id].连接id, 7, "#y/请先解除对方的帮派职务")

			return 0
		else
			参数 = 参数 + 0

			广播帮派消息(UserData[id].角色.帮派, "#bp/#g/" .. UserData[id].角色.名称 .. "#y/动用权限将#r/" .. 帮派数据[UserData[id].角色.帮派].成员名单[参数].名称 .. "#y/踢出了本帮")

			帮派数据[UserData[id].角色.帮派].成员名单[参数] = nil

			self:获取帮派信息(id, UserData[id].角色.帮派)

			if UserData[参数] ~= nil then
				UserData[参数].角色.帮派 = nil

				SendMessage(UserData[参数].连接id, 20023, "1")
				SendMessage(UserData[参数].连接id, 7, "#y/你已经被请离了本帮！")
			end

			SendMessage(UserData[id].连接id, 7, "#y/请离帮派成员成功！")
		end
	elseif 序号 == 71 then
		UserData[id].帮派操作id = 参数 + 0
		SendMessage(UserData[id].连接id,20,{nil,"帮派","请选择您要任命的职务：",{"副帮主","左护法","右护法","长老","帮众"}})
	elseif 序号 == 72 then
		self:设置内政(id, 内容)
	elseif 序号 == 74 then
		self:索要灵饰(id)
	elseif 序号 == 75 then
		self:佩戴灵饰(id, 参数, 内容,编号)
  elseif 序号==87 then
  	  self:索要出售道具(id,内容)
  elseif 序号 == 88 then
	 self:道具提取(id, 参数, 内容,编号)
  elseif 序号 == 89 then
		EquipmentControl:数据处理(id, 4007, 参数, 内容)
	elseif 序号 == 90 then
		self:AddPages(id)
   elseif 序号 == 91 then
		SendMessage(UserData[id].连接id,2033,self:索要道具1(id, "包裹","选择武器"))
  elseif 序号 == 92 then
		SendMessage(UserData[id].连接id,2033,self:索要道具1(id, "包裹","选择装备"))
	elseif 序号 == 93 then   --选择装备
		self:索要单个道具(id,参数,"装备")
		
		
	elseif 序号 == 94 then	--选择武器 
		self:索要单个道具(id,参数,"武器")
		
   	elseif 序号 == 184 then
	 self:佩戴锦衣(id, 参数,内容,编号)
   elseif 序号==185 then
     self:符石升级处理(id,内容)
     elseif 序号==194 then
   	  elseif 序号==195 then
   	self:法宝修炼(id,{格子=参数,类型=内容})
   	elseif 序号 == 196 then
	 self:佩戴法宝(id, 参数,内容,编号)
	elseif 序号 == 197 then
	SendMessage(UserData[id].连接id,2021, self:索要道具2(id, 内容))
	elseif 序号 == 198 then
	SendMessage(UserData[id].连接id,2023,self:索要加锁道具(id, 内容))
	elseif 序号 == 199 then
	self:加锁物品(id,参数,内容+0)
	elseif 序号 == 200 then
	self:解锁物品(id,参数,内容+0)
	elseif 序号 == 201 then
	SendMessage(UserData[id].连接id,2028,self:索要道具3(id, 内容))
	elseif 序号 == 202 then --召唤兽
	-- SendMessage(UserData[id].连接id,2028,self:索要道具3(id, 内容))
	elseif 序号 == 203 then --上架商品
	self:竞拍物品(id,参数,内容+0)
	elseif 序号 == 204 then
	self:竞拍物品价格(id,参数,内容+0)
	elseif 序号 == 205 then
	self:分解装备(id,参数)
	elseif 序号 == 206 then
	self:转盘抽奖(id,参数)
	elseif 序号 == 207 then
	self:月光宝盒使用(id,参数)
	elseif 序号 == 208 then
	self:月光宝盒处理(id,内容)


	elseif 序号==209 then --补充法宝
	self:法宝补充灵气(id,{格子=参数,类型=内容})
 	elseif 序号==210 then --提取  
 	   self:法宝提取灵气(id,{格子=参数,类型=内容})
	elseif 序号 == 211 then
	self:小窗口技能处理(id,参数+0,内容,编号)
   elseif 序号 == 215 then
   	self:法宝任务书处理(id, 参数, 内容)
    elseif 序号 == 216 then
   	self:法宝合成处理(id, 内容)
	elseif 序号 == 221 then
	 	self:摄灵珠处理(id,{格子=参数+0,类型=内容},编号)
	-- elseif 序号 == 223 then
	-- 	self:仙玉抽奖(id,math.random(200,207))
	elseif 序号 == 224 then
		self:仙玉抽奖处理(id,参数+0)
	elseif 序号 == 225 then
		if 内容 and 内容~=""then
			if 编号+0 ==1 then
			广播消息(9,"#jj/#S(幸运抽奖)#Y/恭喜玩家#g/ "..UserData[id].角色.名称.."#y/在第10环中抽奖获得了#r/"..内容)
			else
		     广播消息(9,"#jj/#Y/恭喜玩家#g/ "..UserData[id].角色.名称.."#y/在仙玉抽奖中获得了#r/"..内容)
		    end
		end

	elseif 序号 == 229 then
	 	self:坐骑饰品脱下处理(id,{格子=参数+0,类型=内容},编号)
   	elseif 序号 == 242 then
	self:影蛊使用(id,参数)
	elseif 序号 == 243 then
      if   内容 and UserData[内容+0] then
      	SendMessage(UserData[id].连接id, 7, "#R/[".. UserData[内容+0].角色.名称.."]".."#W/所在#G/"..  MapData[UserData[内容+0].地图].名称.."#W/坐标为:X=#G/"..math.modf(UserData[内容+0].角色.地图数据.x/20)..",#W/Y=#G/"..math.modf(UserData[内容+0].角色.地图数据.y/20) )
      	SendMessage(UserData[id].连接id, 9, "#xt/#R/[".. UserData[内容+0].角色.名称.."]".."#W/所在#G/"..  MapData[UserData[内容+0].地图].名称.."#W/坐标为:X=#G/"..math.modf(UserData[内容+0].角色.地图数据.x/20)..",#W/Y=#G/"..math.modf(UserData[内容+0].角色.地图数据.y/20) )
      else
      	SendMessage(UserData[id].连接id, 7, "#y/你要查找的玩家不在线或者ID输入错误")
      end
	elseif 序号 == 245 then
		self:neaten(id,参数);
	elseif 序号 == 246 then
		SendMessage(UserData[id].连接id, 3009, self:索要道具1(id, "包裹"))
		SendMessage(UserData[id].连接id, 3010, self:索要仓库道具(id, "1"))
	elseif 序号 == 247 then 
		内容 =math.floor(内容+0) 

		local ItemID = UserData[id].角色.道具.包裹[参数]
	    if ItemID and UserData[id].物品[ItemID] then 
	    	if ItemData[UserData[id].物品[ItemID].名称].叠加  then
	    		  if 内容 <1 then 
   		  	 		SendMessage(UserData[id].连接id, 7, "#y/输入的数量参数有误")
	    		  	 return
	    		  elseif UserData[id].物品[ItemID].数量 <= 内容 then
	    		  	 SendMessage(UserData[id].连接id, 7, "#y/输入的数量参数有误")
	    		  	 return
	    		   elseif RoleControl:取可用格子数量(UserData[id],"包裹") < 1 then
	                    SendMessage(UserData[id].连接id, 7, "#y/请先预留1个道具空间")
	                    return 
	    		  end
	    		  local lscon = UserData[id].物品[ItemID].数量 -内容
	    		  
	    		  

	    		  local 临时格子=RoleControl:取可用道具格子(UserData[id],"包裹")

	    		   local 临时编号=self:取道具编号(id)
	    		   --新物品
	    		      UserData[id].物品[临时编号]={}
				      UserData[id].角色.道具.包裹[临时格子]=临时编号
				      UserData[id].物品[临时编号]= table.copy(UserData[id].物品[ItemID])
				      UserData[id].物品[临时编号].数量 =内容
				      	 SendMessage(UserData[id].连接id, 3007,{格子=临时格子,数据=UserData[id].物品[临时编号]})
	    		   --老物品

  
                     UserData[id].物品[ItemID].数量 = lscon
          			
 
                   SendMessage(UserData[id].连接id, 3007,{格子=参数,数据=UserData[id].物品[ItemID]})
                   -- SendMessage(UserData[id].连接id, 7, "#y/物品拆分成功")
	        else 
	        	SendMessage(UserData[id].连接id, 7, "#y/这个物品不能进行拆分")
	    	end
	    else 
	    	SendMessage(UserData[id].连接id, 7, "#y/你没有这样的道具")
				return
	    end
		 elseif 序号 == 248 then                 --id=id, 序号=序号, 内容=文本, 参数=参数,编号=nil
	self:华山论剑处理(id,参数,内容)	
  end
end

function ItemControl:华山论剑处理(id,参数,内容)	
	    
      if 参数 == 1 then
          if 天梯匹配[id].匹配 == 1 then 
		     SendMessage(UserData[id].连接id, 7,"#Y/您已经是备战状态无需重复点击")
			 return
		   end    
	   SendMessage(UserData[id].连接id, 6015,"备战开始")
	   SendMessage(UserData[id].连接id, 7,"#Y/备战中请耐心等待")
      else
	    if 内容 == "11"  then 
		 
		  SendMessage(UserData[id].连接id, 7,"#Y/备战轮空,请重新备战")
		else
         SendMessage(UserData[id].连接id, 7,"#Y/取消成功,请重新备战")
		 end
      end
      天梯匹配[id].匹配=参数
	
end

function ItemControl:AddPages(id)
	if  UserData[id].角色.道具.Pages>=4 then
		SendMessage(UserData[id].连接id, 7, "#y/你的行囊已经无法再扩充")
		return 		
	end
    if  RoleControl:扣除仙玉(UserData[id],5000*UserData[id].角色.道具.Pages,"行囊扩充") then
         UserData[id].角色.道具.Pages =UserData[id].角色.道具.Pages+1
         self:索要道具(id,"包裹",UserData[id].角色.道具.Pages)
         SendMessage(UserData[id].连接id,7,"#Y/扩充包裹栏成功")
    else
         SendMessage(UserData[id].连接id,7,"#Y/你没有足够的仙玉！")
        return 
    end
end
function ItemControl:索要道具4(id, 内容,类型,参数)--------------------------------完成--
	self.发送信息 = {}
	for n = 1, 20 do
		if UserData[id].角色.道具[内容][n] ~= nil and UserData[id].物品[UserData[id].角色.道具[内容][n]] ~= nil then
			self.发送信息[n] = UserData[id].物品[UserData[id].角色.道具[内容][n]]
			self.发送信息[n].编号 = n
		end
	end
    self.发送信息.格子=参数
	self.发送信息.状态 = 类型
	return self.发送信息
 end
function ItemControl:行囊空间互换(id, 原先格子,放置类型)----------完成--

 local 交换格子=RoleControl:取可用Pages(UserData[id],放置类型)
 	if 交换格子==0 then
   		SendMessage(UserData[id].连接id,7,"#Y/您的包裹"..放置类型.."已经无法存储更多的道具了")
  	else
        UserData[id].角色.道具["包裹"][交换格子]=UserData[id].角色.道具["包裹"][原先格子]
        UserData[id].角色.道具["包裹"][原先格子]=nil
   	end
   	    
	 self:索要道具(id,"包裹",放置类型)
	   	
end
function ItemControl:空间互换(id, 原先格子,当前类型,放置类型)----------完成--
 local 交换格子=RoleControl:取可用道具格子(UserData[id],放置类型)
 	if 交换格子==0 then
   		SendMessage(UserData[id].连接id,7,"#Y/您的"..放置类型.."已经无法存储更多的道具了")
   	-- elseif 放置类型 =="法宝" and UserData[id].物品[UserData[id].角色.道具[当前类型][原先格子]].类型~="法宝"   then
   	-- 	SendMessage(UserData[id].连接id,7,"#Y/只有法宝才能放入法宝栏")
  	else
        UserData[id].角色.道具[放置类型][交换格子]=UserData[id].角色.道具[当前类型][原先格子]
        UserData[id].角色.道具[当前类型][原先格子]=nil
   	end
   	     if 放置类型 =="法宝" or 当前类型 =="法宝"then
	        	 self:索要道具(id,放置类型)
	     end   
     	self:索要道具(id,当前类型)
 end
function ItemControl:交换格子(id, 对象, 类型, 原始)---------------------完成--
 local 内容 ={原始格子=原始,交换格子=对象,类型=类型}

 if 内容.原始格子>UserData[id].角色.道具.Pages*20 or 内容.交换格子>UserData[id].角色.道具.Pages*20  then
    return 0
  else
   if UserData[id].角色.道具[内容.类型][内容.交换格子]==nil then
     UserData[id].角色.道具[内容.类型][内容.交换格子]=UserData[id].角色.道具[内容.类型][内容.原始格子]
     UserData[id].角色.道具[内容.类型][内容.原始格子]=nil
    else
	     if 内容.原始格子==内容.交换格子 then
	       self:索要道具(id,内容.类型)
	       return 0
	     elseif UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.原始格子]]==nil or UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.交换格子]]==nil then
	     	 self:索要道具(id,内容.类型)
	     	return
	     end
	     if UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.原始格子]].名称~=UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.交换格子]].名称 or UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.交换格子]].数量==nil then
	        self.临时格子=UserData[id].角色.道具[内容.类型][内容.交换格子]
	        UserData[id].角色.道具[内容.类型][内容.交换格子]=UserData[id].角色.道具[内容.类型][内容.原始格子]
	        UserData[id].角色.道具[内容.类型][内容.原始格子]=self.临时格子
	      else
			       if (UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.原始格子]].名称 == "高级清灵仙露"
			                	or UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.原始格子]].名称 == "中级清灵仙露"
			                	or UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.原始格子]].名称 == "初级清灵仙露" ) and UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.原始格子]].灵气 ~= UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.交换格子]].灵气 then
			        	SendMessage(UserData[id].连接id, 7, "#y/只有相同灵气的清灵仙露才可以叠加")
			        	self:索要道具(id,内容.类型)
			        return
			        end
			        self.叠加数量=UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.原始格子]].数量+UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.交换格子]].数量
			        if self.叠加数量>999 then
			          self.减少数量=999-UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.交换格子]].数量
			          UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.原始格子]].数量=UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.原始格子]].数量-self.减少数量
			          UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.交换格子]].数量=999
			        else
			         UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.交换格子]].数量=UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.交换格子]].数量+UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.原始格子]].数量
			         UserData[id].角色.道具[内容.类型][内容.原始格子]=nil
			         end
	       end
     end

    SendMessage(UserData[id].连接id, 3007, {格子 =内容.原始格子,数据=UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.原始格子]]})
    SendMessage(UserData[id].连接id, 3007, {格子 =内容.交换格子,数据=UserData[id].物品[UserData[id].角色.道具[内容.类型][内容.交换格子]]})
   end
 end
function ItemControl:索要道具(id,内容,Pages)----------------------------------完成--
   if 内容 == 1 then
		内容 = "包裹"
	elseif 内容 == 2 then
		内容 = "行囊"
	elseif 内容 == 3 then
		内容 = "法宝"
	elseif 内容 == 4 then
		内容 = "锦衣"
	end
  self.发送信息={}
  	self.发送信息.类型=内容
  	self.发送信息.MaxPages=1
  	self.发送信息.Pages =1
  if 内容 == "包裹" then
	if self.发送信息.Pages > UserData[id].角色.道具.Pages then  return end
	self.发送信息.Pages=Pages or 1
	self.发送信息.MaxPages=UserData[id].角色.道具.Pages
  end
	local ItemPages=(self.发送信息.Pages-1)*20+1
	local numberPages = 0
	for n=ItemPages,20*self.发送信息.Pages do
		numberPages=numberPages+1
		if UserData[id].角色.道具[内容][n]~=nil and UserData[id].物品[UserData[id].角色.道具[内容][n]]~=nil then
		 self.发送信息[numberPages]=UserData[id].物品[UserData[id].角色.道具[内容][n]]
		end
	end

   if 内容 ~= "法宝" then
	   for n=21,26 do
	     if UserData[id].角色.装备数据[n]~=nil and UserData[id].物品[UserData[id].角色.装备数据[n]]~=nil then
	         self.发送信息[n]=UserData[id].物品[UserData[id].角色.装备数据[n]]
	       end
	     end
	     for n=40,45 do
	     if UserData[id].角色.锦衣[n]~=nil and UserData[id].物品[UserData[id].角色.锦衣[n]]~=0 then
	         self.发送信息[n]=UserData[id].物品[UserData[id].角色.锦衣[n]]
	       end
	     end
		 self.发送信息.仙玉=UserData[id].仙玉
		 self.发送信息.银两=UserData[id].角色.道具.货币.银子
		 self.发送信息.存银=UserData[id].角色.道具.货币.存银
		 self.发送信息.储备=UserData[id].角色.道具.货币.储备
		 self.发送信息.追加技能=UserData[id].角色.追加技能
		 self.发送信息.附加技能=UserData[id].角色.附加技能
		 self.发送信息.变身技能=UserData[id].角色.变身技能
		 self.发送信息.坐骑数据={}
		 self.发送信息.坐骑=UserData[id].角色.坐骑
   else  
		for n=35,38 do
			if UserData[id].角色.法宝[n]~=nil and UserData[id].物品[UserData[id].角色.法宝[n]]~=0 then
			self.发送信息[n]=UserData[id].物品[UserData[id].角色.法宝[n]]
			end
		end
		self.发送信息.剩余灵气 = UserData[id].角色.法宝灵气
   end

    local  sendMsg = (内容 == "法宝")and  3000 or 3001

    SendMessage(UserData[id].连接id,sendMsg,self.发送信息)
 
 end
function ItemControl:存钱处理(id, 参数, 内容)---------------------------完成--
	self.存入数额 = tonumber(参数)
	if self.存入数额 == nil then
		return 0
	elseif self.存入数额 <1 then
		return 0
	elseif UserData[id].角色.道具.货币.银子 < self.存入数额 then
		SendMessage(UserData[id].连接id, 7, "#y/你没有那么多的现金")

		return 0
	end
	RoleControl:添加消费日志(UserData[id],"存钱-存入数额:" .. self.存入数额)
	RoleControl:添加消费日志(UserData[id],"存钱-银子-之前金额:" .. UserData[id].角色.道具.货币.银子)
	RoleControl:添加消费日志(UserData[id],"存钱-存银-之前金额:" .. UserData[id].角色.道具.货币.存银)
	UserData[id].角色.道具.货币.存银 = UserData[id].角色.道具.货币.存银 + self.存入数额
	UserData[id].角色.道具.货币.银子 = UserData[id].角色.道具.货币.银子 - self.存入数额
	RoleControl:添加消费日志(UserData[id],"存钱-银子-存后金额:" .. UserData[id].角色.道具.货币.银子)
	RoleControl:添加消费日志(UserData[id],"存钱-存银-存后金额:" .. UserData[id].角色.道具.货币.存银)
	SendMessage(UserData[id].连接id, 7, "#y/你存入了" .. self.存入数额 .. "两银子")
		SendMessage(UserData[id].连接id, 11011, {银子=UserData[id].角色.道具.货币.银子,存银= UserData[id].角色.道具.货币.存银})
 end
function ItemControl:取钱处理(id, 参数, 内容)---------------------------完成--
	self.存入数额 = tonumber(参数)
	if self.存入数额 == nil then
		return 0
	elseif self.存入数额 <1 then
		return 0
	elseif UserData[id].角色.道具.货币.存银 < self.存入数额 then
		SendMessage(UserData[id].连接id, 7, "#y/你没有那么多的存款")

		return 0
	end
	RoleControl:添加消费日志(UserData[id],"取钱-取出数额:" .. self.存入数额)
	RoleControl:添加消费日志(UserData[id],"取钱-银子-之前金额:" .. UserData[id].角色.道具.货币.银子)
	RoleControl:添加消费日志(UserData[id],"取钱-存银-之前金额:" .. UserData[id].角色.道具.货币.存银)
	UserData[id].角色.道具.货币.存银 = UserData[id].角色.道具.货币.存银 - self.存入数额
	UserData[id].角色.道具.货币.银子 = UserData[id].角色.道具.货币.银子 + self.存入数额
	RoleControl:添加消费日志(UserData[id],"取钱-银子-取后金额:" .. UserData[id].角色.道具.货币.银子)
	RoleControl:添加消费日志(UserData[id],"取钱-存银-取后金额:" .. UserData[id].角色.道具.货币.存银)
	SendMessage(UserData[id].连接id, 7, "#y/你取出了" .. self.存入数额 .. "两银子")
	SendMessage(UserData[id].连接id, 11012, {银子=UserData[id].角色.道具.货币.银子,存银=UserData[id].角色.道具.货币.存银})
 end
function ItemControl:索要道具3(id, 内容,状态)--------------------------------完成--
	self.发送信息 = {}
	for n = 1, 20 do
		if UserData[id].角色.道具[内容][n] ~= nil and UserData[id].物品[UserData[id].角色.道具[内容][n]] ~= nil then
			self.发送信息[n] = UserData[id].物品[UserData[id].角色.道具[内容][n]]
			self.发送信息[n].编号 = n
		end
	end
    self.发送信息.体力=UserData[id].角色.当前体力
	self.发送信息.类型 = 内容
	self.发送信息.银子 = UserData[id].角色.道具.货币.银子
	self.发送信息.存银 = UserData[id].角色.道具.货币.存银
	return self.发送信息
 end
function ItemControl:丢弃道具(id, 数据)---------------------------------完成--
	self.临时数据 = 数据
	if UserData[id].物品[UserData[id].角色.道具[self.临时数据.类型][self.临时数据.格子]] ==nil then
		SendMessage(UserData[id].连接id, 7, "#y/物品不存在，请刷新界面")
		return
	elseif UserData[id].物品[UserData[id].角色.道具[self.临时数据.类型][self.临时数据.格子]].加锁  then
		SendMessage(UserData[id].连接id, 7, "#y/加锁的道具无法丢弃")
	else
	UserData[id].物品[UserData[id].角色.道具[self.临时数据.类型][self.临时数据.格子]] = nil
	UserData[id].角色.道具[self.临时数据.类型][self.临时数据.格子] = nil

	SendMessage(UserData[id].连接id, 3007, {格子 =self.临时数据.格子})
	SendMessage(UserData[id].连接id, 7, "#y/丢弃道具成功")

	end

 end
function ItemControl:点化装备处理(id,数据)------------------------------完成--
   local 套装效果 ={
    {"知己知彼","似玉生香","三味真火","日月乾坤","镇妖","尸腐毒","阎罗令","百万神兵","勾魂","判官令","雷击","魔音摄魂",
    "摄魄","紧箍咒","落岩","含情脉脉","姐妹同心","日光华","水攻","威慑","唧唧歪歪","靛沧海","烈火","催眠符","龙卷雨击",
    "巨岩破","奔雷咒","失心符","龙腾","苍茫树","泰山压顶","落魄符","龙吟","地裂火","水漫金山","定身符","五雷咒","后发制人",
    "地狱烈火","满天花雨","飞砂走石","横扫千军","落叶萧萧","尘土刃","荆棘舞","冰川怒","夺命咒","夺魄令","浪涌","裂石"},
    {"盘丝阵","定心术","极度疯狂","金刚护法","逆鳞","生命之泉","魔王回首","幽冥鬼眼","楚楚可怜","百毒不侵","变身",
    "普渡众生","炼气化神","修罗隐身","杀气诀","一苇渡江","碎星诀","明光宝烛"},
    {"巨蛙","狐狸精","黑熊精","凤凰","大海龟","老虎","僵尸","蛟龙","护卫","黑熊","牛头","雨师","树怪","花妖","马面",
    "如意仙子","赌徒","牛妖","雷鸟人","芙蓉仙子","强盗","小龙女","蝴蝶仙子","巡游天神","海毛虫","野鬼","古代瑞兽",
    "星灵仙子","大蝙蝠","狼","白熊","幽灵","山贼","虾兵","黑山老妖","鬼将","野猪","蟹将","天兵","吸血鬼","骷髅怪",
    "龟丞相","天将","净瓶女娲","羊头怪","兔子怪","地狱战神","律法女娲","蛤蟆精","蜘蛛精","风伯","灵符女娲","画魂",
    "噬天虎" ,"巴蛇"   ,"巨力神猿","幽莹娃娃","踏云兽","葫芦宝贝","修罗傀儡鬼","大力金刚","红萼仙子","修罗傀儡妖",
    "雾中仙","龙龟","金身罗汉","灵鹤","机关兽","蝎子精","藤蔓妖花","夜罗刹","机关鸟","混沌兽","曼珠沙华","炎魔神",
    "连弩车","长眉灵猴","蜃气妖"}}
    local 道具id=UserData[id].角色.道具.包裹[数据.格子]
    local 临时装备类型=UserData[id].物品[道具id].类型
     local 道具等级=UserData[id].物品[道具id].等级
    if 临时装备类型~="武器" and 临时装备类型~="衣服" and 临时装备类型~="头盔" and 临时装备类型~="项链" and 临时装备类型~="腰带" and 临时装备类型~="鞋子"  then
         SendMessage(UserData[id].连接id,7,"#Y/只有装备才可点化")
         return 0
    elseif UserData[id].物品[道具id].鉴定==false then
          SendMessage(UserData[id].连接id,7,"#Y/未鉴定的装备不可以点化")
         return 0
    elseif 道具等级>=100 then
       SendMessage(UserData[id].连接id,7,"#Y/在我这里只能点化100级以下的装备")
       return 0
    end
   if 银子检查(id,道具等级*5000)==false then
      SendMessage(UserData[id].连接id,7,"#Y/本次操作需要消耗"..(道具等级*5000).."两银子")
      return 0
    end
     RoleControl:扣除银子(UserData[id],道具等级*5000,"点化装备")
   if math.random(100)<=70 then
     local 点化类型=math.random(2)
        if 点化类型==1 then
        UserData[id].物品[道具id].套装={类型="追加法术",名称=套装效果[点化类型][math.random(1,#套装效果[点化类型])]}
        elseif 点化类型==2 then
        UserData[id].物品[道具id].套装={类型="附加状态",名称=套装效果[点化类型][math.random(1,#套装效果[点化类型])]}
        elseif 点化类型==3 then
        UserData[id].物品[道具id].套装={类型="变身术",名称=套装效果[点化类型][math.random(1,#套装效果[点化类型])]}
        end
        SendMessage(UserData[id].连接id,7,"#y/点化装备成功！")
        SendMessage(UserData[id].连接id,3006,"66")
    else
      SendMessage(UserData[id].连接id,7,"#y/点化装备失败")
    end
 end
function ItemControl:索要打造道具(id, 内容, 类型)-----------------------完成--
	self.发送信息 = {}

	for n = 1, 20 do
		if UserData[id].角色.道具[内容][n] ~= nil and UserData[id].物品[UserData[id].角色.道具[内容][n]] ~= nil then
			self.发送信息[n] = UserData[id].物品[UserData[id].角色.道具[内容][n]]
		end
	end

	self.发送信息.类型 = 内容
	self.发送信息.状态 = 类型
	self.发送信息.银两 = UserData[id].角色.道具.货币.银子
	self.发送信息.体力 = UserData[id].角色.当前体力
	SendMessage(UserData[id].连接id, 3005, self.发送信息)
 end

function ItemControl:取等级要求(id,等级,特效)---------------------------完成--
  local 无级别特效=false
  local 简易特效=false
  local 超级简易特效=false
  if 特效~=nil then
   for n=1,#特效 do
       if 特效[n]=="无级别限制" then
        无级别特效=true
        elseif 特效[n]=="超级简易" then
        超级简易特效=true
        elseif 特效[n]=="简易" then
          简易特效=true
       end
    end
  end
  if 无级别特效 then
   return true
  elseif 超级简易特效 and UserData[id].角色.等级+20>=等级 then
   return true
  elseif 简易特效 and UserData[id].角色.等级+5>=等级 then
   return true
  elseif UserData[id].角色.等级>=等级 then
   return true
  else
  return false
  end
 end

function ItemControl:索要单个道具(id, 格子,类型)--------------------------------完成--
	self.发送信息 = {}
	
	if UserData[id].角色.道具["包裹"][格子] == nil or UserData[id].物品[UserData[id].角色.道具["包裹"][格子]] == nil then
		SendMessage(UserData[id].连接id,7,"#Y/物品不存在，请重新选择")
		 return 
	end

	self.发送信息.格子=格子
	self.发送信息.仙玉 =UserData[id].仙玉
	 self.发送信息.物品 =UserData[id].物品[UserData[id].角色.道具["包裹"][格子]]
	if 类型 =="武器"  then
		  if UserData[id].物品[UserData[id].角色.道具["包裹"][格子]].类型~="武器" then
			SendMessage(UserData[id].连接id,7,"#Y/只能选择武器，请重新选择")
			 return
		  end 
		  SendMessage(UserData[id].连接id,2036,self.发送信息)
		  
	elseif 类型 =="装备" then
		  if UserData[id].物品[UserData[id].角色.道具["包裹"][格子]].三围属性==nil then
			SendMessage(UserData[id].连接id,7,"#Y/只能选择双加的装备，请重新选择")
			 return
		  end 
		  SendMessage(UserData[id].连接id,2037,self.发送信息)
	end
 end

function ItemControl:索要道具1(id, 内容,状态)--------------------------------完成--
	self.发送信息 = {}
	for n = 1, 20 do
		if UserData[id].角色.道具[内容][n] ~= nil and UserData[id].物品[UserData[id].角色.道具[内容][n]] ~= nil then
			self.发送信息[n] = UserData[id].物品[UserData[id].角色.道具[内容][n]]
			self.发送信息[n].编号 = n
		end
	end
	self.发送信息.类型 = 内容
	self.发送信息.银两 = UserData[id].角色.道具.货币.银子
	self.发送信息.存银 = UserData[id].角色.道具.货币.存银
	self.发送信息.状态 =状态
	return self.发送信息
 end
function ItemControl:创建帮派(id, 内容)---------------------------------完成--
	if 取帮派数量() > 50 then
		SendMessage(UserData[id].连接id, 7, "#y/帮派数量已达上限，无法再创建新的帮派")

		return 0
	elseif UserData[id].角色.帮派 ~= nil then
		SendMessage(UserData[id].连接id, 7, "#y/请先脱离原有的帮派")

		return 0
	end

	if RoleControl:扣除仙玉(UserData[id],2000,"创建帮派")==false then
		SendMessage(UserData[id].连接id, 7, "#y/你没有那么多的仙玉")

		return 0

  else
	帮派数据.编号 = 帮派数据.编号 + 1
	self.临时编号 = 帮派数据.编号
	UserData[id].角色.帮派 = self.临时编号
	帮派数据[self.临时编号] = {
		繁荣 = 500,
		左护法 = 0,
		安定 = 800,
		朱雀堂主 = 0,
		药房 = 1,
		炼化等级 = 0,
		人气 = 500,
		金库 = 1,
		厢房 = 1,
		养生之道 = 0,
		药品增加量 = 0,
		修改宗旨 = false,
		学费指数 = 0,
		右护法 = 0,
		白虎堂主 = 0,
		守护兽等级 = 1,
		资金 = 5000000,
		健身术 = 0,
		青龙堂主 = 0,
		宗旨 = "",
		资材 = 500,
		仓库 = 1,
		书院 = 1,
		兽室 = 1,
		规模 = 1,
		技能上限 = 150,
		副帮主 = 0,
		物价指数 = 0,
		玄武堂主 = 0,
		修理指数 = 0,
		强身术 = 0,
		淬炼等级 = 0,
		行动力 = 0,
		创始人 = {
			id = id,
			名称 = UserData[id].角色.名称
		},
		现任帮主 = {
			id = id,
			名称 = UserData[id].角色.名称
		},
		编号 = self.临时编号,
		帮主 = id,
		临时强化效果 = {
			灵力 = 0,
			伤害 = 0,
			速度 = 0,
			气血 = 0,
			防御 = 0
		},
		成员名单 = {
			[id] = {
				职务 = "帮主",
				id = id,
				名称 = UserData[id].角色.名称,
				帮贡 = {
					当前 = 100,
					获得 = 100
				},
				入帮时间 = os.time(),
				离线时间 = os.time()
			}
		},
		申请名单 = {},
		同盟 = {编号 = 0,名称 = "无"},
		敌对 = {编号 = 0,名称 = "无"
		},
		掌控区域 = {编号 = 0,名称 = "无"
		},
		当前内政 = {名称 = "无",要求进度 = 0,总进度 = 0
		},
		名称 = 内容,
		研究数据 = {类型 = "无",当前经验 = 0,升级经验 = 0
		}
	}
	RoleControl:添加称谓(UserData[id], 内容.."的帮主")

	
     SendMessage(UserData[id].连接id, 7, "#y/创建帮派成功！")
	广播消息(9, "#xt/#g/" .. UserData[id].角色.名称 .. "#w/在长安城帮派总管处成功创建了新帮派#r/" .. 内容)
	RoleControl:添加系统消息(UserData[id], "#h/你成功创建了一个帮派。")

	UserData[id].角色.帮派 = self.临时编号
	end
 end
function ItemControl:卸下bb装备处理(id, 格子, 类型,编号)---------------完成--
 local 数据={格子=格子,类型=编号+0}
 self.临时id1=UserData[id].召唤兽.数据[数据.类型].装备[数据.格子]
 if UserData[id].物品[self.临时id1]==nil or UserData[id].物品[self.临时id1]==0 then
   SendMessage(UserData[id].连接id,7,"#y/你似乎并没有这样的道具")
   return 0
  end

 self.可用格子=RoleControl:取可用道具格子(UserData[id],"包裹")
 if self.可用格子==0 then
   SendMessage(UserData[id].连接id,7,"#y/你已经无法携带更多的道具了")
   return 0

   end

 UserData[id].角色.道具["包裹"][self.可用格子]=self.临时id1
 UserData[id].召唤兽.数据[数据.类型].装备[数据.格子]=nil
 UserData[id].召唤兽:刷新装备属性(数据.类型,id)
 SendMessage(UserData[id].连接id,2005,UserData[id].召唤兽:取头像数据())
 SendMessage(UserData[id].连接id,3040,"")

 SendMessage(UserData[id].连接id,3006,"66")
 end
function ItemControl:佩戴bb装备处理(id,格子,类型,编号)-------------------完成--
 编号= tonumber(编号)
  local 数据={格子=格子,类型=类型}
 self.临时id1=UserData[id].角色.道具[数据.类型][数据.格子]
 if UserData[id].物品[self.临时id1]==nil or UserData[id].物品[self.临时id1]==0 then

   SendMessage(UserData[id].连接id,7,"#y/你似乎并没有这样的道具")
   return 0
  elseif  UserData[id].召唤兽.数据[编号].等级<UserData[id].物品[self.临时id1].等级 then
    SendMessage(UserData[id].连接id,7,"#y/您的召唤兽当前等级无法佩戴此装备")
    SendMessage(UserData[id].连接id,3006,"66")
   return 0
   end

  --寻找格子  1=项圈 2=铠甲 3=项圈

  if UserData[id].物品[self.临时id1].种类=="项圈" then

   self.格子类型=2

  elseif UserData[id].物品[self.临时id1].种类=="铠甲" then

   self.格子类型=3

  else
    self.格子类型=1

    end
 self.已佩戴id=nil
 if UserData[id].召唤兽.数据[编号].装备[self.格子类型]~=nil then

   self.已佩戴id=UserData[id].召唤兽.数据[编号].装备[self.格子类型]


   end
 UserData[id].召唤兽.数据[编号].装备[self.格子类型]=self.临时id1
 UserData[id].角色.道具[数据.类型][数据.格子]=self.已佩戴id
 SendMessage(UserData[id].连接id,7,"#y/召唤兽佩戴装备成功")
 UserData[id].召唤兽:刷新装备属性(编号,id)
 SendMessage(UserData[id].连接id,2005,UserData[id].召唤兽:取头像数据())
 SendMessage(UserData[id].连接id,3006,"66")

 end
function ItemControl:索要道具2(id, 内容)---------------------------------完成--
	self.发送信息 = {}
  local fjf =0
	for n = 1, 20 do
		if UserData[id].角色.道具[内容][n] ~= nil and UserData[id].物品[UserData[id].角色.道具[内容][n]] ~= nil then
			self.发送信息[n] = UserData[id].物品[UserData[id].角色.道具[内容][n]]
			if UserData[id].物品[UserData[id].角色.道具[内容][n]].名称=="分解符" then
			    fjf = fjf + (UserData[id].物品[UserData[id].角色.道具[内容][n]].数量 or 1)
			end
		end
	end

	self.发送信息.类型 = 内容
	self.发送信息.银两 = UserData[id].角色.道具.货币.银子
	self.发送信息.存银 = UserData[id].角色.道具.货币.存银
    self.发送信息.体力 = UserData[id].角色.当前体力
    self.发送信息.分解符 = fjf
	return self.发送信息
 end

function ItemControl:召唤兽快捷加血(id)
	self.召唤兽id = UserData[id].召唤兽.数据.参战

	if UserData[id].战斗 ~= 0 then
		SendMessage(UserData[id].连接id, 7, "#y/战斗中无法使用快捷加血功能")

		return 0
	elseif self.召唤兽id == 0 then
		SendMessage(UserData[id].连接id, 7, "#y/请先设置一只召唤兽为参战状态")

		return 0
	elseif UserData[id].召唤兽.数据[self.召唤兽id].当前气血 == UserData[id].召唤兽.数据[self.召唤兽id].气血上限 then
		SendMessage(UserData[id].连接id, 7, "#y/您的召唤兽气血值是满的")

		return 0
	else
		self.气血差 = UserData[id].召唤兽.数据[self.召唤兽id].气血上限 - UserData[id].召唤兽.数据[self.召唤兽id].当前气血
		self.扣减数量 = 0

		for n = 1, 80 do
			self.临时id1 = UserData[id].角色.道具.包裹[n]

			if self.临时id1 ~= nil and UserData[id].物品[self.临时id1].名称 == "包子" and self.气血差 > 0 then
				for i = 1, UserData[id].物品[self.临时id1].数量 do
					if self.气血差 > 0 then
						UserData[id].物品[self.临时id1].数量 = UserData[id].物品[self.临时id1].数量 - 1
						self.气血差 = self.气血差 - 100
						self.扣减数量 = self.扣减数量 + 1

						if self.气血差 < 0 then
							self.气血差 = 0
						end
					end
				end

				if UserData[id].物品[self.临时id1].数量 <1 then
					UserData[id].物品[self.临时id1] = nil
					UserData[id].角色.道具.包裹[n] = nil
				end
			end
		end

		if self.扣减数量 ~= 0 then
			UserData[id].召唤兽.数据[self.召唤兽id].当前气血 = UserData[id].召唤兽.数据[self.召唤兽id].气血上限 - self.气血差

			SendMessage(UserData[id].连接id, 7, "#y/您消耗了" .. self.扣减数量 .. "个包子")
			SendMessage(UserData[id].连接id, 2005, UserData[id].召唤兽:取头像数据())
		else
			SendMessage(UserData[id].连接id, 7, "#y/您身上没有包子，无法使用快捷加血功能")
		end
	end
 end
function ItemControl:召唤兽快捷加蓝(id)
	self.召唤兽id = UserData[id].召唤兽.数据.参战

	if UserData[id].战斗 ~= 0 then
		SendMessage(UserData[id].连接id, 7, "#y/战斗中无法使用快捷加蓝功能")

		return 0
	elseif self.召唤兽id == 0 then
		SendMessage(UserData[id].连接id, 7, "#y/请先设置一只召唤兽为参战状态")

		return 0
	elseif UserData[id].召唤兽.数据[self.召唤兽id].当前魔法 == UserData[id].召唤兽.数据[self.召唤兽id].魔法上限 then
		SendMessage(UserData[id].连接id, 7, "#y/您的召唤兽魔法值是满的")

		return 0
	else
		self.气血差 = UserData[id].召唤兽.数据[self.召唤兽id].魔法上限 - UserData[id].召唤兽.数据[self.召唤兽id].当前魔法
		self.扣减数量 = 0

		for n = 1, 80 do
			self.临时id1 = UserData[id].角色.道具.包裹[n]

			if self.临时id1 ~= nil and UserData[id].物品[self.临时id1].名称 == "鬼切草" and self.气血差 > 0 then
				for i = 1, UserData[id].物品[self.临时id1].数量 do
					if self.气血差 > 0 then
						UserData[id].物品[self.临时id1].数量 = UserData[id].物品[self.临时id1].数量 - 1
						self.气血差 = self.气血差 - 40
						self.扣减数量 = self.扣减数量 + 1

						if self.气血差 < 0 then
							self.气血差 = 0
						end
					end
				end

				if UserData[id].物品[self.临时id1].数量 <1 then
					UserData[id].物品[self.临时id1] = nil
					UserData[id].角色.道具.包裹[n] = nil
				end
			end
		end

		if self.扣减数量 ~= 0 then
			UserData[id].召唤兽.数据[self.召唤兽id].当前魔法 = UserData[id].召唤兽.数据[self.召唤兽id].魔法上限 - self.气血差

			SendMessage(UserData[id].连接id, 7, "#y/您消耗了" .. self.扣减数量 .. "个鬼切草")
			SendMessage(UserData[id].连接id, 2005, UserData[id].召唤兽:取头像数据())
		else
			SendMessage(UserData[id].连接id, 7, "#y/您身上没有鬼切草，无法使用快捷加蓝功能")
		end
	end
 end
function ItemControl:角色快捷加血(id)
	if UserData[id].战斗 ~= 0 then
		SendMessage(UserData[id].连接id, 7, "#y/战斗中无法使用快捷加血功能")

		return 0
	elseif UserData[id].角色.当前气血 == UserData[id].角色.气血上限 then
		SendMessage(UserData[id].连接id, 7, "#y/您的气血值是满的")

		return 0
	else
		self.气血差 = UserData[id].角色.气血上限 - UserData[id].角色.当前气血
		self.扣减数量 = 0

		for n = 1, 80 do
			self.临时id1 = UserData[id].角色.道具.包裹[n]

			if self.临时id1 ~= nil and UserData[id].物品[self.临时id1].名称 == "包子" and self.气血差 > 0 then
				for i = 1, UserData[id].物品[self.临时id1].数量 do
					if self.气血差 > 0 then
						UserData[id].物品[self.临时id1].数量 = UserData[id].物品[self.临时id1].数量 - 1
						self.气血差 = self.气血差 - 100
						self.扣减数量 = self.扣减数量 + 1

						if self.气血差 < 0 then
							self.气血差 = 0
						end
					end
				end

				if UserData[id].物品[self.临时id1].数量 <1 then
					UserData[id].物品[self.临时id1] = nil
					UserData[id].角色.道具.包裹[n] = nil
				end
			end
		end

		if self.扣减数量 ~= 0 then
			UserData[id].角色.当前气血 = UserData[id].角色.气血上限 - self.气血差

			SendMessage(UserData[id].连接id, 7, "#y/您消耗了" .. self.扣减数量 .. "个包子")
			SendMessage(UserData[id].连接id, 1003, RoleControl:获取角色气血数据(UserData[id]))
			SendMessage(UserData[id].连接id, 2005, UserData[id].召唤兽:取头像数据())
		else
			SendMessage(UserData[id].连接id, 7, "#y/您身上没有包子，无法使用快捷加血功能")
		end
	end
end
function ItemControl:角色快捷加蓝(id)
	if UserData[id].战斗 ~= 0 then
		SendMessage(UserData[id].连接id, 7, "#y/战斗中无法使用快捷加蓝功能")

		return 0
	elseif UserData[id].角色.当前魔法 == UserData[id].角色.魔法上限 then
		SendMessage(UserData[id].连接id, 7, "#y/您的魔法值是满的")

		return 0
	else
		self.气血差 = UserData[id].角色.魔法上限 - UserData[id].角色.当前魔法
		self.扣减数量 = 0

		for n = 1, 80 do
			self.临时id1 = UserData[id].角色.道具.包裹[n]

			if self.临时id1 ~= nil and UserData[id].物品[self.临时id1].名称 == "鬼切草" and self.气血差 > 0 then
				for i = 1, UserData[id].物品[self.临时id1].数量 do
					if self.气血差 > 0 then
						UserData[id].物品[self.临时id1].数量 = UserData[id].物品[self.临时id1].数量 - 1
						self.气血差 = self.气血差 - 40
						self.扣减数量 = self.扣减数量 + 1

						if self.气血差 < 0 then
							self.气血差 = 0
						end
					end
				end

				if UserData[id].物品[self.临时id1].数量 <1 then
					UserData[id].物品[self.临时id1] = nil
					UserData[id].角色.道具.包裹[n] = nil
				end
			end
		end

		if self.扣减数量 ~= 0 then
			UserData[id].角色.当前魔法 = UserData[id].角色.魔法上限 - self.气血差

			SendMessage(UserData[id].连接id, 7, "#y/您消耗了" .. self.扣减数量 .. "个鬼切草")
			SendMessage(UserData[id].连接id, 2005, UserData[id].召唤兽:取头像数据())
		else
			SendMessage(UserData[id].连接id, 7, "#y/您身上没有鬼切草，无法使用快捷加蓝功能")
		end
	end
end
function ItemControl:设置内政(id, 类型)
	self.帮派编号 = UserData[id].角色.帮派

	if 取帮派踢人权限(id, UserData[id].角色.帮派) == false then
		SendMessage(UserData[id].连接id, 7, "#y/你没有此种操作的权限")

		return 0
	elseif 帮派数据[self.帮派编号].当前内政.名称 ~= "无" then
		SendMessage(UserData[id].连接id, 7, "#y/帮派不允许同时进行两种内政")

		return 0
	end

	if 类型 == "聚义厅" then
		self.满足数量 = 0
		self.查找类型 = {"书院","仓库","金库","厢房","兽室"}

		for n, v in pairs(self.查找类型) do
			if 帮派数据[self.帮派编号][self.查找类型[n]] >= 帮派数据[self.帮派编号].规模 * 4 then
				self.满足数量 = self.满足数量 + 1
			end
		end

		if self.满足数量 < 2 then
			SendMessage(UserData[id].连接id, 7, "#y/升级聚义厅需要其它两个建筑等级不低于" .. 帮派数据[self.帮派编号].规模 * 4 .. "级")

			return 0
		end
	elseif 帮派数据[self.帮派编号][类型] >= 帮派数据[self.帮派编号].规模 * 4 then
		SendMessage(UserData[id].连接id, 7, "#y/该建筑等级已达上限！")

		return 0
	end

	if 类型 == "金库" then
		self.所需进度 = 500
		self.损耗资金 = 5000000
		self.损耗繁荣 = 300
		self.损耗人气 = 200
	elseif 类型 == "书院" then
		self.所需进度 = 500
		self.损耗资金 = 5000000
		self.损耗繁荣 = 300
		self.损耗人气 = 200
	elseif 类型 == "聚义厅" then
		self.所需进度 = 帮派数据[self.帮派编号].规模 * 500 + 500
		self.损耗资金 = 帮派数据[self.帮派编号].规模 * 5000000 + 5000000
		self.损耗繁荣 = 500
		self.损耗人气 = 500
	else
		self.所需进度 = 200
		self.损耗资金 = 3000000
		self.损耗繁荣 = 100
		self.损耗人气 = 100
	end

	if 帮派数据[self.帮派编号].人气 < self.损耗人气 then
		SendMessage(UserData[id].连接id, 7, "#y/你帮派的人气不够")

		return 0
	elseif 帮派数据[self.帮派编号].繁荣 < self.损耗繁荣 then
		SendMessage(UserData[id].连接id, 7, "#y/你帮派的繁荣不够")

		return 0
	elseif 帮派数据[self.帮派编号].资金 < self.损耗资金 + 帮派数据[self.帮派编号].规模 * 3000000 then
		SendMessage(UserData[id].连接id, 7, "#y/你帮派的资金不够")

		return 0
	end

	帮派数据[self.帮派编号].人气 = 帮派数据[self.帮派编号].人气 - self.损耗人气
	帮派数据[self.帮派编号].繁荣 = 帮派数据[self.帮派编号].繁荣 - self.损耗繁荣
	帮派数据[self.帮派编号].资金 = 帮派数据[self.帮派编号].资金 - self.损耗资金
	帮派数据[self.帮派编号].当前内政.名称 = 类型
	帮派数据[self.帮派编号].当前内政.要求进度 = self.所需进度
	帮派数据[self.帮派编号].当前内政.总进度 = 0

	SendMessage(UserData[id].连接id, 7, "#y/设置帮派内政成功！")
	广播帮派消息(UserData[id].角色.帮派, "#bp/#r/" .. UserData[id].角色.名称 .. "#y/设置了新的内政建设")
end
function ItemControl:取职务数量(编号, 职务)
	self.找到数量 = 0

	for n, v in pairs(帮派数据[编号].成员名单) do
		if 帮派数据[编号].成员名单[n].职务 == 职务 then
			self.找到数量 = self.找到数量 + 1
		end
	end

	return self.找到数量
end
function ItemControl:职务变更(id, 职务)
	self.操作id = UserData[id].帮派操作id
	UserData[id].帮派操作id = nil

	if self.操作id == nil then
		SendMessage(UserData[id].连接id, 7, "#y/请先选择一位成员")

		return 0
	end

	self.帮派编号 = UserData[id].角色.帮派

	if 帮派数据[self.帮派编号] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/该帮派不存在")

		return 0
	end

	if id == self.操作id then
		if 帮派数据[self.帮派编号].成员名单[self.操作id].职务 == "帮主" or 帮派数据[self.帮派编号].成员名单[self.操作id].职务 == "帮众" then
			SendMessage(UserData[id].连接id, 7, "#y/对方无法进行这样的职务变动")

			return 0
		elseif 职务 ~= "帮众" then
			SendMessage(UserData[id].连接id, 7, "#y/您只能将自己任命为帮众！")

			return 0
		end
			

	    RoleControl:回收称谓(UserData[self.操作id],帮派数据[self.帮派编号].名称.."的"..帮派数据[self.帮派编号].成员名单[self.操作id].职务)

		帮派数据[self.帮派编号].成员名单[self.操作id].职务 = "帮众"
		RoleControl:添加称谓(UserData[self.操作id],帮派数据[self.帮派编号].名称.."的"..帮派数据[self.帮派编号].成员名单[self.操作id].职务)

		广播帮派消息(self.帮派编号, "#bp/#r/" .. UserData[id].角色.名称 .. "#y/将#r/" .. 帮派数据[self.帮派编号].成员名单[self.操作id].名称 .. "#y/任命为" .. 职务)
		SendMessage(UserData[self.操作id].连接id, 7, "#y/你在帮中的职务发生了变化")

		return 0
	elseif 职务 == "副帮主" then
		if 帮派数据[self.帮派编号].成员名单[id].职务 ~= "帮主" then
			SendMessage(UserData[id].连接id, 7, "#y/你没有权限进行此种操作！")

			return 0
		elseif self:取职务数量(self.帮派编号, 职务) >= 1 then
			SendMessage(UserData[id].连接id, 7, "#y/该担任该职务的人数已达上限")

			return 0
		else
	   RoleControl:回收称谓(UserData[self.操作id],帮派数据[self.帮派编号].名称.."的"..帮派数据[self.帮派编号].成员名单[self.操作id].职务)

		帮派数据[self.帮派编号].成员名单[self.操作id].职务 = 职务
		RoleControl:添加称谓(UserData[self.操作id],帮派数据[self.帮派编号].名称.."的"..帮派数据[self.帮派编号].成员名单[self.操作id].职务)

			
			广播帮派消息(self.帮派编号, "#bp/#r/" .. UserData[id].角色.名称 .. "#y/将#r/" .. 帮派数据[self.帮派编号].成员名单[self.操作id].名称 .. "#y/任命为" .. 职务)

			if UserData[self.操作id] ~= nil then
				SendMessage(UserData[self.操作id].连接id, 7, "#y/你在帮中的职务发生了变化")
			end
		end
	elseif 职务 == "左护法" or 职务 == "右护法" then
		if 取帮派踢人权限(id, self.帮派编号) == false then
			SendMessage(UserData[id].连接id, 7, "#y/你没有权限进行此种操作！")

			return 0
		elseif self:取职务数量(self.帮派编号, 职务) >= 1 then
			SendMessage(UserData[id].连接id, 7, "#y/该担任该职务的人数已达上限")

			return 0
		else
				   RoleControl:回收称谓(UserData[self.操作id],帮派数据[self.帮派编号].名称.."的"..帮派数据[self.帮派编号].成员名单[self.操作id].职务)

		帮派数据[self.帮派编号].成员名单[self.操作id].职务 = 职务
		RoleControl:添加称谓(UserData[self.操作id],帮派数据[self.帮派编号].名称.."的"..帮派数据[self.帮派编号].成员名单[self.操作id].职务)

			广播帮派消息(self.帮派编号, "#bp/#r/" .. UserData[id].角色.名称 .. "#y/将#r/" .. 帮派数据[self.帮派编号].成员名单[self.操作id].名称 .. "#y/任命为" .. 职务)

			if UserData[self.操作id] ~= nil then
				SendMessage(UserData[self.操作id].连接id, 7, "#y/你在帮中的职务发生了变化")
			end
		end
	elseif 职务 == "长老" then
		if 取帮派踢人权限(id, self.帮派编号) == false then
			SendMessage(UserData[id].连接id, 7, "#y/你没有权限进行此种操作！")

			return 0
		elseif self:取职务数量(self.帮派编号, 职务) >= 4 then
			SendMessage(UserData[id].连接id, 7, "#y/该担任该职务的人数已达上限")

			return 0
		else
				   RoleControl:回收称谓(UserData[self.操作id],帮派数据[self.帮派编号].名称.."的"..帮派数据[self.帮派编号].成员名单[self.操作id].职务)

		帮派数据[self.帮派编号].成员名单[self.操作id].职务 = 职务
		RoleControl:添加称谓(UserData[self.操作id],帮派数据[self.帮派编号].名称.."的"..帮派数据[self.帮派编号].成员名单[self.操作id].职务)

			广播帮派消息(self.帮派编号, "#bp/#r/" .. UserData[id].角色.名称 .. "#y/将#r/" .. 帮派数据[self.帮派编号].成员名单[self.操作id].名称 .. "#y/任命为" .. 职务)

			if UserData[self.操作id] ~= nil then
				SendMessage(UserData[self.操作id].连接id, 7, "#y/你在帮中的职务发生了变化")
			end
		end
	elseif 职务 == "帮众" then
		if 取帮派踢人权限(id, self.帮派编号) == false then
			SendMessage(UserData[id].连接id, 7, "#y/你没有权限进行此种操作！")

			return 0
		elseif 帮派数据[self.帮派编号].成员名单[self.操作id].职务 == "帮主" then
			SendMessage(UserData[id].连接id, 7, "#y/你没有权限进行此种操作！")

			return 0
		elseif 帮派数据[self.帮派编号].成员名单[self.操作id].职务 == "副帮主" and 帮派数据[self.帮派编号].成员名单[id].职务 ~= "帮主" then
			SendMessage(UserData[id].连接id, 7, "#y/你没有权限进行此种操作！")

			return 0
		else
				   RoleControl:回收称谓(UserData[self.操作id],帮派数据[self.帮派编号].名称.."的"..帮派数据[self.帮派编号].成员名单[self.操作id].职务)

		帮派数据[self.帮派编号].成员名单[self.操作id].职务 = 职务
		RoleControl:添加称谓(UserData[self.操作id],帮派数据[self.帮派编号].名称.."的"..帮派数据[self.帮派编号].成员名单[self.操作id].职务)

			广播帮派消息(self.帮派编号, "#bp/#r/" .. UserData[id].角色.名称 .. "#y/将#r/" .. 帮派数据[self.帮派编号].成员名单[self.操作id].名称 .. "#y/任命为" .. 职务)

			if UserData[self.操作id] ~= nil then
				SendMessage(UserData[self.操作id].连接id, 7, "#y/你在帮中的职务发生了变化")
			end
		end
	end
end
function ItemControl:取帮派人数(编号)
	self.临时人数 = 0

	for n, v in pairs(帮派数据[编号].成员名单) do
		if 帮派数据[编号].成员名单[n] ~= nil then
			self.临时人数 = self.临时人数 + 1
		end
	end

	return self.临时人数
end
function ItemControl:获取帮派信息(id, 编号)
	if 帮派数据[编号] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/帮派数据异常")

		return 0
	end

	self.发送信息 = table.copy(帮派数据[编号])
	self.发送信息.当前人数 = 0

	for n, v in pairs(self.发送信息.成员名单) do
		self.发送信息.当前人数 = self.发送信息.当前人数 + 1

		if UserData[n] ~= nil then
			self.发送信息.成员名单[n].在线 = true
		else
			self.发送信息.成员名单[n].在线 = false
		end
	end

	self.发送信息.人数上限 = 20 + self.发送信息.厢房 * 20

	SendMessage(UserData[id].连接id, 20020, self.发送信息)
end

function ItemControl:获取帮派信息2(id, 编号)
	self.发送信息 = {
		名单 = 帮派数据[编号].成员名单
	}

	SendMessage(UserData[id].连接id, 20024, self.发送信息)
end
function ItemControl:获取帮派信息1(id, 编号)
	if 帮派数据[编号] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/帮派数据异常")

		return 0
	end

	self.发送信息 = table.copy(帮派数据[编号])
	self.发送信息.当前人数 = 0

	for n, v in pairs(self.发送信息.成员名单) do
		self.发送信息.当前人数 = self.发送信息.当前人数 + 1

		if UserData[n] ~= nil then
			self.发送信息.成员名单[n].在线 = true
		else
			self.发送信息.成员名单[n].在线 = false
		end
	end

	self.发送信息.成员名单 = nil
	self.发送信息.申请名单 = nil
	self.发送信息.人数上限 = 20 + self.发送信息.厢房 * 20

	SendMessage(UserData[id].连接id, 20026, self.发送信息)
end
function ItemControl:幻化装备(id)
	local 类型 = UserData[id].道具id.类型
	local 格子 = UserData[id].道具id.格子
	self.道具id = UserData[id].角色.道具[类型][格子]

	if UserData[id].物品[self.道具id] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/你并没有这样的道具")

		return false
	elseif UserData[id].物品[self.道具id].名称 ~= "无名秘籍" then
		SendMessage(UserData[id].连接id, 7, "#y/你并没有这样的道具")

		return false
	elseif self:幻化检查(id, self.道具id) then
		self.消耗数据 = self:幻化消耗(self.道具id)

		if 银子检查(id, self.消耗数据.银子) == false then
			SendMessage(UserData[id].连接id, 7, "#y/你没那么多的银子")

			return false
		elseif UserData[id].角色.当前体力 < self.消耗数据.体力 then
			SendMessage(UserData[id].连接id, 7, "#y/你没那么多的体力")

			return false
		else
			RoleControl:扣除银子(UserData[id],self.消耗数据.银子, 21)

			
			UserData[id].角色.当前体力 = UserData[id].角色.当前体力 - self.消耗数据.体力

			if math.random(100) <= self.消耗数据.失败 then
				SendMessage(UserData[id].连接id, 7, "#y/很遗憾，本次幻化失败了")
			else
				self.幻化id = UserData[id].角色.装备数据[self.查找部位]
				UserData[id].物品[self.幻化id].幻化属性 = {
					附加 = {}
				}
				self.临时属性 = 幻化属性值[math.random( #幻化属性值)]
				self.临时数值 = math.floor(幻化属性基数[self.临时属性] * UserData[id].物品[self.道具id].等级)
				self.临时下限 = math.floor(self.临时数值 * 0.5)

				if self.临时下限 < 1 then
					self.临时下限 = 1
				end

				UserData[id].物品[self.幻化id].幻化属性.基础 = {
					强化 = 0,
					类型 = self.临时属性,
					数值 = math.random(self.临时下限, self.临时数值)
				}

				for n = 1, self.消耗数据.附加 do
					self.临时属性 = 幻化属性值[math.random( #幻化属性值)]
					self.临时数值 = math.floor(幻化属性基数[self.临时属性] * UserData[id].物品[self.道具id].等级)
					self.临时下限 = math.floor(self.临时数值 * 0.5)

					if self.临时下限 < 1 then
						self.临时下限 = 1
					end

					UserData[id].物品[self.幻化id].幻化属性.附加[#UserData[id].物品[self.幻化id].幻化属性.附加 + 1] = {
						强化 = 0,
						类型 = self.临时属性,
						数值 = math.random(self.临时下限, self.临时数值)
					}
				end

				RoleControl:刷新装备属性(UserData[id])
				SendMessage(UserData[id].连接id, 7, "#y/装备幻化成功！")
			end
		end

		UserData[id].物品[self.道具id] = 0
		UserData[id].角色.道具[类型][格子] = nil

		SendMessage(UserData[id].连接id, 3006, "66")
		
	end
end
function ItemControl:幻化消耗(道具id)
	if UserData[id].物品[道具id].等级 <= 5 then
		return {
			银子 = 3000000,
			附加 = 1,
			失败 = 0,
			体力 = 150
		}
	elseif UserData[id].物品[道具id].等级 <= 10 then
		return {
			银子 = 10000000,
			附加 = 2,
			失败 = 10,
			体力 = 250
		}
	elseif UserData[id].物品[道具id].等级 <= 15 then
		return {
			银子 = 50000000,
			附加 = 3,
			失败 = 20,
			体力 = 300
		}
	elseif UserData[id].物品[道具id].等级 <= 20 then
		return {
			银子 = 100000000,
			附加 = 4,
			失败 = 30,
			体力 = 500
		}
	end
end

function ItemControl:幻化检查(id, 道具id)
	self.部位名称 = UserData[id].物品[道具id].部位
	self.查找部位 = 0

	if self.部位名称 == "武器" then
		self.查找部位 = 21
	elseif self.部位名称 == "头盔" then
		self.查找部位 = 22
	elseif self.部位名称 == "腰带" then
		self.查找部位 = 23
	elseif self.部位名称 == "项链" then
		self.查找部位 = 24
	elseif self.部位名称 == "鞋子" then
		self.查找部位 = 25
	elseif self.部位名称 == "衣服" then
		self.查找部位 = 26
	end

	if UserData[id].角色.装备数据[self.查找部位] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/请先佩戴要幻化的装备")

		return false
	else
		return true
	end
end

function ItemControl:添加装备临时效果(RoleItem,Type,Value)
	if RoleItem.临时效果 ==nil then 
		RoleItem.临时效果={[1]= {类型 = Type,数值 =Value ,时间 = os.time() + 86400}}
	else
		for i,v in pairs(RoleItem.临时效果) do
			if v.类型 == Type then
				RoleItem.临时效果[i].数值=Value
				RoleItem.临时效果[i].时间=os.time() + 86400
				return 
			end
		end
		RoleItem.临时效果[#RoleItem.临时效果+1]= {类型 = Type,数值 =Value ,时间 = os.time() + 86400}
	end
end

function ItemControl:删除道具处理(id, 数据)
	if 数据.id == -705 then
		self.临时类型 = UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]].类型
		if self.临时类型 ~= "武器" and self.临时类型 ~= "衣服" and self.临时类型 ~= "头盔" and self.临时类型 ~= "项链" and self.临时类型 ~= "腰带" and self.临时类型 ~= "鞋子" then
			SendMessage(UserData[id].连接id, 7, "#y/只有装备才可以添加临时效果")
			return false
		end

		return true
	elseif 数据.id == -701 and UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]].名称 == "金银宝盒" then
		return true
	elseif 数据.id == -21 and UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]].名称 == "心魔宝珠" then
		return true
	elseif 任务数据[数据.id] == nil then
		return false
	elseif LinkTask[任务数据[数据.id].类型] then
		if UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]]==nil or 任务数据[数据.id].Item ==nil then
		   return false
		end
		if 任务数据[数据.id].Item ~= UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]].名称 then
		    SendMessage(UserData[id].连接id, 7, "#y/对方需要的不是这个东西")
			return false
		else
			    ItemControl:RemoveItems(UserData[id],数据.格子,"道具给")
				return true
		end
	elseif 任务数据[数据.id].类型 == "押镖" or 任务数据[数据.id].类型 == "坐骑" or 任务数据[数据.id].类型 == "神器任务"  or 任务数据[数据.id].类型 == "任务链" or 任务数据[数据.id].类型 == "宝宝任务链"then
		if UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]]==nil or 任务数据[数据.id].道具 ==nil then
		   return false
		end
		if 任务数据[数据.id].道具 ~= UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]].名称 then
		    SendMessage(UserData[id].连接id, 7, "#y/对方需要的不是这个东西")
			return false
		else
			    if 任务数据[数据.id].类型 == "押镖" and 任务数据[数据.id].地图编号 ~=  UserData[id].地图 then
			        封禁账号(UserData[id],"押镖")
			    elseif 任务数据[数据.id].类型 == "任务链" and 任务数据[数据.id].地图名称 ~=  UserData[id].地图 then
			       封禁账号(UserData[id],"任务链")
			      elseif 任务数据[数据.id].类型 == "宝宝任务链" and 任务数据[数据.id].地图名称 ~=  UserData[id].地图 then
			       封禁账号(UserData[id],"宝宝任务链")
			    end
					ItemControl:RemoveItems(UserData[id],数据.格子,"道具给")
				return true
		end
	elseif 任务数据[数据.id].类型 == "师门" then
		if 任务数据[数据.id].道具 ~= UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]].名称 then
		    SendMessage(UserData[id].连接id, 7, "#y/对方需要的不是这个东西")
			return false
		else
			ItemControl:RemoveItems(UserData[id],数据.格子,"道具给")
		end
		return true


	elseif UserData[id].角色.道具.包裹[数据.格子] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/你并没有这样的道具")

		return false
	elseif 任务数据[数据.id].道具 ~= UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]].名称 then
		SendMessage(UserData[id].连接id, 7, "#y/对方需要的不是这个东西")

		return false
	elseif 任务数据[数据.id].数量 == nil then
		if UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]].数量 == nil then
			UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]] = nil
			UserData[id].角色.道具.包裹[数据.格子] = nil

			SendMessage(UserData[id].连接id, 3007, {格子 =数据.格子})

			return true
		else
			UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]].数量 = UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]].数量 - 1

			if UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]].数量 <1 then
				UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]] = nil
				UserData[id].角色.道具.包裹[数据.格子] = nil
			end

			SendMessage(UserData[id].连接id, 3007, {格子 =数据.格子,数据=UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]]})

			return true
		end
	else
		if UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]].数量 == nil or UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]].数量 < 任务数据[数据.id].数量 then
			SendMessage(UserData[id].连接id, 7, "#y/给予对方的道具数量不足")
			return false
		else
			UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]].数量 = UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]].数量 - 任务数据[数据.id].数量
			if UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]].数量 <1 then
				UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]] = nil
				UserData[id].角色.道具.包裹[数据.格子] = nil
			end
			SendMessage(UserData[id].连接id, 3007, {格子 =数据.格子,数据=UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]]})
		end

		return true
	end
end

function ItemControl:城市快捷传送(id)
	if UserData[id].战斗 ~= 0 then
		SendMessage(UserData[id].连接id, 7, "#y/该功能无法在战斗中使用")
	elseif UserData[id].队伍 ~= 0 then
		SendMessage(UserData[id].连接id, 7, "#y/该功能无法在组队状态下使用")
	elseif UserData[id].角色.等级 >= 60 and 取角色银子(id) < 1000 then
		SendMessage(UserData[id].连接id, 7, "#y/使用此功能需要消耗1000两银子")
	elseif RoleControl:GetTaskID(UserData[id],"游泳") ~= 0 then
		SendMessage(UserData[id].连接id, 7, "#y/游戏大赛过程中不可使用此功能")
		return 0
	elseif RoleControl:GetTaskID(UserData[id],"押镖") ~= 0 then
		SendMessage(UserData[id].连接id, 7, "#y/押镖过程中无法飞行")

		return 0
	else
		self.传送数据 = {
			地图 = UserData[id].角色.传送地图
		}
		self.跳转坐标 = {
			{z = 1001,x = 364,y = 36},
			{z = 1501,x = 65,y = 113},
			{z = 1092,x = 123,y = 57},
			{z = 1070,x = 117,y = 148}
		}
		if self.传送数据.地图 == 1001 then
			self.传送数据.x = 364
			self.传送数据.y = 36
		elseif self.传送数据.地图 == 1501 then
			self.传送数据.x = 65
			self.传送数据.y = 113
		elseif self.传送数据.地图 == 1092 then
			self.传送数据.x = 123
			self.传送数据.y = 57
		elseif self.传送数据.地图 == 1070 then
			self.传送数据.x = 117
			self.传送数据.y = 148
		end

		if UserData[id].角色.门派 ~= "无" then
			self.临时门派 = UserData[id].角色.门派
			self.传送数据 = {
				地图 = 门派传送[self.临时门派].z,
				x = 门派传送[self.临时门派].x,
				y = 门派传送[self.临时门派].y
			}
		end

		MapControl:Jump(id, self.传送数据.地图, self.传送数据.x, self.传送数据.y)

		if UserData[id].角色.等级 >= 60 then
			RoleControl:扣除银子(UserData[id],1000, 5)

		end
	end
end


function ItemControl:索要玩家道具(id, 内容)
	self.发送信息 = {}

	for n = 1, 20 do
		if UserData[id].角色.道具[内容][n] ~= nil and UserData[id].物品[UserData[id].角色.道具[内容][n]] ~= nil then
			self.发送信息[n] = UserData[id].物品[UserData[id].角色.道具[内容][n]]
		end
	end

	for n = 21, 26 do
		if UserData[id].角色.装备数据[n] ~= nil and UserData[id].物品[UserData[id].角色.装备数据[n]] ~= nil then
			self.发送信息[n] = UserData[id].物品[UserData[id].角色.装备数据[n]]
		end
	end

	self.发送信息.类型 = 内容
	self.发送信息.银两 = UserData[id].角色.道具.货币.银子
	self.发送信息.存银 = UserData[id].角色.道具.货币.存银

	return self.发送信息
end


function ItemControl:比武奖励(id)
	if 比武大会数据[id] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/你没有这样的奖励可以领取")

		return 0
	elseif 比武大会数据[id].奖励 == false then
		SendMessage(UserData[id].连接id, 7, "#y/你没有这样的奖励可以领取")

		return 0
	else
		比武大会数据[id].奖励 = false
		self.经验奖励 = math.floor(UserData[id].角色.等级 * UserData[id].角色.等级 * UserData[id].角色.等级 * 2.2)
	

		RoleControl:添加经验(UserData[id],self.经验奖励, id, 16)
		 RoleControl:添加银子(UserData[id],比武大会数据[id].积分 * 50000,"比武大会奖励")
		SendMessage(UserData[id].连接id, 7, "#y/你领取了比武大会奖励")
	end
end
function ItemControl:给予随机五宝(id)
	self.临时参数 = math.random( 5)
	if self.临时参数 == 1 then
		self:GiveItem(id,"夜光珠")
		return "夜光珠"
	elseif self.临时参数 == 2 then
		self:GiveItem(id,"避水珠")
		return "避水珠"
	elseif self.临时参数 == 3 then
		self:GiveItem(id,"龙鳞")
		return "龙鳞"
	elseif self.临时参数 == 4 then
		self:GiveItem(id,"定魂珠")
		return "定魂珠"
	elseif self.临时参数 == 5 then
		self:GiveItem(id,"金刚石")
		return "金刚石"
	end
end
function ItemControl:专用装备(id)
	self.临时格子 = UserData[id].道具id.格子
	self.临时类型 = UserData[id].道具id.类型
	self.道具编号 = UserData[id].角色.道具[self.临时类型][self.临时格子]

	if UserData[id].物品[self.道具编号].制造者 == nil then
		self.属性下限 = 1.5
		self.属性上限 = 1.8
		self.制造者 = 0
	elseif UserData[id].物品[self.道具编号].强化 ~= 1 then
		self.属性下限 = 打造属性.普通.下限 + 0.1
		self.属性上限 = 打造属性.普通.上限 + 0.1
		self.制造者 = UserData[id].物品[self.道具编号].制造玩家
	elseif UserData[id].物品[self.道具编号].强化 == 1 then
		self.属性下限 = 打造属性.强化.下限 + 0.1
		self.属性上限 = 打造属性.强化.上限 + 0.1
		self.制造者 = UserData[id].物品[self.道具编号].制造玩家
	end

	EquipmentControl:生成装备(id,UserData[id].物品[self.道具编号].类型, UserData[id].物品[self.道具编号].名称, UserData[id].物品[self.道具编号].等级, self.属性上限, self.属性下限, false, UserData[id].物品[self.道具编号].强化, self.制造者, 1, self.道具编号)

	UserData[id].物品[self.道具编号].专用 = id

	self:鉴定道具(id, self.临时格子, self.临时类型, 1)
end
function ItemControl:删除道具(id, 格子, 类型)
	if UserData[id].物品[格子] == nil then
		return 0
	else
		UserData[id].物品[格子] = nil
		UserData[id].角色.道具[类型][格子] = nil
	end
end
function ItemControl:取低级兽诀名称()
	self.临时名称 = {
		"防御",
		"反击",
		"必杀",
		"吸血",
		"强力",
		"偷袭",
		"反震",
		"法术暴击",
		"冥思",
		"法术波动",
		"夜战",
		"隐身",
		"感知",
		"再生",
		"法术连击",
		"毒",
		"幸运",
		"连击",
		"永恒",
		"敏捷",
		"神佑复生",
		"驱鬼",
		"鬼魂术",
		"魔之心",
		"水攻",
		"落岩",
		"雷击",
		"烈火"
	}

	return self.临时名称[math.random( #self.临时名称)]
end
function ItemControl:取高级兽诀名称()
	self.临时名称 = {
		"高级防御",
		"高级反击",
		"高级必杀",
		"高级吸血",
		"高级强力",
		"高级偷袭",
		"高级反震",
		"高级法术暴击",
		"高级冥思",
		"高级法术波动",
		"高级夜战",
		"高级隐身",
		"高级感知",
		"高级再生",
		"高级法术连击",
		"高级毒",
		"高级幸运",
		"高级连击",
		"高级永恒",
		"高级敏捷",
		"高级神佑复生",
		"高级驱鬼",
		"高级鬼魂术",
		"高级魔之心",
		"水漫金山",
		"泰山压顶",
		"地狱烈火",
		"奔雷咒"
	}

	return self.临时名称[math.random( #self.临时名称)]
end
function ItemControl:GiveItem(id,名称,等级,技能,数量,限时)---------------完成--
	local msString =名称..(等级 or  "")..(技能 or "")
	if Reward[msString] and not 首服奖励[msString]  then
		首服奖励[msString]= {id,UserData[id].角色.名称,UserData[id].角色.造型,名称,os.time()}
		 SendMessage(UserData[id].连接id, 2072, {标题="首服奖励",文本=Reward[msString].介绍})
		 SendMessage(UserData[id].连接id, 7, "#Y/你获得了首服奖励")
		 SendMessage(UserData[id].连接id, 9, "#xt/#Y/你获得了首服奖励")
		 广播消息(string.format("#xt/恭喜玩家#G/%s#W/%s获得了高额奖励！",UserData[id].角色.名称,Reward[msString].介绍))
		 发送游戏公告(string.format("恭喜玩家#G/%s#W/%s获得了高额奖励！",UserData[id].角色.名称,Reward[msString].介绍))
		for k,v in pairs(Reward[msString].奖励) do
		  	   if k == "添加物品" then 
		  	   		self:GiveItem(id,名称)
		  	   else 
		  	      RoleControl[k](RoleControl,UserData[id],v,"首服奖励")
		  	   end 
		  end
	end
			if 名称 =="随机召唤兽宝石" then
			名称 =取召唤兽随机宝石()
			elseif 名称=="随机1-2宝石" then-----自定义等级模板
			名称 =取随机宝石()
			等级=math.random(1, 2)                                     
			elseif 名称 =="随机五宝" then
			名称 =取随机五宝()
			elseif 名称 =="随机强化石" then
			名称 =取随机强化石()
			elseif 名称 =="修炼果10个" then
			名称 ="修炼果"
			数量=10
		    elseif 名称 =="超级修炼果50个" then
			名称 ="超级修炼果"
			数量=50
			elseif 名称 =="超级九转金丹50个" then
			名称 ="超级九转金丹"
			数量=50
			elseif 名称 =="随机宝石" then
			名称 =取随机宝石()
			elseif 名称 =="随机红包" then
			名称 =取随机红包()
			elseif 名称=="随机召唤兽宝石" then
			名称 =取召唤兽随机宝石()
			elseif 名称=="随机五宝" then
			名称=取随机五宝()
			elseif 名称=="随机强化石" then
			名称=取随机强化石()
			elseif 名称=="知了3000" then -----数量模板
			RoleControl:添加知了积分(UserData[id],3000,"物品奖励")
			return
			elseif 名称=="知了5000" then -----数量模板
			RoleControl:添加知了积分(UserData[id],5000,"物品奖励")
			return
			elseif 名称=="仙玉10点" then -----数量模板
			RoleControl:添加仙玉(UserData[id],10,"物品奖励")
			return
			elseif 名称=="仙玉20点" then -----数量模板
			RoleControl:添加仙玉(UserData[id],20,"物品奖励")
			return
			elseif 名称=="仙玉30点" then -----数量模板
			RoleControl:添加仙玉(UserData[id],30,"物品奖励")
			return
			elseif 名称=="仙玉40点" then -----数量模板
			RoleControl:添加仙玉(UserData[id],40,"物品奖励")
			return
			elseif 名称=="仙玉5000点" then -----数量模板
			RoleControl:添加仙玉(UserData[id],5000,"物品奖励")
			return
			elseif 名称=="仙玉10000点" then -----数量模板
			RoleControl:添加仙玉(UserData[id],10000,"物品奖励")
			return
			elseif 名称=="仙玉3000点" then -----数量模板
			RoleControl:添加仙玉(UserData[id],3000,"物品奖励")
			return
			elseif 名称=="召唤兽修炼150" then -----数量模板
			RoleControl:添加修炼点(UserData[id],150)
			return
			elseif 名称=="召唤兽修炼100" then -----数量模板
			RoleControl:添加修炼点(UserData[id],100)
			return
			elseif 名称=="召唤兽修炼50" then -----数量模板
			RoleControl:添加修炼点(UserData[id],50)
			return
			elseif 名称 =="天眼通符5个" then
			名称 ="天眼通符"
			数量=5
	
			elseif 名称 =="镇妖拘魂铃2个" then
			名称 ="镇妖拘魂铃"
			数量=2
			elseif 名称 =="镇妖拘魂铃4个" then
			名称 ="镇妖拘魂铃"
			数量=4
			elseif 名称 =="随机兽决" then
			名称 ="特殊魔兽要诀"
			技能 =取剧情兽诀名称()
			elseif 名称=="内丹碎片5个" then -----数量模板
			名称 ="内丹碎片"
			数量=5
			elseif 名称=="内丹碎片10个" then -----数量模板
			名称 ="内丹碎片"
			数量=10
			elseif 名称=="高级内丹碎片5个" then -----数量模板
			名称 ="高级内丹碎片"
			数量=5
			elseif 名称=="高级内丹碎片10个" then -----数量模板
			名称 ="高级内丹碎片"
			数量=10
			elseif 名称=="随机1-20宝石" then-----自定义等级模板
			名称 =取随机宝石()
			等级=math.random(1, 20) 
			elseif 名称=="随机1-15宝石" then-----自定义等级模板
			名称 =取随机宝石()
			等级=math.random(1, 15) 
			elseif 名称=="随机1-10宝石" then-----自定义等级模板
			名称 =取随机宝石()
			等级=math.random(1, 10) 
			elseif 名称=="随机2-3宝石" then-----自定义等级模板
			名称 =取随机宝石()
			等级=math.random(2, 3) 
			elseif 名称=="随机3-4宝石" then-----自定义等级模板
			名称 =取随机宝石()
			等级=math.random(3, 4) 
			elseif 名称=="随机4-5宝石" then-----自定义等级模板
			名称 =取随机宝石()
			等级=math.random(4, 5) 
			elseif 名称=="随机5-6宝石" then-----自定义等级模板
			名称 =取随机宝石()
			等级=math.random(5, 6) 
			elseif 名称=="随机6-7宝石" then-----自定义等级模板
			名称 =取随机宝石()
			等级=math.random(6, 7) 
			elseif 名称=="随机7-8宝石" then-----自定义等级模板
			名称 =取随机宝石()
			等级=math.random(7, 8) 
			elseif 名称=="随机9-13宝石" then-----自定义等级模板
			名称 =取随机宝石()
			等级=math.random(9,13)
			elseif 名称=="随机7-11宝石" then-----自定义等级模板
			名称 =取随机宝石()
			等级=math.random(7,11)
			elseif 名称=="随机5-10宝石" then-----自定义等级模板
			名称 =取随机宝石()
			等级=math.random(5,10)
			elseif 名称=="附魔宝珠80-100" then-----自定义等级模板
			名称 ="附魔宝珠"
			等级=math.random(8,10)*10
			elseif 名称=="附魔宝珠80-120" then-----自定义等级模板
			名称 ="附魔宝珠"
			等级=math.random(8,12)*10
			elseif 名称=="附魔宝珠80-130" then-----自定义等级模板
			名称 ="附魔宝珠"
			等级=math.random(8,13)*10
			elseif 名称=="珍珠80-120" then-----自定义等级模板
			名称 ="珍珠"
			等级=math.random(8,10)*10
			elseif 名称=="附魔宝珠120-130" then-----自定义等级模板
			名称 ="附魔宝珠"
			等级=math.random(12,13)*10
			elseif 名称=="附魔宝珠130-140" then-----自定义等级模板
			名称 ="附魔宝珠"
			等级=math.random(13,14)*10
			elseif 名称=="附魔宝珠160" then-----自定义等级模板
			名称 ="附魔宝珠"
			等级=160


            end
		local 临时道具=AddItem(名称,等级,技能,数量,nil,id,nil,nil,限时)  
		if 临时道具==nil then
			error(string.format("ID为%d的道具%s增加物品出错！",id,名称))
			return 
		end
  if 临时道具.数量  then
  	--道具Pages
    for n = 1, UserData[id].角色.道具.Pages*20 do
      if 临时道具.数量 >= 1 and UserData[id].角色.道具.包裹[n] ~= nil
      	and UserData[id].物品[UserData[id].角色.道具.包裹[n]] ~= nil and UserData[id].物品[UserData[id].角色.道具.包裹[n]] ~= 0
      	 and UserData[id].物品[UserData[id].角色.道具.包裹[n]].名称 == 临时道具.名称  then
      	 if  (临时道具.名称 == "高级清灵仙露"or 临时道具.名称 == "中级清灵仙露"or 临时道具.名称 == "初级清灵仙露") and UserData[id].物品[UserData[id].角色.道具.包裹[n]].灵气 ~= 临时道具.灵气 then
         else
	        if UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量 + 临时道具.数量 <= 999 then
	          UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量 = UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量 + 临时道具.数量
	          if 临时道具.数量 then
	            SendMessage(UserData[id].连接id,7,"#Y/你获得".."#R/"..临时道具.名称.."*"..临时道具.数量)
	          SendMessage(UserData[id].连接id,9,"#xt/#w/你获得了#R/"..临时道具.名称.."*"..临时道具.数量)
	     	 else
	      	  SendMessage(UserData[id].连接id,7,"#Y/你获得".."#R/"..临时道具.名称)
	          SendMessage(UserData[id].连接id,9,"#xt/#w/你获得了#R/"..临时道具.名称)
	          end

	          SendMessage(UserData[id].连接id, 3006, "66")
	           return
	        elseif UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量 < 999 then
	          local 误差数量 = 999 - UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量
	          UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量 = UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量 + 误差数量
	          临时道具.数量 = 临时道具.数量 - 误差数量
	        end
	     end
      end
    end
  end
   self.临时格子=RoleControl:取可用道具格子(UserData[id],"包裹")
    if self.临时格子==0 then
      SendMessage(UserData[id].连接id,7,"#Y/您当前的包裹空间已满，无法获得新道具")
    else
      self.临时编号=self:取道具编号(id)
      UserData[id].物品[self.临时编号]={}
      UserData[id].角色.道具.包裹[self.临时格子]=self.临时编号
      UserData[id].物品[self.临时编号]= 临时道具
		if UserData[id].物品[self.临时编号].数量 ==nil then
		SendMessage(UserData[id].连接id,7,"#Y/你获得".."#R/"..UserData[id].物品[self.临时编号].名称)
		SendMessage(UserData[id].连接id,9,"#xt/#W/你获得".."#R/"..UserData[id].物品[self.临时编号].名称)
		elseif UserData[id].物品[self.临时编号].数量  then
		SendMessage(UserData[id].连接id,7,"#Y/你获得".."#R/"..UserData[id].物品[self.临时编号].名称.."*"..UserData[id].物品[self.临时编号].数量)
		SendMessage(UserData[id].连接id,9,"#xt/#w/你获得了#R/"..UserData[id].物品[self.临时编号].名称.."*"..UserData[id].物品[self.临时编号].数量)
		end

		SendMessage(UserData[id].连接id, 3007, {格子= self.临时格子,数据=UserData[id].物品[self.临时编号]})
     end
 end
function ItemControl:取道具编号(id)
	for n, v in pairs(UserData[id].物品) do

		if UserData[id].物品[n] == nil then
			return n
		end
	end

	return #UserData[id].物品 + 1
end
function ItemControl:门贡兑换(id)
	UserData[id].角色.门贡 = UserData[id].角色.门贡 - 50
	local 玩家名称 = UserData[id].角色.名称
	SendMessage(UserData[id].连接id, 7, "#y/你消耗了50点门派贡献度")
	self.奖励参数 = math.random( 200)
    local cj
	        if  self.奖励参数 <= 2  then
  				local xtcj = {"玄天残卷·上卷","玄天残卷·中卷","玄天残卷·下卷"}
				cj = xtcj[math.random(1,#xtcj)]
 			elseif  self.奖励参数 <= 8  then
            	cj="法宝任务书"
 			elseif  self.奖励参数 <= 12  then
 				cj="修炼果"
	  		elseif  self.奖励参数 <= 15  then
	  			cj="二级未激活符石"
			elseif  self.奖励参数 <= 18  then
				cj = 取随机五宝()
			elseif  self.奖励参数 <= 21  then
				cj="九转金丹"
			elseif  self.奖励参数 <= 30  then
				cj="一级未激活符石"
			elseif  self.奖励参数 <= 40 then
				cj="符石卷轴"
			elseif  self.奖励参数 <= 50  then
				cj= 取随机宝石()
			elseif  self.奖励参数 <= 55  then
				cj="超级金柳露"
			elseif  self.奖励参数 <= 120  then
                cj  = 取随机强化石()
			else
				 cj = "金银宝盒"
			end
			 ItemControl:GiveItem(id,cj)
			 广播消息("#hd/".."#S/(门派兑换)".."#g/" .. 玩家名称 .. "#y/在门派师父处使用门派贡献度兑换时，居然获得了#r/"..cj)
end

function ItemControl:符石升级处理(id,格子)------------------------------完成--
  local 临时格子=分割文本(格子,"*-*")
  local 符石id = {}
  self.符石={}
  local 序号 = 0
	local 等级 ={}
	self.找到卷轴= false
	 for i=1,4 do
	  临时格子[i]=临时格子[i]+0
	   符石id[i]=UserData[id].角色.道具.包裹[临时格子[i]+0]
	  self.符石[i]= false
	    if UserData[id].角色.道具.包裹[临时格子[i]]==nil or  UserData[id].物品[符石id[i]]==nil  then
	      SendMessage(UserData[id].连接id,7,"#y/数据错误，道具不存在")
	     return 0
	      elseif RoleControl:取可用道具格子(UserData[id],"包裹") ==0 then
		SendMessage(UserData[id].连接id, 7, "#y/请在包裹栏留出一个空余的位置!")
		return 0
	    end
	    if UserData[id].物品[符石id[i]].名称 ~= "符石卷轴" and  UserData[id].物品[符石id[i]].类型 == "符石" then
	         序号= 序号+1
	       等级[#等级+1] = 符石id[i]
	    end
	 end

	  if UserData[id].物品[符石id[1]].名称=="符石卷轴" or UserData[id].物品[符石id[2]].名称=="符石卷轴" or UserData[id].物品[符石id[3]].名称=="符石卷轴"or UserData[id].物品[符石id[4]].名称=="符石卷轴" then
	    self.找到卷轴 =true
	  else
	    SendMessage(UserData[id].连接id,7,"#y/未找到符石卷轴")
	     return  0
	  end
	 if 序号 >= 3 and ItemData[UserData[id].物品[等级[1]].名称].等级 == ItemData[UserData[id].物品[等级[2]].名称].等级 and ItemData[UserData[id].物品[等级[1]].名称].等级 == ItemData[UserData[id].物品[等级[3]].名称].等级 then
	     local 合成几率
	   if  ItemData[UserData[id].物品[等级[1]].名称].等级==1 then ---合成几率 90代表90% 成功  
	     合成几率=90
	   elseif ItemData[UserData[id].物品[等级[1]].名称].等级==2 then
	     合成几率=80
	  elseif ItemData[UserData[id].物品[等级[1]].名称].等级==3 then ---最高3级 以后有4级可以在这里加
	     合成几率=70
	   end
	   if  math.random(100)>=合成几率 then
	         SendMessage(UserData[id].连接id,7,"#y/合成失败，你损失了一个符石卷轴和3块符石")
	   else
	     self:GiveItem(id,取符石(ItemData[UserData[id].物品[等级[1]].名称].等级+1))
	    end
	         for i=1,4 do
	       UserData[id].物品[符石id[i]] =nil
	       UserData[id].角色.道具.包裹[临时格子[i]] =nil
	     end
	      SendMessage(UserData[id].连接id,3006,"66")
	  else
	   SendMessage(UserData[id].连接id,7,"#y/合成符石需要3块同等级符石合成")
	   return
	 end
	 SendMessage(UserData[id].连接id, 20038, self:索要道具3(id,"包裹"))
 end
function ItemControl:炼药处理(id)---------------------------------------完成--
  self.中医等级=UserData[id].角色.辅助技能[6].等级
  if self.中医等级<10 then
    SendMessage(UserData[id].连接id,7,"#Y/你的中药医理技能未足10级，无法进行炼药")
    return 0
  elseif UserData[id].角色.当前活力<(self.中医等级*0.35+10) then
    SendMessage(UserData[id].连接id,7,"#Y/你没那么多的活力")
    return 0
  elseif 银子检查(id,5000)==false then
    SendMessage(UserData[id].连接id,7,"#Y/本次炼药需要消耗5000两银子")
    return 0
  else
    RoleControl:扣除银子(UserData[id],5000,"炼药")
    RoleControl:扣除活力(UserData[id],2,(self.中医等级*0.15+10))
    self.三药几率=self.中医等级
    if self.三药几率>60 then
     self.三药几率=60
      end
   if math.random(100)<=self.三药几率 then
     self.炼药名称={"小还丹","金香玉","千年保心丹","佛光舍利子","九转回魂丹","蛇蝎美人","十香返生丸","风水混元丹","红雪散","定神香","五龙丹"}
    else
     self.炼药名称={"金创药","金创药","金创药","金创药","金创药","金创药","金创药","金创药"}
     end
   self.药品名称=self.炼药名称[math.random(1,#self.炼药名称)]
   self.可用格子=RoleControl:取可用道具格子(UserData[id],"包裹")
   if self.可用格子==0 then
     SendMessage(UserData[id].连接id,7,"#Y/你身上已经没有多余的道具格子了")
     return 0
     end
   self.可用id=self:取道具编号(id)
   UserData[id].物品[self.可用id]={名称=self.药品名称,等级=3,类型="药品",品质=math.random(math.floor(self.中医等级*0.35),self.中医等级)}
   UserData[id].角色.道具.包裹[self.可用格子]=self.可用id
   SendMessage(UserData[id].连接id,7,"#Y/你炼制出了#R/"..self.药品名称)
   SendMessage(UserData[id].连接id,3006,"66")
   end
 end

function ItemControl:烹饪处理(id)---------------------------------------完成--
  self.烹饪等级=UserData[id].角色.辅助技能[5].等级
  if self.烹饪等级<10 then
    SendMessage(UserData[id].连接id,7,"#Y/你的烹饪技巧未足10级，无法进行炼药")
    return 0
  elseif UserData[id].角色.当前活力<self.烹饪等级 then
    SendMessage(UserData[id].连接id,7,"#Y/你没那么多的活力")
    return 0
  else
    if self.烹饪等级 > 80 then
      RoleControl:扣除活力(UserData[id],2,80)
    else
    RoleControl:扣除活力(UserData[id],2,(self.烹饪等级+10))
    end
    if self.烹饪等级<=4 then
    self.烹饪名称={"包子"}
    elseif self.烹饪等级<=9 then
    self.烹饪名称={"包子","佛跳墙"}
    elseif self.烹饪等级<=14 then
    self.烹饪名称={"包子","烤鸭","佛跳墙"}
    elseif self.烹饪等级<=19 then
    self.烹饪名称={"包子","烤鸭","佛跳墙","珍露酒"}
    elseif self.烹饪等级<=24 then
    self.烹饪名称={"包子","烤鸭","佛跳墙","珍露酒","女儿红","虎骨酒"}
    elseif self.烹饪等级<=29 then
    self.烹饪名称={"包子","烤鸭","佛跳墙","珍露酒","女儿红","虎骨酒","豆斋果"}
    elseif self.烹饪等级<=34 then
    self.烹饪名称={"包子","烤鸭","佛跳墙","珍露酒","女儿红","虎骨酒","豆斋果","臭豆腐"}
    elseif self.烹饪等级<=39 then
    self.烹饪名称={"包子","烤鸭","佛跳墙","珍露酒","女儿红","虎骨酒","豆斋果","臭豆腐","烤肉","桂花丸"}
    elseif self.烹饪等级<=44 then
    self.烹饪名称={"包子","烤鸭","佛跳墙","珍露酒","女儿红","虎骨酒","豆斋果","臭豆腐","烤肉","桂花丸","翡翠豆腐"}
    elseif self.烹饪等级<=49 then
    self.烹饪名称={"包子","烤鸭","佛跳墙","珍露酒","女儿红","虎骨酒","豆斋果","臭豆腐","烤肉","桂花丸","翡翠豆腐","长寿面"}
    elseif self.烹饪等级<=54 then
    self.烹饪名称={"包子","烤鸭","佛跳墙","珍露酒","女儿红","虎骨酒","豆斋果","臭豆腐","烤肉","桂花丸","翡翠豆腐","长寿面","梅花酒"}
    elseif self.烹饪等级<=59 then
    self.烹饪名称={"包子","烤鸭","佛跳墙","珍露酒","女儿红","虎骨酒","豆斋果","臭豆腐","烤肉","桂花丸","翡翠豆腐","长寿面","梅花酒","百味酒"}
    elseif self.烹饪等级<=64 then
    self.烹饪名称={"包子","烤鸭","佛跳墙","珍露酒","女儿红","虎骨酒","豆斋果","臭豆腐","烤肉","桂花丸","翡翠豆腐","长寿面","梅花酒","百味酒","蛇胆酒"}
    else
    self.烹饪名称={"包子","烤鸭","佛跳墙","珍露酒","女儿红","虎骨酒","豆斋果","臭豆腐","烤肉","桂花丸","翡翠豆腐","长寿面","梅花酒","百味酒","蛇胆酒","醉生梦死"}
    end

   self.烹饪名称=self.烹饪名称[math.random(1,#self.烹饪名称)]
   self.可用格子=RoleControl:取可用道具格子(UserData[id],"包裹")
   if self.可用格子==0 then
     SendMessage(UserData[id].连接id,7,"#Y/你身上已经没有多余的道具格子了")
     return 0
     end
   self.可用id=self:取道具编号(id)
   UserData[id].物品[self.可用id]={名称=self.烹饪名称,类型="烹饪",品质=math.random(math.floor(self.烹饪等级*0.35),self.烹饪等级)}
   if UserData[id].物品[self.可用id].名称=="包子" then
     UserData[id].物品[self.可用id].数量=1
     end
   UserData[id].角色.道具.包裹[self.可用格子]=self.可用id
   SendMessage(UserData[id].连接id,7,"#W/经过一阵忙碌，你烹饪出了#R/"..self.烹饪名称)
   SendMessage(UserData[id].连接id,3006,"66")
   end
 end
function ItemControl:摇钱树奖励(id, 标识)
	self.任务id = MapControl.MapData[UserData[id].地图].单位组[标识].任务id

	if 任务数据[self.任务id] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/任务数据异常")

		return 0
	elseif 任务数据[self.任务id].次数 <= 0 then
		SendMessage(UserData[id].连接id, 7, "#y/您的这棵摇钱树已经没有可摇动次数了")

		return 0
	elseif 任务数据[self.任务id].id ~= id then
		SendMessage(UserData[id].连接id, 7, "#y/这不是你的摇钱树")

		return 0
	end

	local 奖励等级 ={60}
	if UserData[id].角色.等级 <80 then
	    奖励等级 ={60,80}
	elseif UserData[id].角色.等级 <100 then
		奖励等级 ={60,80,100}
	elseif UserData[id].角色.等级 <120 then
		奖励等级 ={80,100,120}
	elseif UserData[id].角色.等级 <140 then
		奖励等级 ={100,120,140}
	else
		奖励等级 ={120,140,160}
	end
	local 随机奖励 = 奖励等级[math.random(1,#奖励等级)]

	self.奖励参数 = math.random(100)

	if self.奖励参数 <= 5 then
		local 灵饰类型= {"手镯","佩饰","戒指","耳饰"}
     	self:GiveItem(id,"灵饰指南书",随机奖励,灵饰类型[math.random(1,#灵饰类型)])
     	广播消息(9, "#xt/#g/ " .. UserData[id].角色.名称 .. "#y/在摇动摇钱树时，竟然获得了从树上掉落的#r/" .. "灵饰指南书")
	elseif self.奖励参数 <= 8 then
		self:GiveItem(id,"元灵晶石",随机奖励 )
		广播消息(9, "#xt/#g/ " .. UserData[id].角色.名称 .. "#y/在摇动摇钱树时，竟然获得了从树上掉落的#r/" .. "元灵晶石")

	elseif self.奖励参数 <= 40 then
		self.强化石名称 = 取随机强化石()
		ItemControl:GiveItem(id, self.强化石名称)
	elseif self.奖励参数 <= 50 then
		self.宝石名称 = 取随机宝石()
		ItemControl:GiveItem(id, self.宝石名称, math.random( 3))
	elseif self.奖励参数 <= 70 then
		self:GiveItem(id, "魔兽要诀")
		广播消息(9, "#xt/#g/ " .. UserData[id].角色.名称 .. "#y/在摇动摇钱树时，竟然获得了从树上掉落的#r/" .. "魔兽要诀")
	elseif self.奖励参数 <= 85 then
		self.随机名称 = 取随机书铁()
		self.任务链等级 = math.random(8, 11)*10
		ItemControl:GiveItem(id, self.随机名称, self.任务链等级)
		广播消息(9, "#xt/#g/ " .. UserData[id].角色.名称 .. "#y/在摇动摇钱树时，竟然获得了从树上掉落的#r/" .. self.任务链等级 .. "级的#r/" .. self.随机名称)
	else
		RoleControl:添加银子(UserData[id],100000,"摇钱树")
	end
	任务数据[self.任务id].次数 = 任务数据[self.任务id].次数 - 1
	if 任务数据[self.任务id].次数 <= 0 then
		RoleControl:取消任务(UserData[id],self.任务id)
		MapControl:移除单位(UserData[id].地图, self.任务id)
		任务数据[self.任务id] = nil
		SendMessage(UserData[id].连接id, 7, "#y/你的这棵摇钱树消失了")
	end
 end
 
function ItemControl:梦幻瓜子奖励(id, 标识)
	self.任务id = MapControl.MapData[UserData[id].地图].单位组[标识].任务id

	if 任务数据[self.任务id] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/任务数据异常")

		return 0
	elseif 任务数据[self.任务id].次数 <= 0 then
		SendMessage(UserData[id].连接id, 7, "#y/您的这棵梦幻瓜子已经没有可摇动次数了")

		return 0
	elseif 任务数据[self.任务id].id ~= id then
		SendMessage(UserData[id].连接id, 7, "#y/这不是你的梦幻瓜子")

		return 0
	end

	self.奖励参数 = math.random(100)

	if self.奖励参数 <= 5 then
		self:GiveItem(id,"梦幻西瓜",nil,nil,2)
	elseif self.奖励参数 <= 8 then
		self:GiveItem(id,"梦幻西瓜",nil,nil,3)
  elseif self.奖励参数 <= 10 then
		self:GiveItem(id,"梦幻西瓜",nil,nil,4)
	elseif self.奖励参数 <= 11 then
		self:GiveItem(id,"梦幻西瓜",nil,nil,5)	
	else
		self:GiveItem(id,"梦幻西瓜",nil,nil,1)
	end
	任务数据[self.任务id].次数 = 任务数据[self.任务id].次数 - 1
	if 任务数据[self.任务id].次数 <= 0 then
		RoleControl:取消任务(UserData[id],self.任务id)
		MapControl:移除单位(UserData[id].地图, self.任务id)
		任务数据[self.任务id] = nil
		SendMessage(UserData[id].连接id, 7, "#y/你的这棵梦幻瓜子消失了")
	end
end

function ItemControl:ItemCount(id,名称,参数)
	local 数量 = 0
	for i = 1,20 do
		local 道具id=UserData[id].角色.道具.包裹[i]
		if UserData[id].物品[道具id] ~= nil then
			if UserData[id].物品[道具id].名称 == 名称 then
				if 参数 ~= nil then
					local 符合 = true
					for n,v in pairs(参数) do
						if UserData[id].物品[道具id][n] ~= v then
							符合 = false
						end
					end
					if 符合 then
						if UserData[id].物品[道具id].数量 ~= nil then
							数量 = 数量 + UserData[id].物品[道具id].数量
						else
							数量 = 数量 + 1
						end
					end
				else
					if UserData[id].物品[道具id].数量 ~= nil then
						数量 = 数量 + UserData[id].物品[道具id].数量
					else
						数量 = 数量 + 1
					end
				end
			end
		else
			UserData[id].角色.道具.包裹[i] = nil
		end
	end
	return 数量
end

function ItemControl:TakeItem(id,名称,数量,参数)
	print(id,名称,数量,参数)
	if 数量 == nil then
		数量 = 1
	end
	if self:ItemCount(id,名称,参数) < 数量 then
		常规提示(id,"#Y包裹中"..名称.."数量不足。")
		return
	end
	for i = 1,20 do
		if 数量 <= 0 then
			break
		end
		local 道具id=UserData[id].角色.道具.包裹[i]
		if UserData[id].物品[道具id] ~= nil then
			if UserData[id].物品[道具id].名称 == 名称 then
				if 参数 ~= nil then
					local 符合 = true
					for n,v in pairs(参数) do
						if UserData[id].物品[道具id][n] ~= v then
							符合 = false
						end
					end
					if 符合 then
						if UserData[id].物品[道具id].数量 == nil then
							UserData[id].物品[道具id] = nil
							UserData[id].角色.道具.包裹[i] = nil
							数量 = 数量 - 1
						else
							if UserData[id].物品[道具id].数量 > 数量 then
								UserData[id].物品[道具id].数量 = UserData[id].物品[道具id].数量 - 数量
								数量 = 0
							else
								数量 = 数量 - UserData[id].物品[道具id].数量
								UserData[id].物品[道具id] = nil
								UserData[id].角色.道具.包裹[i] = nil
							end
						end
					end
				else
					if UserData[id].物品[道具id].数量 == nil then
						UserData[id].物品[道具id] = nil
						UserData[id].角色.道具.包裹[i] = nil
						数量 = 数量 - 1
					else
						if UserData[id].物品[道具id].数量 > 数量 then
							UserData[id].物品[道具id].数量 = UserData[id].物品[道具id].数量 - 数量
							数量 = 0
						else
							数量 = 数量 - UserData[id].物品[道具id].数量
							UserData[id].物品[道具id] = nil
							UserData[id].角色.道具.包裹[i] = nil
						end
					end
				end
			end
		else
			UserData[id].角色.道具.包裹[i] = nil
		end
	end
	SendMessage(UserData[id].连接id, 3006, "66")
end
 
 
 
 
return ItemControl