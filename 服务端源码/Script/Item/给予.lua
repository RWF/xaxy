--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-01-20 04:46:45
--======================================================================--
function ItemControl:给予玩家物品处理(id, 参数, 序号, 文本)-------------完成--
	self.临时文本 = 分割文本(文本, "*-*")
	local 内容 = {
		id = 参数,
		银子 = 序号+0,
		格子 = {
			0,
			0,
			0
		}
	}


	for n = 1, #self.临时文本 do
		if self.临时文本[n] ~= "" then
			内容.格子[n] = self.临时文本[n] + 0
		end
	end

	if UserData[内容.id] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/这个玩家并不在线")
	elseif UserData[id].地图 ~= UserData[内容.id].地图 then
		SendMessage(UserData[id].连接id, 7, "#y/你离这个玩家太远了")
	elseif MapControl:取距离范围(id, 内容.id, 400) == false then
		SendMessage(UserData[id].连接id, 7, "#y/你离这个玩家太远了")
	elseif UserData[内容.id].屏蔽给予  then
		SendMessage(UserData[id].连接id, 7, "#y/对方未打开给予开关")
	else
		for n = 1, 3 do
			if 内容.格子[n] ~= 0 then
				self:给予玩家物品处理1(id, 内容.id, 内容.格子[n])
			end
		end



		if 内容.银子 > 0 then
			if UserData[id].角色.道具.货币.银子 < 内容.银子 then
				SendMessage(UserData[id].连接id, 7, "#y/你没有那么多的银子")
			else
				UserData[id].角色.道具.货币.银子 = UserData[id].角色.道具.货币.银子 - 内容.银子
				  RoleControl:添加银子(UserData[内容.id],内容.银子,"给予")
				

				SendMessage(UserData[id].连接id, 7, "#y/你给了" .. UserData[内容.id].角色.名称 .. 内容.银子 .. "两银子")
				SendMessage(UserData[内容.id].连接id, 7, "#y/" .. UserData[id].角色.名称 .. "给了你" .. 内容.银子 .. "两银子")
				RoleControl:添加消费日志(UserData[id],"[给予]给予(" .. UserData[内容.id].账号 .. ")银子：" .. 内容.银子)
				RoleControl:添加消费日志(UserData[内容.id],"[给予]获得(" .. UserData[id].账号 .. ")银子：" .. 内容.银子)

			end
		end
				SendMessage(UserData[id].连接id, 3006, "")
				SendMessage(UserData[内容.id].连接id, 3006, "")
	end
 end
function ItemControl:给予玩家物品处理1(id, 给予id, 格子)----------------完成--
	self.道具id = UserData[id].角色.道具.包裹[格子]

	if UserData[id].角色.道具.包裹[格子] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/你没有这样的道具")

		return 0
	elseif UserData[id].物品[self.道具id] == nil or UserData[id].物品[self.道具id] == 0 then
		SendMessage(UserData[id].连接id, 7, "#y/这个道具不存在")

		return 0
	elseif UserData[id].物品[self.道具id].加锁 then
		SendMessage(UserData[id].连接id, 7, "#r/"..UserData[id].物品[self.道具id].名称.."#y/未解锁无法给予!")
		return 0
	elseif UserData[id].物品[self.道具id].Time then
		SendMessage(UserData[id].连接id, 7, "#r/"..UserData[id].物品[self.道具id].名称.."#y/限时道具无法给予!")
		return 0
	elseif UserData[id].物品[self.道具id].类型== "剧情道具" then
		SendMessage(UserData[id].连接id, 7, "#r/"..UserData[id].物品[self.道具id].名称.."#y/剧情道具无法给予!")
		return 0
    elseif UserData[id].物品[self.道具id].类型== "古董" then
		SendMessage(UserData[id].连接id, 7, "#r/"..UserData[id].物品[self.道具id].名称.."#y/古董无法给予!")
		return 0
	else
		self.玩家格子 = RoleControl:取可用道具格子(UserData[给予id],"包裹")

		if self.玩家格子 == 0 then
			SendMessage(UserData[id].连接id, 7, "#y/对方身上没有多余的道具空间")

			return 0
		else
			self.临时道具id = ItemControl:取道具编号(给予id)
			UserData[给予id].物品[self.临时道具id] = table.copy(UserData[id].物品[self.道具id])
			UserData[给予id].角色.道具.包裹[self.玩家格子] = self.临时道具id
			UserData[id].物品[self.道具id] = nil
			UserData[id].角色.道具.包裹[格子] = nil

			SendMessage(UserData[id].连接id, 7, "#y/你给了" .. UserData[给予id].角色.名称 .. UserData[给予id].物品[self.临时道具id].名称)
			RoleControl:添加消费日志(UserData[给予id],"[给予]获得(" .. UserData[id].账号 .. ")道具：" .. UserData[给予id].物品[self.临时道具id].名称)
			RoleControl:添加消费日志(UserData[id],"[给予]给予(" .. UserData[给予id].账号 .. ")道具：" .. UserData[给予id].物品[self.临时道具id].名称)
			SendMessage(UserData[给予id].连接id, 7, "#y/" .. UserData[id].角色.名称 .. "给了你" .. UserData[给予id].物品[self.临时道具id].名称)
		end
	end
 end

