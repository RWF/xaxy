--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-01-14 23:35:09
--======================================================================--
function ItemControl:法宝合成处理(id,格子)------------------------------完成--
  local 临时格子=分割文本(格子,"*-*")
  local 符石id = {}
  local ndmc = {"玄龟板","千年阴沉木","麒麟血","金凤羽","内丹"}
   for i=1,5 do
    临时格子[i]=临时格子[i]+0
     符石id[i]=UserData[id].角色.道具.包裹[临时格子[i]+0]
    if UserData[id].角色.道具.包裹[临时格子[i]]==nil or  UserData[id].物品[符石id[i]]==nil  then
        SendMessage(UserData[id].连接id,7,"#y/数据错误，道具不存在")
      return 0
    elseif RoleControl:取可用道具格子(UserData[id],"包裹") ==0 then
        SendMessage(UserData[id].连接id, 7, "#y/请在包裹栏留出一个空余的位置!")
        return 0
    elseif UserData[id].物品[符石id[i]].名称 ~= ndmc[i] then
        SendMessage(UserData[id].连接id, 7, "#y/法宝材料错误!")
        return 0
    else
       ItemControl:RemoveItems(UserData[id],临时格子[i],"法宝合成")

    end
   end
  -- local 随机法宝 ={
  -- {"碧玉葫芦","飞剑","拭剑石","金甲仙衣","缚妖索","金钱镖","惊魂铃","嗜血幡","风袋","九黎战鼓","盘龙壁","神行飞剑","汇灵盏","天师符","织女扇","雷兽","迷魂灯","定风珠","幽灵珠","会神珠"},
  -- {"降魔斗篷","附灵玉","捆仙绳","发瘟匣","五彩娃娃","七杀","罗汉珠","赤焰","金刚杵","兽王令","摄魂","神木宝鼎","宝烛","天煞","琉璃灯","云雷鼓"},
  -- {"落雨金钱","缩地尺","缚龙索","鬼泣","月光宝盒","混元伞","无魂傀儡","苍白纸人","通灵宝玉","聚宝盆","影蛊","乾坤玄火塔","无尘扇","无字经","葬龙笛","神匠袍","金箍棒","干将莫邪","救命毫毛","伏魔天书","普渡","镇海珠","奇门五行令","失心钹","五火神焰印","九幽","慈悲","月影","斩魔","金蟾"},
  -- {"舞雪冰蝶","河图洛书","炽焰丹珠","凌波仙符","归元圣印","流影云笛","谷玄星盘","重明战鼓","蟠龙玉璧","翡翠芝兰","落星飞鸿","千斗金樽","宿幕星河", "梦云幻甲","软烟罗锦"}}
   local 随机法宝 ={
  {"碧玉葫芦","飞剑","拭剑石","金甲仙衣","缚妖索","金钱镖","惊魂铃","嗜血幡","风袋","九黎战鼓","盘龙壁","神行飞剑","汇灵盏","天师符","织女扇","雷兽","迷魂灯","定风珠","幽灵珠","会神珠"},
  {"降魔斗篷","附灵玉","捆仙绳","发瘟匣","五彩娃娃","七杀","罗汉珠","赤焰","金刚杵","兽王令","摄魂","神木宝鼎","宝烛","天煞","琉璃灯","云雷鼓"},
  {"落雨金钱","缩地尺","缚龙索","鬼泣","月光宝盒","混元伞","无魂傀儡","苍白纸人","聚宝盆","影蛊","乾坤玄火塔","无尘扇","无字经","葬龙笛","神匠袍","金箍棒","干将莫邪","救命毫毛","伏魔天书","普渡","镇海珠","奇门五行令","失心钹","金蟾"},
  {"舞雪冰蝶","河图洛书","炽焰丹珠","凌波仙符","流影云笛","谷玄星盘","重明战鼓","蟠龙玉璧","翡翠芝兰", "梦云幻甲","软烟罗锦"}}
  local sj = 1
   if UserData[id].角色.等级 > 120 then
    sj = math.random(4)
   elseif UserData[id].角色.等级 > 100 then
    sj = math.random(3)
   elseif  UserData[id].角色.等级 > 80 then
    sj = math.random(2)
   end
    self:GiveItem(id, 随机法宝[sj][math.random(1,#随机法宝[sj])])
    SendMessage(UserData[id].连接id,2034,self:索要道具3(id, "包裹"))
 end

function ItemControl:佩戴法宝(id, 格子, 类型,交换格子)------------------完成--
	local 内容 = {
		格子 = tonumber(格子),
		当前类型 =类型,
		交换格子=tonumber(交换格子),
	 }

 if 内容.格子>=35 then --卸下装备
      if 内容.交换格子~= nil then
      self.临时格子 = 内容.交换格子
      else
      self.临时格子=RoleControl:取可用道具格子(UserData[id],内容.当前类型)
      end
      if self.临时格子==0 then
      SendMessage(UserData[id].连接id,7,"#Y/您的"..内容.当前类型.."已经无法存储更多的道具了")
      SendMessage(UserData[id].连接id,3006,"66")
      return 0

      else
      UserData[id].角色.道具[内容.当前类型][self.临时格子]=UserData[id].角色.法宝[内容.格子]
      UserData[id].角色.法宝[内容.格子]=0

             if  UserData[id].物品[ UserData[id].角色.道具[内容.当前类型][self.临时格子]].Time and UserData[id].物品[ UserData[id].角色.道具[内容.当前类型][self.临时格子]].Time <= os.time() then 
                    UserData[id].物品[ UserData[id].角色.道具[内容.当前类型][self.临时格子]] =nil
                  UserData[id].角色.道具.包裹[ UserData[id].角色.道具[内容.当前类型][self.临时格子]] =nil
                  SendMessage(UserData[id].连接id, 7, "#y/你的道具已经过期被系统回收")   
                end

      RoleControl:刷新装备属性(UserData[id])

      SendMessage(UserData[id].连接id,3006,"66")
      end
  else
    self.道具编号=UserData[id].角色.道具[内容.当前类型][内容.格子]
    if UserData[id].物品[self.道具编号] == nil then
        SendMessage(UserData[id].连接id,7,"#Y/此道具不存在请刷新背包")
       return
    end
    local Name =UserData[id].物品[self.道具编号].名称 

        self.格子序号=0
        if ItemData[Name].等级  ==1 then
          if UserData[id].角色.法宝[35]==0 then
             self.格子序号=35
          else
            self.格子序号=36
          end
        elseif ItemData[Name].等级  ==2 then
        self.格子序号=36
        elseif ItemData[Name].等级  ==3 then
        self.格子序号=37
        elseif ItemData[Name].等级  ==4 then
        self.格子序号=38
        end
          if self.格子序号==0 then
          SendMessage(UserData[id].连接id,7,"#Y/此道具无法佩戴")
          SendMessage(UserData[id].连接id,3006,"66")
          return 0
          elseif ItemData[Name].属性.门派 and ItemData[Name].属性.门派~=UserData[id].角色.门派 then
          SendMessage(UserData[id].连接id,7,"#Y/当前门派无法佩戴此类法宝")
          SendMessage(UserData[id].连接id,3006,"66")
          return 0
          elseif  ItemData[Name].属性.佩戴等级>UserData[id].角色.等级  then
          SendMessage(UserData[id].连接id,7,"#Y/当前等级无法佩戴此类法宝")
          SendMessage(UserData[id].连接id,3006,"66")
          return 0
          elseif UserData[id].物品[self.道具编号].灵气 < 0  then
            SendMessage(UserData[id].连接id,7,"#Y/当前灵气不足无法进行佩戴")
            SendMessage(UserData[id].连接id,3006,"66")
          return 0
          elseif  not ItemData[Name].属性.佩戴  then
          SendMessage(UserData[id].连接id,7,"#Y/此法宝不需要佩戴")
          SendMessage(UserData[id].连接id,3006,"66")
          return 0
         elseif  UserData[id].物品[self.道具编号].Time and UserData[id].物品[self.道具编号].Time <= os.time() then 
                    UserData[id].物品[self.道具编号] =nil
                  UserData[id].角色.道具.包裹[内容.格子] =nil
                  SendMessage(UserData[id].连接id, 7, "#y/你的道具已经过期被系统回收")
                  SendMessage(UserData[id].连接id, 3007,{格子=内容.格子,数据=UserData[id].物品[UserData[id].角色.道具.包裹[内容.格子]]})
          else
                self.临时装备=UserData[id].角色.法宝[self.格子序号]
                UserData[id].角色.法宝[self.格子序号]=self.道具编号
                if self.临时装备~=0 then
                UserData[id].角色.道具[内容.当前类型][内容.格子]=self.临时装备
                else
                  UserData[id].角色.道具[内容.当前类型][内容.格子]=nil
                end
                RoleControl:刷新装备属性(UserData[id])
                 SendMessage(UserData[id].连接id,3030,1)
                 SendMessage(UserData[id].连接id,3006,"66")



          end
   end
 end
function ItemControl:法宝修炼(id,数据)----------------------------------完成--
	self.临时id1 = UserData[id].角色.道具[数据.类型][数据.格子]
	if UserData[id].物品[self.临时id1] == nil or UserData[id].物品[self.临时id1] == 0 then
		SendMessage(UserData[id].连接id, 7, "#y/你似乎并没有这样的道具")
		return 0
	end
  local Name =UserData[id].物品[self.临时id1].名称
  
    local 消耗经验 =(UserData[id].物品[self.临时id1].层数+1)*ItemData[Name].等级*UserData[id].角色.等级*1000*(UserData[id].物品[self.临时id1].层数+1)
    if UserData[id].角色.当前经验 < 消耗经验 then
    	SendMessage(UserData[id].连接id, 7, "#y/当前经验不足!")
		return 0
	elseif UserData[id].物品[self.临时id1].最大经验 == "无法提升" then
		    	SendMessage(UserData[id].连接id, 7, "#y/当前法宝已经修炼满层,已经无法在提升!")
		return 0
	else
		UserData[id].物品[self.临时id1].当前经验=UserData[id].物品[self.临时id1].当前经验+消耗经验
		UserData[id].角色.当前经验=UserData[id].角色.当前经验-消耗经验
       SendMessage(UserData[id].连接id, 7, "#y/你成功为当前法宝注入了经验")
       if UserData[id].物品[self.临时id1].当前经验> UserData[id].物品[self.临时id1].最大经验 then
       	  UserData[id].物品[self.临时id1].当前经验=0
       	  UserData[id].物品[self.临时id1].层数 = UserData[id].物品[self.临时id1].层数+1
          UserData[id].物品[self.临时id1].最大经验 = 取法宝经验( UserData[id].物品[self.临时id1].名称,UserData[id].物品[self.临时id1].层数)
          SendMessage(UserData[id].连接id, 7, "#y/你当前的法宝吸入了你经验后,将等级提升了一级!")
       end
	end
    SendMessage(UserData[id].连接id,3030,1)
	SendMessage(UserData[id].连接id, 3006, "66")
 end



function ItemControl:法宝补充灵气(id,数据)----------------------------------完成--
    self.临时id1 = UserData[id].角色.道具[数据.类型][数据.格子]
    if UserData[id].物品[self.临时id1] == nil or UserData[id].物品[self.临时id1] == 0 then
      SendMessage(UserData[id].连接id, 7, "#y/你似乎并没有这样的道具")
      return 0
    end

      if UserData[id].角色.法宝灵气 < 120 then
        SendMessage(UserData[id].连接id, 7, "#y/当前剩余法宝灵气不足120!")
         return 0
     else
        local Name =UserData[id].物品[self.临时id1].名称
         UserData[id].物品[self.临时id1].灵气 =UserData[id].物品[self.临时id1].灵气+120/ItemData[Name].等级
         UserData[id].角色.法宝灵气=UserData[id].角色.法宝灵气-120
            SendMessage(UserData[id].连接id, 7, "#y/你当前的法宝成功补充了"..(120/ItemData[Name].等级).."点灵气")
    end
    SendMessage(UserData[id].连接id,3030,1)
    SendMessage(UserData[id].连接id, 3006, "66")
 end

function ItemControl:法宝提取灵气(id,数据)----------------------------------完成--
   self.临时id1 = UserData[id].角色.道具[数据.类型][数据.格子]
    if UserData[id].物品[self.临时id1] == nil or UserData[id].物品[self.临时id1] == 0 then
      SendMessage(UserData[id].连接id, 7, "#y/你似乎并没有这样的道具")
      return 0
    end


      if UserData[id].物品[self.临时id1].灵气 < 1 then
        SendMessage(UserData[id].连接id, 7, "#y/当前法宝灵气不足1点无法提取")
         return 0
     else
        
         local Name =UserData[id].物品[self.临时id1].名称
         UserData[id].角色.法宝灵气 =UserData[id].角色.法宝灵气+ItemData[Name].等级*300
         SendMessage(UserData[id].连接id, 7, "#y/你成功补充了"..(ItemData[Name].等级*300).."点剩余灵气")
          UserData[id].物品[self.临时id1] = nil 
          UserData[id].角色.道具[数据.类型][数据.格子]=nil 

      end
    SendMessage(UserData[id].连接id,3030,1)
    SendMessage(UserData[id].连接id, 3006, "66")
 end
