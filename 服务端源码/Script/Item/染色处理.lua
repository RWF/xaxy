--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-12-12 00:05:58
--======================================================================--
function ItemControl:染色处理(id, 参数, 序号, 内容)---------------------完成--
	self.染色参数 = {参数,序号,内容 + 0}
	self.花豆消耗 = 0
	self.彩果消耗 = 0
	if self.染色参数 == nil or #self.染色参数 < 3 then
		SendMessage(UserData[id].连接id, 7, "#y/染色参数异常，请重新选择染色方案")
		return 0
	end
 if self.染色参数[1] ~= UserData[id].角色.染色.a then
      if self.染色参数[1]<3 then
      self.花豆消耗=self.花豆消耗+10
      else
      self.彩果消耗=self.彩果消耗+10
      end
  end
  if self.染色参数[2] ~= UserData[id].角色.染色.b then
      if self.染色参数[2]<3 then
      self.花豆消耗=self.花豆消耗+10
      else
      self.彩果消耗=self.彩果消耗+10
      end
  end
    if self.染色参数[3] ~= UserData[id].角色.染色.c then
      if self.染色参数[3]<3 then
      self.花豆消耗=self.花豆消耗+10
      else
      self.彩果消耗=self.彩果消耗+10
      end
  end
	self.花豆计算 = {}
	self.花豆满足 = false
	self.彩果计算 = {}
	self.彩果满足 = false
	 if self.花豆消耗>0 then
	   for n=1,80 do
	     if self.花豆消耗>0 then
	       if self.花豆消耗>0 and  UserData[id].角色.道具.包裹[n]~=nil and UserData[id].物品[UserData[id].角色.道具.包裹[n]]~=nil and  UserData[id].物品[UserData[id].角色.道具.包裹[n]].名称=="花豆" then
	         if UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量==nil then
	            self.花豆消耗=self.花豆消耗-1
	            self.花豆计算[#self.花豆计算+1]={格子=n,数量=1}
	          elseif UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量>=self.花豆消耗 then
	            self.花豆计算[#self.花豆计算+1]={格子=n,数量=self.花豆消耗}
	            self.花豆消耗=0
	          else
	           self.花豆计算[#self.花豆计算+1]={格子=n,数量=UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量}
	           self.花豆消耗=self.花豆消耗-UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量
	           end
	         if self.花豆消耗==0 then
	            self.花豆满足=true
	           end
	         end
	       end
	     end
	   end
	 if self.彩果消耗>0 then
	   for n=1,80 do
	     if self.彩果消耗>0 then
	       if self.彩果消耗>0 and  UserData[id].角色.道具.包裹[n]~=nil and UserData[id].物品[UserData[id].角色.道具.包裹[n]].名称=="彩果" then
	         if UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量==nil then
	            self.彩果消耗=self.彩果消耗-1
	            self.彩果计算[#self.彩果计算+1]={格子=n,数量=1}
	          elseif UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量>=self.彩果消耗 then
	            self.彩果计算[#self.彩果计算+1]={格子=n,数量=self.彩果消耗}
	            self.彩果消耗=0
	          else
	           self.彩果计算[#self.彩果计算+1]={格子=n,数量=UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量}
	           self.彩果消耗=self.彩果消耗-UserData[id].物品[UserData[id].角色.道具.包裹[n]].数量
	           end
	         if self.彩果消耗==0 then
	            self.彩果满足=true
	           end
	         end
	       end
	     end
	   end

   if self.彩果消耗>0  and self.花豆消耗>0  then
    SendMessage(UserData[id].连接id,7,"#Y/您还需要#R/"..self.彩果消耗.."#Y/个彩果和#R/"..self.花豆消耗.."#Y/个花豆才能染色")
   return 0
   elseif self.彩果消耗>0 then
   SendMessage(UserData[id].连接id,7,"#Y/您还需要#R/"..self.彩果消耗.."#Y/个彩果才能染色")
   return 0
   elseif self.花豆消耗>0 then
    SendMessage(UserData[id].连接id,7,"#Y/您还需要#R/"..self.花豆消耗.."#Y/个花豆才能染色")
    return 0
  else
    if #self.花豆计算>0 then
      for n=1,#self.花豆计算 do
       self.临时格子=self.花豆计算[n].格子
       self.临时id=UserData[id].角色.道具.包裹[self.临时格子]
       if UserData[id].物品[self.临时id].数量==nil then
          UserData[id].物品[self.临时id]=0
          UserData[id].角色.道具.包裹[self.临时格子]=nil
        else
          UserData[id].物品[self.临时id].数量=UserData[id].物品[self.临时id].数量-self.花豆计算[n].数量
          if UserData[id].物品[self.临时id].数量<1 then

           UserData[id].物品[self.临时id]=0
           UserData[id].角色.道具.包裹[self.临时格子]=nil
            end
         end
        end
      end
    if #self.彩果计算>0 then
      for n=1,#self.彩果计算 do
       self.临时格子=self.彩果计算[n].格子
       self.临时id=UserData[id].角色.道具.包裹[self.临时格子]
       if UserData[id].物品[self.临时id].数量==nil then
          UserData[id].物品[self.临时id]=0
          UserData[id].角色.道具.包裹[self.临时格子]=nil
        else
          UserData[id].物品[self.临时id].数量=UserData[id].物品[self.临时id].数量-self.彩果计算[n].数量
          if UserData[id].物品[self.临时id].数量<1 then
           UserData[id].物品[self.临时id]=0
           UserData[id].角色.道具.包裹[self.临时格子]=nil
            end
         end
        end
      end
   end
	UserData[id].角色.染色.a = self.染色参数[1]
	UserData[id].角色.染色.b = self.染色参数[2]
	UserData[id].角色.染色.c = self.染色参数[3]
    MapControl:MapSend(id,1016,RoleControl:获取地图数据(UserData[id]),UserData[id].地图)
	SendMessage(UserData[id].连接id, 2020, {UserData[id].角色.染色方案,UserData[id].角色.染色.a,UserData[id].角色.染色.b,UserData[id].角色.染色.c})
	SendMessage(UserData[id].连接id, 3006, "66")
	SendMessage(UserData[id].连接id, 7, "#y/染色成功")
 end