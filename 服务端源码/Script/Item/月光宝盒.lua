--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-12-03 23:45:45
--======================================================================--
function ItemControl:月光宝盒使用(id, 格子)
	local 临时id = UserData[id].角色.道具["法宝"][格子]
	if UserData[id].物品[临时id] == nil or UserData[id].物品[临时id] == 0 then
		SendMessage(UserData[id].连接id, 7, "#Y/你似乎并没有这样的道具")
    elseif UserData[id].物品[临时id].名称 ~= "月光宝盒" then
    	SendMessage(UserData[id].连接id, 7, "#Y/数据错误")
	elseif  UserData[id].物品[临时id].坐标  then
		UserData[id].月光格子 = 格子
		SendMessage(UserData[id].连接id,20,{"","月光宝盒","你当前的坐标为:"..UserData[id].物品[临时id].坐标.地图.."  坐标X:"..UserData[id].物品[临时id].坐标.x.."  坐标Y:"..UserData[id].物品[临时id].坐标.y,{"我要传送","我要重新定标"}})
    else

    	UserData[id].物品[临时id].坐标 ={时间=0,编号=UserData[id].地图,地图=MapData[UserData[id].地图].名称,x=math.floor(UserData[id].角色.地图数据.x/20),y=math.floor(UserData[id].角色.地图数据.y/20)}
    	SendMessage(UserData[id].连接id, 7, "#Y/定标成功!")
	end
 end
function ItemControl:影蛊使用(id, 格子)
	local 临时id = UserData[id].角色.道具["法宝"][格子]
	if UserData[id].物品[临时id] == nil or UserData[id].物品[临时id] == 0 then
		SendMessage(UserData[id].连接id, 7, "#Y/你似乎并没有这样的道具")
    elseif UserData[id].物品[临时id].名称 ~= "影蛊" then
    	SendMessage(UserData[id].连接id, 7, "#Y/数据错误")

	elseif  UserData[id].物品[临时id].影蛊  then

	 if UserData[id].物品[临时id].影蛊==0 or  (((os.time() - UserData[id].物品[临时id].影蛊) / 60) > 60-UserData[id].物品[临时id].层数*2) then
			 UserData[id].物品[临时id].影蛊 = os.time()
               SendMessage(UserData[id].连接id, 15, {"影蛊","请输入你需要查找的玩家ID号码"})
      else
      	 SendMessage(UserData[id].连接id, 7, "#Y/你还剩"..math.floor((60-UserData[id].物品[临时id].层数*2)-((os.time() - UserData[id].物品[临时id].影蛊) / 60))  .."分才能再次使用影蛊,提高法宝等级可以减少冷却时间")
	  end
    else
        UserData[id].物品[临时id].影蛊=os.time()
   			SendMessage(UserData[id].连接id, 15, {"影蛊","请输入你需要查找的玩家ID号码"})
	end
 end
function ItemControl:月光宝盒处理(id, 内容)
 	if UserData[id].月光格子 then
	      local 临时id = UserData[id].角色.道具["法宝"][UserData[id].月光格子]
		if UserData[id].物品[临时id] == nil or UserData[id].物品[临时id] == 0 then
			SendMessage(UserData[id].连接id, 7, "#Y/你似乎并没有这样的道具")
	    elseif UserData[id].物品[临时id].名称 ~= "月光宝盒" then
	    	SendMessage(UserData[id].连接id, 7, "#Y/数据错误")
	    elseif 内容 == "我要重新定标" then
              local ls=UserData[id].物品[临时id].坐标.时间
	    	   UserData[id].物品[临时id].坐标 ={时间=ls,编号=UserData[id].地图,地图=MapData[UserData[id].地图].名称,x=math.floor(UserData[id].角色.地图数据.x/20),y=math.floor(UserData[id].角色.地图数据.y/20)}
	    	SendMessage(UserData[id].连接id, 7, "#Y/定标成功!")
	    elseif 内容 == "我要传送" then
             if UserData[id].队伍~=0 and UserData[id].队长==false then
                SendMessage(UserData[id].连接id,7,"#Y/只有队长才可使用此道具")
                 return true
            end
			  if UserData[id].物品[临时id].坐标.时间==0 or  (((os.time() - UserData[id].物品[临时id].坐标.时间) / 60) > 60-UserData[id].物品[临时id].层数*2) then
					if RoleControl:取飞行限制(UserData[id]) then
					 MapControl:Jump(id,UserData[id].物品[临时id].坐标.编号,UserData[id].物品[临时id].坐标.x,UserData[id].物品[临时id].坐标.y)
					 UserData[id].物品[临时id].坐标.时间 = os.time()
					else
					   return
					end
		      else
		      	 SendMessage(UserData[id].连接id, 7, "#Y/你还剩"..math.floor((60-UserData[id].物品[临时id].层数*2)-((os.time() - UserData[id].物品[临时id].坐标.时间) / 60))  .."分才能再次使用月光宝盒,提高法宝等级可以减少冷却时间")
			  end
	    else
	       SendMessage(UserData[id].连接id, 7, "#Y/数据错误请重新使用一次")
	    end
   end
 end