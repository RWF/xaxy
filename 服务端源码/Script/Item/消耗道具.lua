-- @Author: 作者QQ381990860
-- @Date:   2021-09-07 00:54:26
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2023-08-06 21:01:37
-- @Author: 作者QQ381990860
-- @Date:   2021-09-07 00:54:26
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-09-08 21:35:21
local 物品={
摄妖香 = function(user,Item,PitchOn,Text)
        if RoleControl:GetTaskID(user,"摄妖香") ~= 0 then
            任务数据[RoleControl:GetTaskID(user,"摄妖香")].起始 = os.time()
            TaskControl:刷新追踪任务信息(user.id)
        else
            TaskControl:添加摄妖香(user.id)
        end
        SendMessage(user.连接id, 7, "#y/你使用了摄妖香")
end,
清灵净瓶 = function(user,Item,PitchOn,Text)
       local 可用格子=RoleControl:取可用道具格子(user,"包裹")
        if 可用格子==0 then
            SendMessage(user.连接id,7,"#y/你的包裹已满,无法使用")
            return true
        end
        local 随机瓶子={"高级清灵仙露","中级清灵仙露","初级清灵仙露"}
        ItemControl:GiveItem(user.id,随机瓶子[math.random(1,#随机瓶子)])
end,
洞冥草 = function(user,Item,PitchOn,Text)
        if RoleControl:GetTaskID(user,"摄妖香") ~= 0 then
            RoleControl:取消任务(user,RoleControl:GetTaskID(user,"摄妖香"))
            任务数据[RoleControl:GetTaskID(user,"摄妖香")] = nil
            SendMessage(user.连接id, 7, "#y/你取消了摄妖香效果")
        else
            SendMessage(user.连接id, 7, "#y/你似乎并没有使用过摄妖香")
            return true
        end
end,
鬼谷子 = function(user,Item,PitchOn,Text)
        if user.角色.阵法[Item.技能] then
            SendMessage(user.连接id, 7, "#y/你已经学习过此阵型，请勿重复学习")
            return true
        else
            if Item.技能 ==nil then 
              SendMessage(user.连接id, 7, "#y/数据已经更新物品已经失效")
              return
            end
            user.角色.阵法[Item.技能] = true
            SendMessage(user.连接id, 7, "#y/你学会了如何使用" .. Item.技能)
        end
end,
天眼通符 = function(user,Item,PitchOn,Text)
            if DebugMode then
              
              TaskControl:设置抓鬼任务(user.id)
            end
            if RoleControl:取飞行限制(user)==false then
                SendMessage(user.连接id, 7, "#y/无法使用飞行道具")
                return true
            elseif RoleControl:GetTaskID(user,"抓鬼") ~= 0  then
                local 任务id = RoleControl:GetTaskID(user,"抓鬼")
                MapControl:Jump(user.id, 任务数据[任务id].地图编号,任务数据[任务id].坐标.x,任务数据[任务id].坐标.y)
            elseif  RoleControl:GetTaskID(user,"鬼王") ~= 0  then
                local 任务id = RoleControl:GetTaskID(user,"鬼王")
                MapControl:Jump(user.id, 任务数据[任务id].地图编号,任务数据[任务id].坐标.x,任务数据[任务id].坐标.y)
            else
                SendMessage(user.连接id, 7, "#y/请先领取抓鬼任务")
                return true
            end
            SendMessage(user.连接id, 7, "#y/你消耗了一张天眼通符")
end,
海马 = function(user,Item,PitchOn,Text)
           RoleControl:添加体力(user,100)
           RoleControl:添加活力(user,100)
end,

聚灵袋 = function(user,Item,PitchOn,Text)
   user.角色.法宝灵气=user.角色.法宝灵气+40
      SendMessage(user.连接id,7,"#Y/你的法宝增加了40点灵气")
       -- SendMessage(user.连接id, 7, "#y/你获得了" .. 数额 .. "点法宝灵气")
    --SendMessage(user.连接id, 9, "#xt/#w/你获得了" .. 数额 .. "点法宝灵气")

          
         
end,
灵气礼包 = function(user,Item,PitchOn,Text)


   if not user.角色.神器 then
      SendMessage(user.连接id,7,"#Y/你还没有神器")
       return true
   end
      user.角色.神器.灵气 =user.角色.神器.灵气+20
      SendMessage(user.连接id,7,"#Y/你的神器增加了20点灵气")
     -- self:获取神器数据(user)
end,

超级聚灵袋 = function(user,Item,PitchOn,Text)
   user.角色.法宝灵气=user.角色.法宝灵气+360
      SendMessage(user.连接id,7,"#Y/你的法宝增加了360点灵气")
       -- SendMessage(user.连接id, 7, "#y/你获得了" .. 数额 .. "点法宝灵气")
    --SendMessage(user.连接id, 9, "#xt/#w/你获得了" .. 数额 .. "点法宝灵气")
       
end,

灵饰60级礼包 = function(user,Item,PitchOn,Text)

                if RoleControl:取可用格子数量(user,"包裹") < 4 then
                    SendMessage(user.连接id, 7, "#y/请先预留出4个道具空间")
                    return true
                else
                    EquipmentControl:取灵饰礼包(user.id,60)
                     SendMessage(user.连接id, 7, "#y/灵光一现你打开了礼包")
                     


                end

end,
灵饰80级礼包 = function(user,Item,PitchOn,Text)

                if RoleControl:取可用格子数量(user,"包裹") < 4 then
                    SendMessage(user.连接id, 7, "#y/请先预留出4个道具空间")
                    return true
                else
                    EquipmentControl:取灵饰礼包(user.id,80)

                end

end,
灵饰100级礼包 = function(user,Item,PitchOn,Text)

                if RoleControl:取可用格子数量(user,"包裹") < 4 then
                    SendMessage(user.连接id, 7, "#y/请先预留出4个道具空间")
                    return true
                else
                    EquipmentControl:取灵饰礼包(user.id,100)

                end

end,
灵饰120级礼包 = function(user,Item,PitchOn,Text)

                if RoleControl:取可用格子数量(user,"包裹") < 4 then
                    SendMessage(user.连接id, 7, "#y/请先预留出4个道具空间")
                    return true
                else
                    EquipmentControl:取灵饰礼包(user.id,120)

                end

end,
灵饰140级礼包 = function(user,Item,PitchOn,Text)

                if RoleControl:取可用格子数量(user,"包裹") < 1 then
                    SendMessage(user.连接id, 7, "#y/请先预留出4个道具空间")
                    return true
                else
                    EquipmentControl:取灵饰礼包(user.id,140)

                end

end,




灵饰160级礼包 = function(user,Item,PitchOn,Text)

                if RoleControl:取可用格子数量(user,"包裹") < 4 then
                    SendMessage(user.连接id, 7, "#y/请先预留出4个道具空间")
                    return true
                else
                    EquipmentControl:取灵饰礼包(user.id,160)

                end

end,
聚气袋 = function(user,Item,PitchOn,Text)


   if not user.角色.神器 then
      SendMessage(user.连接id,7,"#Y/你还没有神器")
       return true
   end
      user.角色.神器.灵气 =user.角色.神器.灵气+30
      SendMessage(user.连接id,7,"#Y/你的神器增加了30点灵气")
     -- self:获取神器数据(user)
end,   

	超级九转金丹 = function(user,Item,PitchOn,Text)
        if user.角色.人物修炼[user.角色.人物修炼.当前].等级>=user.角色.人物修炼[user.角色.人物修炼.当前].上限 then
            SendMessage(user.连接id,7,"#Y/你修炼等级已经达到上限")
            return true
        elseif user.角色.等级 < 40 then
            SendMessage(user.连接id,7,"#Y/等级达到40级才能使用")
            return true
        else
            RoleControl:添加人物修炼经验(user,300)
        end
end,

九转金丹 = function(user,Item,PitchOn,Text)
        if user.角色.人物修炼[user.角色.人物修炼.当前].等级>=user.角色.人物修炼[user.角色.人物修炼.当前].上限 then
            SendMessage(user.连接id,7,"#Y/你修炼等级已经达到上限")
            return true
        elseif user.角色.等级 < 40 then
            SendMessage(user.连接id,7,"#Y/等级达到40级才能使用")
            return true
        else
            RoleControl:添加人物修炼经验(user,150)
        end
end,

超级修炼果 = function(user,Item,PitchOn,Text)
        if user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].上限 <= user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].等级 then
            SendMessage(user.连接id, 7, "#y/你当前的这项修炼等级已达上限，无法再服用修炼果")
            return true
        elseif user.角色.等级 < 65 then
             SendMessage(user.连接id,7,"#Y/等级达到65级才能使用")
             return true
        else
            RoleControl:添加召唤兽修炼经验(user,300)
        end
end,

	修炼果 = function(user,Item,PitchOn,Text)
        if user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].上限 <= user.角色.召唤兽修炼[user.角色.召唤兽修炼.当前].等级 then
            SendMessage(user.连接id, 7, "#y/你当前的这项修炼等级已达上限，无法再服用修炼果")
            return true
        elseif user.角色.等级 < 65 then
             SendMessage(user.连接id,7,"#Y/等级达到65级才能使用")
             return true
        else
            RoleControl:添加召唤兽修炼经验(user,150)
        end
end,
天机培元丹 = function(user,Item,PitchOn,Text)
      if RoleControl:GetTaskID(user,"天机培元丹") ~= 0 then
        任务数据[RoleControl:GetTaskID(user,"天机培元丹")].结束 = 任务数据[RoleControl:GetTaskID(user,"天机培元丹")].结束+3600*3
        SendMessage(user.连接id, 7, "#y/你使用了天机培元丹")
        TaskControl:刷新追踪任务信息(user.id)
      else
        TaskControl:添加天机培元丹(user.id)
        SendMessage(user.连接id, 7, "#y/你使用了天机培元丹")
      end
end,
九霄清心丸 = function(user,Item,PitchOn,Text)
          if RoleControl:GetTaskID(user,"九霄清心丸") ~= 0 then
            任务数据[RoleControl:GetTaskID(user,"九霄清心丸")].结束 = 任务数据[RoleControl:GetTaskID(user,"九霄清心丸")].结束+3600
            SendMessage(user.连接id, 7, "#y/你使用了九霄清心丸")
            TaskControl:刷新追踪任务信息(user.id)
          else
            TaskControl:添加九霄清心丸(user.id)
            SendMessage(user.连接id, 7, "#y/你使用了九霄清心丸")
          end
end,
怪物卡片 = function(user,Item,PitchOn,Text)
            if CardData[Item.类型].卡片等级>1  and CardData[Item.类型].卡片等级>user.角色.剧情技能[4].等级 then
            SendMessage(user.连接id,7,"#Y/你的变化之术等级不够")
            return true
            end
            user.角色.变身.造型=Item.类型
            user.角色.变身.等级=CardData[Item.类型].卡片等级
             user.角色.变身.技能=nil
            -- user.角色.变身.技能=置技能(Item.技能)
            if  CardData[Item.类型].技能~="无" then  
               user.角色.变身.技能=置技能(CardData[Item.类型].技能)
            end
            user.角色.变身.属性.数值 = CardData[Item.类型].数值
            user.角色.变身.属性.类型 = CardData[Item.类型].类型
            user.角色.变身.属性.属性 = CardData[Item.类型].三维
            TaskControl:添加变身卡(user.id)
            RoleControl:刷新战斗属性(user)
            SendMessage(user.连接id, 2010, RoleControl:获取地图数据(user))
            MapControl:MapSend(user.id, 1016, RoleControl:获取地图数据(user), user.地图)
            SendMessage(user.连接id,7,"#Y/你使用了变身卡")
            Item.使用次数=Item.使用次数-1
            if Item.使用次数> 0 then
                 return true
            end
end,
新手玩家礼包 = function(user,Item,PitchOn,Text)
        if Item.绑定id~=user.角色.id then
          SendMessage(user.连接id,7,"#y/该礼包只能由id为"..Item.绑定id.."的玩家使用")
           return true
        elseif Item.等级>user.角色.等级 then
           SendMessage(user.连接id,7,"#y/你当前的等级不足以打开此礼包")
           return true
        else
            if Item.等级==10 then
                if RoleControl:取可用格子数量(user,"包裹") < 19 then
                    SendMessage(user.连接id, 7, "#y/请先预留出19个道具空间")
                    return true
                    elseif #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    -- EquipmentControl:取装备礼包(user.id,160,true,1,true)--24小时
                    -- EquipmentControl:取装备礼包(user.id,120,true,nil,true)
                    EquipmentControl:取装备礼包(user.id,60,true,nil,true)

                    --------限时区

                    EquipmentControl:定制灵饰(user.id,60,"手镯","防御",30,"1","气血=50@防御=50@伤害=20@速度=10",10)
                    -- EquipmentControl:定制灵饰(user.id,60,"戒指","速度",30,"1","气血=50@防御=50@伤害=20@速度=10",10)
                    -- EquipmentControl:定制灵饰(user.id,60,"耳饰","速度",30,"1","气血=50@防御=50@法术伤害=20@速度=10",10)
                    -- EquipmentControl:定制灵饰(user.id,60,"佩饰","气血",50,"1","气血=50@防御=50@伤害=20@速度=10",10)
                      ItemControl:GiveItem(user.id,"冰灵蝶翼·月笼",nil,nil,nil)


                   -----------------


                    local 临时资质={攻资=1100 ,体资=3500 ,法资=2500 ,防资=1200 ,速资=1200 ,躲资=1400,成长=1.01}
                    user.召唤兽:创建召唤兽("超级腾蛇","宝宝",临时资质,30,{"连击","高级吸血","必杀","偷袭","高级强力"})
                    
                    SendMessage(user.连接id, 7, "#y/你获得了一只新手宠物,未来可期与你同行")
                    ItemControl:GiveItem(user.id,"飞行符",nil,nil,30)
                    ItemControl:GiveItem(user.id,"红色合成旗",nil,nil,1)

                    
                                             

                    for i=1,4 do
                    ItemControl:GiveItem(user.id,"灵宝图鉴",160,nil,1,nil)
                    end
                    for i=1,2 do
                    ItemControl:GiveItem(user.id,"神兵图鉴",160,nil,1,nil)
                    end
                    TaskControl:添加秘制红罗羹(user.id)
                    RoleControl:添加储备(user,50000,"新手礼包")
                    --RoleControl:添加仙玉(user,80000000,"新手礼包")
                    --RoleControl:添加银子(user,8000000000000,"新手礼包")
                    SendMessage(user.连接id, 3006, "66")
                end

          -- elseif Item.等级==50 then
          --       if RoleControl:取可用格子数量(user,"包裹") < 6 then
          --       SendMessage(user.连接id, 7, "#y/请先预留出6个道具空间")
          --       return 0
          --       else
          --         EquipmentControl:取装备礼包(user.id,100,true,nil,true)
                 
          --       end
          else

                   
                    RoleControl:添加经验(user,Item.等级*150000,"新手礼包")
                    RoleControl:添加储备(user,Item.等级*50000,"新手礼包")
            end
            Item.等级=Item.等级+10
            if Item.等级<60 then
                return true
            end
         end
end,
龙宫孩子礼包 = function(user,Item,PitchOn,Text)

                if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else

                    local 临时资质={攻资=1600 ,体资=7500 ,法资=3000 ,防资=1600 ,速资=1500 ,躲资=1500,成长=1.35}
                    user.召唤兽:创建召唤兽("白豹","神兽",临时资质,0,{"龙卷雨击","张弛有道","高级法术暴击","高级法术波动","高级法术连击","高级魔之心","逍遥游","龙魂","浮云神马","须弥真言","灵能激发", "高级神佑复生"})
                    SendMessage(user.连接id, 7, "#y/你收养了一个可爱的孩子,好好保护他吧")



              end
          end,

魔王孩子礼包 = function(user,Item,PitchOn,Text)

                   if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else

                    local 临时资质={攻资=1600 ,体资=7500 ,法资=3000 ,防资=1600 ,速资=1500 ,躲资=1500,成长=1.35}
                    user.召唤兽:创建召唤兽("白豹","神兽",临时资质,0,{"飞砂走石","张弛有道","高级法术暴击","高级法术波动","高级法术连击","高级魔之心","逍遥游","龙魂","浮云神马","须弥真言","灵能激发", "高级神佑复生"})
                    SendMessage(user.连接id, 7, "#y/你收养了一个可爱的孩子,好好保护他吧")



              end
          end,
神木孩子礼包 = function(user,Item,PitchOn,Text)

                   if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else

                    local 临时资质={攻资=1600 ,体资=7500 ,法资=3000 ,防资=1600 ,速资=1500 ,躲资=1500,成长=1.35}
                    user.召唤兽:创建召唤兽("白豹","神兽",临时资质,0,{"落叶萧萧","张弛有道","高级法术暴击","高级法术波动","高级法术连击","高级魔之心","逍遥游","龙魂","浮云神马","须弥真言","灵能激发", "高级神佑复生"})
                    SendMessage(user.连接id, 7, "#y/你收养了一个可爱的孩子,好好保护他吧")



              end
          end,
天宫孩子礼包 = function(user,Item,PitchOn,Text)

                   if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else

                    local 临时资质={攻资=1600 ,体资=7500 ,法资=3000 ,防资=1600 ,速资=1500 ,躲资=1500,成长=1.35}
                    user.召唤兽:创建召唤兽("白豹","神兽",临时资质,0,{"天雷斩","高级必杀","高级偷袭","高级吸血","凭风借力","昼伏夜出","高级敏捷","出其不意","浮云神马","无赦魔决","津津有味", "高级神佑复生"})
                    SendMessage(user.连接id, 7, "#y/你收养了一个可爱的孩子,好好保护他吧")



              end
          end,
大唐孩子礼包 = function(user,Item,PitchOn,Text)

                   if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else

                    local 临时资质={攻资=1600 ,体资=7500 ,法资=3000 ,防资=1600 ,速资=1500 ,躲资=1500,成长=1.35}
                    user.召唤兽:创建召唤兽("白豹","神兽",临时资质,0,{"横扫千军","高级必杀","高级偷袭","高级吸血","凭风借力","昼伏夜出","高级敏捷","出其不意","浮云神马","无赦魔决","津津有味", "高级神佑复生"})
                    SendMessage(user.连接id, 7, "#y/你收养了一个可爱的孩子,好好保护他吧")



              end
          end,
 五庄孩子礼包 = function(user,Item,PitchOn,Text)

                   if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else

                    local 临时资质={攻资=1600 ,体资=7500 ,法资=3000 ,防资=1600 ,速资=1500 ,躲资=1500,成长=1.35}
                    user.召唤兽:创建召唤兽("白豹","神兽",临时资质,0,{"烟雨剑法","高级必杀","高级偷袭","高级吸血","凭风借力","昼伏夜出","高级敏捷","出其不意","浮云神马","无赦魔决","津津有味", "高级神佑复生"})
                    SendMessage(user.连接id, 7, "#y/你收养了一个可爱的孩子,好好保护他吧")



              end
          end,
化生孩子礼包 = function(user,Item,PitchOn,Text)

                   if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else

                    local 临时资质={攻资=1600 ,体资=7500 ,法资=3000 ,防资=1600 ,速资=1500 ,躲资=1500,成长=1.35}
                    user.召唤兽:创建召唤兽("白豹","神兽",临时资质,0,{"推气过宫","我佛慈悲","溜之大吉","高级否定信仰","逍遥游","移花接木","高级敏捷","高级防御","浮云神马","高级招架","净台妙谛", "高级神佑复生"})
                    SendMessage(user.连接id, 7, "#y/你收养了一个可爱的孩子,好好保护他吧")



              end
          end,
普陀孩子礼包 = function(user,Item,PitchOn,Text)

                   if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else

                    local 临时资质={攻资=1600 ,体资=7500 ,法资=3000 ,防资=1600 ,速资=1500 ,躲资=1500,成长=1.35}
                    user.召唤兽:创建召唤兽("白豹","神兽",临时资质,0,{"普渡众生","杨柳甘露","溜之大吉","高级否定信仰","逍遥游","移花接木","高级敏捷","高级防御","浮云神马","高级招架","净台妙谛", "高级神佑复生"})
                    SendMessage(user.连接id, 7, "#y/你收养了一个可爱的孩子,好好保护他吧")



              end
          end,
首冲攻宠礼包 = function(user,Item,PitchOn,Text)
                
                   if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    
                    local 临时资质={攻资=1400 ,体资=4000 ,法资=2800 ,防资=1400 ,速资=1400 ,躲资=1400,成长=1.2}
                    user.召唤兽:创建召唤兽("超级小熊猫","宝宝",临时资质,0,{"高级必杀","高级强力","高级连击","高级偷袭","高级吸血","高级夜战","高级神佑复生"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只首冲宠物,未来可期与你同行")
                    

              
              end
          end,

神兽礼包 = function(user,Item,PitchOn,Text)

if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
return true
end
local  随机神兽 ={"超级土地公公-法术型","超级六耳猕猴-法术型","超级神鸡-法术型","超级玉兔-法术型","超级神猴-法术型","超级神龙-法术型","超级神羊-法术型","超级孔雀-法术型","超级灵狐-法术型","超级筋斗云-法术型","超级神鸡-物理型","超级玉兔-物理型","超级神猴-物理型","超级土地公公-物理型","超级神羊-物理型","超级六耳猕猴-物理型","超级神马-物理型","超级神马-法术型","超级孔雀-物理型","超级灵狐-物理型","超级筋斗云-物理型","超级神龙-物理型","超级麒麟-物理型","超级麒麟-法术型","超级大鹏-法术型","超级大鹏-物理型","超级神蛇-法术型","超级神蛇-物理型","超级赤焰兽-法术型","超级赤焰兽-物理型","超级白泽-法术型","超级白泽-物理型","超级灵鹿-法术型","超级灵鹿-物理型","超级大象-法术型","超级大象-物理型","超级金猴-法术型","超级金猴-物理型","超级大熊猫-法术型","超级大熊猫-物理型","超级泡泡-法术型","超级泡泡-物理型","超级神兔-法术型","超级神兔-物理型","超级神虎-法术型","超级神虎-物理型","超级神牛-法术型","超级神牛-物理型","超级海豚-法术型","超级海豚-物理型","超级人参娃娃-法术型","超级人参娃娃-物理型","超级青鸾-法术型","超级青鸾-物理型","超级腾蛇-法术型","超级腾蛇-物理型","超级神狗-法术型","超级神狗-物理型","超级神鼠-法术型","超级神鼠-物理型","超级神猪-法术型","超级神猪-物理型","超级孙小圣-法术型","超级孙小圣-物理型","超级小白龙-法术型","超级小白龙-物理型","超级猪小戒-法术型","超级猪小戒-物理型","超级飞天-法术型","超级飞天-物理型"}
local  随机名称 = 随机神兽[math.random(1, #随机神兽)]
table.insert(user.召唤兽.数据,AddPet(随机名称,"神兽"))
user.召唤兽:刷新属性(#user.召唤兽.数据)
SendMessage(user.连接id, 7, "#y/恭喜你兑换到了"..随机名称)
广播消息("#hd/".."#S/(神兽礼包)".."#R/ "..user.角色.名称.."#Y/拆开神兽礼包时，意外获得了#g/"..随机名称)
end,

首冲法宠礼包 = function(user,Item,PitchOn,Text)
              
                   if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    
                    local 临时资质={攻资=1400 ,体资=4000 ,法资=2800 ,防资=1400 ,速资=1400 ,躲资=1400,成长=1.2}
                    user.召唤兽:创建召唤兽("超级小熊猫","宝宝",临时资质,0,{"高级法术连击","高级法术暴击","高级法术波动","高级神佑复生","泰山压顶","高级魔之心","高级精神集中"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只首冲宠物,未来可期与你同行")
                    
              
              end
          end,


          定制宠物礼包 = function(user,Item,PitchOn,Text)
              
                   if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    
                    local 临时资质={攻资=1500 ,体资=5500 ,法资=3000 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("超级小鲲鹏","宝宝",临时资质,0,{"高级法术连击","高级法术暴击","高级法术波动","高级神佑复生","泰山压顶","高级魔之心","地狱烈火","奔雷咒","水漫金山","光照万象","高级神迹","高级敏捷"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只天赐宠物,未来可期与你同行")
                   广播消息("#hd/".."#S/(定制宠物礼包)".."#R/ "..user.角色.名称.."#Y/拆开定制礼包时，意外获得了#g/关照定制宠物一个") 
                    发送游戏公告("#hd/".."#S/(定制宠物礼包)".."#R/ "..user.角色.名称.."#Y/拆开定制礼包时，意外获得了GM奖励的#g/关照定制宠物一个");
              
              end
          end,

坐骑礼包 = function(user,Item,PitchOn,Text)
        RoleControl:添加随机坐骑(user)

end,
灵兜兜 = function(user,Item,PitchOn,Text)

    if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true

    end
         if  Item.数量 >= 999  then
              Item.数量 =Item.数量-998
              local  随机神兽 ={"超级鲲鹏-法术型","超级鲲鹏-物理型"}
              local  随机名称 = 随机神兽[math.random(1, #随机神兽)]
              table.insert(user.召唤兽.数据,AddPet(随机名称,"神兽"))
              SendMessage(user.连接id, 7, "#y/恭喜你兑换到了"..随机名称)
              广播消息("#hd/".."#S/(灵兜兜)".."#R/ "..user.角色.名称.."#Y/使用灵兜兜时，意外获得了#g/"..随机名称)
         else
                SendMessage(user.连接id, 7, "#y/需要集齐999个灵兜兜")
                  return true
         end
  end,
卡片礼包 = function(user,Item,PitchOn,Text)
      local 卡片 ={"海星","狸","章鱼","大海龟","大蝙蝠","赌徒","海毛虫","护卫","巨蛙","强盗","山贼","树怪" ,"蛤蟆精","黑熊","狐狸精","花妖",
                         "老虎","羊头怪","骷髅怪","狼","牛妖","虾兵","小龙女","蟹将","野鬼","龟丞相","黑熊精","僵尸","马面","牛头","兔子怪","蜘蛛精","白熊",
                         "进阶白熊","古代瑞兽","进阶古代瑞兽","黑山老妖","进阶黑山老妖","蝴蝶仙子","进阶蝴蝶仙子","雷鸟人","进阶雷鸟人","地狱战神","进阶地狱战神",
                         "风伯","进阶风伯","天兵","进阶天兵","天将","进阶天将" ,"凤凰" ,"进阶凤凰","雨师","进阶雨师","蛟龙","进阶蛟龙","蚌精","进阶蚌精",
                         "进阶碧水夜叉","碧水夜叉","进阶鲛人","百足将军","进阶百足将军","锦毛貂精","进阶锦毛貂精","镜妖","进阶镜妖","泪妖","进阶泪妖","千年蛇魅",
                         "进阶千年蛇魅","如意仙子","进阶如意仙子","鼠先锋","进阶鼠先锋","星灵仙子","进阶星灵仙子","巡游天神","进阶巡游天神","野猪精","进阶野猪精",
                         "芙蓉仙子","进阶芙蓉仙子","进阶犀牛将军人形","犀牛将军人形","犀牛将军兽形","进阶犀牛将军兽形","阴阳伞","进阶阴阳伞","进阶巴蛇","巴蛇","进阶龙龟",
                         "龙龟","进阶大力金刚","大力金刚","进阶鬼将","鬼将","进阶红萼仙子","红萼仙子","葫芦宝贝","进阶葫芦宝贝","画魂","进阶画魂","机关鸟",
                         "进阶机关鸟","机关人","进阶机关人","机关兽","进阶机关兽","金饶僧" ,"进阶金饶僧","进阶净瓶女娲","净瓶女娲","连弩车","进阶连弩车","进阶灵鹤",
                         "灵鹤","进阶灵符女娲","灵符女娲","进阶律法女娲","律法女娲","琴仙","进阶琴仙","进阶踏云兽" ,"踏云兽","进阶雾中仙","雾中仙","进阶吸血鬼",
                         "吸血鬼","进阶噬天虎" ,"噬天虎","进阶炎魔神","炎魔神","进阶幽灵","幽灵" ,"进阶幽萤娃娃","幽萤娃娃","夜罗刹" ,"进阶夜罗刹","超级泡泡",
                         "超级大熊猫","进阶毗舍童子","毗舍童子","长眉灵猴","进阶长眉灵猴","进阶持国巡守","持国巡守","进阶混沌兽","混沌兽","金身罗汉","进阶金身罗汉",
                         "巨力神猿","进阶巨力神猿","狂豹人形","进阶狂豹人形","狂豹兽形","进阶狂豹兽形","曼珠沙华","进阶曼珠沙华" ,"猫灵人形","进阶猫灵人形","猫灵兽形","进阶猫灵兽形",
                         "藤蔓妖花","进阶藤蔓妖花","进阶蝎子精","蝎子精","修罗傀儡鬼","进阶修罗傀儡鬼","修罗傀儡妖","进阶修罗傀儡妖","增长巡守","进阶增长巡守",
                         "进阶真陀护法","真陀护法","蜃气妖","进阶蜃气妖","灵灯侍者","进阶灵灯侍者","般若天女","进阶般若天女"
                        }
       ItemControl:GiveItem(user.id,"怪物卡片",nil,卡片[math.random(#卡片)])
    
end,

锦衣碎片 = function(user,Item,PitchOn,Text)
 if RoleControl:取可用道具格子(user,"包裹") ==0 then
        SendMessage(user.连接id, 7, "#y/请在包裹栏留出一个空余的位置!")
        return true
    end
         if  Item.数量 >= 99  then
            Item.数量 =Item.数量-98
                 local 锦衣 ={"羽仙歌·墨黑","羽仙歌·月白","浪淘纱·月白","齐天大圣","浪淘纱","冰灵蝶翼·月笼","从军行·须眉","铃儿叮当","青花瓷·月白","落星织","碧华锦·凌雪","冰雪玉兔","五毒锦绣","鹿角湾湾","浪迹天涯","胡旋回雪","萌萌小厨","鎏金婚姻服","雀之恋骨","烈焰澜翻","双鲤寄情","凌波微步","水墨游龙","星光熠熠","浩瀚星河","荷塘涟漪","飞天足迹","龙卷风足迹","皮球足迹","旋律足迹","光剑足迹","地裂足迹","猫爪足迹","爱心足迹","元宝足迹","桃心足迹(红)","桃心足迹(粉)","波纹足迹(绿)","波纹足迹(蓝)","波纹足迹(粉)","蝴蝶足迹","普通足迹","璀璨烟花","跃动喷泉","鱼群足迹","炎火足迹","旋星足迹","星如雨","星光","小心机","藤蔓蔓延","闪光足迹","雀屏足迹","鬼脸南瓜","浮游水母","枫叶足迹","翅膀足迹","采蘑菇" }
                 ItemControl:GiveItem(user.id,锦衣[math.random(#锦衣)])
      
         else 
                SendMessage(user.连接id, 7, "#y/只有集齐99个锦衣碎片可合成")
                  return true
         end


end,

五宝礼盒 = function(user,Item,PitchOn,Text)
    if RoleControl:取可用道具格子(user,"包裹") ==0 then
        SendMessage(user.连接id, 7, "#y/请在包裹栏留出一个空余的位置!")
        return true
    end
    local  随机五宝 ={"高级藏宝图","定魂珠","金刚石","龙鳞","避水珠","夜光珠"}
    local  获得五宝  =随机五宝[math.random(1,#随机五宝)]
    ItemControl:GiveItem(user.id,获得五宝 )

end,

孩子金蛋 = function(user,Item,PitchOn,Text)
    if RoleControl:取可用道具格子(user,"包裹") ==0 then
        SendMessage(user.连接id, 7, "#y/请在包裹栏留出一个空余的位置!")
        return true
    end
    local  随机孩子礼包 ={"龙宫孩子礼包","魔王孩子礼包","神木孩子礼包","大唐孩子礼包","天宫孩子礼包","五庄孩子礼包","化生孩子礼包","普陀孩子礼包"}
    local  获得孩子礼包  =随机孩子礼包[math.random(1,#随机孩子礼包)]
    ItemControl:GiveItem(user.id,获得孩子礼包 )

end,

银币礼包 = function(user,Item,PitchOn,Text)
     RoleControl:添加银子(user,300000000,"银币礼包")
end,
三百点仙玉卡片 = function(user,Item,PitchOn,Text)
     RoleControl:添加仙玉(user,300,"三百点仙玉卡片")
end,
二百点仙玉卡片 = function(user,Item,PitchOn,Text)
     RoleControl:添加仙玉(user,200,"二百点仙玉卡片")
end,
一百五十点仙玉卡片 = function(user,Item,PitchOn,Text)
     RoleControl:添加仙玉(user,150,"一百五十点仙玉卡片")
end,
一百点仙玉卡片 = function(user,Item,PitchOn,Text)
     RoleControl:添加仙玉(user,100,"一百点仙玉卡片")
end,
仙玉卡片 = function(user,Item,PitchOn,Text)
     RoleControl:添加仙玉(user,math.random(1000,5000),"仙玉卡片")
end,
储备礼包 = function(user,Item,PitchOn,Text)
         RoleControl:添加储备(user,200000000,"储备礼包")
end,
一级未激活符石 = function(user,Item,PitchOn,Text)
          if  RoleControl:取可用格子数量(user,"包裹") < 1  then
                SendMessage(user.连接id, 7, "#y/请在包裹栏留出一个空余的位置!")
                return true
          elseif 银子检查(user.id,ItemData[Item.名称].等级*50000)==false then
                SendMessage(user.连接id,7,"#y/你没那么多的银子")
                return true
           elseif user.角色.当前经验<user.角色.等级*ItemData[Item.名称].等级*20000 then
                SendMessage(user.连接id,7,"#y/你没那么多的经验")
                return true
           end
           local  jhjq = ItemData[Item.名称].等级*50000
           local  jhjy = user.角色.等级*ItemData[Item.名称].等级*20000
           local  jhjl = 110-ItemData[Item.名称].等级*10
                RoleControl:扣除银子(user,jhjq,"符石激活")
               user.角色.当前经验=user.角色.当前经验-jhjy
               SendMessage(user.连接id,7,"#y/你消耗了".."#r/"..jhjq.."金钱"..jhjy.."经验")
           if math.random(100)<=jhjl then
                  SendMessage(user.连接id,7,"#y/符石激活成功")
                  ItemControl:GiveItem(user.id,取符石(ItemData[Item.名称].等级))
           else
                SendMessage(user.连接id,7,"#y/符石激活失败")
                return true
           end
end,
双倍经验丹 = function(user,Item,PitchOn,Text)
            if RoleControl:GetTaskID(user,"双倍") ~= 0 then
                任务数据[RoleControl:GetTaskID(user,"双倍")].结束 = 任务数据[RoleControl:GetTaskID(user,"双倍")].结束+3600
                SendMessage(user.连接id, 7, "#y/你使用了双倍经验丹")
                TaskControl:刷新追踪任务信息(user.id)
            else
                TaskControl:添加双倍(user.id)
                SendMessage(user.连接id, 7, "#y/你使用了双倍经验丹")
            end
end,


法宝礼盒 = function(user,Item,PitchOn,Text)
    if RoleControl:取可用道具格子(user,"包裹") ==0 then
        SendMessage(user.连接id, 7, "#y/请在包裹栏留出一个空余的位置!")
        return true
    end
    local 随机法宝 ={"碧玉葫芦","飞剑","拭剑石","金甲仙衣","缚妖索","金钱镖","惊魂铃","嗜血幡","风袋","九黎战鼓","盘龙壁","神行飞剑","汇灵盏","天师符","织女扇","雷兽","迷魂灯","定风珠","幽灵珠","会神珠","降魔斗篷","附灵玉","捆仙绳","发瘟匣","五彩娃娃","七杀","罗汉珠","赤焰","金刚杵","兽王令","摄魂","神木宝鼎","宝烛","天煞","琉璃灯","云雷鼓","落雨金钱","缩地尺","缚龙索","鬼泣","月光宝盒","混元伞","无魂傀儡","苍白纸人","通灵宝玉","聚宝盆","影蛊","乾坤玄火塔","无尘扇","无字经","葬龙笛","神匠袍","金箍棒","干将莫邪","救命毫毛","伏魔天书","普渡","镇海珠","奇门五行令","失心钹","五火神焰印","九幽","慈悲","月影","斩魔","金蟾","舞雪冰蝶","河图洛书","炽焰丹珠","凌波仙符","归元圣印","流影云笛","谷玄星盘","重明战鼓","蟠龙玉璧","翡翠芝兰","落星飞鸿","千斗金樽","宿幕星河", "梦云幻甲","软烟罗锦"}
    local  获得法宝  =随机法宝[math.random(1,#随机法宝)]
    ItemControl:GiveItem(user.id,获得法宝 )

end,

特殊兽决宝盒 = function(user,Item,PitchOn,Text)
    if RoleControl:取可用道具格子(user,"包裹") ==0 then
        SendMessage(user.连接id, 7, "#y/请在包裹栏留出一个空余的位置!")
        return true
    end
       local name =取礼盒兽诀名称()
      ItemControl:GiveItem(user.id,"特殊魔兽要诀",nil,name)

      广播消息("#hd/".."#S/(特殊兽决宝盒)".."#R/ "..user.角色.名称.."#Y/打开特殊兽决宝盒时，意外获得#g/"..name.."特殊兽决#80")
      end,

法宝任务书  = function(user,Item,PitchOn,Text)
    if RoleControl:取可用道具格子(user,"包裹") ==0 then
        SendMessage(user.连接id, 7, "#y/请在包裹栏留出一个空余的位置!")
        return true
    end
    local  随机法宝材料 ={"内丹","玄龟板","金凤羽","麒麟血","千年阴沉木"}
    local  获得法宝材料  =随机法宝材料[math.random(1,#随机法宝材料)]
    ItemControl:GiveItem(user.id,获得法宝材料 )

end,
秘制红罗羹 = function(user,Item,PitchOn,Text)
            TaskControl:添加秘制红罗羹(user.id)
            SendMessage(user.连接id, 7, "#y/你使用了秘制红罗羹")
end,
武器幻色丹 = function(user,Item,PitchOn,Text)
          if user.角色.武器数据.名称 == "" and  user.角色.武器数据.类别 == "" then
            SendMessage(user.连接id,7,"#y/请佩戴武器后在使用")
            return true
          end
          local 临时文本 = 分割文本(Text, "*-*")
          local 道具等级 = user.角色.武器数据.等级
          local 临时属性,临时数值
          local 主属性 ={"命中","气血","法防","灵力","魔法","速度","防御","伤害"}
          local 副属性 ={"法术伤害" ,"法术防御" ,"治疗能力" ,"气血回复效果" ,"固定伤害" ,"法术伤害结果" ,"格挡值" ,"狂暴等级" ,"穿刺等级" ,"抗物理暴击等级" ,"法术暴击等级" ,"封印命中等级" ,"物理暴击等级" ,"抗法术暴击等级" ,"抵抗封印等级"}
          if  math.random(100) > 50 then
          临时属性=主属性[math.random(1,#主属性)]
          临时数值=math.random(math.floor(道具等级*0.5),math.floor(道具等级))
          else
          临时属性=副属性[math.random(1,#副属性)]
          临时数值=math.random(math.floor(道具等级*0.3),math.floor(道具等级*0.6))
          end
          user.物品[user.角色.装备数据[23]].染色={染色方案=临时文本[1]+0,染色组={a=临时文本[2]+0,b=临时文本[3]+0},类型=临时属性,属性=临时数值}
          RoleControl:刷新装备属性(user,true)
          SendMessage(user.连接id,7,"#y/武器染色成功,增加"..临时属性..临时数值)
end,
飞行符 = function(user,Item,PitchOn,Text)
        if user.队伍~=0 and user.队长==false then
            SendMessage(user.连接id,7,"#Y/只有队长才可使用此道具")
             return true
        end
         local 允许使用=true
         local 提示信息
         if RoleControl:取飞行限制(user)==false or PitchOn==0 then
           允许使用=false
           提示信息="#y/您当前无法使用飞行道具"
         end
         if user.队伍~=0 then
              for n=1,#队伍数据[user.队伍].队员数据 do
                local TempID=队伍数据[user.队伍].队员数据[n]
                if RoleControl:取飞行限制(UserData[TempID])==false then
                  允许使用=false
                  提示信息="#y/"..UserData[TempID].角色.名称.."无法使用飞行道具"
                  end
               end
         end
          if 允许使用==false then
            SendMessage(user.连接id,7,提示信息)
            return true
          else
             local 跳转坐标={
             {x=361,y=36,z=1001},
             {x=65,y=113,z=1501},
             {x=121,y=50,z=1092},
             {x=117,y=148,z=1070},
             {x=110,y=88,z=1040},
             {x=115,y=50,z=1226},
             {x=108,y=50,z=1208},}
             MapControl:Jump(user.id,跳转坐标[PitchOn].z,跳转坐标[PitchOn].x,跳转坐标[PitchOn].y)
         end
end,
摇钱树树苗=function(user,Item,PitchOn,Text)
  if user.地图 ~= 1193 and user.地图 ~= 1110 and user.地图 ~= 15060 and user.地图 ~= 1173 and user.地图 ~= 1091 then
    SendMessage(user.连接id, 7, "#y/你无法在这样的场景种植摇钱树")
    return true
  elseif RoleControl:GetTaskID(user,"摇钱树") ~= 0 then
    SendMessage(user.连接id, 7, "#y/你已经种了一棵摇钱树,请去收取在来种植吧")
    return true
   elseif RoleControl:扣除体力(user,100) then
    SendMessage(user.连接id, 7, "#y/种植摇钱树需要扣除100点体力")
    return true
  end
  TaskControl:添加摇钱树(user.id)
end,
梦幻瓜子=function(user,Item,PitchOn,Text)
  if user.地图 ~= 1193 and user.地图 ~= 1110 and user.地图 ~= 15060 and user.地图 ~= 1173 and user.地图 ~= 1091 then
    SendMessage(user.连接id, 7, "#y/你无法在这样的场景种植梦幻瓜子")
    return true
  elseif RoleControl:GetTaskID(user,"梦幻瓜子") ~= 0 then
    SendMessage(user.连接id, 7, "#y/你已经种了一棵梦幻瓜子,请去收取在来种植吧")
    return true
   elseif RoleControl:扣除体力(user,100) then
    SendMessage(user.连接id, 7, "#y/种植梦幻瓜子需要扣除100点体力")
    return true
  end
  TaskControl:添加梦幻瓜子(user.id)
end,


新年红包 = function(user,Item,PitchOn,Text)
        local cj = ""
        local 奖励参数 = math.random(100)
        if 奖励参数 >=1 and 奖励参数 <=20 then
          cj= "超级金柳露"
        elseif 奖励参数 >=21 and 奖励参数 <=31 then
            cj=取随机宝石()
        elseif 奖励参数 >=32 and 奖励参数 <=43 then
            cj="玲珑宝图"
        elseif 奖励参数 >=44 and 奖励参数 <=60 then
          cj=取随机强化石()
        elseif 奖励参数 >=61 and 奖励参数 <=73 then
          cj=取随机五宝()
        elseif 奖励参数 >=74 and 奖励参数 <=83 then
          cj= 取召唤兽随机宝石()
        elseif 奖励参数 >=84 and 奖励参数 <=90 then
          cj="烟花礼盒"
        elseif 奖励参数 >=91 and 奖励参数 <=96 then
          cj="元宵"
        elseif 奖励参数 >=99 and 奖励参数 <=100 then
           local 随机仙玉=math.random(1,3)
           RoleControl:添加仙玉(user,随机仙玉,"新年红包")
           广播消息("#hd/".."#S/(新年红包)".."#R/ "..user.角色.名称.."#Y/拆开新年红包时，意外获得#g/"..随机仙玉.."点仙玉！")
        end
        if cj ~= "" then
          ItemControl:GiveItem(user.id,cj)
          广播消息("#hd/".."#S/(新年红包)".."#R/ "..user.角色.名称.."#Y/拆开新年红包时，意外获得了#g/"..cj)
        end
end,
逆袭宝箱 = function(user,Item,PitchOn,Text)

     ----寻找
        local gz = ItemControl:寻找道具(user,{"逆袭钥匙"})
         if #gz ~= 1 then
           SendMessage(user.连接id, 7, "#y/只有拥有逆袭钥匙才可以打开")
            return true
         else
             ItemControl:RemoveItems(user,gz[1],"逆袭宝箱")
             local cj = ""
            local 奖励参数 = math.random(100)
            if 奖励参数 >=1 and 奖励参数 <=20 then
            cj= "120无级别单件"
            elseif 奖励参数 >=21 and 奖励参数 <=31 then
            cj="120无级别单件"
            elseif 奖励参数 >=32 and 奖励参数 <=43 then
            cj="120无级别单件"
            elseif 奖励参数 >=44 and 奖励参数 <=60 then
            cj="130无级别单件"
            elseif 奖励参数 >=61 and 奖励参数 <=73 then
            cj="130无级别单件"
            elseif 奖励参数 >=74 and 奖励参数 <=83 then
            cj= "130无级别单件"
            elseif 奖励参数 >=84 and 奖励参数 <=90 then
            cj="140无级别单件"
            elseif 奖励参数 >=91 and 奖励参数 <=93 then
            cj="150无级别单件"
            elseif 奖励参数 >=94 and 奖励参数 <=95 then
            cj="160无级别单件"
            elseif 奖励参数 >=96 and 奖励参数 <=100 then
            cj="140无级别单件"
          
            end
            if cj ~= "" then
              ItemControl:GiveItem(user.id,cj)
              广播消息("#hd/".."#S/(逆袭宝箱)".."#R/ "..user.角色.名称.."#Y/打开逆袭宝箱时，意外获得#g/"..cj)
            end
            SendMessage(user.连接id,2031,Item.名称)
            MapControl:MapSend(user.id, 1011,{id=user.id,名称=Item.名称}, user.地图)
          
         end
         -------奖励
        
           

end,
神秘钥匙 = function(user,Item,PitchOn,Text)
 -- SendMessage(user.连接id, 2072, {标题="牛了！无级别啊",文本="鉴定至少80级装备出现特效无级别"})
        if RoleControl:取可用道具格子(user,"包裹") ==0 then
        SendMessage(user.连接id, 7, "#y/请在包裹栏留出一个空余的位置!")
        return true
    end
        local 奖励参数 = math.random(120)
        if   奖励参数 <=5 then
        ItemControl:GiveItem(user.id, "仙玉5000点")
        elseif  奖励参数 <= 10 then
        ItemControl:GiveItem(user.id, "知了5000")
        elseif  奖励参数 <= 15 then
        ItemControl:GiveItem(user.id, "知了3000")
        elseif  奖励参数 <= 20 then
        ItemControl:GiveItem(user.id, "仙玉3000点")
        elseif  奖励参数 <= 25 then
        ItemControl:GiveItem(user.id, "灵饰160级礼包")
        elseif  奖励参数 <= 30 then
        ItemControl:GiveItem(user.id, "灵饰140级礼包")
        elseif  奖励参数 <= 35 then
        ItemControl:GiveItem(user.id, "孩子金蛋")
        elseif  奖励参数 <= 40 then
        ItemControl:GiveItem(user.id,"点化石",nil,"进阶善恶有报")
        elseif  奖励参数 <= 45 then
        ItemControl:GiveItem(user.id, "仙玉3000点")
        elseif  奖励参数 <= 50 then
        ItemControl:GiveItem(user.id, "仙玉5000点")
        elseif  奖励参数 <= 55 then
        RoleControl:添加特殊积分(user,3000)
        elseif  奖励参数 <= 60 then
        RoleControl:添加特殊积分(user,5000)
        elseif  奖励参数 <= 65 then
        ItemControl:GiveItem(user.id, "陨铁")
        elseif  奖励参数 <= 70 then
        ItemControl:GiveItem(user.id, "孩子金蛋")
        elseif  奖励参数 <= 75 then
        ItemControl:GiveItem(user.id, "超级九转金丹50个")
        elseif  奖励参数 <= 80 then
        ItemControl:GiveItem(user.id, "灵饰160级礼包")
        elseif  奖励参数 <= 85 then
        ItemControl:GiveItem(user.id, "十五随机技能胚子")
        elseif  奖励参数 <= 90 then
        ItemControl:GiveItem(user.id, "战魄")
        elseif  奖励参数 <= 95 then
        ItemControl:GiveItem(user.id,"点化石",nil,"进阶力劈华山")
        elseif  奖励参数 <= 100 then
        ItemControl:GiveItem(user.id, "仙玉10000点")
        elseif  奖励参数 <= 105 then
        RoleControl:添加特殊积分(user,8000)
        elseif  奖励参数 <= 110 then
        ItemControl:GiveItem(user.id, "超级修炼果50个")
        elseif  奖励参数 <= 115 then
        ItemControl:GiveItem(user.id, "灵饰120级礼包")
        elseif  奖励参数 <= 120 then
        ItemControl:GiveItem(user.id, "高级装备升星石")
        end

end,

新年烟花 = function(user,Item,PitchOn,Text)
            local cj = ""
            local 奖励参数 = math.random(100)
            if 奖励参数 >=1 and 奖励参数 <=20 then
            cj= "超级金柳露"
            elseif 奖励参数 >=21 and 奖励参数 <=31 then
            cj=取随机宝石()
            elseif 奖励参数 >=32 and 奖励参数 <=43 then
            cj="玲珑宝图"
            elseif 奖励参数 >=44 and 奖励参数 <=60 then
            cj=取随机红包()
            elseif 奖励参数 >=61 and 奖励参数 <=73 then
            cj="修炼果"
            elseif 奖励参数 >=74 and 奖励参数 <=83 then
            cj= 取随机五宝()
            elseif 奖励参数 >=84 and 奖励参数 <=90 then
            cj=取随机强化石()
            elseif 奖励参数 >=91 and 奖励参数 <=96 then
            cj=取召唤兽随机宝石()
            elseif 奖励参数 >=99 and 奖励参数 <=100 then
            local 随机仙玉=math.random(5)
            RoleControl:添加仙玉(user,随机仙玉,"新年红包")
            广播消息("#hd/".."#S/(新年烟花)".."#R/ "..user.角色.名称.."#Y/点燃新年烟花时，意外获得#g/"..随机仙玉.."点仙玉！")
            end
            if cj ~= "" then
            ItemControl:GiveItem(user.id,cj)
            广播消息("#hd/".."#S/(新年烟花)".."#R/ "..user.角色.名称.."#Y/点燃新年烟花时，意外获得了#g/"..cj)
            end
            SendMessage(user.连接id,2031,Item.名称)
            MapControl:MapSend(user.id, 1011,{id=user.id,名称=Item.名称}, user.地图)

end,
烟花 = function(user,Item,PitchOn,Text)
       local cj = ""
            local 奖励参数 = math.random(100)
            if 奖励参数 >=1 and 奖励参数 <=20 then
            cj= "超级金柳露"
            elseif 奖励参数 >=21 and 奖励参数 <=31 then
            cj=取随机宝石()
            elseif 奖励参数 >=32 and 奖励参数 <=43 then
            cj="玲珑宝图"
            elseif 奖励参数 >=44 and 奖励参数 <=60 then
            cj=取随机红包()
            elseif 奖励参数 >=61 and 奖励参数 <=73 then
            cj="修炼果"
            elseif 奖励参数 >=74 and 奖励参数 <=83 then
            cj= "元宵"
            elseif 奖励参数 >=84 and 奖励参数 <=90 then
            cj=取随机强化石()
            elseif 奖励参数 >=91 and 奖励参数 <=96 then
            cj=取召唤兽随机宝石()
            elseif 奖励参数 >=99 and 奖励参数 <=100 then
            local 随机仙玉=math.random(20)
            RoleControl:添加仙玉(user,随机仙玉,"烟花")
            广播消息("#hd/".."#S/(烟花)".."#R/ "..user.角色.名称.."#Y/点燃烟花时，意外获得#g/"..随机仙玉.."点仙玉！")
            end
            if cj ~= "" then
            ItemControl:GiveItem(user.id,cj)
            广播消息("#hd/".."#S/(烟花)".."#R/ "..user.角色.名称.."#Y/点燃烟花时，意外获得了#g/"..cj)
            end
                        SendMessage(user.连接id,2031,Item.名称)
            MapControl:MapSend(user.id, 1011,{id=user.id,名称=Item.名称}, user.地图)
end,
烟花礼盒 = function(user,Item,PitchOn,Text)
          local cj = ""
            local 奖励参数 = math.random(100)
            if 奖励参数 >=1 and 奖励参数 <=20 then
              cj= "超级金柳露"
            elseif 奖励参数 >=21 and 奖励参数 <=31 then
                cj=取随机宝石()
            elseif 奖励参数 >=32 and 奖励参数 <=43 then
                cj="玲珑宝图"
            elseif 奖励参数 >=44 and 奖励参数 <=60 then
              cj="元宵"
            elseif 奖励参数 >=61 and 奖励参数 <=73 then
              cj="修炼果"
            elseif 奖励参数 >=74 and 奖励参数 <=83 then
              cj= "五行石"
            elseif 奖励参数 >=84 and 奖励参数 <=90 then
              cj="装备洗炼石"
            elseif 奖励参数 >=91 and 奖励参数 <=96 then

             elseif 奖励参数 >=99 and 奖励参数 <=100 then
           local 随机仙玉=math.random(1,5)
           RoleControl:添加仙玉(user,随机仙玉,"烟花礼盒")
           广播消息("#hd/".."#S/(烟花礼盒)".."#R/ "..user.角色.名称.."#Y/拆开烟花礼盒时，意外获得#g/"..随机仙玉.."点仙玉！")

            elseif 奖励参数 >=99 and 奖励参数 <=100 then
               local 临时兽决= 取新年兽诀名称()
          ItemControl:GiveItem(user.id, "高级魔兽要诀",nil, 临时兽决)
          SendMessage(user.连接id,20054,AddItem("高级魔兽要诀", nil, 临时兽决))
            end
            if cj ~= "" then
              ItemControl:GiveItem(user.id,cj)
              广播消息("#hd/".."#S/(烟花礼盒)".."#R/ "..user.角色.名称.."#Y/点燃新年烟花礼盒时，意外获得了#g/"..cj)
            end
            SendMessage(user.连接id,2031,Item.名称)
            MapControl:MapSend(user.id, 1011,{id=user.id,名称=Item.名称}, user.地图)
end,

三界悬赏令 = function(user,Item,PitchOn,Text)
            FightGet:进入处理(user.id, 100017, "66", 0)
end,

藏宝图 = function(user,Item,PitchOn,Text)
        if user.队伍~=0 and user.队长==false then
            SendMessage(user.连接id,7,"#Y/只有队长才可使用此道具")
             return true
        end
      MapControl:Jump(user.id, Item.地图编号, Item.x, Item.y,true)
          if MapData[user.地图].名称 ~= MapData[Item.地图编号].名称 then
               SendMessage(user.连接id, 7,"#Y/宝图记载的藏宝地点是在#R/" .. MapData[Item.地图编号].名称 .. "#Y/，你好像走错路了")
               return true
          elseif 取两点距离(user.角色.地图数据,{x = Item.x*20,y = Item.y*20}) > 100 then
                SendMessage(user.连接id, 7,"#Y/宝藏就在(" .. Item.x .. "，" ..Item.y .. ")附近")
                return true
          end
             local 奖励参数 = math.random(100)
            if  奖励参数 <=5 then
              ItemControl:GiveItem(user.id,"内丹碎片")
            elseif  奖励参数 <=8 then
              ItemControl:GiveItem(user.id,"钨金",math.random(10)*10)
            elseif 奖励参数 <=10 then
            ItemControl:GiveItem(user.id, "魔兽要诀")
            elseif 奖励参数 <= 15 then
            SendMessage(user.连接id, 7, "#y/你好像得到了点什么")
            ItemControl:给予随机五宝(user.id)
            elseif 奖励参数 <= 18 then
            ItemControl:GiveItem(user.id,取随机书铁(),math.random(3, 7)*10)
            elseif 奖励参数 <= 28 then
            ItemControl:GiveItem(user.id, "金柳露")
            SendMessage(user.连接id, 7, "#y/你获得了传说中的金柳露")
            elseif 奖励参数 <= 38 then
            ItemControl:GiveItem(user.id, "花豆")
            elseif 奖励参数 <= 30 then
            ItemControl:GiveItem(user.id, "彩果")
            elseif 奖励参数 <= 58 then
            SendMessage(user.连接id, 7, "#y/你一锄头挖下去，好像什么都没挖到……")
           -- TaskControl:设置封妖任务(user.id)
            elseif 奖励参数 <= 80 then
            FightGet:进入处理(user.id, 100004, "66", 0)
            else
            SendMessage(user.连接id, 7, "#y/你一锄头挖下去，似乎挖到了什么……")
            TaskControl:设置封妖任务(user.id)
            end

end,

考古铁铲 = function(user,Item,PitchOn,Text)
        if user.队伍~=0 and user.队长==false then
            SendMessage(user.连接id,7,"#Y/只有队长才可使用此道具")
             return true
        end
        if DebugMode then
           MapControl:Jump(user.id, Item.地图编号, Item.x, Item.y,true)
        end
          if MapData[user.地图].名称 ~= MapData[Item.地图编号].名称 then
               SendMessage(user.连接id, 7,"#Y/铁铲记载的藏宝地点是在#R/" .. MapData[Item.地图编号].名称 .. "#Y/，你好像走错路了")
               return true
          elseif 取两点距离(user.角色.地图数据,{x = Item.x*20,y = Item.y*20}) > 100 then
                SendMessage(user.连接id, 7,"#Y/宝藏就在(" .. Item.x .. "，" ..Item.y .. ")附近")
                return true
          end
              local 奖励参数 = math.random(2400)

             local 随机古董 =  {"骨算筹","楚国计官之玺","韩国武遂大夫玺","齐国大车之玺","秦国工师之印","燕国外司炉印","赵国榆平发弩玺",
            "刀币","十二枝灯","古玉簪","雁足灯","素纱禅衣","菊月古鉴·饕鬄镜","凉造新泉",
            "铜车马","鎏金铜尺","嵌绿松石象牙杯","鸭形玻璃注","神面卣","熹平经残石","毛公鼎","羊首勺","铁矛",
            "绿釉陶狗","朱雀纹瓦当","盘古神斧","四足鬲","四帛书","史叔编钟"}

            if 奖励参数 <= 600 then
              SendMessage(user.连接id, 7, "#y/你一锄头挖下去，好像什么都没挖到……")
            elseif 奖励参数 <= 700 then
              FightGet:进入处理(user.id, 100004, "66", 0)
            elseif 奖励参数 <= 900 then
              ItemControl:GiveItem(user.id, "未鉴定的古董")
            elseif 奖励参数 <= 950 then  
              local gdname =随机古董[math.random(#随机古董)]
                 ItemControl:GiveItem(user.id,gdname )
                  SendMessage(user.连接id, 2072, {标题=gdname,文本="恭喜你获得了"..gdname.."古董"})
                 --RoleControl:添加成就积分(user,1)
                  广播消息("#hd/".."#S/(考古·逐梦敦煌)".."#R/ "..user.角色.名称.."#Y/在挖掘古董时，意外获得了#g/"..gdname)
            elseif 奖励参数 <= 970 then  -- 10
               ItemControl:GiveItem(user.id, "一代将军令")
                         SendMessage(user.连接id, 2072, {标题="一代将军令",文本="恭喜你获得了一代将军令古董"})
                 --RoleControl:添加成就积分(user,1)
                广播消息("#hd/".."#S/(考古·逐梦敦煌)".."#R/ "..user.角色.名称.."#Y/在挖掘古董时，意外获得了#g/一代将军令")
            elseif 奖励参数 <= 997 then --12  
              ItemControl:GiveItem(user.id, "和田青白玉戈")
                                 SendMessage(user.连接id, 2072, {标题="和田青白玉戈",文本="恭喜你获得了和田青白玉戈古董"})
                 --RoleControl:添加成就积分(user,1)
               广播消息("#hd/".."#S/(考古·逐梦敦煌)".."#R/ "..user.角色.名称.."#Y/在挖掘古董时，意外获得了#g/和田青白玉戈")
            elseif 奖励参数 <= 999 then  --13
                ItemControl:GiveItem(user.id, "莲鹤方壶")
                  SendMessage(user.连接id, 2072, {标题="莲鹤方壶",文本="恭喜你获得了莲鹤方壶古董"})
                 --RoleControl:添加成就积分(user,1)
                广播消息("#hd/".."#S/(考古·逐梦敦煌)".."#R/ "..user.角色.名称.."#Y/在挖掘古董时，意外获得了#g/莲鹤方壶")
               elseif 奖励参数 <= 1300 then  --13
                  SendMessage(user.连接id, 7, "#y/你一锄头挖下去，好像什么都没挖到……")

                   elseif 奖励参数 <= 1400 then  --13
                  SendMessage(user.连接id, 7, "#y/你一锄头挖下去，好像什么都没挖到……")

                   elseif 奖励参数 <= 1500 then  --13
                  SendMessage(user.连接id, 7, "#y/你一锄头挖下去，好像什么都没挖到……")

                   elseif 奖励参数 <= 1600 then  --13
                  SendMessage(user.连接id, 7, "#y/你一锄头挖下去，好像什么都没挖到……")
                 elseif 奖励参数 <= 1700 then  --13
                   FightGet:进入处理(user.id, 100004, "66", 0)

                    elseif 奖励参数 <= 1800 then  --13
                   FightGet:进入处理(user.id, 100004, "66", 0)
                  
                  elseif 奖励参数 <= 1900 then  --13
                   local gdname =随机古董[math.random(#随机古董)]
                 ItemControl:GiveItem(user.id,gdname )
                  SendMessage(user.连接id, 2072, {标题=gdname,文本="恭喜你获得了"..gdname.."古董"})
                 --RoleControl:添加成就积分(user,1)
                  广播消息("#hd/".."#S/(考古·逐梦敦煌)".."#R/ "..user.角色.名称.."#Y/在挖掘古董时，意外获得了#g/"..gdname)


                   elseif 奖励参数 <= 2000 then  --13
                   local gdname =随机古董[math.random(#随机古董)]
                 ItemControl:GiveItem(user.id,gdname )
                  SendMessage(user.连接id, 2072, {标题=gdname,文本="恭喜你获得了"..gdname.."古董"})
                 --RoleControl:添加成就积分(user,1)
                  广播消息("#hd/".."#S/(考古·逐梦敦煌)".."#R/ "..user.角色.名称.."#Y/在挖掘古董时，意外获得了#g/"..gdname)

                  elseif 奖励参数 <= 2100 then
              ItemControl:GiveItem(user.id, "未鉴定的古董")


              elseif 奖励参数 <= 2200 then
              ItemControl:GiveItem(user.id, "未鉴定的古董")

              elseif 奖励参数 <= 2300 then
              ItemControl:GiveItem(user.id, "未鉴定的古董")

               elseif 奖励参数 <= 2400 then  --13
                  SendMessage(user.连接id, 7, "#y/你一锄头挖下去，好像什么都没挖到……")
            end
end,




高级内丹碎片 = function(user,Item,PitchOn,Text)
         if  Item.数量 >= 50  then
            Item.数量 =Item.数量-49 
               local 临时兽决= 取内丹("高级")
                ItemControl:GiveItem(user.id, "高级召唤兽内丹", nil,临时兽决)
                SendMessage(user.连接id,20054,AddItem("高级召唤兽内丹",nil, 临时兽决))
         else 
                SendMessage(user.连接id, 7, "#y/只有集齐50个高级内丹碎片可合成")
                  return true
         end

         
end,
镇妖拘魂铃  = function(user,Item,PitchOn,Text)

        local cj = ""
                local 奖励参数 = math.random(1000)   --- 20/100  0.2 20%
                if 奖励参数 >=1 and 奖励参数 <=200 then
                      cj= 取随机五宝()
                elseif 奖励参数 >=200 and 奖励参数 <=280 then --元宵
                     cj= "元宵"
                elseif 奖励参数 >=280 and 奖励参数 <=360 then--随机3-8级宝石
                     local 随机名称 =取随机宝石()
                    local 随机等级 = math.random(3.6)
                    ItemControl:GiveItem(user.id, 随机名称, 随机等级)
                    广播消息("#hd/".."#S/(镇妖拘魂铃)".."#G/".. user.角色.名称.. "#y/的在开启镇妖拘魂铃中意外获得了#G/"..随机名称.."#"..math.random(110))
                elseif 奖励参数 >=360 and 奖励参数 <=480 then--特设令牌
                  cj="特赦令牌"
                elseif 奖励参数 >=480 and 奖励参数 <=580 then--金银宝盒
                  cj="金银宝盒"
                elseif 奖励参数 >=580 and 奖励参数 <=600 then--随机特殊兽决
                local 临时兽决=取内丹("高级")
                ItemControl:GiveItem(user.id, "高级召唤兽内丹",nil, 临时兽决)
               SendMessage(user.连接id,20054,AddItem("高级召唤兽内丹", nil,临时兽决))
                广播消息("#hd/".."#S/(镇妖拘魂铃)".."#G/".. user.角色.名称.. "#y/的在开启镇妖拘魂铃中意外获得了#G/高级召唤兽内丹".."#"..math.random(110))
                elseif 奖励参数 >=600 and 奖励参数 <=750 then--100-160级附魔宝珠
                    local 随机名称 ="附魔宝珠"
                    local 随机等级 = math.random(10, 16)*10
                    ItemControl:GiveItem(user.id, 随机名称, 随机等级)
                    广播消息("#hd/".."#S/(镇妖拘魂铃)".."#G/".. user.角色.名称.. "#y/的在开启镇妖拘魂铃中意外获得了#G/"..随机名称.."#"..math.random(110))
                elseif 奖励参数 >=750 and 奖励参数 <=800 then --随机等级 上古锻造图策 5%
                 local 随机名称 ="上古锻造图策"
                    local 随机等级 = math.floor(user.角色.等级/10)*10+5
                    ItemControl:GiveItem(user.id, 随机名称, 随机等级)
                    广播消息("#hd/".."#S/(镇妖拘魂铃)".."#G/".. user.角色.名称.. "#y/的在开启镇妖拘魂铃中意外获得了#G/"..随机名称.."#"..math.random(110))
                elseif 奖励参数 >=800 and 奖励参数 <=850 then--随机等级炼妖石 5%  
                   local 随机名称 ="炼妖石"
                    local 随机等级 = math.floor(user.角色.等级/10)*10+5
                    ItemControl:GiveItem(user.id, 随机名称, 随机等级)
                    广播消息("#hd/".."#S/(镇妖拘魂铃)".."#G/".. user.角色.名称.. "#y/的在开启镇妖拘魂铃中意外获得了#G/"..随机名称.."#"..math.random(110))
                elseif 奖励参数 >=850 and 奖励参数 <=900 then  --100-130级随机制造指南书 5%
                       local 随机名称 ="制造指南书"
                    local 随机等级 = math.random(10, 13)*10
                    ItemControl:GiveItem(user.id, 随机名称, 随机等级)
                    广播消息("#hd/".."#S/(镇妖拘魂铃)".."#G/".. user.角色.名称.. "#y/的在开启镇妖拘魂铃中意外获得了#G/"..随机名称.."#"..math.random(110))
                elseif 奖励参数 >=900 and 奖励参数 <=1000 then --100-140随机百炼精铁 10%
                    local 随机名称 ="百炼精铁"
                    local 随机等级 = math.random(10, 14)*10
                    ItemControl:GiveItem(user.id, 随机名称, 随机等级)
                    广播消息("#hd/".."#S/(镇妖拘魂铃)".."#G/".. user.角色.名称.. "#y/的在开启镇妖拘魂铃中意外获得了#G/"..随机名称.."#"..math.random(110))
                 
                end
                if cj ~= "" then
                  ItemControl:GiveItem(user.id,cj)
                  广播消息("#hd/".."#S/(镇妖拘魂铃)".."#G/".. user.角色.名称.. "#y/的在开启镇妖拘魂铃中意外获得了#G/"..cj.."#"..math.random(110))
                end
         
end,
内丹碎片 = function(user,Item,PitchOn,Text)
         if  Item.数量 >= 50  then
            Item.数量 =Item.数量-49 
               local 临时兽决= 取内丹("低级")
                ItemControl:GiveItem(user.id, "召唤兽内丹", nil,临时兽决)
                SendMessage(user.连接id,20054,AddItem("召唤兽内丹",nil, 临时兽决))
         else 
                SendMessage(user.连接id, 7, "#y/只有集齐50个内丹碎片可合成")
                  return true
         end
end,

阵法碎片 = function(user,Item,PitchOn,Text)
         if  Item.数量 >= 50  then
            Item.数量 =Item.数量-49 
             
                ItemControl:GiveItem(user.id, "鬼谷子")
               
         else 
                SendMessage(user.连接id, 7, "#y/只有集齐50个阵法碎片可合成")
                  return true
         end
end,
-- 法宝任务书 = function(user,Item,PitchOn,Text)
--          if  Item.数量 >= 5  then
--             Item.数量 =Item.数量-4 
             
--                 ItemControl:GiveItem(user.id, "超级聚灵袋")
--                 SendMessage(user.连接id, 7, "#y/只有集齐5个法宝任务书可合成超级聚灵袋")
               
--          else 
--          if RoleControl:GetTaskID(user,"法宝") ~= 0 then
--           SendMessage(user.连接id, 7, "#y/你似乎还有法宝任务没有完成,想要取消任务的话,去天宫金童子处取消")
--           return true
--         end
--         TaskControl:添加法宝任务(user.id)
               
--                   return true
--          end
-- end,

金蝉子礼包 = function(user,Item,PitchOn,Text)
 -- SendMessage(user.连接id, 2072, {标题="牛了！无级别啊",文本="鉴定至少80级装备出现特效无级别"})
        if RoleControl:取可用道具格子(user,"包裹") ==0 then
        SendMessage(user.连接id, 7, "#y/请在包裹栏留出一个空余的位置!")
        return true
    end
        local 奖励参数 = math.random(130)
        if   奖励参数 <=5 then
        ItemControl:GiveItem(user.id, "中级装备升星石")
        elseif  奖励参数 <= 10 then
        ItemControl:GiveItem(user.id, "特殊兽决宝盒")
        elseif  奖励参数 <= 15 then
        ItemControl:GiveItem(user.id,"钨金",160)
        elseif  奖励参数 <= 20 then
        RoleControl:添加仙玉(user,2500,"仙玉点卡")
        elseif  奖励参数 <= 25 then
        ItemControl:GiveItem(user.id, "神兜兜")
        elseif  奖励参数 <= 30 then
        ItemControl:GiveItem(user.id, "附魔宝珠160")
        elseif  奖励参数 <= 35 then
        ItemControl:GiveItem(user.id, "五宝礼盒")
        elseif  奖励参数 <= 40 then
        ItemControl:GiveItem(user.id, "星辉石",1)
        elseif  奖励参数 <= 45 then
        ItemControl:GiveItem(user.id,"灵箓")
        elseif  奖励参数 <= 50 then
        ItemControl:GiveItem(user.id,"五宝礼盒")
        elseif  奖励参数 <= 55 then
        ItemControl:GiveItem(user.id, "大米")
        elseif  奖励参数 <= 60 then
        ItemControl:GiveItem(user.id, "武器幻色丹")
        elseif  奖励参数 <= 65 then
        ItemControl:GiveItem(user.id, "法宝任务书")
        elseif  奖励参数 <= 70 then
        ItemControl:GiveItem(user.id, "神之符")
        elseif  奖励参数 <= 75 then
        ItemControl:GiveItem(user.id, "不磨符")
        elseif  奖励参数 <= 80 then
        ItemControl:GiveItem(user.id, "灵能符")
        elseif  奖励参数 <= 85 then
        ItemControl:GiveItem(user.id, "八随机技能胚子")
        elseif  奖励参数 <= 90 then
        ItemControl:给予随机五宝(user.id)
        elseif  奖励参数 <= 95 then
        ItemControl:GiveItem(user.id, "修炼果")
        elseif  奖励参数 <= 100 then
        ItemControl:GiveItem(user.id, "九转金丹")
        elseif  奖励参数 <= 105 then
        ItemControl:GiveItem(user.id, "神秘钥匙")
        elseif  奖励参数 <= 110 then
        ItemControl:GiveItem(user.id, "灵饰100级礼包")
        elseif  奖励参数 <= 115 then
        ItemControl:GiveItem(user.id, "装备升星石")
        elseif  奖励参数 <= 120 then
        RoleControl:添加银子(user,10000000,"银币礼包")
        elseif  奖励参数 <= 130 then
        local 临时兽决=取内丹("高级")
        ItemControl:GiveItem(user.id, "高级召唤兽内丹",nil, 临时兽决)
        end

end,

高级藏宝图 = function(user,Item,PitchOn,Text)
 -- SendMessage(user.连接id, 2072, {标题="牛了！无级别啊",文本="鉴定至少80级装备出现特效无级别"})
        if user.队伍~=0 and user.队长==false then
            SendMessage(user.连接id,7,"#Y/只有队长才可使用此道具")
             return true
        end
        if RoleControl:取可用道具格子(user,"包裹") ==0 then
        SendMessage(user.连接id, 7, "#y/请在包裹栏留出一个空余的位置!")
        return true
    end
       MapControl:Jump(user.id, Item.地图编号, Item.x, Item.y,true)
         if MapData[user.地图].名称 ~= MapData[Item.地图编号].名称 then
               SendMessage(user.连接id, 7,"#Y/宝图记载的藏宝地点是在#R/" .. MapData[Item.地图编号].名称 .. "#Y/，你好像走错路了")

               return true
          elseif 取两点距离(user.角色.地图数据,{x = Item.x*20,y = Item.y*20}) > 100 then
                SendMessage(user.连接id, 7,"#Y/宝藏就在(" .. Item.x .. "，" ..Item.y .. ")附近")
                return true
          end
        local 奖励参数 = math.random(120)
        if   奖励参数 <=5 then
        local 临时兽决=取内丹("高级")
 
        ItemControl:GiveItem(user.id, "高级召唤兽内丹",nil, 临时兽决)
        SendMessage(user.连接id,20054,AddItem("高级召唤兽内丹", nil,临时兽决))
        elseif  奖励参数 <= 10 then
           ItemControl:GiveItem(user.id,"钨金",math.random(10,16)*10)
        elseif  奖励参数 <= 20 then
        local 临时兽决= 取活动兽诀名称()
        ItemControl:GiveItem(user.id, "高级魔兽要诀",nil, 临时兽决)
        SendMessage(user.连接id,20054,AddItem("高级魔兽要诀", nil, 临时兽决))
        elseif  奖励参数 <= 25 then
        ItemControl:GiveItem(user.id, "神兜兜")
        SendMessage(user.连接id,20054,AddItem("神兜兜"))
        elseif  奖励参数 <= 35 then
        ItemControl:GiveItem(user.id, "上古锻造图策",math.random(6, 14) * 10+5)
        elseif  奖励参数 <= 45 then
        ItemControl:GiveItem(user.id, "彩果")
        elseif  奖励参数 <= 50 then
        ItemControl:GiveItem(user.id, "星辉石",1)
        elseif  奖励参数 <= 60 then
          local 随机名称 = 取随机书铁()
          ItemControl:GiveItem(user.id,  随机名称,math.random(10, 13)*10)
        elseif  奖励参数 <= 70 then
         RoleControl:添加银子(user,math.random(50000, 200000),"高级藏宝图奖励")
        elseif  奖励参数 <= 80 then
        ItemControl:GiveItem(user.id, "海马")
        elseif  奖励参数 <= 85 then
        local 临时兽决= 取礼盒兽诀名称()
        ItemControl:GiveItem(user.id, "特殊魔兽要诀", nil,临时兽决)
        SendMessage(user.连接id,20054,AddItem("特殊魔兽要诀",nil, 临时兽决))
        elseif  奖励参数 <= 100 then
        ItemControl:GiveItem(user.id, "珍珠", math.random(10, 15) * 10)
        else
        EquipmentControl:取随机装备(user.id, math.random(6, 8), true)
        end

end,
玲珑宝图 = function(user,Item,PitchOn,Text)
        if user.队伍~=0 and user.队长==false then
            SendMessage(user.连接id,7,"#Y/只有队长才可使用此道具")
             return true
        end
        if RoleControl:取可用道具格子(user,"包裹") ==0 then
        SendMessage(user.连接id, 7, "#y/请在包裹栏留出一个空余的位置!")
        return true
    end
    MapControl:Jump(user.id, Item.地图编号, Item.x, Item.y,true)
         if MapData[user.地图].名称 ~= MapData[Item.地图编号].名称 then
               SendMessage(user.连接id, 7,"#Y/宝图记载的藏宝地点是在#R/" .. MapData[Item.地图编号].名称 .. "#Y/，你好像走错路了")
               return true
          elseif 取两点距离(user.角色.地图数据,{x = Item.x*20,y = Item.y*20}) > 100 then
                SendMessage(user.连接id, 7,"#Y/宝藏就在(" .. Item.x .. "，" ..Item.y .. ")附近")
                return true
          end
          local 奖励参数 = math.random(100)
          local 奖励等级 ={60}
          if user.角色.等级 <80 then
              奖励等级 ={60,80}
          elseif user.角色.等级 <100 then
            奖励等级 ={60,80,100}
          elseif user.角色.等级 <120 then
            奖励等级 ={80,100,120}
          elseif user.角色.等级 <140 then
            奖励等级 ={100,120,140}
          else
            奖励等级 ={120,140,160}
          end
          local 随机奖励 = 奖励等级[math.random(1,#奖励等级)]
          if  奖励参数 <=10 then
          ItemControl:GiveItem(user.id,"星辉石",math.random(3))
          elseif 奖励参数 <=20 then
          ItemControl:GiveItem(user.id,"元灵晶石",随机奖励 )
          elseif 奖励参数 <=30 then
          local 灵饰类型= {"手镯","佩饰","戒指","耳饰"}
          ItemControl:GiveItem(user.id,"灵饰指南书",随机奖励,灵饰类型[math.random(1,#灵饰类型)])
          else
          ItemControl:GiveItem(user.id,取随机宝石())
          end

end,

普通攻宠礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=1500 ,体资=5500 ,法资=3000 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("礼包攻宠","神兽",临时资质,0,{"大快朵颐","高级夜战","高级吸血","高级连击","高级偷袭","高级驱鬼","高级强力"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只礼包神兽。")
            end
end,

普通法宠礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=1500 ,体资=5500 ,法资=3000 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("礼包法宠","神兽",临时资质,0,{"须弥真言","泰山压顶","高级感知","高级冥思","高级法术暴击","高级神佑复生","高级法术波动"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只礼包神兽。")
            end
end,
定制攻宠礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=1500 ,体资=5500 ,法资=3000 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("定制攻宠","神兽",临时资质,0,{"嗜血追击","神出鬼没","高级合纵","高级独行","高级必杀","高级偷袭","高级连击","高级神佑复生","高级吸血","高级反击","高级夜战"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只礼包神兽。")
            end
end,
定制法宠礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=1500 ,体资=5500 ,法资=3000 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("定制法宠","神兽",临时资质,0,{"须弥真言","高级遗志","灵能激发","高级法术连击","高级神迹","泰山压顶","高级法术暴击","高级神佑复生","高级魔之心","高级法术波动","高级飞行"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只礼包神兽。")
            end
end,

超级神兽定制24礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=2100 ,体资=6000 ,法资=3600 ,防资=2100 ,速资=2100 ,躲资=2100,成长=1.35}
                    user.召唤兽:创建召唤兽("暗黑童子","神兽",临时资质,0,{"高级连击","高级防御","高级吸血","高级偷袭","高级法术波动","泰山压顶","魔之心","法术连击","高级法术暴击","必杀","夜战","奔雷咒","地狱烈火","力劈华山","善恶有报","防御","吸血","连击","独行","高级驱鬼","遗志","夜舞倾城","月光","神迹"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只24技能超级神兽。")
            end

end,
帝级神兽定制24礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=2500 ,体资=7600 ,法资=4100 ,防资=2500 ,速资=2500 ,躲资=2500,成长=2.0}
                    user.召唤兽:创建召唤兽("暗黑童子","神兽",临时资质,0,{"高级连击","高级防御","高级吸血","高级偷袭","高级法术波动","泰山压顶","魔之心","法术连击","高级法术暴击","必杀","夜战","奔雷咒","地狱烈火","力劈华山","善恶有报","防御","吸血","连击","独行","高级驱鬼","遗志","夜舞倾城","月光","神迹"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只24技能帝级级神兽。")
            end

end,
圣级神兽定制24礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=2500 ,体资=7600 ,法资=4100 ,防资=2500 ,速资=2500 ,躲资=2500,成长=2.5}
                    user.召唤兽:创建召唤兽("白豹","神兽",临时资质,0,{"高级连击","高级防御","高级吸血","高级偷袭","高级法术波动","泰山压顶","魔之心","法术连击","高级法术暴击","必杀","夜战","奔雷咒","地狱烈火","力劈华山","善恶有报","防御","吸血","连击","独行","高级驱鬼","遗志","夜舞倾城","月光","神迹"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只24技能圣级级神兽。")
            end

end,
十二技能定制攻宠礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=2000 ,体资=7000 ,法资=3500 ,防资=2000 ,速资=1800 ,躲资=1800,成长=2.5}
                    user.召唤兽:创建召唤兽("五行神兽","神兽",临时资质,0,{"嗜血追击","苍鸾怒击","高级必杀","高级感知","高级连击","高级敏捷","高级强力","高级驱鬼","高级神佑复生","高级偷袭","高级吸血","高级夜战"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只12技能定制攻宠。")
            end
end,
八随机技能胚子 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                     local 临时资质={攻资=1500 ,体资=5500 ,法资=3500 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("暗黑童子","宝宝",临时资质,0,{"吸血","反震","隐身","法术连击","再生","泰山压顶","冥思","慧根"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只11随机技能胚子。")
            end
end,
十一随机技能胚子 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                     local 临时资质={攻资=1500 ,体资=5500 ,法资=3500 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("暗黑童子","神兽",临时资质,0,{"吸血","反震","隐身","法术连击","再生","泰山压顶","冥思","慧根","必杀","招架","偷袭","驱鬼"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只11随机技能胚子。")
            end
end,
十五随机技能胚子 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                     local 临时资质={攻资=1500 ,体资=5500 ,法资=3500 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("暗黑童子","神兽",临时资质,0,{"火属性吸收","反震","隐身","法术连击","再生","泰山压顶","冥思","慧根","必杀","招架","幸运","感知","吸血","偷袭","驱鬼"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只12随机技能胚子。")
            end
end,
二十四随机技能胚子 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=1500 ,体资=5500 ,法资=3500 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("暗黑童子","神兽",临时资质,0,{"吸血","反震","隐身","法术连击","再生","魔之心","泰山压顶","冥思","慧根","必杀","招架","招架","偷袭","驱鬼","合纵","盾气","水属性吸收","火属性吸收","土属性吸收","雷属性吸收","高级驱鬼","遗志","落岩","雷击","神迹"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只24随机技能胚子。")
            end
end,
二十四技能定制攻宠礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=2000 ,体资=7000 ,法资=3500 ,防资=2000 ,速资=1800 ,躲资=1800,成长=2.5}
                    user.召唤兽:创建召唤兽("五行神兽","神兽",临时资质,0,{"高级感知","高级独行","高级反震","高级必杀","从天而降","苍鸾怒击","高级幸运","高级神迹","浮云神马","理直气壮","大快朵颐","神出鬼没","昼伏夜出","高级飞行","乘胜追击","高级吸血","溜之大吉","高级神佑复生","高级驱鬼","高级强力","高级敏捷","高级连击","力大无穷（土）","高级偷袭"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只24技能定制攻宠神兽。")
            end
end,
二十四技能定制法宠礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=2000 ,体资=7000 ,法资=3500 ,防资=2000 ,速资=1800 ,躲资=1800,成长=2.5}
                    user.召唤兽:创建召唤兽("五行神兽","神兽",临时资质,0,{"龙魂","叱咤风云","八凶法阵","上古灵符","天降灵葫","水漫金山","须弥真言","高级法术连击","食指大动","净台妙谛","流沙轻音","扶摇万里","灵山禅语","高级魔之心","泰山压顶","高级神佑复生","高级法术波动","高级敏捷","出其不意","高级法术暴击","地狱烈火","奔雷咒","月光","光照万象"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只24技能帝定制法宠神兽。")
            end
end,
孙小圣礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=1500 ,体资=5500 ,法资=3000 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("商城孙小圣","神兽",临时资质,0,{"须弥真言","泰山压顶","高级吸血","高级法术连击","永恒","高级法术暴击"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只礼包神兽。")
            end
end,
超级飞天礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=1500 ,体资=5500 ,法资=3000 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("商城超级飞天","神兽",临时资质,0,{"流沙轻音","高级魔之心","高级感知","高级法术连击","高级法术波动"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只礼包神兽。")
            end
end,
超级神马礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=1500 ,体资=5500 ,法资=3000 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("商城神马","神兽",临时资质,0,{"高级神佑复生","浮云神马","高级连击","幸运","神迹"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只礼包神兽。")
            end
end,
超级神鼠礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=1500 ,体资=5500 ,法资=3000 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("商城神鼠","神兽",临时资质,0,{"昼伏夜出","神出鬼没","溜之大吉","偷袭","吸血"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只礼包神兽。")
            end
end,
超级猪小戒礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=1500 ,体资=5500 ,法资=3000 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("商城超级猪小戒","神兽",临时资质,0,{"食指大动","高级魔之心","高级感知","高级法术波动","高级法术连击"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只礼包神兽。")
            end
end,
超级神猪礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=1500 ,体资=5500 ,法资=3000 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("商城超级神猪","神兽",临时资质,0,{"高级神佑复生","津津有味","高级防御","幸运","再生"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只礼包神兽。")
            end
end,
超级小白龙礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=1500 ,体资=5500 ,法资=3000 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("商城超级小白龙","神兽",临时资质,0,{"高级法术连击","高级法术暴击","叱咤风云","永恒","高级神佑复生"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只礼包神兽。")
            end
end,
超级神龙礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=1500 ,体资=5500 ,法资=3000 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("商城超级神龙","神兽",临时资质,0,{"龙魂","高级法术波动","奔雷咒","再生","魔之心"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只礼包神兽。")
            end
end,
超级青鸾礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=1500 ,体资=5500 ,法资=3000 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("商城超级青鸾","神兽",临时资质,0,{"高级驱鬼","苍鸾怒击","高级飞行","高级反震","高级连击"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只礼包神兽。")
            end
end,
超级赤焰兽礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=1500 ,体资=5500 ,法资=3000 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("商城超级赤焰兽","神兽",临时资质,0,{"高级法术暴击","八凶法阵","魔之心","高级法术波动","高级冥思"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只礼包神兽。")
            end
end,
超级神鸡兽礼包 = function(user,Item,PitchOn,Text)
                  if #user.召唤兽.数据 >= user.召唤兽.数据.召唤兽上限 then
                    SendMessage(user.连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    return true
                else
                    local 临时资质={攻资=1500 ,体资=5500 ,法资=3000 ,防资=1500 ,速资=1500 ,躲资=1500,成长=1.25}
                    user.召唤兽:创建召唤兽("商城超级神鸡","神兽",临时资质,0,{"高级神佑复生","高级强力","从天而降","驱鬼","必杀"})
                    SendMessage(user.连接id, 7, "#y/你获得了一只礼包神兽。")
            end
end,
银币礼包200W = function(user,Item,PitchOn,Text)
     RoleControl:添加银子(user,2000000,"银币礼包")
 end,

银币礼包1亿 = function(user,Item,PitchOn,Text)
     RoleControl:添加银子(user,100000000,"银币礼包")
 end,
银币礼包2亿 = function(user,Item,PitchOn,Text)
     RoleControl:添加银子(user,200000000,"银币礼包")
 end,
银币礼包5亿 = function(user,Item,PitchOn,Text)
     RoleControl:添加银子(user,500000000,"银币礼包")
 end,
银币礼包10亿 = function(user,Item,PitchOn,Text)
     RoleControl:添加银子(user,1000000000,"银币礼包")
 end,
银币礼包15亿 = function(user,Item,PitchOn,Text)
     RoleControl:添加银子(user,1500000000,"银币礼包")
 end,
银币礼包20亿 = function(user,Item,PitchOn,Text)
     RoleControl:添加银子(user,2000000000,"银币礼包")
end,

笑傲江湖称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"笑傲江湖")
   end,
江湖少侠称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"江湖少侠")
   end,
江湖大侠称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"江湖大侠")
   end,
江湖豪侠称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"江湖豪侠")
   end,
武林高手称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"武林高手")
   end,
孤独求败称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"孤独求败")
   end,
天外飞仙称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"天外飞仙")
   end,
武林盟主称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"武林盟主")
   end,
大内密探称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"大内密探")
   end,
皓月战神称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"皓月战神")
   end,
万元户称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"万元户")
   end,
梦回新秀称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"★梦回新秀★")
   end,
覆海神龙称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"覆海神龙")
   end,
归元圣者称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"归元圣者")
   end,
忘情罗刹称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"忘情罗刹")
   end,
梦幻元勋称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"梦幻元勋")
   end,
三届凌绝顶称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"三届凌绝顶")
   end,
天人开造化称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"天人开造化")
   end,
梦幻任逍遥称谓礼包 = function(user,Item,PitchOn,Text)

                    RoleControl:添加称谓(user,"梦幻任逍遥")
   end,
  
灵饰气血佩饰礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 1 then
                    SendMessage(user.id, 7, "#y/请先预留出1个道具空间")
                    return true
                else
                  EquipmentControl:定制灵饰(user.id,160,"佩饰","速度",53,"1","气血=204@气血=204@气血=204@气血=204")
                end
end,
灵饰防御佩饰礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 1 then
                    SendMessage(user.id, 7, "#y/请先预留出1个道具空间")
                    return true
                else
                  EquipmentControl:定制灵饰(user.id,160,"佩饰","速度",53,"1","防御=64@防御=64@防御=64@防御=64")
                end
end,
灵饰防御手镯礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 1 then
                    SendMessage(user.id, 7, "#y/请先预留出1个道具空间")
                    return true
                else
                  EquipmentControl:定制灵饰(user.id,160,"手镯","抵抗封印等级",53,"1","防御=64@防御=64@防御=64@防御=64")
                end
end,
灵饰气血手镯礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 1 then
                    SendMessage(user.id, 7, "#y/请先预留出1个道具空间")
                    return true
                else
                  EquipmentControl:定制灵饰(user.id,160,"手镯","抵抗封印等级",53,"1","气血=204@气血=204@气血=204@气血=204")
                end
end,
灵饰伤害戒指礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 1 then
                    SendMessage(user.id, 7, "#y/请先预留出1个道具空间")
                    return true
                else
                  EquipmentControl:定制灵饰(user.id,160,"戒指","伤害",53,"1","伤害=44@伤害=44@伤害=44@伤害=44")
                end
end,
灵饰物理暴击戒指礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 1 then
                    SendMessage(user.id, 7, "#y/请先预留出1个道具空间")
                    return true
                else
                  EquipmentControl:定制灵饰(user.id,160,"戒指","伤害",53,"1","物理暴击等级=44@物理暴击等级=44@物理暴击等级=44@物理暴击等级=44")
                end
end,
灵饰伤害耳饰礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 1 then
                    SendMessage(user.id, 7, "#y/请先预留出1个道具空间")
                    return true
                else
                  EquipmentControl:定制灵饰(user.id,160,"耳饰","法术防御",53,"1","伤害=44@伤害=44@伤害=44@伤害=44")
                end
end,
灵饰物理暴击耳饰礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 1 then
                    SendMessage(user.id, 7, "#y/请先预留出1个道具空间")
                    return true
                else
                  EquipmentControl:定制灵饰(user.id,160,"耳饰","法术防御",53,"1","物理暴击等级=44@物理暴击等级=44@物理暴击等级=44@物理暴击等级=44")
                end
end,
灵饰法术伤害戒指礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 1 then
                    SendMessage(user.id, 7, "#y/请先预留出1个道具空间")
                    return true
                else
                  EquipmentControl:定制灵饰(user.id,160,"戒指","防御",53,"1","法术伤害=64@法术伤害=64@法术伤害=64@法术伤害=64")
                end
end,
灵饰法术暴击戒指礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 1 then
                    SendMessage(user.id, 7, "#y/请先预留出1个道具空间")
                    return true
                else
                  EquipmentControl:定制灵饰(user.id,160,"戒指","防御",53,"1","法术暴击等级=44@法术暴击等级=44@法术暴击等级=44@法术暴击等级=44")
                end
end,
灵饰法术暴击耳饰礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 1 then
                    SendMessage(user.id, 7, "#y/请先预留出1个道具空间")
                    return true
                else
                  EquipmentControl:定制灵饰(user.id,160,"耳饰","法术伤害",53,"1","法术暴击等级=44@法术暴击等级=44@法术暴击等级=44@法术暴击等级=44")
                end
end,
灵饰法术伤害耳饰礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 1 then
                    SendMessage(user.id, 7, "#y/请先预留出1个道具空间")
                    return true
                else
                  EquipmentControl:定制灵饰(user.id,160,"耳饰","法术伤害",53,"1","法术伤害=64@法术伤害=64@法术伤害=64@法术伤害=64")
                end
end,
灵饰治疗能力耳饰礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 1 then
                    SendMessage(user.id, 7, "#y/请先预留出1个道具空间")
                    return true
                else
                  EquipmentControl:定制灵饰(user.id,160,"耳饰","法术防御",53,"1","治疗能力=44@治疗能力=44@治疗能力=44@治疗能力=44")
                end
end,
灵饰治疗能力戒指礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 1 then
                    SendMessage(user.id, 7, "#y/请先预留出1个道具空间")
                    return true
                else
                  EquipmentControl:定制灵饰(user.id,160,"戒指","防御",53,"1","治疗能力=44@治疗能力=44@治疗能力=44@治疗能力=44")
                end
end,
善恶点化石礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 3 then
                    SendMessage(user.id, 7, "#y/请先预留出3个道具空间")
                    return true
                else
                   ItemControl:GiveItem(user.id,"点化石",nil,"善恶有报")
                   ItemControl:GiveItem(user.id,"点化石",nil,"善恶有报")
                   ItemControl:GiveItem(user.id,"点化石",nil,"善恶有报")
                end
end,
剑荡四方点化石礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 3 then
                    SendMessage(user.id, 7, "#y/请先预留出3个道具空间")
                    return true
                else
                   ItemControl:GiveItem(user.id,"点化石",nil,"剑荡四方")
                   ItemControl:GiveItem(user.id,"点化石",nil,"剑荡四方")
                   ItemControl:GiveItem(user.id,"点化石",nil,"剑荡四方")
                end
end,
高级神佑复生点化石礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 3 then
                    SendMessage(user.id, 7, "#y/请先预留出3个道具空间")
                    return true
                else
                   ItemControl:GiveItem(user.id,"点化石",nil,"高级神佑复生")
                   ItemControl:GiveItem(user.id,"点化石",nil,"高级神佑复生")
                   ItemControl:GiveItem(user.id,"点化石",nil,"高级神佑复生")
                end
end,
新手CDK兑换礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 10 then
                    SendMessage(user.连接id, 7, "#y/请先预留出10个道具空间")
                    return true
                else
                    ItemControl:GiveItem(user.id,"120级玩家礼包",nil,nil,nil,1)
                    ItemControl:GiveItem(user.id,"孙小圣礼包",nil,nil,nil,1)
                end
   end,
牛转钱坤礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 10 then
                    SendMessage(user.连接id, 7, "#y/请先预留出10个道具空间")
                    return true
                else
                    ItemControl:GiveItem(user.id,"灵兜兜",nil,nil,nil,10)
                    ItemControl:GiveItem(user.id,"神兜兜",nil,nil,nil,100)
                    ItemControl:GiveItem(user.id,"银币礼包5亿",nil,nil,nil,1)
                    ItemControl:GiveItem(user.id,"仙玉30000",nil,nil,nil,1)
                    ItemControl:GiveItem(user.id,"随机法宝礼盒",nil,nil,nil,1)
                    ItemControl:GiveItem(user.id,"随机锦衣礼包",nil,nil,nil,1)
                    ItemControl:GiveItem(user.id,"随机脚印礼包",nil,nil,nil,1)
                    ItemControl:GiveItem(user.id,"随机光环礼包",nil,nil,nil,1)
                    ItemControl:GiveItem(user.id,"160双加100礼包",nil,nil,nil,1)
                    ItemControl:GiveItem(user.id,"普通攻宠礼包",nil,nil,nil,1)

                end
   end,
牛气冲天礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 10 then
                    SendMessage(user.连接id, 7, "#y/请先预留出10个道具空间")
                    return true
                else
                    ItemControl:GiveItem(user.id,"灵兜兜",nil,nil,nil,10)
                    ItemControl:GiveItem(user.id,"神兜兜",nil,nil,nil,100)
                    ItemControl:GiveItem(user.id,"银币礼包5亿",nil,nil,nil,1)
                    ItemControl:GiveItem(user.id,"仙玉30000",nil,nil,nil,1)
                    ItemControl:GiveItem(user.id,"随机法宝礼盒",nil,nil,nil,1)
                    ItemControl:GiveItem(user.id,"随机锦衣礼包",nil,nil,nil,1)
                    ItemControl:GiveItem(user.id,"随机脚印礼包",nil,nil,nil,1)
                    ItemControl:GiveItem(user.id,"随机光环礼包",nil,nil,nil,1)
                    ItemControl:GiveItem(user.id,"160双加100礼包",nil,nil,nil,1)
                    ItemControl:GiveItem(user.id,"普通法宠礼包",nil,nil,nil,1)

                end
   end,
宝石大礼包 = function(user,Item,PitchOn,Text)
                if RoleControl:取可用格子数量(user,"包裹") < 6 then
                    SendMessage(user.连接id, 7, "#y/请先预留出6个道具空间")
                    return true
                else
                ItemControl:GiveItem(user.id,"月亮石",math.random(8,13))
                ItemControl:GiveItem(user.id,"黑宝石",math.random(8,13))
                ItemControl:GiveItem(user.id,"红玛瑙",math.random(8,13))
                ItemControl:GiveItem(user.id,"太阳石",math.random(8,13))
                ItemControl:GiveItem(user.id,"舍利子",math.random(8,13))
                ItemControl:GiveItem(user.id,"光芒石",math.random(8,13))
              end

end,


福气结晶 = function(user,Item,PitchOn,Text)
         RoleControl:添加仙玉(user,100,"福气结晶")
end,
仙玉200 = function(user,Item,PitchOn,Text)
         RoleControl:添加仙玉(user,200,"仙玉点卡")
end,
仙玉1000 = function(user,Item,PitchOn,Text)
         RoleControl:添加仙玉(user,1000,"仙玉点卡")
end,
仙玉2000 = function(user,Item,PitchOn,Text)
         RoleControl:添加仙玉(user,2000,"仙玉点卡")
end,
仙玉2500 = function(user,Item,PitchOn,Text)
         RoleControl:添加仙玉(user,2500,"仙玉点卡")
end,
仙玉3000 = function(user,Item,PitchOn,Text)
         RoleControl:添加仙玉(user,3000,"仙玉点卡")
end,
仙玉5000 = function(user,Item,PitchOn,Text)
         RoleControl:添加仙玉(user,5000,"仙玉点卡")
end,
仙玉10000 = function(user,Item,PitchOn,Text)
         RoleControl:添加仙玉(user,10000,"仙玉点卡")
end,
仙玉20000 = function(user,Item,PitchOn,Text)
         RoleControl:添加仙玉(user,20000,"仙玉点卡")
end,
仙玉30000 = function(user,Item,PitchOn,Text)
         RoleControl:添加仙玉(user,30000,"仙玉点卡")
end,
仙玉50000  = function(user,Item,PitchOn,Text)
         RoleControl:添加仙玉(user,50000,"仙玉点卡")
end,
仙玉80000  = function(user,Item,PitchOn,Text)
         RoleControl:添加仙玉(user,80000,"仙玉点卡")
end,
仙玉100000 = function(user,Item,PitchOn,Text)
         RoleControl:添加仙玉(user,100000,"仙玉点卡")
end,
仙玉200000 = function(user,Item,PitchOn,Text)
         RoleControl:添加仙玉(user,200000,"仙玉点卡")
end,
随机法宝礼盒 = function(user,Item,PitchOn,Text)
        local  随机法宝 ={"碧玉葫芦","飞剑","拭剑石","金甲仙衣","缚妖索","金钱镖","惊魂铃","嗜血幡","风袋","九黎战鼓","盘龙壁","神行飞剑","汇灵盏","天师符","织女扇","雷兽","迷魂灯","定风珠","幽灵珠","会神珠",
    "降魔斗篷","附灵玉","捆仙绳","发瘟匣","五彩娃娃","七杀","罗汉珠","赤焰","金刚杵","兽王令","摄魂","神木宝鼎","宝烛","天煞","琉璃灯","云雷鼓",
    "落雨金钱","缩地尺","缚龙索","鬼泣","月光宝盒","混元伞","无魂傀儡","苍白纸人","聚宝盆","影蛊","乾坤玄火塔","无尘扇","无字经","葬龙笛","神匠袍","金箍棒","干将莫邪","救命毫毛","伏魔天书","普渡","镇海珠","奇门五行令","失心钹","金蟾",
    "舞雪冰蝶","河图洛书","炽焰丹珠","凌波仙符","流影云笛","谷玄星盘","重明战鼓","蟠龙玉璧","翡翠芝兰", "梦云幻甲","软烟罗锦"}
    local  获得法宝  =随机法宝[math.random(1,#随机法宝)]
    ItemControl:GiveItem(user.id,获得法宝 )
end,

随机锦衣礼包 = function(user,Item,PitchOn,Text)
                         local 锦衣 ={"羽仙歌·墨黑","羽仙歌·月白","浪淘纱·月白","齐天大圣","浪淘纱","冰灵蝶翼·月笼","从军行·须眉","铃儿叮当","青花瓷·月白","落星织","碧华锦·凌雪","冰雪玉兔","五毒锦绣","鹿角湾湾","浪迹天涯","胡旋回雪","萌萌小厨","鎏金婚姻服","雀之恋骨"}
                 ItemControl:GiveItem(user.id,锦衣[math.random(#锦衣)])
end,
随机脚印礼包 = function(user,Item,PitchOn,Text)
                         local 锦衣 ={"飞天足迹","龙卷风足迹","皮球足迹","旋律足迹","光剑足迹","地裂足迹","猫爪足迹","爱心足迹","元宝足迹","桃心足迹(红)","桃心足迹(粉)","波纹足迹(绿)","波纹足迹(蓝)","波纹足迹(粉)","蝴蝶足迹","普通足迹","璀璨烟花","跃动喷泉","鱼群足迹","炎火足迹","旋星足迹","星如雨","星光","小心机","藤蔓蔓延","闪光足迹","雀屏足迹","鬼脸南瓜","浮游水母","枫叶足迹","翅膀足迹","采蘑菇" }
                 ItemControl:GiveItem(user.id,锦衣[math.random(#锦衣)])
end,
随机光环礼包 = function(user,Item,PitchOn,Text)
                         local 锦衣 ={"烈焰澜翻","双鲤寄情","凌波微步","水墨游龙","星光熠熠","浩瀚星河","荷塘涟漪"}
                 ItemControl:GiveItem(user.id,锦衣[math.random(#锦衣)])
end,
特殊魔兽要诀礼包 = function(user,Item,PitchOn,Text)
                 local 临时名称 ={"须弥真言","光照万象","风起龙游","欣欣向荣","无赦魔决","凝光练彩","食指大动","流沙轻音","理直气壮","叱咤风云","出其不意","灵山禅语","无畏布施","净台妙谛","津津有味","哼哼哈兮","大快朵颐","龙魂","浮云神马","千钧一怒","灵能激发","从天而降","八凶法阵","天降灵葫","月光","苍鸾怒击"}
                 ItemControl:GiveItem(user.id,"特殊魔兽要诀",临时名称[math.random(#临时名称)])
end,




}
物品.二级未激活符石 =物品.一级未激活符石
物品.三级未激活符石 =物品.一级未激活符石
物品["玄天残卷·上卷"] = function(user,Item,PitchOn,Text)
         local gz = ItemControl:寻找道具(user,{"玄天残卷·上卷","玄天残卷·中卷","玄天残卷·下卷"})
         if #gz ~= 3 then
           SendMessage(user.连接id, 7, "#y/只有集齐玄天残卷·上卷、玄天残卷·中卷、玄天残卷·下卷才可合成")
         else
            for n = 1, 3 do
             ItemControl:RemoveItems(user,gz[n],"玄天残卷·上卷")
           end
           ItemControl:GiveItem(user.id,"圣兽丹")
         end
         return true
end
物品["玄天残卷·中卷"]= 物品["玄天残卷·上卷"]
物品["玄天残卷·下卷"]= 物品["玄天残卷·上卷"]
物品["红包-我"] = function(user,Item,PitchOn,Text)
      local gz = ItemControl:寻找道具(user,{"红包-我","红包-为","红包-中","红包-国","红包-喝","红包-彩"})
        if #gz ~= 6 then
           SendMessage(user.连接id, 7, "#y/只有集齐红包-我、为、中、国、喝、彩才可合成")
         else
           for n = 1, 6 do
              ItemControl:RemoveItems(user,gz[n],"新年红包")
           end
           ItemControl:GiveItem(user.id,"新年红包")

        end
        return true
end
物品["红包-为"]=物品["红包-我"]
物品["红包-中"]=物品["红包-我"]
物品["红包-国"]=物品["红包-我"]
物品["红包-喝"]=物品["红包-我"]
物品["红包-彩"]=物品["红包-我"]

物品["大米"] = function(user,Item,PitchOn,Text)
      local gz = ItemControl:寻找道具(user,{"千年阴沉木","麒麟血","金凤羽","玄龟板","内丹","大米"})
        if #gz ~= 6 then
           SendMessage(user.连接id, 7, "#y/只有集齐千年阴沉木,麒麟血,金凤羽,玄龟板,内丹,大米才可合成法宝礼盒")
         else
           for n = 1, 6 do
              ItemControl:RemoveItems(user,gz[n],"法宝礼盒")
           end
           ItemControl:GiveItem(user.id,"法宝礼盒")

        end
        return true
end
物品["千年阴沉木"]=物品["法宝礼盒"]
物品["麒麟血"]=物品["法宝礼盒"]
物品["金凤羽"]=物品["法宝礼盒"]
物品["玄龟板"]=物品["法宝礼盒"]
物品["内丹"]=物品["法宝礼盒"]

物品["120无级别单件"] = function(user,Item,PitchOn,Text)
         if RoleControl:取可用格子数量(user,"包裹") < 1 then
            SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
          return true
         end
           EquipmentControl:取单件装备礼包(user.id,120)
           SendMessage(user.连接id, 3006, "66")
end
物品["130无级别单件"] = function(user,Item,PitchOn,Text)
         if RoleControl:取可用格子数量(user,"包裹") < 1 then
            SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
          return true
         end
           EquipmentControl:取单件装备礼包(user.id,130)
           SendMessage(user.连接id, 3006, "66")
end
物品["140无级别单件"] = function(user,Item,PitchOn,Text)
         if RoleControl:取可用格子数量(user,"包裹") < 1 then
            SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
          return true
         end
           EquipmentControl:取单件装备礼包(user.id,140)
           SendMessage(user.连接id, 3006, "66")
end
物品["150无级别单件"] = function(user,Item,PitchOn,Text)
         if RoleControl:取可用格子数量(user,"包裹") < 1 then
            SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
          return true
         end
           EquipmentControl:取单件装备礼包(user.id,150)
           SendMessage(user.连接id, 3006, "66")
end
物品["160无级别单件"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
        EquipmentControl:取单件装备礼包(user.id,160)
        SendMessage(user.连接id, 3006, "66")
end
物品["150无级别礼包"] = function(user,Item,PitchOn,Text)
         if RoleControl:取可用格子数量(user,"包裹") < 1 then
            SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
          return true
         end
           EquipmentControl:取单件装备礼包(user.id,150)
           SendMessage(user.连接id, 3006, "66")
end
物品["160无级别礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
        EquipmentControl:取单件装备礼包(user.id,160)
         -- EquipmentControl:取单件装备礼包(user.id,160,100)
        SendMessage(user.连接id, 3006, "66")
end
物品["120级玩家礼包"] = function(user,Item,PitchOn,Text)
         if RoleControl:取可用格子数量(user,"包裹") < 6 then
            SendMessage(user.连接id, 7, "#y/请先预留出6个道具空间")
            return true
         end

               EquipmentControl:取装备礼包(user.id,120,nil,true,true)-- EquipmentControl:取装备礼包(user.id,120,nil,true,true,500000)--限时时间
        
           
            SendMessage(user.连接id, 3006, "66")
end
物品["150级玩家礼包"] = function(user,Item,PitchOn,Text)
         if RoleControl:取可用格子数量(user,"包裹") < 6 then
                SendMessage(user.连接id, 7, "#y/请先预留出6个道具空间")
            return true
         end

               EquipmentControl:取装备礼包(user.id,150,nil,true,true)-- EquipmentControl:取装备礼包(user.id,150,nil,true,true,500000)--限时时间
     

           SendMessage(user.连接id, 3006, "66")
end
物品["160级玩家礼包"] = function(user,Item,PitchOn,Text)
         if RoleControl:取可用格子数量(user,"包裹") < 6 then
                SendMessage(user.连接id, 7, "#y/请先预留出6个道具空间")
            return true
         end
            if 小凡 then
               EquipmentControl:取装备礼包(user.id,160,nil,true)-- EquipmentControl:取装备礼包(user.id,160,nil,true,true,500000)--限时时间
            else
               EquipmentControl:取装备礼包(user.id,160,nil,true,true)-- EquipmentControl:取装备礼包(user.id,160,nil,true,true,500000)--限时时间
            end
      
           SendMessage(user.连接id, 3006, "66")
end
物品["160双加100礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,160,100)
        SendMessage(user.连接id, 3006, "66")
end
物品["160双加120礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,160,120)
        SendMessage(user.连接id, 3006, "66")
end
物品["160双加150礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,160,150)
        SendMessage(user.连接id, 3006, "66")
end
物品["160双加200礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,160,200)
        SendMessage(user.连接id, 3006, "66")
end
物品["160双加250礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,160,250)
        SendMessage(user.连接id, 3006, "66")
end
物品["160双加300礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,160,300)
        SendMessage(user.连接id, 3006, "66")
end

物品["150双加50礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,150,50)
        SendMessage(user.连接id, 3006, "66")
end
物品["150双加80礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,150,80)
        SendMessage(user.连接id, 3006, "66")
end
物品["150双加100礼包"] = function(user,Item,PitchOn,Text)  --150双加
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,150,100)
        SendMessage(user.连接id, 3006, "66")
end
物品["150双加150礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,150,150)
        SendMessage(user.连接id, 3006, "66")
end
物品["150双加120礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,150,120)
        SendMessage(user.连接id, 3006, "66")
end
物品["150双加200礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,150,200)
        SendMessage(user.连接id, 3006, "66")
end
物品["150双加250礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,150,250)
        SendMessage(user.连接id, 3006, "66")
end
物品["150双加300礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,150,300)
        SendMessage(user.连接id, 3006, "66")
end
物品["140双加100礼包"] = function(user,Item,PitchOn,Text)  --140双加
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,140,100)
        SendMessage(user.连接id, 3006, "66")
end
物品["140双加150礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,140,140)
        SendMessage(user.连接id, 3006, "66")
end
物品["140双加120礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,140,120)
        SendMessage(user.连接id, 3006, "66")
end
物品["140双加200礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,140,200)
        SendMessage(user.连接id, 3006, "66")
end
物品["140双加250礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,140,250)
        SendMessage(user.连接id, 3006, "66")
end
物品["140双加300礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,140,300)
        SendMessage(user.连接id, 3006, "66")
end
物品["130双加100礼包"] = function(user,Item,PitchOn,Text)  --130双加
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,130,100)
        SendMessage(user.连接id, 3006, "66")
end
物品["130双加150礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,130,130)
        SendMessage(user.连接id, 3006, "66")
end
物品["130双加200礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,130,200)
        SendMessage(user.连接id, 3006, "66")
end
物品["130双加250礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,130,250)
        SendMessage(user.连接id, 3006, "66")
end
物品["130双加300礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,130,300)
        SendMessage(user.连接id, 3006, "66")
end

物品["120双加200礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,120,200)
        SendMessage(user.连接id, 3006, "66")
end
物品["120双加250礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,120,250)
        SendMessage(user.连接id, 3006, "66")
end
物品["120双加300礼包"] = function(user,Item,PitchOn,Text)
        if RoleControl:取可用格子数量(user,"包裹") < 1 then
        SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
        return true
        end
          EquipmentControl:取单件装备礼包(user.id,120,300)
        SendMessage(user.连接id, 3006, "66")
end

物品["160双加100整套礼包"] = function(user,Item,PitchOn,Text)
         if RoleControl:取可用格子数量(user,"包裹") < 6 then
            SendMessage(user.连接id, 7, "#y/请先预留出6个道具空间")
            return true
         end
            EquipmentControl:取装备礼包(user.id,160,nil,true,nil,100)
            -- EquipmentControl:取装备礼包(id,等级,新手,属性,专用,数值)
            SendMessage(user.连接id, 3006, "66")
end


物品["首充礼包"] = function(user,Item,PitchOn,Text)
         if RoleControl:取可用格子数量(user,"包裹") < 6 then
            SendMessage(user.连接id, 7, "#y/请先预留出6个道具空间")
            return true
         end
            if 小凡 then
               EquipmentControl:取装备礼包(user.id,140,nil,true)-- EquipmentControl:取装备礼包(user.id,140,nil,true,true,500000)--限时时间
            else
               EquipmentControl:取装备礼包(user.id,140,nil,true,true)-- EquipmentControl:取装备礼包(user.id,140,nil,true,true,500000)--限时时间
            end
           
            SendMessage(user.连接id, 3006, "66")
end
物品["内测礼包"] = function(user,Item,PitchOn,Text)
         if RoleControl:取可用格子数量(user,"包裹") < 4 then
            SendMessage(user.连接id, 7, "#y/请先预留出4个道具空间")
            return true
         end
 
      ItemControl:GiveItem(user.id,"双倍经验丹",nil,nil,5)
      ItemControl:GiveItem(user.id,"特赦令牌",nil,nil,5)
      ItemControl:GiveItem(user.id,"碧华锦·凌雪")
      ItemControl:GiveItem(user.id,"考古铁铲")
       ItemControl:GiveItem(user.id,"考古铁铲")
        ItemControl:GiveItem(user.id,"考古铁铲")
         ItemControl:GiveItem(user.id,"考古铁铲")
          ItemControl:GiveItem(user.id,"考古铁铲")
end





物品["推广礼包3人"] = function(user,Item,PitchOn,Text)
         if RoleControl:取可用格子数量(user,"包裹") < 1 then
            SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
            return true
         end
 
      ItemControl:GiveItem(user.id,"彩果",nil,nil,30)
            RoleControl:添加银子(user,5000000,"首充礼包")
            RoleControl:添加储备(user,10000000,"首充礼包")

end

物品["推广礼包5人"] = function(user,Item,PitchOn,Text)
         if RoleControl:取可用格子数量(user,"包裹") < 1 then
            SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
            return true
         end
 
      ItemControl:GiveItem(user.id,"金银宝盒",nil,nil,50)
    
            RoleControl:添加储备(user,30000000,"首充礼包")

end
物品["推广礼包10人"] = function(user,Item,PitchOn,Text)
         if RoleControl:取可用格子数量(user,"包裹") < 1 then
            SendMessage(user.连接id, 7, "#y/请先预留出1个道具空间")
            return true
         end
          local   suij= {
          "羽仙歌·墨黑"
          ,"羽仙歌·月白"
          ,"浪淘纱·月白"
          ,"齐天大圣"
          ,"浪淘纱"
          ,"飞天舞"
          ,"从军行·须眉"
          ,"铃儿叮当"
          ,"青花瓷·月白"
          ,"落星织"
          ,"碧华锦·凌雪"
          ,"冰雪玉兔"
          ,"五毒锦绣"
          ,"鹿角湾湾"
          ,"浪迹天涯"
          ,"胡旋回雪"
          ,"萌萌小厨"
          ,"鎏金婚姻服"
          ,"雀之恋骨"}
            ItemControl:GiveItem(user.id,suij[math.random(#suij)]) --锦衣
            RoleControl:添加银子(user,10000000,"首充礼包")
            RoleControl:添加仙玉(user,5000,"首充礼包")

end
物品["推广礼包15人"] = function(user,Item,PitchOn,Text)
         if RoleControl:取可用格子数量(user,"包裹") < 3 then
            SendMessage(user.连接id, 7, "#y/请先预留出3个道具空间")
            return true
         end
                   local   suij= {
         "飞天足迹"
,"龙卷风足迹"
,"皮球足迹"
,"旋律足迹"
,"光剑足迹"
,"地裂足迹"
,"猫爪足迹"
,"爱心足迹"
,"元宝足迹"
,"桃心足迹(红)"
,"桃心足迹(粉)"
,"波纹足迹(绿)"
,"波纹足迹(蓝)"
,"波纹足迹(粉)"
,"蝴蝶足迹"
,"普通足迹"
,"璀璨烟花"
,"跃动喷泉"
,"鱼群足迹"
,"炎火足迹"
,"旋星足迹"
,"星如雨"
,"星光"
,"小心机"
,"藤蔓蔓延"
,"闪光足迹"
,"雀屏足迹"
,"鬼脸南瓜"
,"浮游水母"
,"枫叶足迹"
,"翅膀足迹"
,"采蘑菇"}

           ItemControl:GiveItem(user.id,suij[math.random(#suij)]) --锦衣
      ItemControl:GiveItem(user.id,"金银宝盒",nil,nil,99)
    ItemControl:GiveItem(user.id,"修炼果",nil,nil,10)
     
end
物品["推广礼包20人"] = function(user,Item,PitchOn,Text)
         if RoleControl:取可用格子数量(user,"包裹") < 2 then
            SendMessage(user.连接id, 7, "#y/请先预留出2个道具空间")
            return true
         end

                local   suij= {
                  "烈焰澜翻"
                  ,"双鲤寄情"
                  ,"凌波微步"
                  ,"水墨游龙"
                  ,"星光熠熠"
                  ,"浩瀚星河"
                  ,"荷塘涟漪"}       
 ItemControl:GiveItem(user.id,suij[math.random(#suij)]) --锦衣
    ItemControl:GiveItem(user.id,"神兜兜",nil,nil,33)
end
物品["推广礼包30人"] = function(user,Item,PitchOn,Text)
         if RoleControl:取可用格子数量(user,"包裹") < 3 then
            SendMessage(user.连接id, 7, "#y/请先预留出3个道具空间")
            return true
         end
 
      ItemControl:GiveItem(user.id,"修炼果",nil,nil,20)
      ItemControl:GiveItem(user.id,"特赦令牌")
      ItemControl:GiveItem(user.id,"特赦令牌")
      ItemControl:GiveItem(user.id,"特赦令牌")
RoleControl:添加仙玉(user,30000,"首充礼包")
end



function ItemControl:消耗道具使用(user,Item,PitchOn,Text)
   return 物品[Item.名称](user,Item,PitchOn,Text) 
end
function ItemControl:队标使用(user,Item)
   for i=1,#user.角色.队标 do
     if user.角色.队标[i]== Item.名称 then
        SendMessage(user.连接id,7,"你已经掌握了这个队标，不需要重新掌握")
        return true
     end
   end
   table.insert(user.角色.队标,Item.名称)
   SendMessage(user.连接id, 7, "#y/你掌握了"..Item.名称)
end
function ItemControl:合成旗使用(user,Item,PitchOn)------------------------完成
 if #Item.坐标<7 then
      if Item.地图编号==0 then
            if user.地图==1001 or user.地图==1070 or user.地图==1501 or user.地图==1092 or user.地图==1208 or user.地图==1173   then
            Item.地图编号=user.地图
            Item.地图=MapData[user.地图].名称
            Item.坐标[#Item.坐标+1]={x=math.floor(user.角色.地图数据.x/20),y=math.floor(user.角色.地图数据.y/20)}
            SendMessage(user.连接id,7,"#Y/定标成功，当前已定标#r/"..#Item.坐标.."#Y/个点")
            SendMessage(user.连接id,3006,"刷新道具")
            return true
            else
            SendMessage(user.连接id,7,"#Y/只可以在长安城、建邺城、傲来国、长寿村、朱紫国、大唐境外处定标")
            return true
            end
      elseif Item.地图编号==user.地图 then
         Item.坐标[#Item.坐标+1]={x=math.floor(user.角色.地图数据.x/20),y=math.floor(user.角色.地图数据.y/20)}
          SendMessage(user.连接id,7,"#Y/定标成功，当前已定标#R/"..#Item.坐标.."#Y/个点")
          return true
      else
             SendMessage(user.连接id,7,"#Y/您只能在#G/"..MapData[Item.地图编号].名称.."#Y/处定标")
             return true
      end
 else

      if user.队伍~=0 and user.队长==false then
       SendMessage(user.连接id,7,"#Y/只有队长才可使用此道具")
        return true
      else
            local 允许使用=true
            local 提示信息 =""
            if RoleControl:取飞行限制(user)==false or PitchOn ==0 then
            允许使用=false
            提示信息="#Y/您当前无法使用飞行道具"
            end
            if user.队伍~=0 then
              for n=1,#队伍数据[user.队伍].队员数据 do
              local 临时id1=队伍数据[user.队伍].队员数据[n]
                if RoleControl:取飞行限制(user)==false then
                允许使用=false
                提示信息="#Y/"..user.角色.名称.."无法使用飞行道具"
                end
              end
            end
            if 允许使用==false then
               SendMessage(user.连接id,7,提示信息)
               return true
            else
                MapControl:Jump(user.id,Item.地图编号,Item.坐标[PitchOn].x,Item.坐标[PitchOn].y)
                Item.次数=Item.次数-1
                if Item.次数<1 then
                 SendMessage(user.连接id,7,"#Y/您的合成旗次数已经用尽")
                else
                   return true
                end
            end
      end
 end
end
function ItemControl:超级合成旗使用(user,Item,PitchOn)------------------------完成
 if #Item.坐标<21 then
      if Item.地图编号==0 then
            if user.地图==1001 or user.地图==1070 or user.地图==1501 or user.地图==1092 or user.地图==1208 or user.地图==1173  or user.地图==1110 or user.地图==1111 or user.地图==1174 then
            Item.地图编号=user.地图
            Item.地图=MapData[user.地图].名称
            Item.坐标[#Item.坐标+1]={x=math.floor(user.角色.地图数据.x/20),y=math.floor(user.角色.地图数据.y/20)}
            SendMessage(user.连接id,7,"#Y/定标成功，当前已定标#r/"..#Item.坐标.."#Y/个点")
            SendMessage(user.连接id,3006,"刷新道具")
            return true
            else
            SendMessage(user.连接id,7,"#Y/只可以在长安城、建邺城、傲来国、长寿村、朱紫国、大唐境外、大唐国境、北俱、天宫处定标")
            return true
            end
      elseif Item.地图编号==user.地图 then
         Item.坐标[#Item.坐标+1]={x=math.floor(user.角色.地图数据.x/20),y=math.floor(user.角色.地图数据.y/20)}
          SendMessage(user.连接id,7,"#Y/定标成功，当前已定标#R/"..#Item.坐标.."#Y/个点")
          return true
      else
             SendMessage(user.连接id,7,"#Y/您只能在#G/"..MapData[Item.地图编号].名称.."#Y/处定标")
             return true
      end
 else

      if user.队伍~=0 and user.队长==false then
       SendMessage(user.连接id,7,"#Y/只有队长才可使用此道具")
        return true
      else
            local 允许使用=true
            local 提示信息 =""
            if RoleControl:取飞行限制(user)==false or PitchOn ==0 then
            允许使用=false
            提示信息="#Y/您当前无法使用飞行道具"
            end
            if user.队伍~=0 then
              for n=1,#队伍数据[user.队伍].队员数据 do
              local 临时id1=队伍数据[user.队伍].队员数据[n]
                if RoleControl:取飞行限制(user)==false then
                允许使用=false
                提示信息="#Y/"..user.角色.名称.."无法使用飞行道具"
                end
              end
            end
            if 允许使用==false then
               SendMessage(user.连接id,7,提示信息)
               return true
            else
                MapControl:Jump(user.id,Item.地图编号,Item.坐标[PitchOn].x,Item.坐标[PitchOn].y)
                Item.次数=Item.次数-1
                if Item.次数<1 then
                 SendMessage(user.连接id,7,"#Y/您的合成旗次数已经用尽")
                else
                   return true
                end
            end
      end
 end
end
function ItemControl:导标旗使用(user,Item)--------------------------------完成--
  if Item.地图编号 ~= 0 then
        if user.队伍~=0 and user.队长==false then
            SendMessage(user.连接id,7,"#Y/只有队长才可使用此道具")
             return true
        else
             local 允许使用=true
             local 提示信息=""
             if RoleControl:取飞行限制(user)==false then
               允许使用=false
               提示信息="#Y/您当前无法使用飞行道具"
             end
             if user.队伍~=0 then
                  for n=1,#队伍数据[user.队伍].队员数据 do
                        local 临时id1=队伍数据[user.队伍].队员数据[n]
                        if RoleControl:取飞行限制(UserData[临时id1])==false then
                          允许使用=false
                          提示信息="#y/"..UserData[临时id1].角色.名称.."无法使用飞行道具"
                        end
                   end
              end
             if 允许使用==false then
               SendMessage(user.连接id,7,提示信息)
                return true
             else
                  MapControl:Jump(user.id,Item.地图编号,Item.x,Item.y)
                  Item.次数=Item.次数-1
                  if Item.次数<1 then
                  SendMessage(user.连接id,7,"#Y/您的导标旗次数已经用尽")
                  else
                   return true
                  end
              end
        end
  else
      if user.地图~=1501 and user.地图~=1001 and user.地图~=1092 and user.地图~=1070 and user.地图~=1208 and  user.地图~=1173 and user.地图~=1091 and user.地图~=1110 then
          SendMessage(user.连接id,7,"#Y/导标旗只能在长安城、建邺城、傲来国、长寿村、大唐境外、朱紫国定标")
          return true
      else
        Item.地图编号=user.地图
        Item.地图名称=MapData[user.地图].名称
        Item.x=math.floor(user.角色.地图数据.x/20)
        Item.y=math.floor(user.角色.地图数据.y/20)
        SendMessage(user.连接id,7,"#Y/定标成功")
        SendMessage(user.连接id,3006,"刷新道具")
         return true
      end
   end
end
function ItemControl:传音使用(user,Item,PitchOn,Text)-----------------------完成--
  if Text=="" then
    SendMessage(user.连接id,7,"#Y/你还没填写内容！")
    return true
  end
  Text="["..user.角色.名称.."("..user.id..")".."]#Y/"..Text
      for n, v in pairs(UserData) do
        if UserData[n] ~= nil  then
            SendMessage(UserData[n].连接id, 26, {Text,Item.名称})
        end
      end
 end
function ItemControl:药品使用(user,Item)-----------------------完成--
    if ItemData[Item.名称].等级==3 then
      if Item.名称=="金创药" then
      RoleControl:恢复血魔(user,2,400)
      elseif Item.名称=="千年保心丹" then
      RoleControl:恢复血魔(user,2,Item.品质*4+200)
      RoleControl:恢复血魔(user,4,Item.品质*4+100)
      elseif Item.名称=="金香玉" then
      RoleControl:恢复血魔(user,2,Item.品质*12+150)
      elseif Item.名称=="小还丹" then
      RoleControl:恢复血魔(user,2,Item.品质*8+100)
      RoleControl:恢复血魔(user,4,Item.品质+80)
      elseif Item.名称=="红雪散" then
      RoleControl:恢复血魔(user,2,Item.品质*4)
      SendMessage(user.连接id,7,"#Y/您使用了"..Item.名称)
      elseif Item.名称=="五龙丹" then
      RoleControl:恢复血魔(user,2,Item.品质*3)
      elseif Item.名称=="风水混元丹" then
      RoleControl:恢复血魔(user,3,Item.品质*3+50)
      elseif Item.名称=="蛇蝎美人" then
      RoleControl:恢复血魔(user,3,Item.品质*5+100)
      elseif Item.名称=="定神香" then
      RoleControl:恢复血魔(user,3,Item.品质*5+50)
      elseif Item.名称=="十香返生丸" then
      RoleControl:恢复血魔(user,3,Item.品质*3+50)
      else
      SendMessage(user.连接id,7,"#Y/您无法使用这样的药品")
      return true
      end
    else
      if ItemData[Item.名称].属性.气血 then
      RoleControl:恢复血魔(user,2,ItemData[Item.名称].属性.气血)
      end
      if ItemData[Item.名称].属性.魔法~=nil then
      RoleControl:恢复血魔(user,3,ItemData[Item.名称].属性.魔法)
      end
      if ItemData[Item.名称].属性.伤势~=nil then
      RoleControl:恢复血魔(user,4,ItemData[Item.名称].属性.伤势)
      end
    end
    SendMessage(user.连接id,7,"#Y/您使用了"..Item.名称)
 end
function ItemControl:烹饪使用(user,Item,PitchOn)---------------------- 完成--
  if Item.名称=="包子" then
      RoleControl:恢复血魔(user,2,100)
  elseif Item.名称=="烤鸭" then
      RoleControl:恢复血魔(user,2,Item.品质*10+100)
  elseif Item.名称=="佛跳墙" then
      RoleControl:恢复血魔(user,3,Item.品质*10+100)
  elseif Item.名称=="臭豆腐" then
     RoleControl:恢复血魔(user,2,Item.品质*20+200)
   elseif Item.名称=="烤肉" then
     RoleControl:恢复血魔(user,2,Item.品质*20+200)
  elseif Item.名称=="翡翠豆腐" then
     RoleControl:恢复血魔(user,2,Item.品质*15+150)
     RoleControl:恢复血魔(user,3,Item.品质*10+100)
  elseif Item.名称=="珍露酒" then
     RoleControl:添加愤怒(user,Item.品质*0.4+10)
  elseif Item.名称=="女儿红" or Item.名称=="虎骨酒" then
    RoleControl:添加愤怒(user,20)
  elseif Item.名称=="百味酒" then
    RoleControl:添加愤怒(user,Item.品质*0.5)
  elseif Item.名称=="梅花酒" then
    RoleControl:添加愤怒(user,Item.品质*0.6)
  elseif Item.名称=="蛇胆酒" or Item.名称=="醉生梦死" then
   user.角色.愤怒=math.floor(user.角色.愤怒+Item.品质*1)
  else
      if PitchOn==0 then
          SendMessage(user.连接id, 7, "#y/请你选中一只召唤兽才能使用")
          return true
      elseif user.召唤兽.数据[PitchOn] == nil then
          SendMessage(user.连接id, 7, "#y/这只召唤兽貌似不存在")
          return true
      elseif user.召唤兽.数据[PitchOn].类型=="神兽" then
        SendMessage(user.连接id,7,"#Y/你的这只召唤兽无法增加寿命")
        return true
      end
        local lifetime = 0
       if Item.名称=="桂花丸" then
        lifetime=math.floor(Item.品质*0.5)
       elseif Item.名称=="豆斋果" then
        lifetime=math.floor(Item.品质*3)
       elseif Item.名称=="长寿面" then
        lifetime=math.floor(Item.品质*2+50)
       end
       user.召唤兽.数据[PitchOn].寿命=user.召唤兽.数据[PitchOn].寿命+lifetime
      SendMessage(user.连接id,7,"#Y/你的这只召唤兽寿命增加了"..lifetime.."点")
  end
  SendMessage(user.连接id,7,"#Y/您使用了"..Item.名称)

 end