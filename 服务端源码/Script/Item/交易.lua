--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-01-20 04:46:45
--======================================================================--
function ItemControl:完成交易(id, 交易id)-------------------------------完成--
	if 交易数据[交易id] == nil or 交易数据[交易id].结束标记 ~= nil then
		SendMessage(UserData[id].连接id, 7, "#w/这样的交易并不存在")

		交易数据[交易id] = nil

		return 0
	elseif 交易数据[交易id].确认[1] == false or 交易数据[交易id].确认[2] == false then
		SendMessage(UserData[id].连接id, 7, "#w/双方尚未锁定交易，无法完成交易")

		return 0
	elseif 交易数据[交易id].发起方.id == id then
		交易数据[交易id].确定[1] = true

		if 交易数据[交易id].确定[2] == false then
			SendMessage(UserData[id].连接id, 7, "#y/请等待对方确认交易")

			return 0
		end
	else
		交易数据[交易id].确定[2] = true

		if 交易数据[交易id].确定[1] == false then
			SendMessage(UserData[id].连接id, 7, "#y/请等待对方确认交易")

			return 0
		end
	end
	self.接受id = 交易数据[交易id].接受方.id
	self.发起id = 交易数据[交易id].发起方.id
	self.禁止交易 = false
	self.禁止提示语 = ""

	for n = 1, #交易数据[交易id].发起方.召唤兽 do
			if UserData[self.发起id].召唤兽.数据[交易数据[交易id].发起方.召唤兽[n]].加锁 then
		       self.禁止提示语 = "#y/" .. UserData[self.发起id].角色.名称 .. "的召唤兽携带未解锁，无法交易"
				self.禁止交易 = true
			end
			for i = 1, #UserData[self.发起id].召唤兽.数据[交易数据[交易id].发起方.召唤兽[n]].装备 do
				if UserData[self.发起id].召唤兽.数据[交易数据[交易id].发起方.召唤兽[n]].装备[i] ~= nil then
					self.禁止提示语 = "#y/" .. UserData[self.发起id].角色.名称 .. "的召唤兽携带有装备，无法交易"
					self.禁止交易 = true

				end
			end
	end

	for n = 1, #交易数据[交易id].接受方.召唤兽 do
		if UserData[self.接受id].召唤兽.数据[交易数据[交易id].接受方.召唤兽[n]].加锁 then
		  		self.禁止提示语 = "#y/" .. UserData[self.接受id].角色.名称 .. "的召唤兽未解锁，无法交易"
				self.禁止交易 = true
		end
		for i = 1, #UserData[self.接受id].召唤兽.数据[交易数据[交易id].接受方.召唤兽[n]].装备 do
			if UserData[self.接受id].召唤兽.数据[交易数据[交易id].接受方.召唤兽[n]].装备[i] ~= nil then
				self.禁止提示语 = "#y/" .. UserData[self.接受id].角色.名称 .. "的召唤兽携带有装备，无法交易"
				self.禁止交易 = true
			end
		end
	end
	for n = 1, #交易数据[交易id].发起方.道具 do
		if UserData[self.发起id].角色.道具.包裹[交易数据[交易id].发起方.道具[n]] ~= nil then

			if UserData[self.发起id].物品[UserData[self.发起id].角色.道具.包裹[交易数据[交易id].发起方.道具[n]]].加锁 then
				self.禁止提示语 = "#y/" .. UserData[self.发起id].角色.名称 .. "的道具未解锁，无法交易"
				self.禁止交易 = true
			end
			if UserData[self.发起id].物品[UserData[self.发起id].角色.道具.包裹[交易数据[交易id].发起方.道具[n]]].Time then
				self.禁止提示语 = "#y/" .. UserData[self.发起id].角色.名称 .. "的限时道具，无法交易"
				self.禁止交易 = true
			end
			if UserData[self.发起id].物品[UserData[self.发起id].角色.道具.包裹[交易数据[交易id].发起方.道具[n]]].类型=="剧情道具" then
				self.禁止提示语 = "#y/" .. UserData[self.发起id].角色.名称 .. "剧情道具无法交易"
				self.禁止交易 = true

			elseif UserData[self.发起id].物品[UserData[self.发起id].角色.道具.包裹[交易数据[交易id].发起方.道具[n]]].类型=="古董" then
				self.禁止提示语 = "#y/" .. UserData[self.发起id].角色.名称 .. "古董无法交易"
				self.禁止交易 = true
			end
		end
	end

	for n = 1, #交易数据[交易id].接受方.道具 do
		if UserData[self.接受id].角色.道具.包裹[交易数据[交易id].接受方.道具[n]] ~= nil then
			if UserData[self.接受id].物品[UserData[self.接受id].角色.道具.包裹[交易数据[交易id].接受方.道具[n]]].加锁 then
				self.禁止提示语 = "#y/" .. UserData[self.接受id].角色.名称 .. "的道具未解锁，无法交易"
				self.禁止交易 = true
			end
			if UserData[self.接受id].物品[UserData[self.接受id].角色.道具.包裹[交易数据[交易id].接受方.道具[n]]].Time then
				self.禁止提示语 = "#y/" .. UserData[self.接受id].角色.名称 .. "的限时道具，无法交易"
				self.禁止交易 = true
			end
			if UserData[self.接受id].物品[UserData[self.接受id].角色.道具.包裹[交易数据[交易id].接受方.道具[n]]].类型=="剧情道具"  then
				self.禁止提示语 = "#y/" .. UserData[self.接受id].角色.名称 .. "剧情道具无法交易"
				self.禁止交易 = true
			elseif UserData[self.接受id].物品[UserData[self.接受id].角色.道具.包裹[交易数据[交易id].接受方.道具[n]]].类型=="古董"  then
				self.禁止提示语 = "#y/" .. UserData[self.接受id].角色.名称 .. "古董无法交易"
				self.禁止交易 = true
			end
		end
	end




	if 交易数据[交易id].接受方.银两 == "" or 交易数据[交易id].接受方.银两 == nil then
		交易数据[交易id].接受方.银两 = 0
	end

	if 交易数据[交易id].发起方.银两 == "" or 交易数据[交易id].发起方.银两 == nil then
		交易数据[交易id].发起方.银两 = 0
	end

	if 取角色银子(self.发起id) < 交易数据[交易id].发起方.银两 or 交易数据[交易id].发起方.银两 < 0 then
		self.禁止提示语 = "#y/" .. UserData[self.发起id].角色.名称 .. "没有那么多的银子"
		self.禁止交易 = true
	elseif 取角色银子(self.接受id) < 交易数据[交易id].接受方.银两 or 交易数据[交易id].接受方.银两 < 0 then
		self.禁止提示语 = "#y/" .. UserData[self.接受id].角色.名称 .. "没有那么多的银子"
		self.禁止交易 = true
	end

	if self.禁止交易 == false then
		self.格子总数 = #交易数据[交易id].接受方.道具
		self.格子数量 = RoleControl:取可用格子数量(UserData[交易数据[交易id].发起方.id],"包裹")

		if self.格子数量 < self.格子总数 then
			self.禁止提示语 = "#y/" .. UserData[self.发起id].角色.名称 .. "身上的道具空间不足"
			self.禁止交易 = true
		end

		self.格子总数 = #交易数据[交易id].发起方.道具
		self.格子数量 = RoleControl:取可用格子数量(UserData[交易数据[交易id].接受方.id],"包裹")

		if self.格子数量 < self.格子总数 then
			self.禁止提示语 = "#y/" .. UserData[self.接受id].角色.名称 .. "身上的道具空间不足"
			self.禁止交易 = true
		end
   -- self.格子总数=#交易数据[交易id].接受方.道具
   -- self.格子数量=RoleControl:取可用格子数量(UserData[交易数据[交易id].发起方.id],"包裹")

   --  if self.格子总数>self.格子数量 then
   --  self.禁止提示语="#y/"..UserData[self.发起id].角色.名称.."身上的道具空间不足"
   --  self.禁止交易=true
   --  end
   -- self.格子总数=#交易数据[交易id].发起方.道具
   -- self.格子数量=RoleControl:取可用格子数量(UserData[交易数据[交易id].接受方.id],"包裹")

   --  if self.格子总数>self.格子数量 then
   --  self.禁止提示语="#y/"..UserData[self.接受id].角色.名称.."身上的道具空间不足"
   --  self.禁止交易=true
   --  end


		self.格子总数 = #交易数据[交易id].接受方.召唤兽
		self.格子数量 = #UserData[交易数据[交易id].发起方.id].召唤兽.数据

		if self.格子总数 + self.格子数量 > UserData[self.发起id].召唤兽.数据.召唤兽上限 then
			self.禁止提示语 = "#y/" .. UserData[self.发起id].角色.名称 .. "无法携带更多的召唤兽了"
			self.禁止交易 = true
		end

		self.格子总数 = #交易数据[交易id].发起方.召唤兽
		self.格子数量 = #UserData[交易数据[交易id].接受方.id].召唤兽.数据

		if self.格子总数 + self.格子数量 > UserData[self.接受id].召唤兽.数据.召唤兽上限  then
			self.禁止提示语 = "#y/" .. UserData[self.接受id].角色.名称 .. "无法携带更多的召唤兽了"
			self.禁止交易 = true
		end

		if self.禁止交易 == false then
			交易数据[交易id].结束标记 = true
			self.发起名称 = UserData[self.发起id].角色.名称
			self.接受名称 = UserData[self.接受id].角色.名称

			if 交易数据[交易id].接受方.银两 > 0 then
				 RoleControl:添加银子(UserData[self.发起id],交易数据[交易id].接受方.银两,"交易")
				RoleControl:添加消费日志(UserData[self.发起id],"[交易]获得" .. UserData[self.接受id].账号 .. "的" .. 交易数据[交易id].接受方.银两 .. "两银子")
				RoleControl:扣除银子(UserData[self.接受id],交易数据[交易id].接受方.银两, 12)
				RoleControl:添加消费日志(UserData[self.接受id],"[交易]给予" .. UserData[self.发起id].账号 .. "的" .. 交易数据[交易id].接受方.银两 .. "两银子")
				SendMessage(UserData[self.发起id].连接id, 7, "#y/" .. self.接受名称 .. "给了你" .. 交易数据[交易id].接受方.银两 .. "两银子")
				SendMessage(UserData[self.发起id].连接id, 9, "#xt/#y/" .. self.接受名称 .. "给了你" .. 交易数据[交易id].接受方.银两 .. "两银子")
			end

			if 交易数据[交易id].发起方.银两 > 0 then
				RoleControl:添加银子(UserData[self.接受id],交易数据[交易id].发起方.银两,"交易")
				RoleControl:添加消费日志(UserData[self.接受id],"[交易]获得" .. UserData[self.发起id].账号 .. "的" .. 交易数据[交易id].发起方.银两 .. "两银子")
				RoleControl:扣除银子(UserData[self.发起id],交易数据[交易id].发起方.银两, 12)
				RoleControl:添加消费日志(UserData[self.发起id],"[交易]给予" .. UserData[self.接受id].账号 .. "的" .. 交易数据[交易id].发起方.银两 .. "两银子")
				SendMessage(UserData[self.接受id].连接id, 7, "#y/" .. self.发起名称 .. "给了你" .. 交易数据[交易id].发起方.银两 .. "两银子")
				SendMessage(UserData[self.接受id].连接id, 9, "#xt/#y/" .. self.发起名称 .. "给了你" .. 交易数据[交易id].发起方.银两 .. "两银子")
			end

			for n = 1, #交易数据[交易id].发起方.道具 do
				if UserData[self.发起id].角色.道具.包裹[交易数据[交易id].发起方.道具[n]] ~= nil then
					self.临时格子 = RoleControl:取可用道具格子(UserData[self.接受id],"包裹")
					self.临时道具 = ItemControl:取道具编号(self.接受id)
					self.临时数据 = table.tostring(UserData[self.发起id].物品[UserData[self.发起id].角色.道具.包裹[交易数据[交易id].发起方.道具[n]]])
					UserData[self.接受id].物品[self.临时道具] = table.loadstring(self.临时数据)
					UserData[self.接受id].角色.道具.包裹[self.临时格子] = self.临时道具

					SendMessage(UserData[self.接受id].连接id, 7, "#y/" .. self.发起名称 .. "给了你" .. UserData[self.接受id].物品[self.临时道具].名称)
					SendMessage(UserData[self.接受id].连接id, 9, "#xt/#y/" .. self.发起名称 .. "给了你" .. UserData[self.接受id].物品[self.临时道具].名称)
					RoleControl:添加消费日志(UserData[self.发起id],"[交易]给予" .. UserData[self.接受id].账号 .. "的" .. UserData[self.接受id].物品[self.临时道具].名称)
					RoleControl:添加消费日志(UserData[self.接受id],"[交易]获得" .. UserData[self.发起id].账号 .. "的" .. UserData[self.接受id].物品[self.临时道具].名称)

					UserData[self.发起id].物品[UserData[self.发起id].角色.道具.包裹[交易数据[交易id].发起方.道具[n]]] = nil
					UserData[self.发起id].角色.道具.包裹[交易数据[交易id].发起方.道具[n]] = nil
				end
			end

			for n = 1, #交易数据[交易id].接受方.道具 do
				if UserData[self.接受id].角色.道具.包裹[交易数据[交易id].接受方.道具[n]] ~= nil then
					self.临时格子 = RoleControl:取可用道具格子(UserData[self.发起id],"包裹")
					self.临时道具 = ItemControl:取道具编号(self.发起id)
					self.临时数据 = table.tostring(UserData[self.接受id].物品[UserData[self.接受id].角色.道具.包裹[交易数据[交易id].接受方.道具[n]]])
					UserData[self.发起id].物品[self.临时道具] = table.loadstring(self.临时数据)
					UserData[self.发起id].角色.道具.包裹[self.临时格子] = self.临时道具

					SendMessage(UserData[self.发起id].连接id, 7, "#y/" .. self.接受名称 .. "给了你" .. UserData[self.发起id].物品[self.临时道具].名称)
					SendMessage(UserData[self.发起id].连接id, 9, "#xt/#y/" .. self.接受名称 .. "给了你" .. UserData[self.发起id].物品[self.临时道具].名称)
					RoleControl:添加消费日志(UserData[self.接受id],"[交易]给予" .. UserData[self.发起id].账号 .. "的" .. UserData[self.发起id].物品[self.临时道具].名称)
					RoleControl:添加消费日志(UserData[self.发起id],"[交易]获得" .. UserData[self.接受id].账号 .. "的" .. UserData[self.发起id].物品[self.临时道具].名称)

					UserData[self.接受id].物品[UserData[self.接受id].角色.道具.包裹[交易数据[交易id].接受方.道具[n]]] = nil
					UserData[self.接受id].角色.道具.包裹[交易数据[交易id].接受方.道具[n]] = nil
				end
			end

			SendMessage(UserData[self.发起id].连接id, 3006, "1")
			SendMessage(UserData[self.接受id].连接id, 3006, "1")

			self.发起方召唤 = false
			self.接受方召唤 = false
			  table.sort(交易数据[交易id].接受方.召唤兽,function(a, b) return a > b end)
			  table.sort(交易数据[交易id].发起方.召唤兽,function(a, b) return a > b end)
			for n = 1, #交易数据[交易id].发起方.召唤兽 do
				self.临时数据 = table.tostring(UserData[self.发起id].召唤兽.数据[交易数据[交易id].发起方.召唤兽[n]])
				

				table.remove(UserData[self.发起id].召唤兽.数据,交易数据[交易id].发起方.召唤兽[n])
				UserData[self.接受id].召唤兽.数据[#UserData[self.接受id].召唤兽.数据 + 1] = table.loadstring(self.临时数据)
				
				SendMessage(UserData[self.接受id].连接id, 7, "#y/" .. self.发起名称 .. "给了你" .. UserData[self.接受id].召唤兽.数据[#UserData[self.接受id].召唤兽.数据].名称)
				SendMessage(UserData[self.接受id].连接id, 9, "#xt/#y/" .. self.发起名称 .. "给了你" .. UserData[self.接受id].召唤兽.数据[#UserData[self.接受id].召唤兽.数据].名称)
				RoleControl:添加消费日志(UserData[self.发起id],"[交易]给予" .. UserData[self.接受id].账号 .. "的" .. UserData[self.接受id].召唤兽.数据[#UserData[self.接受id].召唤兽.数据].名称)
				RoleControl:添加消费日志(UserData[self.接受id],"[交易]获得" .. UserData[self.发起id].账号 .. "的" .. UserData[self.接受id].召唤兽.数据[#UserData[self.接受id].召唤兽.数据].名称)

				self.发起方召唤 = true
			end
	
			for n = 1, #交易数据[交易id].接受方.召唤兽 do
				self.临时数据 = table.tostring(UserData[self.接受id].召唤兽.数据[交易数据[交易id].接受方.召唤兽[n]])
				
				table.remove(UserData[self.接受id].召唤兽.数据,交易数据[交易id].接受方.召唤兽[n])
				UserData[self.发起id].召唤兽.数据[#UserData[self.发起id].召唤兽.数据 + 1] = table.loadstring(self.临时数据)
				
				SendMessage(UserData[self.发起id].连接id, 7, "#y/" .. self.接受名称 .. "给了你" .. UserData[self.发起id].召唤兽.数据[#UserData[self.发起id].召唤兽.数据].名称)
				SendMessage(UserData[self.发起id].连接id, 9, "#xt/#y/" .. self.接受名称 .. "给了你" .. UserData[self.发起id].召唤兽.数据[#UserData[self.发起id].召唤兽.数据].名称)
				RoleControl:添加消费日志(UserData[self.接受id],"[交易]给予" .. UserData[self.发起id].账号 .. "的" .. UserData[self.发起id].召唤兽.数据[#UserData[self.发起id].召唤兽.数据].名称)
				RoleControl:添加消费日志(UserData[self.发起id],"[交易]获得" .. UserData[self.接受id].账号 .. "的" .. UserData[self.发起id].召唤兽.数据[#UserData[self.发起id].召唤兽.数据].名称)

				self.接受方召唤 = true
			end

			if self.发起方召唤 then
				self.临时bb = {}
				self.参战标记 = 0

				for n = 1, #UserData[self.发起id].召唤兽.数据 do
					if UserData[self.发起id].召唤兽.数据[n] ~= nil and UserData[self.发起id].召唤兽.数据[n] ~= "1" and UserData[self.发起id].召唤兽.数据[n].造型 ~= nil then
						if UserData[self.发起id].召唤兽.数据.参战 == n then
							UserData[self.发起id].召唤兽.数据[n].参战标记 = true
						end

						self.临时bb[#self.临时bb + 1] = table.tostring(UserData[self.发起id].召唤兽.数据[n])
					end
				end

				UserData[self.发起id].召唤兽.数据.参战 = 0
				UserData[self.发起id].召唤兽.数据.观看 = 0
				for n = 1, #self.临时bb do
					UserData[self.发起id].召唤兽.数据[n] = table.loadstring(self.临时bb[n])

					-- if UserData[self.发起id].召唤兽.数据[n].参战标记 then
					-- 	UserData[self.发起id].召唤兽.数据.参战 = n
					-- end
				end
				SendMessage(UserData[self.发起id].连接id, 2005, UserData[self.发起id].召唤兽:取头像数据())
			end

			if self.接受方召唤 then
				self.临时bb = {}
				self.参战标记 = 0

				for n = 1, #UserData[self.接受id].召唤兽.数据 do
					if UserData[self.接受id].召唤兽.数据[n] ~= nil and UserData[self.接受id].召唤兽.数据[n] ~= 1 and UserData[self.接受id].召唤兽.数据[n].造型 ~= nil then
						if UserData[self.接受id].召唤兽.数据.参战 == n then
							UserData[self.接受id].召唤兽.数据[n].参战标记 = true
						end

						self.临时bb[#self.临时bb + 1] = table.tostring(UserData[self.接受id].召唤兽.数据[n])
					end
				end

				UserData[self.接受id].召唤兽.数据.参战 = 0
				UserData[self.接受id].召唤兽.数据.观看 = 0

				for n = 1, #self.临时bb do
					UserData[self.接受id].召唤兽.数据[n] = table.loadstring(self.临时bb[n])

					-- if UserData[self.接受id].召唤兽.数据[n].参战标记 then
					-- 	UserData[self.接受id].召唤兽.数据.参战 = n
					-- end
				end
				SendMessage(UserData[self.接受id].连接id, 2005, UserData[self.接受id].召唤兽:取头像数据())
			end
		end
	end

	if self.禁止交易 then
		SendMessage(UserData[self.发起id].连接id, 7, self.禁止提示语)
		SendMessage(UserData[self.接受id].连接id, 7, self.禁止提示语)
	end

	SendMessage(UserData[self.发起id].连接id, 3018, "1")
	SendMessage(UserData[self.接受id].连接id, 3018, "1")

	交易数据[交易id] = nil
	UserData[self.发起id].交易 = 0
	UserData[self.接受id].交易 = 0
 end
function ItemControl:锁定交易(id, id1, 银子, 数据)----------------------完成--
	self.临时数据 = 分割文本(数据, "@-@")
	local 数据 = {
		id = id1,
		银两 = 银子,
		道具 = {},
		召唤兽 = {}
	}
	self.临时数据1 = 分割文本(self.临时数据[1], "*-*")

	for n = 1, 3 do
		数据.道具[n] = self.临时数据1[n] + 0
	end

	self.临时数据1 = 分割文本(self.临时数据[2], "*-*")
   

	for n = 1, 3 do
		数据.召唤兽[n] = self.临时数据1[n] + 0
	end
   
	if 交易数据[数据.id] == nil   then
		SendMessage(UserData[id].连接id, 7, "#y/这样的交易并不存在")
		SendMessage(UserData[id].连接id, 3016, "66")

		return 0
	elseif    交易数据[数据.id].发起方 ==nil  then
				SendMessage(UserData[id].连接id, 7, "#y/交易数据错误，请重新交易")
			SendMessage(UserData[id].连接id, 3016, "66")

		return 0
	else
		if 交易数据[数据.id].发起方.id == id then
			if 交易数据[数据.id].确认[1] then
				SendMessage(UserData[id].连接id, 7, "#y/你已经锁定了交易，无法进行二次锁定")

				return 0
			end

			self.交易id = "发起方"
			self.接受id = "接受方"
			交易数据[数据.id].确认[1] = true
		else
			if 交易数据[数据.id].确认[2] then
				SendMessage(UserData[id].连接id, 7, "#y/你已经锁定了交易，无法进行二次锁定")

				return 0
			end

			self.交易id = "接受方"
			self.接受id = "发起方"
			交易数据[数据.id].确认[2] = true
		end

		self.发送信息 = {
			道具 = {},
			召唤兽 = {},
			银两 = 数据.银两
		}

		for n = 1, 3 do
			if 数据.道具[n] ~= 0 then
				交易数据[数据.id][self.交易id].道具[#交易数据[数据.id][self.交易id].道具 + 1] = 数据.道具[n]
				self.发送信息.道具[#self.发送信息.道具 + 1] = UserData[id].物品[UserData[id].角色.道具.包裹[数据.道具[n]]]
			end
		end

		for n = 1, 3 do
			if 数据.召唤兽[n] ~= 0 then
				交易数据[数据.id][self.交易id].召唤兽[#交易数据[数据.id][self.交易id].召唤兽 + 1] = 数据.召唤兽[n]
				self.发送信息.召唤兽[#self.发送信息.召唤兽 + 1] = UserData[id].召唤兽.数据[数据.召唤兽[n]]
			end
		end

		交易数据[数据.id][self.交易id].银两 = 数据.银两

		SendMessage(UserData[交易数据[数据.id][self.接受id].id].连接id, 3017, self.发送信息)
		SendMessage(UserData[id].连接id, 7, "#y/你锁定了交易状态，你将无法修改交易中的东西")
		SendMessage(UserData[交易数据[数据.id][self.接受id].id].连接id, 7, "#y/" .. UserData[id].角色.名称 .. "锁定了交易状态")
	end
 end
function ItemControl:交易条件判断(id, 内容)-----------------------------完成--
	self.找到id = 内容 + 0

	if UserData[self.找到id] == nil then
		SendMessage(UserData[id].连接id, 7, "#y/这个玩家并不存在")

		return 0
	elseif UserData[self.找到id].交易 ~= 0 then
		SendMessage(UserData[id].连接id, 7, "#y/对方正在与其他人交易中……")

		return 0
	elseif UserData[self.找到id].屏蔽交易  then
		SendMessage(UserData[id].连接id, 7, "#y/对方禁止交易……")

		return 0
	elseif UserData[id].交易 ~= 0 then
		SendMessage(UserData[id].连接id, 7, "#y/你的上一次交易还没有完成")

		return 0
	elseif UserData[id].角色.等级 < 50 then
		SendMessage(UserData[id].连接id, 7, "#y/等级达到50级后才可使用交易功能")

		return 0
	elseif UserData[self.找到id].角色.等级 < 50 then
		SendMessage(UserData[id].连接id, 7, "#y/对方等级未达到50级无法进行交易")

		return 0
	elseif UserData[id].认证 == "0" or UserData[self.找到id].认证 == "0" then
		SendMessage(UserData[id].连接id, 7, "#y/认证模式下无法使用交易功能")

		return 0
	else
		if UserData[id].角色.道具.货币.银子 < 0 or UserData[self.找到id].角色.道具.货币.银子 < 0 then
			SendMessage(UserData[id].连接id, 7, "#y/错误的交易数据")

			return 0
		end

		交易数据[UserData[id].id] = {
			发起方 = {
				银两 = 0,
				id = id,
				道具 = {},
				召唤兽 = {}
			},
			接受方 = {
				银两 = 0,
				id = self.找到id,
				道具 = {},
				召唤兽 = {}
			},
			确认 = {
				false,
				false
			},
			确定 = {
				false,
				false
			}
		}
		UserData[self.找到id].交易 = UserData[id].角色.id
		UserData[id].交易 = UserData[id].角色.id
		self.发送信息 = {
			id = UserData[id].id,
			道具 = self:索要玩家道具(id, "包裹"),
			名称 = UserData[self.找到id].角色.名称,
			等级 = UserData[self.找到id].角色.等级
		}

		SendMessage(UserData[id].连接id, 3013, self.发送信息)

		self.发送信息 = {
			id = UserData[id].角色.id,
			道具 = ItemControl:索要玩家道具(self.找到id, "包裹"),
			名称 = UserData[id].角色.名称,
			等级 = UserData[id].角色.等级
		}

		SendMessage(UserData[self.找到id].连接id, 3013, self.发送信息)
		SendMessage(UserData[id].连接id, 7, "#w/你正在与#y/[" .. UserData[self.找到id].角色.名称 .. "]#w/交易")
		SendMessage(UserData[self.找到id].连接id, 7, "#w/你正在与#y/[" .. UserData[id].角色.名称 .. "]#w/交易")
	end
 end