-- @Author: 作者QQ381990860
-- @Date:   2021-09-07 00:54:26
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-09-07 10:36:07
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-12-15 16:50:38
--======================================================================--
function ItemControl:脱下装备(id, 格子, 类型,交换格子)------------------完成--
  local 内容 = {
    格子 = tonumber(格子),
    当前类型 =类型,
    交换格子=tonumber(交换格子),
   }

  if 内容.格子>=21 then --卸下装备
      if 内容.交换格子~= nil then
      self.临时格子 = 内容.交换格子
      else
      self.临时格子=RoleControl:取可用道具格子(UserData[id],内容.当前类型)
      end
      if self.临时格子==0 then
      SendMessage(UserData[id].连接id,7,"#Y/您的"..内容.当前类型.."已经无法存储更多的道具了")
      SendMessage(UserData[id].连接id,3006,"66")
      return 0
      else
      UserData[id].角色.道具[内容.当前类型][self.临时格子]=UserData[id].角色.装备数据[内容.格子]
      UserData[id].角色.装备数据[内容.格子]=nil
                   if  UserData[id].物品[ UserData[id].角色.道具[内容.当前类型][self.临时格子]].Time and UserData[id].物品[ UserData[id].角色.道具[内容.当前类型][self.临时格子]].Time <= os.time() then 
                    UserData[id].物品[ UserData[id].角色.道具[内容.当前类型][self.临时格子]] =nil
                  UserData[id].角色.道具.包裹[ UserData[id].角色.道具[内容.当前类型][self.临时格子]] =nil
                  SendMessage(UserData[id].连接id, 7, "#y/你的道具已经过期被系统回收")   
                end
      RoleControl:刷新装备属性(UserData[id])

      SendMessage(UserData[id].连接id,3006,"66")
          if 内容.格子==23  then
          self.发送信息=UserData[id].角色.武器数据
          self.发送信息.id=id
            SendMessage(UserData[id].连接id, 2010, RoleControl:获取地图数据(UserData[id]))
           MapControl:MapSend(id, 1016, RoleControl:获取地图数据(UserData[id]))

          end
      end
  end
end
function ItemControl:佩戴装备(id, 格子, 类型,交换格子)------------------完成--
	local 内容 = {
		格子 = tonumber(格子),
		当前类型 =类型,
		交换格子=tonumber(交换格子),
	 }


    self.道具编号=UserData[id].角色.道具[内容.当前类型][内容.格子]
    if UserData[id].物品[self.道具编号] == nil then
        SendMessage(UserData[id].连接id,7,"#Y/此道具不存在请刷新背包")
       return
    end
    self.道具类型=UserData[id].物品[self.道具编号].类型
    self.道具等级=UserData[id].物品[self.道具编号].等级
    self.道具耐久=UserData[id].物品[self.道具编号].耐久度
    self.道具特效=UserData[id].物品[self.道具编号].特效
    self.名称=UserData[id].物品[self.道具编号].名称
    self.格子序号=0
        if self.道具类型=="武器" then
        self.格子序号=23
        elseif self.道具类型=="头盔" then
        self.格子序号=21
        elseif self.道具类型=="腰带" then
        self.格子序号=25
        elseif self.道具类型=="项链" then
        self.格子序号=22
        elseif self.道具类型=="鞋子" then
        self.格子序号=26
        elseif self.道具类型=="衣服" then
        self.格子序号=24
        end

          if self.格子序号==0 then
          SendMessage(UserData[id].连接id,7,"#Y/此道具无法佩戴")
          SendMessage(UserData[id].连接id,3006,"66")
          return 0
          elseif  self.道具耐久<=0 then
          SendMessage(UserData[id].连接id,7,"#Y/此道具的耐久度为0，已经无法佩戴了")
          SendMessage(UserData[id].连接id,3006,"66")
          return 0
           elseif  UserData[id].物品[self.道具编号].鉴定~=true then
          SendMessage(UserData[id].连接id,7,"#Y/此道具还未鉴定无法穿戴")
          SendMessage(UserData[id].连接id,3006,"66")
          return 0
         elseif UserData[id].物品[self.道具编号].专用~=nil and UserData[id].物品[self.道具编号].专用~=id then
            SendMessage(UserData[id].连接id,7,"#Y/你无法佩戴他人的专用装备")
            SendMessage(UserData[id].连接id,3006,"66")
              return 0
          elseif  UserData[id].物品[self.道具编号].Time and UserData[id].物品[self.道具编号].Time <= os.time() then 
                    UserData[id].物品[self.道具编号] =nil
                  UserData[id].角色.道具.包裹[内容.格子] =nil
                  SendMessage(UserData[id].连接id, 7, "#y/你的道具已经过期被系统回收")
                  SendMessage(UserData[id].连接id, 3007,{格子=内容.格子,数据=UserData[id].物品[UserData[id].角色.道具.包裹[内容.格子]]})
          else
              if self:取等级要求(id,self.道具等级,self.道具特效)==false then
              SendMessage(UserData[id].连接id,7,"#Y/你当前的等级无法佩戴这样的装备")
              SendMessage(UserData[id].连接id,3006,"66")
              return 0
              else
              	 local lssx =取属性(UserData[id].角色.造型)
                    if self.道具类型=="武器" then
                    self.角色=取武器类型(self.名称)

                    elseif self.道具类型=="衣服" or self.道具类型=="头盔" then
                    self.角色=取防具性别(self.名称)
                    else
                    self.角色 ="无"
                   end

                    if self.道具类型=="武器" and (self.角色~=lssx.武器[1]) and (self.角色~=lssx.武器[2])  then

                    SendMessage(UserData[id].连接id,7,"#Y/你的角色无法佩戴此种武器")
                    SendMessage(UserData[id].连接id,3006,"66")
                    return 0
                    elseif self.道具类型~="武器" and self.角色~="无" and  string.find(self.角色,UserData[id].角色.性别)==nil then
                    SendMessage(UserData[id].连接id,7,"#Y/你的角色不适合佩戴此种装备")
                    SendMessage(UserData[id].连接id,3006,"66")
                    return 0
                    else
                        self.临时装备=UserData[id].角色.装备数据[self.格子序号]
                        UserData[id].角色.装备数据[self.格子序号]=self.道具编号
                        UserData[id].角色.道具[内容.当前类型][内容.格子]=self.临时装备
                        RoleControl:刷新装备属性(UserData[id])
                        if self.格子序号==23 then 
                                SendMessage(UserData[id].连接id, 2010, RoleControl:获取地图数据(UserData[id]))
                               MapControl:MapSend(id, 1016, RoleControl:获取地图数据(UserData[id]))
                        end
                  
                      SendMessage(UserData[id].连接id,3006,"66")

                    end
              end
          end
 end
function ItemControl:灵饰处理(id,道具id,等级,强化,类型)-----------------完成--
    local 灵饰属性 ={}
    灵饰属性.手镯={主属性={"封印命中等级","抵抗封印等级"},副属性={"气血回复效果","气血","防御","抗法术暴击等级","格挡值","法术防御","抗物理暴击等级"}}
    灵饰属性.佩饰={主属性={"速度"},副属性={"气血回复效果","气血","防御","抗法术暴击等级","格挡值","法术防御","抗物理暴击等级"}}
    灵饰属性.戒指={主属性={"伤害","防御"},副属性={"固定伤害","法术伤害","伤害","封印命中等级","法术暴击等级","物理暴击等级","狂暴等级","穿刺等级","法术伤害结果","治疗能力","速度"}}
    灵饰属性.耳饰={主属性={"法术伤害","法术防御"},副属性={"固定伤害","法术伤害","伤害","封印命中等级","法术暴击等级","物理暴击等级","狂暴等级","穿刺等级","法术伤害结果","治疗能力","速度"}}
    灵饰属性.基础={
    封印命中等级={[160]={a=42,b=64},[150]={a=28,b=50},[140]={a=28,b=42},[120]={a=24,b=36},[100]={a=20,b=30},[80]={a=16,b=24},[60]={a=12,b=18},[0]=4}
    ,抵抗封印等级={[160]={a=42,b=64},[150]={a=28,b=50},[140]={a=28,b=42},[120]={a=24,b=36},[100]={a=20,b=30},[80]={a=16,b=24},[60]={a=12,b=18},[0]=4}
    ,防御={[160]={a=42,b=64},[150]={a=28,b=50},[140]={a=28,b=42},[120]={a=24,b=36},[100]={a=20,b=30},[80]={a=16,b=24},[60]={a=12,b=18},[0]=8}
    ,气血回复效果={[160]={a=21,b=40},[150]={a=15,b=30},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=4}
    ,气血={[160]={a=142,b=204},[150]={a=128,b=160},[140]={a=98,b=147},[120]={a=84,b=126},[100]={a=70,b=105},[80]={a=56,b=84},[60]={a=42,b=63},[0]=28}
    ,抗法术暴击等级={[160]={a=42,b=64},[150]={a=28,b=50},[140]={a=28,b=42},[120]={a=24,b=36},[100]={a=20,b=30},[80]={a=16,b=24},[60]={a=12,b=18},[0]=8}
    ,格挡值={[160]={a=42,b=64},[150]={a=28,b=50},[140]={a=28,b=42},[120]={a=24,b=36},[100]={a=20,b=30},[80]={a=16,b=24},[60]={a=12,b=18},[0]=8}
    ,法术防御={[160]={a=42,b=64},[150]={a=28,b=50},[140]={a=28,b=42},[120]={a=24,b=36},[100]={a=20,b=30},[80]={a=16,b=24},[60]={a=12,b=18},[0]=8}
    ,抗物理暴击等级={[160]={a=42,b=64},[150]={a=28,b=50},[140]={a=28,b=42},[120]={a=24,b=36},[100]={a=20,b=30},[80]={a=16,b=24},[60]={a=12,b=18},[0]=8}
    ,速度={[160]={a=30,b=60},[150]={a=28,b=50},[140]={a=20,b=30},[120]={a=18,b=27},[100]={a=16,b=24},[80]={a=14,b=21},[60]={a=12,b=18},[0]=3}
    ,法术伤害={[160]={a=42,b=64},[150]={a=28,b=50},[140]={a=28,b=42},[120]={a=24,b=36},[100]={a=20,b=30},[80]={a=16,b=24},[60]={a=12,b=18},[0]=4}
    ,伤害={[160]={a=22,b=44},[150]={a=28,b=32},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=4}
    ,固定伤害={[160]={a=22,b=34},[150]={a=18,b=23},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=4}
    ,法术伤害={[160]={a=22,b=44},[150]={a=21,b=30},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=4}
    ,法术暴击等级={[160]={a=22,b=44},[150]={a=18,b=23},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=4}
    ,物理暴击等级={[160]={a=22,b=44},[150]={a=18,b=23},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=4}
    ,狂暴等级={[160]={a=22,b=44},[150]={a=18,b=23},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=3}
    ,穿刺等级={[160]={a=22,b=44},[150]={a=18,b=23},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=4}
    ,法术伤害结果={[160]={a=22,b=44},[150]={a=18,b=23},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=3}
    ,治疗能力={[160]={a=22,b=44},[150]={a=18,b=23},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=3}
    }
   self.幻化id=道具id
   UserData[id].物品[self.幻化id]={幻化等级=0}
   UserData[id].物品[self.幻化id].幻化属性={附加={}}
   self.临时属性=灵饰属性[类型].主属性[math.random(1,#灵饰属性[类型].主属性)]
   self.临时数值=灵饰属性.基础[self.临时属性][等级].b
   self.临时下限=灵饰属性.基础[self.临时属性][等级].a
   self.临时数值=math.random(self.临时下限,self.临时数值)
   if 强化==1 then
     self.临时数值=math.floor(self.临时数值*1.1)
   end
   UserData[id].物品[self.幻化id].幻化属性.基础={类型=self.临时属性,数值=self.临时数值,强化=0}
   if math.random(100) <= 5 then
    UserData[id].物品[self.幻化id].特效={[1]="超级简易"}
   end
   for n=1,math.random(6) do
     self.临时属性=灵饰属性[类型].副属性[math.random(1,#灵饰属性[类型].副属性)]
     self.临时数值=灵饰属性.基础[self.临时属性][等级].b
     self.临时下限=灵饰属性.基础[self.临时属性][等级].a
     self.临时数值=math.random(self.临时下限,self.临时数值)
      for i=1,#UserData[id].物品[self.幻化id].幻化属性.附加 do
        if UserData[id].物品[self.幻化id].幻化属性.附加[i].类型==self.临时属性 then
         self.临时数值=UserData[id].物品[self.幻化id].幻化属性.附加[i].数值
        end
      end
     UserData[id].物品[self.幻化id].幻化属性.附加[n]={类型=self.临时属性,数值=self.临时数值,强化=0}
   end
 end

function ItemControl:脱下灵饰(id, 格子, 类型,交换格子)------------------完成--
  数据 = {
    格子 = tonumber(格子),
    当前类型 = 类型,
    交换格子=tonumber(交换格子)
  }


      if 数据.交换格子~= nil then
            self.临时格子 = 数据.交换格子
      else
            self.临时格子=RoleControl:取可用道具格子(UserData[id],数据.当前类型)
      end
      if self.临时格子==0 then
        SendMessage(UserData[id].连接id,7,"#Y/您的"..数据.当前类型.."已经无法存储更多的道具了")
        SendMessage(UserData[id].连接id,3006,"66")
        return 0
      else



       UserData[id].角色.道具[数据.当前类型][self.临时格子]=UserData[id].角色.灵饰[数据.格子]
       if UserData[id].物品[UserData[id].角色.灵饰[数据.格子]].特性 then
           UserData[id].物品[UserData[id].角色.灵饰[数据.格子]].特性.套装 =nil
       end
       UserData[id].角色.灵饰[数据.格子]=0

                if  UserData[id].物品[ UserData[id].角色.道具[数据.当前类型][self.临时格子]].Time and UserData[id].物品[ UserData[id].角色.道具[数据.当前类型][self.临时格子]].Time <= os.time() then 
                    UserData[id].物品[ UserData[id].角色.道具[数据.当前类型][self.临时格子]] =nil
                  UserData[id].角色.道具.包裹[ UserData[id].角色.道具[数据.当前类型][self.临时格子]] =nil
                  SendMessage(UserData[id].连接id, 7, "#y/你的道具已经过期被系统回收")   
                end
        RoleControl:刷新装备属性(UserData[id])

        self:索要灵饰(id)
         if   数据.格子==35 then
            SendMessage(UserData[id].连接id, 2010, RoleControl:获取地图数据(UserData[id]))

           MapControl:MapSend(id, 1016, RoleControl:获取地图数据(UserData[id]))
         end  
        SendMessage(UserData[id].连接id,3006,"66")
        return 0
      end

end
function ItemControl:佩戴灵饰(id, 格子, 类型,交换格子)------------------完成--
	数据 = {
		格子 = tonumber(格子),
		当前类型 = 类型,
		交换格子=tonumber(交换格子)
	}



      self.道具编号=UserData[id].角色.道具[数据.当前类型][数据.格子]
      if UserData[id].物品[self.道具编号] == nil then
          SendMessage(UserData[id].连接id,7,"#Y/此道具不存在请刷新背包")
         return
      end

      self.道具类型=UserData[id].物品[self.道具编号].类型
      if self.道具类型=="定制锦衣" then
        self.道具等级=0
      self.道具耐久=1
      self.道具特效={}
      else
      self.道具等级=UserData[id].物品[self.道具编号].等级
      self.道具耐久=UserData[id].物品[self.道具编号].耐久
      self.道具特效=UserData[id].物品[self.道具编号].特效
      end
      self.名称=UserData[id].物品[self.道具编号].名称
      self.格子序号=0
      if self.道具类型=="耳饰" then
      self.格子序号=31
      elseif self.道具类型=="佩饰" then
      self.格子序号=32
      elseif self.道具类型=="戒指" then
      self.格子序号=33
      elseif self.道具类型=="手镯" then
      self.格子序号=34
     elseif self.道具类型=="定制锦衣" then
      self.格子序号=35
      end
      if self.格子序号==0 then
          SendMessage(UserData[id].连接id,7,"#Y/此道具无法佩戴")
          SendMessage(UserData[id].连接id,3006,"66")
          return 0
      elseif  self.道具耐久<=0 then
          SendMessage(UserData[id].连接id,7,"#Y/此道具的耐久度为0，已经无法佩戴了")
          SendMessage(UserData[id].连接id,3006,"66")
          return 0
      elseif   not UserData[id].物品[self.道具编号].鉴定 and self.道具类型~="定制锦衣" then
          SendMessage(UserData[id].连接id,7,"#Y/此灵饰还未鉴定无法穿戴")
          SendMessage(UserData[id].连接id,3006,"66")
      elseif   self.道具类型=="定制锦衣" and  UserData[id].角色.造型~=UserData[id].物品[self.道具编号].造型 then
              SendMessage(UserData[id].连接id,7,"#Y/当前角色无法佩戴此定制")
              return 0
        elseif UserData[id].物品[self.道具编号].专用~=nil and UserData[id].物品[self.道具编号].专用~=id then
            SendMessage(UserData[id].连接id,7,"#Y/你无法佩戴他人的专用装备")
            SendMessage(UserData[id].连接id,3006,"66")
              return 0
       elseif  UserData[id].物品[self.道具编号].Time and UserData[id].物品[self.道具编号].Time <= os.time() then 
                    UserData[id].物品[self.道具编号] =nil
                  UserData[id].角色.道具.包裹[数据.格子] =nil
                  SendMessage(UserData[id].连接id, 7, "#y/你的道具已经过期被系统回收")
                  SendMessage(UserData[id].连接id, 3007,{格子=数据.格子,数据=UserData[id].物品[UserData[id].角色.道具.包裹[数据.格子]]})
      else
          if self:取等级要求(id,self.道具等级,self.道具特效)==false then
          SendMessage(UserData[id].连接id,7,"#Y/您的等级不足以佩戴这种灵饰")
          SendMessage(UserData[id].连接id,3006,"66")
          return 0
          else

                  if UserData[id].角色.灵饰[ self.格子序号]~=0 then
                     UserData[id].角色.道具[数据.当前类型][数据.格子]=UserData[id].角色.灵饰[ self.格子序号]
                  else
                       UserData[id].角色.道具[数据.当前类型][数据.格子]=nil
                  end
                  UserData[id].角色.灵饰[ self.格子序号]=self.道具编号
                    RoleControl:刷新装备属性(UserData[id])
                 
                     self:索要灵饰(id)
                     if  self.格子序号==35 then
                        SendMessage(UserData[id].连接id, 2010, RoleControl:获取地图数据(UserData[id]))
  
                       MapControl:MapSend(id, 1016, RoleControl:获取地图数据(UserData[id]))
                     end  
                    SendMessage(UserData[id].连接id,3006,"66")
          end
      end
 end
function ItemControl:索要灵饰(id)---------------------------------------完成--
 self.发送信息={}
 for n=31,35 do
   if UserData[id].角色.灵饰[n]~=0 then
      self.发送信息[n]=table.loadstring(table.tostring(UserData[id].物品[UserData[id].角色.灵饰[n]]))
    else
      self.发送信息[n]=0
     end
   end
  SendMessage(UserData[id].连接id,3021,self.发送信息)
 end