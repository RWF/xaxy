--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-05-04 19:14:42
--======================================================================--
function ItemControl:小窗口技能处理(id, 格子,类型,技能格子)
	local 临时id = UserData[id].角色.道具["包裹"][格子]
	if UserData[id].物品[临时id] == nil or UserData[id].物品[临时id] == 0 then
		SendMessage(UserData[id].连接id, 7, "#Y/你似乎并没有这样的道具")
	elseif 类型 == "兵器谱" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "武器" and  道具类型 ~= "女衣" and  道具类型 ~= "男衣" and  道具类型 ~= "衣服" then
			SendMessage(UserData[id].连接id, 7, "#y/只有武器和衣服才可以使用鉴定功能")
		elseif 技能名称 ~= "兵器谱"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif 技能等级 < 道具等级 then
            SendMessage(UserData[id].连接id, 7, "#y/技能等级不够,无法鉴定这个装备")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次鉴定需要消耗" .. 技能等级 .. "点活力")
		elseif UserData[id].物品[临时id].鉴定 then
			SendMessage(UserData[id].连接id, 7, "#y/你的这件装备已经鉴定过了")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
			UserData[id].物品[临时id].鉴定 = true

			if UserData[id].物品[临时id].特效 and UserData[id].物品[临时id].等级 >80 then
                for i=1,#UserData[id].物品[临时id].特效 do
                    if UserData[id].物品[临时id].特效[i] =="无级别限制" then
                        SendMessage(UserData[id].连接id, 2072, {标题="牛了！无级别啊",文本="鉴定至少80级装备出现特效无级别"})
                         --RoleControl:添加成就积分(UserData[id],1)
                    end
                end

            end

            if UserData[id].物品[临时id].特技  then
                SendMessage(UserData[id].连接id, 2072, {标题="我的"..UserData[id].物品[临时id].特技,文本="鉴定至少80级装备出现特技"..UserData[id].物品[临时id].特技})
                 --RoleControl:添加成就积分(UserData[id],1)
            end
			SendMessage(UserData[id].连接id, 7, "#y/鉴定成功,你消耗了" .. 技能等级 .. "点活力" )
			SendMessage(UserData[id].连接id,3006,"66")
		end
	elseif 类型 == "堪察令" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "鞋子" and  道具类型 ~= "腰带" and  道具类型 ~= "项链" and  道具类型 ~= "头盔" then
			SendMessage(UserData[id].连接id, 7, "#y/只有装备和饰品才可以使用鉴定功能")
		elseif 技能名称 ~= "堪察令"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif 技能等级 < 道具等级 then
            SendMessage(UserData[id].连接id, 7, "#y/技能等级不够,无法鉴定这个装备")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次鉴定需要消耗" .. 技能等级 .. "点活力")
		elseif UserData[id].物品[临时id].鉴定 then
			SendMessage(UserData[id].连接id, 7, "#y/你的这件装备已经鉴定过了")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
			UserData[id].物品[临时id].鉴定 = true
			if UserData[id].物品[临时id].特效 and UserData[id].物品[临时id].等级 >80 then
                for i=1,#UserData[id].物品[临时id].特效 do
                    if UserData[id].物品[临时id].特效[i] =="无级别限制" then
                        SendMessage(UserData[id].连接id, 2072, {标题="牛了！无级别啊",文本="鉴定至少80级装备出现特效无级别"})
                         --RoleControl:添加成就积分(UserData[id],1)
                    end
                end

            end

            if UserData[id].物品[临时id].特技  then
                SendMessage(user.连接id, 2072, {标题="我的"..UserData[id].物品[临时id].特技,文本="鉴定至少80级装备出现特技"..UserData[id].物品[临时id].特技})
                 --RoleControl:添加成就积分(user,1)
            end
			SendMessage(UserData[id].连接id, 7, "#y/鉴定成功,你消耗了" .. 技能等级 .. "点活力" )
			SendMessage(UserData[id].连接id,3006,"66")
		end
	elseif 类型 == "嗜血" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "项链" then
			SendMessage(UserData[id].连接id, 7, "#y/只有项链才能增加效果")
		elseif 技能名称 ~= "嗜血"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次增加效果需要消耗" .. 技能等级 .. "点活力")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
			ItemControl:添加装备临时效果(UserData[id].物品[临时id],"体质",math.random(math.floor(技能等级 * 0.2), math.floor(技能等级 * 0.5)))
		   
	       SendMessage(UserData[id].连接id, 7, "#y/添加装备临时效果成功！")
			SendMessage(UserData[id].连接id,3006,"66")
		end
elseif 类型 == "担山赶月" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "项链" then
			SendMessage(UserData[id].连接id, 7, "#y/只有项链才能增加效果")
		elseif 技能名称 ~= "担山赶月"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次增加效果需要消耗" .. 技能等级 .. "点活力")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
			ItemControl:添加装备临时效果(UserData[id].物品[临时id],"力量",math.random(math.floor(技能等级 * 0.2), math.floor(技能等级 * 0.5)))
		   
	       SendMessage(UserData[id].连接id, 7, "#y/添加装备临时效果成功！")
			SendMessage(UserData[id].连接id,3006,"66")
		end
elseif 类型 == "鬼斧神工" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "腰带" then
			SendMessage(UserData[id].连接id, 7, "#y/只有腰带才能增加效果")
		elseif 技能名称 ~= "鬼斧神工"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次增加效果需要消耗" .. 技能等级 .. "点活力")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
			ItemControl:添加装备临时效果(UserData[id].物品[临时id],"封印命中等级",math.random(math.floor(技能等级 * 0.2), math.floor(技能等级 * 0.5)))
		   
	       SendMessage(UserData[id].连接id, 7, "#y/添加装备临时效果成功！")
			SendMessage(UserData[id].连接id,3006,"66")
		end

elseif 类型 == "幽影灵魄" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "鞋子" then
			SendMessage(UserData[id].连接id, 7, "#y/只有鞋子才能增加效果")
		elseif 技能名称 ~= "幽影灵魄"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次增加效果需要消耗" .. 技能等级 .. "点活力")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
			ItemControl:添加装备临时效果(UserData[id].物品[临时id],"抵抗封印等级",math.random(math.floor(技能等级 * 0.2), math.floor(技能等级 * 0.5)))
		   
	       SendMessage(UserData[id].连接id, 7, "#y/添加装备临时效果成功！")
			SendMessage(UserData[id].连接id,3006,"66")
		end
	elseif 类型 == "神兵护法" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "头盔" then
			SendMessage(UserData[id].连接id, 7, "#y/只有帽子才能增加效果")
		elseif 技能名称 ~= "神兵护法"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次增加效果需要消耗" .. 技能等级 .. "点活力")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
			ItemControl:添加装备临时效果(UserData[id].物品[临时id],"命中",math.random(math.floor(技能等级 *1), math.floor(技能等级 * 2)))
		   
	       SendMessage(UserData[id].连接id, 7, "#y/添加装备临时效果成功！")
			SendMessage(UserData[id].连接id,3006,"66")
		end
	elseif 类型 == "轻如鸿毛" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "武器" then
			SendMessage(UserData[id].连接id, 7, "#y/只有武器才能增加效果")
		elseif 技能名称 ~= "轻如鸿毛"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次增加效果需要消耗" .. 技能等级 .. "点活力")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
			ItemControl:添加装备临时效果(UserData[id].物品[临时id],"魔法",math.random(math.floor(技能等级 *1), math.floor(技能等级 * 2)))
		
	       SendMessage(UserData[id].连接id, 7, "#y/添加装备临时效果成功！")
			SendMessage(UserData[id].连接id,3006,"66")
		end
	elseif 类型 == "元阳护体" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "武器" then
			SendMessage(UserData[id].连接id, 7, "#y/只有武器才能增加效果")
		elseif 技能名称 ~= "元阳护体"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次增加效果需要消耗" .. 技能等级 .. "点活力")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
		   
		   ItemControl:添加装备临时效果(UserData[id].物品[临时id],"气血回复效果",math.random(math.floor(技能等级 *0.2), math.floor(技能等级 * 0.5)))
	       SendMessage(UserData[id].连接id, 7, "#y/添加装备临时效果成功！")
			SendMessage(UserData[id].连接id,3006,"66")
		end
	elseif 类型 == "神木呓语" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "鞋子" then
			SendMessage(UserData[id].连接id, 7, "#y/只有鞋子才能增加效果")
		elseif 技能名称 ~= "神木呓语"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次增加效果需要消耗" .. 技能等级 .. "点活力")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
			ItemControl:添加装备临时效果(UserData[id].物品[临时id],"固定伤害",math.random(math.floor(技能等级 *0.2), math.floor(技能等级 * 0.5)))
		  
	       SendMessage(UserData[id].连接id, 7, "#y/添加装备临时效果成功！")
			SendMessage(UserData[id].连接id,3006,"66")
		end
	elseif 类型 == "龙附" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "武器" then
			SendMessage(UserData[id].连接id, 7, "#y/只有武器才能增加效果")
		elseif 技能名称 ~= "龙附"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次增加效果需要消耗" .. 技能等级 .. "点活力")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
			ItemControl:添加装备临时效果(UserData[id].物品[临时id],"伤害",math.random(math.floor(技能等级 *0.5), math.floor(技能等级 * 1)))
		  
	       SendMessage(UserData[id].连接id, 7, "#y/添加装备临时效果成功！")
			SendMessage(UserData[id].连接id,3006,"66")
		end
	elseif 类型 == "拈花妙指" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "鞋子" then
			SendMessage(UserData[id].连接id, 7, "#y/只有鞋子才能增加效果")
		elseif 技能名称 ~= "拈花妙指"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次增加效果需要消耗" .. 技能等级 .. "点活力")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
			ItemControl:添加装备临时效果(UserData[id].物品[临时id],"速度",math.random(math.floor(技能等级 *0.2), math.floor(技能等级 * 0.5)))
		   
	       SendMessage(UserData[id].连接id, 7, "#y/添加装备临时效果成功！")
			SendMessage(UserData[id].连接id,3006,"66")
		end
	elseif 类型 == "尸气漫天" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "头盔" then
			SendMessage(UserData[id].连接id, 7, "#y/只有头盔才能增加效果")
		elseif 技能名称 ~= "尸气漫天"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次增加效果需要消耗" .. 技能等级 .. "点活力")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
			ItemControl:添加装备临时效果(UserData[id].物品[临时id],"耐力",math.random(math.floor(技能等级 *0.2), math.floor(技能等级 * 0.5)))
		   
	       SendMessage(UserData[id].连接id, 7, "#y/添加装备临时效果成功！")
			SendMessage(UserData[id].连接id,3006,"66")
		end
	elseif 类型 == "神力无穷" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "腰带" then
			SendMessage(UserData[id].连接id, 7, "#y/只有腰带才能增加效果")
		elseif 技能名称 ~= "神力无穷"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次增加效果需要消耗" .. 技能等级 .. "点活力")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
			ItemControl:添加装备临时效果(UserData[id].物品[临时id],"愤怒",math.random(math.floor(技能等级 *0.1), math.floor(技能等级 * 0.2)))
		 
	       SendMessage(UserData[id].连接id, 7, "#y/添加装备临时效果成功！")
			SendMessage(UserData[id].连接id,3006,"66")
		end
	elseif 类型 == "盘丝舞" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "武器" then
			SendMessage(UserData[id].连接id, 7, "#y/只有武器才能增加效果")
		elseif 技能名称 ~= "盘丝舞"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次增加效果需要消耗" .. 技能等级 .. "点活力")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
			ItemControl:添加装备临时效果(UserData[id].物品[临时id],"防御",math.random(math.floor(技能等级 *0.5), math.floor(技能等级 * 1)))
		  
	       SendMessage(UserData[id].连接id, 7, "#y/添加装备临时效果成功！")
			SendMessage(UserData[id].连接id,3006,"66")
		end
	elseif 类型 == "一气化三清" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "衣服" then
			SendMessage(UserData[id].连接id, 7, "#y/只有衣服才能增加效果")
		elseif 技能名称 ~= "一气化三清"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次增加效果需要消耗" .. 技能等级 .. "点活力")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
		   ItemControl:添加装备临时效果(UserData[id].物品[临时id],"魔力", math.random(math.floor(技能等级 * 0.2), math.floor(技能等级 * 0.5)))
	       SendMessage(UserData[id].连接id, 7, "#y/添加装备临时效果成功！")
			SendMessage(UserData[id].连接id,3006,"66")
		end
	elseif 类型 == "莲华妙法" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "项链" then
			SendMessage(UserData[id].连接id, 7, "#y/只有项链才能增加效果")
		elseif 技能名称 ~= "莲华妙法"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次增加效果需要消耗" .. 技能等级 .. "点活力")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
		   ItemControl:添加装备临时效果(UserData[id].物品[临时id],"法术伤害", math.random(math.floor(技能等级 * 0.2), math.floor(技能等级 * 0.5)))
	       SendMessage(UserData[id].连接id, 7, "#y/添加装备临时效果成功！")
			SendMessage(UserData[id].连接id,3006,"66")
		end
	elseif 类型 == "魔王护持" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "武器" then
			SendMessage(UserData[id].连接id, 7, "#y/只有武器才能增加效果")
		elseif 技能名称 ~= "魔王护持"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次增加效果需要消耗" .. 技能等级 .. "点活力")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
		   ItemControl:添加装备临时效果(UserData[id].物品[临时id],"气血", math.random(math.floor(技能等级 * 1), math.floor(技能等级 * 2)))
	       SendMessage(UserData[id].连接id, 7, "#y/添加装备临时效果成功！")
			SendMessage(UserData[id].连接id,3006,"66")
		end
	elseif 类型 == "浩然正气" then
		local 道具类型 = UserData[id].物品[临时id].类型
		local 道具等级 = UserData[id].物品[临时id].等级
		技能格子 = 分割文本(技能格子,"*-*")
		local 技能名称 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].名称
		local 技能等级 = UserData[id].角色.师门技能[技能格子[1]+0].包含技能[技能格子[2]+0].等级
		if  道具类型 ~= "衣服" then
			SendMessage(UserData[id].连接id, 7, "#y/只有衣服才能增加效果")
		elseif 技能名称 ~= "浩然正气"then
		    SendMessage(UserData[id].连接id, 7, "#y/技能数据错误")
		elseif UserData[id].角色.当前活力 < 技能等级 then
			SendMessage(UserData[id].连接id, 7, "#y/本次增加效果需要消耗" .. 技能等级 .. "点活力")
		else
			UserData[id].角色.当前活力 = UserData[id].角色.当前活力 - 技能等级
		   ItemControl:添加装备临时效果(UserData[id].物品[临时id],"法防", math.random(math.floor(技能等级 * 0.2), math.floor(技能等级 * 0.5)))
	       SendMessage(UserData[id].连接id, 7, "#y/添加装备临时效果成功！")
			SendMessage(UserData[id].连接id,3006,"66")
		end
	end

 end