-- @Author: 作者QQ381990860
-- @Date:   2021-09-07 00:54:26
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-03-19 01:12:04
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-06-25 02:07:25
--======================================================================--
function ItemControl:道具合宠(id,参数,内容,类型)------------------------完成--
 local lysj={参数,内容+0}
  if UserData[id].召唤兽.数据.参战~=0 then
     SendMessage(UserData[id].连接id,7,"#y/请先将所有召唤兽设置为休息状态")
     return 0
  elseif UserData[id].角色.道具[类型][lysj[2]]==nil then
     SendMessage(UserData[id].连接id,7,"#y/你没有这样的道具")
     return 0
  end

 local djid=UserData[id].角色.道具[类型][lysj[2]]
  if UserData[id].物品[djid].名称=="金柳露" then
        if UserData[id].召唤兽.数据[lysj[1]].类型 =="变异" then
        	SendMessage(UserData[id].连接id,7,"#Y/这个道具不支持对变异召唤兽使用!")
        elseif UserData[id].召唤兽.数据[lysj[1]].类型 =="神兽" then
        		SendMessage(UserData[id].连接id,7,"#Y/这个道具不支持对神兽使用!")
        elseif UserData[id].召唤兽.数据[lysj[1]].内丹.总数 ~= UserData[id].召唤兽.数据[lysj[1]].内丹.可用数量 then
           SendMessage(UserData[id].连接id,7,"#Y/有内丹的宝宝不能使用")
        elseif UserData[id].召唤兽.数据[lysj[1]].参战等级 > 86 then
        SendMessage(UserData[id].连接id,7,"#Y/只能对参战等级在85以下召唤兽使用")
        else 
            local 临时召唤兽=require("Script/Pet/PetControl")(id)
            临时召唤兽:创建召唤兽(UserData[id].召唤兽.数据[lysj[1]].造型,"宝宝")
            UserData[id].召唤兽.数据[lysj[1]]=table.loadstring(临时召唤兽:获取指定数据(1))
            临时召唤兽=nil
            ItemControl:RemoveItems(UserData[id],lysj[2],"炼妖")
            SendMessage(UserData[id].连接id,7,"#Y/洗宠成功")
        end
  elseif UserData[id].物品[djid].名称=="超级金柳露" then
      if UserData[id].召唤兽.数据[lysj[1]].类型 =="变异" then
      SendMessage(UserData[id].连接id,7,"#Y/这个道具不支持对变异召唤兽使用!")
      elseif UserData[id].召唤兽.数据[lysj[1]].类型 =="神兽" then
      	SendMessage(UserData[id].连接id,7,"#Y/这个道具不支持对神兽使用!")
      elseif UserData[id].召唤兽.数据[lysj[1]].参战等级 < 94 then
      SendMessage(UserData[id].连接id,7,"#Y/只能对参战等级在95以上召唤兽使用")
      elseif UserData[id].召唤兽.数据[lysj[1]].内丹.总数 ~= UserData[id].召唤兽.数据[lysj[1]].内丹.可用数量 then
      SendMessage(UserData[id].连接id,7,"#Y/有内丹的宝宝不能使用")
      else 
          local 临时召唤兽=require("Script/Pet/PetControl")(id)
          临时召唤兽:创建召唤兽(UserData[id].召唤兽.数据[lysj[1]].造型,"宝宝")
          UserData[id].召唤兽.数据[lysj[1]]=table.loadstring(临时召唤兽:获取指定数据(1))
          临时召唤兽=nil
          ItemControl:RemoveItems(UserData[id],lysj[2],"炼妖")
          SendMessage(UserData[id].连接id,7,"#Y/洗宠成功")
      end
  elseif UserData[id].物品[djid].名称=="净瓶玉露" then
      if UserData[id].召唤兽.数据[lysj[1]].类型 ~="变异" then
      SendMessage(UserData[id].连接id,7,"#Y/这个道具只能对变异召唤兽使用!")
      elseif UserData[id].召唤兽.数据[lysj[1]].内丹.总数 ~= UserData[id].召唤兽.数据[lysj[1]].内丹.可用数量 then
       SendMessage(UserData[id].连接id,7,"#Y/有内丹的宝宝不能使用")
      elseif UserData[id].召唤兽.数据[lysj[1]].参战等级 > 86 then
      SendMessage(UserData[id].连接id,7,"#Y/只能对参战等级在85以下召唤兽使用")
      else 
          local 临时召唤兽=require("Script/Pet/PetControl")(id)
          临时召唤兽:创建召唤兽(UserData[id].召唤兽.数据[lysj[1]].造型,"变异",nil,nil,nil,UserData[id].召唤兽.数据[lysj[1]].染色方案,UserData[id].召唤兽.数据[lysj[1]].染色组)
          UserData[id].召唤兽.数据[lysj[1]]=table.loadstring(临时召唤兽:获取指定数据(1))
          临时召唤兽=nil
          ItemControl:RemoveItems(UserData[id],lysj[2],"炼妖")
          SendMessage(UserData[id].连接id,7,"#Y/洗宠成功")
      end
  elseif UserData[id].物品[djid].名称=="超级净瓶玉露" then
      if UserData[id].召唤兽.数据[lysj[1]].类型 ~="变异" then
      SendMessage(UserData[id].连接id,7,"#Y/这个道具只能对变异召唤兽使用!")
      elseif UserData[id].召唤兽.数据[lysj[1]].参战等级 < 94 then
      SendMessage(UserData[id].连接id,7,"#Y/只能对参战等级在95以上召唤兽使用")
      elseif UserData[id].召唤兽.数据[lysj[1]].内丹.总数 ~= UserData[id].召唤兽.数据[lysj[1]].内丹.可用数量 then
      SendMessage(UserData[id].连接id,7,"#Y/有内丹的宝宝不能使用")
      else 
          local 临时召唤兽=require("Script/Pet/PetControl")(id)
          临时召唤兽:创建召唤兽(UserData[id].召唤兽.数据[lysj[1]].造型,"变异",nil,nil,nil,UserData[id].召唤兽.数据[lysj[1]].染色方案,UserData[id].召唤兽.数据[lysj[1]].染色组)
          UserData[id].召唤兽.数据[lysj[1]]=table.loadstring(临时召唤兽:获取指定数据(1))
          临时召唤兽=nil
          ItemControl:RemoveItems(UserData[id],lysj[2],"炼妖")
          SendMessage(UserData[id].连接id,7,"#Y/洗宠成功")
      end
  elseif UserData[id].物品[djid].名称=="召唤兽内丹" or UserData[id].物品[djid].名称=="高级召唤兽内丹"  and UserData[id].物品[djid].技能~=nil then
        local ndsj =  UserData[id].召唤兽.数据[lysj[1]].内丹
       if #ndsj > 0 then
            for i=1,#ndsj do
              if ndsj[i].技能 == UserData[id].物品[djid].技能 then
                    if ndsj[i].等级 == 10 then
                      SendMessage(UserData[id].连接id,7,"#y/该内丹已经学满")
                       return 0
                    else
              						UserData[id].召唤兽.数据[lysj[1]].内丹[i].等级 = UserData[id].召唤兽.数据[lysj[1]].内丹[i].等级 + 1
              						SendMessage(UserData[id].连接id,7,"#y/恭喜你的"..UserData[id].召唤兽.数据[lysj[1]].名称.."#r/"..UserData[id].物品[djid].技能.."#y/升到第#r/"..UserData[id].召唤兽.数据[lysj[1]].内丹[i].等级.."#y/层")
              						UserData[id].召唤兽:刷新属性(lysj[1])
              						ItemControl:RemoveItems(UserData[id],lysj[2],"炼妖")
                           break
                     end
               elseif ndsj[i+1] == nil then
          					if ndsj.可用数量 > 0 then
          						UserData[id].召唤兽.数据[lysj[1]].内丹[i+1] = {}
          						UserData[id].召唤兽.数据[lysj[1]].内丹[i+1].技能= UserData[id].物品[djid].技能
          						UserData[id].召唤兽.数据[lysj[1]].内丹[i+1].等级 = 1
          						UserData[id].召唤兽.数据[lysj[1]].内丹.可用数量=UserData[id].召唤兽.数据[lysj[1]].内丹.可用数量 - 1
          						SendMessage(UserData[id].连接id,7,"#y/恭喜你的"..UserData[id].召唤兽.数据[lysj[1]].名称.."学会#r/"..UserData[id].物品[djid].技能)
                      ItemControl:RemoveItems(UserData[id],lysj[2],"炼妖")
          						UserData[id].召唤兽:刷新属性(lysj[1])
          					else
                       local 随机编号 = math.random(1,#UserData[id].召唤兽.数据[lysj[1]].内丹)
                       local 替换内丹 = UserData[id].召唤兽.数据[lysj[1]].内丹[随机编号].技能
                      UserData[id].召唤兽.数据[lysj[1]].内丹[随机编号] = {}
                      UserData[id].召唤兽.数据[lysj[1]].内丹[随机编号].技能= UserData[id].物品[djid].技能
                      UserData[id].召唤兽.数据[lysj[1]].内丹[随机编号].等级 = 1
                      SendMessage(UserData[id].连接id,7,"#y/恭喜你的"..UserData[id].召唤兽.数据[lysj[1]].名称.."的内丹#R/"..替换内丹 .."#Y/替换成#r/"..UserData[id].物品[djid].技能)
                      ItemControl:RemoveItems(UserData[id],lysj[2],"炼妖")
                      UserData[id].召唤兽:刷新属性(lysj[1])
          					end
                end
            end
       else
          UserData[id].召唤兽.数据[lysj[1]].内丹[1] = {}
          UserData[id].召唤兽.数据[lysj[1]].内丹[1].技能= UserData[id].物品[djid].技能
          UserData[id].召唤兽.数据[lysj[1]].内丹[1].等级 = 1
          UserData[id].召唤兽.数据[lysj[1]].内丹.可用数量=UserData[id].召唤兽.数据[lysj[1]].内丹.可用数量 - 1
          ItemControl:RemoveItems(UserData[id],lysj[2],"炼妖")
          SendMessage(UserData[id].连接id,7,"#y/恭喜你的"..UserData[id].召唤兽.数据[lysj[1]].名称.."学会#r/"..UserData[id].召唤兽.数据[lysj[1]].内丹[1].技能)
          UserData[id].召唤兽:刷新属性(lysj[1])
      end
 elseif UserData[id].物品[djid].名称=="魔兽要诀" or UserData[id].物品[djid].名称=="高级魔兽要诀" or UserData[id].物品[djid].名称=="特殊魔兽要诀" then
    local 临时技能=UserData[id].物品[djid].技能
    local 技能格子=0
    self.基础几率=10    
    if UserData[id].角色.祈福>0 then
        UserData[id].角色.祈福=UserData[id].角色.祈福-1
        self.基础几率=self.基础几率+30      
        SendMessage(UserData[id].连接id,7,"#y/你消耗了1次祈福次数，当前可用祈福次数为#r/"..UserData[id].角色.祈福.."#y/次")
    end
                                 
    if math.random(1000)<=self.基础几率 +(8-#UserData[id].召唤兽.数据[lysj[1]].技能+1)*5 then
      if UserData[id].召唤兽.数据[lysj[1]].类型 =="神兽"and #UserData[id].召唤兽.数据[lysj[1]].技能<11 then
          技能格子=#UserData[id].召唤兽.数据[lysj[1]].技能+1
          SendMessage(UserData[id].连接id,7,"#y/你的神兽已经成功顶出一个新技能,神兽可以顶到11技能上限!")
          elseif UserData[id].召唤兽.数据[lysj[1]].类型 =="变异"and #UserData[id].召唤兽.数据[lysj[1]].技能<11 then
          技能格子=#UserData[id].召唤兽.数据[lysj[1]].技能+1
          SendMessage(UserData[id].连接id,7,"#y/你的变异已经成功顶出一个新技能,神兽可以顶到11技能上限!")

       elseif #UserData[id].召唤兽.数据[lysj[1]].技能<Config.顶书限制 then
        技能格子=#UserData[id].召唤兽.数据[lysj[1]].技能+1
       else
         技能格子=math.random(1,#UserData[id].召唤兽.数据[lysj[1]].技能)
       end
    elseif #UserData[id].召唤兽.数据[lysj[1]].技能<1 then
        技能格子=#UserData[id].召唤兽.数据[lysj[1]].技能+1
    else
        技能格子=math.random(1,#UserData[id].召唤兽.数据[lysj[1]].技能)
    end
    local 重复技能=0
    for n=1,#UserData[id].召唤兽.数据[lysj[1]].技能 do
     if UserData[id].召唤兽.数据[lysj[1]].技能[n].名称==临时技能   then
     	重复技能=n
     end
    end
    if UserData[id].召唤兽.数据[lysj[1]].技能[技能格子] then
     if UserData[id].召唤兽.数据[lysj[1]].技能[技能格子].认证 then
       技能格子=math.random(1,#UserData[id].召唤兽.数据[lysj[1]].技能)
     end
     if UserData[id].召唤兽.数据[lysj[1]].技能[技能格子].认证 then
      for i=1,#UserData[id].召唤兽.数据[lysj[1]].技能 do
        if not UserData[id].召唤兽.数据[lysj[1]].技能[i].认证 then
            技能格子=i
            break
        end
      end
     end
    end
    if 重复技能==0  then
     UserData[id].召唤兽.数据[lysj[1]].技能[技能格子]=置技能(临时技能)
    end
    SendMessage(UserData[id].连接id,7,"#y/你的这只召唤兽学会了新技能#r/"..临时技能)
    ItemControl:RemoveItems(UserData[id],lysj[2],"炼妖")
    UserData[id].召唤兽.数据[lysj[1]].默认法术=nil
 end
   self:索要炼妖数据(id,类型)
   SendMessage(UserData[id].连接id,3040,"1")
 end
function ItemControl:道具提取(id,参数,内容,类型)------------------------完成--
 local lysj={参数,内容+0}
  if UserData[id].召唤兽.数据.参战~=0 then
     SendMessage(UserData[id].连接id,7,"#y/请先将所有召唤兽设置为休息状态")
     return 0
  elseif UserData[id].角色.道具[类型][lysj[2]]==nil then
     SendMessage(UserData[id].连接id,7,"#y/你没有这样的道具")
     return 0
  end

 local djid=UserData[id].角色.道具[类型][lysj[2]]
  if UserData[id].物品[djid].名称=="圣兽丹" then
		local zxsj =  UserData[id].召唤兽.数据[lysj[1]].造型
		if RoleControl:取可用格子数量(UserData[id],"包裹") < 1 then
		SendMessage(UserData[id].连接id, 7, "#y/请先预留出1个道具空间")
		return 0
		end
		if 取召唤兽饰品(zxsj) then

      self:GiveItem(id,zxsj.."饰品")
      ItemControl:RemoveItems(UserData[id],lysj[2],"炼妖")
      SendMessage(UserData[id].连接id,7,"#y/炼化成功你的"..UserData[id].召唤兽.数据[lysj[1]].名称.."消失的无影无踪,恭喜你获得了#r/"..zxsj.."饰品")
      table.remove(UserData[id].召唤兽.数据,lysj[1])
      UserData[id].召唤兽.数据.参战=0
      self:索要炼妖数据(id,类型)
      
	   else
	   	SendMessage(UserData[id].连接id,7,"#y/该召唤兽目前没有饰品")
	   	return
	   end
 elseif UserData[id].物品[djid].名称=="吸附石" then
      if #UserData[id].召唤兽.数据[lysj[1]].技能 <=0 then
        	SendMessage(UserData[id].连接id,7,"#y/该召唤兽目前没有技能无法提取!")
      	return
      else
        local odds = DebugMode and 100 or 2 
         if math.random(100) <=odds then
            local sj = UserData[id].召唤兽.数据[lysj[1]].技能[math.random(1,#UserData[id].召唤兽.数据[lysj[1]].技能)]
            if sj.名称=="须弥真言" or  sj.名称=="光照万象" or  sj.名称=="北冥之渊" or   sj.名称=="势如破竹"  then
              SendMessage(UserData[id].连接id,7,"#y/须弥真言.光照万象等无法吸附！")
            else
         	 self:GiveItem(id,"点化石",nil,sj.名称)
         	 SendMessage(UserData[id].连接id,7,"#y/你成功的从召唤兽身上吸取到了#r"..sj.名称.."#y/技能")
          end
         else
            SendMessage(UserData[id].连接id,7,"#y/召唤兽技能提取失败,你的召唤兽消失的无影无踪")
         end
      ItemControl:RemoveItems(UserData[id],lysj[2],"炼妖")
      table.remove(UserData[id].召唤兽.数据,lysj[1])
      UserData[id].召唤兽.数据.参战=0
      self:索要炼妖数据(id,类型)
    end
 elseif UserData[id].物品[djid].名称=="炼妖石" then
      if  UserData[id].物品[djid].等级 <  UserData[id].召唤兽.数据[lysj[1]].参战等级  then
        	SendMessage(UserData[id].连接id,7,"#y/召唤兽的参战等级太低无法炼化!")
      	return
      else
      	local lv = UserData[id].物品[djid].等级
         local sj =math.random(100)
         if  sj<=20 then
         	 self:GiveItem(id,"九眼天珠",lv)

         elseif sj <=50 then
            self:GiveItem(id,"三眼天珠",lv)
         elseif sj <=100 then
         	self:GiveItem(id,"天眼珠",lv)
         end
      SendMessage(UserData[id].连接id,7,"#y/炼化成功,你的召唤兽已经消失无影无踪")
       ItemControl:RemoveItems(UserData[id],lysj[2],"炼妖")
      table.remove(UserData[id].召唤兽.数据,lysj[1])
      UserData[id].召唤兽.数据.参战=0
      self:索要炼妖数据(id,类型)
      
    end
 end
 end
function ItemControl:炼妖合宠(id,参数,内容)-----------------------------完成--
  self.基础几率=0
   -- if 编号~=0 then
   --     SendMessage(UserData[id].连接id,7,"#y/请先将所有召唤兽设置为休息状态")
   --     return 0
   -- 	 end
   self.临时炼妖={参数,内容+0}
   if self.临时炼妖==nil then
       SendMessage(UserData[id].连接id,7,"#y/炼妖数据异常")
       return 0
    elseif UserData[id].召唤兽.数据[self.临时炼妖[1]]==nil or  UserData[id].召唤兽.数据[self.临时炼妖[2]]==nil then
             SendMessage(UserData[id].连接id,7,"#y/召唤兽数据错误请重新打开后炼妖")
       return 0
    elseif UserData[id].召唤兽.数据[self.临时炼妖[1]].类型=="神兽" or UserData[id].召唤兽.数据[self.临时炼妖[2]].类型=="神兽"  then
     SendMessage(UserData[id].连接id,7,"#y/神兽不允许进行此种操作")
       return 0
  elseif UserData[id].召唤兽.数据[self.临时炼妖[1]].加锁 or UserData[id].召唤兽.数据[self.临时炼妖[2]].加锁  then
     SendMessage(UserData[id].连接id,7,"#y/加锁召唤兽不允许进行此种操作")
       return 0
     elseif UserData[id].召唤兽.数据[self.临时炼妖[1]].类型=="变异" or UserData[id].召唤兽.数据[self.临时炼妖[2]].类型=="变异" then
     SendMessage(UserData[id].连接id,7,"#y/变异宝宝不允许进行此种操作")
       return 0
    elseif DebugMode==false and (UserData[id].召唤兽.数据[self.临时炼妖[1]].等级<30 or UserData[id].召唤兽.数据[self.临时炼妖[2]].等级<30) then
      SendMessage(UserData[id].连接id,7,"#y/等级小于30级的召唤兽无法进行炼妖操作")
      return 0
     end
   self.禁止名称=""
   for n=1,3 do

     if UserData[id].召唤兽.数据[self.临时炼妖[1]].装备[n]~=nil then

        self.禁止名称=UserData[id].召唤兽.数据[self.临时炼妖[1]].名称

       end

     end

    for n=1,3 do

     if UserData[id].召唤兽.数据[self.临时炼妖[2]].装备[n]~=nil then

        self.禁止名称=UserData[id].召唤兽.数据[self.临时炼妖[2]].名称

       end

     end

   if self.禁止名称~="" then


     SendMessage(UserData[id].连接id,7,"#y/请先卸下"..self.禁止名称.."所佩戴的装备")
      return 0

     end
  self.重置技能=true
   local 临时随机数=math.random(100)
   local 临时造型=""
   if  临时随机数<=65 then
       临时造型=UserData[id].召唤兽.数据[self.临时炼妖[1]].造型
    elseif 临时随机数>65 and 临时随机数<=95 then
        临时造型=UserData[id].召唤兽.数据[self.临时炼妖[2]].造型
     else
        self.重置技能=false
        临时造型="大海龟"
      end

  if 临时随机数<= 5 then
     临时等级 =1
  else
     临时等级=UserData[id].召唤兽.数据[self.临时炼妖[1]].等级+UserData[id].召唤兽.数据[self.临时炼妖[2]].等级
     临时等级=math.floor(临时等级/2*0.75)
  end
 if 临时等级<0 then 临时等级=0 end
 if UserData[id].召唤兽.数据[self.临时炼妖[1]].类型 =="宝宝" and UserData[id].召唤兽.数据[self.临时炼妖[2]].类型 =="宝宝"  then
     临时类型 ="宝宝"
  elseif  UserData[id].召唤兽.数据[self.临时炼妖[1]].类型 =="野怪" and UserData[id].召唤兽.数据[self.临时炼妖[2]].类型 =="野怪" then
    临时类型 ="野怪"
  elseif math.random(100) <=50 then
    临时类型 ="宝宝"
  else
    临时类型 ="野怪"
  end
 self.临时技能={}


    if BBData[UserData[id].召唤兽.数据[self.临时炼妖[1]].造型] and BBData[UserData[id].召唤兽.数据[self.临时炼妖[1]].造型].天生 then
      for i=1,#BBData[UserData[id].召唤兽.数据[self.临时炼妖[1]].造型].天生 do
        self.临时技能[#self.临时技能+1] = BBData[UserData[id].召唤兽.数据[self.临时炼妖[1]].造型].天生[i]
      end
    end
    if BBData[UserData[id].召唤兽.数据[self.临时炼妖[2]].造型] and BBData[UserData[id].召唤兽.数据[self.临时炼妖[2]].造型].天生 then
         for i=1,#BBData[UserData[id].召唤兽.数据[self.临时炼妖[2]].造型].天生 do
          self.临时技能[#self.临时技能+1] = BBData[UserData[id].召唤兽.数据[self.临时炼妖[2]].造型].天生[i]
       end
    end

    for n=1,#UserData[id].召唤兽.数据[self.临时炼妖[1]].技能 do
        self.临时技能[#self.临时技能+1]=UserData[id].召唤兽.数据[self.临时炼妖[1]].技能[n].名称
      end
    for n=1,#UserData[id].召唤兽.数据[self.临时炼妖[2]].技能 do
       self.临时技能[#self.临时技能+1]=UserData[id].召唤兽.数据[self.临时炼妖[2]].技能[n].名称
     end

 local 成长1=UserData[id].召唤兽.数据[self.临时炼妖[1]].成长*1000
 local 成长2=UserData[id].召唤兽.数据[self.临时炼妖[2]].成长*1000

  local 临时资质={}
      临时资质.攻资 = math.floor(math.min(math.random((UserData[id].召唤兽.数据[self.临时炼妖[1]].攻资+UserData[id].召唤兽.数据[self.临时炼妖[2]].攻资)*0.40,(UserData[id].召唤兽.数据[self.临时炼妖[1]].攻资+UserData[id].召唤兽.数据[self.临时炼妖[2]].攻资)*0.60),1700))--这个1800是上线得工资
      临时资质.体资 = math.floor(math.min(math.random((UserData[id].召唤兽.数据[self.临时炼妖[1]].体资+UserData[id].召唤兽.数据[self.临时炼妖[2]].体资)*0.40,(UserData[id].召唤兽.数据[self.临时炼妖[1]].体资+UserData[id].召唤兽.数据[self.临时炼妖[2]].体资)*0.60),6000))--这就对应前面得数值体资
      临时资质.法资 = math.floor(math.min(math.random((UserData[id].召唤兽.数据[self.临时炼妖[1]].法资+UserData[id].召唤兽.数据[self.临时炼妖[2]].法资)*0.40,(UserData[id].召唤兽.数据[self.临时炼妖[1]].法资+UserData[id].召唤兽.数据[self.临时炼妖[2]].法资)*0.60),3000))
      临时资质.防资 = math.floor(math.min(math.random((UserData[id].召唤兽.数据[self.临时炼妖[1]].防资+UserData[id].召唤兽.数据[self.临时炼妖[2]].防资)*0.40,(UserData[id].召唤兽.数据[self.临时炼妖[1]].防资+UserData[id].召唤兽.数据[self.临时炼妖[2]].防资)*0.60),1600))
      临时资质.速资 = math.floor(math.min(math.random((UserData[id].召唤兽.数据[self.临时炼妖[1]].速资+UserData[id].召唤兽.数据[self.临时炼妖[2]].速资)*0.40,(UserData[id].召唤兽.数据[self.临时炼妖[1]].速资+UserData[id].召唤兽.数据[self.临时炼妖[2]].速资)*0.60),1700))
      临时资质.躲资 = math.floor(math.min(math.random((UserData[id].召唤兽.数据[self.临时炼妖[1]].躲资+UserData[id].召唤兽.数据[self.临时炼妖[2]].躲资)*0.40,(UserData[id].召唤兽.数据[self.临时炼妖[1]].躲资+UserData[id].召唤兽.数据[self.临时炼妖[2]].躲资)*0.60),1700))
      临时资质.成长 = math.floor(math.min(math.random((成长1+成长2)*0.45,(成长1+成长2)*0.52),1350))/1000--1.35是上限得成长

   self.临时召唤兽=require("Script/Pet/PetControl")(id)
 self.临时召唤兽:创建召唤兽(临时造型,临时类型,临时资质,临时等级)
 if self.临时炼妖[1]>#UserData[id].召唤兽.数据 then
   self.临时炼妖[1]=#UserData[id].召唤兽.数据
   end
 UserData[id].召唤兽.数据[self.临时炼妖[1]]=table.loadstring(self.临时召唤兽:获取指定数据(1))
 self.临时召唤兽=nil
 if self.重置技能 then
      UserData[id].召唤兽.数据[self.临时炼妖[1]].技能={}
         if UserData[id].角色.祈福>0 then
           UserData[id].角色.祈福=UserData[id].角色.祈福-1
           self.基础几率=35 --祈福几率
           SendMessage(UserData[id].连接id,7,"#y/你消耗了1次祈福次数，当前可用祈福次数为#r/"..UserData[id].角色.祈福.."#y/次")
        end       self.临时技能 = 删除重复(self.临时技能)

          
           local 天生 = BBData[UserData[id].召唤兽.数据[self.临时炼妖[1]].造型].天生  
      for n=1,#self.临时技能 do
        local 几率 = math.random(100)
        local probability = (77+(7-n*7))
           if (几率<=math.max(probability,5) +self.基础几率 ) and #UserData[id].召唤兽.数据[self.临时炼妖[1]].技能<24 then
                 

                  table.insert(UserData[id].召唤兽.数据[self.临时炼妖[1]].技能,置技能(self.临时技能[n]))
           	end

      end


 	end

   UserData[id].召唤兽.数据[self.临时炼妖[1]].成长=临时资质.成长
    if #UserData[id].召唤兽.数据[self.临时炼妖[1]].技能 >=8 then---这个地方
         -- RoleControl:添加成就积分(UserData[id],1)
     SendMessage(UserData[id].连接id, 2072, {标题="合宠达人",文本="恭喜你炼妖出了8技能以上的召唤兽"})

      广播消息(9,"#xt/#y/恭喜玩家#r/ "..UserData[id].角色.名称.."#y/合成"..#UserData[id].召唤兽.数据[self.临时炼妖[1]].技能.."技能以上的召唤兽！牛逼#80继续回炉翻页不是梦#1")
    end
   table.remove(UserData[id].召唤兽.数据,self.临时炼妖[2])
    UserData[id].召唤兽.数据.参战=0
   SendMessage(UserData[id].连接id,7,"#y/恭喜你合出了#r/"..临时造型)
   SendMessage(UserData[id].连接id,3002,UserData[id].召唤兽:获取数据())
   SendMessage(UserData[id].连接id,3040,"6")

 end

function ItemControl:索要炼妖数据(id, 内容)-----------------------------完成--
	SendMessage(UserData[id].连接id, 3002, UserData[id].召唤兽:获取数据())
	self.发送信息 = {}
	for n = 1, 20 do
		if UserData[id].角色.道具[内容][n] ~= nil and UserData[id].物品[UserData[id].角色.道具[内容][n]] ~= nil then
			self.发送信息[n] = UserData[id].物品[UserData[id].角色.道具[内容][n]]
		end
	end
	self.发送信息.类型 = 内容
	self.发送信息.银两 = UserData[id].角色.道具.货币.银子
	self.发送信息.存银 = UserData[id].角色.道具.货币.存银
	SendMessage(UserData[id].连接id, 3003, self.发送信息)
	SendMessage(UserData[id].连接id, 3004, "66")
 end