--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-12-08 02:28:36
--======================================================================--
function ItemControl:仓库拿走事件(id, 格子, 仓库, 类型)-----------------完成--
	local 内容 = {
		当前类型 = 类型,
		格子 = 格子,
		仓库 = 仓库
	}
	self.可用格子 = RoleControl:取可用道具格子(UserData[id],内容.当前类型)
	if self.可用格子 == 0 then
		SendMessage(UserData[id].连接id, 7, "#y/您身上没有足够的空间")
		return 0
	else
		UserData[id].角色.道具[内容.当前类型][self.可用格子] = UserData[id].角色.仓库[内容.仓库][内容.格子]
		UserData[id].角色.仓库[内容.仓库][内容.格子] = nil
		SendMessage(UserData[id].连接id, 3009, self:索要道具1(id, 内容.当前类型))
		SendMessage(UserData[id].连接id, 3010, self:索要仓库道具(id, 内容.仓库))
		SendMessage(UserData[id].连接id, 3006, "66")
	end
 end
function ItemControl:索要仓库道具(id, 内容)-----------------------------完成--
	self.发送信息 = {}
	内容 = 内容 + 0

	for n = 1, 20 do
		if UserData[id].角色.仓库[内容][n] ~= nil then
			self.发送信息[n] = {
				道具 = UserData[id].物品[UserData[id].角色.仓库[内容][n]],
				编号 = n
			}
		end
	end

	self.发送信息.类型 = 内容
	self.发送信息.总数 = #UserData[id].角色.仓库

	return self.发送信息

 end
function ItemControl:neaten(id, data )----仓库整理

local array={};
local nuber=0;


	for i,v in pairs(UserData[id].角色.仓库[data]) do
		nuber =nuber+1;
		array[nuber]=v;
	end
	UserData[id].角色.仓库[data]= array;
		SendMessage(UserData[id].连接id, 3010, self:索要仓库道具(id, data))
end
function ItemControl:仓库存放事件(id, 格子, 仓库, 类型)-----------------完成--
	local 内容 = {
		当前类型 = 类型,
		格子 = 格子,
		仓库 = 仓库
	}
	self.可用格子 = 0

	for n = 1, 20 do
		if UserData[id].角色.仓库[内容.仓库][n] == UserData[id].角色.道具[内容.当前类型][内容.格子] then
			UserData[id].角色.道具[内容.当前类型][内容.格子] = nil

			SendMessage(UserData[id].连接id, 3009, self:索要道具1(id, 内容.当前类型))
			SendMessage(UserData[id].连接id, 3010, self:索要仓库道具(id, 内容.仓库))
			SendMessage(UserData[id].连接id, 3006, "66")

			return 0
		end

		if self.可用格子 == 0 and UserData[id].角色.仓库[内容.仓库][n] == nil then
			self.可用格子 = n
		end
	end

	if self.可用格子 == 0 then
		SendMessage(UserData[id].连接id, 7, "#y/您当前的仓库已经无法存放更多的道具了")

		return 0
	elseif UserData[id].物品[UserData[id].角色.道具[内容.当前类型][内容.格子]].Time then
				SendMessage(UserData[id].连接id, 7, "#y/有效期的道具无法存入仓库")

		return 0
	else
		UserData[id].角色.仓库[内容.仓库][self.可用格子] = UserData[id].角色.道具[内容.当前类型][内容.格子]
		UserData[id].角色.道具[内容.当前类型][内容.格子] = nil

		SendMessage(UserData[id].连接id, 3009, self:索要道具1(id, 内容.当前类型))
		SendMessage(UserData[id].连接id, 3010, self:索要仓库道具(id, 内容.仓库))
		SendMessage(UserData[id].连接id, 3006, "66")
	end
 end