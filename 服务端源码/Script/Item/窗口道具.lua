-- @Author: 作者QQ381990860
-- @Date:   2021-09-07 00:54:26
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-09-09 21:05:34
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-06-30 17:25:57
--======================================================================--


local 物品={
神兵图鉴=function (user,Item,TempItem)
		if  TempItem.类型 ~= "武器" and  TempItem.类型 ~= "女衣" and  TempItem.类型 ~= "男衣" and  TempItem.类型 ~= "衣服" then
			SendMessage(user.连接id, 7, "#y/只有武器和衣服才可以使用鉴定功能")
			return true
		elseif Item.等级 < TempItem.等级 then
            SendMessage(user.连接id, 7, "#y/Item.等级不够,无法鉴定这个装备")
		elseif TempItem.鉴定 then
			SendMessage(user.连接id, 7, "#y/你的这件装备已经鉴定过了")
		else
			TempItem.鉴定 = true
            if TempItem.特效 and TempItem.等级 >80 then
                for i=1,#TempItem.特效 do
                    if TempItem.特效[i] =="无级别限制" then
                        SendMessage(user.连接id, 2072, {标题="牛了！无级别啊",文本="鉴定至少80级装备出现特效无级别"})
                        --RoleControl:添加成就积分(user,1)
                    end
                end

            end

            if TempItem.特技  then
                SendMessage(user.连接id, 2072, {标题="我的"..TempItem.特技,文本="鉴定至少80级装备出现特技"..TempItem.特技})
                 --RoleControl:添加成就积分(user,1)
            end


   
      
          
			SendMessage(user.连接id, 7, "#y/鉴定成功" )
		end
end,
武器幻化石=function (user,Item,TempItem)
        if TempItem.类型~="武器" and  TempItem.类型~="衣服" and TempItem.类型~="头盔" then
             SendMessage(user.连接id,7,"#Y/只有武器或者衣服，头盔才可幻化造型")
             return true
        elseif not TempItem.鉴定 then
              SendMessage(user.连接id,7,"#Y/未鉴定的装备不可以幻化造型")
             return true
        else

          local  武器类型={}
            if TempItem.类型=="武器" then
               武器类型={[1]="剑",[2]="扇",[3]="锤",[4]="枪",[5]="刀",[6]="斧",[7]="环",[8]="双",[9]="飘",[10]="棒",[11]="爪",[12]="鞭",[13]="伞",[14]="巨剑",[15]="宝珠",[16]="法杖",[17]="弓弩",[18]="灯笼"}
            elseif TempItem.类型=="衣服" then
                武器类型={[1]="男衣",[2]="女衣"}
            else
                武器类型={[1]="头盔",[2]="发钗"}
            end
             local  强化  =  TempItem.强化 and 2 or 1
            TempItem.名称= EquipmentControl:获取名称(TempItem.类型,武器类型[math.random(#武器类型)],TempItem.等级,强化)
            --TempItem.专用=user.id
            SendMessage(user.连接id,7,"#Y/你的装备成功幻化为"..TempItem.名称)
        end
end,





灵饰洗炼石=function (user,Item,TempItem)
        if TempItem.类型~="戒指" and TempItem.类型~="耳饰" and TempItem.类型~="佩饰" and TempItem.类型~="手镯"   then
             SendMessage(user.连接id,7,"#Y/只有灵饰才可洗炼")
             return true
        elseif not TempItem.鉴定 then
              SendMessage(user.连接id,7,"#Y/未鉴定的装备不可以洗炼")
             return true
        elseif  TempItem.幻化等级 > 0 then
              SendMessage(user.连接id,7,"#Y/打过宝石的灵饰无法洗炼")
             return true
        else

            local 灵饰属性 ={}
            灵饰属性.手镯={主属性={"封印命中等级","抵抗封印等级"},副属性={"气血回复效果","气血","防御","抗法术暴击等级","格挡值","法术防御","抗物理暴击等级"}}
            灵饰属性.佩饰={主属性={"速度"},副属性={"气血回复效果","气血","防御","抗法术暴击等级","格挡值","法术防御","抗物理暴击等级"}}
            灵饰属性.戒指={主属性={"伤害","防御"},副属性={"固定伤害","法术伤害","伤害","封印命中等级","法术暴击等级","物理暴击等级","狂暴等级","穿刺等级","法术伤害结果","治疗能力","速度"}}
            灵饰属性.耳饰={主属性={"法术伤害","法术防御"},副属性={"固定伤害","法术伤害","伤害","封印命中等级","法术暴击等级","物理暴击等级","狂暴等级","穿刺等级","法术伤害结果","治疗能力","速度"}}
            灵饰属性.基础={
            封印命中等级={[160]={a=42,b=64},[150]={a=28,b=50},[140]={a=28,b=42},[120]={a=24,b=36},[100]={a=20,b=30},[80]={a=16,b=24},[60]={a=12,b=18},[0]=4}
            ,抵抗封印等级={[160]={a=42,b=64},[150]={a=28,b=50},[140]={a=28,b=42},[120]={a=24,b=36},[100]={a=20,b=30},[80]={a=16,b=24},[60]={a=12,b=18},[0]=4}
            ,防御={[160]={a=42,b=64},[150]={a=28,b=50},[140]={a=28,b=42},[120]={a=24,b=36},[100]={a=20,b=30},[80]={a=16,b=24},[60]={a=12,b=18},[0]=8}
            ,气血回复效果={[160]={a=21,b=40},[150]={a=15,b=30},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=4}
            ,气血={[160]={a=142,b=204},[150]={a=128,b=160},[140]={a=98,b=147},[120]={a=84,b=126},[100]={a=70,b=105},[80]={a=56,b=84},[60]={a=42,b=63},[0]=28}
            ,抗法术暴击等级={[160]={a=42,b=64},[150]={a=28,b=50},[140]={a=28,b=42},[120]={a=24,b=36},[100]={a=20,b=30},[80]={a=16,b=24},[60]={a=12,b=18},[0]=8}
            ,格挡值={[160]={a=42,b=64},[150]={a=28,b=50},[140]={a=28,b=42},[120]={a=24,b=36},[100]={a=20,b=30},[80]={a=16,b=24},[60]={a=12,b=18},[0]=8}
            ,法术防御={[160]={a=42,b=64},[150]={a=28,b=50},[140]={a=28,b=42},[120]={a=24,b=36},[100]={a=20,b=30},[80]={a=16,b=24},[60]={a=12,b=18},[0]=8}
            ,抗物理暴击等级={[160]={a=42,b=64},[150]={a=28,b=50},[140]={a=28,b=42},[120]={a=24,b=36},[100]={a=20,b=30},[80]={a=16,b=24},[60]={a=12,b=18},[0]=8}
            ,速度={[160]={a=30,b=60},[150]={a=28,b=50},[140]={a=20,b=30},[120]={a=18,b=27},[100]={a=16,b=24},[80]={a=14,b=21},[60]={a=12,b=18},[0]=3}
            ,法术伤害={[160]={a=42,b=64},[150]={a=28,b=50},[140]={a=28,b=42},[120]={a=24,b=36},[100]={a=20,b=30},[80]={a=16,b=24},[60]={a=12,b=18},[0]=4}
            ,伤害={[160]={a=22,b=44},[150]={a=28,b=32},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=4}
            ,固定伤害={[160]={a=22,b=34},[150]={a=18,b=23},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=4}
            ,法术伤害={[160]={a=22,b=44},[150]={a=21,b=30},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=4}
            ,法术暴击等级={[160]={a=22,b=44},[150]={a=18,b=23},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=4}
            ,物理暴击等级={[160]={a=22,b=44},[150]={a=18,b=23},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=4}
            ,狂暴等级={[160]={a=22,b=44},[150]={a=18,b=23},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=3}
            ,穿刺等级={[160]={a=22,b=44},[150]={a=18,b=23},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=4}
            ,法术伤害结果={[160]={a=22,b=44},[150]={a=18,b=23},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=3}
            ,治疗能力={[160]={a=22,b=44},[150]={a=18,b=23},[140]={a=14,b=21},[120]={a=12,b=18},[100]={a=10,b=15},[80]={a=8,b=12},[60]={a=6,b=9},[0]=3}
            }
              for n=1,#TempItem.幻化属性.附加 do
                  local 临时属性=灵饰属性[TempItem.类型].副属性[math.random(1,#灵饰属性[TempItem.类型].副属性)]
                  local 临时数值=灵饰属性.基础[临时属性][TempItem.等级].b
                  local 临时下限=灵饰属性.基础[临时属性][TempItem.等级].a
                  临时数值=math.random(临时下限,临时数值)
                  TempItem.幻化属性.附加[n].类型=临时属性
                  TempItem.幻化属性.附加[n].数值=临时数值

              end
              SendMessage(user.连接id,7,"#Y/你的"..TempItem.名称.."洗炼属性成功")
        end
end,

高级装备升星石=function (user,Item,TempItem,PitchOn,Text)
        if TempItem.类型~="武器" and TempItem.类型~="衣服" and TempItem.类型~="头盔" and TempItem.类型~="项链" and TempItem.类型~="腰带" and TempItem.类型~="鞋子"  then
             SendMessage(user.连接id,7,"#Y/只有装备才可强化")
             return true
        elseif not TempItem.鉴定 then
              SendMessage(user.连接id,7,"#Y/未鉴定的装备不可洗炼")
             return true
        elseif TempItem.升星 ==nil then
           TempItem.升星 =0
        elseif TempItem.升星 >=30 then
            SendMessage(user.连接id,7,"#Y/请使用特级升星石进行强化")
           return true
        end

        local probability = math.max(100 - TempItem.升星 * 3,3)

        if math.random(100) < probability  then
               TempItem.升星=TempItem.升星+1
              SendMessage(user.连接id,7,"#Y/你的"..TempItem.名称.."升至"..TempItem.升星.."星成功,为装备增加了"..TempItem.升星..[[%的基础属性]] )
        else
            if TempItem.升星 >= 29 then --2.30级以上清空星
                 TempItem.升星=TempItem.升星-1
                  SendMessage(user.连接id, 7, "#y/升星失败，你的装备星级降为"..TempItem.升星.."少侠再次来过")
            elseif TempItem.升星 >= 25 then--1.20级以上升星掉一星
                TempItem.升星=TempItem.升星
                SendMessage(user.连接id,7,"#Y/你的"..TempItem.名称.."升星失败")
            end

        end
end,

中级装备升星石=function (user,Item,TempItem,PitchOn,Text)
        if TempItem.类型~="武器" and TempItem.类型~="衣服" and TempItem.类型~="头盔" and TempItem.类型~="项链" and TempItem.类型~="腰带" and TempItem.类型~="鞋子"  then
             SendMessage(user.连接id,7,"#Y/只有装备才可强化")
             return true
        elseif not TempItem.鉴定 then
              SendMessage(user.连接id,7,"#Y/未鉴定的装备不可洗炼")
             return true
        elseif TempItem.升星 ==nil then
           TempItem.升星 =0
        elseif TempItem.升星 >=20 then
            SendMessage(user.连接id,7,"#Y/请使用高级升星石进行强化")
           return true
        end

        local probability = math.max(100 - TempItem.升星 * 4,3)

        if math.random(100) < probability  then
               TempItem.升星=TempItem.升星+1
              SendMessage(user.连接id,7,"#Y/你的"..TempItem.名称.."升至"..TempItem.升星.."星成功,为装备增加了"..TempItem.升星..[[%的基础属性]] )
        else
            if TempItem.升星 >= 19 then --2.30级以上清空星
                 TempItem.升星=TempItem.升星-1
                  SendMessage(user.连接id, 7, "#y/升星失败，你的装备星级降为"..TempItem.升星.."少侠再次来过")
            elseif TempItem.升星 >= 15 then--1.20级以上升星掉一星
                TempItem.升星=TempItem.升星
                SendMessage(user.连接id,7,"#Y/你的"..TempItem.名称.."升星失败")
            end

        end
end,

装备升星石=function (user,Item,TempItem,PitchOn,Text)
        if TempItem.类型~="武器" and TempItem.类型~="衣服" and TempItem.类型~="头盔" and TempItem.类型~="项链" and TempItem.类型~="腰带" and TempItem.类型~="鞋子"  then
             SendMessage(user.连接id,7,"#Y/只有装备才可强化")
             return true
        elseif not TempItem.鉴定 then
              SendMessage(user.连接id,7,"#Y/未鉴定的装备不可洗炼")
             return true
        elseif TempItem.升星 ==nil then 
           TempItem.升星 =0
           elseif TempItem.升星 >=10 then
            SendMessage(user.连接id,7,"#Y/请使用中级升星石进行强化")
             return true
        end 
      
        local probability = math.max(100 - TempItem.升星 * 5,3)

        if math.random(100) < probability  then
               TempItem.升星=TempItem.升星+1
              SendMessage(user.连接id,7,"#Y/你的"..TempItem.名称.."升至"..TempItem.升星.."星成功,为装备增加了"..TempItem.升星..[[%的基础属性]] )
        else
            if TempItem.升星 >= 9 then --2.30级以上清空星
                 TempItem.升星=TempItem.升星-1
                  SendMessage(user.连接id, 7, "#y/升星失败，你的装备星级降为"..TempItem.升星.."少侠再次来过")
            elseif TempItem.升星 >= 5 then--1.20级以上升星掉一星
                TempItem.升星=TempItem.升星
                SendMessage(user.连接id,7,"#Y/你的"..TempItem.名称.."升星失败")
            end
        end 
end,

装备洗炼石=function (user,Item,TempItem)
        if TempItem.类型~="武器" and TempItem.类型~="衣服" and TempItem.类型~="头盔" and TempItem.类型~="项链" and TempItem.类型~="腰带" and TempItem.类型~="鞋子"  then
             SendMessage(user.连接id,7,"#Y/只有装备才可洗炼")
             return true
        elseif not TempItem.鉴定 then
              SendMessage(user.连接id,7,"#Y/未鉴定的装备不可洗炼")
             return true
        elseif TempItem.三围属性 ==nil then
                SendMessage(user.连接id,7,"#Y/只能改变双加的装备")
                return true
        elseif TempItem.三围属性[2] ==nil then
                SendMessage(user.连接id,7,"#Y/只能改变双加的装备")
                return true
        else
                for i=1,#TempItem.三围属性 do
                  TempItem.三围属性[i].类型=  全局变量.基础属性[math.random(1,#全局变量.基础属性)]
                  if i==2 and TempItem.三围属性[i].类型 == TempItem.三围属性[i-1].类型 then
                          repeat         
                                TempItem.三围属性[i].类型=  全局变量.基础属性[math.random(1,#全局变量.基础属性)]
                          until    TempItem.三围属性[i].类型 ~= TempItem.三围属性[i-1].类型  
                  end
   

                end
                SendMessage(user.连接id,7,"#Y/你的"..TempItem.名称.."改变双加成功")
        end
end,
附魔宝珠=function (user,Item,TempItem)
 			local 套装效果 ={
		    {"知己知彼","似玉生香","三味真火","日月乾坤","镇妖","尸腐毒","阎罗令","百万神兵","勾魂","判官令","雷击","魔音摄魂",
		    "摄魄","紧箍咒","落岩","含情脉脉","姐妹同心","日光华","水攻","威慑","唧唧歪歪","靛沧海","烈火","催眠符","龙卷雨击",
		    "巨岩破","奔雷咒","失心符","龙腾","苍茫树","泰山压顶","落魄符","龙吟","地裂火","水漫金山","定身符","五雷咒","后发制人",
		    "地狱烈火","满天花雨","飞砂走石","横扫千军","落叶萧萧","尘土刃","荆棘舞","冰川怒","夺命咒","夺魄令","浪涌","裂石","鹰击","翻江搅海"},
		    {"盘丝阵","定心术","极度疯狂","金刚护法","逆鳞","生命之泉","魔王回首","幽冥鬼眼","楚楚可怜","百毒不侵","变身",
		    "普渡众生","炼气化神","修罗隐身","杀气诀","一苇渡江","碎星诀","明光宝烛"},
		    {"巨蛙","狐狸精","黑熊精","凤凰","大海龟","老虎","僵尸","蛟龙","护卫","黑熊","牛头","雨师","树怪","花妖","马面",
		    "如意仙子","赌徒","牛妖","雷鸟人","芙蓉仙子","强盗","小龙女","蝴蝶仙子","巡游天神","海毛虫","野鬼","古代瑞兽",
		    "星灵仙子","大蝙蝠","狼","白熊","幽灵","山贼","虾兵","黑山老妖","鬼将","野猪","蟹将","天兵","吸血鬼","骷髅怪",
		    "龟丞相","天将","净瓶女娲","羊头怪","兔子怪","地狱战神","律法女娲","蛤蟆精","蜘蛛精","风伯","灵符女娲","画魂",
		    "噬天虎" ,"巴蛇","巨力神猿","幽莹娃娃","踏云兽","葫芦宝贝","修罗傀儡鬼","大力金刚","红萼仙子","修罗傀儡妖",
		    "雾中仙","龙龟","金身罗汉","灵鹤","机关兽","蝎子精","藤蔓妖花","夜罗刹","机关鸟","混沌兽","曼珠沙华","炎魔神",
		    "连弩车","长眉灵猴","蜃气妖"}}

		    if TempItem.类型~="武器" and TempItem.类型~="衣服" and TempItem.类型~="头盔" and TempItem.类型~="项链" and TempItem.类型~="腰带" and TempItem.类型~="鞋子"  then
		         SendMessage(user.连接id,7,"#Y/只有装备才可附魔")
		         return true
		    elseif not TempItem.鉴定 then
		          SendMessage(user.连接id,7,"#Y/未鉴定的装备不可以附魔")
		         return true
		    elseif Item.等级 < TempItem.等级 then
		          SendMessage(user.连接id,7,"#Y/当前附魔宝珠小于装备等级不能附魔")
		         return true
		    else
			   		local 点化类型=math.random(3)
			        if 点化类型==1 then
			        TempItem.套装={类型="追加法术",名称=套装效果[点化类型][math.random(1,#套装效果[点化类型])]}
			        elseif 点化类型==2 then
			        TempItem.套装={类型="附加状态",名称=套装效果[点化类型][math.random(1,#套装效果[点化类型])]}
			        elseif 点化类型==3 then
			        TempItem.套装={类型="变身术",名称=套装效果[点化类型][math.random(1,#套装效果[点化类型])]}
			        end
			        SendMessage(user.连接id,7,"#y/附魔装备成功！")
			        
		    end
end,
七彩石=function (user,Item,TempItem)
		    if TempItem.类型~="武器" and TempItem.类型~="衣服" and TempItem.类型~="头盔" and TempItem.类型~="项链" and TempItem.类型~="腰带" and TempItem.类型~="鞋子"  then
		         SendMessage(user.连接id,7,"#Y/只有装备才可附加效果")
		         return true
		    elseif not TempItem.鉴定 then
		          SendMessage(user.连接id,7,"#Y/未鉴定的装备不可以附魔")
		         return true

		    else
                    local 临时属性,临时数值
				
					local 副属性 ={"法术伤害" ,"法术防御" ,"治疗能力" ,"气血回复效果" ,"固定伤害" ,"法术伤害结果" ,"格挡值" ,"狂暴等级" ,"穿刺等级" ,"抗物理暴击等级" ,"法术暴击等级" ,"封印命中等级" ,"物理暴击等级" ,"抗法术暴击等级" ,"抵抗封印等级"}
			
					临时属性=副属性[math.random(1,#副属性)]
					临时数值=math.random(math.floor(TempItem.等级*0.1),math.floor(TempItem.等级*0.3))
				
					TempItem.特殊属性 = {类型=临时属性,属性=临时数值}
					SendMessage(user.连接id, 7, "#y/添加装备附加效果成功！")
					
		    end
end,
神之符=function (user,Item,TempItem)
            if TempItem.类型~="武器" and TempItem.类型~="衣服" and TempItem.类型~="头盔" and TempItem.类型~="项链" and TempItem.类型~="腰带" and TempItem.类型~="鞋子"  then
                 SendMessage(user.连接id,7,"#Y/只有装备才可附加特效")
                 return true
            elseif not TempItem.鉴定 then
                  SendMessage(user.连接id,7,"#Y/未鉴定的装备不可以附加特效")
                 return true
            else
                   if math.random(100)<= 5 then
                        EquipmentControl:添加特效(user,TempItem,"无级别限制")
                   else
                    SendMessage(user.连接id,7,"#y/附加无级别限制失败！")
                    end
            end
end,
灵能符=function (user,Item,TempItem)
		    if TempItem.类型~="手镯" and TempItem.类型~="佩饰" and TempItem.类型~="戒指" and TempItem.类型~="耳饰" then
		         SendMessage(user.连接id,7,"#Y/只有灵饰才可附加特效")
		         return true
		    elseif not TempItem.鉴定 then
		          SendMessage(user.连接id,7,"#Y/未鉴定的灵饰不可以附加特效")
		         return true
		    else
			       if math.random(100)<= 5 then
                        EquipmentControl:添加特效(user,TempItem,"无级别限制")
			       else
			      	SendMessage(user.连接id,7,"#y/附加无级别限制失败！")
			        end  
		    end
end,
灵箓=function (user,Item,TempItem)
            if TempItem.类型~="手镯" and TempItem.类型~="佩饰" and TempItem.类型~="戒指" and TempItem.类型~="耳饰"then
                 SendMessage(user.连接id,7,"#Y/只有灵饰才可以吸取特性")
                 return true
            elseif  not TempItem.鉴定 then
                  SendMessage(user.连接id,7,"#Y/你的灵饰好像没有鉴定")
                 return true
            else
                local  tempr= 取随机灵饰特性()
                TempItem.特性={等级=math.random(2),技能=tempr}  
                SendMessage(user.连接id,7,string.format("#Y/你的%s已经附加了#R/%s#Y/特性",TempItem.名称,tempr))
            end
end,
钟灵石=function (user,Item,TempItem)
            if TempItem.类型~="手镯" and TempItem.类型~="佩饰" and TempItem.类型~="戒指" and TempItem.类型~="耳饰" then
                 SendMessage(user.连接id,7,"#Y/只有灵饰才可以吸取特性")
                 return true
            elseif not TempItem.鉴定 then
                  SendMessage(user.连接id,7,"#Y/你的灵饰好像没有鉴定")
                 return true
            else
                 
                 TempItem.特性={等级=Item.等级,技能=Item.技能}
                  SendMessage(user.连接id,7,string.format("#Y/你的%s已经附加了#R/%s#Y/特性",TempItem.名称,Item.技能))
                    
            end
end,
淬灵石=function (user,Item,TempItem)
            if TempItem.类型~="手镯" and TempItem.类型~="佩饰" and TempItem.类型~="戒指" and TempItem.类型~="耳饰" then
                 SendMessage(user.连接id,7,"#Y/只有灵饰才可以吸取特性")
                 return true
            elseif not TempItem.特性 then
                  SendMessage(user.连接id,7,"#Y/你的灵饰好像没有特性")
                 return true
            elseif user.角色.道具.货币.银子 < TempItem.等级*10000  then
                    SendMessage(user.连接id, 7, "#y/当前银子不足无法吸取灵饰!")
                return true
            elseif user.角色.当前体力 < TempItem.等级*0.5  then
                SendMessage(user.连接id, 7, "#y/当前体力不足无法吸取灵饰!")
                return true
            else
                 RoleControl:扣除体力(user,TempItem.等级*0.5)
                 RoleControl:扣除银子(user,TempItem.等级*10000,"吸取灵饰")
                 ItemControl:GiveItem(user.id,"钟灵石",TempItem.特性.等级,TempItem.特性.技能)
                 TempItem.特性=nil
                    
            end
end,
不磨符=function (user,Item,TempItem)
		    if TempItem.类型~="武器" and TempItem.类型~="衣服" and TempItem.类型~="头盔" and TempItem.类型~="项链" and TempItem.类型~="腰带" and TempItem.类型~="鞋子"  then
		         SendMessage(user.连接id,7,"#Y/只有装备才可附加特效")
		         return true
		    elseif not TempItem.鉴定 then
		          SendMessage(user.连接id,7,"#Y/未鉴定的装备不可以附加特效")
		         return true
		    else

			      if math.random(100)<= 5 then
                      EquipmentControl:添加特效(user,TempItem,"永不磨损")
			       else
			      	SendMessage(user.连接id,7,"#y/附加永不磨损特效失败！")
			        end
			        
		    end
end,
愤怒符=function (user,Item,TempItem)
            if  TempItem.类型~="腰带"  then
                 SendMessage(user.连接id,7,"#Y/只有腰带才可附加特效")
                 return true
            elseif not TempItem.鉴定 then
                  SendMessage(user.连接id,7,"#Y/未鉴定的装备不可以附加特效")
                 return true
            else

                   EquipmentControl:添加特效(user,TempItem,"愤怒")
                      
                    
            end
end,

特技符=function (user,Item,TempItem,prsid)
            if TempItem.类型~="武器" and TempItem.类型~="衣服" and TempItem.类型~="头盔" and TempItem.类型~="项链" and TempItem.类型~="腰带" and TempItem.类型~="鞋子"  then
                 SendMessage(user.连接id,7,"#Y/只有装备才可附加特效")
                 return true
            elseif not TempItem.鉴定 then
                  SendMessage(user.连接id,7,"#Y/未鉴定的装备不可以附加特效")
                 return true
            else

             local 特技名称={"破血狂攻","弱点击破","凝气诀","凝神诀","冥王爆杀","慈航普渡","诅咒之伤","气疗术","命疗术","气归术","命归术",
             "水清诀","冰清诀","玉清诀","晶清诀","四海升平","破血狂攻","罗汉金钟","圣灵之甲","魔兽之印","野兽之力","放下屠刀","太极护法","光辉之甲","流云诀","笑里藏刀","破碎无双"}
               TempItem.特技=特技名称[math.random(1,#特技名称)]
               SendMessage(user.连接id,7,"#Y/你的装备附加了"..TempItem.特技)
            end
end,
陨铁=function (user,Item,TempItem,PitchOn,Text)
		if  TempItem.类型 ~= "武器" and  TempItem.类型 ~= "女衣"  and  TempItem.类型 ~= "元身" and  TempItem.类型 ~= "男衣" and  TempItem.类型 ~= "衣服" and TempItem.类型 ~= "鞋子" and  TempItem.类型 ~= "腰带" and  TempItem.类型 ~= "项链" and  TempItem.类型 ~= "头盔" then
			SendMessage(user.连接id, 7, "#y/只有装备和元身才可以幻化")
         return true
		 elseif not TempItem.鉴定 and TempItem.类型 ~= "元身" then
			SendMessage(user.连接id, 7, "#y/未鉴定的装备不能幻化")
			return true
		elseif TempItem.等级 <150 and TempItem.类型 ~= "元身"  then
            SendMessage(user.连接id, 7, "#y/只有150级以上的装备才可以幻化")
            return true
        elseif  TempItem.制造者 and  TempItem.制造者=="新手礼包" then
              SendMessage(user.连接id, 7, "#y/新手准备不能幻化")
              return true
		else

       
         if TempItem.类型 == "元身" then
            if  user.角色.当前体力<20 then
                SendMessage(user.连接id, 7, "#y/你的体力不足20")
                return true
            end
            local msNumber  = 分割文本(Text,"!") 
            table.print(msNumber)
            RoleControl:扣除体力(user,20)
            TempItem.特效=nil
            TempItem.特技=nil 
            if TempItem.幻化次数 == nil then 
                 TempItem.幻化次数 =0
            end 
            local 基础几率= 0
              if math.random(100) > math.max(100-math.floor(TempItem.幻化次数*0.5)+基础几率,40)   then
                      user.物品[user.角色.道具["包裹"][PitchOn]]=nil
                        user.角色.道具["包裹"][PitchOn]=nil
                        SendMessage(user.连接id, 7, "#y/幻化失败，你的装备一溜烟消失了")
                        return true
              end
              TempItem.幻化次数 =TempItem.幻化次数 +1
                if TempItem.种类=="爪" or TempItem.种类=="法杖" or TempItem.种类=="鞭"  or TempItem.种类=="双" or TempItem.种类=="环" or TempItem.种类=="扇" or TempItem.种类=="伞" or TempItem.种类=="枪" or TempItem.种类=="飘" or TempItem.种类=="棒" or TempItem.种类=="巨剑" or TempItem.种类=="剑" or TempItem.种类=="弓弩" or TempItem.种类=="斧" or TempItem.种类=="灯笼" or TempItem.种类=="刀" or TempItem.种类=="锤" or TempItem.种类=="宝珠" then
                  TempItem.伤害=math.random(math.floor((16*30+10)*1.15),math.floor((16*30+10)*1.3))
                  TempItem.命中=math.random(math.floor((16*35+10)*1.15),math.floor((16*35+10)*1.3))
                        if math.random(100) <= 50 then
                       TempItem.属性={[1]={全局变量.基础属性[math.random(1,#全局变量.基础属性)],math.random(2)}}
                        if math.random(100) <= 20 then
                          TempItem.属性[2]={全局变量.基础属性[math.random(1,#全局变量.基础属性)],math.random(2)}
                        end
                      end
                  elseif TempItem.种类=="衣服" or TempItem.种类=="男衣" or TempItem.种类=="女衣" then
                      TempItem.防御=math.random(math.floor((16*15+25)*1.15),math.floor((16*15+25)*1.3))
                      if math.random(100) <= 50 then
                       TempItem.属性={[1]={全局变量.基础属性[math.random(1,#全局变量.基础属性)],math.random(2)}}
                        if math.random(100) <= 20 then
                          TempItem.属性[2]={全局变量.基础属性[math.random(1,#全局变量.基础属性)],math.random(2)}
                        end
                      end
            
                  elseif TempItem.种类=="项链" then
                      TempItem.灵力=math.random(math.floor((16*12+5)*1.15),math.floor((16*12+5)*1.3))
                  elseif TempItem.种类=="鞋子" then
                      TempItem.防御=math.random(math.floor((16*5+5)*1.15),math.floor((16*5+5)*1.3))
                      TempItem.敏捷=math.random(math.floor((16*3+5)*1.15),math.floor((16*3+5)*1.3))
                  elseif TempItem.种类=="腰带" then
                    TempItem.防御=math.random(math.floor((16*5+5)*1.15),math.floor((16*5+5)*1.3))
                    TempItem.气血=math.random(math.floor((16*20+10)*1.15),math.floor((16*20+10)*1.3))
                  elseif TempItem.种类=="头盔" or TempItem.种类=="发钗" then
                      TempItem.防御=math.random(math.floor((16*5+5)*1.15),math.floor((16*5+5)*1.3))
                      TempItem.魔法=math.random(math.floor((16*10+5)*1.15),math.floor((16*10+5)*1.3))
                  end
                   if math.random(100)<= 10 then
                    TempItem.特效=math.random(15)
                   end
                   if math.random(100)<=5 then
                    TempItem.特技=math.random(15)
                   end
          else
                           user.物品[user.角色.道具["包裹"][PitchOn]]=nil
                        user.角色.道具["包裹"][PitchOn]=nil
                    if math.random(100) <= 60 then
                      	if TempItem.类型 =="头盔" then
                      		if 取防具性别(TempItem.名称) == "男" then
                      			ItemControl:GiveItem(user.id,"头盔·元身")
                      		else
                      			ItemControl:GiveItem(user.id,"冠冕·元身")
                      		end
                      	elseif TempItem.类型 =="衣服" then
                      	    if 取防具性别(TempItem.名称) == "男" then
                      			ItemControl:GiveItem(user.id,"坚甲·元身")
                      		else
                      			ItemControl:GiveItem(user.id,"纱衣·元身")
                      		end
                        elseif TempItem.类型 =="项链" then
                        	ItemControl:GiveItem(user.id,"挂坠·元身")
                        elseif TempItem.类型 =="鞋子" then
                        	ItemControl:GiveItem(user.id,"鞋履·元身")
                      	elseif TempItem.类型 =="腰带" then
                      		ItemControl:GiveItem(user.id,"束带·元身")
                        elseif TempItem.类型 =="元身" then  
                            ItemControl:GiveItem(user.id,TempItem.名称)  
                      	elseif TempItem.类型 =="武器"  then
                            if 取武器类型(TempItem.名称) == "法杖" then
                            	ItemControl:GiveItem(user.id,"长杖·元身")
                            elseif 取武器类型(TempItem.名称) == "弓弩" then
                            	ItemControl:GiveItem(user.id,"弓·元身")
                            elseif 取武器类型(TempItem.名称) == "飘" then
                            	ItemControl:GiveItem(user.id,"飘带·元身")
                            elseif 取武器类型(TempItem.名称) == "棒" then
                            	ItemControl:GiveItem(user.id,"魔棒·元身")
                            elseif 取武器类型(TempItem.名称) == "环" then
                            	ItemControl:GiveItem(user.id,"双环·元身")
                            elseif 取武器类型(TempItem.名称) == "双" then
                            	ItemControl:GiveItem(user.id,"双剑·元身")
                            elseif 取武器类型(TempItem.名称) == "鞭" then
                            	ItemControl:GiveItem(user.id,"长鞭·元身")
                            elseif 取武器类型(TempItem.名称) == "爪" then
                            	ItemControl:GiveItem(user.id,"爪刺·元身")
                            else
                            	ItemControl:GiveItem(user.id,取武器类型(TempItem.名称).."·元身")
                            end
                      	end

                    else
                    	SendMessage(user.连接id, 7, "#y/幻化失败，你的装备一溜烟消失了")
                    end

                     
        		end
          end
 
end,
碎石锤=function (user,Item,TempItem)
			if TempItem.类型 ~= "武器" and  TempItem.类型 ~= "女衣" and  TempItem.类型 ~= "男衣" and  TempItem.类型 ~= "衣服" and TempItem.类型 ~= "鞋子" and  TempItem.类型 ~= "腰带" and  TempItem.类型 ~= "项链" and  TempItem.类型 ~= "头盔" then
			SendMessage(user.连接id, 7, "#y/只有装备才可以去除宝石")
			return true
			elseif #TempItem.锻造数据 <1 then
				SendMessage(user.连接id, 7, "#y/只有镶嵌了宝石的装备才可以使用")
				return true
			elseif #TempItem.锻造数据 >1 then
				SendMessage(user.连接id, 7, "#y/只有镶嵌1种宝石的装备才可以使用")
				return true
		    elseif Item.等级 < TempItem.锻造数据.等级 then
		    	SendMessage(user.连接id, 7, "#y/当前装备的锻造等级太高，请用高等级的碎石锤")

				return true
			else
              	local  宝石名称 = TempItem.锻造数据[1].名称
		         if 宝石名称=="红玛瑙" then
		            if TempItem.类型=="武器" then
		            	TempItem["命中"]=TempItem["命中"]-25*TempItem.锻造数据.等级
		            end
		          elseif 宝石名称=="太阳石" then
		            if TempItem.类型=="武器" then
		            TempItem["伤害"]=TempItem["伤害"]-8*TempItem.锻造数据.等级
		            end
		          elseif 宝石名称=="舍利子" then

		            if TempItem.类型=="项链" then
		            TempItem["灵力"]=TempItem["灵力"]-6*TempItem.锻造数据.等级
		            end
		          elseif 宝石名称=="光芒石" then
		            if TempItem.类型=="腰带" then
		            TempItem["气血"]=TempItem["气血"]-40*TempItem.锻造数据.等级
		            end
		          elseif 宝石名称=="月亮石" then
		            if TempItem.类型=="头盔" then
		            TempItem["防御"]=TempItem["防御"]-12*TempItem.锻造数据.等级
		            elseif TempItem.类型=="衣服" then
		            TempItem["防御"]=TempItem["防御"]-12*TempItem.锻造数据.等级
		            end
		          end
                 TempItem.锻造数据={}
            	 
				SendMessage(user.连接id, 7, "#y/去除宝石成功" )
		    end
end,
超级碎石锤=function (user,Item,TempItem)
			if TempItem.类型 ~= "手镯" and  TempItem.类型 ~= "戒指" and  TempItem.类型 ~= "佩饰" and  TempItem.类型 ~= "耳饰" then
			SendMessage(user.连接id, 7, "#y/只有灵饰才可以去除宝石")
				return true
			elseif TempItem.幻化等级 <1 then
				SendMessage(user.连接id, 7, "#y/只有镶嵌了星辉石的装备才可以使用")

				return true
		    elseif Item.等级 < TempItem.幻化等级 then
		    	SendMessage(user.连接id, 7, "#y/当前装备的锻造等级太高，请用高等级的碎石锤")
		    	return true
			else
		        for n=1,# TempItem.幻化属性.附加 do
		           TempItem.幻化属性.附加[n].强化= 0
		        end
				ItemControl:GiveItem(user.id,"星辉石",TempItem.幻化等级)
         		 TempItem.幻化等级= 0
            	 
				 SendMessage(user.连接id, 7, "#y/去除星辉石成功" )
			end
end,
灵饰图鉴=function (user,Item,TempItem)
		if TempItem.类型 ~= "手镯" and  TempItem.类型 ~= "戒指" and  TempItem.类型 ~= "佩饰" and  TempItem.类型 ~= "耳饰" then
			SendMessage(user.连接id, 7, "#y/只有灵饰才可以使用鉴定功能")
			return true
		elseif Item.等级 < TempItem.等级 then
            SendMessage(user.连接id, 7, "#y/Item.等级不够,无法鉴定这个灵饰")
            return true
		elseif TempItem.鉴定 then
			SendMessage(user.连接id, 7, "#y/你的这件灵饰已经鉴定过了")
			return true
		else
             TempItem.鉴定 =true

			SendMessage(user.连接id, 7, "#y/鉴定成功" )
		end
end,
灵宝图鉴=function (user,Item,TempItem)
		if  TempItem.类型 ~= "鞋子" and  TempItem.类型 ~= "腰带" and  TempItem.类型 ~= "项链" and  TempItem.类型 ~= "头盔" then
			SendMessage(user.连接id, 7, "#y/只有腰带、项链、头盔、鞋子才可以使用鉴定功能")
			return true
		elseif Item.等级 < TempItem.等级 then
            SendMessage(user.连接id, 7, "#y/Item.等级不够,无法鉴定这个装备")
            return true
		elseif TempItem.鉴定 then
			SendMessage(user.连接id, 7, "#y/你的这件装备已经鉴定过了")
			return true
		else
             TempItem.鉴定 =true
            if TempItem.特效 and TempItem.等级 >80 then
                for i=1,#TempItem.特效 do
                    if TempItem.特效[i] =="无级别限制" then
                        SendMessage(user.连接id, 2072, {标题="牛了！无级别啊",文本="鉴定至少80级装备出现特效无级别"})
                         --RoleControl:添加成就积分(user,1)
                    end
                end

            end

            if TempItem.特技  then
                SendMessage(user.连接id, 2072, {标题="我的"..TempItem.特技,文本="鉴定至少80级装备出现特技"..TempItem.特技})
                 --RoleControl:添加成就积分(user,1)
            end
			SendMessage(user.连接id, 7, "#y/鉴定成功" )
		end
end,
强化符=function (user,Item,TempItem)
		if Item.技能 == "嗜血" then
				if  TempItem.类型 ~= "项链" then
					SendMessage(user.连接id, 7, "#y/只有项链才能增加效果")
					return true
				else
                    

				   ItemControl:添加装备临时效果(TempItem,"体质", math.random(math.floor(Item.等级 * 0.2), math.floor(Item.等级 * 0.5)))
				end
		elseif Item.技能 == "神兵护法" then
			if  TempItem.类型 ~= "头盔" then
				SendMessage(user.连接id, 7, "#y/只有帽子才能增加效果")
				return true
			else
			   ItemControl:添加装备临时效果(TempItem,"命中", math.random(math.floor(Item.等级 * 1), math.floor(Item.等级 * 2)))
			end
        elseif Item.技能 == "担山赶月" then
            if  TempItem.类型 ~= "项链" then
                SendMessage(user.连接id, 7, "#y/只有项链才能增加效果")
                return true
            else
               ItemControl:添加装备临时效果(TempItem,"力量", math.random(math.floor(Item.等级 * 0.2), math.floor(Item.等级 * 0.5)))
            end
        elseif Item.技能 == "鬼斧神工" then
            if  TempItem.类型 ~= "腰带" then
                SendMessage(user.连接id, 7, "#y/只有腰带才能增加效果")
                return true
            else
               ItemControl:添加装备临时效果(TempItem,"封印命中等级", math.random(math.floor(Item.等级 * 0.2), math.floor(Item.等级 * 0.5)))
            end
        elseif Item.技能 == "幽影灵魄" then
            if  TempItem.类型 ~= "鞋子" then
                SendMessage(user.连接id, 7, "#y/只有鞋子才能增加效果")
                return true
            else
               ItemControl:添加装备临时效果(TempItem,"抵抗封印等级", math.random(math.floor(Item.等级 * 0.2), math.floor(Item.等级 * 0.5)))
            end


		elseif Item.技能 == "轻如鸿毛" then
			if  TempItem.类型 ~= "武器" then
				SendMessage(user.连接id, 7, "#y/只有武器才能增加效果")
				return true
			else
			   ItemControl:添加装备临时效果(TempItem,"魔法", math.random(math.floor(Item.等级 * 1), math.floor(Item.等级 * 2)))
			end
		elseif Item.技能 == "元阳护体" then
			if  TempItem.类型 ~= "武器" then
				SendMessage(user.连接id, 7, "#y/只有武器才能增加效果")
				return true
			else
			   ItemControl:添加装备临时效果(TempItem,"气血回复效果", math.random(math.floor(Item.等级 * 0.2), math.floor(Item.等级 * 0.5)))
			end
		elseif Item.技能 == "神木呓语" then
			if  TempItem.类型 ~= "鞋子" then
				SendMessage(user.连接id, 7, "#y/只有鞋子才能增加效果")
				return true
			else
			   ItemControl:添加装备临时效果(TempItem,"固定伤害", math.random(math.floor(Item.等级 * 0.2), math.floor(Item.等级 * 0.5)))
			end
		elseif Item.技能 == "龙附" then
			if  TempItem.类型 ~= "武器" then
				SendMessage(user.连接id, 7, "#y/只有武器才能增加效果")
				return true
			else
			   ItemControl:添加装备临时效果(TempItem,"伤害", math.random(math.floor(Item.等级 * 0.5), math.floor(Item.等级 * 1)))
			end
		elseif Item.技能 == "拈花妙指" then
			if  TempItem.类型 ~= "鞋子" then
				SendMessage(user.连接id, 7, "#y/只有鞋子才能增加效果")
				return true
			else
			   ItemControl:添加装备临时效果(TempItem,"速度", math.random(math.floor(Item.等级 * 0.2), math.floor(Item.等级 * 0.5)))
			end
		elseif Item.技能 == "尸气漫天" then
			if  TempItem.类型 ~= "头盔" then
				SendMessage(user.连接id, 7, "#y/只有头盔才能增加效果")
				return true
			else
			   ItemControl:添加装备临时效果(TempItem,"耐力", math.random(math.floor(Item.等级 * 0.2), math.floor(Item.等级 * 0.5)))
			end
		elseif Item.技能 == "神力无穷" then
			if  TempItem.类型 ~= "腰带" then
				SendMessage(user.连接id, 7, "#y/只有腰带才能增加效果")
				return true
			else
			   ItemControl:添加装备临时效果(TempItem,"愤怒", math.random(math.floor(Item.等级 * 0.1), math.floor(Item.等级 * 0.2)))
			end
		elseif Item.技能 == "盘丝舞" then
			if  TempItem.类型 ~= "武器" then
				SendMessage(user.连接id, 7, "#y/只有武器才能增加效果")
				return true
			else
			   ItemControl:添加装备临时效果(TempItem,"防御", math.random(math.floor(Item.等级 * 0.5), math.floor(Item.等级 * 1)))
			end
		elseif Item.技能 == "一气化三清" then
			if  TempItem.类型 ~= "衣服" then
				SendMessage(user.连接id, 7, "#y/只有衣服才能增加效果")
				return true
			else
			   ItemControl:添加装备临时效果(TempItem,"魔力", math.random(math.floor(Item.等级 * 0.2), math.floor(Item.等级 * 0.5)))
			end
		elseif Item.技能 == "莲华妙法" then
			if  TempItem.类型 ~= "项链" then
				SendMessage(user.连接id, 7, "#y/只有项链才能增加效果")
				return true
			else
			   ItemControl:添加装备临时效果(TempItem,"法术伤害", math.random(math.floor(Item.等级 * 0.2), math.floor(Item.等级 * 0.5)))
			end
		elseif Item.技能 == "魔王护持" then
			if  TempItem.类型 ~= "武器" then
				SendMessage(user.连接id, 7, "#y/只有武器才能增加效果")
				return true
			else
			   ItemControl:添加装备临时效果(TempItem,"气血", math.random(math.floor(Item.等级 * 1), math.floor(Item.等级 * 2)))
			end
		elseif Item.技能 == "浩然正气" then
			if  TempItem.类型 ~= "衣服" then
				SendMessage(user.连接id, 7, "#y/只有衣服才能增加效果")
				return true
			else
			   ItemControl:添加装备临时效果(TempItem,"法防", math.random(math.floor(Item.等级 * 0.2), math.floor(Item.等级 * 0.5)))
			end
		end
		SendMessage(user.连接id, 7, "#y/添加装备临时效果成功！")	
		
end,
点化石=function (user,Item,TempItem)
		if TempItem.类型~= "召唤兽装备"  then
		    SendMessage(user.连接id, 7, "#Y/只有召唤兽装备才能点化")
		     return true
        else
              TempItem.套装效果={名称=Item.技能,类型=取点化石类型(Item.技能)}
              SendMessage(user.连接id,7,"#g/"..TempItem.名称.."#y/增加了#r"..Item.技能.."#y的套装效果")
              
		end
end,

}
function ItemControl:窗口道具使用(user,Item,PitchOn,Text)
    if PitchOn==0 then
        SendMessage(user.连接id, 7, "#y/请你选中一件物品")
        return true
    elseif not user.角色.道具["包裹"][PitchOn]  or not user.物品[user.角色.道具["包裹"][PitchOn]] then
        SendMessage(user.连接id, 7, "#y/这件物品貌似不存在")
        return true
    end
	local  TempItem= user.物品[user.角色.道具["包裹"][PitchOn]]
    local   ReturnValue  =物品[Item.名称](user,Item,TempItem,PitchOn,Text)

    SendMessage(user.连接id, 3007,{格子=PitchOn,数据=user.物品[user.角色.道具["包裹"][PitchOn]]})
    return  ReturnValue
 end