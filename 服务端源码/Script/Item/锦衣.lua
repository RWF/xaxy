--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-06-21 10:03:15
--======================================================================--
function ItemControl:佩戴锦衣(id, 格子, 类型,交换格子)------------------完成--
  local 内容 = {
    格子 = tonumber(格子),
    当前类型 =类型,
    交换格子=tonumber(交换格子),
   }

 if 内容.格子>=40 then --卸下装备
      if 内容.交换格子~= nil then
      self.临时格子 = 内容.交换格子
      else
      self.临时格子=RoleControl:取可用道具格子(UserData[id],内容.当前类型)
      end
      if self.临时格子==0 then
      SendMessage(UserData[id].连接id,7,"#Y/您的"..内容.当前类型.."已经无法存储更多的道具了")
      SendMessage(UserData[id].连接id,3006,"66")
      return 0
      else
        UserData[id].角色.道具[内容.当前类型][self.临时格子]=UserData[id].角色.锦衣[内容.格子]
        UserData[id].角色.锦衣[内容.格子]=0
        RoleControl:刷新装备属性(UserData[id],true)
        SendMessage(UserData[id].连接id,3006,"66")
      end
  else
    self.道具编号=UserData[id].角色.道具[内容.当前类型][内容.格子]
    if UserData[id].物品[self.道具编号] == nil then
        SendMessage(UserData[id].连接id,7,"#Y/此道具不存在请刷新背包")
       return
    end
    local Name =UserData[id].物品[self.道具编号].名称
    self.格子序号=0
        if ItemData[Name].部位=="头盔" then
        self.格子序号=40
        elseif ItemData[Name].部位=="项链" then
        self.格子序号=41
        elseif ItemData[Name].部位=="衣服" then
        self.格子序号=42
        elseif ItemData[Name].部位=="脚印"then
        self.格子序号=43
         elseif ItemData[Name].部位=="战斗锦衣" and ItemData[Name].种类==UserData[id].角色.造型 then
        self.格子序号=44
           elseif ItemData[Name].部位=="光环"then
        self.格子序号=45
        end
          if self.格子序号==0 then
          SendMessage(UserData[id].连接id,7,"#Y/此道具无法佩戴")
          SendMessage(UserData[id].连接id,3006,"66")
          return 0
          else
              self.临时装备=UserData[id].角色.锦衣[self.格子序号]
              UserData[id].角色.锦衣[self.格子序号]=self.道具编号
                if self.临时装备~=0 then
                UserData[id].角色.道具[内容.当前类型][内容.格子]=self.临时装备
                else
                  UserData[id].角色.道具[内容.当前类型][内容.格子]=nil
                end
              RoleControl:刷新装备属性(UserData[id],true)
              SendMessage(UserData[id].连接id,3006,"66")
          end
   end
 end