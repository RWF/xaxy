--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-02-08 01:01:34
--======================================================================--

function ItemControl:仙玉抽奖(id,序号,扣除)
   self.抽奖道具={}

   ---------------------tie
   self.抽奖道具[10] = {--仙玉抽奖
    AddItem("金锭",nil,nil,nil,nil,nil,nil,{1,200}),
    AddItem("九转金丹",nil,nil,nil,nil,nil,nil,{201,250}),
    AddItem("彩果",nil,nil,nil,nil,nil,nil,{251,300}),
    AddItem("金锭",nil,nil,nil,nil,nil,nil,{301,360}),
    AddItem("仙丹",nil,nil,nil,nil,nil,nil,{361,420}),
    AddItem("金柳露",nil,nil,nil,nil,nil,nil,{421,480}),
    AddItem("藏宝图",nil,nil,nil,nil,nil,nil,{481,520}),
    AddItem("魔兽要诀",nil,nil,nil,nil,nil,nil,{961,963}),
    AddItem("金锭",nil,nil,nil,nil,nil,nil,{521,560}),
    AddItem("金锭",nil,nil,nil,nil,nil,nil,{561,610}),
    AddItem("魔兽要诀",nil,nil,nil,nil,nil,nil,{611,710}),
    AddItem("超级金柳露",nil,nil,nil,nil,nil,nil,{711,800}),
    AddItem("太阳石",math.random(5),nil,nil,nil,nil,nil,{801,850}),
    AddItem("卡片礼包",nil,nil,nil,nil,nil,nil,{998,1000}),
    AddItem("金锭",nil,nil,nil,nil,nil,nil,{901,930}),
    AddItem("金锭",nil,nil,nil,nil,nil,nil,{931,955}),
    AddItem("仙丹",nil,nil,nil,nil,nil,nil,{956,960}),
    AddItem("彩果",nil,nil,nil,nil,nil,nil,{964,975}),
    AddItem("彩果",nil,nil,nil,nil,nil,nil,{976,980}),
    AddItem("双倍经验丹",nil,nil,nil,nil,nil,nil,{981,986}),
    AddItem("颜灵果",nil,nil,nil,nil,nil,nil,{987,989}),
    AddItem("金锭",nil,nil,nil,nil,nil,nil,{990,994}),
    AddItem("仙丹",nil,nil,nil,nil,nil,nil,{995,997}),
    AddItem("彩果",nil,nil,nil,nil,nil,nil,{851,900}),

    }
    self.抽奖道具[11] = {
 ------------------------------------
    AddItem("金锭",nil,nil,nil,nil,nil,nil,{1,200}),
    AddItem("金刚石",nil,nil,nil,nil,nil,nil,{301,400}),
    AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{401,450}),
    AddItem("金柳露",nil,nil,nil,nil,nil,nil,{451,500}),
    AddItem("超级金柳露",nil,nil,nil,nil,nil,nil,{501,520}),
    AddItem("青龙石",nil,nil,nil,nil,nil,nil,{521,550}),
    AddItem("月华露",nil,nil,nil,nil,nil,nil,{551,600}),
    AddItem("彩果",nil,nil,nil,nil,nil,nil,{561,610}),
    AddItem("双倍经验丹",nil,nil,nil,nil,nil,nil,{901,960}),
    AddItem("月亮石",nil,nil,nil,nil,nil,nil,{611,650}),
    AddItem("萝卜王",nil,nil,nil,nil,nil,nil,{651,661}),
    AddItem("定魂珠",nil,nil,nil,nil,nil,nil,{662,700}),
    AddItem("红玫瑰",nil,nil,nil,nil,nil,nil,{701,731}),
    AddItem("卡片礼包",nil,nil,nil,nil,nil,nil,{732,760}),
    AddItem("九转金丹",nil,nil,nil,nil,nil,nil,{761,780}),
    AddItem("仙丹",nil,nil,nil,nil,nil,nil,{781,790}),
    AddItem("金锭",nil,nil,nil,nil,nil,nil,{791,800}),
    AddItem("金锭",nil,nil,nil,nil,nil,nil,{801,830}),
    AddItem("仙丹",nil,nil,nil,nil,nil,nil,{831,900}),
    AddItem("魔兽要诀",nil,nil,nil,nil,nil,nil,{961,981}),
    AddItem("魔兽要诀",nil,nil,nil,nil,nil,nil,{201,300}),
    AddItem("红玛瑙",math.random(5),nil,nil,nil,nil,nil,{988,990}),
    AddItem("仙丹",nil,nil,nil,nil,nil,nil,{991,1000}),
    AddItem("仙丹",nil,nil,nil,nil,nil,nil,{201,300}),
    }
    self.抽奖道具[12] = {
  ----------------------------------

  AddItem("海马",nil,nil,nil,nil,nil,nil,{561,610}),
  AddItem("月亮石",math.random(5),nil,nil,nil,nil,nil,{611,650}),
  AddItem("金锭",nil,nil,nil,nil,nil,nil,{651,661}),
  AddItem("定魂珠",nil,nil,nil,nil,nil,nil,{662,700}),
  AddItem("金刚石",nil,nil,nil,nil,nil,nil,{701,731}),
  AddItem("九转金丹",nil,nil,nil,nil,nil,nil,{732,760}),
  AddItem("金锭",nil,nil,nil,nil,nil,nil,{1,200}),
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{201,300}),
  AddItem("红玛瑙",math.random(5),nil,nil,nil,nil,nil,{301,400}),
  AddItem("颜灵果",nil,nil,nil,nil,nil,nil,{401,450}),
  AddItem("金柳露",nil,nil,nil,nil,nil,nil,{451,500}),
  AddItem("超级金柳露",nil,nil,nil,nil,nil,nil,{501,520}),
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{551,600}),
  AddItem("魔兽要诀",nil,nil,nil,nil,nil,nil,{961,981}),
  AddItem("魔兽要诀",nil,nil,nil,nil,nil,nil,{775,780}),
  AddItem("萝卜王",nil,nil,nil,nil,nil,nil,{781,790}),
  AddItem("金锭",nil,nil,nil,nil,nil,nil,{791,800}),
  AddItem("卡片礼包",nil,nil,nil,nil,nil,nil,{801,830}),
  AddItem("龙鳞",nil,nil,nil,nil,nil,nil,{831,900}),
  AddItem("双倍经验丹",nil,nil,nil,nil,nil,nil,{901,960}),
  AddItem("彩果",nil,nil,nil,nil,nil,nil,{561,610}),
  AddItem("玄武石",nil,nil,nil,nil,nil,nil,{982,990}),
  AddItem("白虎石",nil,nil,nil,nil,nil,nil,{991,1000}),
  AddItem("青龙石",nil,nil,nil,nil,nil,nil,{521,550}),
    }
    self.抽奖道具[20] = {
  ----------------------------------------------
  AddItem("金锭",nil,nil,nil,nil,nil,nil,{1,200}),
  AddItem("九转金丹",nil,nil,nil,nil,nil,nil,{301,400}),
  AddItem("修炼果",nil,nil,nil,nil,nil,nil,{401,450}),
  AddItem("龙鳞",nil,nil,nil,nil,nil,nil,{451,500}),
  AddItem("红玛瑙",nil,nil,nil,nil,nil,nil,{501,520}),
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{521,550}),
  AddItem("双倍经验丹",nil,nil,nil,nil,nil,nil,{551,600}),
  AddItem("彩果",nil,nil,nil,nil,nil,nil,{561,610}),
  AddItem("魔兽要诀",nil,nil,nil,nil,nil,nil,{982,990}),
  AddItem("月亮石",nil,nil,nil,nil,nil,nil,{611,650}),
  AddItem("召唤兽内丹",nil,nil,nil,nil,nil,nil,{651,661}),
  AddItem("定魂珠",nil,nil,nil,nil,nil,nil,{662,700}),
  AddItem("青龙石",nil,nil,nil,nil,nil,nil,{701,731}),
  AddItem("朱雀石",nil,nil,nil,nil,nil,nil,{732,760}),
  AddItem("九转金丹",nil,nil,nil,nil,nil,nil,{761,780}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{781,790}),
  AddItem("特赦令牌",nil,nil,nil,nil,nil,nil,{901,960}),
  AddItem("炼兽真经",nil,nil,nil,nil,nil,nil,{791,800}),
  AddItem("卡片礼包",nil,nil,nil,nil,nil,nil,{801,830}),
  AddItem("龙鳞",nil,nil,nil,nil,nil,nil,{831,900}),
  AddItem("高级魔兽要诀",取高级兽诀名称(),nil,nil,nil,nil,nil,{961,981}),
  AddItem("易经丹",nil,nil,nil,nil,nil,nil,{991,998}),
  AddItem("萝卜王",nil,nil,nil,nil,nil,nil,{999,1000}),
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{202,300}),
    }
    self.抽奖道具[21] = {
  ------------------------------------------------
  AddItem("九转金丹",nil,nil,nil,nil,nil,nil,{732,760}),
  AddItem("九转金丹",nil,nil,nil,nil,nil,nil,{761,780}),
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{1,200}),
  AddItem("金锭",nil,nil,nil,nil,nil,nil,{781,790}),
  AddItem("炼兽真经",nil,nil,nil,nil,nil,nil,{791,800}),
  AddItem("卡片礼包",nil,nil,nil,nil,nil,nil,{801,830}),
  AddItem("龙鳞",nil,nil,nil,nil,nil,nil,{831,900}),
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{901,960}),
  AddItem("易经丹",nil,nil,nil,nil,nil,nil,{991,998}),
  AddItem("特赦令牌",nil,nil,nil,nil,nil,nil,{999,1000}),
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{201,300}),
  AddItem("红玛瑙",nil,nil,nil,nil,nil,nil,{301,400}),
  AddItem("双倍经验丹",nil,nil,nil,nil,nil,nil,{401,450}),
  AddItem("高级魔兽要诀",取高级兽诀名称(),nil,nil,nil,nil,nil,{961,981}),
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{451,500}),
  AddItem("月亮石",nil,nil,nil,nil,nil,nil,{501,520}),
  AddItem("青龙石",nil,nil,nil,nil,nil,nil,{521,550}),
  AddItem("彩果",nil,nil,nil,nil,nil,nil,{561,610}),
  AddItem("月华露",nil,nil,nil,nil,nil,nil,{982,990}),
  AddItem("月亮石",nil,nil,nil,nil,nil,nil,{611,650}),
  AddItem("召唤兽内丹",nil,nil,nil,nil,nil,nil,{651,661}),
  AddItem("定魂珠",nil,nil,nil,nil,nil,nil,{662,700}),
  AddItem("珍珠",nil,nil,nil,nil,nil,nil,{701,731}),
  AddItem("修炼果",nil,nil,nil,nil,nil,nil,{551,600}),
    }
    self.抽奖道具[22] = {
  -------------------------------------
  AddItem("修炼果",nil,nil,nil,nil,nil,nil,{732,760}),
  AddItem("九转金丹",nil,nil,nil,nil,nil,nil,{761,780}),
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{1,200}),
  AddItem("金锭",nil,nil,nil,nil,nil,nil,{781,790}),
  AddItem("炼兽真经",nil,nil,nil,nil,nil,nil,{791,800}),
  AddItem("白虎石",nil,nil,nil,nil,nil,nil,{801,830}),
  AddItem("龙鳞",nil,nil,nil,nil,nil,nil,{831,900}),
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{201,300}),
  AddItem("红玛瑙",nil,nil,nil,nil,nil,nil,{301,400}),
  AddItem("魔兽要诀",nil,nil,nil,nil,nil,nil,{401,450}),
  AddItem("召唤兽内丹",nil,nil,nil,nil,nil,nil,{961,981}),
  AddItem("彩果",nil,nil,nil,nil,nil,nil,{451,500}),
  AddItem("卡片礼包",nil,nil,nil,nil,nil,nil,{501,520}),
  AddItem("青龙石",nil,nil,nil,nil,nil,nil,{521,550}),
  AddItem("彩果",nil,nil,nil,nil,nil,nil,{551,600}),
  AddItem("双倍经验丹",nil,nil,nil,nil,nil,nil,{901,960}),
  AddItem("易经丹",nil,nil,nil,nil,nil,nil,{991,998}),
  AddItem("特赦令牌",nil,nil,nil,nil,nil,"奖励",{999,1000}),
  AddItem("特赦令牌",nil,nil,nil,nil,nil,nil,{982,990}),
  AddItem("月亮石",nil,nil,nil,nil,nil,nil,{611,650}),
  AddItem("召唤兽内丹",nil,nil,nil,nil,nil,nil,{651,661}),
  AddItem("定魂珠",nil,nil,nil,nil,nil,nil,{662,700}),
  AddItem("神兜兜",math.random(6, 16)*10,nil,nil,nil,nil,nil,{701,731}),
  AddItem("彩果",nil,nil,nil,nil,nil,nil,{561,610}),
    }
    self.抽奖道具[30] = {
  -------------------------------------
  AddItem("金锭",nil,nil,nil,nil,nil,nil,{501,520}),
  AddItem("青龙石",nil,nil,nil,nil,nil,nil,{521,550}),
  AddItem("特赦令牌",nil,nil,nil,nil,nil,nil,{551,600}),
  AddItem("双倍经验丹",nil,nil,nil,nil,nil,nil,{901,960}),
  AddItem("易经丹",nil,nil,nil,nil,nil,nil,{991,998}),
  AddItem("修炼果",nil,nil,nil,nil,nil,nil,{732,760}),
  AddItem("九转金丹",nil,nil,nil,nil,nil,nil,{761,780}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{781,790}),
  AddItem("高级魔兽要诀",取高级兽诀名称(),nil,nil,nil,nil,nil,{791,800}),
  AddItem("卡片礼包",nil,nil,nil,nil,nil,nil,{801,830}),
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{1,200}),
  AddItem("高级召唤兽内丹",nil,nil,nil,nil,nil,nil,{961,981}),
  AddItem("龙鳞",nil,nil,nil,nil,nil,nil,{831,900}),
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{201,300}),
  AddItem("红玛瑙",nil,nil,nil,nil,nil,nil,{301,400}),
  AddItem("特殊魔兽要诀",取特殊兽诀名称(),nil,nil,nil,nil,nil,{999,1000}),
  AddItem("玄天残卷·上卷",nil,nil,nil,nil,nil,nil,{451,500}),
  AddItem("玄天残卷·下卷",nil,nil,nil,nil,nil,nil,{401,450}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{561,610}),
  AddItem("玄天残卷·中卷",nil,nil,nil,nil,nil,nil,{982,990}),
  AddItem("月亮石",nil,nil,nil,nil,nil,nil,{611,650}),
  AddItem("高级召唤兽内丹",nil,nil,nil,nil,nil,nil,{651,661}),
  AddItem("水墨游龙",nil,nil,nil,nil,nil,nil,{662,700}),
  AddItem("齐天大圣",nil,nil,nil,nil,nil,nil,{701,731}),
    }
    self.抽奖道具[31] = {
  -----------------------------------------------------
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{1,200}),
  AddItem("金锭",nil,nil,nil,nil,nil,nil,{451,500}),
  AddItem("九转金丹",nil,nil,nil,nil,nil,nil,{761,780}),
  AddItem("元宝足迹",nil,nil,nil,nil,nil,nil,{732,760}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{781,790}),
  AddItem("齐天大圣",nil,nil,nil,nil,nil,nil,{791,800}),
  AddItem("卡片礼包",nil,nil,nil,nil,nil,nil,{801,830}),
  AddItem("红玛瑙",nil,nil,nil,nil,nil,nil,{301,400}),
  AddItem("高级召唤兽内丹",nil,nil,nil,nil,nil,nil,{961,981}),
  AddItem("特殊魔兽要诀",取特殊兽诀名称(),nil,nil,nil,nil,nil,{999,1000}),
  AddItem("特赦令牌",nil,nil,nil,nil,nil,nil,{501,520}),
  AddItem("青龙石",nil,nil,nil,nil,nil,nil,{521,550}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{551,600}),
  AddItem("双倍经验丹",nil,nil,nil,nil,nil,nil,{401,450}),
  AddItem("易经丹",nil,nil,nil,nil,nil,nil,{991,998}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{999,1000}),--神兜兜
  AddItem("彩果",nil,nil,nil,nil,nil,nil,{561,610}),
  AddItem("玄天残卷·上卷",nil,nil,nil,nil,nil,nil,{982,990}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{831,900}),
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{201,300}),
  AddItem("月亮石",nil,nil,nil,nil,nil,nil,{611,650}),
  AddItem("高级召唤兽内丹",nil,nil,nil,nil,nil,nil,{651,661}),
  AddItem("定魂珠",nil,nil,nil,nil,nil,nil,{662,700}),
  AddItem("魔兽要诀",nil,nil,nil,nil,nil,nil,{701,731}),
}
    self.抽奖道具[32] = {
  -----------------------------------------------------
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{1,200}),
  AddItem("修炼果",nil,nil,nil,nil,nil,nil,{451,500}),
  AddItem("九转金丹",nil,nil,nil,nil,nil,nil,{761,780}),
  AddItem("元宝足迹",nil,nil,nil,nil,nil,nil,{732,760}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{781,790}),
  AddItem("齐天大圣",nil,nil,nil,nil,nil,nil,{791,800}),
  AddItem("卡片礼包",nil,nil,nil,nil,nil,nil,{801,830}),
  AddItem("红玛瑙",nil,nil,nil,nil,nil,nil,{301,400}),
  AddItem("高级召唤兽内丹",nil,nil,nil,nil,nil,nil,{961,981}),
  AddItem("玄武石",nil,nil,nil,nil,nil,nil,{401,450}),
  AddItem("玄天残卷·下卷",nil,nil,nil,nil,nil,nil,{501,520}),
  AddItem("特赦令牌",nil,nil,nil,nil,nil,nil,{521,550}),
  AddItem("萝卜王",nil,nil,nil,nil,nil,nil,{551,600}),
  AddItem("双倍经验丹",nil,nil,nil,nil,nil,nil,{901,960}),
  AddItem("易经丹",nil,nil,nil,nil,nil,nil,{991,998}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{999,1000}),
  AddItem("彩果",nil,nil,nil,nil,nil,nil,{561,610}),
  AddItem("玄天残卷·上卷",nil,nil,nil,nil,nil,nil,{982,990}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{831,900}),
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{201,300}),
  AddItem("月亮石",nil,nil,nil,nil,nil,nil,{611,650}),
  AddItem("高级召唤兽内丹",nil,nil,nil,nil,nil,nil,{651,661}),
  AddItem("定魂珠",nil,nil,nil,nil,nil,nil,{662,700}),
  AddItem("魔兽要诀",nil,nil,nil,nil,nil,nil,{701,731}),
}
    self.抽奖道具[40] = {
  -----------------------------------------------------
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{1,200}),
  AddItem("金锭",nil,nil,nil,nil,nil,nil,{451,500}),
  AddItem("九转金丹",nil,nil,nil,nil,nil,nil,{761,780}),
  AddItem("元宝足迹",nil,nil,nil,nil,nil,nil,{732,760}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{781,790}),
  AddItem("齐天大圣",nil,nil,nil,nil,nil,nil,{791,800}),
  AddItem("卡片礼包",nil,nil,nil,nil,nil,nil,{801,830}),
  AddItem("红玛瑙",nil,nil,nil,nil,nil,nil,{301,400}),
  AddItem("高级召唤兽内丹",nil,nil,nil,nil,nil,nil,{961,981}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{401,450}),
  AddItem("摇钱树树苗",nil,nil,nil,nil,nil,nil,{501,520}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{521,550}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{551,600}),
  AddItem("超级净瓶玉露",nil,nil,nil,nil,nil,nil,{901,960}),
  AddItem("易经丹",nil,nil,nil,nil,nil,nil,{991,998}),
  AddItem("130无级别单件",nil,nil,nil,nil,nil,nil,{999,1000}),
  AddItem("仙玉卡片",nil,nil,nil,nil,nil,nil,{561,610}),
  AddItem("玄天残卷·上卷",nil,nil,nil,nil,nil,nil,{982,990}),
  AddItem("仙玉卡片",nil,nil,nil,nil,nil,nil,{831,900}),
  AddItem("法宝任务书",nil,nil,nil,nil,nil,nil,{201,300}),
  AddItem("金银宝盒",nil,nil,nil,nil,nil,nil,{611,650}),
  AddItem("高级召唤兽内丹",nil,nil,nil,nil,nil,nil,{651,661}),
  AddItem("定魂珠",nil,nil,nil,nil,nil,nil,{662,700}),
  AddItem("魔兽要诀",nil,nil,nil,nil,nil,nil,{701,731}),
}

    self.抽奖道具[41] = {
  -----------------------------------------------------
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{1,200}),
  AddItem("金柳露",nil,nil,nil,nil,nil,nil,{451,500}),
  AddItem("九转金丹",nil,nil,nil,nil,nil,nil,{761,780}),
  AddItem("元宝足迹",nil,nil,nil,nil,nil,nil,{732,760}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{781,790}),
  AddItem("齐天大圣",nil,nil,nil,nil,nil,nil,{791,800}),
  AddItem("卡片礼包",nil,nil,nil,nil,nil,nil,{801,830}),
  AddItem("红玛瑙",nil,nil,nil,nil,nil,nil,{301,400}),
  AddItem("高级召唤兽内丹",nil,nil,nil,nil,nil,nil,{961,981}),
  AddItem("星辉石",nil,nil,nil,nil,nil,nil,{401,450}),
  AddItem("净瓶玉露",nil,nil,nil,nil,nil,nil,{501,520}),
  AddItem("法宝任务书",nil,nil,nil,nil,nil,nil,{521,550}),
  AddItem("金银宝盒",nil,nil,nil,nil,nil,nil,{551,600}),
  AddItem("特殊魔兽要诀",取特殊兽诀名称(),nil,nil,nil,nil,nil,{999,1000}),
  AddItem("易经丹",nil,nil,nil,nil,nil,nil,{991,998}),
  AddItem("130无级别单件",nil,nil,nil,nil,nil,"奖励",{901,960}),
  AddItem("仙玉卡片",nil,nil,nil,nil,nil,nil,{561,610}),
  AddItem("玄天残卷·上卷",nil,nil,nil,nil,nil,nil,{982,990}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{831,900}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{201,300}),
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{611,650}),
  AddItem("高级召唤兽内丹",nil,nil,nil,nil,nil,nil,{651,661}),
  AddItem("定魂珠",nil,nil,nil,nil,nil,nil,{662,700}),
  AddItem("水墨游龙",nil,nil,nil,nil,nil,nil,{701,731}),
}

    self.抽奖道具[42] = {
  -----------------------------------------------------
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{1,200}),
  AddItem("冰雪玉兔",nil,nil,nil,nil,nil,nil,{451,500}),
  AddItem("九转金丹",nil,nil,nil,nil,nil,nil,{761,780}),
  AddItem("元宝足迹",nil,nil,nil,nil,nil,nil,{732,760}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{781,790}),
  AddItem("齐天大圣",nil,nil,nil,nil,nil,nil,{791,800}),
  AddItem("卡片礼包",nil,nil,nil,nil,nil,nil,{801,830}),
  AddItem("红玛瑙",nil,nil,nil,nil,nil,nil,{301,400}),
  AddItem("高级召唤兽内丹",nil,nil,nil,nil,nil,nil,{961,981}),
  AddItem("130无级别单件",nil,nil,nil,nil,nil,nil,{999,1000}),
  AddItem("净瓶玉露",nil,nil,nil,nil,nil,nil,{501,520}),
  AddItem("仙玉卡片",nil,nil,nil,nil,nil,nil,{521,550}),
  AddItem("金银宝盒",nil,nil,nil,nil,nil,nil,{551,600}),
  AddItem("神兜兜",nil,nil,nil,nil,nil,nil,{901,960}),
  AddItem("易经丹",nil,nil,nil,nil,nil,nil,{991,998}),
  AddItem("法宝任务书",nil,nil,nil,nil,nil,nil,{401,450}),
  AddItem("彩果",nil,nil,nil,nil,nil,nil,{561,610}),
  AddItem("玄天残卷·上卷",nil,nil,nil,nil,nil,nil,{982,990}),
  AddItem("金锭",nil,nil,nil,nil,nil,nil,{831,900}),
  AddItem("仙丹",nil,nil,nil,nil,nil,nil,{201,300}),
  AddItem("月亮石",nil,nil,nil,nil,nil,nil,{611,650}),
  AddItem("高级召唤兽内丹",nil,nil,nil,nil,nil,nil,{651,661}),
  AddItem("定魂珠",nil,nil,nil,nil,nil,nil,{662,700}),
  AddItem("法宝任务书",nil,nil,nil,nil,nil,nil,{701,731}),
}
 self.发送消息={}
 self.抽奖道具[序号].组号=序号
 self.抽奖道具[序号].仙玉=UserData[id].仙玉
 self.抽奖道具[序号].扣除=扣除
 self.抽奖道具[序号].抽奖=UserData[id].抽奖
 SendMessage(UserData[id].连接id,20044,self.抽奖道具[序号])
end


function ItemControl:仙玉抽奖处理(id,组号)------------------------
    local 扣除 =0
     if 组号 >= 10 and 组号 <20 then
         扣除 =300
     elseif 组号 >= 20 and 组号 <30 then
     扣除 =600
     elseif 组号 >= 30 and 组号 <40 then
     扣除 =1500
     elseif 组号 >= 40 and 组号 <50 then
     扣除 =2000
     end
		if RoleControl:取可用格子数量(UserData[id],"包裹") < 1 then
			SendMessage(UserData[id].连接id, 7, "#y/请先预留出1个道具空间")
			return 0
	    elseif self.抽奖道具[组号] ==nil  then
			封禁账号(UserData[id],"仙玉抽奖")
	    	return 0
		elseif UserData[id].抽奖 or RoleControl:扣除仙玉(UserData[id],扣除,"仙玉抽奖")  then

			local 随机概率 =math.random(1000)
			local 购买编号 =0
         if UserData[id].管理模式 then
         	   购买编号 = math.random(4,23)
         else
	         for i=1,24 do
             
	              if  随机概率 >= self.抽奖道具[组号][i].概率[1] and 随机概率 <= self.抽奖道具[组号][i].概率[2]  then
	          	    购买编号 = i
	          	end
	         end
	      end
           if 购买编号 ~=0 then
           	 if self.抽奖道具[组号][购买编号].名称 == "金锭" then
           	 	 local 获得金钱 = 10000 *math.random(50,100)
                RoleControl:添加银子(UserData[id],获得金钱,"仙玉抽奖")
           	 	SendMessage(UserData[id].连接id,20045,{名称=获得金钱.."金钱",编号=购买编号,抽奖=UserData[id].抽奖})
           	 elseif self.抽奖道具[组号][购买编号].名称 == "仙丹" then
           	 	 local 获得经验 =  UserData[id].角色.等级 *UserData[id].角色.等级*math.random(50,100)
           	 	 RoleControl:添加经验(UserData[id],获得经验,"仙玉抽奖")
           	 	SendMessage(UserData[id].连接id,20045,{名称=(获得经验*2).."经验",编号=购买编号,抽奖=UserData[id].抽奖})
           	 else
	 			local 道具编号 = self:取道具编号(id)
	 			local 临时格子 = RoleControl:取可用道具格子(UserData[id],"包裹")
				UserData[id].角色.道具.包裹[临时格子] = 道具编号
				UserData[id].物品[道具编号] = table.loadstring(table.tostring(self.抽奖道具[组号][购买编号]))
				SendMessage(UserData[id].连接id,20045,{名称=UserData[id].物品[道具编号].名称,编号=购买编号,抽奖=UserData[id].抽奖})

			 end
		  else
				SendMessage(UserData[id].连接id, 7, "#y/错误封包数据")
	    		return 0
			end
              UserData[id].抽奖 =nil

		end

end