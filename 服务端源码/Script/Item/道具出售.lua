--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-01-20 04:29:52
--======================================================================--
function ItemControl:出售全部道具(id,类型)------------------------------完成--

    -- local  format = 类型=="行囊"and 20 or 80

  for i=1,20 do
    if UserData[id].角色.道具[类型][i] ~=nil then
        self.临时id1=UserData[id].角色.道具[类型][i]
        if UserData[id].物品[self.临时id1] then
            local 数量
            if UserData[id].物品[self.临时id1].数量 ==nil then
            数量 = 1
            else
            数量 =UserData[id].物品[self.临时id1].数量
            end
            self:出售道具处理(id,i,类型,数量)
        end
    end
  end
 end
function ItemControl:出售道具处理(id, 格子, 类型,数量)------------------完成--

  local 数据 = {
		格子 = 格子,
		类型 = 类型,
		数量=tonumber(数量)}
  self.临时id1=UserData[id].角色.道具[数据.类型][数据.格子]
 if UserData[id].角色.道具[数据.类型][数据.格子]==nil then
   SendMessage(UserData[id].连接id,7,"#Y/你没有这样的道具")
   return 0
  elseif UserData[id].物品[self.临时id1].加锁 then
       SendMessage(UserData[id].连接id,7,"#Y/加锁的物品不能出售")
   return 0
  elseif UserData[id].物品[self.临时id1].Time then
       SendMessage(UserData[id].连接id,7,"#Y/有效期的物品不能出售")
      return 0
   end
 self.收购价格=0
 self.临时类型=UserData[id].物品[self.临时id1].类型
 self.临时名称=UserData[id].物品[self.临时id1].名称
 if (self.临时类型=="武器" or self.临时类型=="衣服" or self.临时类型=="头盔" or self.临时类型=="项链" or self.临时类型=="腰带" or self.临时类型=="鞋子" or self.临时类型=="手镯" or self.临时类型=="耳饰" or self.临时类型=="戒指" or self.临时类型=="佩饰" ) then
   self.临时差价=UserData[id].物品[self.临时id1].等级*40
   if  UserData[id].物品[self.临时id1].等级 > 40 then
     self.临时差价=UserData[id].物品[self.临时id1].等级*1000
     if self.临时类型=="武器" then
        self.收购价格=(UserData[id].物品[self.临时id1].命中+UserData[id].物品[self.临时id1].伤害)*5
      elseif self.临时类型=="衣服" then
       self.收购价格=(UserData[id].物品[self.临时id1].防御)*5
      elseif self.临时类型=="项链" then
       self.收购价格=(UserData[id].物品[self.临时id1].灵力)*30
      elseif self.临时类型=="腰带" then
       self.收购价格=(UserData[id].物品[self.临时id1].防御+UserData[id].物品[self.临时id1].气血)*12
      elseif self.临时类型=="鞋子" then
       self.收购价格=(UserData[id].物品[self.临时id1].敏捷+UserData[id].物品[self.临时id1].防御)*29
      elseif self.临时类型=="头盔" then
       self.收购价格=(UserData[id].物品[self.临时id1].魔法+UserData[id].物品[self.临时id1].防御)*22
      elseif self.临时类型=="手镯" then
           self.收购价格=20000
      elseif self.临时类型=="耳饰" then
           self.收购价格=20000
      elseif self.临时类型=="戒指" then
           self.收购价格=20000
      elseif self.临时类型=="佩饰" then
           self.收购价格=20000
      end
   end
          self.收购价格= math.floor(self.收购价格+self.临时差价)


    if UserData[id].物品[self.临时id1].特效=="珍宝" then
    self.收购价格=self.收购价格*10
    end
 end

 if self.收购价格==0 then
    self.收购价格 =ItemData[self.临时名称].出售 or 0
 end



 if self.收购价格==0 or  UserData[id].物品[self.临时id1].专用  then
   SendMessage(UserData[id].连接id,7,"#Y/这样的道具无法被出售")
   return 0
  else
      数据.数量 =math.floor(数据.数量) 
    if 数据.数量<1 then
      return
    end
    if 数据.数量> 1 then
       if UserData[id].物品[self.临时id1].数量  then
         if 数据.数量 > UserData[id].物品[self.临时id1].数量 then
          数据.数量 = UserData[id].物品[self.临时id1].数量 
         end
       else 
          数据.数量=1
       end
    end
      
     self.收购价格=self.收购价格* 数据.数量
     RoleControl:添加银子(UserData[id],self.收购价格,"出售".. UserData[id].物品[self.临时id1].名称)
     if self.临时类型 == "古董" then
      RoleControl:添加经验(UserData[id],ItemData[self.临时名称].价格, "古董")
     end
    if UserData[id].物品[self.临时id1].数量 ~=nil and UserData[id].物品[self.临时id1].数量>数据.数量 then
      UserData[id].物品[self.临时id1].数量=UserData[id].物品[self.临时id1].数量 - 数据.数量

    else
      
        UserData[id].物品[self.临时id1]=nil
        UserData[id].角色.道具[数据.类型][数据.格子]=nil
    end
    SendMessage(UserData[id].连接id,3006,"66")
    self:索要出售道具(id,数据.类型)
   end
 end

function ItemControl:索要出售道具(id,内容)------------------------------完成--
 self.发送信息={}
  for n=1,20 do
     if UserData[id].角色.道具[内容][n]~=nil and UserData[id].物品[UserData[id].角色.道具[内容][n]]~=nil then
         self.发送信息[n]=UserData[id].物品[UserData[id].角色.道具[内容][n]]
       end
     end
 self.发送信息.类型=内容
 self.发送信息.银两=UserData[id].角色.道具.货币.银子
 self.发送信息.体力=UserData[id].角色.当前体力
 SendMessage(UserData[id].连接id,3074,self.发送信息)
 end
