--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-12-26 22:46:23
--======================================================================--
local TeamControl = class()
function TeamControl:初始化()
	队伍数据 = {}
end
function TeamControl:数据处理(data)
	self.临时数据 = data
	local id = self.临时数据.数字id
	内容 = self.临时数据.文本
	local 序号 = self.临时数据.序号
	local 参数=self.临时数据.参数
	if 序号 == 1 then
		self:队伍状态检查(id)
	elseif 序号 == 2 then
		self:创建队伍(id)
	elseif 序号 == 3 then
		SendMessage(UserData[id].连接id, 5008, UserData[id].角色.阵法)
	elseif 序号 == 5 then
		self:改变阵型(id, self.临时数据.文本)
	elseif 序号 == 6 then
		self:申请入队(id, self.临时数据.参数 + 0)
	elseif 序号 == 7 then
		if UserData[id].队伍 == 0 then
			SendMessage(UserData[id].连接id, 7, "#Y/请先加入一个队伍")
		elseif UserData[id].队长 == false then
			SendMessage(UserData[id].连接id, 7, "#Y/只有队长才能查看申请列表")
		else
			self:发送请求信息(UserData[id].队伍, 5010, UserData[id].连接id)
		end
	elseif 序号 == 9 then
		if UserData[id].队伍 == 0 then
			SendMessage(UserData[id].连接id, 7, "#Y/请先加入一个队伍")
		elseif UserData[id].队长 == false then
			SendMessage(UserData[id].连接id, 7, "#Y/只有队长才执行此操作")
		else
			self:拒绝玩家申请(UserData[id].队伍, id, self.临时数据.参数 + 0)
		end
	elseif 序号 == 8 then
		if UserData[id].队伍 == 0 then
			SendMessage(UserData[id].连接id, 7, "#Y/请先加入一个队伍")
		elseif UserData[id].队长 == false then
			SendMessage(UserData[id].连接id, 7, "#Y/只有队长才执行此操作")
		else
			self:同意玩家申请(UserData[id].队伍, id, self.临时数据.参数 + 0)
		end
	elseif 序号 == 10 then
		if UserData[id].队伍 == 0 then
			SendMessage(UserData[id].连接id, 7, "#Y/请先加入一个队伍")
		elseif UserData[id].队长 == false then
			SendMessage(UserData[id].连接id, 7, "#Y/只有队长才执行此操作")
		else
			self:踢出玩家(UserData[id].队伍, id, self.临时数据.参数 + 0)
		end
	elseif 序号 == 11 then
		if UserData[id].队伍 == 0 then
			SendMessage(UserData[id].连接id, 7, "#Y/请先加入一个队伍")
		else
			self:离开队伍(UserData[id].队伍, id)
		end
	elseif 序号 == 12 then
		self.临时数据.参数 = self.临时数据.参数 + 0

		if UserData[id].队伍 == 0 then
			SendMessage(UserData[id].连接id, 7, "#Y/请先加入一个队伍")
			return 0
		elseif UserData[id].队长 == false then
			SendMessage(UserData[id].连接id, 7, "#Y/只有队长才执行此操作")
			return 0
		elseif UserData[队伍数据[UserData[id].队伍].队员数据[self.临时数据.参数]] ==nil then
						SendMessage(UserData[id].连接id, 7, "#Y/队员数据不存在")
			return 0
		elseif 队伍数据[UserData[id].队伍].队员数据[self.临时数据.参数] == id then
			SendMessage(UserData[id].连接id, 7, "#Y/你不能将自己提升为队长")
			return 0
		else
			SendMessage(UserData[队伍数据[UserData[id].队伍].队员数据[self.临时数据.参数]].连接id,20, {nil,"更换队长", "队长将任命你为新的队长，请确认是否同意担任新的队长：",{"我同意","我拒绝"}})
			UserData[队伍数据[UserData[id].队伍].队员数据[self.临时数据.参数]].队长时间 = os.time()
		end
	elseif 序号 == 13 then
		self.临时数据.参数 = self.临时数据.参数 + 0
		if UserData[id].队伍 == 0 then
			SendMessage(UserData[id].连接id, 7, "#Y/请先加入一个队伍")
		elseif UserData[id].队长 == false then
			SendMessage(UserData[id].连接id, 7, "#Y/只有队长才执行此操作")
		elseif 队伍数据[UserData[id].队伍].队员数据[self.临时数据.参数] == id then
			SendMessage(UserData[id].连接id, 7, "#Y/你不能更换自己的位置")
		else
            self:交换位置(id,参数,内容+0)
		end
	elseif 序号 == 14 then
		if UserData[id].队伍 == 0 then
			SendMessage(UserData[id].连接id, 7, "#Y/请先加入一个队伍")
		elseif UserData[id].队长 == false then
			SendMessage(UserData[id].连接id, 7, "#Y/只有队长才执行此操作")
		elseif 参数+0 <0 then
			SendMessage(UserData[id].连接id, 7, "#Y/最低等级不能小于0级")
		elseif 内容+0 >175 then
			SendMessage(UserData[id].连接id, 7, "#Y/最高等级不高于175级")
        else
        	队伍数据[UserData[id].队伍].最低等级 = 参数+0
        	队伍数据[UserData[id].队伍].最高等级 =内容+0
        	SendMessage(UserData[id].连接id, 7, "#Y/队伍当前限制等级为:"..队伍数据[UserData[id].队伍].最低等级.."-"..队伍数据[UserData[id].队伍].最高等级)
        end
	elseif 序号 == 15 then
        SendMessage(UserData[id].连接id, 5012,UserData[id].角色.队标)
    elseif 序号 == 16 then
		if UserData[id].队伍 == 0 then
			SendMessage(UserData[id].连接id, 7, "#Y/请先加入一个队伍")
		elseif UserData[id].队长 == false then
			SendMessage(UserData[id].连接id, 7, "#Y/只有队长才执行此操作")
        else
    		队伍数据[UserData[id].队伍].队标 = UserData[id].角色.队标[参数+0]
			SendMessage(UserData[id].连接id, 5002, 队伍数据[UserData[id].队伍].队标)
            MapControl:更改队长(id, true)
    		SendMessage(UserData[id].连接id, 7, "#Y/更换队标成功")
    	end
    elseif self.临时数据.序号 == 17 then
     	 tchs()
         return
	end
end
function TeamControl:交换位置(id, 序列,序号2)
	self.原始序列 = 序号2
	self.更换序列 = 序列
	self.队伍id = UserData[id].队伍
	if UserData[id].队长 == false then
		SendMessage(UserData[id].连接id, 7, "#Y/你没有进行此种操作的权限")
		return 0
	elseif self.原始序列 == self.更换序列 then
		SendMessage(UserData[id].连接id, 7, "#Y/你无法在同一个位置上进行交换")
		return 0
	elseif self.原始序列 == 1 or self.更换序列 == 1 then
		SendMessage(UserData[id].连接id, 7, "#Y/你无法更换队长的位置")
		return 0
	elseif 队伍数据[self.队伍id].队员数据[self.更换序列] == nil then
		SendMessage(UserData[id].连接id, 7, "#Y/请选择正确的队伍位置")
		return 0
	else
		self.原始id = 队伍数据[self.队伍id].队员数据[self.原始序列]
		队伍数据[self.队伍id].队员数据[self.原始序列] = 队伍数据[self.队伍id].队员数据[self.更换序列]
		队伍数据[self.队伍id].队员数据[self.更换序列] = self.原始id
		for n = 1, #队伍数据[self.队伍id].队员数据 do
			self:发送队伍信息(id, 5009, UserData[队伍数据[self.队伍id].队员数据[n]].连接id)
		end
		SendMessage(UserData[id].连接id, 7, "#Y/更换位置成功")
		self:队伍状态检查(id)
	end
end
function TeamControl:新任队长(玩家id)
	self.原来id = UserData[玩家id].队伍
	队伍数据[玩家id] = table.copy(队伍数据[UserData[玩家id].队伍])
	for n = 1, #队伍数据[玩家id].队员数据 do
		UserData[队伍数据[玩家id].队员数据[n]].队伍 = 玩家id
		SendMessage(UserData[队伍数据[玩家id].队员数据[n]].连接id, 7, "#G/" .. UserData[玩家id].角色.名称 .. "#Y/被任命为新的队长")
		if 玩家id == 队伍数据[玩家id].队员数据[n] then
			self.位置序列 = n
		end
	end
	队伍数据[玩家id].队员数据[self.位置序列] = self.原来id
	队伍数据[玩家id].队员数据[1] = 玩家id
	队伍数据[玩家id].队长编号 = 玩家id
	UserData[玩家id].队长 = true
	UserData[self.原来id].队长 = false
	UserData[self.原来id].队长时间 = nil
	MapControl:更改队长(玩家id, true)
	MapControl:更改队长(self.原来id, false)
	SendMessage(UserData[玩家id].连接id, 5002, 队伍数据[UserData[玩家id].队伍].队标)
	SendMessage(UserData[self.原来id].连接id, 5003, "66")
	SendMessage(UserData[玩家id].连接id, 7, "#G/你被任命为新的队长")
	 队伍数据[UserData[玩家id].队伍].队标="普通"
	 队伍数据[UserData[玩家id].队伍].阵法="普通"
	for n = 1, #队伍数据[玩家id].队员数据 do
		self:发送队伍信息(玩家id, 5009, UserData[队伍数据[玩家id].队员数据[n]].连接id)
	end
end
function TeamControl:离开队伍(队伍id, 玩家id, 退出)
	if UserData[玩家id].队长 then
		self.记录id = {}
		for n = 1, #队伍数据[队伍id].队员数据 do
			self.记录id[n] = 队伍数据[队伍id].队员数据[n]
			UserData[队伍数据[队伍id].队员数据[n]].队伍 = 0
		end
		队伍数据[队伍id] = {
			队长编号 = 0,
			队员数据 = {},
			申请数据 = {}
		}
		UserData[玩家id].队长 = false

		for n = 1, #self.记录id do
			self:发送队伍信息(队伍id, 5009, UserData[self.记录id[n]].连接id)
			SendMessage(UserData[self.记录id[n]].连接id, 7, "#Y/队长离开了队伍，本队伍已经被强制解散")
			SendMessage(UserData[self.记录id[n]].连接id, 5011, "66")

			UserData[self.记录id[n]].队伍 = 0
		end

		if 退出 == nil then
			MapControl:更改队长(玩家id, false)
			SendMessage(UserData[玩家id].连接id, 5003, "66")
		end
	else
		self.记录id = 0
		for n = 1, #队伍数据[队伍id].队员数据 do
				if 玩家id == 队伍数据[队伍id].队员数据[n] then
					self.记录id = n
				end
		end
		table.remove(队伍数据[队伍id].队员数据, self.记录id)
		for n = 1, #队伍数据[队伍id].队员数据 do
			self:发送队伍信息(队伍id, 5009, UserData[队伍数据[队伍id].队员数据[n]].连接id)
			SendMessage(UserData[队伍数据[队伍id].队员数据[n]].连接id, 7, "#G/" .. UserData[玩家id].角色.名称 .. "#Y/离开了队伍")
		end

		UserData[玩家id].队伍 = 0
		SendMessage(UserData[玩家id].连接id, 7, "#Y/你离开了队伍")
		SendMessage(UserData[玩家id].连接id, 5011, "66")
		SendMessage(UserData[玩家id].连接id, 5009, "66")
	end
end
function TeamControl:踢出玩家(队伍id, 玩家id, 踢出id)
	if 踢出id == 1 then
		SendMessage(UserData[玩家id].连接id, 7, "#Y/您不能将自己踢出队伍")

		return 0
	end
	for n = 1, #队伍数据[队伍id].队员数据 do
		if UserData[队伍数据[队伍id].队员数据[n]] then
		SendMessage(UserData[队伍数据[队伍id].队员数据[n]].连接id, 7, "#G/" .. UserData[队伍数据[队伍id].队员数据[踢出id]].角色.名称 .. "#Y/被队长从队伍中踢了出去")
		end
	end
	UserData[队伍数据[队伍id].队员数据[踢出id]].队伍 = 0
	SendMessage(UserData[队伍数据[队伍id].队员数据[踢出id]].连接id, 5009, "66")
	table.remove(队伍数据[队伍id].队员数据, 踢出id)
	for n = 1, #队伍数据[队伍id].队员数据 do
		self:发送队伍信息(队伍id, 5009, UserData[队伍数据[队伍id].队员数据[n]].连接id)
	end
	self:发送队伍信息(队伍id, 5006, UserData[玩家id].连接id)
end
function TeamControl:同意玩家申请(队伍id, 玩家id, 拒绝id)
	if 队伍数据[队伍id].申请数据[拒绝id] == nil then
		SendMessage(UserData[玩家id].连接id, 7, "#Y/操作异常，错误代号703")
		return 0
	elseif UserData[队伍数据[队伍id].申请数据[拒绝id]] == nil then
		SendMessage(UserData[玩家id].连接id, 7, "#Y/该玩家已经下线")
		self:移除玩家申请(队伍id, 玩家id, 拒绝id, 1)
		return 0
	elseif UserData[队伍数据[队伍id].申请数据[拒绝id]].战斗 ~= 0  then
		SendMessage(UserData[玩家id].连接id, 7, "#Y/该玩家正在战斗中")
		return 0
	elseif UserData[队伍数据[队伍id].申请数据[拒绝id]].队伍 ~= 0 then
		SendMessage(UserData[玩家id].连接id, 7, "#Y/对方已经加入了其它队伍")
		self:移除玩家申请(队伍id, 玩家id, 拒绝id, 1)
		return 0
	elseif UserData[队伍数据[队伍id].申请数据[拒绝id]].地图 ~= UserData[玩家id].地图 then
		SendMessage(UserData[玩家id].连接id, 7, "#Y/对方离你太远了")
		self:移除玩家申请(队伍id, 玩家id, 拒绝id, 1)
		return 0
	elseif 取两点距离(UserData[玩家id].角色.地图数据, UserData[队伍数据[队伍id].申请数据[拒绝id]].角色.地图数据) > 500 then
		SendMessage(UserData[玩家id].连接id, 7, "#Y/这个玩家离你太远了")
		self:移除玩家申请(队伍id, 玩家id, 拒绝id, 1)
		return 0
	elseif #队伍数据[队伍id].队员数据 >= 5 then
		SendMessage(UserData[玩家id].连接id, 7, "#Y/您的队伍已经满员了")
		return 0
	else
		SendMessage(UserData[队伍数据[队伍id].申请数据[拒绝id]].连接id, 7, "#G/" .. UserData[玩家id].角色.名称 .. "#Y/同意了你的入队申请")
		队伍数据[队伍id].队员数据[#队伍数据[队伍id].队员数据 + 1] = 队伍数据[队伍id].申请数据[拒绝id]
		for n = 1, #队伍数据[队伍id].队员数据 do
			SendMessage(UserData[队伍数据[队伍id].队员数据[n]].连接id, 7, "#G/" .. UserData[队伍数据[队伍id].申请数据[拒绝id]].角色.名称 .. "#Y/加入了队伍")
			self:发送队伍信息(队伍id, 5009, UserData[队伍数据[队伍id].队员数据[n]].连接id)
		end
		UserData[队伍数据[队伍id].申请数据[拒绝id]].队伍 = 队伍id
		self.发送信息 = {
			x = UserData[玩家id].角色.地图数据.x,
			y = UserData[玩家id].角色.地图数据.y,
			id=UserData[玩家id].角色.id
		}
		SendMessage(UserData[队伍数据[队伍id].申请数据[拒绝id]].连接id, 2013, self.发送信息)
		table.remove(队伍数据[队伍id].申请数据, 拒绝id)
		self:发送请求信息(UserData[玩家id].队伍, 5010, UserData[玩家id].连接id)
		self:队伍状态检查(玩家id)
	end
end
function TeamControl:移除玩家申请(队伍id, 玩家id, 拒绝id)
	if 队伍数据[队伍id].申请数据[拒绝id] == nil then
		SendMessage(UserData[玩家id].连接id, 7, "#Y/操作异常，错误代号704")
		return 0
	else
		table.remove(队伍数据[队伍id].申请数据, 拒绝id)
		self:发送请求信息(UserData[玩家id].队伍, 5010, UserData[玩家id].连接id)
	end
end
function TeamControl:拒绝玩家申请(队伍id, 玩家id, 拒绝id, 提示)
	if 队伍数据[队伍id].申请数据[拒绝id] == nil then
		SendMessage(UserData[玩家id].连接id, 7, "#Y/操作异常，错误代号702")
		return 0
	else
		if UserData[队伍数据[队伍id].申请数据[拒绝id]] ~= nil and 提示 == nil then
			SendMessage(UserData[队伍数据[队伍id].申请数据[拒绝id]].连接id, 7, "#G/" .. UserData[玩家id].角色.名称 .. "#Y/拒绝了你的入队申请")
		end
		table.remove(队伍数据[队伍id].申请数据, 拒绝id)
		self:发送请求信息(UserData[玩家id].队伍, 5010, UserData[玩家id].连接id)
	end
end
function TeamControl:申请入队(id, 编号)
	if UserData[编号] == nil then
		SendMessage(UserData[id].连接id, 7, "#Y/这个玩家并不存在")
	elseif UserData[编号].地图 == 5135 then
		SendMessage(UserData[id].连接id, 7, "#Y/本地图不允许组队")
		return 0
	elseif 	 RoleControl:GetTaskID(UserData[id],"押镖")~=0 then 
		SendMessage(UserData[id].连接id, 7, "#Y/有押镖任务不能组队")
		return 0
	elseif 队伍数据[UserData[编号].队伍] == nil then
		SendMessage(UserData[id].连接id, 7, "#Y/对方并没有队伍")
	elseif #队伍数据[UserData[编号].队伍].申请数据 >= 5 then
		SendMessage(UserData[id].连接id, 7, "#Y/对方的申请列表已经满了，请等会再尝试")
	elseif #队伍数据[UserData[编号].队伍].队员数据 >= 5 then
		SendMessage(UserData[id].连接id, 7, "#Y/这个队伍已经满员了")
	elseif 	UserData[id].角色.称谓.特效 ~= UserData[队伍数据[UserData[编号].队伍].队长编号].角色.称谓.特效 then
				SendMessage(UserData[id].连接id, 7, "#Y/不同阵营的队伍不能相互组队")
		return 0
	elseif UserData[id].角色.等级>队伍数据[UserData[编号].队伍].最高等级  or  UserData[id].角色.等级<队伍数据[UserData[编号].队伍].最低等级 then
        SendMessage(UserData[id].连接id, 7, "#Y/队伍当前限制等级为:"..队伍数据[UserData[编号].队伍].最低等级.."-"..队伍数据[UserData[编号].队伍].最高等级)
	else
		for n = 1, #队伍数据[UserData[编号].队伍].申请数据 do
			if 队伍数据[UserData[编号].队伍].申请数据[n] == id then
				SendMessage(UserData[id].连接id, 7, "#Y/你已经申请过这个队伍了，请耐心等待")

				return 0
			end
		end
		队伍数据[UserData[编号].队伍].申请数据[#队伍数据[UserData[编号].队伍].申请数据 + 1] = id
		SendMessage(UserData[队伍数据[UserData[编号].队伍].队长编号].连接id, 7, "#G/" .. UserData[id].角色.名称 .. "#Y/申请加入你的队伍")
		SendMessage(UserData[队伍数据[UserData[编号].队伍].队长编号].连接id, 23, "#G")
		SendMessage(UserData[id].连接id, 7, "#Y/你的入队申请已经提交")
	end
end
function TeamControl:改变阵型(id, 内容)
	if UserData[id].队长 == false then
		SendMessage(UserData[id].连接id, 7, "#Y/只有队长才可以更改阵型")
	elseif UserData[id].角色.阵法[内容] ~= true then
		SendMessage(UserData[id].连接id, 7, "#Y/您似乎还没有学会如何运用此阵法吧")
	else
		队伍数据[UserData[id].队伍].阵法 = 内容

		for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
			if UserData[队伍数据[UserData[id].队伍].队员数据[n]] ~= nil then
				SendMessage(UserData[队伍数据[UserData[id].队伍].队员数据[n]].连接id, 7, "#Y/阵型更改为#r/" .. 内容)
				SendMessage(UserData[队伍数据[UserData[id].队伍].队员数据[n]].连接id, 5007, 队伍数据[UserData[id].队伍].阵法)
			end
		end
	end
end
function TeamControl:发送队伍信息(id, 序号, 发送id)
	self.发送信息 = {最低等级=队伍数据[id].最低等级,最高等级=队伍数据[id].最高等级,队标=队伍数据[id].队标}

	for n = 1, #队伍数据[id].队员数据 do
		if UserData[队伍数据[id].队员数据[n]] ~= nil then
			self.发送信息[#self.发送信息 + 1] = RoleControl:获取地图数据(UserData[队伍数据[id].队员数据[n]],队伍数据[id].队员数据[n])
		end
	end
	 if 发送id == nil then
	 	for n = 1, #队伍数据[id].队员数据 do
	 		SendMessage(UserData[UserData[队伍数据[id].队员数据[n]]].连接id, 序号, self.发送信息)
	 		SendMessage(UserData[UserData[队伍数据[id].队员数据[n]]].连接id, 5007, 队伍数据[id].阵法)
	 	end
	 else
		SendMessage(发送id, 序号, self.发送信息)
	 end
end

function TeamControl:发送请求信息(id, 序号, 发送id)
	self.发送信息 = {}

	for n = 1, #队伍数据[id].申请数据 do
		if UserData[队伍数据[id].申请数据[n]] ~= nil then
			self.发送信息[n] = RoleControl:获取地图数据(UserData[队伍数据[id].申请数据[n]])
		end
	end

	SendMessage(发送id, 5010, self.发送信息)
end

function TeamControl:创建队伍(id)
	if UserData[id].地图 == 5135 then
		SendMessage(UserData[id].连接id, 7, "#Y/本地图不允许组队")
		return 0
	elseif UserData[id].队伍 == 0 then
		SendMessage(UserData[id].连接id, 7, "#Y/创建队伍成功！")
		队伍数据[id] = {
			阵法 = "普通",
			队标 ="普通",
			最低等级 =0,
			最高等级 = 175,
			队长编号 = id,
			申请数据 = {},
			队员数据 = {
				id
			}
		}
		UserData[id].队伍 = id
		UserData[id].队长 = true
		SendMessage(UserData[id].连接id, 5002, 队伍数据[UserData[id].队伍].队标)
		MapControl:更改队长(id, true)
		self:发送队伍信息(UserData[id].队伍, 5009, UserData[id].连接id)
	end
end

function TeamControl:队伍状态检查(id)
	if UserData[id]==nil then
		return
	end
	if UserData[id].队伍 == 0 then
		SendMessage(UserData[id].连接id, 5001, "6")
	else
		self:发送队伍信息(UserData[id].队伍, 5006, UserData[id].连接id)
	end
end
return TeamControl
