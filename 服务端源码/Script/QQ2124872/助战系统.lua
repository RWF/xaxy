-- @Author: 作者QQ381990860
-- @Date:   2021-11-27 17:21:26
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2023-08-06 18:17:03

local 助战系统 = class()
local floor = math.floor
local ceil = math.ceil
--local jnzb = require("script/角色处理类/技能类")
function 助战系统:初始化()
	self.数据={}
end

function 助战系统:加载数据(账号,玩家id)
	self.id = 玩家id

	if file_exists([[玩家信息/账号]]..账号..[[/]]..self.id..[[/助战.txt]])==false then
		WriteFile([[玩家信息/账号]]..账号..[[/]]..self.id.."/助战.txt",table.tostring({}))
	end
	self.数据=table.loadstring(ReadFile([[玩家信息/账号]]..账号..[[/]]..self.id..[[/助战.txt]]))
end

function 助战系统:数据处理(参数,序号,文本)
	if string.find(文本,"do local ret",1) ~= nil then
		文本 = table.loadstring(文本)
	end
	if type(文本) == "table" then
		--print(table.tostring(文本))
	else
		--print(文本)
	end
	if 参数 == 3 then
		if 序号 == 1 then
			local 编号 = self:取助战编号(文本.识别码)
			if self.数据[编号] == nil then
				常规提示(self.id,"#y助战数据异常，请重新尝试。")
				return
			end
			self:刷新装备属性(编号)
			self:发送指定助战物品(编号)
		elseif 序号 == 2 then
			local 编号 = self:取助战编号(文本.识别码)
			local 道具格子=文本.物品编号
			local 道具id=UserData[self.id].角色.道具.包裹[道具格子]
			if 道具格子 == nil or 道具id == nil or UserData[self.id].物品[道具id] ==nil then
				UserData[self.id].角色.道具.包裹[道具格子]=nil
				常规提示(self.id,"#Y道具数据异常请重新获取。")
				return
			end
			if UserData[self.id].物品[道具id].类型 == "武器" or UserData[self.id].物品[道具id].类型 == "头盔" or UserData[self.id].物品[道具id].类型 == "项链" or UserData[self.id].物品[道具id].类型 == "衣服" or UserData[self.id].物品[道具id].类型 == "腰带" or UserData[self.id].物品[道具id].类型 == "鞋子" then
				self:佩戴装备(编号,道具格子)
			else
				常规提示(self.id,"#Y此道具暂不可对助战使用。")
				return
			end
		elseif 序号 == 3 then
			local 编号 = self:取助战编号(文本.识别码)
			local 装备格子=文本.物品编号
			if self.数据[编号].装备数据[装备格子+20] == nil then
				常规提示(self.id,"#Y装备数据异常。")
				return
			elseif UserData[self.id].物品[self.数据[编号].装备数据[装备格子+20]] == nil then
				self.数据[编号].装备数据[装备格子+20] = nil
				self:刷新装备属性(编号)
				常规提示(self.id,"#Y装备数据异常。")
				return
			end
			local 可用格子 = RoleControl:取可用道具格子(UserData[self.id],"包裹")
			if 可用格子 == 0 then
				SendMessage(UserData[self.id].连接id,7,"#y/你已经无法携带更多的道具了")
				return
			end
			UserData[self.id].角色.道具["包裹"][可用格子]=self.数据[编号].装备数据[装备格子+20]
			self.数据[编号].装备数据[装备格子+20] = nil
			if self.数据[编号].装备数据[23] == nil then
				self.数据[编号].武器数据={名称="",子类="",等级=0,染色方案=0,染色组={}}
			end
			self:刷新装备属性(编号)
			self:发送指定助战物品(编号)
			SendMessage(UserData[self.id].连接id,3006,"66")
		elseif 序号 == 4 then
			self:设置战斗(self:取助战编号(文本.识别码),文本.识别码)
		elseif 序号 == 5 then
			UserData[self.id].最后操作 = {动作="助战洗点",识别码=文本.识别码}
			SendMessage(UserData[self.id].连接id,20,{UserData[self.id].角色.模型,UserData[self.id].角色.名称,"#W40级以下助战洗点免费，40级以上洗点每次需消耗#R等级*10000#W银子，请确认是否继续洗点？",{"确认洗点","暂不洗点"},"特殊"})
		elseif 序号 == 6 then
			local 编号 = self:取助战编号(文本.识别码)
			if self.数据[编号] == nil then
				常规提示(self.id,"#Y助战数据异常。")
				return
			end
			UserData[self.id].最后操作 = {动作="助战门派",识别码=文本.识别码}
			
			if self.数据[编号].门派 ~= "无门派" then
				SendMessage(UserData[self.id].连接id,20,{UserData[self.id].角色.模型,"退出门派","退出门派需要消耗500W银子,你确定要退出么。",{"退出助战门派","算了算了"},"特殊"})
			else
				SendMessage(UserData[self.id].连接id,20,{UserData[self.id].角色.模型,"加入门派","请选择该助战要拜入的门派。",{"方寸山(助战)","女儿村(助战)","神木林(助战)","化生寺(助战)","大唐官府(助战)","盘丝洞(助战)","阴曹地府(助战)","无底洞(助战)","魔王寨(助战)","狮驼岭(助战)","天宫(助战)","普陀山(助战)","凌波城(助战)","五庄观(助战)","龙宫(助战)"},"特殊"})
			end
		elseif 序号 == 7 then
			self:助战改名(self:取助战编号(文本.识别码),文本.名称)
		elseif 序号 == 8 then
			local 编号 = self:取助战编号(文本.识别码)
			if self.数据[编号] == nil then
				常规提示(self.id,"#Y助战数据异常。")
				return
			end
			UserData[self.id].最后操作 = {动作="助战培养",识别码=文本.识别码}
			SendMessage(UserData[self.id].连接id,20,{UserData[self.id].角色.模型,"助战培养","请选择您要进行的操作,单次培养消耗100W经验、50W银子。",{"培养一次","培养十次","培养五十次","算了算了"},"特殊"})
		elseif 序号 == 9 then
			local 编号 = self:取助战编号(文本.识别码)
			if self.数据[编号] == nil then
				常规提示(self.id,"#Y助战数据异常。")
				return
			end
			UserData[self.id].最后操作 = {动作="助战遗弃",识别码=文本.识别码}
			SendMessage(UserData[self.id].连接id,20,{UserData[self.id].角色.模型,"助战遗弃","你确定要遗弃该助战么,一旦遗弃无法找回！",{"确定遗弃","算了算了"},"特殊"})
		elseif 序号 == 10 then	--学习技能
			self:师门技能(self:取助战编号(文本.识别码))
		elseif 序号 == 11 then	--修炼
			local 编号 = self:取助战编号(文本.识别码)
			if self.数据[编号] == nil then
				常规提示(self.id,"#Y助战数据异常。")
				return
			end
			local id = self.id
			SendMessage(UserData[id].连接id,20062,{识别码 = self.数据[编号].识别码,助战修炼 = self.数据[编号].修炼})
		elseif 序号 == 12 then	--辅助
			local 编号 = self:取助战编号(文本.识别码)
			if self.数据[编号] == nil then
				常规提示(self.id,"#Y助战数据异常。")
				return
			end
			local id = self.id
			SendMessage(UserData[id].连接id,20063,{识别码 = self.数据[编号].识别码,辅助技能=self.数据[编号].辅助技能,强化技能=self.数据[编号].强化技能})
		elseif 序号 == 13 then	--锦衣
		elseif 序号 == 14 then	--奇经八脉
			self:奇经八脉(self:取助战编号(文本.识别码))
		elseif 序号 == 15 then	--奇经八脉
			self:助战加点(self:取助战编号(文本.识别码),文本.加点)
		elseif 序号 == 16 then	--奇经八脉
			self:修炼提升(self:取助战编号(文本.识别码),文本.修炼类型,文本.次数)
		elseif 序号 == 17 then	--奇经八脉
			self:师门技能学习(self:取助战编号(文本.识别码),文本.序列)
		elseif 序号 == 18 then	--奇经八脉
			self:辅助技能学习(self:取助战编号(文本.识别码),文本.学习号)
		end
	end
end



function 助战系统:辅助技能学习(编号,技能名)
	if self.数据[编号] == nil then
		常规提示(self.id,"#Y助战数据异常。")
		return
	end
	local 类型 = ""
	if self.数据[编号].辅助技能[技能名] ~= nil then
		类型 = "辅助技能"
	elseif self.数据[编号].强化技能[技能名] ~= nil then
		类型 = "强化技能"
	else
		常规提示(self.id,"#Y助战技能数据异常。")
		return
	end
	local 本次消耗 = 计算技能数据(self.数据[编号][类型][技能名]+1)
	if self.数据[编号][类型][技能名] >= 40 then
		本次消耗.帮贡 = floor(self.数据[编号][类型][技能名] * 0.5)
	end
	if 银子检查(self.id,本次消耗.金钱,1) == false then
		常规提示(self.id,"#Y/本次助战学习"..类型.."需消耗#R"..本次消耗.金钱.."#Y两银子!")
		return
	elseif self.数据[编号].当前经验 < 本次消耗.经验 then
		常规提示(self.id,"#Y/本次助战学习"..类型.."需消耗#R"..本次消耗.经验.."#Y点经验!")
		return
	else
		if 本次消耗.帮贡 ~= nil and 本次消耗.帮贡 > 0 then
			if UserData[self.id].角色.帮派==nil then
				常规提示(self.id,"#Y/请加入帮派后才能继续学习")
				return
			elseif 帮派数据[UserData[self.id].角色.帮派].成员名单[self.id].帮贡.当前 < 本次消耗.帮贡 then
				常规提示(self.id, "#y/你没有那么多的帮贡")
				return
			end
		end
	end
	RoleControl:扣除银子(UserData[self.id],本次消耗.金钱,"助战学习师门技能",1)
	self.数据[编号].当前经验 = self.数据[编号].当前经验 - 本次消耗.经验
	if 本次消耗.帮贡 ~= nil and 本次消耗.帮贡 > 0 then
		帮派数据[UserData[self.id].角色.帮派].成员名单[self.id].帮贡.当前 = 帮派数据[UserData[self.id].角色.帮派].成员名单[self.id].帮贡.当前 - 本次消耗.帮贡
	end
	self.数据[编号][类型][技能名] = self.数据[编号][类型][技能名] + 1
	常规提示(self.id,"#Y你的助战"..类型.."#G"..技能名.."#Y等级成功提升至"..self.数据[编号][类型][技能名].."级。")
	self:刷新战斗属性(编号,1)
	SendMessage(UserData[self.id].连接id,20063,{识别码 = self.数据[编号].识别码,辅助技能=self.数据[编号].辅助技能,强化技能=self.数据[编号].强化技能})
end


function 助战系统:师门技能学习(编号,序列)
	if self.数据[编号] == nil then
		常规提示(self.id,"#Y助战数据异常。")
		return
	elseif self.数据[编号].师门技能[序列] == nil then
		常规提示(self.id,"#Y技能数据异常。")
		return
	elseif self.数据[编号].师门技能[序列].等级 >= self.数据[编号].等级 + 10 then
		常规提示(self.id,"#Y助战的师门技能等级不能大于自身等级10级。")
		return
	end
	local 本次消耗 = 计算技能数据(self.数据[编号].师门技能[序列].等级+1)
	if 银子检查(self.id,本次消耗.金钱,1) == false then
		常规提示(self.id,"#Y/本次助战学习师门技能需消耗#R"..本次消耗.金钱.."#Y两银子!")
		return
	elseif self.数据[编号].当前经验 < 本次消耗.经验 then
		常规提示(self.id,"#Y/本次助战学习师门技能需消耗#R"..本次消耗.经验.."#Y点经验!")
		return
	end
	RoleControl:扣除银子(UserData[self.id],本次消耗.金钱,"助战学习师门技能",1)
	self.数据[编号].当前经验 = self.数据[编号].当前经验 - 本次消耗.经验
	self.数据[编号].师门技能[序列].等级 = self.数据[编号].师门技能[序列].等级 + 1
	for n,v in pairs(self.数据[编号].师门技能[序列].包含技能) do
		self.数据[编号].师门技能[序列].包含技能[n].等级 = self.数据[编号].师门技能[序列].等级
		for nn,vv in pairs(self.数据[编号].学会技能) do
			if vv.学会 and vv.名称 == self.数据[编号].师门技能[序列].包含技能[n].名称 then
				self.数据[编号].学会技能[nn].等级 = self.数据[编号].师门技能[序列].包含技能[n].等级
			end
		end
	end
	self.数据[编号].技能属性 = robotskill:skillup(self.id,self.数据[编号])
	self:刷新战斗属性(编号,1)
	SendMessage(UserData[self.id].连接id,20061,{等级=self.数据[编号].等级,识别码 = self.数据[编号].识别码,师门技能=self.数据[编号].师门技能,门派=self.数据[编号].门派,当前经验=floor(self.数据[编号].当前经验),银子=UserData[self.id].角色.道具.货币.银子,储备=UserData[self.id].角色.道具.货币.储备,存银=UserData[self.id].角色.道具.货币.存银})
end

function 助战系统:修炼提升(编号,类型,次数)
	if self.数据[编号] == nil then
		常规提示(self.id,"#Y助战数据异常。")
		return
	end
	if self.数据[编号].修炼[类型].当前等级 >= self.数据[编号].修炼[类型].上限等级 then
		常规提示(self.id,"#Y该助战当前修炼已达到上限。")
		return
	end
	local 消耗 = 20000
	if 类型 == "抗法修炼" or 类型 == "防御修炼" then
		消耗 = 15000
	end
	消耗 = tonumber(消耗 * 次数)
	if 银子检查(self.id,消耗) == false then
		常规提示(self.id,"#Y/本次提升需消耗#R"..消耗.."#Y两银子!")
		return
	end
	RoleControl:扣除银子(UserData[self.id],消耗,"提升助战修炼")
	self.数据[编号].修炼[类型].当前经验 = self.数据[编号].修炼[类型].当前经验 + 次数 * 10
	常规提示(self.id,"#Y/你的助战"..self.数据[编号].名称..类型.."经验增加了#R"..(次数 * 10).."#Y点!")
	while (self.数据[编号].修炼[类型].当前经验 >= self.数据[编号].修炼[类型].升级经验) do
		self.数据[编号].修炼[类型].当前经验 = self.数据[编号].修炼[类型].当前经验 - self.数据[编号].修炼[类型].升级经验
		self.数据[编号].修炼[类型].当前等级 = self.数据[编号].修炼[类型].当前等级 + 1
		self.数据[编号].修炼[类型].升级经验 = 计算修炼等级经验(self.数据[编号].修炼[类型].当前等级,self.数据[编号].修炼[类型].上限等级)
		常规提示(self.id,"#Y/你的助战"..self.数据[编号].名称..类型.."等级提升至#R"..self.数据[编号].修炼[类型].当前等级.."#Y级!")
	end
	--self:存档(self.id)
	SendMessage(UserData[self.id].连接id,20062,{识别码 = self.数据[编号].识别码,助战修炼 = self.数据[编号].修炼})
end


function 助战系统:助战加点(编号,加点)
	if self.数据[编号] == nil then
		常规提示(self.id,"#Y助战数据异常。")
		return
	end
	local 总点数 = 0
	for n,v in pairs(加点) do
		总点数 = 总点数 + v
	end
	if 总点数 <= 0 or self.数据[编号].潜力 < 总点数 then
		常规提示(self.id,"#Y禁止非法修改游戏数据，超过三次永久封禁。")
		return
	end
    self.数据[编号].体质 = self.数据[编号].体质 + 加点.体质
    self.数据[编号].魔力 = self.数据[编号].魔力 + 加点.魔力
    self.数据[编号].力量 = self.数据[编号].力量 + 加点.力量
    self.数据[编号].耐力 = self.数据[编号].耐力 + 加点.耐力
    self.数据[编号].敏捷 = self.数据[编号].敏捷 + 加点.敏捷
    self.数据[编号].潜力 = self.数据[编号].潜力 - 总点数
	常规提示(self.id,"#Y加点成功。")
	self:刷新战斗属性(编号,1)
end

function 助战系统:奇经八脉(编号)
	if self.数据[编号] == nil then
		常规提示(self.id,"#Y助战数据异常。")
		return
	end
    local id = self.id
	if self.数据[编号].等级>=69 then
		self.数据[编号].可换乾元丹=1
	end
	if self.数据[编号].等级>=89 then
		self.数据[编号].可换乾元丹=2
	end
	if self.数据[编号].等级>=109 then
		self.数据[编号].可换乾元丹=3
	end
	if self.数据[编号].等级>=129 then
		self.数据[编号].可换乾元丹=4
	end
	if self.数据[编号].等级>=155 then
		self.数据[编号].可换乾元丹=5
	end
	if self.数据[编号].等级>=159 then
		self.数据[编号].可换乾元丹=6
	end
	if self.数据[编号].等级>=164 then
		self.数据[编号].可换乾元丹=7
	end
	if self.数据[编号].等级>=168 then
		self.数据[编号].可换乾元丹=8
	end
	if self.数据[编号].等级>=171 then
		self.数据[编号].可换乾元丹=9
	end
	SendMessage(UserData[id].连接id,20064,{识别码 = self.数据[编号],门派=self.数据[编号].门派,奇经八脉=self.数据[编号].奇经八脉,装备属性=self.数据[编号].装备属性})
end


function 助战系统:师门技能(编号)
	if self.数据[编号] == nil then
		常规提示(self.id,"#Y助战数据异常。")
		return
	end
	local id = self.id
	if self.数据[编号].门派 == nil then
		常规提示(id,"该助战未加入门派")
		return 0
	end
	if self.数据[编号].门派 ~= "无门派" and self.数据[编号].师门技能 == nil then
		self.数据[编号].师门技能={}
		local 列表 = 取门派技能(self.数据[编号].门派)
		for n=1,#列表 do
			self.数据[编号].师门技能[n]=置技能(列表[n])
			self.数据[编号].师门技能[n].等级=0
			local w = 取包含技能(self.数据[编号].师门技能[n].名称)
			self.数据[编号].师门技能[n].包含技能 = {}
			for s=1 , #w do
				if w[s] ~= nil then
					self.数据[编号].师门技能[n].包含技能[s]=置技能(w[s])
					self.数据[编号].师门技能[n].包含技能[s].等级 = self.数据[编号].师门技能[n].等级
				end
			end
		end
		--self:存档(self.id)
	end
	SendMessage(UserData[id].连接id,20061,{等级=self.数据[编号].等级,识别码 = self.数据[编号].识别码,师门技能=self.数据[编号].师门技能,门派=self.数据[编号].门派,当前经验=floor(self.数据[编号].当前经验),银子=UserData[id].角色.道具.货币.银子,储备=UserData[id].角色.道具.货币.储备,存银=UserData[id].角色.道具.货币.存银})
end


function 助战系统:助战遗弃(识别码)
	local 编号 = self:取助战编号(识别码)
	local id = self.id
	if self.数据[编号] == nil then
		常规提示(self.id,"#Y助战数据异常。")
		return
	end
	for i = 1, 6 do
		if self.数据[编号].装备数据[i+20] ~= nil then
			常规提示(self.id,"#Y请先卸下该助战身上的装备。")
			return
		end
	end
	for i = 1, 4 do
		if self.数据[编号].灵饰[i] ~= nil then
			常规提示(self.id,"#Y请先卸下该助战身上的灵饰。")
			return
		end
	end
	if self.数据[编号].参战 then
		常规提示(self.id,"#YY请先取消该助战参战状态。")
		return
	end
	local 临时数据 = {}
	for i = 1,#self.数据 do
		if self.数据[i].识别码 ~= 识别码 then
			临时数据[#临时数据+1] = self.数据[i]
		end
	end
	self.数据 = 临时数据
	SendMessage(UserData[self.id].连接id,20058,self:取数据())
end

function 助战系统:助战培养(事件,识别码)
	local 编号 = self:取助战编号(识别码)
	local id = self.id
	if self.数据[编号] == nil then
		常规提示(self.id,"#Y助战数据异常。")
		return
	end
	local 次数 = 1
	if 事件 == "培养十次" then
		次数 = 10
	elseif 事件 == "培养五十次" then
		次数 = 50
	end
	local 经验 = 100000*次数
	local 银子 = 50000*次数
	-- if 银子检查(self.id,银子) == false then
	-- 	常规提示(self.id,"#Y/本次培养助战需消耗#R"..银子.."#Y两银子!")
	-- 	return
	-- end
	-- if UserData[id].角色.当前经验 < 经验 then
	-- 	常规提示(self.id,"#Y/本次培养助战需消耗#R"..经验.."#Y点经验!")
	-- 	return
	-- end
	-- RoleControl:扣除银子(UserData[self.id],银子,"助战培养")
	-- UserData[id].角色.当前经验 = UserData[id].角色.当前经验 - 经验
	常规提示(id,"#Y助战培养成功，你消耗#R"..银子.."#Y两银子。")
	常规提示(id,"#Y助战培养成功，你消耗#R"..经验.."#Y点经验。")
	常规提示(id,"#Y助战培养成功，助战增加了#R"..经验.."#Y点经验。")
	self:添加经验(编号,经验)
end

function 助战系统:助战改名(编号,姓名)
	if self.数据[编号] == nil then
		常规提示(self.id,"#Y助战数据异常。")
		return
	end
	local idd = UserData[self.id].连接id
	if self.数据[编号].名称 == 姓名 then
		SendMessage(idd,7,"#y/请正确输入您要修改的助战名称")
		return
	elseif string.len(姓名) < 4 then
		SendMessage(idd,7,"#y/助战名称长度过短")
		return
	elseif string.len(姓名) > 15 then
		SendMessage(idd,7,"#y/助战名称长度过长")
		return
	elseif 姓名 == "" then
		SendMessage(idd,7,"#y/助战名称不能为空")
		return
	elseif string.find(姓名, "/") ~= nil then
		SendMessage(idd,7,"#y/助战名称含有特殊符号")
		return
	elseif string.find(姓名, "@") ~= nil then
		SendMessage(idd,7,"#y/助战名称含有特殊符号")
		return false
	elseif string.find(姓名, "#") ~= nil then
		SendMessage(idd,7,"#y/助战名称含有特殊符号")
		return false
	elseif string.find(姓名, "*") ~= nil then
		SendMessage(idd,7,"#y/助战名称含有特殊符号")
		return false
	end
	for n = 1, string.len(姓名) do
		if __gge.crc32(string.sub(姓名, n, n)) == -378745019 or __gge.crc32(string.sub(姓名, n, n)) == -1327500718 then
		SendMessage(idd,7,"#y/助战名称含有特殊符号")
			return false
		end
	end
	for n = 1, #敏感词数据 do
		if string.find(姓名, 敏感词数据[n]) ~= nil then
			SendMessage(idd,7,"#y/助战名称存在不允许使用的字符")
			return false
		end
	end
	self.数据[编号].名称 = 姓名
	SendMessage(idd,7,"#y/该助战名称修改成功.")
	--self:存档(self.id)
	self:发送指定助战数据(编号)
end

function 助战系统:助战门派(事件,识别码)
	local 编号 = self:取助战编号(识别码)
	if self.数据[编号] == nil then
		常规提示(self.id,"#Y助战数据异常。")
		return
	end
	if 事件 == "退出助战门派" then
		if 银子检查(self.id,5000000) == false then
			常规提示(self.id,"#Y/你当前的银子好像不够哟!")
			return
		end
		self:清空奇经八脉(编号,1)
		RoleControl:扣除银子(UserData[self.id],5000000,"助战退出门派")
		self.数据[编号].门派 ="无门派"
		self.数据[编号].技能 = {}
		self.数据[编号].师门技能 = nil
		self.数据[编号].技能属性 = {气血=0,魔法=0,命中=0,伤害=0,防御=0,速度=0,躲避=0,灵力=0,体质=0,魔力=0,力量=0,耐力=0,敏捷=0}
		常规提示(self.id,"#Y/该助战退出门派成功!")
		--self:存档(self.id)
		self:发送指定助战数据(编号)
	else
		self.数据[编号].门派 = 分割文本1(事件,"(")[1]
		常规提示(self.id,"#Y/该助战加入门派成功!")
		--self:存档(self.id)
		self:发送指定助战数据(编号)
	end
end


function 助战系统:清空奇经八脉(编号,免费)
	local id = self.id
	if 免费 == nil and self.数据[编号].乾元丹 ~= nil and self.数据[编号].乾元丹 == 0 then
		常规提示(id,"#Y/此助战当前没有可清空的奇经八脉！")
		return
	end
	if 免费 == nil then
		local 清空奇经八脉费用 = 5000000
		if 银子检查(self.id, 清空奇经八脉费用) == false then
			常规提示(self.id,"#Y清空奇经八脉需消耗#R"..清空奇经八脉费用.."#Y两银子。")
			return
		end
		RoleControl:扣除银子(UserData[self.id],清空奇经八脉费用,"清空奇经八脉")
	end
	self.数据[编号].奇经八脉 = {}
	self.数据[编号].开启奇经八脉 = nil
	self.数据[编号].技能树 = nil
	self.数据[编号].剩余乾元丹=self.数据[编号].剩余乾元丹+self.数据[编号].乾元丹
	self.数据[编号].乾元丹=0
	--self:存档(self.id)
	self:发送指定助战数据(编号)
	常规提示(id,"#Y/该助战经脉重置成功！")
end

function 助战系统:助战洗点(识别码)
	local 编号 = self:取助战编号(识别码)
	if self.数据[编号] == nil then
		常规提示(self.id,"#Y助战数据异常。")
		return
	end
	local 消耗银子 = 0
	if self.数据[编号].等级 >= 40 then
		消耗银子 = self.数据[编号].等级 * 10000
		if 银子检查(self.id, 消耗银子) == false then
			常规提示(self.id,"#Y本次助战洗点需消耗#R"..消耗银子.."#Y两银子。")
			return
		end
		RoleControl:扣除银子(UserData[self.id],消耗银子,"赞助洗点")
	end
	
	local cs = self:取初始属性(self.数据[编号].种族)
	self.数据[编号].潜力 = self.数据[编号].等级*5
	self.数据[编号].体质 =	cs[1]+self.数据[编号].等级
	self.数据[编号].魔力 =	cs[2]+self.数据[编号].等级
	self.数据[编号].力量 =	cs[3]+self.数据[编号].等级
	self.数据[编号].耐力 =	cs[4]+self.数据[编号].等级
	self.数据[编号].敏捷 =	cs[5]+self.数据[编号].等级
	常规提示(self.id,"#Y助战洗点成功！！")
	--self:存档(self.id)
	self:发送指定助战数据(编号)
end

function 助战系统:设置战斗(编号,识别码)
	if self.数据[编号] == nil then
		常规提示(self.id,"#Y助战数据异常。")
		return
	elseif self.数据[编号].等级 > UserData[self.id].角色.等级 + 5 then
		常规提示(self.id,"#Y参战助战等级不能大于自身5级。")
		return
	elseif not self.数据[编号].参战 then
		local 参战数量 = 0
		for n,v in pairs(self.数据) do
			if v.参战 then
				参战数量 = 参战数量 + 1
			end
		end
		if 参战数量 >= 4 then
			常规提示(self.id,"#Y最多只能参战设置4个助战参战。")
			return
		end
	end
	if self.数据[编号].参战 then
		self.数据[编号].参战 = false
	else
		self.数据[编号].参战 = true
	end
	self:发送指定助战数据(编号)
	--self:存档(self.id)
end

function 助战系统:取技能数据(编号)
	local 发送信息={}
	发送信息.技能 = self.数据[编号].技能
	发送信息.特殊技能 = self.数据[编号].特殊技能
	return 发送信息
end

function 助战系统:取灵饰数据(编号)
	local 返回数据={}
	for n, v in pairs(self.数据[编号].灵饰) do
		返回数据[n]=table.loadstring(table.tostring(UserData[self.id].物品[self.数据[编号].灵饰[n]]))
	end
	return 返回数据
end

function 助战系统:取锦衣数据(连接id,id,助战编号)
	local id = self.id
	local 编号 = 助战编号
	锦衣数据 = {}
	if self.数据[编号].锦衣[1]~=nil then
		锦衣数据[1]=table.loadstring(table.tostring(UserData[self.id].物品[self.数据[编号].锦衣[1]]))
	end
	if self.数据[编号].锦衣[2]~=nil then
		锦衣数据[2]=table.loadstring(table.tostring(UserData[self.id].物品[self.数据[编号].锦衣[2]]))
	end
	if self.数据[编号].锦衣[3]~=nil then
		锦衣数据[3]=table.loadstring(table.tostring(UserData[self.id].物品[self.数据[编号].锦衣[3]]))
	end
	if self.数据[编号].锦衣[4]~=nil then
		锦衣数据[4]=table.loadstring(table.tostring(UserData[self.id].物品[self.数据[编号].锦衣[4]]))
	end
	return {锦衣 = 锦衣数据}
end

function 助战系统:取装备数据(编号)
	local 返回数据={}
	if self.数据[编号].装备数据 == nil then
		self.数据[编号].装备数据 = {}
	end
	for n, v in pairs(self.数据[编号].装备数据) do
		返回数据[n]=table.loadstring(table.tostring(UserData[self.id].物品[self.数据[编号].装备数据[n]]))
	end
	return 返回数据
end

function 助战系统:佩戴装备(编号,道具格子)
	local 道具id = UserData[self.id].角色.道具["包裹"][道具格子]
	if UserData[self.id].物品[道具id] == nil then
		SendMessage(UserData[self.id].连接id,7,"#Y/此道具不存在请刷新背包")
		return
	end
	local id = self.id
	local 道具类型=UserData[id].物品[道具id].类型
	local 道具等级=UserData[id].物品[道具id].等级
	local 道具耐久=UserData[id].物品[道具id].耐久度
	local 道具特效=UserData[id].物品[道具id].特效
	local 道具名称=UserData[id].物品[道具id].名称
	local 格子序号=0
	if 道具类型=="武器" then
		格子序号=23
	elseif 道具类型=="头盔" then
		格子序号=21
	elseif 道具类型=="腰带" then
		格子序号=25
	elseif 道具类型=="项链" then
		格子序号=22
	elseif 道具类型=="鞋子" then
		格子序号=26
	elseif 道具类型=="衣服" then
		格子序号=24
	end
	if 格子序号==0 then
		SendMessage(UserData[id].连接id,7,"#Y/此道具无法佩戴")
		SendMessage(UserData[id].连接id,3006,"66")
		return 0
	elseif 道具耐久 <= 0 then
		SendMessage(UserData[id].连接id,7,"#Y/此道具的耐久度为0，已经无法佩戴了")
		SendMessage(UserData[id].连接id,3006,"66")
		return 0
	elseif UserData[id].物品[道具id].鉴定 ~= true then
		SendMessage(UserData[id].连接id,7,"#Y/此道具还未鉴定无法穿戴")
		SendMessage(UserData[id].连接id,3006,"66")
		return 0
	 elseif UserData[id].物品[道具id].专用 ~= nil and UserData[id].物品[道具id].专用~= id then
		SendMessage(UserData[id].连接id,7,"#Y/你无法佩戴他人的专用装备")
		SendMessage(UserData[id].连接id,3006,"66")
		return 0
	elseif UserData[id].物品[道具id].Time and UserData[id].物品[道具id].Time <= os.time() then 
		UserData[id].物品[道具id] =nil
		UserData[id].角色.道具.包裹[道具格子] =nil
		SendMessage(UserData[id].连接id, 7, "#y/你的道具已经过期被系统回收")
		SendMessage(UserData[id].连接id, 3007,{格子=道具格子,数据=UserData[id].物品[UserData[id].角色.道具.包裹[道具格子]]})
	else
		if self:取等级要求zz(编号,道具等级,道具特效)==false then
			SendMessage(UserData[id].连接id,7,"#Y/你当前的等级无法佩戴这样的装备")
			SendMessage(UserData[id].连接id,3006,"66")
			return 0
		else
			local lssx = 取属性(self.数据[编号].模型)
			local zblx = "无"
			if 道具类型 == "武器" then
				zblx=取武器类型(道具名称)
			elseif 道具类型=="衣服" or 道具类型=="头盔" then
				zblx=取防具性别(道具名称)
			end
			if 道具类型=="武器" and (zblx ~= lssx.武器[1]) and (zblx ~= lssx.武器[2]) then
				SendMessage(UserData[id].连接id,7,"#Y/该助战角色无法佩戴此种武器")
				SendMessage(UserData[id].连接id,3006,"66")
				return 0
			elseif 道具类型 ~="武器" and zblx ~= "无" and self.数据[编号].性别 ~= zblx then
				SendMessage(UserData[id].连接id,7,"#Y/该助战角色不适合佩戴此种装备")
				SendMessage(UserData[id].连接id,3006,"66")
				return 0
			else
				if self.数据[编号].装备数据 == nil then
					self.数据[编号].装备数据 = {}
				end
				local 临时装备 = self.数据[编号].装备数据[格子序号]
				self.数据[编号].装备数据[格子序号] = 道具id
				UserData[self.id].角色.道具["包裹"][道具格子] = 临时装备
				self:刷新装备属性(编号)
				self:发送指定助战物品(编号)
				SendMessage(UserData[id].连接id,3006,"66")
			end
		end
	end
end

function 助战系统:刷新装备属性(编号)
	local id = self.id
	self.数据[编号].装备属性 = {}
		self.数据[编号].愤怒特效 = false
		self.数据[编号].特技数据 = {}
	local 临时追加 = {}
	local 临时变身 = {}
	local 临时附加 = {}
	for n,v in pairs(self.数据[编号].装备数据) do
		if UserData[id].物品[v] ~= nil and UserData[id].物品[v].耐久度 ~= nil and UserData[id].物品[v].耐久度 > 0 then
			if UserData[id].物品[v].Time and UserData[id].物品[v].Time <= os.time() then
				UserData[id].物品[v] =nil
				self.数据[编号].装备数据[n]=nil
				SendMessage(UserData[self.id].连接id, 7, "#y/你的道具已经过期被系统回收")
			else
				if UserData[id].物品[v].装备属性 ~= nil then
					local 幻化类型 = UserData[id].物品[v].装备属性.基础.类型
					self:添加装备属性(编号,幻化类型,UserData[id].物品[v].装备属性.基础.数值)
					self:添加装备属性(编号,幻化类型,UserData[id].物品[v].装备属性.基础.强化)
					for i = 1, #UserData[id].物品[v].装备属性.附加 do
						幻化类型 = UserData[id].物品[v].装备属性.附加[i].类型
						self:添加装备属性(编号,幻化类型,UserData[id].物品[v].装备属性.附加[i].数值)
						self:添加装备属性(编号,幻化类型,UserData[id].物品[v].装备属性.附加[i].强化)
					end
				end
				if UserData[id].物品[v].套装 ~= nil then
					local 套装名称 = UserData[id].物品[v].套装.名称
					if UserData[id].物品[v].套装.类型 == "追加法术" then
						if 临时追加[套装名称] == nil then
							临时追加[套装名称] = 1
						else
							临时追加[套装名称] = 临时追加[套装名称] + 1
						end
					elseif UserData[id].物品[v].套装.类型 == "变身术" then
						if 临时变身[套装名称] == nil then
							临时变身[套装名称] = 1
						else
							临时变身[套装名称] = 临时变身[套装名称] + 1
						end
					elseif 临时附加[套装名称] == nil then
						临时附加[套装名称] = 1
					else
						临时附加[套装名称] = 临时附加[套装名称] + 1
					end
				end
				if UserData[id].物品[v].熔炼~=nil then
					for i=1,#UserData[id].物品[v].熔炼 do
						self:添加装备属性(编号,UserData[id].物品[v].熔炼[i].类型,UserData[id].物品[v].熔炼[i].属性)
					end
				end
				if UserData[id].物品[v].附魔~=nil then
					if UserData[id].物品[v].附魔.时间 <= os.time() then
						UserData[id].物品[v].附魔 = nil
					else
						self:添加装备属性(编号,UserData[id].物品[v].附魔.类型,UserData[id].物品[v].附魔.属性)
					end
				end
				if UserData[id].物品[v].特殊属性 then
					self:添加装备属性(编号,UserData[id].物品[v].特殊属性.类型,UserData[id].物品[v].特殊属性.属性)
				end
				if UserData[id].物品[v].特效 ~= nil then
					for i = 1, #UserData[id].物品[v].特效 do
						if UserData[id].物品[v].特效[i] == "愤怒" then
							self.数据[编号].愤怒特效 = true
						end
					end
				end
				if UserData[id].物品[v].特技 ~= nil then
					local 特技重复 = false
					for i = 1, #self.数据[编号].特技数据 do
						if self.数据[编号].特技数据[i] ~= nil and self.数据[编号].特技数据[i].名称 == UserData[id].物品[v].特技 then
							self.特技重复 = true
						end
					end
					if 特技重复 == false then
						self.数据[编号].特技数据[#self.数据[编号].特技数据 + 1] =	置技能(UserData[id].物品[v].特技)
					end
				end
				if UserData[id].物品[v].三围属性 == nil then
					UserData[id].物品[v].三围属性 = {}
				end
				if UserData[id].物品[v].临时效果 ~= nil then
					for i=#UserData[id].物品[v].临时效果,1,-1 do
			
						if UserData[id].物品[v].临时效果[i].时间 <= os.time() then
							table.remove(UserData[id].物品[v].临时效果, i)
						else
							self:添加装备属性(编号,UserData[id].物品[v].临时效果[i].类型,UserData[id].物品[v].临时效果[i].数值)
						end
					end
				end
				for i = 1, #UserData[id].物品[v].三围属性 do
					local 临时数值 = UserData[id].物品[v].三围属性[i].数值
					local 临时类型 = UserData[id].物品[v].三围属性[i].类型
					self:添加装备属性(编号,临时类型,临时数值)
					if UserData[id].物品[v].升星	and UserData[id].物品[v].升星> 0 then 
						self:添加装备属性(编号,临时类型,UserData[id].物品[v].升星*10)
					end
				end
				for i = 1, #UserData[id].物品[v].锻造数据 do
					self:添加装备属性(编号,UserData[id].物品[v].锻造数据[i].类型,UserData[id].物品[v].锻造数据[i].数值)
				end
				--符石
				if UserData[id].物品[v].符石~= nil and #UserData[id].物品[v].符石 > 0 then
					if UserData[id].物品[v].符石.组合 ~= nil then
						local 符石名称 = UserData[id].物品[v].符石.组合.名称
						local 符石等级 = UserData[id].物品[v].符石.组合.等级
						if 符石名称 == "万丈霞光" then
							if 符石等级 ==1	then
								self:添加装备属性(编号,"气血回复效果",50)
							elseif	符石等级 ==2	then
								self:添加装备属性(编号,"气血回复效果",80)
							elseif	符石等级 ==3	then
								self:添加装备属性(编号,"气血回复效果",120)
							elseif	符石等级 ==4	then
								self:添加装备属性(编号,"气血回复效果",200)
							end
						elseif 符石名称 == "望穿秋水" then
							self:添加装备属性(编号,"灵力",60)
						elseif 符石名称 == "无懈可击" then
							self:添加装备属性(编号,"防御",30)
						elseif 符石名称 == "万里横行" then
							self:添加装备属性(编号,"伤害",40)
						elseif 符石名称 == "日落西山" then
							self:添加装备属性(编号,"速度",40)
						elseif 符石名称 == "高山流水" then
							if 符石等级 ==1	then
								self:添加装备属性(编号,"法术伤害",math.floor(self.数据[编号].等级/3+300))
							elseif	符石等级 ==2	then
								self:添加装备属性(编号,"法术伤害",math.floor(self.数据[编号].等级/2+300))
							elseif	符石等级 ==3	then
								self:添加装备属性(编号,"法术伤害",math.floor(self.数据[编号].等级+300))
							end
						elseif 符石名称 == "百无禁忌" then
							if 符石等级 ==1	then
								self:添加装备属性(编号,"抵抗封印等级",math.floor(4*100*self.数据[编号].等级/1000))
							elseif	符石等级 ==2	then
								self:添加装备属性(编号,"抵抗封印等级",math.floor(8*100*self.数据[编号].等级/1000))
							elseif	符石等级 ==3	then
								self:添加装备属性(编号,"抵抗封印等级",math.floor(12*100*self.数据[编号].等级/1000))
							end
						end
					end
					for i=1,#UserData[id].物品[v].符石 do
						if UserData[id].物品[v].符石[i]~=nil and	UserData[id].物品[v].符石[i].耐久>0 then
							local Name = UserData[id].物品[v].符石[i].名称
							for i,v in ipairs({"气血","魔法","命中","伤害","防御","速度","灵力","敏捷","法术防御","法术伤害","固定伤害","力量","体质","耐力","魔力"}) do
								if ItemData[Name].属性[v] then
									self:添加装备属性(编号,v,ItemData[Name].属性[v])
								end
							end
						end
					end				--装备
				end
				if n == 23 then
					if UserData[id].物品[v].升星 and UserData[id].物品[v].升星> 0 then 
						self:添加装备属性(编号,"伤害",math.floor(UserData[id].物品[v].伤害 * (UserData[id].物品[v].升星/10)))
						self:添加装备属性(编号,"命中",math.floor(UserData[id].物品[v].命中 * (UserData[id].物品[v].升星/10)))
					end
					self:添加装备属性(编号,"伤害",UserData[id].物品[v].伤害)
					self:添加装备属性(编号,"命中",UserData[id].物品[v].命中)
					self.数据[编号].武器数据 = {
						强化 = 0,
						名称 = UserData[id].物品[v].名称,
						类别 = 取武器类型(UserData[id].物品[v].名称),
						等级 = UserData[id].物品[v].等级
					}
					if UserData[id].物品[v].强化 == nil then
						self.数据[编号].武器数据.强化 = nil
					else
						self.数据[编号].武器数据.强化 = 1
					end
					if UserData[id].物品[v].染色 then
						self.数据[编号].武器数据.染色 = table.copy(UserData[id].物品[v].染色)
						self:添加装备属性(编号,UserData[id].物品[v].染色.类型,UserData[id].物品[v].染色.属性)
					end
				elseif n == 21 then
					if UserData[id].物品[v].升星	and UserData[id].物品[v].升星> 0 then 
						self:添加装备属性(编号,"魔法",math.floor(UserData[id].物品[v].魔法 * (UserData[id].物品[v].升星/10)))
						self:添加装备属性(编号,"防御",math.floor(UserData[id].物品[v].防御 * (UserData[id].物品[v].升星/10)))
					end
					self:添加装备属性(编号,"魔法",UserData[id].物品[v].魔法)
					self:添加装备属性(编号,"防御",UserData[id].物品[v].防御)
				elseif n == 25 then
					if UserData[id].物品[v].升星 and UserData[id].物品[v].升星> 0 then 
						self:添加装备属性(编号,"气血",math.floor(UserData[id].物品[v].气血 * (UserData[id].物品[v].升星/10)))
						self:添加装备属性(编号,"防御",math.floor(UserData[id].物品[v].防御 * (UserData[id].物品[v].升星/10)))
					end
					self:添加装备属性(编号,"气血",UserData[id].物品[v].气血)
					self:添加装备属性(编号,"防御",UserData[id].物品[v].防御)
				elseif n == 22 then
					if UserData[id].物品[v].升星 and UserData[id].物品[v].升星> 0 then 
						self:添加装备属性(编号,"灵力",math.floor(UserData[id].物品[v].灵力 * (UserData[id].物品[v].升星/10)))
					end
					self:添加装备属性(编号,"灵力",UserData[id].物品[v].灵力)
					self:添加装备属性(编号,"法防",self.数据[编号].装备属性.灵力)
				elseif n == 26 then
					if UserData[id].物品[v].升星 and UserData[id].物品[v].升星> 0 then 
						self:添加装备属性(编号,"敏捷",math.floor(UserData[id].物品[v].敏捷 * (UserData[id].物品[v].升星/10)))
						self:添加装备属性(编号,"防御",math.floor(UserData[id].物品[v].防御 * (UserData[id].物品[v].升星/10)))
					end
					self:添加装备属性(编号,"敏捷",UserData[id].物品[v].敏捷)
					self:添加装备属性(编号,"防御",UserData[id].物品[v].防御)
				elseif n == 24 then
					if UserData[id].物品[v].升星 and UserData[id].物品[v].升星 > 0 then 
						self:添加装备属性(编号,"防御",math.floor(UserData[id].物品[v].防御 * (UserData[id].物品[v].升星/10)))
					end
					self:添加装备属性(编号,"防御",UserData[id].物品[v].防御)
				end
			end
		end
	end
	self.数据[编号].追加技能 = {}
	for n, v in pairs(临时追加) do
		if 临时追加[n] >= 3 then
			self.数据[编号].追加技能[#self.数据[编号].追加技能 + 1] = n
		end
	end
	self.数据[编号].变身技 = {}
	for n, v in pairs(临时变身) do
		if 临时变身[n] >= 3 then
			self.数据[编号].变身技能[#self.数据[编号].变身技能 + 1] = n
		end
	end
	self.数据[编号].附加技能 = {}
	for n, v in pairs(临时附加) do
		if 临时附加[n] >= 3 then
			self.数据[编号].附加技能[#self.数据[编号].附加技能 + 1] = n
		end
	end
	
	self:添加装备属性(编号,"固定伤害",self.数据[编号].辅助技能.暗器技巧 * 2 + self.数据[编号].强化技能.固伤强化)
	self:添加装备属性(编号,"治疗能力",self.数据[编号].强化技能.治疗强化)
	if self.数据[编号].奇经八脉.咒法 then
		self:添加装备属性(编号,"法术伤害",60)
	end
	if self.数据[编号].奇经八脉.锤炼 then
		self:添加装备属性(编号,"伤害",math.floor(self.数据[编号].装备属性.伤害*0.03))
	end
	if self.数据[编号].神器 then
		for i=1,5 do	
			self:添加装备属性(编号,self.数据[编号].神器.属性[i].类型,self.数据[编号].神器.属性[i].数额)
		end
	end
	self:刷新战斗属性(编号,1)
end


function 助战系统:添加装备属性(编号,类型,数值)
	if self.数据[编号].装备属性[类型] == nil then
		self.数据[编号].装备属性[类型] = 0
	end
	self.数据[编号].装备属性[类型] = math.floor(self.数据[编号].装备属性[类型] + 数值)
end

function 助战系统:刷新战斗属性(编号,更新)
	local ret = {"气血","魔法","命中","伤害","防御","速度","躲避","灵力","体质","魔力","力量","耐力","敏捷","法防","躲闪","法术防御"}
	for n,v in pairs(ret) do
		if self.数据[编号].装备属性[v] == nil then
			self.数据[编号].装备属性[v] = 0
		end
		if self.数据[编号].技能属性[v] == nil then
			self.数据[编号].技能属性[v] = 0
		end
	end
	local id = self.id
	local 坐骑力量,坐骑敏捷,坐骑魔力,坐骑耐力,坐骑体质=0,0,0,0,0
	if UserData[id].角色.坐骑.编号 ~= 0 then
		坐骑体质=math.floor(UserData[id].角色.坐骑数据[UserData[id].角色.坐骑.编号].体质*UserData[id].角色.坐骑数据[UserData[id].角色.坐骑.编号].成长/25)
		坐骑力量=math.floor(UserData[id].角色.坐骑数据[UserData[id].角色.坐骑.编号].力量*UserData[id].角色.坐骑数据[UserData[id].角色.坐骑.编号].成长/21)
		坐骑魔力=math.floor(UserData[id].角色.坐骑数据[UserData[id].角色.坐骑.编号].魔力*UserData[id].角色.坐骑数据[UserData[id].角色.坐骑.编号].成长/31)
		坐骑耐力=math.floor(UserData[id].角色.坐骑数据[UserData[id].角色.坐骑.编号].耐力*UserData[id].角色.坐骑数据[UserData[id].角色.坐骑.编号].成长/24)
		坐骑敏捷=math.floor(UserData[id].角色.坐骑数据[UserData[id].角色.坐骑.编号].敏捷*UserData[id].角色.坐骑数据[UserData[id].角色.坐骑.编号].成长/33)
	end
	local 临时五围 = {体质=0,魔力=0,力量=0,耐力=0,敏捷=0}
	for n,v in pairs(临时五围) do
		临时五围[n] = self.数据[编号][n] + self.数据[编号].装备属性[n] + self.数据[编号].技能属性[n]
	end
	if self.数据[编号].种族=="人" then
		self.数据[编号].伤害=math.floor((临时五围.力量 + 坐骑力量)*0.7)+34
		self.数据[编号].命中=math.floor((临时五围.力量 + 坐骑力量)*2)+30
		self.数据[编号].灵力=math.floor((临时五围.力量 + 坐骑力量)*0.2+(临时五围.魔力 + 坐骑魔力)*0.9+(临时五围.体质 + 坐骑体质)*0.3+(临时五围.耐力 + 坐骑耐力)*0.2)
		self.数据[编号].速度=math.floor((临时五围.敏捷 + 坐骑敏捷)*0.7+(临时五围.力量 + 坐骑力量)*0.1+(临时五围.体质 + 坐骑体质)*0.1+(临时五围.耐力 + 坐骑耐力)*0.1)
		self.数据[编号].躲闪=math.floor(临时五围.敏捷 + 坐骑敏捷)
		self.数据[编号].魔法上限=math.floor((临时五围.魔力 + 坐骑魔力)*3)+80
		self.数据[编号].最大气血=math.floor((临时五围.体质 + 坐骑体质)*5)+100
		self.数据[编号].防御=math.floor((临时五围.耐力 + 坐骑耐力)*1.5)
		self.数据[编号].法防=math.floor(self.数据[编号].灵力 + self.数据[编号].装备属性.灵力 + self.数据[编号].技能属性.灵力)
	elseif self.数据[编号].种族=="魔" then
		self.数据[编号].伤害=math.floor((临时五围.力量 + 坐骑力量)*0.8)+34
		self.数据[编号].命中=math.floor((临时五围.力量 + 坐骑力量)*2.3)+27
		self.数据[编号].防御=math.floor((临时五围.耐力 + 坐骑耐力)*1.3)
		self.数据[编号].灵力=math.floor((临时五围.力量 + 坐骑力量)*0.2+(临时五围.魔力 + 坐骑魔力)*0.9+(临时五围.体质 + 坐骑体质)*0.3+(临时五围.耐力 + 坐骑耐力)*0.2)
		self.数据[编号].速度=math.floor((临时五围.敏捷 + 坐骑敏捷)*0.7+(临时五围.力量 + 坐骑力量)*0.1+(临时五围.体质 + 坐骑体质)*0.1+(临时五围.耐力 + 坐骑耐力)*0.1)
		self.数据[编号].躲闪=math.floor(临时五围.敏捷 + 坐骑敏捷)
		self.数据[编号].魔法上限=math.floor((临时五围.魔力 + 坐骑魔力)*2.5)+80
		self.数据[编号].最大气血=math.floor((临时五围.体质 + 坐骑体质)*6)+100
		self.数据[编号].法防=math.floor(self.数据[编号].灵力 + self.数据[编号].装备属性.灵力 + self.数据[编号].技能属性.灵力)
	elseif self.数据[编号].种族=="仙" then
		self.数据[编号].伤害=math.floor((临时五围.力量 + 坐骑力量)*0.6)+40
		self.数据[编号].命中=math.floor((临时五围.力量 + 坐骑力量)*1.7)+30
		self.数据[编号].防御=math.floor((临时五围.耐力 + 坐骑耐力)*1.6)
		self.数据[编号].速度=math.floor((临时五围.敏捷 + 坐骑敏捷)*0.7+(临时五围.力量 + 坐骑力量)*0.1+(临时五围.体质 + 坐骑体质)*0.1+(临时五围.耐力 + 坐骑耐力)*0.1)
		self.数据[编号].灵力=math.floor((临时五围.力量 + 坐骑力量)*0.2+(临时五围.魔力 + 坐骑魔力)*0.9+(临时五围.体质 + 坐骑体质)*0.3+(临时五围.耐力 + 坐骑耐力)*0.2)
		self.数据[编号].躲闪=math.floor(临时五围.敏捷 + 坐骑敏捷)
		self.数据[编号].魔法上限=math.floor((临时五围.魔力 + 坐骑魔力)*3.5)+80
		self.数据[编号].最大气血=math.floor((临时五围.体质 + 坐骑体质)*4.5)+100
		self.数据[编号].法防=math.floor(self.数据[编号].灵力 + self.数据[编号].装备属性.灵力 + self.数据[编号].技能属性.灵力)
	end
	
	if self.数据[编号].奇经八脉.狂狷 and self.数据[编号].耐力 > 10 then
		self.数据[编号].伤害=math.floor(self.数据[编号].伤害+(临时五围.力量/self.数据[编号].耐力)*16)
	end
	if self.数据[编号].奇经八脉.神附 then
		self.数据[编号].伤害=math.floor(self.数据[编号].伤害+(临时五围.力量*0.08))
	end
	if self.数据[编号].奇经八脉.海沸 then
		self.数据[编号].伤害=math.floor(self.数据[编号].伤害+(临时五围.力量*0.08))
	end
	if self.数据[编号].奇经八脉.夜行 then
		self.数据[编号].伤害=math.floor(self.数据[编号].伤害+40)
		self.数据[编号].速度=math.floor(self.数据[编号].速度+40)
	end
	if self.数据[编号].奇经八脉.倩影 then
		self.数据[编号].速度=self.数据[编号].速度+50
	end
	self.数据[编号].躲闪=self.数据[编号].躲闪+self.数据[编号].装备属性.躲闪+self.数据[编号].技能属性.躲避
	self.数据[编号].命中 =	math.floor(self.数据[编号].命中 + self.数据[编号].装备属性.命中+self.数据[编号].技能属性.命中)
	self.数据[编号].伤害 = math.floor(self.数据[编号].伤害 + self.数据[编号].装备属性.伤害 + self.数据[编号].命中 * 0.35 +self.数据[编号].技能属性.伤害)
	self.数据[编号].防御 =	math.floor(self.数据[编号].防御 + self.数据[编号].装备属性.防御 + 5 +self.数据[编号].技能属性.防御)
	self.数据[编号].法防 = math.floor(self.数据[编号].法防	+self.数据[编号].装备属性.法防 * 2 + 临时五围.魔力 * 0.35 + self.数据[编号].灵力 * 0.25+self.数据[编号].技能属性.法防)
	self.数据[编号].灵力 = math.floor(self.数据[编号].灵力 + self.数据[编号].装备属性.灵力 +self.数据[编号].技能属性.灵力)
	self.数据[编号].速度 = math.floor(self.数据[编号].速度 + self.数据[编号].装备属性.速度 +self.数据[编号].技能属性.速度)
	self.数据[编号].魔法上限 = math.floor((self.数据[编号].魔法上限 + self.数据[编号].装备属性.魔法+self.数据[编号].技能属性.魔法)) 
	self.数据[编号].最大气血 = math.floor((self.数据[编号].最大气血 + self.数据[编号].装备属性.气血+self.数据[编号].技能属性.气血))
	--self.数据[编号].最大气血 = math.floor(self.数据[编号].最大气血 * (1 + 0.01*self.数据[编号].辅助技能.强身术))
	--self.数据[编号].魔法上限 = math.floor(self.数据[编号].魔法上限 * (1 + 0.01 *self.数据[编号].辅助技能.冥想))
	
	if self.数据[编号].奇经八脉.护佑 then
		self.数据[编号].防御=self.数据[编号].防御+50
		self.数据[编号].法防=self.数据[编号].法防+50
	end
	if self.数据[编号].变身 ~= nil and self.数据[编号].变身.属性.类型 ~= 0	and self.数据[编号].变身.造型~=nil then ---1类型 2 属性 3 数值
		if self.数据[编号].变身.属性.类型 == 1 then
			self.数据[编号][self.数据[编号].变身.属性.属性] =self.数据[编号][self.数据[编号].变身.属性.属性]+self.数据[编号].变身.属性.数值
		elseif self.数据[编号].变身.属性.类型 == 2 then
			self.数据[编号][self.数据[编号].变身.属性.属性] =math.floor(self.数据[编号][self.数据[编号].变身.属性.属性]+self.数据[编号][self.数据[编号].变身.属性.属性]*self.数据[编号].变身.属性.数值)
		elseif self.数据[编号].变身.属性.类型 == 3 then
			self.数据[编号][self.数据[编号].变身.属性.属性] =math.floor(self.数据[编号][self.数据[编号].变身.属性.属性]-self.数据[编号][self.数据[编号].变身.属性.属性]*self.数据[编号].变身.属性.数值)
		end
	end
	
	if LabelData[self.数据[编号].当前称谓] and LabelData[self.数据[编号].当前称谓].属性 then
		for k,v in pairs(LabelData[self.数据[编号].当前称谓].属性) do
			self.数据[编号][k]=self.数据[编号][k]+v
		end
	end
	if #self.数据[编号].锦衣 > 0 then
		self.数据[编号].最大气血=self.数据[编号].最大气血+ 1000 * #self.数据[编号].锦衣
	end
	
	self.数据[编号].气血上限 = self.数据[编号].最大气血
	if self.数据[编号].飞升	then
		self.数据[编号].最大气血= self.数据[编号].最大气血+500
	end

	self.数据[编号].气血上限 = self.数据[编号].最大气血
	if self.数据[编号].渡劫	then
		self.数据[编号].最大气血= self.数据[编号].最大气血+1000
	end
	if self.数据[编号].辅助技能 ~= nil then
		if self.数据[编号].辅助技能.强身术 ~= nil and self.数据[编号].辅助技能.强身术 > 0 then
			self.数据[编号].最大气血 = math.floor(self.数据[编号].最大气血 * (0.01*self.数据[编号].辅助技能.强身术 + 1))
		end
		if self.数据[编号].辅助技能.冥想 ~= nil and self.数据[编号].辅助技能.冥想 > 0 then
			self.数据[编号].魔法上限 = math.floor(self.数据[编号].魔法上限 * (0.01*self.数据[编号].辅助技能.冥想 + 1))
		end
		if self.数据[编号].辅助技能.强壮 ~= nil and self.数据[编号].辅助技能.强壮 > 0 then
			self.数据[编号].最大气血 = math.floor(self.数据[编号].最大气血 + (4*self.数据[编号].辅助技能.强壮))
		end
	end
	
	if self.数据[编号].强化技能 ~= nil then
		if self.数据[编号].强化技能.命中强化 ~= nil and self.数据[编号].强化技能.命中强化 > 0 then
			self.数据[编号].命中 = math.floor(self.数据[编号].命中 + (3*self.数据[编号].强化技能.命中强化))
		end
		if self.数据[编号].强化技能.伤害强化 ~= nil and self.数据[编号].强化技能.伤害强化 > 0 then
			self.数据[编号].伤害 = math.floor(self.数据[编号].伤害 + (1.5*self.数据[编号].强化技能.伤害强化))
		end
		if self.数据[编号].强化技能.防御强化 ~= nil and self.数据[编号].强化技能.防御强化 > 0 then
			self.数据[编号].防御 = math.floor(self.数据[编号].防御 + (2*self.数据[编号].强化技能.防御强化))
		end
		if self.数据[编号].强化技能.灵力强化 ~= nil and self.数据[编号].强化技能.灵力强化 > 0 then
			self.数据[编号].灵力 = math.floor(self.数据[编号].灵力 + self.数据[编号].强化技能.灵力强化)
		end
		if self.数据[编号].强化技能.速度强化 ~= nil and self.数据[编号].强化技能.速度强化 > 0 then
			self.数据[编号].速度 = math.floor(self.数据[编号].速度 + self.数据[编号].强化技能.速度强化)
		end
	end
	self.数据[编号].气血上限 = self.数据[编号].最大气血
	if self.数据[编号].气血上限 == nil then
		self.数据[编号].气血上限 = self.数据[编号].最大气血
	end
	if self.数据[编号].当前气血 == nil then
		self.数据[编号].当前气血 = self.数据[编号].气血上限
	end
	if self.数据[编号].当前魔法 == nil then
		self.数据[编号].当前魔法 = self.数据[编号].魔法上限
	end
	if self.数据[编号].最大气血 < self.数据[编号].气血上限 then
		self.数据[编号].气血上限 = self.数据[编号].最大气血
	end
	if self.数据[编号].气血上限 < self.数据[编号].当前气血 then
		self.数据[编号].当前气血 = self.数据[编号].气血上限
	end
	if self.数据[编号].魔法上限 < self.数据[编号].当前魔法 then
		self.数据[编号].当前魔法 = self.数据[编号].魔法上限
	end
	if 恢复 == nil then
		self.数据[编号].气血上限 = self.数据[编号].最大气血
		self.数据[编号].当前气血 = self.数据[编号].最大气血
		self.数据[编号].当前魔法 = self.数据[编号].魔法上限
	end
	if 更新 then
		SendMessage(UserData[self.id].连接id,20058,self:取数据())
	end
end


function 助战系统:取等级要求zz(编号,等级,特效)---------------------------完成--
	local 无级别特效=false
	local 简易特效=false
	local 超级简易特效=false
	if 特效~=nil then
		for n=1,#特效 do
			if 特效[n]=="无级别限制" then
				无级别特效=true
			elseif 特效[n]=="超级简易" then
				超级简易特效=true
			elseif 特效[n]=="简易" then
				简易特效=true
			end
		end
	end
	if 无级别特效 then
		return true
	elseif 超级简易特效 and self.数据[编号].等级 + 20>=等级 then
		return true
	elseif 简易特效 and self.数据[编号].等级 + 5>=等级 then
		return true
	elseif self.数据[编号].等级 >= 等级 then
		return true
	else
		return false
	end
 end
 

function 助战系统:新增助战(id,模型)
	for n=1,#self.数据 do
		if #self.数据>=7 then
			常规提示(self.id,"#Y/最多携带7个助战")
			return
		end
	end
	local ls = self:助战信息(模型)
 	local cs = self:取初始属性(ls.种族)
	self.数据[#self.数据+1]={
		等级 = 0,
		名称 = ls.模型,
		性别 = ls.性别,
		模型 = ls.模型,
		种族 = ls.种族,
		编号=#self.数据,
		识别码=self.id..os.time()..取随机数(10000000,99999999)..随机序列,
		称谓 = {
		"初出茅庐"
		},
		当前称谓="初出茅庐",
		门派 = "无门派",
		体质 = cs[1],
		魔力 = cs[2],
		力量 = cs[3],
		耐力 = cs[4],
		敏捷 = cs[5],
		物理暴击伤害=0,
		法术暴击伤害=0,
		潜力 = 0,
		愤怒 = 0,
		当前经验 = 0,
		最大经验 = 0,
		装备数据 = {},
		灵饰 = {},
		修炼 = {攻击修炼={当前等级=0,上限等级=0,当前经验=0,升级经验=0},法术修炼={当前等级=0,上限等级=0,当前经验=0,升级经验=0},防御修炼={当前等级=0,上限等级=0,当前经验=0,升级经验=0},抗法修炼={当前等级=0,上限等级=0,当前经验=0,升级经验=0}},
		技能 = {},
		学会技能 = {},
		锦衣 = {},
		特殊技能 = {},
		染色方案 = ls.染色方案,
		染色组 = {0,0,0},
		奇经八脉 = {},
		剩余乾元丹=0,
		乾元丹=0,
		辅助技能={强身术=0,冥想=0,强壮=0,暗器技巧=0},
		强化技能 = {命中强化=0,伤害强化=0,防御强化=0,灵力强化=0,速度强化=0,固伤强化=0,治疗强化=0},
		技能属性 = {},
		装备属性 = {抵抗封印等级=0,法术伤害结果=0,狂暴等级=0,穿刺等级=0,气血回复效果=0,抗物理暴击等级=0,格挡值=0,抗法术暴击等级=0,封印命中等级=0,法术防御=0,固定伤害=0,法术伤害=0,物理暴击等级=0,法术暴击等级=0,物理暴击伤害=0,法术暴击伤害=0,治疗能力=0,气血=0,魔法=0,命中=0,伤害=0,防御=0,速度=0,躲避=0,灵力=0,体质=0,魔力=0,力量=0,耐力=0,敏捷=0},
		武器数据={名称="",子类="",等级=0,染色方案=0,染色组={}},
		参战=false,
		自动=false,
		变身套=0,
		蚩尤元神=0,
		助战等级="伙伴"
		}
	随机序列 = 随机序列 + 1
	self:刷新战斗属性(#self.数据,1)
	常规提示(self.id,"#Y购买助战成功。")
end

function 助战系统:取初始属性(种族)
	local 属性 = {
		人 = {10,10,10,10,10},
		魔 = {12,11,11,8,8},
		仙 = {12,5,11,12,10},
	}
	return 属性[种族]
end

function 助战系统:助战信息(模型)
	local 角色信息 = {
		飞燕女 = {模型="飞燕女",ID=1,染色方案=3,性别="女",种族="人",武器={"双短剑","环圈"}},
		英女侠 = {模型="英女侠",ID=2,染色方案=4,性别="女",种族="人",武器={"双短剑","鞭"}},
		巫蛮儿 = {模型="巫蛮儿",ID=3,染色方案=1,性别="女",种族="人",武器={"宝珠","法杖"}},
		逍遥生 = {模型="逍遥生",ID=4,染色方案=1,性别="男",种族="人",武器={"扇","剑"}},
		剑侠客 = {模型="剑侠客",ID=5,染色方案=2,性别="男",种族="人",武器={"刀","剑"}},
		狐美人 = {模型="狐美人",ID=6,染色方案=7,性别="女",种族="魔",武器={"爪刺","鞭"}},
		骨精灵 = {模型="骨精灵",ID=7,染色方案=8,性别="女",种族="魔",武器={"魔棒","爪刺"}},
		杀破狼 = {模型="杀破狼",ID=8,染色方案=1,性别="男",种族="魔",武器={"宝珠","弓弩"}},
		巨魔王 = {模型="巨魔王",ID=9,染色方案=5,性别="男",种族="魔",武器={"刀","斧钺"}},
		虎头怪 = {模型="虎头怪",ID=10,染色方案=6,性别="男",种族="魔",武器={"斧钺","锤子"}},
		舞天姬 = {模型="舞天姬",ID=11,染色方案=11,性别="女",种族="仙",武器={"飘带","环圈"}},
		玄彩娥 = {模型="玄彩娥",ID=12,染色方案=12,性别="女",种族="仙",武器={"飘带","魔棒"}},
		羽灵神 = {模型="羽灵神",ID=13,染色方案=1,性别="男",种族="仙",武器={"法杖","弓弩"}},
		神天兵 = {模型="神天兵",ID=14,染色方案=9,性别="男",种族="仙",武器={"锤","枪矛"}},
		龙太子 = {模型="龙太子",ID=15,染色方案=10,性别="男",种族="仙",武器={"扇","枪矛"}},
		桃夭夭 = {模型="桃夭夭",ID=16,染色方案=1,性别="女",种族="仙",武器={"灯笼"}},
		偃无师 = {模型="偃无师",ID=17,染色方案=1,性别="男",种族="人",武器={"剑","巨剑"}},
		鬼潇潇 = {模型="鬼潇潇",ID=18,染色方案=2,性别="女",种族="魔",武器={"爪刺","伞"}},
	}
	return 角色信息[模型]
end

function 助战系统:存档(id)
	print("助战系统:存档",id)
	WriteFile([[玩家信息/账号]]..UserData[id].账号..[[/]]..id.."/助战.txt",table.tostring(self.数据))
end


function 助战系统:取数据()
	for n = 1,#self.数据 do
		if self.数据[n].识别码 == nil then
			self.数据[n].识别码 = self.id..os.time()..取随机数(10000000,99999999)..随机序列
			随机序列 = 随机序列 + 1
		end
		if self.数据[n].剩余乾元丹 == nil then
			self.数据[n].剩余乾元丹 = 0
		end
		if self.数据[n].乾元丹 == nil then
			self.数据[n].乾元丹 = 0
		end
		if self.数据[n].学会技能 == nil then
			self.数据[n].学会技能= {}
		end
		if self.数据[n].锦衣名称 == nil then
			self.数据[n].锦衣名称= {}
		end
		self.数据[n].锦衣名称= {}
		if self.数据[n].锦衣==nil then
			self.数据[n].锦衣={}
		end
		if self.数据[n].锦衣[1]~=nil then
			self.数据[n].锦衣名称[1]=table.loadstring(table.tostring(UserData[self.id].物品[self.数据[n].锦衣[1]]))
		end
		if self.数据[n].锦衣[2]~=nil then
			self.数据[n].锦衣名称[2]=table.loadstring(table.tostring(UserData[self.id].物品[self.数据[n].锦衣[2]]))
		end
		if self.数据[n].锦衣[3]~=nil then
			self.数据[n].锦衣名称[3]=table.loadstring(table.tostring(UserData[self.id].物品[self.数据[n].锦衣[3]]))
		end
		if self.数据[n].锦衣[4]~=nil then
			self.数据[n].锦衣名称[4]=table.loadstring(table.tostring(UserData[self.id].物品[self.数据[n].锦衣[4]]))
		end
	end
	--self:存档(self.id)
	return self.数据
end


function 助战系统:取助战编号(识别码)
	for n,v in pairs(self.数据) do
		if v.识别码 == 识别码 then
			return n
		end
	end
end

function 助战系统:发送指定助战数据(编号)
	if self.数据[编号] ~= nil then
		SendMessage(UserData[self.id].连接id,20060,self.数据[编号])
	end
end

function 助战系统:添加经验(编号,数额)
	local 刷新 = false
	if self.数据[编号] == nil then
		常规提示(self.id,"#Y助战数据异常。")
		return
	end
	self.数据[编号].当前经验 = self.数据[编号].当前经验 + 数额
	while (self.数据[编号].当前经验 >= self.数据[编号].最大经验 ) do
		if self.数据[编号].等级 >= UserData[self.id].角色.等级 + 5 then
			break
		end
		self:助战升级(编号)
		刷新 = true
		常规提示(self.id,"#W/你的助战#R/"..self.数据[编号].名称.."#W/等级提升到了#R/"..self.数据[编号].等级.."#W/级")
	end
	if 刷新 then
		self:刷新战斗属性(编号,1)
	end
end

function 助战系统:助战升级(编号)
	if self.数据[编号] == nil then
		常规提示(self.id,"#Y助战数据异常。")
		return
	end
	self.数据[编号].等级 = self.数据[编号].等级 + 1
	self.数据[编号].体质 = self.数据[编号].体质 + 1
	self.数据[编号].魔力 = self.数据[编号].魔力 + 1
	self.数据[编号].力量 = self.数据[编号].力量 + 1
	self.数据[编号].耐力 = self.数据[编号].耐力 + 1
	self.数据[编号].敏捷 = self.数据[编号].敏捷 + 1
	self.数据[编号].潜力 = self.数据[编号].潜力 + 5
	self.数据[编号].当前经验 = self.数据[编号].当前经验 - self.数据[编号].最大经验
	for n,v in pairs({"攻击修炼","法术修炼","防御修炼","抗法修炼"}) do
		local lv = math.floor((self.数据[编号].等级 - 45)/5+0.5) + 4
		if lv < 4 then
			lv = 4
		end
		self.数据[编号].修炼[v].上限等级 = lv
		self.数据[编号].修炼[v].升级经验 = 计算修炼等级经验(self.数据[编号].修炼[v].当前等级,self.数据[编号].修炼[v].上限等级)
	end
	if self.数据[编号].等级 <= 174 then
		self.数据[编号].最大经验 = self:取升级经验(编号)
	end
end

function 助战系统:取升级经验(编号)
	if self.数据[编号] == nil then
		常规提示(self.id,"#Y助战数据异常。")
		return
	end
	return 1000*(self.数据[编号].等级^((self.数据[编号].等级/100+1)*((175-self.数据[编号].等级)/1000+1)))*2
end


function 助战系统:发送指定助战物品(编号)
	if self.数据[编号] ~= nil then
		SendMessage(UserData[self.id].连接id,20059,{识别码=self.数据[编号].识别码,门派=self.数据[编号].门派,模型=self.数据[编号].模型,装备=self:取装备数据(编号),锦衣=self:取锦衣数据(UserData[self.id].连接id,self.id,编号),道具=ItemControl:索要道具2(self.id,"包裹"),灵饰=self:取灵饰数据(编号),技能=self:取技能数据(编号),助战修炼 = self.数据[编号].修炼})
	end
end


function 助战系统:获取战斗数据(识别码)
	local 编号 = self:取助战编号(识别码)
	local SendMessage = {
    名称 = self.数据[编号].名称,
    id = self.id,
    愤怒 = self.数据[编号].愤怒,
    门派 = self.数据[编号].门派,
    宠物={},
   -- 战斗锦衣=self.数据[编号].锦衣数据.战斗锦衣,
    染色方案=self.数据[编号].染色方案,
    装备属性=self.数据[编号].装备属性,
    奇经八脉 =self.数据[编号].奇经八脉,
    特技数据=self.数据[编号].特技数据,
    当前气血 = self.数据[编号].当前气血,
    气血上限 = self.数据[编号].气血上限,
    最大气血 = self.数据[编号].最大气血,
    当前魔法 = self.数据[编号].当前魔法,
    法防=self.数据[编号].法防,
    魔法上限 = self.数据[编号].魔法上限,
    伤害 = self.数据[编号].伤害,
     追加技能=self.数据[编号].追加技能,
     变身技能=self.数据[编号].变身技能,
     附加技能=self.数据[编号].附加技能,
    法宝={},
    主动技能={},
    乾元丹技能={},
    愤怒特效 = self.数据[编号].愤怒特效,
    命中 = self.数据[编号].命中,
    防御 = self.数据[编号].防御,
    灵力 = self.数据[编号].灵力,
    速度 = self.数据[编号].速度,
    躲闪 = self.数据[编号].躲闪,
    躲避 = self.数据[编号].躲闪,
    染色 = self.数据[编号].染色组,
    等级 = self.数据[编号].等级,
    造型 = self.数据[编号].模型,
    武器数据 = self.数据[编号].武器数据,
    默认法术 = self.数据[编号].默认法术,
    自动 = self.数据[编号].自动,
    修炼数据={}}

	for n = 1, #全局变量.基础属性 do
		SendMessage[全局变量.基础属性[n]] = self.数据[编号][全局变量.基础属性[n]]
	end
	if self.数据[编号].学会技能~=nil then
		for i=1,#self.数据[编号].学会技能 do
			if self.数据[编号].学会技能[i].种类 ~= 0 and self.数据[编号].学会技能[i].种类~= 12 then
				if self.数据[编号].学会技能[i].种类 == 10  then
					SendMessage.主动技能[#SendMessage.主动技能+1]={名称=self.数据[编号].学会技能[i].名称,等级=self.数据[编号].等级,种类=self.数据[编号].学会技能[i].种类}
				else
					SendMessage.主动技能[#SendMessage.主动技能+1]={名称=self.数据[编号].学会技能[i].名称,等级=self.数据[编号].学会技能[i].等级,种类=self.数据[编号].学会技能[i].种类}
				end
			end
		end
	end
	if self.数据[编号].乾元丹技能~=nil then
		for i=1,#self.数据[编号].乾元丹技能 do
			if self.数据[编号].乾元丹技能[i].种类~= 0 and self.数据[编号].乾元丹技能[i].种类~= 12 then
				if self.数据[编号].乾元丹技能[i].种类== 10  then
					SendMessage.主动技能[#SendMessage.主动技能+1]={名称=self.数据[编号].乾元丹技能[i].名称,等级=self.数据[编号].等级,种类=self.数据[编号].乾元丹技能[i].种类}
				else
					SendMessage.主动技能[#SendMessage.主动技能+1]={名称=self.数据[编号].乾元丹技能[i].名称,等级=self.数据[编号].乾元丹技能[i].等级,种类=self.数据[编号].乾元丹技能[i].种类}
				end
			end
		end
	end
	SendMessage.修炼数据.法术 =self.数据[编号].修炼.法术修炼.当前等级
	SendMessage.修炼数据.法抗 =self.数据[编号].修炼.抗法修炼.当前等级
	SendMessage.修炼数据.攻击 =self.数据[编号].修炼.攻击修炼.当前等级
	SendMessage.修炼数据.防御 =self.数据[编号].修炼.防御修炼.当前等级
	return SendMessage
end






return 助战系统