-- @Author: 80后痴汉
-- @Date:	2021-10-25 00:54:26
-- @Last Modified by:	作者QQ2124872
-- @Last Modified time: 2021-10-25 10:34:10
-- @GGELUA游戏爱好者基地 https://www.ggelua.cc
-- @GGELUA游戏爱好者基地 https://bbs.ggelua.cc
-- @GGELUA游戏爱好者交流Q群 560529474
-- @GGELUA声明：编写此脚本只因个人兴趣爱好，如若放出，仅供个人用于学习、研究;不得用于商业用途。





function RoleControl:增加元神突破(id)
	if UserData[id].角色.元神 == nil then
		UserData[id].角色.元神 = 0
    end
	if UserData[id].角色.元神 >= 5 then
		常规提示(id,"#Y当前元神已提升到满阶。")
		return
	end
	local 突破消耗 = {}
	for n = 1,3 do
		if SpiritData[UserData[id].角色.门派]["突破"..(UserData[id].角色.元神+1).."介消耗类型"..n] ~= 0 then
			突破消耗[#突破消耗+1] = {
				类型 = SpiritData[UserData[id].角色.门派]["突破"..(UserData[id].角色.元神+1).."介消耗类型"..n],
				名称 = SpiritData[UserData[id].角色.门派]["突破"..(UserData[id].角色.元神+1).."介消耗名称"..n],
				数量 = SpiritData[UserData[id].角色.门派]["突破"..(UserData[id].角色.元神+1).."介消耗数量"..n]
				}
		end
	end
	if #突破消耗 > 0 then
		for n = 1,#突破消耗 do
			if 突破消耗[n].类型 == 1 then
				if 突破消耗[n].名称 == "银子" then
					if UserData[id].角色.道具.货币.银子 < 突破消耗[n].数量 then
						常规提示(id,"#Y本次突破元神需消耗"..突破消耗[n].数量..突破消耗[n].名称)
						return
					end
				elseif 突破消耗[n].名称 == "储备" then
					if UserData[id].角色.道具.货币.储备 < 突破消耗[n].数量 then
						常规提示(id,"#Y本次突破元神需消耗"..突破消耗[n].数量..突破消耗[n].名称)
						return
					end
				elseif 突破消耗[n].名称 == "仙玉" then
					if tonumber(f函数.读配置(ServerDirectory.."玩家信息/账号" .. UserData[id].账号 .. "/账号.txt", "账号信息", "仙玉")) < 突破消耗[n].数量 then
						常规提示(id,"#Y本次突破元神需消耗"..突破消耗[n].数量..突破消耗[n].名称)
						return
					end
				end
			elseif 突破消耗[n].类型 == 2 then
				local 道具数量 = ItemControl:ItemCount(id,突破消耗[n].名称)
				if 道具数量 < 突破消耗[n].数量 then
					常规提示(id,"#Y本次突破元神需消耗"..突破消耗[n].数量.."个"..突破消耗[n].名称)
					return
				end
			end
		end
		for n = 1,#突破消耗 do
			if 突破消耗[n].类型 == 1 then
				if 突破消耗[n].名称 == "银子" then
					RoleControl:扣除银子(UserData[id],突破消耗[n].数量,"突破"..(UserData[id].角色.元神+1).."阶元神")
				elseif 突破消耗[n].名称 == "储备" then
					RoleControl:扣除银子(UserData[id],突破消耗[n].数量,"突破"..(UserData[id].角色.元神+1).."阶元神",1)
				elseif 突破消耗[n].名称 == "仙玉" then
					RoleControl:扣除仙玉(UserData[id],突破消耗[n].数量,"突破"..(UserData[id].角色.元神+1).."阶元神")
				end
			elseif 突破消耗[n].类型 == 2 then
				ItemControl:TakeItem(id,突破消耗[n].名称,突破消耗[n].数量)
			end
		end
	end
	UserData[id].角色.元神 = UserData[id].角色.元神 + 1
	self:刷新元神(id)
end


function RoleControl:刷新元神(id)
	if UserData[id].角色.门派 == "无门派" or UserData[id].角色.门派 == "无" then
		常规提示(id,"#Y/请先加入门派才能查看")
		return
    end
    local 发送内容={}
	if UserData[id].角色.元神 == nil then
		UserData[id].角色.元神=0
    end
    发送内容.层数 = UserData[id].角色.元神
    发送内容.门派 = UserData[id].角色.门派
	发送数据(UserData[id].连接id,20069,发送内容)
end
