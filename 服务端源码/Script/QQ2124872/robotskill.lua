-- @Author: 80后痴汉
-- @Date:	2021-10-25 00:54:26
-- @Last Modified by:	作者QQ2124872
-- @Last Modified time: 2021-10-25 10:34:10
-- @GGELUA游戏爱好者基地 https://www.ggelua.cc
-- @GGELUA游戏爱好者基地 https://bbs.ggelua.cc
-- @GGELUA游戏爱好者交流Q群 560529474
-- @GGELUA声明：编写此脚本只因个人兴趣爱好，如若放出，仅供个人用于学习、研究;不得用于商业用途。





local robotskill = class()
local floor = math.floor

function robotskill:学会技能(玩家id,zz,id,gz)-----------------------------------------------------
	if zz.师门技能[id] ~= nil then
		for s=1,#zz.师门技能[id].包含技能 do
			if zz.师门技能[id].包含技能[s].名称 == gz and not self:有无技能(zz,gz) then
				zz.师门技能[id].包含技能[s].学会 = true
				zz.师门技能[id].包含技能[s].等级 = zz.师门技能[id].等级
				local x = table.tostring(zz.师门技能[id].包含技能[s])
				table.insert(zz.学会技能,table.loadstring(x))
				常规提示(玩家id,"#Y/你的助战学会了门派技能#r/"..gz)
			end
		end
	end
end

function robotskill:有无技能(zz,名称)-------------------------------------------------------
	for n=1,#zz.学会技能 do
		if zz.学会技能[n].名称 == 名称 then
			return true
		end
	end
	return false
end

function robotskill:skillup(玩家id,zz)
	local lxss = {气血=0,魔法=0,灵力=0,伤害=0,命中=0,速度=0,防御=0,躲避=0,法术防御=0}
	for n,v in pairs(zz.师门技能) do
		local jn = zz.师门技能[n]
		if jn.等级 == nil then
			jn.等级 = 1
		end
		if jn.名称 == "小乘佛法" then
			lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "金刚伏魔" then
			lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)
			if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
				self:学会技能(玩家id,zz,2,"佛法无边")
			end
		elseif jn.名称 == "诵经" then
			if jn.等级 >= 1 then
				self:学会技能(玩家id,zz,3,"唧唧歪歪")
			end
		elseif jn.名称 == "佛光普照" then
			if jn.等级 >= 20 then
				self:学会技能(玩家id,zz,4,"韦陀护法")
			end
			if jn.等级 >= 35 then
				self:学会技能(玩家id,zz,4,"金刚护体")
			self:学会技能(玩家id,zz,4,"拈花妙指")
			end
			if jn.等级 >= 30 then
				self:学会技能(玩家id,zz,4,"达摩护体")
				self:学会技能(玩家id,zz,4,"一苇渡江")
				self:学会技能(玩家id,zz,4,"金刚护法")
			end
		elseif jn.名称 == "大慈大悲" then
			if jn.等级 >= 40 then
				self:学会技能(玩家id,zz,5,"我佛慈悲")
			end
			lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "歧黄之术" then
		if jn.等级 >= 15 then
			self:学会技能(玩家id,zz,6,"解毒")
		end
		if jn.等级 >= 25 then
			self:学会技能(玩家id,zz,6,"活血")
		end
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,6,"推气过宫")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,6,"舍生取义")
		end
		elseif jn.名称 == "渡世步" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,7,"佛门普渡")
		end
		lxss.躲避 = lxss.躲避 + floor(jn.等级 * 2.5)
		-- 大唐
		elseif jn.名称 == "为官之道" then
		if jn.等级 >= 15 then
			self:学会技能(玩家id,zz,1,"杀气诀")
		end
		elseif jn.名称 == "无双一击" then
		if jn.等级 >= 25 then
			self:学会技能(玩家id,zz,2,"后发制人")
		end
		lxss.命中 = lxss.命中 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "神兵鉴赏" then
		if jn.等级 >= 5 then
			self:学会技能(玩家id,zz,3,"兵器谱")
		end
		elseif jn.名称 == "疾风步" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,4,"千里神行")
		end
		lxss.躲避 = lxss.躲避 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "十方无敌" then
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,5,"横扫千军")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,5,"破釜沉舟")
		end
		lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "紫薇之术" then
		lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "文韬武略" then
			if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,7,"嗜血")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,7,"安神诀")
		end
		lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)
		-- 龙宫
		elseif jn.名称 == "九龙诀" then
		if jn.等级 >= 15 then
			self:学会技能(玩家id,zz,1,"清心")
		end
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,1,"解封")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,1,"二龙戏珠")
		end
		lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "破浪诀" then
		lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,2,"神龙摆尾")
		end
		elseif jn.名称 == "呼风唤雨" then
		if jn.等级 >= 15 then
			self:学会技能(玩家id,zz,3,"龙卷雨击")
		end
		elseif jn.名称 == "龙腾术" then
			if jn.等级 >= 50 then
				self:学会技能(玩家id,zz,4,"龙腾")
			end
		elseif jn.名称 == "逆鳞术" then
			if jn.等级 >= 30 then
				self:学会技能(玩家id,zz,5,"逆鳞")
			end
			lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "游龙术" then
			if jn.等级 >= 1 then
				self:学会技能(玩家id,zz,6,"水遁")
			end
			if jn.等级 >= 20 then
				self:学会技能(玩家id,zz,6,"乘风破浪")
			end
			lxss.躲避 = lxss.躲避 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "龙附术" then
			if jn.等级 >= 25 then
				self:学会技能(玩家id,zz,"龙啸九天")
			end
			if jn.等级 >= 30 then
				self:学会技能(玩家id,zz,7,"龙吟")
			end
			if jn.等级 >= 35 then
				self:学会技能(玩家id,zz,7,"龙附")
			end
		-- 方寸
		elseif jn.名称 == "黄庭经" then
			if jn.等级 >= 25 then
				self:学会技能(玩家id,zz,1,"三星灭魔")
			end
		elseif jn.名称 == "磐龙灭法" then
		lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "霹雳咒" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,3,"五雷咒")
		end
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,3,"落雷符")
		end
		if jn.等级 >= 25 then
			self:学会技能(玩家id,zz,3,"失魂符")
			self:学会技能(玩家id,zz,3,"离魂符")
		end
		if jn.等级 >= 40 then
			self:学会技能(玩家id,zz,3,"失心符")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,3,"碎甲符")
		end
		elseif jn.名称 == "符之术" then
		if jn.等级 >= 10 then
			self:学会技能(玩家id,zz,4,"催眠符")
		 -- self:学会技能(玩家id,zz,4,"兵解符")
		end
		-- if jn.等级 >= 15 then
		--	self:学会技能(玩家id,zz,4,"落魄符")
		-- end
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,4,"追魂符")
			self:学会技能(玩家id,zz,4,"失忆符")
		end
		if jn.等级 >= 21 then
			self:学会技能(玩家id,zz,4,"飞行符")
		end
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,4,"定身符")
		end
		elseif jn.名称 == "归元心法" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,5,"归元咒")
		end
		if jn.等级 >= 100 then
			self:学会技能(玩家id,zz,5,"凝神术")
		end
		lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "神道无念" then
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,6,"乾天罡气")
		end
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,6,"神兵护法")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,6,"分身术")
		end
		lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "斜月步" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,7,"乙木仙遁")
		end
		lxss.躲避 = lxss.躲避 + floor(jn.等级 * 2.5)
		-- 地府
		elseif jn.名称 == "灵通术" then
		if jn.等级 >= 5 then
			self:学会技能(玩家id,zz,1,"堪察令")
		end
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,1,"寡欲令")
		end
		elseif jn.名称 == "幽冥术" then
		if jn.等级 >= 25 then
			self:学会技能(玩家id,zz,3,"阎罗令")
		end
		 if jn.等级 >= 75 then
			self:学会技能(玩家id,zz,3,"锢魂术")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,3,"黄泉之息")
		end
		elseif jn.名称 == "六道轮回" then
		lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)
		 if jn.等级 >= 50 then
			self:学会技能(玩家id,zz,2,"魂飞魄散")
		end
		elseif jn.名称 == "拘魂诀" then
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,4,"判官令")
		end
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,4,"尸气漫天")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,4,"还阳术")
		end
		elseif jn.名称 == "九幽阴魂" then
		lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)
			if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,5,"幽冥鬼眼")
		end
		elseif jn.名称 == "尸腐恶" then
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,6,"尸腐毒")
			self:学会技能(玩家id,zz,6,"修罗隐身")
		end
		lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "无常步" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,7,"杳无音讯")
		end
		lxss.躲避 = lxss.躲避 + floor(jn.等级 * 2.5)
		-- 天宫
		elseif jn.名称 == "天罡气" then
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,1,"天神护体")
		end
		if jn.等级 >= 40 then
			self:学会技能(玩家id,zz,1,"天神护法")
		end
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,1,"五雷轰顶")
			self:学会技能(玩家id,zz,1,"浩然正气")
		end
		if jn.等级 >= 50	then
			self:学会技能(玩家id,zz,1,"雷霆万钧")
		end
		lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "傲世诀" then
		if jn.等级 >= 25 then
			self:学会技能(玩家id,zz,2,"天雷斩")
		end
		elseif jn.名称 == "宁气诀" then
			if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,4,"宁心")
		end
		lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "清明自在" then
				if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,3,"知己知彼")
		end
		lxss.气血 = lxss.气血 + floor(jn.等级 * 3)
		elseif jn.名称 == "乾坤塔" then
			if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,5,"镇妖")
		end


		if jn.等级 >= 50 then
			self:学会技能(玩家id,zz,5,"错乱")
		end
		lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "混天术" then
		if jn.等级 >= 40 then
			self:学会技能(玩家id,zz,6,"百万神兵")
		end
		 if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,6,"金刚镯")
		end
		elseif jn.名称 == "云霄步" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,7,"腾云驾雾")
		end
		lxss.躲避 = lxss.躲避 + floor(jn.等级 * 2.5)
		-- 魔王
	 elseif jn.名称 == "牛逼神功" then
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,1,"魔王护持")
		end


		elseif jn.名称 == "火云术" then
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,3,"飞砂走石")
		end
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,3,"三昧真火")
		end
		elseif jn.名称 == "裂石步" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,7,"牛屎遁")
		end
		lxss.躲避 = lxss.躲避 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "震天诀" then
		lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "火牛阵" then
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,4,"牛劲")
		end
		 if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,4,"火甲术")
		end
		lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "牛虱阵" then
		-- if jn.等级 >= 25 then
		--	self:学会技能(玩家id,zz,5,"无敌牛虱")
		--	self:学会技能(玩家id,zz,5,"无敌牛妖")
		-- end
		 if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,5,"摇头摆尾")
		end
		elseif jn.名称 == "回身击" then
		lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)
			if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,6,"魔王回首")
		end
		elseif jn.名称 == "裂石步" then
		lxss.速度 = lxss.速度 + floor(jn.等级 * 2.5)
		--普陀
		elseif jn.名称 == "灵性" then
			if jn.等级 >= 60 then
			self:学会技能(玩家id,zz,1,"自在心法")
		end
		lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "护法金刚" then
		lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "金刚经" then
		if jn.等级 >= 15 then
			self:学会技能(玩家id,zz,5,"普渡众生")
		end
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,5,"莲华妙法")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,5,"灵动九天")
		end
		elseif jn.名称 == "观音咒" then
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,3,"紧箍咒")
		end
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,3,"杨柳甘露")
		end
		elseif jn.名称 == "五行学说" then
		if jn.等级 >= 10 then
			self:学会技能(玩家id,zz,4,"日光华")
		end
		if jn.等级 >= 10 then
			self:学会技能(玩家id,zz,4,"靛沧海")
		end
		if jn.等级 >= 10 then
			self:学会技能(玩家id,zz,4,"巨岩破")
		end
		if jn.等级 >= 10 then
			self:学会技能(玩家id,zz,4,"苍茫树")
		end
		if jn.等级 >= 10 then
			self:学会技能(玩家id,zz,4,"地裂火")
		end
		elseif jn.名称 == "五行扭转" then
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,6,"颠倒五行")
		end
		lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "莲花宝座" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,7,"坐莲")
		end
		lxss.躲避 = lxss.躲避 + floor(jn.等级 * 2.5)
		-- 五庄观
		elseif jn.名称 == "周易学" then
		if jn.等级 >= 25 then
			self:学会技能(玩家id,zz,1,"驱魔")
		end
		if	jn.等级 >= 20 then
			self:学会技能(玩家id,zz,1,"驱尸")
		end
		lxss.魔法 = lxss.魔法 + floor(jn.等级 * 3)
		elseif jn.名称 == "潇湘仙雨" then
		if jn.等级 >= 25 then
			self:学会技能(玩家id,zz,2,"烟雨剑法")
		end
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,2,"飘渺式")
		end
		lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "乾坤袖" then
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,3,"日月乾坤")
		end
			if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,3,"天地同寿")
			self:学会技能(玩家id,zz,3,"乾坤妙法")
		end
		elseif jn.名称 == "修仙术" then
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,4,"炼气化神")
		end
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,4,"生命之泉")
		end
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,4,"一气化三清")
		end
		elseif jn.名称 == "混元道果" then
		lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "明性修身" then
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,6,"三花聚顶")
		end
		lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "七星遁" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,7,"斗转星移")
		end
		lxss.躲避 = lxss.躲避 + floor(jn.等级 * 2.5)
		-- 狮驼岭
		elseif jn.名称 == "魔兽神功" then
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,1,"变身")
		end

		elseif jn.名称 == "生死搏" then
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,2,"象形")
		end
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,2,"鹰击")
		end
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,2,"狮搏")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,2,"天魔解体")
		end
		lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "训兽诀" then
		lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)
			if jn.等级 >= 15 then
			self:学会技能(玩家id,zz,3,"威慑")
		 end
		elseif jn.名称 == "阴阳二气诀" then
			if jn.等级 >= 40 then
			self:学会技能(玩家id,zz,4,"定心术")
		 end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,4,"魔息术")
		end
		elseif jn.名称 == "狂兽诀" then
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,5,"连环击")
		end
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,5,"神力无穷")
		end

		elseif jn.名称 == "大鹏展翅" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,6,"振翅千里")
		end
		lxss.躲避 = lxss.躲避 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "魔兽反噬" then
		lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)
			if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,7,"极度疯狂")
		end
		-- 盘丝洞
		elseif jn.名称 == "蛛丝阵法" then
			--	if jn.等级 >= 25 then
			--	self:学会技能(玩家id,zz,1,"夺命蛛丝")
			-- end
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,1,"盘丝舞")
		end
		lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "秋波暗送" then
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,3,"勾魂")
		end
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,3,"摄魄")
		end
		lxss.命中 = lxss.命中 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "迷情大法" then
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,2,"含情脉脉")
		end
		if jn.等级 >= 45 then
			self:学会技能(玩家id,zz,2,"魔音摄魂")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,2,"瘴气")
		end
		elseif jn.名称 == "天外魔音" then

		lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)

		elseif jn.名称 == "盘丝大法" then
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,5,"盘丝阵")
			self:学会技能(玩家id,zz,5,"复苏")
		end
		lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "盘丝步" then
		if jn.等级 >= 15 then
			self:学会技能(玩家id,zz,6,"天罗地网")
		end
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,6,"天蚕丝")
		end
		 if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,6,"幻镜术")
		end
		lxss.躲避 = lxss.躲避 + floor(jn.等级 * 2.5)
		-- 凌波城
		elseif jn.名称 == "九转玄功" then
		if jn.等级 >= 25 then
			self:学会技能(玩家id,zz,2,"不动如山")
		end
		elseif jn.名称 == "武神显圣" then
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,3,"碎星诀")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,3,"镇魂诀")
		end
		elseif jn.名称 == "气吞山河" then
		if jn.等级 >= 25 then
			self:学会技能(玩家id,zz,5,"裂石")
		end
		if jn.等级 >= 25 then
			self:学会技能(玩家id,zz,5,"浪涌")
		end
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,5,"断岳势")
		end
		if jn.等级 >= 45 then
			self:学会技能(玩家id,zz,5,"天崩地裂")
		end
		if jn.等级 >= 45 then
			self:学会技能(玩家id,zz,5,"翻江搅海")
		end
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,5,"惊涛怒")
		end
		lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "啸傲" then
		lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "诛魔" then
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,6,"腾雷")
		end
		lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "法天象地" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,7,"纵地金光")
		end
		lxss.命中 = lxss.命中 + floor(jn.等级 * 2.5)
		-- 神木林
		elseif jn.名称 == "瞬息万变" then
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,1,"落叶萧萧")
		end
		elseif jn.名称 == "万灵诸念" then
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,2,"荆棘舞")
		end
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,2,"尘土刃")
		end
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,2,"冰川怒")
		end
		lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "巫咒" then
		if jn.等级 >= 40 then
			self:学会技能(玩家id,zz,3,"雾杀")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,3,"血雨")
		end

		lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "万物轮转" then
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,4,"星月之惠")
		end
		lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "天人庇护" then
		if jn.等级 >= 50 then
			self:学会技能(玩家id,zz,5,"炎护")
		end
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,5,"叶隐")
		end
		lxss.躲避 = lxss.躲避 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "神木恩泽" then
		 if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,6,"神木呓语")
		end
		elseif jn.名称 == "驭灵咒" then
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,7,"蜜润")
		end
		lxss.魔法 = lxss.魔法 + floor(jn.等级 * 3)
		-- 无底洞
		elseif jn.名称 == "枯骨心法" then
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,1,"移魂化骨")
		end
		elseif jn.名称 == "阴风绝章" then
		if jn.等级 >= 25 then
			self:学会技能(玩家id,zz,2,"夺魄令")
		end
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,2,"煞气诀")
		end
		if jn.等级 >= 50 then
			self:学会技能(玩家id,zz,2,"惊魂掌")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,2,"摧心术")
		end
		elseif jn.名称 == "鬼蛊灵蕴" then
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,3,"夺命咒")
		end
		lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "燃灯灵宝" then
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,4,"明光宝烛")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,4,"金身舍利")
		end
		lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "地冥妙法" then
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,5,"地涌金莲")
		end
		elseif jn.名称 == "混元神功" then
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,6,"元阳护体")
		end
		lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "秘影迷踪" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,7,"遁地术")
		end
		lxss.速度 = lxss.速度 + floor(jn.等级 * 2.5)
		-- 女儿
		elseif jn.名称 == "毒经" then
		lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "倾国倾城" then
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,2,"红袖添香")
		end
		if jn.等级 >= 10 then
			self:学会技能(玩家id,zz,2,"楚楚可怜")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,2,"一笑倾城")
		end
		elseif jn.名称 == "沉鱼落雁" then
		if jn.等级 >= 25 then
			self:学会技能(玩家id,zz,3,"满天花雨")
		end
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,3,"雨落寒沙")
		end
		elseif jn.名称 == "闭月羞花" then
		if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,4,"莲步轻舞")
			self:学会技能(玩家id,zz,4,"如花解语")
		end
		if jn.等级 >= 25 then
			self:学会技能(玩家id,zz,4,"娉婷嬝娜")
		end
		if jn.等级 >= 40 then
			self:学会技能(玩家id,zz,4,"似玉生香")
		end
		elseif jn.名称 == "香飘兰麝" then
		 if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,5,"轻如鸿毛")
		end
		lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "玉质冰肌" then
		if jn.等级 >= 10 then
			self:学会技能(玩家id,zz,6,"百毒不侵")
		end
		lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "清歌妙舞" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,7,"移形换影")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode)	then
			self:学会技能(玩家id,zz,7,"飞花摘叶")
		end
		lxss.速度 = lxss.速度 + floor(jn.等级 * 2.5)


	-------------------------------------花果山---------------------------------------------


		elseif jn.名称 == "神通广大" then
		if jn.等级 >= 120 and (zz.飞升 or DebugMode)	then
			self:学会技能(玩家id,zz,1,"威震凌霄")
		end
		if jn.等级 >= 120 and (zz.飞升 or DebugMode)	then
			self:学会技能(玩家id,zz,1,"气慑天军")
		end

		elseif jn.名称 == "金刚之躯" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,4,"铜头铁臂")
		end
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,4,"担山赶月")
		end
		lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)

		elseif jn.名称 == "如意金箍" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,2,"当头一棒")
		end
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,2,"神针撼海")
		end
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,2,"杀威铁棒")
		end
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,2,"泼天乱棒")
		end
		lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "齐天逞胜" then
		-- if jn.等级 >= 1 then
		--	self:学会技能(玩家id,zz,3,"九幽除名")
		-- end
		if jn.等级 >= 15 then
			self:学会技能(玩家id,zz,3,"移星换斗")
		end
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,3,"云暗天昏")
		end
		elseif jn.名称 == "灵猴九窍" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,5,"无所遁形")
		end
		-- if jn.等级 >= 175 then ---化圣
		--	self:学会技能(玩家id,zz,5,"天地洞明")
		-- end
		-- if jn.等级 >= 25 then
		--	self:学会技能(玩家id,zz,5,"除光息焰")
		-- end
		lxss.命中 = lxss.命中 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "七十二变" then
		if jn.等级 >= 10 then
			self:学会技能(玩家id,zz,6,"呼子唤孙")
		end
		if jn.等级 >= 45 then
			self:学会技能(玩家id,zz,6,"八戒上身")
		end
		lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "滕云驾雾" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,7,"筋斗云")
		end

		----------------------------新门派

		-----------------------女魃墓
		elseif jn.名称 == "天罚之焰" then
		if jn.等级 >= 10 then
			self:学会技能(玩家id,zz,2,"炽火流离")
		end
		-- if jn.等级 >= 25 then
		--	self:学会技能(玩家id,zz,2,"极天炼焰")
		-- end
		lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "煌火无明" then
		if jn.等级 >= 15 then
			self:学会技能(玩家id,zz,3,"谜毒之缚")
		end
		-- if jn.等级 >= 10 then
		--	self:学会技能(玩家id,zz,3,"诡蝠之刑")
		-- end
			if jn.等级 >= 30 then
			self:学会技能(玩家id,zz,3,"怨怖之泣")
		end
		--	 if jn.等级 >= 40 then
		--	self:学会技能(玩家id,zz,3,"誓血之祭")
		-- end
		lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)

		elseif jn.名称 == "化神以灵" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,4,"唤灵·魂火")
		end
		if jn.等级 >= 20 then
			self:学会技能(玩家id,zz,4,"唤魔·堕羽")
		end
		if jn.等级 >= 120 and( zz.飞升 or DebugMode)	then
			self:学会技能(玩家id,zz,4,"唤魔·毒魅")
		end
			if jn.等级 >= 10 then
			self:学会技能(玩家id,zz,4,"唤灵·焚魂")
		end
		 if jn.等级 >= 175 and zz.化圣 then---化圣
			self:学会技能(玩家id,zz,4,"天魔觉醒")
		end

		elseif jn.名称 == "弹指成烬" then
		if jn.等级 >= 25 then
			self:学会技能(玩家id,zz,5,"净世煌火")
		end
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,5,"焚魔烈焰")
		end
		lxss.法术防御 = lxss.法术防御 + floor(jn.等级 * 2.5)

			elseif jn.名称 == "藻光灵狱" then
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,6,"幽影灵魄")
		end
		lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)

			elseif jn.名称 == "离魂" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,7,"魂兮归来")
		end
		lxss.速度 = lxss.速度 + floor(jn.等级 * 2.5)

	------天机城
			elseif jn.名称 == "神工无形" then
			lxss.速度 = lxss.速度 + floor(jn.等级 * 2.5)

			elseif jn.名称 == "攻玉以石" then
		if jn.等级 >= 25 then
			self:学会技能(玩家id,zz,2,"针锋相对")
		end

			elseif jn.名称 == "擎天之械" then
		if jn.等级 >= 25 then
			self:学会技能(玩家id,zz,3,"锋芒毕露")
		end


			elseif jn.名称 == "千机奇巧" then
		if jn.等级 >= 45 then
			self:学会技能(玩家id,zz,4,"诱袭")
		end
			if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,4,"破击")
		end
		lxss.防御 = lxss.防御 + floor(jn.等级 * 2.5)

			elseif jn.名称 == "匠心不移" then
		if jn.等级 >= 120 and (zz.飞升 or DebugMode) then
			self:学会技能(玩家id,zz,5,"匠心·削铁")
		end
			if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,5,"匠心·固甲")
		end
				if jn.等级 >= 25 then
			self:学会技能(玩家id,zz,5,"匠心·蓄锐")
		end
		lxss.伤害 = lxss.伤害 + floor(jn.等级 * 2.5)
		elseif jn.名称 == "运思如电" then
		if jn.等级 >= 1 then
			self:学会技能(玩家id,zz,6,"天马行空")
		end
		lxss.躲避 = lxss.躲避 + floor(jn.等级 * 2.5)

		elseif jn.名称 == "探奥索隐" then
		if jn.等级 >= 35 then
			self:学会技能(玩家id,zz,7,"鬼斧神工")
		end
			--	 if jn.等级 >= 25 then
			--	self:学会技能(玩家id,zz,7,"移山填海")
			-- end
		lxss.灵力 = lxss.灵力 + floor(jn.等级 * 2.5)
		end
	end
	return lxss
end






return robotskill








