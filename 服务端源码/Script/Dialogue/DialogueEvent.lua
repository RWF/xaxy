-- @Author: 作者QQ414628710
-- @Date:   2021-11-27 17:21:22
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-05-19 01:32:35
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-06-11 18:25:47
--======================================================================--
local 事件解析 = class()
local format = string.format
local insert = table.insert
local wb,xx

function 事件解析:对话栏(事件,名称,id,地图1)

   -----通用npc
	if UserData[id].最后操作 ~= nil then
		print(table.tostring(UserData[id].最后操作))
	end
	if UserData[id].最后操作 ~= nil and UserData[id].最后操作.动作 =="购买助战" then
		if 事件 == "飞燕女" or 事件 == "英女侠" or 事件 == "巫蛮儿" or 事件 == "逍遥生" or 事件 == "剑侠客" or 事件 == "狐美人" or 事件 == "骨精灵" or 事件 == "杀破狼" or 事件 == "巨魔王" or 事件 == "虎头怪" or 事件 == "舞天姬" or 事件 == "玄彩娥" or 事件 == "羽灵神" or 事件 == "神天兵" or 事件 == "龙太子" or 事件 == "桃夭夭" or 事件 == "偃无师" or 事件 == "鬼潇潇" then
			UserData[id].助战:新增助战(id,事件)
			UserData[id].最后操作 = nil
		end
	elseif UserData[id].最后操作 ~= nil and UserData[id].最后操作.动作 =="助战培养" then
		if 事件 == "培养一次" or 事件 == "培养十次" or 事件 == "培养五十次" then
			UserData[id].助战:助战培养(事件,UserData[id].最后操作.识别码)
			UserData[id].最后操作 = nil
		end
	elseif UserData[id].最后操作 ~= nil and UserData[id].最后操作.动作 =="助战遗弃" then
		if 事件 == "确定遗弃" then
			UserData[id].助战:助战遗弃(UserData[id].最后操作.识别码)
			UserData[id].最后操作 = nil
		end
	elseif UserData[id].最后操作 ~= nil and UserData[id].最后操作.动作 =="助战门派" then
		if 事件 == "方寸山(助战)" or 事件 == "女儿村(助战)" or 事件 == "神木林(助战)" or 事件 == "化生寺(助战)" or 事件 == "大唐官府(助战)" or 事件 == "盘丝洞(助战)" or 事件 == "阴曹地府(助战)" or 事件 == "无底洞(助战)" or 事件 == "魔王寨(助战)" or 事件 == "狮驼岭(助战)" or 事件 == "天宫(助战)" or 事件 == "普陀山(助战)" or 事件 == "凌波城(助战)" or 事件 == "五庄观(助战)" or 事件 == "龙宫(助战)" or 事件 == "退出助战门派" then
			UserData[id].助战:助战门派(事件,UserData[id].最后操作.识别码)
			UserData[id].最后操作 = nil
		end
	elseif UserData[id].最后操作 ~= nil and UserData[id].最后操作.动作 =="助战洗点" then
		if 事件 == "确认洗点" then
			UserData[id].助战:助战洗点(UserData[id].最后操作.识别码)
			UserData[id].最后操作 = nil
		end
    elseif 事件 == "确认脱离门派" and (地图1 == 1043 or 地图1 == 1252 or 地图1 == 1257 or 地图1 == 1251  or 地图1 == 1054 or 地图1 == 1117 or 地图1 == 1137 or 地图1 == 1124 or 地图1 == 1112 or 地图1 == 1145 or 地图1 == 1141 or 地图1 == 1147 or 地图1 == 1134 or 地图1 == 1144 or 地图1 == 1150 or 地图1 == 1154 or 地图1 == 1156 or 地图1 == 1143 ) then
          RoleControl:脱离门派(UserData[id])

    elseif LinkTaskEvent1[事件] then--给予道具
        if RoleControl:GetTaskID(UserData[id],LinkTaskEvent1[事件]) == 0  then
             SendMessage(UserData[id].连接id,7,"你还没有领取"..LinkTaskEvent1[事件].."任务")
        else 
           SendMessage(UserData[id].连接id,3011,{名称=LinkTaskEvent1[事件] ,id=RoleControl:GetTaskID(UserData[id],LinkTaskEvent1[事件]),道具=ItemControl:索要道具1(id,"包裹")})
        end 
    elseif LinkTaskEvent2[事件] then --打听消息
        if RoleControl:GetTaskID(UserData[id],LinkTaskEvent2[事件]) == 0  then
             SendMessage(UserData[id].连接id,7,"你还没有领取"..LinkTaskEvent2[事件].."任务")
        else 
              local  TempTaskID = RoleControl:GetTaskID(UserData[id],LinkTaskEvent2[事件])
              local   TempType =LinkTaskEvent2[事件]
              local   course ="进程"..任务数据[TempTaskID].进程
              if LinkTask[TempType][course][5] then
               SendMessage(UserData[id].连接id,20,{UserData[id].角色.造型,UserData[id].角色.名称,LinkTask[TempType][course][5]})
              end
              TaskControl:FulfilLinkTask(id, RoleControl:GetTaskID(UserData[id],TempType))
        end 

    elseif LinkTaskEvent3[事件] then --开始挑战
        if RoleControl:GetTaskID(UserData[id],LinkTaskEvent3[事件]) == 0  then
             SendMessage(UserData[id].连接id,7,"你还没有领取"..LinkTaskEvent3[事件].."任务")
        else 
          TaskControl:LinkTaskFight(id, RoleControl:GetTaskID(UserData[id],LinkTaskEvent3[事件]))
        end 
    elseif 事件 == "我来完成任务链" and RoleControl:GetTaskID(UserData[id],"任务链") ~= 0  then
      local  taskid=RoleControl:GetTaskID(UserData[id],"任务链")
        if 任务数据[taskid].地图名称~= UserData[id].地图 then
            封禁账号(UserData[id],"任务链")
         else
             if 任务数据[taskid].分类==1 then
                TaskControl:完成任务链(id,taskid)
            elseif  任务数据[taskid].分类==2 then
                SendMessage(UserData[id].连接id,3011,{id=taskid,名称=名称,道具=ItemControl:索要道具2(id,"包裹")})
            elseif  任务数据[taskid].分类==3 then
                        local 符合召唤兽id=0
                        for n=1,#UserData[id].召唤兽.数据 do
                            if UserData[id].召唤兽.数据[n].造型==任务数据[taskid].召唤兽  then
                            符合召唤兽id=n
                             break
                            end
                        end
                        if 符合召唤兽id==0 then
                            SendMessage(UserData[id].连接id,7,"#y/你身上没有我所需要的召唤兽")
                            return 0
                        else
                            UserData[id].召唤兽.数据.参战=0
                            table.remove(UserData[id].召唤兽.数据,符合召唤兽id)
                            TaskControl:完成任务链(id,taskid)
                        end
            elseif  任务数据[taskid].分类==4 then
                     FightGet:进入处理(id,100013,"66",taskid)
             end
        end

	elseif 事件 =="我确定扩充召唤兽栏" then 
		local  扩展银子 = 100000000 
		if UserData[id].召唤兽.数据.扩充 > 4  then 
			SendMessage(UserData[id].连接id, 20, {UserData[id].角色.造型,UserData[id].角色.名称,"你已经扩充了4个召唤兽栏无法在进行扩充"})
		elseif 银子检查(id,扩展银子) then
		   RoleControl:扣除银子(UserData[id],扩展银子, "扩充召唤兽栏")
			UserData[id].召唤兽.数据.扩充 =UserData[id].召唤兽.数据.扩充+1 
			UserData[id].召唤兽.数据.召唤兽上限 = math.floor(UserData[id].角色.宠物.等级/25+3)+UserData[id].召唤兽.数据.扩充
			SendMessage(UserData[id].连接id,3040,"1")
		else
			SendMessage(UserData[id].连接id, 20, {UserData[id].角色.造型,UserData[id].角色.名称,"你的银子不足"..扩展银子.."无法进行扩充！"})

		end
	elseif 事件 =="车迟斗法" then
		TaskControl:创建车迟斗法副本(id)

elseif 事件 == "牛大哥你发彪啊（切入战斗）" and 地图1 == 1512 then
  TaskControl:触发神器任务(id,RoleControl:GetTaskID(UserData[id],"神器任务"))
elseif 事件 == "只好先用拳头说话了（切入战斗）" and 地图1 == 1111 then
  TaskControl:触发神器任务(id,RoleControl:GetTaskID(UserData[id],"神器任务"))
elseif 事件 == "除非你先打败我（切入战斗）" and 地图1 == 1217 then
TaskControl:触发神器任务(id,RoleControl:GetTaskID(UserData[id],"神器任务"))
  elseif 事件 =="我要报道" then
           TaskControl:触发游泳比赛(id)
  elseif 事件 == "领取法宝任务" and UserData[id].法宝  then
        TaskControl:添加法宝任务(id)
        UserData[id].法宝 = nil
  elseif 事件 == "生死已看淡不服就是干" then
    local 战斗参数=UserData[id].战斗对象
   	if UserData[战斗参数]==nil then
       SendMessage(UserData[id].连接id,7,"#y/这个玩家不存在或没在线")
       return 0
	elseif UserData[id].角色.pk开关==false then
		SendMessage(UserData[id].连接id,7,"#y/请先按F4打开pk开关")
		return 0
	elseif UserData[战斗参数].战斗~=0 and UserData[战斗参数].观战模式==false then
		SendMessage(UserData[id].连接id,7,"#y/对方正在战斗中，请稍后再试")
		return 0
	elseif  UserData[战斗参数].地图~=UserData[id].地图 then
		SendMessage(UserData[id].连接id,7,"#y/你与对方不处于同一地图内，无法发动攻击")
		return 0
	elseif os.time()<UserData[战斗参数].角色.pk保护 then
		SendMessage(UserData[id].连接id,7,"#y/对方正处于保护期，无法对其发动攻击")
		return 0
	elseif UserData[id].角色.人气<600 then
		SendMessage(UserData[id].连接id,7,"#y/你的人气值低于600点，无法发动攻击")
		return 0
  elseif  UserData[战斗参数].角色.变身.造型 then
    SendMessage(UserData[id].连接id,7,"#y/对方处于变身状态,无法强行PK")
    return 0
    else
         if UserData[id].地图==1001 or UserData[id].地图==5131 or UserData[id].地图==1092 then
            SendMessage(UserData[id].连接id,7,"#y/本地图不允许发起pk战斗")
           return 0
          end
          local 消耗人气=50
          if math.abs(UserData[id].角色.等级-UserData[战斗参数].角色.等级)>=20 then
            消耗人气=100
          end
          UserData[id].角色.人气=UserData[id].角色.人气-消耗人气
          SendMessage(UserData[id].连接id,7,"#y/你消耗了"..消耗人气.."点人气")
          if UserData[战斗参数].战斗~=0 and UserData[战斗参数].观战模式 then --令其退出观战模式
           FightGet.战斗盒子[UserData[战斗参数].战斗]:退出观战(战斗参数)
           UserData[战斗参数].战斗=0
           UserData[战斗参数].观战模式=false
           end
         广播消息(9,"#xt/#y/生死看淡，不服就干。#g/"..UserData[id].角色.名称.."("..UserData[id].id.."）".."#y/对#g/"..UserData[UserData[id].战斗对象].角色.名称.."("..UserData[UserData[id].战斗对象].id..")#y/发起了pk战斗")
          RoleControl:添加系统消息(UserData[UserData[UserData[id].战斗对象].id],"#h/"..UserData[id].角色.名称.."["..UserData[id].id.."]对你发起了pk战斗。")
         FightGet:创建战斗(id,200003,战斗参数,0)
     end
    elseif 事件 == "副帮主"and 名称 == "帮派" then
		ItemControl:职务变更(id, "副帮主")
	elseif 事件 == "我同意"and 名称 == "更换队长" then
		if UserData[id].队伍 == 0 then
			SendMessage(UserData[id].连接id, 7, "#y/请先加入一个队伍")
			return 0
		elseif UserData[id].队长时间 == nil or os.time() - UserData[id].队长时间 >= 60 then
			SendMessage(UserData[id].连接id, 7, "#y/操作超时")
			return 0
		else
			TeamControl:新任队长(id)
		end
  elseif 事件 == "我要收养孩子" and 名称 == "马婆婆"then
      local temp =ItemControl:寻找道具(UserData[id],{"孤儿名册"})
    if #temp>0 then
        ItemControl:RemoveItems(UserData[id],temp[1],"孤儿名册")
        RoleControl:NewChildren(UserData[id])
    else
        SendMessage(UserData[id].连接id,20,{"老太婆","马婆婆","你的包裹里面没有孤儿手册无法领养孩子"})
    end
  elseif 事件 == "领取逍遥镜" and 名称 == "马婆婆"then
     if UserData[id].角色.子女列表.逍遥镜 then
        SendMessage(UserData[id].连接id,20,{"老太婆","马婆婆","只能免费领取一次逍遥镜"})
     else
        ItemControl:GiveItem(id,"逍遥镜")
        UserData[id].角色.子女列表.逍遥镜=1
     end
  elseif 事件 == "带我冲上云霄" and (地图1 == 1001) and 名称 == "大雁塔传送使者" then
      SendMessage(UserData[id].连接id, 15, {"大雁塔","请输入你需要传送的层数,每次消耗1万*层数的银子"})
 elseif 事件 == "我要扫荡大雁塔" and (地图1 == 1001) and 名称 == "大雁塔传送使者" then
      if UserData[id].角色.大雁塔 > 0 then
         if RoleControl:扣除仙玉(UserData[id],5000,"大雁塔") then
           if UserData[id].角色.活跃度 > 100 then
                RoleControl:扣除活跃度(UserData[id],100)
                 for i=1,UserData[id].角色.大雁塔 do
                   if 活动数据.大雁塔数据[i][id] then 
                      SendMessage(UserData[id].连接id,20,{"御林军","大雁塔传送使者","你已经完成了当天的扫荡"})
                      break
                   else  
                      TaskControl:完成大雁塔({id}, i)
                   end
                 end
           else 
              SendMessage(UserData[id].连接id,20,{"御林军","大雁塔传送使者","你当前活跃度不足100点无法进行扫荡"})
           end 
         else
           SendMessage(UserData[id].连接id,20,{"御林军","大雁塔传送使者","你当前的仙玉不足5000点"})
         end
      else
          SendMessage(UserData[id].连接id,20,{"御林军","大雁塔传送使者","你当前没有大雁塔记录不能扫荡"})
      end



  elseif 事件 == "我要炼丹" and 名称 == "八卦炼丹炉"then
          炼丹查看[id] = 1
          if UserData[id].角色.炼丹灵气 == nil then
            UserData[id].角色.炼丹灵气 = 0
          end
          SendMessage(UserData[id].连接id,3077,{时间=炼丹炉.时间,数据=炼丹炉[id],灵气=UserData[id].角色.炼丹灵气,物品数据=ItemControl:索要道具1(id,"包裹")})
  elseif 事件 == "买点炼丹材料" and 名称 == "八卦炼丹炉"then
            local 价格 = 1000000
            if 银子检查(id,价格) then
              RoleControl:扣除银子(UserData[id],价格,"炼丹材料")
             ItemControl:GiveItem(id, "翡翠琵琶")
            else
            SendMessage(UserData[id].连接id,7,"#Y/你个穷鬼,没有钱还想买炼丹材料啊！")
            end
  elseif 事件 == "兑换特殊魔兽要诀（消耗999神兜兜）" then
      local 找到物品 = false
      local 特赦格子 = 0
      for n = 1, 20 do
      if UserData[id].角色.道具["包裹"][n] ~= nil and 找到物品 == false and UserData[id].物品[UserData[id].角色.道具["包裹"][n]].名称 == "神兜兜" then
      找到物品 = true
      特赦格子=n
          break
      end
      end
      if 找到物品 == false then
          SendMessage(UserData[id].连接id, 7,"你身上没有神兜兜呀")
      elseif UserData[id].物品[UserData[id].角色.道具["包裹"][特赦格子]].数量 < 999 then  
           SendMessage(UserData[id].连接id, 7, "你身上神兜兜数量不够")
      else
         ItemControl:RemoveItems(UserData[id],特赦格子,"神兜兜",999)
            ItemControl:GiveItem(id, "特殊兽决宝盒")
          
      end 
    elseif 事件 == "我来完成宝宝任务链" and RoleControl:GetTaskID(UserData[id],"宝宝任务链") ~= 0  then
      local  taskid=RoleControl:GetTaskID(UserData[id],"宝宝任务链")
        if 任务数据[taskid].地图名称~= UserData[id].地图 then
            封禁账号(UserData[id],"宝宝任务链")
         else
             if 任务数据[taskid].分类==1 then
                TaskControl:完成宝宝任务链(id,taskid)
            elseif  任务数据[taskid].分类==2 then
                SendMessage(UserData[id].连接id,3011,{id=taskid,名称=名称,道具=ItemControl:索要道具2(id,"包裹")})
            elseif  任务数据[taskid].分类==3 then
                        local 符合召唤兽id=0
                        for n=1,#UserData[id].召唤兽.数据 do
                            if UserData[id].召唤兽.数据[n].造型==任务数据[taskid].召唤兽 and   UserData[id].召唤兽.数据[n].造型==任务数据[taskid].召唤兽 and UserData[id].召唤兽.数据[n].类型=="变异"then
                            符合召唤兽id=n
                            end
                        end
                        if 符合召唤兽id==0 then
                            SendMessage(UserData[id].连接id,7,"#y/你身上没有我所需要的召唤兽")
                            return 0
                        else
                            UserData[id].召唤兽.数据.参战=0
                            table.remove(UserData[id].召唤兽.数据,符合召唤兽id)
                            TaskControl:完成宝宝任务链(id,taskid)
                        end
            elseif  任务数据[taskid].分类==4 then
                     FightGet:进入处理(id,100013,"66",taskid)
             end
        end
   elseif 事件 == "领取任务（100W银币）" and 地图1 == 1040 and 名称 == "马真人" then
          if UserData[id].角色.等级 < 60 then
          SendMessage(UserData[id].连接id,20,{"太白金星","马真人","只有等级达到60级的玩家才可帮助朝廷"})
          return 0
        elseif RoleControl:GetTaskID(UserData[id],"宝宝任务链") ~= 0 then
          SendMessage(UserData[id].连接id,20,{"太白金星","马真人","你已经领取过此任务了"})
          return 0
        elseif UserData[id].队伍 ~= 0 then
          SendMessage(UserData[id].连接id, 7, "#y/该任务不允许组队领取")
        elseif 银子检查(id, 取角色等级(id) * 取角色等级(id) * 20) == false then
          SendMessage(UserData[id].连接id, 7, "#y/领取此任务需要消耗" .. 取角色等级(id) * 取角色等级(id) * 20 .. "两银子")
          return 0
        else
          RoleControl:扣除银子(UserData[id], 取角色等级(id) * 取角色等级(id) * 20, 2)
          local lsid =tonumber(id .. "187")
          任务数据[lsid] = {
            次数 = 0,
            类型 = "宝宝任务链",
            编号 = 0,
            npc名称 = "马真人",
            npc编号 = 104005,
            结束 = 3600,
            跳过 = 1,
            id = id,
            起始 = os.time(),
            任务id = lsid,
            数字id = UserData[id].id
          }
          TaskControl:设置宝宝任务链(id, lsid)
          UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])] = lsid
                    TaskControl:刷新追踪任务信息(id)
        end
   elseif 事件 == "确认跳过任务" and 地图1 == 1040 and 名称 == "马真人" then
     local taskData =RoleControl:GetTaskID(UserData[id],"宝宝任务链")
      if 银子检查(id, 任务数据[taskData].跳过 * 2500000) == false then
      SendMessage(UserData[id].连接id, 7, "#y/领取此任务需要消耗" .. 任务数据[taskData].跳过 * 2500000 .. "两银子")
    else
      RoleControl:扣除银子(UserData[id], 任务数据[taskData].跳过 * 2500000, 2)

      任务数据[taskData].跳过 = 任务数据[taskData].跳过 + 1

      TaskControl:完成宝宝任务链(id, taskData, 1)
    end


  elseif 事件 == "攻击修炼" and 地图1 == 1040 and 名称 == "马真人" then
          UserData[id].角色.召唤兽修炼.当前 = "攻击"
    SendMessage(UserData[id].连接id, 20,{"太白金星","马真人","您当前的修炼类型已经更换为#G/" .. UserData[id].角色.召唤兽修炼.当前 .. "#W/修炼"})
  elseif 事件 == "法术修炼" and 地图1 == 1040 and 名称 == "马真人" then
    UserData[id].角色.召唤兽修炼.当前 = "法术"
    SendMessage(UserData[id].连接id, 20,{"太白金星","马真人","您当前的修炼类型已经更换为#G/" .. UserData[id].角色.召唤兽修炼.当前 .. "#W/修炼"})
  elseif 事件 == "防御修炼" and 地图1 == 1040 and 名称 == "马真人" then
        UserData[id].角色.召唤兽修炼.当前 = "防御"
    SendMessage(UserData[id].连接id, 20,{"太白金星","马真人","您当前的修炼类型已经更换为#G/" .. UserData[id].角色.召唤兽修炼.当前 .. "#W/修炼"})
  elseif 事件 == "法抗修炼" and 地图1 == 1040 and 名称 == "马真人" then
    UserData[id].角色.召唤兽修炼.当前 = "法抗"
    SendMessage(UserData[id].连接id, 20,{"太白金星","马真人","您当前的修炼类型已经更换为#G/" .. UserData[id].角色.召唤兽修炼.当前 .. "#W/修炼"})

  elseif 事件 == "设置修炼类型" and 地图1 == 1040 and 名称 == "马真人" then
       SendMessage(UserData[id].连接id,20,{"太白金星","马真人","您当前的修炼类型为#G/" .. UserData[id].角色.召唤兽修炼.当前 .. "#W/修炼",{"攻击修炼","法术修炼","防御修炼","法抗修炼"}})
  elseif 事件 == "我要把点数兑换为召唤兽修炼经验" and 地图1 == 1040 and 名称 == "马真人" then
      if UserData[id].角色.召唤兽修炼点数> 0 then
          if UserData[id].角色.召唤兽修炼[UserData[id].角色.召唤兽修炼.当前].上限 <= UserData[id].角色.召唤兽修炼[UserData[id].角色.召唤兽修炼.当前].等级 then
              SendMessage(UserData[id].连接id, 7, "#y/你当前的这项修炼等级已达上限，无法再服用修炼果")
              return 
          else

              RoleControl:添加召唤兽修炼经验(UserData[id],UserData[id].角色.召唤兽修炼点数)
              UserData[id].角色.召唤兽修炼点数=0
          end
      else
         SendMessage(UserData[id].连接id,20,{"太白金星","马真人","你当前的点数不足0不能兑换"})
      end
  elseif 事件 == "我要把点数兑换为修炼果" and 地图1 == 1040 and 名称 == "马真人" then
      if UserData[id].角色.召唤兽修炼点数> 150 then
           UserData[id].角色.召唤兽修炼点数=UserData[id].角色.召唤兽修炼点数-150
           ItemControl:GiveItem(id, "修炼果")
      else
         SendMessage(UserData[id].连接id,20,{"太白金星","马真人","你当前的点数不足150不能兑换"})
      end
  elseif 事件 == "管理修炼点数" and 地图1 == 1040 and 名称 == "马真人" then
   SendMessage(UserData[id].连接id,20,{"太白金星","马真人","水积不厚，负大舟无力，风积不厚，负大翼无力，少侠想做什么那？目前拥有修炼点数：#G/" .. UserData[id].角色.召唤兽修炼点数 .. "#W/点，少侠可以选择兑换 修炼经验，或者将最多兑换#G/"..math.floor(UserData[id].角色.召唤兽修炼点数/150).."#W/个修炼果",{"我要把点数兑换为召唤兽修炼经验","我要把点数兑换为修炼果"}})
   elseif 事件 == "跳过本环（消耗银币）" and 地图1 == 1040 and 名称 == "马真人" then
            if RoleControl:GetTaskID(UserData[id],"宝宝任务链") ~= 0 then
          local wb1="你可以在我这里花费银子跳过任务，根据跳过任务的次数而需要花费一定数量的银子。跳过此任务仍然会获得此任务奖励。您当前需要花费#Y/" .. 任务数据[RoleControl:GetTaskID(UserData[id],"宝宝任务链")].跳过 * 2500000 .."#W/两银子，请确认是否跳过此任务："
      SendMessage(UserData[id].连接id,20,{"太白金星","马真人",wb1,{"确认跳过任务","我在考虑一下"}})
    else
      SendMessage(UserData[id].连接id,20,{"太白金星","马真人","请先领取任务"})
    end
   elseif 事件 == "取消修炼任务" and 地图1 == 1040 and 名称 == "马真人" then
    if RoleControl:GetTaskID(UserData[id],"宝宝任务链") ~= 0 then

      RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"宝宝任务链"))
            任务数据[RoleControl:GetTaskID(UserData[id],"宝宝任务链")] = nil
      SendMessage(UserData[id].连接id,20,{"太白金星","马真人","你的宝宝任务链取消了"})
    else
      SendMessage(UserData[id].连接id,20,{"太白金星","马真人","请先领取任务"})
    end

  -- elseif 事件 == "兑换超级鲲鹏（999个灵兜兜）" then 
  --     local 找到物品 = false
  --     local 特赦格子 = 0
  --     for n = 1, 20 do
  --     if UserData[id].角色.道具["包裹"][n] ~= nil and 找到物品 == false and UserData[id].物品[UserData[id].角色.道具["包裹"][n]].名称 == "神兜兜" then
  --     找到物品 = true
  --     特赦格子=n
  --         break
  --     end
  --     end
  --     if 找到物品 == false then
  --         SendMessage(UserData[id].连接id, 7, "#Y/你身上没有灵兜兜呀")
  --     elseif UserData[id].物品[UserData[id].角色.道具["包裹"][特赦格子]].数量 < 999 then  
  --          SendMessage(UserData[id].连接id, 7, "#Y/你身上神兜兜数量不够")
  --     else
  --          ItemControl:RemoveItems(UserData[id],特赦格子,"灵兜兜",999)
  --           ItemControl:GiveItem(id, "灵兜兜")
          
  --     end 

  elseif 事件 == "领取寄存丹药" and 名称 == "八卦炼丹炉"then

      if 商品存放[id] == nil then
            SendMessage(UserData[id].连接id,7,"#Y/这里没有存放你的奖励哟！")
            return
      else
            local 奖励数据 = 商品存放[id]
            if 奖励数据[1] > 0 then
              ItemControl:GiveItem(id, "金砂丹",nil,nil,奖励数据[1])
            end
            if 奖励数据[2] > 0 then
              ItemControl:GiveItem(id, "银砂丹",nil,nil,奖励数据[2])
            end
            if 奖励数据[3] > 0 then
              ItemControl:GiveItem(id, "铜砂丹",nil,nil,奖励数据[3])
            end
             SendMessage(UserData[id].连接id,7,"#Y/恭喜你在炼丹中中得八卦位！")
            广播消息(string.format("#S(八卦炼丹)#Y恭喜:#G%s#Y在炼丹中获得了:#G%s颗#Y金丹 #G%s颗#Y银丹 #G%s颗#Y铜丹",UserData[id].角色.名称,奖励数据[1],奖励数据[2],奖励数据[3]))
            商品存放[id] = nil
        end
  elseif 事件 == "看看你这有什么卖的" and 名称 == "古董商人"then
     
     ShopControl:GetNpcShop(id,19)
  elseif 事件 == "参加黑市拍卖会" and 名称 == "古董商人"then
      SendMessage(UserData[id].连接id,3075,{UserData[id].角色.造型,UserData[id].角色.名称})
  elseif 事件 == "卖点古玩换钱花" and 名称 == "古董商人"then
        SendMessage(UserData[id].连接id,20,{"钱庄老板","古董商人","古董可以直接在背包出售中出售"})
  elseif 事件 == "关于六艺修行" and 名称 == "马婆婆"then
        SendMessage(UserData[id].连接id,20,{"老太婆","马婆婆","可以直接使用六艺技能书对孩子使用"})
  elseif (事件 == "设置孩子成长方向" or 事件 == "重置孩子属性")  and 名称 == "马婆婆"then
        local xx ={}
        for i=1,#UserData[id].角色.子女列表 do
            table.insert(xx,i..UserData[id].角色.子女列表[i].名称)
        end
        table.insert(xx,"我随便看看")
        local  TempText = 事件 == "设置孩子成长方向" and "成长之路指的是孩子升级以后属性点自动分配的方向，请选择哪个孩子要设置的属性点分配" or "如果你需要重置孩子属性，请先#R设置好成长之路！"
        SendMessage(UserData[id].连接id,20,{"老太婆","马婆婆",TempText,xx,事件})
  elseif 事件 == "关于门派法术" and 名称 == "马婆婆"then
     SendMessage(UserData[id].连接id,20,{"老太婆","马婆婆","可以直接使用门派技能书对孩子使用"})
  elseif 事件 == "关于孩子进阶" and 名称 == "马婆婆"then
      SendMessage(UserData[id].连接id,20,{"老太婆","马婆婆","子女成长达到1.27使用圣兽丹可以进阶,进阶孩子可以再一次使用圣兽丹可以佩戴饰品"})
  elseif 事件 == "孩子饰品" and 名称 == "马婆婆"then
        SendMessage(UserData[id].连接id,20,{"老太婆","马婆婆","可以直接对进阶的孩子使用圣兽丹获得饰品"})
	elseif 事件 == "左护法" and 名称 == "帮派"then
		ItemControl:职务变更(id, "左护法")
	elseif 事件 == "右护法" and 名称 == "帮派"then
		ItemControl:职务变更(id, "右护法")
	elseif 事件 == "长老"and 名称 == "帮派" then
		ItemControl:职务变更(id, "长老")
	elseif 事件 == "谁怕谁开打" and RoleControl:GetTaskID(UserData[id],"法宝") ~= 0  then
		FightGet:进入处理(id, 100062, "66", RoleControl:GetTaskID(UserData[id],"法宝"))
	elseif 事件 == "帮众"and 名称 == "帮派" then
		ItemControl:职务变更(id, "帮众")

	elseif 事件 == "领取建设任务"and 名称 == "青龙堂总管" then
			if UserData[id].角色.帮派~=nil and 帮派数据[UserData[id].角色.帮派]~=nil and 帮派数据[UserData[id].角色.帮派].当前内政.名称~="无" then
         if RoleControl:GetTaskID(UserData[id],"帮派青龙")==0  then
               TaskControl:设置青龙任务(id)
         else 
             SendMessage(UserData[id].连接id,20,{"兰虎","青龙堂总管","你已经领取过青龙任务了"})
         end
					
			else
			SendMessage(UserData[id].连接id,7,"#y/当前没有可领取的建设任务")
			end
  elseif 事件 == "挑战神器副本（消耗1000W银币）"  and 名称 == "邪神蚩尤" then

    if 银子检查(id,10000000) then
        
        TaskControl:挑战蚩尤挑战(id)
      else
        SendMessage(UserData[id].连接id,20,{"蚩尤","蚩尤","走走走!你这个穷光蛋!"})
      end
      
	elseif 事件 == "上交物品"and 名称 == "青龙堂总管" then
			if RoleControl:GetTaskID(UserData[id],"帮派青龙")==0 then
			SendMessage(UserData[id].连接id,7,"#y/请先领取帮派建设任务")
			else
			self.发送信息={id=RoleControl:GetTaskID(UserData[id],"帮派青龙"),名称="青龙堂总管",道具=ItemControl:索要道具2(id,"包裹")}
			SendMessage(UserData[id].连接id,3011,self.发送信息)
			end
	elseif 事件 == "取消任务"and 名称 == "青龙堂总管" then
   		if RoleControl:GetTaskID(UserData[id],"帮派青龙") ~= 0 then
			任务数据[RoleControl:GetTaskID(UserData[id],"帮派青龙")] = nil
			RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"帮派青龙"))
			SendMessage(UserData[id].连接id,20,{"兰虎","青龙堂总管","你的青龙堂任务已经取消了"})
			帮派数据[UserData[id].角色.帮派].成员名单[id].青龙 = 0
		else
			SendMessage(UserData[id].连接id,20,{"兰虎","青龙堂总管","请先领取任务"})
		end
	elseif 事件 == "学习辅助技能"and 名称 == "帮派师爷" then
		  SendMessage(UserData[id].连接id,2019,RoleControl:学习技能信息(UserData[id],"辅助技能"))
	elseif 事件 == "学习生活技能"and 名称 == "帮派师爷" then
		 SendMessage(UserData[id].连接id,2019,RoleControl:学习技能信息(UserData[id],"辅助技能","你大爷"))
	elseif 事件 == "上交金银宝盒"and 名称 == "白虎堂总管" then
    	SendMessage(UserData[id].连接id,3011,{id=-701,名称="白虎堂总管",道具=ItemControl:索要道具2(id,"包裹")})
	elseif 事件 == "查看成员信息"and 名称 == "帮派管事" then
		ItemControl:获取帮派信息2(id,UserData[id].角色.帮派)
	elseif 事件 == "查看帮派信息"and 名称 == "帮派管事" then
		ItemControl:获取帮派信息1(id,UserData[id].角色.帮派)
	elseif 事件 == "更改帮派宗旨"and 名称 == "帮派管事" then
  		SendMessage(UserData[id].连接id, 15, {"帮派宗旨","请输入你需要修改的帮派宗旨内容"})


	elseif 事件 == "送我去聚义厅" then
		MapControl:Jump(id,1875,46,35)
	elseif 事件 == "送我去书院" then
		MapControl:Jump(id,1825,31,26)
	elseif 事件 == "送我去金库" then
		MapControl:Jump(id,1815,35,28)
	elseif 事件 == "送我去厢房" then
		MapControl:Jump(id,1845,30,23)
	elseif 事件 == "送我去兽室" then
		MapControl:Jump(id,1835,28,24)
	elseif 事件 == "送我去仓库" then
		MapControl:Jump(id,1865,30,23)
	elseif 事件 == "送我去长安城" then
		MapControl:Jump(id,1001,419,8)

	elseif 事件 == "领取任务"and 名称 == "玄武堂总管" then
       if RoleControl:GetTaskID(UserData[id],"帮派玄武")~=0 then
       SendMessage(UserData[id].连接id,7,"#y/你已经领取过玄武堂任务了")
      else
			TaskControl:添加玄武堂任务(id)
       end
    elseif 事件 == "送我进去" and 地图1 == 3131 then
        MapControl:Jump(id,3132,26,57)
	elseif 事件 == "取消任务"and 名称 == "玄武堂总管" then
   		if RoleControl:GetTaskID(UserData[id],"帮派玄武") ~= 0 then
   			MapControl:移除单位(任务数据[RoleControl:GetTaskID(UserData[id],"帮派玄武")].地图编号,RoleControl:GetTaskID(UserData[id],"帮派玄武"))
			RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"帮派玄武"))
			SendMessage(UserData[id].连接id,20,{"兰虎","玄武堂总管","你的玄武堂任务已经取消了"})
		else
			SendMessage(UserData[id].连接id,20,{"兰虎","玄武堂总管","请先领取任务"})
		end
	elseif 事件 == "修理装备" and 名称 == "装备修理商" then
           SendMessage(UserData[id].连接id,3019,ItemControl:索要道具3(id,"包裹"))
	elseif 事件 == "我要用千亿经验更换兽决"and 名称 == "李世民" and 地图1 == 1044 then
     if UserData[id].角色.飞升 ~= true then

        SendMessage(UserData[id].连接id, 7, "#y/飞升玩家才可以兑换")
        return 0
      end
	      if UserData[id].角色.当前经验 >= 100000000000 then
	      	UserData[id].角色.当前经验=UserData[id].角色.当前经验-100000000000
	      	SendMessage(UserData[id].连接id,7,"#y/当前经验减少100000000000点")

             local cj = ""
         local 奖励参数 = math.random(100)
          if 奖励参数 >=1 and 奖励参数 <=20 then
            ItemControl:GiveItem(id, "高级魔兽要诀")
             广播消息("#hd/".."#S/(千亿经验)" .. UserData[id].角色.名称 .. "#y/在长安皇宫李世民处，消耗了千亿经验成功兑换了#r高级魔兽要诀")
          elseif 奖励参数 >=21 and 奖励参数 <=31 then
               ItemControl:GiveItem(id,"特殊魔兽要诀",nil, "月光")
            广播消息(9, "#xt/#g/ " .. UserData[id].角色.名称 .. "#y/在长安皇宫李世民处，消耗了千亿经验成功兑换了特殊魔兽要诀")

          elseif 奖励参数 >=32 and 奖励参数 <=43 then
            ItemControl:GiveItem(id,"特殊魔兽要诀",nil, "灵能激发")
            广播消息(9, "#xt/#g/ " .. UserData[id].角色.名称 .. "#y/在长安皇宫李世民处，消耗了千亿经验成功兑换了#r特殊魔兽要诀")
          elseif 奖励参数 >=44 and 奖励参数 <=54 then
               ItemControl:GiveItem(id,"特殊魔兽要诀",nil, "月光")
            广播消息(9, "#xt/#g/ " .. UserData[id].角色.名称 .. "#y/在长安皇宫李世民处，消耗了千亿经验成功兑换了#r殊魔兽要诀")
          elseif 奖励参数 >=55 and 奖励参数 <=70 then
              ItemControl:GiveItem(id,"特殊魔兽要诀",nil, "月光")
            广播消息(9, "#xt/#g/ " .. UserData[id].角色.名称 .. "#y/在长安皇宫李世民处，消耗了千亿经验成功兑换了#r特殊魔兽要诀")
          elseif 奖励参数 >=71 and 奖励参数 <=75 then
               ItemControl:GiveItem(id,"特殊魔兽要诀",nil, "天降灵葫")
            广播消息(9, "#xt/#g/ " .. UserData[id].角色.名称 .. "#y/在长安皇宫李世民处，消耗了千亿经验成功兑换了#r特殊魔兽要诀")
          elseif 奖励参数 >=76 and 奖励参数 <=80 then
            ItemControl:GiveItem(id,"特殊魔兽要诀",nil, "出其不意")
            广播消息(9, "#xt/#g/ " .. UserData[id].角色.名称 .. "#y/在长安皇宫李世民处，消耗了千亿经验成功兑换了#r特殊魔兽要诀")
          elseif 奖励参数 >=81 and 奖励参数 <=82 then
             ItemControl:GiveItem(id,"特殊魔兽要诀",nil, "灵山禅语")
            广播消息(9, "#xt/#g/ " .. UserData[id].角色.名称 .. "#y/在长安皇宫李世民处，消耗了千亿经验成功兑换了#r特殊魔兽要诀")
            elseif 奖励参数 >=83 and 奖励参数 <=86 then
             ItemControl:GiveItem(id,"高级魔兽要诀",nil, "无畏布施")
            广播消息(9, "#xt/#g/ " .. UserData[id].角色.名称 .. "#y/在长安皇宫李世民处，消耗了千亿经验成功兑换了#r特殊魔兽要诀")
            elseif 奖励参数 >=87 and 奖励参数 <=88 then
             ItemControl:GiveItem(id,"高级魔兽要诀",nil, "凭风借力")
            广播消息(9, "#xt/#g/ " .. UserData[id].角色.名称 .. "#y/在长安皇宫李世民处，消耗了千亿经验成功兑换了#r特殊魔兽要诀")
            elseif 奖励参数 >=89 and 奖励参数 <=98 then
              ItemControl:GiveItem(id,"特殊魔兽要诀",nil, "八凶法阵")
            广播消息(9, "#xt/#g/ " .. UserData[id].角色.名称 .. "#y/在长安皇宫李世民处，消耗了千亿经验成功兑换了#r特殊魔兽要诀")
            elseif 奖励参数 >=99 and 奖励参数 <=100 then
              ItemControl:GiveItem(id,"特殊魔兽要诀",nil, "哼哼哈兮")
            广播消息(9, "#xt/#g/ " .. UserData[id].角色.名称 .. "#y/在长安皇宫李世民处，消耗了千亿经验成功兑换了#r特殊魔兽要诀")
              end


	      else
	       SendMessage(UserData[id].连接id,20,{"李世民","李世民","少侠你的经验不够呀需要千亿"})
	      end

	elseif 事件 == "我要挑战你"  then 
       local  JumpPlies= 1
      if  地图1 == 1004 then
           JumpPlies =1
      elseif  地图1 == 1005 then
            JumpPlies =2
      elseif  地图1 == 1006 then
        JumpPlies =3
      elseif  地图1 == 1007 then
        JumpPlies =4
      elseif  地图1 == 1008 then
        JumpPlies =5
      elseif  地图1 == 1090 then
        JumpPlies =6
      -- elseif  地图1 == 1009 then
      --   JumpPlies =7
      elseif 地图1 >= 7000 and  地图1<=7092 then
         JumpPlies = 地图1-6992
      end 
      TaskControl:挑战大雁塔(id,JumpPlies)

      elseif 事件 == "我要挑战你大雁塔"  then 



    local 检查通过 =""

     if UserData[id].队伍 ~= 0 then
            for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
              
                if UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.活跃度 < 100  then
                    检查通过 =检查通过..UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称.."、"
                end
               
            end
      elseif UserData[id].角色.活跃度 < 100 then 
             SendMessage(UserData[id].连接id,20,{"红萼仙子","七层守卫","你的活跃度不足100"}) 
             return 
      end
      if 检查通过~= "" then 
          SendMessage(UserData[id].连接id,20,{"红萼仙子","七层守卫","队员："..检查通过.."活跃度不足100点无法进行挑战"})
      else 
            if UserData[id].队伍 ~= 0 then
                  for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
                      RoleControl:扣除活跃度(UserData[队伍数据[UserData[id].队伍].队员数据[n]],100)
                  end
            else
                   RoleControl:扣除活跃度(UserData[id],100)
            end
          if  地图1 == 1009 then
              JumpPlies =7
          end 
         TaskControl:挑战大雁塔(id,JumpPlies)
      end 


      
	elseif 事件 == "什么是化境" and 地图1 ==1114  and 名称 == "吴刚" then
			 SendMessage(UserData[id].连接id,20,{"大生","吴刚","万物变化，虚而不屈，相生相克，无穷无尽，欲强者先弱，欲扬者先抑，欲刚者先柔，欲取者先与，欲升者先降，欲张者先合。入与化境之后会变的暂时弱些，但是不久你就可以达到更强的境界。"})
	elseif 事件 == "我想入化境，请指点一二。" and 地图1 ==1114  and 名称 == "吴刚" then
	    if UserData[id].角色.飞升 ~= true then
			if UserData[id].角色.等级 < 135 then
				SendMessage(UserData[id].连接id, 7, "#y/等级达到135级后才可进行飞升挑战")
				return 0
			else
				self.是否获得 = false
				for i = 1, #UserData[id].角色.师门技能 do
					if UserData[id].角色.师门技能[i].等级 < 140 then
						self.是否获得 = true
					end
				end
        self.是否获得1=false
                self.修炼关键词="人物修炼"
               -- for n=1,#全局变量.修炼名称 do
                  for n=1,#全局变量.修炼名称 do
                    if  UserData[id].角色.人物修炼.猎术.等级 < 15 then
                    UserData[id].角色.人物修炼.猎术.等级=25
                    end

                    if UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].等级<15 then
                    self.是否获得1=true
                    end
                end
                if  self.是否获得1 and DebugMode ==false then
                SendMessage(UserData[id].连接id,7,"#y/所有人物修炼技能达到15后才可进行飞升挑战")
                return 0
                end
            self.是否获得2=false
            self.修炼关键词="召唤兽修炼"
            for n=1,#全局变量.修炼名称 do
                if  UserData[id].角色.召唤兽修炼.猎术.等级 < 15 then
                    UserData[id].角色.召唤兽修炼.猎术.等级=25
                    end

                if UserData[id].角色.召唤兽修炼[全局变量.修炼名称[n]].等级<15 then
                self.是否获得2=true
                end
            end
            if  self.是否获得2 and DebugMode ==false then
            SendMessage(UserData[id].连接id,7,"#y/所有召唤修炼技能达到15后才可进行飞升挑战")
            return 0
 end
				if self.是否获得 then
					SendMessage(UserData[id].连接id, 7, "#y/所有门派技能达到140级后才可进行飞升挑战")
					return 0
				else
					FightGet:进入处理(id, 100033, "66", 1)
				end
			end
		else
			SendMessage(UserData[id].连接id, 7, "#y/你已经完成了飞升挑战，请不要来调戏我")
		end
	elseif 事件 == "入化境后有什么变化？" and 地图1 ==1114  and 名称 == "吴刚" then
		SendMessage(UserData[id].连接id,20,{"大生","吴刚","飞升以后可以在我这里提高人物修炼和召唤兽修炼哦"})
	elseif 事件 == "我已经通过考验，请带我入与化境。" and 地图1 ==1114  and 名称 == "吴刚" then
		if UserData[id].角色.飞升 then
				self.修炼关键词 = "人物修炼"

				for n = 1, #全局变量.修炼名称 do
					if UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].等级 >= 20 and UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].上限 < 25 then
						UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].上限+1
						UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].等级 - 1
					end
				end

				self.修炼关键词 = "召唤兽修炼"

				for n = 1, #全局变量.修炼名称 do
					if UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].等级 >= 20 and UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].上限 < 25 then
						UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].上限 = UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].上限+1
						UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].等级 = UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].等级 - 1
					end
				end

				SendMessage(UserData[id].连接id, 7, "#y/已为您对修炼进行了调整，请注意只有修炼等级达到20级才可以提升修炼等级上限噢")
			else
				SendMessage(UserData[id].连接id, 7, "#y/只有飞升后的玩家才可以在我这里提升修炼上限")
			end
    elseif 事件 == "打开仓库" and 名称 == "仓库管理员" then
    	 SendMessage(UserData[id].连接id, 3009, ItemControl:索要道具1(id, "包裹"))
		SendMessage(UserData[id].连接id, 3010, ItemControl:索要仓库道具(id, "1"))
    elseif 事件 == "购买仓库(消耗50万银子)" and 名称 == "仓库管理员" then
			if #UserData[id].角色.仓库>=26 then
			SendMessage(UserData[id].连接id,7,"#Y/您当前可拥有的仓库数量已经达到上限")
			elseif
			UserData[id].角色.道具.货币.银子<500000 then
			SendMessage(UserData[id].连接id,7,"#Y/购买仓库失败，你身上的钱不够")
			else
			RoleControl:扣除银子(UserData[id],500000,10)
			UserData[id].角色.仓库[#UserData[id].角色.仓库+1]={}
			SendMessage(UserData[id].连接id,7,"#y/购买仓库成功")
			RoleControl:添加消费日志(UserData[id],"花费50万购买仓库,购买仓库编号："..#UserData[id].角色.仓库)
			end
    elseif 事件 == "购买仓库" and 名称 == "仓库管理员" then
      if #UserData[id].角色.仓库>=26 then
      SendMessage(UserData[id].连接id,7,"#Y/您当前可拥有的仓库数量已经达到上限")
      elseif
      UserData[id].角色.道具.货币.银子<500000 then
      SendMessage(UserData[id].连接id,7,"#Y/购买仓库失败，你身上的钱不够")
      else
      RoleControl:扣除银子(UserData[id],500000,10)
      UserData[id].角色.仓库[#UserData[id].角色.仓库+1]={}
      SendMessage(UserData[id].连接id,7,"#y/购买仓库成功")
      RoleControl:添加消费日志(UserData[id],"花费50万购买仓库,购买仓库编号："..#UserData[id].角色.仓库)
      end
    elseif 事件 == "存款" and 名称 == "钱庄老板" then
        	SendMessage(UserData[id].连接id, 11011, {银子=UserData[id].角色.道具.货币.银子,存银= UserData[id].角色.道具.货币.存银})
    elseif 事件 == "取款" and 名称 == "钱庄老板" then
    	    SendMessage(UserData[id].连接id, 11012, {银子=UserData[id].角色.道具.货币.银子,存银=UserData[id].角色.道具.货币.存银})
	elseif 事件 == "我的召唤兽受伤了请帮我救治一下吧" and 名称 == "超级巫医" then
		UserData[id].召唤兽:巫医治疗(id,1)
	elseif 事件 == "我的召唤兽忠诚度降低了请帮我驯养一下吧" and 名称 == "超级巫医" then
		UserData[id].召唤兽:巫医治疗(id,2)
	elseif 事件 == "我要同时补满召唤兽的气血、魔法和忠诚值" and 名称 == "超级巫医" then
		UserData[id].召唤兽:巫医治疗(id,3)
	elseif 事件 == "重置召唤兽属性点(消耗300万银子)" and 名称 == "超级巫医" then
			if UserData[id].召唤兽.数据.参战==0 then
			 local wb = "请先将要洗点的召唤兽设置为参战状态"
	         SendMessage(UserData[id].连接id,20,{"超级巫医","超级巫医",wb})
			elseif UserData[id].角色.道具.货币.银子<3000000 then
			local wb = "召唤兽属性点重置失败，你身上的钱不够"
	         SendMessage(UserData[id].连接id,20,{"超级巫医","超级巫医",wb})
			else
			RoleControl:扣除银子(UserData[id],3000000,13)
			UserData[id].召唤兽:重置属性点(UserData[id].召唤兽.数据.参战)
			RoleControl:添加消费日志(UserData[id],"花费3000000银子重置召唤兽属性点")
			SendMessage(UserData[id].连接id,7,"#Y/召唤兽属性点重置成功")
			end




   elseif 事件 == "我要渡劫" and 地图1 == 1111 and 名称 == "渡劫使者" then --自己改地图和对话和名字  渡劫
            if UserData[id].角色.渡劫~=true  then
            if UserData[id].角色.等级<155 and DebugMode ==false then
                SendMessage(UserData[id].连接id,7,"#y/等级达到155后才可进行渡劫挑战")
                return 0
            else
                self.是否获得 = false
                for i = 1, #UserData[id].角色.师门技能 do
                    if UserData[id].角色.师门技能[i].等级 < 165 then
                        self.是否获得 = true
                    end
                end
                if  self.是否获得 and DebugMode ==false then
                SendMessage(UserData[id].连接id,7,"#y/所有门派技能达到165后才可进行渡劫挑战")
                return 0
                end
                self.是否获得1=false
                self.修炼关键词="人物修炼"
                for n=1,#全局变量.修炼名称 do
                  if  UserData[id].角色.人物修炼.猎术.等级 < 25 then
                    UserData[id].角色.人物修炼.猎术.等级=25
                    end

                    if UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].等级<25 then
                    self.是否获得1=true
                    end
                end
                if  self.是否获得1 and DebugMode ==false then
                SendMessage(UserData[id].连接id,7,"#y/所有人物修炼技能达到25后才可进行渡劫挑战")
                return 0
                end
            self.是否获得2=false
            self.修炼关键词="召唤兽修炼"
            for n=1,#全局变量.修炼名称 do
               if  UserData[id].角色.召唤兽修炼.猎术.等级 < 25 then
                    UserData[id].角色.召唤兽修炼.猎术.等级=25
                    end

                if UserData[id].角色.召唤兽修炼[全局变量.修炼名称[n]].等级<25 then
                self.是否获得2=true
                end
            end
            if  self.是否获得2 and DebugMode ==false then
            SendMessage(UserData[id].连接id,7,"#y/所有召唤修炼技能达到25后才可进行渡劫挑战")
            return 0
            else
            FightGet:进入处理(id,100044,"66",1)
            end
            end
            else
            SendMessage(UserData[id].连接id,7,"#y/你已经完成了渡劫挑战，请不要来调戏我")
            end
  elseif 事件 == "渡劫降修" and 地图1 == 1111 and 名称 == "渡劫使者" then --自己改地图和对话和名字  渡劫
        if UserData[id].角色.渡劫 then
                self.修炼关键词="人物修炼"
                for n=1,#全局变量.修炼名称 do
                        if UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].等级>=25 and UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].上限<30 then
                        UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].上限=30
                        UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].等级=UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].等级
                        end
                end
                self.修炼关键词="召唤兽修炼"
                for n=1,#全局变量.修炼名称 do
                    if UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].等级>=25 and UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].上限<30 then
                    UserData[id].角色[self.修炼关键词][全局变量.修炼名称[n]].上限=30
                    end
                end
                SendMessage(UserData[id].连接id,7,"#y/已为您对修炼进行了调整，请注意只有修炼等级达到25才可以提升修炼等级上限噢")
        else
        SendMessage(UserData[id].连接id,7,"#y/只有渡劫后的玩家才可以在我这里提升修炼上限")
        end


    elseif 事件 == "60级(扣除60点活力)" and 地图1 == 1019 and 名称 == "颜如玉" then
          RoleControl:制作灵饰图鉴(UserData[id],60)
    elseif 事件 == "80级(扣除80点活力)" and 地图1 == 1019 and 名称 == "颜如玉" then
			RoleControl:制作灵饰图鉴(UserData[id],80)
    elseif 事件 == "100级(扣除100点活力)" and 地图1 == 1019 and 名称 == "颜如玉" then
			RoleControl:制作灵饰图鉴(UserData[id],100)
    elseif 事件 == "120级(扣除120点活力)" and 地图1 == 1019 and 名称 == "颜如玉" then
			RoleControl:制作灵饰图鉴(UserData[id],120)
    elseif 事件 == "140级(扣除140点活力)" and 地图1 == 1019 and 名称 == "颜如玉" then
			RoleControl:制作灵饰图鉴(UserData[id],140)
    elseif 事件 == "160级(扣除160点活力)" and 地图1 == 1019 and 名称 == "颜如玉" then
			RoleControl:制作灵饰图鉴(UserData[id],160)
    elseif 事件 == "是的,我要打工" and 地图1 == 1019 and 名称 == "颜如玉" then
    	if UserData[id].角色.当前体力 < 40 then
    		SendMessage(UserData[id].连接id, 7, "#y/当前体力不足40点,无法打工")
    	 else
    	 	UserData[id].角色.当前体力=UserData[id].角色.当前体力-40
         RoleControl:添加银子(UserData[id],3000, "书店打工")
    	end

    elseif 事件 == "是的我要打工" and 地图1 == 1019 and 名称 == "颜如玉" then
    	SendMessage(UserData[id].连接id,20,{"老书生","颜如玉","每次打工需要40体力,额外获得银子3000银子,是否要打工呢?",{"是的,我要打工","不用了"}})
    elseif 事件 == "我来制作灵饰图鉴" and 地图1 == 1019 and 名称 == "颜如玉" then
	    SendMessage(UserData[id].连接id,20,{"老书生","颜如玉","我这里可以制作60-160的灵饰图鉴,每次扣除对应等级的活力,你选择哪个阶段的呢?",{"60级(扣除60点活力)","80级(扣除80点活力)","100级(扣除100点活力)","120级(扣除120点活力)","140级(扣除140点活力)","160级(扣除160点活力)","我来看看"}})
	elseif 事件 == "交易" and 地图1 == 1501 and 名称 == "长寿商人" then
    				SendMessage(UserData[id].连接id,20,{"钱庄老板","长寿商人","你没有跑商任务瞎点什么"})
   
   elseif 事件 == "我要打开PK开关" and 地图1 == 1070 and 名称 == "强行PK申请人" then

        if UserData[id].角色.道具.货币.银子<300000000 then
     SendMessage(UserData[id].连接id,7,"#y/你没有3亿银两，无法启动PK开关")
    return 0

  end
			if UserData[id].角色.pk开关 == false then
				UserData[id].角色.pk开关 = true
				UserData[id].角色.pk时间 = os.time() + 43200
       RoleControl:扣除银子(UserData[id], 300000000, "PK")
    SendMessage(UserData[id].连接id,7,"#y/扣除成功")
			   SendMessage(UserData[id].连接id,20,{"兰虎","强行PK申请人","你打开了pk开关，现在可以自由攻击其他玩家了，pk开关最少要在24小时后才可被关闭"})
			else
				 SendMessage(UserData[id].连接id,20,{"兰虎","强行PK申请人","你已经打开过PK开关了。"})
			end


   elseif 事件 == "我要关闭PK开关" and 地图1 == 1070 and 名称 == "强行PK申请人" then
   	        if UserData[id].角色.pk开关 == false then
   	        	SendMessage(UserData[id].连接id,20,{"兰虎","强行PK申请人","你还没有开启过PK开关。"})
 			elseif os.time() < UserData[id].角色.pk时间 then
				SendMessage(UserData[id].连接id,20,{"兰虎","强行PK申请人","#W/您需要等待#r/" .. math.floor((UserData[id].角色.pk时间 - os.time()) / 60) .. "#W/分钟后才可关闭pk开关"})
			else
				UserData[id].角色.pk开关 = false
				SendMessage(UserData[id].连接id,20,{"兰虎","强行PK申请人","有话好好说，我已经帮你关闭了PK开关。"})
			end
   elseif 事件 == "我来领取任务链任务" and 地图1 == 1070 and 名称 == "陆萧然" then
		   		if UserData[id].角色.等级 < 60 then
					SendMessage(UserData[id].连接id,20,{"老书生","陆萧然","只有等级达到60级的玩家才可帮助朝廷"})
					return 0
				elseif RoleControl:GetTaskID(UserData[id],"任务链") ~= 0 then
					SendMessage(UserData[id].连接id,20,{"老书生","陆萧然","你已经领取过此任务了"})
					return 0
				elseif UserData[id].队伍 ~= 0 then
					SendMessage(UserData[id].连接id, 7, "#y/该任务不允许组队领取")
				elseif 银子检查(id, 取角色等级(id) * 取角色等级(id) * 20) == false then
					SendMessage(UserData[id].连接id, 7, "#y/领取此任务需要消耗" .. 取角色等级(id) * 取角色等级(id) * 20 .. "两银子")
					return 0
				else
					RoleControl:扣除银子(UserData[id], 取角色等级(id) * 取角色等级(id) * 20, 2)
          local lsid =tonumber(UserData[id].id .. "14")
					任务数据[lsid] = {
						次数 = 0,
						类型 = "任务链",
						编号 = 0,
						npc名称 = "太白金星",
						npc编号 = 1309,
						结束 = 3600,
						跳过 = 1,
						id = id,
						起始 = os.time(),
						任务id = lsid,
						数字id = UserData[id].id
					}
					TaskControl:设置任务链(id, lsid)
					UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])] = lsid
                    TaskControl:刷新追踪任务信息(id)
				end
   elseif 事件 == "我来取消任务链任务" and 地图1 == 1070 and 名称 == "陆萧然" then
   	if RoleControl:GetTaskID(UserData[id],"任务链") ~= 0 then

			RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"任务链"))
            任务数据[RoleControl:GetTaskID(UserData[id],"任务链")] = nil
			SendMessage(UserData[id].连接id,20,{"老书生","陆萧然","你的任务链取消了"})
		else
			SendMessage(UserData[id].连接id,20,{"老书生","陆萧然","请先领取任务"})
		end

     elseif 事件 =="我要查看长安保卫战当前积分" and 地图1 == 1110 then
        SendMessage(UserData[id].连接id, 7, "#y/你当前有".. UserData[id].角色.长安保卫战积分.."长安保卫战积分")

        elseif 事件 =="我要查看长安保卫战累计积分" and 地图1 == 1110 then
        SendMessage(UserData[id].连接id, 7, "#y/你当前有".. UserData[id].角色.长安保卫战累计积分.."长安保卫战累计积分")

        elseif 事件 =="我要抽奖(消耗长安保卫战当前积分)" and 地图1 == 1110 then

      if  UserData[id].角色.长安保卫战积分 < 25 then
      SendMessage(UserData[id].连接id, 7, "#y/抽奖需要消耗25点长安保卫战积分")
    else
      RoleControl:扣除长安保卫战积分(UserData[id], 25)
           local cj = ""
         local 奖励参数 = math.random(75)
          if 奖励参数 >=1 and 奖励参数 <=20 then
            ItemControl:GiveItem(id, "聚灵袋")
             广播消息("#hd/".."#S/(长安保卫战)" .. UserData[id].角色.名称 .. "#y/抵御西方妖魔有功，唐王奖励了#r聚灵袋")
          elseif 奖励参数 >=21 and 奖励参数 <=40 then
               ItemControl:GiveItem(id,"聚气袋")
            广播消息("#hd/".."#S/(长安保卫战)" .. UserData[id].角色.名称 .. "#y/抵御西方妖魔有功，唐王奖励了#r聚气袋")
             elseif 奖励参数 >=41 and 奖励参数 <=50 then
               ItemControl:GiveItem(id,"萝卜王")
            广播消息("#hd/".."#S/(长安保卫战)" .. UserData[id].角色.名称 .. "#y/抵御西方妖魔有功，唐王奖励了#r萝卜王")

             elseif 奖励参数 >=51 and 奖励参数 <=55 then
               ItemControl:GiveItem(id,"灵兜兜")
            广播消息("#hd/".."#S/(长安保卫战)" .. UserData[id].角色.名称 .. "#y/抵御西方妖魔有功，唐王奖励了#r灵兜兜")

            elseif 奖励参数 >=56 and 奖励参数 <=65 then
               
               ItemControl:GiveItem(id, "珍珠", math.random(14, 16) * 10)
                广播消息("#hd/".."#S/(长安保卫战)" .. UserData[id].角色.名称 .. "#y/抵御西方妖魔有功，唐王奖励了#r珍珠")

                 elseif 奖励参数 >=66 and 奖励参数 <=75 then
               
               ItemControl:GiveItem(id, "锦衣碎片",nil,nil,2)
                广播消息("#hd/".."#S/(长安保卫战)" .. UserData[id].角色.名称 .. "#y/抵御西方妖魔有功，唐王奖励了#r锦衣碎片*2")
            

         
              end
       
      
    end

     elseif 事件 =="我要查看新年积分" and 地图1 == 1001 then
   
    SendMessage(UserData[id].连接id, 7, "#y/你当前有".. UserData[id].角色.新年积分.."新年积分")
  elseif 事件 =="我要抽奖(消耗新年积分)" and 地图1 == 1001 then

      if  UserData[id].角色.新年积分 < 25 then
      SendMessage(UserData[id].连接id, 7, "#y/抽奖需要消耗25点新年积分")
    else
      RoleControl:扣除新年积分(UserData[id], 25)
           local cj = ""
         local 奖励参数 = math.random(75)
          if 奖励参数 >=1 and 奖励参数 <=20 then
            ItemControl:GiveItem(id, "聚灵袋")
             广播消息("#hd/".."#S/(新年积分)" .. UserData[id].角色.名称 .. "#y/在新年活动使者处，消耗了新年积分成功兑换了#r聚灵袋")
          elseif 奖励参数 >=21 and 奖励参数 <=40 then
               ItemControl:GiveItem(id,"聚气袋")
            广播消息("#hd/".."#S/(新年积分)" .. UserData[id].角色.名称 .. "#y/在新年活动使者处，消耗了新年积分成功兑换了#r聚气袋")
             elseif 奖励参数 >=41 and 奖励参数 <=50 then
               ItemControl:GiveItem(id,"萝卜王")
            广播消息("#hd/".."#S/(新年积分)" .. UserData[id].角色.名称 .. "#y/在新年活动使者处，消耗了新年积分成功兑换了#r萝卜王")

             elseif 奖励参数 >=51 and 奖励参数 <=55 then
               ItemControl:GiveItem(id,"灵兜兜")
            广播消息("#hd/".."#S/(新年积分)" .. UserData[id].角色.名称 .. "#y/在新年活动使者处，消耗了新年积分成功兑换了#r灵兜兜")

            elseif 奖励参数 >=56 and 奖励参数 <=65 then
               
               ItemControl:GiveItem(id, "珍珠", math.random(14, 16) * 10)
                广播消息("#hd/".."#S/(新年积分)" .. UserData[id].角色.名称 .. "#y/在新年活动使者处，消耗了新年积分成功兑换了#r珍珠")

                 elseif 奖励参数 >=66 and 奖励参数 <=75 then
               
               ItemControl:GiveItem(id, "锦衣碎片",nil,nil,2)
                广播消息("#hd/".."#S/(新年积分)" .. UserData[id].角色.名称 .. "#y/在新年活动使者处，消耗了新年积分成功兑换了#r锦衣碎片*2")
            

         
              end
       
      
    end

   elseif 事件 == "确认跳过任务" and 地图1 == 1070 and 名称 == "陆萧然" then
     local taskData =RoleControl:GetTaskID(UserData[id],"任务链")
   		if 银子检查(id, 任务数据[taskData].跳过 * 2500000) == false then
			SendMessage(UserData[id].连接id, 7, "#y/领取此任务需要消耗" .. 任务数据[taskData].跳过 * 2500000 .. "两银子")
		else
			RoleControl:扣除银子(UserData[id], 任务数据[taskData].跳过 * 2500000, 2)

			任务数据[taskData].跳过 = 任务数据[taskData].跳过 + 1

			TaskControl:完成任务链(id, taskData, 1)
		end
   elseif 事件 == "跳过任务(消耗银子)" and 地图1 == 1070 and 名称 == "陆萧然" then
   	   	  	if RoleControl:GetTaskID(UserData[id],"任务链") ~= 0 then
   	  		local wb1="你可以在我这里花费银子跳过任务，根据跳过任务的次数而需要花费一定数量的银子。跳过此任务仍然会获得此任务奖励。您当前需要花费#Y/" .. 任务数据[RoleControl:GetTaskID(UserData[id],"任务链")].跳过 * 2500000 .."#W/两银子，请确认是否跳过此任务："
			SendMessage(UserData[id].连接id,20,{"老书生","陆萧然",wb1,{"确认跳过任务","我在考虑一下"}})
		else
			SendMessage(UserData[id].连接id,20,{"老书生","陆萧然","请先领取任务"})
		end
 --桃源村
	elseif 事件 == "是的送我过去"and 地图1==1003 and 名称 == "新手接引人" then
  		 MapControl:Jump(id,1001,39,223)
       elseif 事件 == "生活技能" and 地图1 == 1003 and 名称 == "生活技能" then
         SendMessage(UserData[id].连接id,2019,RoleControl:学习技能信息(UserData[id],"辅助技能"))
          elseif 事件 == "强化技能" and 地图1 == 1003 and 名称 == "技能强化" then
       SendMessage(UserData[id].连接id,2019,RoleControl:学习技能信息(UserData[id],"强化技能")) --学习强化技能
    --建业noc

    elseif 事件 == "我已经想清楚了" and 地图1 == 1501 and 名称 == "勾魂马面" then
        SendMessage(UserData[id].连接id, 15, {"删除角色","请输入你需要删除的角色ID："..UserData[id].id})

    elseif 事件 == "交易" and 地图1 == 1501 and 名称 == "建邺特产商人" then
    				SendMessage(UserData[id].连接id,20,{"特产商人","建邺特产商人","你没有跑商任务瞎点什么"})
    elseif 事件 == "我要鉴定" and (地图1 == 1501 or 地图1 == 1001  )and 名称 == "装备鉴定商" then
    	SendMessage(UserData[id].连接id,3020,ItemControl:索要道具3(id,"包裹"))
    elseif 事件 == "我要更换当前的宠物" and 地图1 == 1501 and 名称 == "宠物仙子" then
			if UserData[id].角色.宠物.领取 ~= nil then
			 SendMessage(UserData[id].连接id,20,{"小花","宠物仙子","#Y/已经领养过一次宠物了"})
			else
			 SendMessage(UserData[id].连接id,2060,"1")
			end
	elseif 事件 == "购买" and 地图1 == 1501 and 名称 == "飞儿" then
		 ShopControl:GetNpcShop(id,0)
   	elseif 事件 == "填写推广员号" and 地图1 == 1501 and 名称 == "推广使者" then
    elseif 事件 == "学习强化技能" and 地图1 == 1501 and 名称 == "教书先生" then
       SendMessage(UserData[id].连接id,2019,RoleControl:学习技能信息(UserData[id],"强化技能")) --学习强化技能
	elseif 事件 == "快些治疗我吧" and 地图1 == 1501 and 名称 == "陈长寿" then
		if UserData[id].角色.等级 <= 100 then
				UserData[id].角色.当前气血 = UserData[id].角色.最大气血
				UserData[id].角色.气血上限 = UserData[id].角色.最大气血
				UserData[id].角色.当前魔法 = UserData[id].角色.魔法上限
			local wb = "你已经恢复健康了"
          self.发送文本 ={"药店老板","陈长寿",wb}
			SendMessage(UserData[id].连接id,20,table.tostring(self.发送文本))
		else
		    local wb = "少侠你已经不需要我的帮助了"
		    self.发送文本 ={"药店老板","陈长寿",wb}
		    SendMessage(UserData[id].连接id,20,table.tostring(self.发送文本))
		end
	 elseif 事件 == "领取维护礼包" and (地图1 == 1501 or 地图1 == 1001) and 名称 == "礼物兔子" then
          if  UserData[id].角色.维护版本 ==nil or UserData[id].角色.维护版本 ~= ServerConfig.版本 then
           	 	 RoleControl:添加储备(UserData[id],1000000 *math.random(50,100),"维护奖励")
           	 	 RoleControl:添加经验(UserData[id],UserData[id].角色.等级 *UserData[id].角色.等级*math.random(50,100),"维护奖励")
           	 	 UserData[id].角色.维护版本 = ServerConfig.版本
              -- EquipmentControl:取装备礼包(id,100,true,nil,true)
              --RoleControl:添加称谓(UserData[id],"★内测元勋★")

               
           	 广播消息("#hd/#W/玩家#G/"..UserData[id].角色.名称.."#W/在长安礼物兔子处领取了维护补偿!")
          else
               SendMessage(UserData[id].连接id,20,{"兔子怪","礼物兔子","你已经领取过了维护礼包,或者当前版本没有更新!"})
          end

  elseif 事件 == "把我送出场地" and ( 地图1 == 11351 or 地图1 == 11352 or 地图1 == 11353 or 地图1 == 11354 or 地图1 == 11355 )and 名称 == "帮派竞赛主持人" then
          if UserData[id].角色.帮派 == nil then
          SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有加入一个帮派哟")
          else
          帮派竞赛:送出场地(id)
          end
  elseif 事件 == "我要进入场地领取宝箱" and 地图1 == 1001 and 名称 == "帮派竞赛主持人" then
          if UserData[id].角色.帮派 == nil then
          SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有加入一个帮派哟")
          else
          帮派竞赛:进入帮派宝箱(id)
          end
  elseif 事件 == "我要报名参赛" and 地图1 == 1001 and 名称 == "帮派竞赛主持人" then
          if UserData[id].角色.帮派 == nil then
          SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有加入一个帮派哟")
          else
          帮派竞赛:报名参赛(id)
          end
  elseif 事件 == "送我去比赛场地" and 地图1 == 1001 and 名称 == "帮派竞赛主持人" then
          if UserData[id].角色.帮派 == nil then
          SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有加入一个帮派哟")
          else
          帮派竞赛:进入场地(id)
          end
  elseif 事件 == "对战表" and (地图1 == 1001 or 地图1 == 11351 or 地图1 == 11352 or 地图1 == 11353 or 地图1 == 11354 or 地图1 == 11355 )and 名称 == "帮派竞赛主持人" then
          if UserData[id].角色.帮派 == nil then
          SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有加入一个帮派哟")
          else
          帮派竞赛:查看对战表(id)
          end
  elseif 事件 == "我要进入帮派迷宫" and 地图1 == 1001 and 名称 == "帮派竞赛主持人" then
          if UserData[id].角色.帮派 == nil then
          SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有加入一个帮派哟")
          else
          帮派竞赛:进入帮派迷宫(id)
          end
   

   elseif 事件 == "我要进入帮派迷宫" and 地图1 == 1001 and 名称 == "帮派竞赛主持人" then

		  ItemControl:索要出售道具(id,"包裹")
      SendMessage(UserData[id].连接id,3075,"出售界面")

	elseif 事件 == "我要做其他事情" and 地图1 == 1501 and 名称 == "牛大胆" then

		tp:对话事件(1501,13,1)

	elseif 地图1 == 1506 and 事件 == "是的我要去" and 名称 == "船夫" then
		if UserData[id].角色.等级 >= 10 then

          MapControl:跳转处理(id,{1092,165*20,132*20})
		else
			 SendMessage(UserData[id].连接id,20,{"驿站老板","船夫","等级不够，不敢给你传送"})
		end


	elseif 事件 == "我卖大海龟（250两银子/只或者300两储备金/只）给你" and 地图1 == 1501 and 名称 == "海产收购商" then
		 wb = "请选择你要出售的方式#R（会将所有未参战状态下的大海龟全部出售！）"
		 xx = {"250两银子卖给你","300两储备银子卖给你"}
		    SendMessage(UserData[id].连接id,20,{"捕鱼人","海产收购商",wb,xx})
	elseif 事件 == "我卖巨蛙（350两银子/只或者400两储备金/只）给你" and 地图1 == 1501 and 名称 == "海产收购商" then
		 wb = "请选择你要出售的方式#R（会将所有未参战状态下的巨蛙全部出售！）"
		 xx = {"350两银子卖给你","400两储备银子卖给你"}
		    SendMessage(UserData[id].连接id,20,{"捕鱼人","海产收购商",wb,xx})
	elseif 事件 == "我卖海毛虫（500两银子/只或者600两储备金/只）给你" and 地图1 == 1501 and 名称 == "海产收购商" then
		 wb = "请选择你要出售的方式#R（会将所有未参战状态下的海毛虫全部出售！）"
		 xx = {"500两银子卖给你","600两储备银子卖给你"}
		    self.发送文本 ={"捕鱼人","海产收购商",wb,xx}
		    SendMessage(UserData[id].连接id,20,table.tostring(self.发送文本))
	elseif 事件 == "250两银子卖给你" and 地图1 == 1501 and 名称 == "海产收购商" then
		local zhs = 0
		for n=#UserData[id].召唤兽.数据,1,-1 do
			if UserData[id].召唤兽.数据[n] ~= nil and n ~= UserData[id].召唤兽.数据.参战  and UserData[id].召唤兽.数据[n].造型 == "大海龟" then
				zhs = zhs + 1
				table.remove(UserData[id].召唤兽.数据,n)
			end
		end
		if zhs == 0 then
			wb = "你身上没有我要的召唤兽"
			self.发送文本 ={"捕鱼人","海产收购商",wb}
		    SendMessage(UserData[id].连接id,20,table.tostring(self.发送文本))
		else
			local cb = (250 * zhs)
      RoleControl:添加银子(UserData[id],cb,"捕鱼人")
		
			wb = "你成功出售了#Y/"..zhs.."只#W/大海龟给我共赚取了#R"..cb.."两银子"
			self.发送文本 ={"捕鱼人","海产收购商",wb,xx}
		    SendMessage(UserData[id].连接id,20,table.tostring(self.发送文本))
			SendMessage(UserData[id].连接id,3040,"1")
		end
	elseif 事件 == "300两储备银子卖给你" and 地图1 == 1501 and 名称 == "海产收购商" then
	    local zhs = 0
		for n=1,#UserData[id].召唤兽.数据 do
			if UserData[id].召唤兽.数据[n] ~= nil and n ~= UserData[id].召唤兽.数据.参战  and UserData[id].召唤兽.数据[n].造型 == "大海龟" then
				zhs = zhs + 1
				table.remove(UserData[id].召唤兽.数据,n)
			end
		end
		if zhs == 0 then
			wb = "你身上没有我要的召唤兽"
			self.发送文本 ={"捕鱼人","海产收购商",wb}
		    SendMessage(UserData[id].连接id,20,table.tostring(self.发送文本))
		else
			local cb = (250 * zhs)
			UserData[id].角色.道具.货币.储备 = UserData[id].角色.道具.货币.储备 + cb
			wb = "你成功出售了#Y/"..zhs.."只#W/大海龟给我共赚取了#R"..cb.."两银子"
			self.发送文本 ={"捕鱼人","海产收购商",wb,xx}
		    SendMessage(UserData[id].连接id,20,table.tostring(self.发送文本))
			SendMessage(UserData[id].连接id,3040,"1")
		end
	elseif 事件 == "350两银子卖给你" and 地图1 == 1501 and 名称 == "海产收购商" then
		local zhs = 0
		for n=1,#UserData[id].召唤兽.数据 do
			if UserData[id].召唤兽.数据[n] ~= nil and n ~= UserData[id].召唤兽.数据.参战  and UserData[id].召唤兽.数据[n].造型 == "巨蛙" then
				zhs = zhs + 1
				table.remove(UserData[id].召唤兽.数据,n)
			end
		end
		if zhs == 0 then
			wb = "你身上没有我要的召唤兽"
			self.发送文本 ={"捕鱼人","海产收购商",wb}
		    SendMessage(UserData[id].连接id,20,table.tostring(self.发送文本))
		else
			local cb = (350 * zhs)
			  RoleControl:添加银子(UserData[id],cb,"捕鱼人")
			wb = "你成功出售了#Y/"..zhs.."只#W/巨蛙给我共赚取了#R"..cb.."两银子"
			self.发送文本 ={"捕鱼人","海产收购商",wb,xx}
		    SendMessage(UserData[id].连接id,20,table.tostring(self.发送文本))
			SendMessage(UserData[id].连接id,3040,"1")
		end
	elseif 事件 == "400两储备银子卖给你" and 地图1 == 1501 and 名称 == "海产收购商" then
		local zhs = 0
		for n=1,#UserData[id].召唤兽.数据 do
			if UserData[id].召唤兽.数据[n] ~= nil and n ~= UserData[id].召唤兽.数据.参战  and UserData[id].召唤兽.数据[n].造型 == "巨蛙" then
				zhs = zhs + 1
				table.remove(UserData[id].召唤兽.数据,n)
			end
		end
		if zhs == 0 then
		    SendMessage(UserData[id].连接id,20,{"捕鱼人","海产收购商","你身上没有我要的召唤兽"})
		else
			local cb = (400 * zhs)
			UserData[id].角色.道具.货币.储备 = UserData[id].角色.道具.货币.储备 + cb
			wb = "你成功出售了#Y/"..zhs.."只#W/巨蛙给我共赚取了#R"..cb.."两银子"
			self.发送文本 ={"捕鱼人","海产收购商",wb,xx}
		    SendMessage(UserData[id].连接id,20,table.tostring(self.发送文本))
			SendMessage(UserData[id].连接id,3040,"1")
		end
	elseif 事件 == "500两银子卖给你" and 地图1 == 1501 and 名称 == "海产收购商" then
		local zhs = 0
		for n=1,#UserData[id].召唤兽.数据 do
			if UserData[id].召唤兽.数据[n] ~= nil and n ~= UserData[id].召唤兽.数据.参战  and UserData[id].召唤兽.数据[n].造型 == "海毛虫" then
				zhs = zhs + 1
				table.remove(UserData[id].召唤兽.数据,n)
			end
		end
		if zhs == 0 then
			wb = "你身上没有我要的召唤兽"
			self.发送文本 ={"捕鱼人","海产收购商",wb}
		    SendMessage(UserData[id].连接id,20,table.tostring(self.发送文本))
		else
			local cb = (500 * zhs)
			  RoleControl:添加银子(UserData[id],cb,"捕鱼人")
			wb = "你成功出售了#Y/"..zhs.."只#W/海毛虫给我共赚取了#R"..cb.."两银子"
			self.发送文本 ={"捕鱼人","海产收购商",wb,xx}
		    SendMessage(UserData[id].连接id,20,table.tostring(self.发送文本))
			SendMessage(UserData[id].连接id,3040,"1")
		end
	elseif 事件 == "600两储备银子卖给你" and 地图1 == 1501 and 名称 == "海产收购商" then
		local zhs = 0
		for n=1,#UserData[id].召唤兽.数据 do
			if UserData[id].召唤兽.数据[n] ~= nil and n ~= UserData[id].召唤兽.数据.参战  and UserData[id].召唤兽.数据[n].造型 == "海毛虫" then
				zhs = zhs + 1
				remove(UserData[id].召唤兽.数据,n)
			end
		end
		if zhs == 0 then
			wb = "你身上没有我要的召唤兽"
			self.发送文本 ={"捕鱼人","海产收购商",wb}
		    SendMessage(UserData[id].连接id,20,table.tostring(self.发送文本))
		else
			local cb = (600 * zhs)
			UserData[id].角色.道具.货币.储备 = UserData[id].角色.道具.货币.储备 + cb
			wb = "你成功出售了#Y/"..zhs.."只#W/大海龟给我共赚取了#R"..cb.."两银子"
			self.发送文本 ={"捕鱼人","海产收购商",wb,xx}
		    SendMessage(UserData[id].连接id,20,table.tostring(self.发送文本))
			SendMessage(UserData[id].连接id,3040,"1")
		end

	elseif 事件 == "一级镖银(要求等级30、可得250000两银子和5000000储备)" and 地图1 == 1024 and 名称 == "郑镖头" then
         TaskControl:设置押镖任务(id,1)
	elseif 事件 == "二级镖银(要求等级50、可得350000两银子和7500000储备)" and 地图1 == 1024 and 名称 == "郑镖头" then
           TaskControl:设置押镖任务(id,2)
	elseif 事件 == "三级镖银(要求等级70、可得750000两银子和15000000储备)" and 地图1 == 1024 and 名称 == "郑镖头" then
        TaskControl:设置押镖任务(id,3)
	elseif 事件 == "四级镖银(要求等级90、可得1100000两银子和22500000储备)" and 地图1 == 1024 and 名称 == "郑镖头" then
           TaskControl:设置押镖任务(id,4)
	elseif 事件 == "五级镖银(要求等级110、可得1500000两银子和30000000储备)" and 地图1 == 1024 and 名称 == "郑镖头" then
          TaskControl:设置押镖任务(id,5)
	elseif 事件 == "取消运镖任务"and 地图1 == 1024 and 名称 == "郑镖头" then
          if RoleControl:GetTaskID(UserData[id],"押镖")==0 then
			SendMessage(UserData[id].连接id,20,{"郑镖头","郑镖头","#Y/你好像没有在我这里领取过押镖任务"})
			else
			for n = 1, 80 do
				if UserData[id].角色.道具.包裹[n] ~= nil and UserData[id].物品[UserData[id].角色.道具.包裹[n]] ~= nil then
					if 任务数据[RoleControl:GetTaskID(UserData[id],"押镖")].道具 == UserData[id].物品[UserData[id].角色.道具.包裹[n]].名称 then
						UserData[id].物品[UserData[id].角色.道具.包裹[n]] = nil
						UserData[id].角色.道具.包裹[n] = nil
						break
					end
				end
			end
      UserData[id].押镖记录= nil
			RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"押镖"),id)
			SendMessage(UserData[id].连接id,20,{"郑镖头","郑镖头","#R/您的任务已经取消了"})
			end

	elseif 事件 == "我要更改造型" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		wb = " 请选择你要更改的造型，更改一次造型需要100万银子"
		local xx = {"飞燕女","英女侠","巫蛮儿","逍遥生","剑侠客","狐美人","骨精灵","杀破狼","巨魔王","虎头怪","舞天姬","玄彩娥","羽灵神","神天兵","龙太子","偃无师","鬼潇潇","桃夭夭"}
           self.发送文本 ={"张老财","贸易车队总管",wb,xx}
         SendMessage(UserData[id].连接id,20,table.tostring(self.发送文本))
	elseif 事件 == "飞燕女" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"飞燕女")
	elseif 事件 == "英女侠" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"英女侠")
	elseif 事件 == "巫蛮儿" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"巫蛮儿")
	elseif 事件 == "逍遥生" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"逍遥生")
	elseif 事件 == "剑侠客" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"剑侠客")
	elseif 事件 == "狐美人" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"狐美人")
	elseif 事件 == "骨精灵" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"骨精灵")
	elseif 事件 == "杀破狼" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"杀破狼")
	elseif 事件 == "巨魔王" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"巨魔王")
	elseif 事件 == "虎头怪" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"虎头怪")
	elseif 事件 == "舞天姬" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"舞天姬")
	elseif 事件 == "玄彩娥" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"玄彩娥")
	elseif 事件 == "羽灵神" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"羽灵神")
	elseif 事件 == "神天兵" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"神天兵")
	elseif 事件 == "龙太子" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"龙太子")
	elseif 事件 == "桃夭夭" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"桃夭夭")
	elseif 事件 == "偃无师" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"偃无师")
	elseif 事件 == "鬼潇潇" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		 RoleControl:改变造型(UserData[id],"鬼潇潇")
    elseif 事件 == "我要更改角色的名字" and 地图1 == 1537 and 名称 == "贸易车队总管" then
		   SendMessage(UserData[id].连接id, 15, {"改名","请输入你需要更改的名字，需要消耗400仙玉"})
	elseif 事件 == "我有物品需要典当" and 地图1 == 1523 and 名称 == "当铺老板" then

      SendMessage(UserData[id].连接id,3075,"出售界面")
    elseif 事件 == "出售" and 地图1 == 1501 and 名称 == "装备收购商" then
        ItemControl:索要出售道具(id,"包裹")
        -- wb = "你的东西太贵重了，我只是想收点纸壳子卖废品。"
        -- self.发送文本 ={"装备收购商","装备收购商",wb}
        -- SendMessage(UserData[id].连接id,20,table.tostring(self.发送文本))
	elseif 事件 == "购买"  and 名称 == "杂货店老板" then
		ShopControl:GetNpcShop(id,12)
	elseif 事件 == "购买"  and 名称 == "武器店掌柜" then
		 ShopControl:GetNpcShop(id,1)
	elseif 事件 == "购买" and 地图1 == 1020 and 名称 == "武器店老板" then
		ShopControl:GetNpcShop(id,3)
	elseif 事件 == "购买" and 地图1 == 1085 and 名称 == "武器店老板" then
		ShopControl:GetNpcShop(id,5)
	elseif 事件 == "查看熟练度"  and 名称 == "缝纫台" then
		 SendMessage(UserData[id].连接id,20,{"","缝纫台","你当前的熟练度:"..UserData[id].角色.裁缝熟练度})
	elseif 事件 == "查看熟练度"  and 名称 == "打铁炉" then
		 SendMessage(UserData[id].连接id,20,{"","打铁炉","你当前的熟练度:"..UserData[id].角色.打造熟练度})
	elseif 事件 == "打造" and(  名称 == "打铁炉" or 名称 == "缝纫台" )then
		ItemControl:索要打造道具(id, "包裹", "打造")
	elseif 事件 == "合成"  and (  名称 == "打铁炉" or 名称 == "缝纫台" ) then
		ItemControl:索要打造道具(id, "包裹", "合成")
	elseif 事件 == "修理"  and 名称 == "打铁炉" then
		ItemControl:索要打造道具(id, "包裹", "修理")
	elseif 事件 == "分解"  and 名称 == "打铁炉" then
		SendMessage(UserData[id].连接id,2021, ItemControl:索要道具2(id, "包裹"))
	elseif 事件 == "熔炼"  and (  名称 == "打铁炉" or 名称 == "缝纫台" ) then
		ItemControl:索要打造道具(id, "包裹", "熔炼")
	elseif 事件 == "购买"  and 名称 == "武器店老板" then
		ShopControl:GetNpcShop(id,2)
	elseif 事件 == "购买" and 地图1 == 1503 and 名称 == "服装店老板" then
		 ShopControl:GetNpcShop(id,9)
	elseif 事件 == "购买" and 地图1 == 1504 and 名称 == "药店老板" then
		ShopControl:GetNpcShop(id,14)
	elseif 事件 == "请帮我治疗" and 地图1 == 1506 and 名称 == "云游神医" then
		self.关闭事件 = true
		if UserData[id].角色.等级 <= 10 then
				UserData[id].角色.当前气血 = UserData[id].角色.最大气血
				UserData[id].角色.当前魔法 = UserData[id].角色.魔法上限
			  SendMessage(UserData[id].连接id,20,{"老太爷","云游神医","你已经恢复健康了"})
		else

			SendMessage(UserData[id].连接id,20,{"老太爷","云游神医","少侠你已经不需要我的帮助了"})
		end
	elseif 事件 == "是的，我想进去探个究竟" and 地图1 == 1506 and 名称 == "捕鱼人" then
		if UserData[id].角色.等级 >= 5 then
           MapControl:跳转处理(id,{1126,12*20,77*20,true})
		else
         SendMessage(UserData[id].连接id,20,{"捕鱼人","捕鱼人","你的修为也太低了吧，里面的怪物可是很可怕的。"})
		end
	elseif 事件 == "是的我要去" and 地图1 == 1506 and 名称 == "老虾" then
       MapControl:跳转处理(id,{1116,17*20,107*20,true})
	elseif 事件 == "是的" and 地图1 == 1507 and 名称 == "螃蟹精" then
       MapControl:跳转处理(id,{1501,265*20,113*20,true})
	elseif 事件 == "是的" and 地图1 == 1508 and 名称 == "虾精" then
        MapControl:跳转处理(id,{1501,265*20,113*20,true})
	elseif 事件 == "传送江南野外" and 地图1 == 1501 and 名称 == "建邺守卫" then
		if UserData[id].角色.等级 >= 10 then
			MapControl:跳转处理(id,{1193,149*20,65*20})
		else
		    SendMessage(UserData[id].连接id,20,{"衙役","建邺守卫","等级不足，不敢为你传送"})
		end
   ----江南野外
   	elseif 事件 == "切入战斗" and 地图1 == 1193 and 名称 == "江湖奸商" then
   		FightGet:进入处理(id, 100026, "66", RoleControl:GetTaskID(UserData[id],"嘉年华"))

    elseif 事件 == "我来用20个西瓜兑换" and 地图1 == 1193 and 名称 == "王婆" then
   
      local 找到物品 = false
      local 特赦格子 = 0
      for n = 1, 80 do
      if UserData[id].角色.道具["包裹"][n] ~= nil and 找到物品 == false and UserData[id].物品[UserData[id].角色.道具["包裹"][n]].名称 == "梦幻西瓜" then
      找到物品 = true
      特赦格子=n
          break
      end
      end
      if 找到物品 == false then
          SendMessage(UserData[id].连接id, 20, {"王大嫂","王婆","你身上没有梦幻西瓜呀"})
      elseif UserData[id].物品[UserData[id].角色.道具["包裹"][特赦格子]].数量 < 20 then  
           SendMessage(UserData[id].连接id, 20, {"王大嫂","王婆","你身上梦幻西瓜数量不够"})
      else
        ItemControl:RemoveItems(UserData[id],特赦格子,"梦幻西瓜",20)
           RoleControl:添加仙玉(UserData[id],500,"梦幻西瓜")
      end 

	-- 长安

 	elseif (事件=="雷击" or 事件=="落岩" or 事件=="水攻"or 事件=="烈火"or 事件=="奔雷咒"or 事件=="泰山压顶"or 事件=="水漫金山"or 事件=="地狱烈火"or 事件=="上古灵符" or 事件=="天降灵葫" or 事件=="苍鸾怒击" or 事件=="八凶法阵" or 事件=="月光" or 事件=="从天而降"or 事件=="大快朵颐"or 事件=="龙魂"or 事件=="浮云神马"or 事件=="哼哼哈兮"or 事件=="津津有味"or 事件=="叱咤风云"or 事件=="出其不意"or 事件=="理直气壮"or 事件=="嗜血追击"or 事件=="流沙轻音"or 事件=="食指大动" or 事件=="昼伏夜出" or 事件=="溜之大吉" or 事件=="神出鬼没")  and  (地图1 == 1001 or 地图1 == 1514 ) and 名称 == "法宠认证人" then
		 	 if UserData[id].召唤兽.数据.参战~=0 then
		           if  UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战] then
       	           	   if UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].认证 then
			       	   	SendMessage(UserData[id].连接id, 20,{"道士","法宠认证人","这只召唤兽已经认证过了"})
			       	   else
				           	local v =0
				           	    for i=1,#UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].技能 do
				           	    	if UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].技能[i].名称 == 事件 then
				                         v =i
				           	    	end
				           	    end
				           	   if v ~= 0 then
				           	   	    local rzfy =999999999999999
				           	   	if UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].类型 == "神兽" then
				           	   		rzfy=80000000
				           	   	else
				           	      rzfy = UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].参战等级*UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].参战等级*160+UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].参战等级*4000
				           	 	end
									if 银子检查(id, rzfy) then
									 	UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].认证= true
										UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].技能[v].认证= true
									  local 临时技能={"再生","冥思","慧根"}
		                                临时技能=临时技能[math.random(1,#临时技能)]


								       UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].技能[#UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].技能+1]=置技能(临时技能)
								      SendMessage(UserData[id].连接id,7,"#y/你的这只召唤兽学会了新技能#r/"..临时技能)
									RoleControl:扣除银子(UserData[id], rzfy, "召唤兽认证")
									RoleControl:添加消费日志(UserData[id],"召唤兽认证")
									else
									SendMessage(UserData[id].连接id, 7, "#y/你没那么多的银子")
									end
				           	   else
				           	    	SendMessage(UserData[id].连接id, 20,{"道士","法宠认证人","召唤兽数据异常请重新认证"})
				           	   end
				        end
		           else
		           	SendMessage(UserData[id].连接id, 20,{"道士","法宠认证人","参战的召唤兽好像不存在吧,请重新设置参战"})
		           end
		      else
		       	SendMessage(UserData[id].连接id, 20,{"道士","法宠认证人","请将认证的召唤兽设置成参战状态"})
		      end

	elseif 事件 == "取消除暴安良任务" and 地图1 == 1001 and 名称 == "王捕头" then
			if RoleControl:GetTaskID(UserData[id],"除暴安良")==0 then
			SendMessage(UserData[id].连接id,20,{"衙役","王捕头","#Y/你好像没有在我这里领取过除暴安良任务"})
			else
			MapControl:移除单位(任务数据[RoleControl:GetTaskID(UserData[id],"除暴安良")].地图编号,RoleControl:GetTaskID(UserData[id],"除暴安良"))
			RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"除暴安良"),id)
			SendMessage(UserData[id].连接id,20,{"衙役","王捕头","#R/您的任务已经取消了"})
			end
	elseif 事件 == "领取除暴安良任务" and 地图1 == 1001 and 名称 == "王捕头" then
		if RoleControl:GetTaskID(UserData[id],"除暴安良") ~= 0 then
			SendMessage(UserData[id].连接id, 7, "#y/你已经在我这里领取了除暴安良任务")
		elseif UserData[id].角色.等级 > 70 then
			SendMessage(UserData[id].连接id, 7, "#y/只有等级小于70级的玩家才可领取此任务")
		elseif 活动数据.除暴安良[id] ~= nil and 活动数据.除暴安良[id] <= 0 then
			SendMessage(UserData[id].连接id, 7, "#y/你今天的除暴安良次数已经耗尽")
		else
			if 活动数据.除暴安良[id] == nil then
				活动数据.除暴安良[id] = 100
			end

			TaskControl:设置除暴安良任务(id)
		end

	elseif 事件 == "领取平定安邦任务" and  地图1 == 1001 and 名称 == "皇宫护卫" then
		if RoleControl:GetTaskID(UserData[id],"平定安邦")==0 then
				local 临时id6=tonumber(UserData[id].id.."1021")
				任务数据[临时id6]={
				类型="平定安邦"
				,id=id
				,起始=os.time()
				,等级=UserData[id].角色.等级
				,任务id=临时id6
 				,结束=7200
 				,上次 = 7200
				,当前 = 7200
				}
				UserData[id].角色.任务数据[RoleControl:生成任务id(UserData[id])]=临时id6
				SendMessage(UserData[id].连接id,7,"#Y/你领取了平定安邦任务，请点击任务列表查看")
				TaskControl:刷新追踪任务信息(id)
		else
			SendMessage(UserData[id].连接id,20,{"御林军","皇宫护卫","#Y/你已经领取过了这个任务,不需要重复领取"})
		end
	elseif 事件 == "我来取消任务" and  地图1 == 1001 and 名称 == "皇宫护卫" then
		if RoleControl:GetTaskID(UserData[id],"平定安邦")==0 then
		SendMessage(UserData[id].连接id,20,{"御林军","皇宫护卫","#Y/请先领取平定安邦任务"})
		else
		SendMessage(UserData[id].连接id,20,{"御林军","皇宫护卫","#Y/你取消了平定安邦任务"})
		RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"平定安邦"))
		任务数据[RoleControl:GetTaskID(UserData[id],"平定安邦")]=nil
		end
	elseif 事件 == "上交心魔宝珠" and  地图1 == 1001 and 名称 == "皇宫护卫" then
       SendMessage(UserData[id].连接id,3011,{id=-21,名称="皇宫护卫",道具=ItemControl:索要道具2(id,"包裹")})
    elseif 事件 == "我需要购买" and  地图1 == 1001 and 名称 == "护卫总管" then
    	   if #UserData[id].召唤兽.数据 >= UserData[id].召唤兽.数据.召唤兽上限 then
    		SendMessage(UserData[id].连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法购买")
   			 elseif 银子检查(id, 3000) then
				RoleControl:扣除银子(UserData[id], 3000, "购买护卫")
				UserData[id].召唤兽:创建召唤兽("护卫","宝宝")
				SendMessage(UserData[id].连接id, 7, "#y/你获得了一只护卫")
			else
				SendMessage(UserData[id].连接id, 7, "#y/你没那么多的银子")
			end
    elseif 事件 == "领取一小时双倍经验" and  地图1 == 1001 and 名称 == "马副将" then
          	TaskControl:领取双倍经验(id,1)
	elseif 事件 == "领取二小时双倍经验" and  地图1 == 1001 and 名称 == "马副将" then
			TaskControl:领取双倍经验(id,2)
	elseif 事件 == "领取三小时双倍经验" and  地图1 == 1001 and 名称 == "马副将" then
			TaskControl:领取双倍经验(id,3)
	elseif 事件 == "查看剩余的双倍时间" and  地图1 == 1001 and 名称 == "马副将" then
        	if  活动数据.双倍[id]==nil then
           	活动数据.双倍[id] = 3600*3
    		end
			SendMessage(UserData[id].连接id, 20,{"御林军","马副将","你今日可用的双倍时间#R/"..math.floor(活动数据.双倍[id]/3600).."#W/小时"})
	elseif 事件 == "查看队伍双倍时间" and  地图1 == 1001 and 名称 == "马副将" then
		local 队伍双倍 =""

		 if UserData[id].队伍 ~= 0 then
            for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
            	local 队伍时间 =0
            	if RoleControl:GetTaskID(UserData[队伍数据[UserData[id].队伍].队员数据[n]],"双倍") ~= 0  then
            	   队伍时间 =  math.floor((任务数据[RoleControl:GetTaskID(UserData[队伍数据[UserData[id].队伍].队员数据[n]],"双倍")].结束 - (os.time() - 任务数据[RoleControl:GetTaskID(UserData[队伍数据[UserData[id].队伍].队员数据[n]],"双倍")].起始)) / 60)
            	end
                队伍双倍=队伍双倍..UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称.."  剩余双倍时间#R/"..队伍时间.."#W/分钟。\n"

            end
            SendMessage(UserData[id].连接id, 20,{"御林军","马副将",队伍双倍})
         else
         	SendMessage(UserData[id].连接id, 20,{"御林军","马副将","你还没有队伍,无法查看队员的双倍时间"})
        end

 	elseif 事件 == "是的我要认证" and  (地图1 == 1001 or 地图1 == 1514) and 名称 == "法宠认证人" then
      if UserData[id].召唤兽.数据.参战~=0 then
           if  UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战] then
           	   if UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].认证 then
           	   	SendMessage(UserData[id].连接id, 20,{"道士","法宠认证人","这只召唤兽已经认证过了"})
           	   else
	           		local v = {}
	           	    for i=1,#UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].技能 do
	           	    	if 取认证技能(UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].技能[i].名称) then
	                         v[#v+1] = UserData[id].召唤兽.数据[UserData[id].召唤兽.数据.参战].技能[i].名称
	           	    	end
	           	    end
	           	   if #v>0 then
	                   SendMessage(UserData[id].连接id, 20,{"道士","法宠认证人","请你选择需要认证的技能",v})
	           	   else
	           	    	SendMessage(UserData[id].连接id, 20,{"道士","法宠认证人","你这只召唤兽没有法术技能无法认证!"})
	           	   end
	           	end
           else
           	SendMessage(UserData[id].连接id, 20,{"道士","法宠认证人","参战的召唤兽好像不存在吧,请重新设置参战"})
           end
      else
       	SendMessage(UserData[id].连接id, 20,{"道士","法宠认证人","请将认证的召唤兽设置成参战状态"})
      end

	elseif 事件 == "查看人物修炼信息" and 地图1 == 1001 and 名称 == "修炼大师" then
             RoleControl:取人物修炼(UserData[id])
	elseif 事件 == "攻击修炼" and 地图1 == 1001 and 名称 == "修炼大师" then
      		UserData[id].角色.人物修炼.当前 = "攻击"
		SendMessage(UserData[id].连接id, 20,{"兰虎","修炼大师","您当前的修炼类型已经更换为#G/" .. UserData[id].角色.人物修炼.当前 .. "#W/修炼"})
	elseif 事件 == "法术修炼" and 地图1 == 1001 and 名称 == "修炼大师" then
		UserData[id].角色.人物修炼.当前 = "法术"
		SendMessage(UserData[id].连接id, 20,{"兰虎","修炼大师","您当前的修炼类型已经更换为#G/" .. UserData[id].角色.人物修炼.当前 .. "#W/修炼"})
	elseif 事件 == "防御修炼" and 地图1 == 1001 and 名称 == "修炼大师" then
				UserData[id].角色.人物修炼.当前 = "防御"
		SendMessage(UserData[id].连接id, 20,{"兰虎","修炼大师","您当前的修炼类型已经更换为#G/" .. UserData[id].角色.人物修炼.当前 .. "#W/修炼"})
	elseif 事件 == "法抗修炼" and 地图1 == 1001 and 名称 == "修炼大师" then
		UserData[id].角色.人物修炼.当前 = "法抗"
		SendMessage(UserData[id].连接id, 20,{"兰虎","修炼大师","您当前的修炼类型已经更换为#G/" .. UserData[id].角色.人物修炼.当前 .. "#W/修炼"})
	elseif 事件 == "设置人物修炼类型" and 地图1 == 1001 and 名称 == "修炼大师" then
		local xx={"攻击修炼","法术修炼","防御修炼","法抗修炼"}
		SendMessage(UserData[id].连接id, 20,{"兰虎","修炼大师","你可以在我这里查看以及设置人物修炼。你还可以在我这里提升人物修炼经验，但是需要消耗一定数量的银子与人物经验。您当前设置的人物修炼类型为：#G/" .. UserData[id].角色.人物修炼.当前 .. "修炼",xx})
	elseif 事件 == "我要参加" and 地图1 == 1001 and 名称 == "礼部侍郎" then
		if UserData[id].角色.等级 < 40 then
			SendMessage(UserData[id].连接id, 7, "#y/等级达到40级才可参加答题")

			return 0
		elseif 活动数据.科举名单[UserData[id].id] ~= nil and 活动数据.科举名单[UserData[id].id].次数 >= 2 then
			SendMessage(UserData[id].连接id, 7, "#y/你已经参加过本日的答题了，请明日再来")
		elseif RoleControl:GetTaskID(UserData[id],"答题") ~= 0 then
			if 任务数据[RoleControl:GetTaskID(UserData[id],"答题")].已答 >= 20 then
				RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"答题"))
				SendMessage(UserData[id].连接id, 7, "#y/您的任务已经被强制取消，无法获得额外的奖励")
			else
				TaskControl:回答科举题目(id, 0)
			end
		else
			TaskControl:设置科举任务(id)
		end

	-- elseif 内容 == "55-1-1" then
	-- 	TaskControl:回答科举题目(id, 1)
	-- elseif 内容 == "55-1-2" then
	-- 	TaskControl:回答科举题目(id, 2)
	-- elseif 内容 == "55-1-3" then
	-- 	TaskControl:回答科举题目(id, 3)
	-- elseif 内容 == "55-1-4" then
	-- 	self:索取npc对话(id, UserData[id].npc, 1)
	-- elseif 内容 == "55-2-1" then
	-- 	TaskControl:回答科举题目(id, 1, 1)
	-- elseif 内容 == "55-2-2" then
	-- 	TaskControl:回答科举题目(id, 1, 2)
	-- elseif 内容 == "55-2-3" then
	-- 	TaskControl:回答科举题目(id, 1, 3)
    elseif 事件 == "我要测试(切入战斗)" and 地图1 == 1001 and 名称 == "测试员" then
   			FightGet:进入处理(id,888, "66", 0)
	-- elseif 事件 == "确认购买(消耗2000W银子)" and 地图1 == 1001 and 名称 == "商会总管" then
	-- 	商会处理类:购买商铺(id)
	-- elseif 事件 == "我来买些东西" and 地图1 == 1001 and 名称 == "商会总管" then
 --         商会处理类:获取商会商铺信息(id)
	-- elseif 事件 == "我想申请开个店" and 地图1 == 1001 and 名称 == "商会总管" then
 --           SendMessage(UserData[id].连接id,20,{"商会总管","商会总管"," 你可以在我这里花费2000W银子购买一间商铺。每增加1个店面需要花费200点仙玉。请问您是否要花费1000点仙玉银子购买商铺？ ",{"确认购买(消耗2000W银子)","我再想想"}})
	-- elseif 事件 == "我要进入我的商店" and 地图1 == 1001 and 名称 == "商会总管" then
	-- 		商会处理类:发送管理商铺信息(id)

 elseif 事件 == "我要参加PK活动" and 地图1 == 1001 and 名称 == "华山论剑" then	
	  if 华山论剑开关 then
	     if 天梯匹配[id]==nil then
          天梯匹配[id]={id=id,名称=UserData[id].角色.名称,华山论剑积分=0,华山论剑累计积分=0,门派=UserData[id].角色.门派,等级=UserData[id].角色.等级,匹配=0,连胜次数=0,临时次数=0}
        end 
	  
    		if UserData[id].队伍 == 0 then
    			SendMessage(UserData[id].连接id, 7, "#y/此活动最少需要三人组队完成")
				return
    		elseif #队伍数据[UserData[id].队伍].队员数据 < 3 and DebugMode== false then
    			SendMessage(UserData[id].连接id, 7, "#y/此活动最少需要三人组队完成")
				return
    		elseif 取队伍符合等级(id, 69) == false then
    			广播队伍消息(id, 7, "#y/角色等级达到69级才可参加活动")
				return
    		else
    			SendMessage(UserData[id].连接id,6014,"华山论剑")
				
    			
         end
       else
         SendMessage(UserData[id].连接id, 7, "#y/华山论剑活动还还未开启，请关注游戏通知")
		end
		
	elseif 事件 == "查看PK次数" and 地图1 == 1001 and 名称 == "华山论剑" then	

	     local 临时月 =os.date("%Y-%m-%d-%H:%M:%S",os.time())
	
       if 天梯匹配[id]==nil then
          天梯匹配[id]={id=id,名称=UserData[id].角色.名称,华山论剑积分=0,华山论剑累计积分=0,门派=UserData[id].角色.门派,等级=UserData[id].角色.等级,匹配=0,连胜次数=0,临时次数=0}
        end 	
	SendMessage(UserData[id].连接id,20,{"御林军","李将军","玩家 #Y/"..UserData[id].角色.名称.."(ID"..id..") #W/您当前已PK #Y/" .. 天梯匹配[id].临时次数 .. "#W/ 次，还可以PK #Y/"..(50-天梯匹配[id].临时次数).." #W/次，您当前华山积分为 #Y/"..天梯匹配[id].华山论剑累计积分.." #W/分,参与次数>=30次的玩家，请在活动结束后凭此截图找GM领取参与奖,当前系统时间为 #R/"..临时月 })	
	elseif 事件 == "查看排行榜" and 地图1 == 1001 and 名称 == "华山论剑" then	
	
	   if 华山论剑开关 then 
	      SendMessage(UserData[id].连接id, 7, "#y/请在活动结束后查看")
	   return
	   else 
	   GameActivities:刷新华山论剑排行(id)
	   end

  elseif 事件 == "确认扩张"and 名称 == "商会总管" then
    商会处理类:扩张柜台(id)
  elseif 事件 == "我要扩张"and 名称 == "商会总管" then
    商会处理类:扩张唤兽柜台(id)
  elseif 事件 == "购买物品店铺" and 地图1 == 1001 and 名称 == "商会总管" then
    商会处理类:购买物品商铺(id)
    elseif 事件 == "购买唤兽店铺" and 地图1 == 1001 and 名称 == "商会总管" then
    商会处理类:购买唤兽商铺(id)
    elseif 事件 == "确认购买(消耗2000W银子)" and 地图1 == 1001 and 名称 == "商会总管" then
        商会处理类:购买商铺(id)
    elseif 事件 == "我来买些东西" and 地图1 == 1001 and 名称 == "商会总管" then
         商会处理类:获取商会商铺信息(id)
    elseif 事件 == "我想申请开个店" and 地图1 == 1001 and 名称 == "商会总管" then
    SendMessage(UserData[id].连接id,20,{"商会总管","商会总管"," 你可以在我这里花费1000万两银子购买一间商铺，进行售卖东西。",{"购买物品店铺","购买唤兽店铺","我再想想"}})
    elseif 事件 == "我要进入我的商店" and 地图1 == 1001 and 名称 == "商会总管" then
      SendMessage(UserData[id].连接id,20,{"商会总管","商会总管"," 你要管理那一间店铺？",{"物品店铺","唤兽店铺","取消"}})
  elseif 事件 == "物品店铺" and 地图1 == 1001 and 名称 == "商会总管" then
      商会处理类:发送管理物品商铺(id)
  elseif 事件 == "唤兽店铺" and 地图1 == 1001 and 名称 == "商会总管" then
      商会处理类:发送管理唤兽商铺(id)




	elseif 事件 == "送我进去" and 地图1 == 1001 and 名称 == "兰虎" then
		if UserData[id].角色.等级 < 60 then
			SendMessage(UserData[id].连接id, 7, "#y/等级达到60级的玩家才可以参加此活动")

			return 0
		elseif 比武大会进场开关 == false then
			SendMessage(UserData[id].连接id, 7, "#y/进场时间为每日19.50-20点")

			return 0
		else
			if UserData[id].角色.等级 < 70 then
				self.传送编号 = 6001
			elseif UserData[id].角色.等级 < 110 then
				self.传送编号 = 6002
			else
				self.传送编号 = 6003
			end

			MapControl:Jump(id, self.传送编号, 61, 24)
		end
	elseif 事件 == "领取奖励" and 地图1 == 1001 and 名称 == "兰虎" then
			ItemControl:比武奖励(id)
	elseif 事件 == "兑换物品" and 地图1 == 1001 and 名称 == "兰虎" then

    SendMessage(UserData[id].连接id,20,{"兰虎","兰虎","现在可以在比武积分商城处购买"})
	elseif 事件 == "我要改名" and 地图1 == 1001 and 名称 == "户部侍郎" then
			SendMessage(UserData[id].连接id, 15, {"改名","请输入你需要更改的名字，需要消耗400仙玉"})
	elseif 事件 == "协助抓捕盗贼" and 地图1 == 1001 and 名称 == "御林军左统领" then
    if 飞贼开关 then
    			if UserData[id].队伍 == 0 then
    			SendMessage(UserData[id].连接id, 7, "#y/此任务最少需要三人组队完成")
    		elseif #队伍数据[UserData[id].队伍].队员数据 < 3 and DebugMode== false then
    			SendMessage(UserData[id].连接id, 7, "#y/此任务最少需要三人组队完成")
    		elseif 取队伍符合等级(id, 60) == false then
    			广播队伍消息(id, 7, "#y/角色等级达到60级才可领取此任务")
    		else
    			for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
    				if RoleControl:GetTaskID(UserData[队伍数据[UserData[id].队伍].队员数据[n]],"飞贼") ~= 0 then
    					广播队伍消息(id, 7, "#y/" .. UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称 .. "已经领取过此任务了")

    					return 0
    				end
    			end
    			TaskControl:设置皇宫飞贼任务(id)
         end
    else
       SendMessage(UserData[id].连接id, 7, "#y/皇宫飞贼活动还还未开启，请关注游戏通知")
		end
	elseif 事件 == "取消任务" and 地图1 == 1001 and 名称 == "御林军左统领" then
        if RoleControl:GetTaskID(UserData[id],"飞贼") == 0 then
			SendMessage(UserData[id].连接id, 7, "#y/你尚未领取此任务")

			return 0
		else

			RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"飞贼"))
      UserData[id].角色.飞贼次数 =0
			SendMessage(UserData[id].连接id, 7, "#y/你取消了飞贼任务")
		end
	elseif 事件 == "加入帮派" and 地图1 == 1001 and 名称 == "帮派总管" then
			self.发送信息 = {}
		for n, v in pairs(帮派数据) do
			if 帮派数据[n] ~= nil and n ~= "编号" then
				self.发送信息[#self.发送信息 + 1] = {
					编号 = n,
					名称 = 帮派数据[n].名称,
					帮主 = 帮派数据[n].现任帮主.名称,
					宗旨 = 帮派数据[n].宗旨
				}
			end
		end
		SendMessage(UserData[id].连接id, 20019, self.发送信息)
	elseif 事件 == "送我回帮" and 地图1 == 1001 and 名称 == "帮派总管" then
		if UserData[id].角色.帮派 == nil or 帮派数据[UserData[id].角色.帮派] == nil then
			SendMessage(UserData[id].连接id, 7, "#y/请先加入一个帮派")
			return 0
		elseif UserData[id].队伍 ~= 0 then
			SendMessage(UserData[id].连接id, 7, "#y/请先脱离队伍")
			return 0
		else
			UserData[id].帮派进入方式 = 1
			UserData[id].帮派进入编号 = UserData[id].角色.帮派
			MapControl:Jump(id, 1875, 46, 35)
		end
	elseif 事件 == "消耗2000点仙玉创建帮派" and 地图1 == 1001 and 名称 == "帮派总管" then
       SendMessage(UserData[id].连接id, 15, {"帮派名称","请输入你需要更改的名字，需要消耗2000仙玉"})
	elseif 事件 == "创建帮派" and 地图1 == 1001 and 名称 == "帮派总管" then

      SendMessage(UserData[id].连接id,20,{"蒋大全","帮派总管"," 你可以在我这里花费2000点仙玉创建一个帮派。本服最多允许6个帮派同时存在，超过6个则无法创建帮派。当帮派安定值为0时，帮派规模会下降1级。若已是1级规模的帮派则会直接解散该帮派。请选择你要进行的操作：",{"消耗2000点仙玉创建帮派","我再想想"}})


	elseif 事件 == "领取官职任务" and 地图1 == 1001 and 名称 == "李将军" then
		TaskControl:设置官职任务(id)
	elseif 事件 == "领取俸禄" and 地图1 == 1001 and 名称 == "李将军" then
           local gz = ""
           	if  UserData[id].角色.官职贡献度 > 9599 then
		          gz = [[可领取俸禄为仙玉:#G/200#W/,可以领取称谓:"皇帝"]]
			elseif  UserData[id].角色.官职贡献度 > 4849 then
                  gz = [[可领取俸禄为仙玉:#G/150#W/,可以领取称谓:"辅佐王"]]
			elseif  UserData[id].角色.官职贡献度 > 2449 then
                  gz = [[可领取俸禄为仙玉:#G/100#W/,可以领取称谓:"郡王"]]
			elseif  UserData[id].角色.官职贡献度 > 1249 then
                  gz = [[可领取俸禄为仙玉:#G/50#W/,可以领取称谓:"一品膘骑大将军"]]
			elseif  UserData[id].角色.官职贡献度 > 599 then
                  gz = [[可领取俸禄为仙玉:#G/30#W/,可以领取称谓:"二品辅国大将军"]]
			elseif  UserData[id].角色.官职贡献度 > 299 then
                  gz = [[可领取俸禄为仙玉:#G/20#W/,可以领取称谓:"三品归德将军"]]
             else

             	gz ="#W/当前没有官职,无法领取官职俸禄"
		   end
		     SendMessage(UserData[id].连接id,20,{"御林军","李将军","你可以在我这里领取俸禄。俸禄两次领取的时间不能低于24小时，根据官职贡献度可换取对应的仙玉。且每次领取俸禄都将扣除当前10%的官职贡献度。你当前拥有#G/" .. UserData[id].角色.官职贡献度 .. "#W/点官职贡献度。" .. gz,{"确定领取俸禄","我再想想"}})

    elseif 事件 == "确定领取俸禄" and 地图1 == 1001 and 名称 == "李将军" then
    	if UserData[id].角色.官职贡献度 <= 0 then
			SendMessage(UserData[id].连接id, 7, "#y/你在我这里没有可领取的俸禄")
		elseif UserData[id].角色.俸禄时间 ~= nil and os.time() - UserData[id].角色.俸禄时间 < 86400 then
			SendMessage(UserData[id].连接id, 7, "#y/你下次领取俸禄的时间为" .. os.date("[%Y年%m月%d日%X]",UserData[id].角色.俸禄时间 + 86400) .. "")
		else
			UserData[id].角色.俸禄时间 = os.time()
			if  UserData[id].角色.官职贡献度 > 9599 then
		        RoleControl:添加仙玉(UserData[id],200,"官职工资")
		            if RoleControl:检查称谓(UserData[id],"辅佐王") then
		         	      RoleControl:回收称谓(UserData[id],"辅佐王")
		         	end
					if RoleControl:检查称谓(UserData[id],"郡王") then
                       RoleControl:回收称谓(UserData[id],"郡王")
					end
					if RoleControl:检查称谓(UserData[id],"一品膘骑大将军") then
                       RoleControl:回收称谓(UserData[id],"一品膘骑大将军")
					end
					if RoleControl:检查称谓(UserData[id],"二品辅国大将军") then
                       RoleControl:回收称谓(UserData[id],"二品辅国大将军")
					end
					if RoleControl:检查称谓(UserData[id],"三品归德将军") then
                       RoleControl:回收称谓(UserData[id],"三品归德将军")
					end
		            if RoleControl:检查称谓(UserData[id],"皇帝") ==false then
		         	    RoleControl:添加称谓(UserData[id],"皇帝")
		         	end
			elseif  UserData[id].角色.官职贡献度 > 4849 then
                  RoleControl:添加仙玉(UserData[id],150,"官职工资")
                   if RoleControl:检查称谓(UserData[id],"辅佐王") ==false  then
		         	      RoleControl:添加称谓(UserData[id],"辅佐王")
		         	end
					if RoleControl:检查称谓(UserData[id],"郡王") then
                       RoleControl:回收称谓(UserData[id],"郡王")
					end
					if RoleControl:检查称谓(UserData[id],"一品膘骑大将军") then
                       RoleControl:回收称谓(UserData[id],"一品膘骑大将军")
					end
					if RoleControl:检查称谓(UserData[id],"二品辅国大将军") then
                       RoleControl:回收称谓(UserData[id],"二品辅国大将军")
					end
					if RoleControl:检查称谓(UserData[id],"三品归德将军") then
                       RoleControl:回收称谓(UserData[id],"三品归德将军")
					end
		            if RoleControl:检查称谓(UserData[id],"皇帝") then
		         	    RoleControl:回收称谓(UserData[id],"皇帝")
		         	end
			elseif  UserData[id].角色.官职贡献度 > 2449 then
                 RoleControl:添加仙玉(UserData[id],100,"官职工资")
                   if RoleControl:检查称谓(UserData[id],"辅佐王") then
		         	      RoleControl:回收称谓(UserData[id],"辅佐王")
		         	end
					if RoleControl:检查称谓(UserData[id],"郡王")==false  then
                       RoleControl:添加称谓(UserData[id],"郡王")
					end
					if RoleControl:检查称谓(UserData[id],"一品膘骑大将军") then
                       RoleControl:回收称谓(UserData[id],"一品膘骑大将军")
					end
					if RoleControl:检查称谓(UserData[id],"二品辅国大将军") then
                       RoleControl:回收称谓(UserData[id],"二品辅国大将军")
					end
					if RoleControl:检查称谓(UserData[id],"三品归德将军") then
                       RoleControl:回收称谓(UserData[id],"三品归德将军")
					end
		            if RoleControl:检查称谓(UserData[id],"皇帝") then
		         	    RoleControl:回收称谓(UserData[id],"皇帝")
		         	end
			elseif  UserData[id].角色.官职贡献度 > 1249 then
                 RoleControl:添加仙玉(UserData[id],50,"官职工资")
                 if RoleControl:检查称谓(UserData[id],"辅佐王") then
		         	      RoleControl:回收称谓(UserData[id],"辅佐王")
		         	end
					if RoleControl:检查称谓(UserData[id],"郡王") then
                       RoleControl:回收称谓(UserData[id],"郡王")
					end
					if RoleControl:检查称谓(UserData[id],"一品膘骑大将军") ==false then
                       RoleControl:添加称谓(UserData[id],"一品膘骑大将军")
					end
					if RoleControl:检查称谓(UserData[id],"二品辅国大将军") then
                       RoleControl:回收称谓(UserData[id],"二品辅国大将军")
					end
					if RoleControl:检查称谓(UserData[id],"三品归德将军") then
                       RoleControl:回收称谓(UserData[id],"三品归德将军")
					end
		            if RoleControl:检查称谓(UserData[id],"皇帝") then
		         	    RoleControl:回收称谓(UserData[id],"皇帝")
		         	end
			elseif  UserData[id].角色.官职贡献度 > 599 then
                 RoleControl:添加仙玉(UserData[id],30,"官职工资")
                 if RoleControl:检查称谓(UserData[id],"辅佐王") then
		         	      RoleControl:回收称谓(UserData[id],"辅佐王")
		         	end
					if RoleControl:检查称谓(UserData[id],"郡王") then
                       RoleControl:回收称谓(UserData[id],"郡王")
					end
					if RoleControl:检查称谓(UserData[id],"一品膘骑大将军") then
                       RoleControl:回收称谓(UserData[id],"一品膘骑大将军")
					end
					if RoleControl:检查称谓(UserData[id],"二品辅国大将军")==false then
                       RoleControl:添加称谓(UserData[id],"二品辅国大将军")
					end
					if RoleControl:检查称谓(UserData[id],"三品归德将军") then
                       RoleControl:回收称谓(UserData[id],"三品归德将军")
					end
		            if RoleControl:检查称谓(UserData[id],"皇帝")  then
		         	    RoleControl:回收称谓(UserData[id],"皇帝")
		         	end
			elseif  UserData[id].角色.官职贡献度 > 299 then
                 RoleControl:添加仙玉(UserData[id],20,"官职工资")
                 if RoleControl:检查称谓(UserData[id],"辅佐王") then
		         	      RoleControl:回收称谓(UserData[id],"辅佐王")
		         	end
					if RoleControl:检查称谓(UserData[id],"郡王") then
                       RoleControl:回收称谓(UserData[id],"郡王")
					end
					if RoleControl:检查称谓(UserData[id],"一品膘骑大将军") then
                       RoleControl:回收称谓(UserData[id],"一品膘骑大将军")
					end
					if RoleControl:检查称谓(UserData[id],"二品辅国大将军") then
                       RoleControl:回收称谓(UserData[id],"二品辅国大将军")
					end
					if RoleControl:检查称谓(UserData[id],"三品归德将军") ==false then
                       RoleControl:添加称谓(UserData[id],"三品归德将军")
					end
		            if RoleControl:检查称谓(UserData[id],"皇帝")  then
		         	    RoleControl:回收称谓(UserData[id],"皇帝")
		         	end
            else
             	SendMessage(UserData[id].连接id, 7, "#y/当前没有官职,无法领取官职俸禄")
             	if RoleControl:检查称谓(UserData[id],"辅佐王") then
		         	      RoleControl:回收称谓(UserData[id],"辅佐王")
		         	end
					if RoleControl:检查称谓(UserData[id],"郡王") then
                       RoleControl:回收称谓(UserData[id],"郡王")
					end
					if RoleControl:检查称谓(UserData[id],"一品膘骑大将军") then
                       RoleControl:回收称谓(UserData[id],"一品膘骑大将军")
					end
					if RoleControl:检查称谓(UserData[id],"二品辅国大将军") then
                       RoleControl:回收称谓(UserData[id],"二品辅国大将军")
					end
					if RoleControl:检查称谓(UserData[id],"三品归德将军") then
                       RoleControl:回收称谓(UserData[id],"三品归德将军")
					end
		            if RoleControl:检查称谓(UserData[id],"皇帝")  then
		         	    RoleControl:回收称谓(UserData[id],"皇帝")
		         	end
             	    return
		    end
			UserData[id].角色.官职贡献度 = math.floor(UserData[id].角色.官职贡献度 * 0.95)
			SendMessage(UserData[id].连接id, 7, "#y/你领取了本日俸禄")
		end
	elseif 事件 == "取消官职任务" and 地图1 == 1001 and 名称 == "李将军" then
      SendMessage(UserData[id].连接id,20,{"御林军","李将军","在领取任务5分钟后，你可以在我这里取消任务。取消任务需要扣除2点官职贡献度。您确定要取消任务吗？",{"确定取消官职任务","我再想想"}})
	elseif 事件 == "确定取消官职任务" and 地图1 == 1001 and 名称 == "李将军" then
		if RoleControl:GetTaskID(UserData[id],"官职") == 0 then
			SendMessage(UserData[id].连接id, 7, "#y/你没有在我这里领取官职任务")
		elseif os.time() - 任务数据[RoleControl:GetTaskID(UserData[id],"官职")].起始 < 300 then
			SendMessage(UserData[id].连接id, 7, "#y/" .. math.floor((300 - (os.time() - 任务数据[RoleControl:GetTaskID(UserData[id],"官职")].起始)) / 60) .. "分钟后才可取消此任务")
		else
			if 任务数据[RoleControl:GetTaskID(UserData[id],"官职")].分类 ~= 3 then
				MapControl:移除单位(任务数据[RoleControl:GetTaskID(UserData[id],"官职")].地图编号, RoleControl:GetTaskID(UserData[id],"官职"))
			end

			RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"官职"))

			任务数据[RoleControl:GetTaskID(UserData[id],"官职")] = nil
			UserData[id].角色.官职贡献度 = UserData[id].角色.官职贡献度 - 2

			if UserData[id].角色.官职贡献度 <= 0 then
				UserData[id].角色.官职贡献度 = 0
			end

			UserData[id].角色.官职任务次数 = 1

			SendMessage(UserData[id].连接id, 7, "#y/您的任务已经取消了")
		end
	elseif 事件 == "找到了" and 地图1 == 1001 and 名称 == "李将军" then
		SendMessage(UserData[id].连接id, 3011, {名称="李将军",id = RoleControl:GetTaskID(UserData[id],"官职"),道具 = ItemControl:索要道具2(id, "包裹")})
    elseif 事件 == "给予强化石" and 地图1 == 1001 and 名称 == "冯铁匠" then
           SendMessage(UserData[id].连接id,3011,{名称="冯铁匠" ,id=RoleControl:GetTaskID(UserData[id],"打造"),道具=ItemControl:索要道具1(id,"包裹")})
elseif 事件 == "我要用25个神兜兜+随机神兽兑换" and 地图1 == 1001 and 名称 == "袁天罡"   then
        local xx ={}
        for i=1,#UserData[id].召唤兽.数据 do
           if UserData[id].召唤兽.数据[i].类型=="神兽" then
            table.insert(xx,i..'.'..UserData[id].召唤兽.数据[i].名称)
           end
        end
        table.insert(xx,"我随便看看")
      
        SendMessage(UserData[id].连接id,20,{"诗中仙","袁天罡","请选择你要替换的神兽",xx,"兑换神兽"})
  
elseif 事件 == "我要用99个神兜兜兑换神兽" and 地图1 == 1001 and 名称 == "袁天罡"   then
      local 找到物品 = false
      local 特赦格子 = 0
      for n = 1, 80 do
      if UserData[id].角色.道具["包裹"][n] ~= nil and 找到物品 == false and UserData[id].物品[UserData[id].角色.道具["包裹"][n]].名称 == "神兜兜" then
      找到物品 = true
      特赦格子=n
          break
      end
      end
      if 找到物品 == false then
          SendMessage(UserData[id].连接id, 20, {"诗中仙","袁天罡","你身上没有神兜兜呀"})
      elseif UserData[id].物品[UserData[id].角色.道具["包裹"][特赦格子]].数量 < 99 then  
           SendMessage(UserData[id].连接id, 20, {"诗中仙","袁天罡","你身上神兜兜数量不够"})
      elseif #UserData[id].召唤兽.数据 >= UserData[id].召唤兽.数据.召唤兽上限 then
                    SendMessage(UserData[id].连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")

      else
        ItemControl:RemoveItems(UserData[id],特赦格子,"神兜兜",99)
            local  随机神兽 ={"超级六耳猕猴-法术型","超级神鸡-法术型","超级玉兔-法术型","超级神猴-法术型","超级神龙-法术型","超级神羊-法术型","超级孔雀-法术型","超级灵狐-法术型","超级筋斗云-法术型","超级神鸡-物理型","超级玉兔-物理型","超级神猴-物理型","超级土地公公-物理型","超级神羊-物理型","超级六耳猕猴-物理型","超级神马-物理型","超级神马-法术型","超级孔雀-物理型","超级灵狐-物理型","超级筋斗云-物理型","超级神龙-物理型","超级麒麟-物理型","超级麒麟-法术型","超级大鹏-法术型","超级大鹏-物理型","超级神蛇-法术型","超级神蛇-物理型","超级赤焰兽-法术型","超级赤焰兽-物理型","超级白泽-法术型","超级白泽-物理型","超级灵鹿-法术型","超级灵鹿-物理型","超级大象-法术型","超级大象-物理型","超级金猴-法术型","超级金猴-物理型","超级大熊猫-法术型","超级大熊猫-物理型","超级泡泡-法术型","超级泡泡-物理型","超级神兔-法术型","超级神兔-物理型","超级神虎-法术型","超级神虎-物理型","超级神牛-法术型","超级神牛-物理型","超级海豚-法术型","超级海豚-物理型","超级人参娃娃-法术型","超级人参娃娃-物理型","超级青鸾-法术型","超级腾蛇-法术型","超级腾蛇-物理型","超级神狗-法术型","超级神鼠-法术型","超级神鼠-物理型","超级神猪-法术型","超级神猪-物理型","超级孙小圣-法术型","超级孙小圣-物理型","超级小白龙-物理型","超级猪小戒-物理型","超级飞天-物理型"}
             local  随机名称 = 随机神兽[math.random(1, #随机神兽)]
                table.insert(UserData[id].召唤兽.数据,AddPet(随机名称,"神兽"))
                 UserData[id].召唤兽:刷新属性(#UserData[id].召唤兽.数据)
           SendMessage(UserData[id].连接id, 7, "#y/恭喜你兑换到了"..随机名称)
        SendMessage(UserData[id].连接id, 3006, "66")
      end

  elseif 事件 == "神兜兜相关" and 地图1 == 1001 and 名称 == "袁天罡"   then
    SendMessage(UserData[id].连接id,20,{"诗中仙","袁天罡","您确定用99个“神兜兜”兑换一只神兽吗?特别提醒:可以用25个神兜兜兑换获得的随机神兽需要到对应的超级神兽。",{"我要用99个神兜兜兑换神兽","我要用25个神兜兜+随机神兽兑换"}})
  elseif 事件 == "前往剑冢" and 地图1 == 1001 and 名称 == "袁天罡"   then
    if  RoleControl:GetTaskID(UserData[id],"神器任务")==0  then
       SendMessage(UserData[id].连接id,20,{"诗中仙","袁天罡","你还有没有领取神器任务！"})
    elseif 任务数据[RoleControl:GetTaskID(UserData[id],"神器任务")].进程<5 then 
      SendMessage(UserData[id].连接id,20,{"诗中仙","袁天罡","你还有没有做到这一步任务！"})
    else 
       if UserData[id].队伍~=0 then
        SendMessage(UserData[id].连接id,20,{"诗中仙","袁天罡","请解散队伍以后再找我传送"})
       else 
           MapControl:跳转处理(id,{1217,65*20,91*20,true})
           
         
           SendMessage(UserData[id].连接id,20,{"诗中仙","袁天罡","轩辕竟然真的想杀了陈潇然，上前质问轩辕！"})
       end
    end
  elseif 事件 == "开启神器·序章" and 地图1 == 1001 and 名称 == "袁天罡" then
         if UserData[id].角色.神器 then
            SendMessage(UserData[id].连接id,20,{"诗中仙","袁天罡","你已经获取过神器了"})
         elseif UserData[id].角色.等级 <109 then 
             SendMessage(UserData[id].连接id,20,{"诗中仙","袁天罡","需要109才能开启神器任务"})
        elseif  RoleControl:GetTaskID(UserData[id],"神器任务") ~= 0  then
           SendMessage(UserData[id].连接id,20,{"诗中仙","袁天罡","你已经领取过了神器任务"})
         else 
             if  RoleControl:扣除仙玉(UserData[id],10000,"神器任务") then
                  TaskControl:添加神器任务(id)
            else
                 SendMessage(UserData[id].连接id,20,{"诗中仙","袁天罡","你的仙玉不足1万，无法开启神器任务"})
                return 
            end
         end
	elseif 事件 == "购买灵饰指南书" and 地图1 == 1001 and 名称 == "袁天罡" then
		 ShopControl:GetNpcShop(id,17)
	elseif 事件 == "购买元灵晶石" and 地图1 == 1001 and 名称 == "袁天罡" then
		 ShopControl:GetNpcShop(id,18)
	elseif 事件 == "镶嵌宝石" and 地图1 == 1001 and 名称 == "宝石商人" then
        ItemControl:索要打造道具(id, "包裹", "镶嵌")
	elseif 事件 == "镶嵌珍珠" and 地图1 == 1001 and 名称 == "宝石商人" then
		ItemControl:索要打造道具(id, "包裹", "修理")
	elseif 事件 == "购买宝石" and 地图1 == 1001 and 名称 == "宝石商人" then
		 ShopControl:GetNpcShop(id,16)
	elseif 事件 == "我来给装备开运" and 地图1 == 1001 and 名称 == "符石道人" then
		 	SendMessage(UserData[id].连接id, 20037, ItemControl:索要道具3(id,"包裹"))
   elseif 事件 == "点化装备星位" and 地图1 == 1001 and 名称 == "符石道人" then
   	 		SendMessage(UserData[id].连接id,20,{"道童","符石道人","系统还没有出星位系统"})
	elseif 事件 == "我来合成符石" and 地图1 == 1001 and 名称 == "符石道人" then
		SendMessage(UserData[id].连接id, 20038, ItemControl:索要道具3(id,"包裹"))
	elseif 事件 == "购买" and 地图1 == 20 and 名称 == "武器店掌柜" then
		 ShopControl:GetNpcShop(id,3)
	elseif 事件 == "购买" and 地图1 == 20 and 名称 == "武器店老板" then
          ShopControl:GetNpcShop(id,4)
	elseif 事件 == "购买" and 地图1 == 1017 and 名称 == "饰品店老板" then
          ShopControl:GetNpcShop(id,7)
	elseif 事件 == "购买" and 地图1 == 1022 and 名称 == "服装店老板" then
        ShopControl:GetNpcShop(id,6)
	elseif 事件 == "咨询打造方法" and 地图1 == 1022 and 名称 == "张裁缝" then
			   SendMessage(UserData[id].连接id,20,{"服装店老板","张裁缝","你可以在我这里打工来增加你的裁缝熟练度,熟练度越高制造出来的装备属性越好哦,每次消耗40点体力.可以额外获得3000两银子"})
	elseif 事件 == "有什么需要帮忙的（打工增加熟练度）" and 地图1 == 1022 and 名称 == "张裁缝" then
		    if UserData[id].角色.当前体力 > 40 then
		    	UserData[id].角色.裁缝熟练度=UserData[id].角色.裁缝熟练度+1
               RoleControl:添加银子(UserData[id],3000, "裁缝打工")
               UserData[id].角色.当前体力=UserData[id].角色.当前体力-40
		    else
		      SendMessage(UserData[id].连接id,20,{"服装店老板","张裁缝","你的体力不足40点"})
		    end
	elseif 事件 == "查看熟练度" and 地图1 == 1022 and 名称 == "张裁缝" then
       SendMessage(UserData[id].连接id,20,{"服装店老板","张裁缝","你当前的熟练度:"..UserData[id].角色.裁缝熟练度})
	elseif 事件 == "购买" and 地图1 == 1030 and 名称 == "酒店老板" then

		ShopControl:GetNpcShop(id,28)
	elseif 事件 == "购买" and 地图1 == 1001 and 名称 == "罗道人" then
		ShopControl:GetNpcShop(id,13)
	elseif 事件 == "购买" and 地图1 == 1101 and 名称 == "杜天" then
        ShopControl:GetNpcShop(id,4)
   -- 长安
    elseif 事件 == "点化装备" and 地图1 == 1001 and 名称 == "五行大师" then
           SendMessage(UserData[id].连接id,3011,{名称="五行大师" ,id=0,道具=ItemControl:索要道具2(id,"包裹")})
    elseif 事件 == "学习辅助技能" and 地图1 == 1001 and 名称 == "紫衣盗" then
          SendMessage(UserData[id].连接id,2019,RoleControl:学习技能信息(UserData[id],"辅助技能","你大爷"))
    elseif 事件 == "我要挑战(切入战斗)" and 地图1 == 1001 and 名称 == "梦倾笙" then
		TaskControl:挑战无限轮回(id)
	elseif 事件 == "准备好了，请告诉我们第一关的挑战地点" and 地图1 == 1001 and 名称 == "门派闯关使" then
		TaskControl:领取门派闯关任务(id)
	elseif 事件 == "我来取消任务" and 地图1 == 1001 and 名称 == "门派闯关使" then
			if RoleControl:GetTaskID(UserData[id],"门派闯关") == 0 then
			SendMessage(UserData[id].连接id, 7, "#y/你当前没有可取消的任务")
		else
			RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"门派闯关"))
			SendMessage(UserData[id].连接id, 7, "#y/你的门派闯关任务已经取消了")
		end
     --镖局
	elseif 事件 == "一级镖银(要求等级30、消耗活力30、可得200000两银子)" and 地图1 == 1024 and 名称 == "郑镖头" then
	elseif 事件 == "二级镖银(要求等级50、消耗活力50、可得350000两银子)" and 地图1 == 1024 and 名称 == "郑镖头" then
	elseif 事件 == "三级镖银(要求等级70、消耗活力70、可得600000两银子)" and 地图1 == 1024 and 名称 == "郑镖头" then
	elseif 事件 == "四级镖银(要求等级90、消耗活力90、可得850000两银子)" and 地图1 == 1024 and 名称 == "郑镖头" then
	elseif 事件 == "五级镖银(要求等级110、消耗活力110、可得1200000两银子)" and 地图1 == 1024 and 名称 == "郑镖头" then
	elseif 事件 == "取消运镖任务" and 地图1 == 1024 and 名称 == "郑镖头" then
	elseif 事件 == "我想为人物染色" and 地图1 == 1001 and 名称 == "染色师" then
      SendMessage(UserData[id].连接id, 3012, RoleControl:获取角色染色数据(UserData[id]))
	elseif 事件 == "是的我要去" and 地图1 == 1001 and 名称 == "驿站老板" then
      MapControl:跳转处理(id,{1110,80*20,83*20,true})
    elseif 事件 == "我要更改造型" and 地图1 == 1001 and 名称 == "袁守诚" then
		wb = " 请选择你要更改的造型，更改一次造型需要100万银子"
		local xx = {"飞燕女","英女侠","巫蛮儿","逍遥生","剑侠客","狐美人","骨精灵","杀破狼","巨魔王","虎头怪","舞天姬","玄彩娥","羽灵神","神天兵","龙太子","偃无师","鬼潇潇","桃夭夭"}
         SendMessage(UserData[id].连接id,20,{"诗中仙","袁守诚",wb,xx})
	elseif 事件 == "飞燕女" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"飞燕女")
	elseif 事件 == "英女侠" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"英女侠")
	elseif 事件 == "巫蛮儿" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"巫蛮儿")
	elseif 事件 == "逍遥生" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"逍遥生")
	elseif 事件 == "剑侠客" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"剑侠客")
	elseif 事件 == "狐美人" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"狐美人")
	elseif 事件 == "骨精灵" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"骨精灵")
	elseif 事件 == "杀破狼" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"杀破狼")
	elseif 事件 == "巨魔王" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"巨魔王")
	elseif 事件 == "虎头怪" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"虎头怪")
	elseif 事件 == "舞天姬" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"舞天姬")
	elseif 事件 == "玄彩娥" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"玄彩娥")
	elseif 事件 == "羽灵神" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"羽灵神")
	elseif 事件 == "神天兵" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"神天兵")
	elseif 事件 == "龙太子" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"龙太子")
	elseif 事件 == "桃夭夭" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"桃夭夭")
	elseif 事件 == "偃无师" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"偃无师")
	elseif 事件 == "鬼潇潇" and 地图1 == 1001 and 名称 == "袁守诚" then
		 RoleControl:改变造型(UserData[id],"鬼潇潇")
	elseif 事件 == "是的我要为角色洗点一次" and 地图1 == 1001 and 名称 == "洗点人" then
		if RoleControl:取是否佩戴装备(UserData[id])  then
			SendMessage(UserData[id].连接id,20,{"道士","洗点人","急着洗点被雷劈啊？衣服和法宝先脱下来！"})
		elseif 银子检查(id,1000000) then
			RoleControl:扣除银子(UserData[id],1000000, "角色洗点")
			RoleControl:重置属性点(UserData[id])
			RoleControl:添加消费日志(UserData[id],"花费100万银子重置角色属性点")
			SendMessage(UserData[id].连接id,20,{"道士","洗点人","已经洗点成功了！要想再次洗点，欢迎下次再来"})
		else
			SendMessage(UserData[id].连接id,20,{"道士","洗点人","走走走!你这个穷光蛋!"})
		end
	-- 药店
	elseif 事件 == "购买" and 地图1 == 1016 and 名称 == "药店老板" then
		 ShopControl:GetNpcShop(id,10)
	-- 服饰店
	elseif 事件 == "购买" and 地图1 == 1095 then
         ShopControl:GetNpcShop(id,8)
	elseif 事件 == "购买" and 地图1 == 1083 then
		 ShopControl:GetNpcShop(id,11)
	elseif 事件 == "购买" and 地图1 == 1085 then
		local ab = {}
		for i=1,17 do
			insert(ab,tp.打造物品[i][5])
		end
		tp.窗口.商店:打开(ab)

	elseif 事件 == "乌鸡国" and 地图1 == 1026  and 名称 == "吴举人" then
		    SendMessage(UserData[id].连接id,20,{"书生","吴举人"," 你可以在我这里开启乌鸡国副本。开启本副本需要队伍成员数达到5人。且等级必须达到50级。每次开启本副本需要消耗所有队员100点人气与50万两银子。",{"开启副本","进入副本","取消"}})
	elseif 事件 == "开启副本" and 地图1 == 1026  and 名称 == "吴举人" then
            TaskControl:创建乌鸡国副本(id)
	elseif 事件 == "进入副本" then
		if UserData[id].副本 == 0 then
			SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有开启副本任务")
		else
			self.允许传送 = true

			if UserData[id].队伍 ~= 0 then
				for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
					if UserData[队伍数据[UserData[id].队伍].队员数据[n]].副本 ~= UserData[id].副本 then
						self.允许传送 = false
					end
				end
			end

			if self.允许传送 then
				MapControl:Jump(id, 任务数据[UserData[id].副本].传送数据.地图, 任务数据[UserData[id].副本].传送数据.x, 任务数据[UserData[id].副本].传送数据.y)
			else
				SendMessage(UserData[id].连接id, 7, "#y/有队员与队长副本不一致，无法从我这里传送")
			end
		end
	elseif 事件 == "是的我要去" and 地图1 == 1001 and 名称 == "圣山传送人" then
     MapControl:跳转处理(id,{1205,115*20,94*20})

    ---兜率宫

     elseif 事件 == "我来合成法宝" and 地图1 == 1113 and 名称 == "太上老君" then
     	SendMessage(UserData[id].连接id,2034,ItemControl:索要道具3(id, "包裹"))
     elseif 事件 == "我来取消任务" and 地图1 == 1113 and 名称 == "金童子" then
		if RoleControl:GetTaskID(UserData[id],"法宝") == 0 then
			SendMessage(UserData[id].连接id, 20, {"道童","金童子","你似乎还没有领取本任务"})
		else
			RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"法宝"))
			SendMessage(UserData[id].连接id, 7, "#y/你取消了法宝任务")
		end
  	---仙缘洞天

  	elseif 事件 == "领取坐骑" and 地图1 == 1216 and 名称 == "百兽王" then
  		if RoleControl:GetTaskID(UserData[id],"坐骑") ~= 0  and 任务数据[RoleControl:GetTaskID(UserData[id],"坐骑")].进程==5  then
  			TaskControl:完成坐骑任务(id,RoleControl:GetTaskID(UserData[id],"坐骑"))
  		end
	elseif 事件 == "啊！太好了!我正好想换换我的坐骑颜色" and 地图1 == 1216 and 名称 == "仙缘染坊主" then
		 if UserData[id].角色.坐骑.编号 == 0 then
		 	SendMessage(UserData[id].连接id, 20, {"赵姨娘","仙缘染坊主","请将需要染色的坐骑选中骑乘状态"})
		 else
			SendMessage(UserData[id].连接id, 20049,UserData[id].角色.坐骑数据[UserData[id].角色.坐骑.编号])
		end
	elseif 事件 == "我要更换饰品的颜色" and 地图1 == 1216 and 名称 == "仙缘染坊主" then
		 if UserData[id].角色.坐骑.编号 == 0 then
		 	SendMessage(UserData[id].连接id, 20, {"赵姨娘","仙缘染坊主","请将需要染色的坐骑选中骑乘状态"})
		 elseif UserData[id].角色.坐骑数据[UserData[id].角色.坐骑.编号].饰品 == nil then
		 	SendMessage(UserData[id].连接id, 20, {"赵姨娘","仙缘染坊主","你的坐骑还没有佩戴饰品"})
		 else
			SendMessage(UserData[id].连接id, 20050,UserData[id].角色.坐骑数据[UserData[id].角色.坐骑.编号])
		end



  	elseif 事件 == "领取坐骑任务" and 地图1 == 1216 and 名称 == "百兽王" then
		if UserData[id].队伍 ~= 0 then
			SendMessage(UserData[id].连接id, 7, "#y/本任务需要单人来领取")
		elseif	RoleControl:GetTaskID(UserData[id],"坐骑") ~= 0  then
			SendMessage(UserData[id].连接id, 7, "#y/你当前还有任务没做完，无法领取新的任务")
		elseif UserData[id].角色.等级 < 60  then
			SendMessage(UserData[id].连接id, 7, "#y/本任务需要等级达到60级才可领取")
		elseif 银子检查(id,2000000)  then
            if UserData[id].角色.当前体力 >= 200 then
              	RoleControl:扣除银子(UserData[id],2000000, "坐骑任务")
              	UserData[id].角色.当前体力=UserData[id].角色.当前体力 - 200
			   TaskControl:添加坐骑任务(id)
			else
				SendMessage(UserData[id].连接id, 20, {"大大王","百兽王","你的体力不足200点无法领取任务"})
            end
		else
			SendMessage(UserData[id].连接id, 20, {"大大王","百兽王","你的钱不足200万无法领取任务。"})
		end
  	elseif 事件 == "取消坐骑任务" and 地图1 == 1216 and 名称 == "百兽王" then
		if RoleControl:GetTaskID(UserData[id],"坐骑") == 0 then
			SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有领取本任务")
		else
			RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"坐骑"))
			SendMessage(UserData[id].连接id, 7, "#y/你取消了坐骑任务")
		end

    ---地狱迷宫3

     elseif 事件 == "我来换高级藏宝图" and 地图1 == 1129 and 名称 == "无名野鬼" then
		local 找到物品 = false
		local 特赦格子 = 0
		for n = 1, 80 do
			if UserData[id].角色.道具["包裹"][n] ~= nil and 找到物品 == false and UserData[id].物品[UserData[id].角色.道具["包裹"][n]].名称 == "特赦令牌" then
				找到物品 = true
				特赦格子=n
	          break
			end
		end
		if 找到物品 == false then
			SendMessage(UserData[id].连接id, 20, {"野鬼","无名野鬼","你身上没有特赦令牌呀"})
		else
       ItemControl:RemoveItems(UserData[id],特赦格子,"特赦令牌")
			ItemControl:GiveItem(id, "高级藏宝图")
			SendMessage(UserData[id].连接id, 3006, "66")
		end

	-- 傲来国
      elseif 事件 == "回收魔兽要诀"   then
 
    local 五宝格子 = {}
    local nubersd = 0
    for i = 1, 1 do
    
      for n = nubersd+1, 20 do
        if UserData[id].角色.道具["包裹"][n] ~= nil  and UserData[id].物品[UserData[id].角色.道具["包裹"][n]].名称 =="魔兽要诀"  then
          五宝格子[#五宝格子 + 1] = n
          nubersd=n
           break
        end
      end
    end

    if #五宝格子 ~= 1 then
      SendMessage(UserData[id].连接id, 20, {"钱庄老板","回收商人","没有找到你所要回收的物品"})
    else
      for n = 1, 1 do
          ItemControl:RemoveItems(UserData[id],五宝格子[n],"魔兽要诀")

      end
      RoleControl:添加成就积分(UserData[id],5)
      SendMessage(UserData[id].连接id, 3006, "66")
    end
    --
      elseif 事件 == "回收高级魔兽要诀"   then
 
    local 五宝格子 = {}
    local nubersd = 0
    for i = 1, 1 do
    
      for n = nubersd+1, 20 do
        if UserData[id].角色.道具["包裹"][n] ~= nil  and UserData[id].物品[UserData[id].角色.道具["包裹"][n]].名称 =="高级魔兽要诀"  then
          五宝格子[#五宝格子 + 1] = n
          nubersd=n
           break
        end
      end
    end

    if #五宝格子 ~= 1 then
      SendMessage(UserData[id].连接id, 20, {"钱庄老板","回收商人","没有找到你所要回收的物品"})
    else
      for n = 1, 1 do
          ItemControl:RemoveItems(UserData[id],五宝格子[n],"高级魔兽要诀")

      end
      RoleControl:添加成就积分(UserData[id],50)
      SendMessage(UserData[id].连接id, 3006, "66")
    end
    ---
      elseif 事件 == "回收特殊魔兽要诀"   then
 
    local 五宝格子 = {}
    local nubersd = 0
    for i = 1, 1 do
    
      for n = nubersd+1, 20 do
        if UserData[id].角色.道具["包裹"][n] ~= nil  and UserData[id].物品[UserData[id].角色.道具["包裹"][n]].名称 =="特殊魔兽要诀"  then
          五宝格子[#五宝格子 + 1] = n
          nubersd=n
           break
        end
      end
    end

    if #五宝格子 ~= 1 then
      SendMessage(UserData[id].连接id, 20, {"钱庄老板","回收商人","没有找到你所要回收的物品"})
    else
      for n = 1, 1 do
          ItemControl:RemoveItems(UserData[id],五宝格子[n],"特殊魔兽要诀")

      end
      RoleControl:添加成就积分(UserData[id],500)
      SendMessage(UserData[id].连接id, 3006, "66")
    end
     elseif 事件 == "熔炼内丹"   then
 
    local 五宝格子 = {}
    local nubersd = 0
    for i = 1, 3 do
    
      for n = nubersd+1, 20 do
        if UserData[id].角色.道具["包裹"][n] ~= nil  and UserData[id].物品[UserData[id].角色.道具["包裹"][n]].名称 =="召唤兽内丹"  then
          五宝格子[#五宝格子 + 1] = n
          nubersd=n
           break
        end
      end
    end

    if #五宝格子 ~= 3 then
      SendMessage(UserData[id].连接id, 20, {"兔子怪","偷偷怪","只有集齐笔、墨、纸、砚才可换炼兽真经"})
    else
      for n = 1, 3 do
          ItemControl:RemoveItems(UserData[id],五宝格子[n],"召唤兽内丹")

      end
      ItemControl:GiveItem(id, "召唤兽内丹")
      SendMessage(UserData[id].连接id, 3006, "66")
    end
      elseif 事件 == "熔炼高级内丹"   then
 
    local 五宝格子 = {}
    local nubersd = 0
    for i = 1, 3 do
    
      for n = nubersd+1, 20 do
        if UserData[id].角色.道具["包裹"][n] ~= nil  and UserData[id].物品[UserData[id].角色.道具["包裹"][n]].名称 =="高级召唤兽内丹"  then
          五宝格子[#五宝格子 + 1] = n
          nubersd=n
           break
        end
      end
    end

    if #五宝格子 ~= 3 then
      SendMessage(UserData[id].连接id, 20, {"兔子怪","偷偷怪","只有集齐笔、墨、纸、砚才可换炼兽真经"})
    else
      for n = 1, 3 do
          ItemControl:RemoveItems(UserData[id],五宝格子[n],"高级召唤兽内丹")

      end
      ItemControl:GiveItem(id, "高级召唤兽内丹")
      SendMessage(UserData[id].连接id, 3006, "66")
    end
	elseif 事件 == "我要换炼兽真经" and 地图1 == 1092 and 名称 == "偷偷怪" then
		local 五宝名称 = {"笔","墨","纸","砚"}
		local 五宝格子 = {}
		for i = 1, #五宝名称 do
			local 五宝找到 = false
			for n = 1, 80 do
				if UserData[id].角色.道具["包裹"][n] ~= nil and 五宝找到 == false and UserData[id].物品[UserData[id].角色.道具["包裹"][n]].名称 == 五宝名称[i] then
					五宝找到 = true
					五宝格子[#五宝格子 + 1] = n
				end
			end
		end

		if #五宝格子 ~= 4 then
			SendMessage(UserData[id].连接id, 20, {"兔子怪","偷偷怪","只有集齐笔、墨、纸、砚才可换炼兽真经"})
		else
			for n = 1, 4 do

        ItemControl:RemoveItems(UserData[id],五宝格子[n],"五宝")
			end
			ItemControl:GiveItem(id, "炼兽真经")
			SendMessage(UserData[id].连接id, 3006, "66")
		end

	elseif 事件 == "我要换特赦令牌" and 地图1 == 1092 and 名称 == "偷偷怪" then
		local 五宝名称 = {"夜光珠","龙鳞","定魂珠","避水珠","金刚石"}
		local 五宝格子 = {}
		for i = 1, #五宝名称 do
			local 五宝找到 = false
			for n = 1, 80 do
				if UserData[id].角色.道具["包裹"][n] ~= nil and 五宝找到 == false and UserData[id].物品[UserData[id].角色.道具["包裹"][n]].名称 == 五宝名称[i] then
					五宝找到 = true
					五宝格子[#五宝格子 + 1] = n
				end
			end
		end

		if #五宝格子 ~= 5 then
			SendMessage(UserData[id].连接id, 20, {"兔子怪","偷偷怪","只有集齐龙鳞、夜光珠、避水珠、定魂珠、金刚石才可换特赦令牌"})
		else
			for n = 1, 5 do
        ItemControl:RemoveItems(UserData[id],五宝格子[n],"五宝")
				
			end
			ItemControl:GiveItem(id, "特赦令牌")
			
		end
	elseif 事件 == "学习宝石工艺" and 地图1 == 1092 and 名称 == "蝴蝶妹妹" then
		SendMessage(UserData[id].连接id, 20, {"蝴蝶仙子","蝴蝶妹妹","当前还未开放剧情技能哦#24"})
	elseif 事件 == "我来合成碎石锤" and 地图1 == 1092 and 名称 == "蝴蝶妹妹" then
			ItemControl:索要打造道具(id, "包裹", "合成")
	elseif 事件 == "送我过去" and 地图1 == 1092 and 名称 == "驿站老板" then
		MapControl:跳转处理(id,{1001,360*20,200*20})
	elseif 事件 == "领取任务" and 地图1 == 1040 and 名称 == "嘉年华活动" then
		if UserData[id].队伍 == 0 then
			SendMessage(UserData[id].连接id, 7, "#y/本任务需要五人组队完成")
		elseif #队伍数据[UserData[id].队伍].队员数据 < 5 and  DebugMode ==false then
			SendMessage(UserData[id].连接id, 7, "#y/本任务需要五人组队完成")
		elseif 取队伍符合等级(id, 60) == false then
			SendMessage(UserData[id].连接id, 7, "#y/本任务需要等级达到60级才可领取")
		else
			for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
				if RoleControl:GetTaskID(UserData[队伍数据[UserData[id].队伍].队员数据[n]],"嘉年华") ~= 0 or 活动数据.嘉年华数据[队伍数据[UserData[id].队伍].队员数据[n]] ~= nil then
					SendMessage(UserData[id].连接id, 7, "#y/" .. UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称 .. "已经领取过任务了")

					return 0
				end
			end

			TaskControl:添加嘉年华任务(id)
		end
	elseif 事件 == "取消任务" and 地图1 == 1040 and 名称 == "嘉年华活动" then
		if RoleControl:GetTaskID(UserData[id],"嘉年华") == 0 then
			SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有领取本任务")
		else
			RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"嘉年华"))
			SendMessage(UserData[id].连接id, 7, "#y/你取消了嘉年华任务")
		end
	elseif 事件 == "是的我要去"and 地图1 == 1092 and 名称 == "船夫" then
         MapControl:跳转处理(id,{1506,70*20,93*20})
	elseif 事件 == "我来取消任务" and 地图1 == 1092 and 名称 == "游泳大赛报名官" then
				if RoleControl:GetTaskID(UserData[id],"游泳")==0 then
				SendMessage(UserData[id].连接id,20,{"进阶超级海豚","游泳大赛报名官","#Y/请先领取游泳任务"})
			    elseif UserData[id].队伍~=0 then
				     SendMessage(UserData[id].连接id,20,{"进阶超级海豚","游泳大赛报名官","#Y/请解散队伍以后再过来取消"})
				else
				SendMessage(UserData[id].连接id,20,{"进阶超级海豚","游泳大赛报名官","#Y/你取消了游泳任务"})
				RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"游泳"))
          任务数据[RoleControl:GetTaskID(UserData[id],"游泳")]=nil
				end
	elseif 事件 == "我要参赛" and 地图1 == 1092 and 名称 == "游泳大赛报名官" then
				    if 游戏比赛开关==false then
				      SendMessage(UserData[id].连接id,7,"#Y/12-22点才可参加比赛")
				     return 0
				    elseif UserData[id].队伍==0 then
				     SendMessage(UserData[id].连接id,7,"#Y/本大赛最少需要三人组队参加")
				     return 0
				    elseif #队伍数据[UserData[id].队伍].队员数据<3  and DebugMode == false then
				     SendMessage(UserData[id].连接id,7,"#Y/本大赛最少需要三人组队参加")
				     return 0
				    elseif 取队伍符合等级(id,65)==false  then
				     SendMessage(UserData[id].连接id,7,"#Y/本大赛要求等级达到65级后才可参加")
				     return 0
				    else
				      self.检查通过=""
				      self.检查通过1=""
				      for n=1,#队伍数据[UserData[id].队伍].队员数据 do
				        if 游泳比赛数据[队伍数据[UserData[id].队伍].队员数据[n]]==nil then
				             游泳比赛数据[队伍数据[UserData[id].队伍].队员数据[n]]=0
				        elseif 游泳比赛数据[队伍数据[UserData[id].队伍].队员数据[n]]>=2 then
				         self.检查通过=UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称
				         elseif RoleControl:GetTaskID(UserData[队伍数据[UserData[id].队伍].队员数据[n]],"游泳") then
				         self.检查通过1=UserData[队伍数据[UserData[id].队伍].队员数据[n]].角色.名称
				         end
				        end
				      if self.检查通过~="" then
				            SendMessage(UserData[id].连接id,7,"#Y/"..self.检查通过.."本日已经参赛两次，无法再参赛了")
				            return 0
				       elseif self.检查通过1~="" then
				            SendMessage(UserData[id].连接id,7,"#Y/"..self.检查通过1.."已经领取了任务")
				             return 0
				        end
				      TaskControl:添加游泳任务(id)
				      end
	elseif 事件 == "购买商品" and 地图1 == 1226 and 名称 == "云游道人" then
		ShopControl:GetNpcShop(id,15)
    elseif 事件 == "我要去昆仑仙境" and 地图1 == 1001 and 名称 == "2021春节使者" then
        if UserData[id].队伍 ~= 0 then
            SendMessage(UserData[id].连接id, 7, "#y/无昆仑仙境不允许组队进入")
        elseif UserData[id].角色.等级 < 40 then
            SendMessage(UserData[id].连接id, 7, "#y/只有等级达到40级的玩家才可进入无间炼狱")
        elseif RoleControl:GetTaskID(UserData[id],"天机培元丹") == 0 then
            SendMessage(UserData[id].连接id, 7, "#y/你还未使用天机培元丹")

        else
            self.临时坐标 = MapControl:Randomloadtion(1214)
            MapControl:Jump(id, 1214, self.临时坐标.x,self.临时坐标.y)

            SendMessage(UserData[id].连接id, 7, "#y/在本地图中每隔60秒可获得一次奖励")
            if RoleControl:GetTaskID(UserData[id],"九霄清心丸") ~= 0 then
                SendMessage(UserData[id].连接id, 7, "#y/你现在可以获得九霄清心丸的双倍加成效果")
            end

        end

    elseif 事件 == "我要购买挂机物品" and 地图1 == 1001 and 名称 == "2021春节使者" then
        ShopControl:GetNpcShop(id,38)
           elseif 事件 == "购买" and 地图1 == 1001 and 名称 == "便捷道具" then
    ShopControl:GetNpcShop(id,39)
   elseif 事件 == "购买" and 地图1 == 1501 and 名称 == "建邺便捷道具" then
    ShopControl:GetNpcShop(id,39)
    elseif 事件 == "购买" and 地图1 == 1092 and 名称 == "傲来便捷道具" then
    ShopControl:GetNpcShop(id,39)
	elseif 事件 == "花费2000两购买鱼竿" and 地图1 == 1092 and 名称 == "渔夫" then
		if 银子检查(id, 2000) then
			RoleControl:扣除银子(UserData[id], 2000, "买鱼竿")
			   ItemControl:GiveItem(id,"鱼竿")
		else
			SendMessage(UserData[id].连接id, 20, {"捕鱼人","渔夫","钱不够呢。"})
		end

	elseif 事件 == "兑换海产" and 地图1 == 1092 and 名称 == "渔夫" then
		local xx = {"随机二级鱼类","随机三级鱼类","随机属性人参果","祥瑞腾蛇"}
	  SendMessage(UserData[id].连接id, 20, {"捕鱼人","渔夫","请选择你要兑换的物品",xx})
	elseif 事件 == "是的我要去" and 地图1 == 1092 and 名称 == "仙岛引路人" then
        MapControl:跳转处理(id,{1207,10*20,109*20,true})
	-- 未知
	elseif 事件 == "是的我要去" and 地图1 == 1116 and 名称 == "虾兵" then

    MapControl:跳转处理(id,{1506,95*20,37*20,true})
 	elseif 事件 == "我们要挑战" and 地图1 == 1208 and 名称 == "巡游天官" then
          TaskControl:领取群雄逐鹿任务(id)
 	elseif 事件 == "我要祈福(消耗200仙玉)"  and 名称 == "神机道人" then
 		SendMessage(UserData[id].连接id,20048,{类型="仙玉",祈福=UserData[id].角色.祈福})
 	elseif 事件 == "我要祈福(消耗5千万银子)"  and 名称 == "神机道人" then
		SendMessage(UserData[id].连接id,20048,{类型="银子",祈福=UserData[id].角色.祈福})
    elseif 事件 == "我要抽奖(消耗2千万银子)" and 地图1 == 1208 and 名称 == "银币抽奖" then
            ItemControl:银币抽奖(id)

	elseif 事件 == "我来取消任务" and 地图1 == 1208 and 名称 == "巡游天官" then
	    		if RoleControl:GetTaskID(UserData[id],"群雄逐鹿")==0 then
				SendMessage(UserData[id].连接id,20,{"巡游天官","巡游天官","#Y/请先领取活动"})
			    elseif UserData[id].队伍~=0 then
				     SendMessage(UserData[id].连接id,20,{"巡游天官","游巡游天官","#Y/请解散队伍以后再过来取消"})
				else
				SendMessage(UserData[id].连接id,20,{"巡游天官","游巡游天官","#Y/你取消了群雄逐鹿任务"})
				RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"群雄逐鹿"))
				任务数据[RoleControl:GetTaskID(UserData[id],"群雄逐鹿")]=nil
				end
	elseif 事件 == "传送仙缘洞天" and 地图1 == 1208 and 名称 == "申太公" then
		MapControl:跳转处理(id,{1216,211*20,40*20,true})
	-- 北俱


	elseif 事件 == "是的我要去" and 地图1 == 1174 and 名称 == "地遁鬼" then
     MapControl:跳转处理(id,{1091,73*20,103*20,true})
	elseif 事件 == "是的我要去" and 地图1 == 1174 and 名称 == "女娲神迹传送人" then

      MapControl:跳转处理(id,{1201,47*20,105*20,true})
	-- 长寿郊外
	elseif 事件 == "是的我要去" and 地图1 == 1091 and 名称 == "驿站老板" then

      MapControl:跳转处理(id,{1174,193*20,155*20,true})

	elseif 事件 == "是的我要去" and 地图1 == 1091 and 名称 == "传送天将" then

      MapControl:跳转处理(id,{1111,246*20,158*20,true})

	elseif 事件 == "是的我要去" and 地图1 == 1091 and 名称 == "西牛贺洲土地" then

      MapControl:跳转处理(id,{1173,33*20,96*20,true})

	-- 境外



	elseif 事件 == "切入战斗" and 地图1 == 1173 and 名称 == "野猪王" then
		 FightGet:进入处理(id, 100027, "66", RoleControl:GetTaskID(UserData[id],"嘉年华"))
	elseif 事件 == "是的我要去" and 地图1 == 1173 and 名称 == "大唐境外土地" then
       MapControl:跳转处理(id,{1091,84*20,151*20,true})
	elseif 事件 == "是的我要去" and 地图1 == 1173 and 名称 == "碗子山土地" then
     MapControl:跳转处理(id,{1228,85*20,183*20,true})
     elseif 事件 == "送我过去" and 地图1 == 1173 and 名称 == "驿站老板" then
     MapControl:跳转处理(id,{1001,363*20,198*20,true})
     --建邺名居
     elseif 事件 == "开打" and 地图1 == 1526 and 名称 == "周猎户" then
      	FightGet:进入处理(id, 100061, "66", RoleControl:GetTaskID(UserData[id],"坐骑"))
	-- 天宫

    elseif 事件 == "岂有此理,谁怕谁" and 地图1 == 1111 and 名称 == "水兵统领" then
          FightGet:进入处理(id, 100060, "66", RoleControl:GetTaskID(UserData[id],"坐骑"))
    elseif 事件 == "上交材料" and 地图1 == 1111 and 名称 == "顺风耳" then
       		if RoleControl:GetTaskID(UserData[id],"坐骑") ~= 0  and 任务数据[RoleControl:GetTaskID(UserData[id],"坐骑")].进程==2  then
            SendMessage(UserData[id].连接id,3011,{名称="顺风耳" ,id=RoleControl:GetTaskID(UserData[id],"坐骑"),道具=ItemControl:索要道具1(id,"包裹")})
            end
   	elseif 事件 == "抓捕生肖" and 地图1 == 1111 and 名称 == "守门天兵" then
   		 SendMessage(UserData[id].连接id,20,{"天兵","守门天兵","活动还未开发"})
	elseif 事件 == "是的我要去" and 地图1 == 1111 and 名称 == "守门天兵" then
      MapControl:跳转处理(id,{1091,24*20,119*20,true})
	-- 国境

	elseif 事件 == "领取奖励" and 地图1 == 1110 and 名称 == "吴老爹" then
		if RoleControl:GetTaskID(UserData[id],"嘉年华") ~= 0 and 任务数据[RoleControl:GetTaskID(UserData[id],"嘉年华")].进程 == 2 then
			TaskControl:完成嘉年华任务1(id)
		end
	elseif 事件 == "送我去凌波城" and 地图1 == 1110 and 名称 == "大唐国境土地" then
      MapControl:跳转处理(id,{1150,9*20,94*20,true})
	elseif 事件 == "是的我要去" and 地图1 == 1110 and 名称 == "普陀山接引仙女" then
     MapControl:跳转处理(id,{1140,84*20,62*20,true})
    elseif 事件 == "送我过去" and 地图1 == 1110 and 名称 == "驿站老板" then
     MapControl:跳转处理(id,{1001,280*20,240*20,true})
    elseif 事件 == "学习生活技能" and 地图1 == 1110 and 名称 == "黑山老妖" then
         SendMessage(UserData[id].连接id,2019,RoleControl:学习技能信息(UserData[id],"辅助技能"))
	-- 盘丝
	elseif 事件 == "切入战斗" and 地图1 == 1513 and 名称 == "女妖" then
       FightGet:进入处理(id,100028,"66",RoleControl:GetTaskID(UserData[id],"嘉年华"))
	-- 碗子山
	elseif 事件 == "是的我要去" and 地图1 == 1228 and 名称 == "碗子山土地" then
     MapControl:跳转处理(id,{1139,83*20,45*20})
	-- 西梁女国
	elseif 事件 == "是的我要去" and 地图1 == 1040 and 名称 == "驿站老板" then
       MapControl:跳转处理(id,{1208,128*20,36*20})
	elseif 事件 == "我去！我去！" and 地图1 == 1040 and 名称 == "驿站老板" then
      MapControl:跳转处理(id,{1235,465*20,85*20})
	-- 丝绸之路
	elseif 事件 == "是的我要去" and 地图1 == 1235 and 名称 == "驿站老板" then
        MapControl:跳转处理(id,{1040,25*20,110*20})
	-- 麒麟山
	elseif 事件 == "送我过去吧" and 地图1 == 1210 and 名称 == "驿站老板" then
       MapControl:跳转处理(id,{1226,10*20,111*20})
	-- 蓬莱
	elseif 事件 == "送我过去吧" and 地图1 == 1207 and 名称 == "驿站老板" then
     MapControl:跳转处理(id,{1092,34*20,61*20})
	-- 玩法

	elseif 事件 == "我来帮你抓鬼" and (地图1 == 1122 or 地图1 ==1209) and 名称 == "钟馗" then

		TaskControl:设置抓鬼任务(id)
    elseif 事件 == "取消任务" and (地图1 == 1122 or 地图1 ==1209) and 名称 == "钟馗" then
		if RoleControl:GetTaskID(UserData[id],"抓鬼")==0 then
		SendMessage(UserData[id].连接id,20,{"钟馗","钟馗","#Y/请先领取抓鬼任务"})
		else
		SendMessage(UserData[id].连接id,20,{"钟馗","钟馗","#Y/你取消了抓鬼任务"})
		MapControl:移除单位(任务数据[RoleControl:GetTaskID(UserData[id],"抓鬼")].地图编号,RoleControl:GetTaskID(UserData[id],"抓鬼"))
		RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"抓鬼"))
		任务数据[RoleControl:GetTaskID(UserData[id],"抓鬼")]=nil
		UserData[id].角色.抓鬼次数=1
		end

	elseif 事件 == "我要领取鬼王任务" and (地图1 == 1125 or 地图1 ==1209)and 名称 == "黑无常" then

		TaskControl:设置鬼王任务(id)
    elseif 事件 == "我要取消任务" and (地图1 == 1125 or 地图1 ==1209) and 名称 == "黑无常" then
		if RoleControl:GetTaskID(UserData[id],"鬼王")==0 then
		SendMessage(UserData[id].连接id,20,{"黑无常","黑无常","#Y/请先领取鬼王任务"})
		else
		SendMessage(UserData[id].连接id,20,{"黑无常","黑无常","#Y/你取消了鬼王任务"})
        MapControl:移除单位(任务数据[RoleControl:GetTaskID(UserData[id],"鬼王")].地图编号,RoleControl:GetTaskID(UserData[id],"鬼王"))
		RoleControl:取消任务(UserData[id],RoleControl:GetTaskID(UserData[id],"鬼王"))
		任务数据[RoleControl:GetTaskID(UserData[id],"鬼王")]=nil
		end

	elseif 事件 == "听听无妨。。。(消耗2000两银子)" and 地图1 == 1028 and 名称 == "店小二" then
		  TaskControl:设置打图任务(id)

   elseif 事件 == "给予道具" and 地图1 == 1028 and 名称 == "店小二" then
        SendMessage(UserData[id].连接id, 3011, {名称="店小二",id = RoleControl:GetTaskID(UserData[id],"神器任务"),道具 = ItemControl:索要道具2(id, "包裹")})
	elseif 事件 == "我要住店休息" and 地图1 == 1028 and 名称 == "酒店老板" then
				if 银子检查(id,500) == false then
					SendMessage(UserData[id].连接id, 7, "银子不足500两")
					return 0
				 else
				 	RoleControl:扣除银子(UserData[id], 500, 2)
				 	RoleControl:刷新战斗属性(UserData[id])
				 	SendMessage(UserData[id].连接id,20,{"酒店老板","酒店老板","你的气血魔法已经恢复,欢迎下次再来"})
				end

	-- 门派传送
	elseif 事件 == "是的我要去" and ((地图1 == 1135 and 名称 == "接引道童")  or (地图1 == 1142 and 名称 == "接引女使")  or (地图1 == 1198 and 名称 == "传送护卫") or
		(地图1 == 1002 and 名称 == "接引僧") or (地图1 == 1513 and 名称 == "引路小妖") or (地图1 == 1139 and 名称 == "接引小妖") or
		(地图1 == 1131 and 名称 == "传送小妖") or (地图1 == 1140 and 名称 == "接引仙女") or (地图1 == 1111 and 名称 == "接引仙女") or (地图1 == 1150 and 名称 == "传送天将") or
		(地图1 == 1116 and 名称 == "传送蟹将") or (地图1 == 1146 and 名称 == "接引道童") or (地图1 == 1251 and 名称 == "引路灵猴") or (地图1 == 1249 and 名称 == "引路孤魂") or (地图1 == 1250 and 名称 == "哒哒") or (地图1 == 1138 and 名称 == "引路族民") or (地图1 == 1122 and 名称 == "地遁鬼") or (地图1 == 1198 and 名称 == "传送护卫") or
		(地图1 == 1512 and 名称 == "传送牛妖")) then
       MapControl:跳转处理(id,{1001,473*20,250*20,true})



	end
end



function 事件解析:取师门地图(师门)
	local 地图
	if 师门 == "化生寺" then
		地图 = 1002
	elseif 师门 == "方寸山" then
		地图 = 1135
	elseif 师门 == "女儿村" then
		地图 = 1142
	elseif 师门 == "神木林" then
		地图 = 1138
	elseif 师门 == "大唐官府" then
		地图 = 1198
	elseif 师门 == "阴曹地府" then
		地图 = 1122
	elseif 师门 == "盘丝洞" then
		地图 = 1513
	elseif 师门 == "无底洞" then
		地图 = 1139
	elseif 师门 == "魔王寨" then
		地图 = 1512
	elseif 师门 == "狮驼岭" then
		地图 = 1131
	elseif 师门 == "天宫" then
		地图 = 1111
	elseif 师门 == "普陀山" then
		地图 = 1140
	elseif 师门 == "凌波城" then
		地图 = 1150
	elseif 师门 == "五庄观" then
		地图 = 1146
	elseif 师门 == "龙宫" then
		地图 = 1116
  elseif 师门 == "花果山" then
    地图 = 1251
  elseif 师门 == "天机城" then
    地图 = 1257
  elseif 师门 == "女魃墓" then
    地图 = 1252

	end
	return 地图
end


function 事件解析:取师父地图(ID)
	local 名称 =""
    if ID == 1043 then
     名称 = "空度禅师"
	elseif ID == 1054 then
	 名称 = "程咬金"
	elseif ID == 1117 then
	 名称 = "东海龙王"
	elseif ID == 1137 then
	 名称 = "菩提祖师"
	elseif ID == 1124 then
	 名称 = "地藏王"
	elseif ID == 1112 then
	 名称 = "李靖"
	elseif ID == 1145 then
	 名称 = "牛魔王"
	elseif ID == 1141 then
	 名称 = "观音姐姐"
	elseif ID == 1147 then
	 名称 = "镇元子"
	elseif ID == 1134 then
	 名称 = "大大王"
	elseif ID == 1144 then
	 名称 = "白晶晶"
	elseif ID == 1150 then
	 名称 = "二郎神"
	elseif ID == 1154 then
	 名称 = "巫奎虎"
	elseif ID == 1156 then
	 名称 = "地涌夫人"
	elseif ID == 1143 then
	 名称 = "孙婆婆"
  elseif ID == 1251 then
   名称 = "齐天大圣"
  elseif ID == 1250 then
   名称 = "天女魃"
  elseif ID == 1217 then
   名称 = "小夫子"
	end


	return 名称
end

return 事件解析