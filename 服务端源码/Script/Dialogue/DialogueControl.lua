-- @Author: 作者QQ414628710
-- @Date:   2021-10-30 19:45:20
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-19 01:15:07

--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-06-21 09:56:38
--======================================================================--
local DialogueControl = class()

local format = string.format
local insert = table.insert
local wb,xx
function DialogueControl:初始化()
end
function DialogueControl:数据处理(内容)
	if UserData[内容.数字id].摆摊 or  UserData[内容.数字id].战斗 ~= 0 then
		SendMessage(UserData[内容.数字id].连接id,7,"#Y/摆摊或者战斗中无法进行此操作")
        return 0
	elseif 内容.序号~=3 and UserData[内容.数字id].队伍 ~= 0 and  not UserData[内容.数字id].队长 then
		SendMessage(UserData[内容.数字id].连接id,7,"#Y/只有队长才可进行此操作")
		return 0
	end
	if 内容.序号 == 1 then 
		self:Dialogue(UserData[内容.数字id],内容.参数)
	elseif 内容.序号 == 2 then 
		self:MonsterDialogue(UserData[内容.数字id],内容.参数)
 	elseif 内容.序号 == 3 then---NPC选项解析
		if LinkTaskDialogue[内容.文本] then 
			TaskControl:GetLinkTask(UserData[内容.数字id],LinkTaskDialogue[内容.文本])
		elseif LinkTaskClear[内容.文本] then
			TaskControl:CancelLinkTask(UserData[内容.数字id],LinkTaskClear[内容.文本])
		else 
			DialogueEvent:对话栏(内容.文本,内容.编号,内容.数字id,UserData[内容.数字id].地图)
		end
		
	elseif 内容.序号 == 4 then --刷新怪选项解析
       self:刷新怪选项解析(UserData[内容.数字id],内容.参数,内容.文本)
	elseif 内容.序号 == 5 then
		  MapControl:置传送(内容.数字id,内容.文本)
  elseif 内容.序号 == 6 then
      self:师门执行事件(UserData[内容.数字id],内容.参数,内容.文本)
  elseif 内容.序号 == 7 then
      SendMessage(UserData[内容.数字id].连接id,20,self:单位对话事件(内容.数字id,内容.参数+0,内容.文本))
  elseif 内容.序号 == 8 then   
      TaskControl:触发道士(内容.数字id, 内容.参数, 内容.文本)
  elseif 内容.序号==9 then
       self:执行事件(内容.数字id,内容.文本,内容.参数+0)
  elseif 内容.序号==10 then
         TaskControl:触发宝箱(内容.数字id,内容.参数+0,内容.文本)
  elseif 内容.序号==11 then
          for i=1,#UserData[内容.数字id].角色.子女列表 do
            if 内容.文本 == i..UserData[内容.数字id].角色.子女列表[i].名称 then
                SendMessage(UserData[内容.数字id].连接id,3041,{UserData[内容.数字id].角色.子女列表[i].成长之路,i,UserData[内容.数字id].角色.子女列表[i].名称})
               break
            end
        end
 elseif 内容.序号==12 then
          for i=1,#UserData[内容.数字id].角色.子女列表 do
            if 内容.文本 == i..UserData[内容.数字id].角色.子女列表[i].名称 then
                  RoleControl:Children重置属性点(UserData[内容.数字id],i)
               break
            end
        end


  elseif 内容.序号==13 then
      local  deductMoney =内容.参数*10000
        if 银子检查(内容.数字id, deductMoney) == false then
          SendMessage(UserData[内容.数字id].连接id, 7, "#y/传送到"..内容.参数.."层需要#R/"..deductMoney.."#Y/银子！")

        elseif 内容.参数 >100 then
          SendMessage(UserData[内容.数字id].连接id, 7, "#y/目前最高只能传送到100层")
        elseif 内容.参数 <8 then
          SendMessage(UserData[内容.数字id].连接id, 7, "#y/最低传送到第8层，就这么点路少侠你走过去吧")
        else
            RoleControl:扣除银子(UserData[内容.数字id], deductMoney,"大雁塔传送")
            local JumpPlies=6992+内容.参数
            MapControl:Jump(内容.数字id,JumpPlies,32,37)
        end
 elseif 内容.序号==14 then
          for i=1,#UserData[内容.数字id].召唤兽.数据 do
            if 内容.文本 ==  i..'.'..UserData[内容.数字id].召唤兽.数据[i].名称 then

                    local 找到物品 = false
                    local 特赦格子 = 0
                    for n = 1, 80 do
                    if UserData[内容.数字id].角色.道具["包裹"][n] ~= nil and 找到物品 == false and UserData[内容.数字id].物品[UserData[内容.数字id].角色.道具["包裹"][n]].名称 == "神兜兜" then
                    找到物品 = true
                    特赦格子=n
                    break
                    end
                    end
                    if 找到物品 == false then
                    SendMessage(UserData[内容.数字id].连接id, 20, {"诗中仙","袁天罡","你身上没有神兜兜呀"})
                    elseif UserData[内容.数字id].物品[UserData[内容.数字id].角色.道具["包裹"][特赦格子]].数量 < 25 then  
                    SendMessage(UserData[内容.数字id].连接id, 20, {"诗中仙","袁天罡","你身上神兜兜数量不够"})
                    elseif #UserData[内容.数字id].召唤兽.数据 >= UserData[内容.数字id].召唤兽.数据.召唤兽上限 then
                      SendMessage(UserData[内容.数字id].连接id, 7, "#y/你当前可携带的召唤兽已达上限，无法打开礼包")
                    else
                    ItemControl:RemoveItems(UserData[内容.数字id],特赦格子,"神兜兜",25)
                    local  随机神兽 ={"超级土地公公-法术型","超级六耳猕猴-法术型","超级神鸡-法术型","超级玉兔-法术型","超级神猴-法术型","超级神龙-法术型","超级神羊-法术型","超级孔雀-法术型","超级灵狐-法术型","超级筋斗云-法术型","超级神鸡-物理型","超级玉兔-物理型","超级土地公公-物理型","超级神羊-物理型","超级六耳猕猴-物理型","超级神马-物理型","超级神马-法术型","超级孔雀-物理型","超级灵狐-物理型","超级筋斗云-物理型","超级神龙-物理型","超级麒麟-物理型","超级麒麟-法术型","超级大鹏-法术型","超级大鹏-物理型","超级神蛇-法术型","超级神蛇-物理型","超级赤焰兽-法术型","超级赤焰兽-物理型","超级白泽-法术型","超级白泽-物理型","超级灵鹿-法术型","超级灵鹿-物理型","超级大象-法术型","超级大象-物理型","超级金猴-法术型","超级金猴-物理型","超级大熊猫-法术型","超级大熊猫-物理型","超级泡泡-法术型","超级泡泡-物理型","超级神兔-法术型","超级神兔-物理型","超级神虎-法术型","超级神虎-物理型","超级神牛-法术型","超级神牛-物理型","超级海豚-法术型","超级海豚-物理型","超级人参娃娃-法术型","超级人参娃娃-物理型","超级青鸾-法术型","超级腾蛇-法术型","超级腾蛇-物理型","超级神狗-法术型","超级神狗-物理型","超级神鼠-法术型","超级神鼠-物理型","超级神猪-法术型","超级神猪-物理型","超级孙小圣-法术型","超级孙小圣-物理型","超级小白龙-法术型","超级小白龙-物理型","超级猪小戒-物理型","超级飞天-物理型"}
                    local  随机名称 = 随机神兽[math.random(1, #随机神兽)]
                    UserData[内容.数字id].召唤兽.数据[i] = table.copy(AddPet(随机名称,"神兽"))
                    UserData[内容.数字id].召唤兽:刷新属性(i)
                    SendMessage(UserData[内容.数字id].连接id, 7, "#y/恭喜你兑换到了"..随机名称)
                    SendMessage(UserData[内容.数字id].连接id, 3006, "66")
                    end
               break
            end
        end
 elseif 内容.序号==15 then
    if 内容.文本 == "扩充召唤兽" then  
       local  扩展银子 = 100000000  -- 修改扩充银子
       SendMessage(UserData[内容.数字id].连接id, 20, {UserData[内容.数字id].角色.造型,UserData[内容.数字id].角色.名称,"#y/扩展你的召唤兽栏需要花费#G/"..扩展银子.."#Y/现金,扩展后你的召唤兽栏位将增加1格,确认对你的召唤兽栏进行扩充吗？",{"我确定扩充召唤兽栏","取消"}})
    end 
-- elseif 内容.序号==11 then
--         TaskControl:触发道士(内容.数字id, 内容.编号, 内容.文本)
 elseif 内容.序号==16 then
      TaskControl:触发任务(内容.数字id,内容.参数+0,内容.文本)
	end
end
local function NpcExist(user,num)
  if MapData[user.地图].NPC then
      for k,v in pairs(MapData[user.地图].NPC) do
         if v == num  and   取两点距离(user.角色.地图数据,{x=NpcData[num].X*20,y=NpcData[num].Y*20} ) < 500 then
   
              return true 
         end
      end
  end
  return false 
end
local function 名称取门派(名称)
  local 师门 
  if  名称 == "空度禅师" then
    师门 = "化生寺"
  elseif  名称 == "程咬金" then
    师门 = "大唐官府"
  elseif 名称 == "东海龙王" then
    师门 = "龙宫"
  elseif 名称 == "菩提祖师" then
    师门 = "方寸山"
  elseif 名称 == "地藏王" then
    师门 = "阴曹地府"
  elseif 名称 == "李靖" then
    师门 = "天宫"
  elseif 名称 == "牛魔王" then
    师门 = "魔王寨"
  elseif 名称 == "观音姐姐" then
    师门 = "普陀山"
  elseif 名称 == "镇元子" then
    师门 = "五庄观"
  elseif 名称 == "大大王" then
    师门 = "狮驼岭"
  elseif 名称 == "白晶晶" then
    师门 = "盘丝洞"
  elseif 名称 == "二郎神" then
    师门 = "凌波城"
  elseif 名称 == "巫奎虎" then
    师门 = "神木林"
  elseif 名称 == "地涌夫人" then
    师门 = "无底洞"
  elseif 名称 == "孙婆婆" then
    师门 = "女儿村"
  elseif 名称 == "齐天大圣" then
    师门 = "花果山"
  elseif 名称 == "天女魃" then
    师门 = "女魃墓" 
  elseif 名称 == "小夫子" then
    师门 = "天机城"
  end
  return 师门
end
function DialogueControl:师门执行事件(user,num,事件)
  if NpcData[num] and NpcExist(user,num) then
     local 名称 =NpcData[num].名称
    if 事件 == "拜师" then
      local mp = 名称取门派(NpcData[num].名称)
      local ky = false
      for i,v in pairs(取属性(user.角色.造型)["门派"]) do
        if v == mp then
          ky = true
        end
      end
      if  DebugMode  or Config.门派限制=="无" then
        ky=true
      end

      if ky  then
        RoleControl:加入门派(user,mp,名称)
      else
        SendMessage(user.连接id,20,{名称,名称,format("你不适合修习%s的法术，请回吧",mp)})
      end
    elseif 事件 == "学习技能" then
      local mp = 名称取门派(名称)
      if user.角色.门派 == mp then
            SendMessage(user.连接id,2027,RoleControl:刷新技能学习(user))

          elseif user.角色.门派 == "无" then
        SendMessage(user.连接id,20,{名称,名称,"拜入门派才可以学习师门技能"})
      else
        SendMessage(user.连接id,20,{名称,名称,"你不是本门派的弟子不能学习"})
      end
    elseif 事件 == "脱离门派" then
      local x ={"确认脱离门派","我在考虑一下"}
        SendMessage(user.连接id,20,{名称,名称,"脱离门派需要100万的银子,确认脱离吗?",x})
      elseif 事件 == "学习乾元丹" then
         if user.角色.等级 < 69 then
        SendMessage(user.连接id,20,{名称,名称,"当前等级不足69无法学习乾元丹!"})
      else
        SendMessage(user.连接id, 2017, RoleControl:兑换乾元丹数据(user))
      end
      elseif 事件 == "兑换奖励" then
        if user.角色.门贡 < 50 then
        SendMessage(user.连接id,20,{名称,名称,"需要50点门派贡献,你没有那么多的门派贡献度"})
      else
        ItemControl:门贡兑换(user.id)
      end
      elseif 事件 == "兑换潜能果" then
        if user.角色.等级 < 69 then
        SendMessage(user.连接id,20,{名称,名称,"当前等级不足69无法兑换潜能果!"})
      else
         SendMessage(user.连接id, 2017, {潜能果=user.角色.潜能果,当前经验=user.角色.当前经验,状态="潜能果"})
      end
    elseif  事件 == "给予"  then
        if  RoleControl:GetTaskID(user,"押镖")~=0 and 任务数据[RoleControl:GetTaskID(user,"押镖")].NPC==num then
           SendMessage(user.连接id,3011,{名称=名称 ,id=RoleControl:GetTaskID(user,"押镖"),道具=ItemControl:索要道具1(user.id,"包裹")})
        elseif  RoleControl:GetTaskID(user,"师门")~=0 then
           SendMessage(user.连接id,3011,{名称=名称 ,id=RoleControl:GetTaskID(user,"师门"),道具=ItemControl:索要道具1(user.id,"包裹")})
        else
          SendMessage(user.连接id,20,{名称,名称,"你当前没有任何任务,别乱点哦!"})
        end
    elseif  事件 == "上交召唤兽"  then
      local zhs = 0
      for n=1,#user.召唤兽.数据 do
        if user.召唤兽.数据[n] ~= nil and n ~= user.召唤兽.数据.参战  and user.召唤兽.数据[n].造型 == 任务数据[RoleControl:GetTaskID(user,"师门")].召唤兽  and user.召唤兽.数据[n].名称 == 任务数据[RoleControl:GetTaskID(user,"师门")].召唤兽 then
          zhs = n
        end
      end
      if zhs == 0 then
          SendMessage(user.连接id,20,{名称,名称,"你身上没有我要的召唤兽,请勿将召唤兽改名或者设置参战"})
      else
        table.remove(user.召唤兽.数据,zhs)
              TaskControl:完成师门任务(user.id)
      end
    elseif 事件 == "取消任务" then
      if RoleControl:GetTaskID(user,"师门")==0 then
      SendMessage(user.连接id,20,{名称,名称,"#Y/请先领取师门任务"})
      else
      SendMessage(user.连接id,20,{名称,名称,"#Y/你取消了师门任务"})
      RoleControl:取消任务(user,RoleControl:GetTaskID(user,"师门"))
      任务数据[RoleControl:GetTaskID(user,"师门")]=nil
      user.角色.师门环数 =0
      end
    elseif 事件 == "开始挑战"  then
          if user.队伍==0  then
          SendMessage(user.连接id,7,"#y/你的队伍呢？")
          return 0
          end
          local  全部通过=true
          for n=1,#队伍数据[user.队伍].队员数据 do
            if RoleControl:GetTaskID(UserData[队伍数据[user.队伍].队员数据[n]],"群雄逐鹿")~=RoleControl:GetTaskID(user,"群雄逐鹿") then
            广播队伍消息(user.id,7,"#y/"..UserData[队伍数据[user.队伍].队员数据[n]].角色.名称.."当前任务与队长任务不相符")
              全部通过=false
            end
          end
          if 全部通过 then
          FightGet:进入处理(user.id,100064,"66",RoleControl:GetTaskID(user,"群雄逐鹿"))
          end
    elseif 事件 == "师门任务" then
          if RoleControl:GetTaskID(user,"师门")~=0 then
           SendMessage(user.连接id,7,"#y/你已经领取过此任务了")
          elseif user.角色.等级<20 then
           SendMessage(user.连接id,7,"#y/角色等级达到20级才可领取此任务")
          elseif user.角色.人气<600 then
           SendMessage(user.连接id,7,"#y/你的人气低于600点，无法领取师门任务")
          elseif 活动数据.师门名单[user.id]~=nil and 活动数据.师门名单[user.id]<=0 then
            SendMessage(user.连接id,7,"#y/你今天的师门次数已经耗尽")
          else
              local mp = 名称取门派(名称)
              if user.角色.门派 ~= mp then
              SendMessage(user.连接id,20,{名称,名称,"你跟本派没有任何关联，请回吧"})
              return false
              end
              if 活动数据.师门名单[user.id]==nil then
              活动数据.师门名单[user.id]=20
              end
              if RoleControl:GetTaskID(user,"法宝") == 0 and math.random(100)<=2 and user.角色.等级>=60 then
                    user.法宝 =true
              SendMessage(user.连接id,20,{名称,名称,"恭喜你激活了一个法宝任务,是否领取该任务呢?",{"领取法宝任务","我不需要法宝"}})
              else
              活动数据. 师门名单[user.id]=活动数据.师门名单[user.id]-1
              TaskControl:设置师门任务(user.id,num)
              SendMessage(user.连接id,7,"#y/你本日还可以领取"..活动数据.师门名单[user.id].."次师门任务")
              end
           end
    else 
       SendMessage(user.连接id,20,{NpcData[num].模型,NpcData[num].名称,NpcData[num].对话[math.random(1,#NpcData[num].对话)]})
    end
  end
end
function DialogueControl:刷新怪选项解析(user,num,Text)
 local TempID =MapControl.MapData[user.地图].单位组[num]
       if TempID and 取两点距离(user.角色.地图数据,{x=TempID.X*20,y=TempID.Y*20} ) < 500 then
         if 任务数据[TempID.任务id] then
             local ExeclData=RefreshMonsterData[任务数据[TempID.任务id].类型]
              if Text=="仙玉抽奖" then 
                  if 任务数据[TempID.任务id].造型 == "乌铁盒" then
                     ItemControl:仙玉抽奖(user.id,math.random(10,12),ExeclData.等级差)
                  elseif 任务数据[TempID.任务id].造型 == "黄铜箱" then
                     ItemControl:仙玉抽奖(user.id,math.random(20,22),ExeclData.等级差)
                  elseif 任务数据[TempID.任务id].造型 == "白银匣" then
                     ItemControl:仙玉抽奖(user.id,math.random(30,32),ExeclData.等级差)
                  elseif 任务数据[TempID.任务id].造型 == "赤金宝箱" then
                     ItemControl:仙玉抽奖(user.id,math.random(40,42),ExeclData.等级差)
                  end
                  MapControl:移除单位(user.地图,TempID.任务id)
                  任务数据[TempID.任务id]=nil
              elseif Text=="BOSS" or Text==ExeclData.选项[1]   then
                  if 任务数据[TempID.任务id].战斗==false then
                      if ExeclData.人数 then--组队任务
                          if user.队伍 == 0 then
                               SendMessage(user.连接id, 7, "#y/挑战我需要组队哦")
                              return
                          elseif #队伍数据[user.队伍].队员数据 + #UserData[user.id].助战.数据 < ExeclData.人数 and DebugMode == false then
                              -- 广播队伍消息(user.id, 7, string.format("#y/挑战我需要队伍成员数达到%d人",ExeclData.人数))
                              广播队伍消息(user.id, 7, string.format("#y/挑战我需要队伍成员数%d加上助战数%d达到%d人",#队伍数据[user.队伍].队员数据 , #UserData[user.id].助战.数据 ,ExeclData.人数))
                              return
                           elseif ExeclData.等级差 then 
                               --- 判断上下20
                                  local MaxLv,MinLv,Prompt=0,175,""
                                  for n = 1, #队伍数据[UserData[user.id].队伍].队员数据 do
                                    if  UserData[队伍数据[UserData[user.id].队伍].队员数据[n]].角色.等级 > MaxLv then
                                      MaxLv = UserData[队伍数据[UserData[user.id].队伍].队员数据[n]].角色.等级
                                    end
                                    if UserData[队伍数据[UserData[user.id].队伍].队员数据[n]].角色.等级 < MinLv then
                                        MinLv = UserData[队伍数据[UserData[user.id].队伍].队员数据[n]].角色.等级
                                    end
                                 end

                                  if MinLv ~= MaxLv and math.abs(MaxLv - MinLv) >= ExeclData.等级差  then
                                  Prompt = "#y/队伍中存在等级差距达到"..ExeclData.等级差.."级的玩家"
                                  end
                                  if Prompt ~= ""  then
                                      SendMessage(user.连接id, 7, Prompt)
                                      return 
                                  end
        
                          end
                      else --单人任务
                          if user.队伍 ~= 0 then
                               SendMessage(user.连接id, 7, "#y/挑战本妖怪需要单人完成")
                              return
                          end
                      end
                      if ExeclData.等级限制 and DebugMode == false then
                         if  not 取队伍符合等级(user.id, ExeclData.等级限制[1])  then
                          广播队伍消息(user.id, 7, string.format("#y/挑战本妖怪需要队伍所有成员等级达到%d级",ExeclData.等级限制[1]))
                          return 
                         elseif not 取队伍高于等级(user.id, ExeclData.等级限制[2]) then
                          广播队伍消息(user.id, 7, string.format("#y/挑战本妖怪需要队伍所有成员等级低于%d级",ExeclData.等级限制[2]))
                          return
                         end 
                      end    

                      任务数据[TempID.任务id].战斗 = true
                      MapControl:NPC更改战斗(任务数据[TempID.任务id].地图编号,TempID.任务id)

                      FightGet:LoadMonster(user,TempID.任务id,ExeclData.怪物,1)
                  else
                    SendMessage(user.连接id,20,{TempID.模型,TempID.名称,"我正在战斗中，请勿扰。"})
                  end             
              end
         else 
               SendMessage(user.连接id,7,"#Y/当前单位任务数据不存在")
         end
       else 
          SendMessage(user.连接id,7,"#Y/当前单位不存在，请重新加载地图")
       end  
end 
function DialogueControl:MonsterDialogue(user,num)  
  local TempID =MapControl.MapData[user.地图].单位组[num]
     if TempID and 取两点距离(user.角色.地图数据,{x=TempID.X*20,y=TempID.Y*20} ) < 500 then
       if 任务数据[TempID.任务id] then
         local ExeclData=RefreshMonsterData[任务数据[TempID.任务id].类型]
           if 任务数据[TempID.任务id].战斗==false then
              if ExeclData.对话[1] =="BOSS" then
                SendMessage(user.连接id,1021,{num,"BOSS"})
              else 
                SendMessage(user.连接id,20,{TempID.模型,TempID.名称,ExeclData.对话[math.random(1,#ExeclData.对话)],ExeclData.选项,"刷新",num})
              end 
           else
              SendMessage(user.连接id,20,{TempID.模型,TempID.名称,"我正在战斗中，请勿扰。"})
           end
       else 
             SendMessage(user.连接id,7,"#Y/当前单位任务数据不存在")
       end
     else 
        SendMessage(user.连接id,7,"#Y/当前单位不存在，请重新加载地图")
     end       
end

function DialogueControl:Dialogue(user,num)
  if NpcData[num] and NpcExist(user,num) then
    local MsgData = NpcData[num]
    ------------------副本
      if user.副本~=0 and 任务数据[user.副本] and 取副本地图(user.地图)then
         if 任务数据[user.副本].类型 =="乌鸡"  then
             if num==313101 then 
                if 任务数据[user.副本].进度<3 then
                   SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"请先完成之前的副本任务。"})
                else 
                   SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"那妖怪带领了一帮手下冒充我占据了皇宫，还请你们帮我赶走他们。",{"送我进去","有妖怪？怕怕，我先跑你殿后"}})
                end
            elseif num==313205 or num==313206 then
                if not 任务数据[user.副本].国王记录 then 
                     SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"请完成前置任务"})
                elseif 任务数据[user.副本].国王记录[num -313204] then
                        SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"这个国王的身份你们不是知晓了么"})   
                else 
                    FightGet:进入处理(user.id, 8006 + num -313204, "66", user.副本)
                end
            elseif num==313201 or num==313202 or num==313203 then
                  if 任务数据[user.副本].三妖记录[num-313200] then
                      SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"这个妖怪已经被你们打败了，难道还要鞭尸么？"})
                  else 
                     FightGet:进入处理(user.id, 8002 + num-313200, "66", user.副本)
                  end
            end
           
         end
         return 
      end
      ---------------------任务
      for n, v in pairs(user.角色.任务数据) do
        if 任务数据[v] and 任务数据[v].NPC and 任务数据[v].NPC == num then
            if  LinkTask[任务数据[v].类型] then 
                local 临时进程 = 任务数据[v].进程
                local option=""
                if LinkTask[任务数据[v].类型]["进程"..临时进程][1] == "需要物品" then
                   option=string.format("给予道具(%s)",任务数据[v].类型)
                elseif LinkTask[任务数据[v].类型]["进程"..临时进程][1] == "需要找人" then
                   option=string.format("好的，我帮你。(%s)",任务数据[v].类型)
                elseif LinkTask[任务数据[v].类型]["进程"..临时进程][1] == "NPC战斗" then
                    option=string.format("开始挑战(%s)",任务数据[v].类型)
                end
                  SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,LinkTask[任务数据[v].类型]["进程"..临时进程][4],{option,"我只是随便看看"},任务数据[v].类型,num})
            elseif 任务数据[v].类型=="群雄逐鹿" then--完成
                SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"我可是门派的师傅，想要挑战我，你可想好了！",{"开始挑战","取消"},MsgData.事件,num})
            elseif 任务数据[v].类型=="法宝" then --完成
                SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"想抢我法宝门都没用#4,来吧我不怕你!",{"谁怕谁开打","取消"},任务数据[v].类型,num})

            elseif 任务数据[v].类型=="神器任务" then--完成
              if  任务数据[v].进程==1 then
                  SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"准备一个#R/醉生梦死#w/找酒店的店小二交换线索吧"})
                  TaskControl:完成神器任务(user.id,v)
              elseif  任务数据[v].进程==2 then
                  SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"为兄,到底发生饿了什么，我一定会调查清楚的，一路走好，来世再与你对酒当歌吧。",{"给予道具","取消"},任务数据[v].类型,num})
              elseif  任务数据[v].进程==3 then
                  SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"来人，拿下~",{"牛大哥你发彪啊（切入战斗）","还是别打了·······"},任务数据[v].类型,num})
              elseif  任务数据[v].进程==4 then
                  SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"来人，这里有轩辕同党",{"只好先用拳头说话了（切入战斗）","还是别打了·······"},任务数据[v].类型,num})
              elseif  任务数据[v].进程==5 then
                 SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"听说各大门派已经找到轩辕踪迹，围追堵截，老夫刚刚感受到大唐官府剑冢中有强大的力量发出，恐怕双方已经交战了！少侠若愿意前去，老夫可送你一程。",{"前往剑冢","让我想想"},任务数据[v].类型,num})
                 TaskControl:完成神器任务(user.id,v)
              elseif  任务数据[v].进程==6 then
                  
                SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"我，只差这一步了！",{"除非你先打败我（切入战斗）","等下···在让我说两句"},任务数据[v].类型,num})
              end
            elseif 任务数据[v].类型=="宝宝任务链" then--完成
                SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,MsgData.对话[math.random(1,#MsgData.对话)],{"我来完成宝宝任务链","取消"},任务数据[v].类型,num})
            elseif 任务数据[v].类型=="任务链" then--完成
                SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,MsgData.对话[math.random(1,#MsgData.对话)],{"我来完成任务链","取消"},任务数据[v].类型,num})
            elseif 任务数据[v].类型=="嘉年华" then--完成
                if 任务数据[v].进程==1 then
                  SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"本商人可没有骗过谁，你可不要冤枉我。",{"切入战斗","取消"},任务数据[v].类型,num})
                elseif 任务数据[v].进程==2 then
                  SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"想不到朝廷居然还记得我这么一个老头子，我真是太感动了。",{"领取奖励","取消"},任务数据[v].类型,num})
                elseif 任务数据[v].进程==3 then
                  SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"作为一只猪，尤其是猪的王者，就要好吃懒做。劳动？不存在的，这辈子都不可能劳动的。。",{"切入战斗","取消"},任务数据[v].类型,num})
                elseif 任务数据[v].进程==4 then
                  SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"想要教训我？得先看看你拳头有没有石头那么硬了。",{"切入战斗","取消"},任务数据[v].类型,num})
                end
            elseif 任务数据[v].类型=="坐骑" then --完成
                if 任务数据[v].进程==1 then 
                   TaskControl:完成坐骑任务1(user.id)
                   SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"我真的马儿不知道跑哪里去了，天上事少侠还得从天上查起哦，天宫的顺风耳听的远，知的多，你去找他问问吧"})
                elseif  任务数据[v].进程==2 then
                  SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"你帮我找一个#R"..任务数据[RoleControl:GetTaskID(user,"坐骑")].道具.."#W和我交换线索吧",{"上交材料","取消"},任务数据[v].类型,num})
                elseif  任务数据[v].进程==3 then
                     SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"#4什么?,千里眼,那丫的还欠我钱呢,火大!让你走,你不走,我打你走",{"岂有此理,谁怕谁","取消"},任务数据[v].类型,num})
                elseif  任务数据[v].进程==4 then
                  SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"#28终于被你发现了,除非你能打赢我,我才会吧坐骑消息告诉你",{"开打","取消"},任务数据[v].类型,num})
                elseif  任务数据[v].进程==5 then
                  SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"恭喜你完成了任务,请领走你的坐骑",{"领取坐骑","取消"},任务数据[v].类型,num})
                end
            elseif 任务数据[v].类型=="师门" then --完成
                  SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,MsgData.对话[math.random(1,#MsgData.对话)],{"交谈","给予","学习技能","学习乾元丹","兑换潜能果","兑换奖励","上交召唤兽","取消任务"},MsgData.事件,num})
            elseif 任务数据[v].类型=="押镖" then --完成
                  SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,MsgData.对话[math.random(1,#MsgData.对话)],{"交谈","给予"},MsgData.事件,num})
            elseif 任务数据[v].类型=="游泳" then    --完成   
                   SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"你是否要在我这里报道？。",{"我要报道","取消"},任务数据[v].类型,num})
            elseif 任务数据[v].类型=="打造" then  --完成
                 SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"少侠你是否已经准备好了强化石？接下去的事交给我吧",{"给予强化石","取消"},任务数据[v].类型,num})
                         
            end
            return
        end
      end
      if  user.队伍 ~= 0 and MsgData.队伍  then
        for k,v in pairs(队伍数据[user.队伍].队员数据) do
          SendMessage(UserData[v].连接id,20,{MsgData.模型,MsgData.名称,MsgData.对话[math.random(1,#MsgData.对话)],MsgData.选项,MsgData.事件,num})
        end
        return
      end
      ----------NPC
      if MsgData.事件=="门派师傅" then
        if user.角色.门派 ==名称取门派(MsgData.名称) then
          SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,MsgData.对话[math.random(1,#MsgData.对话)],{"交谈","给予","师门任务","学习技能","脱离门派","学习乾元丹","兑换潜能果","兑换奖励"},MsgData.事件,num})
        elseif user.角色.门派 == "无" then
          SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,MsgData.对话[math.random(1,#MsgData.对话)],{"交谈","给予","拜师"},MsgData.事件,num})
        else 
            SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"非本门弟子不要来捣乱！"})
        end
      elseif MsgData.名称=="门派闯关使" and  门派闯关开关 then        
           SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,"欢迎参加门派闯关竞赛活动，你是否已经叫齐伙伴准备闯关了？",{"准备好了，请告诉我们第一关的挑战地点","我来取消任务","我只是来凑凑热闹"},MsgData.事件,num})
      else 
        SendMessage(user.连接id,20,{MsgData.模型,MsgData.名称,MsgData.对话[math.random(1,#MsgData.对话)],MsgData.选项,MsgData.事件,num})
      end
  end 
end
function DialogueControl:执行事件(id,事件,标识)
  if UserData[id]==nil then
  SendMessage(UserData[id].连接id,7,"#Y/该单位当前不存在,错误代号rw2")
    return 0
  elseif UserData[id].队伍~=0 and UserData[id].队长==false then
  SendMessage(UserData[id].连接id,7,"#Y/只有队长才可进行此操作")
    return 0
  elseif MapControl.MapData[UserData[id].地图].单位组[标识]==nil then
  SendMessage(UserData[id].连接id,7,"#Y/该单位当前不存在,错误代号rw1")
  return 0

  elseif 取两点距离(UserData[id].角色.地图数据,{x=MapControl.MapData[UserData[id].地图].单位组[标识].X*20,y=MapControl.MapData[UserData[id].地图].单位组[标识].Y*20} ) > 500  then 
       
      return 0
  end
  local 任务id=MapControl.MapData[UserData[id].地图].单位组[标识].任务id
 
  if 任务数据[任务id]==nil then
    SendMessage(UserData[id].连接id,7,"#Y/该单位当前不存在")
    return 0
  else
    if 事件 == "缉拿海盗" then
      TaskControl:触发单人任务(id,标识,"除暴安良")
    elseif 事件 == "送你回地府" then
      TaskControl:触发任务(id,标识,"抓鬼")
      elseif 事件 == "送你上西天" then
      TaskControl:触发任务(id,标识,"鬼王")
      elseif 事件 =="战就战谁怕谁啊" then
      TaskControl:触发任务(id,标识,"天罡星69")
      elseif 事件 =="战就战谁怕谁哦" then
      TaskControl:触发任务(id,标识,"天罡星2")
      elseif 事件 =="战就战谁怕谁哇" then
      TaskControl:触发任务(id,标识,"天罡星3")
       elseif 事件 =="战就战谁怕谁勒" then
      TaskControl:触发任务(id,标识,"天罡星4")
      elseif 事件 =="战就战谁怕谁呀" then
      TaskControl:触发任务(id,标识,"天罡星5")
      elseif 事件 =="战就战谁怕谁哟" then
      TaskControl:触发任务(id,标识,"天罡星6")
  
    elseif 事件 =="妖星受死吧" then
     TaskControl:触发任务(id,标识,"地煞")
    elseif 事件 =="把藏宝图交出来" then
     TaskControl:触发单人任务(id,标识,"宝图")
    elseif 事件 =="摇一摇" then
      ItemControl:摇钱树奖励(id, 标识)
    elseif 事件 =="摇一下" then
      ItemControl:梦幻瓜子奖励(id, 标识)
    elseif 事件 =="我是奉命来捉拿你的" then
     TaskControl:触发单人任务(id,标识,"官职")
    elseif 事件 == "请接收物资" then
     TaskControl:完成官职押送任务(id, 标识)
    elseif 事件 =="战就战谁怕谁" then
     TaskControl:触发任务(id,标识,"天罡")
    elseif 事件 =="快交出赃物" then
     TaskControl:触发任务(id,标识,"飞贼")
    elseif 事件 == "不服你就干" then
    TaskControl:触发封妖任务(id, 标识)
    elseif 事件 =="参加首席争霸赛" then
      if 首席争霸赛进场开关 == false then
        SendMessage(UserData[id].连接id, 7, "#y/进场时间为每日的18:50-19:00")
      elseif UserData[id].角色.门派 ~= 任务数据[任务id].门派 then
        SendMessage(UserData[id].连接id, 7, "#y/你不是本门派弟子，瞎凑什么热闹")
      elseif UserData[id].角色.等级 < 60 then
        SendMessage(UserData[id].连接id, 7, "#y/想要成为本门派首席弟子，必须等级达到60级")
      elseif UserData[id].队伍 ~= 0 then
        SendMessage(UserData[id].连接id, 7, "#y/首席争霸赛不允许组队进入")
      else
        MapControl:Jump(id, 5135, 61, 24)
      end
    elseif 事件 =="辱我师门、跟你拼了" then
       TaskControl:触发任务(id,标识,"师门守卫")
    elseif 事件 =="放马过来" then
      TaskControl:触发门派闯关任务(id, 标识)
    elseif 事件 =="领取首席弟子称谓" then
      if UserData[id].角色.门派 == 任务数据[任务id].门派 then
        if UserData[id].id == 首席资源数据[任务数据[任务id].门派编号].玩家id then
          TaskControl:更换首席弟子(id, 任务id)
          return 0
        else
          SendMessage(UserData[id].连接id, 7, "#y/你似乎还没有成为本门派首席弟子")
          return 0
        end
      end
    elseif 事件 =="让我好好地教训你" then
       TaskControl:触发玄武堂任务(id,标识)
    elseif 事件 == "打得你变回木头" then
      TaskControl:触发芭蕉木妖(id, 标识)
    elseif 事件 == "多谢仙人相助" then
    TaskControl:触发热心仙人(id, 标识)
    elseif 事件 == "做你的春秋大梦去" then
       TaskControl:触发鬼祟小妖(id, 标识)
    elseif 事件 =="兑换奖励" then
    elseif 事件 == "离开本层迷宫" then
        if 任务数据[MapControl.MapData[UserData[id].地图].单位组[标识].id].真假 then
          self.临时坐标 =   MapControl:Randomloadtion(1193) 
          MapControl:Jump(id, UserData[id].地图 + 1, math.floor(self.临时坐标.x / 20), math.floor(self.临时坐标.y / 20))
        else
          self.临时坐标 = MapControl:Randomloadtion(1193) 
          MapControl:Jump(id, UserData[id].地图 - 1, math.floor(self.临时坐标.x / 20), math.floor(self.临时坐标.y / 20))
        end
        if UserData[id].队伍 == 0 then
          if math.random(100) <= 20 and 幻域迷宫数据[UserData[id].id].完成 == false and 幻域迷宫数据[UserData[id].id].层数[UserData[id].地图] == nil then
            if math.random(100) <= 50 then
              ItemControl:GiveItem(id, "飞行卷", "功能", 0)
              SendMessage(UserData[id].连接id, 7, "#y/你获得了飞行卷")
            else
              ItemControl:GiveItem(id, "如意棒", "功能", 0)
              SendMessage(UserData[id].连接id, 7, "#y/你获得了如意棒")
            end
          end
          幻域迷宫数据[UserData[id].id].层数[UserData[id].地图] = 1
        else
          for n = 1, #队伍数据[UserData[id].队伍].队员数据 do
            if math.random(100) <= 20 and 幻域迷宫数据[UserData[队伍数据[UserData[id].队伍].队员数据[n]]].完成 == false and 幻域迷宫数据[UserData[队伍数据[UserData[id].队伍].队员数据[n]]].层数[UserData[id].地图] == nil then
              if math.random(100) <= 50 then
                ItemControl:GiveItem(队伍数据[UserData[id].队伍].队员数据[n], "飞行卷")
                SendMessage(UserData[队伍数据[UserData[id].队伍].队员数据[n]].连接id, 7, "#y/你获得了飞行卷")
              else
                ItemControl:GiveItem(队伍数据[UserData[id].队伍].队员数据[n], "如意棒")
                SendMessage(UserData[队伍数据[UserData[id].队伍].队员数据[n]].连接id, 7, "#y/你获得了如意棒")
              end
            end

            幻域迷宫数据[UserData[队伍数据[UserData[id].队伍].队员数据[n]]].层数[UserData[id].地图] = 1
          end
        end
    elseif 事件 == "胡说，你就是贡品" then
        TaskControl:触发贡品(id, 标识)
    elseif 事件 == "是天尊也照打不误" then
      TaskControl:触发三清天尊(id, 标识)
    elseif 事件 == "动你大爷" then
      TaskControl:触发双不动(id, 标识)
   elseif 事件 == "妖孽休要狂妄" then
      TaskControl:触发三妖大仙(id, 标识)

    end
  end
end
function DialogueControl:单位对话事件(id,标识,类型) ----刷新怪物对话
  if UserData[id]==nil then
    return {"野鬼","系统","该单位当前不存在,错误代号rw2"}
  elseif UserData[id].队伍~=0 and UserData[id].队长==false then
        return {UserData[id].角色.造型,UserData[id].角色.名称,"只有队长才可进行此操作"}
  elseif MapControl.MapData[UserData[id].地图].单位组[标识]==nil then
    return {UserData[id].角色.造型,UserData[id].角色.名称,"该单位当前不存在,错误代号rw1"}
 elseif 取两点距离(UserData[id].角色.地图数据,{x=MapControl.MapData[UserData[id].地图].单位组[标识].X*20,y=MapControl.MapData[UserData[id].地图].单位组[标识].Y*20} ) > 500  then 
      
      return 0
  end
   local 任务id=MapControl.MapData[UserData[id].地图].单位组[标识].任务id

   if 任务数据[任务id]==nil then
      return {UserData[id].角色.造型,UserData[id].角色.名称,"该单位当前不存在"}
    else
        local 造型 = 任务数据[任务id].造型
        local 名称 = 任务数据[任务id].名称
        local wb
        local xx
        if 类型=="除暴安良" then --除暴安良
            if 任务数据[任务id].数字id==UserData[id].id then
             wb= "老子要把这东海湾抢个天昏地暗。"
             xx = {"缉拿海盗","我是路过的"}
            else
            wb= "看什么看，本大爷又不认识你。"
            end
        elseif 类型=="抓鬼" then --抓鬼
            if 任务数据[任务id].战斗==false then
              local 符合id=false
                for n=1,#任务数据[任务id].数字id do
                  if 任务数据[任务id].数字id[n]==UserData[id].id then
                     符合id=true
                  end
                end
              if 符合id then
                wb= "好不容易从地府里逃出来，得在这人间好好潇洒一回。"
                xx = {"送你回地府","我是路过的"}
              else
                wb= "你是什么人？我认识你吗？"
              end
           else
              wb="我正在战斗中，请勿扰。"
           end
        elseif 类型=="鬼王" then --鬼王
            if 任务数据[任务id].战斗==false then
              local 符合id=false
                for n=1,#任务数据[任务id].数字id do
                  if 任务数据[任务id].数字id[n]==UserData[id].id then
                     符合id=true
                  end
                end
              if 符合id then
                wb= "好不容易从地府里逃出来，得在这人间好好潇洒一回。"
                xx = {"送你上西天","我是路过的"}
              else
                wb= "你是什么人？我认识你吗？"
              end
           else
              wb="我正在战斗中，请勿扰。"
           end
          elseif 类型=="天罡星69" then --妖魔
          if 任务数据[任务id].战斗==false then
           wb= " 我乃精锐星幻影,少侠可否与我一战？"
           xx = {"战就战谁怕谁啊","我先准备一下吧"}
          else
           wb="我正在战斗中，请勿扰。"
          end
          elseif 类型=="天罡星2" then --妖魔
          if 任务数据[任务id].战斗==false then
           wb= " 我乃勇武星幻影,少侠可否与我一战？"
           xx = {"战就战谁怕谁哦","我先准备一下吧"}
          else
           wb="我正在战斗中，请勿扰。"
          end
           elseif 类型=="天罡星3" then --妖魔
          if 任务数据[任务id].战斗==false then
           wb= " 我乃神威星幻影,少侠可否与我一战？"
           xx = {"战就战谁怕谁哇","我先准备一下吧"}
          else
           wb="我正在战斗中，请勿扰。"
          end
          elseif 类型=="天罡星4" then --妖魔
          if 任务数据[任务id].战斗==false then
           wb= " 我乃天科星幻影,少侠可否与我一战？"
           xx = {"战就战谁怕谁勒","我先准备一下吧"}
          else
           wb="我正在战斗中，请勿扰。"
          end
          elseif 类型=="天罡星5" then --妖魔
          if 任务数据[任务id].战斗==false then
           wb= " 我乃天启星幻影,少侠可否与我一战？"
           xx = {"战就战谁怕谁呀","我先准备一下吧"}
          else
           wb="我正在战斗中，请勿扰。"
          end
          elseif 类型=="天罡星6" then --妖魔
          if 任务数据[任务id].战斗==false then
           wb= " 我乃天元星幻影,少侠可否与我一战？"
           xx = {"战就战谁怕谁哟","我先准备一下吧"}
          else
           wb="我正在战斗中，请勿扰。"
          end

        elseif 类型=="地煞" then --地煞
          if 任务数据[任务id].战斗==false then
            wb=[[ 我的难度级别是#R/]]..任务数据[任务id].难度..[[#W/星。难度越高奖励也会越高哟。#R/(本玩法为高难度挑战型，请勿轻易尝试)#W/]]
            xx={"妖星受死吧","后会有期"}
          else
          wb=[[ 我正在战斗中，请勿扰。]]
          end
        elseif 类型=="宝图" then  --藏宝图
          if 任务数据[任务id].数字id==UserData[id].id then
           xx = {"把藏宝图交出来","我是路过的"}
           wb= "这藏宝图可得好好藏着啦，千万别给那些坏人夺了去。"
          else
           wb=[[ 看什么看，本大爷又不认识你。]]
          end
        elseif 类型=="官职" and 任务数据[任务id].分类==1 then --官职飞贼任务
            if 任务数据[任务id].数字id==UserData[id].id then
            wb=[[ 本大爷今天要喝好、玩好、抢好。。]]
            xx={"我是奉命来捉拿你的","大爷您玩的开心"}
            else
            wb=[[ 看什么看，本大爷又不认识你。]]
            end
        elseif 类型=="官职" and 任务数据[任务id].分类==2 then --官职飞贼任务
           if 任务数据[任务id].数字id==UserData[id].id then
            wb=[[ 战事越来越激烈，我军物资匮乏，后方的补给还没跟上。这可如何是好，难道我大唐要亡了？]]
            xx={"请接收物资","我是一路过的"}
            else
            wb=[[ 本官不想和你说话，并朝你脸上吐了把口水。]]
            end
        elseif 类型=="师门守卫" then --师门守卫
          if 任务数据[任务id].门派==UserData[id].角色.门派 then
          wb=[[ 毫不客气地说，这个门派的人都要叫我爷爷！]]
          xx={"辱我师门、跟你拼了","爷爷好"}
          else
          wb=[[ 你是何方神圣？请不要挡我的财路。]]
          end

        elseif 类型=="天罡" then --天罡
          if 任务数据[任务id].战斗==false then
            wb=[[ 我的难度级别是#R/]]..任务数据[任务id].难度..[[#W/星。难度越高奖励也会越高哟。#R/(本玩法为高难度挑战型，请勿轻易尝试)#W/]]
            xx={"战就战谁怕谁","后会有期"}
          else
          wb=[[ 我正在战斗中，请勿扰。]]
          end
        elseif 类型=="摇钱树" then --摇钱树
          if 任务数据[任务id].数字id==UserData[id].id then
          wb=[[ 您当前可用摇动#R/]]..任务数据[任务id].次数..[[#W/次摇钱树，每次摇动都会有意外惊喜。]]
          xx={"摇一摇","取消"}
          else
          wb=[[ 这可不是你的摇钱树哦。]]
          end
        elseif 类型=="梦幻瓜子" then --梦幻瓜子
          if 任务数据[任务id].数字id==UserData[id].id then
          wb=[[ 您当前可用摇动#R/]]..任务数据[任务id].次数..[[#W/次梦幻瓜子，每次摇动都会有意外惊喜。]]
          xx={"摇一下","取消"}
          else
          wb=[[ 这可不是你的梦幻瓜子哦。]]
          end
        elseif 类型=="门派首席" then
          if UserData[id].角色.门派==任务数据[任务id].门派  then
          wb=[[ 本门弟子需要勤加练习，终有一日会像我一样成为本门派首席弟子，为门派争光。]]
          xx ={"参加首席争霸赛","领取首席弟子称谓","兑换奖励","我只是路过的"}
          else
          wb=" 你非本门弟子，请不要前来捣乱。"
          end
        elseif 类型=="飞贼" then --封妖任务
            if 任务数据[任务id].战斗==false then
            self.符合id=false
            for n=1,#任务数据[任务id].数字id do
            if 任务数据[任务id].数字id[n]==UserData[id].id then
            self.符合id=true
            end
            end
            if self.符合id then
            wb=[[ 前些日子从皇宫盗走了不少宝贝，得赶紧找个买家卖掉这些宝贝。]]
            xx={"快交出赃物","我是路过的"}
            else
            wb=[[ 你是什么人？我认识你吗？]]
            end
            else
            wb=[[ 我正在战斗中，请勿扰。]]
            end
        elseif 类型=="门派闯关开始" then
            if RoleControl:GetTaskID(UserData[id],"门派闯关")~=0 then
              if 任务数据[门派闯关活动id[任务数据[RoleControl:GetTaskID(UserData[id],"门派闯关")].当前关卡]].门派==任务数据[任务id].门派  then
              wb=[[ 你们是否已经准备好了进入战斗？]]
              xx={"放马过来","再准备会"}
              else
              wb=" 你们当前需要前往#Y/"..任务数据[门派闯关活动id[任务数据[RoleControl:GetTaskID(UserData[id],"门派闯关")].当前关卡]].门派.."#W/接受考验。"
              end
            else
            wb=[[ 请先找长安城门派闯关使者领取任务。]]
            end
        elseif 类型=="帮派玄武" then --帮派任务
          if 任务数据[任务id].数字id==UserData[id].id then
          wb=[[ 帮派了不起啊，老子一个散人照样搞得你们鸡犬不宁。]]
          xx={"让我好好地教训你","取消"}
          else
          wb=[[ 看什么看，我又没骚扰你。]]
          end
        elseif 类型=="迷宫" then
          wb=[[ 我可以帮你离开本层迷宫，至于是帮你传送至下一层迷宫或是传送至上一层迷宫我就不知晓了。]]
          xx= {"离开本层迷宫","我是路过的"}
        elseif 类型=="封妖" then --封妖任务
          if 任务数据[任务id].战斗==false then
          wb=[[ 老子就是来这里惹事的，不服你就干。]]
          xx={"不服你就干","后会有期"}
          else
          wb=[[ 我正在战斗中，请勿扰。]]
          end
        elseif 类型=="乌鸡2" then
          if 任务数据[任务id].战斗==false then
          wb=[[ 我不是树怪，我可是只可爱滴木妖。]]
          xx={"打得你变回木头","取消"}
          else
          wb=[[ 我正在战斗中，请勿扰。]]
          end
        elseif 类型=="乌鸡3" then
          wb=[[ 这里妖气冲天，待本仙人助你一臂之力。]]
          xx={"多谢仙人相助","取消"}
        elseif 类型=="乌鸡4" then
          if 任务数据[任务id].战斗==false then
          wb=[[ 这里的一切都是俺滴，包括你也是俺滴。]]
          xx={"做你的春秋大梦去","取消"}
          else
          wb=[[ 我正在战斗中，请勿扰。]]
          end
        elseif 类型=="车迟1" then
            wb= 任务数据[任务id].对话
            xx= 任务数据[任务id].选项
        elseif 类型=="车迟2" then
          if 任务数据[任务id].战斗==false then
          wb=[[ 我是可爱滴泡泡，不是用来吃滴贡品。]]
          xx={"胡说，你就是贡品","取消"}
          else
          wb=[[ 我正在战斗中，请勿扰。]]
          end
        elseif 类型=="车迟3" then
          if 任务数据[任务id].战斗==false then
          wb=[[ 你这个蝼蚁，我可是天尊，信不信我分分钟把你给秒了。]]
          xx={"是天尊也照打不误","怕怕，走先"}
          else
          wb=[[ 我正在战斗中，请勿扰。]]
          end
        elseif 类型=="车迟4" then
          if 任务数据[任务id].战斗==false then
          wb=[[ 这位小朋友，我看你全身上下没有一点协调的地方，看起来是有小儿多动症，要不要我来帮你医治医治？]]
          xx ={"动你大爷","动次打次，我就是喜欢这么动"}
          else
          wb=[[ 我正在战斗中，请勿扰。]]
          end
        elseif 类型=="车迟5" then
          if 任务数据[任务id].战斗==false then
          wb=[[ 老子无法无天，要把毁灭这三界六道。]]
          xx ={"妖孽休要狂妄","我是路过的"}
          else
          wb=[[ 我正在战斗中，请勿扰。]]
          end

        end
        return {造型,名称,wb,xx,类型,标识}
    end
end
return DialogueControl