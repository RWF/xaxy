﻿
using GameServerApp.Common;
using Microsoft.Win32;
using System;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace GM管理工具
{

    public partial class Form1 : Form
    {
      

        //异或因子
        private byte[] xorScale = new byte[] { 99, 66, 138, 55, 23, 254, 109, 165, 90, 19, 41, 145, 201, 58, 55, 37, 254, 185, 165, 169, 19, 171, 38, 1, 99, 9, 86, 12, 74, 1, 215, 88, 64, 56, 22, 56 };//.data文件的xor加解密因子


        public SynchronizationContext SyncContext = null;
        public string key = "FT@!xasd";
        public string Notice;
        public int flag = 381990860;
        public byte[] keyArray;



        public Form1()
        {
            InitializeComponent();
        }
        #region 输出控制台
        public void print(object msg)
        {
       
                textBox1.AppendText(msg.ToString());
        }
        public void OnlineNumber(object msg)
        {
            label4.Text = msg.ToString() + "人";
        }
        public void Status(object msg)
        {
            label6.Text = msg.ToString();

        }
        #endregion
        #region 第一次加载
        private void Form1_Load(object sender, EventArgs e)
        {
        
            //Control.CheckForIllegalCrossThreadCalls = false;
            keyArray = Encoding.UTF8.GetBytes(key);


          
            SyncContext = SynchronizationContext.Current;
       

            listBox1.SetSelected(0, true);
            listBox2.SetSelected(0, true);
            listBox3.SetSelected(0, true);
            listBox4.SetSelected(0, true);





        }
        #endregion
        #region 连接服务端
        private void button1_Click(object sender, EventArgs e)
        {
            textBox17.ReadOnly = true;
            ConnectServer();
        }
        #endregion
        #region 清空控制台
        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Clear();

        }
        #endregion


        #region 输入限制数字
        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar < 48 || e.KeyChar > 57)
                e.Handled = true;
            if (e.KeyChar == 8)
                e.Handled = false;
        }
        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar < 48 || e.KeyChar > 57)
                e.Handled = true;
            if (e.KeyChar == 8 || e.KeyChar == 46)
                e.Handled = false;
        }
        #endregion
        #region 取时间戳
        public string GetTimeStamp()
        {
            long a = (DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000000;
            return a.ToString();

        }
        #endregion

    
     
        #region 启动服务端
        private void ConnectServer()
        {
            Client.ClientMsg.Connect();

        }
   
        #endregion
        #region 开关
        private void Switch_Click(object sender, EventArgs e)
        {
            Client.ClientMsg.SendMsg(1000, ((Button)sender).Text);
        }
        #endregion
        #region 玩家操作处理
        private void button13_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox6.Text))
            {
                MessageBox.Show("玩家ID不能为空！");
                return;
            }

            Client.ClientMsg.SendMsg(1001, "do local ret={[1]=\"" + textBox6.Text + "\",[2]=\"" + listBox1.SelectedItem + "\"} return ret end");
        }
        private void button56_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox39.Text))
            {
                MessageBox.Show("玩家ID不能为空！");
                return;
            }

            Client.ClientMsg.SendMsg(1009, textBox39.Text);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox8.SelectedIndex == -1)
            {
                MessageBox.Show("必须选择一个操作方式！");
                return;
            }

            Client.ClientMsg.SendMsg(1009, listBox8.SelectedItem.ToString());
        }
        #endregion
        #region 监听
        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox5.Checked)
                Client.ClientMsg.SendMsg(1000, "当前监听开启");
            else
                Client.ClientMsg.SendMsg(1000, "当前监听关闭");
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox6.Checked)
                Client.ClientMsg.SendMsg(1000, "队伍监听开启");
            else
                Client.ClientMsg.SendMsg(1000, "队伍监听关闭");
        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox7.Checked)
                Client.ClientMsg.SendMsg(1000, "世界监听开启");
            else
                Client.ClientMsg.SendMsg(1000, "世界监听关闭");
        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox8.Checked)
                Client.ClientMsg.SendMsg(1000, "帮派监听开启");
            else
                Client.ClientMsg.SendMsg(1000, "帮派监听关闭");
        }

        private void checkBox10_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox10.Checked)
                Client.ClientMsg.SendMsg(1000, "门派监听开启");
            else
                Client.ClientMsg.SendMsg(1000, "门派监听关闭");
        }

        private void checkBox9_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox9.Checked)
                Client.ClientMsg.SendMsg(1000, "传闻监听开启");
            else
                Client.ClientMsg.SendMsg(1000, "传闻监听关闭");
        }
        #endregion
    

        #region 全服发送
        private void button42_Click(object sender, EventArgs e)
        {
            if (listBox4.SelectedIndex == -1)
            {
                MessageBox.Show("必须选择一个操作方式！");
                return;
            }

            Client.ClientMsg.SendMsg(1000, listBox4.SelectedItem.ToString());
        }
        #endregion
        #region 调整经验
        private void button39_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox22.Text))
            {
                MessageBox.Show("参数不能为空");
                return;
            }
            Client.ClientMsg.SendMsg(1002, "do local ret={[1]=" + textBox22.Text + ",[2]=\"调整经验\"} return ret end");
        }
        #endregion
        #region 充值业务
        private void button31_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox2.Text) || string.IsNullOrEmpty(textBox35.Text))
            {
                MessageBox.Show("玩家ID和数额不能为空！");
                return;
            }

            Client.ClientMsg.SendMsg(1004, "do local ret={[1]=" + textBox2.Text + ",[2]=\"" + listBox2.SelectedItem + "\",[3]=" + textBox35.Text + "} return ret end");
        }
        #endregion
        #region 赠送称谓
        private void button34_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox3.Text))
            {
                MessageBox.Show("玩家ID不能为空！");
                return;
            }

            Client.ClientMsg.SendMsg(1003, "do local ret={[1]=" + textBox3.Text + ",[2]=\"" + listBox3.SelectedItem + "\"} return ret end");
        }
        #endregion
        #region 定制装备
        private void button26_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox9.Text) || comboBox1.SelectedItem == null || string.IsNullOrEmpty(textBox13.Text) || string.IsNullOrEmpty(textBox12.Text))
            {
                MessageBox.Show("红色标签为必须填写的值！");
                return;
            }
            StringBuilder Msg = new StringBuilder();
            Msg.Append(textBox9.Text);
            Msg.Append("*-*");
            Msg.Append(comboBox1.SelectedItem);
            Msg.Append("*-*");
            Msg.Append(textBox12.Text);
            Msg.Append("*-*");
            if (!(comboBox4.SelectedItem == null))//特效
                Msg.Append(comboBox4.SelectedItem);
            Msg.Append("*-*");
            if (!(comboBox3.SelectedItem == null))//特技
                Msg.Append(comboBox3.SelectedItem);
            Msg.Append("*-*");
            if (radioButton1.Checked)
                Msg.Append("1");
            if (radioButton2.Checked)
                Msg.Append("2");
            Msg.Append("*-*");
            if (!(string.IsNullOrEmpty(comboBox9.Text)))
                Msg.Append(comboBox9.Text);
            Msg.Append("*-*");
            if (!(comboBox2.SelectedItem == null))
                Msg.Append(comboBox2.SelectedItem);
            Msg.Append("=");
            if (!(string.IsNullOrEmpty(textBox10.Text)))
                Msg.Append(textBox10.Text);
            Msg.Append("@");
            if (!(comboBox7.SelectedItem == null))
                Msg.Append(comboBox7.SelectedItem);
            Msg.Append("=");// 三围
            if (!(string.IsNullOrEmpty(textBox11.Text)))
                Msg.Append(textBox11.Text);
            Client.ClientMsg.SendMsg(1005, "do local ret={[1]=" + textBox13.Text + ",[2]=\"" + Msg.ToString() + "\"} return ret end");
        }
        #endregion
        #region 定制灵饰
        private void button29_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox20.Text) || comboBox12.SelectedItem == null || comboBox16.SelectedItem == null || comboBox8.SelectedItem == null || string.IsNullOrEmpty(textBox36.Text))
            {
                MessageBox.Show("红色标签为必须填写的值！");
                return;
            }
            StringBuilder Msg = new StringBuilder();
            Msg.Append(comboBox16.SelectedItem);//等级 
            Msg.Append("*-*");
            Msg.Append(comboBox12.SelectedItem);//类型
            Msg.Append("*-*");
            Msg.Append(comboBox8.SelectedItem);//主类型
            Msg.Append("*-*");
            Msg.Append(textBox36.Text);//主属性
            Msg.Append("*-*");
            if (checkBox11.Checked)//超级简易
                Msg.Append("1");
            Msg.Append("*-*");
            if (!(comboBox5.SelectedItem == null))//附加属性1
                Msg.Append(comboBox5.SelectedItem);
            Msg.Append("=");
            if (!(string.IsNullOrEmpty(textBox14.Text)))
                Msg.Append(textBox14.Text);
            Msg.Append("@");

            if (!(comboBox6.SelectedItem == null))//附加属性2
                Msg.Append(comboBox6.SelectedItem);
            Msg.Append("=");
            if (!(string.IsNullOrEmpty(textBox16.Text)))
                Msg.Append(textBox16.Text);
            Msg.Append("@");
            if (!(comboBox10.SelectedItem == null))//附加属性3
                Msg.Append(comboBox10.SelectedItem);
            Msg.Append("=");
            if (!(string.IsNullOrEmpty(textBox18.Text)))
                Msg.Append(textBox18.Text);
            Msg.Append("@");
            if (!(comboBox11.SelectedItem == null))//附加属性4
                Msg.Append(comboBox11.SelectedItem);
            Msg.Append("=");
            if (!(string.IsNullOrEmpty(textBox19.Text)))
            { Msg.Append(textBox19.Text); }


            Client.ClientMsg.SendMsg(1006, "do local ret={[1]=" + textBox20.Text + ",[2]=\"" + Msg.ToString() + "\"} return ret end");
        }
        #endregion
        #region 定制宝宝
        private void button46_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox32.Text) || comboBox15.SelectedItem == null || string.IsNullOrEmpty(textBox33.Text) || string.IsNullOrEmpty(textBox15.Text))
            {
                MessageBox.Show("红色标签为必须填写的值！");
                return;
            }
            StringBuilder Msg = new StringBuilder();
            Msg.Append(textBox33.Text);
            Msg.Append("*-*");
            Msg.Append(textBox15.Text);
            Msg.Append("*-*");
            Msg.Append(comboBox15.SelectedItem);
            Msg.Append("*-*");
            if (!(comboBox14.SelectedItem == null))
                Msg.Append(comboBox14.SelectedItem);
            Msg.Append("*-*");
            if (!(string.IsNullOrEmpty(textBox34.Text)))
                Msg.Append(textBox34.Text);
            Msg.Append("*-*");
            if (!(string.IsNullOrEmpty(textBox26.Text)))
                Msg.Append(textBox26.Text);
            Msg.Append("*-*");
            if (!(string.IsNullOrEmpty(textBox27.Text)))
                Msg.Append(textBox27.Text);
            Msg.Append("*-*");
            if (!(string.IsNullOrEmpty(textBox28.Text)))
                Msg.Append(textBox28.Text);
            Msg.Append("*-*");
            if (!(string.IsNullOrEmpty(textBox29.Text)))
                Msg.Append(textBox29.Text);
            Msg.Append("*-*");
            if (!(string.IsNullOrEmpty(textBox37.Text)))
                Msg.Append(textBox37.Text);
            Msg.Append("*-*");
            if (!(string.IsNullOrEmpty(textBox30.Text)))
                Msg.Append(textBox30.Text);
            Msg.Append("*-*");
            if (!(string.IsNullOrEmpty(textBox31.Text)))
                Msg.Append(textBox31.Text);
            Msg.Append("*-*");

            string strCollected = string.Empty;
            for (int i = 0; i < checkedListBox2.Items.Count; i++)
            {
                if (checkedListBox2.GetItemChecked(i))
                {
                    if (strCollected == string.Empty)
                    {
                        strCollected = checkedListBox2.GetItemText(checkedListBox2.Items[i]);
                    }
                    else
                    {
                        strCollected = strCollected + "@" + checkedListBox2.GetItemText(checkedListBox2.Items[i]);
                    }
                }
            }
            Msg.Append(strCollected);
            Msg.Append("*-*");
            string strCollected1 = string.Empty;
            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                if (checkedListBox1.GetItemChecked(i))
                {
                    if (strCollected1 == string.Empty)
                    {
                        strCollected1 = checkedListBox1.GetItemText(checkedListBox1.Items[i]);
                    }
                    else
                    {
                        strCollected1 = strCollected1 + "@" + checkedListBox1.GetItemText(checkedListBox1.Items[i]);
                    }
                }
            }
            Msg.Append(strCollected1);

            Client.ClientMsg.SendMsg(1007, "do local ret={[1]=" + textBox32.Text + ",[2]=\"" + Msg.ToString() + "\"} return ret end");

        }
        #endregion


        private void button37_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox41.Text) || string.IsNullOrEmpty(textBox24.Text))
            {
                MessageBox.Show("玩家ID和物品名称不能为空！");
                return;
            }

            Client.ClientMsg.SendMsg(1010, "do local ret={[1]=" + textBox24.Text + ",[2]=\"" + textBox41.Text + "\""
                + ",[3]=\"" + textBox42.Text + "\""
                + ",[4]=\"" + textBox43.Text + "\""
                + ",[5]=\"" + textBox44.Text + "\""
                + ",[6]=\"" + textBox45.Text + "\"} return ret end");
        }

        private void button3_Click(object sender, EventArgs e)
        {
                if (string.IsNullOrEmpty(textBox46.Text))
                {
                    MessageBox.Show("参数不能为空");
                    return;
                }
                Client.ClientMsg.SendMsg(1002, "do local ret={[1]=" + textBox46.Text + ",[2]=\"限制等级\"} return ret end");
            
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox41.Text) )
            {
                MessageBox.Show("物品名称不能为空！");
                return;
            }

            Client.ClientMsg.SendMsg(1010, "do local ret={[1]=\"全服\",[2]=\"" + textBox41.Text + "\""
                + ",[3]=\"" + textBox42.Text + "\""
                + ",[4]=\"" + textBox43.Text + "\""
                + ",[5]=\"" + textBox44.Text + "\""
                + ",[6]=\"" + textBox45.Text + "\"} return ret end");
        }
    }
}
