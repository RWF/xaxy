﻿namespace GM管理工具
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.Packet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numberIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.identificationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nmaeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConnectTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UserState = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.comboBox15 = new System.Windows.Forms.ComboBox();
            this.comboBox14 = new System.Windows.Forms.ComboBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.button46 = new System.Windows.Forms.Button();
            this.label44 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.checkedListBox2 = new System.Windows.Forms.CheckedListBox();
            this.label43 = new System.Windows.Forms.Label();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.label36 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.button34 = new System.Windows.Forms.Button();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.button31 = new System.Windows.Forms.Button();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.button42 = new System.Windows.Forms.Button();
            this.listBox4 = new System.Windows.Forms.ListBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label23 = new System.Windows.Forms.Label();
            this.comboBox9 = new System.Windows.Forms.ComboBox();
            this.button26 = new System.Windows.Forms.Button();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.消息处理 = new System.Windows.Forms.TabPage();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button56 = new System.Windows.Forms.Button();
            this.listBox8 = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button13 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.button35 = new System.Windows.Forms.Button();
            this.button59 = new System.Windows.Forms.Button();
            this.button58 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button57 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.button40 = new System.Windows.Forms.Button();
            this.button39 = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label33 = new System.Windows.Forms.Label();
            this.button29 = new System.Windows.Forms.Button();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.comboBox11 = new System.Windows.Forms.ComboBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.comboBox16 = new System.Windows.Forms.ComboBox();
            this.comboBox12 = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tabPage8.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.消息处理.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(314, 134);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 30);
            this.button1.TabIndex = 1;
            this.button1.Text = "连接服务器";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.Black;
            this.textBox1.Font = new System.Drawing.Font("微软雅黑", 9.07563F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox1.ForeColor = System.Drawing.Color.Lime;
            this.textBox1.Location = new System.Drawing.Point(1, 1);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(302, 174);
            this.textBox1.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(331, 119);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "人数:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(377, 119);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "0人";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(331, 98);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 12);
            this.label5.TabIndex = 7;
            this.label5.Text = "状态:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Green;
            this.label6.Location = new System.Drawing.Point(369, 98);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 7;
            this.label6.Text = "未连接";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(377, 134);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(51, 30);
            this.button4.TabIndex = 1;
            this.button4.Text = "清空控制台";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Packet
            // 
            this.Packet.DataPropertyName = "Packet";
            this.Packet.HeaderText = "封包记录";
            this.Packet.MinimumWidth = 6;
            this.Packet.Name = "Packet";
            this.Packet.ReadOnly = true;
            this.Packet.Width = 129;
            // 
            // numberIDDataGridViewTextBoxColumn
            // 
            this.numberIDDataGridViewTextBoxColumn.DataPropertyName = "NumberID";
            this.numberIDDataGridViewTextBoxColumn.HeaderText = "玩家ID";
            this.numberIDDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.numberIDDataGridViewTextBoxColumn.Name = "numberIDDataGridViewTextBoxColumn";
            this.numberIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.numberIDDataGridViewTextBoxColumn.Width = 85;
            // 
            // identificationDataGridViewTextBoxColumn
            // 
            this.identificationDataGridViewTextBoxColumn.DataPropertyName = "Identification";
            this.identificationDataGridViewTextBoxColumn.HeaderText = "账号";
            this.identificationDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.identificationDataGridViewTextBoxColumn.Name = "identificationDataGridViewTextBoxColumn";
            this.identificationDataGridViewTextBoxColumn.ReadOnly = true;
            this.identificationDataGridViewTextBoxColumn.Width = 90;
            // 
            // nmaeDataGridViewTextBoxColumn
            // 
            this.nmaeDataGridViewTextBoxColumn.DataPropertyName = "Nmae";
            this.nmaeDataGridViewTextBoxColumn.HeaderText = "角色名称";
            this.nmaeDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.nmaeDataGridViewTextBoxColumn.Name = "nmaeDataGridViewTextBoxColumn";
            this.nmaeDataGridViewTextBoxColumn.ReadOnly = true;
            this.nmaeDataGridViewTextBoxColumn.Width = 124;
            // 
            // ConnectTime
            // 
            this.ConnectTime.DataPropertyName = "ConnectTime";
            this.ConnectTime.HeaderText = "登入时间";
            this.ConnectTime.MinimumWidth = 6;
            this.ConnectTime.Name = "ConnectTime";
            this.ConnectTime.ReadOnly = true;
            this.ConnectTime.Width = 155;
            // 
            // UserState
            // 
            this.UserState.DataPropertyName = "UserState";
            this.UserState.HeaderText = "游戏状态";
            this.UserState.MinimumWidth = 6;
            this.UserState.Name = "UserState";
            this.UserState.ReadOnly = true;
            this.UserState.Width = 94;
            // 
            // IP
            // 
            this.IP.DataPropertyName = "IP";
            this.IP.HeaderText = "IP";
            this.IP.MinimumWidth = 6;
            this.IP.Name = "IP";
            this.IP.ReadOnly = true;
            this.IP.Width = 124;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.MinimumWidth = 6;
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Width = 40;
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(357, 9);
            this.textBox17.Margin = new System.Windows.Forms.Padding(2);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(79, 21);
            this.textBox17.TabIndex = 4;
            this.textBox17.Text = "127.0.0.1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(306, 11);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 7;
            this.label7.Text = "服务器IP";
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.textBox31);
            this.tabPage8.Controls.Add(this.textBox37);
            this.tabPage8.Controls.Add(this.textBox30);
            this.tabPage8.Controls.Add(this.textBox29);
            this.tabPage8.Controls.Add(this.textBox28);
            this.tabPage8.Controls.Add(this.textBox32);
            this.tabPage8.Controls.Add(this.textBox27);
            this.tabPage8.Controls.Add(this.textBox34);
            this.tabPage8.Controls.Add(this.textBox26);
            this.tabPage8.Controls.Add(this.textBox15);
            this.tabPage8.Controls.Add(this.textBox33);
            this.tabPage8.Controls.Add(this.label48);
            this.tabPage8.Controls.Add(this.label39);
            this.tabPage8.Controls.Add(this.label47);
            this.tabPage8.Controls.Add(this.label46);
            this.tabPage8.Controls.Add(this.comboBox15);
            this.tabPage8.Controls.Add(this.comboBox14);
            this.tabPage8.Controls.Add(this.label45);
            this.tabPage8.Controls.Add(this.label49);
            this.tabPage8.Controls.Add(this.button46);
            this.tabPage8.Controls.Add(this.label44);
            this.tabPage8.Controls.Add(this.label51);
            this.tabPage8.Controls.Add(this.checkedListBox2);
            this.tabPage8.Controls.Add(this.label43);
            this.tabPage8.Controls.Add(this.checkedListBox1);
            this.tabPage8.Controls.Add(this.label50);
            this.tabPage8.Controls.Add(this.label52);
            this.tabPage8.Controls.Add(this.label42);
            this.tabPage8.Controls.Add(this.label41);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage8.Size = new System.Drawing.Size(442, 340);
            this.tabPage8.TabIndex = 10;
            this.tabPage8.Text = "定制召唤兽";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(364, 34);
            this.textBox31.Margin = new System.Windows.Forms.Padding(2);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(48, 21);
            this.textBox31.TabIndex = 8;
            this.textBox31.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox4_KeyPress);
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(212, 59);
            this.textBox37.Margin = new System.Windows.Forms.Padding(2);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(48, 21);
            this.textBox37.TabIndex = 8;
            this.textBox37.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(364, 9);
            this.textBox30.Margin = new System.Windows.Forms.Padding(2);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(48, 21);
            this.textBox30.TabIndex = 8;
            this.textBox30.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // textBox29
            // 
            this.textBox29.Location = new System.Drawing.Point(212, 33);
            this.textBox29.Margin = new System.Windows.Forms.Padding(2);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(48, 21);
            this.textBox29.TabIndex = 8;
            this.textBox29.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(212, 8);
            this.textBox28.Margin = new System.Windows.Forms.Padding(2);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(48, 21);
            this.textBox28.TabIndex = 8;
            this.textBox28.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(62, 298);
            this.textBox32.Margin = new System.Windows.Forms.Padding(2);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(76, 21);
            this.textBox32.TabIndex = 8;
            this.textBox32.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(62, 61);
            this.textBox27.Margin = new System.Windows.Forms.Padding(2);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(48, 21);
            this.textBox27.TabIndex = 8;
            this.textBox27.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // textBox34
            // 
            this.textBox34.Location = new System.Drawing.Point(62, 10);
            this.textBox34.Margin = new System.Windows.Forms.Padding(2);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(48, 21);
            this.textBox34.TabIndex = 8;
            this.textBox34.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(62, 34);
            this.textBox26.Margin = new System.Windows.Forms.Padding(2);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(48, 21);
            this.textBox26.TabIndex = 8;
            this.textBox26.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(191, 298);
            this.textBox15.Margin = new System.Windows.Forms.Padding(2);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(76, 21);
            this.textBox15.TabIndex = 8;
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(62, 323);
            this.textBox33.Margin = new System.Windows.Forms.Padding(2);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(76, 21);
            this.textBox33.TabIndex = 8;
            this.textBox33.Text = "1";
            this.textBox33.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(310, 36);
            this.label48.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(29, 12);
            this.label48.TabIndex = 7;
            this.label48.Text = "成长";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(158, 63);
            this.label39.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(53, 12);
            this.label39.TabIndex = 7;
            this.label39.Text = "速度资质";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(310, 10);
            this.label47.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(53, 12);
            this.label47.TabIndex = 7;
            this.label47.Text = "躲避资质";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(157, 37);
            this.label46.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(53, 12);
            this.label46.TabIndex = 7;
            this.label46.Text = "法术资质";
            // 
            // comboBox15
            // 
            this.comboBox15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox15.FormattingEnabled = true;
            this.comboBox15.Items.AddRange(new object[] {
            "宝宝",
            "变异",
            "神兽",
            "野怪",
            "孩子",
            "变异神兽"});
            this.comboBox15.Location = new System.Drawing.Point(191, 322);
            this.comboBox15.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox15.Name = "comboBox15";
            this.comboBox15.Size = new System.Drawing.Size(76, 20);
            this.comboBox15.TabIndex = 2;
            // 
            // comboBox14
            // 
            this.comboBox14.FormattingEnabled = true;
            this.comboBox14.Items.AddRange(new object[] {
            "",
            "复仇",
            "自恋",
            "灵刃",
            "灵法",
            "预知",
            "灵动",
            "瞬击",
            "瞬法",
            "抗法",
            "抗物",
            "阳护",
            "识物",
            "护佑",
            "洞察",
            "弑神",
            "御风",
            "顺势",
            "怒吼",
            "逆境",
            "乖巧",
            "力破",
            "识药",
            "吮魔",
            "争锋",
            "灵断"});
            this.comboBox14.Location = new System.Drawing.Point(364, 60);
            this.comboBox14.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox14.Name = "comboBox14";
            this.comboBox14.Size = new System.Drawing.Size(48, 20);
            this.comboBox14.TabIndex = 2;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(157, 10);
            this.label45.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(53, 12);
            this.label45.TabIndex = 7;
            this.label45.Text = "体力资质";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.ForeColor = System.Drawing.Color.Red;
            this.label49.Location = new System.Drawing.Point(10, 298);
            this.label49.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(41, 12);
            this.label49.TabIndex = 7;
            this.label49.Text = "玩家ID";
            // 
            // button46
            // 
            this.button46.Location = new System.Drawing.Point(291, 304);
            this.button46.Margin = new System.Windows.Forms.Padding(2);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(130, 33);
            this.button46.TabIndex = 1;
            this.button46.Text = "定制";
            this.button46.UseVisualStyleBackColor = true;
            this.button46.Click += new System.EventHandler(this.button46_Click);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(7, 65);
            this.label44.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(53, 12);
            this.label44.TabIndex = 7;
            this.label44.Text = "防御资质";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(10, 12);
            this.label51.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(29, 12);
            this.label51.TabIndex = 7;
            this.label51.Text = "灵性";
            // 
            // checkedListBox2
            // 
            this.checkedListBox2.FormattingEnabled = true;
            this.checkedListBox2.Items.AddRange(new object[] {
            "迅敏",
            "狂怒",
            "阴伤",
            "静岳",
            "擅咒",
            "灵身",
            "矫健",
            "深思",
            "钢化",
            "坚甲",
            "慧心",
            "撞击",
            "无畏",
            "愤恨",
            "淬毒",
            "狙刺",
            "连环",
            "圣洁",
            "灵光",
            "神机步",
            "腾挪劲",
            "玄武躯",
            "龙胄铠",
            "玉砥柱",
            "碎甲刃",
            "阴阳护",
            "凛冽气",
            "舍身击",
            "电魂闪",
            "通灵法",
            "双星爆",
            "催心浪",
            "隐匿击",
            "生死决",
            "血债偿"});
            this.checkedListBox2.Location = new System.Drawing.Point(13, 86);
            this.checkedListBox2.Margin = new System.Windows.Forms.Padding(2);
            this.checkedListBox2.MultiColumn = true;
            this.checkedListBox2.Name = "checkedListBox2";
            this.checkedListBox2.Size = new System.Drawing.Size(422, 68);
            this.checkedListBox2.TabIndex = 0;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(7, 38);
            this.label43.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(53, 12);
            this.label43.TabIndex = 7;
            this.label43.Text = "攻击资质";
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Items.AddRange(new object[] {
            "进阶善恶有报",
            "进阶力劈华山",
            "进阶壁垒击破",
            "灵山禅语",
            "北冥之渊",
            "扶摇万里",
            "逍遥游",
            "水击三千",
            "风起龙游",
            "无赦魔决",
            "欣欣向荣",
            "万物之灵（火）",
            "万物之灵（水）",
            "万物之灵（金）",
            "万物之灵（土）",
            "万物之灵（木）",
            "力大无穷（火）",
            "力大无穷（金）",
            "力大无穷（土）",
            "力大无穷（水）",
            "力大无穷（木）",
            "神来气旺",
            "哼哼哈兮",
            "津津有味",
            "净台妙谛",
            "叱咤风云",
            "出其不意",
            "灵山禅语",
            "无畏布施",
            "独行",
            "高级独行",
            "法术反震",
            "高级法术反震",
            "高级进击必杀",
            "进击必杀",
            "高级进击法暴",
            "进击法暴",
            "高级遗志",
            "遗志",
            "乘胜追击",
            "拘魂索命",
            "理直气壮",
            "溜之大吉",
            "流沙轻音",
            "食指大动",
            "凝光练彩",
            "昼伏夜出",
            "势如破竹",
            "凭风借力",
            "神出鬼没",
            "光照万象",
            "大快朵颐",
            "浮云神马",
            "嗜血追击",
            "须弥真言",
            "天降灵葫",
            "上古灵符",
            "八凶法阵",
            "月光",
            "惊心一剑",
            "死亡召唤",
            "壁垒击破",
            "剑荡四方",
            "力劈华山",
            "夜舞倾城",
            "善恶有报",
            "法术防御",
            "光照万象",
            "灵能激发",
            "龙魂",
            "苍鸾怒击",
            "从天而降",
            "奔雷咒",
            "地狱烈火",
            "高级必杀",
            "高级毒",
            "高级盾气",
            "高级法术暴击",
            "高级法术波动",
            "高级法术连击",
            "高级反击",
            "高级反震",
            "高级防御",
            "高级飞行",
            "高级否定信仰",
            "高级感知",
            "高级鬼魂术",
            "高级合纵",
            "高级慧根",
            "高级火属性吸收",
            "高级精神集中",
            "高级雷属性吸收",
            "高级连击",
            "高级敏捷",
            "高级冥思",
            "高级魔之心",
            "高级强力",
            "高级驱鬼",
            "高级神迹",
            "高级神佑复生",
            "高级水属性吸收",
            "高级偷袭",
            "高级土属性吸收",
            "高级吸血",
            "高级幸运",
            "高级夜战",
            "高级隐身",
            "高级永恒",
            "高级再生",
            "高级招架",
            "水漫金山",
            "泰山压顶",
            "防御",
            "反击",
            "必杀",
            "吸血",
            "强力",
            "偷袭",
            "反震",
            "法术暴击",
            "冥思",
            "法术波动",
            "夜战",
            "隐身",
            "感知",
            "再生",
            "法术连击",
            "毒",
            "幸运",
            "连击",
            "永恒",
            "敏捷",
            "神佑复生",
            "驱鬼",
            "鬼魂术",
            "魔之心",
            "水攻",
            "落岩",
            "雷击",
            "烈火",
            "火属性吸收",
            "土属性吸收",
            "水属性吸收",
            "雷属性吸收",
            "迟钝",
            "招架",
            "神迹",
            "精神集中",
            "盾气",
            "合纵",
            "否定信仰",
            "飞行"});
            this.checkedListBox1.Location = new System.Drawing.Point(13, 174);
            this.checkedListBox1.Margin = new System.Windows.Forms.Padding(2);
            this.checkedListBox1.MultiColumn = true;
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(422, 100);
            this.checkedListBox1.TabIndex = 0;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.ForeColor = System.Drawing.Color.Red;
            this.label50.Location = new System.Drawing.Point(28, 328);
            this.label50.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(29, 12);
            this.label50.TabIndex = 7;
            this.label50.Text = "等级";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.ForeColor = System.Drawing.Color.Red;
            this.label52.Location = new System.Drawing.Point(158, 325);
            this.label52.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(29, 12);
            this.label52.TabIndex = 7;
            this.label52.Text = "类型";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.ForeColor = System.Drawing.Color.Red;
            this.label42.Location = new System.Drawing.Point(157, 300);
            this.label42.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(29, 12);
            this.label42.TabIndex = 7;
            this.label42.Text = "造型";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(310, 61);
            this.label41.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(29, 12);
            this.label41.TabIndex = 7;
            this.label41.Text = "特性";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox13);
            this.tabPage4.Controls.Add(this.groupBox12);
            this.tabPage4.Controls.Add(this.groupBox11);
            this.tabPage4.Controls.Add(this.groupBox14);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage4.Size = new System.Drawing.Size(442, 340);
            this.tabPage4.TabIndex = 7;
            this.tabPage4.Text = "充值系统";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.textBox45);
            this.groupBox13.Controls.Add(this.label62);
            this.groupBox13.Controls.Add(this.textBox44);
            this.groupBox13.Controls.Add(this.textBox43);
            this.groupBox13.Controls.Add(this.textBox42);
            this.groupBox13.Controls.Add(this.label61);
            this.groupBox13.Controls.Add(this.label60);
            this.groupBox13.Controls.Add(this.label59);
            this.groupBox13.Controls.Add(this.label40);
            this.groupBox13.Controls.Add(this.textBox41);
            this.groupBox13.Controls.Add(this.textBox24);
            this.groupBox13.Controls.Add(this.label37);
            this.groupBox13.Controls.Add(this.label34);
            this.groupBox13.Controls.Add(this.button10);
            this.groupBox13.Controls.Add(this.button37);
            this.groupBox13.Controls.Add(this.label36);
            this.groupBox13.Location = new System.Drawing.Point(5, 149);
            this.groupBox13.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox13.Size = new System.Drawing.Size(165, 189);
            this.groupBox13.TabIndex = 31;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "赠送物品";
            // 
            // textBox45
            // 
            this.textBox45.Location = new System.Drawing.Point(69, 109);
            this.textBox45.Margin = new System.Windows.Forms.Padding(2);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(68, 21);
            this.textBox45.TabIndex = 23;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.ForeColor = System.Drawing.Color.Black;
            this.label62.Location = new System.Drawing.Point(37, 112);
            this.label62.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(29, 12);
            this.label62.TabIndex = 22;
            this.label62.Text = "限时";
            // 
            // textBox44
            // 
            this.textBox44.Location = new System.Drawing.Point(69, 84);
            this.textBox44.Margin = new System.Windows.Forms.Padding(2);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(68, 21);
            this.textBox44.TabIndex = 19;
            // 
            // textBox43
            // 
            this.textBox43.Location = new System.Drawing.Point(69, 61);
            this.textBox43.Margin = new System.Windows.Forms.Padding(2);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(68, 21);
            this.textBox43.TabIndex = 20;
            // 
            // textBox42
            // 
            this.textBox42.Location = new System.Drawing.Point(69, 38);
            this.textBox42.Margin = new System.Windows.Forms.Padding(2);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(68, 21);
            this.textBox42.TabIndex = 21;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.ForeColor = System.Drawing.Color.Black;
            this.label61.Location = new System.Drawing.Point(37, 87);
            this.label61.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(29, 12);
            this.label61.TabIndex = 15;
            this.label61.Text = "数量";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.ForeColor = System.Drawing.Color.Black;
            this.label60.Location = new System.Drawing.Point(37, 64);
            this.label60.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(29, 12);
            this.label60.TabIndex = 16;
            this.label60.Text = "技能";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.ForeColor = System.Drawing.Color.Black;
            this.label59.Location = new System.Drawing.Point(37, 41);
            this.label59.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(29, 12);
            this.label59.TabIndex = 17;
            this.label59.Text = "等级";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(50, 69);
            this.label40.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(0, 12);
            this.label40.TabIndex = 13;
            // 
            // textBox41
            // 
            this.textBox41.Location = new System.Drawing.Point(69, 14);
            this.textBox41.Margin = new System.Windows.Forms.Padding(2);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(82, 21);
            this.textBox41.TabIndex = 12;
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(69, 134);
            this.textBox24.Margin = new System.Windows.Forms.Padding(2);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(82, 21);
            this.textBox24.TabIndex = 10;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Location = new System.Drawing.Point(14, 136);
            this.label37.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(41, 12);
            this.label37.TabIndex = 8;
            this.label37.Text = "玩家ID";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.ForeColor = System.Drawing.Color.Red;
            this.label34.Location = new System.Drawing.Point(14, 17);
            this.label34.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(53, 12);
            this.label34.TabIndex = 9;
            this.label34.Text = "物品名称";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(100, 158);
            this.button10.Margin = new System.Windows.Forms.Padding(2);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(54, 26);
            this.button10.TabIndex = 6;
            this.button10.Text = "全服";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(13, 158);
            this.button37.Margin = new System.Windows.Forms.Padding(2);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(54, 26);
            this.button37.TabIndex = 6;
            this.button37.Text = "单人";
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(37, 61);
            this.label36.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(0, 12);
            this.label36.TabIndex = 3;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.button34);
            this.groupBox12.Controls.Add(this.listBox3);
            this.groupBox12.Controls.Add(this.textBox3);
            this.groupBox12.Controls.Add(this.label2);
            this.groupBox12.Location = new System.Drawing.Point(298, 7);
            this.groupBox12.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox12.Size = new System.Drawing.Size(142, 330);
            this.groupBox12.TabIndex = 31;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "赠送称谓";
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(16, 293);
            this.button34.Margin = new System.Windows.Forms.Padding(2);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(118, 33);
            this.button34.TabIndex = 6;
            this.button34.Text = "执行操作";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // listBox3
            // 
            this.listBox3.FormattingEnabled = true;
            this.listBox3.ItemHeight = 12;
            this.listBox3.Items.AddRange(new object[] {
            "VIP玩家",
            "★未饮先醉★",
            "江湖新秀",
            "江湖少侠",
            "江湖大侠",
            "江湖豪侠",
            "武林高手",
            "独孤求败",
            "天外飞仙",
            "武林盟主",
            "大内密探",
            "皓月战神",
            "覆海神龙",
            "归元圣者",
            "忘情罗刹",
            "梦幻元勋",
            "三届凌绝顶",
            "天人开造化",
            "梦幻任逍遥",
            "笑傲江湖",
            "皇帝",
            "辅佐王",
            "郡王",
            "一品膘骑大将军",
            "二品辅国大将军",
            "三品归德将军",
            "超级土豪",
            "梦幻元勋",
            "大唐官府首席弟子",
            "化生寺首席弟子",
            "女儿村首席弟子",
            "方寸山首席弟子",
            "天宫首席弟子",
            "龙宫首席弟子",
            "五庄观首席弟子",
            "普陀山府首席弟子",
            "狮驼岭首席弟子",
            "魔王寨首席弟子",
            "盘丝洞首席弟子",
            "阴曹地府首席弟子",
            "凌波城首席弟子",
            "神木林首席弟子",
            "无底洞首席弟子",
            "花果山首席弟子",
            "天机城首席弟子",
            "女魃墓首席弟子",
            "极乐仙人",
            "无上金仙",
            "至尊魔君",
            "生死劫·止戈",
            "生死劫·清心",
            "生死劫·雷霆",
            "生死劫·惜花",
            "生死劫·忘情",
            "生死劫·卧龙",
            "生死劫·天象",
            "生死劫·轮回",
            "生死劫·娑罗",
            "游戏管理员",
            "武神坛明星赛亚军",
            "武神坛甲组季军",
            "武神坛甲组八强",
            "武神坛明星赛冠军",
            "武神坛甲组亚军",
            "武神坛甲组冠军",
            "炼妖大师",
            "万元户"});
            this.listBox3.Location = new System.Drawing.Point(16, 42);
            this.listBox3.Margin = new System.Windows.Forms.Padding(2);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(120, 244);
            this.listBox3.TabIndex = 5;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(54, 19);
            this.textBox3.Margin = new System.Windows.Forms.Padding(2);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(82, 21);
            this.textBox3.TabIndex = 4;
            this.textBox3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 22);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "玩家ID";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.button31);
            this.groupBox11.Controls.Add(this.listBox2);
            this.groupBox11.Controls.Add(this.textBox35);
            this.groupBox11.Controls.Add(this.textBox2);
            this.groupBox11.Controls.Add(this.label53);
            this.groupBox11.Controls.Add(this.label1);
            this.groupBox11.Location = new System.Drawing.Point(174, 8);
            this.groupBox11.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox11.Size = new System.Drawing.Size(119, 330);
            this.groupBox11.TabIndex = 31;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "充值业务";
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(16, 286);
            this.button31.Margin = new System.Windows.Forms.Padding(2);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(95, 33);
            this.button31.TabIndex = 6;
            this.button31.Text = "执行操作";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.ItemHeight = 12;
            this.listBox2.Items.AddRange(new object[] {
            "充值仙玉",
            "充值银子",
            "充值储备",
            "充值门贡",
            "充值帮贡",
            "充值经验",
            "比武积分",
            "活动积分",
            "副本积分",
            "充值活跃度",
            "地煞积分",
            "知了积分",
            "天罡积分",
            "单人积分",
            "成就积分",
            "特殊积分"});
            this.listBox2.Location = new System.Drawing.Point(16, 66);
            this.listBox2.Margin = new System.Windows.Forms.Padding(2);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(96, 196);
            this.listBox2.TabIndex = 5;
            // 
            // textBox35
            // 
            this.textBox35.Location = new System.Drawing.Point(54, 42);
            this.textBox35.Margin = new System.Windows.Forms.Padding(2);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(58, 21);
            this.textBox35.TabIndex = 4;
            this.textBox35.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(54, 19);
            this.textBox2.Margin = new System.Windows.Forms.Padding(2);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(58, 21);
            this.textBox2.TabIndex = 4;
            this.textBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(14, 44);
            this.label53.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(29, 12);
            this.label53.TabIndex = 3;
            this.label53.Text = "数额";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 22);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "玩家ID";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.button42);
            this.groupBox14.Controls.Add(this.listBox4);
            this.groupBox14.Location = new System.Drawing.Point(5, 7);
            this.groupBox14.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox14.Size = new System.Drawing.Size(164, 137);
            this.groupBox14.TabIndex = 31;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "全服发送";
            // 
            // button42
            // 
            this.button42.Location = new System.Drawing.Point(16, 90);
            this.button42.Margin = new System.Windows.Forms.Padding(2);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(135, 33);
            this.button42.TabIndex = 6;
            this.button42.Text = "执行操作";
            this.button42.UseVisualStyleBackColor = true;
            this.button42.Click += new System.EventHandler(this.button42_Click);
            // 
            // listBox4
            // 
            this.listBox4.FormattingEnabled = true;
            this.listBox4.ItemHeight = 12;
            this.listBox4.Items.AddRange(new object[] {
            "随机仙玉(50-500)",
            "随机银子(50W-200W)",
            "随机兽决",
            "随机高级兽决",
            "随机储备"});
            this.listBox4.Location = new System.Drawing.Point(13, 15);
            this.listBox4.Margin = new System.Windows.Forms.Padding(2);
            this.listBox4.Name = "listBox4";
            this.listBox4.Size = new System.Drawing.Size(138, 52);
            this.listBox4.TabIndex = 5;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage3.Size = new System.Drawing.Size(442, 340);
            this.tabPage3.TabIndex = 6;
            this.tabPage3.Text = "定制装备";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.button26);
            this.groupBox2.Controls.Add(this.textBox13);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.comboBox4);
            this.groupBox2.Controls.Add(this.comboBox3);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.textBox12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.textBox9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Location = new System.Drawing.Point(2, 5);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(437, 336);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "定制装备";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(68, 307);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 12);
            this.label15.TabIndex = 3;
            this.label15.Text = "玩家ID";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.radioButton2);
            this.groupBox5.Controls.Add(this.radioButton1);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.comboBox9);
            this.groupBox5.Location = new System.Drawing.Point(124, 153);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox5.Size = new System.Drawing.Size(296, 106);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "套装效果";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(170, 75);
            this.radioButton2.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(71, 16);
            this.radioButton2.TabIndex = 5;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "追加状态";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(70, 75);
            this.radioButton1.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(71, 16);
            this.radioButton1.TabIndex = 5;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "追加法术";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label23.Location = new System.Drawing.Point(43, 32);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 12);
            this.label23.TabIndex = 3;
            this.label23.Text = "套装名称";
            // 
            // comboBox9
            // 
            this.comboBox9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox9.FormattingEnabled = true;
            this.comboBox9.Items.AddRange(new object[] {
            "------附加状态------",
            "",
            "盘丝阵",
            "定心术",
            "极度疯狂",
            "金刚护法",
            "逆鳞",
            "生命之泉",
            "魔王回首",
            "幽冥鬼眼",
            "楚楚可怜",
            "百毒不侵",
            "变身",
            "普渡众生",
            "炼气化神",
            "修罗隐身",
            "杀气诀",
            "一苇渡江",
            "碎星诀",
            "明光宝烛",
            "------追加法术------",
            "知己知彼",
            "似玉生香",
            "三味真火",
            "日月乾坤",
            "镇妖",
            "尸腐毒",
            "阎罗令",
            "百万神兵",
            "勾魂",
            "判官令",
            "雷击",
            "魔音摄魂",
            "摄魄",
            "紧箍咒",
            "落岩",
            "含情脉脉",
            "姐妹同心",
            "日光华",
            "水攻",
            "威慑",
            "唧唧歪歪",
            "靛沧海",
            "烈火",
            "催眠符",
            "龙卷雨击",
            "巨岩破",
            "奔雷咒",
            "失心符",
            "龙腾",
            "苍茫树",
            "泰山压顶",
            "落魄符",
            "龙吟",
            "地裂火",
            "水漫金山",
            "定身符",
            "五雷咒",
            "后发制人",
            "地狱烈火",
            "满天花雨",
            "飞砂走石",
            "横扫千军",
            "落叶萧萧",
            "尘土刃",
            "荆棘舞",
            "冰川怒",
            "夺命咒",
            "夺魄令",
            "浪涌",
            "裂石",
            "鹰击",
            "翻江搅海"});
            this.comboBox9.Location = new System.Drawing.Point(110, 26);
            this.comboBox9.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox9.Name = "comboBox9";
            this.comboBox9.Size = new System.Drawing.Size(125, 20);
            this.comboBox9.TabIndex = 4;
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(256, 299);
            this.button26.Margin = new System.Windows.Forms.Padding(2);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(100, 32);
            this.button26.TabIndex = 5;
            this.button26.Text = "定制装备";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(128, 305);
            this.textBox13.Margin = new System.Windows.Forms.Padding(2);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(107, 21);
            this.textBox13.TabIndex = 4;
            this.textBox13.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.comboBox7);
            this.groupBox4.Controls.Add(this.textBox11);
            this.groupBox4.Controls.Add(this.comboBox2);
            this.groupBox4.Controls.Add(this.textBox10);
            this.groupBox4.Location = new System.Drawing.Point(128, 17);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(292, 131);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "双加属性";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label19.Location = new System.Drawing.Point(110, 102);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 12);
            this.label19.TabIndex = 3;
            this.label19.Text = "数值2";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label18.Location = new System.Drawing.Point(105, 22);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 12);
            this.label18.TabIndex = 3;
            this.label18.Text = "数值1";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label17.Location = new System.Drawing.Point(10, 102);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 12);
            this.label17.TabIndex = 3;
            this.label17.Text = "类型2";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label16.Location = new System.Drawing.Point(5, 22);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 12);
            this.label16.TabIndex = 3;
            this.label16.Text = "类型1";
            // 
            // comboBox7
            // 
            this.comboBox7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Items.AddRange(new object[] {
            "",
            "力量",
            "敏捷",
            "体质",
            "魔力",
            "耐力"});
            this.comboBox7.Location = new System.Drawing.Point(46, 100);
            this.comboBox7.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(59, 20);
            this.comboBox7.TabIndex = 4;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(146, 100);
            this.textBox11.Margin = new System.Windows.Forms.Padding(2);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(48, 21);
            this.textBox11.TabIndex = 4;
            this.textBox11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "",
            "力量",
            "敏捷",
            "体质",
            "魔力",
            "耐力"});
            this.comboBox2.Location = new System.Drawing.Point(41, 20);
            this.comboBox2.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(59, 20);
            this.comboBox2.TabIndex = 4;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(141, 20);
            this.textBox10.Margin = new System.Windows.Forms.Padding(2);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(48, 21);
            this.textBox10.TabIndex = 4;
            this.textBox10.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(31, 218);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(77, 12);
            this.label14.TabIndex = 3;
            this.label14.Text = "装备属性倍率";
            // 
            // comboBox4
            // 
            this.comboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "",
            "无级别限制",
            "简易",
            "珍宝",
            "易修理",
            "永不磨损",
            "精致",
            "必中",
            "绝杀"});
            this.comboBox4.Location = new System.Drawing.Point(37, 182);
            this.comboBox4.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(68, 20);
            this.comboBox4.TabIndex = 4;
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "",
            "破血狂攻",
            "弱点击破",
            "凝气诀",
            "凝神诀",
            "冥王爆杀",
            "慈航普渡",
            "诅咒之伤",
            "气疗术",
            "命疗术",
            "气归术",
            "命归术",
            "水清诀",
            "冰清诀",
            "玉清诀",
            "晶清诀",
            "四海升平",
            "破血狂攻",
            "罗汉金钟",
            "圣灵之甲",
            "魔兽之印",
            "野兽之力",
            "放下屠刀",
            "太极护法",
            "光辉之甲",
            "流云诀",
            "笑里藏刀",
            "破碎无双"});
            this.comboBox3.Location = new System.Drawing.Point(37, 130);
            this.comboBox3.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(68, 20);
            this.comboBox3.TabIndex = 4;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label13.Location = new System.Drawing.Point(4, 185);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 12);
            this.label13.TabIndex = 3;
            this.label13.Text = "特效";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label12.Location = new System.Drawing.Point(4, 132);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 12);
            this.label12.TabIndex = 3;
            this.label12.Text = "特技";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(18, 239);
            this.textBox12.Margin = new System.Windows.Forms.Padding(2);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(102, 21);
            this.textBox12.TabIndex = 4;
            this.textBox12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox4_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(4, 72);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 12);
            this.label11.TabIndex = 3;
            this.label11.Text = "等级";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "武器",
            "衣服",
            "项链",
            "头盔",
            "腰带",
            "鞋子"});
            this.comboBox1.Location = new System.Drawing.Point(37, 14);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(68, 20);
            this.comboBox1.TabIndex = 4;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(37, 69);
            this.textBox9.Margin = new System.Windows.Forms.Padding(2);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(68, 21);
            this.textBox9.TabIndex = 4;
            this.textBox9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(4, 17);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 3;
            this.label10.Text = "类型";
            // 
            // 消息处理
            // 
            this.消息处理.Controls.Add(this.checkBox10);
            this.消息处理.Controls.Add(this.checkBox9);
            this.消息处理.Controls.Add(this.checkBox8);
            this.消息处理.Controls.Add(this.checkBox7);
            this.消息处理.Controls.Add(this.checkBox6);
            this.消息处理.Controls.Add(this.checkBox5);
            this.消息处理.Controls.Add(this.textBox7);
            this.消息处理.Location = new System.Drawing.Point(4, 22);
            this.消息处理.Margin = new System.Windows.Forms.Padding(2);
            this.消息处理.Name = "消息处理";
            this.消息处理.Padding = new System.Windows.Forms.Padding(2);
            this.消息处理.Size = new System.Drawing.Size(442, 340);
            this.消息处理.TabIndex = 5;
            this.消息处理.Text = "消息处理";
            this.消息处理.UseVisualStyleBackColor = true;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(382, 259);
            this.checkBox10.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(48, 16);
            this.checkBox10.TabIndex = 1;
            this.checkBox10.Text = "门派";
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.checkBox10_CheckedChanged);
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(382, 322);
            this.checkBox9.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(48, 16);
            this.checkBox9.TabIndex = 1;
            this.checkBox9.Text = "传闻";
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.checkBox9_CheckedChanged);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(382, 196);
            this.checkBox8.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(48, 16);
            this.checkBox8.TabIndex = 1;
            this.checkBox8.Text = "帮派";
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.checkBox8_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(382, 133);
            this.checkBox7.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(48, 16);
            this.checkBox7.TabIndex = 1;
            this.checkBox7.Text = "世界";
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(382, 70);
            this.checkBox6.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(48, 16);
            this.checkBox6.TabIndex = 1;
            this.checkBox6.Text = "队伍";
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(382, 6);
            this.checkBox5.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(48, 16);
            this.checkBox5.TabIndex = 1;
            this.checkBox5.Text = "当前";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(2, 2);
            this.textBox7.Margin = new System.Windows.Forms.Padding(2);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(369, 336);
            this.textBox7.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.button27);
            this.tabPage5.Controls.Add(this.button28);
            this.tabPage5.Controls.Add(this.button18);
            this.tabPage5.Controls.Add(this.button7);
            this.tabPage5.Controls.Add(this.button41);
            this.tabPage5.Controls.Add(this.button33);
            this.tabPage5.Controls.Add(this.button32);
            this.tabPage5.Controls.Add(this.button25);
            this.tabPage5.Controls.Add(this.button24);
            this.tabPage5.Controls.Add(this.button17);
            this.tabPage5.Controls.Add(this.button16);
            this.tabPage5.Controls.Add(this.button38);
            this.tabPage5.Controls.Add(this.button9);
            this.tabPage5.Controls.Add(this.button30);
            this.tabPage5.Controls.Add(this.button22);
            this.tabPage5.Controls.Add(this.button6);
            this.tabPage5.Controls.Add(this.button14);
            this.tabPage5.Controls.Add(this.button21);
            this.tabPage5.Controls.Add(this.button8);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage5.Size = new System.Drawing.Size(442, 340);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "服务器操作";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(20, 204);
            this.button27.Margin = new System.Windows.Forms.Padding(2);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(68, 32);
            this.button27.TabIndex = 34;
            this.button27.Text = "刷出地煞";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(112, 154);
            this.button28.Margin = new System.Windows.Forms.Padding(2);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(68, 32);
            this.button28.TabIndex = 37;
            this.button28.Text = "刷出天罡";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(112, 105);
            this.button18.Margin = new System.Windows.Forms.Padding(2);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(68, 32);
            this.button18.TabIndex = 1;
            this.button18.Text = "维护通知";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(20, 154);
            this.button7.Margin = new System.Windows.Forms.Padding(2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(68, 32);
            this.button7.TabIndex = 27;
            this.button7.Text = "清除竞拍";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(200, 204);
            this.button41.Margin = new System.Windows.Forms.Padding(2);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(68, 32);
            this.button41.TabIndex = 26;
            this.button41.Text = "帮战3结束";
            this.button41.UseVisualStyleBackColor = true;
            this.button41.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(200, 254);
            this.button33.Margin = new System.Windows.Forms.Padding(2);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(68, 32);
            this.button33.TabIndex = 24;
            this.button33.Text = "帮战2结束";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(201, 154);
            this.button32.Margin = new System.Windows.Forms.Padding(2);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(68, 32);
            this.button32.TabIndex = 23;
            this.button32.Text = "比武大会开关";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(200, 303);
            this.button25.Margin = new System.Windows.Forms.Padding(2);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(68, 32);
            this.button25.TabIndex = 22;
            this.button25.Text = "帮战1结束";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(112, 55);
            this.button24.Margin = new System.Windows.Forms.Padding(2);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(68, 32);
            this.button24.TabIndex = 21;
            this.button24.Text = "首席争霸开关";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(20, 105);
            this.button17.Margin = new System.Windows.Forms.Padding(2);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(68, 32);
            this.button17.TabIndex = 20;
            this.button17.Text = "重置活动次数";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(200, 105);
            this.button16.Margin = new System.Windows.Forms.Padding(2);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(68, 32);
            this.button16.TabIndex = 19;
            this.button16.Text = "皇宫飞贼开关";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button38
            // 
            this.button38.Location = new System.Drawing.Point(112, 303);
            this.button38.Margin = new System.Windows.Forms.Padding(2);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(68, 32);
            this.button38.TabIndex = 18;
            this.button38.Text = "帮战3开启";
            this.button38.UseVisualStyleBackColor = true;
            this.button38.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(20, 6);
            this.button9.Margin = new System.Windows.Forms.Padding(2);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(68, 32);
            this.button9.TabIndex = 17;
            this.button9.Text = "清空排行数据";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(112, 254);
            this.button30.Margin = new System.Windows.Forms.Padding(2);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(68, 32);
            this.button30.TabIndex = 16;
            this.button30.Text = "帮战2开启";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(112, 204);
            this.button22.Margin = new System.Windows.Forms.Padding(2);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(68, 32);
            this.button22.TabIndex = 28;
            this.button22.Text = "帮战1开启";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(112, 6);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(68, 32);
            this.button6.TabIndex = 15;
            this.button6.Text = "门派闯关开关";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(20, 55);
            this.button14.Margin = new System.Windows.Forms.Padding(2);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(68, 32);
            this.button14.TabIndex = 13;
            this.button14.Text = "清除全服任务数据";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(20, 254);
            this.button21.Margin = new System.Windows.Forms.Padding(2);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(68, 32);
            this.button21.TabIndex = 12;
            this.button21.Text = "刷出师门守卫";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(20, 303);
            this.button8.Margin = new System.Windows.Forms.Padding(2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(68, 32);
            this.button8.TabIndex = 7;
            this.button8.Text = "全局存档";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.Switch_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.textBox46);
            this.tabPage2.Controls.Add(this.label63);
            this.tabPage2.Controls.Add(this.textBox39);
            this.tabPage2.Controls.Add(this.label57);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.button56);
            this.tabPage2.Controls.Add(this.listBox8);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.button35);
            this.tabPage2.Controls.Add(this.button59);
            this.tabPage2.Controls.Add(this.button58);
            this.tabPage2.Controls.Add(this.button19);
            this.tabPage2.Controls.Add(this.button57);
            this.tabPage2.Controls.Add(this.button20);
            this.tabPage2.Controls.Add(this.button36);
            this.tabPage2.Controls.Add(this.button12);
            this.tabPage2.Controls.Add(this.button11);
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.textBox22);
            this.tabPage2.Controls.Add(this.button40);
            this.tabPage2.Controls.Add(this.button39);
            this.tabPage2.Controls.Add(this.label35);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage2.Size = new System.Drawing.Size(442, 340);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "系统设置";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(111, 49);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(40, 27);
            this.button3.TabIndex = 47;
            this.button3.Text = "修改";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox46
            // 
            this.textBox46.Location = new System.Drawing.Point(58, 51);
            this.textBox46.Margin = new System.Windows.Forms.Padding(2);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(42, 21);
            this.textBox46.TabIndex = 46;
            this.textBox46.Text = "2";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(4, 56);
            this.label63.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(53, 12);
            this.label63.TabIndex = 45;
            this.label63.Text = "限制等级";
            // 
            // textBox39
            // 
            this.textBox39.Location = new System.Drawing.Point(350, 266);
            this.textBox39.Margin = new System.Windows.Forms.Padding(2);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(75, 21);
            this.textBox39.TabIndex = 41;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(308, 270);
            this.label57.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(41, 12);
            this.label57.TabIndex = 40;
            this.label57.Text = "自定义";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(305, 294);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(56, 33);
            this.button2.TabIndex = 42;
            this.button2.Text = "刷出";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button56
            // 
            this.button56.Location = new System.Drawing.Point(376, 294);
            this.button56.Margin = new System.Windows.Forms.Padding(2);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(56, 33);
            this.button56.TabIndex = 43;
            this.button56.Text = "自定义";
            this.button56.UseVisualStyleBackColor = true;
            this.button56.Click += new System.EventHandler(this.button56_Click);
            // 
            // listBox8
            // 
            this.listBox8.FormattingEnabled = true;
            this.listBox8.ItemHeight = 12;
            this.listBox8.Items.AddRange(new object[] {
            "知了先锋",
            "知了小王",
            "知了王",
            "千年知了王",
            "12生肖",
            "七仙女",
            "天命之战",
            "年兽",
            "邪恶年兽",
            "邪恶年兽·黛",
            "星宿",
            "精灵",
            "精灵王",
            "150级BOSS",
            "100级BOSS",
            "50级BOSS"});
            this.listBox8.Location = new System.Drawing.Point(310, 14);
            this.listBox8.Margin = new System.Windows.Forms.Padding(2);
            this.listBox8.Name = "listBox8";
            this.listBox8.Size = new System.Drawing.Size(120, 244);
            this.listBox8.TabIndex = 44;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button13);
            this.groupBox1.Controls.Add(this.listBox1);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(156, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(142, 322);
            this.groupBox1.TabIndex = 39;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "玩家处理";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(16, 275);
            this.button13.Margin = new System.Windows.Forms.Padding(2);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(118, 33);
            this.button13.TabIndex = 6;
            this.button13.Text = "执行操作";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 12;
            this.listBox1.Items.AddRange(new object[] {
            "解封(账号)",
            "强制下线",
            "取ID账号",
            "退出战斗",
            "封禁账号",
            "清除摆摊",
            "清空背包",
            "限制喊话",
            "删除角色",
            "解除限制喊话"});
            this.listBox1.Location = new System.Drawing.Point(16, 44);
            this.listBox1.Margin = new System.Windows.Forms.Padding(2);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 220);
            this.listBox1.TabIndex = 5;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(54, 19);
            this.textBox6.Margin = new System.Windows.Forms.Padding(2);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(82, 21);
            this.textBox6.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 22);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 3;
            this.label9.Text = "玩家ID";
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(84, 169);
            this.button35.Margin = new System.Windows.Forms.Padding(2);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(68, 32);
            this.button35.TabIndex = 38;
            this.button35.Text = "更新NPC数据";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button59
            // 
            this.button59.Location = new System.Drawing.Point(12, 291);
            this.button59.Margin = new System.Windows.Forms.Padding(2);
            this.button59.Name = "button59";
            this.button59.Size = new System.Drawing.Size(68, 32);
            this.button59.TabIndex = 38;
            this.button59.Text = "更新技能数据";
            this.button59.UseVisualStyleBackColor = true;
            this.button59.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button58
            // 
            this.button58.Location = new System.Drawing.Point(84, 291);
            this.button58.Margin = new System.Windows.Forms.Padding(2);
            this.button58.Name = "button58";
            this.button58.Size = new System.Drawing.Size(68, 32);
            this.button58.TabIndex = 38;
            this.button58.Text = "更新商城数据";
            this.button58.UseVisualStyleBackColor = true;
            this.button58.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(12, 250);
            this.button19.Margin = new System.Windows.Forms.Padding(2);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(68, 32);
            this.button19.TabIndex = 38;
            this.button19.Text = "更新刷新怪数据";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button57
            // 
            this.button57.Location = new System.Drawing.Point(84, 250);
            this.button57.Margin = new System.Windows.Forms.Padding(2);
            this.button57.Name = "button57";
            this.button57.Size = new System.Drawing.Size(68, 32);
            this.button57.TabIndex = 38;
            this.button57.Text = "更新召唤兽数据";
            this.button57.UseVisualStyleBackColor = true;
            this.button57.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(84, 128);
            this.button20.Margin = new System.Windows.Forms.Padding(2);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(68, 32);
            this.button20.TabIndex = 38;
            this.button20.Text = "更新物品数据";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(84, 210);
            this.button36.Margin = new System.Windows.Forms.Padding(2);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(68, 32);
            this.button36.TabIndex = 38;
            this.button36.Text = "更新怪物数据";
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(12, 210);
            this.button12.Margin = new System.Windows.Forms.Padding(2);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(68, 32);
            this.button12.TabIndex = 38;
            this.button12.Text = "更新传送圈数据";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(12, 169);
            this.button11.Margin = new System.Windows.Forms.Padding(2);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(68, 32);
            this.button11.TabIndex = 38;
            this.button11.Text = "更新地图数据";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(12, 128);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(68, 32);
            this.button5.TabIndex = 38;
            this.button5.Text = "更新变身卡数据";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.Switch_Click);
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(58, 14);
            this.textBox22.Margin = new System.Windows.Forms.Padding(2);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(31, 21);
            this.textBox22.TabIndex = 12;
            this.textBox22.Text = "2";
            this.textBox22.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // button40
            // 
            this.button40.Location = new System.Drawing.Point(28, 90);
            this.button40.Margin = new System.Windows.Forms.Padding(2);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(104, 34);
            this.button40.TabIndex = 27;
            this.button40.Text = "更新全部数据";
            this.button40.UseVisualStyleBackColor = true;
            this.button40.Click += new System.EventHandler(this.Switch_Click);
            // 
            // button39
            // 
            this.button39.Location = new System.Drawing.Point(105, 8);
            this.button39.Margin = new System.Windows.Forms.Padding(2);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(46, 27);
            this.button39.TabIndex = 6;
            this.button39.Text = "修改";
            this.button39.UseVisualStyleBackColor = true;
            this.button39.Click += new System.EventHandler(this.button39_Click);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(3, 18);
            this.label35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(53, 12);
            this.label35.TabIndex = 3;
            this.label35.Text = "经验倍率";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.消息处理);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(1, 178);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(450, 366);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(442, 340);
            this.tabPage1.TabIndex = 11;
            this.tabPage1.Text = "定制灵饰";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label33);
            this.groupBox3.Controls.Add(this.button29);
            this.groupBox3.Controls.Add(this.checkBox11);
            this.groupBox3.Controls.Add(this.textBox20);
            this.groupBox3.Controls.Add(this.groupBox10);
            this.groupBox3.Controls.Add(this.groupBox9);
            this.groupBox3.Controls.Add(this.groupBox8);
            this.groupBox3.Controls.Add(this.groupBox6);
            this.groupBox3.Controls.Add(this.groupBox7);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.comboBox16);
            this.groupBox3.Controls.Add(this.comboBox12);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Location = new System.Drawing.Point(4, 14);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(435, 324);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "定制灵饰";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.ForeColor = System.Drawing.Color.Red;
            this.label33.Location = new System.Drawing.Point(55, 298);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 12);
            this.label33.TabIndex = 3;
            this.label33.Text = "玩家ID";
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(313, 287);
            this.button29.Margin = new System.Windows.Forms.Padding(2);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(100, 32);
            this.button29.TabIndex = 5;
            this.button29.Text = "定制灵饰";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(23, 106);
            this.checkBox11.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(96, 16);
            this.checkBox11.TabIndex = 5;
            this.checkBox11.Text = "是否超级简易";
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(120, 294);
            this.textBox20.Margin = new System.Windows.Forms.Padding(2);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(107, 21);
            this.textBox20.TabIndex = 4;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label31);
            this.groupBox10.Controls.Add(this.label32);
            this.groupBox10.Controls.Add(this.comboBox11);
            this.groupBox10.Controls.Add(this.textBox19);
            this.groupBox10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox10.Location = new System.Drawing.Point(302, 186);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox10.Size = new System.Drawing.Size(129, 94);
            this.groupBox10.TabIndex = 0;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "附加属性4";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label31.Location = new System.Drawing.Point(3, 71);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(29, 12);
            this.label31.TabIndex = 3;
            this.label31.Text = "数值";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label32.Location = new System.Drawing.Point(3, 28);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(29, 12);
            this.label32.TabIndex = 3;
            this.label32.Text = "类型";
            // 
            // comboBox11
            // 
            this.comboBox11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox11.FormattingEnabled = true;
            this.comboBox11.Items.AddRange(new object[] {
            "",
            "气血回复效果",
            "气血",
            "防御",
            "抗法术暴击等级",
            "格挡值",
            "法术防御",
            "抗物理暴击等级",
            "固定伤害",
            "法术伤害",
            "伤害",
            "封印命中等级",
            "法术暴击等级",
            "物理暴击等级",
            "狂暴等级",
            "穿刺等级",
            "法术伤害结果",
            "治疗能力",
            "速度"});
            this.comboBox11.Location = new System.Drawing.Point(34, 28);
            this.comboBox11.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox11.Name = "comboBox11";
            this.comboBox11.Size = new System.Drawing.Size(62, 20);
            this.comboBox11.TabIndex = 4;
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(34, 67);
            this.textBox19.Margin = new System.Windows.Forms.Padding(2);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(62, 21);
            this.textBox19.TabIndex = 4;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label27);
            this.groupBox9.Controls.Add(this.label28);
            this.groupBox9.Controls.Add(this.comboBox10);
            this.groupBox9.Controls.Add(this.textBox18);
            this.groupBox9.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox9.Location = new System.Drawing.Point(149, 186);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox9.Size = new System.Drawing.Size(129, 94);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "附加属性3";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label27.Location = new System.Drawing.Point(4, 71);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(29, 12);
            this.label27.TabIndex = 3;
            this.label27.Text = "数值";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label28.Location = new System.Drawing.Point(4, 28);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(29, 12);
            this.label28.TabIndex = 3;
            this.label28.Text = "类型";
            // 
            // comboBox10
            // 
            this.comboBox10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Items.AddRange(new object[] {
            "",
            "气血回复效果",
            "气血",
            "防御",
            "抗法术暴击等级",
            "格挡值",
            "法术防御",
            "抗物理暴击等级",
            "固定伤害",
            "法术伤害",
            "伤害",
            "封印命中等级",
            "法术暴击等级",
            "物理暴击等级",
            "狂暴等级",
            "穿刺等级",
            "法术伤害结果",
            "治疗能力",
            "速度"});
            this.comboBox10.Location = new System.Drawing.Point(35, 28);
            this.comboBox10.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(62, 20);
            this.comboBox10.TabIndex = 4;
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(35, 67);
            this.textBox18.Margin = new System.Windows.Forms.Padding(2);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(62, 21);
            this.textBox18.TabIndex = 4;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label24);
            this.groupBox8.Controls.Add(this.label26);
            this.groupBox8.Controls.Add(this.comboBox6);
            this.groupBox8.Controls.Add(this.textBox16);
            this.groupBox8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox8.Location = new System.Drawing.Point(8, 186);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox8.Size = new System.Drawing.Size(129, 94);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "附加属性2";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label24.Location = new System.Drawing.Point(4, 71);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(29, 12);
            this.label24.TabIndex = 3;
            this.label24.Text = "数值";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label26.Location = new System.Drawing.Point(4, 28);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(29, 12);
            this.label26.TabIndex = 3;
            this.label26.Text = "类型";
            // 
            // comboBox6
            // 
            this.comboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Items.AddRange(new object[] {
            "",
            "气血回复效果",
            "气血",
            "防御",
            "抗法术暴击等级",
            "格挡值",
            "法术防御",
            "抗物理暴击等级",
            "固定伤害",
            "法术伤害",
            "伤害",
            "封印命中等级",
            "法术暴击等级",
            "物理暴击等级",
            "狂暴等级",
            "穿刺等级",
            "法术伤害结果",
            "治疗能力",
            "速度"});
            this.comboBox6.Location = new System.Drawing.Point(37, 28);
            this.comboBox6.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(62, 20);
            this.comboBox6.TabIndex = 4;
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(35, 67);
            this.textBox16.Margin = new System.Windows.Forms.Padding(2);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(62, 21);
            this.textBox16.TabIndex = 4;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.comboBox5);
            this.groupBox6.Controls.Add(this.textBox14);
            this.groupBox6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox6.Location = new System.Drawing.Point(302, 28);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox6.Size = new System.Drawing.Size(129, 94);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "附加属性1";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label20.Location = new System.Drawing.Point(3, 58);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(29, 12);
            this.label20.TabIndex = 3;
            this.label20.Text = "数值";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label21.Location = new System.Drawing.Point(3, 16);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(29, 12);
            this.label21.TabIndex = 3;
            this.label21.Text = "类型";
            // 
            // comboBox5
            // 
            this.comboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Items.AddRange(new object[] {
            "",
            "气血回复效果",
            "气血",
            "防御",
            "抗法术暴击等级",
            "格挡值",
            "法术防御",
            "抗物理暴击等级",
            "固定伤害",
            "法术伤害",
            "伤害",
            "封印命中等级",
            "法术暴击等级",
            "物理暴击等级",
            "狂暴等级",
            "穿刺等级",
            "法术伤害结果",
            "治疗能力",
            "速度"});
            this.comboBox5.Location = new System.Drawing.Point(34, 14);
            this.comboBox5.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(62, 20);
            this.comboBox5.TabIndex = 4;
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(34, 55);
            this.textBox14.Margin = new System.Windows.Forms.Padding(2);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(62, 21);
            this.textBox14.TabIndex = 4;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label22);
            this.groupBox7.Controls.Add(this.label25);
            this.groupBox7.Controls.Add(this.comboBox8);
            this.groupBox7.Controls.Add(this.textBox36);
            this.groupBox7.ForeColor = System.Drawing.Color.Red;
            this.groupBox7.Location = new System.Drawing.Point(149, 28);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox7.Size = new System.Drawing.Size(129, 94);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "主属性";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(4, 58);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(29, 12);
            this.label22.TabIndex = 3;
            this.label22.Text = "数值";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(4, 16);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(29, 12);
            this.label25.TabIndex = 3;
            this.label25.Text = "类型";
            // 
            // comboBox8
            // 
            this.comboBox8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Items.AddRange(new object[] {
            "封印命中等级",
            "抵抗封印等级",
            "速度",
            "伤害",
            "防御",
            "法术伤害",
            "法术防御"});
            this.comboBox8.Location = new System.Drawing.Point(35, 14);
            this.comboBox8.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(61, 20);
            this.comboBox8.TabIndex = 4;
            // 
            // textBox36
            // 
            this.textBox36.Location = new System.Drawing.Point(34, 55);
            this.textBox36.Margin = new System.Windows.Forms.Padding(2);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(62, 21);
            this.textBox36.TabIndex = 4;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.Color.Red;
            this.label29.Location = new System.Drawing.Point(12, 73);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(29, 12);
            this.label29.TabIndex = 3;
            this.label29.Text = "等级";
            // 
            // comboBox16
            // 
            this.comboBox16.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox16.FormattingEnabled = true;
            this.comboBox16.Items.AddRange(new object[] {
            "60",
            "80",
            "100",
            "120",
            "140",
            "160"});
            this.comboBox16.Location = new System.Drawing.Point(44, 70);
            this.comboBox16.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox16.Name = "comboBox16";
            this.comboBox16.Size = new System.Drawing.Size(68, 20);
            this.comboBox16.TabIndex = 4;
            // 
            // comboBox12
            // 
            this.comboBox12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox12.FormattingEnabled = true;
            this.comboBox12.Items.AddRange(new object[] {
            "手镯",
            "佩饰",
            "戒指",
            "耳饰"});
            this.comboBox12.Location = new System.Drawing.Point(44, 39);
            this.comboBox12.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox12.Name = "comboBox12";
            this.comboBox12.Size = new System.Drawing.Size(68, 20);
            this.comboBox12.TabIndex = 4;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(12, 42);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(29, 12);
            this.label30.TabIndex = 3;
            this.label30.Text = "类型";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(357, 31);
            this.textBox4.Margin = new System.Windows.Forms.Padding(2);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(79, 21);
            this.textBox4.TabIndex = 8;
            this.textBox4.Text = "9097";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(306, 34);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 9;
            this.label8.Text = "端口";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(357, 56);
            this.textBox5.Margin = new System.Windows.Forms.Padding(2);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(79, 21);
            this.textBox5.TabIndex = 10;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(306, 58);
            this.label38.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 12);
            this.label38.TabIndex = 11;
            this.label38.Text = "验证码";
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(452, 546);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox17);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label7);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "笑傲管理工具";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.消息处理.ResumeLayout(false);
            this.消息处理.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

#endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button4;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Packet;
        private System.Windows.Forms.DataGridViewTextBoxColumn numberIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn identificationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nmaeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConnectTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn UserState;
        private System.Windows.Forms.DataGridViewTextBoxColumn IP;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        public System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.ComboBox comboBox15;
        private System.Windows.Forms.ComboBox comboBox14;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.CheckedListBox checkedListBox2;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox comboBox9;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage 消息处理;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox5;
        public System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button59;
        private System.Windows.Forms.Button button58;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button57;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.ListBox listBox4;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.ListBox listBox8;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox comboBox11;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox comboBox10;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox comboBox16;
        private System.Windows.Forms.ComboBox comboBox12;
        private System.Windows.Forms.Label label30;
        public System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Button button10;
    }
}

