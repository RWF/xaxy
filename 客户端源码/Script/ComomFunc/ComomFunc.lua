-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-24 01:56:55

-- 通用函数
 local ffi = require("ffi")
  ffi.cdef[[
    const char* 数据加密(const char *qq,const char *ww);
  ]]

ffi.cdef [[
  //修改窗口图标
  void*   LoadImageA(int,const char*,int,int,int,int);
  int   SendMessageA(int,int,int,void*);
  //取硬盘序列号
  void*   CreateFileA(const char*,int,int,void*,int,int,void*);
  bool    DeviceIoControl(void*,int,void*,int,void*,int,void*,void*);
  bool  CloseHandle(void*);

  //取窗口信息
  int GetWindowRect(int,void*);
  //取剪贴板
  int      OpenClipboard(unsigned);
  void*    GetClipboardData(unsigned);
  bool     CloseClipboard();
  void*    GlobalLock(void*);
  int      GlobalUnlock(void*);
  size_t   GlobalSize(void*);
  //置剪贴板
  bool  EmptyClipboard();
  void*   GlobalAlloc(unsigned, unsigned);
  void*   GlobalFree(void*);
  void*   lstrcpy(void*,const char*);
  void*   SetClipboardData(unsigned,void*);
  //输出
  int printf(const char *fmt, ...);
  //读配置
  int GetPrivateProfileStringA(const char*, const char*, const char*, const char*, unsigned, const char*);
  //写配置
  bool WritePrivateProfileStringA(const char*, const char*, const char*, const char*);
  //设置窗口样式
  void SetWindowLongA(int ,int ,int );
  //取MD5
  typedef struct {
      unsigned long i[2]; /* number of _bits_ handled mod 2^64 */
      unsigned long buf[4]; /* scratch buffer */
      unsigned char in[64]; /* input buffer */
      unsigned char digest[16]; /* actual digest after MD5Final call */
  } MD5_CTX;
  void MD5Init(MD5_CTX *);
  void MD5Update(MD5_CTX *, const char *, unsigned int);
  void MD5Final(MD5_CTX *);
  //信息框
  int MessageBoxA(void *, const char*, const char*, int);
  //延迟
  void Sleep(int);
  //文件是否存在
  int _access(const char*, int);
  //打开网站
  void *ShellExecuteA(void*, const char *, const char*, const char*, const char*, int);
  //复制文件
  bool CopyFileA(const char*,const char*,bool );
  //读注册表
  long RegOpenKeyExA(unsigned,const char*,unsigned,unsigned,unsigned*);
  long RegQueryValueExA(unsigned,const char*,unsigned*,unsigned*,char*,unsigned*);
  long RegCloseKey(unsigned);
  //改窗口标题
  //int SetWindowTextA(int,const char*);
  //闪烁窗口
  int FlashWindow(int,bool);
]]
--local shell = ffi.load("shell32")




local advapi32 = ffi.load("advapi32.dll")
function GetColour(n)
  local Colour;
   if n >= 0 and n <10 then--白
    Colour=0xFFFFFFFF
   elseif n >= 10 and n <20 then--绿
    Colour=4278255360
   elseif n >= 20 and n <30 then--蓝
    Colour=0xFF03A89E
    elseif n >= 30 and n <40 then--紫
    Colour=0xFFFFFF00
    
  elseif n >= 40 and n <50 then--黄
    Colour=0xFFFFFF00
  elseif n >= 50 and n <60 then--金
   
    Colour=0xFFFFF275
    

    elseif n >= 60 and n <70 then--红
    Colour=4294901760
   else 
    Colour=0xFFF729A2  --粉红
   end
   return Colour
end
function GetColour1(n)
  local Colour;
   if n >= 0 and n <10 then
    Colour="强化1"
   elseif n >= 10 and n <20 then
    Colour="强化2"
   elseif n >= 20 and n <30 then
    Colour="强化3"
   elseif n >= 30 and n <40 then
    Colour="Y"
    
   elseif n >= 40 and n <50 then
    Colour="强化5"
    
   elseif n >= 50 and n <60 then
    Colour="强化6"
    
   elseif n >= 60 and n <70 then
    Colour="强化7"
   else 
    Colour="强化8"
   end
   return Colour
end
function 取窗口信息()
  local data = ffi.new('int[4]')
  ffi.C.GetWindowRect(引擎.取窗口句柄(),data)
  rect = {
    Left  = data[0],
    Top   = data[1],
    Right   = data[2],
    Bottom  = data[3]
  }
  return rect
end
function 判断是否数组(是否数组)
  if type(是否数组) == "table" then
      return true
  end
  return false
end
function 写配置(文件,节点,名称,值)
  return ffi.C.WritePrivateProfileStringA(节点,名称,tostring(值),文件)
end
function 分割文本2(str,delimiter)
    local dLen = string.len(delimiter)
    local newDeli = ''
    for i=1,dLen,1 do
        newDeli = newDeli .. "["..string.sub(delimiter,i,i).."]"
    end
    local locaStart,locaEnd = string.find(str,newDeli)
    local arr = {}
    local n = 1
    while locaStart ~= nil
    do
        if locaStart>0 then
            arr[n] = string.sub(str,1,locaStart-1)
            n = n + 1
        end
        str = string.sub(str,locaEnd+1,string.len(str))
        locaStart,locaEnd = string.find(str,newDeli)
    end
    if str ~= nil then
        arr[n] = str
    end
    return arr
end
function 取剪贴板()
  local text = ""
  local ok1    = ffi.C.OpenClipboard(0)
  local handle = ffi.C.GetClipboardData(1)
  if handle ~= nil then
    local size   = ffi.C.GlobalSize( handle )
    local mem    = ffi.C.GlobalLock( handle )
    text   = ffi.string( mem, size -1)
    local ok     = ffi.C.GlobalUnlock( handle )
  end
  local ok3    = ffi.C.CloseClipboard()
  return text
end

function 置剪贴板(txt)
  if txt then
    local ok1 = ffi.C.OpenClipboard(0)
    local ok2 = ffi.C.EmptyClipboard() --清空
    local handle = ffi.C.GlobalAlloc(66, #txt+1)
    if handle ~= nil then
      local mem = ffi.C.GlobalLock(handle)
      ffi.copy(mem, txt)
      local ok3 = ffi.C.GlobalUnlock(handle)
      handle = ffi.C.SetClipboardData(1, mem)
    end
    local ok4 = ffi.C.CloseClipboard()
  end
end
function 输出(...)
  ffi.C.printf(...)
end


function 取数组长度(数组)

 local 临时计数=0
 for n, v in pairs(数组) do

     临时计数=临时计数+1
     end

 return 临时计数
 end

--======================================================================--

--======================================================================--
local tonumber = tonumber
local setmetatable = setmetatable
local byte = string.byte
local pairs = pairs
local lshift = bit.lshift
local insert = table.insert
local byte = string.byte
local sub = string.sub
local randomseed = math.randomseed
local number = tonumber
local string1 = tostring
local time = os.time()
local sort = table.sort
local random = math.random
function ARGB(a,r,g,b)
  return (lshift(a,24) + lshift(r,16) + lshift(g,8) + b)
end

function 分割字符(str,tv)
  local t = tv or {}
  local i = 1
  local ascii = 0
  while true do
    ascii = byte(str, i)
    if ascii then
      if ascii < 127 then
        insert(t,sub(str, i, i))
        i = i+1
      else
        insert(t,sub(str, i, i+1))
          i = i+2
      end
    else
        break
    end
  end
  return t
end

function random_array(arr)
  randomseed(number(string1(time):reverse():sub(1,6)))
    local tmp, index
    for i=1, #arr-1 do
        index = random(i, #arr)
        if i ~= index then
            tmp = arr[index]
            arr[index] = arr[i]
            arr[i] = tmp
        end
    end
    return arr
end


local xys = 生成XY
local pi = math.pi
local pi2 = pi*2
local sqrt = math.sqrt
local pow = math.pow
local atan = math.atan
local deg = math.deg
local floor = math.floor
local cos = math.cos
local sin = math.sin
local insert = table.insert
local remove = table.remove
local random = 引擎.取随机整数
local with = 引擎.宽度2
local hegt = 引擎.高度2
local withs = 引擎.宽度
local hegts = 引擎.高度







function tablefor(table)
  local mfc = {}
  for i=1,#table do
    for j=i+1,#table do
      if(table[i] == table[j]) then
        table[i] = - 1
      end
    end
  end
  k = 1;
  for i=1, #table do
    if (table[k] == -1) then
      remove(table, k);k=k - 1
    end
    k=k+1
  end
  for j=1,#table do
    insert(mfc,table[j])
  end
end

local function chsize(char)
      if not char then
         return 0
     elseif char > 240 then
         return 4
     elseif char > 225 then
         return 3
     elseif char > 192 then
         return 2
     else
         return 1
     end
 end

function 随机表(表,限制)
  if #表 <= 0 then
    return
  end
  local a=表
  local b={}
  限制 = 限制 or 0
  while #a > 限制 do
    local c=random(1,#a)
    insert(b,a[c])
    remove(a,c)
  end
  return 表
end


function 删除重复(key)
  local k;
  for i=1,#key do
    for j=i+1,#key do
      if(key[i] == key[j]) then
        key[i] = - 1
      end
    end
  end
  k = 1;
  for i=1, #key do
    if (key[k] == -1) then
      table.remove(key, k);k=k - 1
    end
    k=k+1
  end
  k = nil;
  return key
end


function 取文本中间(str, startChar, numChars)
    local startIndex = 1
     while startChar > 1 do
         local char = byte(str, startIndex)
         startIndex = startIndex + chsize(char)
         startChar = startChar - 1
     end

     local currentIndex = startIndex

     while numChars > 0 and currentIndex <= #str do
        local char = byte(str, currentIndex)
        currentIndex = currentIndex + chsize(char)
         numChars = numChars -1
     end
     return str:sub(startIndex, currentIndex - 1)
 end


function 取数组内容(table,txt)
  for i,v in pairs(table) do
    if v == txt then
      return true
    end
  end
  return false
end
function 取表数量(s)

 local s1=0

  for n, v in pairs(s) do
    if s[n] ~= nil then
      s1 = s1 + 1
    end
  end

  return s1

end
function 取四方向(j)
  local f = 0
  if j < 90 then
    f =0
  elseif j<180 then
    f= 1
  elseif j<270 then
    f= 2
  elseif j<360 then
    f= 3
  end
  return f
end


function 日常任务(id)
  if id == "抓鬼" or id =="鬼王" or id =="帮派玄武"    or id =="官职"or id =="迷宫" or id =="封妖"or id =="师门守卫" 
    or id =="除暴安良" or id == "飞贼"  or id == "地煞"or id == "天罡" or id == "宝图" or id == "乌鸡2" or id == "乌鸡3" 
    or id == "乌鸡4" or id == "车迟2" or id == "车迟3" or id == "车迟4" or id == "车迟5" or id == "天罡星69" or id == "天罡星2" 
    or id == "天罡星3" or id == "天罡星4" or id == "天罡星5" or id == "天罡星6"   then
    return  true
  else
    return  false
  end
end
-- function 取画面坐标(x, y, w, h)
--  local w2 = 引擎.宽度2
--  local h2 = 引擎.高度2
--  local rx = 0
--  local ry = 0

--  if w2 < x and x < w - w2 then
--    rx = -(x - w2)
--  elseif x <= w2 then
--    rx = 0
--  elseif x >= w - w2 then
--    rx = -(w - 引擎.宽度)
--  end

--  if h2 < y and y < h - h2 then
--    ry = -(y - h2)
--  elseif y <= h2 then
--    ry = 0
--  elseif y >= h - h2 then
--    ry = -(h - 引擎.高度)
--  end

--  return 生成XY(rx, ry)
-- end
function 取画面坐标(x,y,w,h,yd) --人物坐标,地图宽高
  local w2,h2 = 全局游戏宽度/2,全局游戏高度/2--窗口宽高的一半
  local rx,ry = 0,0
  if (x>w2 and x<w-w2) then
    rx = -(x-w2)
  elseif x<=w2 then
    rx=0
  elseif x>=w-w2 then
    rx=-(w-全局游戏宽度)
  end
  if (y>h2 and y<h-h2) then
    ry = -(y-h2)
  elseif y<=h2 then
    ry=0
  elseif y>=h-h2 then
    ry=-(h-全局游戏高度)
  end
  if w < 全局游戏宽度 then
    rx = 全局游戏宽度 - w
  end
  if h < 全局游戏高度 then
    ry = 全局游戏高度 - h
  end
  return 生成XY(floor(rx),floor(ry))
end
function 取八方向(角,t)
  local 方向 = 0
  if(角 > 157 and 角 < 203) then
    方向 = 5 --"左"
  elseif(角 >202 and 角 < 248) then
    方向 = 2 --"左上"
  elseif(角 > 247 and 角 < 293) then
    方向 = 6 --"上"
  elseif(角 > 292 and 角 < 338) then
    方向 = 3 --"右上"
  elseif(角 > 337 or 角 < 24 ) then
    方向 = 7        --"右"
  elseif( 角 > 23 and 角 < 69 ) then
    方向 = 0       --"右下"
  elseif(角 > 68 and 角 < 114 )then
    方向 = 4 --"下"
  elseif(角 > 113 ) then
    方向 = 1 --"左下"
  end
  if t then
    方向 = 取四至八方向(方向)
  end
  return 方向
 end

function 取符石介绍(数据)
  local jies = ""
    if 数据.名称=="无心插柳" then
      if 数据.等级 == 1 then
          jies="目标的15%受到伤害,溅射的随机目标。"
      elseif 数据.等级 == 2 then
          jies="目标的20%受到伤害,溅射的随机目标。"
      elseif 数据.等级 == 3 then
        jies="目标的25%受到伤害,溅射的随机目标。"
      elseif 数据.等级 == 4 then
        jies="目标的30%受到伤害,溅射的随机目标。"
      end
    elseif 数据.名称=="万丈霞光" then
      if 数据.等级 == 1 then
          jies="增加50点恢复气血效果，包括师门技能和特技"
      elseif 数据.等级 == 2 then
          jies="增加80点恢复气血效果，包括师门技能和特技"
      elseif 数据.等级 == 3 then
        jies="增加120点恢复气血效果，包括师门技能和特技"
      elseif 数据.等级 == 4 then
        jies="增加200点恢复气血效果，包括师门技能和特技"
      end
    elseif 数据.名称=="百步穿杨" then
      if 数据.等级 == 1 then
          jies="物理类攻击时有20%的几率给目标额外造成200点伤害"
      elseif 数据.等级 == 2 then
          jies="物理类攻击时有20%的几率给目标额外造成450点伤害"
      elseif 数据.等级 == 3 then
        jies="物理类攻击时有20%的几率给目标额外造成600点伤害"
      elseif 数据.等级 == 4 then
        jies="物理类攻击时有20%的几率给目标额外造成800点伤害"
      end
  elseif 数据.名称=="隔山打牛" then
      if 数据.等级 == 1 then
          jies="法术攻击时有20%的几率临时提升自身80点灵力"
      elseif 数据.等级 == 2 then
          jies="法术攻击时有20%的几率临时提升自身120点灵力"
      elseif 数据.等级 == 3 then
        jies="法术攻击时有25%的几率临时提升自身170点灵力"
      elseif 数据.等级 == 4 then
        jies="法术攻击时有25%的几率临时提升自身200点灵力"
      end
    elseif 数据.名称=="心随我动" then
      if 数据.等级 == 1 then
          jies="遭受物理类攻击时有25%几率抵挡250点伤害"
      elseif 数据.等级 == 2 then
          jies="遭受物理攻击时有25%几率抵挡400点伤害"
      elseif 数据.等级 == 3 then
        jies="遭受物理类攻击时有25%几率抵挡700点伤害"
      elseif 数据.等级 == 4 then
        jies="遭受物理类攻击时有25%几率抵挡900点伤害"
      end
  elseif 数据.名称=="云随风舞" then
      if 数据.等级 == 1 then
          jies="遭受法术攻击时有20%几率抵挡200点伤害"
      elseif 数据.等级 == 2 then
          jies="遭受法术攻击时有20%几率抵挡400点伤害"
      elseif 数据.等级 == 3 then
        jies="遭受法术攻击时有20%几率抵挡700点伤害"
      elseif 数据.等级 == 4 then
        jies="遭受法术攻击时有20%几率抵挡800点伤害"
      end
  elseif 数据.名称=="无懈可击" then
        jies="提升自身30点防御力"
    elseif 数据.名称=="望穿秋水" then
        jies="提升自身30点灵力"
    elseif 数据.名称=="万里横行" then
        jies="提升自身40点伤害"
    elseif 数据.名称=="日落西山" then
        jies="提升自身40点速度"
    elseif 数据.名称=="网罗乾坤" then
    if 数据.等级 == 1 then
    jies="使用天罗地网时，增加人物等级/2的伤害"
    elseif 数据.等级 == 2 then
    jies="使用天罗地网时，增加人物等级/1.5的伤害"
    elseif 数据.等级 == 3 then
    jies="使用天罗地网时，增加人物等级的伤害"
    end
    elseif 数据.名称=="石破天惊" then
      if 数据.等级 == 1 then
          jies="使用落雷符时增加人物等级/2的伤害，装备该组合时降低5%的防御"
      elseif 数据.等级 == 2 then
          jies="使用落雷符时增加人物等级/1.5的伤害，装备该组合时降低5%的防御"
      elseif 数据.等级 == 3 then
        jies="使用落雷符时增加人物等级的伤害，装备该组合时降低5%的防御"
      end
    elseif 数据.名称=="天雷地火" then
      if 数据.等级 == 1 then
          jies="使用天雷斩、雷霆万钧时增加人物等级/2的伤害，装备该组合时降低5%的防御"
      elseif 数据.等级 == 2 then
          jies="使用天雷斩、雷霆万钧时增加人物等级/1.5的伤害，装备该组合时降低5%的防御"
      elseif 数据.等级 == 3 then
        jies="使用天雷斩、雷霆万钧时增加人物等级的伤害，装备该组合时降低5%的防御"
      end
    elseif 数据.名称=="烟雨飘摇" then
      if 数据.等级 == 1 then
          jies="使用烟雨剑法、飘渺式时增加人物等级/2的伤害，装备该组合时降低5%的防御"
      elseif 数据.等级 == 2 then
          jies="使用烟雨剑法、飘渺式时增加人物等级/1.5的伤害，装备该组合时降低5%的防御"
      elseif 数据.等级 == 3 then
        jies="使用烟雨剑法、飘渺式时增加人物等级的伤害，装备该组合时降低5%的防御"
      end
    elseif 数据.名称=="索命无常" then
      if 数据.等级 == 1 then
          jies="使用阎罗令时，增加人物等级/2的伤害，装备该组合时降低5%的防御"
      elseif 数据.等级 == 2 then
          jies="使用阎罗令时，增加人物等级/1.5的伤害，装备该组合时降低5%的防御"
      elseif 数据.等级 == 3 then
        jies="使用阎罗令时，增加人物等级的伤害，装备该组合时降低5%的防御"
      end
    elseif 数据.名称=="行云流水" then
      if 数据.等级 == 1 then
          jies="使用五行法术时，增加人物等级/2的伤害，装备该组合时降低5%的防御"
      elseif 数据.等级 == 2 then
          jies="使用五行法术时，增加人物等级/1.5的伤害，装备该组合时降低5%的防御"
      elseif 数据.等级 == 3 then
        jies="使用五行法术时，增加人物等级的伤害，装备该组合时降低5%的防御"
      end
    elseif 数据.名称=="福泽天下" then
      if 数据.等级 == 1 then
          jies="使用唧唧歪歪时，增加人物等级/2的伤害，装备该组合时降低5%的防御"
      elseif 数据.等级 == 2 then
          jies="使用唧唧歪歪时，增加人物等级/2的伤害，装备该组合时降低5%的防御"
      elseif 数据.等级 == 3 then
        jies="使用唧唧歪歪时，增加人物等级/2的伤害，装备该组合时降低5%的防御"
      end
  elseif 数据.名称=="暗度陈仓" then
    jies="受到物理攻击时，降低3%的所受伤害。"
  elseif 数据.名称=="点石成金" then
    jies="防御时，遭受物理攻击所受到的伤害降低20%"
  elseif 数据.名称=="化敌为友" then
    jies="受到法术攻击时，降低3%的所受伤害。"
  elseif 数据.名称=="百步穿杨" then
    jies="物理类攻击时有20%的几率给目标额外造成850点伤害"
  elseif 数据.名称=="凤舞九天" then
    jies="提升自身500点灵力"
  elseif 数据.名称=="君临天下" then
    jies="提升自身500点伤害"
  elseif 数据.名称=="势如破竹" then
    jies="提升自身500点敏捷"
  elseif 数据.名称=="不动明王" then
    jies="提升自身300点防御和500点体质"
    elseif 数据.名称=="高山流水" then
      if 数据.等级 == 1 then
          jies="增加人物等级/3+30的法术伤害。(该组合全身只有一件装备起效)"
      elseif 数据.等级 == 2 then
          jies="增加人物等级/2+30的法术伤害。(该组合全身只有一件装备起效)"
      elseif 数据.等级 == 3 then
        jies="增加人物等级+30的法术伤害。(该组合全身只有一件装备起效)"
      end
    elseif 数据.名称=="百无禁忌" then
      if 数据.等级 == 1 then
          jies="提高自身4%对抗封印类技能的能力"
      elseif 数据.等级 == 2 then
          jies="提高自身8%对抗封印类技能的能力"
      elseif 数据.等级 == 3 then
        jies="提高自身12%对抗封印类技能的能力"
      end
   end
  return jies
end
function 计算技能数据(目标技能等级)----------------------------------------
 local 经验={[1]=16,[2]=32,[3]=52,[4]=75,[5]=103,[6]=136,[7]=179,[8]=231,[9]=295,[10]=372,[11]=466,[12]=578,[13]=711,[14]=867,[15]=1049,[16]=1280,[17]=1503,[18]=1780,[19]=2096,[20]=2452,[21]=2854,[22]=3304,[23]=3807,[24]=4364,
 [25]=4983,[26]=5664,[27]=6415,[28]=7238,[29]=8138,[30]=9120,[31]=10188,[32]=11347,[33]=12602,[34]=13959,[35]=15423,[36]=16998,[37]=18692,[38]=20508,[39]=22452,[40]=24532,[41]=26753,[42]=29121,[43]=31642,[44]=34323,
 [45]=37169,[46]=40186,[47]=43388,[48]=46773,[49]=50352,[50]=54132,[51]=58120,[52]=62324,[53]=66750,[54]=71407,[55]=76303,[56]=81444,[57]=86840,[58]=92500,[59]=104640,[60]=111136,[61]=117931,[62]=125031,[63]=132444,
 [64]=140183,[65]=148253,[66]=156666,[67]=156666,[68]=165430,[69]=174556,[70]=184052,[71]=193930,[72]=204198,[73]=214868,[74]=225948,[75]=237449,[76]=249383,[77]=261760,[78]=274589,[79]=287884,[80]=301652,[81]=315908,
 [82]=330662,[83]=345924,[84]=361708,[85]=378023,[86]=394882,[87]=412297,[88]=430280,[89]=448844,[90]=468000,[91]=487760,[92]=508137,[93]=529145,[94]=550796,[95]=573103,[96]=596078,[97]=619735,[98]=644088,[99]=669149,
 [100]=721452,[101]=748722,[102]=776755,[103]=805566,[104]=835169,[105]=865579,[106]=896809,[107]=928876,[108]=961792,[109]=995572,[110]=1030234,[111]=1065190,[112]=1102256,[113]=1139649,[114]=1177983,[115]=1217273,
 [116]=1256104,[117]=1298787,[118]=1341043,[119]=1384320,[120]=1428632,[121]=1473999,[122]=1520435,[123]=1567957,[124]=1616583,[125]=1666328,[126]=1717211,[127]=1769248,[128]=1822456,[129]=1876852,[130]=1932456,[131]=1989284,
 [132]=2047353,[133]=2106682,[134]=2167289,[135]=2229192,[136]=2292410,[137]=2356960,[138]=2422861,[139]=2490132,[140]=2558792,[141]=2628860,[142]=2700356,[143]=2773296,[144]=2847703,[145]=2923593,[146]=3000989,[147]=3079908,
 [148]=3160372,[149]=3242400,[150]=6652022,[151]=6822452,[152]=6996132,[153]=7173104,[154]=7353406,[155]=11305620,[156]=15305620,[157]=22305620,[158]=27305620,[159]=37305620,[160]=45305620,[161]=54305620,[162]=57305620,[163]=60305620,
 [164]=65305620,[165]=70305620,[166]=84305620,[167]=90103220,[168]=98430562,[169]=102436520,[170]=240146920,[171]=244383080,[172]=248668800,[173]=25300432,[174]=257390000,[175]=327282520,[176]=332890920,[177]=338563080,[178]=344299280,
 [179]=408420000,[180]=520984200,[181]=99999999999}
 local 金钱={[1]=6,[2]=12,[3]=19,[4]=28,[5]=38,[6]=51,[7]=67,[8]=86,[9]=110,[10]=139,[11]=174,[12]=216,[13]=266,[14]=325,[15]=393,[16]=472,[17]=563,[18]=667,[19]=786,[20]=919,[21]=1070,[22]=1238,[23]=1426,[24]=1636,[25]=1868,[26]=2124,
 [27]=2404,[28]=2714,[29]=3050,[30]=3420,[31]=3820,[32]=4255,[33]=4725,[34]=5234,[35]=5783,[36]=6374,[37]=7009,[38]=7680,[39]=8419,[40]=9199,[41]=10032,[42]=10920,[43]=11865,[44]=12871,[45]=13938,[46]=15070,[47]=16270,[48]=17540,[49]=18882,
 [50]=20299,[51]=21795,[52]=23371,[53]=25031,[54]=26777,[55]=28613,[56]=30541,[57]=32565,[58]=34687,[59]=36911,[60]=39240,[61]=41676,[62]=44224,[63]=46886,[64]=49666,[65]=52568,[66]=55595,[67]=58749,[68]=62036,[69]=65458,[70]=69019,[71]=72723,
 [72]=76574,[73]=80575,[74]=84730,[75]=89043,[76]=93516,[77]=98160,[78]=102971,[79]=107956,[80]=113119,[81]=118465,[82]=123998,[83]=129721,[84]=135640,[85]=141758,[86]=148080,[87]=154611,[88]=161355,[89]=168316,[90]=175500,[91]=182910,[92]=190551,
 [93]=198429,[94]=206548,[95]=214913,[96]=223529,[97]=232400,[98]=241533,[99]=250931,[100]=260599,[101]=270544,[102]=280770,[103]=291283,[104]=302087,[105]=313188,[106]=324592,[107]=336303,[108]=348328,[109]=360672,[110]=373339,[111]=386337,
 [112]=399671 ,[113]=413346,[114]=427368,[115]=441743,[116]=456477,[117]=471576,[118]=487045,[119]=502891,[120]=519120,[121]=535737,[122]=552749,[123]=570163,[124]=587984,[125]=606218,[126]=624873,[127]=643954,[128]=663468 ,[129]=683421,[130]=703819,
 [131]=724671,[132]=745981,[133]=767757,[134]=790005,[135]=812733,[136]=835947 ,[137]=859653,[138]=883860,[139]=908573 ,[140]=933799 ,[141]=959547 ,[142]=985822,[143]=1012633,[144]=1039986,[145]=1067888 ,[146]=1096347,[147]=1125371,[148]=1154965,
 [149]=1185139,[150]=1215900,[151]=2494508,[152]=2558419,[153]=2623549,[154]=2689914,[155]=2757527,[156]=4239607,[157]=6239607,[158]=8239607,[159]=10239607,[160]=15239607,[161]=18239607,[162]=20239607,[163]=25239607,[164]=30239607,[165]=36239607,
 [166]=40239607,[167]=46022396,[168]=50052309,[169]=53239607,[170]=72044070,[171]=73314900,[172]=74600640,[173]=75901290,[174]=77217001,[175]=98184750,[176]=99867270,[177]=101568930,[178]=103289790,[179]=122526000,[180]=168239607,[181]=99999999999}

  return {经验=经验[目标技能等级]*2,金钱=金钱[目标技能等级]*2}
 end
function 引擎.取金钱颜色(s)
  if  type(s) ~= "number"  then
    return
  end
  if s >=100000000 then
    return -256
  elseif s >=10000000 then
    return -65281
  elseif s >=1000000 then
    return -65536
  elseif s >=100000 then
    return -16737280
  else
    return -16777216
  end
 end
function 取随机数(q, w)
  随机序列 = 随机序列 + 1
  if q == nil or w == nil then
    q = 1
    w = 100
  end
  math.randomseed(tostring(os.clock() * os.time() * 随机序列))
  return math.random(math.floor(q), math.floor(w))
 end
function 分割文本(szFullString, szSeparator)
  if szFullString == "" or szFullString == nil then
    return nil
  end
  local nFindStartIndex = 1
  local nSplitIndex = 1
  local nSplitArray = {}
  while true do
    local nFindLastIndex = string.find(szFullString, szSeparator, nFindStartIndex)

    if not nFindLastIndex then
      nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, string.len(szFullString))

      break
    end
    nSplitArray[nSplitIndex] = string.sub(szFullString, nFindStartIndex, nFindLastIndex - 1)
    nFindStartIndex = nFindLastIndex + string.len(szSeparator)
    nSplitIndex = nSplitIndex + 1
  end

  return nSplitArray
 end
function 取召唤兽武器(mx)
  if mx == "金饶僧" or mx == "捕鱼人"or mx == "道士" or mx == "小花" or mx == "守门天将" or mx == "李靖" or mx == "葫芦宝贝" or  mx == "猫灵兽形"or  mx == "狂豹人形" or  mx == "蝎子精" or
   mx == "猫灵人形" or  mx == "金身罗汉" or  mx =="般若天女"  or  mx == "持国巡守" or  mx == "灵灯侍者" or mx == "增长巡守" or  mx == "真陀护法" or  mx == "进阶白熊" or  mx == "进阶雷鸟人" or
   mx == "进阶风伯" or  mx == "进阶天兵" or  mx =="进阶天将"  or  mx == "进阶碧水夜叉" or  mx == "进阶凤凰" or mx == "进阶蛟龙" or mx == "进阶鲛人" or  mx == "进阶锦毛貂精" or  mx == "进阶千年蛇魅"  or mx =="进阶犀牛将军人形" or
   mx == "进阶星灵仙子" or  mx == "进阶巡游天神" or  mx ==  "进阶百足将军" or  mx == "进阶野猪精" or mx =="进阶吸血鬼"  or  mx == "进阶鬼将"   or mx ==  "进阶炎魔神" or mx == "进阶机关人"  or
   mx == "进阶踏云兽"  or mx == "进阶机关鸟"  or mx == "进阶连驽车"  or mx == "进阶长眉灵猴"  or mx == "进阶巨力神猿"  or mx == "进阶金身罗汉"  or mx == "进阶般若天女"  or mx == "进阶毗舍童子" or mx == "御林军" or mx == "进阶真陀护法"
     or mx == "进阶大力金刚" or mx == "唯美雾中仙"   or mx == "唯美噬天虎" or mx == "唯美大力金刚" or mx == "唯美鬼将" 
     or mx =="进阶小魔头"or mx =="进阶小精灵"or mx =="进阶小仙女"or mx =="进阶小神灵"or mx =="进阶小毛头"or mx =="进阶小丫丫" then
     return  true
   else
      return  false
   end
 end
function 取召唤兽饰品(mx)
 if mx == "野猪"or mx == "羊头怪"or mx == "吸血鬼"or mx == "马面"or mx == "牛头"or mx == "黑山老妖"or mx == "雷鸟人"or
   mx == "古代瑞兽"or mx == "白熊"or mx == "天兵"or mx == "碧水夜叉"or mx == "鲛人"or mx == "雨师"or mx == "蛟龙"or 
   mx == "凤凰"or mx == "锦毛貂精"or mx == "犀牛将军人形"or mx == "犀牛将军兽形" or mx == "如意仙子"or mx == "星灵仙子"or 
   mx == "巡游天神"or mx == "百足将军"or mx == "鼠先锋"or mx == "野猪精" or mx == "镜妖"or mx == "阴阳伞"or mx == "灵符女娲"or 
   mx == "律法女娲"or mx == "幽灵"or mx == "幽萤娃娃"or mx == "画魂"or mx == "鬼将"or mx == "净瓶女娲"or mx == "金铙僧"or 
   mx == "大力金刚"or mx == "夜罗刹" or mx == "灵鹤"or mx == "噬天虎"or mx == "雾中仙"or mx == "炎魔神"or mx == "葫芦宝贝"or 
   mx == "机关人"or mx == "蝎子精"or mx == "狂豹兽形"or mx == "猫灵兽形"or mx == "红萼仙子"or mx == "龙龟"or mx == "巴蛇" or 
   mx == "机关鸟"or mx == "机关兽"or mx == "长眉灵猴"or mx == "混沌兽"or mx == "巨力神猿"or mx == "狂豹人形"or mx == "猫灵人形"or 
   mx == "藤蔓妖"or mx == "修罗傀儡鬼"or mx == "蜃气妖"or mx == "金身罗汉"or mx == "曼珠沙华"or mx == "修罗傀儡妖"or mx == "般若天女"or 
   mx == "持国巡守"or mx == "灵灯侍者"or mx == "增长巡守"or mx == "真陀护法"or mx == "进阶古代瑞兽"or mx == "进阶黑山老妖"or 
   mx == "进阶蝴蝶仙子"or mx == "进阶雷鸟人"or mx == "进阶地狱战神"or mx == "进阶风伯"or mx == "进阶天兵"or mx == "进阶天将"or 
   mx == "进阶蚌精"or mx == "进阶碧水夜叉"or mx == "进阶凤凰"or mx == "进阶雨师"or mx == "进阶蛟龙"or mx == "进阶鲛人"or 
   mx == "进阶千年蛇魅"or mx == "进阶如意仙子"or mx == "进阶犀牛将军人形"or mx == "进阶犀牛将军兽形"or mx == "进阶星灵仙"or 
   mx == "进阶巡游天神"or mx == "进阶芙蓉仙子"or mx == "进阶百足将军"or mx == "进阶镜妖"or mx == "进阶鼠先锋"or mx == "进阶野猪精"or 
   mx == "进阶灵符女娲"or mx == "进阶律法女娲"or mx == "进阶吸血鬼"or mx == "进阶阴阳伞"or mx == "进阶鬼将"or mx == "进阶画魂"or
   mx == "进阶净瓶女娲"or mx == "进阶大力金刚"or mx == "进阶金铙僧"or mx == "进阶噬天虎"or mx == "进阶雾中仙"or mx == "进阶炎魔神"or 
   mx == "进阶机关人"or mx == "进阶龙龟"or mx == "进阶踏云兽"or mx == "进阶蝎子精"or mx == "进阶巴蛇"or mx == "进阶机关鸟"or 
   mx == "进阶机关兽"or mx == "进阶连驽车"or mx == "进阶长眉灵猴"or mx == "进阶混沌兽"or mx == "进阶巨力神猿"or mx == "进阶猫灵人形"or 
   mx == "进阶修罗傀儡鬼"or mx == "进阶金身罗汉"or mx == "进阶修罗傀儡妖"or mx == "进阶般若天女"or mx == "进阶灵灯侍者"or mx == "进阶毗舍童子" or 
   mx == "进阶小魔头"or mx == "进阶小精灵"or mx == "进阶小仙女"or mx == "进阶小神灵"or mx == "进阶小毛头"or mx == "进阶小丫丫" or mx == "猪八戒"or 
   mx == "章鱼"or mx == "山贼"or mx == "泡泡"or mx == "进阶夜罗刹"or mx == "进阶犀牛将军兽"or mx == "进阶曼珠沙华"or mx == "进阶连弩车"or mx == "机关人战车"or 
   mx == "海星"or mx == "风伯"or mx == "碧海夜叉"or mx == "蚌精"or mx == "进阶锦毛貂精"or mx == "进阶星灵仙子"or 
   mx == "进阶幽灵"or mx == "进阶幽萤娃娃"or mx == "进阶灵鹤"or mx == "进阶葫芦宝贝"or mx == "进阶狂豹兽形" or mx == "进阶猫灵兽形"or 
   mx == "进阶红萼仙子"or mx == "进阶狂豹人形"or mx == "进阶藤蔓妖"or mx == "进阶蜃气妖"or mx == "进阶持国巡守"or mx == "进阶增长巡守"or mx == "进阶真陀护法" or mx == "毗舍童子" or mx == "芙蓉仙子" or mx == "进阶灵鹤"  then
     return  true
   else
      return  false
   end
 end
function 取Bgm(map)
  if map == 1209 then
    return 1208
  elseif map == 1137 then
    return 1135
  elseif map == 1511 then
    return 1231
  elseif map == 1123 or map == 1124 then
    return 1122
  elseif map == 1211 then
    return 1210
  elseif map == 1227 then
    return 1226
  elseif map == 1119 or map == 1120 or map == 1121 or map == 1532 then
    return 1118
  end
  return map
end
-------------------
function 生成XY(x,y)
  local f ={}
  f.x = tonumber(x) or 0
  f.y = tonumber(y) or 0
  setmetatable(f,{
  __add = function (a,b)
    return 生成XY(a.x + b.x,a.y + b.y)
  end,
  __sub = function (a,b)
    return 生成XY(a.x - b.x,a.y - b.y)
  end
  })
  return f
end

function 临时时间(时间)
  return os.date("%m/%d %H:%M",时间)
end
 function 测试时间(时间)
  return "[" .. os.date("%Y", 时间) .. "年" .. os.date("%m", 时间) .. "月" .. os.date("%d", 时间) .. "日 " .. os.date("%X", 时间) .. "]"
end


 function 取四至八方向(d)
  n = 0
  if d == 0 or d == 4 then
    n = 0
  elseif d == 1 or d == 5 then
    n = 1
  elseif d == 2 or d == 6 then
    n = 2
  elseif d == 3 or d == 7 then
    n = 3
  end
  return n
end

通信密码表={["40"]=0,["rg"]=1,["hw"]=2,["ru"]=3,["w1"]=4,["v4"]=5,["uf"]=6,["p7"]=7,["wn"]=8,["rv"]=9}
function abs(密码)
  local a = nil
  local b = 0
  local c = 0
  for n = 1, string.len(密码), 1 do
    b = string.sub(密码, n, n)
    for i, v in pairs(通信密码表) do
      if 通信密码表[i] == b + 0 then
        c = i
      end
    end
    if a == nil then
      a = c
    else
      a = a .. c
    end
  end
  return a
end
  local buf = ffi.new("const unsigned char[?]",1024)
function 读配置(文件,节点,名称)
  ffi.C.GetPrivateProfileStringA(节点,名称,"空",buf,1024,文件)
  return ffi.string(buf)
end
function 更改窗口标题(时间)
  同步时间 = os.time()
  游戏时间 = os.time()
  角色移动 = 时间
  全局时间 = 时间
  回收时间 = 回收时间 + 1

  if 回收时间 >= 10 then
 
    collectgarbage("collect")
    
    回收时间 = 0
  end
  if 移动时间 == 0 then
    移动时间 = 时间
  end

  
end

function 生成XY(x, y)
  local f = {
    x = tonumber(x) or 0,
    y = tonumber(y) or 0
  }

  setmetatable(f, {
    __add = function (a, b)
      return 生成XY(a.x + b.x, a.y + b.y)
    end,
    __sub = function (a, b)
      return 生成XY(a.x - b.x, a.y - b.y)
    end
  })

  return f
end

math.pi2 = math.pi * 2

function 取数组最小值(a, c)
  local b = 1
  local e = 1e+20
  local d = false
  for n = 1, #a, 1 do
    if a[n] < e and a[n] ~= c then
      e = a[n]
      b = n
      d = true
    end
  end
  if d == false then
    return 0
  else
    return b
  end
end

function 取两点距离(src, dst)
  return math.sqrt(math.pow(src.x - dst.x, 2) + math.pow(src.y - dst.y, 2))
end
function 取两点距离a(x, y, x1, y1)
  return math.sqrt(math.pow(x - x1, 2) + math.pow(y - y1, 2))
end
function 取两点孤度(src, dst)
  if dst == nil then
    return 0
  end
  if dst.y == src.y and dst.x == src.x then
    return 0
  elseif src.y <= dst.y and dst.x <= src.x then
    return math.pi - math.abs(math.atan((dst.y - src.y) / (dst.x - src.x)))
  elseif dst.y <= src.y and src.x <= dst.x then
    return math.pi2 - math.abs(math.atan((dst.y - src.y) / (dst.x - src.x)))
  elseif dst.y <= src.y and dst.x <= src.x then
    return math.atan((dst.y - src.y) / (dst.x - src.x)) + math.pi
  elseif src.y <= dst.y and src.x <= dst.x then
    return math.atan((dst.y - src.y) / (dst.x - src.x))
  end
end
function 取两点孤度a(x, y, x1, y1)
  if y1 == y and x1 == x then
    return 0
  elseif y <= y1 and x1 <= x then
    return math.pi - math.abs(math.atan((y1 - y) / (x1 - x)))
  elseif y1 <= y and x <= x1 then
    return math.pi2 - math.abs(math.atan((y1 - y) / (x1 - x)))
  elseif y1 <= y and x1 <= x then
    return math.atan((y1 - y) / (x1 - x)) + math.pi
  elseif y <= y1 and x <= x1 then
    return math.atan((y1 - y) / (x1 - x))
  end
end
local ffi = require("ffi")
ffi.cdef("    const char* dll替换文本(const char *qq,const char *ww,const char *ee);\n  ")
function 替换文本(qq, ww, ee)
  if dll文件3 == nil then
    dll文件3 = ffi.load("effi")
  end

  local text = dll文件3.dll替换文本(qq, ww, ee)

  return ffi.string(text)
end
function 取两点角度(src, dst)
  return math.deg(取两点孤度(src, dst))
end
function 取两点角度a(x, y, x1, y1)
  return math.deg(取两点孤度a(x, y, x1, y1))
end
function 取距离坐标(xy, r, a)
  local x1 = 0
  local y1 = 0
  x1 = r * math.cos(a) + xy.x
  y1 = r * math.sin(a) + xy.y

  return 生成XY(math.floor(x1), math.floor(y1))
end
function 取距离坐标a(x, y, r, a)
  local x1 = 0
  local y1 = 0
  x1 = r * math.cos(a) + x
  y1 = r * math.sin(a) + y

  return math.floor(x1), math.floor(y1)
end
function 取移动坐标(src, dst, r)
  local a = 取两点孤度(src, dst)

  return 生成XY(r * math.cos(a) + src.x, r * math.sin(a) + src.y)
end

function 逻辑改变(q)
  if q then
    return false
  else
    return true
  end
end
function 取百分比(q, w)
  if q == nil and w == nil then
    q = 100
    w = 100
  end

  if q == nil then
    q = w
  end

  if w == nil then
    w = q
  end

  初始百分比 = w / 100
  中间百分比 = q / 初始百分比

  return 中间百分比
end


function 角度算方向(角, 变身)
  if 变身 ~= nil and 变身 ~= "" then
    return 角度算四方向(角)
  end
  local 方向 = 0

  if 角 > 157 and 角 < 203 then
    方向 = 5
  elseif 角 > 202 and 角 < 248 then
    方向 = 2
  elseif 角 > 247 and 角 < 293 then
    方向 = 6
  elseif 角 > 292 and 角 < 338 then
    方向 = 3
  elseif 角 > 337 or 角 < 24 then
    方向 = 7
  elseif 角 > 23 and 角 < 69 then
    方向 = 0
  elseif 角 > 68 and 角 < 114 then
    方向 = 4
  elseif 角 > 113 then
    方向 = 1
  end

  return 方向
end
function 角度算四方向(角)
  local 方向 = 0
  if 角 > 0 and 角 < 91 then
    方向 = 0
  elseif 角 > 90 and 角 < 181 then
    方向 = 1
  elseif 角 > 180 and 角 < 271 then
    方向 = 2
  elseif 角 > 270 or 角 == 0 then
    方向 = 3
  end

  return 方向
end

function 无错误取模型(pg,wq,bu)
-- print(pg,wq,bu)
  if not pg then
    return
  end
  local pgs = {}
  -- if 迭代== false and  (pg == "龙太子" or pg == "神天兵"  or pg == "舞天姬" or  pg == "玄彩娥" or pg == "骨精灵" or  pg == "巨魔王" or  pg == "虎头怪"or pg == "逍遥生"or pg == "飞燕女"or pg == "狐美人" ) then
  --   pg = "非迭代_"..pg
  -- end

  if bu or pg == "巫蛮儿"  or pg == "杀破狼" or pg == "羽灵神" or pg == "偃无师"or pg == "桃夭夭"or pg == "鬼潇潇" then
        if wq then
        pg = pg.."_"..wq
        end
  end

  if not ModelData[pg] then
     return 1
      
  end
  
  return ModelData[pg]
end
function 取模型(pg,wq,bu)
-- print(pg,wq,bu)
  if not pg then
    return
  end
  local pgs = {}
  -- if 迭代== false and  (pg == "龙太子" or pg == "神天兵"  or pg == "舞天姬" or  pg == "玄彩娥" or pg == "骨精灵" or  pg == "巨魔王" or  pg == "虎头怪"or pg == "逍遥生"or pg == "飞燕女"or pg == "狐美人" ) then
  --   pg = "非迭代_"..pg
  -- end

  if bu or pg == "巫蛮儿"  or pg == "杀破狼" or pg == "羽灵神" or pg == "偃无师"or pg == "桃夭夭"or pg == "鬼潇潇" then
        if wq then
        pg = pg.."_"..wq
        end
  end

  if not ModelData[pg] then
       error(string.format("模型=[%s]数据表不存在数据",pg)) 
  end
  
  return ModelData[pg]
end
function 取y偏移(头像)
  if 头像 == "杀破狼" then
    return 35
  elseif 头像 == "巫蛮儿" then
    return 19
  elseif 头像 == "羽灵神" then
    return 25
  elseif 头像 == "吸血鬼" then
    return 25
  elseif 头像 == "鬼潇潇" then
    return 1
  elseif 头像 == "桃夭夭" then
    return 58
  elseif 头像 == "偃无师" then
    return 33
  elseif 头像 == "狂豹" or 头像 == "曼珠沙华"  or 头像 == "狂豹人形" then
    return 303
  elseif 头像 == "鲛人" then
    return 27
  elseif 头像 == "犀牛将军_人" then
    return 32
  elseif 头像 == "野猪精" then
    return 25
  elseif 头像 == "修罗傀儡妖" or 头像 == "金身罗汉" then
    return 235
  elseif 头像 == "猫灵人形" then
    return 275
  elseif 头像 == "蔓藤妖花"  or 头像 == "修罗傀儡鬼"then
    return 260
  elseif 头像 == "混沌兽" then
    return 350
  elseif 头像 == "蜃气妖" then
    return 215
  elseif 头像 == "泡泡"  then
    return 20
  elseif 头像 == "福星" or 头像 == "长眉灵猴" or 头像 == "巨力神猿" then
    return 28
  end
  return 0
end

function ReadFile(fileName)
  -- TODO:账号记录.txt
    fileName=程序目录..fileName
    local f = assert( io.open(fileName, 'r'))
    local content = f:read("*all")
    f:close()
    return content
end
function WriteFile(fileName,content)
     fileName=程序目录..fileName
    local f = assert( io.open( fileName, 'w'))
    f:write(content)
    f:close()
end

function 发送数据(a,b,c,d)
  if c == nil then
    c = 1
  end
  if d == nil then
    d = 1
  end
  if b == nil then
    b = ""
  end
  if type(b) == "table" then
    b = table.tostring(b)
  end
  if a ~= nil then
    客户端:发送数据(c, d, a, b)
  end
end

function pwd(as)

  local 临时 =引擎.资源取文件(as..'.was')
  local b=引擎.资源取大小(as..'.was')
  local ddd =require("Script/Resource/动画类1")(临时,b,"MAX.7z",as,缓存)
  引擎.资源释放(临时)
  return ddd

end



function 整秒处理()
  if tp ~= nil and tp.窗口 ~= nil and tp.窗口.挂机系统 ~= nil and tp.窗口.挂机系统.托管数据 ~= nil then
    for n,v in pairs(tp.窗口.挂机系统.托管数据) do
      if v == true then
        tp.窗口.挂机系统:刷新处理(n)
      end
    end
  end
end

function 取场景等级(map)
  if map == 1506 then--"东海湾"
    return 0,9
  elseif map == 1507 then--"东海海底"
    return 2,9
  elseif map == 1508 then--"沉船"
    return 2,12
  elseif map == 1126 then--"东海岩洞"
    return 2,12
  elseif map == 1193 then--"江南野外"
    return 6,16
  elseif map == 1004 then--"大雁塔一层"
    return 8,18
  elseif map == 1005 then--"大雁塔二层"
    return 12,22
  elseif map == 1006 then--"大雁塔三层"
    return 16,26
  elseif map == 1007 then--"大雁塔四层"
    return 20,30
  elseif map == 1008 then--"大雁塔五层"
    return 24,34
  elseif map == 1090 then--"大雁塔六层"
    return 28,38
  elseif map == 1110 then--"大唐国境"
    return 13,23
  elseif map == 1173 then--"大唐境外"
    return 20,30
  elseif map == 1091 then--"长寿郊外"
    return 26,36
  elseif map == 1512 then--"魔王寨"
    return 32,42
  elseif map == 1140 then--"普陀山"
    return 36,46
  elseif map == 1513 then--"盘丝岭"
    return 38,48
  elseif map == 1131 then--"狮驼岭"
    return 50,60
  elseif map == 1514 then--"花果山"
    return 30,40
  elseif map == 1118 then--"海底迷宫一层"
    return 33,43
  elseif map == 1119 then--"海底迷宫二层"
    return 34,44
  elseif map == 1120 then--"海底迷宫三层"
    return 36,46
  elseif map == 1121 then--"海底迷宫四层"
    return 38,48
  elseif map == 1532 then--"海底迷宫五层"
    return 40,50
  elseif map == 1127 then--"地狱迷宫一层"
    return 37,47
  elseif map == 1128 then--"地狱迷宫二层"
    return 40,50
  elseif map == 1129 then--"地狱迷宫三层"
    return 43,53
  elseif map == 1130 then--"地狱迷宫四层"
    return 46,56
  elseif map == 1202 then--"无名鬼城"
    return 96,106
  elseif map == 1174 then--"北俱芦洲"
    return 54,64
  elseif map == 1177 then--"龙窟一层"
    return 58,68
  elseif map == 1178 then--"龙窟二层"
    return 61,71
  elseif map == 1179 then--"龙窟三层"
    return 65,75
  elseif map == 1180 then--"龙窟四层"
    return 68,78
  elseif map == 1181 then--"龙窟五层"
    return 72,82
  elseif map == 1182 then--"龙窟六层"
    return 75,85
  elseif map == 1183 then--"龙窟七层"
    return 80,90
  elseif map == 1186 then--"凤巢一层"
    return 65,75
  elseif map == 1187 then--"凤巢二层"
    return 69,79
  elseif map == 1188 then--"凤巢三层"
    return 73,83
  elseif map == 1189 then--"凤巢四层"
    return 77,87
  elseif map == 1190 then--"凤巢五层"
    return 81,91
  elseif map == 1191 then--"凤巢六层"
    return 85,95
  elseif map == 1192 then--"凤巢七层"
    return 88,98
  elseif map == 1201 then--"女娲神迹"
    return 104,114
  elseif map == 1207 then--"蓬莱仙岛"
    return 129,139
  elseif map == 1203 then--小西天
    return 112,122
  elseif map == 1204 then--"小雷音寺"
    return 118,128
  elseif map == 1114 then--"月宫"
    return 48,58
  elseif map == 1231 then--"蟠桃园"
    return 150,160
  elseif map == 1221 then--"墨家禁地"
    return 140,150
  elseif map == 1042 then--"解阳山"
    return 83,93
  elseif map == 1041 then--"子母河底"
    return 73,83
  elseif map == 1210 then--"麒麟山"
    return 93,103
  elseif map == 1228 then--"碗子山"
    return 129,139
  elseif map == 1229 then--"波月洞"
    return 150,160
  elseif map == 1233 then--"柳林坡"
    return 153,163
  elseif map == 1232 then--"比丘国"
    return 155,165
  elseif map == 1242 then--须弥东界
    return 173,175
  elseif map == 1605 then--天鸣洞天
    return 50,55
  elseif map == 1223 then--观星台
    return 50,55
  elseif map == 1876 then--南岭山
    return 30,45
  elseif map == 1920 then--"凌云渡"
    return 175,180
  --==================
  end
end

function print_r ( t )
    local print_r_cache={}
    local function sub_print_r(t,indent)
        if (print_r_cache[tostring(t)]) then
            print(indent.."*"..tostring(t))
        else
            print_r_cache[tostring(t)]=true
            if (type(t)=="table") then
                for pos,val in pairs(t) do
                    if (type(val)=="table") then
                        print(indent.."["..pos.."] => "..tostring(t).." {")
                        sub_print_r(val,indent..string.rep(" ",string.len(pos)+8))
                        print(indent..string.rep(" ",string.len(pos)+6).."}")
                    elseif (type(val)=="string") then
                        print(indent.."["..pos..'] => "'..val..'"')
                    else
                        print(indent.."["..pos.."] => "..tostring(val))
                    end
                end
            else
                print(indent..tostring(t))
            end
        end
    end
    if (type(t)=="table") then
        print(tostring(t).." {")
        sub_print_r(t,"  ")
        print("}")
    else
        sub_print_r(t,"  ")
    end
    print()
end

table.print = print_r

--字符串转换为字符数组
--针对中文
function string.toCharArray(str,byteCount)
    str = str or ""
    local array = {}
    local len = string.len(str)
    while str do
        local fontUTF = string.byte(str,1)

        if fontUTF == nil then
            break
        end

          local tmp = string.sub(str,1,byteCount)
            table.insert(array,tmp)
            str = string.sub(str,byteCount + 1,len)
    end
    return array
end

familyOne = string.toCharArray("赵钱孙李周吴郑王冯陈褚卫蒋沈韩杨朱秦尤许何吕施张孔曹严华金魏陶姜戚谢邹喻水云苏潘葛奚范彭郎鲁韦昌马苗凤花方俞任袁柳鲍史唐费岑薛雷贺倪汤滕殷罗毕郝邬安常乐于时傅卞齐康伍余元卜顾孟平黄和穆萧尹姚邵湛汪祁毛禹狄米贝明臧计成戴宋茅庞熊纪舒屈项祝董粱杜阮席季麻强贾路娄危江童颜郭梅盛林刁钟徐邱骆高夏蔡田胡凌霍万柯卢莫房缪干解应宗丁宣邓郁单杭洪包诸左石崔吉龚程邢滑裴陆荣翁荀羊甄家封芮储靳邴松井富乌焦巴弓牧隗山谷车侯伊宁仇祖武符刘景詹束龙叶幸司韶黎乔苍双闻莘劳逄姬冉宰桂牛寿通边燕冀尚农温庄晏瞿茹习鱼容向古戈终居衡步都耿满弘国文东殴沃曾关红游盖益桓公晋楚闫",2);
familyTwo = string.toCharArray("欧阳太史端木上官司马东方独孤南宫万俟闻人夏侯诸葛尉迟公羊赫连澹台皇甫宗政濮阳公冶太叔申屠公孙慕容仲孙钟离长孙宇文司徒鲜于司空闾丘子车亓官司寇巫马公西颛孙壤驷公良漆雕乐正宰父谷梁拓跋夹谷轩辕令狐段干百里呼延东郭南门羊舌微生公户公玉公仪梁丘公仲公上公门公山公坚左丘公伯西门公祖第五公乘贯丘公皙南荣东里东宫仲长子书子桑即墨达奚褚师吴铭",4);
girlName = string.toCharArray("秀娟英华慧巧美娜静淑惠珠翠雅芝玉萍红娥玲芬芳燕彩春菊兰凤洁梅琳素云莲真环雪荣爱妹霞香月莺媛艳瑞凡佳嘉琼勤珍贞莉桂娣叶璧璐娅琦晶妍茜秋珊莎锦黛青倩婷姣婉娴瑾颖露瑶怡婵雁蓓纨仪荷丹蓉眉君琴蕊薇菁梦岚苑婕馨瑗琰韵融园艺咏卿聪澜纯毓悦昭冰爽琬茗羽希宁欣飘育滢馥筠柔竹霭凝晓欢霄枫芸菲寒伊亚宜可姬舒影荔枝思丽",2);
boyName = string.toCharArray("伟刚勇毅俊峰强军平保东文辉力明永健世广志义兴良海山仁波宁贵福生龙元全国胜学祥才发武新利清飞彬富顺信子杰涛昌成康星光天达安岩中茂进林有坚和彪博诚先敬震振壮会思群豪心邦承乐绍功松善厚庆磊民友裕河哲江超浩亮政谦亨奇固之轮翰朗伯宏言若鸣朋斌梁栋维启克伦翔旭鹏泽晨辰士以建家致树炎德行时泰盛雄琛钧冠策腾楠榕风航弘",2);

function getSexByName(name)
  if name=="飞燕女" then
   return 2;
  elseif name=="英女侠" then
   return 2;
  elseif name=="巫蛮儿" then
   return 2;
  elseif name=="偃无师" then
   return 1;
  elseif name=="逍遥生" then
   return 1;
  elseif name=="剑侠客" then
   return 1;
  elseif name=="狐美人" then
   return 2;
  elseif name=="骨精灵" then
   return 2;
  elseif name=="鬼潇潇" then
   return 2;
  elseif name=="杀破狼" then
   return 1;
  elseif name=="巨魔王" then
   return 1;
  elseif name=="虎头怪" then
   return 1;
  elseif name=="舞天姬" then
   return 2;
  elseif name=="玄彩娥" then
   return 2;
  elseif name=="桃夭夭" then
   return 2;
  elseif name=="羽灵神" then
   return 1;
  elseif name=="神天兵" then
   return 1;
  elseif name=="龙太子" then
   return 1;
  end
end

ulr ="http://xyq.v5ent.com/";