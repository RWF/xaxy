--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:52
--======================================================================--
function 引擎.坐骑库(id,zq,sp)
	-- 站立，行走
	local zqs = {宝贝葫芦 = {0x63C1AA04,0x939B6AA2,1,"ZY.dll"}, 神气小龟 = {0xE88353,0x702610D3,2,"ZY.dll"},
				 汗血宝马 = {0x7B49FA9A,0x3F76F5B2,3,"ZY.dll"},欢喜羊羊 = {0x8D4DBAAE,0x2636063C,4,"ZY.dll"},
				 魔力斗兽 = {0x4B0E16F1,0x28F7499E,3,"ZY.dll"},披甲战狼 = {0x3B0CC9,0xF6B76F79,4,"ZY.dll"},
				 闲云野鹤 = {0x49CAB729,0x1544FBAD,3,"ZY.dll"},云魅仙鹿 = {0x621ECF47,0x98D7DB2,4,"ZY.dll"},
				 风火飞轮 = {0xE9121B70,0xCCD5B122,5,"JM.dll"},翠云宝扇 = {0x803D9F96,0xA09A6760,5,"JM.dll"},
				 齐天小轿 = {0x6ED61C4C,0x76EA6FF7,5,"ZY.dll"},七彩祥云 = {0xD4CED8F7,0x4391944D,5,"JM.dll"},
				 凤舞灵蝶 = {0xF580A63E,0xF809B4D1,5,"JM.dll"},净土莲台 = {0x80FB8EED,0x33BFF46F,5,"JM.dll"},


				 叠彩仙蜗 = {0xF3C7A79F,0xB9B1B192,5,"ZY.dll"},瑞彩祥云 = {0x089A78C7,0x43FB6660,5,"ZY.dll"},
				 御风灵貂 = {0x5BCF5B7B,0x0A6E3345,5,"ZY.dll"},太白仙骑 = {0x13F76327,0x3DB2E7AC,5,"ZY.dll"},
				 青霄天麟 = {0x0BBA930C,0xF5B6264C,5,"ZY.dll"},野趣蘑菇 = {0x15521B1F,0xD47915FB,5,"ZY.dll"},
				 九霄冰风 = {0x0F74DAF0,0x925DA881,5,"ZY.dll"},暗夜战豹 = {0x2283C8E4,0x256B2E02,5,"ZY.dll"},
				 沙漠驼铃 = {0x10E2138C,0x5CE41EF8,5,"ZY.dll"},玄冰灵虎 = {0x22FE1F3E,0xA7A653F9,5,"ZY.dll"},
				 踏雪灵熊 = {0x91921EE5,0x23C8E5DE,5,"ZY.dll"},金鳞仙子 = {0xEBCD273D,0x37196BDB,5,"ZY.dll"},
                 玄霜玉兔 = {0x50985A64,0x268E9022,5,"ZY.dll"},碧海鳐鱼 = {0x37D98B57,0x93871E46,5,"ZY.dll"},
                 蝠翼冥骑 = {0x40FFF86D,0x27858783,5,"ZY.dll"},玉脂福羊 = {0x386B9BE9,0x4C508BC0,5,"ZY.dll"},
                 鹤雪锦犀 = {0x2C490405,0x61F5973D,5,"ZY.dll"},璇彩灵仙 = {0x3A0E45CF,0x6A7FC191,5,"ZY.dll"},
                 猪猪小侠 = {0x2D5B61B8,0x366A7D69,5,"ZY.dll"},沉星寒犀 = {0x43E7BE1F,0x77C1746C,5,"ZY.dll"},
                 飞天猪猪 = {0x2D7948EF,0xD9195AA8,5,"ZY.dll"},烈焰斗猪 = {0xD8E8D600,0x4771DEA3,5,"ZY.dll"},
                 疾风战马 = {0x2EDE08D6,0x687324BB,5,"ZY.dll"},妙缘暖犀 = {0x52282FC2,0xB19DF18D,5,"ZY.dll"},
                 跃动精灵 = {0x9C565325,0x30B204B2,5,"ZY.dll"},玄火神驹 = {0x5504B0BE,0xE8DD3341,5,"ZY.dll"},
                 甜蜜猪猪 = {0x31C69560,0xD3DAA128,5,"ZY.dll"},腾云仙牛 = {0xC745F2D8,0x5570453A,5,"ZY.dll"},
                 琉璃宝象 = {0xC01D7558,0x343087B3,5,"ZY.dll"},如意宝狮 = {0xBCDA9EDE,0x55DCA86B,5,"ZY.dll"},
                 雪域圣灵 = {0x36BC39A8,0x80C30B8B,5,"ZY.dll"},怒雷狂狮 = {0xEC31AF3B,0x55E4FB5C,5,"ZY.dll"},
                 仙缘战鹿 = {0xF0D76857,0x62735411,5,"ZY.dll"},青蛙呱呱 = {0x8640CC00,0x6B404CD1,5,"ZY.dll"},
                 萌萌海狮 = {0x6307DC3F,0xC5C9968C,5,"ZY.dll"},竹林熊猫 = {0x6E932BD6,0xBDF58C53,5,"ZY.dll"},
                 紫霞云麒 = {0x81FB5DFA,0x65FF3621,5,"ZY.dll"},飞天灵鼠 = {0x7CC1F74C,0xC0EF1F6A,5,"ZY.dll"},
                 月影天马 = {0xC9086441,0x76C33E92,5,"ZY.dll"},幽骨战龙 = {0xF040B821,0x7E002D8D,5,"ZY.dll"},
                 嬉闹灵狮 = {0x7F92EFAC,0x8056E391,5,"ZY.dll"},魔骨战熊 = {0xBB49CBB2,0x8247EBC3,5,"ZY.dll"},
                 沐月灵猫 = {0xE814EFEF,0x86E72B40,5,"ZY.dll"},炫影魔蝎 = {0x94FD9126,0xFCB0282F,5,"ZY.dll"},
                 轻云羊驼 = {0x9BDE4F15,0xC5C3525E,5,"ZY.dll"},莽林猛犸 = {0xBE8BCD59,0x1EE017FE,5,"ZY.dll"},
                 粉红小驴 = {0xC7655C83,0xAD4889BD,5,"ZY.dll"},赤瞳妖猫 = {0xB08E7973,0xE1FC4981,5,"ZY.dll"},
                 灵尾松鼠 = {0x0181CFC1,0xC6FED2C7,5,"ZY.dll"},神行小驴 = {0x9CEA9EAE,0xDA83EF16,5,"ZY.dll"},
                七彩神驴 = {0x3D33442E,0xE8252C7B,5,"ZY.dll"},天使猪猪 = {0x876E0D2B,0x6088EDAC,5,"ZY.dll"},
                灯笼锦鱼 = {0xBC893B6A,0x7453F531,5,"ZY.dll"},灯笼锦鱼红 = {0xB78E7A06,0x3D345627,5,"ZY.dll"},
                 赤炎战狼 = {0xE47361C0,0xEB15D736,5,"ZY.dll"},浣碧石犀 = {0xE5304126,0xE8DB426B,5,"ZY.dll"}

				--  飞天黄金狮坐骑 = {0x9EA86797,0x7B745DE4,2,"shape.wdc"},飞天黄金狮坐骑2 = {0x804119A2,0xF260474D,2,"shape.wdc"},
				-- 未知坐骑3 = {0xE65707DE,0xE65707DE,3,"shape.wdc"},大黑虎坐骑 = {0x33668F97,0xDD66EE9C,2,"shape.wdc"},
				-- 黄金象坐骑 = {0xD8E8D600,0x4771DEA3,3,"shape.wdc"},飞天猪坐骑粉 = {0xD3DAA128,0xD3DAA128,3,"shape.wdc"},--9E1A3456
				-- 飞天猪坐骑黑 = {0xCB51189B,0xBB051DE9,3,"shape.wdc"},小狐狸坐骑 = {0xC6FED2C7,0xC6FED2C7,2,"shape.wdc"},--C4835466  小狐狸坐骑跑动
				-- 蝎子坐骑 = {0x94FD9126,0xFCB0282F,2,"shape.wdc"},未知坐骑1 = {0xFC14DF68,0xFC14DF68,3,"shape.wdb"}
	}
	local sps = {展翅高飞 = {0x2DC16EF4,0x47A59E6C,"ZY.dll"},旗开得胜 = {0x4FB7A645,0xC89B8D7B,"ZY.dll"},霸王雄风 = {0x8AC5514E,0xD30116BE,"ZY.dll"},
				 独眼观天 = {0xCB41BF07,0x6D415352,"ZY.dll"},威武不屈 = {0xE385373B,0x71FE0155,"ZY.dll"},深藏不露 = {0x2529E5A5,0x51C03CD4,"ZY.dll"},
				 异域浓情 = {0xE8B35E96,0x3949C769,"ZY.dll"},流星天马 = {0x72489CFD,0x4D136355,"ZY.dll"},威猛将军 = {0x5BDBA7CB,0x5CDC5A5E,"ZY.dll"},
				 知情达理 = {0xCCBF24B8,0xFE4B37F2,"ZY.dll"},气宇轩昂 = {0xEC4C09DF,0x57B096DF,"ZY.dll"},如花似玉 = {0xA6966FD2,0xCA8864D1,"ZY.dll"},
				 傲视天下 = {0xBB906984,0x2549904, "ZY.dll"},铁血豪情 = {0x742FBF19,0x103FFB93,"ZY.dll"},唯我独尊 = {0x7F6FFC35,0x716B5DC1,"ZY.dll"},
				 叱咤风云 = {0x1FED0CD8,0xD8EB6880,"ZY.dll"},异域风情 = {0xAB007164,0x2E177381,"ZY.dll"},假面勇者 = {0xE7CB8205,0xE615404,"ZY.dll"},
				 霓裳魅影 = {0xE0CB07C8,0xD9D958E6,"ZY.dll"},披星戴月 = {0x8ED6D8CC,0xC5D8F53D,"ZY.dll"},烈焰燃情 = {0x8C575D26,0x7B15590A,"ZY.dll"},
				 天雨流芳 = {0x503F394B,0x23BF657B,"ZY.dll"},灵光再现 = {0xC4D118C5,0xCBC6930A,"ZY.dll"},倾国倾城 = {0x1F01B8BE,0xBDA4DDAB,"ZY.dll"},
				 空 = {}
	}
	local scs
	if id == "飞燕女" then
		scs = {{0x4492502E,0xF6D6D5E6,"JS.dll"},{0xDCB946EC,0xDCC84D4E,"JS.dll"},{0xDF01F29D,0xA77B55E4,"JS.dll"},{0x6E0AD379,0x87C7A650,"JS.dll"},{3234089534,3234089534,"JS.dll"}}
	elseif id == "英女侠" then
		scs = {{0xD43912A9,0xD2D4CAD3,"ZY.dll"},{0x70291C50,0x30CABF19,"ZY.dll"},{0x726C392E,0x68FB1969,"ZY.dll"},{0x2474769B,0xACD868DE,"ZY.dll"},{283881972,283881972,"ZY.dll"}}
	elseif id == "巫蛮儿" then
		scs = {{0xF2BC9369,0xB11F6642,"ZY.dll"},{0x9B73C75F,0xDA0A8B06,"ZY.dll"},{0x2F5EAD3F,0x499F9D37,"ZY.dll"},{0xCCC0985C,0xC8F56BA3,"ZY.dll"},{3847342356,3847342356,"ZY.dll"}}
	elseif id == "偃无师" then
		scs = {{0x281C3FEE,0xEE3624A8,"JS.dll"},{0,0,"JS.dll"},{0,0,"JS.dll"},{0,0,"JS.dll"},{0xC8FDA204,0xC8FDA204,"ZY.dll"}}
	elseif id == "逍遥生" then
		scs = {{0xA35491C9,0x49D7C76E,"JS.dll"},{0xB770EAD4,0x9A1479D8,"JS.dll"},{0x76D629EA,0xFB50C58F,"JS.dll"},{0x3D392EF4,0xA5E02A65,"JS.dll"},{947396698,947396698,"JS.dll"}}
	elseif id == "剑侠客" then
		scs = {{0x67101CB7,0x9C8790BA,"ZY.dll"},{0x32DA9583,0xEC9AC961,"ZY.dll"},{0x766731D,0x8C50358A,"ZY.dll"},{0xA95A126D,0x513DDE6C,"ZY.dll"},{2626634241,2626634241,"ZY.dll"}}
	elseif id == "狐美人" then
		scs = {{0xE3123BDA,0x956305B5,"JS.dll"},{0xFB798485,0xD1997415,"JS.dll"},{0x64C21A63,0xD5D2FA14,"JS.dll"},{0xBCD86DDA,0xF0062006,"JS.dll"},{1734212905,1734212905,"JS.dll"}}
	elseif id == "骨精灵" then
		scs = {{0xBEEF3795,0x3C6BF98F,"JS.dll"},{0xE2C1CDE4,0xBAD0F711,"ZY.dll"},{0x75B09FA1,0x5E5736EE,"ZY.dll"},{0x83DD50D3,0xB84C7C38,"ZY.dll"},{4229617854,4229617854,"JS.dll"}}
	elseif id == "鬼潇潇" then
		scs = {{0x094C5BFB,0xFE2D2B18,"JS.dll"},{0,0,"JS.dll"},{0,0,"JS.dll"},{0,0,"JS.dll"},{0xC8FDA244,0xC8FDA244,"ZY.dll"}}
	elseif id == "杀破狼" then
		scs = {{0xE137A55D,0xDB553291,"ZY.dll"},{0x8BEA762D,0x14EE7109,"ZY.dll"},{0x46A79E5,0x3CD5444,"ZY.dll"},{0xF974CEB,0x54A8F096,"ZY.dll"},{4241012400,4241012400,"ZY.dll"}}
	elseif id == "巨魔王" then
		scs = {{0x21ED721D,0x5A05E1C0,"JS.dll"},{0x9DFEB143,0x77C20678,"JS.dll"},{0x1AF61311,0x6E370D46,"JS.dll"},{0xCC1426ED,0x39FE09DB,"JS.dll"},{2639845870,2639845870,"JS.dll"}}
	elseif id == "虎头怪" then
		scs = {{0x99AD84CD,0x9FA6D533,"JS.dll"},{0xF56603D1,0x83DBBA94,"JS.dll"},{0x37FFB9DF,0x64426F93,"JS.dll"},{0x95BC0425,0xC6053278,"JS.dll"},{1705376638,1705376638,"JS.dll"}}
	elseif id == "舞天姬" then
		scs = {{0x54DB4F4D,0xCB722714,"JS.dll"},{0xD92FC3DE,0x809F42FE,"JS.dll"},{0x212848A1,0xAAD7CB93,"JS.dll"},{0xB44DF735,0xECA5DB49,"JS.dll"},{3443216643,3443216643,"JS.dll"}}
	elseif id == "玄彩娥" then
		scs = {{0x861EE4D9,0x9F2F9C11,"JS.dll"},{0x3316877C,0x31F77503,"JS.dll"},{0x779A3DF,0x622664DC,"JS.dll"},{0xA6FD7850,0xB9FD9DBD,"JS.dll"},{2291528828,2291528828,"JS.dll"}}
	elseif id == "桃夭夭" then
		scs = {{0x5AA5E164,0x2C255CEE,"JS.dll"},{0,0,"JS.dll"},{0,0,"JS.dll"},{0,0,"JS.dll"},{0xC8FDA234,0xC8FDA234,"ZY.dll"}}
	elseif id == "羽灵神" then
		scs = {{0x7D31F43E,0x76E4E3D6,"ZY.dll"},{0x7B86A5F4,0xE496A2D7,"ZY.dll"},{0x8072202A,0x6528F013,"ZY.dll"},{0x4BBD02E6,0x694A236B,"ZY.dll"},{1384231887,1384231887,"ZY.dll"}}
	elseif id == "神天兵" then
		scs = {{0x77104303,0xBC38000F,"JS.dll"},{0xF4EF98B5,0x7C731501,"JS.dll"},{0x7F6D09AB,0x13AD1C23,"JS.dll"},{0x60A47C21,0x63930A54,"JS.dll"},{1539170847,1539170847,"JS.dll"}}
	elseif id == "龙太子" then
		scs = {{0x4F27A59F,0x801F438D,"JS.dll"},{0x46F4FDF6,0xC9EF2751,"ZY.dll"},{0x5B0EDDAD,0xB227D39F,"ZY.dll"},{0x643F7DDE,0xCC8E0921,"ZY.dll"},{397538195,397538195,"JS.dll"}}
	end
	local bh = zqs[zq][3]
	return {坐骑资源=zqs[zq][4],坐骑站立=zqs[zq][1],坐骑行走=zqs[zq][2],人物资源=scs[bh][3],人物站立=scs[bh][1],人物行走=scs[bh][2],坐骑饰品站立=sps[sp][1],坐骑饰品行走=sps[sp][2],坐骑饰品资源=sps[sp][3]}
end