--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:52
--======================================================================--
function 引擎.特效库(tx)
	if tx =="烟花礼盒" then
		tx="烟花礼盒"..取随机数(1,4)
	end
	local txs = {}
	if tx == "雨落寒沙" then --新0x6115641F
		txs[1] =  0x494DC152
		txs[2] = "ZY.dll"

	elseif tx == "状态_法宝" then --新
		txs[1] = 0x061869D7
		txs[2] = "ZY.dll"
	elseif tx == "烟花" then --新
		txs[1] = 0x34169A57
		txs[2] = "JM.dll"
	elseif tx == "新年烟花" then --新
		txs[1] = 0x7E5255D9
		txs[2] = "JM.dll"
	elseif tx == "逆袭宝箱" then --新
		txs[1] =0xDB361AE8
		txs[2] = "ZHS.dll"
	elseif tx == "幸运宝箱" then --新
		txs[1] =0xDB361AE8
		txs[2] = "ZHS.dll"
	elseif tx == "特殊兽决宝箱" then --新
		txs[1] =0xDB361AE8
		txs[2] = "ZHS.dll"

	elseif tx == "食指大动" then --新
	txs[1] = 0xCEEA8F28--619D789E
	txs[2] = "ZY.dll"

	elseif tx == "状态_无畏布施" then --新
	txs[1] = 0xA8D0CA5A--619D789E
	txs[2] = "ZY.dll"

	elseif tx == "无畏布施" then --新
	txs[1] = 0x33DD2829--619D789E
	txs[2] = "ZY.dll"

	elseif tx == "流沙轻音"  then --新
	txs[1] = 0x7BE878E0--619D789E
	txs[2] = "ZY.dll"

	elseif  tx == "扶摇万里" then --新
	txs[1] = 0x5B5938BC--619D789E
	txs[2] = "ZY.dll"

	elseif tx == "叱咤风云" then --新
	txs[1] = 0x18967984--619D789E
	txs[2] = "ZY.dll"
	elseif tx == "哼哼哈兮" then --新
		txs[1] = 0x048B222C--619D789E
		txs[2] = "ZY.dll"



	elseif tx == "召还" then --新
		txs[1] = 0x3C876325
		txs[2] = "ZY.dll"
	elseif tx == "唤灵·魂火" or tx == "唤魔·堕羽" or tx == "唤灵·焚魂"  or tx == "唤魔·毒魅" then --新
		txs[1] = 0x5927908D
		txs[2] = "ZY.dll"
	elseif tx == "召唤类" then --新
		txs[1] = 0x2BAFA21D--619D789E
		txs[2] = "ZY.dll"

	elseif tx == "烟花礼盒1" then --新
		txs[1] = 0xA19E02D2
		txs[2] = "JM.dll"
	elseif tx == "烟花礼盒2" then --新
		txs[1] = 0x73493422
		txs[2] = "JM.dll"
	elseif tx == "烟花礼盒3" then --新
		txs[1] = 0x4E62D068
		txs[2] = "JM.dll"
	elseif tx == "烟花礼盒4" then --新
		txs[1] = 0xB4CF22B7
		txs[2] = "JM.dll"
	elseif tx == "八凶法阵"  then --新
		txs[1] = 0x4E86F9B3
		txs[2] = "ZY.dll"

     elseif tx == "神来气旺"  then --新
		txs[1] = 0x559C8ECF
		txs[2] = "ZY.dll"

	elseif tx == "状态_光照万象" then --新
		txs[1] = 0x155C6908
		txs[2] = "ZY.dll"

	elseif tx == "善恶有报" then --新
		txs[1] = 0x8D8A818D
		txs[2] = "ZY.dll"

		elseif tx == "进阶善恶有报" then --新
		txs[1] = 0x8D8A818D
		txs[2] = "ZY.dll"


	elseif tx == "潜力激发" or tx =="状态_潜力激发" then
		txs[1] = 0xF8031590
		txs[2] = "ZY.dll"
	elseif tx == "蚩尤之搏" then
		txs[1] = 0x096766A8
		txs[2] = "ZY.dll"
	elseif tx == "治疗" or tx == "仙人指路" or tx == "峰回路转" then
		txs[1] = 0xFBF89980
		txs[2] = "ZY.dll"
	elseif tx == "还魂咒" then
		txs[1] = 0x5229F242
		txs[2] = "ZY.dll"
	elseif tx == "四面埋伏"  then
		txs[1] = 0x8AA8F567
		txs[2] = "ZY.dll"
	elseif tx == "状态_四面埋伏"  then
		txs[1] = 0x5816CDC7
		txs[2] = "ZY.dll"
	elseif tx == "天降灵葫" then --新
		txs[1] = 0xF99822CE
		txs[2] = "ZY.dll"
	elseif tx == "弱点击破" then --新
		txs[1] = 2978096965
		txs[2] = "ZY.dll"
	elseif tx == "冥王爆杀" then --新
		txs[1] = 2751336734
		txs[2] = "ZY.dll"
	elseif tx == "由己渡人" then --新
		txs[1] = 0xC0A21E7F
		txs[2] = "ZY.dll"
	elseif tx == "状态_由己渡人" then --新
		txs[1] = 0x2ED4AF64
		txs[2] = "ZY.dll"
	elseif tx == "状态_电芒" then --新
		txs[1] = 0xFC309480
		txs[2] = "ZY.dll"
	elseif tx == "失忆符" then
		txs[1] = 0x3B86B284
		txs[2] = "ZY.dll"
	elseif tx == "破碎无双" then
		txs[1] = 0x21FAE73F
		txs[2] = "ZY.dll"
	elseif tx == "凝神术" then
		txs[1] = 0x5175D5FF
		txs[2] = "ZY.dll"
	elseif tx == "状态_凝神术" then
		txs[1] = 0xC2472120
		txs[2] = "ZY.dll"

	elseif tx == "流云诀" then
		txs[1] = 0xF4B50BFF
		txs[2] = "ZY.dll"
	elseif tx == "状态_流云诀" then
		txs[1] = 0x5826238A
		txs[2] = "ZY.dll"

	elseif tx == "笑里藏刀" then
		txs[1] = "37098608"
		txs[2] = "MAX.7z"
	elseif tx == "寡欲令" then
		txs[1] = 0xFDE4BF24
		txs[2] = "ZY.dll"
	elseif tx == "状态_寡欲令" then
		txs[1] = 0xE89A19DF
		txs[2] = "ZY.dll"
   elseif tx == "逃跑" then
		txs[1] = 0xF5189E48
		txs[2] = "JM.dll"
	elseif tx == "状态_失忆符" then
		txs[1] = "94E90B97"
		txs[2] = "MAX.7z"
	elseif tx == "龙腾" then --新
		txs[1] = 0x7367AF0F
		txs[2] = "ZY.dll"
	elseif tx == "法术防御" then --新
		txs[1] = 0x61C3FF68
		txs[2] = "ZY.dll"
	elseif tx == "状态_法术防御" then --新
		txs[1] = 0x04EBA6E1
		txs[2] = "ZY.dll"
	elseif tx == "妙手回春" then
		txs[1] = 51617616
		txs[2] = "ZY.dll"
	elseif tx == "凝神诀" then
		txs[1] = 2128818470
		txs[2] = "ZY.dll"
	elseif tx == "乾坤玄火塔" then
		txs[1] = 0xE4A2B66E
		txs[2] = "ZY.dll"
   	elseif tx == "状态_乾坤玄火塔" then
		txs[1] = 0xC236BF21
		txs[2] = "ZY.dll"
	 elseif tx == "状态_乾坤玄火塔1" then
		txs[1] = 0x325A1F6A
		txs[2] = "ZY.dll"
	elseif tx == "金钱镖" or tx == "落雨金钱" then
		txs[1] = 0x87D204F0
		txs[2] = "ZY.dll"
	elseif tx == "混元伞" then
		txs[1] = 0x1F334661
		txs[2] = "ZY.dll"
   	elseif tx == "状态_混元伞" then
		txs[1] = 0x5ECEBEDD
		txs[2] = "ZY.dll"
	elseif tx == "神木宝鼎" then
		txs[1] = 0x8D7D4A28
		txs[2] = "ZY.dll"
   	elseif tx == "状态_神木宝鼎" then
		txs[1] = 0x167650D4
		txs[2] = "ZY.dll"
	elseif tx == "摄魂" then
		txs[1] = 0x3A63D8A8
		txs[2] = "ZY.dll"
   	elseif tx == "状态_摄魂" then
		txs[1] = 0x0ACA5045
		txs[2] = "ZY.dll"
	elseif tx == "无尘扇" then
		txs[1] = 0x4C652A39
		txs[2] = "ZY.dll"
   	elseif tx == "状态_无尘扇" then
		txs[1] = 0x178EBC5E
		txs[2] = "ZY.dll"
	elseif tx == "失心钹" then
		txs[1] = 0xB5DC4541
		txs[2] = "ZY.dll"
   	elseif tx == "状态_失心钹" then
		txs[1] = 0x61CB13B5
		txs[2] = "ZY.dll"
	elseif tx == "发瘟匣" then
		txs[1] = 0x1C5F1F4C
		txs[2] = "ZY.dll"
   	elseif tx == "状态_发瘟匣" then
		txs[1] = 0x0D48B920
		txs[2] = "ZY.dll"
	elseif tx == "北冥巨鳞" then
		txs[1] = 0x1EDD5D74
		txs[2] = "ZY.dll"
   	elseif tx == "状态_北冥巨鳞" then
		txs[1] = 0xFC309480
		txs[2] = "ZY.dll"
	elseif tx == "冷龙石磐" then
		txs[1] = 0x49EB8017
		txs[2] = "ZY.dll"
   	elseif tx == "状态_冷龙石磐" then
		txs[1] = 0xA8D0CA5A
		txs[2] = "ZY.dll"
	elseif tx == "无字经" then
		txs[1] = 0x266B36D8
		txs[2] = "ZY.dll"
   	elseif tx == "状态_无字经" then
		txs[1] = 0xE42C0F60
		txs[2] = "ZY.dll"
	elseif tx == "无魂傀儡" then
		txs[1] = 0x83F019EC
		txs[2] = "ZY.dll"
   	elseif tx == "状态_无魂傀儡" then
		txs[1] = 0x9B16147A
		txs[2] = "ZY.dll"
	elseif tx == "舞雪冰蝶" then
		txs[1] = 0xEBA466BA
		txs[2] = "ZY.dll"
   	elseif tx == "状态_舞雪冰蝶" then
		txs[1] = 0xFAF4A2D4
		txs[2] = "ZY.dll"
	elseif tx == "血魄元幡" then
		txs[1] = 0xB89D2570
		txs[2] = "ZY.dll"
   	elseif tx == "状态_血魄元幡" then
		txs[1] = 0xB86FC1A3
		txs[2] = "ZY.dll"
	elseif tx == "五彩娃娃" then
		txs[1] = 0xC7113232
		txs[2] = "ZY.dll"
   	elseif tx == "状态_五彩娃娃" then
		txs[1] = 0x333A3805
		txs[2] = "ZY.dll"
	elseif tx == "苍白纸人" then
		txs[1] = 0xDB00E6D8
		txs[2] = "ZY.dll"
   	elseif tx == "状态_苍白纸人" then
		txs[1] = 0x592EBD89
		txs[2] = "ZY.dll"

	elseif tx == "奇门五行令" then
		txs[1] = 0xF7103C1D
		txs[2] = "ZY.dll"
   	elseif tx == "状态_奇门五行令" then
		txs[1] = 0xAF07277F
		txs[2] = "ZY.dll"

	elseif tx == "赤焰" then
		txs[1] = 0xCD2E1D5F
		txs[2] = "ZY.dll"
   	elseif tx == "状态_赤焰" then
		txs[1] = 0x411C5D82
		txs[2] = "ZY.dll"
	elseif tx == "千里神行" then
		txs[1] = 3356723242
		txs[2] = "ZY.dll"
	elseif tx == "鬼泣 " or tx == "惊魂铃" then
		txs[1] = 0x96E5FD5D
		txs[2] = "ZY.dll"
	elseif tx == "翩鸿一击" then
		txs[1] = 0xBCF85212
		txs[2] = "ZY.dll"
	elseif tx == "长驱直入" then
		txs[1] = 0x8E895CEE
		txs[2] = "ZY.dll"
	elseif tx == "水攻" then
		txs[1] = 4180877467
		txs[2] = "ZY.dll"
	elseif tx == "碎星诀" then
		txs[1] = 0x1C7F8B19
		txs[2] = "ZY.dll"
	elseif tx == "尸腐毒" then  --新
		txs[1] = 0x849E48FB
		txs[2] = "ZY.dll"


	elseif tx == "捕捉" then
		txs[1] = 3195920150
		txs[2] = "ZY.dll"
	elseif tx == "满天花雨" then --新
		txs[1] = 0x253C9538
		txs[2] = "ZY.dll"
	elseif tx == "似玉生香"  then --新
		txs[1] = 0xBA10E451
		txs[2] = "ZY.dll"
	elseif tx == "娉婷嬝娜"  then --新
		txs[1] = 0xAF8E7DDB
		txs[2] = "ZY.dll"
	elseif tx == "状态_娉婷嬝娜"  then --新
		txs[1] = 0x400392AD
		txs[2] = "ZY.dll"
	elseif tx == "状态_似玉生香" then
		txs[1] = 0x59D77EF9
		txs[2] = "ZY.dll"
	elseif tx == "状态_象形" then
		txs[1] = 0x3646A4D9
		txs[2] = "ZY.dll"
	elseif tx == "状态_雾杀" then
		txs[1] = 1506044316
		txs[2] = "ZY.dll"
	elseif tx == "落岩" then
		txs[1] = 180555238
		txs[2] = "ZY.dll"
	elseif tx == "回魂咒" then
		txs[1] = 2391977602
		txs[2] = "ZY.dll"
	elseif tx == "韦陀护法" then
		txs[1] = 2493890284
		txs[2] = "ZY.dll"
	elseif tx == "烟雨剑法" then
		txs[1] = 0xD06119B3
		txs[2] = "ZY.dll"
	elseif tx == "百万神兵" then  --新
		txs[1] = 0x8B8516F1
		txs[2] = "ZY.dll"
	elseif tx == "巨岩破" then --新
		txs[1] = 0xC4FABE4C
		txs[2] = "ZY.dll"
	elseif tx == "惊心一剑" then
		txs[1] = 1000651155
		txs[2] = "ZY.dll"

		elseif tx == "进阶惊心一剑" then
		txs[1] = 1000651155
		txs[2] = "ZY.dll"
		elseif tx == "壁垒击破" then
		txs[1] = 0x99DDC32A
		txs[2] = "ZY.dll"

		elseif tx == "进阶壁垒击破" then
		txs[1] = 0x99DDC32A
		txs[2] = "ZY.dll"
		elseif tx == "剑荡四方" then
		txs[1] = 0x729ADE62
		txs[2] = "ZY.dll"

	elseif tx == "落魄符" then --新
		txs[1] = 0xB008AC11
		txs[2] = "ZY.dll"
	elseif tx == "水遁" then
		txs[1] = 1280017893
		txs[2] = "ZY.dll"
	elseif tx == "泰山压顶" then
		txs[1] = 2017434912
		txs[2] = "ZY.dll"
	elseif tx == "泰山压顶1" then
		txs[1] = 0xDF4C3BE3
		txs[2] = "ZY.dll"
	elseif tx == "泰山压顶2" then
		txs[1] = 0xE9401B00
		txs[2] = "ZY.dll"
	elseif tx == "泰山压顶3" then
		txs[1] = 0x133F8E31
		txs[2] = "JM.dll"
	elseif tx == "达摩护体" then
		txs[1] = 2565901429
		txs[2] = "ZY.dll"
	elseif tx == "地涌金莲" then
		txs[1] = 0x0E1DCDB9
		txs[2] = "ZY.dll"
	elseif tx == "杀气诀" then
		txs[1] = 3264874295
		txs[2] = "ZY.dll"
	-- elseif tx == "龙卷雨击" then --新
	-- 	txs[1] ="EB429244"--"--"6E9245E7"
	-- 	txs[2] = "MAX.7z"
	elseif tx == "地狱烈火" then
		txs[1] = 0xBE325D99
		txs[2] = "ZY.dll"
	elseif tx == "炎护" then
		txs[1] = 0xADF90980
		txs[2] = "ZY.dll"
	elseif tx == "加血" then
		txs[1] = 2010253357
		txs[2] = "ZY.dll"
	elseif tx == "杳无音讯" or tx == "凋零之歌" then
		txs[1] = 2709550029
		txs[2] = "ZY.dll"
	elseif tx == "苍茫树" then --新
		txs[1] = 0x9CCB6E84
		txs[2] = "ZY.dll"
	elseif tx == "自在心法" then --新
		txs[1] = 0xD6BF55AD
		txs[2] = "ZY.dll"
	elseif tx == "魂飞魄散" then --新
		txs[1] = 0xAFF3AFBD
		txs[2] = "ZY.dll"
	elseif tx == "天魔解体" then --新
		txs[1] = 0xD20E78BF
		txs[2] = "ZY.dll"
	elseif tx == "裂石" then --新
		txs[1] = 0x3C5C1616 --新
		txs[2] = "ZY.dll"
	elseif tx == "断岳势" then --新
		txs[1] = 0x6AD55205 --新
		txs[2] = "ZY.dll"
	elseif tx == "推气过宫" then--新
		txs[1] = 0x4CFC6116
		txs[2] = "ZY.dll"
	elseif tx == "状态_金刚护体" then
		txs[1] = 916220457
		txs[2] = "ZY.dll"
	elseif tx == "离魂符" then
		txs[1] = 0x9A94D461
		txs[2] = "ZY.dll"
	elseif tx == "加愤怒" then
		txs[1] = 2156718962
		txs[2] = "ZY.dll"
	elseif tx == "五雷轰顶" then --新
		txs[1] = 0x6044E21A
		txs[2] = "ZY.dll"
	elseif tx == "冰清诀" or tx == "晶清诀" or tx == "玉清诀" or tx == "水清诀" or tx == "同生共死" then
		txs[1] = 388205471
		txs[2] = "ZY.dll"
	elseif tx == "楚楚可怜" then --新
		txs[1] = 0x28FAEFF7
		txs[2] = "ZY.dll"
	elseif tx == "佛门普渡" then
		txs[1] = 428453890
		txs[2] = "ZY.dll"
	elseif tx == "防御" then
		txs[1] = 908223343
		txs[2] = "ZY.dll"
	elseif tx == "天神护体" then
		txs[1] = 3231493430
		txs[2] = "ZY.dll"
	elseif tx == "活血" then --新
		txs[1] = 0xEF9691F3
		txs[2] = "ZY.dll"
	elseif tx == "救死扶伤" then --新
		txs[1] = 0xF2C7BC8E
		txs[2] = "ZY.dll"
	elseif tx == "雷击" then
		txs[1] = 238079300
		txs[2] = "ZY.dll"
	elseif tx == "天蚕丝" then
		txs[1] = 341882911
		txs[2] = "ZY.dll"
	elseif tx == "天罗地网" then --新
		txs[1] = 0x7DB0552B
		txs[2] = "ZY.dll"
	elseif tx == "天神护法" then
		txs[1] = 1935690327
		txs[2] = "ZY.dll"
	elseif tx == "状态_夺魄令" then
		txs[1] = 566328485
		txs[2] = "ZY.dll"
	elseif tx == "牛刀小试" then
		txs[1] = 1000651155
		txs[2] = "ZY.dll"
	elseif tx == "二龙戏珠" then --新
		txs[1] = 0xA680A821
		txs[2] = "ZY.dll"
	elseif tx == "盘丝阵" then
		txs[1] = 1774142217
		txs[2] = "ZY.dll"
	elseif tx == "惊魂掌" then
		txs[1] = 0xC3D52CB3
		txs[2] = "ZY.dll"
	elseif tx == "煞气诀" then
		txs[1] = 0x8CD6756E
		txs[2] = "ZY.dll"
	elseif tx == "状态_煞气诀" then
		txs[1] = 0x3EE6C7FA
		txs[2] = "ZY.dll"
	elseif tx == "状态_尸腐毒" then
		txs[1] = 2208088730
		txs[2] = "ZY.dll"
	elseif tx == "四海升平" then
		txs[1] = 3497509792
		txs[2] = "ZY.dll"
	elseif tx == "慈航普渡" then
		txs[1] = 0x2FF74578
		txs[2] = "ZY.dll"
	elseif tx == "诅咒之伤" then
		txs[1] = 0x3D14B496
		txs[2] = "ZY.dll"
	elseif tx == "罗汉金钟" then
		txs[1] = 0x9B23F38E
		txs[2] = "ZY.dll"
	elseif tx == "状态_罗汉金钟" then
		txs[1] = 0x5C5C1544
		txs[2] = "ZY.dll"
	elseif tx == "状态_太极护法" then
		txs[1] = 0x5C5C1544
		txs[2] = "ZY.dll"
	elseif tx == "圣灵之甲" then
		txs[1] = 0x38FD76EA
		txs[2] = "ZY.dll"
	elseif tx == "状态_圣灵之甲" then
		txs[1] = 956135146
		txs[2] = "ZY.dll"
	elseif tx == "魔兽之印" then
		txs[1] = 0x203B8C75
		txs[2] = "ZY.dll"
	elseif tx == "状态_魔兽之印" then
		txs[1] = 0x203B8C75
		txs[2] = "ZY.dll"
	elseif tx == "状态_野兽之力" then
		txs[1] = 0x203B8C75
		txs[2] = "ZY.dll"
	elseif tx == "野兽之力" then
		txs[1] = 540773493
		txs[2] = "ZY.dll"
	elseif tx == "放下屠刀" then
		txs[1] = "4C7EFEB1"
		txs[2] = "MAX.7z"
	elseif tx == "太极护法" then
		txs[1] = 0x5CC81980
		txs[2] = "ZY.dll"
	elseif tx == "光辉之甲" then
		txs[1] = 0x38FD76EA
		txs[2] = "ZY.dll"
	elseif tx == "状态_光辉之甲" then
		txs[1] = 956135146
		txs[2] = "ZY.dll"
	elseif tx == "雷霆万钧" then ---新
		txs[1] = 0x279AF873
		txs[2] = "ZY.dll"
	elseif tx == "状态_韦陀护法" then
		txs[1] = 2243155697
		txs[2] = "ZY.dll"
	elseif tx == "移形换影" then
		txs[1] = 1963789198
		txs[2] = "ZY.dll"
	elseif tx == "坐莲" then
		txs[1] = 1915810093
		txs[2] = "ZY.dll"
	elseif tx == "后发制人" then --新
		txs[1] = 0x39B69986
		txs[2] = "ZY.dll"
	elseif tx == "阎罗令" then  --新
		txs[1] = 0x9EB11259
		txs[2] = "ZY.dll"
	elseif tx == "飞砂走石" then
		txs[1] = 0x4DB67680--0x47FE2CC6
		txs[2] = "ZY.dll"
	elseif tx == "红袖添香" then
		txs[1] = 3858048292
		txs[2] = "ZY.dll"
	elseif tx == "状态_追魂符" then
		txs[1] = "975B66F1"
		txs[2] = "MAX.7z"
	elseif tx == "牛屎遁" then
		txs[1] = 2938873934
		txs[2] = "ZY.dll"
	elseif tx == "横扫千军" then --新
		txs[1] = 0xACA4A54A
		txs[2] = "ZY.dll"
		elseif tx == "进阶夜舞倾城" then --新
		txs[1] = 0xACA4A54A
		txs[2] = "ZY.dll"
	elseif tx == "破釜沉舟" then --新
		txs[1] = 0x78D6FD06
		txs[2] = "ZY.dll"
	elseif tx == "命归术" then
		txs[1] = 821212684
		txs[2] = "ZY.dll"
	elseif tx == "炼气化神" then --新
		txs[1] = 0x1CFE70FA
		txs[2] = "ZY.dll"
	elseif tx == "移魂化骨" then
		txs[1] = 330324521
		txs[2] = "ZY.dll"
	elseif tx == "推拿" then
		txs[1] = 51617616
		txs[2] = "ZY.dll"
	elseif tx == "状态_红袖添香" then
		txs[1] = 3037907947
		txs[2] = "ZY.dll"
	elseif tx == "日月乾坤" then --新
		txs[1] = 0xA70D1263
		txs[2] = "ZY.dll"
	elseif tx == "飘渺式" then --新
		txs[1] = 0xD06119B3
		txs[2] = "ZY.dll"
	elseif tx == "状态_楚楚可怜" then
		txs[1] = 1923968442
		txs[2] = "ZY.dll"
	elseif tx == "追魂符" then
		txs[1] = 0x25F14A5E
		txs[2] = "ZY.dll"
	elseif tx == "状态_离魂符" then
		txs[1] = "2EFE1BA6"
		txs[2] = "MAX.7z"
	elseif tx == "状态_百万神兵" then
		txs[1] = 0xC48ABA7B
		txs[2] = "ZY.dll"
	elseif tx == "烈火" then
		txs[1] = "239C8207"
		txs[2] = "MAX.7z"
	elseif tx == "变身" then
		txs[1] = 0x0B361FA6
		txs[2] = "ZY.dll"
	elseif tx == "状态_变身"  then
		txs[1] = 702028255
		txs[2] = "ZY.dll"
	elseif tx == "破血狂攻" then
		txs[1] = 387269810
		txs[2] = "ZY.dll"
	elseif tx == "鹰击" then
		txs[1] = 0x82C9074A
		txs[2] = "ZY.dll"
	elseif tx == "弓弩攻击" then
		txs[1] = 3804004647
		--txs[1] =0x775176BB
		--txs[1] =0xDFC0E136
		txs[2] = "ZY.dll"
	elseif tx == "状态_炼气化神" then
		txs[1] = 1906144895
		txs[2] = "ZY.dll"
	elseif tx == "状态_达摩护体" then
		txs[1] = 0x0EEC978E
		txs[2] = "ZY.dll"
	elseif tx == "状态_生命之泉" then
		txs[1] = 4176381242
		txs[2] = "ZY.dll"
	elseif tx == "状态_定身符" then
		txs[1] = "975B66F1"
		txs[2] = "MAX.7z"
	elseif tx == "状态_定心术"or tx =="定心术" then
		txs[1] = 0x2D07CCEC
		txs[2] = "ZY.dll"
	elseif tx == "状态_极度疯狂" or tx == "极度疯狂" then
		txs[1] = 0x718754A2
		txs[2] = "ZY.dll"
	elseif tx == "状态_不动如山" then
		txs[1] = 281904888
		txs[2] = "ZY.dll"
	elseif tx == "金刚护体" then --新
		txs[1] = 0x9E4E9755
		txs[2] = "ZY.dll"
	-- elseif tx == "龙卷雨击3" then
	-- 	txs[1] = 3514247917
	-- 	txs[2] = "ZY.dll"
	elseif tx == "浪涌" then --新
		txs[1] = 0xD53A25EB
		txs[2] = "ZY.dll"
	elseif tx == "状态_如花解语" then
		txs[1] = 505084121
		txs[2] = "ZY.dll"
	elseif tx == "气归术" then
		txs[1] = 3497509792
		txs[2] = "ZY.dll"
	elseif tx == "天崩地裂" then
		txs[1] = 0xEEF5BAF6
		txs[2] = "ZY.dll"
	elseif tx == "龙吟" then --新
		txs[1] = "A267208"
		txs[2] = "MAX.7z"
	elseif tx == "状态_莲步轻舞"  then
		txs[1] = 3387695093
		txs[2] = "ZY.dll"
	elseif tx == "状态_一笑倾城"   then
		txs[1] = 3387695093
		txs[2] = "ZY.dll"
	elseif tx == "加蓝" then
		txs[1] = 1049700101
		txs[2] = "ZY.dll"

	elseif tx == "狮搏" then --新
		txs[1] = 0x096766A8
		txs[2] = "ZY.dll"
	elseif tx == "尘土刃" then
		txs[1] = 0x25EC2A4D
		txs[2] = "ZY.dll"
	elseif tx == "百爪狂杀" then
		txs[1] = 0x25A881B4
		txs[2] = "ZY.dll"
	elseif tx == "天命剑法" then
		txs[1] = "837644B5"
		txs[2] = "MAX.7z"
	elseif tx == "状态_明光宝烛" then
		txs[1] = 540412418
		txs[2] = "JM.dll"
	elseif tx == "判官令" then --新
		txs[1] = 0x241C668A
		txs[2] = "ZY.dll"
	elseif tx == "状态_煞气决" then
		txs[1] = 2126428619
		txs[2] = "ZY.dll"
	elseif tx == "状态_惊魂掌" then
		txs[1] = 2156799976
		txs[2] = "ZY.dll"
	elseif tx == "错乱" then  --新
		txs[1] = 0x8D302CE0
		txs[2] = "ZY.dll"

	elseif tx == "驱尸" then  --新
		txs[1] = 0xA7BC14C9
		txs[2] = "ZY.dll"
	elseif tx == "驱魔" then  --新
		txs[1] = 0xF001A7B5
		txs[2] = "ZY.dll"
	elseif tx == "魔王回首" then
		txs[1] = 0x1597C433
		txs[2] = "ZY.dll"
	elseif tx == "状态_魔王回首" then
		txs[1] = 0xBBEA83F4
		txs[2] = "ZY.dll"
	elseif tx == "魔音摄魂" then --新
		txs[1] = 0x85E6AE55
		txs[2] = "ZY.dll"
	elseif tx == "姐妹同心" then --新
		txs[1] = 0x42ED98B8
		txs[2] = "ZY.dll"
	elseif tx == "状态_魔音摄魂" then
		txs[1] = 0xC4AA8E18
		txs[2] = "ZY.dll"
	elseif tx == "状态_炎护" then
		txs[1] = 0xD2578505
		txs[2] = "ZY.dll"
	elseif tx == "状态_后发制人" then
		txs[1] = 0x2A2A0663
		txs[2] = "ZY.dll"
	elseif tx == "阵型_不动如山" then
		txs[1] = 4106238113
		txs[2] = "wzife.wd2"
	elseif tx == "状态_碎星诀" then
		txs[1] = 814399755
		txs[2] = "ZY.dll"
	elseif tx == "心疗术" then
		txs[1] = 4227373440
		txs[2] = "ZY.dll"
	elseif tx == "状态_日月乾坤" then
		txs[1] = 0x313BCC79
		txs[2] = "ZY.dll"
	elseif tx == "状态_盘丝阵" then
		txs[1] = 0x69BF4309
		txs[2] = "ZY.dll"
	elseif tx == "落叶萧萧"  then --新
		txs[1] = 0x36BFE018
		txs[2] = "ZY.dll"
	-- elseif tx == "状态_象形" then
	-- 	txs[1] = 2213550572
	-- 	txs[2] = "ZY.dll"
	elseif tx == "死亡召唤" then
		txs[1] = 0x1B291F17
		txs[2] = "ZY.dll"
	elseif tx == "状态_死亡召唤" then
		txs[1] = 0x19182964
		txs[2] = "ZY.dll"

	elseif tx == "黄泉之息" then
		txs[1] = 0x1ADABFA6
		txs[2] = "ZY.dll"
	elseif tx == "锢魂术" then
		txs[1] = "ECE70F0"
		txs[2] = "MAX.7z"
	elseif tx == "状态_锢魂术" then
		txs[1] = 0x966EC3CB
		txs[2] = "ZY.dll"

	elseif tx == "幽冥鬼眼" then
		txs[1] = 0x21435203
		txs[2] = "ZY.dll"
	elseif tx == "状态_幽冥鬼眼" then
		txs[1] = 0x07DF52CA
		txs[2] = "ZY.dll"
	elseif tx == "状态_冰冻" then
		txs[1] = 2213550572
		txs[2] = "ZY.dll"
	elseif tx == "水漫金山" then
		txs[1] = 0x97311BAD
		txs[2] = "ZY.dll"
	elseif tx == "天雷斩" then  --新
		txs[1] = 0xB6141EB9
		txs[2] = "ZY.dll"
	elseif tx == "状态_紧箍咒" then
		txs[1] = "CE358A80"
		txs[2] = "MAX.7z"
	elseif tx == "命疗术" then
		txs[1] = 4088602190
		txs[2] = "ZY.dll"
	elseif tx == "状态_失心符" then
		txs[1] = 3635978625
		txs[2] = "ZY.dll"
	elseif tx == "定身符" then ---新
		txs[1] = 0x8485B3E2
		txs[2] = "ZY.dll"
	elseif tx == "状态_金身舍利" or tx == "金身舍利" then
		txs[1] = 0xECEC37F7
		txs[2] = "ZY.dll"
	elseif tx == "摧心术" then
		txs[1] = 2702496872
		txs[2] = "ZY.dll"
	elseif tx == "状态_摧心术" then
		txs[1] = 0x5512DA5C
		txs[2] = "ZY.dll"
	elseif tx == "碎甲术" or tx == "破甲术" then
		txs[1] = 3505309973
		txs[2] = "ZY.dll"
	elseif tx == "夺命咒" then
		txs[1] = 0x011F0C5B
		txs[2] = "ZY.dll"
	elseif tx == "状态_一苇渡江" then
		txs[1] = 382287583
		txs[2] = "ZY.dll"
	elseif tx == "一苇渡江" then
		txs[1] = 4082420920
		txs[2] = "ZY.dll"
	elseif tx == "凝气诀" then
		txs[1] = 843116756
		txs[2] = "ZY.dll"
	elseif tx == "状态_乘风破浪" then
		txs[1] = "E3677D73"
		txs[2] = "MAX.7z"
	elseif tx == "状态_冰封" then
		txs[1] = 324036383
		txs[2] = "ZY.dll"
	elseif tx == "如花解语" then --新
		txs[1] = 0xB493EF60
		txs[2] = "ZY.dll"
	elseif tx == "状态_杀气诀" then
		txs[1] = "C14BE652"
		txs[2] = "MAX.7z"
	elseif tx == "保护" then
		txs[1] = 4027829983
		txs[2] = "ZY.dll"
	elseif tx == "复苏" then
		txs[1] = 0xDC756B6F
		txs[2] = "ZY.dll"
	elseif tx == "状态_复苏" then
		txs[1] = 0xF946ED77
		txs[2] = "ZY.dll"
	elseif tx == "瘴气" then ---新
		txs[1] = 0x9B69747B
		txs[2] = "ZY.dll"
	elseif tx == "状态_催眠符" then
		txs[1] = "848B43BA"
		txs[2] = "MAX.7z"
	elseif tx == "安神诀" then ---新
		txs[1] = 0xE61DB8B4
		txs[2] = "ZY.dll"
	elseif tx == "状态_安神诀" then ---新
		txs[1] = 0xE61DB8B4
		txs[2] = "ZY.dll"
	elseif tx == "状态_横扫千军" or tx == "状态_破釜沉舟" then
		txs[1] = 0xD9463A0C
		txs[2] = "ZY.dll"
	elseif tx == "还阳术" then
		txs[1] = 1538345049
		txs[2] = "ZY.dll"
	elseif tx == "状态_毒" then
		txs[1] = 3687768876
		txs[2] = "ZY.dll"
	elseif tx == "月光" then
		txs[1] = 685510219
		txs[2] = "ZY.dll"
	elseif tx == "含情脉脉" then --新
		txs[1] = 0x5363AF2D
		txs[2] = "ZY.dll"
	elseif tx == "状态_错乱" then
		txs[1] = 0x2F78C46E
		txs[2] = "ZY.dll"
	elseif tx == "纵地金光" then
		txs[1] = 3546433571
		txs[2] = "ZY.dll"
	elseif tx == "血雨" then --新
		txs[1] = 0xBACD6F79
		txs[2] = "ZY.dll"
	elseif tx == "普渡众生" then --新
		txs[1] = 0x2FF74578
		txs[2] = "ZY.dll"
	elseif tx == "状态_颠倒五行" then
		txs[1] = 923673984
		txs[2] = "ZY.dll"
	elseif tx == "催眠符" then --新
		txs[1] = 0xB8E57120
		txs[2] = "ZY.dll"
	elseif tx == "冰川怒" then
		txs[1] = 0x20636506
		txs[2] = "ZY.dll"
	elseif tx == "状态_落魄符" then
		txs[1] = 3635978625
		txs[2] = "ZY.dll"
	elseif tx == "气疗术" then
		txs[1] = 4088602190
		txs[2] = "ZY.dll"
	elseif tx == "起死回生" then
		txs[1] = 2391977602
		txs[2] = "ZY.dll"
	elseif tx == "力劈华山" then
		txs[1] = 0xFF33B0EE
		txs[2] = "ZY.dll"

		elseif tx == "进阶力劈华山" then
		txs[1] = 0xFF33B0EE
		txs[2] = "ZY.dll"
	elseif tx == "腾雷" then --新
		txs[1] = 0x8A506A89
		txs[2] = "ZY.dll"
	elseif tx == "叶隐" then
		txs[1] = 1859374553
		txs[2] = "ZY.dll"
	elseif tx == "失心符" then --新
		txs[1] = 0x89B2D33A
		txs[2] = "ZY.dll"
	elseif tx == "腾云驾雾" then
		txs[1] = 871130409
		txs[2] = "ZY.dll"
	elseif tx == "不动如山" then
		txs[1] = 0xC514B5E7
		txs[2] = "ZY.dll"
	elseif tx  == "分身术" then --新
		txs[1] = 0x4B9ACA92
		txs[2] = "ZY.dll"
	elseif tx  == "镇妖" then --新
		txs[1] = 0x14965928
		txs[2] = "ZY.dll"
	elseif tx  == "状态_镇妖" then --新
		txs[1] = 0x98753F94
		txs[2] = "ZY.dll"
	elseif tx == "解毒" then
		txs[1] = 3727996990
		txs[2] = "ZY.dll"
	elseif tx == "奔雷咒" then
		txs[1] = 0xF54C4025
		txs[2] = "ZY.dll"
		elseif tx == "莲花心音" then
		txs[1] = 0xFE0B5253
		txs[2] = "ZY.dll"
	elseif tx == "明光宝烛" then
		txs[1] = 1479134995
		txs[2] = "JM.dll"
	elseif tx == "状态_移魂化骨" then
		txs[1] = 759125516
		txs[2] = "ZY.dll"
	elseif tx == "状态_干将莫邪" then
		txs[1] = 0x7838CE56
		txs[2] = "ZY.dll"
	elseif tx == "唧唧歪歪" then --新
		txs[1] = 0xC12C2663
		txs[2] = "ZY.dll"
	elseif tx == "连环击" then --新
		txs[1] =0x83020DAE
		txs[2] = "ZY.dll"
	elseif tx == "被击中" then
		txs[1] = 490729788
		txs[2] = "ZY.dll"
	elseif tx == "金甲仙衣" then
		txs[1] = 0x23EBA54C
		txs[2] = "ZY.dll"
	elseif tx == "降魔斗篷" then
		txs[1] = 0x23EBA54C
		txs[2] = "ZY.dll"
	elseif tx == "干将莫邪" then
		txs[1] = "D333F382"
		txs[2] = "MAX.7z"
	elseif tx == "翻江搅海" then
		txs[1] = 0x18967984
		txs[2] = "ZY.dll"
	elseif tx == "落雷符" then --新
		txs[1] = 0xC1731A60
		txs[2] = "ZY.dll"
	elseif tx == "日光华" then
		txs[1] = 0x45CA9913
		txs[2] = "ZY.dll"
	elseif tx == "地裂火" then  --新
		txs[1] = 0x18E13BE9
		txs[2] = "ZY.dll"
	elseif tx == "解封" then
		txs[1] = 1833750106
		txs[2] = "ZY.dll"
	elseif tx == "反震" then
		txs[1] = 3690777786
		txs[2] = "ZY.dll"
	elseif tx == "蜜润" then
		txs[1] = 3298164407
		txs[2] = "ZY.dll"
	elseif tx == "状态_蜜润" then
		txs[1] = 0x20DFE815
		txs[2] = "ZY.dll"
	elseif tx == "三花聚顶" then
		txs[1] = 2002768611
		txs[2] = "ZY.dll"
	elseif tx == "暴击" then
		txs[1] = 0xECD0E003
		txs[2] = "JM.dll"
	elseif tx == "法暴" then
		txs[1] = 0xDAD8AC20
		txs[2] = "ZY.dll"
	elseif tx == "宁心" then
		txs[1] = 0xA72FDB18
		txs[2] = "ZY.dll"
	elseif tx == "状态_天神护法" then
		txs[1] = 0xAC3D253B
		txs[2] = "ZY.dll"
	elseif tx == "荆棘舞" then
		txs[1] = 0x34826440
		txs[2] = "ZY.dll"
	elseif tx == "三昧真火" then --新
		txs[1] = 0x774B7CEF
		txs[2] = "ZY.dll"
	elseif tx == "我佛慈悲" then--新
		txs[1] = 0x6FC329C6
		txs[2] = "ZY.dll"
	elseif tx == "紧箍咒" then --新
		txs[1] = "88D9F645"
		txs[2] = "MAX.7z"
	elseif tx == "杨柳甘露" then
		txs[1] = 939734977
		txs[2] = "ZY.dll"
	elseif tx == "莲步轻舞" then --新
		txs[1] = 0x73D87E89
		txs[2] = "ZY.dll"

		elseif tx == "龙卷雨击1" then --新
		txs[1] = 0xD1772AED
		txs[2] = "ZY.dll"

		elseif tx == "龙卷雨击2" then --新
		txs[1] = 0xE926E4D8
		txs[2] = "ZY.dll"

		elseif tx == "龙卷雨击3" then --新
		txs[1] = 0x56F9A205
		txs[2] = "ZY.dll"

		elseif tx == "龙卷雨击4" then --新
		txs[1] = 0xD61E2519
		txs[2] = "ZY.dll"



	elseif tx == "靛沧海" then --新
		txs[1] = 0x6C5F8376
		txs[2] = "ZY.dll"
	elseif tx == "五雷咒" then --新
		txs[1] = 0x5BCBB64F
		txs[2] = "ZY.dll"
	-- elseif tx == "龙卷雨击2" then
	-- 	txs[1] = 3592299801
	-- 	txs[2] = "ZY.dll"
	elseif tx == "摄魄" then  --新
		txs[1] = 0xB1A504B7
		txs[2] = "ZY.dll"
	elseif tx == "生命之泉" then --新
		txs[1] = 0xA1A5E0CD
		txs[2] = "ZY.dll"
	elseif tx == "颠倒五行" then
		txs[1] = 923673984
		txs[2] = "ZY.dll"
	elseif tx == "乘风破浪" then
		txs[1] = "E2FA7AD5"
		txs[2] = "MAX.7z"
	elseif tx == "捕捉2" then
		txs[1] = 2601915514
		txs[2] = "ZY.dll"
	elseif tx == "清心" then
		txs[1] = 822452251
		txs[2] = "ZY.dll"
	elseif tx == "状态_天神护体" then
		txs[1] = 1724676649
		txs[2] = "ZY.dll"
	elseif tx == "勾魂" then ---新
		txs[1] = 0x1C57DA8A
		txs[2] = "ZY.dll"
	elseif tx == "炽火流离" then
		txs[1]="2470EE2A"
		txs[2] = 'MAX.7z'
	elseif tx == "焚魔烈焰" then
		txs[1]="2BAFA21D"
		txs[2] = 'MAX.7z'
	elseif tx == "谜毒之缚" then
	txs[1]="63BADFEC"
	txs[2] = 'MAX.7z'
	elseif tx == "怨怖之泣" then
	txs[1]="3A63D8A8"
	txs[2] = 'MAX.7z'
		elseif tx == "状态_谜毒之缚" then
	txs[1]=0x32D3023F
	txs[2] = 'ZY.dll'

	elseif tx == "怨怖之泣" then
	txs[1]="68E51BB6"
	txs[2] = 'MAX.7z'

	elseif tx == "净世煌火" then
	txs[1]="2AEB8662"
	txs[2] = 'MAX.7z'

		elseif tx =="破击"  then
	txs[1]="0116C932"
	txs[2] = 'MAX.7z'
		elseif tx =="匠心·蓄锐"	  then
	txs[1]="109B780F"
	txs[2] = 'MAX.7z'
		elseif tx =="匠心·削铁"   then
	txs[1]="048B222C"
	txs[2] = 'MAX.7z'
	elseif tx =="状态_匠心·削铁" then
	txs[1]="AFFB5781"
	txs[2] = 'MAX.7z'
	elseif tx =="匠心·固甲"  then
	txs[1]="17EDF032"
	txs[2] = 'MAX.7z'
	elseif tx =="状态_匠心·固甲" then
	txs[1]="0DB83081"
	txs[2] = 'MAX.7z'
		elseif tx =="锋芒毕露"   then
	txs[1]="1EDD5D74"
	txs[2] = 'MAX.7z'
	elseif tx == "状态_锋芒毕露" then
	txs[1]="33BBFA1D"
	txs[2] = 'MAX.7z'
	elseif tx =="诱袭"   then
	txs[1]="266B36D8"
	txs[2] = 'MAX.7z'
	elseif tx =="状态_诱袭" then
	txs[1]="0CEAF91A"
	txs[2] = 'MAX.7z'
		elseif tx =="针锋相对"  then
	txs[1]="76653AFB"
	txs[2] = 'MAX.7z'
		elseif tx =="攻之械" or tx =="齐天大圣" then
	txs[1]=0xC1B48527
	txs[2] = 'ZY.dll'


	elseif tx == "振翅千里" then
		txs[1] = 2661053669
		txs[2] = "ZY.dll"
	elseif tx == "逆鳞" then
		txs[1] = "613615A"
		txs[2] = "MAX.7z"
	elseif tx == "状态_逆鳞" then
		txs[1] = "ABF9B445"
		txs[2] = "MAX.7z"
	elseif tx == "象形"  then --新
		txs[1] = 0x73DA1CCF
		txs[2] = "ZY.dll"
	elseif tx == "惊涛怒" then
		txs[1] = 0x697E01E6
		txs[2] = "ZY.dll"
	elseif tx == "反间之计" then
		txs[1] = 1804814488
		txs[2] = "ZY.dll"
	elseif tx == "星月之惠" then
		txs[1] = 3298164407
		txs[2] = "ZY.dll"
	elseif tx == "斗转星移" then
		txs[1] = 1056770863
		txs[2] = "ZY.dll"
	elseif tx == "状态_含情脉脉" then
		txs[1] = 2164502482
		txs[2] = "ZY.dll"
	elseif tx == "雾杀" then
		txs[1] = 0x033F18D1
		txs[2] = "ZY.dll"
	elseif tx == "夺魄令" then
		txs[1] = 0x9F07F1C3
		txs[2] = "ZY.dll"
	elseif tx == "上古灵符" then
		txs[1] = 48901659
		txs[2] = "ZY.dll"
	elseif tx == "冰冻" then
		txs[1] = "90F782F7"
		txs[2] = "MAX.7z"
	elseif tx == "流沙" then
		txs[1] = "905278E4"
		txs[2] = "MAX.7z"
	elseif tx == "心火" then
		txs[1] = "A736125A"
		txs[2] = "MAX.7z"
	elseif tx == "怒雷" then
		txs[1] = "D34E6025"
		txs[2] = "MAX.7z"
	elseif tx == "升级" then
		txs[1] = 2604332261
		txs[2] = "ZY.dll"
	elseif tx == "放下屠刀" or tx == "河东狮吼" then
		txs[1] = 0x81B4599F
		txs[2] = "ZY.dll"
	elseif tx == "夜舞倾城" then
		txs[1] = 0xF18C76DD
		txs[2] = "ZY.dll"
	elseif tx == "打狗棒" then
		txs[1] = 0x1EE9406C
		txs[2] = "ZY.dll"
	elseif tx == "金刚护法"  then --新
		txs[1] = 0x876DBCC6
		txs[2] = "ZY.dll"
	elseif tx == "归元咒" or tx == "乾天罡气" then
		txs[1] = 1366709986
		txs[2] = "ZY.dll"
	elseif tx == "牛劲"  then --新
		txs[1] = 0x26F090BE
		txs[2] = "ZY.dll"
	elseif tx ==  "状态_牛劲" then --新
		txs[1] = 0xA168E26F
		txs[2] = "ZY.dll"
	elseif tx == "失魂符" then --新
		txs[1] = 0x2406DA40
		txs[2] = "ZY.dll"
	elseif tx == "碎甲符"  then --新
		txs[1] = "3BE6964F"
		txs[2] = "MAX.7z"
	elseif tx == "状态_失魂符" then
		txs[1] = "2EFE1BA6"
		txs[2] = "MAX.7z"
	elseif tx == "状态_失心符" then
		txs[1] = "2EFE1BA6"
		txs[2] = "MAX.7z"
	elseif tx == "状态_碎甲符" then
		txs[1] = 0xEB3D0AC1
		txs[2] = "ZY.dll"
	elseif tx == "飞花摘叶" then
		txs[1] = 0xCD98BD88
		txs[2] = "ZY.dll"
	elseif tx == "百毒不侵" then
		txs[1] = 0xD69CAE82
		txs[2] = "ZY.dll"
	elseif tx == "一笑倾城" then --新
		txs[1] = 0x935FA878
		txs[2] = "ZY.dll"
	elseif tx == "舍生取义" then --新
		txs[1] = 0xF0CE9045
		txs[2] = "ZY.dll"
	elseif tx == "状态_百毒不侵" then
		txs[1] = 0x860E67C9
		txs[2] = "ZY.dll"
	elseif tx == "状态_血雨" then
		txs[1] = 0x497E890D
		txs[2] = "ZY.dll"
	elseif tx == "状态_护法紫丝" then
		txs[1] = 0x042B06AC
		txs[2] = "ZY.dll"
	elseif tx == "状态_佛法无边" then
		txs[1] = 0xEA4D704A
		txs[2] = "ZY.dll"
	elseif tx == "佛法无边" then
		txs[1] = 0xC8011EF1
		txs[2] = "ZY.dll"
	elseif tx == "威慑" then
		txs[1] = 0x8476181B
		txs[2] = "ZY.dll"
	elseif tx == "状态_威慑" then
		txs[1] = 0x8476181B
		txs[2] = "ZY.dll"
	elseif tx == "舍生取义"  then--新
		txs[1] = "E464FF36"
		txs[2] = "MAX.7z"
	elseif tx == "状态_普渡众生"  then
		txs[1] = 0xD8857128
		txs[2] = "ZY.dll"
	elseif tx == "状态_灵动九天"  then
		txs[1] = 0x95FF4460
		txs[2] = "ZY.dll"
	elseif tx == "灵动九天"  then
		txs[1] = 0x3FCA8C5A
		txs[2] = "ZY.dll"
	elseif tx == "状态_神龙摆尾"  then
		txs[1] = "7A7262E2"
		txs[2] = "MAX.7z"
	elseif tx == "神龙摆尾"  then
		txs[1] = 0xA4790CE1
		txs[2] = "ZY.dll"
	elseif tx == "摇头摆尾"  then
		txs[1] = "5A4912AB"
		txs[2] = "MAX.7z"
	elseif tx == "状态_摇头摆尾"  then
		txs[1] = 0x59322DBA
		txs[2] = "ZY.dll"
	elseif tx == "火甲术"  then
		txs[1] = "607C9557"
		txs[2] = "MAX.7z"
	elseif tx == "状态_火甲术"  then
		txs[1] = 0x82D17DF8
		txs[2] = "ZY.dll"
	elseif tx == "瘴气"  then
		txs[1] = 0xBA6FC6C2
		txs[2] = "ZY.dll"
	elseif tx == "状态_瘴气"  then
		txs[1] = 0x0D1CD1E6
		txs[2] = "ZY.dll"

	elseif tx == "镇魂诀"  then
		txs[1] = 0x5927908D
		txs[2] = "ZY.dll"
	elseif tx == "状态_镇魂诀"  then
		txs[1] = 0x23634602
		txs[2] = "ZY.dll"
	elseif tx == "魔息术"  then
		txs[1] = 0x60AF6327
		txs[2] = "ZY.dll"
	elseif tx == "状态_魔息术"  then
		txs[1] = 0x0D25E055
		txs[2] = "ZY.dll"
	elseif tx == "波澜不惊"  then
		txs[1] = 0x842506A0
		txs[2] = "ZY.dll"
	elseif tx == "状态_波澜不惊"  then
		txs[1] = 0x842506A0
		txs[2] = "ZY.dll"
	elseif tx == "天魔解体"  then
		txs[1] = 0xD20E78BF
		txs[2] = "ZY.dll"
	elseif tx == "状态_天魔解体"  then
		txs[1] = 0x214905B6
		txs[2] = "ZY.dll"

	elseif tx == "金刚镯"  then
		txs[1] = 0xB4F1628C
		txs[2] = "ZY.dll"
	elseif tx == "状态_金刚镯"  then
		txs[1] = 0x2CE2133E
		txs[2] = "ZY.dll"
	elseif tx == "乾坤妙法"  then
		txs[1] = 0x184AA512
		txs[2] = "ZY.dll"
	elseif tx == "状态_乾坤妙法"  then
		txs[1] = 0x7F842CEF
		txs[2] = "ZY.dll"
	elseif tx == "天地同寿"  then
		txs[1] = 0xFAD3AD96
		txs[2] = "ZY.dll"
	elseif tx == "状态_天地同寿"  then
		txs[1] = 0x21F550FF
		txs[2] = "ZY.dll"
	elseif tx == "状态_鹰击"  then
		txs[1] = 0x58628406
		txs[2] = "ZY.dll"
	elseif tx == "状态_象形限制"  then
		txs[1] = 0x58628406
		txs[2] = "ZY.dll"

	elseif tx == "呼子唤孙" then
	txs[1] =0xA7BC14C9
	txs[2] ="ZY.dll"
	elseif tx == "云暗天昏" then
	txs[1] ="C79DA607"
	txs[2] = 'MAX.7z'
	elseif tx == "移星换斗" then
	txs[1] = 0xF9C3EE74
	txs[2] = "ZY.dll"


	elseif tx == "无所遁形" then
	txs[1] ="985D9164"
	txs[2] = 'MAX.7z'
	elseif tx == "状态_无所遁形" then
	txs[1] ="0CEAF91A"
	txs[2] = 'MAX.7z'
	elseif tx == "铜头铁臂" then
	txs[1] ="8BE2BAE9"
	txs[2] = 'MAX.7z'
	elseif tx == "状态_铜头铁臂" then
	txs[1] ="26ADD570"
	txs[2] = 'MAX.7z'
	elseif tx == "泼天乱棒" then
	txs[1] ="65955666"
	txs[2] = 'MAX.7z'
	elseif tx == "神针撼海" then
	txs[1]=0x1EDD5D74
	txs[2] = 'ZY.dll'
	elseif tx == "杀威铁棒" then
	txs[1]="05391F73"
	txs[2] = 'MAX.7z'
	elseif tx == "当头一棒" then
	txs[1]=0X3200B6CC
	txs[2] = 'ZY.dll'



	-- elseif tx == "九幽除名" then
	-- txs[1] ="F62896B4"
	-- txs[2] = 'MAX.7z'
	elseif tx == "八戒上身" then
	txs[1] ="AA5E6357"
	txs[2] = 'MAX.7z'
	elseif tx == "威震凌霄" then
	txs[1]="2ED4AF64"
	txs[2] = 'MAX.7z'
	elseif tx == "气慑天军" then
	txs[1]="BE7C40B6"
	txs[2] = 'MAX.7z'
	elseif tx == "状态_威震凌霄" then
	txs[1]=0xD2578505
	txs[2] = 'ZY.dll'
	elseif tx == "状态_气慑天军" then
	txs[1]=0x7CD6EADC
	txs[2] = 'ZY.dll'
	end
	return txs
end
function 引擎.取光环(tx)
	local n={}
	if tx == "烈焰澜翻" then
	   n[1]=0x52A385FE
	   n[2]=0x60FB8930
	   n[3]=0x1CDB4C3F
	   n[4]="ZY.dll"
	elseif tx == "水墨游龙" then
	   n[1]=0x0DF5F610
	   n[2]=0x8109B081
	   n[3]=0xFFCF398C
	    n[4]="ZY.dll"
	elseif tx == "星光熠熠" then
	   n[1]=0xCA82DD54
	   n[2]=0xFCC5CE3A
	   n[3]=0xCA82DD54
	      n[4]="ZY.dll"
	elseif tx == "双鲤寄情" then
	   n[1]=0xC056D8DD
	   n[2]=0x865FF084
	   n[3]=0x654EAD9E
	      n[4]="ZY.dll"
	elseif tx == "凌波微步" then
	   n[1]=0x6F1A7860
	   n[2]=0x242264D2
	   n[3]=0xE1D6A08F
	      n[4]="ZY.dll"
	elseif tx == "浩瀚星河" then
	   n[1]=0x200B20C2
	   n[2]=0x1E064D72
	   n[3]=0x04A28CA1
	      n[4]="ZY.dll"
	elseif tx == "荷塘涟漪" then
	   n[1]=0x085A1CE8
	   n[2]=0x4F049527
	   n[3]=0x268ED6D4
	      n[4]="ZY.dll"
	end
	return n
end
function 引擎.取脚印(tx)
	local n={}
	if tx =="飞天足迹" then
	   n[1]=0xE7B37A0B
	   n[2]="ZY.dll"
	elseif tx == "龙卷风足迹" then
		n[1]=0x57BCCF22
		n[2]="ZY.dll"
	elseif tx == "皮球足迹" then
		n[1]=0x9C146BBA
		n[2]="ZY.dll"
	elseif tx == "旋律足迹" then
		n[1]=0xD8D8AFFF
		n[2]="ZY.dll"
	elseif tx == "光剑足迹" then
		n[1]=0x988D7A27
		n[2]="ZY.dll"
	elseif tx == "地裂足迹" then
		n[1]=0xC86CEDDA
		n[2]="ZY.dll"
	elseif tx == "猫爪足迹" then
		n[1]=0x8A55BD39
		n[2]="ZY.dll"
	elseif tx == "爱心足迹" then
		n[1]=0xA30F641C
		n[2]="ZY.dll"
	elseif tx == "元宝足迹" then
		n[1]=0x8E5FA0FF
		n[2]="JM.dll"
	elseif tx == "桃心足迹(红)" then
		n[1]=0x9D1D8A21
		n[2]="ZY.dll"
	elseif tx == "桃心足迹(粉)" then
		n[1]=0x3CD4FA5F
		n[2]="ZY.dll"
	elseif tx == "波纹足迹(绿)" then
		n[1]=0x2E96BB2C
		n[2]="ZY.dll"
	elseif tx == "波纹足迹(蓝)" then
		n[1]=0x4E99F714
		n[2]="ZY.dll"
	elseif tx == "波纹足迹(粉)" then
		n[1]=0xFC24FBDD
		n[2]="ZY.dll"
	elseif tx == "蝴蝶足迹" then
		n[1]=0x238A4911
		n[2]="ZY.dll"
	elseif tx == "普通足迹" then
		n[1]=0xE9BFE67D
		n[2]="ZY.dll"
    elseif tx == "璀璨烟花" then
    	n[1]=0x2EA044B9
    	n[2]="ZY.dll"
    elseif tx == "跃动喷泉" then
    	n[1]=0x4274906D
    	n[2]="ZY.dll"
    elseif tx == "鱼群足迹" then
    	n[1]=0x01EEE4C8
    	n[2]="ZY.dll"
    elseif tx == "炎火足迹" then
    	n[1]=0x7D931979
    	n[2]="JM.dll"
    elseif tx == "旋星足迹" then
    	n[1]=0xFCA197EB
    	n[2]="ZY.dll"
    elseif tx == "星如雨" then
    	n[1]=0xB77ED878
    	n[2]="ZY.dll"
    elseif tx == "星光" then
    	n[1]=0x58D8C27C
    	n[2]="ZY.dll"
    elseif tx == "小心机" then
    	n[1]=0x977F441E
    	n[2]="ZY.dll"
    elseif tx == "甜蜜糖果" then
    	n[1]=0x2FF794F8
    	n[2]="ZY.dll"
    elseif tx == "藤蔓蔓延" then
    	n[1]=0xE5D60FFA
    	n[2]="ZY.dll"
    elseif tx == "闪光足迹" then
    	n[1]=0x38079F9D
    	n[2]="ZY.dll"
    elseif tx == "雀屏足迹" then
    	n[1]=0xFFED8401
    	n[2]="ZY.dll"
    elseif tx == "鬼脸南瓜" then
    	n[1]=0xF46601C7
    	n[2]="ZY.dll"
    elseif tx == "浮游水母" then
    	n[1]=0x5841D4FC
    	n[2]="ZY.dll"
    elseif tx == "枫叶足迹" then
    	n[1]=0xF879A813
    	n[2]="ZY.dll"
    elseif tx == "翅膀足迹" then
    	n[1]=0xE6B33F90
    	n[2]="ZY.dll"
    elseif tx == "采蘑菇" then
    	n[1]=0xF2848531
    	n[2]="ZY.dll"
    elseif tx == "随风舞" then
    	n[1]=0xF7071DAB
    	n[2]="ZY.dll"

	end
	   return n
end
