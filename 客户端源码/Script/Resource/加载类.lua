-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-18 16:59:40
-- WDF素材加载及加密
local 资源类_加载 = class()
local aaaa = require("gge纹理类")
local aaab = require("gge精灵类")
local aaac = require("Fmod类")
local bbbb = require("Script/Resource/动画类1")
local cccd = require("gge图像类")

--  local gxy_wdf  = require("gxy1.wdf")
-- local gxy_toid = require("gxy1.toid")
--  local _map = require("gxy1.map")
local yq = 引擎

function 资源类_加载:初始化()
    self.wdf = {}
  self.char = ffi.new('char[10485760]')
  self.p = tonumber(ffi.cast("intptr_t",self.char))
end

function 资源类_加载:打开()
    引擎.添加资源(程序目录..'res.npk',"test")
    -- mhgl1_3 特效组合
    引擎.添加资源(程序目录..'mhgl1_3.dll',"381990860qq")
    -- 引擎.添加资源(程序目录..'mhgl1_4.dll',"381990860qq")
    -- 引擎.添加资源(程序目录..'mhgl1_5.dll',"381990860qq")
    -- 引擎.添加资源(程序目录..'shop.dll',"381990860")

    local files = {
     "JM.dll",
    "ZY.dll",
    "ZHS.dll",
    'WP.dll',
    'WP1.dll',
    "smap.dll",
    "JS.dll",
    "sound.dll",
    "sound1.dll",
    }

    for n=1,#files do

            local __wdf = require("script/Resource/WDF")
            for n=1,#files do
                self.wdf[files[n]] = require("Glow/WDF类")(string.format(程序目录.."Lib/%s",files[n]),self.p,10485760)
                CacheFile[files[n]] ={}
            end


        CacheFile[files[n]] ={}
    end
    CacheFile["MAX.7z"] ={}
    CacheFile["resource"] ={}
     local __wdf = require("script/Resource/WDF1")
    local fileso = {
        "sound2.dll",
        "sound3.dll"
    }

    for n=1,#fileso do
        self.wdf[fileso[n]] = __wdf(string.format("%s/%s",程序目录.."Lib/",fileso[n]))
    end

end
function 资源类_加载:取偏移(file,id)
   if file == "sound.dll" then

   file = "sound2.dll"
   elseif file == "sound1.dll" then
   file = "sound3.dll"

   end
    return self.wdf[file]:读偏移(id)
end
function 资源类_加载:读数据(file,id)
    return self.wdf[file]:取文件(id)
end
function 资源类_加载:载入(文件,类型,文件号,缓存)
    print(文件,类型,文件号)

    if 类型 == "网易WDF动画" then
        -- 这个就是res.npk
        if 文件 =="MAX.7z" then
            local 临时 =引擎.资源取文件(文件号..'.was')
            local b=引擎.资源取大小(文件号..'.was')
            local ddd =bbbb(临时,b,文件,文件号,缓存)
            引擎.资源释放(临时)
            return ddd
        else
            if 文件号 ==nil then
                error(string.format("当前文件地址=%s文件号资源为空",文件 ))
            end

            local a,b=self.wdf[文件 or "JM.dll" ]:取文件(文件号+0 or 0xF51371AA)
            return bbbb(a,b,文件,文件号,缓存)
        end

    elseif 类型 == "图片" then
        return aaab(aaaa(文件))
    elseif 类型 == "音乐" then
        return aaac(文件,2,nil,nil,0 or 0)
    elseif 类型 == "加密图片" then
        local a,b=引擎.资源取文件(文件),引擎.资源取大小(文件)
        local c=aaaa(a,b)
        引擎.资源释放(a)
        return   aaab(c)
    elseif 类型 == "动画" then
        local 临时 =引擎.资源取文件(文件..'.was')
        --local a=ffi.cast("void*",临时)
        local b=引擎.资源取大小(文件..'.was')
        local ddd =bbbb(临时,b,"resource",文件)
        引擎.资源释放(临时)
        return ddd


    end
end
return 资源类_加载