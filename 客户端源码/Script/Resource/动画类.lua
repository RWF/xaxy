--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:52
--======================================================================--
local 动画类 = class()

local WAS类  = require("script/Resource/SP")
--local _was  = require("script/资源类/WAS类")
local jls   = require("gge精灵类")
local ceil  = math.ceil

function 动画类:初始化(p,len,文件,文件号,缓存)
    

     if 缓存 then
         self.was =WAS类(p,len,文件,文件号)
    else  
        if not CacheFile[文件][文件号] then 
            CacheFile[文件][文件号] =WAS类(p,len,文件,文件号)
        end
        self.was = CacheFile[文件][文件号]
   end
   self.文件号=文件号
   self.文件=文件
    self.方向数 = self.was.方向数
    self.帧数 = self.was.帧数
    self.宽度 =   self.was.宽度
    self.高度 = self.was.高度
    self.信息组 = {}
    self.信息组[0] = self.was:取纹理(0)
    self.数量 = self.was.总帧数
    self.精灵 = jls(self.信息组[0].精灵)
    self.开始帧 = 0
    self.结束帧 = 0
    self.当前帧 = 0
    self.时间累积 = 0
    self.差异 = 0
    self.已载入 = 0
    self.提速 = 0
    self.x = 0
    self:置方向(0)
end



function 动画类:置提速(v)
    self.提速 = v or 0
end

function 动画类:置染色(id,a,b,c,d)
    if id == nil  then
        return
    end
    if id == "黑白" then
        self.hdj = a
        return
    end
    CacheFile[self.文件][self.文件号] =nil
    self.was:置调色板(程序目录..[[Data\wpal\]]..id..".wpal")
    self.was:调色(a or 0,b or 0,c or 0,0)
    self.信息组 = {}
    self.信息组[0] = self.was:取纹理(0)
    self.精灵:置纹理(self.信息组[0].精灵)
    self.已载入 = 0
    self:置方向(0)
end

function 动画类:灰度级()
    self.精灵:灰度级()
end

function 动画类:是否选中(x,y) return self.精灵:是否选中(x,y) end

function 动画类:取间隔() return self.当前帧 - self.开始帧 end

function 动画类:取中间() return ceil((self.结束帧 - self.开始帧)/2) end

function 动画类:置差异(v) self.差异 = v end

function 动画类:置高亮(v) self:置混合(1) self:置颜色(-13158601) end

function 动画类:置高亮模式(a) self:置混合(1) self:置颜色(a) end

function 动画类:取消高亮(v) self:置混合(0) self:置颜色(4294967295) end

function 动画类:置混合(b) self.精灵:置混合(b or 0) end

function 动画类:置颜色(v,i) self.精灵:置颜色(v,i or -1) end

function 动画类:取宽度() if self.帧数 == 1 then return self.宽度+5 else return self.宽度 end end

function 动画类:取高度() if self.帧数 == 1 then return self.高度+5 else return self.高度+6 end end

function 动画类:取高度s() return self.高度 end

function 动画类:更新纹理()
    if self.信息组[self.当前帧] == nil then
        self.信息组[self.当前帧] = self.was:取纹理(self.当前帧)
        self.已载入 = self.已载入 +1
    end
    self.精灵:置纹理(self.信息组[self.当前帧].精灵)
end

function 动画类:置当前帧(条件)
    self.当前帧=条件
    self:更新纹理()
end
function 动画类:更新(dt,zl,pt,条件)
    dt = dt or 引擎.取帧时间()
    if zl ~= nil and zl ~= 0 then
        dt = dt / zl
    elseif self.提速 ~= 0 then
        dt = dt * self.提速
    end
    self.时间累积 = self.时间累积 + dt
    if (self.时间累积 > 0.1) then
        self.当前帧 = self.当前帧 + 1


         if (self.当前帧 > self.结束帧 - self.差异) then
            if  条件  then
            self.当前帧 =self.结束帧
            else
            self.当前帧 = self.开始帧
            end
        end

        self.时间累积 = 0
        if pt == nil then
            self:更新纹理()
        end
    end
end

function 动画类:显示(x,y)
    if self.信息组[self.当前帧] ~= nil then

        if y == nil then
            y,x=x.y,x.x
        end
        self.x = x - (self.信息组[self.当前帧].Key_X  or self.信息组[0].Key_X )
        self.y = y - (self.信息组[self.当前帧].Key_Y or self.信息组[0].Key_Y)
        if self.hdj then
            self.精灵:灰度级()
            self.精灵:置颜色(self.hdj)
        end
        self.精灵:显示(self.x,self.y)
    end
end

function 动画类:置方向(d,c)
    if self.方向~=d or c then
        if d ~= nil then
            if d > self.方向数 then
                d = 0
            end
            self.开始帧 = d * self.帧数
            self.结束帧 = self.帧数 + self.开始帧 - 1
            self.当前帧 = self.开始帧
            self:更新纹理()
            self.方向 = d
        end
    end
end

function 动画类:置区域(x,y,w,h)
    self.精灵:置区域(x,y,w,h)
end
function 动画类:置帧数(帧数)
    self.当前帧=帧数
end
function 动画类:换帧更新()
        self.开始帧 = self.方向 * self.帧数
        self.结束帧 = self.帧数 + self.开始帧 - 1
        self.当前帧 = self.开始帧
        self.时间累积=0
        self:更新纹理()

end

return 动画类



