-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-05-15 01:39:46

local 加载界面 = class()
local scale_w = (全局游戏宽度/800);
local scale_h = (全局游戏高度/600);
 local _GUI   = require("ggeui/加载类")()
 local 控件界面 = _GUI:创建控件('控件界面')
local tp
local 渐进加载界面_进度条= 控件界面:创建进度('渐进加载界面_进度条',308,344,200,17)
function 渐进加载界面_进度条:初始化()
  self:置颜色(0xFFffe8ce)
  self.i = 0

end

function 渐进加载界面_进度条:更新(dt)
  self.i = self.i +2
    if self.i==4 then
      CardData=ReadExcel("变身卡数据",授权)
      self.信息="变身卡数据"
    elseif self.i==10 then
      MapData=ReadExcel("地图数据",授权)
      self.信息="地图数据"
    elseif self.i==20 then
      SkillData=ReadExcel("技能数据",授权)
      self.信息="技能数据"
    elseif self.i==30 then  
      PetData=ReadExcel("召唤兽数据",授权)
      PetDataN={[1]={},[2]={},[3]={},[4]={},[5]={}}
          for k,v in pairs(PetData) do
             if v.换造型  then
                if v.等级>=0  and v.等级<= 25 then 
                  PetDataN[1][#PetDataN[1]+1]=k
                elseif v.等级>=35  and v.等级<= 65 then
                       PetDataN[2][#PetDataN[2]+1]=k
                elseif v.等级>=75  and v.等级<= 105 then
                   PetDataN[3][#PetDataN[3]+1]=k
                elseif v.等级>=115  and v.等级<= 145 then 
                    PetDataN[4][#PetDataN[4]+1]=k
                elseif v.等级>=155  and v.等级<= 175 then 
                     PetDataN[5][#PetDataN[5]+1]=k
                end
             end
          end
    elseif self.i==36 then
      Reward=ReadExcel("首服奖励",授权) 
    elseif self.i==40 then
      ItemData=ReadExcel("物品数据",授权) 
      self.信息="物品数据"
    elseif self.i==50 then
      ModelData=ReadExcel("模型数据",授权) 
      self.信息="模型数据"      
    elseif self.i==60 then
      PropertyData=ReadExcel("灵饰特性数据",授权)
      self.信息="灵饰特性数据"      
    elseif self.i==70 then
      LabelData=ReadExcel("称谓数据",授权)
      self.信息="称谓数据"   
    elseif self.i==80 then
       require "Script/Resource/Dye" 
      self.信息="染色数据"
    elseif self.i==90 then
      WeaponModel=ReadExcel("光武拓印",授权)
      self.信息="光武拓印"     
    elseif self.i==96 then
        robottrust=ReadExcel("挂机系统",授权)
        SpiritData=ReadExcel("元神系统",授权)
        self.信息="元神系统"
      end
   tp.字体表.描边字体_:显示(300*scale_w,380*scale_h,string.format("正在加载%s进度%s",self.信息,self.i.."%"))

  if self.i >100 then
      self.i = 1
  end
  渐进加载界面_进度条:置位置(self.i)
  if self.i==100 then

   
    渐进加载界面_进度条:置可视(false)
    require("Script/Resource/Resource").创建(tp)
    
    tp.标题 = require("script/Login/Caption").创建(tp)
    tp.读取 = require("script/Login/Zone").创建(tp)
    tp.登入 = require("script/Login/Login").创建(tp)
    tp.创建 = require("script/Login/New").创建(tp)
    tp.注册 = require("script/Login/Register").创建(tp)
    tp.选择 = require("script/Login/Select").创建(tp)

   tp.窗口 = {
      CDK = require("script/UI/CDK").创建(tp),
      炼丹炉 = require("script/UI/炼丹炉").创建(tp),
      商会唤兽管理= require("script/UI/商会唤兽管理").创建(tp),
      唤兽店铺= require("script/UI/唤兽店铺").创建(tp),
      物品店铺= require("script/UI/物品店铺").创建(tp),
      黑市拍卖 = require("script/UI/黑市拍卖").创建(tp),
      法宝神器 = require("script/UI/法宝神器").创建(tp),
      成就弹窗 = require("script/UI/成就弹窗").创建(tp),
      成长之路 = require("script/UI/成长之路").创建(tp),
       首服奖励 = require("script/UI/首服奖励").创建(tp),
      召唤兽查看栏 = require("script/UI/召唤兽显示类").创建(tp),
      召唤兽资质栏 = require("script/UI/召唤兽资质栏").创建(tp),
      组合输入框 = require("script/UI/组合输入框").创建(tp),
      召唤兽属性栏 = require("script/UI/召唤兽属性栏").创建(tp),
       召唤兽换造型 = require("script/UI/召唤兽换造型").创建(tp),
      召唤兽仓库 = require("script/UI/召唤兽仓库").创建(tp),
      宠物状态栏 = require("script/UI/宠物状态栏").创建(tp),
      宠物领养栏 =  require("script/UI/宠物领养栏").创建(tp),
      存钱界面 = require("script/UI/存钱界面").创建(tp),
      取钱界面 = require("script/UI/取钱界面").创建(tp),
      人物称谓栏 = require("script/UI/人物称谓栏").创建(tp),
      好友列表 = require("script/UI/好友列表").创建(tp),
      好友查看 = require("script/UI/好友查看").创建(tp),
      物品查看= require("script/UI/物品查看类").创建(tp),
      幻化界面= require("script/UI/幻化界面").创建(tp),
      飞行符 = require("script/UI/飞行符").创建(tp),
      系统设置 = require("script/UI/系统设置").创建(tp),
      人物状态栏 = require("script/UI/人物状态栏").创建(tp),
      技能学习 =  require("script/UI/技能学习").创建(tp),
      奇经八脉 = require("script/UI/奇经八脉").创建(tp),
      自由技能栏 = require("script/UI/自由技能栏").创建(tp),
      梦幻指引 = require("script/UI/梦幻指引").创建(tp),
      队伍栏 = require("script/UI/队伍栏").创建(tp),
      队伍阵型栏 = require("script/UI/队伍阵型栏").创建(tp),
      队伍栏申请 = require("script/UI/队伍栏申请").创建(tp),
      道具行囊 = require("script/UI/道具行囊").创建(tp),
      灵饰 = require("script/UI/灵饰").创建(tp),
      出售 =  require("script/UI/出售").创建(tp),
      打造 = require("script/UI/打造").创建(tp),
      道具仓库 = require("script/UI/道具仓库").创建(tp),
      商店 = require("script/UI/商店").创建(tp),
      宠物炼妖栏 =  require("script/UI/宠物炼妖栏").创建(tp),
      合成 =  require("script/UI/合成").创建(tp),
      交易界面 = require("script/UI/交易界面").创建(tp),
       玩家给予 = require("script/UI/玩家给予").创建(tp),
       NPC给予 = require("script/UI/NPC给予").创建(tp),
      商城 = require("script/UI/商城").创建(tp),
      符石 = require("script/UI/符石").创建(tp),
      开运界面 = require("script/UI/开运界面").创建(tp),
      摆摊出售 = require("script/UI/摆摊出售").创建(tp),
      摆摊购买 = require("script/UI/摆摊购买").创建(tp),
      消息框窗口 = require("script/UI/消息框窗口").创建(tp),
      动作界面 = require("Script/UI/动作界面").创建(tp),
      染色 = require("script/UI/染色").创建(tp),
      幻化属性 = require("script/UI/幻化属性").创建(tp),
      兑换界面 = require("script/UI/兑换界面").创建(tp),
       聊天框类 = require("Script/UI/聊天框类").创建(tp),
       人物框 = require("script/System/人物框").创建(tp),
       任务追踪 = require("Script/UI/任务追踪").创建(tp),
       任务栏 = require("script/UI/任务栏").创建(tp),
       对话栏 = require("script/UI/对话栏").创建(tp),
       底图框 = require("script/System/底图框").创建(tp),
       时辰= require("script/System/时辰").创建(tp),
       文本栏 = require("script/UI/文本栏").创建(tp),
       快捷技能栏 = require("script/UI/快捷技能栏").创建(tp),
      分解 = require("script/UI/分解").创建(tp),
      换装 = require("script/UI/换装").创建(tp),
      系统消息 = require("script/UI/系统消息").创建(tp),
      加锁 = require("script/UI/加锁").创建(tp),
      设置密码 = require("script/UI/设置密码").创建(tp),
      解除密码 = require("script/UI/解除密码").创建(tp),
      加入帮派 = require("script/UI/加入帮派").创建(tp),
      道具拍卖 = require("script/UI/道具拍卖").创建(tp),
      道具竞拍 = require("script/UI/道具竞拍").创建(tp),
      --召唤兽拍卖 = require("script/UI/召唤兽拍卖").创建(tp),
      道具小窗口= require("script/UI/道具小窗口").创建(tp),
      抽奖= require("script/UI/抽奖").创建(tp),
       小地图 = require("script/MAP/小地图").创建(tp),--
	   华山论剑= require("script/UI/华山论剑").创建(tp),
	   华山论剑排行= require("script/UI/华山论剑排行").创建(tp),
      自动寻路 = require("script/UI/自动寻路").创建(tp),--
      BOSS战斗 = require("script/UI/BOSS战斗").创建(tp),--
      合成旗类 = require("script/UI/合成旗类").创建(tp),--
      修炼 = require("script/UI/修炼").创建(tp),--
        玩家小窗口 = require("script/UI/玩家小窗口").创建(tp),--
         商会管理类 = require("script/UI/商会管理类").创建(tp),--
         游戏公告 = require("script/UI/游戏公告").创建(tp),
         帮派类 = require("script/UI/帮派类").创建(tp),
        帮派申请列表类 = require("script/UI/帮派申请列表类").创建(tp),
        帮派成员类 = require("script/UI/帮派成员类").创建(tp),
        帮派信息类 = require("script/UI/帮派信息类").创建(tp),
         商会购买类 = require("script/UI/商会购买类").创建(tp),
        坐骑 = require("script/UI/坐骑").创建(tp),
        坐骑技能 = require("script/UI/坐骑技能").创建(tp),
        法宝合成 = require("script/UI/法宝合成").创建(tp),
      世界地图分类小地图 = require("script/MAP/世界地图分类小地图").创建(tp),
      世界大地图= require("script/MAP/世界大地图").创建(tp),
      世界大地图分类a= require("script/MAP/世界大地图分类a").创建(tp),
      世界大地图分类b= require("script/MAP/世界大地图分类b").创建(tp),
      世界大地图分类c= require("script/MAP/世界大地图分类c").创建(tp),
      世界大地图分类d= require("script/MAP/世界大地图分类d").创建(tp),
      召唤兽染色= require("script/UI/召唤兽染色").创建(tp),
      队标栏= require("script/UI/队标栏").创建(tp),
      排行榜= require("script/UI/排行榜").创建(tp),
      仙玉抽奖= require("script/UI/仙玉抽奖").创建(tp),
      祈福= require("script/UI/祈福").创建(tp),
      坐骑染色= require("script/UI/坐骑染色").创建(tp),
      武器染色= require("script/UI/武器染色").创建(tp),
      查看装备= require("script/UI/查看装备").创建(tp),
      -- 圣旨= require("script/UI/圣旨").创建(tp),
      签到= require("script/UI/签到").创建(tp),
      拓展功能= require("script/UI/拓展功能").创建(tp),
      攻略= require("script/UI/攻略").创建(tp),
      活跃兑换= require("script/UI/活跃兑换").创建(tp),
      帮战= require("script/UI/帮战").创建(tp),
      好友聊天= require("script/UI/好友聊天").创建(tp),
      好友查找= require("script/UI/好友查找").创建(tp),
      改变双加= require("script/UI/改变双加").创建(tp),
      光武拓印= require("script/UI/光武拓印").创建(tp),
      仙玉广播= require("script/System/仙玉广播").创建(tp),

      助战界面 = require("script/QQ2124872/助战界面").创建(tp),
      助战物品界面 = require("script/QQ2124872/助战物品").创建(tp),
      助战技能栏 = require("script/QQ2124872/助战技能栏").创建(tp),
      助战宠物 = require("script/QQ2124872/助战宠物").创建(tp),
      助战锦衣 = require("script/QQ2124872/助战锦衣").创建(tp),
      助战修炼界面 = require("script/QQ2124872/助战修炼界面").创建(tp),
      助战技能学习 = require("script/QQ2124872/助战技能学习").创建(tp),
      助战辅助技能 = require("script/QQ2124872/助战辅助技能").创建(tp),
      助战奇经八脉 = require("script/QQ2124872/助战奇经八脉").创建(tp),

      挂机系统 = require("script/QQ2124872/挂机系统").创建(tp),
      元神系统 = require("script/QQ2124872/元神").创建(tp),
      -- 点歌台 = require("script/Mp3/点歌台").创建(tp),
      -- 点歌播放 = require("script/Mp3/点歌播放").创建(tp),
      -- 藏宝阁 = require("script/Mp3/藏宝阁类").创建(tp),
      -- 藏宝阁出售 = require("script/Mp3/藏宝阁出售").创建(tp),
      -- 藏宝阁银子出售 = require("script/Mp3/藏宝阁银子出售").创建(tp),
      -- 藏宝阁角色出售 = require("script/Mp3/藏宝阁角色出售").创建(tp),
      -- 藏宝阁角色取回 = require("script/Mp3/藏宝阁角色取回").创建(tp)
  }

    tp.场景 = require("script/MAP/游戏场景").创建(tp)
    tp.进程=1
     

  end

end



function 加载界面:初始化(根)
  
  客户端:连接处理()
  tp=根
  -- self.梦幻LOGO = 根.资源:载入("logo2.png","加密图片")
  self.梦幻LOGO0 = 根.资源:载入("load0.jpg","加密图片")
  self.梦幻LOGO1 = 根.资源:载入("load1.jpg","加密图片")
  self.梦幻LOGO2 = 根.资源:载入("load2.jpg","加密图片")
  self.梦幻LOGO3 = 根.资源:载入("load3.jpg","加密图片")
  -- self.梦幻LOGO = 根.资源:载入("jy.wdf","网易WDF动画",0x4a28ca1)
end

    local n = 0;
function 加载界面:显示(dt,x, y)
    -- self.梦幻LOGO:置拉伸(-2,0,全局游戏宽度,全局游戏高度)
    if n >= 0 and n < 20 then
       self.梦幻LOGO0:显示(全局游戏宽度/2-self.梦幻LOGO0.宽度/2,全局游戏高度/2-self.梦幻LOGO0.高度/2)
    elseif n >= 20 and n<40 then
       self.梦幻LOGO1:显示(全局游戏宽度/2-self.梦幻LOGO1.宽度/2,全局游戏高度/2-self.梦幻LOGO1.高度/2)
    elseif n >= 40 and n<60 then
       self.梦幻LOGO2:显示(全局游戏宽度/2-self.梦幻LOGO2.宽度/2,全局游戏高度/2-self.梦幻LOGO2.高度/2)
    elseif n >= 60 and n<80 then
        self.梦幻LOGO3:显示(全局游戏宽度/2-self.梦幻LOGO3.宽度/2,全局游戏高度/2-self.梦幻LOGO3.高度/2)
    end
    n = n +1;
    if n == 80 then
      n = 0;
    end

        _GUI:更新(dt,x, y)
        -- _GUI:显示(x, y)
      控件界面:置可视(true, true)
end


return 加载界面

