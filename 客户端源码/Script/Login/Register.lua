-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-12 01:41:44
-- 注册用户
local 注册 = class()
local 临时 = 2
local zts,zts1,zts2
function 注册:初始化(根)
local 按钮 = 根._按钮
local 资源 = 根.资源
local 自适应 =根._自适应

	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('创建控件')
	总控件:置可视(true,true)
	self.zczh = 总控件:创建输入("注册账号",380+(全局游戏宽度-800)/2,152,120,14,根)
	self.zczh:置可视(false,false)
	self.zczh:置限制字数(10)
    self.zczh:置文字颜色(0xFFFFFFFF)
	self.zcmm = 总控件:创建输入("注册密码",380+(全局游戏宽度-800)/2,202,120,14,根)
	self.zcmm:置可视(false,false)
	self.zcmm:置限制字数(10)
	self.zcmm:置密码模式()
    self.zcmm:置文字颜色(0xFFFFFFFF)
	self.qrmm = 总控件:创建输入("确认密码",380+(全局游戏宽度-800)/2,252,120,14,根)
	self.qrmm:置可视(false,false)
	self.qrmm:置限制字数(10)
	self.qrmm:置密码模式()
    self.qrmm:置文字颜色(0xFFFFFFFF)
	self.aqm = 总控件:创建输入("安全码",380+(全局游戏宽度-800)/2,302,120,14,根)
	self.aqm:置可视(false,false)
	self.aqm:置限制字数(10)
    self.aqm:置文字颜色(0xFFFFFFFF)


		self.资源组 = {

		[1] = 资源:载入('MAX.7z',"网易WDF动画",00010020),
		[2] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),
		[3] =  根._按钮(资源:载入('MAX.7z',"网易WDF动画",01000021),0,0,3,true,true), -- 按钮.创建(自适应.创建(12,4,43,22,1,3),0,0,4,true,true,"注册"),
		[4] = 根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010011),0,0,3,true,true),--  按钮.创建(自适应.创建(12,4,43,22,1,3),0,0,4,true,true,"取消"),
		[5]  = 自适应.创建(3,1,150,22,1,3)

	}

		tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.普通字体__
	zts2 = tp.字体表.描边字体

end


function 注册:检查点(x,y)
	if self.资源组[1]:是否选中(x,y)  then
		return true
	end
end


function 注册:显示(dt,x,y)
	local bbs = {"账号输入：","密码输入：","确认密码：","*安全码："}
	-- if  引擎.按键弹起(0x9) then
 --    if 临时 == 1  then
 --    	self.zczh:置焦点(true)
 --    	临时 =2
 -- 	elseif 临时==2 and  self.zczh:取焦点() then
 -- 		self.zcmm:置焦点(true)
 -- 		临时 = 3
 -- 	elseif 临时==3 and self.zcmm:取焦点() then
 -- 		self.qrmm:置焦点(true)
 -- 		临时 = 4
 -- 	elseif 临时==4 and self.qrmm:取焦点() then

 -- 		self.aqm:置焦点(true)
 -- 		临时 = 1
 -- 	end
 --   end
        self.资源组[1]:显示(215+(全局游戏宽度-800)/2,60)
  
        --self.资源组[2]:显示(473+(全局游戏宽度-800)/2,106)
        self.资源组[3]:显示(280+(全局游戏宽度-800)/2,340)
        self.资源组[4]:显示(450+(全局游戏宽度-800)/2,340)
         --self.资源组[2]:更新(x,y)
        self.资源组[3]:更新(x,y)
        self.资源组[4]:更新(x,y)

   -- for i=0,3 do
   --    self.资源组[5]:显示(320+(全局游戏宽度-800)/2,150+i*50)
   --    end
      --   for i=1,#bbs do
      -- zts:显示(265+(全局游戏宽度-800)/2,103+i*50,bbs[i])

      -- end


	zts2:置颜色(0xFFFF0000)
	zts2:显示((全局游戏宽度-800)/2+305,123,"*请牢记安全码可以找回账号和修改密码！")
	zts2:置颜色(0xFFFFFFFF)
	self.控件类:显示(x,y)
	self.控件类:更新(dt,x,y)
	self.zczh:置可视(true,true)
	self.zcmm:置可视(true,true)
	self.aqm:置可视(true,true)
	self.qrmm:置可视(true,true)

     if (self.资源组[2]:事件判断() or self.资源组[4]:事件判断()) or (self:检查点(x,y)and 引擎.鼠标弹起(0x01)) then
        tp.进程= 1
       elseif  self.资源组[3]:事件判断()  then
        	if self.zcmm:取文本() ~= self.qrmm:取文本() then
        	    tp.提示:写入("#Y/请确认密码重新输入")

            elseif   self.zcmm:取文本() =="" or self.qrmm:取文本() =="" or self.zczh:取文本() ==""   or self.aqm:取文本()== "" then
             tp.提示:写入("#Y/请输入完整的账号密码以及安全码")
          else
				local fun  = require("ffi函数")
				local 发送信息 = {
					c=self.zczh:取文本(),
					d=self.zcmm:取文本(),
					e=self.aqm:取文本()
				}
				客户端:发送(9,cjson.encode(发送信息))
                
             end
       end
end

return 注册