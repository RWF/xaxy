-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-05-18 00:06:31
-- 选择角色
local 选择角色 = class()
local tp

local 坐标={[1]={x=582,y=62},
[2]={x=592,y=125},
[3]={x=599,y=186},
[4]={x=599,y=250},
[5]={x=592,y=313},
[6]={x=581,y=371},}

function 选择角色:初始化(根)
  tp =根
  local 资源 = 根.资源
  --self.读取背景 = 资源:载入('JM.dll',"网易WDF动画",0xF47516C0)
  self.读取信息 = 资源:载入('MAX.7z',"网易WDF动画",00010019)
  --self.选择角色 = 资源:载入('JM.dll',"网易WDF动画",0xCA43E749)
  self.背景图片= 资源:载入("背景.png","加密图片")

  local 按钮 = 根._按钮
  self.上一步 =   根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010009),0,0,3,true,true)
  self.创建 =    根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010013),0,0,3,true,true)
  self.进入游戏 =   根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010003),0,0,3,true,true)
  self.档位 = {}
  local x = 0
  local y = 0
  local sj = {}
  self.发送时间=0
  -- self.选中 = 0
  --默认选中第一个
  self.选中 = 1
  self.上一焦点 = 0
end

function 选择角色:显示(dt,x,y)
  self.背景图片:置拉伸(0,0,全局游戏宽度,全局游戏高度)
  -- self.背景图片:置区域(0,0,全局游戏宽度,全局游戏高度)
  self.背景图片:显示(0,0)

  --self.读取背景:显示(360+(全局游戏宽度-800)/2,105)
  --self.选择角色:显示(395+(全局游戏宽度-800)/2,60)
  self.读取信息:显示(30*scale_w,505*scale_h)
  self.上一步:显示(620*scale_w,495*scale_h)
 -- if self.档位[self.选中] ~= nil and self.档位[self.选中].开启 then
    self.进入游戏:显示(620*scale_w,540*scale_h)

    self.创建:显示(620*scale_w,450*scale_h)
 -- end

  for n=1,#self.档位 do
    self.档位[n]:显示(x*scale_w,y*scale_h)
  end
  for n=1,#self.档位 do
    self.档位[n]:更新(dt,x,y)
    if self.档位[n].事件 then
      self.选中 = n
    end
  end
  self.创建:更新(x,y)
  self.上一步:更新(x,y)
  if self.上一步:事件判断() then
    self.选中 = 1
    self.上一焦点 = 0
    self.档位 = {}
    引擎.场景.进程 = 3
  elseif self.创建:事件判断() then
      tp.进程 = 4

  end
  if self.档位[self.选中] ~= nil and self.档位[self.选中].开启 then
      self.进入游戏:更新(x,y)
      if self.进入游戏:事件判断() then
      if self.发送时间 == nil then self.发送时间 = 全局时间-2 end;
      print(全局时间)
      print(self.发送时间)
       if 全局时间-self.发送时间 >=2 then
        self.发送时间=全局时间
        self.发送信息 = {账号=游戏账号,密码=游戏密码,编号=self.选中}
        客户端:发送(0,cjson.encode(self.发送信息))
       else
            tp.提示:写入 ("请稍后再尝试！")
        end
    end
  else

  end
end


function 选择角色:添加角色信息(信息)
  if 信息==nil  then
       error("玩家数据异常,请联系管理员")
  end
  for n=1,#信息 do
    self.档位[n] = require("script/System/读取_格子")(坐标[n].x,坐标[n].y ,n)
    self.档位[n]:置档位(信息[n],tp)

  end

	tp.进程 = 7

end

return 选择角色