-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-05-15 01:56:27
-- 首页
local 场景类_标题 = class()
local tp
function 场景类_标题:初始化(根)


	local xx = {{"1989C5FC",0xEB3FD8C3,0xEC1A0419},{"66279210",0x16E9D48F,0x79560528},{"07B8C541",0x7BBB735E,nil}}
	local sj = 引擎.取随机整数(1,3)
	local 资源 = 根.资源
	self.标题背景 = 资源:载入(xx[sj][1]..".jpg","加密图片")

	self.场景覆盖 = 资源:载入('JM.dll',"网易WDF动画",xx[sj][2])
	self.蓝框= 资源:载入('MAX.7z',"网易WDF动画",00010018)
	self.场景计次 = self.场景覆盖.宽度
	self.场景覆盖:置区域(self.场景计次,0,全局游戏宽度,全局游戏高度)
	if xx[sj][3] ~= nil then
		self.特效覆盖 = 资源:载入('JM.dll',"网易WDF动画",xx[sj][3])
		self.特效计次 = self.特效覆盖.宽度/2+450
		self.特效覆盖:置区域(self.特效计次,0,全局游戏宽度,全局游戏高度)
	end
	local dh = {0xDC739617,0x22E6E35C,0x16CC1B46,0xD8632D20}
    self.动画 = {}
	for n=1,4 do
	    self.动画[n] = 资源:载入('JM.dll',"网易WDF动画",dh[n])
	end
	self.进入游戏 = 根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010003),0,0,3,true,true)
	self.注册账号 = 根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010001),0,0,3,true,true)
	self.制作团队 = 根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010004),0,0,3,true,true)
	self.退出游戏 = 根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010002),0,0,3,true,true)
    self.上翻 = 根._按钮(资源:载入('JM.dll',"网易WDF动画",0x7AB5584C),0,0,3,true,true)
	self.下翻 = 根._按钮(资源:载入('JM.dll',"网易WDF动画",0xCB50AB1D),0,0,3,true,true)

	self.公告榜 = 根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010006),0,0,3,true,true)
	self.游戏建议 = 根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010007),0,0,3,true,true)
	self.服务条款 = 根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010008),0,0,3,true,true)

	self.游戏公告=根._按钮(资源:载入('MAX.7z',"网易WDF动画",01000051),0,0,1,true,true)
	self.超丰富文本 = require("script/System/丰富文本")(530,340)
	 for n=0,119 do
    self.超丰富文本:添加元素(n,根.包子表情动画[n])
    end
   for n=0,53 do
    self.超丰富文本:添加元素(400+n,根.大包子表情动画[n])
  end
     self.超丰富文本:添加文本("                       #95/#r/更新公告（注册账号按钮在这边）#95/")
     self.超丰富文本:添加文本(游戏公告)
	self.超丰富文本.滚动值 = self.超丰富文本.行数量
	self.超丰富文本:滚动(self.超丰富文本.滚动值)
	self.超丰富文本:滚动(-16)
	self.介绍加入 = 0

	tp = 根

end

function 场景类_标题:显示(dt,x,y)
	self.标题背景:置区域(self.特效计次,0,全局游戏宽度,全局游戏高度)
	self.标题背景:显示(0,0)
	self.场景计次 = self.场景计次 - 0.3
	self.场景覆盖:置区域(self.场景计次,0,全局游戏宽度,全局游戏高度)
	self.场景覆盖:显示(0,全局游戏高度)

	for n=1,4 do
		self.动画[n]:更新(dt)
	    self.动画[n]:显示(全局游戏宽度/2-240 + (n-1) *110,全局游戏高度-80)
	end
	if self.特效覆盖 ~= nil then
		self.特效计次 = self.特效计次 - 0.7
		self.特效覆盖:置区域(self.特效计次,0,全局游戏宽度,全局游戏高度)
		self.特效覆盖:显示(0,全局游戏高度)
	end
	if tp.进程== 1 then
		self.进入游戏:更新(x,y)
		self.注册账号:更新(x,y)
		self.制作团队:更新(x,y)
		self.退出游戏:更新(x,y)

		self.游戏公告:更新(x,y)
		self.公告榜:更新(x,y)
		self.游戏建议:更新(x,y)
		self.服务条款:更新(x,y)

		 self.上翻:更新(x,y, self.超丰富文本.行数量>21 and self.超丰富文本.滚动值 < self.超丰富文本.行数量-21)
         self.下翻:更新(x,y,self.超丰富文本.滚动值~=0)
		if self.进入游戏:事件判断() then
			tp.进程= 2
			tp.选中窗口 = nil
		elseif self.注册账号:事件判断() then
			tp.进程= 5
		elseif self.游戏建议:事件判断() then
			os.execute('start http://xyq.163.com/newer/')
		elseif self.制作团队:事件判断() then
			os.execute('start http://xyq.163.com/newer/')
		elseif self.退出游戏:事件判断() then
			引擎.关闭()
			return false
		elseif self.下翻:事件判断() then

			self.超丰富文本:滚动(-5)
		elseif self.上翻:事件判断() then
			self.超丰富文本:滚动(5)
		end
		if self.游戏公告:是否选中(x,y) then
			self.游戏公告:置高亮()
		else
			self.游戏公告:取消高亮()

		end

	end
	if tp.进程== 1 or tp.进程== 5 then

      local msg宽度 =全局游戏宽度/2-360--200
      local msg高度 =全局游戏高度/2-160--200
		self.蓝框:显示(msg宽度,msg高度)
		self.公告榜:显示(msg宽度+30,msg高度+25)
		self.游戏建议:显示(msg宽度+230,msg高度+25)
		self.服务条款:显示(msg宽度+430,msg高度+25)

		self.游戏公告:显示(msg宽度+160,msg高度-150)
		self.超丰富文本:显示(msg宽度+15,msg高度+90)
	   self.上翻:显示(msg宽度+ 540,msg高度+82)
		if self.超丰富文本.行数量>18 then
		    self.下翻:显示(msg宽度+ 540,msg高度 + 398)
		end

		self.注册账号:显示(全局游戏宽度-160,全局游戏高度-280)
		self.进入游戏:显示(全局游戏宽度-160,全局游戏高度-220)
		self.制作团队:显示(全局游戏宽度-160,全局游戏高度-160)
		self.退出游戏:显示(全局游戏宽度-160,全局游戏高度-100)
	end
end

return 场景类_标题