-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2023-08-12 22:43:37
-- 读取服务器分区
local 场景类_分区 = class()
local tp
local insert = table.insert

function 场景类_分区:初始化(根)
  local 资源 = 根.资源
  self.标题背景 = 资源:载入('MAX.7z',"网易WDF动画","服务器").精灵--899E213B
    self.右边分区1 = 资源:载入('JM.dll',"网易WDF动画",0x0E3A7EE6)
    self.右边分区2 = 资源:载入('JM.dll',"网易WDF动画",0xFB057B3D)--红色动态
    self.标题1 = 资源:载入('MAX.7z',"网易WDF动画","选择服务器").精灵
    self.大分区 = 资源:载入('JM.dll',"网易WDF动画",0xD330CE3F)
    self.大分区选中 = 资源:载入('JM.dll',"网易WDF动画",0xF5674AFF)
    self.小分区= 资源:载入('JM.dll',"网易WDF动画",0xBD57A592)
self.红线= 资源:载入('MAX.7z',"网易WDF动画","红线").精灵
  -- self.进入游戏 = 根._按钮(资源:载入('JM.dll',"网易WDF动画",0x0A247197),0,0,3,true,true)
  self.上一步 = 根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010009),0,0,3,true,true)
  self.下一步 =   根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010003),0,0,3,true,true)
  self.退出游戏 =根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010002),0,0,3,true,true)
  tp = 根
  local 按钮 = 根._按钮
  local 自适应 = 根._自适应
    self.临时计次O=0
    --================
    self.大分区按钮事件 = 0
    self.小分区按钮事件 = 0
  local file1=self:分区内容()
  local servertype = 分割文本(file1, "\n")
  self.大分区OL = {  }
  self.小分区OL = {  }
  self.大分区分类 = {  }
  self.小分区分类 = {  }
  self.小分区分类背景 = {  }
  for n = 1, #servertype do
    local typeA = 分割文本(servertype[n], " ")
   
    if #typeA == 4 then
        n = 按钮.创建(自适应.创建(51,4,65,22,1,3),0,0,4,true,true,typeA[4])
        insert(self.大分区OL,n)
        n={tonumber(typeA[1]),tonumber(typeA[2]),tonumber(typeA[3]),typeA[4]}
        insert(self.大分区分类,n)
    elseif #typeA == 10 then--梦幻的是18
        n = 按钮.创建(自适应.创建(52,4,65,22,1,3),0,0,4,true,true,typeA[4])
        insert(self.小分区OL,n)
        n={tonumber(typeA[1]),tonumber(typeA[2]),tonumber(typeA[3]),typeA[4],typeA[5],tonumber(typeA[6]),typeA[7],tonumber(typeA[8]),tonumber(typeA[9]),typeA[10]}
        insert(self.小分区分类,n)
        n=资源:载入('JM.dll',"网易WDF动画",0xBD57A592)
        insert(self.小分区分类背景,n)
    elseif #typeA == 18 then--梦幻官方
        n = 按钮.创建(自适应.创建(52,4,65,22,1,3),0,0,4,true,true,typeA[4])
        insert(self.小分区OL,n)
        n={tonumber(typeA[1]),tonumber(typeA[2]),tonumber(typeA[3]),typeA[4],typeA[5],tonumber(typeA[6]),typeA[7],tonumber(typeA[8]),tonumber(typeA[9]),typeA[10]}
        insert(self.小分区分类,n)
        n=资源:载入('JM.dll',"网易WDF动画",0xBD57A592)
        insert(self.小分区分类背景,n)
    end
  end
  self.服务器数据={}
  for n=1,#self.小分区分类 do
    self.服务器数据[n]={mc=self.小分区分类[n][4],状态=1,x=0,y=0,背景=0,ip="127.0.0.1",dk=8083}
  end
end

function 场景类_分区:显示(dt,x,y)
  if tp.进程 == 2 then
    引擎.置标题(全局游戏标题.."- 请选择分区")
    self.上一步:更新(x,y)
    self.下一步:更新(x,y)
    self.右边分区1:更新(x,y)
    for n = 1, #self.大分区OL do
      self.大分区OL[n]:更新(x,y)
    end
    for n = 1, #self.小分区OL do
      self.小分区OL[n]:更新(x,y)
    end
        self.临时计次O=self.临时计次O+1
        if self.临时计次O==6 then
            self.右边分区2:更新(x,y)
            self.临时计次O=0
        end
    self.退出游戏:更新(x,y)
    -- if self.进入游戏:事件判断() then
    --   引擎.置新标题(全局游戏标题)
    --   tp.进程 = 3
    -- else
      if self.上一步:事件判断() then
      tp.进程 = 1
    elseif self.下一步:事件判断() then
      窗口标题.服务器 = "烟雨濛濛"
      引擎.置标题(全局游戏标题 .. "- " .. 窗口标题.服务器 )
      tp.进程 = 3
      tp.登入.zhsr:置文本(读配置(程序目录..[[配置.ini]],"账号信息","账号"))
      tp.登入.mmsr:置文本(读配置(程序目录..[[配置.ini]],"账号信息","密码"))
      if 低配模式 then
        tp.提示:写入("#Y/你已经开启了低配模式,开启后降低内存使用,关闭各种特效,光环锦衣等")
      end
    elseif self.退出游戏:事件判断() then
      引擎.关闭()
      return false
    end
        local msgq宽度 = 全局游戏宽度/2-250--200
        local msgq高度 = 全局游戏高度/2-200--200
        self.右边分区1:显示(msgq宽度*2+325,msgq高度+50)
        self.右边分区2:显示(msgq宽度*2+340,msgq高度+230)

    self.标题背景:显示(msgq宽度-84,msgq高度+41)
    self.标题1:显示(msgq宽度+80,msgq高度-10)

    --self.进入游戏:显示(全局游戏宽度-160,全局游戏高度-320)
    self.上一步:显示(全局游戏宽度-190,全局游戏高度-190)
    self.下一步:显示(全局游戏宽度-190,全局游戏高度-130)
    self.退出游戏:显示(全局游戏宽度-190,全局游戏高度-70)
    for n = 1, #self.大分区OL do
      if self.大分区OL[n]:事件判断() then
           self.大分区按钮事件 = n
           全局大分区 = self.大分区分类[n][4]
            end
    end

    local msgq列 = 1
    for n = 1, #self.大分区OL do
      msgq行=服务区取行(n)
      if self.大分区按钮事件==n then
          self.大分区选中:显示(全局游戏宽度/2-388+msgq列*71,全局游戏高度/2-171+msgq行*28)
      end
      self.大分区OL[n]:显示(全局游戏宽度/2-388+msgq列*71,全局游戏高度/2-171+msgq行*28,nil,nil,nil,self.大分区按钮事件==n,2)
      if msgq列 ~=7 then
          msgq列 = msgq列 +1
      elseif msgq列 ==7  then
        msgq列 = 1
      end
    end
    local msgq行 = 1
    local msgq列 = 1
    for n = 1, #self.小分区OL do
            if self.大分区按钮事件 == math.ceil(self.小分区分类[n][1]) and self.大分区按钮事件 ~= 0 then
          self.小分区OL[n]:显示(全局游戏宽度/2-388+msgq列*71,全局游戏高度/2+117+msgq行*28,nil,122)
          self.红线:显示(全局游戏宽度/2-381+msgq列*71,全局游戏高度/2+137+msgq行*28,nil,122)

          if msgq列 ~=7 then
              msgq列 = msgq列 +1
          elseif msgq列 ==7  then
            msgq列 = 1
            msgq行=msgq行+1
          end
            end
    end
    for n = 1, #self.小分区OL do
      if self.小分区OL[n]:事件判断() then
           self.小分区按钮事件 = n
           if self.大分区按钮事件 == math.ceil(self.小分区分类[n][1]) and self.小分区分类[n][4]~="" then
                self.服务器编号=math.ceil(self.小分区分类[n][1])
                self.服务器数据[self.服务器编号].名称=self.小分区分类[n][4]
              窗口标题.服务器 = self.服务器数据[self.服务器编号].mc
              引擎.置标题(全局游戏标题 .. "- " .. 窗口标题.服务器 )
              tp.进程 = 3
              tp.登入.zhsr:置文本(读配置(程序目录..[[配置.ini]],"账号信息","账号"))
              tp.登入.mmsr:置文本(读配置(程序目录..[[配置.ini]],"账号信息","密码"))
              if 低配模式 then
                tp.提示:写入("#Y/你已经开启了低配模式,开启后降低内存使用,关闭各种特效,光环锦衣等")
              end
           end
        end
    end
  end
end

function 服务区取行(n)
  local msgq行 = 1
  if n<=7  then
    msgq行=1
  elseif n<=14 then
    msgq行=2
  elseif n<=21 then
    msgq行=3
  elseif n<=28 then
    msgq行=4
  elseif n<=35  then
    msgq行=5
  elseif n<=42 then
    msgq行=6
  elseif n<=49  then
    msgq行=7
  elseif n<=56  then
    msgq行=8
  elseif n<=63 then
    msgq行=9
  elseif n<=70  then
    msgq行=10
  elseif n<=77  then
    msgq行=11
  elseif n<=84  then
    msgq行=12
  elseif n<=91  then
    msgq行=13
  elseif n<=98  then
    msgq行=14
  end
  return msgq行
end

function 场景类_分区:分区内容()
local x返回 =[[-2
-1 7 9 新区推荐
-2 7 7 经典老区
1 1 1 烟雨濛濛 106.2.55.218 11566 4 106.2.55.218 11566 4 106.2.55.218 11566 4 2 欢迎进入梦幻西游 1156 0 201204130000
2 1 1 青城山 59.111.14.15 1088 4 59.111.14.15 1088 4 59.111.14.15 1088 4 5 欢迎进入梦幻西游 10 0 200312182200
2 1 1 紫蓬山 42.186.0.225 2988 4 42.186.0.225 2988 4 42.186.0.225 2988 4 5 欢迎进入梦幻西游 29 0 200504261000
2 1 1 黄鹤楼 42.186.0.225 2988 4 42.186.0.225 2988 4 42.186.0.225 2988 4 5 欢迎进入梦幻西游 29 0 200504261000
2 1 1 少林寺 42.186.0.225 2988 4 42.186.0.225 2988 4 42.186.0.225 2988 4 5 欢迎进入梦幻西游 29 0 200504261000
2 1 1 百花村 42.186.0.225 2988 4 42.186.0.225 2988 4 42.186.0.225 2988 4 5 欢迎进入梦幻西游 29 0 200504261000
2 1 1 镇淮楼 42.186.0.225 2988 4 42.186.0.225 2988 4 42.186.0.225 2988 4 5 欢迎进入梦幻西游 29 0 200504261000
2 1 1 雄鹰岭 42.186.0.225 2988 4 42.186.0.225 2988 4 42.186.0.225 2988 4 5 欢迎进入梦幻西游 29 0 200504261000
2 1 1 夫子庙 42.186.0.225 2988 4 42.186.0.225 2988 4 42.186.0.225 2988 4 5 欢迎进入梦幻西游 29 0 200504261000
2 1 1 普陀山 42.186.0.225 2988 4 42.186.0.225 2988 4 42.186.0.225 2988 4 5 欢迎进入梦幻西游 29 0 200504261000
2 1 1 少林寺 42.186.0.225 2988 4 42.186.0.225 2988 4 42.186.0.225 2988 4 5 欢迎进入梦幻西游 29 0 200504261000
2 1 1 百花村 42.186.0.225 2988 4 42.186.0.225 2988 4 42.186.0.225 2988 4 5 欢迎进入梦幻西游 29 0 200504261000
2 1 1 镇淮楼 42.186.0.225 2988 4 42.186.0.225 2988 4 42.186.0.225 2988 4 5 欢迎进入梦幻西游 29 0 200504261000
2 1 1 雄鹰岭 42.186.0.225 2988 4 42.186.0.225 2988 4 42.186.0.225 2988 4 5 欢迎进入梦幻西游 29 0 200504261000
2 1 1 国子监 42.186.0.225 2988 4 42.186.0.225 2988 4 42.186.0.225 2988 4 5 欢迎进入梦幻西游 29 0 200504261000]]
  return x返回
end

return 场景类_分区