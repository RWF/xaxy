-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-09-10 23:23:58
-- 登录框
 local 登入 = class()
 local 临时 =false
 local zt
 local tp
function 登入:初始化(根)
  tp=根
  local 按钮 = require("script/System/按钮")
  local 资源 = 根.资源
  zt=根.字体表.描边字体

	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('创建控件')
	总控件:置可视(true,true)
	self.zhsr = 总控件:创建输入("账号输入",320+(全局游戏宽度-800)/2,240,140,16,根)
	self.zhsr:置可视(false,false)

	self.zhsr:置限制字数(10)
	self.mmsr = 总控件:创建输入("密码输入",320+(全局游戏宽度-800)/2,280,140,16,根)
	self.mmsr:置可视(false,false)
	self.mmsr:置限制字数(10)
	self.mmsr:置密码模式()



  -- self.zhsr:置文本(读配置(程序目录..[[配置.ini]],"账号信息","账号"))
  -- self.mmsr:置文本(读配置(程序目录..[[配置.ini]],"账号信息","密码"))



  self.上一步 = 根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010011),0,0,3,true,true)
  self.下一步 = 根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010010),0,0,3,true,true)


  self.选择账号 = 根._按钮.创建(资源:载入('MAX.7z',"网易WDF动画","198B2D2A6"),0,0,4,true,true)
  self.选择账号栏 = 根._小型选项栏.创建(根._自适应.创建(6,1,180,115,3,9),"选择账号")


	self.账号密码= 资源:载入('MAX.7z',"网易WDF动画","登录界面1")---CB788484 51F941C4
  -- self.账号密码= 资源:载入('JM.dll',"网易WDF动画",0xCB788484)---CB788484 51F941C4
  self.内部测试 =根._按钮(资源:载入('MAX.7z',"网易WDF动画",01000051),0,0,1,true,true)
  self.普通显示=false
  self.普通显示1 = 读配置(程序目录..[[配置.ini]],"账号信息","状态")
  if self.普通显示1 =="true" then
     self.普通显示=true
  end
  self.低配模式=false
  self.低配模式1 = 读配置(程序目录..[[配置.ini]],"账号信息","模式")
  if self.低配模式1 =="true" then
     self.低配模式=true
        地图特效 = false
        低配模式 =true
  end
  self.普通 = 根._按钮.创建(资源:载入('MAX.7z',"网易WDF动画","记住账号"),0,0,1,true,true)
  self.普通:置根(根)
  self.普通:置偏移(-3,2)
  self.低配 = 根._按钮.创建(资源:载入('MAX.7z',"网易WDF动画","记住账号"),0,0,1,true,true)
  self.对号 = tp.资源:载入('MAX.7z',"网易WDF动画","对号").精灵
  self.低配:置根(根)
  self.低配:置偏移(-3,2)
end
function 登入:账号检查()
   if self.zhsr:取文本()=="" or self.mmsr:取文本()=="" then
      return false
    else
     游戏账号=self.zhsr:取文本()
     游戏密码=self.mmsr:取文本()
      return true
    end
   end
function 登入:显示(dt,x,y)

   self.账号密码:显示(205+(全局游戏宽度-800)/2,150)

    self.低配:显示(280+(全局游戏宽度-800)/2,323)
    self.普通:显示(425+(全局游戏宽度-800)/2,323)
     if    self.普通显示 then
       self.对号:显示(429+(全局游戏宽度-800)/2,331)
    end
    if  self.低配模式  then
      self.对号:显示(284+(全局游戏宽度-800)/2,331)
    end



self.普通:更新(x,y)
self.低配:更新(x,y)

    self.内部测试:更新(x,y)
   
    self.内部测试:显示(全局游戏宽度/2-200,0)
    self.上一步:显示(238+(全局游戏宽度-800)/2,401)
    self.下一步:显示(409+(全局游戏宽度-800)/2,401)
    self.上一步:更新(x,y)
    self.下一步:更新(x,y)
    self.控件类:显示(x,y)
    self.控件类:更新(dt,x,y)
  self.选择账号:更新(x,y)
  self.选择账号:显示(470+(全局游戏宽度-800)/2,236)
  self.选择账号栏:显示(313+(全局游戏宽度-800)/2,263,x,y,true)
    self.zhsr:置可视(true,true)
    self.mmsr:置可视(true,true)
   if self.内部测试:是否选中(x,y)then
      self.内部测试:置高亮()
    else
      self.内部测试:取消高亮()
    end

      if self.zhsr._输入焦点==true then
        if 引擎.按键弹起(KEY.TAB) then
            self.mmsr._输入焦点=true
            self.zhsr._输入焦点=false
            self.zhsr._光标可视=false
        end
      elseif self.mmsr._输入焦点==true then
        if 引擎.按键弹起(KEY.TAB) then
          self.zhsr._输入焦点=true
          self.mmsr._输入焦点=false
          self.mmsr._光标可视=false
        end
      end
		  if self.上一步:事件判断() then
			tp.进程= 2
			tp.选中窗口 = nil
     elseif self.选择账号:事件判断() then
        


       local file1=ReadFile("账号记录.txt")
             self.tb={}
               local servertype = 分割文本(file1, "\n")

               if servertype then
                for i=1,#servertype-1 do
                    local tb = 分割文本(servertype[i], "*-*")
                    table.insert(self.tb,tb[1])
                end
               end
       
        table.insert(self.tb,"保存记录")
        table.insert(self.tb,"清空记录")
         self.选择账号栏:打开(self.tb)
          self.选择账号栏:置选中(self.zhsr:取文本())
    elseif self.选择账号栏:事件判断() then
             if   self.选择账号栏.弹出事件 =="清空记录"  then 

                        WriteFile("账号记录.txt","")
             elseif  self.选择账号栏.弹出事件 =="保存记录"  then
                           local  账号记录=ReadFile("账号记录.txt")
                            账号记录 =账号记录..self.zhsr:取文本().."*-*"..self.mmsr:取文本().."\n"
                            WriteFile("账号记录.txt",账号记录)
             else

                
                  local file1=ReadFile("账号记录.txt")
                   local servertype = 分割文本(file1, "\n")
                   if servertype then
                      for i=1,#servertype-1 do
                          local tb = 分割文本(servertype[i], "*-*")
                          if tb[1]==self.选择账号栏.弹出事件 then
                                self.zhsr:置文本(tb[1])
                                self.mmsr:置文本(tb[2])
                              break 
                          end
                      end
                   end

                  
             end  
            self.选择账号栏:置选中(self.选择账号栏.弹出事件)
            self.选择账号栏.弹出事件 = nil

      elseif self.普通:事件判断() then
        if self.普通显示 then
          self.普通显示=false
        
          写配置(程序目录..[[配置.ini]],"账号信息","状态","false")
        else
          
          self.普通显示=true
          写配置(程序目录..[[配置.ini]],"账号信息","状态","true")
        end
      elseif self.低配:事件判断() then
        if self.低配模式 then
          self.低配模式=false
            地图特效 = true
            低配模式 =false
             
          写配置(程序目录..[[配置.ini]],"账号信息","模式","false")
          tp.提示:写入("#Y/你已经开启了高配模式,所有特效开启,体验最优质的画面")
        else
          self.低配模式=true
        
          写配置(程序目录..[[配置.ini]],"账号信息","模式","true")
            地图特效 = false
            低配模式 =true
            tp.提示:写入("#Y/你已经开启了低配模式,开启后降低内存使用,关闭各种特效,光环锦衣等")
        end
		   elseif self.下一步:事件判断() then
          if self.普通显示 then
            写配置(程序目录..[[配置.ini]],"账号信息","账号",self.zhsr:取文本())
            写配置(程序目录..[[配置.ini]],"账号信息","密码",self.mmsr:取文本())
          end
          if self.zhsr:取文本()=="" or self.mmsr:取文本()=="" then
            tp.提示:写入("#Y/请输入完整的账号密码")
          else
              if  self:账号检查() then
                    local fun  = require("ffi函数")
                    local 发送信息 = {
                    账号=self.zhsr:取文本(),
                    密码=self.mmsr:取文本(),
                    }
                    
                    客户端:发送(1, cjson.encode(发送信息))
              end
	     	 end
  end
end

return 登入