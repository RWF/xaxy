-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-23 02:23:52
-- 新建角色
local 场景类_创建 = class()

local bwh = require("gge包围盒")
local bw = bwh(0,0,56*scale_w,46*scale_h)
local bw1 = bwh(0,0,56*scale_w,46*scale_h)
local tp
local mouseb = 引擎.鼠标按下


function 场景类_创建:初始化(根)

  tp = 根
  self.人物列表 = {
  [1]= {
  {模型="飞燕女",x=212,y=35,染色方案=3},
   {模型="英女侠",x=308,y=35,染色方案=4},
   {模型="巫蛮儿",x=404,y=35,染色方案=2083},
   {模型="偃无师",x=500,y=35,染色方案=18},
   {模型="逍遥生",x=594,y=35,染色方案=1},
   {模型="剑侠客",x=689,y=35,染色方案=2}},
   [2]={
   {模型="狐美人",x=212,y=35,染色方案=7},
   {模型="骨精灵",x=308,y=35,染色方案=8},
   {模型="鬼潇潇",x=404,y=35,染色方案=18},
   {模型="杀破狼",x=500,y=35,染色方案=18},
   {模型="巨魔王",x=594,y=35,染色方案=5},
   {模型="虎头怪",x=689,y=35,染色方案=6}
   },
   [3]={
   {模型="舞天姬",x=212,y=35,染色方案=11},
    {模型="玄彩娥",x=308,y=35,染色方案=12},
   {模型="桃夭夭",x=404,y=35,染色方案=18},
   {模型="羽灵神",x=500,y=35,染色方案=2081},
   {模型="神天兵",x=594,y=35,染色方案=9},
  {模型="龙太子",x=689,y=35,染色方案=10}}}




  self.选中人物 = 0
  self.选择人物 = false
  self.dz = "静立"
  self.fx = 4
  self.染色ID = 0
  --self.jswb = 根._丰富文本(250,81)
  local 资源 = 根.资源
  --self.jswb:置行度(-1)




  self.人物随机名字 = 根._按钮.创建(资源:载入("MAX.7z","网易WDF动画",201),0,0,3,true,true)

    self.状态=1
  -- self.xmsrk = 资源:载入('JM.dll',"网易WDF动画",0x1BA268A5)
  -- self.染色切换 = 资源:载入('JM.dll',"网易WDF动画",0x5C939428)
  self.ztx = {[1]={},[2]={},[3]={}}
  self.dtx = {[1]={},[2]={},[3]={}}
  self.dh ={[1]={},[2]={},[3]={}}
 self.文字 ={[1]={},[2]={},[3]={}}
  for n=1,3 do
    for c=1,6 do

    local s =  ModelData[self.人物列表[n][c].模型]
    local q = 取模型(self.人物列表[n][c].模型)


    --self.文字[n][c]=资源:载入(self.人物列表[n][c].模型..".png","加密图片")
    self.文字[n][c]=资源:载入(self.人物列表[n][c].模型..".png","加密图片")

  
    self.ztx[n][c] = 资源:载入(s.头像资源,"网易WDF动画",s.头像组.创建)
    self.dtx[n][c] = 资源:载入("MAX.7z","网易WDF动画",s.头像组.人物)
    self.dh[n][c] = {}
    self.dh[n][c]["静立"] = 资源:载入(q.资源,"网易WDF动画",q.静立)
    self.dh[n][c]["行走"] = 资源:载入(q.资源,"网易WDF动画",q.行走)
    self.dh[n][c]["攻击"] = 资源:载入(q.战斗资源,"网易WDF动画",q.攻击)
    self.dh[n][c]["施法"] = 资源:载入(q.战斗资源,"网易WDF动画",q.施法)

    self.人物列表[n][c].x = self.人物列表[n][c].x
    self.人物列表[n][c].y = self.人物列表[n][c].y
  end
end
  local 按钮 = tp._按钮
  self.影子 = 资源:载入('JM.dll',"网易WDF动画",0xDCE4B562)
  self.选择框 = 资源:载入('JM.dll',"网易WDF动画",0x37DD63ED)
  self.背景={
   [1]=资源:载入("背景1.png","加密图片"),
   [2]=资源:载入("背景2.png","加密图片"),
   [3]=资源:载入("背景3.png","加密图片"),

 }
  self.一号染色 =   按钮.创建(资源:载入('MAX.7z',"网易WDF动画",200),0,0,3,true,true," 1")
    self.一号染色:置文字颜色(0xFF703B3B)
    self.一号染色:置偏移(7,5)

  self.二号染色 =  按钮.创建(资源:载入('MAX.7z',"网易WDF动画",200),0,0,3,true,true," 2")
      self.二号染色:置文字颜色(0xFF703B3B)
    self.二号染色:置偏移(7,5)
  self.三号染色 =  按钮.创建(资源:载入('MAX.7z',"网易WDF动画",200),0,0,3,true,true," 3")
      self.三号染色:置文字颜色(0xFF703B3B)
    self.三号染色:置偏移(7,5)
  self.人物站立 =   按钮.创建(资源:载入('MAX.7z',"网易WDF动画",200),0,0,3,true,true,"站立")
      self.人物站立:置文字颜色(0xFF703B3B)
    self.人物站立:置偏移(7,5)
  self.人物奔跑 =   按钮.创建(资源:载入('MAX.7z',"网易WDF动画",200),0,0,3,true,true,"奔跑")
     self.人物奔跑:置文字颜色(0xFF703B3B)
   self.人物奔跑:置偏移(7,5)
  self.人物攻击 =  按钮.创建(资源:载入('MAX.7z',"网易WDF动画",200),0,0,3,true,true,"攻击")
      self.人物攻击:置文字颜色(0xFF703B3B)
    self.人物攻击:置偏移(7,5)
  self.人物施法 =   按钮.创建(资源:载入('MAX.7z',"网易WDF动画",200),0,0,3,true,true,"施法")
    self.人物施法:置文字颜色(0xFF703B3B)
    self.人物施法:置偏移(7,5)






  self.后退一个方向 =  按钮.创建(资源:载入('MAX.7z',"网易WDF动画",202),0,0,3,true,true)
  self.前进一个方向 =  按钮.创建(资源:载入('MAX.7z',"网易WDF动画",203),0,0,3,true,true)
  -- 公用
  self.上一步 =   根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010009),0,0,3,true,true)
  self.下一步 =  根._按钮(资源:载入('MAX.7z',"网易WDF动画",00010003),0,0,3,true,true)
  -- 控件
  self.控件类 = require("ggeui/加载类")()
  local 总控件 = self.控件类:创建控件('创建控件')
  总控件:置可视(true,true)
  self.mc = 总控件:创建输入("创建输入",250*scale_w,560*scale_h,110,14,根)
  self.mc:置可视(false,false)
  self.mc:置限制字数(10)
end

function 场景类_创建:置方向(方向,n)
  self.dh[self.状态][n]["静立"]:置方向(方向)
  self.dh[self.状态][n]["行走"]:置方向(方向)
  self.dh[self.状态][n]["攻击"]:置方向(取四至八方向(方向))
  self.dh[self.状态][n]["施法"]:置方向(取四至八方向(方向))
end

function 场景类_创建:置染色(id,染色方案,染色ID,方向)
  self.dh[self.状态][id]["静立"]:置染色(染色方案,染色ID,染色ID,染色ID)
  self.dh[self.状态][id]["行走"]:置染色(染色方案,染色ID,染色ID,染色ID)
  self.dh[self.状态][id]["攻击"]:置染色(染色方案,染色ID,染色ID,染色ID)
  self.dh[self.状态][id]["施法"]:置染色(染色方案,染色ID,染色ID,染色ID)
  self:置方向(方向,id)
end

function 场景类_创建:后退方向()
  if self.fx ~= 7 then
    self.fx = self.fx + 1
    self:置方向(self.fx,self.选中人物)
  end
end

function 场景类_创建:前进方向()
  if self.fx ~= 0 then
    self.fx = self.fx - 1
    self:置方向(self.fx,self.选中人物)
  end
end



function 场景类_创建:显示(dt,x,y)
  if self.选中人物 ~= 0 then
    self.dh[self.状态][self.选中人物][self.dz]:更新(dt)
  end

   self.背景[self.状态]:置拉伸(0,0,全局游戏宽度,全局游戏高度);
   self.背景[self.状态]:显示(0,0)
    bw:置坐标(15*scale_w,3*scale_h)
    bw1:置坐标(10*scale_w,75*scale_h)
  if self.状态==1 then--人
    if bw:检查点(x,y) and 引擎.鼠标按下(0) then
      self.状态=2
    elseif bw1:检查点(x,y) and 引擎.鼠标按下(0) then
       self.状态=3
    end
  elseif self.状态==2 then
        if bw:检查点(x,y) and 引擎.鼠标按下(0) then
      self.状态=3
    elseif bw1:检查点(x,y) and 引擎.鼠标按下(0) then
       self.状态=1
    end
  elseif self.状态==3 then
        if bw:检查点(x,y) and 引擎.鼠标按下(0) then
      self.状态=1
    elseif bw1:检查点(x,y) and 引擎.鼠标按下(0) then
       self.状态=2
    end
  end
          -- 引擎.画矩形(15,3,76,56,-3551379)
          -- 引擎.画矩形(10,75,66,131,-3551379)

  self.后退一个方向:更新(x,y)
  self.前进一个方向:更新(x,y)
  self.人物随机名字:更新(x,y)
  self.一号染色:更新(x,y)
  self.二号染色:更新(x,y)
  self.三号染色:更新(x,y)
  self.人物站立:更新(x,y)
  self.人物奔跑:更新(x,y)
  self.人物攻击:更新(x,y)
  self.人物施法:更新(x,y)
  self.上一步:更新(x,y)
  self.下一步:更新(x,y)
  if self.选中人物 ~= 0 then
    self.控件类:更新(dt,x,y)
    if self.后退一个方向:事件判断() then
      self:前进方向()
    elseif self.前进一个方向:事件判断() then

      self:后退方向()
    elseif self.人物站立:事件判断() then
      self.dz = "静立"
    elseif self.人物奔跑:事件判断() then
      self.dz = "行走"
    elseif self.人物攻击:事件判断() then
      self.dz = "攻击"
    elseif self.人物施法:事件判断() then
      self.dz = "施法"
    elseif self.一号染色:事件判断() then
      if DyeData[self.人物列表[self.状态][self.选中人物].模型][1] ~= nil then  
        if self.染色ID ~= 0 then
          self.染色ID = 0
          self:置方向(0,self.选中人物)
          self:置染色(self.选中人物,DyeData[self.人物列表[self.状态][self.选中人物].模型][1],self.染色ID,self.fx)
        end
      else
        tp.提示:写入("#Y/该角色暂未开放染色")
      end
    elseif self.二号染色:事件判断() then
      if DyeData[self.人物列表[self.状态][self.选中人物].模型][1] ~= nil  then
        if self.染色ID ~= 1 then
          self.染色ID = 1
          self:置方向(0,self.选中人物)
          self:置染色(self.选中人物,DyeData[self.人物列表[self.状态][self.选中人物].模型][1],self.染色ID,self.fx)
        end
      else
        tp.提示:写入("#Y/该角色暂未开放染色")
      end
    elseif self.三号染色:事件判断() then
      if DyeData[self.人物列表[self.状态][self.选中人物].模型][1] ~= nil then
        if self.染色ID ~= 2 then
          self.染色ID = 2
          self:置方向(0,self.选中人物)
          self:置染色(self.选中人物,DyeData[self.人物列表[self.状态][self.选中人物].模型][1],self.染色ID,self.fx)
        end
      else
        tp.提示:写入("#Y/该角色暂未开放染色")
      end

    elseif self.人物随机名字:事件判断()   then
      local x = 取随机数(1,2);
      local sex = getSexByName(self.人物列表[self.状态][self.选中人物].模型)
      local 组合 = nil;
      if x == 1 and sex == 1 then
        组合 = familyOne[取随机数(1,#familyOne)]..boyName[取随机数(1,#boyName)]
      elseif  x == 1 and sex == 2 then
        组合 = familyOne[取随机数(1,#familyOne)]..girlName[取随机数(1,#girlName)]
      elseif  x == 2 and sex == 1 then
        组合 = familyTwo[取随机数(1,#familyTwo)]..boyName[取随机数(1,#boyName)]
      elseif  x == 2 and sex == 2 then
         组合 = familyTwo[取随机数(1,#familyTwo)]..girlName[取随机数(1,#girlName)]
      end

      self.mc:置文本(组合)
    end
  end

  for n=1,6 do
    -- self.ztx[self.状态][n]:置拉伸(0,0,self.人物列表[self.状态][n].x*scale_w,self.人物列表[self.状态][n].y*scale_h)
    self.ztx[self.状态][n]:显示(self.人物列表[self.状态][n].x*scale_w,self.人物列表[self.状态][n].y*scale_h)
    if self.ztx[self.状态][n]:是否选中(x,y) then
      self.选择框:更新(dt)
      self.选择框:显示(self.人物列表[self.状态][n].x*scale_w,self.人物列表[self.状态][n].y*scale_h)
      if mouseb(0) and self.选中人物 ~= n then
        self.选中人物 = n
        self.dz = "静立"
        self.fx = 4
        self.染色ID = 0

        if DyeData[self.人物列表[self.状态][self.选中人物].模型][1] ~= nil  then
          self:置染色(self.选中人物,DyeData[self.人物列表[self.状态][self.选中人物].模型][1],self.染色ID,self.fx)
        end
        self:置方向(self.fx,self.选中人物)
      end
    end
  end
  -- self.xmsrk:显示(28*scale_w,373)
  self.后退一个方向:显示(575*scale_w,300*scale_h)
  self.前进一个方向:显示(730*scale_w,300*scale_h)
  --self.染色切换:显示(34*scale_w,493)
  self.一号染色:显示(580*scale_w,165*scale_h,true,1)
  self.二号染色:显示(640*scale_w,165*scale_h,true,1)
  self.三号染色:显示(700*scale_w,165*scale_h,true,1)

  self.人物站立:显示(550*scale_w,135*scale_h,true,1)
  self.人物奔跑:显示(610*scale_w,135*scale_h,true,1)
  self.人物攻击:显示(670*scale_w,135*scale_h,true,1)
  if self.选中人物 ~= 0 then
    self.人物随机名字:显示(395*scale_w,545*scale_h)
  end
  self.人物施法:显示(730*scale_w,135*scale_h,true,1)
  if self.选中人物 ~= 0 then
    self.控件类:显示(x,y)
    self.mc:置可视(true,true)
    self.影子:显示(108*scale_w,493*scale_h)
    self.dh[self.状态][self.选中人物][self.dz]:显示(673*scale_w,301*scale_h)
  else
    self.mc:置可视(false,false)
  end
  self.上一步:显示(527*scale_w,540*scale_h)
  self.下一步:显示(667*scale_w,540*scale_h)
  if self.选中人物 ~= 0 then
    self.dtx[self.状态][self.选中人物]:显示(150*scale_w,116*scale_h)
    self.文字[self.状态][self.选中人物]:显示(0,0)
  end

  if self.上一步:事件判断() then
    self.选中人物 = 0
    self.dz = "静立"
    self.fx = 4
    self.染色ID = 0
    self.状态=1
    self.mc:置可视(false,false)
    tp.进程 = 3
  elseif self.下一步:事件判断() then
    if self.选中人物 == 0 then
      tp.提示:写入("#Y/请选择一个人物进入游戏")
    else
         local fun  = require("ffi函数")
        self.发送信息 = {
        账号=游戏账号,
        名称=self.mc:取文本(),
        密码=游戏密码,
        编号=self.状态,
        序号=self.选中人物,
        }
        客户端:发送(2,cjson.encode(self.发送信息))
        xjzh =true
    end
  end
end

return 场景类_创建