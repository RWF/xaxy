local 场景类_人物 = class()

local   zqj = 引擎.坐骑库
local tp
function 场景类_人物:初始化(数据,根)
     tp = 根
	 self.循环更新 = false
     self.动作行为 = false
    self.武器不显示=false
	self.人物 = {}
    self.延时 = 0
	self.行为 = "静立"
	self.方向 = 0
    self:置模型(数据)


end
function 场景类_人物:置方向(d)
	self.人物["行走"]:置方向(d)
	if 	self.动作行为 then
		self.人物["静立"]:置方向(取四至八方向(d))
	else
		self.人物["静立"]:置方向(d)
	end
	if self.人物["武器_静立"] ~= nil and self.人物["武器_行走"] ~= nil then
		self.人物["武器_行走"]:置方向(d)
		if 	self.动作行为 then
			self.人物["武器_静立"]:置方向(取四至八方向(d))
		else
			self.人物["武器_静立"]:置方向(d)
		end
	elseif self.人物["坐骑_静立"] ~= nil then
		self.人物["坐骑_静立"]:置方向(d)
		self.人物["坐骑_行走"]:置方向(d)
		if self.人物["坐骑饰品_静立"] ~= nil then
			self.人物["坐骑饰品_静立"]:置方向(d)
			self.人物["坐骑饰品_行走"]:置方向(d)
		end

	end
	if self.人物["翅膀_静立"] ~= nil then
		self.人物["翅膀_静立"]:置方向(d)
		self.人物["翅膀_行走"]:置方向(d)
	end
	self.方向 = d
end

function 场景类_人物:置染色(染色方案,a,b,c,d)
	if 染色方案 ~= nil then
		self.人物["静立"]:置染色(染色方案,a,b,c,0)
		self.人物["行走"]:置染色(染色方案,a,b,c,0)
		self:置方向(self.方向)


	end
end
function 场景类_人物:置模型(数据)--变身.武器数据.染色方案.染色.造型.翅膀.坐骑
	self.数据=数据
	local mx = 数据.造型
	if 数据.锦衣数据 and 低配模式==false and (数据.锦衣数据.锦衣 or 数据.锦衣数据.定制 or 数据.锦衣数据.战斗锦衣) then
		if 数据.锦衣数据.定制 then
			mx =数据.锦衣数据.定制
		elseif 数据.锦衣数据.战斗锦衣 then
			mx =数据.锦衣数据.战斗锦衣
		else
		mx =数据.锦衣数据.锦衣.."_"..数据.造型
		end
	end
	if 数据.变身~= nil then
		mx = 数据.变身
	end
	local 资源 = 取模型(mx)
	self.人物 = {
	["静立"] = tp.资源:载入(资源.资源,"网易WDF动画",资源.静立),
	["行走"] = tp.资源:载入(资源.资源,"网易WDF动画",资源.行走),}

	if 数据.变身  then
			if 数据.召唤兽饰品  and 取召唤兽饰品(mx) then
				 local n = 取模型("饰品_"..mx)
				self.人物["武器_静立"] = tp.资源:载入(n.资源,"网易WDF动画",n.静立)
				self.人物["武器_行走"] = tp.资源:载入(n.资源,"网易WDF动画",n.行走)	
			elseif 取召唤兽武器(mx) then
				 local n = 取模型("武器_"..mx)
				self.人物["武器_静立"] = tp.资源:载入(n.资源,"网易WDF动画",n.静立)
				self.人物["武器_行走"] = tp.资源:载入(n.资源,"网易WDF动画",n.行走)
			end

		if  数据.染色组 then
	    	self:置染色(数据.染色方案,数据.染色组[1],数据.染色组[2],数据.染色组[3])
	    	if  self.人物["武器_静立"]   then
				self.人物["武器_静立"]:置染色(数据.染色方案,数据.染色组[1],数据.染色组[2],数据.染色组[3])
				self.人物["武器_行走"]:置染色(数据.染色方案,数据.染色组[1],数据.染色组[2],数据.染色组[3])
			end
	    end

	elseif  数据.武器数据 and 数据.武器数据.类别 ~= 0 and 数据.武器数据.类别 ~= "" and 数据.武器数据.类别 ~= nil and  数据.锦衣数据.定制 ==nil then

		self:穿戴装备(数据)
    
        --do
	else
		if   数据.变身==nil and  数据.锦衣数据.锦衣 ==nil and  数据.锦衣数据.战斗锦衣 ==nil and  数据.锦衣数据.定制 ==nil   then

			self:置染色(DyeData[数据.造型][1],数据.染色.a,数据.染色.b,数据.染色.c) 

		end
		self:置方向(self.方向)
	end
	if 数据.坐骑 and 数据.坐骑.类型 ~= nil and not 数据.锦衣数据.定制 then
		self:坐骑改变(数据)
	end
	if 数据.锦衣数据 and 数据.锦衣数据.光环  and 低配模式 ==false then
		         local ns=引擎.取光环(数据.锦衣数据.光环)
			    self.人物["光环_静立"] = tp.资源:载入(ns[4],"网易WDF动画",ns[1])
		     self.人物["光环_行走"] = tp.资源:载入(ns[4],"网易WDF动画",ns[2])
	end
	if 数据.翅膀 ~= nil and 数据.翅膀.名称 ~= nil then
		self:穿戴翅膀(数据)
	end

end

function 场景类_人物:坐骑改变(数据) --变身.造型.坐骑.染色方案.染色
	if 数据.变身 ~= nil then
		return false
	end
	self:卸下翅膀()
	local 资源组 = zqj(数据.造型,数据.坐骑.类型,数据.坐骑.饰品 or "空")
	self.人物["静立"] = tp.资源:载入(资源组.人物资源,"网易WDF动画",资源组.人物站立)
	self.人物["行走"] = tp.资源:载入(资源组.人物资源,"网易WDF动画",资源组.人物行走)
	self.人物["坐骑_静立"] = tp.资源:载入(资源组.坐骑资源,"网易WDF动画",资源组.坐骑站立)
	self.人物["坐骑_行走"] = tp.资源:载入(资源组.坐骑资源,"网易WDF动画",资源组.坐骑行走)
	if 数据.坐骑.染色组 then
		self.人物["坐骑_静立"]:置染色(数据.坐骑.类型,数据.坐骑.染色组[1],数据.坐骑.染色组[2],数据.坐骑.染色组[3])
		self.人物["坐骑_行走"]:置染色(数据.坐骑.类型,数据.坐骑.染色组[1],数据.坐骑.染色组[2],数据.坐骑.染色组[3])
    end
	if 资源组.坐骑饰品站立 ~= nil then
		self.人物["坐骑饰品_静立"] = tp.资源:载入(资源组.坐骑饰品资源,"网易WDF动画",资源组.坐骑饰品站立)
		self.人物["坐骑饰品_行走"] = tp.资源:载入(资源组.坐骑饰品资源,"网易WDF动画",资源组.坐骑饰品行走)
		if 数据.坐骑.饰品染色组 then
		    self.人物["坐骑饰品_静立"]:置染色(数据.坐骑.饰品,数据.坐骑.饰品染色组[1],数据.坐骑.饰品染色组[2],数据.坐骑.饰品染色组[3])
			self.人物["坐骑饰品_行走"]:置染色(数据.坐骑.饰品,数据.坐骑.饰品染色组[1],数据.坐骑.饰品染色组[2],数据.坐骑.饰品染色组[3])
		end
	end
	self.人物["武器_静立"] = nil
	self.人物["武器_行走"] = nil
		self.人物["武器_攻击"] = nil
	self.人物["武器_施法"] = nil
	self.人物["武器_跑步"] = nil
	if 数据.锦衣数据.锦衣 ==nil  then
		self:置染色(DyeData[数据.造型][1],数据.染色.a,数据.染色.b,数据.染色.c) 
	end
	self:置方向(self.方向)

end

function 场景类_人物:卸下坐骑(数据)  --变身.武器数据.染色方案.染色.造型.翅膀.坐骑
	self.人物["坐骑_静立"] = nil
	self.人物["坐骑_行走"] = nil
	self.人物["坐骑饰品_静立"] = nil
	self.人物["坐骑饰品_行走"] = nil
	self:置模型(数据)
	if 数据.武器数据.类别 ~= 0 then
		self:穿戴装备(数据)
	end
end

function 场景类_人物:穿戴装备(数据)--变身.武器数据.染色方案.染色
	if 数据.变身~=nil then
		return  0
	end
	local v = 数据.武器数据.类别
	if 数据.武器数据.名称 == "龙鸣寒水" or 数据.武器数据.名称 == "非攻" then
		v = "弓弩1"
	end
    if 数据.锦衣数据.定制 and 低配模式 ==false then
		local 资源 = 取模型(数据.锦衣数据.定制)
		self.人物["静立"] = tp.资源:载入(资源.资源,"网易WDF动画",资源.静立)
		self.人物["行走"] = tp.资源:载入(资源.资源,"网易WDF动画",资源.行走)
	 elseif 数据.锦衣数据.战斗锦衣 and 低配模式 ==false then
		local 资源 = 取模型(数据.锦衣数据.战斗锦衣)
		self.人物["静立"] = tp.资源:载入(资源.资源,"网易WDF动画",资源.静立)
		self.人物["行走"] = tp.资源:载入(资源.资源,"网易WDF动画",资源.行走)
	elseif 数据.锦衣数据.锦衣 and 低配模式 ==false then
		local 资源 = 取模型(数据.锦衣数据.锦衣.."_"..数据.造型)
		self.人物["静立"] = tp.资源:载入(资源.资源,"网易WDF动画",资源.静立)
		self.人物["行走"] = tp.资源:载入(资源.资源,"网易WDF动画",资源.行走)
	else
       
		local 资源 = 取模型(数据.造型,v)

		self.人物["静立"] = tp.资源:载入(资源.资源,"网易WDF动画",资源.静立)
		self.人物["行走"] = tp.资源:载入(资源.资源,"网易WDF动画",资源.行走)
	end

	local m = tp:qfjmc(数据.武器数据.类别,数据.武器数据.等级,数据.武器数据.名称)
	local n = 取模型(m.."_"..数据.造型)
	self.人物["武器_静立"] = tp.资源:载入(n.资源,"网易WDF动画",n.静立)
	self.人物["武器_行走"] = tp.资源:载入(n.资源,"网易WDF动画",n.行走)
	-- self.人物["武器_静立"]:置差异(self.人物["武器_静立"].帧数-self.人物["静立"].帧数)
	-- self.人物["武器_行走"]:置差异(self.人物["武器_行走"].帧数-self.人物["行走"].帧数)

	 if 数据.武器数据.染色 ~= nil then

	self.人物["武器_静立"]:置染色(数据.武器数据.染色.染色方案,数据.武器数据.染色.染色组.a,数据.武器数据.染色.染色组.b)
	self.人物["武器_行走"]:置染色(数据.武器数据.染色.染色方案,数据.武器数据.染色.染色组.a,数据.武器数据.染色.染色组.b)
       	end

	if 数据.锦衣数据.锦衣 ==nil and 数据.锦衣数据.定制 ==nil and 数据.锦衣数据.战斗锦衣 ==nil then
	self:置染色(DyeData[数据.造型][1],数据.染色.a,数据.染色.b,数据.染色.c)
	end
	self:置方向(self.方向)
end
function 场景类_人物:卸下装备(数据) --造型.染色方案.染色
	local 资源 = 取模型(数据.造型)
	self.人物["静立"] = tp.资源:载入(资源.资源,"网易WDF动画",资源.静立)
	self.人物["行走"] = tp.资源:载入(资源.资源,"网易WDF动画",资源.行走)
	self.人物["武器_静立"] = nil
	self.人物["武器_行走"] = nil
	if 数据.锦衣数据.锦衣 ==nil  then
   self:置染色(DyeData[数据.造型][1],数据.染色.a,数据.染色.b,数据.染色.c)
	end
	self:置方向(self.方向)
end

function 场景类_人物:穿戴翅膀(数据)--变身.翅膀
	local n = 取模型(数据.翅膀.名称)
	self.人物["翅膀_静立"] = tp.资源:载入(n.资源,"网易WDF动画",n.静立)
	self.人物["翅膀_行走"] = tp.资源:载入(n.资源,"网易WDF动画",n.行走)
	self:置方向(self.方向)
end

function 场景类_人物:卸下翅膀()
	self.人物["翅膀_静立"] = nil
	self.人物["翅膀_行走"] = nil
end
function 场景类_人物:更换行为(行为)
	if self.数据.变身 or  self.数据.坐骑.类型  or  self.数据.锦衣数据.定制  then
		return  0
	end
	if 行为=="站立" then
	    self:初始行为()
	    return
	end
	 local  普通资源 = 取模型(self.数据.造型)
     local 资源=tp.资源
    self.动作行为 = true
    self.循环更新 = false
    if 行为=="招呼" or 行为=="行礼"or 行为=="舞蹈"or 行为=="悲伤"or 行为=="发怒"or 行为=="休息" then
    	 self.武器不显示=true
    	  self.人物["静立"]=资源:载入(普通资源.动作.资源,"网易WDF动画",普通资源.动作[行为])
		if 行为=="休息" then
			self.循环更新 = true
		end
	elseif 行为=="跑步" then
		self.武器不显示=false
			self.人物["静立"]=self.人物["行走"]
			if self.人物["武器_静立"] then
				local m = tp:qfjmc(self.数据.武器数据.类别,self.数据.武器数据.等级,self.数据.武器数据.名称)
				local n = 取模型(m.."_"..self.数据.造型)
				self.人物["武器_静立"] = tp.资源:载入(n.资源,"网易WDF动画",n.行走)
			end
	elseif 行为=="攻击" or 行为=="施法"or 行为=="挨打"or 行为=="死亡" then
		self.武器不显示=false
		local zl=0
		local yyy
		  	if self.人物["武器_静立"] then
				if self.数据.武器数据.名称 == "龙鸣寒水" or self.数据.武器数据.名称 == "非攻" then
				zl = "弓弩1"
				else
				zl = self.数据.武器数据.类别
				end
				local zzz = tp:qfjmc(zl,self.数据.武器数据.等级,self.数据.武器数据.名称)
				yyy = 取模型(zzz.."_"..self.数据.造型)
			end
			local n = 取模型(self.数据.造型)
			if zl ~= 0  then
				n = 取模型(self.数据.造型,zl,true)
			end
		  self.循环更新 = true
		  self.人物["静立"]=tp.资源:载入(n.战斗资源,"网易WDF动画",n[行为])
	      if zl~=0 then
	         self.人物["武器_静立"]=tp.资源:载入(yyy.战斗资源,"网易WDF动画",yyy[行为])
	      end
		
	end
	self:置染色(DyeData[self.数据.造型][1],self.数据.染色.a,self.数据.染色.b,self.数据.染色.c)
	self:置方向(self.方向)
end
function 场景类_人物:初始行为()
		self.循环更新 = false
		self.武器不显示=false
		if  self.动作行为 then
		   self.动作行为 = false

		       self:置模型(self.数据)

		end

end
function 场景类_人物:显示(dt,x,y,pys,s)
	if self.隐藏 then
		return
	end
	local asa = 1
	if self.行为 == "行走" then
		asa = 1.4
	end
	 self.人物[self.行为]:更新(dt*asa,nil,nil,self.循环更新)

	if self.人物["武器_"..self.行为] ~= nil and self.武器不显示==false then
			self.人物["武器_"..self.行为]:更新(dt*asa,nil,nil,self.循环更新)
	elseif self.人物["坐骑_"..self.行为] ~= nil then
		self.人物["坐骑_"..self.行为]:更新(dt*asa)
		if self.人物["坐骑饰品_"..self.行为] ~= nil then
			self.人物["坐骑饰品_"..self.行为]:更新(dt*asa)
		end
	elseif self.人物["召唤兽"..self.行为] ~= nil then
		self.人物["召唤兽"..self.行为]:更新(dt*asa)
	end
	if self.人物["翅膀_"..self.行为] ~= nil then
		self.人物["翅膀_"..self.行为]:更新(dt*asa)
	end
	if self.人物["光环_"..self.行为] ~= nil then
		self.人物["光环_"..self.行为]:更新(dt*asa)
	end
	if self.延时 > 0 then
		self.延时 = self.延时 - 1
	end
	 self.yx = false
	if self.人物["武器_"..self.行为] ~= nil and self.人物["武器_"..self.行为]:是否选中(x,y) then
		self.yx = true
	elseif self.人物["坐骑_"..self.行为] ~= nil and self.人物["坐骑_"..self.行为]:是否选中(x,y) then
		self.yx = true
	elseif self.人物["坐骑饰品_"..self.行为] ~= nil and self.人物["坐骑饰品_"..self.行为]:是否选中(x,y) then
		self.yx = true
	elseif self.人物["召唤兽"..self.行为] ~= nil and self.人物["召唤兽"..self.行为]:是否选中(x,y) then
		self.yx = true
	end
	if (self.人物[self.行为]:是否选中(x,y) or self.yx) and tp:可操作() and not tp.第一窗口移动中 then

		if self.人物["武器_"..self.行为] ~= nil then
			self.人物["武器_"..self.行为]:置高亮()
		elseif self.人物["坐骑_"..self.行为] ~= nil then
			self.人物["坐骑_"..self.行为]:置高亮()
			if self.人物["坐骑饰品_"..self.行为] ~= nil then
				self.人物["坐骑饰品_"..self.行为]:置高亮()
			end
		elseif self.人物["召唤兽"..self.行为] ~= nil then
			self.人物["召唤兽"..self.行为]:置高亮()
		end
		if self.人物["翅膀_"..self.行为] ~= nil then
			self.人物["翅膀_"..self.行为]:置高亮()
		end
			if self.人物["光环_"..self.行为] ~= nil then
			self.人物["光环_"..self.行为]:置高亮()
		end
		self.人物[self.行为]:置高亮()
	else
		if self.人物["武器_"..self.行为] ~= nil then
			self.人物["武器_"..self.行为]:取消高亮()
		elseif self.人物["坐骑_"..self.行为] ~= nil then
			self.人物["坐骑_"..self.行为]:取消高亮()
			if self.人物["坐骑饰品_"..self.行为] ~= nil then
				self.人物["坐骑饰品_"..self.行为]:取消高亮()
			end
		elseif self.人物["召唤兽"..self.行为] ~= nil then
			self.人物["召唤兽"..self.行为]:取消高亮()
		end
		if self.人物["翅膀_"..self.行为] ~= nil then
			self.人物["翅膀_"..self.行为]:取消高亮()
		end
				if self.人物["光环_"..self.行为] ~= nil then
			self.人物["光环_"..self.行为]:取消高亮()
		end
		self.人物[self.行为]:取消高亮()

	end

	if self.人物["光环_"..self.行为] ~= nil then
		self.人物["光环_"..self.行为]:显示(s + pys)

	end

	tp.影子:显示(s + pys)

	if self.人物["坐骑_"..self.行为] ~= nil then
		self.人物["坐骑_"..self.行为]:显示(s + pys)
		if self.人物["坐骑饰品_"..self.行为] ~= nil then
			self.人物["坐骑饰品_"..self.行为]:显示(s + pys)
		end
	end
	self.人物[self.行为]:显示(s + pys)
	if self.人物["翅膀_"..self.行为] ~= nil then
		self.人物["翅膀_"..self.行为]:显示(s + pys)
	end
	if self.人物["武器_"..self.行为] ~= nil and self.武器不显示==false then
		self.人物["武器_"..self.行为]:显示(s + pys)
	elseif self.人物["召唤兽"..self.行为] ~= nil then
		self.人物["召唤兽"..self.行为]:显示(s + pys)
	end
end
return 场景类_人物