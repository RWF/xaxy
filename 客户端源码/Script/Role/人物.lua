-- @Author: 作者QQ381990860
-- @Date:   2021-08-13 19:47:22
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-08-24 23:34:12
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-12-07 03:16:05
--======================================================================--
local 场景类_人物 = class()
local pi = math.pi/8
local floor = math.floor
local ceil  = math.ceil
local insert = table.insert
local remove = table.remove
local min = math.min
local random = 引擎.取随机整数
local xys = 生成XY
local mtb = 取八方向
local jlb = 取两点距离
local yds = 取移动坐标
local jdb = 取两点角度
local tp
local mousax = 引擎.鼠标按下
local hds = 取两点孤度
local ARGB = ARGB
local mouses = 引擎.鼠标按住
local zd,人物字体
local jcs = 0
local ems = 引擎.取敌人信息
local emj = 引擎.取等级怪
local zqj = 引擎.坐骑库
local bzs = 取四至八方向
function 场景类_人物:初始化(根)
	tp=根
    self.特效组 = {}
	self.召唤兽行走时间=0
	self.召唤兽方向=0
	self.移动 = false
	self.路径组 = {}
	self.称谓宽度 = 0
	self.鼠标点击动画组 = {}
	self.脚印动画组 = {}
	self.跟随目标 =false
	self.跟随时间 =0
	-- 判断
	self.标识 = true
	self.坐标 = xys()
	self.鼠标延时=0
	self.行走时间=0
	self.增加 = {x=0,y=0}
	self.延时 = 0
	self.标识 = true
	人物字体 = tp.字体表.人物字体
	self.pys = {}
	self.队长动画 =tp.资源:载入('ZY.dll',"网易WDF动画",0x2231EBB4)
  self.数据 ={}
end


function 场景类_人物:改变队标(数据)
	if 数据== "普通" then
	 self.队长动画 =tp.资源:载入('ZY.dll',"网易WDF动画",0x2231EBB4)
	 elseif 数据=="玫瑰" then
	self.队长动画 = tp.资源:载入('ZY.dll',"网易WDF动画",0xF8107DB0)
	 elseif 数据=="扇子" then
	self.队长动画 = tp.资源:载入('ZY.dll',"网易WDF动画",0x1B25FA29)
	elseif 数据=="音符队标" then
	self.队长动画 =tp.资源:载入('ZY.dll',"网易WDF动画",0x25D20174)
	elseif 数据=="鸭梨队标" then
	self.队长动画 =tp.资源:载入('ZY.dll',"网易WDF动画",0x7AEF08A1)
		elseif 数据=="心飞翔队标" then
	self.队长动画 =tp.资源:载入('ZY.dll',"网易WDF动画",0x9BEF1016)

	end

end
function 场景类_人物:创建角色(数据)
 self.数据=数据

 local 资源 = 取模型(self.数据.造型)
	self.人物 = {["静立"] = tp.资源:载入(资源.资源,"网易WDF动画",资源.静立),["行走"] = tp.资源:载入(资源.资源,"网易WDF动画",资源.行走)}
	 self.人物 =角色动画类(数据,tp)
	self.名称偏移 = xys(math.floor(tp.字体表.人物字体:取宽度(self.数据.名称) / 2),-15)
	tp.角色坐标.x = self.数据.地图数据.x

	tp.角色坐标.y = self.数据.地图数据.y
	self.称谓偏移 = xys(math.floor(tp.字体表.人物字体:取宽度(self.数据.称谓) / 2),-15)

    self.称谓颜色 = 0xFF80BFFF
    if self.数据.称谓 and LabelData[self.数据.称谓] then
        self.称谓颜色 = LabelData[self.数据.称谓].颜色 and LabelData[self.数据.称谓].颜色 or 0xFF80BFFF
        if LabelData[self.数据.称谓].动画   then
            self.称谓动画=tp.资源:载入(LabelData[self.数据.称谓].资源,"网易WDF动画",LabelData[self.数据.称谓].动画)
            self.称谓动画偏移=LabelData[self.数据.称谓].偏移
        elseif  self.数据.effects  then
            self.称谓动画=tp.资源:载入(LabelData[self.数据.effects].资源,"网易WDF动画",LabelData[self.数据.effects].动画)
            self.称谓动画偏移=LabelData[self.数据.effects].偏移
        end
    end
    self.摊位名称=""

    self.摆摊偏移 =xys(tp.字体表.人物字体:取宽度(self.摊位名称) / 2-5,15)
	 窗口标题.名称=self.数据.名称
	 窗口标题.id=self.数据.id
     self.id =self.数据.id
	 系统处理类.数字id=self.数据.id
	 if 数据.观看召唤兽 then
		tp.召唤兽坐标.x,tp.召唤兽坐标.y = tp.角色坐标.x-20,tp.角色坐标.y-20
	self.召唤兽行走时间 =0
	self.召唤兽移动=false
	self.召唤兽 =角色动画类({造型=数据.观看召唤兽.造型,
        变身=数据.观看召唤兽.造型,
        召唤兽饰品=数据.观看召唤兽.饰品,
        染色方案=数据.观看召唤兽.染色方案,
        染色组=数据.观看召唤兽.染色组},tp)
    else
    	self.召唤兽 =nil
    end
        self.队长偏移 = xys(0,self.人物.人物["静立"].高度/2+50)
 end
function 场景类_人物:置模型(数据)--变身.武器数据.染色方案.染色
  if 数据==nil  then
    	return
  end
      self.数据=数据
	 self.人物:置模型(数据)
     self.队长偏移 = xys(0,self.人物.人物["静立"].高度/2+50)
	 self.锦衣数据=数据.锦衣数据
	 if 数据.观看召唤兽   then
	self.召唤兽行走时间 =0
  	self.召唤兽移动=false
    self.召唤兽 =角色动画类({造型=数据.观看召唤兽.造型,
        变身=数据.观看召唤兽.造型,
        召唤兽饰品=数据.观看召唤兽.饰品,
        染色方案=数据.观看召唤兽.染色方案,
        染色组=数据.观看召唤兽.染色组},tp)

  	 tp.召唤兽坐标.x,tp.召唤兽坐标.y = tp.角色坐标.x-20,tp.角色坐标.y-20
  	 self.观看召唤兽 = 数据.观看召唤兽
	 else
	  	self.召唤兽 =nil
	  	self.召唤兽行走时间=nil
	  	self.召唤兽移动=nil
	  	self.观看召唤兽=nil
	 end
end
function 场景类_人物:添加摊位名称(数据)
	if 数据 then
		 self.摊位名称=数据
	  self.摆摊偏移 =xys(tp.字体表.人物字体:取宽度(self.摊位名称) / 2-5,15)
	 if string.len(self.摊位名称)<=4  then
	 self.摆摊动画 =tp.摆摊动画1
	 elseif string.len(self.摊位名称)<=8  then
	  self.摆摊动画 =tp.摆摊动画2
	 else
	  self.摆摊动画 =tp.摆摊动画3
	 end
	else
	   self.摊位名称=""
	end

 end
function 场景类_人物:取目标(路径组)
  self.路径组1=分割文本(路径组,"*-*")
 local 格子 = xys(self.路径组1[1],self.路径组1[2])
 local a = xys(floor(tp.角色坐标.x / 20),floor(tp.角色坐标.y / 20))
  self.路径组 = tp.场景.地图.寻路:寻路(a,格子)
	if self.路径组 == nil or #self.路径组 ==0 then
		return
	end
    self.人物:初始行为()
	self.移动 = true
	tp.场景.地图.增加.z = 4
	self.临时xy =  xys(floor(self.路径组[1].x*20),floor(self.路径组[1].y*20))
	self.同步时间=角色移动

	if self.观看召唤兽 then
		local 召唤兽格子
   		    	   if  tp.召唤兽坐标.x - 80 >=  self.路径组1[1]*20 and tp.召唤兽坐标.y - 80 >= self.路径组1[2]*20 then
   		    	   	    召唤兽格子 = xys(self.路径组1[1]+2,self.路径组1[2]+2)
			    	elseif tp.召唤兽坐标.x - 80 >=  self.路径组1[1]*20  then
						召唤兽格子 = xys(self.路径组1[1]+2,self.路径组1[2])
			    	elseif tp.召唤兽坐标.y - 80 >= self.路径组1[2]*20 then
						召唤兽格子 = xys(self.路径组1[1],self.路径组1[2]+2)
			    	elseif  tp.召唤兽坐标.x + 80 <=  self.路径组1[1]*20 and tp.召唤兽坐标.y - 80 <= self.路径组1[2]*20 then
			    		召唤兽格子 = xys(self.路径组1[1]-2,self.路径组1[2]-2)
			    	elseif tp.召唤兽坐标.x + 80 <=  self.路径组1[1]*20  then
			    	   召唤兽格子 = xys(self.路径组1[1]-2,self.路径组1[2])
			    	elseif tp.召唤兽坐标.y + 80 <= self.路径组1[2]*20 then
			    		召唤兽格子 = xys(self.路径组1[1],self.路径组1[2]-2)
			    	else
			    		召唤兽格子 = xys(self.路径组1[1],self.路径组1[2]-2)
			    	end

		local abs = xys(floor(tp.召唤兽坐标.x / 20),floor(tp.召唤兽坐标.y / 20))
		self.召唤兽路径组 = tp.场景.地图.寻路:寻路(abs,召唤兽格子)
		if self.召唤兽路径组 == nil or #self.召唤兽路径组 ==0 then
		return
		end
        self.召唤兽临时xy =  xys(floor(self.召唤兽路径组[1].x*20),floor(self.召唤兽路径组[1].y*20))
    end
end
function 场景类_人物:取队伍目标(路径组)
 local 格子 = xys(路径组.x/20,路径组.y/20)
 local a = xys(floor(tp.角色坐标.x / 20),floor(tp.角色坐标.y / 20))
  self.路径组 = tp.场景.地图.寻路:寻路(a,格子)
	if self.路径组 == nil or #self.路径组 ==0 then
		return
	end
	self.移动 = true
	tp.场景.地图.增加.z = 4
	self.临时xy =  xys(floor(路径组.x),floor(路径组.y))
    self.同步时间=角色移动
end
function 场景类_人物:刷新(数据)
	self.数据 =数据
end
function 场景类_人物:开始移动(pys)
	if self.路径组 and (#self.路径组==0  or self.路径组[1] == nil) then
		self:停止移动()
		return
	end
	self.人物.行为 = "行走"
	local dsa = 取八方向(取两点角度(tp.角色坐标,self.临时xy))
	if self.数据.变身~= nil  then
		dsa = bzs(dsa)
	end
	if self.行走时间> 0 then
	   self.行走时间=self.行走时间-1
	end
 	if self.数据.锦衣数据.脚印 and self.行走时间<=0  then
		   local ss = 引擎.取脚印(self.数据.锦衣数据.脚印)
	    	local v = {x = tp.角色坐标.x,y = tp.角色坐标.y,ani = tp.资源:载入(ss[2],"网易WDF动画",ss[1])}
	    	v.ani:置方向(dsa)
	    	self.行走时间=18
		  insert(self.脚印动画组,v)
	end
	 if 取两点距离(tp.角色坐标,self.临时xy) <= 4 then -- 小于可移动点直接到达位置
	 	if  self.路径组 == nil or #self.路径组 == 1 then
	 	   self:停止移动()
	 	   return
	 	end
	 	self.路径组 = tp.场景.地图.寻路:寻路1(self.路径组)
	 	tp.场景.跟随坐标 = {self.路径组}
		if self.路径组 and self.路径组[1]  then
		self.临时xy =  xys(floor(self.路径组[1].x*20),floor(self.路径组[1].y*20))
		end
      -- self.移动 = true
      --客户端:发送数据(0,1,4,self.路径组1[1].."*-*"..self.路径组1[2],1)
	 end

	 tp.角色坐标 = 取移动坐标(tp.角色坐标,self.临时xy,2.8)
	

	self.人物:置方向(dsa)
	 if 角色移动-self.同步时间>=1 then
     客户端:发送数据(math.floor(tp.角色坐标.x),2,4,math.floor(tp.角色坐标.y),tp.场景.数据.编号)
     self.同步时间=角色移动
     end
     if self.观看召唤兽 then
		self.召唤兽行走时间=self.召唤兽行走时间+1
		if self.召唤兽行走时间 > 20 then
		    self.召唤兽移动 = true
		end
	end
end
function 场景类_人物:召唤兽开始移动(pys)
	if self.观看召唤兽 ==nil then
	     self:召唤兽停止移动()
	 	   return
	end
	if self.召唤兽路径组 and (#self.召唤兽路径组==0  or self.召唤兽路径组[1] == nil) then
		self:召唤兽停止移动()
		return
	end
	if self.召唤兽临时xy ==nil then
	    return
	end
	self.召唤兽.行为 = "行走"
	local dsa = bzs(取八方向(取两点角度(tp.召唤兽坐标,self.召唤兽临时xy)))
	 if 取两点距离(tp.召唤兽坐标,self.召唤兽临时xy) <= 4 then -- 小于可移动点直接到达位置
	 	if  self.召唤兽路径组 == nil or #self.召唤兽路径组 == 1 then
	 	   self:召唤兽停止移动()
	 	   return
	 	end
	 	self.召唤兽路径组 = tp.场景.地图.寻路:寻路1(self.召唤兽路径组)
		if self.召唤兽路径组 and self.召唤兽路径组[1]  then
		self.召唤兽临时xy =  xys(floor(self.召唤兽路径组[1].x*20),floor(self.召唤兽路径组[1].y*20))
		end
	 end
	tp.召唤兽坐标 = 取移动坐标(tp.召唤兽坐标,self.召唤兽临时xy,2.8)

	self.召唤兽:置方向(dsa)
end
function 场景类_人物:停止移动()
	tp.窗口.挂机系统.触发延时 = os.time() + 取随机数(1,5)
	self.路径组 = {}
	self.人物.行为 = "静立"
	self.移动 = false
	tp.场景.地图.增加.z = 1
	tp.窗口.小地图:清空()
    self.路径组1=nil
	客户端:发送数据(math.floor(tp.角色坐标.x),2,4,math.floor(tp.角色坐标.y),tp.场景.数据.编号)
end
function 场景类_人物:召唤兽停止移动()
	self.召唤兽路径组 = {}
	self.召唤兽行走时间 =0
	self.召唤兽.行为 = "静立"
	self.召唤兽移动 = false
    self.召唤兽路径组1=nil
end
function 场景类_人物:更改坐标(内容)
  self:停止移动()
	 tp.角色坐标={x=内容.x,y=内容.y}
  tp.场景.滑屏.x,tp.场景.滑屏.y = floor(tp.角色坐标.x),floor(tp.角色坐标.y)
 end



function 场景类_人物:人物显示(dt,x,y,pys)
	local 名称颜色 =  self.数据.名称颜色
	local 称谓颜色 = self.称谓颜色 

	if self.人物.人物[self.人物.行为]:是否选中(x,y) and tp:可操作() and not tp.第一窗口移动中 then
		   if tp.组队开关  and  引擎.鼠标按下(0x00) then
           tp.组队开关=false
           tp.鼠标:还原鼠标()
           tp.隐藏UI = false
           客户端:发送数据(0,2,11,"9",1)
           end
		名称颜色 = -419495936
		称谓颜色 = -419495936
	end
	if self.数据.称谓 ~= "" and self.数据.称谓 ~= "无称谓" and self.数据.称谓 ~= nil then
		if self.名称偏移.y ~= - 35 then
			self.名称偏移.y = -35
		end
	else
		if self.名称偏移.y ~= - 15 then
			self.名称偏移.y = -15
		end
	end
	local s = xys(floor(tp.角色坐标.x),floor(tp.角色坐标.y))
	self.人物:显示 (dt,x,y,pys,s)

	if self.数据.称谓  then

        if LabelData[self.数据.称谓]==nil or LabelData[self.数据.称谓].无文字 ==nil then
            if  self.称谓动画 then
                self.称谓动画:显示(s + pys-self.称谓动画偏移-self.称谓偏移)
                 self.称谓动画:更新(dt)
            end
             人物字体:置颜色(称谓颜色)
            人物字体:显示x(s + pys- self.称谓偏移,self.数据.称谓)
            人物字体:置阴影颜色(-1275068416)

        else 
            if  self.称谓动画 then
                self.称谓动画:显示(s + pys-self.称谓动画偏移)
                 self.称谓动画:更新(dt)
            end
        end
        人物字体:置颜色(名称颜色)
        人物字体:显示x(s + pys - self.名称偏移,self.数据.名称)
        人物字体:置阴影颜色(-1275068416)


	else
		      人物字体:置颜色(0xFF70FC70)
		      人物字体:显示x(s + pys - self.名称偏移,self.数据.名称)
		      人物字体:置阴影颜色(ARGB(170,0,0,0))
	end


     if tp.场景.队长开关 then     self.队长动画:显示(s + pys-self.队长偏移 ) end
     if tp.场景.队长开关 then self.队长动画:更新(dt) end
	 if self.摊位名称~="" then
		self.摆摊动画:显示(s + pys-self.队长偏移)
	    tp.字体表.摆摊字体:置颜色(0xFF0000FF)
        tp.字体表.摆摊字体:显示x(s + pys-self.队长偏移-self.摆摊偏移,self.摊位名称)
        if self.摆摊动画:是否选中(x,y) and 引擎.鼠标弹起(1)  then
        	if tp.窗口.摆摊出售.可视==false then

        	    客户端:发送数据(98,1,44,"5",1)
        	end
		end
	 end
	for i=1,#self.特效组 do
		if self.特效组[i] ~= nil then
			self.特效组[i]:更新(dt)
			self.特效组[i]:显示(s + pys)
			if (self.特效组[i].已载入 >= self.特效组[i].帧数-2) then
				self.特效组[i] = nil
				remove(self.特效组,i)
			end
		end
	end
end
function 场景类_人物:召唤兽显示(dt,x,y,pys)
	local zhs = xys(floor(tp.召唤兽坐标.x),floor(tp.召唤兽坐标.y))
		if not self.召唤兽移动 and self.召唤兽.行为 ~= "静立" then
		self.召唤兽.行为 = "静立"
		end
		self.召唤兽:显示(dt,x,y,pys,zhs)
		local 召唤兽名称 = xys(tp.字体表.人物字体:取宽度(self.观看召唤兽.名称) / 2,-15)
		人物字体:置颜色(0xFF70FC70)
		人物字体:显示x(zhs + pys - 召唤兽名称,self.观看召唤兽.名称)
		人物字体:置阴影颜色(ARGB(170,0,0,0))
end
function 场景类_人物:显示(dt,x,y,pys)
	self.pys = pys
	if self.隐藏 then
		return
	end
   if 引擎.鼠标按住(0) and not tp.选中假人  then
		if self.延时 <= 0 and tp:可通行() then
			tp.窗口.小地图:清空()
			local 格子 = xys(floor(x / 20 - pys.x / 20),floor(y / 20 - pys.y / 20))
			local a = xys(floor(tp.角色坐标.x / 20),floor(tp.角色坐标.y / 20))
 
			self.路径组 = tp.场景.地图.寻路:寻路(a,格子)
			tp.场景.跟随坐标 = {self.路径组}
			if self.路径组 ~= nil and self.路径组[1] ~= nil then
				self.延时 = 20
				local v = {
					x = floor(x - pys.x),
					y = floor(y - pys.y),
					ani = tp.资源:载入('JM.dll',"网易WDF动画",0x0D98AC0A)}
				   insert(self.鼠标点击动画组,v)
				  客户端:发送数据(0,1,4,格子.x.."*-*"..格子.y,1)
			else

				self.路径组 = {}
			end
		end
		self.鼠标延时 = self.鼠标延时 +1
	end
	if mousax(0)  then
		self.鼠标延时=0
	end
	if self.鼠标延时 >=35 and not tp.选中假人  then
		if self.延时 <= 0 and tp:可通行() then
			tp.窗口.小地图:清空()
			local 格子 = xys(floor(x / 20 - pys.x / 20),floor(y / 20 - pys.y / 20))
			local a = xys(floor(tp.角色坐标.x / 20),floor(tp.角色坐标.y / 20))
			self.路径组 = tp.场景.地图.寻路:寻路(a,格子)
			self.延时 = 20
				local v = {
					x = floor(x - pys.x),
					y = floor(y - pys.y),
					ani = tp.资源:载入('JM.dll',"网易WDF动画",0x0D98AC0A)
				}


				insert(self.鼠标点击动画组,v)
				if self.路径组 ~= nil and self.路径组[1]  then
					  客户端:发送数据(0,1,4,格子.x.."*-*"..格子.y,1)
			    end
		end
	end
	if self.延时 > 0 then
		self.延时 = self.延时 - 1
	end
    if not self.移动 and mouses(1) or mouses(2) then
        if mouses(1) and not tp.选中假人 and tp:可通行() then
        	self.移动 = false
			local Qzd =  xys(floor(x  - pys.x),floor(y  - pys.y))
			local dsaQ = mtb(jdb(tp.角色坐标,Qzd))
			self.人物:置方向(dsaQ)
		end
    end
    for i=1,#self.鼠标点击动画组 do
		if self.鼠标点击动画组[i] ~= nil then
			self.鼠标点击动画组[i].ani:更新(dt)
			self.鼠标点击动画组[i].ani:显示(self.鼠标点击动画组[i] + pys)
			if (self.鼠标点击动画组[i].ani.已载入 == 5) then
				self.鼠标点击动画组[i].ani = nil
				remove(self.鼠标点击动画组,i)
			end
		end
	end
    for i=1,#self.脚印动画组 do
		if self.脚印动画组[i] ~= nil then
			self.脚印动画组[i].ani:更新(dt)
			self.脚印动画组[i].ani:显示(self.脚印动画组[i] + pys)
			if (self.脚印动画组[i].ani.当前帧 >= self.脚印动画组[i].ani.结束帧) then
				self.脚印动画组[i].ani = nil
				remove(self.脚印动画组,i)
			end
		end
	end
    if self.跟随目标 and self.跟随路径 then
      self.跟随时间 = self.跟随时间 +1
      if self.跟随时间 > (self.跟随路径.编号-1)*12 then
        self:添加队友路径(self.跟随路径)
    	self.跟随目标 =false
    	self.跟随路径=nil
    	self.跟随时间 =0
      end
    end
	if self.观看召唤兽 then
		if    tp.召唤兽坐标.y  < tp.角色坐标.y  then
			self:召唤兽显示(dt,x,y,pys)
			self:人物显示(dt,x,y,pys)
		else
			self:人物显示(dt,x,y,pys)
			self:召唤兽显示(dt,x,y,pys)
		end
	else
		self:人物显示(dt,x,y,pys)
	end
	if self.移动 then
		self:开始移动(pys)
	end
	if self.召唤兽移动 then
		self:召唤兽开始移动()
	end
	self.坐标.x = floor(tp.角色坐标.x)
	self.坐标.y = floor(tp.角色坐标.y)
end
function 场景类_人物:取跟随坐标(i)
	return xys(math.floor(tp.角色坐标.x),math.floor(tp.角色坐标.y)-i-1)
end
function 场景类_人物:加入动画(动画)
	insert(self.特效组,tp:载入特效(动画))
end
function 场景类_人物:添加队友路径(目标)

			local 队长坐标x,队长坐标y =目标.x*20,目标.y*20

			    	if  tp.角色坐标.x - 目标.编号*20 >=  队长坐标x and tp.角色坐标.y - 目标.编号*20 >= 队长坐标y then
			    		客户端:发送数据(0,4,4,(目标.x+(目标.编号-1)*2).."*-*"..(目标.y+(目标.编号-1)*2))

			    	elseif tp.角色坐标.x - 目标.编号*20 >=  队长坐标x  then
			    	    客户端:发送数据(0,4,4,(目标.x+(目标.编号-1)*2).."*-*"..目标.y)

			    	elseif tp.角色坐标.y - 目标.编号*20 >= 队长坐标y then
			    		客户端:发送数据(0,4,4,目标.x.."*-*"..(目标.y+(目标.编号-1)*2))

			    	elseif  tp.角色坐标.x + 目标.编号*20 <=  队长坐标x and tp.角色坐标.y - 目标.编号*20 <= 队长坐标y then
			    		客户端:发送数据(0,4,4,(目标.x-(目标.编号-1)*2).."*-*"..(目标.y-(目标.编号-1)*2))

			    	elseif tp.角色坐标.x + 目标.编号*20 <=  队长坐标x  then
			    	    客户端:发送数据(0,4,4,(目标.x-(目标.编号-1)*2).."*-*"..目标.y)

			    	elseif tp.角色坐标.y + 目标.编号*20 <= 队长坐标y then
			    		客户端:发送数据(0,4,4,目标.x.."*-*"..(目标.y-(目标.编号-1)*2))

			    	else
			    		客户端:发送数据(0,4,4,目标.x.."*-*"..(目标.y-(目标.编号-1)*2))
			    	end
 end
 
 
return 场景类_人物