-- @Author: 作者QQ381990860
-- @Date:   2021-08-13 19:47:22
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-12 01:31:51
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-01-25 02:03:00
--======================================================================--
local 场景类_假人 = class()

local floor = math.floor
local random = 引擎.取随机整数
local rwzt,tp
local mouse = 引擎.鼠标弹起
local pairs = ipairs
--假人战斗动画 3D3AA29E 71E55D8E
function 场景类_假人:初始化(假人)


	self.遇到时间=0
	self.id =1
	 self.队长偏移 = 生成XY(0,85)
	if tp == nil then
		tp = 引擎.场景
		rwzt = tp.字体表.人物字体
	end
	local mx = 假人.模型

	if 假人.锦衣数据 then
			mx = 假人.锦衣数据.."_"..假人.模型
	end
   local 资源 = 取模型(mx)
	self.假人 = {
		["静立"] = tp.资源:载入(资源.资源,"网易WDF动画",资源.静立),
	}

	if 假人.模型 == "知了王" then
		 self.偏移 = 生成XY(0,self.假人["静立"].高度-5)
	else
	self.偏移 = 生成XY(0,85)
	end
	-- 假人.Y = 假人.Y + (random(1,12) / 100)
	self.名称 = 假人.名称
	self.X,self.Y =假人.X,假人.Y
	self.坐标 = 生成XY(floor(假人.X*20),floor(假人.Y*20)+0.1)
	self.真实坐标 = 生成XY(floor(假人.X*20),floor(假人.Y*20)+0.1)
	self.编号 = 假人.编号
	self.行为 = "静立"
	self.需求 = {}
	self.方向 = 假人.方向
	self.记忆方向 = 假人.方向
	self.战斗 = 假人.战斗
	self.事件 = 假人.事件
	self.初始坐标 = self.坐标
	self.事件ID = 假人.任务id
	self.武器数据 = 假人.武器数据
	self.模型 = 假人.模型
	self.特殊BOSS = 假人.特殊BOSS
	self.组合 = 假人.组合
	self.行走开关 = 假人.行走开关
	if 假人.武器数据 ~= nil and 假人.武器数据.名称 ~="" then
		self:置武器(假人,nil,nil,假人.锦衣数据)
	elseif 假人.显示饰品 then
		self:置武器(假人,true,nil,假人.锦衣数据)
	elseif 取召唤兽武器(假人.模型) then
    	self:置武器(假人,nil,true,假人.锦衣数据)
	end
	-- if DyeData[假人.模型] and 假人.染色组 ~= nil and not 假人.锦衣数据 then
	-- 	self.染色方案 = DyeData[假人.模型][1]
	-- 	self.染色组 = 假人.染色组
	-- 	self:置染色(self.染色方案,假人.染色组.a,假人.染色组.b,假人.染色组.c)
	-- end
	if 假人.染色组 ~= nil and not 假人.锦衣数据 then
		self.染色方案 =  假人.染色方案
		self.染色组 = 假人.染色组
		self:置染色(self.染色方案,假人.染色组[1],假人.染色组[2],假人.染色组[3])
	end
	self.名称偏移 = 生成XY(tp.字体表.人物字体:取宽度(self.名称) / 2,-15)
	if 假人.称谓 ~= "" and 假人.称谓 ~= 0 and 假人.称谓 ~= nil then

		self.称谓 = 假人.称谓
		self.称谓偏移 = 生成XY(math.floor(tp.字体表.人物字体:取宽度(self.称谓) / 2),-15)
		self.名称偏移.y = - 35
	end

	    self.称谓颜色 = 0xFF80BFFF
    if self.称谓 and LabelData[self.称谓] then
        self.称谓颜色 = LabelData[self.称谓].颜色 and LabelData[self.称谓].颜色 or 0xFF80BFFF
        if LabelData[self.称谓].动画 then
        	
            self.称谓动画=tp.资源:载入(LabelData[self.称谓].资源,"网易WDF动画",LabelData[self.称谓].动画)

            self.称谓动画偏移=LabelData[self.称谓].偏移
        end
    end

	self.目标格子 = {}
	self.任务开关 = {}
	self.方向开关 = self.假人.静立.方向数 == 8
	self.移动 = false
	-- if self.事件 == "帮战怪物" or self.行走开关 then
	-- 	self.行走时间 = 20+random(-10,10)
	-- 	if 资源.行走 ~= nil then
	-- 		self.假人.行走 = tp.资源:载入(资源.资源,"网易WDF动画",资源.行走)
	-- 	end
	-- end
	--self.静止转移 = self.假人.静立.帧数 <= 4 or (self.事件 == "帮战怪物" or self.行走开关)
	self.静止转移=self.事件 == "帮战怪物" or self.行走开关
	self.触发事件 = 假人.触发事件
	self.触发计次 = 0
	self:置方向(self.方向)
	self.显示1 = true



end

function 场景类_假人:下一点(xy)
	if xy then
		if #self.目标格子 > 1 then
			local 位置 = #self.目标格子
			local 最后 = self.目标格子[位置]
			for n=1,#self.目标格子 do
				if tp.场景.地图.寻路:判断直线障碍(xy,self.目标格子[n]) then
					位置 = n
					break
				end
			end
			for i=1,位置 do
				table.remove(self.目标格子, 1)
			end
			if #self.目标格子 == 0 then
				self.目标格子 = 最后
			end
		end
	end
end

function 场景类_假人:取目标()
	if self.目标格子[1] == nil then
		return
	end
	self.终点 =  生成XY(floor(self.目标格子[1].x*20),floor(self.目标格子[1].y*20))
end


function 场景类_假人:开始移动()
	if self.目标格子[1] == nil then
		self:停止移动()
		return
	end
	self.行为 = "行走"
	local 方向 = 取八方向(取两点角度(self.真实坐标,self.终点),not self.方向开关)
	if 取两点距离(self.真实坐标,self.终点) < 2 then -- 小于可移动点直接到达位置
		self.真实坐标 = self.终点
		table.remove(self.目标格子, 1)
		self:下一点(self.目标格子[1])
		if #self.目标格子 <= 0 then
			self:停止移动()
		end
		self:取目标()
		return false
	end
	self.真实坐标 = 取移动坐标(self.真实坐标,self.终点,2)
	self:置方向(方向)
end

function 场景类_假人:停止移动()
	self.目标格子 = {}
	self.行为 = "静立"
	self.移动 = false
end


function 场景类_假人:事件开始()
	if (日常任务(self.事件) or self.事件=="车迟1"  or self.事件=="门派首席"  or self.事件=="门派闯关开始"or self.事件== "摇钱树"or self.事件== "梦幻瓜子" )and not tp.消息栏焦点 then
		客户端:发送数据(self.编号,7,6,self.事件)
	elseif self.事件 == "帮战宝箱" and not tp.消息栏焦点 then
		客户端:发送数据(self.编号,10,6,self.事件)
	elseif self.事件 == "刷新" and not tp.消息栏焦点 then
		客户端:发送数据(self.编号,2,6)
	else
		客户端:发送数据(self.编号,1,6)
	end
end

function 场景类_假人:更新(dt,x,y)
	if self.显示1 then
		self.假人[self.行为]:更新(dt)
		if self.假人["武器_"..self.行为] ~= nil then
			self.假人["武器_"..self.行为]:更新(dt)
		end

		if self.假人[self.行为]:是否选中(x,y) and tp:可操作() and not tp.第一窗口移动中 then
			tp.选中假人 = true
			if mouse(0) and 取两点距离(tp.角色坐标,self.坐标) < 250   then
				if not self.静止转移 then
					self:事件开始()
					tp.窗口.对话栏.换向 =true
					self:事件方向(tp.角色坐标,self.坐标)
				end
			end
		end

		if self.事件 == "帮战怪物" then
			if 取两点距离(tp.角色坐标,self.坐标) < 25  then
				self.遇到时间=self.遇到时间+1
				if  self.遇到时间==10 then
					self.遇到时间=0
				   客户端:发送数据(self.编号,16,6,self.事件)
				end

				-- for i,v in pairs(tp.场景.假人) do
				-- 	if v.标识 == nil and v.事件 == "帮战怪物" and v.编号 == self.编号 then

				-- 		print(self.标识,self.事件)
				-- 		--table.remove(tp.场景.假人,i)
    --                      return false

				-- 	end

				-- end
			end
		elseif self.触发事件 then
			if 取两点距离(tp.角色坐标,self.坐标) < 35 then
				if self.触发计次 <= 0 then
					self:事件开始()
					self.触发计次 = 35
				else
					self.触发计次 = self.触发计次 - 1
				end
			end
		end
	end
end

function 场景类_假人:显示(dt,x,y,pys)
	if 取两点距离(tp.角色坐标,self.坐标) < 800 then
		if self.行走时间 ~= nil then
			self.行走时间 = self.行走时间 - 1
			if self.行走时间 <= 0 then
				local 临时目标 = self.初始坐标
				临时目标 = 生成XY(floor(临时目标.x / 20)+random(-6,6),floor(临时目标.y / 20)+random(-6,6))
				if 临时目标 ~= nil and self.目标格子[1] == nil then
					local a = 生成XY(floor(self.坐标.x / 20),floor(self.坐标.y / 20))
					self.目标格子 = tp.场景.地图.寻路:寻路(a,临时目标)
					self:取目标()
					self.移动 = true
				end
				self.行走时间 = 250+random(-20,10)
			end
		end
		if tp.窗口.对话栏.换向 == false and self.方向 ~= self.记忆方向 and (self.事件 ~= "帮战怪物" and not self.行走开关) then
			self:置方向(self.记忆方向)
		end
		local s = 生成XY(floor(self.真实坐标.x),floor(self.真实坐标.y))
		
		tp.影子:显示(s + pys)
		self.假人[self.行为]:显示(s +  pys)
		if self.假人["武器_"..self.行为] ~= nil then
			self.假人["武器_"..self.行为]:显示(s + pys)
		end
		if self.武器显示 then
			self.武器[self.行为]:显示(s + pys)
		end
		local 名称颜色 = 0xFFEAD833
		local 称谓颜色 = self.称谓颜色 
		if self.假人[self.行为]:是否选中(x,y) and tp:可操作() and not tp.第一窗口移动中 then
			self.假人[self.行为]:置高亮()
			if self.假人["武器_"..self.行为] ~= nil then
				self.假人["武器_"..self.行为]:置高亮()
			end
			名称颜色 = -419495936
			称谓颜色 = -419495936
		else
			self.假人[self.行为]:取消高亮()
			if self.假人["武器_"..self.行为] ~= nil then
				self.假人["武器_"..self.行为]:取消高亮()
			end
		end

    if self.称谓  then
        if  self.称谓动画 then
            self.称谓动画:显示(s + pys-self.称谓动画偏移)
            self.称谓动画:更新(dt)
        end
        if LabelData[self.称谓]==nil or LabelData[self.称谓].无文字 ==nil then
             rwzt:置颜色(称谓颜色)
            rwzt:显示x(s + pys- self.称谓偏移,self.称谓)
            rwzt:置阴影颜色(-1275068416)
        end
        rwzt:置颜色(名称颜色)
        rwzt:显示x(s + pys - self.名称偏移,self.名称)
        rwzt:置阴影颜色(-1275068416)
	else
		      rwzt:置颜色(名称颜色)
		     rwzt:显示x(s + pys - self.名称偏移,self.名称)
		      rwzt:置阴影颜色(-1275068416)
	end
		
	   if 日常任务(self.事件) and  self.战斗 ==false then
	   	   tp.战斗动画1:显示(s + pys-self.偏移)
	   end
       if self.战斗 then
           tp.战斗动画:显示(s + pys-self.偏移 )
       end
       if self.事件 == "BOSS" then
           tp.战斗准备:显示(s + pys-self.偏移 )
       end
         if self.事件 == "任务" or self.事件 == "游泳" or self.事件 == "门派闯关开始"  or self.事件 == "门派首席" then
           tp.任务动画:显示(s + pys-self.偏移 )
         elseif self.事件 == "商店" then
              tp.商店动画:显示(s + pys-self.偏移 )
       end
		if self.移动 then
			self:开始移动(dt)
		end
		if self.特效 ~= nil then
			self.特效:更新(dt*self.帧率)
			if (self.特效.已载入 >= self.特效.结束帧) then
				self.特效 = nil
				return false
			end
			self.特效:显示(s + pys)
		end
		self.坐标.x = s.x
		self.坐标.y = s.y
		self.显示1 = true
	else
	    self.显示1 = false
	end
end

function 场景类_假人:置方向(d)

	if self.假人["静立"].方向数 < 4 then
		return false
	end
    
	if self.假人["行走"] then
		self.假人["行走"]:置方向(d)
	end

	self.假人["静立"]:置方向(d)

	if self.假人["武器_静立"] ~= nil then
		self.假人["武器_静立"]:置方向(d)
		self.假人["武器_行走"]:置方向(d)
	end

	self.方向 = d
end

function 场景类_假人:置武器(假人,饰品,武器,锦衣)

	if 饰品 == nil and 武器== nil and 锦衣==nil then
		local 资源 = 取模型(假人.模型)
		self.假人["静立"] = tp.资源:载入(资源.资源,"网易WDF动画",资源.静立)
		self.假人["行走"] = tp.资源:载入(资源.资源,"网易WDF动画",资源.行走)
	end
	local n
	if 饰品 then
		n = 取模型("饰品_"..假人.模型)

    elseif 武器 then
    	n = 取模型("武器_"..假人.模型)
	else
		n = 取模型(  tp:qfjmc(假人.武器数据.类别,假人.武器数据.等级,假人.武器数据.名称) .."_"..假人.模型)
	end
	self.假人["武器_静立"] = tp.资源:载入(n.资源,"网易WDF动画",n.静立)

	self.假人["武器_行走"] = tp.资源:载入(n.资源,"网易WDF动画",n.行走)
	--  self.假人["武器_静立"]:置差异(self.假人["武器_静立"].帧数-self.假人["静立"].帧数)
	-- if self.假人["行走"] ~= nil then

	-- 	self.假人["武器_行走"]:置差异(self.假人["武器_行走"].帧数-self.假人["行走"].帧数)
	-- end

	self:置方向(self.方向)
end

function 场景类_假人:置染色(染色方案,a,b,c)
	self.假人["静立"]:置染色(染色方案,a,b,c)
	if self.假人.行走 ~= nil then
		self.假人.行走:置染色(染色方案,a,b,c)
	end
	self:置方向(self.方向)
end

function 场景类_假人:事件方向(a,b)
	 if self.方向开关 then
	 	self:置方向(取八方向(floor(取两点角度(b,a))))
	 	tp.场景.人物.人物:置方向(取八方向(floor(取两点角度(a,b))))
	 else

		self:置方向(取四至八方向(取八方向(floor(取两点角度(b,a)))))
	 	tp.场景.人物.人物:置方向(取八方向(floor(取两点角度(a,b))))
	 end
end

return 场景类_假人