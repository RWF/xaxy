--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:51
--======================================================================--
local 玩家类 = class()
local floor = math.floor
local ceil  = math.ceil
local remove = table.remove
local min = math.min
local xys = 生成XY
local tp
local 人物字体
local jcs = 0

function 玩家类:初始化(数据,根)
	tp =根
	
	self.坐标 = xys()
	人物字体 = tp.字体表.人物字体

  self.名称=数据.名称
  self.id=数据.id
  self.染色=数据.染色
  self.造型=数据.造型
  self.武器数据=数据.武器数据
  self.坐标 ={x=数据.地图数据.x,y=数据.地图数据.y}
  self.召唤兽xy ={x=数据.地图数据.x-40,y=数据.地图数据.y-40}
  self.称谓=数据.称谓
  self.名称颜色= 数据.名称颜色
  self.变身 = 数据.变身
  self.翅膀 = 数据.翅膀
  self.锦衣数据=数据.锦衣数据
  self.坐骑 = 数据.坐骑
  self.染色方案 = 数据.染色方案
  self.观看召唤兽 = 数据.观看召唤兽
  self.effects=数据.effects
  self.特效组 = {}
  self.移动 =false
  self.摊位关注=false
  self.战斗开关=数据.战斗开关
  self.移动目标=0
  self.观看召唤兽=数据.观看召唤兽
  self.摊位名称=数据.摊位名称
  self.路径组={}
  self.特效组={}
  self.发言内容={}
  self.队长数据={开关=数据.队长.开关,动画=tp.资源:载入('ZY.dll',"网易WDF动画",0x2231EBB4)}
  self.名称偏移 = xys(math.floor(tp.字体表.人物字体:取宽度(self.名称) / 2),-15)

  self.称谓偏移 = xys(math.floor(tp.字体表.人物字体:取宽度(self.称谓) / 2),-15)

    self.摆摊偏移 =xys(math.floor(tp.字体表.人物字体:取宽度(self.摊位名称) / 2-5),15)


    self.称谓颜色 = 0xFF80BFFF
    if self.称谓 and LabelData[self.称谓] then
        self.称谓颜色 = LabelData[self.称谓].颜色 and LabelData[self.称谓].颜色 or 0xFF80BFFF
        if LabelData[self.称谓].动画 then
            self.称谓动画=tp.资源:载入(LabelData[self.称谓].资源,"网易WDF动画",LabelData[self.称谓].动画)
            self.称谓动画偏移=LabelData[self.称谓].偏移
        elseif  self.effects  then
            self.称谓动画=tp.资源:载入(LabelData[self.effects].资源,"网易WDF动画",LabelData[self.effects].动画)
            self.称谓动画偏移=LabelData[self.effects].偏移
        end
    end

 self.人物 =角色动画类(数据,tp)
 -- self.人物:置模型(数据)
 if 数据.动作 then
    self.人物:更换行为(数据.动作)
 end
   self.队长偏移 = xys(0,self.人物.人物["静立"].高度/2+50)

self.行走时间 = 0
self.脚印动画组={}
 if string.len(self.摊位名称)<=4  then
 self.摊位动画 =tp.摆摊动画1
 elseif string.len(self.摊位名称)<=8  then
  self.摊位动画 =tp.摆摊动画2
 else
  self.摊位动画 =tp.摆摊动画3
 end

  if 数据.观看召唤兽 then
  	self.召唤兽行走时间 =0
  	self.召唤兽移动=false
  	self.召唤兽行为 = "行走"
  	self.召唤兽 =角色动画类({造型=数据.观看召唤兽.造型,
        变身=数据.观看召唤兽.造型,
        召唤兽饰品=数据.观看召唤兽.饰品,
        染色方案=数据.观看召唤兽.染色方案,
        染色组=数据.观看召唤兽.染色组},tp)

  end
        self.坐标.x = floor(self.坐标.x)
        self.坐标.y = floor(self.坐标.y)
   if 数据.队长.开关 then
   		self.队长数据.开关=true
      if 数据.队长.动画 =="普通" then
         self.队长数据.动画=tp.资源:载入('ZY.dll',"网易WDF动画",0x2231EBB4)
        elseif 数据.队长.动画=="玫瑰" then
		self.队长数据.动画 = tp.资源:载入('ZY.dll',"网易WDF动画",0xF8107DB0)
        elseif 数据.队长.动画=="扇子" then
		self.队长数据.动画 = tp.资源:载入('ZY.dll',"网易WDF动画",0x1B25FA29)
		elseif 数据.队长.动画=="音符队标" then
		self.队长数据.动画=tp.资源:载入('ZY.dll',"网易WDF动画",0x25D20174)
		elseif 数据.队长.动画=="鸭梨队标" then
		self.队长数据.动画=tp.资源:载入('ZY.dll',"网易WDF动画",0x7AEF08A1)
		elseif 数据.队长.动画=="心飞翔队标" then
		self.队长数据.动画=tp.资源:载入('ZY.dll',"网易WDF动画",0x9BEF1016)
      end
   end
end





function 玩家类:开始移动()

	if self.路径组 == nil or #self.路径组 ==0 then
      self.移动 = false
		self.人物.行为 = "静立"
		return
	end
	self.人物.行为 = "行走"
	local dsa = 取八方向(取两点角度(self.坐标,self.移动目标))
	if self.变身 ~= nil then
		dsa = 取四至八方向(dsa)
	end
	if self.行走时间> 0 then
	   self.行走时间=self.行走时间-1
	end
 	if self.锦衣数据.脚印 and self.行走时间<=0 and 玩家屏蔽==false and 低配模式 ==false  then
		   local ss = 引擎.取脚印(self.锦衣数据.脚印)
	    	local v = {x = self.坐标.x,y = self.坐标.y,ani = tp.资源:载入(ss[2],"网易WDF动画",ss[1])}
	    	v.ani:置方向(dsa)
	    	self.行走时间=18
		  table.insert(self.脚印动画组,v)
	end
	if 取两点距离(self.坐标,self.移动目标) <= 4 then -- 小于可移动点直接到达位置
		if #self.路径组 == 1 then
         self.移动 = false
		  self.人物.行为 = "静立"
		   return
		end
		self.路径组 = tp.场景.地图.寻路:寻路1(self.路径组)
		if self.路径组 and self.路径组[1]  then
		self.移动目标 =  xys(floor(self.路径组[1].x*20),floor(self.路径组[1].y*20))
		end
	end
	self.坐标 = 取移动坐标(self.坐标,self.移动目标,2.8)
	self.人物:置方向(dsa)
	 if self.观看召唤兽 then
		self.召唤兽行走时间=self.召唤兽行走时间+1
		if self.召唤兽行走时间 > 20 then
		    self.召唤兽移动 = true
		end
	end
 end

function 玩家类:召唤兽开始移动(id)
	if self.召唤兽路径组 == nil or #self.召唤兽路径组 ==0 then
      self.召唤兽移动 = false
		self.召唤兽.行为 = "静立"
		return
	end
	self.召唤兽.行为 = "行走"
	local dsa = 取四至八方向(取八方向(取两点角度(self.召唤兽xy,self.召唤兽移动目标)))
	if 取两点距离(self.召唤兽xy,self.召唤兽移动目标) <= 4 then -- 小于可移动点直接到达位置
		if #self.召唤兽路径组 == 1 then
         self.召唤兽移动 = false
		  self.召唤兽.行为 = "静立"
		   return
		end
		self.召唤兽路径组 = tp.场景.地图.寻路:寻路1(self.召唤兽路径组)
		if self.召唤兽路径组 and self.召唤兽路径组[1]  then
		self.召唤兽移动目标 =  xys(floor(self.召唤兽路径组[1].x*20),floor(self.召唤兽路径组[1].y*20))
		end
	end
	self.召唤兽xy = 取移动坐标(self.召唤兽xy,self.召唤兽移动目标,2.8)
	self.召唤兽:置方向(dsa)
 end
function 玩家类:召唤兽停止移动()
	self.召唤兽路径组 = {}
	self.召唤兽行走时间 =0
	self.召唤兽.行为 = "静立"
	self.召唤兽移动 = false
    self.召唤兽路径组1=nil
end


function 玩家类:停止移动()
 self.人物.行为 = "静立"
 self.路径组={}
 self.坐标.x=self.路径组.x
 self.坐标.y=self.路径组.y
self.移动 = false
 end
function 玩家类:穿戴装备(数据)--变身.武器数据.染色方案.染色
  if 数据==nil  then
    	return
  end
	 self.人物:置模型(数据)
	 self.锦衣数据=数据.锦衣数据
	 if 数据.观看召唤兽  and 玩家屏蔽==false  then
	self.召唤兽行走时间 =0
  	self.召唤兽移动=false
    self.召唤兽 =角色动画类({造型=数据.观看召唤兽.造型,
        变身=数据.观看召唤兽.造型,
        召唤兽饰品=数据.观看召唤兽.饰品,
        染色方案=数据.观看召唤兽.染色方案,
        染色组=数据.观看召唤兽.染色组},tp)

  	 self.召唤兽xy ={x=数据.地图数据.x-40,y=数据.地图数据.y-40}
  	 self.观看召唤兽 = 数据.观看召唤兽
	 else
	  	self.召唤兽 =nil
	  	self.召唤兽行走时间=nil
	  	self.召唤兽移动=nil
	  	self.观看召唤兽=nil
	 end
end
function 玩家类:召唤兽显示(dt,x,y,pys,n)
    if 低配模式 then
        return
    end
	local ss = xys(floor(self.召唤兽xy.x),floor(self.召唤兽xy.y))
	if not self.召唤兽移动 and self.召唤兽.行为 ~= "静立" then
		self.召唤兽.行为 = "静立"
	end
	self.召唤兽:显示(dt,x,y,pys,ss)
	local 召唤兽名称偏移 = xys(tp.字体表.人物字体:取宽度(self.观看召唤兽.名称) / 2,-15)
				 人物字体:置颜色(0xFF70FC70)
			     人物字体:显示x(ss + pys - 召唤兽名称偏移,self.观看召唤兽.名称)
			      人物字体:置阴影颜色(ARGB(170,0,0,0))
end
function 玩家类:人物显示(dt,x,y,pys,n)
	for i=1,#self.脚印动画组 do
		if self.脚印动画组[i] ~= nil then
			self.脚印动画组[i].ani:更新(dt)
			self.脚印动画组[i].ani:显示(self.脚印动画组[i] + pys)
			if (self.脚印动画组[i].ani.当前帧 >= self.脚印动画组[i].ani.结束帧) then
				self.脚印动画组[i].ani = nil
				remove(self.脚印动画组,i)
			end
		end
	end

	local 名称颜色 = self.名称颜色
	local 称谓颜色 =  self.称谓颜色 
	if self.称谓 ~= "" and self.称谓 ~= "无称谓" and self.称谓 ~= nil then
		if self.名称偏移.y ~= - 35 then
			self.名称偏移.y = -35
		end
	else
		if self.名称偏移.y ~= - 15 then
			self.名称偏移.y = -15
		end
	end
    if (self.人物.人物[self.人物.行为]:是否选中(x,y) or self.人物.yx) and tp:可操作() and not tp.第一窗口移动中 then
           if tp.好友开关 then
            tp.鼠标.置鼠标("好友")
           end
           if 引擎.鼠标按下(0x00) then
				if tp.组队开关   then
				tp.组队开关=false
				客户端:发送数据(self.id+0,6,11,"69@-@79",1)
				tp.鼠标:还原鼠标()
				tp.隐藏UI = false
				elseif tp.好友开关 then
				tp.好友开关=false
				客户端:发送数据(self.id+0,2, 54, "9s")
				tp.鼠标:还原鼠标()
				tp.隐藏UI = false
				elseif tp.给予开关 then
				tp.给予开关=false
				客户端:发送数据(self.id+0,2,26,"9A@-@14",1)
				tp.鼠标:还原鼠标()
				tp.隐藏UI = false
				elseif tp.交易开关 then
				tp.交易开关=false
				客户端:发送数据(self.id+0,6,27,"HY@-@S1Q1",1)
				tp.鼠标:还原鼠标()
				tp.隐藏UI = false
				elseif tp.pk开关 then
				tp.pk开关=false
				客户端:发送数据(self.id+0,6,28,"P7@-@Q4",1)
				tp.鼠标:还原鼠标()
				tp.隐藏UI = false
                elseif tp.变身开关 then
                tp.pk开关=false
                客户端:发送数据(self.id+0,6,64,"P7@-@Q4",1)
                tp.鼠标:还原鼠标()
                tp.隐藏UI = false
				end
		   elseif  引擎.鼠标弹起(0x1) then
		   	 tp.窗口.玩家小窗口:打开(self,x,y)
		   end
    	名称颜色 = -419495936
		称谓颜色 = -419495936
	end
	 	local s = xys(floor(self.坐标.x),floor(self.坐标.y))
	if 玩家屏蔽 ==false then
		self.人物:显示(dt,x,y,pys,s)
	end


    if self.称谓  then

        if LabelData[self.称谓]==nil or LabelData[self.称谓].无文字 ==nil then
                if  self.称谓动画 and 低配模式==false then

                    self.称谓动画:显示(s + pys-self.称谓动画偏移-self.称谓偏移)

                    self.称谓动画:更新(dt)
                end
             人物字体:置颜色(称谓颜色)
            人物字体:显示x(s + pys- self.称谓偏移,self.称谓)
            人物字体:置阴影颜色(-1275068416)
 
        else 
                 if  self.称谓动画 and 低配模式==false then

                    self.称谓动画:显示(s + pys-self.称谓动画偏移)

                    self.称谓动画:更新(dt)
                end
        end
        人物字体:置颜色(名称颜色)
        人物字体:显示x(s + pys - self.名称偏移,self.名称)
        人物字体:置阴影颜色(-1275068416)
	else
		      人物字体:置颜色(0xFF70FC70)
		     人物字体:显示x(s + pys - self.名称偏移,self.名称)
		      人物字体:置阴影颜色(ARGB(170,0,0,0))
	end
	  if self.队长数据.开关 and 玩家屏蔽 ==false then
      	self.队长数据.动画:显示(s + pys-self.队长偏移 )
      	self.队长数据.动画:更新(dt)
      end
	   if self.摊位名称~="" and 摊位屏蔽 ==false then
			self.摊位动画:显示(s + pys-self.队长偏移 )
			if self.摊位关注  then
				tp.字体表.摆摊字体:置颜色(0xFFFF0000)
			 else
			 	tp.字体表.摆摊字体:置颜色(0xFF086DF0)
			end
            tp.字体表.摆摊字体:显示x(s + pys-self.队长偏移-self.摆摊偏移,self.摊位名称)
			if self.摊位动画:是否选中(x,y) and 引擎.鼠标弹起(1) and tp:可操作()  then
                 if tp.窗口.摆摊购买.可视==false then
                      客户端:发送数据(self.id,7,44,"5",1)
                 end
			end
	    end
      if self.战斗开关 and 玩家屏蔽 ==false then
           tp.战斗动画:显示(s + pys-self.队长偏移 )
       end
	for i=1,#self.特效组 do
		if self.特效组[i] ~= nil  and 玩家屏蔽 ==false and 低配模式==false then
			self.特效组[i]:更新(dt)
			self.特效组[i]:显示(s + pys)
			if (self.特效组[i].已载入 >= self.特效组[i].帧数-2) then
				self.特效组[i] = nil
				table.remove(self.特效组,i)
			end
		end
	end
end

function 玩家类:加入动画(动画)
    table.insert(self.特效组,tp:载入特效(动画))
end
function 玩家类:显示(dt,x,y,pys)
		if self.观看召唤兽 and 玩家屏蔽==false then
			if    self.召唤兽xy.y  < self.坐标.y  then
			self:召唤兽显示(dt,x,y,pys,n)
			 self:人物显示(dt,x,y,pys,n)
			else
			 self:人物显示(dt,x,y,pys,n)
			self:召唤兽显示(dt,x,y,pys,n)
			end
	   else
			 self:人物显示(dt,x,y,pys,n)
	   end
    	if self.移动 then
    		self:开始移动(n)
    	end
    	if self.召唤兽移动 then
    		self:召唤兽开始移动(n)
    	end
 end
return 玩家类