-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-03-22 00:22:24
-- 正式进入游戏控制
local 场景类_场景 = class()
local ceil = math.ceil
local floor = math.floor
local sort = table.sort
local require = require
local pairs = pairs
local insert = table.insert
local txk = 引擎.特效库
local mousea = 引擎.鼠标按下
local mouseb = 引擎.鼠标弹起
local ARGB = ARGB
local type = type
local remove = table.remove
local xys = 生成XY
local 场景假人 = require("script/Role/假人")
local 场景玩家 = require("script/Role/玩家")
local fsb = require("script/Resource/FSB")
local tp
local function 音效类_(文件号,资源包,子类)
	if 文件号 ~= nil and 文件号 ~= 0 and tp.系统设置.声音设置[4] then
		fsb(tp.资源:读数据(资源包,文件号)):播放(false)
	end
end

function 场景类_场景:载入特效(特效,zl,ns)
	local a = txk(特效)
	return self.资源:载入(a[2],"网易WDF动画",a[1],zl,nil,ns,false)
end
function 场景类_场景:qfjmc(子类,级别限制,名称)
	if 子类 == "枪" then
		if 级别限制 < 21 then
			名称 = "红缨枪"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "乌金三叉戟"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "玄铁矛"
		end
	elseif 子类 == "斧" then
		if 级别限制 < 21 then
			名称 = "青铜斧"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "双弦钺"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "乌金鬼头镰"
		end
	elseif 子类 == "剑" then
		if 级别限制 < 21 then
			名称 = "青铜短剑"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "青锋剑"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "游龙剑"
		end
	elseif 子类 == "双" then
		if 级别限制 < 21 then
			名称 = "双短剑"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "竹节双剑"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "赤焰双剑"
		end
	elseif 子类 == "飘" then
		if 级别限制 < 21 then
			名称 = "五色缎带"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "无极丝"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "七彩罗刹"
		end
	elseif 子类 == "爪" then
		if 级别限制 < 21 then
			名称 = "铁爪"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "青龙牙"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "青刚刺"
		end
	elseif 子类 == "扇" then
		if 级别限制 < 21 then
			名称 = "折扇"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "铁面扇"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "神火扇"
		end
	elseif 子类 == "棒" then
		if 级别限制 < 21 then
			名称 = "细木棒"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "点金棒"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "满天星"
		end
	elseif 子类 == "锤" then
		if 级别限制 < 21 then
			名称 = "松木锤"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "狼牙锤"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "震天锤"
		end
	elseif 子类 == "鞭" then
		if 级别限制 < 21 then
			名称 = "牛皮鞭"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "钢结鞭"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "青藤柳叶鞭"
		end
	elseif 子类 == "环" then
		if 级别限制 < 21 then
			名称 = "黄铜圈"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "金刺轮"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "蛇形月"
		end
	elseif 子类 == "刀" then
		if 级别限制 < 21 then
			名称 = "柳叶刀"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "金背大砍刀"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "狼牙刀"
		end
	elseif 子类 == "法杖" then
		if 级别限制 < 21 then
			名称 = "曲柳杖"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "墨铁拐"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "腾云杖"
		end
	elseif 子类 == "弓弩" then
		if 级别限制 < 21 then
			名称 = "硬木弓"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "宝雕长弓"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "连珠神弓"
		end
	elseif 子类 == "宝珠" then
		if 级别限制 < 21 then
			名称 = "琉璃珠"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "翡翠珠"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "如意宝珠"
		end
	elseif 子类 == "巨剑" then
		if 级别限制 < 21 then
			名称 = "钝铁重剑"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "壁玉长铗"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "惊涛雪"
		end
	elseif 子类 == "伞" then
		if 级别限制 < 21 then
			名称 = "红罗伞"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "琳琅盖"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "金刚伞"
		end
	elseif 子类 == 18 then
		if 级别限制 < 21 then
			名称 = "素纸灯"
		elseif 级别限制 > 20 and 级别限制 < 51 then
			名称 = "鲤鱼灯"
		elseif 级别限制 > 59 and 级别限制 < 90 then
			名称 = "玲珑盏"
		end
	end

	-- if 迭代==false then
	-- 	return "非迭代_"..名称
	--  else
	 	return 名称
	-- end

end

function 场景类_场景:初始化()
	队伍id = 0
	游戏时间 = 0
	回收时间 = 0
	门派限制 = false
	客户端连接 = 0
	停止连接 = false
	连接状态 = false
	数据输出 = false
	随机序列 = 0
	数据包流 = 0
	数据包流1 = 0
	数据包时间 = os.time()
	测试文本 = "Abc测试"

  客户端 = require("Script/Client/Client")
  客户端:置根(self)
  self._滑块 =  require("script/System/滑块")
	self._自适应 = require("script/System/自适应")
	self._按钮 = require("script/System/按钮")
	self._小型选项栏 = require("script/System/小型选项栏")
	self._丰富文本 = require("script/System/丰富文本")
	self._列表 = require("script/System/列表")
	self._物品格子 = require("script/System/物品_格子")
	self._队标格子 = require("script/System/队标格子")
	self._技能格子 = require("script/System/技能_格子")
	self._内丹格子 = require("script/System/内丹_格子")
	self.资源 = require("script/Resource/加载类1")(self)
	self.角色坐标 = xys()
	self.召唤兽坐标 = xys()

	self.资源:打开()

	local 资源 = self.资源
	local wz = require("gge文字类")
	self.字体表 = {
    华康字体 = wz.创建(程序目录.."Data/pic/hkyt_w5.ttf",14,false,false,true),
    华康字体_=wz.创建(程序目录.."Data/pic/hkyt_w5.ttf",16,false,false,true),
    普通字体 = wz.创建("C:/Windows/Fonts/simsun.ttc",14,false,false,false),
		普通字体1 = wz.创建("C:/Windows/Fonts/simsun.ttc",16,false,false,false),
		普通字体_ = wz.创建("C:/Windows/Fonts/simsun.ttc",13,false,false,false),
		普通字体__ = wz.创建("C:/Windows/Fonts/simsun.ttc",12,false,false,false),
		描边字体 = wz.创建("C:/Windows/Fonts/simsun.ttc",16,false,true,false),
		道具字体 = wz.创建("C:/Windows/Fonts/simsun.ttc",21,true,false,true),

		人物字体 = wz.创建(程序目录.."Data/pic/hkyt_w5.ttf",15,false,false,true),
		人物字体_ = wz.创建("C:/Windows/Fonts/simsun.ttc",16,false,false,false),
		文书字体 = wz.创建("C:/Windows/Fonts/simsun.ttc",18,false,false,true),
		窗口字体 = wz.创建("C:/Windows/Fonts/simsun.ttc",16,false,true,false),
		登入字体 =wz.创建("C:/Windows/Fonts/simsun.ttc",14,false,false,false),

		描边字体_ = wz.创建("C:/Windows/Fonts/simsun.ttc",15,false,true,true),
		描边字体__ = wz.创建(程序目录.."Data/pic/hkyt_w5.ttf",11,false,true,true),
		楷体 = wz.创建(程序目录.."Data/pic/simkai.ttf",18,true,true,true),
		摆摊字体 = wz.创建("C:/Windows/Fonts/simsun.ttc",14,false,false,false),
	};
	wz=nil
	self.字体表.道具字体:置字间距(3.8)
	self.字体表.描边字体:置描边颜色(-16777216)
	self.字体表.描边字体__:置描边颜色(-16777216)
	self.字体表.描边字体_:置描边颜色(-16777216)

	local fc = self._自适应.创建()

	fc:加载(self)

	self.战斗文字 = {{},{},nil,{}}

	for n=1,10 do
		self.战斗文字[1][n] = 资源:载入("红"..n..".png","加密图片")
		self.战斗文字[2][n] = 资源:载入("绿"..n..".png","加密图片")
		self.战斗文字[4][n] = 资源:载入("大"..n..".png","加密图片")
	end

	 	local bqs = {1295847816,1853525647,1076954610,3552721044,3990239921,3366615112,2782337201,3771134163,1462708813,
	2715893287,3002376600,3991654351,993860032,2666294756,454787878,1382010105,2912848813,3919700593,477926852,352380776,
	811687356,915719759,4249060934,1336751228,1950021903,2235513801,3594739784,1960900090,2382390242,1537855326,3907030953,
	2290431679,3202252097,4033571742,1492820992,655187057,2901001332,3190903022,298183232,1945354141,3436623848,1724964988,
	2592865169,3393696884,1494002331,1310769894,1152800681,2542768010,1378600591,2829693277,3846038890,3901444948,68667698,
	3331959614,432707334,4241851683,3448703702,2542506275,241536076,314448958,2242677963,1887092560,1489631920,2860202818,
	4076591726,4232189420,3498505860,4241140149,3858705890,2271353759,3452523393,963399171,2959831232,2917226350,4215743335,
	3987108486,365569753,3701218951,3044567112,3265766525,2129343522,2401287726,2716317030,292723042,4014574629,4183102172,
	4115700508,2139528734,276624883,4099788650,2578443618,3367759523,576638850,3586214754,2830927389,1915332364,1341579386,
	3743372972,1217313750,1208397371,156713767,2057251015,3084935361,1485268859,470535714,2868631088,3835251562,2896374671,
	3982186902,1708428735,2448085336,1354708809,943667221,1146784672,3592760724,611393967,1688780051,2042812914,1466206851,
	3612578974}
   local dbqs ={0X047A251B,0XD07C8C7B,0XD8E6F9A5,0XDA07B03B,0XDCCFB47E,0XDE35A6C0,0XDEA84608,0XE06AA308,0XE1ED15BB,0XE47DA824,0XECD1D654,0XF4831403,0XF6427212,
   0XF6EBDE4C,0XCC7F0159,0X047D0BD0,0X0F7FA3E5,0X1C9419F8,0X1D3F0301,0X2C3FC5BB,0X30981FA2,0X3A7BCB44,0X3FB960DE,0X42A7366B,0X44210711,
   0X4F692A7B,0X4FD80C52,0X50220D93,0X50D0D23E,0X521DA1C0,0X52BB4A65,0X640A80F4,0X65FBF6CC,0X6E92200B,0X74697B48,0X776ACD4E,0X80DE00A6,
   0X8CCD4B6D,0X8D177ED8,0X8D6D0DEE,0X8D956A19,0X9EEFD716,0XA4DFBD47,0XACA57AF5,0XB260288C,0XB328C05F,0XB9ADFF5C,0XBA1A56B3,0XBA1E074F,
   0XBAB5A3A9,0XBD043668,0XC29278D3,0XC29DE5D7,0XC64B89DD,}


  self.大包子表情动画 = {}
  	for i=0,#dbqs-1 do
  		
	    self.大包子表情动画[i] = 资源:载入('JM.dll',"网易WDF动画",dbqs[i+1])
	end
	self.包子表情动画 = {}
	for i=0,119 do
	    self.包子表情动画[i] = 资源:载入('ZY.dll',"网易WDF动画",bqs[i+1])
	end

	self.字体特效 = {}
	self.字体特效1 = {}
	self.字体特效2 = {}
	for i=0,9 do
	    self.字体特效[i] = 资源:载入("特效文字土豪金"..i,"动画")
	    self.字体特效1[i] = 资源:载入("特效文字蓝色"..i,"动画")
	    self.字体特效2[i] = 资源:载入("特效文字绿色"..i,"动画")
	end
	self.提示框 = self._自适应.创建(6,1,1,1,3,9)

	self.外部聊天框    = {}

	for i=1,11 do
		self.外部聊天框[i]=资源:载入("ltk"..i..".bmp","加密图片")
		self.外部聊天框[i]:置区域(0,0,360,1000)--可以充填
	end

	self.战斗动画1=资源:载入('ZY.dll',"网易WDF动画",0x3D3AA29E)
	self.任务动画=资源:载入('ZY.dll',"网易WDF动画",0xA7B20C0F)--953624BC  A7B20C0F
  self.商店动画=资源:载入('JM.dll',"网易WDF动画",0x7BADA4B3)
  self.战斗准备=资源:载入('JM.dll',"网易WDF动画",0xFD9802B5)
  self.战斗动画=资源:载入('ZY.dll',"网易WDF动画",0x97C79EDB)

	self.物品格子焦点_ = 资源:载入('JM.dll',"网易WDF动画",0x6F88F494)
	self.物品格子确定_ = 资源:载入('JM.dll',"网易WDF动画",0x10921CA7)
	self.队标格子焦点_ = 资源:载入('JM.dll',"网易WDF动画",0xB6429B81)
	self.队标格子确定_ = 资源:载入('JM.dll',"网易WDF动画",0xA057CD9C)
	self.技能认证= 资源:载入('JM.dll',"网易WDF动画",0x10B486EE)
	self.物品格子禁止_ = 资源:载入('JM.dll',"网易WDF动画",0x4138B067)
	self.队伍格子焦点_ = 资源:载入('JM.dll',"网易WDF动画",0x1ADE7867)
	self.队伍格子确定_ = 资源:载入('JM.dll',"网易WDF动画",0xC540D7A7)

	self.竖排花纹背景_ = 资源:载入('JM.dll',"网易WDF动画",0x4E5F661E).精灵
	self.竖排花纹背景1_ = 资源:载入('JM.dll',"网易WDF动画",0xADC83326).精灵
	self.横排花纹背景_ = 资源:载入('JM.dll',"网易WDF动画",0xEA6D0A4D).精灵
	self.宽竖排花纹背景_ = 资源:载入('JM.dll',"网易WDF动画",0x2D0F136C).精灵
	self.宠物头像背景_ = 资源:载入('JM.dll',"网易WDF动画",0x363AAF1B).精灵
	self.人物头像背景_ = 资源:载入('JM.dll',"网易WDF动画",0x360B8373).精灵
	self.符石边框 = 资源:载入('JM.dll',"网易WDF动画",0x7F09EF0A)--边框
	self.热卖图标 = 资源:载入('JM.dll',"网易WDF动画",0x76BCDB6F)--推荐
	self.限时图标 = 资源:载入('JM.dll',"网易WDF动画",0xA99382E0)--推荐
	self.折扣图标 = 资源:载入('JM.dll',"网易WDF动画",0x5434A64F)--推荐
	self.奖励图标 = 资源:载入('JM.dll',"网易WDF动画",0x331240BD)--推荐
 	 self.摆摊动画1 =资源:载入('JM.dll',"网易WDF动画",0xBD80E0DB)
	 self.摆摊动画2 =资源:载入('JM.dll',"网易WDF动画",0x6D33D98E)
	 self.摆摊动画3 =资源:载入('JM.dll',"网易WDF动画",0x74786102)
	self.经验背景_ = self._自适应.创建(7,1,186,22,1,3)
	self.影子 = 资源:载入('JM.dll',"网易WDF动画",0xDCE4B562)
	self.光环 = 资源:载入('角色创建光环',"动画")
	self.底图 = 资源:载入('JM.dll',"网易WDF动画",0xA393A808)
	self.传送点 =  资源:载入('JM.dll',"网易WDF动画",0x7F4CBC8C)
    self.加锁图标=资源:载入('JM.dll',"网易WDF动画",0x85655274)
    self.加锁图标1=资源:载入('JM.dll',"网易WDF动画",0x8CB8EE9C)
    self.加锁图标2=资源:载入('JM.dll',"网易WDF动画",0x96AE4546)
	self.画线 = 资源:载入('JM.dll',"网易WDF动画",0xA1442425)
	self.窗口_ = {}
	-- 初始化标题
    self.第二场景 = require("script/UI/第二场景").创建(self)
	-- 初始化鼠标
    self.进程=0
    self.加载界面 = require("script/Login/Load").创建(self)

	self.提示 = require("script/System/提示").创建(self)


     self.系统设置 = {
		声音设置 = {读配置(程序目录.."配置.ini","环境配置","音量")  + 0,读配置(程序目录.."配置.ini","环境配置","音效")  + 0,true,true},
		战斗设置 = {"一级药",30,30,true,true,true,"默认法术",10,"默认法术"}}
	local a = {"Audio/1091.mp3","Audio/1070.mp3"}
	self.音乐 = self.资源:载入(a[引擎.取随机整数(1,2)],"音乐",nil)
	self.拆分开关=false
     self.组队开关=false
     self.交易开关=false
     self.给予开关=false
     self.pk开关=false
     self.变身开关=false
     self.好友开关=false
     self.角色坐标 = xys()
	self.渐变音量 = 40
	self.任务追踪 = true
	self.战斗中 = false
	self.音乐开启 = true
	self.音效开启 = true
	self.人物选中  = false
	self.选中UI  = false
	self.隐藏UI = false
	self.第二场景开启 = false
	self.下一次确定 = false
	self.按钮焦点  = false
	self.禁止通行 = false
	self.消息栏焦点 = false
	self.快捷技能显示 = false
	self.按下中 = false
	self.第一窗口移动中 = false
	self.第二窗口移动中 = false
	self.第二次删除 = 0
	self.运行时间 = 0
	self.恢复UI = false
    self.快捷技能锁定 = true
    self.鼠标 = require("script/System/鼠标").创建(self)
	self.喊话 = require("script/System/喊话").创建(self)
	self.玩家喊话 = require("script/System/玩家喊话").创建(self)

	  tp = self
     self.窗口_={}


end

function 场景类_场景:关闭窗口()
	for i,v in pairs(self.窗口_) do
		if v.可视 then
			if v.ID ~= 3 then
				if v.ID == 4 or v.ID == 7 then
					v:打开()
				else
					v.可视 = false
				end
			end
		end
	end
	self.选中窗口 = 0
	self.选中UI  = false
end

function 场景类_场景:取鼠标所在窗口(x,y)
	if not self.隐藏UI and not tp.战斗中 then
		for n=#self.窗口_, 1,-1 do
	        if self.窗口_[n]:检查点(x,y)  then
	        	self.选中UI = true
				return n
	        end
		end
	end
	return 0
end

function UI排序(a,b)
 	return a.窗口时间 < b.窗口时间
end
function 场景类_场景:销毁()
  self.音乐:停止()
  self.进程=6
  self.标题 = nil
  self.读取 = nil
  self.创建 = nil
  self.注册 = nil
  self.登入 = nil
  self.选择 = nil
  self.加载界面 = nil
 collectgarbage("collect")
 
end

local jz = 0

function 场景类_场景:显示(dt,x,y)
	self.传送点:更新(dt)
    self.光环:更新(dt)
	for i=0,119 do
			self.包子表情动画[i]:更新(dt)
	end
	for i=0,53 do
			self.大包子表情动画[i]:更新(dt)
	end

	for i=0,9 do
	    self.字体特效[i]:更新(dt)
	    self.字体特效1[i]:更新(dt)
	    self.字体特效2[i]:更新(dt)
	end
	if self.隐藏UI then
		self.选中UI = false
	end
	if self.进程 == 1 or self.进程 == 2 or self.进程 == 3 or self.进程 == 4 or self.进程 == 5 or self.进程 == 7 then
	 	self.音乐:播放()
	    self.标题:显示(dt,x,y)
		if self.进程 == 2 then
			self.读取:显示(dt,x,y)
		elseif self.进程 == 3 then
			self.登入:显示(dt,x,y)
		elseif self.进程 == 4 then
			self.创建:显示(dt,x,y)
		elseif self.进程 == 5 then
			self.注册:显示(dt,x,y)
		elseif self.进程 == 7 then
			
			self.选择:显示(dt,x,y)
		end
	elseif  self.进程 == 0 then
		 self.加载界面:显示(dt,x,y)
	elseif self.进程 == 6  then
        self.场景:显示(dt,x,y)
		self.喊话:显示()
		self.玩家喊话:显示()

		self.战斗动画1:更新(dt)
		self.战斗动画:更新(dt)
		self.战斗准备:更新(dt)
		self.任务动画:更新(dt)
		self.商店动画:更新(dt)
        if not self.隐藏UI then
			self.队伍格子焦点_:更新(dt)
			self.物品格子焦点_:更新(dt)
			self.队标格子焦点_:更新(dt)
			self.队标格子确定_:更新(dt)
			self.符石边框:更新(dt)
			self.技能认证:更新(dt)
			self.窗口.时辰:显示(dt,x,y)
			self.窗口.人物框:显示(dt,x,y)
			self.窗口.底图框:显示(dt,x,y)
			self.窗口.快捷技能栏:显示(dt,x,y)

			if self.战斗中 then
				for n=1,#self.场景.战斗.窗口_ do
					if not self.隐藏UI then
						self.场景.战斗.窗口_[n]:显示(dt,x,y)
						if self.按钮焦点 then
							self.场景.战斗.窗口_[n].焦点 = true
						end
					end
				end
			else
				self.窗口.聊天框类:显示(dt,x,y)
				self.窗口.任务追踪:显示(dt,x,y)
				sort(self.窗口_,UI排序)
				for n=1,#self.窗口_ do
					if self.窗口_[n].可视 then
						self.窗口_[n]:显示(dt,x,y)
						self.窗口_[n].焦点 = self.窗口_[n].焦点 or self.按钮焦点
						if self.窗口_[self.选中窗口] ~= nil and self.窗口_[self.选中窗口].ID == self.窗口_[n].ID and not self.按下中 then
							self.窗口_[n].鼠标 = true
							if mouseb(1) and self.抓取物品 == nil and self.抓取技能==nil and not self.禁止关闭 and not self.消息栏焦点 and not self.窗口_[n].焦点1 then
								self.窗口_[n]:打开()
							end
						else
							if self.第一窗口移动中 == false then
								self.窗口_[n].鼠标 = false
							end
						end
					end
				end
				if mousea(0) and self.选中窗口 ~= 0  and self.窗口_[self.选中窗口] and self.窗口_[self.选中窗口].可移动 and not self.消息栏焦点 then
					self.窗口_[self.选中窗口]:初始移动(x,y)

				elseif mouseb(0)  or self.隐藏UI or self.消息栏焦点 then
					self.移动窗口 = false
					self.第一窗口移动中 = false
				end
				if self.移动窗口 and not self.隐藏UI and not self.消息栏焦点 and self.窗口_[#self.窗口_] then
					self.第一窗口移动中 = true
					self.窗口_[#self.窗口_]:开始移动(x,y)
				end
				for n=1,#self.窗口_ do
					if self.窗口_[n] and not self.窗口_[n].可视 then
						remove(self.窗口_,n)
					end
				end
			end
			self.窗口.游戏公告:显示(dt,x,y)
			self.窗口.仙玉广播:显示(dt,x,y)
		end
		if self.抓取物品 ~= nil then
			self.抓取物品.小模型:显示(x - 25,y - 20)
		end
		if self.抓取技能 ~= nil then
			self.抓取技能.小模型:显示(x - 12,y - 12)
			if mouseb(1) then
				self.抓取技能 =nil
				self.抓取技能ID=nil
			end
		end
    end
    if self.第二场景开启 then
		self.隐藏UI = true
		self.第二场景:显示(dt,x,y)
	end

	if #self.提示.寄存内容 > 0 then
		if self.提示.寄存内容.开启提示 then
			self.提示框:置宽高(self.提示.寄存内容.提示坐标[3]+15,self.提示.寄存内容.提示坐标[4]+12)
			self.提示框:显示(self.提示.寄存内容.提示坐标[1],self.提示.寄存内容.提示坐标[2])
		end
		for i=1,#self.提示.寄存内容 do
			if self.提示.寄存内容[i].内容 ~= nil then
			    self.提示.寄存内容[i].内容:显示(self.提示.寄存内容[i].x,self.提示.寄存内容[i].y)
			else
				if self.提示.寄存内容[i].文字 ~= nil then
				 	self.提示.寄存内容[i].文字:置颜色(self.提示.寄存内容[i].颜色):显示(self.提示.寄存内容[i].坐标[1],self.提示.寄存内容[i].坐标[2],self.提示.寄存内容[i].文本)
				end
			end
		end
		self.提示:清空寄存()
	end

	if self.选中假人 and self.鼠标.取当前()=="普通" then
		self.鼠标.置鼠标("事件")
	end
	if self.好友开关 then
       self.鼠标.置鼠标("好友")
    end
    if self.拆分开关 then
    self.鼠标.置鼠标("拆分")
    end
    if self.组队开关 or self.给予开关 or self.交易开关 or self.pk开关 or self.拆分开关 or self.变身开关 or self.好友开关 then
		if 引擎.鼠标弹起(0x01) then
		self.组队开关=false
		self.给予开关=false
		self.交易开关=false
		self.pk开关=false
		self.变身开关=false
		self.拆分开关=false
		self.好友开关=false
		self.鼠标:还原鼠标()
		tp.隐藏UI = false
		end
    end
    self.提示:显示()
    self.鼠标:更新(dt,x,y)
	self.鼠标:显示(dt,x,y)
	if __gge.isdebug then
		self.字体表.道具字体:显示(50,140,string.format('%d,%d',x,y))
	end
	self.选中UI = false
	if self.下一次确定 then
		self.选中UI = true
	end
	if self.选项栏选中 then
		self.第二次删除 = self.第二次删除 + 1
		if self.第二次删除 == 2 then
			self.第二次删除 = 0
			self.选项栏选中 = false
		end
	end
	self.禁止通行 = false
	self.按钮焦点 = false
	self.禁止关闭 = false
	self.消息栏焦点 = false
	if self.进程 == 1 or self.进程 == 2 or self.进程 == 3 then
		-- if self.渐变音量 < self.系统设置.声音设置[1] then
		-- 	self.渐变音量 = self.渐变音量 + 2
			self.音乐:置音量(self.系统设置.声音设置[1])
		-- end
		if self.进程 == 2 then
		end
	elseif self.进程 == 6 then
		if not self.隐藏UI then
			if self.窗口.文本栏.可视 or self.窗口.对话栏.可视  then
				self.消息栏焦点 = true
			end

			if not self.战斗中 then
				self.选中窗口 = self:取鼠标所在窗口(x,y)
			else
				self.场景.战斗.选中窗口 = self.场景.战斗:取鼠标所在窗口(x,y)
			end
		end
	end

	if 整秒处理时间 == nil or os.time() - 整秒处理时间 >= 1 then
		整秒处理()
		整秒处理时间 = os.time()
	end
end



function 场景类_场景:添加玩家(数据)
			if type(数据)~="table" then
				print(string.format("出错玩家数据="),数据)	
			    return
			end
			self.场景.玩家[数据.id+0]=场景玩家(数据,self)
			self.场景.场景人物[#self.场景.场景人物+1]=self.场景.玩家[数据.id+0]
			 -- insert(self.场景.场景人物,self.场景.玩家[数据.id+0])

end
function 场景类_场景:更换摊位名称(数据)
 if self.场景.玩家[数据.id]==nil then return 0 end
	 self.场景.玩家[数据.id].摊位名称=数据.名称

     self.场景.玩家[数据.id].摆摊偏移 =xys(tp.字体表.人物字体:取宽度(self.场景.玩家[数据.id].摊位名称) / 2-5,15)

	 if string.len(self.场景.玩家[数据.id].摊位名称)<=4  then
	 self.场景.玩家[数据.id].摊位动画 =self.摆摊动画1
	 elseif string.len(self.场景.玩家[数据.id].摊位名称)<=8  then
	  self.场景.玩家[数据.id].摊位动画 =self.摆摊动画2
	 else
	  self.场景.玩家[数据.id].摊位动画 =self.摆摊动画3
	 end
 end

function 场景类_场景:取目标(路径组)
  local 路径组1=分割文本(路径组.数据,"*-*")
 local 格子 = xys(路径组1[1],路径组1[2])
 if self.场景.玩家 ==nil or self.场景.玩家[路径组.id] ==nil then
    return
 end

 local a =xys(floor(self.场景.玩家[路径组.id].坐标.x / 20),floor(self.场景.玩家[路径组.id].坐标.y / 20))

  self.场景.玩家[路径组.id].路径组 = tp.场景.地图.寻路:寻路(a,格子)
	if self.场景.玩家[路径组.id].路径组 == nil or #self.场景.玩家[路径组.id].路径组 ==0 then
		return
	end

	self.场景.玩家[路径组.id].移动 = true
   -- self.场景.玩家[路径组.id].人物:初始行为()
    self.场景.玩家[路径组.id].移动目标= xys(floor(self.场景.玩家[路径组.id].路径组[1].x*20),floor(self.场景.玩家[路径组.id].路径组[1].y*20))
   	if self.场景.玩家[路径组.id].观看召唤兽 then
					local 召唤兽格子=xys(路径组1[1],路径组1[2])
   		    	   if  self.场景.玩家[路径组.id].召唤兽xy.x - 80 >=  路径组1[1]*20 and self.场景.玩家[路径组.id].召唤兽xy.y - 80 >= 路径组1[2]*20 then
   		    	   	    召唤兽格子 = xys(路径组1[1]+2,路径组1[2]+2)
			    	elseif self.场景.玩家[路径组.id].召唤兽xy.x - 80 >=  路径组1[1]*20  then
						召唤兽格子 = xys(路径组1[1]+2,路径组1[2])
			    	elseif self.场景.玩家[路径组.id].召唤兽xy.y - 80 >= 路径组1[2]*20 then
						召唤兽格子 = xys(路径组1[1],路径组1[2]+2)
			    	elseif  self.场景.玩家[路径组.id].召唤兽xy.x + 80 <=  路径组1[1]*20 and self.场景.玩家[路径组.id].召唤兽xy.y - 80 <= 路径组1[2]*20 then
			    		召唤兽格子 = xys(路径组1[1]-2,路径组1[2]-2)
			    	elseif self.场景.玩家[路径组.id].召唤兽xy.x + 80 <=  路径组1[1]*20  then
			    	   召唤兽格子 = xys(路径组1[1]-2,路径组1[2])
			    	elseif self.场景.玩家[路径组.id].召唤兽xy.y + 80 <= 路径组1[2]*20 then
			    		召唤兽格子 = xys(路径组1[1],路径组1[2]-2)
			    	else
			    		召唤兽格子 = xys(路径组1[1],路径组1[2]-2)
			    	end
		local abs = xys(floor(self.场景.玩家[路径组.id].召唤兽xy.x / 20),floor(self.场景.玩家[路径组.id].召唤兽xy.y / 20))
		self.场景.玩家[路径组.id].召唤兽路径组 = tp.场景.地图.寻路:寻路(abs,召唤兽格子)
		if self.场景.玩家[路径组.id].召唤兽路径组 == nil or #self.场景.玩家[路径组.id].召唤兽路径组 ==0 then
		return
		end
         self.场景.玩家[路径组.id].召唤兽移动目标 =  xys(floor(self.场景.玩家[路径组.id].召唤兽路径组[1].x*20),floor(self.场景.玩家[路径组.id].召唤兽路径组[1].y*20))
    end

end


function 场景类_场景:更改队长(数据,内容)
  if self.场景.玩家[数据.id]~=nil then
   self.场景.玩家[数据.id].队长数据.开关=内容
  end
  if 内容 and  self.场景.玩家[数据.id] then
  	    if 数据.动画 =="普通" then
          self.场景.玩家[数据.id].队长数据.动画=tp.资源:载入('ZY.dll',"网易WDF动画",0x2231EBB4)
        elseif 数据.动画 =="玫瑰" then
			self.场景.玩家[数据.id].队长数据.动画 = tp.资源:载入('ZY.dll',"网易WDF动画",0xF8107DB0)
        elseif 数据.动画 =="扇子" then
			self.场景.玩家[数据.id].队长数据.动画 = tp.资源:载入('ZY.dll',"网易WDF动画",0x1B25FA29)
		elseif 数据.动画=="音符队标" then
			self.场景.玩家[数据.id].队长数据.动画=tp.资源:载入('ZY.dll',"网易WDF动画",0x25D20174)
		elseif 数据.动画=="鸭梨队标" then
			self.场景.玩家[数据.id].队长数据.动画=tp.资源:载入('ZY.dll',"网易WDF动画",0x7AEF08A1)
		elseif 数据.动画=="心飞翔队标" then
			self.场景.玩家[数据.id].队长数据.动画=tp.资源:载入('ZY.dll',"网易WDF动画",0x9BEF1016)
     	end
   end
 end

function 场景类_场景:生成Npc(数据)
			
			self.场景.临时NPC[#self.场景.临时NPC+1]=场景假人(数据)
			self.场景.场景人物[#self.场景.场景人物+1]=self.场景.临时NPC[#self.场景.临时NPC]
			-- insert(self.场景.临时NPC,假人)
			-- insert(self.场景.场景人物,假人)
end

function 场景类_场景:Npc(数据)
	if 数据 ~=nil  then
		for i=1,#数据 do
			self.场景.假人[i]=场景假人(数据[i])
			self.场景.场景人物[#self.场景.场景人物+1]=self.场景.假人[i]
		end
		-- for n, v in pairs(数据) do
		--     insert(self.场景.假人,场景假人(数据[n]))
		-- end
	end

	--  for i, v in pairs(self.场景.假人) do
	-- 	insert(self.场景.场景人物, self.场景.假人[i])
	-- end
end
function 场景类_场景:临时Npc(数据)
	if 数据  then

		-- for i=1,#数据 do

		-- 	self.场景.临时NPC[i]=场景假人(数据[i])
		-- 	self.场景.场景人物[#self.场景.场景人物+1]=self.场景.临时NPC[i]
		-- end
		for n, v in pairs(数据) do
			local lsjr=场景假人(数据[n])
		    insert(self.场景.临时NPC,lsjr)
		    insert(self.场景.场景人物,lsjr)
		end
	end

  -- for i=1,#self.场景.临时NPC do
  	
  -- end
 --   for i, v in pairs(self.场景.临时NPC) do
	-- 	insert(self.场景.场景人物, self.场景.临时NPC[i])
	-- end
end



function 场景类_场景:NPC更改战斗(id)
 for i=1,#self.场景.场景人物 do
 	if self.场景.场景人物[i].事件ID == id then
		   self.场景.场景人物[i].战斗 =true
		break
	end
 end
  -- for i, v in pairs(self.场景.场景人物) do
	 --    if v.事件ID == id then
		--     v.战斗 =true
		-- 	break
		-- end
  --  end

end
function 场景类_场景:NPC取消战斗(id)
for i=1,#self.场景.场景人物 do
 	if self.场景.场景人物[i].事件ID == id then
		   self.场景.场景人物[i].战斗 =false
		break
	end
 end

  -- for i, v in pairs(self.场景.场景人物) do
		-- if v.事件ID == id then
		-- 	v.战斗 =false
		-- 	break
		-- end
  --  end


end
function 场景类_场景:删除Npc(id)


  -- for i, v in pairs(self.场景.场景人物) do
	 --    if self.场景.场景人物[i].事件ID == id then
		--     remove(self.场景.场景人物,i)
		-- 	break
		-- end
  --  end
  --  for i, v in pairs(self.场景.临时NPC) do
	 --    if self.场景.临时NPC[i].事件ID == id then
		--     remove(self.场景.临时NPC,i)
		-- 	break
		-- end
  --  end
	for i=#self.场景.临时NPC,1,-1 do
		if self.场景.临时NPC[i].事件ID == id then
			    remove(self.场景.临时NPC,i)
				break
		end
	end
	for i=#self.场景.场景人物,1,-1 do
		if self.场景.场景人物[i].事件ID == id then
			    remove(self.场景.场景人物,i)
				break
		end
	end
end
function 场景类_场景:播放特效(地图,模型,名称,特效,帧率)
	if self.当前地图 == 地图 then
		for i=1,#self.场景.假人 do
			if self.场景.假人[i].模型 == 模型 and self.场景.假人[i].名称 == 名称 then
				self.场景.假人[i].特效 = self:载入特效(特效)
				self.场景.假人[i].帧率 = 帧率
				local mc = 引擎.取音效(特效)
				if mc ~= nil then
					音效类_(mc.文件,mc.资源)
				end
				break
			end
		end
	end
end



function 场景类_场景:可通行()
	return not self.人物选中 and not self.选中UI and not self.按钮焦点 and not self.消息栏焦点 and self.抓取物品 == nil and self.抓取技能 == nil and not self.禁止通行 and not self.下一次确定 and not self.按下中 and not self.第一窗口移动中 and not self.第二窗口移动中
end
function 场景类_场景:载入特效(特效,zl,ns)
	local a = txk(特效)
	return self.资源:载入(a[2],"网易WDF动画",a[1],zl,nil,ns,false)
end
function 场景类_场景:可操作()
	return not self.消息栏焦点 and not self.选中UI
end
return 场景类_场景






