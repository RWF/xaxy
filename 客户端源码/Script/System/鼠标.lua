--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:51
--======================================================================--
local 系统类_鼠标 = class()

local 当前
local 记忆当前
local 指针 = {}
local tp

function 系统类_鼠标:初始化(根)
	local 资源 = 根.资源
	指针["普通"] = 资源:载入('JM.dll',"网易WDF动画",0x535C1994)
	指针["攻击"] = 资源:载入('JM.dll',"网易WDF动画",0x1FBC5273)
	指针["道具"] = 资源:载入('JM.dll',"网易WDF动画",0xB48A9B3D)
	指针["捕捉"] = 资源:载入('JM.dll',"网易WDF动画",0xC5750B15)
	指针["保护"] = 资源:载入('JM.dll',"网易WDF动画",0xB352AE45)
	指针["禁止"] = 资源:载入('JM.dll',"网易WDF动画",0x1733E33B)
	指针["输入"] = 资源:载入('JM.dll',"网易WDF动画",0xC0247799)
	指针["事件"] = 资源:载入('JM.dll',"网易WDF动画",0xB3662702)
	指针["组队"] = 资源:载入('JM.dll',"网易WDF动画",0x183DC759)
	指针["交易"] = 资源:载入('JM.dll',"网易WDF动画",0xB87E0F0C)
	指针["给予"] = 资源:载入('JM.dll',"网易WDF动画",0xCF1D211E)
	指针["好友"] = 资源:载入('JM.dll',"网易WDF动画",0x8DDC5946)
	指针["拆分"] = 资源:载入('ZY.dll',"网易WDF动画",0x6C27FCF1)
	当前 = "普通"
	记忆当前 = 当前
	tp = 根
end

function 系统类_鼠标.置鼠标(v)
	if 当前 == v then
		return false
	end
	if v == "普通" or  v == "拆分"   or v == "输入" or v == "事件"  or (  v == "攻击" and  tp.战斗中) then
		tp.隐藏UI = false
		tp.交易开关=false
		tp.组队开关=false
		tp.给予开关=false
		-- tp.拆分开关=false
		tp.pk开关=false
		tp.变身开关=false
		tp.好友开关=false


	else
		tp.隐藏UI = true
	end
	if 记忆当前 ~= 当前 then
		记忆当前 = 当前
	end
	当前 = v
end

function 系统类_鼠标.取当前()
	return 当前
end

function 系统类_鼠标.还原鼠标()
	当前 = "普通"
	tp.隐藏UI = false
end

function 系统类_鼠标:更新(dt,x,y)
	指针[当前]:更新(dt)

end

function 系统类_鼠标:显示(dt,x,y)
	指针[当前]:显示(x,y)
end

return 系统类_鼠标