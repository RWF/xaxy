-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-20 03:13:51
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:51
--======================================================================--
local 系统类_物品格子 = class()
local tp
local mouse = 引擎.鼠标弹起

function 系统类_物品格子:初始化(x,y,ID,注释,遮挡)
	self.ID = ID
	self.注释 = 注释
	self.物品 = nil
	self.事件 = false
	self.焦点 = false
	self.右键 = false
	self.遮挡 = 遮挡
	self.确定 = false

end

function 系统类_物品格子:置根(根)
	tp = 根

end



function 系统类_物品格子:置物品(物品,Time)

	if 物品 ~= nil then
      
        if not ItemData[物品.名称] then
             error(string.format("名称为%s这个物品数据错误",物品.名称 ))
            return
        end

		local item =ItemData[物品.名称]
		if 物品.名称 ~="怪物卡片" and 物品.名称 ~=nil then
                self.物品 = 物品
                if Time  and self.物品.Time then
                self.物品.Time=os.time()+48*3600
                end
                if 物品.类型== "宝石" then
                  self.物品.部位 =item.部位
                end
                 -- print(物品.名称)
				self.物品.小模型 = tp.资源:载入(item.文件 ,"网易WDF动画",item.小图标)
                   -- print(物品.名称)
				self.物品.大模型 = tp.资源:载入(item.文件 ,"网易WDF动画",item.大图标)
        elseif 物品.名称 == "怪物卡片" then
        	self.物品=  table.copy(CardData[物品.类型])
            self.物品.名称=物品.名称
            self.物品.类型=物品.类型
            self.物品.number =物品.number
            self.物品.使用次数=物品.使用次数
            self.物品.价格=物品.价格
             if self.物品.卡片等级 ==9 and self.物品.小模型==nil then
        		self.物品.小模型 = tp.资源:载入("WP.dll" ,"网易WDF动画",0x76B5210F)
        		   if self.物品.资源 ==nil then
        		   	self.物品.大模型 = tp.资源:载入("WP.dll","网易WDF动画",self.物品.大模型)
        		   else
        		  	self.物品.大模型 = tp.资源:载入(self.物品.资源,"网易WDF动画",self.物品.大模型)
                  end
        	else
        	     self.物品.小模型 = tp.资源:载入(self.物品.资源 ,"网易WDF动画",self.物品.小模型)
                 self.物品.大模型 = tp.资源:载入(self.物品.资源 ,"网易WDF动画",self.物品.大模型)
        	end
		end
	else
		self.物品 = nil
	end
end

function 系统类_物品格子:显示(dt,x,y,条件,总类,xx,yy,abs,类别,图标,主动法宝)

	self.事件 = false
	self.焦点 = false
	self.右键 = false
	if self.物品 ~= nil then
		if self.遮挡 ~= nil then
			self.遮挡:显示(self.x-1+(xx or 0),self.y+4+(yy or 0))
		end
		if self.物品.名称 == "水墨游龙" or self.物品.名称 == "甜蜜糖果" then
			self.物品.小模型:显示(self.x+25,self.y+27)
		 else
		 	self.物品.小模型:显示(self.x-1,self.y+2)
		end

	   if self.物品.加锁 then
	 	tp.加锁图标:显示(self.x+37,self.y+3)
	   end
		 if self.物品.图标 and 图标 then
		 	if self.物品.图标 =="折扣" then
		      tp.折扣图标:显示(self.x-2,self.y+2)
		 	elseif self.物品.图标 =="限时" then
		 		tp.限时图标:显示(self.x-2,self.y+2)
		 	elseif self.物品.图标 =="热卖" then
		 		tp.热卖图标:显示(self.x-2,self.y+2)
		 	elseif self.物品.图标 =="奖励" then
		 		tp.奖励图标:显示(self.x+15,self.y+1)
		 	end
		end
        -- if ( self.物品.类型 == "鞋子" or  self.物品.类型 == "腰带"  or  self.物品.类型 == "项链"  or  self.物品.类型 =="头盔"  or  self.物品.类型 == "衣服" or  self.物品.类型 == "武器") and self.物品.升星 then -- 判断装备药品

        --      tp.字体表.描边字体_:置颜色(GetColour(self.物品.升星)):显示(self.x + 25,self.y + 3,"+"..self.物品.升星)
        if self.物品.名称 == "召唤兽内丹"  or self.物品.名称 == "高级召唤兽内丹" or self.物品.名称 =="魔兽要诀" or self.物品.名称 =="高级魔兽要诀" or self.物品.名称 =="特殊魔兽要诀" then
            -- Picture.小黑图标:显示(self.x,self.y )
            tp.字体表.描边字体__:置颜色(0xFFFFFF00):显示(self.x+(50-tp.字体表.普通字体__:取宽度(self.物品.技能))/2,self.y +40,self.物品.技能)
            
        elseif     self.物品.类型 == "宝石" then 
            tp.字体表.描边字体__:置颜色(0xFFFFFF00):显示(self.x+(50-tp.字体表.普通字体__:取宽度(self.物品.等级.."级"))/2,self.y +40,self.物品.等级.."级")
        end
		if (总类 and 总类 ~= false) then
		  if type(总类)=="table" then
		  	local se = 0
		      for i=1,#总类 do
		      	if self.物品.类型 ~=总类[i] then
		      		se=se+1
		      	end
		      end
		      local sc =0
		      if 类别 then
		        for i=1,#类别 do
					if self.物品.名称 ~=类别[i] then
						sc=sc+1
					end
		        end
		      end
		      if se==#总类  then
		      	if 类别 then
		      		if sc==#类别  then
		      		   tp.物品格子禁止_:显示(self.x + 5,self.y + 6)
                       
		      		end
		      	else
		         tp.物品格子禁止_:显示(self.x + 5,self.y + 6)
                 
		     	end
		      end

          elseif 总类 == "法宝" then
          	-- if self.物品.佩戴 then
          	--    tp.物品格子禁止_:显示(self.x + 5,self.y + 6)
          	-- end

  
          elseif 总类 == "符石显示" then
            tp.符石边框:显示(self.x + 24,self.y + 24)
          elseif 总类 == "符石" then
          	 if self.物品.类型~="符石" and self.物品.名称~="符石卷轴" then
          	   tp.物品格子禁止_:显示(self.x + 5,self.y + 6)
               
          	end
           elseif 总类 == "法宝合成" then
          	 if  self.物品.名称 ~= "内丹" and self.物品.名称 ~= "千年阴沉木"and self.物品.名称 ~= "麒麟血" and self.物品.名称 ~= "玄龟板"and self.物品.名称 ~= "金凤羽" then
          	   tp.物品格子禁止_:显示(self.x + 5,self.y + 6)
               
          	end
          elseif 总类 == "战斗道具" then
          	 if self.物品.类型~="药品" and self.物品.类型~="暗器"  and self.物品.名称~="逍遥镜" and (self.物品.类型~="烹饪" and self.物品.分类~="酒" )then
          	   tp.物品格子禁止_:显示(self.x + 5,self.y + 6)
               
          	 end
		  end

			--tp.物品格子禁止_:显示(self.x + 5,self.y + 6)
		else
			if self.物品.数量  then
			tp.字体表.描边字体_:置颜色(0xFFFFFFFF):显示(self.x + 3,self.y + 3,self.物品.数量)
		   end
		end

	end
	if self.确定 then
		tp.物品格子确定_:显示(self.x+1,self.y+2)
	end

	if not tp.消息栏焦点 then
		if x>=self.x and x<=self.x+50 and y>=self.y and y<=self.y+50 and 条件 then
			tp.按钮焦点 = true
			tp.禁止关闭 = true
			if mouse(0)  then
				self.事件 = true
			elseif mouse(1)  then
				self.右键 = true
			end
			if self.格子显示 == nil then
				if abs == nil and  符石==nil   then
					tp.物品格子焦点_:显示(self.x+1,self.y+1)
				end
			end
			self.焦点 = true
		end
	end
end

function 系统类_物品格子:置坐标(x,y)
	self.x,self.y = x,y
end

return 系统类_物品格子