--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:51
--======================================================================--
local 系统类_队标格子 = class()
local tp,zt
local mouse = 引擎.鼠标弹起

function 系统类_队标格子:初始化(x,y,ID,注释,遮挡)
	self.ID = ID
	self.注释 = 注释
	self.物品 = nil
	self.事件 = false
	self.焦点 = false
	self.右键 = false
	self.遮挡 = 遮挡
	self.确定 = false
end

function 系统类_队标格子:置根(根)
	tp = 根
	zt = tp.字体表.描边字体
end

function 系统类_队标格子:置物品(物品)

	if 物品 ~= nil then
          self.物品 ={名称=物品}
          if 物品=="普通" then
              self.物品.模型 = tp.资源:载入('ZY.dll',"网易WDF动画",0x2231EBB4)
		elseif 物品=="玫瑰" then
			self.物品.模型 = tp.资源:载入('ZY.dll',"网易WDF动画",0xF8107DB0)
		elseif 物品=="扇子" then
			self.物品.模型 =tp.资源:载入('ZY.dll',"网易WDF动画",0x1B25FA29)
		elseif 物品=="音符队标" then
		self.物品.模型 =tp.资源:载入('ZY.dll',"网易WDF动画",0x25D20174)
		elseif 物品=="鸭梨队标" then
		self.物品.模型 =tp.资源:载入('ZY.dll',"网易WDF动画",0x7AEF08A1)
		elseif 物品=="心飞翔队标" then
		self.物品.模型 =tp.资源:载入('ZY.dll',"网易WDF动画",0x9BEF1016)


          end



	else
		self.物品 = nil
	end
end

function 系统类_队标格子:显示(dt,x,y)

	self.事件 = false
	self.焦点 = false
	self.右键 = false
	if self.物品 ~= nil then
		if self.遮挡 ~= nil then
			self.遮挡:显示(self.x-1+(xx or 0),self.y+4+(yy or 0))
		end
		self.物品.模型:显示(self.x+30,self.y+50)
		self.物品.模型:更新(dt)
	end
	if self.确定 then
		tp.队标格子确定_:显示(self.x+1,self.y+2)
	end


	if not tp.消息栏焦点 then
		if x>=self.x and x<=self.x+50 and y>=self.y and y<=self.y+50  then
			tp.按钮焦点 = true
			tp.禁止关闭 = true
			if mouse(0) then
				self.事件 = true
			elseif mouse(1) then
				self.右键 = true
			end
			tp.队标格子焦点_:显示(self.x+1,self.y+1)

			self.焦点 = true
		end
	end
end

function 系统类_队标格子:置坐标(x,y)
	self.x,self.y = x,y
end

return 系统类_队标格子