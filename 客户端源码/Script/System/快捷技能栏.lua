-- @Author: 作者QQ414628710
-- @Date:   2022-01-15 20:06:44
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-24 02:26:45
--======================================================================--
-- @作者: GGE研究群: 342119466
-- @创建时间:   2018-03-03 02:34:19
-- @Last Modified time: 2020-10-06 06:47:05
--======================================================================--
local 系统类_快捷技能栏 = class()
local tp
local mouseb = 引擎.鼠标按下
local keyb   = 引擎.按键弹起
local bw     = require("gge包围盒")(650,555,262,31)
local gl     = 引擎.置纹理过滤

function 系统类_快捷技能栏:初始化(根)
	local 资源 = 根.资源
	self.格子={}
	for i=1,8 do
		self.格子[i] = 资源:载入('JM.FT',"网易WDF动画",0x0493FA27)
	end
	tp = 根
	self.图片组 = {}
	self.操作员 = nil
	self.数据 = {}
end


function 系统类_快捷技能栏:显示(dt,x,y)
	if not tp.快捷技能显示 then
		return
	end
	local kj = (self.操作员 or self.数据).快捷技能
	for i=1,8 do
	    self.格子[i]:显示(417+i*33,535)
	    if kj[i] and kj[i].名称 then
	    	local xsd =  SkillData[kj[i].名称]
					kj[i].剩余冷却回合 = xsd.冷却
					kj[i].介绍=xsd.介绍
					kj[i].消耗说明=xsd.消耗
					kj[i].使用条件=xsd.条件
					kj[i].小模型 = tp.资源:载入(xsd.文件,"网易WDF动画",xsd.小图标)
	    	if self.图片组[i] == nil or self.图片组[i].名称 ~= kj[i].名称 then
	    		self.图片组[i] = tp.资源:载入(xsd.文件,"网易WDF动画",xsd.大图标)
			end
			self.图片组[i].精灵.xx = 0.65
		    self.图片组[i].精灵.xy = 0.615
		    self.图片组[i]:显示(420+i*33,539)
		    gl(self.图片组[i].精灵)
		    if self.图片组[i]:是否选中(x,y) and not tp.选中UI and not tp.消息栏焦点 then
		    	tp.提示:技能(x-130,y,kj[i],nil,true)

		    end
	    end
	    if self.格子[i]:是否选中(x,y) and not tp.选中UI and not tp.消息栏焦点 and not tp.战斗中 then
		    if mouseb(0) and tp.场景.抓取技能 ~= nil then
             客户端:发送数据(2075,{格子=i,主技能id= tp.场景.抓取技能.选中技能,包含技能id=tp.场景.抓取技能ID},1)
                tp.场景.抓取技能ID =nil
		    	tp.场景.抓取技能 =nil
		    elseif mouseb(0) and tp.场景.抓取技能 == nil and self.图片组[i]~= nil then
		    	tp.场景.抓取技能 =kj[i]
		    	客户端:发送数据(2075,{格子=i},1)
		    end
	    end
	    tp.字体表.描边字体:显示(425+i*33,519,"F"..i)
	end
	local ax = self:快捷焦点按下()
	if ax and kj[ax-1] ~= nil and kj[ax-1].名称 ~= nil and not tp.选中UI and not tp.消息栏焦点 and not tp.战斗中 then
		if (kj[ax-1].种类 == 0) then

		end
	end
end
function 系统类_快捷技能栏:快捷焦点按下()
	if keyb(KEY.F1) then
		return 1
	elseif keyb(KEY.F2) then
		return 2
	elseif keyb(KEY.F3) then
		return 3
	elseif keyb(KEY.F4) then
		return 4
	elseif keyb(KEY.F5) then
		return 5
	elseif keyb(KEY.F6) then
		return 6
	elseif keyb(KEY.F7) then
		return 7
	elseif keyb(KEY.F8) then
		return 8
	end
end
function 系统类_快捷技能栏:检查点(x,y)
	return tp.快捷技能显示 and bw:检查点(x,y)
end

return 系统类_快捷技能栏