--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:51
--======================================================================--
local 系统类_队伍格子 = class()
local bw = require("gge包围盒")
local zts
local tp;
local mouseb = 引擎.鼠标弹起
local zqj = 引擎.坐骑库

function 系统类_队伍格子:初始化(x,y,ID,根)
	self.x = x
	self.y = y+1
	self.ID = ID
	self.物品 = nil
	self.模型 = nil
	self.事件 = false
	self.焦点 = false
	self.禁止 = false
	self.包围盒  = bw(x,y,116,141)

end

function 系统类_队伍格子:置人物(人物)
	if 人物 ~= nil then
		if tp == nil then
			tp = 引擎.场景
			zts = tp.字体表.华康字体
		end
		if 人物.变身 == nil then
			self.武器 = nil
			self.坐骑 = nil
			self.坐骑饰品 = nil
			local mx = 人物.造型
			if 人物.锦衣数据.定制 then
				mx = 人物.锦衣数据.定制
		    elseif 人物.锦衣数据.战斗锦衣 then
			mx = 人物.锦衣数据.战斗锦衣
			elseif 人物.锦衣数据.锦衣 then
			mx = 人物.锦衣数据.锦衣.."_"..人物.造型
			end
			local n = 取模型(mx)
			self.模型 = tp.资源:载入(n.资源,"网易WDF动画",n.静立)
			if 人物.坐骑.类型 and not 人物.锦衣数据.定制 then
				local 资源组 = zqj(人物.造型,人物.坐骑.类型,人物.坐骑.饰品 or "空")
				self.模型 = tp.资源:载入(资源组.人物资源,"网易WDF动画",资源组.人物站立)
				self.坐骑 = tp.资源:载入(资源组.坐骑资源,"网易WDF动画",资源组.坐骑站立)
				if 资源组.坐骑饰品站立 ~= nil then
					self.坐骑饰品 = tp.资源:载入(资源组.坐骑饰品资源,"网易WDF动画",资源组.坐骑饰品站立)

					if 人物.坐骑.饰品染色组 then
					self.坐骑饰品:置染色(人物.坐骑.饰品,人物.坐骑.饰品染色组[1],人物.坐骑.饰品染色组[2],人物.坐骑.饰品染色组[3])
					end
					self.坐骑饰品:置方向(4)
				end
				if 人物.坐骑.染色组 then
					self.坐骑:置染色(人物.坐骑.类型,人物.坐骑.染色组[1],人物.坐骑.染色组[2],人物.坐骑.染色组[3])
				end
				self.坐骑:置方向(4)
			elseif  人物.武器数据.类别 ~= 0 and 人物.武器数据.类别 ~= "" and 人物.武器数据.类别 ~= nil and not 人物.锦衣数据.定制 then
				
       		    if 人物.锦衣数据.战斗锦衣 then
				 n = 取模型(人物.锦衣数据.战斗锦衣)
				self.模型 = tp.资源:载入(n.资源,"网易WDF动画",n.静立)
				elseif 人物.锦衣数据.锦衣 then
				 n = 取模型(人物.锦衣数据.锦衣.."_"..人物.造型)
				self.模型 = tp.资源:载入(n.资源,"网易WDF动画",n.静立)
			    else
			    n = 取模型(人物.造型,人物.武器数据.类别)
				self.模型 = tp.资源:载入(n.资源,"网易WDF动画",n.静立)
				end

				local m = tp:qfjmc(人物.武器数据.类别,人物.武器数据.等级,人物.武器数据.名称)


				local n = 取模型(m.."_"..人物.造型)
				self.武器 = tp.资源:载入(n.资源,"网易WDF动画",n.静立)


					  if 人物.武器数据.染色 ~= nil then
			self.武器:置染色(人物.武器数据.染色.染色方案,人物.武器数据.染色.染色组.a,人物.武器数据.染色.染色组.b)  


       end

				self.武器:置方向(4)
				--self.武器:置差异(self.武器.帧数-self.模型.帧数)
			end
			if 人物.染色方案 ~= nil and 人物.锦衣数据.锦衣==nil and 人物.锦衣数据.战斗锦衣==nil and 人物.锦衣数据.定制==nil then
				self.模型:置染色(DyeData[人物.造型][1],人物.染色.a,人物.染色.b,人物.染色.c)  
			end
			self.模型:置方向(4)
			self.人物 = 人物
		else
			local n = 取模型(人物.变身)
			self.模型 = tp.资源:载入(n.资源,"网易WDF动画",n.静立)
			self.模型:置方向(0)
			self.人物 = 人物
				if   取召唤兽武器(人物.变身) and 人物.变身 ~= nil then
				local ns=取模型("武器_"..人物.变身)
				self.召唤兽= tp.资源:载入(ns.资源,"网易WDF动画",ns.静立)
				self.召唤兽:置方向(0)
				end
		end
		if   人物.锦衣数据.光环 then
		local ns=引擎.取光环(人物.锦衣数据.光环)
		self.光环= tp.资源:载入(ns[4],"网易WDF动画",ns[1])
		end

	else
	    self.模型 = nil
		self.人物 = nil
	end
end

function 系统类_队伍格子:显示(dt,x,y,条件,队伍)
	self.事件 = false
	self.焦点 = false
	if self.模型 ~= nil and self.人物 ~= nil then
		if self.坐骑 ~= nil then
			self.坐骑:更新(dt)
			if self.坐骑饰品 ~= nil then
				self.坐骑饰品:更新(dt)
			end
		end
		if self.武器 ~= nil then
			self.武器:更新(dt)
		end
		if self.光环 then
			self.光环:更新(dt)
		end
		self.模型:更新(dt)
	end
	if 条件 and self.包围盒:检查点(x,y) and not tp.消息栏焦点 then
		tp.按钮焦点 = true
		tp.禁止关闭 = true
		self.焦点 = true
		if self.模型 ~= nil and self.人物 ~= nil then
			if mouseb(0) then
				self.事件 = true
			end
		end
		if not self.禁止 then
			tp.队伍格子焦点_:显示(self.x,self.y)
		end
	end
	if self.禁止 then
		tp.队伍格子确定_:显示(self.x,self.y)
	end

	if self.人物 ~= nil then
		if self.模型 == nil then
			self.武器 = nil
			self.坐骑 = nil
			self.坐骑饰品 = nil
        end
		zts:置颜色(-16777216)
	if 队伍 then
		zts:显示(self.x+7,self.y + 147,self.人物.名称)
		zts:显示(self.x+7,self.y + 171,self.人物.门派)
		zts:显示(self.x+7,self.y + 195,self.人物.等级.."级")
	else
		zts:显示(self.x + 18,self.y + 152,self.人物.名称)
		zts:显示(self.x + 18,self.y + 176,self.人物.门派)
		zts:显示(self.x + 18,self.y + 200,self.人物.等级.."级")
	end



	end
	if self.模型 ~= nil and self.人物 ~= nil then
		if self.光环 then
			self.光环:显示(self.x + 54,self.y + 107)
		end
		tp.影子:显示(self.x + 54,self.y + 107)
		if self.坐骑 ~= nil then
			self.坐骑:显示(self.x + 54,self.y + 107)
			if self.坐骑饰品 ~= nil then
				self.坐骑饰品:显示(self.x + 54,self.y + 107)
			end
		end
		self.模型:显示(self.x + 54,self.y + 107)
		if self.武器 ~= nil then
			self.武器:显示(self.x + 54,self.y + 107)
		elseif 	 self.召唤兽 ~= nil then
			self.召唤兽:显示(self.x + 54,self.y + 107)
		end

	end
end

function 系统类_队伍格子:释放()

end

function 系统类_队伍格子:置坐标(x,y)
	self.x = x
	self.y = y
	self.包围盒:置坐标(x,y)
end

return 系统类_队伍格子