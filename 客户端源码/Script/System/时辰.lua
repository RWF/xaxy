-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-24 22:28:02
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-11-02 00:18:35
--======================================================================--

local 系统类_时辰 = class()

local floor = math.floor
local ARGB = ARGB
local require = require
local 矩阵 = require("gge包围盒")(0,0,140,120)
local tp

local format = string.format
local keytq = 引擎.按键弹起

function 系统类_时辰:初始化(根)
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	local 资源 = 根.资源
	self.背景 = 资源:载入('JM.dll',"网易WDF动画",0xDE3F48B7)
	self.元神显示 = true
	self.挂机显示 = true
	self.VIP显示 = false
	self.充值显示 = false
	self.领双显示 = false

	 self.背景1 = 资源:载入('JM.dll',"网易WDF动画",0xF6A5602D)
	self.冒号 = 资源:载入('JM.dll',"网易WDF动画",0xB50234B8)
     self.日历 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xAD514E92),0,0,4)--1DB468B7
     self.人物 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x30712485),0,0,4)
     self.放大镜 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x6ECBCED7),0,0,4)
     self.罗盘 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xCA5242C2),0,0,4)
	self.当前昼夜=1

	self.VIP = 按钮.创建(资源:载入('80wl.dll',"网易WDF动画",0x01000073),0,0,4)
	self.充值 = 按钮.创建(资源:载入('80wl.dll',"网易WDF动画",0x14150000),0,0,4)
	
	self.元神 = 按钮.创建(资源:载入('80wl.dll',"网易WDF动画",0x10001000),0,0,4)
	self.介绍 = 按钮.创建(资源:载入('80wl.dll',"网易WDF动画",0xB96C0000),0,0,4)
	
	self.蓝宝 = 按钮.创建(资源:载入('80wl.dll',"网易WDF动画",0x99998888),0,0,4)
	self.小表 = 按钮.创建(资源:载入('80wl.dll',"网易WDF动画",0x99998889),0,0,4)
	self.红博 = 按钮.创建(资源:载入('80wl.dll',"网易WDF动画",0x99998891),0,0,4)
	self.领双 = 按钮.创建(资源:载入('80wl.dll',"网易WDF动画",0xFC5B0000),0,0,4)
	self.小猪 = 按钮.创建(资源:载入('80wl.dll',"网易WDF动画",0x99998890),0,0,4)

	self.回收 = 按钮.创建(资源:载入('80wl.dll',"网易WDF动画",0x88750000),0,0,4)
	
	self.挂机 = 按钮.创建(资源:载入('80wl.dll',"网易WDF动画",0x62AA0000),0,0,4)
	
	self.昼夜={
	[0]=资源:载入('JM.dll',"网易WDF动画",0xAA23B98F),
	[1]=资源:载入('月亮',"动画")
}
	self.时辰1 = {[1]=资源:载入('JM.dll',"网易WDF动画",0xB9FD3C98),--
	[2]=资源:载入('JM.dll',"网易WDF动画",0xFE026DC3),--
	[3]=资源:载入('JM.dll',"网易WDF动画",0xCDB4C444),--
	[4]=资源:载入('JM.dll',"网易WDF动画",0xDFE4105D),--
	[5]=资源:载入('JM.dll',"网易WDF动画",0x0A66B9C4),--
	[6]=资源:载入('JM.dll',"网易WDF动画",0x5A94EB4C),--
	[7]=资源:载入('JM.dll',"网易WDF动画",0xD2A6E5EC),--
	[8]=资源:载入('JM.dll',"网易WDF动画",0xEEADB7F5),--
	[9]=资源:载入('JM.dll',"网易WDF动画",0x399E1F40),--
	[10]=资源:载入('JM.dll',"网易WDF动画",0x6FFBDFD8),--
	[11]=资源:载入('JM.dll',"网易WDF动画",0xDEBA9F52),
	[12]=资源:载入('JM.dll',"网易WDF动画",0x49D3DE80)--
    }
    	self.数字 = {
	    [0]=资源:载入('JM.dll',"网易WDF动画",0x09B699D5),
	    [1]=资源:载入('JM.dll',"网易WDF动画",0x76384DE9),--
		[2]=资源:载入('JM.dll',"网易WDF动画",0x6DF385D0),--
		[3]=资源:载入('JM.dll',"网易WDF动画",0x303F2BE0),--
		[4]=资源:载入('JM.dll',"网易WDF动画",0x07A37CF4),--
		[5]=资源:载入('JM.dll',"网易WDF动画",0x5103AAD9),--
		[6]=资源:载入('JM.dll',"网易WDF动画",0x13E40D34),--
		[7]=资源:载入('JM.dll',"网易WDF动画",0xE2D75878),--
		[8]=资源:载入('JM.dll',"网易WDF动画",0x258183AE),--
		[9]=资源:载入('JM.dll',"网易WDF动画",0x4A864F94),--
    }




	self.白昼 = 资源:载入('JM.dll',"网易WDF动画",0x9DF6DEBC).精灵
	self.假人 = 资源:载入('JM.dll',"网易WDF动画",0xC7BEBF45)
	 self.假人1 =资源:载入('JM.dll',"网易WDF动画",2888856949.0)
	self.时辰 = {[1]=资源:载入('JM.dll',"网易WDF动画",0x361FA820),--
	[2]=资源:载入('JM.dll',"网易WDF动画",0xC0A66903),--
	[3]=资源:载入('JM.dll',"网易WDF动画",0xD1D11294),--
	[4]=资源:载入('JM.dll',"网易WDF动画",0xAA7DEB05),--
	[5]=资源:载入('JM.dll',"网易WDF动画",0x21274A87),
	[6]=资源:载入('JM.dll',"网易WDF动画",0x09C4978D),--
	[7]=资源:载入('JM.dll',"网易WDF动画",0xC9E2F072),--
	[8]=资源:载入('JM.dll',"网易WDF动画",0x2ACB36B2),--
	[9]=资源:载入('JM.dll',"网易WDF动画",0xC26BF189),--
	[10]=资源:载入('JM.dll',"网易WDF动画",0x1AA170AE),--
	[11]=资源:载入('JM.dll',"网易WDF动画",0x7921D3A3),--
	[12]=资源:载入('JM.dll',"网易WDF动画",0xEA7CAB84)--
    }
	-- 资源:载入('JM.dll',"网易WDF动画",0xC9E2F072)
	self.梦幻指引 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xF102F42D),0,0,4)
	self.收缩 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x6EDD4D71),0,0,4)
	self.灯笼 = {}
	for n=1,3 do
	   self.灯笼[n] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xBAF6A95D),0,0,2)
	end
	self.地图 = "地图"
	self.偏移 = nil
	self.白昼:置区域(self.计次,0,80,30)
	self.状态 = 0
	tp = 根
	self.当前时辰 = 1

end
function 系统类_时辰:显示(dt,x,y)
	self.偏移  = 50-string.len(tostring(tp.场景.地图名称)) *3.72
	if 矩阵:检查点(x,y) then
		tp.选中UI = true
	end


	self.收缩:更新(x,y)
	if self.收缩:事件判断() then
		if self.状态 == 0 then
			self.状态 = 1
			矩阵 = require("gge包围盒")(0,0,115,95)
			矩阵:置坐标(0,0)

		else
		    self.状态 = 0
		    矩阵 = require("gge包围盒")(0,0,140,78)
		    矩阵:置坐标(0,0)
		end
	end

	if self.状态 == 0 then
	    self.背景1:显示(0,0)
	    self.收缩:显示(10,50)
		self.罗盘:显示(30,43)
		self.放大镜:显示(65,42)
		self.人物:显示(95,43)
		self.日历 :显示(120,43)
	    self.日历 :更新(x,y)
	    self.人物:更新(x,y)
	    self.放大镜:更新(x,y)
	    self.罗盘:更新(x,y)
	    self.昼夜[self.当前昼夜]:显示(17,8)
	    self.时辰1[self.当前时辰]:显示(38,8)
		
		-- local miniico = {"VIP","充值","元神","介绍","蓝宝","小表","红博","领双","小猪"}
		-- 蓝宝 = 助战 小表 = 藏宝阁 "红博","小猪",没用
		local miniico = {"蓝宝","小表"} --"红博"
		if self.领双显示 then
			table.insert(miniico,"领双")
		end
		if self.挂机显示 then
			table.insert(miniico,"挂机")
		end
		if self.VIP显示 then
			table.insert(miniico,"VIP")
		end
		if self.充值显示 then
			table.insert(miniico,"充值")
		end
		if self.元神显示 then
			table.insert(miniico,"元神")
		end
		local aaaa,bbbb = 0,0
		for i = 1,#miniico do
			local mini_xy = {x=0,y=0}
			if miniico[i] == "领双" then
				mini_xy = {x=7,y=11}
			elseif miniico[i] == "挂机" then
				mini_xy = {x=13,y=10}
			elseif miniico[i] == "VIP" then
				mini_xy = {x=12,y=12}
			elseif miniico[i] == "元神" then
				mini_xy = {x=15,y=12}
			end
			self[miniico[i]]:更新(x,y)
			self[miniico[i]]:显示( aaaa*35+mini_xy.x,68 + bbbb * 40 + mini_xy.y)
			aaaa = aaaa + 1
			if aaaa >= 4 then
				aaaa = 0
				bbbb = bbbb + 1
			end
			
		end
		
		
	    local xy = "["..floor(tp.角色坐标.x/20)..","..floor(tp.角色坐标.y/20).."]"
	    tp.字体表.普通字体:置颜色(0xFFFFFFFF):显示(self.偏移,25 ,self.地图..xy)
	    local 小时,分钟,秒 = {},{},{}
	    小时.十位,小时.个位=math.modf(os.date("%H",全局时间)/10)
		分钟.十位,分钟.个位=math.modf(os.date("%M",全局时间)/10)
		秒.十位,秒.个位=math.modf(os.date("%S",全局时间)/10)
		self.数字[小时.十位]:显示(75,10)
		self.数字[floor(小时.个位*10)]:显示(83,10)
		self.冒号:显示(92,11)
 	    self.数字[分钟.十位]:显示(97,10)
		self.数字[floor(分钟.个位*10)]:显示(105,10)
		self.冒号:显示(114,11)
 	    self.数字[秒.十位]:显示(119,10)
		self.数字[floor(秒.个位*10)]:显示(127,10)
		if self.罗盘:事件判断() then
		 	tp.窗口.世界大地图:打开()
		elseif self.放大镜:事件判断() then
			tp.窗口.小地图:打开(tp.场景.数据.编号)
		elseif self.人物:事件判断() then
			客户端:发送数据(1, 1, 43, "P7")
		-- 蓝宝就是助战
		elseif self.蓝宝:事件判断() then
			local 表 = {参数=1,序号=2}
			--客户端:发送数据(1, 2, 108, table.tostring(表))
			客户端:发送数据(1, 2, 108, "")
		-- elseif self.红博:事件判断() and not tp.战斗中 then
		--  	客户端:发送数据(1,1,103)
		elseif self.日历:事件判断() then
		 	客户端:发送数据(21, 21, 5, "P7")
		elseif self.挂机:事件判断() then
			if tp.窗口.挂机系统.可视 then
				tp.窗口.挂机系统:打开()
			else
				发送数据(110,{动作=1})
			end
		elseif self.小表:事件判断()  and not tp.战斗中  then
			-- if tp.窗口.藏宝阁.可视 then
			-- 	tp.窗口.藏宝阁:打开()
			-- else
			--     发送数据(600)
			-- end
			客户端:发送数据(1,1,14,"银子")
		elseif self.元神:事件判断() then
			if tp.窗口.元神系统.可视 then
				tp.窗口.元神系统:打开()
			else
				发送数据(111)
			end
		end
    else

	    self.白昼:置区域(self.计次,0,80,30)
		self.白昼:显示(14,32 )
		self.背景:显示(0,0 )
		self.假人:更新(dt)
		self.假人1:更新(dt)
	    if tp.场景.人物.移动 == false then
			self.假人:显示(58,60)
		else
			self.假人1:显示(58,60)
		end
    	self.收缩:显示(3,64)
		for n=1,3 do
		   self.灯笼[n]:更新(x,y)
		   self.灯笼[n]:显示(93,17+((n-1)*18))
		end
		self.梦幻指引:更新(x,y)
		if self.梦幻指引:事件判断() and not tp.战斗中 then
				客户端:发送数据(21, 21, 5, "P7")
		end

		self.时辰[self.当前时辰]:显示(1.3,21.5 )
		tp.字体表.普通字体:显示(self.偏移-1,13.5 ,self.地图)
		local xy = "X:"..floor(tp.角色坐标.x/20).." Y:"..floor(tp.角色坐标.y/20)
		tp.字体表.普通字体:显示(floor(61 - tp.字体表.普通字体:取宽度(xy) / 2)-1.5,62,xy)
	end
	if (self.灯笼[1]:事件判断() or keytq(KEY.TAB)) and not tp.战斗中 and not tp.消息栏焦点 then
		tp.窗口.小地图:打开(tp.场景.数据.编号)
	elseif (self.灯笼[2]:事件判断() or keytq(KEY.TAB)) and not tp.战斗中 and not tp.消息栏焦点 then
		引擎.置全屏()
	end

	if self.人物:是否选中(x,y) then
		tp.提示:自定义(x+40,y+27,"排行榜")
	end
	if self.放大镜:是否选中(x,y) then
		tp.提示:自定义(x+40,y+27,"小地图")
	end
	if self.罗盘:是否选中(x,y) then
		tp.提示:自定义(x+40,y+27,"世界地图,快捷键:ALT+M")
	end
	if self.日历:是否选中(x,y) then
		tp.提示:自定义(x+40,y+27,"日常活动")
	end
	if self.VIP:是否选中(x,y) then
		tp.提示:自定义(x+40,y+27,"VIP福利")
	end
	if self.元神:是否选中(x,y) then
		tp.提示:自定义(x+40,y+27,"元神系统")
	end
	if self.挂机:是否选中(x,y) then
		tp.提示:自定义(x+40,y+27,"挂机系统")
	end
	if self.蓝宝:是否选中(x,y) then
		tp.提示:自定义(x+40,y+27,"助战系统")
	end
	if self.回收:是否选中(x,y) then
		tp.提示:自定义(x+40,y+27,"回收系统")
	end
	if self.小表:是否选中(x,y) then
		tp.提示:自定义(x+40,y+27,"藏宝阁")
	end
	if self.领双:是否选中(x,y) then
		tp.提示:自定义(x+40,y+27,"多倍系统")
	end
end



function 系统类_时辰:检查点(x,y)
	if  矩阵:检查点(x,y) then
		return  true
	end
end

return 系统类_时辰