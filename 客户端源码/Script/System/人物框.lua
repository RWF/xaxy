-- @Author: 作者QQ381990860
-- @Date:   2021-08-13 19:47:23
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2023-08-12 22:30:52
--======================================================================--
local 系统类_人物框 = class()
local keyaz = 引擎.按键按住
local keyax = 引擎.按键按下
local floor = math.floor
local ceil = math.ceil

local tp
local insert = table.insert
local remove = table.remove
local mouseb = 引擎.鼠标弹起
local format = string.format

local min = math.min

local 矩阵 = require("gge包围盒")(全局游戏宽度-230,1,230,52)
local 按钮

function 系统类_人物框:刷新角色头像()
	local s
  		if tp.场景.人物.数据.变身~=nil then
			s = ModelData[tp.场景.人物.数据.变身]
		else
			s = ModelData[tp.场景.人物.数据.造型]
		end
		self.人物头像背景[1] = 按钮(tp.资源:载入(s.头像资源,"网易WDF动画",s.中头像),0,0,1)
end
function 系统类_人物框:初始化(根)
	tp = 根
	local 资源 = 根.资源
	 按钮 = 根._按钮
	self.背景 = 资源:载入('JM.dll',"网易WDF动画",0x2E8758EE)
	self.气血 = 资源:载入('JM.dll',"网易WDF动画",0xAAD44583)
	self.魔法 = 资源:载入('JM.dll',"网易WDF动画",0xCE4D3C2D)
	self.经验 = 资源:载入('JM.dll',"网易WDF动画",0x7B3C08E4)
	self.愤怒 = 资源:载入('JM.dll',"网易WDF动画",0xBAF8009F)
	self.气血底图 = 资源:载入('JM.dll',"网易WDF动画",0x008C2611)
	self.记忆角色 = {}
	self.人物头像背景 = {}
	self.小图标 = {资源:载入('JM.dll',"网易WDF动画",0x24901650),资源:载入('JM.dll',"网易WDF动画",0x09E2B01B)}
	self.图标组 = {}
	self.焦点 = false
	 self.队伍数据={}
	   self.bb数据 ={}
   self.双倍动画 = 资源:载入('JM.dll',"网易WDF动画",0x19B90B9D)
   self.攻击动画 = 资源:载入('JM.dll',"网易WDF动画",0x4A5DD98C)
   self.交易动画 = 资源:载入('JM.dll',"网易WDF动画",0xABB8DCAA)
   self.铃铛动画 = 资源:载入('JM.dll',"网易WDF动画",0x397B8CF6)
   self.红丹动画 = 资源:载入('JM.dll',"网易WDF动画",0x24901650)
   self.盾牌动画 = 资源:载入('JM.dll',"网易WDF动画",0x8C6D00DC)
   self.蓝丹动画 = 资源:载入('JM.dll',"网易WDF动画",0x09E2B01B)
end
function 系统类_人物框:添加队伍(内容)
 self.队伍数据={}
	self.队伍数据 = {}
	self.临时数据 = 内容
	if self.临时数据 ~= nil and self.临时数据 ~= "66" then
		for n = 1, #self.临时数据, 1 do

			self.队伍数据[n] = self.临时数据[n]
             local x
			 if self.队伍数据[n].变身  then
				 x =self.队伍数据[n].变身

				
			else
				 x = self.队伍数据[n].造型
				
			 end
			 self.人物头像背景[n+1] = 按钮(tp.资源:载入(ModelData[x].头像资源,"网易WDF动画",ModelData[x].中头像),0,0,1)
		end
	end

 end


function 系统类_人物框:刷新召唤兽气血(数据)
	self.临时数据 = 数据
	if self.bb数据 ~= "0" then
		self.bb数据.当前气血 = self.临时数据.当前气血
		self.bb数据.气血上限 = self.临时数据.气血上限
		self.bb数据.当前魔法 = self.临时数据.当前魔法
		self.bb数据.魔法上限 = self.临时数据.魔法上限
	end
end
function 系统类_人物框:更新气血数据(数据)

       self.数据=数据


 end
function 系统类_人物框:刷新召唤兽头像(数据)
  local 资源 = tp.资源
  self.bb数据=数据

  if self.bb数据=="0" then
   self.bb头像=nil
  else
   if self.bb数据.造型~=nil then
   	-- print(self.bb数据.造型)
        local n =  ModelData[self.bb数据.造型]
         self.bb头像 = tp.资源:载入(n.头像资源,"网易WDF动画",n.小头像)
 end
end
end

function 系统类_人物框:加入小图标信息(方式,提示信息,变量)
	for n=1,#self.图标组 do
		if self.图标组[n][4] == 方式 then
			return
		end
	end
	insert(self.图标组,{self.小图标[方式],提示信息,变量,方式})
end

function 系统类_人物框:删除小图标信息(方式)
	for n=1,#self.图标组 do
		if self.图标组[n][4] == 方式 then
			remove(self.图标组,n)
			break
		end
	end
end

function 系统类_人物框:显示(dt,x,y)
	self.焦点 = false

	if self.队伍数据 ~=nil and not tp.战斗中 then
		for i=1,#self.队伍数据 do
			tp.人物头像背景_:显示(全局游戏宽度-245-i*51,2)
			self.人物头像背景[i+1]:更新(x,y)
			self.人物头像背景[i+1]:显示(全局游戏宽度-245-i*51+3,5,true)
			if self.人物头像背景[i+1]:事件判断() and not tp.战斗中 and self.队伍数据[i].id ~= tp.场景.人物.数据.id then

				tp.窗口.玩家小窗口:打开(self.队伍数据[i],x,y)
			end
		end
		for i=1,#self.队伍数据 do
			if self.人物头像背景[i+1]:是否选中(x,y) then
				tp.提示:队伍(x,y,self.队伍数据[i])
				self.焦点 = true
				break
			end
		end
	end
    tp.人物头像背景_:显示(全局游戏宽度-120,0)
	self.背景:显示(全局游戏宽度-68,0)
	self.背景:显示(全局游戏宽度-68,13)
	self.背景:显示(全局游戏宽度-68,26)
	self.背景:显示(全局游戏宽度-68,39)
	self.气血:置区域(0,0,min(floor(self.数据.当前气血 / self.数据.最大气血 * 50),50),8)
	self.魔法:置区域(0,0,min(floor(self.数据.当前魔法 / self.数据.魔法上限 * 50),50),8)
	self.愤怒:置区域(0,0,min(floor(self.数据.愤怒 / 150 * 50),50),8)
	self.经验:置区域(0,0,min(floor(self.数据.当前经验 / self.数据.升级经验 * 50),50),8)

    self.人物头像背景[1]:更新(x,y)
	if self.人物头像背景[1]:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.W)) and not tp.消息栏焦点) and not tp.战斗中  then
			客户端:发送数据(0,1,5,0,1)
			-- 客户端:发送数据(99,99,99,"S7")
			-- 客户端:发送数据(102,102,102,"S7")
			-- 客户端:发送数据(5,5,4)
  end



	self.人物头像背景[1]:显示(全局游戏宽度-117,3,true)

	self.气血:显示(全局游戏宽度-56,3)
	self.魔法:显示(全局游戏宽度-56,16)
	self.愤怒:显示(全局游戏宽度-56,29)
	self.经验:显示(全局游戏宽度-56,42)

	if self.气血:是否选中(x,y) and not tp.选中UI then
		tp.提示:自定义(x-42,y+27,format("#Y/气血：%d/%d/%d",self.数据.当前气血,self.数据.气血上限,self.数据.最大气血))
		if mouseb(1) and not tp.战斗中 and not tp.消息栏焦点 then
			客户端:发送数据(88,34,13,"S7")
		end
	elseif self.魔法:是否选中(x,y) and not tp.选中UI then
		tp.提示:自定义(x-42,y+27,format("#Y/魔法：%d/%d",self.数据.当前魔法,self.数据.魔法上限))
		if mouseb(1) and not tp.战斗中 and not tp.消息栏焦点 then
			客户端:发送数据(88,35,13,"S7")
		end
	elseif self.愤怒:是否选中(x,y) and not tp.选中UI then
		tp.提示:自定义(x-42,y+27,format("#Y/愤怒：%d/150",self.数据.愤怒))
	elseif self.经验:是否选中(x,y) and not tp.选中UI then
		tp.提示:自定义(x-42,y+27,"#Y/经验："..self.数据.当前经验.."/"..self.数据.升级经验)
	end
	local xx = 0
	local yy = 0
	for n=1,#self.图标组 do
		self.图标组[n][1]:显示(682+xx*30,58)
		if self.图标组[n][1]:是否选中(x,y) and not tp.选中UI then
			tp.提示:自定义(x-42,y+27,format("#Y/%s%d",self.图标组[n][2],tp.剧情进度[self.图标组[n][3]]))
		end
		xx = xx + 1
	end
	tp.宠物头像背景_:显示(全局游戏宽度-230,0)
	self.背景:显示(全局游戏宽度-190,0)
	self.背景:显示(全局游戏宽度-190,13)
	self.背景:显示(全局游戏宽度-190,26)
	local cz = self.bb数据
	if self.bb头像 ~= nil then
		if self.记忆角色[7] ~= cz.造型 then
			self.宝宝头像背景 = 按钮(tp.资源:载入(ModelData[cz.造型].头像资源,"网易WDF动画",ModelData[cz.造型].小头像),0,0,1)
			self.记忆角色[7] = cz.造型
		end
		self.气血:置区域(0,0,min(floor(cz.当前气血 / cz.气血上限 * 50),50),8)
		self.魔法:置区域(0,0,min(floor(cz.当前魔法 / cz.魔法上限 * 50),50),8)
		self.经验:置区域(0,0,min(floor(cz.当前经验 / cz.升级经验 * 50),50),8)
		self.气血底图:显示(全局游戏宽度-178,3)
		self.气血:显示(全局游戏宽度-178,3)
		self.魔法:显示(全局游戏宽度-178,16)
		self.经验:显示(全局游戏宽度-178,29)
		if self.气血:是否选中(x,y) and not tp.选中UI then
			tp.提示:自定义(x-42,y+27,format("#Y/气血：%d/%d",cz.当前气血,cz.气血上限))
			if mouseb(1) and not tp.战斗中 and not tp.消息栏焦点 then
				客户端:发送数据(88,32,13,"S7")
			end
		elseif self.魔法:是否选中(x,y) and not tp.选中UI then
			tp.提示:自定义(x-42,y+27,format("#Y/魔法：%d/%d",cz.当前魔法,cz.魔法上限))
			if mouseb(1) and not tp.战斗中 and not tp.消息栏焦点 then
					客户端:发送数据(88,33,13,"S7")
			end
		elseif self.经验:是否选中(x,y) and not tp.选中UI then
			tp.提示:自定义(x-42,y+27,format("#Y/经验：%d/%d",cz.当前经验,cz.升级经验))
		end
	else
		if self.记忆角色[7] ~= false then
			self.宝宝头像背景 = 按钮(tp.资源:载入("JM.dll","网易WDF动画",0xCEC838D7),0,0,1)
			self.记忆角色[7] = false
		end
	end
	self.宝宝头像背景:更新(x,y)
	if self.宝宝头像背景:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.O)) and not tp.消息栏焦点) and not tp.战斗中 then

		
	   if tp.窗口.召唤兽属性栏.可视 then
          tp.窗口.召唤兽属性栏:打开()
        else  
            客户端:发送数据(6, 1, 22, "6A", 1)
        end
	end
	self.宝宝头像背景:显示(全局游戏宽度-227,3,true)
	if #tp.窗口.任务追踪.数据 > 0 then
		local 偏移= 0
		for i=1,#tp.窗口.任务追踪.数据 do

			if tp.窗口.任务追踪.数据[i].名称=="双倍经验" then
			     self.双倍动画:显示(全局游戏宽度-120+偏移,58)
				if  self.双倍动画:是否选中(x,y) then
				tp.提示:自定义(x-42,y+27,tp.窗口.任务追踪.数据[i].说明)
				end
				偏移 = 偏移 +27
		    end
		    if tp.窗口.任务追踪.数据[i].名称=="摄妖香" then
			     self.铃铛动画:显示(全局游戏宽度-120+偏移,58)
				if  self.铃铛动画:是否选中(x,y) then
				tp.提示:自定义(x-42,y+27,tp.窗口.任务追踪.数据[i].说明)
				end
				偏移 = 偏移 +27
		    end
		    if tp.窗口.任务追踪.数据[i].名称=="秘制红罗羹" then
			     self.红丹动画:显示(全局游戏宽度-120+偏移,58)
				if  self.红丹动画:是否选中(x,y) then
				tp.提示:自定义(x-42,y+27,tp.窗口.任务追踪.数据[i].说明)
				end
				偏移 = 偏移 +27
		    end
		end

	end

end



function 系统类_人物框:检查点(x,y)
	return 矩阵:检查点(x,y) or self.焦点
end

return 系统类_人物框









