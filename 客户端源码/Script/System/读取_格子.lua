-- @Author: 作者QQ414628710
-- @Date:   2022-01-15 20:06:44
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-03-20 22:59:33
--======================================================================--
-- 这个不是系统类，只有角色选择画面使用,不应该放这里
--======================================================================--
local 系统类_读取格子 = class()


local insert = table.insert
local tp = nil
local tonumber = tonumber
local pairs = pairs
local tq  = 引擎.鼠标弹起
local p1_x
local p1_y
local p2_x
local p2_y

function 系统类_读取格子:初始化(x,y,编号)
	self.x = x
	self.y = y
	self.事件 = false
	self.焦点 = false
	self.开启 = false
	self.编号 = 编号
	self.双击 = nil
	self.时间 = 0
	if 全局游戏宽度==800 then
		p1_x = 115
		p1_y = 515
		p2_x = 350
		p2_y = 550
	else
		p1_x = 122
		p1_y = 660
		p2_x = 360
		p2_y = 692
	end
end

function 系统类_读取格子:置档位(档位,根)
	tp = 根
  self.人物信息 =档位
		local s =   ModelData[档位.造型]
		local q = 取模型(档位.造型)
		self.人物头像 = 根.资源:载入('MAX.7z',"网易WDF动画",s.头像组.人物)
		self.头像 = tp.资源:载入('MAX.7z',"网易WDF动画",s.头像组.头像)
       if 档位.武器数据.类别 ~= 0 and 档位.武器数据.类别 ~= "" then
		local v = 档位.武器数据.类别
		if 档位.武器数据.名称 == "龙鸣寒水" or 档位.武器数据.名称 == "非攻" then
			v = "弓弩1"
		end
		if 档位.锦衣数据.锦衣 then
			q = 取模型(档位.锦衣数据.锦衣.."_"..档位.造型,v)
		else
		 	q = 取模型(档位.造型,v)
		end
			self.人物模型 = 根.资源:载入(q.资源,"网易WDF动画",q.静立)
			local m = 根:qfjmc(档位.武器数据.类别,档位.武器数据.等级,档位.武器数据.名称)
			local x = 取模型(m.."_"..档位.造型,nil)
			self.人物武器 = 根.资源:载入(x.资源,"网易WDF动画",x.静立)
			--self.人物武器:置差异(self.人物武器.帧数-self.人物模型.帧数)

			if 档位.武器数据.染色 ~= nil then
			self.人物武器:置染色(档位.武器数据.染色.染色方案,档位.武器数据.染色.染色组.a,档位.武器数据.染色.染色组.b)   
			end
		else
			self.人物模型 = 根.资源:载入(q.资源,"网易WDF动画",q.静立)
		end

		self.模型 = 档位.造型



		if 档位.染色方案 ~= nil and 档位.锦衣数据.锦衣==nil then
			self.人物模型:置染色(DyeData[档位.造型][1],档位.染色.a,档位.染色.b,档位.染色.c)  
		end

		self.人物模型:置方向(4)
		if self.人物武器 ~= nil then
			self.人物武器:置方向(4,true)
		end

end

function 系统类_读取格子:更新(dt,x,y)
	if self.人物模型  then
		self.人物模型:更新(dt)
		if self.人物武器  then
			self.人物武器:更新(dt)
		end
	end
	self.事件 = false
	if self.头像 ~= nil and self.头像:是否选中(x,y)  then
		self.头像:置高亮()
		-- if self.人物武器 ~= nil then
		-- 	self.人物武器:置高亮()
		-- end
		if tq(0) then
			if self.双击 == nil then
				self.事件 = true
				self.双击 = 1
			else
			    self.事件 = nil
			    self.载入 = 1
			end
		end
		if self.双击 == 1 then
			self.时间 = self.时间 + 1
			if self.时间 >= 16 then
				self.双击 = nil
				self.时间 = 0
			end
		end
	else
		if self.头像 ~= nil then
			self.头像:取消高亮()
			-- if self.人物武器 ~= nil then
			-- 	self.人物武器:取消高亮()
			-- end
		end
	end
end


function 系统类_读取格子:显示()
	self.开启 = false
	if self.人物信息 ~= nil  then
		self.头像:显示(self.x*scale_w,self.y*scale_h)
		tp.字体表.华康字体:置颜色(0xFF8DF2A5):显示((self.x+57)*scale_w,(self.y+10)*scale_h,self.人物信息.名称)
		tp.字体表.华康字体:置颜色(0xFFFFFFFF):显示((self.x+57)*scale_w,(self.y+30)*scale_h,self.人物信息.门派.." "..self.人物信息.等级)

		if tp.选择 ~= nil and tp.选择.选中 == self.编号 then
			tp.影子:显示(120*scale_w,450*scale_h)
			self.人物模型:显示(120*scale_w,450*scale_h)
			if self.人物武器 ~= nil then
				self.人物武器:显示(120*scale_w,450*scale_h)
			end
			self.开启 = true
			self.人物头像:显示(130*scale_w,120*scale_h)
			tp.字体表.华康字体:置颜色(4294967295)
			tp.字体表.华康字体:显示(p1_x,p1_y,self.人物信息.名称)
			tp.字体表.华康字体:显示(p2_x,p1_y,self.人物信息.id)
			tp.字体表.华康字体:显示(p1_x,p2_y,self.人物信息.等级)
			tp.字体表.华康字体:显示(p2_x,p2_y,self.人物信息.门派)
			tp.光环:显示(120*scale_w,460*scale_h)
			--self.人物头像:显示(145+(全局游戏宽度-800)/2,366)
		end

		if self.载入 ~= nil then
			 if 全局时间-tp.选择.发送时间>=5 then
			 	tp.选择.发送时间=全局时间
				self.发送信息 = {账号=游戏账号,密码=游戏密码,编号=self.编号}
				客户端:发送(0,cjson.encode(self.发送信息))
			 else
               tp.提示:写入 ("请稍后再尝试！")
			 end
			 self.载入 = nil
		end
	end
end



return 系统类_读取格子