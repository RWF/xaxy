-- @Author: 作者QQ414628710
-- @Date:   2022-01-15 20:06:44
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-24 02:18:11
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:50
--======================================================================--
local 系统类_底图框 = class()
-- UI层的box包围盒
local 矩阵 = require("gge包围盒")(0,全局游戏高度-36,全局游戏宽度,38);
local tp;
local keyaz = 引擎.按键按住
local keyax = 引擎.按键按下
local keytq = 引擎.按键弹起
local KEY = KEY

function 系统类_底图框:初始化(根)
	local 按钮 = 根._按钮
	local 资源 = 根.资源
	self.UI_底图 = 资源:载入('JM.dll',"网易WDF动画",0x3D1FA249)
	self.UI_攻击 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x6BBC42FA),0,0,4,true)
	self.UI_道具 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x0E53F705),0,0,4,true)
	self.UI_给予 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x7E4DE3DE),0,0,4,true)
	self.UI_交易 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xCAB0B8B4),0,0,4,true)
	self.UI_队伍 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x0D3EA20B),0,0,4,true)
	self.UI_宠物 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x187ABFC8),0,0,4,true)
	self.UI_任务 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xA15292B2),0,0,4,true)
	self.UI_帮派 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xC35B2EC3),0,0,4,true)
	self.UI_快捷 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xBB6E607E),0,0,4,true)
	self.UI_好友 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x7C7A64D9),0,0,4,true)
	self.UI_成就 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x8B3AADDA),0,0,4,true)
	self.UI_动作 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x548156A0),0,0,4,true)
	self.UI_系统 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x5116F7DF),0,0,4,true)
	self.UI_梦幻 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x61D49AEC),0,0,1,true,nil,true)
	self.UI_抽奖 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x16E3D74E),0,0,4,true)
	-- self.UI_仙玉抽奖 = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xAD57E6C0),0,0,4,true)
	self.好友闪烁 =资源:载入('JM.dll',"网易WDF动画",0x6A062464)
   self.精灵边框=资源:载入('7B70D9BA',"动画") 
  -- self.成就边框 =资源:载入('05F00D01',"动画")
	self.队伍闪烁 =资源:载入('JM.dll',"网易WDF动画",0x187AD420)
	tp = 根

end

function 系统类_底图框:显示(dt,x,y)
	self.UI_攻击:更新(x,y,not tp.战斗中)
	self.UI_道具:更新(x,y,not tp.战斗中)
	self.UI_给予:更新(x,y,not tp.战斗中)
	self.UI_交易:更新(x,y,not tp.战斗中)
	self.UI_队伍:更新(x,y,not tp.战斗中)
	self.UI_宠物:更新(x,y,not tp.战斗中)
	self.UI_任务:更新(x,y,not tp.战斗中)
	self.UI_帮派:更新(x,y,not tp.战斗中)
	self.UI_快捷:更新(x,y,not tp.战斗中)
	self.UI_成就:更新(x,y,not tp.战斗中)
	self.UI_动作:更新(x,y,not tp.战斗中)
	self.UI_系统:更新(x,y,not tp.战斗中)
	self.UI_梦幻:更新(x,y,not tp.战斗中)
    self.UI_抽奖:更新(x,y,not tp.战斗中)
    -- self.UI_仙玉抽奖:更新(x,y,not tp.战斗中)
	if not tp.战斗中 then
		if self.UI_攻击:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.A)) and not tp.消息栏焦点) then
			 tp.鼠标.置鼠标("攻击")
		     tp.pk开关 = true
		elseif self.UI_道具:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.E)) and not tp.消息栏焦点)then
				if tp.窗口.道具行囊.可视 then
					tp.窗口.道具行囊:打开()
				else
					客户端:发送数据(1, 1, 13, "9", 1)
				end
		elseif self.UI_给予:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.G)) and not tp.消息栏焦点) then
			tp.给予开关 = true
			 tp.鼠标.置鼠标("给予")
		elseif self.UI_交易:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.X)) and not tp.消息栏焦点)  then
			tp.交易开关 = true
			tp.鼠标.置鼠标("交易")
		-- elseif 引擎.按键弹起(KEY.F4) then
		-- 	客户端:发送数据(854, 24, 43, "14", 1)
	 --    elseif 引擎.按键弹起(KEY.F8) then
		-- 	客户端:发送数据(50, 50, 13, "52", 1)
		elseif self.UI_队伍:事件判断() or (self.队伍闪烁:是否选中(x,y) and 引擎.鼠标弹起(0)) or ((keyaz(KEY.ALT) and keyax(KEY.T)) and not tp.消息栏焦点)  then
				if tp.窗口.队伍栏.可视 then
					tp.窗口.队伍栏:打开()
				else
					客户端:发送数据(0, 1, 11, "66", 1)
				end
		elseif self.UI_宠物:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.P)) and not tp.消息栏焦点)  then
			 客户端:发送数据(1,1,52,"cw")
		elseif self.UI_任务:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.Q)) and not tp.消息栏焦点) then
			客户端:发送数据(12, 26, 25, "93", 1)
		elseif 引擎.按键按住(KEY.ALT) and 引擎.按键弹起(13) then
			引擎.置全屏()
		elseif (keyaz(KEY.ALT) and keyax(KEY._1)) then
                 客户端:发送数据(1,6, 4, "P7")
         elseif(keyaz(KEY.ALT) and keyax(KEY._2)) then
                客户端:发送数据(2,6, 4, "P7")
          elseif(keyaz(KEY.ALT) and keyax(KEY._3)) then
		客户端:发送数据(3,6, 4, "P7")
		elseif(keyaz(KEY.ALT) and keyax(KEY._4)) then
		客户端:发送数据(4,6, 4, "P7")
		elseif(keyaz(KEY.ALT) and keyax(KEY._5)) then
		客户端:发送数据(5,6, 4, "P7")
		elseif(keyaz(KEY.ALT) and keyax(KEY._6)) then
		客户端:发送数据(6,6, 4, "P7")
		elseif(keyaz(KEY.ALT) and keyax(KEY._7)) then
		客户端:发送数据(7,6, 4, "P7")
		elseif(keyaz(KEY.ALT) and keyax(KEY._8)) then
		客户端:发送数据(8,6, 4, "P7")
		elseif(keyaz(KEY.ALT) and keyax(KEY._9)) then
		客户端:发送数据(9,6, 4, "P7")
		elseif(keyaz(KEY.ALT) and keyax(KEY._10)) then
		客户端:发送数据(10,6, 4, "P7")
		elseif(keyaz(KEY.ALT) and keyax(KEY._11)) then
		客户端:发送数据(11,6, 4, "P7")
		elseif(keyaz(KEY.ALT) and keyax(KEY._12)) then
		客户端:发送数据(12,6, 4, "P7")


		elseif self.UI_快捷:事件判断() or ((keyaz(KEY.ALT) and keyax(KEY.C)) and not tp.消息栏焦点) then
	       if tp.快捷技能显示 then
           tp.快捷技能显示 =false
         else
           tp.快捷技能显示 =true
           客户端:发送数据(1, 1, 55, "kjl", 1)
       end
		elseif self.UI_好友:事件判断()or (self.好友闪烁:是否选中(x,y) and 引擎.鼠标弹起(0))  or (keyaz(KEY.ALT) and keyax(KEY.F)) then
			if 消息开关 then
			   客户端:发送数据(51, 51, 13, "9s", 1)

			elseif 消息闪烁 then
			 客户端:发送数据(1, 26, 5, "9s")
			else
				if tp.窗口.好友列表.可视 then
					tp.窗口.好友列表:打开()
				else
				  客户端:发送数据(1, 1, 54, "9s")
				end

			end


		elseif 引擎.按键弹起(KEY.F9) then
			玩家屏蔽 = not 玩家屏蔽
		elseif 引擎.按键弹起(KEY.F10) then
			摊位屏蔽 = not 摊位屏蔽
        elseif self.UI_帮派:事件判断() or 引擎.按键按住(KEY.ALT) and 引擎.按键弹起(KEY.B) then
		    客户端:发送数据(1, 61, 13, "9", 1)
		elseif self.UI_动作:事件判断() or (keyaz(KEY.ALT) and keyax(KEY.D) and not tp.消息栏焦点 ) then
			tp.窗口.动作界面:打开()
		elseif self.UI_系统:事件判断() or ((引擎.按键弹起(KEY.ESCAPE)) and not tp.消息栏焦点) or ((keyaz(KEY.ALT) and keyax(KEY.S)) and not tp.消息栏焦点) then
			tp.窗口.系统设置:打开()
		elseif self.UI_梦幻:事件判断() or ((引擎.按键弹起(KEY.F12)) and not tp.消息栏焦点)  then
			  客户端:发送数据(1,1,14,"银子")
	   	elseif self.UI_抽奖:事件判断() then
			  tp.窗口.抽奖:打开()
	   	-- elseif self.UI_仙玉抽奖:事件判断() then
			  -- --tp.窗口.仙玉抽奖:打开()
			  -- 客户端:发送数据(223,223,13,"52")
	   	elseif self.UI_成就:事件判断() then
			  tp.窗口.拓展功能:打开()

		end
        if self.UI_攻击:是否选中(x,y) then
        	tp.提示:自定义(x+40,y,"快捷键:ALT+A")
		elseif	self.UI_道具:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+E")
		elseif	self.UI_给予:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+G")
		elseif	self.UI_交易:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+X")
		elseif	self.UI_队伍:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+T")
		elseif	self.UI_宠物:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+O")
		elseif	self.UI_任务:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+Q")
		elseif	self.UI_帮派:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+B")
		elseif	self.UI_快捷:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+C")
		elseif	self.UI_好友:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+F")
		elseif	self.UI_成就:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+N")
		elseif	self.UI_动作:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+D")
		elseif	self.UI_系统:是否选中(x,y) then
			tp.提示:自定义(x+40,y,"快捷键:ALT+S")
		elseif	self.UI_梦幻:是否选中(x,y) then
			tp.提示:自定义(x+40,y+20,"霓裳宝阁欢迎您！快捷键F12")
		elseif	self.UI_抽奖:是否选中(x,y) then
			tp.提示:自定义(x+40,y+20,"来一发吧,单车变摩托")
		-- elseif	self.UI_仙玉抽奖:是否选中(x,y) then
		-- 	tp.提示:自定义(x+40,y+20,"仙玉抽奖，走上人生巅峰")
        end
	end

	self.UI_底图:显示(全局游戏宽度-350,全局游戏高度-63)
	self.UI_攻击:显示(全局游戏宽度-346,全局游戏高度-34,true,nil,nil,nil,nil,nil,nil,nil,true,"攻击")
	self.UI_道具:显示(全局游戏宽度-320,全局游戏高度-34,true,nil,nil,nil,nil,nil,nil,nil,true,"道具")
	self.UI_给予:显示(全局游戏宽度-294,全局游戏高度-34,true,nil,nil,nil,nil,nil,nil,nil,true,"给予")
	self.UI_交易:显示(全局游戏宽度-269,全局游戏高度-34,true,nil,nil,nil,nil,nil,nil,nil,true,"交易")


	if 队伍消息 then
		self.队伍闪烁:更新(dt)
		self.队伍闪烁:显示(全局游戏宽度-243,全局游戏高度-38,not tp.战斗中)
	else
	 self.UI_队伍:显示(全局游戏宽度-243,全局游戏高度-34,true,nil,nil,nil,nil,nil,nil,nil,true,"队伍")
	 self.UI_队伍:更新(x,y,not tp.战斗中)
	end
	self.UI_宠物:显示(全局游戏宽度-218,全局游戏高度-34,true,nil,nil,nil,nil,nil,nil,nil,true,"宠物")
	self.UI_任务:显示(全局游戏宽度-191,全局游戏高度-34,true,nil,nil,nil,nil,nil,nil,nil,true,"任务")
	self.UI_帮派:显示(全局游戏宽度-164,全局游戏高度-34,true,nil,nil,nil,nil,nil,nil,nil,true,"帮派")
	self.UI_快捷:显示(全局游戏宽度-137,全局游戏高度-34,true,nil,nil,nil,nil,nil,nil,nil,true,"快捷")
	if 消息开关 or 消息闪烁 then
		self.好友闪烁:更新(dt)
		self.好友闪烁:显示(全局游戏宽度-110,全局游戏高度-34)
	else
	 self.UI_好友:显示(全局游戏宽度-110,全局游戏高度-34,true,nil,nil,nil,nil,nil,nil,nil,true,"好友")
	 self.UI_好友:更新(x,y,not tp.战斗中)
	end
	self.UI_成就:显示(全局游戏宽度-81,全局游戏高度-31,true,nil,nil,nil,nil,nil,nil,nil,true,"功能")
	self.UI_动作:显示(全局游戏宽度-53,全局游戏高度-34,true,nil,nil,nil,nil,nil,nil,nil,true,"动作")
	self.UI_系统:显示(全局游戏宽度-26,全局游戏高度-34,true,nil,nil,nil,nil,nil,nil,nil,true,"系统")

	-- self.UI_抽奖:显示(160,10,true)
	self.UI_梦幻:显示(全局游戏宽度-53,全局游戏高度-120,true)
	self.精灵边框:显示(全局游戏宽度-52,全局游戏高度-112,true)
	self.精灵边框:更新(dt)
	-- self.成就边框:更新(dt)
	-- self.成就边框:显示(全局游戏宽度-83,全局游戏高度-34)
	-- self.UI_仙玉抽奖:显示(200,5,true)
	if self:检查点(x,y) then
		tp.按钮焦点 = true
	end

end

function 系统类_底图框:检查点(x,y)
	return 矩阵:检查点(x,y) or self.UI_梦幻:是否选中(x,y)  or self.UI_抽奖:是否选中(x,y)
end

return 系统类_底图框