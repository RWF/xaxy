-- @Author: 作者QQ381990860
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-12 01:31:43
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-11-06 02:19:39
--======================================================================--
local 场景类_战斗召唤栏 = class()
local tp
function 场景类_战斗召唤栏:初始化(根)
	self.ID = 107
	self.x = 400
	self.y = 140
	self.xx = 0
	self.yy = 0
	self.注释 = "战斗召唤栏"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = require("script/System/按钮")
	self.资源组 = {
		[1] = 根.资源:载入('JM.dll',"网易WDF动画",0xE87F26E1),
		[2] = 按钮(根.资源:载入('JM.dll',"网易WDF动画",0xF11233BB),0,0,4),
		[3] = 按钮(根.资源:载入('JM.dll',"网易WDF动画",0xFD3D61F2),0,0,4),
		[4] = 按钮(根.资源:载入('JM.dll',"网易WDF动画",0x09217E13),0,0,4),
		[5] = 按钮(根.资源:载入('JM.dll',"网易WDF动画",0x2BD1DEF7),0,0,4,nil,nil,"参战"),
	}
	self.窗口时间 = 0
	self.选中召唤兽 = 0
	self.加入 = 0
	self.数据 = {}
	tp = 根
end

function 场景类_战斗召唤栏:打开(数据)
	if self.可视 then
		self.加入 = 0
		self.选中召唤兽 = 0
		self.可视 = false
	else
		self.加入 = 0
		self.选中召唤兽 = 0
		self.数据 = 数据
		self.可视 = true
		tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
	end
end

function 场景类_战斗召唤栏:更新(dt,mx,my)
	if self.可视 then
		self.焦点 = false
		if self.鼠标 then
			if self.资源组[2]:事件判断()  then
				self:打开()
			end
			if self.资源组[3]:事件判断() then
				self.加入 = self.加入 - 1
			end
			if self.资源组[4]:事件判断() then
				self.加入 = self.加入 + 1
			end
			if self.资源组[5]:事件判断() then
			   if self.数据[self.选中召唤兽].参战 then
	            tp.提示:写入("#Y/这只召唤兽已经出战过了")
	           else
		           tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].类型="召唤"
		           tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].目标=self.数据[self.选中召唤兽].编号
		           tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].参数=tp.场景.战斗.命令参数
		             if tp.场景.战斗.当前单位==#tp.场景.战斗.操作单位 then
		                tp.鼠标.置鼠标("普通")
		                tp.场景.战斗.回合进程="等待回合"
		                 客户端:发送数据(1, 235, 24, tp.场景.战斗:转文本(tp.场景.战斗.操作单位))
		             else
		              tp.场景.战斗.当前单位=tp.场景.战斗.当前单位+1
		              tp.场景.战斗.命令版面=true
		              tp.场景.战斗.捕捉开关=false
		              tp.场景.战斗.法术开关=false
		              tp.场景.战斗.命令类型="攻击"
		              tp.场景.战斗.命令对象=2
		              tp.鼠标.置鼠标("普通")
		             end
		             self.可视 = false
	           end
			end
			if 引擎.鼠标弹起(1) and not tp.禁止关闭 then
				self:打开()
			end
			if tp.按钮焦点 then
				self.焦点 = true
			end
		end
	end
end

function 场景类_战斗召唤栏:显示(dt,mx,my)
	if self.可视 then
		self.资源组[1]:显示(self.x,self.y)
		self.资源组[2]:更新(mx,my)
		self.资源组[3]:更新(mx,my,self.加入 > 0)
		self.资源组[4]:更新(mx,my,self.加入 < #self.数据)
		self.资源组[5]:更新(mx,my,self.选中召唤兽 ~= 0 and self.数据 ~= self.数据[self.选中召唤兽])
		if not self.鼠标 then
			self.资源组[2].焦点 = 0
			if self.加入 > 0 then
				self.资源组[3].焦点 = 0
			else
				self.资源组[3].焦点 = 3
			end
			if self.加入 < #self.数据 - 5 then
				self.资源组[4].焦点 = 0
			else
				self.资源组[4].焦点 = 3
			end
			if self.选中召唤兽 ~= 0 then
				self.资源组[5].焦点 = 0
			else
				self.资源组[5].焦点 = 3
			end
		end
		self.资源组[2]:显示(self.x + 148,self.y + 6)
		self.资源组[3]:显示(self.x + 136,self.y + 35)
		self.资源组[4]:显示(self.x + 132,self.y + 149)
		self.资源组[5]:显示(self.x + 53,self.y + 308,true)
		local 偏移x = 26
		local 偏移y = 40
		for m=1,7 do
			if self.数据[m] ~= nil then
				if((m + self.加入) <= (5 + self.加入)) then
					if(mx - self.x > 10 and my - self.y+4 > 偏移y + 20*m and mx - self.x < 130 and my - self.y + 4 < 偏移y + 20*(m+1) ) and self.鼠标 and not 文本栏焦点 then
						if mx - self.x+12 > 0 and my - self.y-35 > 0 + 20 * m and mx - self.x < 130 and my - self.y+7 < 42 + 20*(m+1) and self.数据[m] ~= nil and not 文本栏焦点 then
							if 引擎.鼠标弹起(0) then
								if self.数据[m + self.加入] then
								self.选中召唤兽 = m + self.加入
							   end
							end
							for n=1,19 do
								引擎.画线(self.x-3 + 偏移x,self.y + 偏移y + 20 * m - 3 + n,self.x + 130,self.y + 偏移y + 20 * m - 3 + n,ARGB(255,201,207,109))
							end
							self.焦点 = true
						end
					end
				end
			end
		end
		if (self.选中召唤兽 - self.加入) > 0  and (self.选中召唤兽 - self.加入 <= 5) then
			for n=1,19 do
				引擎.画线(self.x-3 + 偏移x,self.y + 偏移y + 20 * (self.选中召唤兽 - self.加入) - 3 + n,self.x + 130,self.y + 偏移y + 20 * (self.选中召唤兽 - self.加入) - 3 + n,ARGB(255,91,90,219))
			end
		end
		for n=1,7	do
			if((n + self.加入) <= (5 + self.加入)) and self.数据[n + self.加入]~=nil then
				local 颜色 = ARGB(255,0,0,0)
				if self.数据[n + self.加入].参战  then
					颜色 = ARGB(255,255,255,0)
				end
				tp.字体表.普通字体:置颜色(颜色):显示(self.x + 偏移x,self.y + 偏移y + n * 20,self.数据[n + self.加入].名称)
			end
		end
		if self.选中召唤兽 ~= 0 then
			tp.字体表.普通字体:置颜色(ARGB(255,0,0,0))
			tp.字体表.普通字体:显示(self.x + 60,self.y+181,self.数据[self.选中召唤兽].名称)
			tp.字体表.普通字体:显示(self.x + 60,self.y+206,self.数据[self.选中召唤兽].等级)
			tp.字体表.普通字体:显示(self.x + 60,self.y+231,self.数据[self.选中召唤兽].忠诚)
			tp.字体表.普通字体:显示(self.x + 60,self.y+256,self.数据[self.选中召唤兽].当前气血.."/"..self.数据[self.选中召唤兽].气血上限)
			tp.字体表.普通字体:显示(self.x + 60,self.y+281,self.数据[self.选中召唤兽].当前魔法.."/"..self.数据[self.选中召唤兽].魔法上限)
		end
	end
end



function 场景类_战斗召唤栏:检查点(x,y)
	if self.可视 and self.资源组[1]:是否选中(x,y)  then
		return true
	end
end

function 场景类_战斗召唤栏:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.场景.战斗.移动窗口 = true
	end
	if self.可视 and self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end
function 场景类_战斗召唤栏:开始移动(x,y)
	if self.可视 and self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_战斗召唤栏