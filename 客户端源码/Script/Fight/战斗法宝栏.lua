-- @Author: 作者QQ381990860
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-12 01:31:44
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-09-27 04:57:29
--======================================================================--
local 场景类_战斗法宝栏 = class()
local require = require
local tp

function 场景类_战斗法宝栏:初始化(根)
	self.ID = 104
	self.x = 220+(全局游戏宽度-800)/2
	self.y = 175
	self.xx = 0
	self.yy = 0
	self.注释 = "战斗"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = require("script/System/按钮")
	self.资源组 = {
		[1] = 根.资源:载入('JM.dll',"网易WDF动画",0xC6D8094C),
		[2] = 按钮(根.资源:载入('JM.dll',"网易WDF动画",0xF11233BB),0,0,4),
	}
	self.物品 = {}
	local 格子 = require("script/System/物品_格子")
	for h=1,4 do
		for l=1,5 do
			self.物品[#self.物品 + 1] = 格子(l*51-37+self.x+4,h*51+self.y-16,#self.物品 + 1,"战斗道具_物品")
		end
	end
	self.窗口时间 = 0
	tp = 根
end

function 场景类_战斗法宝栏:打开(数据)
	if self.可视 then
		self.可视 = false
	else
		self.x = 220+(全局游戏宽度-800)/2
		for n=1,20 do
			self.物品[n]:置物品(数据[n])
		end
		self.可视 = true
		tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
	end
end

function 场景类_战斗法宝栏:更新(dt,x,y)
end

function 场景类_战斗法宝栏:显示(dt,x,y)
	if not self.可视 then
		return false
	end
	self.焦点 = false
	if self.鼠标 then
		self.资源组[2]:更新(x,y)
		if self.资源组[2]:事件判断() then
			self:打开()
		end
	else
		self.资源组[2].焦点 = 0
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x + 286,self.y + 6,true)
	local n = 0
	for h=1,4 do
		for l=1,5 do
			n = n + 1
			self.物品[n]:置坐标(l*51-37+self.x+4,h*51+self.y-16)
			self.物品[n]:显示(dt,x,y,self.鼠标,"法宝")
			if self.物品[n].物品 ~= nil and self.物品[n].焦点 and self.鼠标 then
				tp.提示:道具行囊(x,y,self.物品[n].物品)
				local Name =self.物品[n].物品.名称

				if self.物品[n].事件 and self.物品[n].物品.类型 == "法宝" and not ItemData[Name].属性.佩戴 and self.鼠标 then
					-- tp.场景.战斗.命令类型="法宝"
     --                tp.场景.战斗.命令对象=1
     --                tp.场景.战斗.命令参数=n
     --                tp.鼠标.置鼠标("道具")
     --                tp.场景.战斗.命令版面=false
					-- self.可视 = false
				 if ItemData[Name].属性.类型 =="攻击" or ItemData[Name].属性.类型 =="诅咒"  then
			         tp.场景.战斗.命令类型="法宝"
			         tp.场景.战斗.命令对象=2
			         tp.场景.战斗.命令参数=n
			         tp.鼠标.置鼠标("道具")
			         tp.场景.战斗.命令版面=false
			        -- elseif ItemData[Name].属性.类型 =="辅助" or self.物品[n].物品.种类 ==6 then
			        --  tp.场景.战斗.命令类型="技能"
			        --  tp.场景.战斗.命令对象=1
			        --  tp.场景.战斗.命令参数=self.物品[n].物品.名称
			        --  tp.鼠标.置鼠标("道具")
			        --  tp.场景.战斗.命令版面=false
			     elseif ItemData[Name].属性.类型 =="防御" or ItemData[Name].属性.类型 =="辅助" or  ItemData[Name].属性.类型 =="恢复"then
			         tp.场景.战斗.命令类型="法宝"
			         tp.场景.战斗.命令对象=1
			         tp.场景.战斗.命令参数=n
			         tp.场景.战斗.命令版面=false
			         tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].类型=tp.场景.战斗.命令类型
			         tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].目标=tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].编号
			         tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].参数=tp.场景.战斗.命令参数
						if tp.场景.战斗.当前单位==#tp.场景.战斗.操作单位 then
						self.回合进程="等待回合"
						 客户端:发送数据(1,235,24,tp.场景.战斗:转文本(tp.场景.战斗.操作单位))
						else
						tp.场景.战斗.当前单位=tp.场景.战斗.当前单位+1
						tp.场景.战斗.命令版面=true
						tp.场景.战斗.命令类型="攻击"
						tp.场景.战斗.命令对象=2
						end
			        end
			         for n=1,#tp.场景.战斗.窗口_ do
						if tp.场景.战斗.窗口_[n].可视 then
							tp.场景.战斗.窗口_[n]:打开()
						end
					end
				break



				end
			end
        end
	end
	if tp.按钮焦点 then
		self.焦点 = true
	end
	if 引擎.鼠标弹起(1)  and not tp.禁止关闭 and self.鼠标 then
		self:打开()
	end
end

function 场景类_战斗法宝栏:检查点(x,y)
	if self.可视 and self.资源组[1]:是否选中(x,y)  then
		return true
	end
	return false
end

function 场景类_战斗法宝栏:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not 引擎.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.场景.战斗.移动窗口 = true
	end
	if self.可视 and self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_战斗法宝栏:开始移动(x,y)
	if self.可视 and self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_战斗法宝栏