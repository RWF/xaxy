-- @Author: 作者QQ381990860
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-12 01:31:41
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-12-01 23:10:19
--======================================================================--
local 精灵 = {}
local 宽度,高度=引擎.宽度,引擎.高度
local 宽度2,高度2=引擎.宽度2,引擎.高度2
local 纹理 = require "gge纹理类"():渲染目标(宽度,高度)
for i=1,20 do--(宽度/5)*(高度/5) do--
    精灵[i] = require("gge精灵类")(纹理)
end

local 变换 = class()


function 变换:初始化()
    引擎.截图到纹理(纹理)
    self.模式 = 取随机数(1,3)--math.random(5)
    self.播放 = true
    self.精灵 = {}
    if self.模式 == 1 then--四角移动
         self.精灵[1]     = 精灵[1]:置区域(0,0,宽度2,高度2)
        self.精灵[1].xy  = require("gge坐标类")(0,0)
        self.精灵[1].xya = require("gge坐标类")(-宽度2,-高度2)
        self.精灵[2]     = 精灵[2]:置区域(宽度2,0,宽度2,高度2)
        self.精灵[2].xy  = require("gge坐标类")(宽度2,0)
        self.精灵[2].xya = require("gge坐标类")(宽度,-高度2)
        self.精灵[3]     = 精灵[3]:置区域(0,高度2,宽度2,高度2)
        self.精灵[3].xy  = require("gge坐标类")(0,高度2)
        self.精灵[3].xya = require("gge坐标类")(-宽度2,高度)
        self.精灵[4]     = 精灵[4]:置区域(宽度2,高度2,宽度2,高度2)
        self.精灵[4].xy  = require("gge坐标类")(宽度2,高度2)
        self.精灵[4].xya = require("gge坐标类")(宽度,高度)
        for i=1,4 do
            self.精灵[i]:置颜色(0xFFFFFFFF)
        end
    elseif self.模式 == 2 then--左右渐变透明移动
        self.精灵[1]     = 精灵[1]:置区域(0,0,宽度,高度2)
        self.精灵[1].xy  = 生成XY(0,0)
        self.精灵[1].透明= 255
        self.精灵[2]     = 精灵[2]:置区域(0,高度2,宽度,高度2)
        self.精灵[2].xy  = 生成XY(0,高度2)
        self.精灵[2].透明= 255
    elseif self.模式 == 3 then--横条渐变
        for i=1,20 do
            local y = (i-1)*30
            self.精灵[i] = 精灵[i]:置区域(0,y,宽度,30)
            self.精灵[i].xy = 生成XY(0,y)
            self.精灵[i].x,self.精灵[i].y = 0,y
            self.精灵[i].w,self.精灵[i].h = 宽度,30
            self.精灵[i]:置颜色(0xFFFFFFFF)
        end
    elseif self.模式 == 4 then--左右线横条移动
        for i=1,高度 do
            local y = i-1
            self.精灵[i] = 精灵[i]:置区域(0,y,宽度,1)
            self.精灵[i].xy = 生成XY(0,y)
            self.精灵[i]:置颜色(0xFFFFFFFF)
        end
    elseif self.模式 == 5 then--花点
        local i = 1
        for h=1,高度/5 do
            for l=1,宽度/5 do
                local x,y = (l-1)*5,(h-1)*5
                self.精灵[i] = 精灵[i]:置区域(x,y,5,5)
                self.精灵[i].xy = 生成XY(x,y)
                self.精灵[i]:置颜色(0xFFFFFFFF)
                i=i+1
            end
        end
    end
end

function 变换:更新(dt)
    if self.播放 then
        if self.模式 == 1 then
            for i,v in ipairs(self.精灵) do
                v.xy = v.xy:取移动坐标(v.xya,dt*300)
            end
            self.播放=self.精灵[1].xy.x>-宽度2
        elseif self.模式 == 2 then
            self.精灵[1].xy.x=self.精灵[1].xy.x-dt*500
            self.精灵[1].透明=self.精灵[1].透明-4
            self.精灵[1]:置颜色(bit.bor(0xFFFFFF,bit.lshift(self.精灵[1].透明, 24)))
            self.精灵[2].xy.x=self.精灵[2].xy.x+dt*500
            self.精灵[2].透明=self.精灵[2].透明-4
            self.精灵[2]:置颜色(bit.bor(0xFFFFFF,bit.lshift(self.精灵[2].透明, 24)))
            self.播放=self.精灵[2].透明>0
        elseif self.模式 == 3 then
            for i,v in ipairs(self.精灵) do
                v.h=v.h-dt*30
                v:置区域(v.x,v.y,v.w,v.h)
                self.播放 = v.h>0
            end
        elseif self.模式 == 4 then
            for i,v in ipairs(self.精灵) do
                if i%2 == 0 then
                    v.xy.x=v.xy.x-dt*500
                else
                    v.xy.x=v.xy.x+dt*500
                    self.播放 = v.xy.x<宽度
                end
            end
        elseif self.模式 == 5 then
            for i=1,math.floor((宽度/5)*(高度/5)*dt) do
                table.remove(self.精灵, math.random(#self.精灵))
            end
            self.播放 = #self.精灵>0
        end
    end
end

function 变换:显示(x,y)
    if self.播放 then
        for i,v in ipairs(self.精灵) do
            v:显示(v.xy)
        end
    end
end

return 变换