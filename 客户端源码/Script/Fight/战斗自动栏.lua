--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-11-17 01:46:48
--======================================================================--
local 场景类_战斗自动栏 = class()
local 开启自动 = false
local format = string.format
local tp
local  zts,zts1


function 场景类_战斗自动栏:初始化(根)
	self.ID = 102
	self.x = 258+(全局游戏宽度-800)/2
	self.y = 260
	self.xx = 0
	self.yy = 0
	self.注释 = "战斗"
	self.可视 = false
	self.可视化 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	self.窗口时间 = 0
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[0] = 自适应.创建(1,1,204,18,1,3,nil,18),
		[1] = 自适应.创建(0,1,220,119,3,9),
		[2] = 按钮.创建(根.资源:载入('JM.dll',"网易WDF动画",0x2BD1DEF7),0,0,4,true,nil,"重置"),
		[3] = 按钮.创建(根.资源:载入('JM.dll',"网易WDF动画",0x2BD1DEF7),0,0,4,true,nil,"取消"),
		[4] = 按钮.创建(根.资源:载入('JM.dll',"网易WDF动画",0x2BD1DEF7),0,0,4,true,nil,"暂离"),
		[5] = 按钮.创建(根.资源:载入('JM.dll',"网易WDF动画",0xF11233BB),0,0,4,true,true),
	}
	for i=2,5 do
		self.资源组[i]:绑定窗口_(102)
	end
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.普通字体__
	self.介绍文本 = 根._丰富文本(130,50,根.字体表.普通字体)
	self.介绍文本:清空()
	self.第一次 = false
end

function 场景类_战斗自动栏:打开()
	if self.可视化 then
		self.可视化 = false
	else
		self.x = 258+(全局游戏宽度-800)/2
		self.可视化 = true
		tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	end
end

function 场景类_战斗自动栏:更新(dt,x,y)
end

function 场景类_战斗自动栏:显示(dt,x,y)


if not self.可视化 then
		return
	end
       if 自动战斗  then
		self.状态 = "取消"
		else
		self.状态 = "开启"
		end
	self.资源组[3]:置文字(self.状态)
	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[0]:显示(self.x+6,self.y+3)
	self.资源组[5]:显示(self.x+200,self.y+3)
	Picture.窗口标题背景_:置区域(0,0,74,16)
	Picture.窗口标题背景_:显示(self.x+72,self.y+3)
	tp.字体表.描边字体:置字间距(2)
	tp.字体表.描边字体:显示(self.x+78,self.y+3,"自动战斗")
	tp.字体表.描边字体:置字间距(0)
	tp.字体表.普通字体:置颜色(-16777216)

	if self.状态 == "取消" then
		self.介绍文本:清空()
		self.介绍文本:添加文本("#W/ 剩余  #Y/"..自动回合.."#W/回合")
		if tp.系统设置.战斗设置[7] == "默认法术" then
			if tp.场景.战斗.参战数据[tp.场景.战斗.操作单位[1].编号].单位.数据.默认法术 ~= nil then
			self.介绍文本:添加文本("#W/ 人物 ：#G/"..tp.场景.战斗.参战数据[tp.场景.战斗.操作单位[1].编号].单位.数据.默认法术)
			else
			self.介绍文本:添加文本("#W/ 人物 ：#G/物理攻击")
			end
		elseif tp.系统设置.战斗设置[7] == "普通攻击" then
			self.介绍文本:添加文本("#W/ 人物 ：#G/物理攻击")
		elseif tp.系统设置.战斗设置[7] == "防御" then
			self.介绍文本:添加文本("#W/ 人物 ：#G/防御")
		else
			self.介绍文本:添加文本("#W/ 人物 ：#G/"..tp.系统设置.战斗设置[7])
        end
		if tp.场景.战斗.操作单位[2]~=nil then
				if tp.系统设置.战斗设置[9] == "默认法术" then
					if tp.场景.战斗.参战数据[tp.场景.战斗.操作单位[2].编号].单位.数据.默认法术 ~= nil then
					self.介绍文本:添加文本("#W/召唤兽：#G/"..tp.场景.战斗.参战数据[tp.场景.战斗.操作单位[2].编号].单位.数据.默认法术)
					else
					self.介绍文本:添加文本("#W/召唤兽：#G/物理攻击")
					end
				elseif tp.系统设置.战斗设置[9] == "普通攻击" then
					self.介绍文本:添加文本("#W/召唤兽：#G/物理攻击")
				elseif tp.系统设置.战斗设置[9] == "防御" then
					self.介绍文本:添加文本("#W/召唤兽：#G/防御")
				else
					self.介绍文本:添加文本("#W/召唤兽：#G/"..tp.系统设置.战斗设置[9])
		        end

	    end
	end
	--tp.字体表.普通字体:显示(self.x + 16,self.y + 55,format("点击按钮%s自动",self.状态))
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y)
	self.资源组[4]:更新(x,y)
	self.资源组[5]:更新(x,y)
	if self.鼠标 then
		if self.资源组[2]:事件判断() then
	          自动回合=30
	     elseif self.资源组[5]:事件判断() or (引擎.鼠标弹起(1)and self:检查点(x,y)) then
				self.介绍文本:清空()
	            自动战斗=false
				self.状态 = "取消"
				self.可视化 = false
				tp.提示:写入("#Y/你取消了自动战斗")
			return
		elseif self.资源组[4]:事件判断() then
			自动回合=99999
		elseif self.资源组[3]:事件判断() then
			if self.状态 == "取消" then
				 self.介绍文本:清空()
	            自动战斗=false
				self.状态 = "取消"
				self.可视化 = false
				tp.提示:写入("#y/你取消了自动战斗")
			else
				自动战斗=true
                自动回合=99999
				self.状态 = "开启"
		        tp.提示:写入("#Y/你开启了自动战斗，在命令回合开始3秒后自动下达指令")
				for n=1,#tp.场景.战斗.窗口_ do
					if tp.场景.战斗.窗口_[n].可视 then
						tp.场景.战斗.窗口_[n]:打开()
					end
				end
				tp.场景.战斗.提示 = ""
			end
			self.资源组[3]:置文字(self.状态)
		end
	end
	self.资源组[4]:显示(self.x+20,self.y+85)
	self.资源组[2]:显示(self.x+148,self.y+85)
	self.资源组[3]:显示(self.x+83,self.y+85,true)
	self.介绍文本:显示(self.x+60,self.y+30)
	if tp.按钮焦点 then
		self.焦点 = true
	end

end

function 场景类_战斗自动栏:检查点(x,y)
	if self.可视化 and self.资源组[1]:是否选中(x,y)  then
		return true
	end
end

function 场景类_战斗自动栏:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not self.第一次 then
		tp.运行时间 = tp.运行时间 + 2
		self.第一次 = true
	end
	if not 引擎.场景.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.场景.战斗.移动窗口 = true
	end
	if self.可视化 and self.鼠标 and not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_战斗自动栏:开始移动(x,y)
	if self.可视化 and self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_战斗自动栏