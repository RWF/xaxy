-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-20 01:41:37
local 场景类_战斗对象 = class()
local floor = math.floor;
local ceil = math.ceil;
local random1 = math.random
local min = math.min
local max = math.max
local insert = table.insert
local remove = table.remove


local random = 引擎.取随机整数


local wav = 引擎.取音效
local bfs = 取八方向
local jds = 取两点角度
local xys = 生成XY
local tp = nil

local 目标 = {}

local pairs = pairs
local typ = type
local yxs = require("script/Resource/FSB")
local FMOD = require("Fmodd类")





local function 取偏移(模型)
	local pysa,pysy
	if 模型 == "蛤蟆精" then
		pysa = -20
	elseif 模型 == "白熊" or 模型 == "巫蛮儿" or 模型 == "强盗" or 模型 == "鲛人"or 模型 == "犀牛将军兽形" then
		pysa = 0
	elseif 模型 == "狐狸精" or 模型 == "巨力神猿" then
		pysa = 10
	elseif 模型 == "山贼" or 模型 == "修罗傀儡妖" or 模型 == "大蝙蝠" or 模型 == "地狱战神" or 模型 == "幽萤娃娃"or 模型 == "幽灵" or 模型 == "蚌精"or 模型 == "龟丞相"or 模型 == "蟹将" or 模型 == "知了王" then
		pysa = 15
	elseif 模型 == "炎魔神" or 模型 == "骷髅怪" or 模型 == "葫芦宝贝" or 模型 == "树怪" then
		pysa = 18
	elseif 模型 == "羊头怪" or 模型 == "进阶雷鸟人" then
		pysa = 20
		pysy = 15
	elseif 模型 == "灵鹤" or 模型 == "古代瑞兽" then
		pysa = 25
		pysy = 12
	elseif 模型 == "桃夭夭" or 模型 == "风伯" or 模型 == "持国巡守" or 模型 == "天将" or 模型 == "混沌兽"or 模型 == "吸血鬼" then
		pysa = -15
	elseif 模型 == "鬼潇潇" then
		pysa = -22
	elseif 模型 == "兔子怪" then
		pysa = -30
	elseif 模型 == "泪妖" or 模型 == "碧水夜叉" or 模型 == "雾中仙"or 模型 == "蔓藤妖花" or 模型 == "海毛虫"or 模型 == "大海龟" then
		pysa = -10
	elseif 模型 == "浣熊" or 模型 == "哮天犬" or 模型 == "海星" then
		pysa = -6
	elseif 模型 == "天兵" or 模型 == "雷鸟人" then
		pysa = 10
		pysy = 8
	elseif 模型 == "鬼将" then
		pysa = 24
		pysy = 18
	elseif 模型 == "泡泡" or 模型 == "花妖" or 模型 == "黑熊" or 模型 == "虾兵" then
		pysa = -12
		pysy = -12
	elseif 模型 == "偃无师"or 模型 == "巨蛙" or 模型 == "桃夭夭" then
		pysa = 2
		pysy = 18
	elseif 模型 == "帮派妖兽" then
		pysa = 40
		pysy = 30
	elseif 模型 == "大力金刚"then
		pysa = 60
		pysy = 30
	elseif 模型 == "牛妖" then
		pysa = 65
		pysy = 55
	elseif 模型 == "画魂" then
		pysa = 5
		pysy = 40
	elseif 模型 == "芙蓉仙子"then
		pysa = 45
		pysy = 40
	elseif 模型 == "狼" then
		pysa = 20
		pysy = 25
	elseif 模型 == "长眉灵猴"then
		pysa = 5
		pysy = 25
	end
	return pysa,pysy
end
local function 取攻击帧(模型,zl)
	local 攻击帧,攻击延迟,终结帧 = 2,1.25,nil
	if 模型 == "偃无师" or 模型 == "桃夭夭" then
		攻击帧 = -1
		攻击延迟 = 1.35
	elseif 模型 == "鬼潇潇" or 模型 == "龙太子" or 模型 == "剑侠客" or 模型 == "真陀护法" then
		攻击帧 = 1
		攻击延迟 = 1.3
	elseif 模型 == "玄彩娥" or 模型 == "舞天姬" or 模型 == "进阶毗舍童子"or 模型 == "羊头怪" or 模型 == "锦毛貂精" then
		攻击帧 = -1
		攻击延迟 = 1.15
	elseif 模型 == "虎头怪" or 模型 == "神天兵" or 模型 == "巨魔王" or 模型 == "杀破狼" or 模型 == "持国巡守" or 模型 == "雷鸟人" or 模型 == "金饶僧" or 模型 == "葫芦宝贝" or 模型 == "幽灵" or 模型 == "凤凰" or 模型 == "野鬼" or 模型 == "帮派妖兽" or 模型 == "修罗傀儡鬼" or 模型 == "踏云兽" or 模型 == "巴蛇" or 模型 == "黑熊" then
		攻击帧 = 1
		攻击延迟 = 1.2
		if zl ~= nil then
			if zl == "弓弩" or zl == "弓弩1" then
				攻击延迟 = 0.88
			end
		end
	elseif 模型 == "强盗" or 模型 == "山贼" or 模型 == "鼠先锋" or 模型 == "增长巡守"or 模型 == "灵灯侍者" or 模型 == "般若天女" or 模型 == "进阶雨师" or 模型 == "进阶如意仙子" or 模型 == "野猪精" or 模型 == "超级玉兔" or 模型 == "幽莹娃娃" or 模型 == "黑熊精" or 模型 == "蚌精" or 模型 == "机关鸟" or 模型 == "连弩车" or 模型 == "蜃气妖" or 模型 == "蛤蟆精" or 模型 == "虾兵" or 模型 == "蟹将" or 模型 == "兔子怪" or 模型 == "蜘蛛精" or 模型 == "花妖" or 模型 == "狐狸精" or 模型 == "哮天犬" or 模型 == "混沌兽" or 模型 == "蝴蝶仙子" or 模型 == "海毛虫" or 模型 == "狼" or 模型 == "老虎" then
		攻击帧 = 2
		攻击延迟 = 1.12
	elseif 模型 == "机关人人形" or 模型 == "机关兽" then
		攻击帧 = 2
		攻击延迟 = 1.25
	elseif 模型 == "泡泡" then
		攻击帧 = 2
		攻击延迟 = 2.1
	elseif 模型 == "混沌兽" then
		攻击延迟 = 1.35
	elseif 模型 == "狂豹人形" then
		攻击帧 = 1
		攻击延迟 = 1.4
	elseif 模型 == "海毛虫" then
		攻击延迟 = 1.35
	elseif 模型 == "大海龟"or 模型 == "骷髅怪" or 模型 == "金身罗汉" or 模型 == "修罗傀儡妖" or 模型 == "曼珠沙华" or 模型 == "幽萤娃娃" then
		攻击帧 = 1
		攻击延迟 = 1.2
	elseif 模型 == "画魂" or 模型 == "羽灵神" then
		攻击帧 = 1
		攻击延迟 = 1.1
	elseif 模型 == "天兵"or 模型 == "巨力神猿" then
		攻击帧 = 1
		攻击延迟 = 1.25
	elseif 模型 == "地狱战神" or 模型 == "风伯" or 模型 == "芙蓉仙子" or 模型 == "毗舍童子" or 模型 == "镜妖" or 模型 == "千年蛇魅"or 模型 == "小龙女" then
		攻击帧 = 0
		攻击延迟 = 1.25
	elseif 模型 == "芙蓉仙子" then
		攻击帧 = 0
		攻击延迟 = 1.1
	elseif 模型 == "百足将军" or 模型 == "天将" or 模型 == "小龙女" or 模型 == "碧水夜叉" or 模型 == "马面" or 模型 == "灵鹤" then
		攻击帧 = 3
		攻击延迟 = 1.23
	elseif 模型 == "鬼将" or 模型 == "大力金刚"then
		攻击帧 = 4
		攻击延迟 = 1.3
		终结帧 = 1
	elseif 模型 == "赌徒" then
		攻击帧 = 4
		攻击延迟 = 1.1
	elseif 模型 == "牛妖" then
		攻击帧 = 3
		攻击延迟 = 1.26
	elseif 模型 == "古代瑞兽" then
		攻击帧 = 4
		攻击延迟 = 1.2
	elseif 模型 == "知了王" then
		攻击帧 = 6
		攻击延迟 = 1.32
	elseif 模型 == "黑山老妖" or 模型 == "炎魔神" then
		攻击帧 = 6
		攻击延迟 = 1.2
	elseif 模型 == "长眉灵猴"or 模型 == "化圣剑侠客" then
		攻击帧 = -1
		攻击延迟 = 1.23
	elseif 模型 == "骨精灵" or 模型 == "狐美人" or 模型 == "剑侠客" or 模型 == "逍遥生" or 模型 == "巫蛮儿" or 模型 == "英女侠" or 模型 == "飞燕女" then
		if zl ~= nil then
			if zl == "魔棒" then
				攻击帧 = -1
			elseif zl == "宝珠" then
				攻击帧 = 2
			elseif 模型 == "英女侠" then
				攻击帧 = 0
			elseif 模型 == "飞燕女" and zl == "双短剑" then
				攻击帧 = 0
			elseif 模型 == "飞燕女" and zl == "环圈" then
				攻击帧 = 3
			elseif 模型 == "逍遥生" and (zl == "扇") then
				攻击帧 = 0
			-- elseif 模型 == "逍遥生" and (zl == "剑") then
			-- 	攻击帧 = 1
			-- 	终结帧 = 1
			elseif 模型 == "巫蛮儿" and (zl == "法杖") then
				攻击帧 = 0
			elseif 模型 == "狐美人" and zl == "爪刺" then
				攻击帧 = 0
			elseif 模型 == "狐美人" and zl == "鞭" then
				攻击帧 = 0
			end
		else
			攻击帧 = 1
		end
		攻击延迟 = 1.25
	end
	return 攻击帧,攻击延迟,终极帧
end
function 场景类_战斗对象:音效类1_(文件号,资源包,子类)


	if 文件号 ~= nil and 文件号 ~= 0 and 资源包~= nil and tp.系统设置.声音设置[4] then

 -- for n = 1,5000 do 
		self.法术音效 = FMOD.创建(程序目录.."lib/"..资源包,nil,tp.资源:取偏移(资源包,文件号),2565224)
		self.法术音效:播放()
	-- end
	end
end
function 场景类_战斗对象:初始化(数据,敌我)

	if tp == nil then
		tp = 引擎.场景
	end
	self.时间 = 0
	目标 = tp.场景.战斗.参战数据


 local 位置 = require "script/Fight/战斗位置"[数据.阵法 or 1];


 self.原始 = {x = 位置[敌我][数据.位置].x+全局游戏宽度/2,y = 位置[敌我][数据.位置].y+全局游戏高度/2}
 self.临时 = {x = 位置[敌我][数据.位置].x+全局游戏宽度/2,y = 位置[敌我][数据.位置].y+全局游戏高度/2}
 self.目标 = {x = 位置[敌我][数据.位置].x+全局游戏宽度/2,y = 位置[敌我][数据.位置].y+全局游戏高度/2}
	self.附加 = {x = 0,y = 0}
	self.刷新 = {x = 0,y = 0}
	self.当前 = {x = 0,y = 0}
	self.敌我 = 敌我
	self.特技内容开关 = false
 self.抖动开关 = false
 self.逃跑开关 = false
 self.法暴开关 = false
 self.逃跑特效 = tp:载入特效("逃跑",0)
 self.法暴特效 = tp:载入特效("法暴",1.2)
 self.法暴特效:置提速(1.2)
 self.聊天背景 = tp._自适应.创建(9,1,125,23,3,9)
	if 敌我 == 1 then
		self.方向 = 2
		self.初始方向 = 2
		self.逃跑坐标 = 3
		if 数据.战斗类型 == "角色" then
		self.位置 = 1
		else
		self.位置 = 2
		end
	elseif 敌我 == 2 then
		self.方向 = 0
		self.初始方向 = 0
		self.位置 = 3
		self.逃跑坐标 = -3
	end
	self.名称 = 数据.名称
	self.名称宽度 = floor(tp.字体表.人物字体:取宽度(数据.名称) / 2)
	self.编号 = 数据.编号
	if self.位置 == 1 or self.位置 == 2 then
		self.差异 = {x = -65,y = -25}
	else
		self.差异 = {x = 60,y = 33}
	end
	
	if random(1,2) == 1 then
		self.攻击类型 = "攻击"
	else
	self.攻击类型 = "攻击1"
	end
	
	local djs
	local zl = 0
	if self.位置 == 1 then
		if 数据.武器数据.类型 ~= "" and 数据.武器数据.类型 ~= 0 and 数据.武器数据.名称 ~= ""then
			if 数据.武器数据.名称 == "龙鸣寒水" or 数据.武器数据.名称 == "非攻" then
				zl = "弓弩1"
			else
				zl = 数据.武器数据.类别
			end
		end
	elseif self.位置 == 3 then
		if 数据.武器数据 ~= nil and 数据.武器数据.类型 ~= "" and 数据.武器数据.类型 ~= 0 and 数据.武器数据.名称 ~= ""then
			if type(数据.武器) ~= "boolean" then
				djs = ItemData[数据.武器数据.名称]
				if 数据.武器数据.名称 == "龙鸣寒水" or 数据.武器数据.名称 == "非攻" then
					zl = "弓弩1"
				else
					zl = 数据.武器数据.类别
				end
			else
				zl = true
			end
		end
	end
	self.武器特效 = {}
	local 模型
	if 数据.战斗类型 == "角色" then
		if 数据.变身 ~= nil then
		模型 = 数据.变身
		if 取召唤兽武器(模型) then
			zl = 3
		else
		zl = 0
		end
		elseif 数据.战斗锦衣 then 
			模型 = 数据.战斗锦衣
			zl = 0
		else
			模型 = 数据.造型
		end

 else
 	模型 = 数据.造型
 	if 数据.饰品 then
 		zl = 4
 	elseif 取召唤兽武器(模型) then
			zl = 3
		else
		zl = 0
		end
 end
	local n = 取模型(模型)
	if zl ~= 0 and zl ~= 4 and zl ~= 3 then
		n = 取模型(模型,zl,true)
	end
	self.追加开关 = false
	self.数据 = 数据
	self.行为 = "待战"
	self.状态行为 = "待战"
	self.单位消失 = false
	self.动画 = {}
	
	local 动画资源 = tp.资源

	local zy = n.战斗资源

	self.动画.待战 = 动画资源:载入(zy,"网易WDF动画",n.待战,true)
	self.动画.跑去 = 动画资源:载入(zy,"网易WDF动画",n.跑去,true)
	self.动画.防御 = 动画资源:载入(zy,"网易WDF动画",n.防御,true)
	self.动画.攻击 = 动画资源:载入(zy,"网易WDF动画",n[self.攻击类型],true)
	self.动画.挨打 = 动画资源:载入(zy,"网易WDF动画",n.挨打,true)
	self.动画.返回 = 动画资源:载入(zy,"网易WDF动画",n.跑去,true)
	self.动画.死亡 = 动画资源:载入(zy,"网易WDF动画",n.死亡,true)
	self.动画.施法 = 动画资源:载入(zy,"网易WDF动画",n.施法,true)
	if self.位置 == 3 and self.种类 ~= -1 then
		local cc = 取模型(模型) 
		if cc.行走 then
		self.动画["行走"] = 动画资源:载入(cc.资源,"网易WDF动画",cc.行走,true)
	end
	end

	self.音效模型 = 模型
	local jj = 0
	local jjj = false
	local c = {}
	分割字符(self.音效模型,c)
	if c[1] == "进" and c[2] == "阶" then
		self.音效模型 = ""
		for n = 3,#c do
			self.音效模型 = self.音效模型..c[n]
		end
		jjj = true
	end
	self.武器 = {}
	
	self.攻击方式 = 0 -- 0代表近战，1代表远程
	self.终结帧 = 0
	local ax,ay = 取偏移(模型)
	local pysa,pysy = ax or floor(self.动画["攻击"]:取宽度()/20),ay or 0
	ax,ay = nil,nil
	if jjj then
		jj = 23
	end
	if zl ~= 0 then
		local w = nil
		local v = nil
		if zl ~= true and zl ~= 3 and zl ~= 4 then
			local a1,a2,a3
			if self.位置 == 3 then
				a1,a2,a3 = 数据.武器数据.类别,数据.武器数据.等级,数据.武器数据.名称
			elseif self.位置 == 1 then
				a1,a2,a3 = 数据.武器数据.类别,数据.武器数据.等级,数据.武器数据.名称
			end
			local m = tp:qfjmc(a1,a2,a3) or a3

			if 数据.战斗类型 == "角色" then
			self.武器特效.攻击 = 动画资源:载入("MAX.7z","网易WDF动画",模型.."_"..数据.武器数据.类别.."_攻击")
			self.武器特效.施法 = 动画资源:载入("MAX.7z","网易WDF动画",模型.."_"..数据.武器数据.类别.."_施法")
		end

			w = 取模型(m.."_"..模型)
			self.音效模型 = 模型.."_"..zl


			if zl == "弓弩" or zl == "弓弩1" then
				self.攻击方式 = 1
			end
		elseif 	zl == 4 then
			local m = "饰品_"..模型
			w = 取模型("饰品_"..模型)
		elseif 	zl == 3 then
			w = 取模型("武器_"..模型)
		end
		
		if w.战斗资源~= nil then
			zy = w.战斗资源

			self.武器.待战 = 动画资源:载入(zy,"网易WDF动画",w.待战,true)
			self.武器.跑去 = 动画资源:载入(zy,"网易WDF动画",w.跑去,true)
			self.武器.防御 = 动画资源:载入(zy,"网易WDF动画",w.防御,true)
			self.武器.攻击 = 动画资源:载入(zy,"网易WDF动画",w[self.攻击类型],true)




			pysa = floor(self.武器.攻击:取宽度()/20)
			if 模型 == "鬼潇潇" then
				pysa = -15
			elseif 模型 == "偃无师" or 模型 == "桃夭夭" or 模型 == "狐美人" then
				pysa = 2
				pysy = 18
			elseif 模型 == "玄彩娥" then
				if zl == "飘带" then
					pysa = -2
				else
					pysa = 18
				end
			elseif 模型 == "舞天姬" then
				pysa = 9
			elseif 模型 == "虎头怪" or 模型 == "真陀护法" then
				pysa = 15
			elseif 模型 == "神天兵" or 模型 == "骨精灵"then
			if zl == "枪矛" then
				pysa = 40
				else
					pysa = 20
			end
			elseif 模型 == "巨魔王" then
			pysa = 12
			elseif 模型 == "杀破狼" or 模型 == "巫蛮儿" then
			pysa = 7
			elseif 模型 == "逍遥生" then
				pysa = 15
				pysy = 32
			elseif 模型 == "剑侠客" then
				if zl == "刀" then
					pysa = 60
					pysy = 12
				else
					pysa = 25
					self.终结帧 = 1
				end
			elseif 模型 == "英女侠" then
				pysa = -5
			elseif 模型 == "飞燕女" then
				pysa = 25
				pysy = 29
			elseif 模型 == "吸血鬼" then
				pysa = -15
			elseif 模型 == "天兵" then
				pysa = 6
				pysy = 10
			end
			self.武器.挨打 = 动画资源:载入(zy,"网易WDF动画",w.挨打,true)
			self.武器.返回 = 动画资源:载入(zy,"网易WDF动画",w.跑去,true)
			if w.死亡 ~= 0 then
				self.武器.死亡 = 动画资源:载入(zy,"网易WDF动画",w.死亡,true)

			end
			self.武器.施法 = 动画资源:载入(zy,"网易WDF动画",w.施法,true)

		if 数据.武器数据 and 数据.武器数据.染色 ~= nil then
			self.武器.施法:置染色(数据.武器数据.染色.染色方案,数据.武器数据.染色.染色组.a,数据.武器数据.染色.染色组.b)
			self.武器.待战:置染色(数据.武器数据.染色.染色方案,数据.武器数据.染色.染色组.a,数据.武器数据.染色.染色组.b)
			self.武器.跑去:置染色(数据.武器数据.染色.染色方案,数据.武器数据.染色.染色组.a,数据.武器数据.染色.染色组.b)
			self.武器.防御:置染色(数据.武器数据.染色.染色方案,数据.武器数据.染色.染色组.a,数据.武器数据.染色.染色组.b)
			self.武器.攻击:置染色(数据.武器数据.染色.染色方案,数据.武器数据.染色.染色组.a,数据.武器数据.染色.染色组.b)
			self.武器.挨打:置染色(数据.武器数据.染色.染色方案,数据.武器数据.染色.染色组.a,数据.武器数据.染色.染色组.b)
			self.武器.返回:置染色(数据.武器数据.染色.染色方案,数据.武器数据.染色.染色组.a,数据.武器数据.染色.染色组.b)
			self.武器.死亡:置染色(数据.武器数据.染色.染色方案,数据.武器数据.染色.染色组.a,数据.武器数据.染色.染色组.b)
 	end
			if self.开启饰品 then
				local ccc = 取模型("饰品_"..模型)
				self.武器.行走 = 动画资源:载入(ccc.资源,"网易WDF动画",ccc.行走,true)
			end
		end
	else
		if 模型 == "羽灵神" or 模型 == "杀破狼" then
			self.攻击方式 = 1
		end
	end
	if self.位置 == 3 then
		if 模型 == "灵灯侍者" then
			self.差异.x = floor(self.差异.x - 13)
			self.差异.y = floor(self.差异.y - 10)
		elseif 模型 == "般若天女" then
			self.差异.x = floor(self.差异.x - 16)
			self.差异.y = floor(self.差异.y - 8)
		elseif 模型 == "进阶如意仙子" then
			self.差异.x = floor(self.差异.x - 10)
			self.差异.y = floor(self.差异.y - 15)
		else
			self.差异.x = floor(self.差异.x + pysa - jj)
			self.差异.y = floor(self.差异.y + pysy / 2)
		end
	else
		if 模型 == "灵灯侍者" then
			self.差异.x = floor(self.差异.x + 13)
			self.差异.y = floor(self.差异.y + 10)
		elseif 模型 == "般若天女" then
			self.差异.x = floor(self.差异.x + 16)
			self.差异.y = floor(self.差异.y + 8)
		else
			self.差异.x = floor(self.差异.x - pysa + jj)
			self.差异.y = floor(self.差异.y - pysy / 2)
		end
	end

	if 数据.染色方案 ~= nil and 数据.变身 == nil and 数据.战斗锦衣 == nil then
		if self.数据.战斗类型 == "角色" then 
			self:置染色(DyeData[数据.造型][1],数据.染色.a,数据.染色.b,数据.染色.c,0)
		else
			self:置染色1(数据.染色方案,数据.染色组[1],数据.染色组[2],数据.染色组[3],0)
		end
	end
	self:置方向(self.方向)
	self.攻击帧,self.攻击延迟,self.终极帧 = 取攻击帧(模型,zl)
	self.挨打方 = {}
	self.指令范围 = 0
	self.死亡方式 = 0 -- 0 未死亡 1原地死亡 2飞出场外 3 被捕捉
	self.回合行动 = 0 -- 1 开始回合 2 行动回合 3 具体回合 4 结束回合
	self.操作完毕 = false
	self.行动完毕 = false
	self.保护对象 = nil
	self.旋转触发 = 0
	self.特效 = nil -- 此类用于战斗的特效
	self.特效2 = nil
	self.特效3 = nil
	self.固定特效 = nil
	self.特效延迟 = nil
	self.前置 = nil
	self.技能 = nil

	self.单位消失 = false
	self.真正死亡 = false
	self.临时攻击方式 = self.攻击方式
	if self.方向 == 0 then
		self.反方向 = 2
	else
	self.反方向 = 0
	end

	self.挨打结束 = false
	self.附加效果 = {
		攻击 = false,
		失误 = false,
		施法 = false,
		挨打 = false,
		挨打开始 = false,
		法术攻击 = false,
		捕捉 = false,
		被反震 = false,
		实施完毕 = false,
	}

	self.追加技能 = nil -- 实现攻击后的追加；只实现2个引用(名称-等级)
	self.上回合技能 = nil -- 重复指令
	self.技能冷却 = {}
	self.印记 = {}
	self.封率 = 0
	self.递减 = 0
	self.攻击次数 = 1
	self.已攻击次数 = 0
	self.流血显示 = false
	self.数字偏移 = 0
	self.流血位置 = 30
	self.流血时间 = 0
	self.行动特效 = nil
	self.旋转计时 = nil
	self.死亡旋转 = false
	self.死亡计次 = nil
	self.旋转方式 = nil
	self.被指令目标 = nil
	self.召唤兽 = 0
	self.提示完毕 = false
	self.挨打状态 = "正常"
	self.躲避状态 = false
	self.保护状态 = false
	self.被暴击 = false
	self.增加挨打速度 = 0
	self.高度 = self.动画["待战"].信息组[0].Key_Y
	if self.高度 > 120 then
		self.高度 = 120
	elseif self.高度 < 60 then
		self.高度 = 60
	elseif self.高度 < 85 then
		self.高度 = 85
	end
	if self.位置 == 2 then
		self.高度 = self.高度 + 20
	else
		self.高度 = self.高度 + 20
	end
	self.名称颜色 = -938153196
	self.当前行为 = "无状态"
	--定义技能
	self.后置特效 = {}
	self.前置特效 = {}
	for i = 1,#(self.数据.套装 or {}) do
		if self.数据.套装[i][1] == "附加状态" then
			self:增加状态(self.数据.套装[i][2],取套装效果(self.数据.套装[i][4],1,self.位置),self.数据.套装[i][2])
		end
	end
	self.反击 = false
	self.反击 = false
	self.反击事件 = false
	self.施法前播放 = false
	self.弹力球 = 0
	self.缓冲 = 0
	self.及时挨打 = false
	self.不显示特效 = nil
	self.多段伤害 = nil
	self.特效固定 = nil
	self.固定特效坐标 = nil
	self.特效附加帧 = 0
	self.色相变身 = 0
	self.挨打延时 = 0
	self.延时 = 0

	if self.攻击方式 == 1 then
		self.攻击延迟 = 1
		self.攻击帧 = -3
		self.弓箭攻击 = nil
	end
		self.超丰富文本 = 引擎.场景._丰富文本(100,270)
	self.超丰富文本:添加元素("w", 4294967295.0)
	self.超丰富文本:添加元素("h", 4278190080.0)
	self.超丰富文本:添加元素("y", 4294967040.0)
	self.超丰富文本:添加元素("r", 红色)
	self.超丰富文本:添加元素("g", 4278255360.0)
 :添加元素("xt",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0x43700E25))
 :添加元素("sj",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0x1B1DCE56))
 :添加元素("私聊@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0xF9ADC3DA))
 :添加元素("dy",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0xF9858C95))
 :添加元素("dt",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0xFC41B9B4))
 :添加元素("盘丝洞@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0xF0F8CF29))
 :添加元素("化生寺@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0x0902DAF6))
 :添加元素("龙宫@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0x212E3A85))
 :添加元素("五庄观@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0x2535CF6D))
 :添加元素("活动@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0x285527E7))
 :添加元素("夫妻@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0x29F78369))
 :添加元素("jj",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0x2A092BE6))
 :添加元素("天宫@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0x2BF201B4))
 :添加元素("普陀山@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0x4100814A))
 :添加元素("阴曹地府@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0x46C2DDA3))
 :添加元素("dq",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0x65C5B7EE))
 :添加元素("方寸山@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0x81B9B7F2))
 :添加元素("GM@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0xE8897A81))
 :添加元素("传闻@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0xCD23D726))
 :添加元素("传音@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0xC6566132))
 :添加元素("拍卖@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0xC363C1CC))
 :添加元素("女儿村@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0xB3296C64))
 :添加元素("帮派@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0xAD9D6490))
 :添加元素("魔王寨@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0xADE6DAA4))
 :添加元素("狮驼岭@@",引擎.场景.资源:载入('JM.dll',"网易WDF动画",0xA5F25FB3))

 for n = 0,119 do
 self.超丰富文本:添加元素(n,引擎.场景.包子表情动画[n])
 end

 for n = 0,53 do
 self.超丰富文本:添加元素(400+n,引擎.场景.大包子表情动画[n])
 end
	self.发言内容 = {}
end
function 场景类_战斗对象:置方向(d,m)
	for i,v in pairs(self.动画) do
		v:置方向(d,m)
		if self.武器[i] ~= nil then
			self.武器[i]:置方向(d,m)
		end
		if self.武器特效[i] ~= nil then
			self.武器特效[i]:置方向(d,m)
		end
	end
end
function 场景类_战斗对象:置染色(染色方案,a,b,c,d)
	if 染色方案 ~= nil then
		for i,v in pairs(self.动画) do
			v:置染色(染色方案,a,b,c,d)
		end
	end
end
function 场景类_战斗对象:置染色1(染色方案,a,b,c,d)
	if 染色方案 ~= nil then
		for i,v in pairs(self.动画) do
			v:置染色(染色方案,a,b,c,d)
		end
		for i,v in pairs(self.武器) do
			v:置染色(染色方案,a,b,c,d)
		end
	end
end
function 场景类_战斗对象:动作更新(dt,方向,帧率)
	if 方向 ~= nil then
		self:置方向(方向)
	end
	self.动画[self.状态行为]:更新(dt*1.45,帧率 or 0)
	if self.武器[self.状态行为] ~= nil then
		self.武器[self.状态行为]:更新(dt*1.45,帧率 or 0)
	end
	if self.武器特效[self.状态行为] ~= nil then
		self.武器特效[self.状态行为]:更新(dt*1.45,帧率 or 0)
	end

end
function 场景类_战斗对象:更新(dt,x,y)
	self:指令更新(dt,x,y)
end
function 场景类_战斗对象:显示(dt,x,y)
	self.刷新.y = 0
	if self.特效 ~= nil then
		self.特效:更新(dt*1.15,nil,tp.场景.战斗.是否拼接 or self.不显示特效)
		if self.特效.当前帧 == floor(self.特效.结束帧 / 2) then
			if self.附加效果.被反震 then
				if self.消失气血 > 0 then
					self:流血逻辑(self.消失气血,"掉血")
					self.消失气血 = 0
					self.特效.被反震 = false
				end
			end
		end
		if self.缓冲 > 0 then
			self.缓冲 = self.缓冲 - 1
		end
		if self.替换特效 ~= nil and self.缓冲 == self.替换特效[1] then
			self.特效 = self:附加特效(self.替换特效[2])
			self.替换特效 = nil
		end
		if self.特效.当前帧 >= self.特效.结束帧 and self.行动特效 == nil and self.缓冲 <= 0 then
			self.特效 = nil
			self.固定特效 = nil
			self.特效偏移 = nil
			self.不显示特效 = nil
			self.特效固定 = nil
			self.固定特效坐标 = nil
			tp.场景.战斗.被遮盖特效 = nil
			tp.场景.战斗.拼接特效 = nil
			tp.场景.战斗.是否拼接 = nil
		end
	end
	for n = 1,#self.后置特效 do
		if self.后置特效[n] ~= nil then
			local v = self.临时.x + self.附加.x
			local b = self.临时.y + self.附加.y
			if self.后置特效[n][2] ~= nil then
				v = v + self.后置特效[n][2][1]
				b = b + self.后置特效[n][2][2]
			end
			self.后置特效[n][1]:更新(dts)
			self.后置特效[n][1]:显示(v,b)
		end
	end
	self.动画[self.状态行为]:显示(self.临时.x + self.附加.x,self.临时.y + self.附加.y)
	if self.武器[self.状态行为] ~= nil then
		self.武器[self.状态行为]:显示(self.临时.x + self.附加.x,self.临时.y + self.附加.y)
	end

	if self.武器特效[self.状态行为] ~= nil then
		self.武器特效[self.状态行为]:显示(self.临时.x + self.附加.x,self.临时.y + self.附加.y)
	end


	for n = 1,#self.前置特效 do
		if self.前置特效[n] ~= nil then
			local v = self.临时.x + self.附加.x
			local b = self.临时.y + self.附加.y
			if self.前置特效[n][2] ~= nil then
				v = v + self.前置特效[n][2][1]
				b = b + self.前置特效[n][2][2]
			end
			self.前置特效[n][1]:更新(dts)
			self.前置特效[n][1]:显示(v,b)
		end
	end

	if self.回合行动 == 1 or self.回合行动 == 2 or self.回合行动 == 15 then
		if self.前置 then
			if self.数据.模型 == "兔子怪" then
				self.刷新.y = 10
			else
				self.刷新.y = 20
			end
		else
			self.刷新.y = -10
		end
	end
	if self.特效固定 then
		self.刷新.y = 200
	end

	if self.动画[self.状态行为]:是否选中(x,y) and not tp.选中UI then
		if tp.场景.战斗.回合进程 == "命令回合" then
			self.刷新.y = 105
			tp.提示:印记(x,y,self.印记)
		end
		if not self.数据.法术状态组.金刚护法 then
			if not self.数据.法术状态组.隐身 then
				if self.武器[self.状态行为] ~= nil then
					self.武器[self.状态行为]:置高亮()
				end
				self.动画[self.状态行为]:置高亮()
			else
				self.动画[self.状态行为]:置混合(2)
				self.动画[self.状态行为]:置颜色(-922746881)
				if self.武器[self.状态行为] ~= nil then
					self.武器[self.状态行为]:置颜色(-922746881)
				end
			end
		end
		self.名称颜色 = 0xFFFF2424
	else
		if self.武器[self.状态行为] ~= nil then
			self.武器[self.状态行为]:取消高亮()
		end
		self.动画[self.状态行为]:取消高亮()
		self.名称颜色 = 4285922956
		-- if self.位置 ~= 3 and tp.场景.战斗.操作单位
		-- and tp.场景.战斗.命令版面
		-- and tp.场景.战斗.回合进程 == "命令回合"
		-- and tp.场景.战斗.参战数据[tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].编号].编号 == self.编号 then
		-- 	self.名称颜色 = 4285922956
		-- end

	end
	if self.位置 ~= 3 then
		tp.场景.战斗.血条背景:显示(floor(self.临时.x + self.附加.x - 23),floor(self.临时.y + self.附加.y -self.高度+5))
		tp.场景.战斗.血条栏:置区域(0,0,math.min((self.数据.当前气血 / self.数据.最大气血),1) * 36,4)
		tp.场景.战斗.血条栏:显示(floor(self.临时.x + self.附加.x -22),floor(self.临时.y + self.附加.y -self.高度 + 6))
	end
	local zt = tp.字体表.人物字体
	zt:置颜色(self.名称颜色)
	zt:置阴影颜色(ARGB(170,0,0,0))
	zt:显示(self.临时.x - self.名称宽度 + 3 + self.附加.x,self.临时.y + 22 + self.附加.y,self.数据.名称)
	if self.特效 ~= nil and tp.场景.战斗.是否拼接 == nil and self.不显示特效 == nil then
		if self.行动特效 == nil then
			local x = {0,0}
			if self.特效偏移 ~= nil then
				x = self.特效偏移
			end
			if self.固定特效 == true then
				if self.固定特效坐标 == nil then
					self.特效:显示(self.原始.x + x[1],self.原始.y + x[2])
				else
					self.特效:显示(self.固定特效坐标[1] + x[1],self.固定特效坐标[2] + x[2])
				end
			else
				self.特效:显示(self.临时.x + self.附加.x + x[1],self.临时.y + self.附加.y + x[2])
			end
		else
			if self.特效影子 ~= nil then
				tp.影子:显示(self.当前.x,self.当前.y)
			end
			self.特效:显示(self.当前.x,self.当前.y)
		end
	end
	if self.数据.法术状态组.金刚护法 then
		self.色相变身 = self.色相变身 + 1
		if self.色相变身 >= 30 then
			self.动画[self.状态行为]:取消高亮()
			if self.武器[self.状态行为] ~= nil then
				self.武器[self.状态行为]:取消高亮()
			end
			if self.色相变身 >= 60 then
				self.色相变身 = 0
			end
		else
			self.动画[self.状态行为]:置高亮模式(-10855936)
			if self.武器[self.状态行为] ~= nil then
				self.武器[self.状态行为]:置高亮模式(-10855936)
			end
		end
	end
	if self.数据.法术状态组.隐身 or self.数据.法术状态组.分身术 then
		self.动画[self.状态行为]:置颜色(1694498815)
		if self.武器[self.状态行为] ~= nil then
			self.武器[self.状态行为]:置颜色(1694498815)
		end
	end
	if self.逃跑开关 then
 self.逃跑特效:更新(dt)
 self.逃跑特效:显示(self.临时.x,self.临时.y)
 	end
 	if self.法暴开关 then
 self.法暴特效:更新(dt)
 self.法暴特效:显示(self.临时.x,self.临时.y)
 	end
	if #self.发言内容 > 0 then
		self.临时宽度 = math.floor(self.发言内容[1].长度) - 2
		if self.临时宽度 > 12 then
			self.临时宽度 = 12
		end
		self.偏差宽度 = self.发言内容[1].长度 * 3 + 5
		if self.偏差宽度 > 60 then
			self.偏差宽度 = 60
		end
		self.聊天背景:置宽高(self.超丰富文本.显示表.宽度+8, self.ab )
		self.偏差高度 = math.floor(self.发言内容[1].长度 * 0.1 + 1) * 5 + 120 - 5
			for i = 1,3 do
				self.聊天背景:显示(self.临时.x + self.附加.x - self.偏差宽度 + 5,self.临时.y + self.附加.y - self.偏差高度)
			end

		self.超丰富文本:显示(self.临时.x + self.附加.x - self.偏差宽度 + 5,self.临时.y + self.附加.y - self.偏差高度)
		if os.time() - self.发言内容[1].起始 >= 5 then
			table.remove(self.发言内容, 1)
		end
	end

	if 取表数量(self.数据.法宝效果) > 0 then
 self:增加状态("法宝",1,"法宝")
	end

	for i = 1,#self.数据.主动技能 do

		if self.数据.主动技能[i].名称 == "光照万象" then

 		self:增加状态("光照万象",1,"光照万象")

 		break;
		end
	end
	if self.特技内容开关 then
		self:特技文本显示()
	end


end
function 场景类_战斗对象:指令更新(dt,x,y)
	if self.行为 == "待战" then
		self.状态行为 = "待战"
		self:动作更新(dt,nil,1.2)
		if self.回合行动 == 3 then
			if tp.场景.战斗.战斗流程[1].类型 == "攻击" then
				if self.攻击方式 == 0 or self.附加效果.攻击 then
					if self.反击 == false and type(tp.场景.战斗.战斗流程[1].挨打方)~= "table"then -- 当自己没有进入反击行为时
					if tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.数据.当前气血 > 0 then -- 敌人可进入
							--检测是否可以触发反击的条件
							if tp.场景.战斗.战斗流程[1].反击数据~= nil then
								if tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.挨打状态 == "正常" then
									self.反击 = true
									self.反击前 = nil
									self.回合行动 = 15
									tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.回合行动 = 17
									tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.反击对象 = true
								end
								return false
							end
						end
					end
					self:行动_攻击完毕()
				end
			end
			if tp.场景.战斗.战斗流程[1].类型 == "技能" and tp.场景.战斗.战斗流程[1].挨打方 then
				local c = false
				local v = 0
			local 挨打 = {}
				for n = 1,#tp.场景.战斗.战斗流程[1].挨打方 do
					挨打[n] = tp.场景.战斗.战斗流程[1].挨打方[n].编号
					if tp.场景.战斗.参战数据[挨打[n]].单位.行动特效 == nil then
						v = v + 1
					else
						if 取两点距离(tp.场景.战斗.参战数据[挨打[n]].单位.当前,tp.场景.战斗.参战数据[挨打[n]].单位.临时) > 30 and tp.场景.战斗.战斗流程[1].参数 ~= "泰山压顶" then
								local 步幅 = 取移动坐标(tp.场景.战斗.参战数据[挨打[n]].单位.当前,tp.场景.战斗.参战数据[挨打[n]].单位.临时,tp.场景.战斗.参战数据[挨打[n]].单位.特效延迟 or 1)
								tp.场景.战斗.参战数据[挨打[n]].单位.当前 = 步幅
						else
							tp.场景.战斗.参战数据[挨打[n]].单位.及时挨打 = true

							if tp.场景.战斗.战斗流程[1].参数 == "泰山压顶" then
								tp.场景.战斗.参战数据[挨打[n]].单位.固定特效 = true
								tp.场景.战斗.参战数据[挨打[n]].单位.缓冲 = 85
								tp.场景.战斗.被遮盖特效 = {}
								tp.场景.战斗.被遮盖特效[2] = {}
								tp.场景.战斗.被遮盖特效[2].特效 = self:附加特效("泰山压顶1")
								tp.场景.战斗.被遮盖特效[2].偏移 = {x = -15,y = -25}
								tp.场景.战斗.被遮盖特效[1] = {}
								tp.场景.战斗.被遮盖特效[1].特效 = self:附加特效("泰山压顶2")
								tp.场景.战斗.被遮盖特效[1].偏移 = {x = 18,y = 23}
								if self.位置 == 3 then
									tp.场景.战斗.拼接偏移.x,tp.场景.战斗.拼接偏移.y = 558+(全局游戏宽度-800)/2,449+(全局游戏高度-600)/2
								else
									tp.场景.战斗.拼接偏移.x,tp.场景.战斗.拼接偏移.y = 278+(全局游戏宽度-800)/2,236+(全局游戏高度-600)/2
								end
								tp.场景.战斗.参战数据[挨打[n]].单位.替换特效 = {10,"泰山压顶3"}
								tp.场景.战斗.背景状态 = 1
							else
								tp.场景.战斗.参战数据[挨打[n]].单位.特效.当前帧 = tp.场景.战斗.参战数据[挨打[n]].单位.特效.结束帧-1
								tp.场景.战斗.参战数据[挨打[n]].单位.特效偏移 = {3,40}
							end
							tp.场景.战斗.参战数据[挨打[n]].单位.行动特效 = nil
							tp.场景.战斗.参战数据[挨打[n]].单位.当前 = {}
							tp.场景.战斗.参战数据[挨打[n]].单位.特效延迟 = nil
							if tp.场景.战斗.战斗流程[1].参数 == "雨落寒沙" then
									-- self.指令目标[self.指令对象[n]]:删除状态("尸腐毒")
									-- self.指令目标[self.指令对象[n]]:增加状态("毒",3,"毒")

							end
							v = v + 1
						end
					end
					if v >= #tp.场景.战斗.战斗流程[1].挨打方 then
						c = true
					end
				end
				if c then
					self:行动_施法完毕()
				end
			end
		elseif self.回合行动 == 4 then
			if tp.场景.战斗.敌方数量 <1 then
				self.行动完毕 = true
			end
			if not self.行动完毕 then
				local 临时编号
				if type(tp.场景.战斗.战斗流程[1].挨打方) == "table" then
					if #tp.场景.战斗.战斗流程[1].挨打方 == 0 then
						self.行动完毕 = true
						return false
					else
					临时编号 = tp.场景.战斗.战斗流程[1].挨打方[1].编号
					end
				else
				临时编号 = tp.场景.战斗.战斗流程[1].挨打方
				end
				if tp.场景.战斗.战斗流程[1].类型 == "技能" then
					-- 处理完毕
					self.技能 = nil
					self.已攻击次数 = 0
					self.行动完毕 = true
					tp.场景.战斗.背景状态 = 0

				elseif tp.场景.战斗.战斗流程[1].类型 == "攻击" and tp.场景.战斗.参战数据[临时编号].单位.挨打状态 == "正常" then
					if tp.场景.战斗.参战数据[临时编号].单位.行为 == "死亡" then
						if not tp.场景.战斗.参战数据[临时编号].单位.真正死亡 then
							return false
						end
					end
					self.已攻击次数 = 0
					self.行动完毕 = true
				end
			end
		elseif self.回合行动 == 5 then
			if tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].保护数据.编号].单位.保护状态 == "路上" then
			self.行为 = "攻击"
			end
		elseif self.回合行动 == 9 then
			local 临时编号 = 0
			if type(tp.场景.战斗.战斗流程[1].挨打方) == "table" then
				临时编号 = tp.场景.战斗.战斗流程[1].挨打方[1].编号
			else
				临时编号 = tp.场景.战斗.战斗流程[1].挨打方
			end
			if tp.场景.战斗.参战数据[临时编号].单位.特效 == nil then
				self.行为 = "返回"
			end
		end
	elseif self.行为 == "跑去" then
		self.状态行为 = "待战"
		local 坐标1 = {x = self.临时.x,y = self.临时.y}
		local 坐标2 = {x = self.目标.x,y = self.目标.y}
		if self.音效开关 == nil then
			local yx = wav(self.音效模型)
			if yx ~= nil then
				self:音效类1_(yx.攻击,yx.资源,"攻击")
			end
			self.音效开关 = 1
		end
		self.状态行为 = "待战"
		if self.攻击方式 == 1 then
			self.行为 = "攻击"
			self.音效开关 = nil
			return false
		end
		self.状态行为 = "跑去"
		if 取两点距离(坐标1,坐标2) > 21 then
			local 步幅 = 取移动坐标(坐标1,坐标2,17)
			self.临时 = 步幅
		else
			self.音效开关 = nil
			if self.位置 == 3 then
				-- if tp.场景.战斗.战斗流程[1].挨打方 < 6 then
				-- 	self.前置 = true
				-- end
			end
			self.临时.x,self.临时.y = ceil(坐标2.x),ceil(坐标2.y)
			if tp.场景.战斗.战斗流程[1] and tp.场景.战斗.战斗流程[1].保护数据 then
				self.行为 = "待战"
				self.回合行动 = 5
			tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].保护数据.编号].单位.保护状态 = "开始"
			else
				self.行为 = "攻击"
			end
			self.音效开关 = nil
		end
 elseif self.行为 == "跑回" then
			self.状态行为 = "跑去"
			self:动作更新(dt,self.反方向)
			local 坐标1 = {x = self.临时.x,y = self.临时.y}
			local 坐标5 = {x = self.原始.x,y = self.原始.y}
			if 取两点距离(坐标1,坐标5) > 15 then
				local 步幅 = 取移动坐标(坐标1,坐标5,17)
				self.临时 = 步幅
			else
				local fx = 0
				if self.位置 == 1 or self.位置 == 2 then
					fx = 2
				end
				self.行为 = "待战"
				self.临时.x = self.原始.x
				self.临时.y = self.原始.y
				self:置方向(fx,true)
				self.前置 = nil
				self.行动完毕 = true
 end
 elseif self.行为 == "逃跑" then
		self.状态行为 = "跑去"
				self:动作更新(dt,self.反方向)
				self.逃跑开关 = true
	elseif self.行为 == "逃跑1" then
			self.时间 = 0
			self.状态行为 = "待战"
			self.方向 = self.初始方向
			self:动作更新(dt,self.方向)
	elseif self.行为 == "逃跑2" then
			self.方向 = self.方向+1
			self.时间 = self.时间+1
 		if self.方向>= 4 then
 			self.方向 = 0
 		end
 		self:动作更新(dt/2,self.方向)
		if self.时间 >= 24 then
			self.方向 = self.初始方向
				self:动作更新(dt,self.方向)
				self.行为 = "待战"
				self.逃跑开关 = false
				self.回合行动 = 0
		table.remove(tp.场景.战斗.战斗流程,1)
		end
	elseif self.行为 == "攻击" then
		self.状态行为 = "攻击"
		local vss = 0
		if self.攻击方式 == 0 and self.动画.攻击.当前帧 >= self.动画.攻击.结束帧 - self.终结帧 - 2 then
			vss = 0.65
		end
		if tp.场景.战斗.战斗流程[1].参数 == nil then
			if not tp.场景.战斗.战斗流程[1].躲避 and (tp.场景.战斗.战斗流程[1].挨打数据.死亡 ~= 0) and tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 ~= nil then
				vss = 1.95
			end
		else
			if tp.场景.战斗.战斗流程[1].参数 ~= "连击" and tp.场景.战斗.战斗流程[1].参数 ~= "理直气壮" then
				tp.场景.战斗提示 = self.数据.名称.."使用了"..tp.场景.战斗.战斗流程[1].参数
			end
			if type(tp.场景.战斗.战斗流程[1].挨打方) == "table" then
				if tp.场景.战斗.战斗流程[1].挨打方[1] and (tp.场景.战斗.战斗流程[1].挨打方[1].死亡 ~= 0 ) and tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位.特效 ~= nil then
					vss = 1.95
				end
			else
				if (tp.场景.战斗.战斗流程[1].动作[1].死亡 ~= 0 ) and tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 ~= nil then
					vss = 1.95
				end
			end
		end
		self:动作更新(dt,nil,self.攻击延迟+vss)
		self:行动_攻击()
	elseif self.行为 == "反击" then
		self.状态行为 = "攻击"
		local vss = 0
		if self.攻击方式 == 0 and self.动画.攻击.当前帧 >= self.动画.攻击.结束帧 - self.终结帧 - 2 then
			vss = 0.63
		end
		self:动作更新(dt,nil,self.攻击延迟+vss)
		self:行动_反击()
	elseif self.行为 == "返回" then
		self.状态行为 = "待战"
		self:动作更新(dt)
		local jnnm = nil
		local fyd = false
		if tp.场景.战斗.战斗流程[1] and tp.场景.战斗.战斗流程[1].参数 then
			jnnm = tp.场景.战斗.战斗流程[1].参数
			self.追加s = 1
		end
		-- 判断是否可以接着攻击，如果不行则返回，行则判断攻击方式和技能
		if tp.场景.战斗.战斗流程[1] and tp.场景.战斗.战斗流程[1].参数 ~= nil then
			if self.攻击次数 <= 0 and self.追加开关 == false then
					self.追加开关 = true
				if tp.场景.战斗.战斗流程[1].免休 == nil and tp.场景.战斗.战斗流程[1].参数 == "横扫千军" or tp.场景.战斗.战斗流程[1].参数 == "破釜沉舟" then
					self:流血逻辑(tp.场景.战斗.战斗流程[1].消耗,"掉血","及时")
					self:增加状态(tp.场景.战斗.战斗流程[1].参数,1,tp.场景.战斗.战斗流程[1].参数)
			elseif tp.场景.战斗.战斗流程[1].参数 == "剑荡四方" then
				self:流血逻辑(tp.场景.战斗.战斗流程[1].消耗,"掉血","及时")
				elseif tp.场景.战斗.战斗流程[1].参数 == "后发制人" then
					if tp.场景.战斗.战斗流程[1].消耗 then
						self:流血逻辑(tp.场景.战斗.战斗流程[1].消耗,"掉血","及时")
					elseif tp.场景.战斗.战斗流程[1].增加 then
						self:流血逻辑(tp.场景.战斗.战斗流程[1].增加,"加血","及时")
					end
					self:删除状态("后发制人")
				elseif tp.场景.战斗.战斗流程[1].免休 == nil and tp.场景.战斗.战斗流程[1].参数 == "鹰击" then
					self:增加状态("鹰击",1,"鹰击")
				elseif tp.场景.战斗.战斗流程[1].参数 == "连环击" then
					self:增加状态("鹰击",1,"鹰击")
					self:删除状态("变身")
				elseif tp.场景.战斗.战斗流程[1].参数 == "象形" then
					self:删除状态("变身")
				end
			end
		end
		if jnnm ~= nil then
			if jnnm == "惊涛怒" or jnnm == "翻江搅海" or jnnm == "鹰击" or jnnm == "浪涌" or jnnm == "牛刀小试" or jnnm == "狮搏"then
				fyd = true
			end
		end
		if jnnm ~= nil and not fyd and self.本回合 == nil and self.攻击次数 > 0 then
			local 临时编号 = 0
			if type(tp.场景.战斗.战斗流程[1].挨打方) == "table" then
				临时编号 = tp.场景.战斗.战斗流程[1].挨打方[1].编号
			else
				临时编号 = tp.场景.战斗.战斗流程[1].挨打方
			end
			local js = false
			if jnnm == "剑荡四方" or jnnm == "破釜沉舟"or jnnm == "进阶力劈华山" or jnnm == "进阶壁垒击破" or jnnm == "进阶惊心一剑" or jnnm == "进阶夜舞倾城" or jnnm == "进阶善恶有报" or jnnm == "天雷斩" or jnnm == "神针撼海" or jnnm == "锋芒毕露" or jnnm == "针锋相对" or jnnm == "百爪狂杀"or jnnm == "飘渺式" then
				self.延时 = self.延时 + 1
				if self.延时 >= 12 then
					js = true
					self.延时 = 0
				end
			end
			if self.攻击次数 <= 0 then
				js = true
			end
			if tp.场景.战斗.参战数据[临时编号].单位.特效 == nil or tp.场景.战斗.参战数据[临时编号].单位.挨打状态 == "正常" or tp.场景.战斗.参战数据[临时编号].单位.挨打状态 == "挨打结束" or js then
				if jnnm == "破血狂攻" or jnnm == "天崩地裂" or jnnm == "断岳势" or jnnm == "烟雨剑法" or jnnm == "连环击"or jnnm == "哼哼哈兮" or jnnm == "横扫千军" or jnnm == "连击"or jnnm == "理直气壮" or jnnm == "天命剑法" then
					if self.攻击次数 > 0 then
						local yx = wav(self.音效模型)
						if yx ~= nil then
							self:音效类1_(yx.攻击,yx.资源,"攻击")
						end
						table.remove(tp.场景.战斗.战斗流程[1].动作,1)
						self.行为 = "攻击"
						self.附加效果.攻击 = false
						return false
					end
				elseif jnnm == "剑荡四方" or jnnm == "破釜沉舟" or jnnm == "进阶力劈华山" or jnnm == "进阶惊心一剑" or jnnm == "进阶夜舞倾城" or jnnm == "进阶壁垒击破" or jnnm == "进阶善恶有报" or jnnm == "天雷斩" or jnnm == "神针撼海" or jnnm == "锋芒毕露" or jnnm == "针锋相对" or jnnm == "飘渺式" or jnnm == "百爪狂杀" then
 table.remove(tp.场景.战斗.战斗流程[1].挨打方,1)
					if #tp.场景.战斗.战斗流程[1].挨打方 > 0 then
						local zl = tp.场景.战斗.战斗流程[1].挨打方[1].编号
							self.目标.x = tp.场景.战斗.参战数据[zl].单位.临时.x - self.差异.x
							self.目标.y = tp.场景.战斗.参战数据[zl].单位.临时.y - self.差异.y
						self.行为 = "跑去"
						self.附加效果.攻击 = false
						return false
					end
				end
			else
				self.行为 = "待战"
				self.回合行动 = 9
				return false
			end
			return false
		end


		if self.临时攻击方式 == 1 then
			self.行为 = "待战"
			local 编号 = 0
			if type(tp.场景.战斗.战斗流程[1].挨打方) == "table" then
				编号 = tp.场景.战斗.战斗流程[1].挨打方[1].编号
			else
				编号 = tp.场景.战斗.战斗流程[1].挨打方
			end
			if tp.场景.战斗.参战数据[编号].单位.特效 ~= nil then
				return
			end
			if self.回合行动 == 18 then
				self.回合行动 = 19
				return false
			end
			self.已攻击次数 = 0
			self.反击 = false
			self.反击对象 = false
			self.回合行动 = 4
				if fyd then
					table.remove(tp.场景.战斗.战斗流程[1].挨打方,1)
					if tp.场景.战斗.战斗流程[1].挨打方 and #tp.场景.战斗.战斗流程[1].挨打方 > 0 then
						local zl = tp.场景.战斗.战斗流程[1].挨打方[1].编号
						self.目标.x = tp.场景.战斗.参战数据[zl].单位.临时.x - self.差异.x
						self.目标.y = tp.场景.战斗.参战数据[zl].单位.临时.y - self.差异.y
						tp.场景.战斗.战斗流程[1].类型 = "攻击"
						self.行为 = "跑去"
						self.附加效果.攻击 = false
						--remove(tp.场景.战斗.战斗流程[1].挨打方, 1)
						return false
					end
				end
			self.行动完毕 = true
			self.追加s = nil
			return false
		else
			local 编号 = 0
			if type(tp.场景.战斗.战斗流程[1].挨打方) == "table" and tp.场景.战斗.战斗流程[1].挨打方[1] then
				编号 = tp.场景.战斗.战斗流程[1].挨打方[1].编号
			else
				编号 = tp.场景.战斗.战斗流程[1].挨打方
			end
			if tp.场景.战斗.参战数据[编号].单位.特效 ~= nil then
				self.行为 = "待战"
				return
			end
			self.状态行为 = "跑去"
			self:动作更新(dt,self.反方向,3.3)
			local 坐标1 = {x = self.临时.x,y = self.临时.y}
			local 坐标5 = {x = self.原始.x,y = self.原始.y}
			if 取两点距离(坐标1,坐标5) > 15 then
				local 步幅 = 取移动坐标(坐标1,坐标5,22)
				self.临时 = 步幅
			else
				local fx = 0
				if self.位置 == 1 or self.位置 == 2 then
					fx = 2
				end
				self.行为 = "待战"
				self.临时.x = self.原始.x
				self.临时.y = self.原始.y
				-- self.攻击次数 = 0
				self:置方向(fx,true)
				self.前置 = nil
				self.已攻击次数 = 0
				self.反击 = false
				self.反击对象 = false
				self.回合行动 = 4
				if fyd then
					table.remove(tp.场景.战斗.战斗流程[1].挨打方,1)
					if #tp.场景.战斗.战斗流程[1].挨打方 > 0 then
						local zl = tp.场景.战斗.战斗流程[1].挨打方[1].编号
						self.目标.x = tp.场景.战斗.参战数据[zl].单位.临时.x - self.差异.x
						self.目标.y = tp.场景.战斗.参战数据[zl].单位.临时.y - self.差异.y
						self.行为 = "跑去"
						self.附加效果.攻击 = false
					return false
					end
				end
				self.追加s = nil
				return false
			end
		end
	elseif self.行为 == "施法" then
		if tp.场景.战斗.战斗流程 == nil or tp.场景.战斗.战斗流程[1] == nil or tp.场景.战斗.战斗流程[1].参数 == nil then
			self.行为 = "待战"
			return
		end
		tp.场景.战斗提示 = self.数据.名称.."使用了"..tp.场景.战斗.战斗流程[1].参数
		self.状态行为 = "施法"
		self:动作更新(dt,self.方向,1.2)
		self:行动_施法()
		if not self.施法前播放 then
			local yx = wav(self.音效模型)
			if yx ~= nil then
				self:音效类1_(yx.施法,yx.资源,"施法")
			end
			self.施法前播放 = true
		end
	elseif self.行为 == "召唤法术" then
		self.状态行为 = "施法"
		self:动作更新(dt,self.方向,1.2)
		if not self.施法前播放 then
			local yx = wav(self.音效模型)
			if yx ~= nil then
				self:音效类1_(yx.施法,yx.资源,"施法")
			end
			self.施法前播放 = true
		end


		if self.动画["施法"].当前帧 == self.动画["施法"].结束帧 then
			self.执行重播 = true
			self.行为 = "待战"
			self.仅播放一次 = false
			self.行动完毕 = true
		end
	elseif self.行为 == "行走" then
		self.状态行为 = "行走"
		self:动作更新(dt)
	elseif self.行为 == "防御" then
		self.状态行为 = "防御"
		if self.特效 ~= nil and self.挨打状态 == "正常" then
 if tp.场景.战斗.战斗流程[1].类型 == "攻击" then
 	if tp.场景.战斗.战斗流程[1].参数 == nil then
 		if self.回合行动 == 15 then
	self:流血逻辑(tp.场景.战斗.战斗流程[1].反击数据.伤害,tp.场景.战斗.战斗流程[1].反击数据.类型)
		else
						self:流血逻辑(tp.场景.战斗.战斗流程[1].挨打数据.伤害,tp.场景.战斗.战斗流程[1].挨打数据.类型)
						if tp.场景.战斗.战斗流程[1].反震数据 then
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.消失气血 = tp.场景.战斗.战斗流程[1].反震数据.伤害
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位:附加特效("反震")
									tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.附加效果.被反震 = true
									if tp.场景.战斗.战斗流程[1].反震数据.死亡~= 0 then
										tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.行为 = "死亡"
										tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.挨打状态 = "正常"
										tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.死亡方式 = tp.场景.战斗.战斗流程[1].反震数据.死亡
									end
						end

					end
 else
 	if type(tp.场景.战斗.战斗流程[1].挨打方) == "table" then
 		self:流血逻辑(tp.场景.战斗.战斗流程[1].挨打方[1].伤害,tp.场景.战斗.战斗流程[1].挨打方[1].类型)
 if tp.场景.战斗.战斗流程[1].挨打方[1].反震 then
					tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.消失气血 = tp.场景.战斗.战斗流程[1].挨打方[1].反震伤害
					tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位:附加特效("反震")
								tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.附加效果.被反震 = true
								if tp.场景.战斗.战斗流程[1].挨打方[1].反震死亡~= 0 then
									tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.行为 = "死亡"
									tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.挨打状态 = "正常"
									tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.死亡方式 = tp.场景.战斗.战斗流程[1].挨打方[1].反震死亡
								end
 end
 	else
 			self:流血逻辑(tp.场景.战斗.战斗流程[1].动作[1].普通伤害,tp.场景.战斗.战斗流程[1].动作[1].伤害类型)
 if tp.场景.战斗.战斗流程[1].动作[1].反震 then
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.消失气血 = tp.场景.战斗.战斗流程[1].动作[1].反震伤害
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位:附加特效("反震")
									tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.附加效果.被反震 = true
								if tp.场景.战斗.战斗流程[1].动作[1].反震死亡~= 0 then
									tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.行为 = "死亡"
									tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.挨打状态 = "正常"
									tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.死亡方式 = tp.场景.战斗.战斗流程[1].动作[1].反震死亡
								end
 end
 	end
 end
 end

			local yx = wav(self.音效模型)
			if yx ~= nil then
				self:音效类1_(yx.防御,yx.资源,"防御")
			end
			self.挨打状态 = "挨打开始"

			self.弹力球 = self.特效:取中间()-1
			self.临时挨打偏移 = xys()
			self.临时挨打速度 = 0
		end
	elseif self.行为 == "死亡" and not self.真正死亡 then
		self.状态行为 = "挨打"
		-- 第一次处理
		self.已攻击次数 = 0
	--if self.特效 == nil then
			-- 处理结束
			if self.死亡方式 == 0 then
				self.挨打状态 = "正常"
				self:置方向(self.方向,true)
			end
			if self.位置 == 1 then
				self.死亡方式 = 1
			end
			if self.位置 ~= 1 and self.死亡方式 == 2 then
				if self.死亡旋转 == false then
					if self.位置 == 2 then
						if self.原始.x > 530 then
							self.临时.x = self.临时.x + 13
							self.临时.y = self.临时.y + 4
						else
							self.临时.x = self.临时.x + 13
							self.临时.y = self.临时.y + 10
						end
						if (self.临时.y > 580) then
							self.死亡计次 = 1
							self.死亡旋转 = true
						elseif (self.临时.x > 920) then
						self.死亡计次 = 2
							self.死亡旋转 = true
						end
					elseif self.位置 == 3 then
						if self.原始.x <= 350 then
							self.临时.x = self.临时.x - 15
							self.临时.y = self.临时.y - 3
							if self.临时.y <= 80 then
								self.死亡旋转 = true
								self.死亡计次 = 1
							elseif self.临时.x <= 80 then
								self.死亡旋转 = true
								self.死亡计次 = 2
							end
						else
							self.临时.x = self.临时.x - 15
							self.临时.y = self.临时.y - 3
							if self.临时.y <= 80 then
								self.死亡旋转 = true
								self.死亡计次 = 1
							elseif self.临时.x <= 100 then
								self.死亡旋转 = true
								self.死亡计次 = 2
							end
					end
					end
				else
					if self.位置 == 2 then
						if self.死亡计次 == 1 then
							self.临时.x = self.临时.x - 13
							self.临时.y = self.临时.y - 3
						elseif self.死亡计次 == 2 then
							self.临时.x = self.临时.x - 4
							self.临时.y = self.临时.y + 11
							if (self.临时.y > 560) then
								self.死亡计次 = 1
							end
						end
					elseif self.位置 == 3 then
						if self.死亡计次 == 1 then
							self.临时.x = self.临时.x + 19
							self.临时.y = self.临时.y +5
						elseif self.死亡计次 == 2 then
							self.临时.x = self.临时.x + 9
							self.临时.y = self.临时.y - 5
							if self.临时.y <= 40 then
								self.死亡旋转 = true
								self.死亡计次 = 1
							end
						elseif self.死亡计次 == 3 then
							self.临时.x = self.临时.x - 6
							self.临时.y = self.临时.y + 5
							if self.临时.y >= 120 then
								self.死亡旋转 = true
								self.死亡计次 = 4
							end
						elseif self.死亡计次 == 4 then
							self.临时.x = self.临时.x + 19
							self.临时.y = self.临时.y + 5
						end
					end
					if (self.临时.x > 1050 or self.临时.y > 670) or (self.临时.x < -350 or self.临时.y < -350) then
						self.挨打状态 = "正常"
						self.死亡方式 = 3
						self.真正死亡 = true
						tp.场景.战斗.敌方数量 = tp.场景.战斗.敌方数量 -1
						self.行动完毕 = true
						self.特效 = nil
						self.死亡旋转 = false
						return
					end

				end
			end

			if (self.位置 == 2 or self.位置 == 3) and self.死亡方式 == 2 then
				if self.方向 >= 3 then
					self.方向 = 0
				else
					self.方向 = self.方向+1
				end
				self.动画.挨打:置方向(self.方向)
				if self.武器.挨打 ~= nil then
					self.武器.挨打:置方向(self.方向)
				end
			end
			if self.位置 == 1 or self.死亡方式 == 1 then
				self.状态行为 = "死亡"
				self:动作更新(dt)
				self.附加.x,self.附加.y = 0,0
				if not self.仅播放一次 then
					local yx = wav(self.音效模型)
					if yx ~= nil then
						self:音效类1_(yx.死亡,yx.资源,"死亡")
						self.仅播放一次 = true
					end
				end
				if self.动画["死亡"].当前帧 >= self.动画["死亡"].结束帧 then
					if self.回合行动 < 2 then
						local x = xys(self.原始.x,self.原始.y)
						self.临时.x = x.x
						self.临时.y = x.y
					end
						self:增加状态("鬼魂术",4)
					self.数据.愤怒 = 0
					self.挨打状态 = "正常"
					self.死亡方式 = 1
					self.回合行动 = 4
					self.真正死亡 = true
					self.行动完毕 = true
				end
			--end
		end
		-- 执行攻击完毕返回的逻辑
 elseif self.行为 == "使用道具" then
		tp.场景.战斗提示 = self.数据.名称.."使用了"..tp.场景.战斗.战斗流程[1].名称
	self.状态行为 = "施法"
		self:动作更新(dt,self.方向,1.2)
		if not self.施法前播放 then
			local yx = wav(self.音效模型)
			if yx ~= nil then
				self:音效类1_(yx.施法,yx.资源,"施法")
			end
			self.施法前播放 = true
		end
	if self.动画["施法"]:取间隔() == self.动画["施法"]:取中间() then
		if tp.场景.战斗.战斗流程[1].参数 == "加血" then
			tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位:流血逻辑(tp.场景.战斗.战斗流程[1].数额,"加血")
			tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 = self:附加特效("加血")
		elseif tp.场景.战斗.战斗流程[1].参数 == "伤势" then
				tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 = self:附加特效("加血")
				tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.数据.气血上限 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.数据.气血上限 + tp.场景.战斗.战斗流程[1].数额
			if tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.数据.气血上限 >= tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.数据.最大气血 then
				tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.数据.气血上限 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.数据.最大气血
			end
		elseif tp.场景.战斗.战斗流程[1].参数 == "复活" then
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 = self:附加特效("加血","加血")
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.行为 = "待战"
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.回合行动 = 0
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.真正死亡 = false
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.死亡方式 = 0
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位:流血逻辑(tp.场景.战斗.战斗流程[1].数额,"加血")

		elseif tp.场景.战斗.战斗流程[1].参数 == "加蓝" then
			tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 = self:附加特效("加蓝")
			tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.数据.当前魔法 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.数据.当前魔法 + tp.场景.战斗.战斗流程[1].数额
				if tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.数据.当前魔法 >= tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.数据.魔法上限 then
				tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.数据.当前魔法 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.数据.魔法上限
			end
		end
		if tp.场景.战斗.战斗流程[1].名称 == "五龙丹" then
				local 解除名称 = {"离魂符","失心符","追魂符","催眠符","定身符","失魂符","失忆符","日月乾坤","莲步轻舞","如花解语","似玉生香","娉婷嬝娜","一笑倾城","镇妖","错乱","百万神兵","紧箍咒","尸腐毒","象形","含情脉脉","天罗地网","瘴气","夺魄令","煞气诀","雾杀","毒","落魄符"}
				for n = 1,#解除名称 do
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位:删除状态(解除名称[n])
				end
		end
	end
		if self.动画["施法"].当前帧 == self.动画["施法"].结束帧 then
		self.执行重播 = true
		self.行为 = "待战"
		self.仅播放一次 = false
		self.行动完毕 = true
		end
 elseif self.行为 == "增益法术" then
 		tp.场景.战斗提示 = self.数据.名称.."使用了"..tp.场景.战斗.战斗流程[1].参数
		self.状态行为 = "施法"
		self:动作更新(dt,self.方向,1.2)
		if not self.施法前播放 then
			local yx = wav(self.音效模型)
			if yx ~= nil then
				self:音效类1_(yx.施法,yx.资源,"施法")
			end
			self.施法前播放 = true
		end
		if self.动画["施法"]:取间隔() == self.动画["施法"]:取中间() then
			if type(tp.场景.战斗.战斗流程[1].挨打方) == "table" then
				local 临时编号 = {}
				for i = 1,#tp.场景.战斗.战斗流程[1].挨打方 do
					if tp.场景.战斗.战斗流程[1].参数~= "九幽" then
		临时编号[i] = tp.场景.战斗.战斗流程[1].挨打方[i].编号
						tp.场景.战斗.参战数据[临时编号[i]].单位.特效 = self:附加特效(tp.场景.战斗.战斗流程[1].参数)
					end
				end
			else
				local skill = tp.场景.战斗.战斗流程[1].参数
				if tp.场景.战斗.战斗流程[1].挨打方 and not (skill == "灵法"or skill == "灵断"or skill == "隐身" or skill == "护佑"or skill == "御风"or skill == "怒吼"or skill == "阳护"or 
					skill == "灵刃"or skill == "瞬击"or skill == "瞬法"or skill == "开门见山" or skill == "高级遗志"or skill == "遗志"or skill == "高级进击必杀"or skill == "进击必杀"or skill == "高级进击法暴"or skill == "进击法暴")then 
					tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 = self:附加特效(skill)
			end
				if (tp.场景.战斗.战斗流程[1].参数 == "勾魂" or tp.场景.战斗.战斗流程[1].参数 == "炽火流离" or tp.场景.战斗.战斗流程[1].参数 == "摄魄") then
					if tp.场景.战斗.战斗流程[1].封印结果 then
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效固定 = true
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.行为 = "挨打"

					end
			end
			end
		end
		if self.动画["施法"].当前帧 == self.动画["施法"].结束帧 then
			self.执行重播 = true
			self.行为 = "待战"
			self.仅播放一次 = false
			self.行动完毕 = true
		end
	elseif self.行为 == "挨打" then
		if self.挨打状态 == "正常" then
			self.状态行为 = "待战" -- 打击感的产生
			self:动作更新(dt)
		else
			self.状态行为 = "挨打"
		end
		if self.行动特效 ~= nil then
			return false
		end
		if self.特效 ~= nil and (self.挨打状态 == "正常" or self.挨打状态 == "挨打跳转") or self.及时挨打 then
			local v = 0
			if not self.及时挨打 then
				if self.特效.结束帧 <= 13 then
					v = 8
				elseif self.特效.结束帧 > 13 and self.特效.结束帧 <= 20 then
					v = 6
				elseif self.特效.结束帧 > 20 and self.特效.结束帧 <= 28 then
					v = 4
				end
			end

			--self.特效:取间隔() >= self.特效:取中间() + (6 - v) + self.特效附加帧 or
			if self.及时挨打 or (tp.场景.战斗.战斗流程[1].类型 == "技能" and ( self.特效.当前帧 == self.特效.结束帧-2 ) or tp.场景.战斗.战斗流程[1].类型 == "攻击" ) then
 if tp.场景.战斗.战斗流程[1].类型 == "攻击" then
 	if tp.场景.战斗.战斗流程[1].参数 == nil then
 		if self.回合行动 == 15 then
 self:流血逻辑(tp.场景.战斗.战斗流程[1].反击数据.伤害,tp.场景.战斗.战斗流程[1].反击数据.类型)
 	else
					self:流血逻辑(tp.场景.战斗.战斗流程[1].挨打数据.伤害,tp.场景.战斗.战斗流程[1].挨打数据.类型)
					if tp.场景.战斗.战斗流程[1].反震数据 then
					tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.消失气血 = tp.场景.战斗.战斗流程[1].反震数据.伤害
					tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位:附加特效("反震")
								tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.附加效果.被反震 = true
								if tp.场景.战斗.战斗流程[1].反震数据.死亡~= 0 then
									tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.行为 = "死亡"
									tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.挨打状态 = "正常"
									tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.死亡方式 = tp.场景.战斗.战斗流程[1].反震数据.死亡
								end
					end

				end
 else
 	if type(tp.场景.战斗.战斗流程[1].挨打方) == "table" then
 self:流血逻辑(tp.场景.战斗.战斗流程[1].挨打方[1].伤害,tp.场景.战斗.战斗流程[1].挨打方[1].类型)
 if tp.场景.战斗.战斗流程[1].挨打方[1].反震 then
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.消失气血 = tp.场景.战斗.战斗流程[1].挨打方[1].反震伤害
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位:附加特效("反震")
									tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.附加效果.被反震 = true
									if tp.场景.战斗.战斗流程[1].挨打方[1].反震死亡~= 0 then
										tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.行为 = "死亡"
										tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.挨打状态 = "正常"
										tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.死亡方式 = tp.场景.战斗.战斗流程[1].挨打方[1].反震死亡
									end
 end
 	else
 	self:流血逻辑(tp.场景.战斗.战斗流程[1].动作[1].普通伤害,tp.场景.战斗.战斗流程[1].动作[1].伤害类型)
 if tp.场景.战斗.战斗流程[1].动作[1].反震 then
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.消失气血 = tp.场景.战斗.战斗流程[1].动作[1].反震伤害
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位:附加特效("反震")
									tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.附加效果.被反震 = true
									if tp.场景.战斗.战斗流程[1].动作[1].反震死亡~= 0 then
										tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.行为 = "死亡"
										tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.挨打状态 = "正常"
										tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.死亡方式 = tp.场景.战斗.战斗流程[1].动作[1].反震死亡
									end
 end

 	end
 end
 elseif tp.场景.战斗.战斗流程[1].类型 == "技能" then
 	if tp.场景.战斗.战斗流程[1].参数 == "勾魂" or tp.场景.战斗.战斗流程[1].参数 == "炽火流离" or tp.场景.战斗.战斗流程[1].参数 == "摄魄" then
 		if tp.场景.战斗.战斗流程[1].参数 == "勾魂" or tp.场景.战斗.战斗流程[1].参数 == "炽火流离" then
						self:流血逻辑(tp.场景.战斗.战斗流程[1].法术伤害,"掉血")
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位:流血逻辑(tp.场景.战斗.战斗流程[1].恢复伤害,"加血")
						end
					elseif tp.场景.战斗.战斗流程[1].参数 == "血雨" then
						self:流血逻辑(tp.场景.战斗.战斗流程[1].挨打方[1].伤害,"掉血")
 tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位:流血逻辑(tp.场景.战斗.战斗流程[1].消耗,"掉血")
 	else
 	for o = 1,#tp.场景.战斗.战斗流程[1].挨打方 do
 		if tp.场景.战斗.战斗流程[1].挨打方[o].编号 == self.编号 then

								if tp.场景.战斗.战斗流程[1].挨打方[o].降魔斗篷 then
								tp.场景.战斗.参战数据[self.编号].单位.特效 = tp.场景.战斗.参战数据[self.编号].单位:附加特效("降魔斗篷")
								end
								if tp.场景.战斗.战斗流程[1].挨打方[o].法术暴击 then
 -- if tp.场景.战斗.参战数据[self.编号].单位.不显示特效 or tp.场景.战斗.战斗流程[1].参数 == "泰山压顶" then
 	tp.场景.战斗.参战数据[self.编号].单位.法暴开关 = true
 -- else
 -- 	tp.场景.战斗.参战数据[self.编号].单位.特效 = tp.场景.战斗.参战数据[self.编号].单位:附加特效("法暴")
 -- end
								end
								if tp.场景.战斗.战斗流程[1].法术吸血 then
 tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位:流血逻辑(tp.场景.战斗.战斗流程[1].法术吸血.伤害,"加血")
								end
								if tp.场景.战斗.战斗流程[1].挨打方[o].电芒 then
 tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[o].编号].单位:增加状态("电芒",4,"电芒")
 elseif tp.场景.战斗.战斗流程[1].挨打方[o].毒 then
 tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[o].编号].单位:增加状态("毒",4,"毒")
								end
								self:流血逻辑(tp.场景.战斗.战斗流程[1].挨打方[o].伤害,tp.场景.战斗.战斗流程[1].挨打方[o].类型,nil,100)
 			break
 		end
 	end
 end
 end
				local yx = wav(self.音效模型)
				if yx ~= nil then
					self:音效类1_(yx.被击中,yx.资源,"被击中")
					if self.音效模型 == "画魂" or self.音效模型 == "犀牛将军兽形" then
						self:音效类1_(yx.被击中,yx.资源,"被击中")
					end
				end
				self.临时挨打偏移 = xys()
				self.临时挨打速度 = 0
				if tp.场景.战斗.战斗流程[1].类型 == "技能" then
					local fjz = self.特效附加帧-1
					if fjz <= 0 then
						fjz = 0
					end
					if self.特效 ~= nil and not self.及时挨打 then
						self.弹力球 = self.特效:取中间() + (6 - v) + fjz + 4
					else
						self.挨打延时 = 18
					end
					self.临时挨打偏移.x = 4
					self.临时挨打偏移.y = 5
				else
					if tp.场景.战斗.战斗流程[1].参数 == nil then
						if self.特效.结束帧 >= 14 then
							self.弹力球 = self.特效.结束帧-10
						else
							self.弹力球 = self.特效.结束帧-3
						end
					elseif tp.场景.战斗.战斗流程[1].参数 ~= "溅射"then
						if self.特效.结束帧 <= 10 then
							self.弹力球 = self.特效:取中间()
						else
							self.弹力球 = self.特效:取中间()-1
						end
					end
					if tp.场景.战斗.战斗流程[1].参数 == nil then
						if tp.场景.战斗.战斗流程[1].挨打数据.类型 == "防御" then
							self.弹力球 = self.特效.结束帧-3
						end
					else
						if type(tp.场景.战斗.战斗流程[1].挨打方) == "table" then
						if tp.场景.战斗.战斗流程[1].挨打方[1].防御动作 == 1 then
								self.弹力球 = self.特效.结束帧-3
							end
						else
						if tp.场景.战斗.战斗流程[1].动作[1].挨打动作 == "防御" then
								self.弹力球 = self.特效.结束帧-3
							end
						end
					end
				end
				if self.特效 ~= nil and self.特效.结束帧 < 4 then
					self.弹力球 = self.特效.结束帧 - 1
				end
				self.及时挨打 = false
				self.特效附加帧 = 0
				self.挨打状态 = "挨打开始"
				self.抖动开关 = false
			elseif self.特效.当前帧 >= self.特效:取中间() then
	
					self.挨打状态 = "挨打跳转"
					local x = nil
					if self.位置 == 3 then
						x = xys(-5,-5)
					else
						x = xys(5,5)
					end
					
					if 取两点距离(self.附加,x) > 3 and self.抖动开关 == false then
						self.附加 = 取移动坐标(self.附加,x,0.5)
					else
						self.附加 = 取移动坐标(self.附加,x,-0.5)
						
						self.抖动开关 = true
						if 取两点距离(self.附加,x) > 5 then
							self.抖动开关 = false
						end
					end

			end

		end

		if not self.附加效果.挨打开始 then
			local vs = self.动画.挨打.结束帧
			if self.位置 == 1 or self.位置 == 2 then
				vs = self.动画.挨打.开始帧+1
			end
			if self.数据.模型 == "马面" then
				vs = self.动画.挨打.开始帧*2
			end
			self.动画.挨打.当前帧 = vs
			self.动画.挨打:更新纹理();
			if self.武器.挨打 ~= nil then
				self.武器.挨打.当前帧 = vs
				self.武器.挨打:更新纹理();
			end
			self.附加效果.挨打开始 = true
		end
	end
	if self.回合行动 == 15 then
		if self.反击 and tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.反击对象 then
			if not self.附加效果.开始反击 then -- 如果这个开关开启，则意味着开始反击了，那么就不再进行反击指令
				tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.临时.x = self.临时.x - tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.差异.x
				tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.临时.y = self.临时.y - tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.差异.y
				local yx = wav(tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.音效模型)
				if yx ~= nil then
					self:音效类1_(yx.攻击,yx.资源,"攻击")
				end
				tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.行为 = "反击"
				self.附加效果.开始反击 = true
			else
				if tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.回合行动 == 16 and self.挨打状态 == "正常" and self.特效 == nil then
					tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.攻击方式 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.临时攻击方式
					if not tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.行动完毕 then
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.回合行动 = 0
					end
					tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.附加效果.攻击 = false
					tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.反击 = false
					tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.反击对象 = false
					--self.回合行动 = 4
					if tp.场景.战斗.战斗流程[1].反击数据.死亡 ~= 0 then

							self.行为 = "死亡"
							self.挨打状态 = "正常"
							self.死亡方式 = tp.场景.战斗.战斗流程[1].反击数据.死亡
					else
						self.行为 = "返回"
					end
					self.附加效果.开始反击 = false
					self.反击 = false
				end
			end
		end
	end
	if self.挨打状态 == "挨打开始" then
		local x = nil
		if self.位置 == 3 then
			x = xys(-35-self.临时挨打偏移.x,-35-self.临时挨打偏移.y)
		else
			x = xys(35+self.临时挨打偏移.x,35+self.临时挨打偏移.y)
		end
		local xx = xys(0,0)
		local jd = 0.287
		if self.位置 == 1 or self.位置 == 2 then
			jd = 0.284
		end
		if self.挨打延时 > 0 then
			self.挨打延时 = self.挨打延时 - 1
			self.附加 = 取移动坐标(self.附加,x,jd)
		end
		if (self.特效 == nil or self.特效.当前帧-1 >= self.弹力球) and self.挨打延时 <= 0 then
			jd = 3.5+self.临时挨打速度
		end
		
		if 取两点距离(self.附加,x) > 3 then
			self.附加 = 取移动坐标(self.附加,x,jd)

		else

			self.挨打延时 = 0
			self.临时挨打偏移.x,self.临时挨打偏移.y = 0,0
			self.临时挨打速度 = 0
			self.附加.x,self.附加.y = x.x,x.y
			self.挨打状态 = "挨打结束"
			self.弹力球 = 0
			if tp.场景.战斗.战斗流程[1] ~= nil then
					if tp.场景.战斗.战斗流程[1].类型 == "攻击" then
						if tp.场景.战斗.战斗流程[1].参数 == nil then
								if tp.场景.战斗.战斗流程[1].挨打数据.死亡~= 0 then
									self.行为 = "死亡"
									self.挨打状态 = "正常"
									self.死亡方式 = tp.场景.战斗.战斗流程[1].挨打数据.死亡
								end
						else
							if type(tp.场景.战斗.战斗流程[1].挨打方) == "table" then
								if tp.场景.战斗.战斗流程[1].参数 == "溅射" then
									for i = 1,#tp.场景.战斗.战斗流程[1].挨打方 do
										if tp.场景.战斗.战斗流程[1].挨打方[i].死亡 ~= 0 then
											tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[i].编号].单位.行为 = "死亡"
											tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[i].编号].单位.挨打状态 = "正常"
											tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[i].编号].单位.死亡方式 = tp.场景.战斗.战斗流程[1].挨打方[i].死亡
										end
									end
								else
								if tp.场景.战斗.战斗流程[1].挨打方[1].死亡 ~= 0 then
										tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位.行为 = "死亡"
										tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位.挨打状态 = "正常"
										tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位.死亡方式 = tp.场景.战斗.战斗流程[1].挨打方[1].死亡
									end
								end

							else
								if tp.场景.战斗.战斗流程[1].动作[1].死亡~= 0 and tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.攻击次数 <= 0 then
									self.行为 = "死亡"
									self.挨打状态 = "正常"
									self.死亡方式 = tp.场景.战斗.战斗流程[1].动作[1].死亡
								end
							end

					end
					elseif tp.场景.战斗.战斗流程[1].类型 == "技能" then
						if tp.场景.战斗.战斗流程[1].参数 == "勾魂" or tp.场景.战斗.战斗流程[1].参数 == "炽火流离" or tp.场景.战斗.战斗流程[1].参数 == "摄魄" then
						if tp.场景.战斗.战斗流程[1].死亡~= 0 then
									self.行为 = "死亡"
								self.挨打状态 = "正常"
								self.死亡方式 = tp.场景.战斗.战斗流程[1].死亡
							end
						elseif tp.场景.战斗.战斗流程[1] and tp.场景.战斗.战斗流程[1].挨打方 then
							for i = 1,#tp.场景.战斗.战斗流程[1].挨打方 do
								if tp.场景.战斗.战斗流程[1].挨打方[i].编号 == self.编号 then
									if tp.场景.战斗.战斗流程[1].挨打方[i].死亡~= 0 then
									self.行为 = "死亡"
								self.挨打状态 = "正常"
								self.死亡方式 = tp.场景.战斗.战斗流程[1].挨打方[i].死亡
									break
								end
							end
							end
						end
					end
			end
		end
	elseif self.挨打状态 == "挨打结束" then
		local x = xys(0,0)
		self.挨打延时 = self.挨打延时 + dt
		if self.挨打延时 >= 0.06 then
			if 取两点距离(self.附加,x) > 7 then
				self.附加 = 取移动坐标(self.附加,x,4)
			else
				self.挨打计时 = 0
				self.附加.x,self.附加.y = 0,0
				if self.多段伤害 ~= nil then
					if #self.多段伤害 > 0 then
						self.挨打状态 = "挨打跳转"
						self.行为 = "挨打"
						self.特效 = self:附加特效(self.多段伤害[1][1])
						self.消失气血 = self.多段伤害[1][2].伤害
						self.消失魔法 = self.多段伤害[1][2].减少魔法
						remove(self.多段伤害,1)
						return false
					end
				end
				self.多段伤害 = nil
				--self.被指令目标.特殊效果.月光 = false
				if self.行为 ~= "攻击" and self.行为 ~= "施法" and self.行为 ~= "跑去" and self.行为 ~= "死亡" then
					self.行为 = "待战"
				end
				if self.特效 == nil or tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.攻击次数 <= 0 then
					self.挨打状态 = "正常"
					if self.回合行动~= 15 then
					self.行动完毕 = true
					end
				end
				if self.流血显示 then
					self.流血时间 = self.流血时间 + 2
				end
				if self.数据.法术状态组["催眠符"] then
				self:删除状态("催眠符")
				end
				self.附加效果.挨打开始 = false
				self.挨打延时 = 0
				self.缓慢延迟 = 0
				self.临时挨打偏移 = nil
				self.临时挨打速度 = nil

			end
		end
	end
	-- 躲避状态的处理
	if self.弓箭攻击 then
		if self.特效 ~= nil then

			if 取两点距离(self.当前,self.目标) > 50 then


				local 步幅 = 取移动坐标(self.当前,self.目标,22)
				self.当前 = 步幅
			else
				if self.位置 ~= 3 then
					self.目标.x = self.目标.x+68
					self.目标.y = self.目标.y-10
				else
					self.目标.x = self.目标.x-48
					self.目标.y = self.目标.y-58
				end

				self:行动_攻击(true)
				self.特效 = nil
				self.行动特效 = nil
				--self.当前 = {}
				self.当前 = {x = self.临时.x,y = self.临时.y}
			end
		end
	end
	if self.躲避状态 == "开始" then
		if self.位置 == 3 then
			local x = xys(-35,-25)
			if 取两点距离(self.附加,x) > 8 then
				local 步幅 = 取移动坐标(self.附加,x,8)
				self.附加 = 步幅
			else
				self.附加.x = x.x
				self.附加.y = x.y
			end
		else
			local x = xys(35,25)
			if 取两点距离(self.附加,x) > 8 then
				local 步幅 = 取移动坐标(self.附加,x,8)
				self.附加 = 步幅
			else
				self.附加.x = x.x
				self.附加.y = x.y
			end
		end
	end
	if self.躲避状态 == "路上" then
		local x = xys(0,0)
		if 取两点距离(self.附加,x) > 8 then
			local 步幅 = 取移动坐标(self.附加,x,8)
			self.附加 = 步幅
		else
			self.附加.x = x.x
			self.附加.y = x.y
			self.躲避状态 = false
				if self.特效 == nil or tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.攻击次数 <= 0 then
					self.挨打状态 = "正常"
					if self.回合行动~= 15 then
					self.行动完毕 = true
					end
				end
		end
	end
	if self.保护状态 == "开始" then


		local 目标 = {}
				目标.x = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.临时.x+tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.差异.x
			目标.y = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.临时.y+tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.差异.y
			if 取两点距离(self.临时,目标) > 8 then
				local 步幅 = 取移动坐标(self.临时,目标,8)
				self.临时 = 步幅
			else
				self.保护状态 = "路上"
			end
	end
	if self.保护状态 == "结束" then

		if 取两点距离(self.临时,self.原始) > 8 then
			local 步幅 = 取移动坐标(self.临时,self.原始,8)
			self.临时 = 步幅
		else
			self.临时.x = self.原始.x
			self.临时.y = self.原始.y
			self.保护状态 = false
				if self.特效 == nil or tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.攻击次数 <= 0 then
					self.状态行为 = "待战"
					if self.回合行动~= 15 then
					self.行动完毕 = true
					end
				end
		end
	end
	if self.执行重播 then
		self.动画.攻击:置方向(self.方向,true)
		if self.武器.攻击 ~= nil then
			self.武器.攻击:置方向(self.方向,true)
		end
		self.动画.施法:置方向(self.方向,true)
		if self.武器.施法 ~= nil then
			self.武器.施法:置方向(self.方向,true)
		end
		self.执行重播 = nil
	end
end


function 场景类_战斗对象:行动_攻击(ts) -- 开关判断进程远程
	-- 检测躲避
	if tp.场景.战斗.战斗流程[1].躲避 then
		tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.躲避状态 = "开始"
	end
	if tp.场景.战斗.战斗流程[1].参数 ~= nil then
		if self.动画.攻击:取间隔() >= self.动画.攻击:取中间()-2 then
			if not self.仅播放一次 then
				local yx = wav(tp.场景.战斗.战斗流程[1].参数)
				if yx ~= nil then
					self:音效类1_(yx.文件,yx.资源,"定义")
				end
				self.仅播放一次 = true
			end
		end
	end
	-- 近距离
	if self.攻击方式 == 0 or ts then
		if (self.动画.攻击:取间隔() >= self.动画.攻击:取中间()+self.攻击帧 or self.动画.攻击.当前帧 == self.动画.攻击.结束帧-1 or ts ) and self.附加效果.攻击 == false then
			self.附加效果.攻击 = true
			self.已攻击次数 = self.已攻击次数 + 1
			if type(tp.场景.战斗.战斗流程[1].挨打方) == "table" then
				tp.场景.战斗提示 = self.数据.名称.."使用了"..tp.场景.战斗.战斗流程[1].参数
					if tp.场景.战斗.战斗流程[1].挨打方[1].防御动作 ~= 1 then
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位.行为 = "挨打"
						if tp.场景.战斗.战斗流程[1].挨打方[1].必杀动作 == "必杀" then
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位.被暴击 = true
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位:附加特效("暴击")
						end
						if tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位.特效 == nil then
							if tp.场景.战斗.战斗流程[1].金甲仙衣 then
							tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位:附加特效("金甲仙衣")
							else
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位:附加特效("被击中")
							end
						end
 	if tp.场景.战斗.战斗流程[1].挨打方[1].吸血 then
							self:流血逻辑(tp.场景.战斗.战斗流程[1].挨打方[1].吸血伤害.伤害,tp.场景.战斗.战斗流程[1].挨打方[1].吸血伤害.类型,"及时")
 	elseif tp.场景.战斗.战斗流程[1].挨打方[1].动作 ~= nil and tp.场景.战斗.战斗流程[1].挨打方[1].动作[1].吸血 then
							self:流血逻辑(tp.场景.战斗.战斗流程[1].挨打方[1].动作[1].吸血伤害.伤害,tp.场景.战斗.战斗流程[1].挨打方[1].动作[1].吸血伤害.类型,"及时")
						end
					else
					tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位.行为 = "防御"
					tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位:附加特效("防御")
					end
				self.攻击次数 = self.攻击次数 - 1
				if tp.场景.战斗.战斗流程[1].参数~= "惊涛怒" and tp.场景.战斗.战斗流程[1].参数~= "破釜沉舟" and tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位.被暴击 == false then
					tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位:附加特效(tp.场景.战斗.战斗流程[1].参数)
				tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位.固定特效 = true

				end
				if tp.场景.战斗.战斗流程[1].参数 == "锋芒毕露" then
 
 						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位:增加状态("锋芒毕露",10,"锋芒毕露")
					
				end
			else
			if tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.躲避状态 ~= "开始" then

					if tp.场景.战斗.战斗流程[1].参数 ~= nil then
						if tp.场景.战斗.战斗流程[1].动作[1].挨打动作 ~= "防御" then
							tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.行为 = "挨打"
							if tp.场景.战斗.战斗流程[1].动作[1].必杀动作 == "必杀" then
							tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.被暴击 = true
							tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位:附加特效("暴击")
							end
							if tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 == nil then
								if tp.场景.战斗.战斗流程[1].金甲仙衣 then
								tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位:附加特效("金甲仙衣")
								else
								tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位:附加特效("被击中")
								end
							end
						else
							tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.行为 = "防御"
							tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位:附加特效("防御")
						end
						self.攻击次数 = self.攻击次数 - 1
						if tp.场景.战斗.战斗流程[1].参数 ~= "后发制人" and tp.场景.战斗.战斗流程[1].参数 ~= "满天花雨" and tp.场景.战斗.战斗流程[1].参数 ~= "力劈华山" and tp.场景.战斗.战斗流程[1].参数 ~= "连击" and tp.场景.战斗.战斗流程[1].参数 ~= "理直气壮" and tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.被暴击 == false then
							tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位:附加特效(tp.场景.战斗.战斗流程[1].参数)
					if tp.场景.战斗.战斗流程[1].参数 == "象形" then
					tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位:	增加状态("象形",5,"象形")
						elseif tp.场景.战斗.战斗流程[1].参数 == "死亡召唤" and tp.场景.战斗.战斗流程[1].封印结果 then
 								tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位:增加状态("死亡召唤",10,"死亡召唤")
 						elseif tp.场景.战斗.战斗流程[1].参数 == "诱袭" then
 
 									tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位:增加状态("诱袭",10,"诱袭")
					
					end
						end
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.固定特效 = true
					else
						if tp.场景.战斗.战斗流程[1].挨打动作 ~= "防御" then
							tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.行为 = "挨打"
							if tp.场景.战斗.战斗流程[1].必杀动作 ~= nil then
							tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.被暴击 = true
							tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位:附加特效("暴击")
							end
							if tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 == nil then
								if tp.场景.战斗.战斗流程[1].金甲仙衣 then
								tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位:附加特效("金甲仙衣")
								else
								tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位:附加特效("被击中")
								end
							end
						else
							tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.行为 = "防御"
							tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位:附加特效("防御")
						end
						self.攻击次数 = self.攻击次数 - 1
					end
					if tp.场景.战斗.战斗流程[1].吸血 then
						self:流血逻辑(tp.场景.战斗.战斗流程[1].吸血伤害.伤害,tp.场景.战斗.战斗流程[1].吸血伤害.类型,"及时")
					elseif tp.场景.战斗.战斗流程[1].动作 ~= nil and tp.场景.战斗.战斗流程[1].动作[1].吸血 then
						self:流血逻辑(tp.场景.战斗.战斗流程[1].动作[1].吸血伤害.伤害,tp.场景.战斗.战斗流程[1].动作[1].吸血伤害.类型,"及时")
					end
					if tp.场景.战斗.战斗流程[1].毒 then
						tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位:增加状态("毒",10,"毒")
					end
				end
			end
			if tp.场景.战斗.战斗流程[1].保护数据 then
				tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].保护数据.编号].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].保护数据.编号].单位:附加特效("被击中")
				tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].保护数据.编号].单位.状态行为 = "挨打"
				tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].保护数据.编号].单位:流血逻辑(tp.场景.战斗.战斗流程[1].保护数据.伤害,tp.场景.战斗.战斗流程[1].保护数据.类型)
				tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].保护数据.编号].单位.保护状态 = "结束"
			end
	end
	elseif self.攻击方式 == 1 then
		if (self.动画.攻击:取间隔() >= self.动画.攻击:取中间()-1) then
			if self.弓箭攻击 == nil then
				self.特效 = self:附加特效("弓弩攻击")
				if self.位置 ~= 3 then
					self.目标.x = self.目标.x-68
					self.目标.y = self.目标.y+10--90
				else
					self.目标.x = self.目标.x+48
					self.目标.y = self.目标.y+58--90
				end
				local jd = bfs(floor(jds(self.临时,self.目标))-10)
				self.特效:置方向(jd)
				self.行动特效 = true
				self.当前 = {x = self.临时.x,y = self.临时.y}
				self.弓箭攻击 = true
			end
		end
	end
	if self.动画.攻击.当前帧 == self.动画.攻击.结束帧 - self.终结帧 then
		if type(tp.场景.战斗.战斗流程[1].挨打方) == "table" then
			if tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位.躲避状态 == "开始" then
			tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方[1].编号].单位.躲避状态 = "路上"
			end
		else
			if tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.躲避状态 == "开始" then
			tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.躲避状态 = "路上"
			end
		end

		self.行为 = "待战"
		self.执行重播 = true
		if self.反击对象 == true then
			self.临时.x = self.原始.x
			self.临时.y = self.原始.y
			self.回合行动 = 16
			return false
		else
			if self.攻击方式 == 0 then
					if self.数据.战斗类型 ~= "角色" then
						local 随机
						if random(1,2) == 1 then
							随机 = "攻击"
						else 
								随机 = "攻击1"
						end
						local zn = 取模型(self.数据.造型)
						self.动画.攻击 = tp.资源:载入(zn.战斗资源,"网易WDF动画",zn[随机],true)
						if self.数据.饰品 then
						local zy = 取模型("饰品_"..self.数据.造型)
						self.武器.攻击 = tp.资源:载入(zy.战斗资源,"网易WDF动画",zy[随机],true)
						elseif 取召唤兽武器(self.数据.造型) then
						local zy = 取模型("武器_"..self.数据.造型)
						self.武器.攻击 = tp.资源:载入(zy.战斗资源,"网易WDF动画",zy[随机],true)
						end
						if self.数据.染色方案 then

						self:置染色1(self.数据.染色方案,self.数据.染色组[1],self.数据.染色组[2],self.数据.染色组[3],0)
						end
					end
				self.回合行动 = 3
			elseif self.攻击方式 == 1 then
				if self.弓箭攻击 then
					self.弓箭攻击 = nil
					self.回合行动 = 3
				end
			end

			self.仅播放一次 = false
		end
	end

end
function 场景类_战斗对象:行动_反击(ts) -- 开关判断进程远程
	if (self.动画.攻击:取间隔() >= self.动画.攻击:取中间()+self.攻击帧 or self.动画.攻击.当前帧 == self.动画.攻击.结束帧-1 or ts ) and self.附加效果.攻击 == false then
		self.附加效果.攻击 = true
		self.已攻击次数 = self.已攻击次数 + 1
		tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.行为 = "挨打"
		if tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.特效 == nil then
			tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位.特效 = tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].攻击方].单位:附加特效("被击中")
		end
	end
	if self.动画.攻击.当前帧 == self.动画.攻击.结束帧 - self.终结帧 then
		self.行为 = "待战"
		self.执行重播 = true
	self.临时.x = self.原始.x
	self.临时.y = self.原始.y
		self.回合行动 = 16
	end
end
function 场景类_战斗对象:行动_施法()
	local xl = 0
	local xl1 = 0
	local 对象 = {}
	local jnm = tp.场景.战斗.战斗流程[1].参数
	if self.动画["施法"]:取间隔() >= self.动画["施法"]:取中间()-4 then
		if not self.仅播放一次 then
			local yx = wav(jnm)
			if yx ~= nil then
				self:音效类1_(yx.文件,yx.资源,"定义")
				if jnm == "二龙戏珠" then
					self:音效类1_(yx.文件,yx.资源,"定义")
				end
				self.仅播放一次 = true
			end
		end
	end
	if self.附加效果.施法 == false and self.已攻击次数 == 0 then
			tp.场景.战斗提示 = self.数据.名称.."使用了"..tp.场景.战斗.战斗流程[1].参数
	end
	if (self.动画["施法"]:取间隔() >= self.动画["施法"]:取中间()+1 or self.动画.施法.当前帧 == self.动画.施法.结束帧-1) and self.附加效果.施法 == false then
		self.已攻击次数 = self.已攻击次数 + 1
		-- 开始固定伤害的回合
			for m = 1,#tp.场景.战斗.战斗流程[1].挨打方 do
				对象[m] = tp.场景.战斗.战斗流程[1].挨打方[m].编号
				if tp.场景.战斗.参战数据[对象[m]].单位 ~= nil then
					tp.场景.战斗.参战数据[对象[m]].单位.被指令目标 = self
					tp.场景.战斗.参战数据[对象[m]].单位.特效固定 = true
						tp.场景.战斗.参战数据[对象[m]].单位.行为 = "挨打"
					if jnm == "龙卷雨击" then
						tp.场景.战斗.参战数据[对象[m]].单位.特效 = tp.场景.战斗.参战数据[对象[m]].单位:附加特效("龙卷雨击2",true)
						tp.场景.战斗.背景状态 = 1
						tp.场景.战斗.参战数据[对象[m]].单位.不显示特效 = true
					elseif jnm == "飞砂走石" then
						tp.场景.战斗.参战数据[对象[m]].单位.特效 = tp.场景.战斗.参战数据[对象[m]].单位:附加特效("飞砂走石",true)
						tp.场景.战斗.参战数据[对象[m]].单位.不显示特效 = true
					elseif jnm == "八凶法阵" then
						tp.场景.战斗.参战数据[对象[m]].单位.特效 = tp.场景.战斗.参战数据[对象[m]].单位:附加特效("八凶法阵",true)
						tp.场景.战斗.参战数据[对象[m]].单位.不显示特效 = true
					elseif jnm == "天降灵葫" then
						tp.场景.战斗.参战数据[对象[m]].单位.特效 = tp.场景.战斗.参战数据[对象[m]].单位:附加特效("天降灵葫",true)
						tp.场景.战斗.参战数据[对象[m]].单位.不显示特效 = true
					-- elseif jnm == "奔雷咒" then
					-- 	tp.场景.战斗.参战数据[对象[m]].单位.特效 = tp.场景.战斗.参战数据[对象[m]].单位:附加特效("奔雷咒",true)
					-- 	tp.场景.战斗.参战数据[对象[m]].单位.不显示特效 = true
					-- elseif jnm == "水漫金山" then
					-- 	tp.场景.战斗.参战数据[对象[m]].单位.特效 = tp.场景.战斗.参战数据[对象[m]].单位:附加特效("水漫金山",true)
					-- 	tp.场景.战斗.参战数据[对象[m]].单位.不显示特效 = true
					-- elseif jnm == "地狱烈火" then
					-- 	tp.场景.战斗.参战数据[对象[m]].单位.特效 = tp.场景.战斗.参战数据[对象[m]].单位:附加特效("地狱烈火",true)
					-- 	tp.场景.战斗.参战数据[对象[m]].单位.不显示特效 = true
					elseif jnm == "落叶萧萧" then
						tp.场景.战斗.参战数据[对象[m]].单位.特效 = tp.场景.战斗.参战数据[对象[m]].单位:附加特效("落叶萧萧",true)
						tp.场景.战斗.参战数据[对象[m]].单位.不显示特效 = true
					elseif jnm == "翻江搅海" then
						tp.场景.战斗.参战数据[对象[m]].单位.特效 = tp.场景.战斗.参战数据[对象[m]].单位:附加特效("翻江搅海",true)
						tp.场景.战斗.参战数据[对象[m]].单位.不显示特效 = true
					else
						tp.场景.战斗.参战数据[对象[m]].单位.特效 = tp.场景.战斗.参战数据[对象[m]].单位:附加特效(jnm)

						if jnm == "泰山压顶" then
						tp.场景.战斗.参战数据[对象[1]].单位.特效固定 = true
							tp.场景.战斗.参战数据[对象[m]].单位.行动特效 = true
							if m ~= 1 then
								tp.场景.战斗.参战数据[对象[m]].单位.不显示特效 = true
								tp.场景.战斗.参战数据[对象[m]].单位.特效固定 = false

							end
							if self.位置 == 3 then
								tp.场景.战斗.参战数据[对象[1]].单位.固定特效坐标 = {558+(全局游戏宽度-800)/2,449+(全局游戏高度-600)/2}
							else
								tp.场景.战斗.参战数据[对象[1]].单位.固定特效坐标 = {278+(全局游戏宽度-800)/2,236+(全局游戏高度-600)/2}
							end
							tp.场景.战斗.参战数据[对象[m]].单位.当前.x = tp.场景.战斗.参战数据[对象[1]].单位.固定特效坐标[1]
							tp.场景.战斗.参战数据[对象[m]].单位.当前.y = tp.场景.战斗.参战数据[对象[1]].单位.固定特效坐标[2] - 180
	end
					end
					--self:被动逻辑(xl,tp.场景.战斗.参战数据[对象[m]].单位)
					if jnm == "雨落寒沙" then
						tp.场景.战斗.参战数据[对象[m]].单位.行动特效 = true
						tp.场景.战斗.参战数据[对象[m]].单位.当前.x,tp.场景.战斗.参战数据[对象[m]].单位.当前.y = self.临时.x,self.临时.y
						if self.位置 == 3 then
						tp.场景.战斗.参战数据[对象[m]].单位.目标.x,tp.场景.战斗.参战数据[对象[m]].单位.目标.y = self.临时.x+48,self.临时.y+48
						else
						tp.场景.战斗.参战数据[对象[m]].单位.目标.x,tp.场景.战斗.参战数据[对象[m]].单位.目标.y = self.临时.x-68,self.临时.y+10
						end
						tp.场景.战斗.参战数据[对象[m]].单位.特效延迟 = 15

						tp.场景.战斗.参战数据[对象[m]].单位.当前.x = tp.场景.战斗.参战数据[对象[m]].单位.当前.x - 15
						tp.场景.战斗.参战数据[对象[m]].单位.当前.y = tp.场景.战斗.参战数据[对象[m]].单位.当前.y + 30
						local jd = bfs(floor(jds(tp.场景.战斗.参战数据[对象[m]].单位.当前,tp.场景.战斗.参战数据[对象[m]].单位.临时)))
						tp.场景.战斗.参战数据[对象[m]].单位.特效:置方向(jd)
					end
					if not jn then
						tp.场景.战斗.参战数据[对象[m]].单位.固定特效 = true
					end
				end
			end
	
		-- 全屏特效接口
		local qp = nil
		if jnm == "龙卷雨击" then
			tp.场景.战斗.拼接特效 = {}
			for s = 1,4 do
					tp.场景.战斗.拼接特效[s] = {}
					tp.场景.战斗.拼接特效[s].特效 = tp.场景.战斗.参战数据[对象[1]].单位:附加特效("龙卷雨击"..s)
					tp.场景.战斗.拼接特效[s].偏移 = {x = 0,y = 0}
				if s == 1 then
					tp.场景.战斗.拼接特效[s].偏移 = {x = 60,y = 20}
				elseif s == 2 then
					tp.场景.战斗.拼接特效[s].偏移 = {x = -10,y = -20}
				elseif s == 4 then

					tp.场景.战斗.拼接特效[s].偏移 = {x = -40,y = -100}
				end
			end
			tp.场景.战斗.背景状态 = 1
			qp = true
		elseif jnm == "飞砂走石" then
			tp.场景.战斗.拼接特效 = {}
			tp.场景.战斗.拼接特效[1] = {}
			tp.场景.战斗.拼接特效[1].特效 = tp.场景.战斗.参战数据[对象[1]].单位:附加特效("飞砂走石")
			-- tp.场景.战斗.拼接特效[1].偏移 = {x = -70,y = }
			tp.场景.战斗.拼接特效[1].偏移 = {x = -160,y = -120}
			qp = true
		elseif jnm == "八凶法阵" then
			tp.场景.战斗.拼接特效 = {}
			tp.场景.战斗.拼接特效[1] = {}
			tp.场景.战斗.拼接特效[1].特效 = tp.场景.战斗.参战数据[对象[1]].单位:附加特效("八凶法阵")
			tp.场景.战斗.拼接特效[1].偏移 = {x = -70,y = 0}
			qp = true
		elseif jnm == "天降灵葫" then
			tp.场景.战斗.拼接特效 = {}
			tp.场景.战斗.拼接特效[1] = {}
			tp.场景.战斗.拼接特效[1].特效 = tp.场景.战斗.参战数据[对象[1]].单位:附加特效("天降灵葫")
			tp.场景.战斗.拼接特效[1].偏移 = {x = -70,y = 0}
			qp = true
		-- elseif jnm == "奔雷咒" then
		-- 	tp.场景.战斗.拼接特效 = {}
		-- 	tp.场景.战斗.拼接特效[1] = {}
		-- 	tp.场景.战斗.拼接特效[1].特效 = tp.场景.战斗.参战数据[对象[1]].单位:附加特效("奔雷咒")
		-- 	tp.场景.战斗.拼接特效[1].偏移 = {x = -70,y = 0}
		-- 	qp = true
		-- elseif jnm == "水漫金山" then



			
		-- 	tp.场景.战斗.拼接特效 = {}
		-- 	tp.场景.战斗.拼接特效[1] = {}
		-- 	tp.场景.战斗.拼接特效[1].特效 = tp.场景.战斗.参战数据[对象[1]].单位:附加特效("水漫金山")
		-- 	tp.场景.战斗.拼接特效[1].偏移 = {x = 0,y = 0}
			
		-- 	-- for i = 1,7 do
		-- 	-- 	tp.场景.战斗.拼接特效[s] = {}
		-- 	-- 	tp.场景.战斗.拼接特效[s].特效 = tp.场景.战斗.参战数据[对象[1]].单位:附加特效("水漫金山1")
		-- 	-- 	tp.场景.战斗.拼接特效[s].偏移 = {x = (全局游戏宽度-800)/2-200,y = +50}
		-- 	-- end
			
			

		-- 	qp = true

		-- elseif jnm == "地狱烈火" then
		-- 	tp.场景.战斗.拼接特效 = {}
		-- 	tp.场景.战斗.拼接特效[1] = {}
		-- 	tp.场景.战斗.拼接特效[1].特效 = tp.场景.战斗.参战数据[对象[1]].单位:附加特效("地狱烈火")
		-- 	tp.场景.战斗.拼接特效[1].偏移 = {x = -70,y = 0}
		-- 	qp = true
		elseif jnm == "落叶萧萧" then
			tp.场景.战斗.拼接特效 = {}
			tp.场景.战斗.拼接特效[1] = {}
			tp.场景.战斗.拼接特效[1].特效 = tp.场景.战斗.参战数据[对象[1]].单位:附加特效("落叶萧萧")
			tp.场景.战斗.拼接特效[1].偏移 = {x = -70,y = 70}
			qp = true
		elseif jnm == "翻江搅海" then
			tp.场景.战斗.拼接特效 = {}
			tp.场景.战斗.拼接特效[1] = {}
			tp.场景.战斗.拼接特效[1].特效 = tp.场景.战斗.参战数据[对象[1]].单位:附加特效("翻江搅海")
			tp.场景.战斗.拼接特效[1].偏移 = {x = -40,y = -50}
			qp = true
		elseif jnm == "风卷残云" then
			self.印记.风灵 = nil
		elseif jnm == "凋零之歌" then
			-- local mb = tp.场景.战斗.敌人
			-- self.队伍秒体 = 随机目标(self.指令对象[1],mb,最大目标(mb,4),true)
			-- remove(self.队伍秒体,1)
			-- for i = 1,#self.队伍秒体 do
			-- 	mb[self.队伍秒体[i]]:增加状态("雾杀",3,"雾杀")
			-- 	mb[self.队伍秒体[i]].附加状态.雾杀.伤害 = floor(self.信息.师门技能[3].等级*3)
			-- 	if self.奇经八脉.咒术 == 1 then
			-- 		mb[self.队伍秒体[i]].雾杀.咒术 = 1
			-- 		mb[self.队伍秒体[i]].雾杀.目标 = self
			-- 	end
			-- 	if self.奇经八脉.破杀 == 1 then
			-- 		mb[self.队伍秒体[i]].雾杀.破杀 = 1
			-- 	end
			-- end
		end
		if qp then
			if self.位置 == 3 then
				tp.场景.战斗.拼接偏移.x,tp.场景.战斗.拼接偏移.y = 638+(全局游戏宽度-800)/2,379+(全局游戏高度-600)/2
			else
				tp.场景.战斗.拼接偏移.x,tp.场景.战斗.拼接偏移.y = 358,236
			end
		end

 


		self.攻击次数 = self.攻击次数 - 1
		self.附加效果.施法 = true
	end
	if self.动画["施法"].当前帧 == self.动画["施法"].结束帧 then
		self.执行重播 = true
		self.行为 = "待战"
		self.回合行动 = 3
		self.仅播放一次 = false
	end
end
function 场景类_战斗对象:行动_攻击完毕()
	self.行为 = "返回"
end
function 场景类_战斗对象:行动_施法完毕()
	self.施法前播放 = false
	if typ(tp.场景.战斗.战斗流程[1].挨打方) == "table" then
		local wc = 0
		local 对象 = {}
		for w = 1,#tp.场景.战斗.战斗流程[1].挨打方 do
	对象[w] = tp.场景.战斗.战斗流程[1].挨打方[w].编号
			if tp.场景.战斗.参战数据[对象[w]].单位.挨打状态 == "正常" and tp.场景.战斗.参战数据[对象[w]].单位.特效 == nil then
				wc = wc + 1
			end
		end
		if wc >= #tp.场景.战斗.战斗流程[1].挨打方 then
			-- if self.攻击次数 > 0 then
			-- 	tp.场景.战斗.拼接特效 = nil
			-- 	tp.场景.战斗.是否拼接 = nil
			-- 	self.附加效果.施法 = false
			-- 	self.行为 = "施法"
			-- 	self.攻击次数 = self.攻击次数 - 1
			-- else
				self.回合行动 = 4
				self.行为 = "待战"
			-- end
		end
	end
end
function 场景类_战斗对象:流血逻辑(血量,类型,及时,延时)
	if self.流血显示 or self.死亡方式 > 1 or not 血量 then
		return false
	end
	self.流血位置 = 80
	self.流血时间 = 0
	self.及时显示 = 及时
	self.延迟 = 0
	if 及时 == nil and self.特效 == nil then
		self.延迟 = 延时 or 20
	end
	local ids = 0
	if 类型 == "掉血" or 类型 == nil then
		ids = 2
	self.数据.当前气血 = self.数据.当前气血 - 血量
	elseif 类型 == "加血" then
		ids = 1
		self.数据.当前气血 = self.数据.当前气血 + 血量
	end
	if self.数据.当前气血 <= 0 then
		self.数据.当前气血 = 0
	end
	if self.数据.当前气血 >= self.数据.气血上限 then
		self.数据.当前气血 = self.数据.气血上限
	end


	血量 = tostring(math.abs(血量))
	local v = 0
	local v1 = 0
	if self.附加效果.攻击 then
		if self.位置 == 3 then
			v = 10
			v1 = 10
		else
			v = -10
			v1 = -10
		end
	end
	local fen = {}
	分割字符(血量,fen)
	self.流血 = {}
	self.流血偏移 = {}
	self.流血显示 = true
	for n = 1,#fen do
		local liuxue = {文字 = fen[n],
			显示时间 = 70*n,
			显示开关 = false,
			x = self.临时.x-v+self.附加.x,
			y = self.临时.y-v+self.附加.y,
			id = ids,
		}
		self.流血偏移[n] = 0
		insert(self.流血,liuxue)
	end
end

function 场景类_战斗对象:流血文字()
	if self.流血显示 then
		if self.及时显示 == nil then
			if self.特效 ~= nil then
				if self.特效:取间隔() >= self.特效:取中间()-3 or self.特效.当前帧 >= self.特效.结束帧-3 or self.流血时间 ~= 0 then

				else
					return false
				end
			end
		end
		if self.延迟 > 0 then
			self.延迟 = self.延迟 - 1
			return false
		end
		for n = 1,#self.流血 do
			self.流血位置 = self.流血位置 + 4
			if self.流血位置>self.流血[n].显示时间 then
				if self.流血[n].显示开关 == false then
				self.流血偏移[n] = self.流血偏移[n] + 5
					if self.流血偏移[n] > 25 then
						self.流血[n].显示开关 = true
					end
				elseif self.流血[n].显示开关 == true and self.流血偏移[n] > 1 then
					self.流血偏移[n] = self.流血偏移[n]- 5
				end
			end
			if self.流血[n].id == 1 then
				tp.场景.战斗.加血文字:显示(self.流血[n].文字,self.流血[n].x+11*n - #self.流血*6.7,self.流血[n].y-30 - self.流血偏移[n])
			else
				-- if self.被暴击 then
				-- 	tp.场景.战斗.暴击文字:显示(self.流血[n].文字,self.流血[n].x+17*n - #self.流血*6.7,self.流血[n].y-30 - self.流血偏移[n])
				-- else
					tp.场景.战斗.掉血文字:显示(self.流血[n].文字,self.流血[n].x+11*n - #self.流血*6.7,self.流血[n].y-30 - self.流血偏移[n])
				-- end
			end
			
		end
		self.流血时间 = self.流血时间 + 1
		if self.流血时间 >= 40 then
			self.流血 = {}
			self.流血显示 = false
			self.被暴击 = false
			self.法暴开关 = false
		end
	end
end
function 场景类_战斗对象:删除状态(状态)

	if not self.数据.法术状态组[状态] then
		return false
	end

	if self.数据.法术状态组[状态] then
		local id = self.数据.法术状态组[状态][1]

		if self.数据.法术状态组[状态][2] then
			self.前置特效[id] = nil
		else
			if self.后置特效[id] == nil then
			self.数据.法术状态组[状态] = nil
			return
			end
			self.后置特效[id] = nil
		end
	end
 self.数据.法术状态组[状态] = nil
end
function 场景类_战斗对象:添加发言内容(内容)
	self.发言内容[#self.发言内容 + 1] = {
		起始 = 0,
		开关 = false,
		长度 = string.len(内容)
	}
	self.发言内容[#self.发言内容].开关 = true
	self.发言内容[#self.发言内容].起始 = os.time()

	self.超丰富文本:清空()
	self.ab = self.超丰富文本:添加文本(内容)+3
end
function 场景类_战斗对象:增加状态(状态,剩余回合,特效)

	if self.数据.法术状态组[状态] then
	return
	end
 self.数据.法术状态组[状态] = {}

	if 特效 ~= nil and 状态~= "隐身" then
		local cp = true
		local py;
		-- 调整偏移值
		if 特效 == "生命之泉" or 特效 == "炼气化神" or 特效 == "天神护体" or 特效 == "明光宝烛" or 特效 == "移魂化骨"then
			py = {0,-75}
		elseif 特效 == "错乱"or 特效 == "娉婷嬝娜"or 特效 == "电芒" or 特效 == "死亡召唤" or 特效 == "摧心术" or 特效 == "罗汉金钟"or 特效 == "太极护法"or 特效 == "横扫千军"or 特效 == "后发制人" or 特效 == "魔音摄魂" or 特效 == "达摩护体"or 特效 == "催眠符" or 特效 == "失忆符" or 特效 == "落魄符" or 特效 == "追魂符" or 特效 == "炎护" or 特效 == "含情脉脉" or 特效 == "离魂符" or 特效 == "失心符" or 特效 == "失魂符" or 特效 == "碎甲符" or 特效 == "一笑倾城" or 特效 == "象形" or 特效 == "似玉生香" or 特效 == "乘风破浪" or 特效 == "一苇渡江" or 特效 == "夺魄令" or 特效 == "百万神兵" or 特效 == "护法紫丝" then
			cp = false
		elseif 特效 == "红袖添香" then
			py = {10,0}
		elseif 特效 == "莲步轻舞" or 特效 == "如花解语" then
			py = {1,3}
			cp = false
		elseif 特效 == "定身符" then
			py = {5,2}
			cp = false
		elseif 特效 == "变身" or 特效 == "定心术" or 特效 == "匠心·固甲" or 特效 == "匠心·削铁" or 特效 == "法宝" or 特效 == "锢魂术" or 特效 == "逆鳞" or 特效 == "幽冥鬼眼" or 特效 == "魔王回首" or 特效 == "牛劲" or 特效 == "极度疯狂" or 特效 == "杀气诀" then
			py = {2,-31}
		elseif 特效 == "紧箍咒" then
			py = {2,-self.高度/2-4}
		elseif 特效 == "普渡众生"or 特效 == "雾杀" or 特效 == "无畏布施"or 特效 == "光照万象"then
			py = {2,-75}
		elseif 特效 == "血雨" then
			py = {0,45}

		end
		local id;
		if cp then
			id = #self.前置特效 + 1
			self.前置特效[id] = {[1] = self:附加特效("状态_"..特效),[2] = py}
		else
			id = #self.后置特效 + 1
			self.后置特效[id] = {[1] = self:附加特效("状态_"..特效),[2] = py}
		end
		self.数据.法术状态组[状态] = {[1] = id,[2] = cp}
	end
end
function 场景类_战斗对象:附加特效(特效,不显示)
	local txz = 0
	if 特效 == "唧唧歪歪" then
		txz = 1.15
	elseif 特效 == "横扫千军" then
		txz = 1.05
	elseif 特效 == "反震" or 特效 == "防御"then
		txz = 1.6
	elseif 特效 == "捕捉开始" then
		txz = 1.55
	elseif 特效 == "地裂火" then
		txz = 1.2
	elseif 特效 == "裂石" or 特效 == "食指大动" then
		txz = 1.2
 elseif 特效 == "浪涌" or 特效 == "阎罗令" or 特效 == "翻江搅海" then
		txz = 1.4
	elseif 特效 == "惊涛怒" then
		txz = 5
	elseif 特效 == "泰山压顶" or 特效 == "满天花雨" then
		txz = 1.4
	elseif 特效 == "飘渺式" or 特效 == "金刚护体"
	or 特效 == "落雷符" or 特效 == "天雷斩" or 特效 == "五雷咒" or 特效 == "龙腾" then
		txz = 2.0
	elseif 特效 == "活血" or 特效 == "巨岩破" or 特效 == "我佛慈悲" or 特效 == "摄魄" or 特效 == "力劈华山" or 特效 == "天命剑法"then
		txz = 2.5

		elseif 特效 == "龙卷雨击1" then
		txz = 1.15
	elseif 特效 == "龙卷雨击2" then
		txz = 1.5
	elseif 特效 == "龙卷雨击3" then
		txz = 1.0
	elseif 特效 == "龙卷雨击4" then
		txz = 1.15

	elseif 特效 == "宠物_静" then
		local mt = 取模型(self.数据.宠物.模型)
		return tp.资源:载入(mt.资源,"网易WDF动画",mt.静立)
	elseif 特效 == "宠物_走" then
		local mt = 取模型(self.数据.宠物.模型)
		return tp.资源:载入(mt.资源,"网易WDF动画",mt.行走)
	elseif 特效 == "地狱烈火" then
		txz = 2.7
	elseif 特效 == "鹰击" or 特效 == "上古灵符" or 特效 == "推气过宫" then
		txz = 1.48

		elseif 特效 == "奔雷咒" then
			txz = 1.5

			elseif 特效 == "水漫金山" then
				txz = 1.9

	elseif 特效 == "五雷轰顶" then
		txz = 1 
	elseif 特效 == "天罗地网" then
		self.特效偏移 = {-170,-220}
		txz = 0.85
	elseif 特效 == "二龙戏珠" then
		txz = 1.3
	elseif 特效 == "法暴" or 特效 == "金刚护法" or 特效 == "舍生取义" or 特效 == "龙吟" then
		txz = 3
	elseif 特效 == "血雨" then
		txz = 3
		self.特效偏移 = {30,65}
	elseif 特效 == "后发制人" or 特效 == "泼天乱棒" then
		txz = 1.3
	elseif 特效 == "泰山压顶1" then
		txz = 1.25
	elseif 特效 == "泰山压顶2" or 特效 == "月光" then
		txz = 1.4
	elseif 特效 == "泰山压顶3" then
		txz = 1.1
	elseif 特效 == "逆鳞" or 特效 == "清心" or 特效 == "牛劲" or 特效 == "魔王回首" or 特效 == "碎星诀" or 特效 == "变身" or
		特效 == "定心术" or 特效 == "归元咒" or 特效 == "乾天罡气" or 特效 == "极度疯狂" or 特效 == "幽冥鬼眼" or 特效 == "杀气诀" or 特效 == "韦陀护法" or 特效 == "天神护法" then
		txz = 1.1
		self.特效偏移 = {2,-31}
	elseif 特效 == "地涌金莲" or 特效 == "夺命咒" or 特效 == "荆棘舞" then
		txz = 1.1
		self.特效偏移 = {2,45}
	elseif 特效 == "推拿" or 特效 == "活血" then
		txz = 0.95
	elseif	特效 == "被击中" or 特效 == "暴击" then
				txz = 1.1
		self.特效偏移 = {2,-31}
	elseif 特效 == "惊心一剑" or 特效 == "牛刀小试" then
		txz = 1.05
	else
		txz = 1
	end

	local cp = tp:载入特效(特效,txz,不显示)
	cp:置提速(txz)
	return cp
end
function 场景类_战斗对象:添加特技内容(q)


	self.特技文本 = 分割字符(q)
	self.特技总数 = #self.特技文本
	self.特技序列 = {}
	self.特技次数 = 20
	self.特技间隔 = 4

	for n = 1, #self.特技文本, 1 do
		self.特技序列[n] = {
			文本 = self.特技文本[n],
			高度 = 0,
		
			x = self.临时.x + self.附加.x - self.特技总数 * 4,
			y = self.临时.y + self.附加.y
		}
	end

	self.特技顺序 = 1
	self.特技内容开关 = true
	self.关闭计次 = 0

end
function 场景类_战斗对象:特技文本显示()
	self.关闭计次 = self.关闭计次 + 1

	for n = 1, self.特技总数, 1 do
		if n == self.特技顺序 then
			self.特技序列[n].高度 = self.特技序列[n].高度 + self.特技间隔

			if self.特技次数 <= self.特技序列[n].高度 then
				self.特技顺序 = self.特技顺序 + 1
			end
		elseif self.特技序列[n].高度 > 0 then
			self.特技序列[n].高度 = self.特技序列[n].高度 - 1
		end

		tp.字体表.华康字体_:置颜色(4287045496):显示(self.特技序列[n].x - 20 + n * 12, self.特技序列[n].y - 30 - self.特技序列[n].高度, self.特技序列[n].文本)
	end

	if self.关闭计次 >= 50 then
		self.特技内容开关 = false
	end
end
return 场景类_战斗对象