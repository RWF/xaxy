-- @Author: 作者QQ381990860
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-12 01:31:42
-- @Author: 作者QQ381990860
-- @Date:   2021-08-13 19:47:22
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-08-31 20:03:30
-- @Author: 作者QQ381990860
-- @Date:   2021-08-13 19:47:22
-- @Last Modified by:   作者QQ381990860
-- @Last Modified time: 2021-08-31 20:01:59
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-11-23 18:44:56
--======================================================================--
local 场景类_战斗文字 = class()
local floor = math.floor
local len = string.len
local string = tostring
local number = tonumber
local wb = 取文本中间

function 场景类_战斗文字:初始化(id,根)
	self.普通 = 根.战斗文字[id]
	self.宽度s = self.普通[1].宽度
	self.高度s = self.普通[1].高度
	self.文本 = ""
	self.文本宽度 = 0
end

function 场景类_战斗文字:显示(文本,x,y)
	self.文本 = string(文本)

	-- self.文本=self.文本.."00000000000"
	self.宽度 = len(self.文本)
	local w = self.文本宽度 * (self.宽度s+10)
	for n=1,self.宽度 do
		local m = assert(number(wb(self.文本,n,1)) + 1, "错误文本"..文本) 
		self.普通[m]:显示(floor((n-1)*(self.宽度s-2)+x)-floor(w/2) +5,floor(y))
	end
end

return 场景类_战斗文字