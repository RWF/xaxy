--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-11-17 01:46:48
--======================================================================--
local 场景类_助战自动栏 = class()
local 开启自动 = false
local format = string.format
local tp
local  zts,zts1


function 场景类_助战自动栏:初始化(根)
	self.ID = 1001
	self.x = 258+204+(全局游戏宽度-800)/2
	self.y = 260
	self.xx = 0
	self.yy = 0
	self.注释 = "战斗"
	self.可视 = false
	self.可视化 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	self.窗口时间 = 0
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[0] = 自适应.创建(1,1,204,18,1,3,nil,18),
		[1] = 自适应.创建(0,1,220,131,3,9),
	}
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.普通字体__
	self.介绍文本 = 根._丰富文本(130,50,根.字体表.普通字体)
	self.介绍文本:清空()
	self.第一次 = false
	self.托管状态 = {}
	local wz = require("gge文字类")
    self.方正字体 = wz.创建(程序目录.."Data/pic/simkai.ttf",16,true,false,true)
end

function 场景类_助战自动栏:打开()
	if self.可视化 then
		self.可视化 = false
	else
		self.x = 258+204+(全局游戏宽度-800)/2
		self.可视化 = true
		tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
		self.取消按钮 = {}
		self.托管状态 = {}
		if #tp.场景.战斗.助战自动 > 0 then
			for n= 1,#tp.场景.战斗.助战自动 do
				local 按钮文字 = "托管"
				self.托管状态[n] = "未托管"
				if tp.场景.战斗.助战自动[n].自动 then
					按钮文字 = "取消"
					self.托管状态[n] = "托管中"
				end
				self.取消按钮[n] = tp._按钮.创建(tp.资源:载入('JM.dll',"网易WDF动画",0x2BD1DEF7),0,0,4,true,nil,按钮文字)
				self.取消按钮[n]:绑定窗口_(self.ID)
			end
		end
	end
end

function 场景类_助战自动栏:更新(dt,x,y)
end

function 场景类_助战自动栏:刷新()
	self.取消按钮 = {}
	self.托管状态 = {}
	if #tp.场景.战斗.助战自动 > 0 then
		for n= 1,#tp.场景.战斗.助战自动 do
			local 按钮文字 = "托管"
			self.托管状态[n] = "未托管"
			if tp.场景.战斗.助战自动[n].自动 then
				按钮文字 = "取消"
				self.托管状态[n] = "托管中"
			end
			self.取消按钮[n] = tp._按钮.创建(tp.资源:载入('JM.dll',"网易WDF动画",0x2BD1DEF7),0,0,4,true,nil,按钮文字)
			self.取消按钮[n]:绑定窗口_(self.ID)
		end
	end
end


function 场景类_助战自动栏:显示(dt,x,y)
	if not self.可视化 then
		return
	end
	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[0]:显示(self.x+6,self.y+3)
	Picture.窗口标题背景_:置区域(0,0,74,16)
	Picture.窗口标题背景_:显示(self.x+72,self.y+3)
	tp.字体表.描边字体:置字间距(2)
	tp.字体表.描边字体:显示(self.x+78,self.y+3,"助战托管")
	tp.字体表.描边字体:置字间距(0)
	tp.字体表.普通字体:置颜色(-16777216)

	for i = 1,#tp.场景.战斗.助战自动 do
		self.方正字体:置颜色(0xffFFFFFF):显示(self.x+15,self.y+30+(i-1)*25,tp.场景.战斗.助战自动[i].名称)
		local 颜色 = 0xffA9A9A9
		if self.托管状态[i] == "托管中" then
			颜色 = 0xff00FF00
		end
		self.方正字体:置颜色(颜色):显示(self.x+15+80,self.y+30+(i-1)*25,self.托管状态[i])
		if self.取消按钮[i] ~= nil then
			self.取消按钮[i]:更新(x,y)
			self.取消按钮[i]:显示(self.x+155,self.y+30+(i-1)*25)
			if self.取消按钮[i]:事件判断() and self.鼠标 then
				客户端:发送数据(1, 242, 24, table.tostring({编号=tp.场景.战斗.助战自动[i].编号}))
			end
		end
	end
	
	if tp.按钮焦点 then
		self.焦点 = true
	end

end

function 场景类_助战自动栏:检查点(x,y)
	if self.可视化 and self.资源组[1]:是否选中(x,y)  then
		return true
	end
end

function 场景类_助战自动栏:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not self.第一次 then
		tp.运行时间 = tp.运行时间 + 2
		self.第一次 = true
	end
	if not 引擎.场景.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.场景.战斗.移动窗口 = true
	end
	if self.可视化 and self.鼠标 and not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_助战自动栏:开始移动(x,y)
	if self.可视化 and self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_助战自动栏