-- @Author: 作者QQ381990860
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-12 01:31:43
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-10-25 00:26:55
--======================================================================--
local 场景类_战斗头像栏 = class()
local require = require
local 按钮 = require("script/System/按钮")

local tp
local tpc
function 场景类_战斗头像栏:初始化(根)
	self.ID = 106
	self.x = 275
	self.y = 3
	self.xx = 0
	self.yy = 0
	self.注释 = "战斗"
	self.可视化 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = false
	self.窗口时间 = 0
	self.资源组 = {
		[1] = 根.资源:载入('JM.dll',"网易WDF动画",0xE03A06B3),
		[2] = {},
		[3] = 根.资源:载入('JM.dll',"网易WDF动画",0x360B8373),
	}
	self.头像可视 = true
	tp = 根

end

function 场景类_战斗头像栏:打开()
	if self.可视化 then
		self.可视化 = false
	else

		self.窗口时间 = tp.运行时间 + 3
		self.头像可视 = false
		self.可视化 = true
	end
end

function 场景类_战斗头像栏:更新()
end

function 场景类_战斗头像栏:显示(dt,x,y)
	if not self.可视化 then
		return false
	end
	local py = 0
	if self.资源组[1]:是否选中(x,y) and 引擎.鼠标按住(0) then
		py = 1
	end
	if self.资源组[1]:是否选中(x,y) and 引擎.鼠标弹起(0) then
		self.头像可视 = not self.头像可视
		self.窗口时间 = self.窗口时间 + 1
	end

	--self.资源组[1]:置坐标_高级(self.x + py,self.y + py,1.57)
	self.资源组[1]:显示(self.x + py,self.y + py)
     local a =0
		for n=1,#tp.场景.战斗.参战数据 do
			if tp.场景.战斗.参战数据[n].战斗类型=="角色" and tp.场景.战斗.参战数据[n].单位.敌我==1 then
			a=a+1
			local tx = ModelData[tp.场景.战斗.参战数据[n].单位.数据.造型]
			self.资源组[2][a] = {}
			self.资源组[2][a][1] = 按钮(tp.资源:载入(tx.头像资源,"网易WDF动画",tx.中头像),0,0,1,true)
			self.资源组[2][a][2] = false
			self.资源组[2][a][3] =tp.场景.战斗.参战数据[n].单位.编号
		   end
		end
	if self.头像可视 then
		for n=1,#self.资源组[2] do
			self.资源组[3]:显示(self.x+((n-1)*54)+1,self.y+19)
			self.资源组[2][n][1]:显示(self.x+((n-1)*54)+4,self.y+22,true)
		end
		for n=1,#self.资源组[2] do
			self.资源组[2][n][1]:更新(x,y)
			if self.资源组[2][n][1]:事件判断() then
				self.窗口时间 = self.窗口时间 + 1
			end
			if self.资源组[2][n][1]:是否选中(x,y) and tp.场景.战斗.参战数据[self.资源组[2][n][3]].单位.数据 then
				local f = tp.场景.战斗.参战数据[self.资源组[2][n][3]].单位.数据
				tp.提示:其他提示(x-90,y+35,f)
			end
		end
	end
end

function 场景类_战斗头像栏:检查点(x,y)
end

function 场景类_战斗头像栏:初始移动(x,y)
end

function 场景类_战斗头像栏:开始移动(x,y)
end

return 场景类_战斗头像栏