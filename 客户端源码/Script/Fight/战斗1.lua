-- @Author: 作者QQ381990860
-- @Date:   2021-09-12 16:35:35
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-03-22 02:50:00
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:	2019-12-03 02:17:19
-- @Last Modified time: 2021-02-28 17:53:33
--======================================================================--
local 场景类_战斗 = class()
local collectgarbage = collectgarbage
local sort = table.sort

local floor = math.floor
local insert = table.insert
local min = math.min
local random = 引擎.取随机整数
local pairs = pairs
local format = string.format
local tp;
local mousea = 引擎.鼠标按下
local mouseb = 引擎.鼠标弹起

function 场景类_战斗:战斗初始化()
	self.操作单位={}
	self.显示表={}
	self.参战单位={}
	self.参战数据={}
	self.单位总数=0
	self.我方数量=0
	self.敌方数量=0
	self.回合进程="加载回合"
	self.加载结束=false
	self.群体法术={开关=false}
	self.道具开关=false
	self.命令数据={计时=游戏时间,分=6,秒=0}
	self.保护开关=false
	self.捕捉开关=false
	self.召唤开关=false
	self.助战自动={}
	self.召唤编号=0
	self.窗口.头像栏:打开()
end
local function 显示排序1(a,b)
	return a.临时.y + a.刷新.x + a.刷新.y < b.临时.y + b.刷新.x + b.刷新.y
end
function 场景类_战斗:初始化(根)

	tp = 根
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	self.战斗 = require("script/Fight/战斗2")
	self.战斗背景 = 资源:载入('JM.dll',"网易WDF动画",0x00D17553)
	self.背景状态 = 0
	self.血条背景 = 资源:载入('JM.dll',"网易WDF动画",0x4D0A334C)
	self.血条栏 = 资源:载入('JM.dll',"网易WDF动画",0x4FD9FFF3)
	self.战斗3 = require("script/Fight/战斗3")

	self.掉血文字 = self.战斗3.创建(1,根)
	self.加血文字 = self.战斗3.创建(2,根)
	self.暴击文字 = self.战斗3.创建(4,根)
	self.窗口 = {
		技能栏 = require("script/Fight/战斗技能栏").创建(根),
		道具栏 = require("script/Fight/战斗道具栏").创建(根),
		召唤栏 = require("script/Fight/战斗召唤栏").创建(根),
		自动栏 = require("script/Fight/战斗自动栏").创建(根),
		助战栏 = require("script/Fight/助战自动栏").创建(根),
		头像栏 = require("script/Fight/战斗头像栏").创建(根),
		特技栏 = require("script/Fight/战斗特技栏").创建(根),
		法宝栏 = require("script/Fight/战斗法宝栏").创建(根),
		消息框窗口 = 根.窗口.消息框窗口
	}
	self.退出观战= 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x132041E1),0,0,4,true)
	self.人物资源组 = {
		[1] = 资源:载入('JM.dll',"网易WDF动画",0xF5A9A3F5),
		[2] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x839C6C7D),0,0,4,true),
		[3] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x2E8F2187),0,0,4,true),
		[4] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x389DCCF5),0,0,4,true),
		[5] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x1587C26E),0,0,4,true),
		[6] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xA662D44B),0,0,4,true),
		[7] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x0467A0A8),0,0,4,true),
		[8] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x8A8A21AD),0,0,4,true),
		[9] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xA2E2DC42),0,0,4,true),
		[10] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xE1A79F93),0,0,4,true),
		[11] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xEFB4F757),0,0,4,true),
		[12] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x2ACB414D),0,0,4,true),
		[13] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x132041E1),0,0,4,true)
	}
	self.召唤兽资源组 = {
		[1] = 资源:载入('JM.dll',"网易WDF动画",0xB5B958DF),
		[2] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x839C6C7D),0,0,4,true),
		[3] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x1587C26E),0,0,4,true),
		[4] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xA662D44B),0,0,4,true),
		[5] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x0467A0A8),0,0,4,true),
		[6] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x132041E1),0,0,4,true),
	}
	self.窗口_ = {
		[1] = self.窗口.技能栏,
		[2] = self.窗口.道具栏,
		[3] = self.窗口.召唤栏,
		[4] = self.窗口.自动栏,
		[5] = self.窗口.头像栏,
		[6] = self.窗口.特技栏,
		[7] = self.窗口.法宝栏,
		[8] = self.窗口.消息框窗口,
		[9] = self.窗口.助战栏,
		[10] = tp.窗口.自动系统,
	}
	self.选中窗口 = 0
	self.跳转事件 = nil
	self.拼接偏移 = 生成XY()
	self.对话组 = nil
 self.数字图片={}
 for n=1,10 do
	self.数字图片[n]=资源:载入("23-"..n..".png","加密图片") --4EE0010A
 	end
 self.请等待=资源:载入('JM.dll',"网易WDF动画",0xEE1713AC)
	self.战斗次数=0

	local wz = require("gge文字类")
    self.方正字体 = wz.创建(程序目录.."Data/pic/FZQianLXSJW.ttf",22,false,true,true)
	self.方正字体:置颜色(0xffffffff)
	self.方正字体:置描边颜色(0xff000000)

 self.参战数据={}
 self.回合进程=""
 self.命令数据={计时=游戏时间,分=3,秒=0}

end
function 场景类_战斗:写入(对话组)
	self.对话组 = 对话组
end
function 场景类_战斗:取鼠标所在窗口(x,y)
	if not tp.隐藏UI then
		for n=#self.窗口_, 1,-1 do
			if self.窗口_[n]:检查点(x,y) then
				tp.选中UI = true
				return n
			end
		end
	end
	return 0
end

local function UI排序(a,b)
 	return a.窗口时间 < b.窗口时间
end
function 场景类_战斗:显示(dt,x,y)
	if tp.窗口.对话栏.可视 then
		self.回合进程 = 10
	end
	self.纯色背景:显示(0,0)
	self.战斗背景:显示(全局游戏宽度/2-235,全局游戏高度/2-125)
	local bt = false
	if self.背景状态 == 1 then
		self.黑幕背景:显示(0,0)
	end
	if 底图背景 ~= nil then
		底图背景:显示((全局游戏宽度-800)/2+502,321)
	end
 	if self.加载结束==false then 
		return 0 
	end
	if self.被遮盖特效 ~= nil then
		for n=1,#self.被遮盖特效 do
			if self.被遮盖特效[n].特效 then
				self.被遮盖特效[n].特效:显示(self.拼接偏移.x + self.被遮盖特效[n].偏移.x ,self.拼接偏移.y + self.被遮盖特效[n].偏移.y)
			end
		end
	end

	sort(self.显示表,显示排序1)
	for i=1,#self.显示表 do
		if self.显示表[i].死亡方式 ~= 3 and not self.显示表[i].被捕捉 then
			tp.影子:显示(self.显示表[i].临时.x + self.显示表[i].附加.x,self.显示表[i].临时.y + self.显示表[i].附加.y)
		end
	end
	for i=1,#self.显示表 do
		if self.显示表[i].死亡方式 ~= 3 and not self.显示表[i].被捕捉 then
			self.显示表[i]:更新(dt,x,y)
			self.显示表[i]:显示(dt,x,y)
		end
	end
	for q=1,#self.显示表 do
		if self.显示表[q] ~= nil then
			self.显示表[q]:流血文字()
		end
	end

	if self.拼接特效 ~= nil then
		for n=1,#self.拼接特效 do
			if self.拼接特效[n].特效 then
			self.拼接特效[n].特效:显示(self.拼接偏移.x + self.拼接特效[n].偏移.x ,self.拼接偏移.y + self.拼接特效[n].偏移.y)
			end
		end
		if self.是否拼接 ~= true then
			self.是否拼接 = true
		end
	end
	if self.提示 ~= "" and self.提示 ~= nil then
		tp.提示:战斗提示(x,y,self.提示)
	end
	if not tp.隐藏UI then
		tp.窗口.聊天框类:显示(dt,x,y)
	end
	if self.回合进程 == 10 then
		tp.窗口.对话栏:显示(dt,x,y)
		if tp.窗口.对话栏:检查点(x,y) and mouseb(0) then
			tp.窗口.对话栏.可视 = false
		end
		if tp.窗口.对话栏.可视 == false then
			self.回合进程 = "命令回合"
		end
	end


	if self.回合进程=="命令回合" then
		self:命令回合显示()
	end
	if self.回合进程~="命令回合" then
		self.请等待:显示((全局游戏宽度-800)/2+320,60)
 	end
	if 低配模式 ==false then
		self.开场动画:显示()
	end
	if self.观战模式 then
		self.退出观战:显示((全局游戏宽度-800)/2+720, 510)

	end
end
function 场景类_战斗:更新(dt,x,y)
	if self.渐变音量 < tp.系统设置.声音设置[1] then
		self.渐变音量 = self.渐变音量 + 2
		self.背景音乐:置音量(self.渐变音量)
	end
	if self.加载结束==false then 
		return 0 
	end
	if self.回合进程=="命令回合" and self.观战模式 == false then
		self:命令回合更新(dt,x,y)
	elseif self.回合进程=="执行回合" then
		self:执行回合更新(dt,x,y)
	end
		sort(self.窗口_,UI排序)
	if mousea(0) and self.选中窗口 ~= 0 and self.窗口_[self.选中窗口].可移动 and not tp.消息栏焦点 then
		self.窗口_[self.选中窗口]:初始移动(x,y)
	end
	if mouseb(0) or self.隐藏UI or self.消息栏焦点 then
		self.移动窗口 = false
	end
	if self.移动窗口 then
		self.窗口_[#self.窗口_]:开始移动(x,y)
	end
	if mouseb(0) then
		self.移动窗口 = false
	end
	if self.被遮盖特效 ~= nil then
		for n=1,#self.被遮盖特效 do
			self.被遮盖特效[n].特效:更新(dt*1.15)
		end
	end
	if self.拼接特效 ~= nil then
		for n=1,#self.拼接特效 do
			if self.拼接特效[n].特效 then
				self.拼接特效[n].特效:更新(dt*1.15)
			end

		end
	end
	for n=1,#self.窗口_ do
		if self.选中窗口 ~= 0 and self.窗口_[self.选中窗口].ID == self.窗口_[n].ID then
			self.窗口_[n].鼠标 = true
		else
			self.窗口_[n].鼠标 = false
		end
		if not tp.隐藏UI then
			self.窗口_[n]:更新(dt,x,y)
		else
			if self.窗口_[n].ID == 1 then
				self.窗口_[n]:更新(dt,x,y)
			end
		end
	end
	if 低配模式 ==false then
	self.开场动画:更新(dt)
	end
		if self.观战模式 then
			self.退出观战:更新(x,y)
		if self.退出观战:事件判断() then
				客户端:发送数据(1, 239, 24, "77")
		end
	end
			for i=2,#self.人物资源组 do
			self.人物资源组[i]:更新(x,y)
		end
		for i=2,#self.召唤兽资源组 do
			self.召唤兽资源组[i]:更新(x,y)
		end

	-- 窗口逻辑结束
end
-------------------------------------
function 场景类_战斗:添加参战单位1(数据,id)-----召唤宝宝



 self.召唤数据={数据=数据,id=id}
 self.参战数据[self.召唤数据.id]=table.loadstring(table.tostring(self.召唤数据.数据))
 if self.参战数据[self.召唤数据.id].队伍id==队伍id then
	self.我方数量=self.我方数量+1
	self.参战数据[self.召唤数据.id].我方=true
	self.参战数据[self.召唤数据.id].单位=self.战斗(self.参战数据[self.召唤数据.id],1)
 else
	self.敌方数量=self.敌方数量+1
	self.参战数据[self.召唤数据.id].我方=false
	self.参战数据[self.召唤数据.id].单位=self.战斗(self.参战数据[self.召唤数据.id],2)
	end
	self.显示表 = {}
	for k,v in pairs(self.参战数据) do
		table.insert(self.显示表,v.单位)
	end
 end
function 场景类_战斗:添加单位(数据)
	self.单位总数=self.单位总数-1

	self.临时数据=数据
	self.参战数据[self.临时数据.编号]=数据
	if self.参战数据[self.临时数据.编号].队伍id==队伍id then
		self.我方数量=self.我方数量+1
		self.参战数据[self.临时数据.编号].我方=true
		self.参战数据[self.临时数据.编号].单位=self.战斗(self.参战数据[self.临时数据.编号],1)
	else
		self.敌方数量=self.敌方数量+1
		self.参战数据[self.临时数据.编号].我方=false
		self.参战数据[self.临时数据.编号].单位=self.战斗(self.参战数据[self.临时数据.编号],2)
	end

 if	self.临时数据.单位消失 then 
	self.参战数据[self.临时数据.编号].单位.临时.x=9999
	self.参战数据[self.临时数据.编号].单位.临时.y=9999
 end
 
 if self.单位总数<=0 then
 	self.显示表 = {}
	for k,v in pairs(self.参战数据) do
		table.insert(self.显示表,v.单位)
	end
	self.加载结束=true
	客户端:发送数据(3, 232, 24, "77", 0)
	end

 end

function 场景类_战斗:准备战斗(观战,boss)
	tp:关闭窗口()
	if tp.窗口.对话栏.可视 then
		tp.窗口.对话栏.可视=false
	end
	if 低配模式 ==false then
		self.开场动画 = require "Script/Fight/开场动画"()
	end
	
	local 音乐_ = {"战斗1","战斗2","战斗3"}
	音乐 = 音乐_[random(1,#音乐_)]
	if 是否BOSS战 ~= nil then
		音乐 = "战斗5"
	end
	if 结束事件 ~= nil then
		if 结束事件[1] == "抓鬼" then
			音乐 = "战斗5"
		end
		self.结束事件 = 结束事件
	end
	self.背景音乐 = tp.资源:载入("Audio/"..音乐..".mp3","音乐",nil)
	if tp.系统设置.声音设置[3] then
		self.背景音乐:播放(true)
	end
	tp.系统设置.声音设置[1] = tp.系统设置.声音设置[1] or 0
	self.背景音乐:置音量(tp.系统设置.声音设置[1]*0.4)
	self.渐变音量 = 0
		if 观战 == nil then
		self.观战模式 = false
	else
		self.观战模式 = true
	end
		local jl = require("gge精灵类")
	self.纯色背景 = jl(0,0,0,全局游戏宽度,全局游戏高度)
	self.纯色背景:置颜色(0x96101C4A) ---1189208494
	self.黑幕背景 = jl(0,0,0,全局游戏宽度,全局游戏高度)
	self.黑幕背景:置颜色(2097152000)
	self:战斗初始化()
 
	tp:关闭窗口()
	tp.消息栏焦点 = false
	tp.战斗中 = true
	tp.音乐:停止()
end
-------------------------------
function 场景类_战斗:设置命令回合(数据)
	self.回合进程="命令回合"
	self.命令类型="攻击"
	self.命令对象=2
	self.命令结束={}
	self.自动起始=os.time()
	self.操作单位={}
	self.当前单位=1
	self.命令版面=true
	self.捕捉开关=false
	self.法术开关=false
	self.召唤开关=false
	self.自动减少=true
	self.特技开关=false
	self.道具开关=false
	self.助战自动={}
	for n=1,#数据 do
		if self.参战数据[数据[n].编号]~=nil then
			if 数据[n].助战 then
				self.助战自动[#self.助战自动+1] = 数据[n]
				if 数据[n].自动 then
				else
					self.操作单位[#self.操作单位+1]={助战=true,名称=数据[n].名称,编号=数据[n].编号,类型="",目标="",附加=0,参数=0}
					if self.参战数据[self.操作单位[#self.操作单位].编号].单位.数据.付费 then
						self.自动减少 = false
					end
				end
			else
				self.操作单位[#self.操作单位+1]={名称=数据[n].名称,编号=数据[n].编号,类型="",目标="",附加=0,参数=0}
				if self.参战数据[self.操作单位[#self.操作单位].编号].单位.数据.付费 then
					self.自动减少 = false
				end
			end
		end
 	end
	self.命令数据={计时=游戏时间,分=6,秒=0}
end

function 场景类_战斗:取消助战托管(数据)
	for n,v in pairs(self.助战自动) do
		if v.编号 == 数据.编号 then
			self.助战自动[n].自动 = 数据.自动
		end
	end
	self.窗口.助战栏:刷新()
end

function 场景类_战斗:刷新法宝数据(数据)
	if self.回合进程~="命令回合" then
		return 0
	end
	self.窗口.法宝栏:打开(数据)
end
 
function 场景类_战斗:命令回合更新(dt,x,y)
	if self.参战数据[self.操作单位[self.当前单位].编号].战斗类型=="角色" and self.命令版面 then
		if self.人物资源组[2]:事件判断()or (引擎.按键按住(KEY.ALT) and 引擎.按键弹起(KEY.W)) then
			self:指令完毕()
			self.窗口.技能栏:打开(self.参战数据[self.操作单位[self.当前单位].编号])
		elseif self.人物资源组[3]:事件判断() then
			self:指令完毕()
			客户端:发送数据(1, 240, 24, "91")
		elseif self.人物资源组[4]:事件判断() then
			self.窗口.特技栏:打开(self.参战数据[self.操作单位[self.当前单位].编号].特技数据)
		elseif self.人物资源组[5]:事件判断() or (引擎.按键按住(KEY.ALT) and 引擎.按键弹起(KEY.E))then
							self:指令完毕()
							客户端:发送数据(1, 236, 24, "91")
			elseif self.人物资源组[6]:事件判断() or (引擎.按键按住(KEY.ALT) and 引擎.按键弹起(KEY.D))then
				self.操作单位[self.当前单位].类型="防御"
				self.操作单位[self.当前单位].目标=0
				if self.当前单位==#self.操作单位 then
					self:指令完毕()
					self.回合进程="等待回合"
					客户端:发送数据(1, 235, 24, self:转文本(self.操作单位))
				else
					self.当前单位=self.当前单位+1
				end
			elseif self.人物资源组[7]:事件判断()or (引擎.按键按住(KEY.ALT) and 引擎.按键弹起(KEY.T)) then
				self:指令完毕()
				self.命令类型="保护"
				self.命令对象=1
				tp.鼠标.置鼠标("保护")
				self.命令版面=false
				tp.提示:写入("#Y/再次按下鼠标右键可解除保护指令")
			elseif self.人物资源组[9]:事件判断() then
			if self.窗口.召唤栏.可视 then
					self.窗口.召唤栏:打开()
				else
					客户端:发送数据(1, 237, 24, "91")
					end
			elseif self.人物资源组[10]:事件判断() then
				--------召还
			elseif self.人物资源组[11]:事件判断() then
				if self.操作单位[self.当前单位].助战 then
					客户端:发送数据(1, 242, 24, table.tostring({编号=self.操作单位[self.当前单位].编号}))
				else
					客户端:发送数据(1, 241, 24, "91")
					自动战斗=true
					自动回合=99999
					tp.提示:写入("#Y/你开启了自动战斗，在命令回合开始3秒后自动下达指令")
				end
			elseif self.人物资源组[12]:事件判断()or (引擎.按键按住(KEY.ALT) and 引擎.按键弹起(KEY.G)) then
				self:指令完毕()
				self.命令类型="捕捉"
				self.命令对象=2
				tp.鼠标.置鼠标("捕捉")
				self.命令版面=false
				tp.提示:写入("#Y/再次按下鼠标右键可解除捕捉指令")
			elseif self.人物资源组[13]:事件判断() then
				self.操作单位[self.当前单位].类型="逃跑"
				self.操作单位[self.当前单位].目标=0
				if self.当前单位==#self.操作单位 then
				self.回合进程="等待回合"
				self:指令完毕()
				客户端:发送数据(1, 235, 24, self:转文本(self.操作单位))
				else
				self.当前单位=self.当前单位+1
				end
			end
	elseif self.命令版面 then

			if self.召唤兽资源组[2]:事件判断() or (引擎.按键按住(KEY.ALT) and 引擎.按键弹起(KEY.W)) then
			self:指令完毕()
			self.窗口.技能栏:打开(self.参战数据[self.操作单位[self.当前单位].编号])
			elseif self.召唤兽资源组[3]:事件判断() or (引擎.按键按住(KEY.ALT) and 引擎.按键弹起(KEY.E)) then
				self:指令完毕()
				客户端:发送数据(1, 236, 24, "91")
			elseif self.召唤兽资源组[4]:事件判断() or (引擎.按键按住(KEY.ALT) and 引擎.按键弹起(KEY.D)) then
				self.操作单位[self.当前单位].类型="防御"
				self.操作单位[self.当前单位].目标=0
				if self.当前单位==#self.操作单位 then
				self:指令完毕()
				self.回合进程="等待回合"
				客户端:发送数据(1, 235, 24, self:转文本(self.操作单位))
				else
				self.当前单位=self.当前单位+1
				end
			elseif self.召唤兽资源组[5]:事件判断() or (引擎.按键按住(KEY.ALT) and 引擎.按键弹起(KEY.T)) then
				self:指令完毕()
				self.命令类型="保护"
				self.命令对象=1
				tp.鼠标.置鼠标("保护")
				self.命令版面=false
				tp.提示:写入("#Y/再次按下鼠标右键可解除保护指令")
			elseif self.召唤兽资源组[6]:事件判断() then
				self.操作单位[self.当前单位].类型="逃跑"
				self.操作单位[self.当前单位].目标=0
				if self.当前单位==#self.操作单位 then
				self.回合进程="等待回合"
				self:指令完毕()
				客户端:发送数据(1, 235, 24, self:转文本(self.操作单位))
				else
				self.当前单位=self.当前单位+1
				end
			end
	end
	self:单位选择(x,y)
	self:按键事件()
	local 助战托管数量 = 0
	for n,v in pairs(self.助战自动) do
		if v.自动 then
			助战托管数量 = 助战托管数量 + 1
		end
	end
	if 助战托管数量 > 0 then
		if self.窗口.助战栏.可视化==false then
			self.窗口.助战栏:打开()
		end
	else
		if self.窗口.助战栏.可视化 then
			self.窗口.助战栏:打开()
		end
	end
	if 自动战斗 then
		if self.窗口.自动栏.可视化==false then
			self.窗口.自动栏:打开()
		end
		if os.time()-self.自动起始>=3 then
			for i=1,#self.操作单位 do
				local num	
				if self.参战数据[self.操作单位[self.当前单位].编号].战斗类型 =="角色" then 
					num =7
				else 
					num =9	
				end

				if tp.系统设置.战斗设置[num] == "默认法术" then
					self:自动法术()
				elseif tp.系统设置.战斗设置[num] == "普通攻击" then
					self.操作单位[self.当前单位].类型 = "攻击"
					self.操作单位[self.当前单位].参数 = ""
					local 敌方单位组={}
					for n=1,#self.参战数据 do
						if self.参战数据[n].单位.敌我==2 then
							敌方单位组[#敌方单位组+1]=n
						end
					end
					self.操作单位[self.当前单位].目标 = 敌方单位组[取随机数(1, #敌方单位组)]
				elseif tp.系统设置.战斗设置[num] == "防御" then
					self.操作单位[self.当前单位].类型="防御"
					self.操作单位[self.当前单位].目标=0
				else
					self:自动法术()
				end
				if self.当前单位==#self.操作单位 then
				self:指令完毕()
				self.回合进程="等待回合"
				客户端:发送数据(1, 235, 24, self:转文本(self.操作单位))
				else
				self.当前单位=self.当前单位+1
				end
			end

			self.自动起始=os.time()
			if self.自动减少 then
				自动回合=自动回合-1
				if 自动回合<5 then
				tp.提示:写入("#y/您当前的自动回合数还剩"..自动回合.."回合")
				end
				if 自动回合<=0 then
				自动战斗=false
				tp.提示:写入("#Y/你的自动战斗功能已经被取消")
				end
			end
		end
	end
	if 引擎.鼠标弹起(1) then
		if tp.鼠标:取当前()~= "普通" then
		self.命令版面=true
		self.命令类型="攻击"
		self.命令对象=2
		tp.鼠标.置鼠标("普通")
		end
		end
 end
function 场景类_战斗:命令回合结束()
 	self:指令完毕()
 	self.回合进程="等待回合"
 end
function 场景类_战斗:命令回合显示()
	if self.加载结束==false then 
		return 0 
	end
	self.秒显示=0
	self.分显示=0
	self.结果 = 游戏时间 - self.命令数据.计时
	self.显示时间=0
	if self.结果>=1 then
		self.命令数据.计时=游戏时间
		self.命令数据.秒=self.命令数据.秒-1
		if self.命令数据.秒<0 then
			if self.命令数据.分<=0 and self.命令数据.秒<=0 then
				self:命令回合结束()
				self.显示时间=1
			elseif self.命令数据.秒<=0 then
				self.命令数据.秒=9
				self.命令数据.分=self.命令数据.分-1
			end
		end
	end
	if self.显示时间 == 0 then
		self.分显示=self.命令数据.分+1
		if self.分显示>10 then
			self.分显示=1
		end
		self.秒显示=self.命令数据.秒+1
		if self.秒显示>10 then
			self.秒显示=1
		end
		self.数字图片[self.分显示]:显示((全局游戏宽度-800)/2+350,60)
		self.数字图片[self.秒显示]:显示((全局游戏宽度-800)/2+400,60)
 	end
	if self.操作单位[self.当前单位] == nil then
		tp.提示:写入("数据出现错误，请重新启动客户端，请截图给GM。代码为"..self.当前单位.."/"..#self.操作单位)
		return
	end
	if self.命令版面 and not 自动战斗 then
		local 显示文字 = "请为"..self.操作单位[self.当前单位].名称.."设置战斗命令……"
		self.方正字体:显示((全局游戏宽度-100-self.方正字体:取宽度(显示文字)),全局游戏高度-100,显示文字)
	end
	if self.参战数据[self.操作单位[self.当前单位].编号].战斗类型=="角色" and self.命令版面 and not 自动战斗 then
 		self.人物资源组[1]:显示((全局游戏宽度-800)/2+687,168)
		for i=2,#self.人物资源组 do
			self.人物资源组[i]:显示((全局游戏宽度-800)/2+690,124+i*24)
		end
	elseif self.命令版面 and not 自动战斗 then
		self.召唤兽资源组[1]:显示((全局游戏宽度-800)/2+687,337)
		for i=2,#self.召唤兽资源组 do
			self.召唤兽资源组[i]:显示((全局游戏宽度-800)/2+690,293+i*24)
		end
 	end
 end
function 场景类_战斗:转文本(q)
	local 组合文本 = ""
	for n, v in pairs(q) do
		if q[n].参数 == nil then
			q[n].参数 = "1"
		end
		if q[n].附加 == nil then
			q[n].附加 = "1"
		end
		组合文本 = 组合文本 .. q[n].编号 .. "@-@" .. q[n].目标 .. "@-@" .. q[n].类型 .. "@-@" .. q[n].参数 .. "@-@" .. q[n].附加 .. "@-@*-*"
	end
	return 组合文本
end
function 场景类_战斗:按键事件()
 if 引擎.按键按住(KEY.ALT) and 引擎.按键弹起(KEY.A) then
	self.操作单位[self.当前单位].类型="攻击"
	local 敌方单位组={}
		for n=1,#self.参战数据 do
				if self.参战数据[n].单位.敌我==2 then
				敌方单位组[#敌方单位组+1]=n
				end
			end
			self.操作单位[self.当前单位].目标 = 敌方单位组[取随机数(1, #敌方单位组)]
			self:指令完毕()
	if self.当前单位==#self.操作单位 then
		self.回合进程="等待回合"
		客户端:发送数据(1, 235, 24, self:转文本(self.操作单位))
	else
		self.命令类型="攻击"
		self.命令版面=true
		self.当前单位=self.当前单位+1
	end
	elseif 引擎.按键按住(KEY.ALT) and 引擎.按键弹起(KEY.Q) then
	self:使用默认法术(self.操作单位[self.当前单位].编号)
 	end
 end
function 场景类_战斗:单位选择(x,y)
	self.选中单位=0
	self.选择结果=false
	for n=1,#self.参战数据 do
		if self.窗口.技能栏.可视==false and self.窗口.法宝栏.可视==false and self.窗口.特技栏.可视==false and self.窗口.道具栏.可视==false and self.窗口.召唤栏.可视==false and self.参战数据[n].单位.动画[self.参战数据[n].单位.状态行为]:是否选中(x,y) then
			if self.命令类型=="攻击" and (self.参战数据[n].单位.位置 == 3 or 引擎.按键按住(KEY.CTRL) ) and self.参战数据[n].单位.数据.编号~=self.操作单位[self.当前单位].编号 then
				tp.鼠标.置鼠标("攻击")
				self.选中单位=n
			end
			if 引擎.鼠标弹起(0) and self.选择结果==false then
				self.选择结果=true
				if (self.命令对象==self.参战数据[n].单位.敌我 ) then
					if self.命令类型=="保护" and self.参战数据[n].单位.数据.编号==self.操作单位[self.当前单位].编号 then
						tp.提示:写入("#y/你无法选择自己作为保护目标")
						return 0
					end
					if self.命令类型=="道具" and ItemData[self.窗口.道具栏.物品[self.命令参数].物品.名称].种类 and ItemData[self.窗口.道具栏.物品[self.命令参数].物品.名称].种类== "酒" and self.参战数据[n].单位.数据.编号~=self.操作单位[self.当前单位].编号 then
						tp.提示:写入("#Y/这东西只能对自己使用")
						return 0
					end
					self.操作单位[self.当前单位].类型=self.命令类型
					self.操作单位[self.当前单位].目标=self.参战数据[n].单位.数据.编号
					self.操作单位[self.当前单位].参数=self.命令参数
					if self.当前单位==#self.操作单位 then
						self.回合进程="等待回合"
						self:指令完毕()
						客户端:发送数据(1, 235, 24, self:转文本(self.操作单位))
					else
						self.当前单位=self.当前单位+1
						self.命令版面=true
						self.捕捉开关=false
						self.法术开关=false
						self.保护开关=false
						self.命令类型="攻击"
						self.命令对象=2
						tp.鼠标.置鼠标("普通")
					end
				elseif self.命令类型=="攻击" and 引擎.按键按住(KEY.CTRL) and self.参战数据[n].单位.数据.编号~=self.操作单位[self.当前单位].编号 then
					self.操作单位[self.当前单位].类型=self.命令类型
					self.操作单位[self.当前单位].目标=self.参战数据[n].单位.数据.编号
					self.操作单位[self.当前单位].参数=self.命令参数
					if self.当前单位==#self.操作单位 then
						self.回合进程="等待回合"
						self:指令完毕()
						客户端:发送数据(1, 235, 24, self:转文本(self.操作单位))
					else
						self.当前单位=self.当前单位+1
						self.命令版面=true
						self.捕捉开关=false
						self.法术开关=false
						self.保护开关=false
						self.命令类型="攻击"
						self.命令对象=2
						tp.鼠标.置鼠标("普通")
					end
				end
			end
		end
	end
	if self.选中单位==0 then
		if self.命令类型=="攻击" then
			tp.鼠标.置鼠标("普通")
		end
 	end
 end
function 场景类_战斗:指令完毕()
	tp.鼠标.置鼠标("普通")
 	for n=1,#self.窗口_ do
		if self.窗口_[n].可视 then
			self.窗口_[n]:打开()
		end
	end
end
--------------------------------
function 场景类_战斗:退出战斗()
	tp.战斗中 = false


	if self.背景音乐 then
	self.背景音乐:停止()
	end
	if tp.系统设置.声音设置[3] then
		tp.音乐:播放(true)
		tp.音乐:置音量(math.floor(tp.系统设置.声音设置[1]))
	end
	
	self.参战数据={}
	self.显示表={}
	if self.窗口.助战栏.可视化 then
		self.窗口.助战栏:打开()
	end
	if self.窗口.自动栏.可视化 then
		self.窗口.自动栏:打开()
	end

	tp.窗口.快捷技能栏.操作员 = nil
	self:战斗初始化()
	if 引擎.失去焦点 then
		引擎.执行一次 = true
	end
	tp.音乐:置音量(floor(tp.系统设置.声音设置[1]))
	if 低配模式 ==false then
	tp.场景.渐变动画:初始化() 
	end

end
function 场景类_战斗:附加特效(tx)
	底图背景 = tp:载入特效(tx)
	return
end
function 场景类_战斗:删除特效()
	底图背景 = nil
	return
end

function 场景类_战斗:更新状态组(数据)
 if tp.战斗中 then
	for n = 1, #数据, 1 do
		if self.参战数据[数据[n].id] then
				self.参战数据[数据[n].id].单位:删除状态(数据[n].名称)

				if 数据[n].名称 == "鬼魂术" then
					self.参战数据[数据[n].id].单位.行为 = "待战"
					self.参战数据[数据[n].id].单位.回合行动 = 0
					self.参战数据[数据[n].id].单位.真正死亡 = false
					self.参战数据[数据[n].id].单位.死亡方式 = 0
					self.参战数据[数据[n].id].单位:流血逻辑(数据[n].气血,"加血")
				end
				-- elseif 数据[n].名称 == "隐身" or 数据[n].名称 == "分身术" then
				--	self.参战数据[数据[n].id].单位.隐身 = false
				--	self.参战数据[数据[n].id].单位.动画:置颜色("待战", ARGB(255, 255, 255, 255))
				-- elseif 数据[n].名称 == "金刚护法" or 数据[n].名称 == "逆鳞" then
				--	self.参战数据[数据[n].id].单位.闪光模式.开关 = false
				--	self.参战数据[数据[n].id].单位.动画[self.参战数据[数据[n].id].单位.动作]:取消高亮()
				-- end
		end
	end
end
end
 --------------------------
function 场景类_战斗:设置执行回合(数据)
	self.战斗流程=数据.流程
	self.战斗时间 = 数据.时间
	self.回合进程="执行回合"
	--显示提示
	for n = 1, #self.战斗流程.信息提示, 1 do
		if self.战斗流程.信息提示[n].id == 系统处理类.数字id or self.战斗流程.信息提示[n].id == 0 then
			tp.提示:写入(self.战斗流程.信息提示[n].内容)
		end
	end

	if #self.战斗流程==0 then
		self.回合进程="等待回合"
		客户端:发送数据(5, 234, 24, self.战斗时间)
	end
 end

function 场景类_战斗:执行回合更新(dt)
	if self.战斗流程[1]==nil then return 0 end
	if self.战斗流程[1].执行==false then
		self.战斗流程[1].执行=true
		self.执行流程=self.战斗流程[1].流程
		if self.战斗流程[1].类型=="技能" and self.战斗流程[1].流程~=100	then
			if self.参战数据[self.战斗流程[1].攻击方]==nil then
			tp.提示:写入("#Y/错误的技能参数#R/"..self.战斗流程[1].参数)
			table.remove(self.战斗流程,1)
			end
		elseif self.战斗流程[1].类型=="特技" then
			-- tp.场景.战斗提示=self.参战数据[self.战斗流程[1].攻击方].单位.数据.名称 .. "使用了" .. self.战斗流程[1].参数
			self.参战数据[self.战斗流程[1].攻击方].单位:添加特技内容(self.战斗流程[1].参数)
		end
	end
	if self.战斗流程[1].类型=="攻击" then
		self:攻击事件()
	elseif self.战斗流程[1].类型=="捕捉" then
		self:捕捉事件()
	elseif self.战斗流程[1].类型=="技能" then
		self:技能事件()
	elseif self.战斗流程[1].类型=="特技" then
		self.战斗流程[1].类型="技能"
		self:技能事件()
	elseif self.战斗流程[1].类型=="逃跑" then
		self:逃跑事件()
	elseif self.战斗流程[1].类型=="变身" then
	if self.执行流程==1 then
				local 临时造型 =self.战斗流程[1].参数
				self.参战数据[self.战斗流程[1].攻击方].单位.数据.造型 =临时造型
				local zyw = 取模型(临时造型)
				self.参战数据[self.战斗流程[1].攻击方].单位.武器 ={}
				self.参战数据[self.战斗流程[1].攻击方].单位.武器特效={} 
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.待战 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.待战,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.跑去 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.跑去,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.防御 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.防御,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.攻击 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw[self.参战数据[self.战斗流程[1].攻击方].单位.攻击类型],true)
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.挨打 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.挨打,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.返回 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.跑去,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.死亡 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.死亡,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.施法 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.施法,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.行走 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.跑去,true)
				self.执行流程=2
			elseif self.执行流程==2	then
				-- if self.战斗流程[1].类型 == "变身" then
				self.参战数据[self.战斗流程[1].攻击方].单位.施法前播放 = false
				self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 =false
				self.参战数据[self.战斗流程[1].攻击方].单位.特效 = self.参战数据[self.战斗流程[1].攻击方].单位:附加特效(self.战斗流程[1].类型)
				self.参战数据[self.战斗流程[1].攻击方].单位.行为 = "召唤法术"
				-- else
				--	self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕=true
				-- end
				self.执行流程=3
			elseif self.执行流程==3 and self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 then
					self.参战数据[self.战斗流程[1].攻击方].单位.回合行动=0
					table.remove(self.战斗流程,1)
			end
	elseif self.战斗流程[1].类型=="攻之械" or self.战斗流程[1].类型=="八戒上身" or self.战斗流程[1].类型=="齐天大圣" then
	
			
			if self.执行流程==1 then
				local 临时造型
				if self.战斗流程[1].类型 == "攻之械"then
					临时造型 ="唯美鬼将"
				elseif self.战斗流程[1].类型 == "齐天大圣"then
					临时造型 ="齐天大圣"
				else 
						临时造型 ="超级猪小戒"
				end
				self.参战数据[self.战斗流程[1].攻击方].单位.数据.造型 =临时造型
				local zyw = 取模型(临时造型)
				self.参战数据[self.战斗流程[1].攻击方].单位.武器 ={}
				self.参战数据[self.战斗流程[1].攻击方].单位.武器特效={}
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.待战 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.待战,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.跑去 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.跑去,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.防御 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.防御,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.攻击 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw[self.参战数据[self.战斗流程[1].攻击方].单位.攻击类型],true)
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.挨打 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.挨打,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.返回 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.跑去,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.死亡 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.死亡,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.施法 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.施法,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.动画.行走 = tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.跑去,true)
			
			if self.战斗流程[1].类型 == "攻之械" then
				zyw = 取模型("武器_唯美鬼将")
				self.参战数据[self.战斗流程[1].攻击方].单位.武器.施法= tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.施法,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.武器.待战= tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.待战,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.武器.跑去= tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.跑去,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.武器.防御= tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.防御,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.武器.攻击= tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw[self.参战数据[self.战斗流程[1].攻击方].单位.攻击类型],true)
				self.参战数据[self.战斗流程[1].攻击方].单位.武器.挨打= tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.挨打,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.武器.返回= tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.跑去,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.武器.死亡= tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.死亡,true)
				self.参战数据[self.战斗流程[1].攻击方].单位.武器.行走= tp.资源:载入(zyw.战斗资源,"网易WDF动画",zyw.跑去,true)


				for i,v in pairs(self.参战数据[self.战斗流程[1].攻击方].单位.动画) do
					v:置染色(33,3,0)
				end
				for i,v in pairs(self.参战数据[self.战斗流程[1].攻击方].单位.武器) do
					v:置染色(33,3,0)
				end
			end

				self.执行流程=2
			elseif self.执行流程==2	then
				if self.战斗流程[1].类型 == "攻之械" or self.战斗流程[1].类型 == "齐天大圣" then
				self.参战数据[self.战斗流程[1].攻击方].单位.施法前播放 = false
				self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 =false
				self.参战数据[self.战斗流程[1].攻击方].单位.特效 = self.参战数据[self.战斗流程[1].攻击方].单位:附加特效(self.战斗流程[1].类型)
				self.参战数据[self.战斗流程[1].攻击方].单位.行为 = "召唤法术"
				else
					self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕=true
				end
				self.执行流程=3
			elseif self.执行流程==3 and self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 then
					self.参战数据[self.战斗流程[1].攻击方].单位.回合行动=0
					table.remove(self.战斗流程,1)
			end
	elseif self.战斗流程[1].类型=="召还" then
		
			if self.执行流程==1 then
				for i=1,#self.战斗流程[1].挨打方 do
					self.参战数据[self.战斗流程[1].挨打方[i]].单位.施法前播放 = false
					self.参战数据[self.战斗流程[1].挨打方[i]].单位.行动完毕 =false
					self.参战数据[self.战斗流程[1].挨打方[i]].单位.特效 = self.参战数据[self.战斗流程[1].挨打方[i]].单位:附加特效("召还")
					self.参战数据[self.战斗流程[1].挨打方[i]].单位.行为 = "召唤法术"
				end

				self.执行流程=2
			elseif self.执行流程==2	then
				local vvvv =0
				for i=1,#self.战斗流程[1].挨打方 do
					if self.参战数据[self.战斗流程[1].挨打方[i]].单位.行动完毕 then
						vvvv=vvvv+1
					end
				end
				if vvvv ==#self.战斗流程[1].挨打方 then
					for i=1,#self.战斗流程[1].挨打方 do
					self.参战数据[self.战斗流程[1].挨打方[i]].单位.临时.x =9999
						self.参战数据[self.战斗流程[1].挨打方[i]].单位.临时.y =9999
					
					end
					self.执行流程=3
				end

			elseif self.执行流程==3	then
					table.remove(self.战斗流程,1)
			end

	elseif self.战斗流程[1].类型=="召唤类" then
	
			if self.执行流程==1 then
				self.参战数据[self.战斗流程[1].攻击方].单位.施法前播放 = false
				self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 =false
				self.参战数据[self.战斗流程[1].攻击方].单位.行为 = "增益法术"
				self.参战数据[self.战斗流程[1].攻击方].单位.特效 = self.参战数据[self.战斗流程[1].攻击方].单位:附加特效(self.战斗流程[1].参数)
				self.执行流程=2
			elseif self.执行流程==2 and self.参战数据[self.战斗流程[1].攻击方].单位.行为=="待战" then
					for i=1,#self.战斗流程[1].数据 do
							self:添加参战单位1(self.战斗流程[1].数据[i],self.战斗流程[1].数据[i].编号)
							tp.场景.战斗提示=self.战斗流程[1].名称.."加入了战斗"
					end
					for i=1,#self.战斗流程[1].数据 do
						
						self.参战数据[self.战斗流程[1].数据[i].编号].单位.施法前播放 = false
						self.参战数据[self.战斗流程[1].数据[i].编号].单位.行动完毕 =false
						self.参战数据[self.战斗流程[1].数据[i].编号].单位.特效 = self.参战数据[self.战斗流程[1].数据[i].编号].单位:附加特效("召唤类")
						self.参战数据[self.战斗流程[1].数据[i].编号].单位.行为 = "召唤法术"

					end
		
				self.执行流程=3
			elseif self.执行流程==3 and self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 then
						local v=0
						for i=1,#self.战斗流程[1].数据 do
							local 挨打编号 = self.战斗流程[1].数据[i].编号
							if self.参战数据[挨打编号].单位.特效==nil and self.参战数据[挨打编号].单位.行动完毕	then
								-- self.参战数据[挨打编号].单位.回合行动 = 0
								-- self.参战数据[挨打编号].单位.行动完毕 =false
								v=v+1
							end
						end

						if v== #self.战斗流程[1].数据 then
							self.参战数据[self.战斗流程[1].攻击方].单位.回合行动=0

							self.执行流程=4
						end

			elseif self.执行流程==4 then		
					table.remove(self.战斗流程,1)
			end

	elseif self.战斗流程[1].类型=="召唤" and self.战斗流程[1].进行==false then
		self:添加参战单位1(self.战斗流程[1].数据,self.战斗流程[1].id)
		tp.场景.战斗提示=self.战斗流程[1].名称.."加入了战斗"
		self.战斗流程[1].进行=true
		table.remove(self.战斗流程,1)
	end
	if #self.战斗流程==0 then
		self.回合进程="等待回合"
		客户端:发送数据(5, 234, 24, self.战斗时间)
	end


 end

function 场景类_战斗:逃跑事件()
 	if self.执行流程==1 then
		self.参战数据[self.战斗流程[1].攻击方].单位.行为 ="逃跑"
		self.战斗流程[1].等待计时=os.time()
		self.执行流程=2
	elseif self.执行流程==2 and os.time()-self.战斗流程[1].等待计时>=3 then
			if self.战斗流程[1].结果 then
						self.执行流程 = 3
			else
					self.执行流程 = 4
				end
	elseif self.执行流程==3 then
		self.参战数据[self.战斗流程[1].攻击方].单位.临时.x=self.参战数据[self.战斗流程[1].攻击方].单位.临时.x+self.参战数据[self.战斗流程[1].攻击方].单位.逃跑坐标
		self.参战数据[self.战斗流程[1].攻击方].单位.临时.y=self.参战数据[self.战斗流程[1].攻击方].单位.临时.y+self.参战数据[self.战斗流程[1].攻击方].单位.逃跑坐标
		if self.战斗流程[1].逃跑单位 ==0 then
		if 取两点距离(生成XY(self.参战数据[self.战斗流程[1].攻击方].单位.临时.x,self.参战数据[self.战斗流程[1].攻击方].单位.临时.y),生成XY(self.参战数据[self.战斗流程[1].攻击方].单位.原始.x,self.参战数据[self.战斗流程[1].攻击方].单位.原始.y))>=500 then
			self.参战数据[self.战斗流程[1].攻击方].单位.真正死亡,self.参战数据[self.战斗流程[1].攻击方].单位.逃跑开关 = true,false
			self.执行流程=6
			end
		else
			self.参战数据[self.战斗流程[1].逃跑单位].单位.行为="逃跑"
		self.参战数据[self.战斗流程[1].逃跑单位].单位.临时.x=self.参战数据[self.战斗流程[1].逃跑单位].单位.临时.x+self.参战数据[self.战斗流程[1].逃跑单位].单位.逃跑坐标
		self.参战数据[self.战斗流程[1].逃跑单位].单位.临时.y=self.参战数据[self.战斗流程[1].逃跑单位].单位.临时.y+self.参战数据[self.战斗流程[1].逃跑单位].单位.逃跑坐标
		if 取两点距离(生成XY(self.参战数据[self.战斗流程[1].攻击方].单位.临时.x,self.参战数据[self.战斗流程[1].攻击方].单位.临时.y),生成XY(self.参战数据[self.战斗流程[1].攻击方].单位.原始.x,self.参战数据[self.战斗流程[1].攻击方].单位.原始.y))>=500 and 取两点距离(生成XY(self.参战数据[self.战斗流程[1].逃跑单位].单位.临时.x,self.参战数据[self.战斗流程[1].逃跑单位].单位.临时.y),生成XY(self.参战数据[self.战斗流程[1].逃跑单位].单位.原始.x,self.参战数据[self.战斗流程[1].逃跑单位].单位.原始.y))>=500 then
			self.参战数据[self.战斗流程[1].攻击方].单位.真正死亡,self.参战数据[self.战斗流程[1].攻击方].单位.逃跑开关 = true,false
					self.参战数据[self.战斗流程[1].逃跑单位].单位.真正死亡,self.参战数据[self.战斗流程[1].逃跑单位].单位.逃跑开关 = true,false
					self.执行流程=6
			end
		end

	elseif self.执行流程==4 then
		self.参战数据[self.战斗流程[1].攻击方].单位.行为 ="逃跑1"
		self.执行流程=5
	elseif self.执行流程==5 then
			self.参战数据[self.战斗流程[1].攻击方].单位.行为 ="逃跑2"
	elseif self.执行流程==6 then
		self.参战数据[self.战斗流程[1].攻击方].单位.回合行动 = 0
		if self.战斗流程[1].id==系统处理类.数字id and self.战斗流程[1].逃跑类型=="角色" then
		self:退出战斗()
		end
		table.remove(self.战斗流程,1)

		-- if self.战斗流程[1].结束 and tp.队伍[1].数字id==self.战斗流程[1].id then
		--	发送数据(5506)
		--	self.执行流程=999999
		-- else
		--	self.执行流程=6
		-- end

	end

 end
function 场景类_战斗:攻击事件()----------
 	if self.执行流程==1 then
		if type(self.战斗流程[1].挨打方)=="table" then
				self.参战数据[self.战斗流程[1].攻击方].单位.攻击次数= #self.战斗流程[1].挨打方
				local 临时编号 = {}
				for i=1,#self.战斗流程[1].挨打方 do
					临时编号[i]= self.战斗流程[1].挨打方[i].编号
					self.参战数据[临时编号[i]].单位.附加效果.攻击 = false
					self.参战数据[临时编号[i]].单位.行动完毕 =false
					self.参战数据[临时编号[i]].单位.追加开关 = false
				end
					self.参战数据[self.战斗流程[1].攻击方].单位.追加开关 = false
					self.参战数据[self.战斗流程[1].攻击方].单位.附加效果.攻击 = false
					self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 =false
				if tp.场景.战斗.战斗流程[1].参数== "惊涛怒" or tp.场景.战斗.战斗流程[1].参数== "破釜沉舟" then
					self.参战数据[self.战斗流程[1].攻击方].单位.行为 = "增益法术"
					self.执行流程=3
				else
					self.参战数据[self.战斗流程[1].攻击方].单位.目标.x=self.参战数据[self.战斗流程[1].挨打方[1].编号].单位.临时.x-self.参战数据[self.战斗流程[1].攻击方].单位.差异.x
					self.参战数据[self.战斗流程[1].攻击方].单位.目标.y=self.参战数据[self.战斗流程[1].挨打方[1].编号].单位.临时.y-self.参战数据[self.战斗流程[1].攻击方].单位.差异.y
					self.参战数据[self.战斗流程[1].攻击方].单位.行为 ="跑去"
					self.执行流程=2
				end
		else
			if self.战斗流程[1].动作 ~= nil then
				self.参战数据[self.战斗流程[1].攻击方].单位.攻击次数= #self.战斗流程[1].动作
			end
			self.参战数据[self.战斗流程[1].挨打方].单位.附加效果.攻击 = false
			self.参战数据[self.战斗流程[1].攻击方].单位.附加效果.攻击 = false
			self.参战数据[self.战斗流程[1].攻击方].单位.追加开关 = false
			self.参战数据[self.战斗流程[1].挨打方].单位.追加开关 = false
			self.参战数据[self.战斗流程[1].挨打方].单位.行动完毕 =false
			self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 =false
			if tp.场景.战斗.战斗流程[1].参数== "满天花雨" or tp.场景.战斗.战斗流程[1].参数== "力劈华山" then
						self.参战数据[self.战斗流程[1].攻击方].单位.行为 = "增益法术"
						self.执行流程=4
			else
				self.参战数据[self.战斗流程[1].攻击方].单位.目标.x=self.参战数据[self.战斗流程[1].挨打方].单位.临时.x-self.参战数据[self.战斗流程[1].攻击方].单位.差异.x
				self.参战数据[self.战斗流程[1].攻击方].单位.目标.y=self.参战数据[self.战斗流程[1].挨打方].单位.临时.y-self.参战数据[self.战斗流程[1].攻击方].单位.差异.y
				self.参战数据[self.战斗流程[1].攻击方].单位.行为 ="跑去"
				self.执行流程=2
			end
		end
	elseif self.执行流程==2 and self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 then
		if type(self.战斗流程[1].挨打方)=="table" then
			local 临时编号 = {}
			local v =0
			for i=1,#self.战斗流程[1].挨打方 do
				临时编号[i] = self.战斗流程[1].挨打方[i].编号
				if self.参战数据[临时编号[i]].单位.行动完毕	then
					self.参战数据[临时编号[i]].单位.回合行动 = 0
					v=v+1
				end
			end
			if v == #self.战斗流程[1].挨打方 then
			self.参战数据[self.战斗流程[1].攻击方].单位.回合行动 = 0
			table.remove(self.战斗流程,1)
			end
		else
			if self.参战数据[self.战斗流程[1].挨打方].单位.行动完毕	then
				self.参战数据[self.战斗流程[1].挨打方].单位.回合行动 = 0
				self.参战数据[self.战斗流程[1].攻击方].单位.回合行动 = 0
				table.remove(self.战斗流程,1)
			end
		end
	elseif self.执行流程==3 and self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 then
		self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 =false
		self.参战数据[self.战斗流程[1].攻击方].单位.攻击次数= #self.战斗流程[1].挨打方
				local 临时编号 = {}
				for i=1,#self.战斗流程[1].挨打方 do
					临时编号[i]= self.战斗流程[1].挨打方[i].编号
					self.参战数据[临时编号[i]].单位.附加效果.攻击 = false
					self.参战数据[临时编号[i]].单位.行动完毕 =false
					self.参战数据[临时编号[i]].单位.追加开关 = false
				end
					self.参战数据[self.战斗流程[1].攻击方].单位.追加开关 = false
					self.参战数据[self.战斗流程[1].攻击方].单位.附加效果.攻击 = false
					self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 =false
					self.参战数据[self.战斗流程[1].攻击方].单位.目标.x=self.参战数据[self.战斗流程[1].挨打方[1].编号].单位.临时.x-self.参战数据[self.战斗流程[1].攻击方].单位.差异.x
					self.参战数据[self.战斗流程[1].攻击方].单位.目标.y=self.参战数据[self.战斗流程[1].挨打方[1].编号].单位.临时.y-self.参战数据[self.战斗流程[1].攻击方].单位.差异.y
					self.参战数据[self.战斗流程[1].攻击方].单位.行为 ="跑去"
					self.执行流程=2
	elseif self.执行流程==4 and self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 and self.参战数据[ self.战斗流程[1].挨打方].单位.特效==nil and self.参战数据[self.战斗流程[1].攻击方].单位.行为 =="待战" then
			self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 =false
			self.参战数据[ self.战斗流程[1].挨打方].单位.行动完毕 =false
			self.参战数据[self.战斗流程[1].攻击方].单位.目标.x=self.参战数据[self.战斗流程[1].挨打方].单位.临时.x-self.参战数据[self.战斗流程[1].攻击方].单位.差异.x
			self.参战数据[self.战斗流程[1].攻击方].单位.目标.y=self.参战数据[self.战斗流程[1].挨打方].单位.临时.y-self.参战数据[self.战斗流程[1].攻击方].单位.差异.y
			self.参战数据[self.战斗流程[1].攻击方].单位.行为 ="跑去"
			self.执行流程=2

	elseif self.执行流程==5 then

	for i=1,#self.战斗流程[1].挨打方 do
		self.参战数据[self.战斗流程[1].挨打方[i].编号].单位.及时挨打=true
			self.参战数据[self.战斗流程[1].挨打方[i].编号].单位.行为 = "挨打"
		end
		self.执行流程=6
	elseif self.执行流程==6 then
		local js =0
		for i=1,#self.战斗流程[1].挨打方 do
			if self.参战数据[self.战斗流程[1].挨打方[i].编号].单位.行动完毕 and self.参战数据[self.战斗流程[1].挨打方[i].编号].单位.流血显示 ==false then
			js = js+1
			-- self.参战数据[self.战斗流程[1].挨打方[i].编号].单位.及时挨打=false
			end
	end
		if js ==#self.战斗流程[1].挨打方 then
				self.执行流程=2
		end


	end
 end
-------------------------------------
function 场景类_战斗:添加发言内容(id,内容)
 --寻找战斗id
	for n=1,#self.参战数据 do
		if self.参战数据[n].单位.数据.助战 == nil and self.参战数据[n].单位.数据.数字id==id and self.参战数据[n].单位.数据.战斗类型=="角色" then

			self.参战数据[n].单位:添加发言内容(内容)

		end
	end
 end
function 场景类_战斗:添加怪物发言内容(编号,内容)
	self.参战数据[编号].单位:添加发言内容(内容)
end
function 场景类_战斗:刷新召唤数据(数据)
 if self.回合进程~="命令回合" then
	return 0
 elseif self.参战数据[self.操作单位[self.当前单位].编号].战斗类型~="角色" then
	return 0
 else
	self.召唤数据={}
	self.加载文本={"参战","等级","名称","当前气血","气血上限","忠诚","当前魔法","魔法上限"}
	self.临时数据=数据
	for n=1,#self.临时数据 do
	self.召唤数据[n]={}
	for i=1,#self.加载文本 do
		self.召唤数据[n][self.加载文本[i]]=self.临时数据[n][self.加载文本[i]]
		end
	self.召唤数据[n].包围盒=包围盒.创建(323,140+n*20,64,18)
	self.召唤数据[n].包围盒:置坐标(323,140+n*20)
	self.召唤数据[n].包围盒:置宽高(64,18)
	end
	self.召唤开关=true
	self.召唤编号=0
	end
 end
function 场景类_战斗:刷新道具数据(数据)
 if self.回合进程~="命令回合" then
	return 0
 end

 self.窗口.道具栏:打开(数据)
 end
function 场景类_战斗:使用默认法术(数据)
	local 默认法术 = self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术
	local 默认法术编号=0
 if self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术=="" or self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术==nil then
	tp.提示:写入("#Y/请先设置好默认法术")
 else
		for i=1,#self.参战数据[数据].主动技能 do
			if self.参战数据[数据].主动技能[i].名称 == 默认法术 then
				默认法术编号=i
				break
			end
		end
		if 默认法术编号== 0 then
			tp.提示:写入("#Y/默认法术参数设置错误,请重新设置!")
			return
		end
		local SkillName=self.参战数据[数据].主动技能[默认法术编号].名称
		if SkillData[SkillName].类型 ==4 then	
				self.命令类型="技能"
				self.命令对象=2
				self.命令参数=默认法术
				tp.鼠标.置鼠标("道具")
				self.命令版面=false
		elseif SkillData[SkillName].类型 ==3 or SkillData[SkillName].类型 ==6 then
				self.命令类型="技能"
				self.命令对象=1
				self.命令参数=默认法术
				tp.鼠标.置鼠标("道具")
				self.命令版面=false
		elseif SkillData[SkillName].类型 ==2 or SkillData[SkillName].类型 ==5 then
				self.命令类型="技能"
				self.命令对象=1
				self.命令参数=默认法术
				self.命令版面=false
				self.操作单位[self.当前单位].类型=self.命令类型
				self.操作单位[self.当前单位].目标=self.操作单位[self.当前单位].编号
				self.操作单位[self.当前单位].参数=self.命令参数
					if self.当前单位==#self.操作单位 then
					tp.鼠标.置鼠标("普通")
					self.回合进程="等待回合"
					客户端:发送数据(1, 235, 24, self:转文本(self.操作单位))
				else
						self.当前单位=self.当前单位+1
						self.命令版面=true
						self.命令类型="攻击"
						self.命令对象=2
					end
		end
		for n=1,#self.窗口_ do
			if self.窗口_[n].可视 then
				self.窗口_[n]:打开()
			end
		end
 end
 end
function 场景类_战斗:自动使用法术()

	for n = 1, #self.操作单位, 1 do
		self.当前单位 = n
		self:自动法术()

	end

	self:指令完毕()
	self.回合进程 = "等待回合"

	客户端:发送数据(1, 235, 24, self:转文本(self.操作单位))
end
function 场景类_战斗:自动法术()
	self.敌方单位组 = {}
	self.我方单位组 = {}
	for n = 1, #self.参战数据, 1 do
		if self.参战数据[n].单位.敌我 == 2 then
			self.敌方单位组[#self.敌方单位组 + 1] = n
		elseif self.参战数据[n].单位.敌我 == 1 then
			self.我方单位组[#self.我方单位组 + 1] = n
		end
	end
	if self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术 == "" or self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术 == nil then
		self.操作单位[self.当前单位].类型 = "攻击"
		self.操作单位[self.当前单位].参数 = ""
		self.操作单位[self.当前单位].目标 = self.敌方单位组[取随机数(1, #self.敌方单位组)]
	else
		self.临时法术名称 = self.参战数据[self.操作单位[self.当前单位].编号].单位.数据.默认法术
		self.操作单位[self.当前单位].类型 = "技能"
		self.操作单位[self.当前单位].参数 = self.临时法术名称
		local 临时技能 = SkillData[self.临时法术名称]
		if SkillData[self.临时法术名称].类型 ==4 then
			self.操作单位[self.当前单位].目标 = self.敌方单位组[取随机数(1, #self.敌方单位组)]
		elseif SkillData[self.临时法术名称].类型 ==3 or SkillData[self.临时法术名称].类型 ==6 then
			self.操作单位[self.当前单位].目标 = self.我方单位组[取随机数(1, #self.我方单位组)]
		elseif SkillData[self.临时法术名称].类型 ==2 or SkillData[self.临时法术名称].类型 ==5 then
			self.操作单位[self.当前单位].目标 = self.操作单位[self.当前单位].编号
		end
	end
end


function 场景类_战斗:捕捉事件()
	local m = 0 -- 开关，必需要两个行为都开启的情况下才能继续
	if self.执行流程==1 and self.参战数据[self.战斗流程[1].攻击方].单位.行为=="待战" then
		self.参战数据[self.战斗流程[1].挨打方].单位.行动完毕 =false
		if self.参战数据[self.战斗流程[1].挨打方].单位.特效 == nil then
		self.参战数据[self.战斗流程[1].挨打方].单位.特效 = self.参战数据[self.战斗流程[1].攻击方].单位:附加特效("宠物_走")
		self.参战数据[self.战斗流程[1].挨打方].单位.特效:置方向(self.参战数据[self.战斗流程[1].攻击方].单位.方向)
		self.参战数据[self.战斗流程[1].挨打方].单位.当前 = {x=self.参战数据[self.战斗流程[1].攻击方].单位.临时.x,y=self.参战数据[self.战斗流程[1].攻击方].单位.临时.y}
		self.参战数据[self.战斗流程[1].挨打方].单位.行动特效 = true
		self.参战数据[self.战斗流程[1].挨打方].单位.特效影子 = true
		self.参战数据[self.战斗流程[1].攻击方].单位.目标.x=self.参战数据[self.战斗流程[1].挨打方].单位.临时.x
 		self.参战数据[self.战斗流程[1].攻击方].单位.目标.y=self.参战数据[self.战斗流程[1].挨打方].单位.临时.y
		else
			if 取两点距离(self.参战数据[self.战斗流程[1].挨打方].单位.当前,self.参战数据[self.战斗流程[1].攻击方].单位.目标) > 60 then
			local 步幅 = 取移动坐标(self.参战数据[self.战斗流程[1].挨打方].单位.当前,self.参战数据[self.战斗流程[1].攻击方].单位.目标,5)
			self.参战数据[self.战斗流程[1].挨打方].单位.当前 = 步幅

			else
				self.执行流程=2
			end
		end
	elseif self.执行流程==2 then
		self.参战数据[self.战斗流程[1].挨打方].单位.特效 = self.参战数据[self.战斗流程[1].攻击方].单位:附加特效("宠物_静")
		self.参战数据[self.战斗流程[1].挨打方].单位.特效:置方向(self.参战数据[self.战斗流程[1].攻击方].单位.方向)
		self.参战数据[self.战斗流程[1].攻击方].单位.目标 = {x=self.参战数据[self.战斗流程[1].攻击方].单位.临时.x,y=self.参战数据[self.战斗流程[1].攻击方].单位.临时.y}
		self.参战数据[self.战斗流程[1].挨打方].单位.行动特效 = true
		self.执行流程=3
	elseif self.执行流程==3 then
		if self.参战数据[self.战斗流程[1].攻击方].单位.特效 ~= nil then
			if self.参战数据[self.战斗流程[1].攻击方].单位.特效.当前帧 >= self.参战数据[self.战斗流程[1].攻击方].单位.特效.结束帧 then
			self.参战数据[self.战斗流程[1].挨打方].单位.特效 = self.参战数据[self.战斗流程[1].攻击方].单位:附加特效("宠物_走")
			self.参战数据[self.战斗流程[1].挨打方].单位.特效:置方向(self.参战数据[self.战斗流程[1].攻击方].单位.反方向)
			self.参战数据[self.战斗流程[1].挨打方].单位.行为 = "行走"
			self.参战数据[self.战斗流程[1].攻击方].单位.特效 = self.参战数据[self.战斗流程[1].攻击方].单位:附加特效("捕捉2")
			self.执行流程=4
			return false
			end
		end
		if self.参战数据[self.战斗流程[1].攻击方].单位.特效 == nil then
		self.参战数据[self.战斗流程[1].攻击方].单位.特效 = self.参战数据[self.战斗流程[1].攻击方].单位:附加特效("捕捉")
		self.参战数据[self.战斗流程[1].攻击方].单位.当前 = {x=self.参战数据[self.战斗流程[1].挨打方].单位.原始.x+5,y=self.参战数据[self.战斗流程[1].挨打方].单位.原始.y-10}
		self.参战数据[self.战斗流程[1].攻击方].单位.行动特效 = true
		end
	elseif self.执行流程==4 then
		local m =0
		if 取两点距离(self.参战数据[self.战斗流程[1].挨打方].单位.当前,self.参战数据[self.战斗流程[1].攻击方].单位.目标) > 45 then
			local 步幅 = 取移动坐标(self.参战数据[self.战斗流程[1].挨打方].单位.当前,self.参战数据[self.战斗流程[1].攻击方].单位.目标,5)
			self.参战数据[self.战斗流程[1].挨打方].单位.当前 = 步幅
		else
			m = m + 1
		end

			if 取两点距离(self.参战数据[self.战斗流程[1].攻击方].单位.当前,self.参战数据[self.战斗流程[1].攻击方].单位.目标) > 95 then
			local 步幅 = 取移动坐标(self.参战数据[self.战斗流程[1].攻击方].单位.当前,self.参战数据[self.战斗流程[1].攻击方].单位.目标,5)
			self.参战数据[self.战斗流程[1].攻击方].单位.当前 = 步幅
			self.参战数据[self.战斗流程[1].挨打方].单位.临时 = 步幅
			else
				m = m + 1
			end
		if m == 2 then
			if self.战斗流程[1].结果 then
				self.执行流程=5
			else
				self.执行流程=6
			end
		end
	elseif self.执行流程==5 then
		self.参战数据[self.战斗流程[1].挨打方].单位.临时.x,self.参战数据[self.战斗流程[1].挨打方].单位.临时.y=9999,9999
		self.参战数据[self.战斗流程[1].挨打方].单位.真正死亡 = true
		self.参战数据[self.战斗流程[1].挨打方].单位.特效 = nil
		self.参战数据[self.战斗流程[1].挨打方].单位.行动特效 = nil
		self.参战数据[self.战斗流程[1].攻击方].单位.特效 = nil
		self.参战数据[self.战斗流程[1].攻击方].单位.行动特效 = nil
		self.参战数据[self.战斗流程[1].挨打方].单位.被捕捉 = true
		tp.场景.战斗提示 = self.参战数据[self.战斗流程[1].攻击方].单位.名称.."捕捉"..self.参战数据[self.战斗流程[1].挨打方].单位.名称.."成功"
		self.执行流程=7
	elseif self.执行流程==6 then
		self.参战数据[self.战斗流程[1].挨打方].单位.特效 = nil
		self.参战数据[self.战斗流程[1].挨打方].单位.行动特效 = nil
		self.参战数据[self.战斗流程[1].攻击方].单位.特效 = nil
		self.参战数据[self.战斗流程[1].攻击方].单位.行动特效 = nil
		self.参战数据[self.战斗流程[1].挨打方].单位.行为 = "跑回"
	if self.参战数据[self.战斗流程[1].挨打方].单位.行动完毕 then
		tp.场景.战斗提示 = self.参战数据[self.战斗流程[1].攻击方].单位.名称.."捕捉"..self.参战数据[self.战斗流程[1].挨打方].单位.名称.."失败"
		self.执行流程=7
	end
	elseif self.执行流程==7 then
		self.参战数据[self.战斗流程[1].攻击方].单位.回合行动 = 0
		table.remove(self.战斗流程,1)

	end
 end
function 场景类_战斗:技能事件()
	if self.执行流程==1 then
 	self.参战数据[self.战斗流程[1].攻击方].单位.附加效果.施法 = false
 	self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 =false
	self.参战数据[self.战斗流程[1].攻击方].单位.行为 = "施法"
	self.执行流程=2
	elseif self.执行流程==2 and self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 then
 		local v=0
		for i=1,#self.战斗流程[1].挨打方 do
			local 挨打编号 = self.战斗流程[1].挨打方[i].编号
			if self.参战数据[挨打编号].单位.特效==nil and self.参战数据[挨打编号].单位.流血显示 ==false and (self.参战数据[挨打编号].单位.行为=="待战" or self.参战数据[挨打编号].单位.真正死亡) then
				self.参战数据[挨打编号].单位.回合行动 = 0
				self.参战数据[挨打编号].单位.行动完毕 =false
				v=v+1
			end
		end

			if v== #self.战斗流程[1].挨打方 then
				self.执行流程=3
			end
	elseif self.执行流程==3 then
 	table.remove(self.战斗流程,1)
	elseif self.执行流程==4 then
		self.参战数据[self.战斗流程[1].攻击方].单位.施法前播放 = false
		self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 =false
		self.参战数据[self.战斗流程[1].攻击方].单位.行为 = "增益法术"
		if type(tp.场景.战斗.战斗流程[1].挨打方) == "table" then
			self.执行流程=6
		else
			tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.回合行动 = 0
		self.执行流程=5
		end
	elseif self.执行流程==5 and self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 then

		if tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 ==nil then

			if self.战斗流程[1].封印结果 and self.战斗流程[1].参数~="勾魂" and self.战斗流程[1].参数~="炽火流离" and self.战斗流程[1].参数~="摄魄"then
				if self.战斗流程[1].参数=="归元咒" or self.战斗流程[1].参数=="活血"or self.战斗流程[1].参数=="净世煌火" or self.战斗流程[1].参数=="治疗"or self.战斗流程[1].参数=="仙人指路"or self.战斗流程[1].参数=="舍生取义" or self.战斗流程[1].参数=="气疗术"or self.战斗流程[1].参数=="命疗术"or self.战斗流程[1].参数=="命归术"or self.战斗流程[1].参数=="气归术" or self.战斗流程[1].参数=="三花聚顶"or self.战斗流程[1].参数=="星月之惠" or self.战斗流程[1].参数=="自在心法"or self.战斗流程[1].参数=="乾天罡气" or self.战斗流程[1].参数=="移星换斗"then
					self.参战数据[self.战斗流程[1].挨打方].单位:流血逻辑(self.战斗流程[1].法术伤害,self.战斗流程[1].伤害类型,"及时")

					if self.战斗流程[1].参数=="自在心法" then

						self.参战数据[self.战斗流程[1].挨打方].单位:删除状态("普渡众生")
					end
				elseif self.战斗流程[1].参数=="分身术"	then
					self.参战数据[self.战斗流程[1].挨打方].单位:增加状态(self.战斗流程[1].参数,4)
				elseif	self.战斗流程[1].参数=="放下屠刀"or self.战斗流程[1].参数== "野兽之力"or self.战斗流程[1].参数=="光辉之甲" or self.战斗流程[1].参数=="笑里藏刀" or self.战斗流程[1].参数=="凝气诀"or self.战斗流程[1].参数=="凝神诀"	then
				elseif self.战斗流程[1].参数=="解封" or self.战斗流程[1].参数=="驱魔" or self.战斗流程[1].参数=="寡欲令" or self.战斗流程[1].参数=="八戒上身" or self.战斗流程[1].参数=="魂飞魄散" or self.战斗流程[1].参数=="复苏" or self.战斗流程[1].参数=="宁心" or self.战斗流程[1].参数=="冰清诀" or self.战斗流程[1].参数=="水清诀" or self.战斗流程[1].参数=="解毒" or self.战斗流程[1].参数=="驱尸" or self.战斗流程[1].参数=="清心" then
					local 解除名称 = {}
					if self.战斗流程[1].参数=="解封" or self.战斗流程[1].参数=="宁心" then
						解除名称={"如花解语","似玉生香","莲步轻舞","娉婷嬝娜"}
					elseif self.战斗流程[1].参数=="驱魔" then
						解除名称={"离魂符","失心符","追魂符","催眠符","定身符","失魂符","失忆符"}
					elseif self.战斗流程[1].参数=="寡欲令" then
						解除名称={"含情脉脉"}
						self.参战数据[self.战斗流程[1].挨打方].单位:增加状态(self.战斗流程[1].参数,4,self.战斗流程[1].参数)
					elseif self.战斗流程[1].参数=="魂飞魄散" or self.战斗流程[1].参数=="八戒上身" then
						解除名称={"生命之泉" ,"普渡众生" ,"火甲术","颠倒五行","乾坤妙法","天地同寿","四面埋伏","灵动九天","神龙摆尾",
								"佛法无边","幽冥鬼眼","一苇渡江","金刚护体","金刚护法","不动如山","韦陀护法","明光宝烛","镇魂诀","金身舍利","蜜润","红袖添香",
								"达摩护体" ,"定心术","潜力激发","杀气诀","魔王回首","牛劲" ,"安神诀" ,"极度疯狂" ,"分身术","百毒不侵" ,"楚楚可怜","盘丝阵" ,"移魂化骨",
								"天神护法" ,"天神护体","逆鳞" ,"炎护" ,"碎星诀","乘风破浪","凝神术","匠心·削铁","匠心·固甲","无所遁形","铜头铁臂","气慑天军"}
					elseif	self.战斗流程[1].参数=="冰清诀" or self.战斗流程[1].参数=="水清诀" then
							解除名称={"离魂符","失心符","追魂符","催眠符","定身符","失魂符","失忆符","日月乾坤","莲步轻舞","如花解语","似玉生香","娉婷嬝娜","一笑倾城","镇妖","错乱","百万神兵","紧箍咒","尸腐毒","象形","含情脉脉","天罗地网","瘴气","夺魄令","煞气诀","雾杀","毒","落魄符"}
							if self.战斗流程[1].参数=="冰清诀" then
								self.参战数据[self.战斗流程[1].挨打方].单位:流血逻辑(self.战斗流程[1].法术伤害,self.战斗流程[1].伤害类型,"及时")
							end
					elseif self.战斗流程[1].参数=="解毒" or self.战斗流程[1].参数=="清心" or self.战斗流程[1].参数=="驱尸" then
							解除名称={"毒","尸腐毒"}
					elseif self.战斗流程[1].参数=="复苏" then
							解除名称={"百万神兵","镇妖"}
							self.参战数据[self.战斗流程[1].挨打方].单位:增加状态(self.战斗流程[1].参数,4,self.战斗流程[1].参数)
					end
						for n=1,#解除名称 do
								self.参战数据[self.战斗流程[1].挨打方].单位:删除状态(解除名称[n])
						end
				elseif self.战斗流程[1].参数=="我佛慈悲" or self.战斗流程[1].参数=="还阳术" or self.战斗流程[1].参数=="还魂咒" or self.战斗流程[1].参数=="莲花心音" or self.战斗流程[1].参数=="杨柳甘露" or self.战斗流程[1].参数=="由己渡人" then
					self.参战数据[self.战斗流程[1].挨打方].单位.行为 = "待战"
					self.参战数据[self.战斗流程[1].挨打方].单位.真正死亡 = false
					self.参战数据[self.战斗流程[1].挨打方].单位.死亡方式 = 0
						self.参战数据[self.战斗流程[1].挨打方].单位:流血逻辑(self.战斗流程[1].法术伤害,self.战斗流程[1].伤害类型,"及时")
					if self.战斗流程[1].自身伤害~=nil then
						self.参战数据[self.战斗流程[1].攻击方].单位:流血逻辑(self.战斗流程[1].自身伤害,"掉血","及时")
					end
					if self.战斗流程[1].参数=="由己渡人" then
					self.参战数据[self.战斗流程[1].挨打方].单位:增加状态(self.战斗流程[1].参数,4,self.战斗流程[1].参数)
					end
				else
					if self.战斗流程[1].自身伤害~=nil then
						self.参战数据[self.战斗流程[1].攻击方].单位:流血逻辑(self.战斗流程[1].自身伤害,"掉血","及时")
					end
					if self.战斗流程[1].伤害 then
						self.参战数据[self.战斗流程[1].挨打方].单位:流血逻辑(self.战斗流程[1].伤害,"掉血","及时")
						if self.战斗流程[1].死亡 and self.战斗流程[1].死亡~=0 then
										self.参战数据[self.战斗流程[1].挨打方].单位.行为 = "死亡"
									self.参战数据[self.战斗流程[1].挨打方].单位.挨打状态 = "正常"
									self.参战数据[self.战斗流程[1].挨打方].单位.死亡方式 =self.战斗流程[1].死亡
						end
					end
					self.参战数据[self.战斗流程[1].挨打方].单位:增加状态(self.战斗流程[1].参数,4,self.战斗流程[1].参数)

				end
			end
			self.执行流程=101

		end
	elseif self.执行流程==6 and self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 then
		local v=0

			for i=1,#self.战斗流程[1].挨打方 do

			if self.参战数据[self.战斗流程[1].挨打方[i].编号].单位.特效==nil then
				if self.战斗流程[1].参数=="地涌金莲" or self.战斗流程[1].参数=="峰回路转" or self.战斗流程[1].参数=="推气过宫" or self.战斗流程[1].参数=="匠心·蓄锐" or self.战斗流程[1].参数=="四海升平" or self.战斗流程[1].参数=="九幽" then
					self.参战数据[self.战斗流程[1].挨打方[i].编号].单位:流血逻辑(self.战斗流程[1].挨打方[i].伤害,self.战斗流程[1].挨打方[i].类型,"及时")
				elseif	self.战斗流程[1].参数=="玉清诀" or self.战斗流程[1].参数=="晶清诀" then
					local 解除名称={}
					解除名称={"离魂符","失心符","追魂符","催眠符","定身符","失魂符","失忆符","日月乾坤","莲步轻舞","如花解语","似玉生香","娉婷嬝娜","一笑倾城","镇妖","错乱","百万神兵","紧箍咒","尸腐毒","象形","含情脉脉","天罗地网","瘴气","夺魄令","煞气诀","雾杀","毒","落魄符"}
					if self.战斗流程[1].参数=="晶清诀" then
						self.参战数据[self.战斗流程[1].挨打方[i].编号].单位:流血逻辑(self.战斗流程[1].挨打方[i].伤害,self.战斗流程[1].挨打方[i].类型,"及时")
					end
					for n=1,#解除名称 do
							self.参战数据[self.战斗流程[1].挨打方[i].编号].单位:删除状态(解除名称[n])
					end
				elseif self.战斗流程[1].参数=="金刚护法" then
					self.参战数据[self.战斗流程[1].挨打方[i].编号].单位:增加状态(self.战斗流程[1].参数,4)
				elseif self.战斗流程[1].参数=="一笑倾城" then
					if self.战斗流程[1].挨打方[i].封印结果 then

					self.参战数据[self.战斗流程[1].挨打方[i].编号].单位:增加状态(self.战斗流程[1].参数,4,self.战斗流程[1].参数)
					end
				elseif self.战斗流程[1].参数=="飞花摘叶" or self.战斗流程[1].参数=="怨怖之泣"or self.战斗流程[1].参数=="圣灵之甲"or self.战斗流程[1].参数=="魔兽之印" then


				elseif self.战斗流程[1].参数=="普渡众生" or self.战斗流程[1].参数=="生命之泉" then
					self.参战数据[self.战斗流程[1].挨打方[i].编号].单位:流血逻辑(self.战斗流程[1].挨打方[i].伤害,self.战斗流程[1].挨打方[i].类型,"及时")
					self.参战数据[self.战斗流程[1].挨打方[i].编号].单位:增加状态(self.战斗流程[1].参数,4,self.战斗流程[1].参数)
				elseif self.战斗流程[1].参数=="慈航普渡" then
						self.参战数据[self.战斗流程[1].挨打方[i].编号].单位.行为 = "待战"
						self.参战数据[self.战斗流程[1].挨打方[i].编号].单位.真正死亡 = false
						self.参战数据[self.战斗流程[1].挨打方[i].编号].单位.死亡方式 = 0
						self.参战数据[self.战斗流程[1].挨打方[i].编号].单位:流血逻辑(self.战斗流程[1].挨打方[i].伤害,"加血","及时")
				else

						self.参战数据[self.战斗流程[1].挨打方[i].编号].单位:增加状态(self.战斗流程[1].参数,4,self.战斗流程[1].参数)
				end
 				v=v+1

			end
			end

			if v== #self.战斗流程[1].挨打方 then
				self.执行流程=101
			end
				if self.战斗流程[#self.战斗流程].消耗伤害 then
				self.参战数据[self.战斗流程[1].攻击方].单位:流血逻辑(self.战斗流程[#self.战斗流程].消耗伤害)
			end
	elseif self.执行流程==100 then --持续掉血
		self.参战数据[self.战斗流程[1].挨打方].单位:流血逻辑(self.战斗流程[1].伤害,self.战斗流程[1].伤害类型,"及时")
		if self.战斗流程[1].死亡~=0 then
											self.参战数据[self.战斗流程[1].挨打方].单位.行为 = "死亡"
										self.参战数据[self.战斗流程[1].挨打方].单位.挨打状态 = "正常"
										self.参战数据[self.战斗流程[1].挨打方].单位.死亡方式 =self.战斗流程[1].死亡
		end

		self.执行流程=101
	elseif self.执行流程==101 then
			if type(self.战斗流程[1].挨打方) =="table" then
				local v=0
				for i=1,#self.战斗流程[1].挨打方 do
					local 临时编号 =self.战斗流程[1].挨打方[i].编号
					if self.参战数据[临时编号].单位.流血显示 ==false then
						self.参战数据[临时编号].单位.回合行动=0
						v=v+1
					elseif self.参战数据[临时编号].单位.真正死亡 then
						v=v+1
					end
				end
				if v== #self.战斗流程[1].挨打方 then
					self.参战数据[self.战斗流程[1].攻击方].单位.回合行动=0
					self.执行流程=3
				end
			else
					if self.参战数据[self.战斗流程[1].挨打方].单位.流血显示 ==false and (self.参战数据[self.战斗流程[1].挨打方].单位.行为=="待战" or self.参战数据[self.战斗流程[1].挨打方].单位.真正死亡) then
						self.参战数据[self.战斗流程[1].挨打方].单位.回合行动=0
					if self.战斗流程[1].攻击方 then
					self.参战数据[self.战斗流程[1].攻击方].单位.回合行动=0
					end
				self.执行流程=3
				end

			end
	elseif self.执行流程==105 then --道具
		self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 =false
		self.参战数据[self.战斗流程[1].攻击方].单位.施法前播放 = false
		self.参战数据[self.战斗流程[1].攻击方].单位.行为 = "使用道具"
		self.执行流程=106
		elseif self.执行流程==106 and self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 then
		if tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 ==nil then
			self.执行流程=101
		end
	elseif self.执行流程==108 then
		self.参战数据[self.战斗流程[1].攻击方].单位.施法前播放 = false
		self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 =false
		self.参战数据[self.战斗流程[1].攻击方].单位.行为 = "增益法术"
		self.执行流程= 109
	elseif self.执行流程==109 and self.参战数据[self.战斗流程[1].攻击方].单位.行动完毕 then
			if tp.场景.战斗.参战数据[tp.场景.战斗.战斗流程[1].挨打方].单位.特效 ==nil then
				self.执行流程=101
			end
	end
 end
function 场景类_战斗:快捷技能(名称)

 if self.回合进程 == "命令回合" and self.参战数据[self.操作单位[self.当前单位].编号].战斗类型=="角色" and self.命令版面 then
 if SkillData[名称].类型 ==4 or SkillData[名称].类型 ==10 then
	self.命令类型="技能"
	self.命令对象=2
	self.命令参数=名称
	tp.鼠标.置鼠标("道具")
	self.命令版面=false
	self.法术开关=false
 elseif SkillData[名称].类型 ==3 or SkillData[名称].类型 ==6 then
	self.命令类型="技能"
	self.命令对象=1
	self.命令参数=名称
	tp.鼠标.置鼠标("道具")
	self.命令版面=false
	self.法术开关=false
 elseif SkillData[名称].类型 ==2 or SkillData[名称].类型 ==5 then
	self.命令类型="技能"
	self.命令对象=1
	self.命令参数=名称
	tp.鼠标.置鼠标("道具")
	self.命令版面=false
	self.法术开关=false
	self.操作单位[self.当前单位].类型=self.命令类型
	self.操作单位[self.当前单位].目标=self.操作单位[self.当前单位].编号
	self.操作单位[self.当前单位].参数=self.命令参数
	if self.当前单位==#self.操作单位 then
		self:指令完毕()
		self.回合进程="等待回合"
		客户端:发送数据(1,235,24,self:转文本(self.操作单位))
	else
		self.当前单位=self.当前单位+1
		self.命令版面=true
		self.捕捉开关=false
		self.法术开关=false
		self.命令类型="攻击"
		self.命令对象=2
		self:指令完毕()
	end
 end
 end
end
return 场景类_战斗