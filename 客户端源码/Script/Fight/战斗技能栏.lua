--======================================================================--
-- @作者: QQ381990860
-- @创建时间:	2019-12-03 02:17:19
-- @Last Modified time: 2020-10-13 18:29:18
--======================================================================--
local 场景类_战斗技能栏 = class()
local tp
local ARGB = ARGB

function 场景类_战斗技能栏:初始化(根)
	self.ID = 103
	self.x = 300+(全局游戏宽度-800)/2
	self.y = 115
	self.xx = 0
	self.yy = 0
	self.注释 = "战斗"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 资源 = 根.资源
	self.资源组 = {
		[1] = 资源:载入('JM.dll',"网易WDF动画",0x69823EE5),
		[2] = 资源:载入('JM.dll',"网易WDF动画",0x32F119A5),
		[3] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xF11233BB),0,0,4),
	}
	self.技能 = {}
	local 格子 = 根._技能格子
	for i=1,20 do
		self.技能[i] = 格子(0,0,i,"战斗技能栏")
	end
	self.资源组[3]:绑定窗口_(103)
	self.背景 = 1
	self.窗口时间 = 0
	self.上回合 = nil
	self.偏移 = {0,0}
	tp = 根
end

function 场景类_战斗技能栏:打开(数据)

	if self.可视 then
		self.背景 = 1
		self.上回合 = nil
		self.可视 = false
	else
		self.x = 300+(全局游戏宽度-800)/2
		self.背景 = 1
		self.数据 = 数据
		local i = 0

		for w=1,20 do
			self.技能[w]:置技能(nil)
			
			self.技能[w]:置技能(self.数据.主动技能[w],self.数据.战斗类型)


		end
		if #self.数据.主动技能 > 10 then
			self.背景 = 2
			self.偏移[2] = -5
		else
			self.偏移[2] = 0
		end
		if self.上回合 == nil and self.数据.默认法术 ==nil then
			self.上回合 = "未设置默认法术"
		else
			self.上回合 = self.数据.默认法术
		end

		tp.运行时间 = tp.运行时间 + 1
			self.窗口时间 = tp.运行时间
			self.可视 = true
	end
end

function 场景类_战斗技能栏:更新()
end

function 场景类_战斗技能栏:显示(dt,x,y)
	if not self.可视 then
		return
	end
	self.焦点 = false
	self.资源组[self.背景]:显示(self.x,self.y)
	if self.鼠标 then
		self.资源组[3]:更新(x,y)
		if self.资源组[3]:事件判断() then
			self:打开()
			return false
		end
	end
	self.资源组[3]:显示(self.x+165,self.y+6,true)
	local xx = 0
	local yy = 0
	for w=1,20 do
		self.技能[w]:置坐标(self.x+30+xx*88+self.偏移[1],self.y+32+yy*42+self.偏移[2])
		self.技能[w]:显示(x,y,self.鼠标)
		if self.技能[w].技能 ~= nil and self.技能[w].焦点 then
			tp.提示:技能(x,y,self.技能[w].技能,self.技能[w].剩余冷却回合)
			if self.技能[w].事件 and self.鼠标 and self.技能[w].剩余冷却回合 == nil then
				local SkillName=self.技能[w].技能.名称 
				if SkillData[SkillName].类型 ==4 or SkillData[SkillName].类型 ==10 then	
					tp.场景.战斗.命令类型="技能"
					tp.场景.战斗.命令对象=2
					tp.场景.战斗.命令参数=self.技能[w].技能.名称
					tp.鼠标.置鼠标("道具")
					tp.场景.战斗.命令版面=false
				elseif SkillData[SkillName].类型 ==3 or SkillData[SkillName].类型 ==6 then
					tp.场景.战斗.命令类型="技能"
					tp.场景.战斗.命令对象=1
					tp.场景.战斗.命令参数=self.技能[w].技能.名称
					tp.鼠标.置鼠标("道具")
					tp.场景.战斗.命令版面=false
				elseif SkillData[SkillName].类型 ==2 or SkillData[SkillName].类型 ==5 then
					tp.场景.战斗.命令类型="技能"
					tp.场景.战斗.命令对象=1
					tp.场景.战斗.命令参数=self.技能[w].技能.名称
					tp.场景.战斗.命令版面=false
					tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].类型=tp.场景.战斗.命令类型
					tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].目标=tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].编号
					tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].参数=tp.场景.战斗.命令参数
					if tp.场景.战斗.当前单位==#tp.场景.战斗.操作单位 then
						self.回合进程="等待回合"
						客户端:发送数据(1,235,24,tp.场景.战斗:转文本(tp.场景.战斗.操作单位))
					else
						tp.场景.战斗.当前单位=tp.场景.战斗.当前单位+1
						tp.场景.战斗.命令版面=true
						tp.场景.战斗.命令类型="攻击"
						tp.场景.战斗.命令对象=2
					end
				end
				for n=1,#tp.场景.战斗.窗口_ do
					if tp.场景.战斗.窗口_[n].可视 then
						tp.场景.战斗.窗口_[n]:打开()
					end
				end
				break
			elseif self.技能[w].右键 and self.鼠标	then
				if tp.场景.战斗.参战数据[tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].编号].单位.数据.默认法术~=self.技能[w].技能.名称 then
				tp.场景.战斗.参战数据[tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].编号].单位.数据.默认法术=self.技能[w].技能.名称
				客户端:发送数据(tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].编号,238,24,self.技能[w].技能.名称)
				self.上回合 = self.技能[w].技能.名称
				else
				tp.场景.战斗.参战数据[tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].编号].单位.数据.默认法术=nil

				客户端:发送数据(tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].编号,238,24,"0")
				self.上回合 = nil
				end
			end
			self.焦点 = true
		end
		xx = xx + 1
		if xx > 1 then
			xx = 0
			yy = yy + 1
		end
	end
	if self.上回合 ~= nil and self.背景 == 1 then
		tp.字体表.普通字体:置颜色(-256):显示(self.x+60,self.y+269,self.上回合)
	end
	if tp.按钮焦点 then
		self.焦点 = true
	end
	if 引擎.鼠标弹起(1) and not tp.禁止关闭 and self.鼠标 then
		self:打开()
	end
end

function 场景类_战斗技能栏:检查点(x,y)
	if self.可视 and self.资源组[self.背景]:是否选中(x,y)	then
		return true
	end
end

function 场景类_战斗技能栏:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
			self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.场景.战斗.移动窗口 = true
	end
	if self.可视 and self.鼠标 and	not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_战斗技能栏:开始移动(x,y)
	if self.可视 and self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_战斗技能栏