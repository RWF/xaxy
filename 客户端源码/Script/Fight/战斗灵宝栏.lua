-- @Author: 豆包
-- @Date:   2021-10-27 16:23:52
-- @Last Modified by:   豆包
-- @Last Modified time: 2021-10-28 20:46:59
local 场景类_战斗灵宝栏 = class()
local require = require
local ff = require("script/System/丰富文本")
local ffwb={}
 ffwb[1]=ff(200,220)
  ffwb[2]=ff(200,220)
local tp
ItemData =ReadExcel("物品数据",授权)
function 场景类_战斗灵宝栏:初始化(根)
    self.ID = 104
    self.x = 220+(全局游戏宽度-800)/2
    self.y = 175
    self.xx = 0
    self.yy = 0
    self.注释 = "战斗"
    self.可视 = false
    self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    local 按钮 = require("script/System/按钮")
    self.资源组 = {
        [1] = 根.资源:载入('doub.dll',"网易WDF动画",0x00000204),
        [2] = 按钮(根.资源:载入('MAX.7z',"网易WDF动画",2),0,0,3),
        [3] =  按钮(根.资源:载入('MAX.7z',"网易WDF动画",1),0,0,4,nil,nil,"      使 用 "),
        [4] =  按钮(根.资源:载入('MAX.7z',"网易WDF动画",1),0,0,4,nil,nil,"      使 用 "),
    }
    zts1 = 根.字体表.普通字体
    self.物品 = {}
    local 格子 = require("script/System/物品_格子")
    for h=1,2 do

            self.物品[#self.物品 + 1] = 格子(h*236-142+self.x+4,70+self.y,#self.物品 + 1,"战斗道具_物品")

    end
    self.剩余回合=8
    self.窗口时间 = 0
    tp = 根
end

function 场景类_战斗灵宝栏:打开(数据)

    if self.可视 then
        self.可视 = false
    else
         self.灵宝={}



         for i=1,#数据.灵宝  do
              if  数据.灵宝[i].等级==nil then
                 数据.灵宝[i].等级=1
                end
                self.灵宝[i]={}
                self.灵宝[i].名称=数据.灵宝[i].名称
                self.灵宝[i].等级=数据.灵宝[i].等级
                self.灵宝[i].说明=ItemData[数据.灵宝[i].名称].说明
                self.灵宝[i].效果=ItemData[数据.灵宝[i].名称].效果
                self.灵宝[i].属性=ItemData[数据.灵宝[i].名称].属性
                self.灵宝[i].图标=tp.资源:载入(ItemData[数据.灵宝[i].名称].文件,"网易WDF动画",ItemData[数据.灵宝[i].名称].小图标)
       ffwb[i]:清空()
       -- ffwb[i]:添加文本(self.灵宝[i].说明)
       ffwb[i]:添加文本("名称："..self.灵宝[i].名称)
       ffwb[i]:添加文本("效果："..self.灵宝[i].效果)
        ffwb[i]:添加文本("等级："..self.灵宝[i].等级)


    end
    self.回合数=数据.回合数
    self.灵元=数据.灵元
    self.剩余回合=5


  if self.回合数<5 then----法宝
self.mod =self.剩余回合-self.回合数
else
    self.mod = math.fmod( self.回合数, self.剩余回合 )
    self.mod=self.剩余回合-self.mod
  end


     -- 取余数


        self.x = 220+(全局游戏宽度-800)/2
        -- for n=1,20 do
        --     self.物品[n]:置物品(数据[n])
        -- end
        self.可视 = true
        tp.运行时间 = tp.运行时间 + 1
        self.窗口时间 = tp.运行时间
    end
end

function 场景类_战斗灵宝栏:更新(dt,x,y)
end

function 场景类_战斗灵宝栏:显示(dt,x,y)
    if not self.可视 then
        return false
    end
    self.焦点 = false
      self.资源组[3]:更新(x,y,self.灵元>0 and self.灵宝[1]~=nil)
        self.资源组[4]:更新(x,y,self.灵元>0 and self.灵宝[2]~=nil)
    if self.鼠标 then
        self.资源组[2]:更新(x,y)

        if self.资源组[2]:事件判断() then
            self:打开()
        elseif self.资源组[3]:事件判断() then
            if self.灵宝[1].属性.类型=="攻击"  then
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                          self.可视=false
            elseif self.灵宝[1].名称 =="赤炎战笛" then
                     tp.场景.战斗.命令类型="召唤兽灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
           elseif self.灵宝[1].名称 =="惊兽云尺" then
                     tp.场景.战斗.命令类型="召唤兽灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
            elseif self.灵宝[1].名称 =="战神宝典" then
                     tp.场景.战斗.命令类型="角色灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                  elseif self.灵宝[1].名称 =="静心禅珠" then ----------------
                     tp.场景.战斗.命令类型="角色灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                   elseif self.灵宝[1].名称 =="宁心道符" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                     elseif self.灵宝[1].名称 =="相思寒针" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                   elseif self.灵宝[1].名称 =="风舞心经" then -------------
                     tp.场景.战斗.命令类型="角色灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                      elseif self.灵宝[1].名称 =="神匠火炮" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                     elseif self.灵宝[1].名称 =="锢魂命谱" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                        elseif self.灵宝[1].名称 =="青狮獠牙" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                          elseif self.灵宝[1].名称 =="冥火炼炉" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                                     elseif self.灵宝[1].名称 =="缚仙蛛丝" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                      elseif self.灵宝[1].名称 =="煞魂冥灯" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                        elseif self.灵宝[1].名称 =="三彩琉璃" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                              elseif self.灵宝[1].名称 =="九霄龙锥" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                              elseif self.灵宝[1].名称 =="化怨清莲" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                              elseif self.灵宝[1].名称 =="真阳令旗" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                              elseif self.灵宝[1].名称 =="天雷音鼓" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                              elseif self.灵宝[1].名称 =="寒霜盾戟" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                     elseif self.灵宝[1].名称 =="伏魔灵圈" then -------------
                     tp.场景.战斗.命令类型="角色灵宝1"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
               elseif self.灵宝[1].名称 =="乾坤火卷" or self.灵宝[1].名称 =="乾坤木卷" or self.灵宝[1].名称 =="乾坤金卷" or self.灵宝[1].名称 =="乾坤土卷" or self.灵宝[1].名称 =="乾坤水卷" then
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
          elseif self.灵宝[1].名称 =="定神仙琴" then
                    tp.场景.战斗.命令类型="角色灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
             elseif self.灵宝[1].属性.类型 =="防御" or self.灵宝[1].属性.类型 =="辅助" or  self.灵宝[1].属性.类型 =="恢复"or  self.灵宝[1].属性.类型 =="解封" then
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=1
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                          self.可视=false

                        end




       elseif self.资源组[4]:事件判断() then
            if self.灵宝[2].属性.类型=="攻击"  then
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                          self.可视=false
            elseif self.灵宝[2].名称 =="赤炎战笛" then
                     tp.场景.战斗.命令类型="召唤兽灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
           elseif self.灵宝[2].名称 =="惊兽云尺" then
                     tp.场景.战斗.命令类型="召唤兽灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
            elseif self.灵宝[2].名称 =="战神宝典" then
                     tp.场景.战斗.命令类型="角色灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                  elseif self.灵宝[2].名称 =="静心禅珠" then ----------------
                     tp.场景.战斗.命令类型="角色灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                   elseif self.灵宝[2].名称 =="宁心道符" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                     elseif self.灵宝[2].名称 =="相思寒针" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                   elseif self.灵宝[2].名称 =="风舞心经" then -------------
                     tp.场景.战斗.命令类型="角色灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                      elseif self.灵宝[2].名称 =="神匠火炮" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                     elseif self.灵宝[2].名称 =="锢魂命谱" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                        elseif self.灵宝[2].名称 =="青狮獠牙" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                          elseif self.灵宝[2].名称 =="冥火炼炉" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                                     elseif self.灵宝[2].名称 =="缚仙蛛丝" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                      elseif self.灵宝[2].名称 =="煞魂冥灯" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                        elseif self.灵宝[2].名称 =="三彩琉璃" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                              elseif self.灵宝[2].名称 =="九霄龙锥" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                              elseif self.灵宝[2].名称 =="化怨清莲" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                              elseif self.灵宝[2].名称 =="真阳令旗" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                              elseif self.灵宝[2].名称 =="天雷音鼓" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                              elseif self.灵宝[2].名称 =="寒霜盾戟" then -------------
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
                     elseif self.灵宝[2].名称 =="伏魔灵圈" then -------------
                     tp.场景.战斗.命令类型="角色灵宝1"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
               elseif self.灵宝[2].名称 =="乾坤火卷" or self.灵宝[2].名称 =="乾坤木卷" or self.灵宝[2].名称 =="乾坤金卷" or self.灵宝[2].名称 =="乾坤土卷" or self.灵宝[2].名称 =="乾坤水卷" then
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=2
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
          elseif self.灵宝[2].名称 =="定神仙琴" then
                    tp.场景.战斗.命令类型="角色灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                     self.可视=false
             elseif self.灵宝[2].属性.类型 =="防御" or self.灵宝[2].属性.类型 =="辅助" or  self.灵宝[2].属性.类型 =="恢复"or  self.灵宝[2].属性.类型 =="解封" then
                     tp.场景.战斗.命令类型="灵宝"
                     tp.场景.战斗.命令对象=1
                     tp.场景.战斗.命令参数=2
                     tp.鼠标.置鼠标("道具")
                     tp.场景.战斗.命令版面=false
                          self.可视=false

                        end


         end



    else
        self.资源组[2].焦点 = 0

    end
    self.资源组[1]:显示(self.x,self.y)
    self.资源组[2]:显示(self.x + 462,self.y + 1,true)

    self.资源组[3]:显示(self.x + 50,self.y + 260,true)
    self.资源组[4]:显示(self.x + 300,self.y + 260,true)
    ffwb[1]:显示(self.x + 30,self.y + 140)
    ffwb[2]:显示(self.x + 280,self.y + 140)
    zts1:显示(self.x+120,30+self.y,"当前剩余灵元："..self.灵元.." ("..self.mod.."回合后增加一点)")
    local n = 0
    for h=1,#self.灵宝 do

            n = n + 1
            self.灵宝[h].图标:显示(h*236-142+self.x+4,70+self.y)

            if self.灵宝[h].物品 ~= nil and self.物品[n].焦点 and self.鼠标 then
                tp.提示:道具行囊(x,y,self.灵宝[h].物品)


            end

    end
    if tp.按钮焦点 then
        self.焦点 = true
    end
    if 引擎.鼠标弹起(1)  and not tp.禁止关闭 and self.鼠标 then
        self:打开()
    end
end

function 场景类_战斗灵宝栏:检查点(x,y)
    if self.可视 and self.资源组[1]:是否选中(x,y)  then
        return true
    end
    return false
end

function 场景类_战斗灵宝栏:初始移动(x,y)
    tp.运行时间 = tp.运行时间 + 1
    if not 引擎.消息栏焦点 then
        self.窗口时间 = tp.运行时间
    end
    if not self.焦点 then
        tp.场景.战斗.移动窗口 = true
    end
    if self.可视 and self.鼠标 and  not self.焦点 then
        self.xx = x - self.x
        self.yy = y - self.y
    end
end

function 场景类_战斗灵宝栏:开始移动(x,y)
    if self.可视 and self.鼠标 then
        self.x = x - self.xx
        self.y = y - self.yy
    end
end

return 场景类_战斗灵宝栏