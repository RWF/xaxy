-- @Author: 作者QQ381990860
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-12 01:31:44
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-10-06 23:49:52
--======================================================================--
local 场景类_战斗特技栏 = class()
local tp
function 场景类_战斗特技栏:初始化(根)
	self.ID = 105
	self.x = 300+(全局游戏宽度-800)/2
	self.y = 115
	self.xx = 0
	self.yy = 0
	self.注释 = "战斗"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	tp =根
	self.可移动 = true
	local 按钮 = require("script/System/按钮")
	self.资源组 = {
		[1] = 根.资源:载入('JM.dll',"网易WDF动画",0x2FD95E30),
		[2] = 按钮(根.资源:载入('JM.dll',"网易WDF动画",0xF11233BB),0,0,4),
	}
	self.技能 = {}
	self.坐标 = {{34.5,95.5,34.5,95.5,34.5,95.5},{32.4,32.4,92.4,92.4,152.5,152.5}}
	local 格子 = require("script/System/技能_格子")
	for i=1,6 do
		self.技能[i] = 格子(self.x + self.坐标[1][i],self.y + self.坐标[2][i],i,"战斗特技栏")
	end
	self.窗口时间 = 0
end

function 场景类_战斗特技栏:打开(数据)

	if self.可视 then
		self.可视 = false
	else
			self.x = 300+(全局游戏宽度-800)/2
		for i=1,6 do
			self.技能[i]:置技能(数据[i],true)
		end
		引擎.场景.运行时间 = 引擎.场景.运行时间 + 1
	  	self.窗口时间 = 引擎.场景.运行时间
	  	self.可视 = true
	end
end

function 场景类_战斗特技栏:更新(dt,x,y)

end

function 场景类_战斗特技栏:显示(dt,x,y)
	if not self.可视 then
		return -- 继续
	end
	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x+150,self.y+3)
	self.资源组[2]:更新(x,y)
		if self.资源组[2]:事件判断() then
			self:打开()
			return false
		end

	for i=1,6 do
		self.技能[i]:置坐标(self.x + self.坐标[1][i],self.y + self.坐标[2][i])
		self.技能[i]:显示(x,y,self.鼠标)
		if self.技能[i].技能 ~= nil and self.技能[i].焦点 then
			引擎.场景.提示:技能(x,y,self.技能[i].技能)
			if self.技能[i].事件 and self.鼠标 then
				 if self.技能[i].技能.种类 ==4  then
			         tp.场景.战斗.命令类型="特技"
			         tp.场景.战斗.命令对象=2
			         tp.场景.战斗.命令参数=self.技能[i].技能.名称
			         tp.鼠标.置鼠标("道具")
			         tp.场景.战斗.命令版面=false
			        elseif self.技能[i].技能.种类 ==3 or self.技能[i].技能.种类 ==6 then
			         tp.场景.战斗.命令类型="特技"
			         tp.场景.战斗.命令对象=1
			         tp.场景.战斗.命令参数=self.技能[i].技能.名称
			         tp.鼠标.置鼠标("道具")
			         tp.场景.战斗.命令版面=false
			         elseif self.技能[i].技能.种类 ==2 or self.技能[i].技能.种类 ==5 then
			         tp.场景.战斗.命令类型="特技"
			         tp.场景.战斗.命令对象=1
			         tp.场景.战斗.命令参数=self.技能[i].技能.名称
			         tp.场景.战斗.命令版面=false
			         tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].类型=tp.场景.战斗.命令类型
			         tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].目标=tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].编号
			         tp.场景.战斗.操作单位[tp.场景.战斗.当前单位].参数=tp.场景.战斗.命令参数
						if tp.场景.战斗.当前单位==#tp.场景.战斗.操作单位 then
						self.回合进程="等待回合"
						 客户端:发送数据(1,235,24,tp.场景.战斗:转文本(tp.场景.战斗.操作单位))
						else
						tp.场景.战斗.当前单位=tp.场景.战斗.当前单位+1
						tp.场景.战斗.命令版面=true
						tp.场景.战斗.命令类型="攻击"
						tp.场景.战斗.命令对象=2
						end
			        end
			         for n=1,#tp.场景.战斗.窗口_ do
						if tp.场景.战斗.窗口_[n].可视 then
							tp.场景.战斗.窗口_[n]:打开()
						end
					end

			end
			self.焦点 = true
		end
	end
	if 引擎.鼠标弹起(1) and not 引擎.场景.禁止关闭 and self.鼠标 then
		self:打开()
	end
end

function 场景类_战斗特技栏:检查点(x,y)
	local n = false
	if self.可视 and self.资源组[1]:是否选中(x,y)  then
		n  = true
	end
	return n
end

function 场景类_战斗特技栏:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.场景.战斗.移动窗口 = true
	end
	if self.可视 and self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end
function 场景类_战斗特技栏:开始移动(x,y)
	if self.可视 and self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_战斗特技栏













