-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-09-12 01:15:57
-- 客户端通讯及回调
local 回调 = require("HPClient类")()
local tp
function 回调:初始化()

	self.连接账号 = ""
	self.连接密码 = ""
	self.连接结果 = false
end

function 回调:置根(根)
        tp =根
end
function 回调:更新(dt)
end
function 回调:重新连接()
  停止连接=true
   if self:连接(全局IP, 全局端口) then
     连接状态=true
     停止连接=false
     重连状态=true
     -- self:发送(96, {账号=游戏账号,密码=游戏密码})
     self:发送数据(窗口标题.id + 0, 1, 96, 窗口标题.id)
    else
      连接状态=false
      停止连接=false
      end
end
function 回调:连接处理()
    self.连接结果 = self:连接(全局IP, 全局端口)
	self.临时时间 = os.time()
	self.发送时间 = self.临时时间 .. 取随机数(10, 99)
	-- self.发送信息 = {空了="~*?77????12377???*@@",皮皮="???",版本=版本号}--
    self.发送信息 = {空了="~*?77????77???*@@",皮皮="???",版本=版本号}
	self:发送(17038454,cjson.encode(self.发送信息))
	 
	 if not self.连接结果 then
       __gge.messagebox("服务器连接断开，请检查网络或服务器是否在维护中.....","网络中断")
        引擎.关闭()
	 end

end
function 回调:发送数据(a,b,c,f,g)
    print(string.format("发送数据=>入参1:%s,入参2:%s,指令序号:%s,全局时间:%s,玩家id:%s,文本:%s,g:%s",a,b,c,全局时间,窗口标题.id,f,g))
 if a and b and c  then
      数据包流=数据包流+1
      self:发送(c,cjson.encode({a=a,b=b,c=c,f=f,d=全局时间,g=g,id=窗口标题.id}))
 else 
    print(a,b,c,"同时为空值")
 end


end


function 回调:取状态(连接)
	return self:取状态()
end

function 回调:连接断开(enOperation,iErrorCode)
     tp.提示:写入("#Y/你与服务器连接断开")
end
function 回调:断开连接(连接)
	禁止重连 = true
	self:断开()
end

local repeatContent = nil;
function 回调:数据到达(序号, 内容)
	if 序号 ~= 10 and 内容~=nil then
		print("序号:"..序号,"  "..内容)
	end
    if 序号 ~= 7 then
        repeatContent = nil
    end

    if tp.进程 <1 and not (序号==8 or 序号==16 or 序号==99998) then
        return  
    end
    if 内容 and string.find(内容, "do local ret=")~=nil then
        内容=table.loadstring(内容)
    end

	if 序号 == 0 then
		if 重连状态 == false then
			self.处理信息 = table.loadstring(内容)
			连接参数 = string.sub(self.处理信息.c, 1, 1) + 0
			self:发送数据(认证号, 1, 1, self.连接账号 .. "@-@" .. self.连接密码 .. "@-@" .. 取硬盘特征字("1"))
		else
			self.处理信息 = table.loadstring(内容)
			连接参数 = string.sub(self.处理信息.c, 1, 1) + 0
			self:发送数据(窗口标题.id + 0, 1, 3, 窗口标题.id)
		end
	elseif 序号 == 2 or 序号 == 3 then
         __gge.messagebox(内容, "下线通知")
		引擎.关闭()
	elseif 序号 == 99995 then
		os.exit()
	elseif 序号 == 99996 then
		self:断开连接()
	elseif 序号 == 99998 then
        __gge.messagebox(内容, "注册通知")
        tp.进程= 5

        -- self:断开连接()
        -- 引擎.关闭()
	elseif 序号 == 4 then
		tp.进程=4
	elseif 序号 == 5 then
		tp:销毁()
		检测2 = 引擎.取游戏时间()
		窗口大小 = {x = 160,y = 120}
		tp.场景.人物:创建角色(内容)
		tp.窗口.人物框:刷新角色头像()
		客户端:发送数据(1, 1, 55, "kjl", 1)
        引擎.置标题(全局游戏标题 .."- " ..窗口标题.服务器.. "- (" .. 窗口标题.名称 .. "[" .. 窗口标题.id .. "])" ) --.. " - 版本v"..版本号
	elseif 序号 == 6 then
		tp.选择:添加角色信息(内容)
	elseif 序号 == 7 then
        if repeatContent ~= 内容 then
    		tp.提示:写入(内容)
            repeatContent = 内容
        end
    elseif 序号 == 8 then
         游戏公告 = 内容
	elseif 序号 == 10 and tp.进程 ==6 then
    		更改窗口标题(内容+0)
	elseif 序号 == 99997 then
		  tp.窗口.游戏公告:添加公告内容(内容,true)
	elseif 序号 == 11 then
		  tp.窗口.时辰.当前时辰=内容+0
	elseif 序号 == 12 then
		消息开关 = false
	elseif 序号 == 13 then
		消息开关 = true
	elseif 序号 == 14 then
		if tp.窗口.系统消息.可视 then
		 tp.窗口.系统消息.数据 = 内容
		else
		   tp.窗口.系统消息:打开(内容)
		end

	elseif 序号 == 15 then
		    tp.窗口.组合输入框:打开(内容[1],内容[2])
    elseif 序号 == 16 then
        全局时间=内容+0
    elseif 序号 == 17 then
        tp.窗口.时辰.当前昼夜=内容+0
	elseif 序号 == 20 then
		if tp.窗口.对话栏.可视 then
		    tp.窗口.对话栏.可视 =false
		end
		tp.窗口.对话栏:文本(内容[1],内容[2],内容[3],内容[4],内容[5],内容[6],内容[7])

	elseif 序号 == 21 or 序号 == 9 then
			tp.窗口.聊天框类:添加文本(内容)
	elseif 序号 == 22 then
		self.临时聊天 = 内容
		tp.窗口.聊天框类:添加文本("#r/以下为本场战斗信息：")
		for n = 1, #self.临时聊天, 1 do
			tp.窗口.聊天框类:添加文本("")
			tp.窗口.聊天框类:添加文本(self.临时聊天[n])
		end
	elseif 序号 == 23 then
		队伍消息 =true
	elseif 序号 == 24 then
		屏蔽变身 =内容

    elseif 序号 == 25 then
        tp.进程= 1
            写配置(程序目录..[[配置.ini]],"账号信息","账号",内容[1])
            写配置(程序目录..[[配置.ini]],"账号信息","密码",内容[2])
          local  账号记录=ReadFile("账号记录.txt")
        账号记录 =账号记录..内容[1].."*-*"..内容[2].."\n"
        WriteFile("账号记录.txt",账号记录)
        tp.提示:写入("#y/您的账号已经建立成功，账号密码已经保存")
    elseif 序号 == 26 then
          tp.窗口.仙玉广播:添加公告(内容) 


 elseif 序号==27 then
    tp.窗口.物品查看:打开(内容.数据)
 elseif 序号==28 then
    tp.窗口.召唤兽查看栏:打开(内容.数据)

     elseif 序号==29 then

        tp.窗口.首服奖励:打开(内容)
	elseif 序号==112 then
        tp.窗口.藏宝阁:打开(内容)
    elseif 序号 == 113 then
      tp.窗口.藏宝阁出售:打开(内容,"道具")
    elseif 序号 == 114 then
      tp.窗口.藏宝阁出售:打开(内容,"召唤兽")
    elseif 序号 == 115 then
      tp.窗口.藏宝阁银子出售:打开(内容)
    elseif 序号 == 116 then
      if tp.窗口.藏宝阁.可视 then
        tp.窗口.藏宝阁:刷新数据(内容)
      else
        发送数据(607)
      end
    -- elseif 序号==117 then
    --     tp.窗口.点歌台:打开(内容)
    -- elseif 序号 == 118 then
    --     tp.窗口.点歌播放:打开(内容.地址,内容.祝福)

    elseif 序号 == 1001 then
		tp.场景:转移(内容)
	elseif 序号 == 6014 then
		tp.窗口.华山论剑:打开()
	elseif 序号 == 6015 then
		tp.窗口.华山论剑.进程 = 2
	elseif 序号 == 6016 then
		tp.窗口.华山论剑.进程 = 1
	elseif 序号 == 6017 then
		tp.窗口.华山论剑排行:打开(内容)
	elseif 序号 == 1002 then
		tp.场景.人物:取目标(内容)
	elseif 序号 == 1003 then
		tp.窗口.人物框:更新气血数据(内容)
	elseif 序号 == 1004 then
		tp:添加玩家(内容)
	elseif 序号 == 1005 then
	   tp:取目标(内容)

	elseif 序号 == 1008 then
		for i=#tp.场景.场景人物,1,-1 do
             if tp.场景.场景人物[i].id == 内容+0 then
                    table.remove(tp.场景.场景人物,i)
                    break
             end
        end
		  --  for i, v in ipairs(tp.场景.场景人物) do
			 --    if tp.场景.场景人物[i].id == 内容+0 then
				--     table.remove(tp.场景.场景人物,i)
				-- 	break
				-- end
		  --  end
           tp.场景.玩家[内容+0]=nil
    elseif 序号==1009 then  -------------------------------玩家改称谓
	  if tp.场景.玩家[内容.id+0] then
	   	tp.场景.玩家[内容.id+0].称谓=内容.称谓
        tp.场景.玩家[内容.id+0].称谓动画 =nil
        tp.场景.玩家[内容.id+0].称谓颜色 = 0xFF80BFFF
        if LabelData[内容.称谓] then
            tp.场景.玩家[内容.id+0].称谓颜色 = LabelData[内容.称谓].颜色 and LabelData[内容.称谓].颜色 or 0xFF80BFFF
            if LabelData[内容.称谓].动画 then
                tp.场景.玩家[内容.id+0].称谓动画=tp.资源:载入(LabelData[内容.称谓].资源,"网易WDF动画",LabelData[内容.称谓].动画)
                tp.场景.玩家[内容.id+0].称谓动画偏移=LabelData[内容.称谓].偏移
             elseif 内容.effects then 
                  tp.场景.人物.称谓动画=tp.资源:载入(LabelData[内容.effects].资源,"网易WDF动画",LabelData[内容.effects].动画)
                    tp.场景.人物.称谓动画偏移=LabelData[内容.effects].偏移
            end
        end
       tp.场景.玩家[内容.id+0].称谓偏移 = 生成XY(math.floor(tp.字体表.人物字体:取宽度(内容.称谓) / 2),-15)

	  end
	elseif 序号 == 1010 then
		tp:Npc(内容)

    elseif 序号 == 1011 then
        tp.场景.玩家[内容.id+0]:加入动画(内容.名称)
    elseif 序号==1012 then
		if tp.场景.玩家[内容+0] then
			tp.场景.玩家[内容+0].战斗开关=true
		end
    elseif 序号==1013 then
		if tp.场景.玩家[内容+0] then
			tp.场景.玩家[内容+0].战斗开关=false
		end
	elseif 序号 == 1014 then
		self.临时消息 = 内容
		if not tp.战斗中 then
			 tp.喊话:写入(tp.场景.人物,self.临时消息.消息)
		elseif tp.战斗中 then
			tp.场景.战斗:添加发言内容(self.临时消息.id, self.临时消息.消息)
		end
	elseif 序号 == 1015 then
		self.临时消息 = 内容
		if not tp.战斗中 and  tp.场景.玩家[self.临时消息.id] then
			 tp.玩家喊话:写入(tp.场景.玩家[self.临时消息.id],self.临时消息.消息,nil)
		elseif tp.战斗中 then
			tp.场景.战斗:添加发言内容(self.临时消息.id, self.临时消息.消息)
		end
	elseif 序号 == 1016 then
		if tp.场景.玩家[内容.id+0]  then
		tp.场景.玩家[内容.id+0]:穿戴装备(内容)
		end

	elseif 序号 == 1017 then
         tp:生成Npc(内容)
	elseif 序号 == 1018 then
        tp:删除Npc(内容+0)
	elseif 序号 == 1019 then
      tp:临时Npc(内容)
	elseif 序号 == 1020 then
		    tp.场景.人物.跟随目标 =true
		    tp.场景.人物.跟随路径 =内容
	elseif 序号 == 1021 then
	tp.窗口.BOSS战斗:打开(内容)
   elseif 序号 == 1022 then
        if  tp.场景.玩家[内容.id+0] then
        tp.场景.玩家[内容.id+0].人物:更换行为(内容.动作)
       end
   elseif 序号==1024 then
      tp:NPC更改战斗(内容+0)
     elseif 序号==1025 then
      tp:NPC取消战斗(内容+0)
    elseif 序号 == 1026 then
        tp.场景.人物.数据.名称颜色=内容.特效


 --   function 玩家类:添加升级(内容)
 --  if tp.场景.玩家[内容+0] then
 --    table.insert(tp.场景.玩家[内容+0].特效组,tp:载入特效("升级"))
 --   end
 -- end
 -- function 玩家类:添加加血(内容)
 --  if tp.场景.玩家[内容+0] then
 --    table.insert(tp.场景.玩家[内容+0].特效组,tp:载入特效("加血"))
 --    end
 -- end
 -- function 玩家类:添加加蓝(内容)

 --  if tp.场景.玩家[内容+0] then
 --    table.insert(tp.场景.玩家[内容+0].特效组,tp:载入特效("加蓝"))
 --    end
 -- end
    elseif 序号 == 1027 then
        if tp.窗口.帮战.可视 then
            tp.窗口.帮战.数据=内容
        else
            tp.窗口.帮战:打开(内容)
        end
    elseif 序号 == 1028 then
        if tp.窗口.好友查看.可视 then
            tp.窗口.好友查看.数据=内容
        end



  elseif 序号==1029 then
   消息闪烁=false
  elseif 序号==1030 then

    if  tp.窗口.好友聊天.可视 then
        tp.窗口.好友聊天.数据=内容
       tp.窗口.好友聊天.资源组[2]=tp.资源:载入(tp.窗口.好友聊天.数据.造型.."头像.png","加密图片")
    else

       tp.窗口.好友聊天:打开(内容)
    end

        tp.窗口.好友聊天:刷新(内容)
  elseif 序号==1031 then
    if tp.窗口.好友聊天.可视  and 内容.id == tp.窗口.好友聊天.数据.id then
     tp.窗口.好友聊天:刷新(内容)

    end
  elseif 序号==1032 then
   消息闪烁=true
  elseif 序号==1033 then
    if tp.窗口.好友查找.可视 then
    tp.窗口.好友查找.数据 = 内容
  end
    elseif 序号 == 2001 then
        tp.场景.人物:停止移动()
        tp.场景.人物.人物:更换行为(内容)
	elseif 序号 == 2002 then
		if tp.窗口.人物状态栏.可视 then
			tp.窗口.人物状态栏:刷新(内容)
		end
    elseif 序号 == 2003 then
        tp.窗口.梦幻指引:打开(内容)
	elseif 序号 == 2004 then
		tp.场景.人物:加入动画("升级")
	elseif 序号 == 2005 then
		tp.窗口.人物框:刷新召唤兽头像(内容)
	elseif 序号 == 2006 then
        if tp.窗口.召唤兽属性栏.可视 then
          tp.窗口.召唤兽属性栏:刷新(内容)
        else  
            tp.窗口.召唤兽属性栏:打开(内容)
        end
	elseif 序号 == 2007 then
		tp.窗口.召唤兽属性栏:更改参战(内容+0, true)
	elseif 序号 == 2008 then
		tp.窗口.召唤兽属性栏:更改参战(内容+0, false)

	elseif 序号 == 2010 then
		 tp.场景.人物:置模型(内容)
		 tp.窗口.人物框:刷新角色头像()
	elseif 序号 == 2011 then
		tp.窗口.人物称谓栏:打开(内容)
	elseif 序号 == 2012 then
		tp.场景.人物.数据.称谓=内容.称谓
        self.称谓颜色 = 0xFF80BFFF
        if LabelData[内容.称谓] then
            tp.场景.人物.称谓颜色 = LabelData[内容.称谓].颜色 and LabelData[内容.称谓].颜色 or 0xFF80BFFF
            tp.场景.人物.称谓动画=nil
            if LabelData[内容.称谓].动画 then
                tp.场景.人物.称谓动画=tp.资源:载入(LabelData[内容.称谓].资源,"网易WDF动画",LabelData[内容.称谓].动画)
                tp.场景.人物.称谓动画偏移=LabelData[内容.称谓].偏移
            elseif 内容.effects then 
                  tp.场景.人物.称谓动画=tp.资源:载入(LabelData[内容.effects].资源,"网易WDF动画",LabelData[内容.effects].动画)
                    tp.场景.人物.称谓动画偏移=LabelData[内容.effects].偏移
            end
        end
        tp.场景.人物.称谓偏移 = 生成XY(math.floor(tp.字体表.人物字体:取宽度(内容.称谓) / 2),-15)
	elseif 序号 == 2013 then
		 tp.场景.人物:取队伍目标(内容)
	elseif 序号 == 2014 then
		tp.场景.人物:更改坐标(内容)
		tp.场景.人物.跟随目标 =false
	elseif 序号 == 2015 then
		tp.窗口.人物框:刷新召唤兽气血(内容)
	elseif 序号 == 2016 then
		 tp.窗口.人物状态栏:打开(内容)
    elseif 序号 == 2017 then
    	if tp.窗口.兑换界面.可视 then
    		tp.窗口.兑换界面.数据=内容
    	else
    	   tp.窗口.兑换界面:打开(内容)
    	end
	elseif 序号 == 2018 then
		tp.窗口.人物称谓栏:刷新(内容)
	elseif 序号 == 2019 then
	 if tp.窗口.自由技能栏.可视 then
	 	tp.窗口.自由技能栏:刷新(内容)
	 else
     tp.窗口.自由技能栏:打开(内容)
     end
	elseif 序号 == 2020 then
		tp.场景.人物.人物:置染色(DyeData[tp.场景.人物.人物.数据.造型][1],内容[2],内容[3],内容[4])
	elseif 序号 == 2021 then
		tp.窗口.分解:打开(内容)
	elseif 序号 == 2022 then
		tp.主界面.界面数据[27]:刷新(内容)
	elseif 序号 == 2023 then
		tp.窗口.加锁:打开(内容)

	elseif 序号 == 2024 then  --设置加锁密码
		tp.窗口.设置密码:打开(内容)
	elseif 序号 == 2025 then  --解除解锁模式
		tp.窗口.解除密码:打开(内容)
	 elseif 序号==2027 then
	 	if tp.窗口.技能学习.可视 then
	 		  tp.窗口.技能学习:刷新(内容)
	 	else
	 	  	  tp.窗口.技能学习:打开(内容)
	 	end
	elseif 序号==2028 then
		tp.窗口.道具拍卖:打开(内容)
    elseif 序号==2029 then
        if tp.窗口.道具竞拍.可视 then
            tp.窗口.道具竞拍:刷新(内容)
        end
	elseif 序号==2030 then
		    tp.窗口.道具竞拍:打开(内容)

    elseif 序号 == 2031 then
        tp.场景.人物:加入动画(内容)
	elseif 序号==2032 then
		if tp.窗口.道具竞拍.可视 then
		    tp.窗口.道具竞拍:打开()
		end
    elseif 序号==2033 then

        tp.窗口.道具小窗口:打开(内容)  
	elseif 序号==2034 then
		if tp.窗口.法宝合成.可视 then
			tp.窗口.法宝合成:刷新(内容)
		 else
		 	tp.窗口.法宝合成:打开(内容)
		end
    elseif 序号 == 2035 then
        tp.窗口.人物称谓栏.数据 = 内容[1]
        tp.窗口.人物称谓栏.仙玉 = 内容[2]
        tp.窗口.人物称谓栏.特效 = 内容[3]
    elseif 序号 == 2036 then
      if  tp.窗口.光武拓印.可视 then
        tp.窗口.光武拓印:刷新(内容)
      end
    elseif 序号 == 2037 then
      if  tp.窗口.改变双加.可视 then
        tp.窗口.改变双加:刷新(内容)
      end
    elseif 序号==2060 then
      tp.窗口.宠物领养栏:打开()
	elseif 序号==2062 then
      tp.窗口.宠物状态栏:打开(内容)

    elseif 序号==2068 then
    tp.窗口.奇经八脉:打开(内容)
    elseif 序号==2069 then
    tp.窗口.奇经八脉:刷新(内容)
	elseif 序号==2070 then
		tp.窗口.召唤兽仓库:打开(内容)
	elseif 序号==2071 then
	    tp.窗口.召唤兽仓库.数据=内容
    elseif 序号==2072 then
        tp.窗口.成就弹窗:打开(内容)
	elseif 序号==2073 then
       tp.窗口.宠物状态栏.宠物数据=内容
    elseif 序号==2074 then
	  tp.窗口.快捷技能栏.数据 =table.loadstring(table.tostring(内容))
	  -- tp.快捷技能显示 =true
	elseif 序号==2077 then
		 if  tp.窗口.修炼.可视 then
		 	 tp.窗口.修炼:刷新(内容)
		 else
		 	 tp.窗口.修炼:打开(内容)
		 end
    elseif 序号==2078 then
	    if tp.窗口.好友列表.可视 then
	    tp.窗口.好友列表.数据=内容
	    else
	    tp.窗口.好友列表:打开(内容)
	    end
    elseif 序号 == 3000 then
        if tp.窗口.法宝神器.可视 then
            tp.窗口.法宝神器:刷新(内容)
        else
           tp.窗口.法宝神器:打开(内容)
        end
    elseif 序号 == 3001 then
		tp.金钱 = 内容.银两
		tp.存银 = 内容.存银
		tp.储备 = 内容.储备
        if tp.窗口.道具行囊.可视 then
            tp.窗口.道具行囊:刷新(内容)
        else
           tp.窗口.道具行囊:打开(内容)
        end

    elseif 序号==3002 then
    tp.窗口.宠物炼妖栏:添加召唤兽(内容)
    elseif 序号==3003 then
    tp.窗口.宠物炼妖栏:添加道具(内容)
    elseif 序号==3004 then
    	if  tp.窗口.宠物炼妖栏.可视==false then
    	    tp.窗口.宠物炼妖栏:打开()
    	end
	elseif 序号 == 3005 then
		if  tp.窗口.打造.可视 then
		  tp.窗口.打造:刷新(内容)
		else
			tp.窗口.打造:打开(内容)
		end
	elseif 序号 == 3006 then
		if  tp.窗口.道具行囊.可视  then
			local sdf
			if tp.窗口.道具行囊.更新 =="包裹" then
				sdf =1

			elseif tp.窗口.道具行囊.更新 =="锦衣" then
				sdf =4
			else
				sdf =2
			end
			客户端:发送数据(sdf, 1, 13, "9", 1)
         end
        if tp.窗口.法宝神器.可视 then
            客户端:发送数据(3, 1, 13, "9", 1)
		end
    elseif 序号 == 3007 then
       if  tp.窗口.道具行囊.可视 then
        
        local tempNumber =(tp.窗口.道具行囊.数据.Pages-1)*20
        print(tempNumber)
         if 内容.格子 > tempNumber and 内容.格子 <= tp.窗口.道具行囊.数据.Pages*20 then 
            tp.窗口.道具行囊.物品[tonumber(内容.格子)-tempNumber]:置物品(内容.数据)
         end
         
       end


    elseif 序号 == 3008 then
        if tp.窗口.道具行囊.可视 then
            tp.窗口.道具行囊.数据[内容.Type]=内容.Value

        end

    elseif 序号==3009 then
      if tp.窗口.道具仓库.可视 then
        local 数据 = 内容
        for i=1,20 do
          tp.窗口.道具仓库.物品[i]:置物品(数据[i])
        end
      else
      tp.窗口.道具仓库:打开(内容)
      end
    elseif 序号==3010 then
      tp.窗口.道具仓库:刷新仓库数据(内容)

	elseif 序号 == 3011 then
		tp.窗口.NPC给予:打开(内容)
	elseif 序号 == 3012 then
		tp.窗口.染色:打开(内容)
	elseif 序号==3013 then
	tp.窗口.交易界面:打开(内容)
	elseif 序号==3014 then
	 tp.窗口.交易界面:刷新道具(内容)
	elseif 序号==3015 then
		tp.窗口.交易界面:刷新召唤兽(内容)
    elseif 序号==3016 or 序号==3018 then
      if tp.窗口.交易界面.可视 then
          tp.窗口.交易界面.可视=false
      end
	elseif 序号 == 3017 then
		tp.窗口.交易界面:刷新交易数据(内容)
	elseif 序号 == 3019 then
		if tp.窗口.开运界面.可视 then
    		 tp.窗口.开运界面:刷新(内容,"修理")
    	 else
    	 	 tp.窗口.开运界面:打开(内容,"修理")
    	end
    elseif 序号 == 3020 then
		if tp.窗口.开运界面.可视 then
    		 tp.窗口.开运界面:刷新(内容,"鉴定")
    	 else
    	 	 tp.窗口.开运界面:打开(内容,"鉴定")
    	end
    elseif 序号==3021 then
      if tp.窗口.灵饰.可视 then
         local 灵饰数据 = 内容
          for i=31,35 do
            tp.窗口.灵饰.物品[i]:置物品(nil)
          if 灵饰数据[i] ~= nil and 灵饰数据[i] ~= 0 then
          tp.窗口.灵饰.物品[i]:置物品(灵饰数据[i])
          end
          end
      else
       tp.窗口.灵饰:打开(内容)
     end
     elseif 序号==3022 then
         if tp.窗口.道具行囊.可视 then
           tp.窗口.道具行囊.数据.宝宝列表 = 内容
          tp.窗口.道具行囊:置形象()
        end
	elseif 序号 == 3030 then
		if tp.窗口.人物状态栏.可视 then
			客户端:发送数据(0, 13, 5, 0, 1)
		end
	elseif 序号 == 3039 then
		tp.窗口.玩家给予:打开(内容)
	elseif 序号 == 3040 then
		if tp.窗口.召唤兽属性栏.可视 then
			客户端:发送数据(6, 1, 22, "6A", 1)
		end
    elseif 序号 == 3041 then
        tp.窗口.成长之路:打开(内容)
    elseif 序号==3074 then
    	if tp.窗口.出售.可视 then
    		tp.窗口.出售:刷新(内容)
        else
    	  	tp.窗口.出售:打开(内容)
    	end
    elseif 序号==3075 then
        tp.窗口.黑市拍卖:打开(内容)

    elseif 序号==3076 then
        tp.窗口.黑市拍卖:执行流程(内容)

    elseif 序号==3077 then
        tp.窗口.炼丹炉:打开(内容)
    elseif 序号==3078 then
    tp.窗口.炼丹炉:刷新(内容)


  --     elseif 序号==108 then
  --   tp.窗口.炼丹炉:打开(内容)
  -- elseif 序号==108.1 then
  --   tp.窗口.炼丹炉:更新(内容)
  -- elseif 序号==108.2 then
  --   tp.窗口.炼丹炉:开奖(内容)
  -- elseif 序号==109 then
  --   tp.窗口.炼丹炉:刷新(内容)
  -- elseif 序号==110 then
  --   tp.窗口.指定系统:打开(内容)
    
	elseif 序号 == 5001 then
		tp.组队开关 = true
		tp.鼠标.置鼠标("组队")
    elseif 序号==5002 then
      tp.场景.队长开关=true
      tp.场景.人物:改变队标(内容)
    elseif 序号==5003 then
      tp.场景.队长开关=false
    elseif 序号==5004 then
     	tp:更改队长(内容,true)
    elseif 序号==5005 then
    	tp:更改队长(内容,false)
	elseif 序号 == 5006 then
		if tp.窗口.队伍栏.可视 then
			tp.窗口.队伍栏:刷新(内容)
		else
         tp.窗口.队伍栏:打开(内容)
        end
	elseif 序号 == 5007 then
		tp.窗口.队伍栏:刷新阵法(内容)
	elseif 序号 == 5008 then
		tp.窗口.队伍阵型栏:打开(内容)
	elseif 序号 == 5009 then
		tp.窗口.人物框:添加队伍(内容)
	elseif 序号 == 5010 then
		if tp.窗口.队伍栏申请.可视 then
			tp.窗口.队伍栏申请:刷新(内容)
		else
         tp.窗口.队伍栏申请:打开(内容)
        end
   	elseif 序号 == 5011 then
		tp.窗口.队伍栏.可视=false
   	elseif 序号 == 5012 then
		tp.窗口.队标栏:打开(内容)


	elseif 序号 == 6001 then
		队伍id = 内容 + 0
	elseif 序号 == 6002 then
        
		tp.场景.战斗:准备战斗()
		tp.场景.战斗.单位总数 = 内容 + 0
		self:发送数据(3, 231, 24, "77", 0)
	elseif 序号 == 6003 then
		tp.场景.战斗:添加单位(内容)
	elseif 序号 == 6004 then
		tp.场景.战斗:设置命令回合(内容)
	elseif 序号 == 6018 then
		tp.场景.战斗:取消助战托管(内容)
	elseif 序号 == 6005 then
		tp.场景.战斗:设置执行回合(内容)
	elseif 序号 == 6006 then
		tp.场景.战斗:更新状态组(内容)
	elseif 序号 == 6007 then
		tp.场景.战斗:退出战斗()
        
        collectgarbage("collect")
	elseif 序号 == 6008 then

		tp.场景.战斗:刷新道具数据(内容)
	elseif 序号 == 6009 then
		tp.场景.战斗.窗口.召唤栏:打开(内容)
	elseif 序号 == 6010 then
        
		tp.场景.战斗:准备战斗(1)
		tp.场景.战斗.单位总数 = 内容 + 0
		self:发送数据(3, 231, 24, "77", 0)
	elseif 序号 == 6011 then
		tp.场景.战斗:刷新法宝数据(内容)
	elseif 序号 == 6012 then
		tp.场景.战斗:准备战斗(nil,1)
		tp.场景.战斗.单位总数 = 内容 + 0
		self:发送数据(3, 231, 24, "77", 0)
	elseif 序号 == 6013 then
		tp.场景.战斗:添加怪物发言内容(内容.id, 内容.消息)
	elseif 序号 == 9001 then

		 tp.窗口.商店:打开(内容)
    elseif 序号 == 9002 then
       tp.窗口.商店.数据.银子= 内容+0
    elseif 序号 == 9998 then
        if tp.窗口.唤兽店铺.可视 then
              tp.窗口.唤兽店铺:刷新(内容)
          else
              tp.窗口.唤兽店铺:打开(内容)
          end
     elseif 序号 == 9999 then
          if tp.窗口.商会唤兽管理.可视 then
            tp.窗口.商会唤兽管理.暂停营业:置文字("暂停营业")
         end
    elseif 序号 == 10000 then
          if tp.窗口.商会唤兽管理.可视 then
            tp.窗口.商会唤兽管理.暂停营业:置文字("开始营业")
         end


    elseif 序号 == 10001 then
        tp.窗口.任务栏:打开(内容)   
    elseif 序号 == 10002 then
        tp.窗口.商会唤兽管理:打开(内容)
    elseif 序号 == 10003 then
          tp.窗口.商会唤兽管理:刷新柜台道具(内容)
    elseif 序号 == 10004 then
          tp.窗口.商会唤兽管理:刷新玩家道具(内容)

	elseif 序号 == 11001 then

		 tp.窗口.商会管理类:打开(内容)
     elseif 序号 == 11002 then    
        if  tp.窗口.商会唤兽管理.可视 then
              tp.窗口.商会唤兽管理.店铺信息.店面 = 内容
        end
       
     elseif 序号 == 11003 then
         if  tp.窗口.商会唤兽管理.可视 then
         tp.窗口.商会唤兽管理.店铺信息.现金 = 内容
         tp.窗口.商会唤兽管理.店铺信息.日常运营资金 = 0
         end
	elseif 序号 == 11004 then
		  tp.窗口.商会管理类:刷新玩家道具(内容)
	elseif 序号 == 11005 then
		  tp.窗口.商会管理类:刷新柜台道具(内容)
	elseif 序号 == 11006 then
		  if tp.窗口.商会管理类.可视 then
		  	tp.窗口.商会管理类.暂停营业:置文字("开始营业")
		 end
	elseif 序号 == 11007 then
		  if tp.窗口.商会管理类.可视 then
		  	tp.窗口.商会管理类.暂停营业:置文字("暂停营业")
	     end
	elseif 序号 == 11009 then
		tp.窗口.商会购买类:打开(内容)
    elseif 序号 == 11010 then
        if tp.窗口.物品店铺.可视 then
              tp.窗口.物品店铺:刷新(内容)
          else
              tp.窗口.物品店铺:打开(内容)
          end
	elseif 序号 == 11011 then
      if tp.窗口.存钱界面.可视  then
       tp.窗口.存钱界面.数据 = 内容
      else
       tp.窗口.存钱界面:打开(内容)
      end
	elseif 序号 == 11012 then
	  if tp.窗口.取钱界面.可视  then
       tp.窗口.取钱界面.数据 = 内容
      else
       tp.窗口.取钱界面:打开(内容)
      end
    elseif 序号==11015 then
      tp.窗口.任务栏:刷新(内容)
	elseif 序号 == 20001 then
		tp.窗口.摆摊出售:打开(内容)
	elseif 序号 == 20002 then
		tp.窗口.摆摊出售:刷新道具(内容)
	elseif 序号 == 20003 then
		tp.窗口.摆摊出售:刷新道具状态(内容)
	elseif 序号 == 20004 then
		 tp.窗口.摆摊出售:移除道具(内容)
	elseif 序号 == 20005 then
		tp.窗口.摆摊出售:刷新宝宝(内容)
	elseif 序号 == 20006 then
		tp.窗口.摆摊出售:刷新bb状态(内容)
	elseif 序号 == 20007 then
		 tp.窗口.摆摊出售:移除bb(内容)
	elseif 序号 == 20008 then
		tp.场景.人物:添加摊位名称(内容)
	elseif 序号 == 20009 then
		 tp:更换摊位名称(内容)
	elseif 序号 == 20010 then
		 tp.窗口.摆摊购买:打开(内容)
	elseif 序号 == 20011 then
		tp.窗口.摆摊购买:刷新道具(内容)
	elseif 序号 == 20012 then
	  if tp.窗口.摆摊出售.可视 and tp.窗口.摆摊出售.状态 == "物品类" then
       客户端:发送数据(42, 4, 44, "1", 1)
      end
	elseif 序号 == 20013 then
		tp.窗口.摆摊购买:刷新宝宝(内容)
	elseif 序号 == 20014 then
	 if tp.窗口.摆摊出售.可视 and tp.窗口.摆摊出售.状态 == "召唤兽类" then
       客户端:发送数据(42, 5, 44, "1", 1)
      end
	elseif 序号 == 20015 then
		 tp.窗口.摆摊出售.bb列表 = 内容
	elseif 序号 == 20017 then
		tp.主界面.界面数据[33]:刷新(内容)
	elseif 序号 == 20019 then
		tp.窗口.加入帮派:打开(内容)
	elseif 序号 == 20020 then
		tp.窗口.帮派类:打开(内容)
	elseif 序号 == 20022 then
		tp.窗口.帮派申请列表类:打开(内容)
	elseif 序号 == 20023 then
		tp.窗口.帮派申请列表类.可视 = false
		tp.窗口.帮派类.可视 = false
	elseif 序号 == 20024 then
		tp.窗口.帮派成员类:打开(内容)
	elseif 序号 == 20026 then
		tp.窗口.帮派信息类:打开(内容)
	elseif 序号 == 20031 then
		   tp.窗口.坐骑:打开(内容)
	elseif 序号 == 20032 then
		if tp.窗口.坐骑.可视 then
			tp.窗口.坐骑:刷新(内容)
		end
        if tp.窗口.道具行囊.可视 then
           tp.窗口.道具行囊.数据.坐骑数据 = 内容.坐骑数据
           tp.窗口.道具行囊.数据.坐骑 = 内容.坐骑
           tp.窗口.道具行囊:置形象()
        end
    elseif 序号 == 20033 then
        if tp.窗口.道具行囊.可视 then
           tp.窗口.道具行囊.数据.子女列表 = 内容
           tp.窗口.道具行囊:置形象()
        end
	elseif 序号 == 20034 then
        if tp.窗口.商城.可视 then
            if 内容.ID =="召唤兽" then
                tp.窗口.商城:刷新宝宝(内容)
            else 
                tp.窗口.商城:刷新(内容)
            end 
        else 
            tp.窗口.商城:打开(内容)
        end
    elseif 序号 == 20037 then
    	if tp.窗口.开运界面.可视 then
    		 tp.窗口.开运界面:刷新(内容,"开运")
    	 else
    	 	 tp.窗口.开运界面:打开(内容,"开运")
    	end
    elseif 序号 == 20038 then
		if tp.窗口.合成.可视 then
		tp.窗口.合成:刷新(内容)
		else
		tp.窗口.合成:打开(内容)
		end
 	elseif 序号 == 20039 then
 		tp.窗口.摆摊购买:刷新制造(内容)
 	elseif 序号 == 20040 then
		tp.窗口.摆摊出售:刷新制造(内容)
	elseif 序号 == 20041 then
		tp.窗口.摆摊出售:刷新制造状态(内容)
	elseif 序号 == 20042 then
		 tp.窗口.摆摊出售:移除制造(内容)
    elseif 序号 == 20043 then
    	tp.窗口.商城.数据.银子 =内容+0
     elseif 序号 == 20044 then
    	tp.窗口.仙玉抽奖:打开(内容)
     elseif 序号 == 20045 then
    	tp.窗口.仙玉抽奖:停止(内容)
     elseif 序号 == 20046 then
    	tp.窗口.排行榜:打开(内容)
     elseif 序号 == 20047 then
   		  tp.窗口.排行榜:刷新(内容)
     elseif 序号 == 20048 then
   		  tp.窗口.祈福:打开(内容)
   	 elseif 序号 == 20049 then
   	 	if  tp.窗口.坐骑染色.可视 then
   	 	     tp.窗口.坐骑染色.数据=内容
   	 	else
   	 	 	 tp.窗口.坐骑染色:打开(内容,"坐骑")
   	 	end
   	 elseif 序号 == 20050 then
   	 	if  tp.窗口.坐骑染色.可视 then
   	 	     tp.窗口.坐骑染色.数据=内容
   	 	else
   	 	 	 tp.窗口.坐骑染色:打开(内容,"饰品")
   	 	end
    elseif 序号 == 20052 then
        tp.窗口.查看装备:打开(内容)
    elseif 序号 == 20053 then
        if tp.窗口.人物状态栏.可视  then
            tp.窗口.人物状态栏.数据.剧情技能点=内容.剧情点
            for n=1,#内容.剧情技能 do
                    tp.窗口.人物状态栏.剧情技能[n]:置技能(内容.剧情技能[n])
            end
        end
    elseif 序号 == 20054 then
         -- tp.窗口.圣旨:打开(内容)
     elseif 序号 == 20055 then
        -- if tp.窗口.签到.可视 then
            tp.窗口.签到:打开(内容)
        -- else
        --     tp.窗口.签到.可视 = true
        -- end
    elseif 序号 == 20056 then
        tp.窗口.活跃兑换:打开(内容)
    elseif 序号 == 20057 then
        tp.窗口.活跃兑换.数据.银子 =内容+0
    elseif 序号==30 or 序号==9957 then
     	tp.窗口.快捷技能栏.数据.快捷技能={}
	elseif 序号 == 20058 then
		tp.助战列表=内容
		if not tp.窗口.助战界面.可视 then
			tp.窗口.助战界面:打开()
		else
			tp.窗口.助战界面:刷新武器()
		end
	elseif 序号 == 20059 then
		if not tp.窗口.助战物品界面.可视 then
			tp.窗口.助战物品界面:打开(内容)
		else
			tp.窗口.助战物品界面:刷新(内容)
		end
		
	elseif 序号 == 20060 then
		tp.窗口.助战界面:刷新(内容)
	elseif 序号 == 20061 then	--助战技能
		if not tp.窗口.助战技能学习.可视 then
			tp.窗口.助战技能学习:打开(内容)
		else
			tp.窗口.助战技能学习:刷新(内容)
		end
	elseif 序号 == 20062 then	--助战修炼
		if not tp.窗口.助战修炼界面.可视 then
			tp.窗口.助战修炼界面:打开(内容)
		else
			tp.窗口.助战修炼界面:刷新(内容)
		end
	elseif 序号 == 20063 then	--辅助技能
		if not tp.窗口.助战辅助技能.可视 then
			tp.窗口.助战辅助技能:打开(内容)
		else
			tp.窗口.助战辅助技能:刷新(内容)
		end
	elseif 序号 == 20064 then	--奇经八脉
		if not tp.窗口.助战奇经八脉.可视 then
			tp.窗口.助战奇经八脉:打开(内容)
		else
			tp.窗口.助战奇经八脉:刷新(内容)
		end
    elseif 序号 == 20070 then --助战宠物
        if not tp.窗口.助战宠物.可视 then
            tp.窗口.助战宠物:打开(内容)
        else
            tp.窗口.助战宠物:刷新(内容)
        end

	elseif 序号 == 20065 then
		tp.窗口.挂机系统:打开(内容)
	elseif 序号 == 20066 then
		tp.窗口.挂机系统:更新(内容)
	elseif 序号 == 20067 then
		tp.窗口.挂机系统:刷新开关(内容)
	elseif 序号 == 20068 then
		tp.窗口.挂机系统:申请移动(内容)
	elseif 序号 == 20069 then
		if not tp.窗口.元神系统.可视 then
			tp.窗口.元神系统:打开(内容)
		else
			tp.窗口.元神系统:刷新(内容)
		end
    -- elseif 序号==381990860 then
    --     管理模式=true
	end
	序号, 内容, 时间 = nil
end

function 回调:显示(x, y)
end

return 回调
