-- @Author: 作者QQ414628710
-- @Date:   2022-01-15 20:06:44
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-24 02:01:12
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:47
--======================================================================--
local 场景类_商店 = class()
local tp,zts
local insert = table.insert
local xxx = 0
local yyy = 0
local sts = {"数量","总额","现金"}
local sts1 = {"数量","总额","仙玉"}
local sts2 = {"数量","总额","积分"}
local tos = 引擎.取金钱颜色
local mousea = 引擎.鼠标按住
local mouseb = 引擎.鼠标弹起

local zqj = 引擎.坐骑库
local bw = require("gge包围盒")(0,0,122,122)
local box = 引擎.画矩形
local bd0 = {"气血","魔法","攻击","防御","速度","灵力"}
local bd = {"体质","魔力","力量","耐力","敏捷"}
function 场景类_商店:初始化(根)
	self.ID = 56
	self.x = 50
	self.y = 50
	self.xx = 0
	self.yy = 0
	self.注释 = "商店"
	tp = 根
		self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.方向 = 1
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.Icon={
   		资源:载入('图标1.png',"加密图片"),
   		资源:载入('图标2.png',"加密图片"),
   		资源:载入('图标3.png',"加密图片"),
   		资源:载入('图标4.png',"加密图片"),
   		资源:载入('图标5.png',"加密图片"),
   		资源:载入('图标6.png',"加密图片"),
     }
	self.资源组 = {
		[0] = Picture.ShopItem1,
        [1] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城侧边灰色"),0,0,4,true,true,"      银子商城"),
        [2] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城侧边黑色"),0,0,4,true,true,"      仙玉商城"),
        [3] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城侧边灰色"),0,0,4,true,true,"      积分商城"),
        [4] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城侧边黑色"),0,0,4,true,true,"      锦衣商城"),
        [5] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城侧边灰色"),0,0,4,true,true,"      神兽商城"),
        [6] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城侧边灰色"),0,0,4,true,true,"      仙玉充值"),
        -------------------------------------------
		[11] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"普 通"),
		-------------------------
  		[21] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"仙 玉"),
		[22] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"法 宝"),
		[23] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"孩 子"),
  		[31] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"活 动"),
  		-----------------
		[32] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"比 武"),
		[33] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"副 本"),


		[34] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"地 煞"),
		[35] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"知 了"),
		[36] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"天 罡"),
		[37] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"单 人"),
		[38] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"成 就"),
		[39] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"特 殊"),
		--------------------------
        [41] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"锦 衣"),
		[42] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"光 环"),
		[43] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"脚 印"),
		[44] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"祥 瑞"),
		[45] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"定 制"),
		[46] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"传 音"),
         -----------------
        [51] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"神 兽"),
		--[45] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城上边按钮"),0,0,4,true,true,"法宝商城"),
		----------

		[61] = 按钮.创建(自适应.创建(15,4,72,20,1,3),0,0,4,true,true," 购  买"),
		[62] = 自适应.创建(3,1,94,22,1,3),

	
		[63] = 按钮.创建(自适应.创建(101,4,72,22,1,3),0,0,4,true,true," 上一页"),
		[64] = 按钮.创建(自适应.创建(101,4,72,22,1,3),0,0,4,true,true," 下一页"),

		[68] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x92ABEFD3),0,0,4,true,true),

		[70] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城关闭按钮"),0,0,4,true,true),
		[71] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城转向按钮"),0,0,4,true,true),
		[72] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画","商城动作按钮"),0,0,4,true,true),

	}
	self.资源组[61]:绑定窗口_(56)
	self.资源组[63]:绑定窗口_(56)
	self.资源组[64]:绑定窗口_(56)
	for n=11,11 do
	  self.资源组[n]:置文字颜色(0xFFB0CCF1)
	  self.资源组[n]:置偏移(2,7)
	  self.资源组[n]:绑定窗口_(56)
	end
	for n=21,23 do
	  self.资源组[n]:置文字颜色(0xFFB0CCF1)
	  self.资源组[n]:置偏移(2,7)
	  self.资源组[n]:绑定窗口_(56)
	end
	for n=31,39 do

	self.资源组[n]:置文字颜色(0xFFB0CCF1)
	self.资源组[n]:置偏移(2,7)
	self.资源组[n]:绑定窗口_(56)
	end
	for n=41,46 do
	  self.资源组[n]:置文字颜色(0xFFB0CCF1)
	  self.资源组[n]:置偏移(2,7)
	  self.资源组[n]:绑定窗口_(56)
	end
	for n=51,51 do
	  self.资源组[n]:置文字颜色(0xFFB0CCF1)
	  self.资源组[n]:置偏移(2,7)
	  self.资源组[n]:绑定窗口_(56)
	end
	-- for n=6,30 do
	--    self.资源组[n]:绑定窗口_(56)
	-- end

	for i=1,6 do
		self.资源组[i]:置文字颜色(0xFFB0CCF1)
		self.资源组[i]:置偏移(-2,4)
		self.资源组[i]:绑定窗口_(56)
	end
	self.商品 = {}
	local 物品格子 = 根._物品格子
	for i=1,32 do
		self.商品[i] = 物品格子(0,i,n,"商店")
	end
	self.maximum=32
	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('商店总控件')
	总控件:置可视(true,true)
	self.输入框 = 总控件:创建输入("数量输入", 437,383,100,14)
	self.输入框:置可视(false,false)
	self.输入框:置限制字数(3)
	self.输入框:置数字模式()
	self.输入框:置光标颜色(-16777216)
	self.输入框:置文字颜色(0xFFFFFFFF)
	self.单价 = 0
	self.数量 = 0
	self.上一次 = 1
	self.窗口时间 = 0
	zts = 根.字体表.华康字体
    self.加入 = 0
	self.状态= 1
	self.焦点1 = false
	self.头像组 ={}
	self.召唤兽数据={}


end


function 场景类_商店:打开(数据)
	if self.可视 then
		self.可视 = false
       	for i=1,self.maximum do
			self.商品[i]:置物品(nil)
		end
		self.道具 = nil
		self.方向 = 1
		self.单价 = 0
		self.数量 = 0
		self.可视 = false
		self.商品[self.上一次].确定 = false
		self.上一次 = 1
		self.输入框:置焦点(false)
		self.输入框:置可视(false,false)
		self.头像组 ={}
		self.召唤兽数据={}
		self.选中召唤兽 = nil
	else
		self.资源组[99] = nil
		self.资源组[100] = nil
		self.资源组[101] = nil
		self.资源组[102] = nil
		self.资源组[103] = nil
		if  self.x > 全局游戏宽度 then
		self.x = 100
		end
    	self.资源组[0] = Picture.ShopItem1
		self.maximum=32
		self.加入 = 0
        self.头像组 ={}
		self.数据=数据
		self.min =数据.min
		self.状态 =数据.ID
		insert(tp.窗口_,self)
		for n=1,self.maximum do
			self.商品[n]:置物品(self.数据[n],1)
	  	end
	    tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end
function 场景类_商店:刷新(数据)
	

	self.资源组[99] = nil
		self.资源组[100] = nil
		self.资源组[101] = nil
		self.资源组[102] = nil
		self.资源组[103] = nil
	self.资源组[0] = (数据.ID=="锦衣" or 数据.ID=="光环" or 数据.ID=="脚印"or 数据.ID=="祥瑞"or 数据.ID=="定制"or 数据.ID=="传音") and   Picture.ShopItem or Picture.ShopItem1
	self.maximum =(数据.ID=="锦衣" or 数据.ID=="光环" or 数据.ID=="脚印"or 数据.ID=="祥瑞"or 数据.ID=="传音"or 数据.ID=="定制") and   20 or 32
	self.选中召唤兽=nil
	self.召唤兽数据={}
	self.方向 = 1
	self.头像组 ={}
		self.道具 = nil
		self.单价 = 0
		self.数量 = 0
		self.商品[self.上一次].确定 = false
		self.上一次 = 1
		self.输入框:置焦点(false)
		self.输入框:置可视(false,false)
		self.数据=数据
		self.min =数据.min
		self.状态 =数据.ID
		for n=1,self.maximum do
			self.商品[n]:置物品(self.数据[n],1)
	  	end
end
function 场景类_商店:刷新宝宝(数据)

		self.资源组[0] = Picture.ShopPet
       self.maximum = 12
        self.头像组 ={}
	  	for i=1,32 do
			self.商品[i]:置物品(nil)
		end
		self.选中召唤兽 = nil
		self.道具 = nil
		self.单价 = 0
		self.数量 = 0
		self.商品[self.上一次].确定 = false
		self.上一次 = 1
		self.输入框:置焦点(false)
		self.输入框:置可视(false,false)
		self.数据=数据
		self.加入 = 0
		self.召唤兽数据=数据
		self.min =数据.min
		self.状态 =数据.ID
		for i=1,#self.召唤兽数据 do
			if self.召唤兽数据[i] ~= nil then
	
			local n = ModelData[self.召唤兽数据[i].造型]

			self.头像组[i] =self:置形象(self.召唤兽数据[i]) 
			end
	   end
end
function 场景类_商店:显示(dt,x,y)
	self.资源组[70]:更新(x,y)
	self.资源组[61]:更新(x,y,self.道具 ~= nil or self.选中召唤兽 ~= nil)
	self.焦点 = false
	self.焦点1 = false
	self.资源组[0]:显示(self.x,self.y)
	self.资源组[70]:显示(self.x+641,self.y+10,true)
	if self.资源组[70]:事件判断() then
	  self:打开()
	end
	
	self.资源组[63]:更新(x,y,self.min >self.maximum)  ---上

	self.资源组[64]:更新(x,y)--下

    if self.资源组[63]:事件判断() then  --上

    	 local del =self.状态=="召唤兽"  and 12 or self.maximum
    	 客户端:发送数据(self.min-del,1,14,self.状态)
    elseif self.资源组[64]:事件判断() then --下
    	 local del =self.状态=="召唤兽"  and 12 or self.maximum
    	 客户端:发送数据(self.min+del,1,14,self.状态)
    elseif self.资源组[1]:事件判断() then
       客户端:发送数据(1,1,14,"银子")
    elseif self.资源组[2]:事件判断() then--仙玉
           客户端:发送数据(1,1,14,"仙玉")
    elseif self.资源组[3]:事件判断() then--积分
         客户端:发送数据(1,1,14,"活动积分")
    elseif self.资源组[4]:事件判断() then--锦衣
      	客户端:发送数据(1,1,14,"锦衣")
    elseif self.资源组[5]:事件判断() then--召唤兽
      	客户端:发送数据(1,1,14,"召唤兽")
    elseif self.资源组[6]:事件判断() then--CDK
      	os.execute('start '..ulr)
	end
	if self.状态 == "银子" then
		self.资源组[11]:显示(self.x+132,self.y+59,true,1,nil,self.状态=="银子",2)
		self.资源组[11]:更新(x,y,self.状态~="银子")
		if self.资源组[11]:事件判断() then
		     客户端:发送数据(1,1,14,"银子")
        end
	elseif self.状态=="仙玉" or self.状态=="法宝" or self.状态=="孩子"then
		self.资源组[21]:显示(self.x+132,self.y+59,true,1,nil,self.状态=="仙玉",2)
		self.资源组[21]:更新(x,y,self.状态~="仙玉")
		self.资源组[22]:显示(self.x+188,self.y+59,true,1,nil,self.状态=="法宝",2)
		self.资源组[22]:更新(x,y,self.状态~="法宝")
		self.资源组[23]:显示(self.x+244,self.y+59,true,1,nil,self.状态=="孩子",2)
		self.资源组[23]:更新(x,y,self.状态~="孩子")
		if self.资源组[21]:事件判断() then
		    客户端:发送数据(1,1,14,"仙玉")
		elseif self.资源组[22]:事件判断() then
		    客户端:发送数据(1,1,14,"法宝")
		elseif self.资源组[23]:事件判断() then
		    客户端:发送数据(1,1,14,"孩子")
        end


	elseif self.状态 =="活动积分" or self.状态 =="比武积分"or self.状态 =="副本积分" or self.状态 =="地煞积分"or self.状态 =="知了积分"or self.状态 =="天罡积分"or self.状态 =="单人积分"or self.状态 =="成就积分"or self.状态 =="特殊积分" then
		local state={"活动积分","比武积分","副本积分","地煞积分","知了积分","天罡积分","单人积分","成就积分","特殊积分"}
		for i=1,9 do
			self.资源组[30+i]:显示(self.x+132+(i-1)*56,self.y+59,true,1,nil,self.状态==state[i],2)
			self.资源组[30+i]:更新(x,y,self.状态~=state[i])
				if self.资源组[30+i]:事件判断() then
		         	客户端:发送数据(1,1,14,state[i])
		        end
		end
	elseif self.状态=="锦衣" or self.状态=="光环" or self.状态=="脚印"or self.状态=="祥瑞"or self.状态=="定制"or self.状态=="传音"then
		local state ={"锦衣","光环","脚印","祥瑞","定制","传音"}
		for i=1,6 do
			self.资源组[40+i]:显示(self.x+132+(i-1)*56,self.y+59,true,1,nil,self.状态==state[i],2)
			self.资源组[40+i]:更新(x,y,self.状态~=state[i])
				if self.资源组[40+i]:事件判断() then
		         	客户端:发送数据(1,1,14,state[i])
		        end
		end

	end
    self.资源组[1]:更新(x,y,self.状态~="银子")
    self.资源组[2]:更新(x,y,self.状态~="仙玉" and self.状态~="法宝"and self.状态~="孩子" )
    self.资源组[3]:更新(x,y,self.状态 ~="活动积分" and self.状态 ~="比武积分"and self.状态 ~="副本积分" and self.状态 ~="地煞积分"and self.状态 ~="知了积分"and self.状态 ~="天罡积分"and self.状态 ~="单人积分"and self.状态 ~="成就积分"and self.状态 ~="特殊积分")
    self.资源组[4]:更新(x,y,self.状态~="锦衣"and self.状态~="光环"and self.状态~="脚印"and self.状态~="祥瑞"and self.状态~="定制"and self.状态~="传音")
    self.资源组[5]:更新(x,y,self.状态~="召唤兽")
     self.资源组[6]:更新(x,y)
	self.资源组[1]:显示(self.x+2,self.y+65+(1-1)*36,true,1,nil,self.状态=="银子",2)
	self.资源组[2]:显示(self.x+2,self.y+65+(2-1)*36,true,1,nil,self.状态=="仙玉" or self.状态=="法宝"or self.状态=="孩子",2)
	self.资源组[3]:显示(self.x+2,self.y+65+(3-1)*36,true,1,nil,self.状态 =="活动积分" or self.状态 =="比武积分"or self.状态 =="副本积分" or self.状态 =="地煞积分"or self.状态 =="知了积分"or self.状态 =="天罡积分"or self.状态 =="单人积分"or self.状态 =="成就积分"or self.状态 =="特殊积分",2)
	self.资源组[4]:显示(self.x+2,self.y+65+(4-1)*36,true,1,nil,self.状态=="锦衣"or self.状态=="光环"or self.状态=="脚印"or self.状态=="祥瑞"or self.状态=="定制"or self.状态=="传音",2)
	self.资源组[5]:显示(self.x+2,self.y+65+(5-1)*36,true,1,nil,self.状态=="召唤兽",2)
	self.资源组[6]:显示(self.x+2,self.y+65+(6-1)*36,true,1)
  
   for i=1,6 do
   	self.Icon[i]:显示(self.x+24,self.y+73+(i-1)*36)
   end


	if self.状态 ~= "召唤兽"  then
			self.资源组[61]:显示(self.x+570,self.y+353+3*30,true,1)
		self.资源组[63]:显示(self.x+570,self.y+353+1*30,true,1)
	    self.资源组[64]:显示(self.x+570,self.y+353+2*30,true,1)
		   zts:置颜色(0xFFB0CCF1)
	       zts:置字间距(15)
			for i=1,3 do
				if self.状态 == "银子" then
					zts:显示(self.x+57+305,self.y+353+i*30,sts[i])
			    elseif  self.状态 =="活动积分" or self.状态 =="比武积分"or self.状态 =="副本积分" or self.状态 =="地煞积分"or self.状态 =="知了积分"or self.状态 =="天罡积分"or self.状态 =="单人积分"or self.状态 =="成就积分"or self.状态 =="特殊积分" then
			    	zts:显示(self.x+57+305,self.y+353+i*30,sts2[i])
				else
					zts:显示(self.x+57+305,self.y+353+i*30,sts1[i])
				end
			end

			zts:置字间距(0)
			zts:显示(self.x+145,self.y+396,"商品名称")
			zts:显示(self.x+145,self.y+441,"商品单价")
			local xx =  0
			local yy =  0

			for i=1,self.maximum do
				self.商品[i]:置坐标(self.x + xx * 60.5+(self.maximum==32 and 173 or 354),self.y + yy * 60+110 )
				self.商品[i]:显示(dt,x,y,self.鼠标,nil,nil,nil,nil,nil,true)
				if self.商品[i].物品  then

					 if self.商品[i].焦点 then
						tp.提示:道具行囊(x,y,self.商品[i].物品,true)
						if mouseb(0) then
							if self.道具 == nil then
								self.商品[self.上一次].确定 = false
								self.商品[i].确定 = true
								self.上一次 = i
								self.道具 = self.商品[i].物品
								self.单价 = tostring(self.道具.价格)
								self.数量 = 1
								self.输入框:置可视(true,true)
							else
								if self.上一次 == i then
									self.数量 = self.数量 + 1
								else
									self.商品[self.上一次].确定 = false
									self.商品[i].确定 = true
									self.上一次 = i
									self.道具 = self.商品[i].物品
									self.单价 = tostring(self.道具.价格)
									self.数量 = 1
									self.输入框:置可视(true,true)
								end

							end
							self:置人物形象(self.商品[i].物品,"静立")
							self.输入框:置文本(self.数量)
						end
				       if mouseb(1) then
							if self.道具~= nil then
								if self.上一次 == i   then
									self.数量  = self.数量 - 1
								else
									self.商品[self.上一次].确定 = false
									self.商品[i].确定 = true
									self.上一次 = i
									self.道具 = self.商品[i].物品
									self.单价 = tostring(self.道具.价格)
									self.数量 = 1
									self.输入框:置可视(true,true)
								end

								 self:置人物形象(self.商品[i].物品,"静立")

							end
							self.输入框:置文本(self.数量)
						end
					end
					xx = xx + 1
					if xx == self.maximum/4 then
						xx = 0
						yy = yy + 1
					end
				end
			end
			if self.道具 ~= nil then
				self.输入框:置坐标(self.x,self.y)

				if self.输入框:取文本() == "" then
					self.输入框:置文本(1)
				end
				if self.输入框:取文本()+0 > 99 then
					self.输入框:置文本(99)
				end
				if self.输入框:取文本()+0 < 0 then
					self.输入框:置文本(0)
				end

				zts:置颜色(0xFFFFFFFF):显示(self.x + 217,self.y + 396,self.道具.名称)
				zts:置颜色(0xFFFFFFFF):显示(self.x + 217,self.y +441,self.道具.价格)
				self.数量 = tonumber(self.输入框:取文本())
				zts:置颜色(0xFFFFFFFF):显示(self.x+435,self.y+355+2*30,(self.数量 * self.单价))


			end
			zts:置颜色(tos(self.数据.银子))
			zts:显示(self.x+435,self.y+355+3*30,self.数据.银子)
			self.控件类:更新(dt,x,y)
			if self.输入框._已碰撞 then
				self.焦点 = true
			end
			if self.资源组[103]  then
				self.资源组[103]:更新(dt)
				self.资源组[103]:显示(self.x + 94+150,self.y + 285)
	        end
			if self.状态=="锦衣" or self.状态=="脚印" or self.状态=="祥瑞"or self.状态=="光环"or self.状态=="定制"or self.状态=="传音" then
				if self.资源组[99] ~= nil then
					if self.资源组[101] ~= nil then
						self.资源组[101]:更新(dt)
						self.资源组[101]:显示(self.x + 94+150,self.y + 283)
					end
					if self.资源组[102] ~= nil then
						self.资源组[102]:更新(dt)
						self.资源组[102]:显示(self.x + 134+150,self.y + 283)
					end
					
					self.资源组[99]:更新(dt)
					self.资源组[99]:显示(self.x + 94+150,self.y + 283)
					if self.资源组[100] ~= nil then
						self.资源组[100]:更新(dt)
						self.资源组[100]:显示(self.x + 94+150,self.y + 283)
					end
					self.资源组[71]:显示(self.x + 54+150,self.y + 326,true,1)
					self.资源组[71]:更新(x,y)
					self.资源组[72]:显示(self.x + 54+200,self.y + 326,true,1)
					self.资源组[72]:更新(x,y)

					if self.资源组[71]:事件判断() then
						self.方向 = self.方向 - 1
						if self.方向 < 0 then
							self.方向 = 7
						end
					   self.资源组[99]:置方向(self.方向)
					   if self.资源组[100] then
					       self.资源组[100]:置方向(self.方向)
					   end
					    if self.资源组[102] then
					       self.资源组[102]:置方向(self.方向)
					    end
					   	if self.资源组[103] then
					       self.资源组[103]:置方向(self.方向)
					    end
					elseif self.资源组[72]:事件判断() then 
					     self:置人物形象(self.商品[self.上一次].物品,"行走")
					end
				 tp.影子:显示(self.x + 94+150,self.y + 283)	
				end

			end
			if self.资源组[61]:事件判断() then--购买
			  客户端:发送数据(self.商品[self.上一次].物品.number,2,14,self.数量) 
			end
    else
       	self.资源组[63]:显示(self.x+435,self.y+456,true,1)
	    self.资源组[64]:显示(self.x+515,self.y+456,true,1)
		self.资源组[61]:显示(self.x+595,self.y+456,true,1)
	    if self.资源组[61]:事件判断() then
          客户端:发送数据(self.召唤兽数据[self.选中召唤兽].number,2,14,1)
		end


		 zts:置颜色(0xFFB0CCF1):显示(self.x+195-50,self.y+460,"价 格")
		 zts:置颜色(0xFFB0CCF1):显示(self.x+345-50,self.y+460,"仙 玉")

		 -- zts:置颜色(tos(self.召唤兽数据.银子))
		 -- zts:显示(self.x+435,self.y+355+3*30,self.召唤兽数据.银子)

	  	 local 序号 =0
		for o=1,3 do
			for i=1,4 do
				序号 =序号 +1
			if self.召唤兽数据 [序号]~=nil then
				local jx = self.x+131*(i-1)+195
				local jy =  self.y+((o-1)*133)+157
				  bw:置坐标(jx-65+10,jy-105+10)
	            local xz = bw:检查点(x,y)
				if self.选中召唤兽 ~= 序号 then
					if  xz and not tp.消息栏焦点 and self.鼠标 then
						box(jx-65+10,jy-105+10,jx+67,jy+7,-10790181)
						self.焦点 = true
						self.焦点1 = true
						if mouseb(0) and self.鼠标 and not tp.消息栏焦点   then
			                 self.选中召唤兽 = 序号
			                 
						elseif mouseb(1) and self.鼠标 and not tp.消息栏焦点 then
							tp.窗口.召唤兽查看栏:打开(self.召唤兽数据[序号])
						end
					end
				else
					if  xz and not tp.消息栏焦点 and self.鼠标 then
						self.焦点 = true
						self.焦点1 = true
						if mouseb(1) and self.鼠标 and not tp.消息栏焦点 then
							tp.窗口.召唤兽查看栏:打开(self.召唤兽数据[序号])
						end
					end
						-- box(jx-65+10,jy-105+10,jx+67,jy+7,-10790181)

				end
				zts:置颜色(0xFFFFFFFF)

	
				self.头像组[序号][1]:显示(jx+2,jy+4)
				tp.影子:显示(jx+2,jy+4)

				self.头像组[序号][1]:更新(dt)
				if self.头像组[序号][2] then
					self.头像组[序号][2]:显示(jx+2,jy+4)
				end
				Picture.蓝色小边框:显示(jx-54,jy+4)
				if self.选中召唤兽==序号 then
				  	 zts:置颜色(0xFFFF0000):显示(jx-50,jy+8,"右键可以查看属性")
				  	 Picture.选中金色框:显示(jx-65+10,jy-105+10)
				 else 
				 		zts:置颜色(0xFFFFE304):显示(jx-52,jy+8,"("..self.召唤兽数据[序号].类型..")")
						zts:置颜色(0xFFB0CCF1):显示(jx-14,jy+8,self.召唤兽数据[序号].造型)

				end
			end

		   end
		end
		if self.选中召唤兽 ~= nil then
		  	 zts:置颜色(tos(self.召唤兽数据[self.选中召唤兽].价格))
			 zts:显示(self.x+195,self.y+460,self.召唤兽数据[self.选中召唤兽].价格)

		 end

			zts:置颜色(tos(self.数据.银子)):显示(self.x+345,self.y+460,self.数据.银子)
		
    end
  self.控件类:显示(x,y)
end
function 场景类_商店:置形象(数据)

   local model ={}
		local n = 取模型(数据.造型)
		--print(数据.造型)
		model[1] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
		if 数据.饰品  then
			n = 取模型("饰品_"..数据.造型)
			model[2] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
 			model[2]:置方向(0)
	   	elseif  取召唤兽武器(数据.造型)  then
             local ns=取模型("武器_"..数据.造型)
			  model[2] = tp.资源:载入(ns.战斗资源,"网易WDF动画",ns.待战)
		     model[2]:置方向(0)
		end
		if 数据.染色方案 ~= nil then

		model[1]:置染色(DyeData[数据.造型][1],数据.染色组[1],数据.染色组[2],数据.染色组[3]) 
			model[1]:置方向(0)
		end

		return model
     
end
function 场景类_商店:置人物形象(数据,动作)
	self.资源组[99] = nil
	self.资源组[100] = nil
	self.资源组[101] = nil
	self.资源组[102] = nil
	self.资源组[103] = nil


	if self.状态=="锦衣" or self.状态=="脚印" or self.状态=="祥瑞"or self.状态=="光环"or self.状态=="定制" or self.状态=="传音" then
 			if tp.场景.人物.数据.武器数据.类别 ~= 0 and tp.场景.人物.数据.武器数据.类别 ~= "" and tp.场景.人物.数据.武器数据.类别 ~= nil then
				local v = tp.场景.人物.数据.武器数据.类别
				if tp.场景.人物.数据.武器数据.名称 == "龙鸣寒水" or tp.场景.人物.数据.武器数据.名称 == "非攻" then
					v = "弓弩1"
				end
				local 资源 = 取模型(tp.场景.人物.数据.造型,v)
				self.资源组[99] = tp.资源:载入(资源.资源,"网易WDF动画",资源[动作])
				local m = tp:qfjmc(tp.场景.人物.数据.武器数据.类别,tp.场景.人物.数据.武器数据.等级,tp.场景.人物.数据.武器数据.名称)
				local n = 取模型(m.."_"..tp.场景.人物.数据.造型)
				self.资源组[100] = tp.资源:载入(n.资源,"网易WDF动画",n[动作])
				if tp.场景.人物.数据.武器数据.染色 then
					self.资源组[100]:置染色(tp.场景.人物.数据.武器数据.染色.染色方案,tp.场景.人物.数据.武器数据.染色.染色组.a,tp.场景.人物.数据.武器数据.染色.染色组.b)
				end
				self.资源组[100]:置方向(1)
			else
					local n = 取模型(tp.场景.人物.数据.造型)
					self.资源组[99] = tp.资源:载入(n.资源,"网易WDF动画",n[动作])
			end
        	self.资源组[99]:置染色(DyeData[tp.场景.人物.数据.造型][1],tp.场景.人物.数据.染色.a,tp.场景.人物.数据.染色.b,tp.场景.人物.数据.染色.c) 
			self.资源组[99]:置方向(1)
		 if self.状态== "光环" and 数据 then
					local 资源 = 引擎.取光环(数据.名称)
					self.资源组[101] = tp.资源:载入(资源[4],"网易WDF动画",资源[1])
		elseif self.状态== "脚印" and 数据 then
					local 资源 = 引擎.取脚印(数据.名称)
					self.资源组[102] = tp.资源:载入(资源[2],"网易WDF动画",资源[1])
					self.资源组[102]:置方向(1)
		elseif self.状态== "锦衣" and 数据 then
					local 资源 = 取模型(数据.名称.."_"..tp.场景.人物.数据.造型)
				self.资源组[99] = tp.资源:载入(资源.资源,"网易WDF动画",资源[动作])
				self.资源组[99]:置方向(1)
		elseif  self.状态== '祥瑞' and 数据 then
			  	self.资源组[99] = nil
				self.资源组[100] = nil
				local 资源组 = zqj(tp.场景.人物.数据.造型,数据.名称,"空")
				self.资源组[103] = tp.资源:载入(资源组.坐骑资源,"网易WDF动画",资源组.坐骑站立)
				self.资源组[103]:置方向(0)
		elseif  self.状态== "定制" and 数据 and 数据.锦衣 then
				self.资源组[99] = nil
				self.资源组[100] = nil
				local 资源组 = 取模型(数据.锦衣.."_"..数据.造型)
				self.资源组[99] = tp.资源:载入(资源组.资源,"网易WDF动画",资源组[动作])
				self.资源组[99]:置方向(0)
		elseif  self.状态== '传音' and 数据 then
			local cyModel={
				传音纸鸢="DB87C0E3",
				财源滚滚纸鸢="8E406176",
				鸿雁传书纸鸢="FBA23D04",       
				浪漫玫瑰纸鸢="5D1420D0",
				万丈红尘纸鸢="94C67C3C",
				喜从天降纸鸢="D5F775D1",
				雪花纷飞纸鸢="2410FE72",       
				忽尔今夏纸鸢="84FFACCE",
				金粉世界纸鸢="43DCFD42" }   
			  	self.资源组[99] = nil
				self.资源组[100] = nil
				self.资源组[103] =nil

				self.资源组[103] = tp.资源:载入("MAX.7z","网易WDF动画",cyModel[数据.名称])
				
		end
	end
end



function 场景类_商店:检查点(x,y)
	if self.资源组[0]:是否选中(x,y) or bw:检查点(x,y)  then
		return true
	end
end

function 场景类_商店:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_商店:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end


return 场景类_商店