-- @Author: 作者QQ414628710
-- @Date:   2022-01-15 20:06:44
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-24 02:22:36
--======================================================================--
local 系统类_快捷技能栏 = class()
local tp,zts1
local mouseb = 引擎.鼠标按下
local keyb   = 引擎.按键弹起
local bw     = require("gge包围盒")(全局游戏宽度-376,全局游戏高度-65,262,31)
local gl     = 引擎.置纹理过滤

function 系统类_快捷技能栏:初始化(根)
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	self.格子={}
	self.锁定 =按钮(资源:载入('JM.dll',"网易WDF动画",0xFD4CC022),0,0,4,true)
	self.上 =按钮(资源:载入('JM.dll',"网易WDF动画",0x72ECBE07),0,0,4,true)
	self.下 =按钮(资源:载入('JM.dll',"网易WDF动画",0x5B0263AB),0,0,4,true)
	self.窗口背景 =资源:载入('JM.dll',"网易WDF动画",0x382A3975)
	self.锁定背景 =资源:载入('JM.dll',"网易WDF动画",0x43CE678A)
	for i=1,8 do
		self.格子[i] = 资源:载入('JM.dll',"网易WDF动画",0x0493FA27)
	end
	tp = 根
	zts1 = 根.字体表.描边字体
	self.图片组 = {}
	self.操作员 = nil
	self.数据 = {}
end


function 系统类_快捷技能栏:显示(dt,x,y)

	if not tp.快捷技能显示 then
		return
	end
	local kj = self.数据.快捷技能
   self.锁定背景:显示(全局游戏宽度-376,全局游戏高度-63)
   self.锁定:显示(全局游戏宽度-372,全局游戏高度-59)
   self.窗口背景:显示(全局游戏宽度-349,全局游戏高度-65)
   self.上:显示(全局游戏宽度-346,全局游戏高度-62)
   self.下:显示(全局游戏宽度-346,全局游戏高度-50)

   self.上:更新(x,y)
   self.下:更新(x,y)
   self.锁定:更新(x,y)
   if self.锁定:事件判断() then
   	tp.快捷技能锁定 = not tp.快捷技能锁定
   	 if tp.快捷技能锁定 then
     self.锁定 =tp._按钮(tp.资源:载入('JM.dll',"网易WDF动画",0xFD4CC022),0,0,4,true)
	else
	self.锁定= tp._按钮(tp.资源:载入('JM.dll',"网易WDF动画",0x03BCA9B6),0,0,4,true)
	end
   end
	for i=1,8 do
	    self.格子[i]:显示(全局游戏宽度-363+i*33,全局游戏高度-65)
	    if kj[i] and kj[i].名称 then
	    	local xsd =  SkillData[kj[i].名称]
					kj[i].剩余冷却回合 = xsd.冷却
					kj[i].介绍=xsd.介绍
					kj[i].消耗说明=xsd.消耗
					kj[i].使用条件=xsd.条件
					kj[i].小模型 = tp.资源:载入(xsd.文件,"网易WDF动画",xsd.小图标)
	    	if self.图片组[i] == nil or self.图片组[i].名称 ~= kj[i].名称 then
	    		self.图片组[i] = tp.资源:载入(xsd.文件,"网易WDF动画",xsd.小图标)
			end
		    self.图片组[i]:显示(全局游戏宽度-360+i*33,全局游戏高度-61)
		    if self.图片组[i]:是否选中(x,y) and not tp.选中UI and not tp.消息栏焦点 then
		    	tp.提示:技能(x-130,y,kj[i],nil,true)
		    end
	    end
	    if self.格子[i]:是否选中(x,y) and not tp.选中UI and not tp.消息栏焦点 and not tp.战斗中 and tp.快捷技能锁定 == false then
		    if mouseb(0) and tp.抓取技能 ~= nil then
		    	if tp.抓取技能ID ==nil then
		    		客户端:发送数据(i,tp.抓取技能.主技能id, 30,tp.抓取技能.包含技能id)
		    		tp.抓取技能 =nil
		    	else
                客户端:发送数据(i,tp.抓取技能.选中技能, 30,tp.抓取技能ID)
                tp.抓取技能ID =nil
		    	tp.抓取技能 =nil
		       end
		    elseif mouseb(0) and tp.抓取技能 == nil and self.图片组[i]~= nil then
		    	tp.抓取技能 =kj[i]
                客户端:发送数据(i,2, 30,"空")
		    end
	    end
	    tp.字体表.描边字体_:置颜色(0xFFFFFFFF):显示(全局游戏宽度-355+i*33,全局游戏高度-81,"F"..i)
	end
	local ax = self:快捷焦点按下()
	if ax and kj[ax] ~= nil and kj[ax].名称 ~= nil and not tp.选中UI and not tp.消息栏焦点 and not tp.战斗中 then
		if (kj[ax].种类 == 0) then
         客户端:发送数据(6321, 57, 40, "61", 1)
        elseif (kj[ax].种类 == 12) then

		  客户端:发送数据(kj[ax].主技能id,11,5,kj[ax].包含技能id)
		end
    elseif  ax and kj[ax] ~= nil and kj[ax].名称 ~= nil and not tp.选中UI and not tp.消息栏焦点 and kj[ax].种类 ~= 0 and kj[ax].种类 ~= 12 and tp.战斗中 then
    	     tp.场景.战斗:快捷技能(kj[ax].名称)
	end

end
function 系统类_快捷技能栏:快捷焦点按下()
	if keyb(KEY.F1)   then
		return 1
	elseif keyb(KEY.F2) then
		return 2
	elseif keyb(KEY.F3) then
		return 3
	elseif keyb(KEY.F4) then
		return 4
	elseif keyb(KEY.F5) then
		return 5
	elseif keyb(KEY.F6) then
		return 6
	elseif keyb(KEY.F7) then
		return 7
	elseif keyb(KEY.F8) then
		return 8
	end
end
function 系统类_快捷技能栏:检查点(x,y)
	return tp.快捷技能显示 and bw:检查点(x,y)
end

return 系统类_快捷技能栏