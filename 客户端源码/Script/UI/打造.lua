--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:48
--======================================================================--
local 场景类_打造 = class()

local floor = math.floor
local insert = table.insert
local random = 引擎.取随机整数
local mouseb = 引擎.鼠标弹起
local zts,zts1,tp
local remove = table.remove
local sts = {"所需资金:","现有资金:","所需体力:","现有体力:"}

function 场景类_打造:初始化(根)
	self.ID = 18
	self.x = 265+(全局游戏宽度-800)/2
	self.y = 100
	self.xx = 0
	self.yy = 0
	self.注释 = "打造"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应



	self.资源组 = {
		[0] = 自适应.创建(99,1,461,325,3,9),

		[2] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
		[3] = 按钮.创建(自适应.创建(100,4,55,26,1,3),0,0,4,true,true,"打造"),
		[4] = 按钮.创建(自适应.创建(100,4,55,26,1,3),0,0,4,true,true,"镶嵌"),
		[5] = 按钮.创建(自适应.创建(100,4,55,26,1,3),0,0,4,true,true,"合成"),
		[6] = 按钮.创建(自适应.创建(100,4,55,26,1,3),0,0,4,true,true,"修理"),
		[7] = 按钮.创建(自适应.创建(100,4,55,26,1,3),0,0,4,true,true,"熔炼"),
		[8] = 按钮.创建(自适应.创建(100,4,55,26,1,3),0,0,4,true,true,"分解"),
		[9] = 按钮.创建(自适应.创建(100,4,55,26,1,3),0,0,4,true,true,"神器"),
		[10] = 自适应.创建(93,1,94,22,1,3),
		[11] = 按钮.创建(自适应.创建(102,4,55,22,1,3),0,0,4,true,true,"打造"),
		[12] = 按钮.创建(自适应.创建(102,4,55,22,1,3),0,0,4,true,true,"取消"),


	}

	local 格子 = 根._物品格子
	self.物品 = {}
	for i=1,20 do
		self.物品[i] = 格子(0,0,i1,"打造")
	end
	for n=2,9 do
		self.资源组[n]:绑定窗口_(18)
		self.资源组[n]:置偏移(5,1)
	end
	self.材料 = {}
	self.材料[1] = 格子(self.x+4,self.y,1,"打造材料")
	self.材料[2] = 格子(self.x+4,self.y,2,"打造材料")
	self.总价 = 0
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.华康字体
	zts1 = tp.字体表.华康字体
	self.选中材料 = {}
	for i=1,2 do
		self.选中材料[i]=0
	end
 self.状态 ="打造"


end


	-- self.资源组[2]:更新(x,y)
	-- self.资源组[3]:更新(x,y)
	-- self.资源组[4]:更新(x,y)
	-- self.资源组[5]:更新(x,y)
	-- self.资源组[6]:更新(x,y)

	-- self.资源组[3]:更新(x,y,self.主召唤兽 ~= nil and self.副召唤兽 ~= nil)
	-- self.资源组[4]:更新(x,y,((self.主召唤兽 ~= nil and self.副召唤兽 == nil) or (self.副召唤兽 ~= nil and self.主召唤兽 == nil)))
	-- self.资源组[5]:更新(x,y,((self.主召唤兽 ~= nil and self.副召唤兽 == nil) or (self.副召唤兽 ~= nil and self.主召唤兽 == nil)))
	-- self.资源组[6]:更新(x,y,((self.主召唤兽 ~= nil and self.副召唤兽 == nil) or (self.副召唤兽 ~= nil and self.主召唤兽 == nil)))


function 场景类_打造:刷新(数据)
		for i=1,20 do
		self.物品[i]:置物品(数据[i])
		end
		self.数据=数据
		self.状态=self.数据.状态
		self.资源组[11]:置文字(self.状态)
end
function 场景类_打造:打开(数据)
	if self.可视 then
		for i=1,20 do
		self.物品[i]:置物品(nil)
		end
		self.材料[1]:置物品(nil)
		self.材料[2]:置物品(nil)
		for i=1,2 do
		self.选中材料[i]=0
		end
		self.可视 = false
	else
		if  self.x > 全局游戏宽度 then
		self.x = 265+(全局游戏宽度-800)/2
		end
		for i=1,20 do
		self.物品[i]:置物品(数据[i])
		end
		self.数据=数据
		self.状态=数据.状态
		self.资源组[11]:置文字(self.状态)
		self.更新="包裹"
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end
function 场景类_打造:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[12]:更新(x,y)
	self.资源组[11]:更新(x,y,self.材料[1].物品 ~= nil and self.材料[2].物品 ~= nil)
	if self.鼠标 then
		if self.资源组[2]:事件判断()or  self.资源组[12]:事件判断() then
			self:打开()
			return false
		elseif self.资源组[8]:事件判断() then
			客户端:发送数据(1, 197, 13,"包裹")
		elseif self.资源组[11]:事件判断() then
			if self.状态=="打造" then
				if (self.材料[1].物品.类型 =="打造" or self.材料[1].物品.类型 =="元身" )and  (self.材料[2].物品.类型 == "打造" or self.材料[2].物品.类型 =="元身")and self.材料[1].物品.名称 ~= self.材料[2].物品.名称  then
					if  self.材料[1].物品.名称=="上古锻造图策" or self.材料[2].物品.名称=="上古锻造图策" then
					    客户端:发送数据(self.选中材料[1],self.选中材料[2],17,"普通打造","打造")
							for i=1,2 do
							self.选中材料[i] =0
							end
							self:打开()
					 else
					 	  tp.窗口.文本栏:载入("#H/强化打造的属性范围会高过普通打造，但是消耗的银子会是普通打造的三倍，且还会消耗强化石。#R等级低于60级的书铁无法进行强化打造#H/，请选择你要打造的方式：","打造",true)
					end
				else
					tp.提示:写入("#Y/只有同时放入#R/制造指南书#Y/和#R/百炼精铁#Y/才可以进行打造操作")
				end
			elseif self.状态 == "镶嵌" or self.状态 == "合成" or self.状态 == "修理" or self.状态 == "熔炼"  then
				客户端:发送数据(self.选中材料[1],self.选中材料[2],17,"强化打造",self.状态)
				for i=1,2 do
					self.材料[i]:置物品(nil)
					self.选中材料[i]=0
				end


			end
		end
		for i=3,7 do
			if self.资源组[i]:事件判断() then
				self.状态 = self.资源组[i]:取文字()
				self.资源组[11]:置文字(self.状态)
			end
		end
	end
	self.资源组[0]:显示(self.x,self.y)
	Picture.标题:显示(self.x+130,self.y)

	zts1:置颜色(0xFFFFFFFF):显示(self.x+200,self.y+3,"制 造")
	self.资源组[2]:显示(self.x + 436,self.y + 6)
	self.资源组[11]:显示(self.x + 35,self.y + 290,true,1)
    self.资源组[12]:显示(self.x + 115,self.y + 290,true,1)
	Picture.物品格子背景:显示(self.x+194,self.y+101)
	Picture.物品格子:显示(self.x+36,self.y+105)
	Picture.物品格子:显示(self.x+116,self.y+105)
    self.资源组[10]:显示(self.x+57,self.y+72)
    zts:置颜色(0xFFFFFFFF):显示(self.x+200,self.y+72,"需要材料:")

    if self.状态=="打造" then
         zts:置颜色(0xFFFFFF00):显示(self.x+264,self.y+72,"制造书,上古锻造图策,元身等")
         zts:置颜色(-16777216):显示(self.x+74,self.y+77,"装备打造")
	elseif self.状态=="镶嵌" then
		zts:置颜色(0xFFFFFF00):显示(self.x+264,self.y+72,"人物宝宝装备,宝石")
		zts:置颜色(-16777216):显示(self.x+74,self.y+77,"宝石镶嵌")
	elseif self.状态=="合成" then
		zts:置颜色(0xFFFFFF00):显示(self.x+264,self.y+72,"宝石,召唤兽宝石,钟灵石")
		zts:置颜色(-16777216):显示(self.x+74,self.y+77,"宝石合成")
	elseif self.状态=="修理" then
		zts:置颜色(0xFFFFFF00):显示(self.x+264,self.y+72,"人物装备,珍珠")
		zts:置颜色(-16777216):显示(self.x+74,self.y+77,"装备修理")
	elseif self.状态=="熔炼" then
		zts:置颜色(0xFFFFFF00):显示(self.x+264,self.y+72,"人物装备,钨金")
		zts:置颜色(-16777216):显示(self.x+74,self.y+77,"装备熔炼")
    end

  	for i=3,9 do
  		self.资源组[i]:更新(x,y,self.状态~=self.资源组[i]:取文字())
  	    self.资源组[i]:显示(self.x-170+i*62,self.y+35,true,1,nil,self.状态==self.资源组[i]:取文字(),2)
  	end
	for i=1,4 do
		self.资源组[10]:显示(self.x+86,self.y+140+i*30)
		zts:置颜色(0xFFFFFFFF):显示(self.x+16,self.y+140+i*30,sts[i])
	end
	zts:置颜色(引擎.取金钱颜色(self.数据.银两+0)):显示(self.x + 90,self.y + 203,self.数据.银两)
    zts:置颜色(0xFF000000):显示(self.x + 90,self.y + 264,self.数据.体力)
	local is = 0
	for h=1,4 do
		for l=1,5 do
			is = is + 1
			self.物品[is]:置坐标(l * 51 +145+ self.x,h * 51 + self.y + 50)
			self.物品[is]:显示(dt,x,y,self.鼠标,{"武器","宝石","打造","衣服","项链","头盔","鞋子","腰带","元身","召唤兽装备","手镯","佩饰","戒指","耳饰"},nil,nil,nil,{"碎石锤","超级碎石锤","钟灵石"})
			if self.物品[is].物品 ~= nil and self.物品[is].焦点 then
				tp.提示:道具行囊(x,y,self.物品[is].物品)
				if self.物品[is].事件 and (self.物品[is].物品.类型 == "手镯" or self.物品[is].物品.类型 == "佩饰" or self.物品[is].物品.类型 == "戒指" or self.物品[is].物品.名称 == "钟灵石"or self.物品[is].物品.类型 == "耳饰" or self.物品[is].物品.类型 == "武器" or self.物品[is].物品.类型 == "头盔"or self.物品[is].物品.类型 == "衣服"or self.物品[is].物品.类型 == "召唤兽装备"or self.物品[is].物品.类型 == "项链" or self.物品[is].物品.类型 == "鞋子" or self.物品[is].物品.类型 == "腰带" or self.物品[is].物品.类型 == "衣服" or self.物品[is].物品.类型 =="打造" or self.物品[is].物品.类型 =="元身" or self.物品[is].物品.类型 == "宝石" or self.物品[is].物品.名称 == "碎石锤" or self.物品[is].物品.名称 == "超级碎石锤")  and self.鼠标 then
					if self.材料[1].物品 == nil and self.材料[2].物品 == nil then
						self.材料[1]:置物品(self.物品[is].物品)
						self.选中材料[1] =is
						self.物品[is]:置物品(nil)
					elseif self.材料[1].物品 ~= nil and self.材料[2].物品 == nil then
						self.选中材料[2] =is
						self.材料[2]:置物品(self.物品[is].物品)
						self.物品[is]:置物品(nil)
					elseif self.材料[1].物品 == nil and self.材料[2].物品 ~= nil then
						self.材料[1]:置物品(self.物品[is].物品)
						self.选中材料[1] =is
						self.物品[is]:置物品(nil)
					elseif self.材料[1].物品 ~= nil and self.材料[2].物品 ~= nil then
						if self.材料[1].物品.分类 == self.物品[is].物品.分类 and self.材料[1].物品.子类 == self.物品[is].物品.子类 then
							local cl1 = self.材料[1].物品
							self.材料[1]:置物品(self.物品[is].物品)
							self.物品[is]:置物品(cl1)
						end
						if self.材料[2].物品.分类 == self.物品[is].物品.分类 and self.材料[2].物品.子类 == self.物品[is].物品.子类 then
							local cl2 = self.材料[2].物品
							self.选中材料[2] =is
							self.材料[2]:置物品(self.物品[is].物品)

							self.物品[is]:置物品(cl2)
						end
					end

				end
			end
			if self.物品[is].焦点 then
				self.焦点 = true
			end
		end
	end
	for ns=1,2 do
		self.材料[ns]:置坐标(self.x -40+ (ns * 80),self.y+105)
		self.材料[ns]:显示(dt,x,y,self.鼠标,false)
		if self.材料[ns].物品 ~= nil and self.材料[ns].焦点 then
			tp.提示:道具行囊(x,y,self.材料[ns].物品)
			if mouseb(0) and self.鼠标 then
					self.物品[self.选中材料[ns]]:置物品(self.材料[ns].物品)
					self.材料[ns]:置物品(nil)
					self.选中材料[ns]=0
			end
		end
		if self.材料[ns].焦点 then
			self.焦点 = true
		end
	end
	if self.材料[1].物品 ~= nil and self.材料[2].物品 ~= nil then
		local jq = 0
		local tl = 0
	   if self.状态 == "打造" then
		if self.材料[1].物品.名称=="上古锻造图策" or self.材料[2].物品.名称=="上古锻造图策" then
			tl = floor(self.材料[1].物品.等级 * 2)
			jq= floor(self.材料[1].物品.等级 * 50000)
        else
			tl = floor(self.材料[1].物品.等级 * 2)
			jq= floor(self.材料[1].物品.等级 * self.材料[1].物品.等级*100)
		end
		zts:置颜色(-16777216)
		zts:显示(self.x + 90,self.y + 175,jq)
		zts:显示(self.x + 90,self.y + 233,tl)
       end
	end

end

function 场景类_打造:检查点(x,y)
	local n = false
	if self.可视 and self.资源组[0]:是否选中(x,y)  then
		n  = true
	end
	return n
end

function 场景类_打造:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.可视 and self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_打造:开始移动(x,y)
	if self.可视 and self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end


return 场景类_打造










