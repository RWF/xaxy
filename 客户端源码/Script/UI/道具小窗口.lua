--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--
local 场景类_道具小窗口 = class(窗口逻辑)
local tp,zts,zts1
local floor = math.floor
local tonumber = tonumber
local tostring = tostring
local insert = table.insert


function 场景类_道具小窗口:初始化(根)
	self.ID = 123
	self.x = 280+(全局游戏宽度-800)/2
	self.y = 86
	self.xx = 0
	self.yy = 0
	self.注释 = "道具小窗口"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 自适应.创建(99,1,272,260,3,9),
		[2] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
        [3] = 按钮.创建(自适应.创建(100,4,100,26,1,3),0,0,4,true,true,"点化"),

	}
   self.资源组[2]:绑定窗口_(123)
   self.资源组[3]:绑定窗口_(123)

	self.物品 = {}
	local 格子 = 根._物品格子
	self.材料 = {格子(),格子()}
	self.选中材料 = {0,0}
	for i=1,20 do
		self.物品[i] = 格子(0,0,i,"道具小窗口")
	end
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.华康字体
	zts1 = tp.字体表.华康字体
end
function 场景类_道具小窗口:刷新(数据)
		for i=1,20 do
		self.物品[i]:置物品(数据[i])
		end
		self.数据=数据
		self.道具 = nil
		if self.上一次 ~= nil then
			self.物品[self.上一次].确定 = false
		end
		self.上一次 = nil
end
function 场景类_道具小窗口:打开(数据,格子,数据格子)
	if self.可视 then
		self.道具 = nil
		self.可视 = false
		if self.上一次 ~= nil then
			self.物品[self.上一次].确定 = false
		end
		self.上一次 = nil
		self.资源组[1]=tp._自适应(99,1,272,260,3,9)
	else
		if  self.x > 全局游戏宽度 then
			self.x = 280+(全局游戏宽度-800)/2
	    end
	    if 格子 then	
			self.物品=数据
			self.状态 =self.物品[格子].物品.名称
			self.格子=格子
			self.数据格子 = 数据格子
	    else 
	    	for i=1,20 do
			self.物品[i]:置物品(数据[i])
			end
			self.状态=数据.状态
			self.格子 = 数据.格子
	    end
   
		if self.状态 =="灵箓" or self.状态 =="淬灵石" or self.状态 =="钟灵石" then
			self.资源组[1]= self.状态 =="淬灵石" and  Picture.特性吸收界面 or	Picture.灵饰点化 
			self.资源组[3]:置文字(self.状态 =="淬灵石" and "  吸  取"  or"  点  化" )
			self.材料[1]:置物品(self.物品[格子].物品)
			self.物品[格子]:置物品(nil)
			self.选中材料[1]=格子
			self.材料[2]:置物品(nil)
			self.选中材料[2]=0

		end
		if self.上一次 ~= nil then
			self.物品[self.上一次].确定 = false
		end
	
		self.上一次 = nil
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_道具小窗口:显示(dt,x,y)
	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:更新(x,y)
	if self.鼠标 then
		if self.资源组[2]:事件判断() then
			self:打开()
		 elseif self.资源组[3]:事件判断() then
		 	客户端:发送数据(self.选中材料[1],self.选中材料[2],16)
		end
	end
	if self.状态 =="灵箓" or self.状态 =="淬灵石" or self.状态 =="钟灵石" then
		 self.资源组[2]:显示(self.x+300,self.y+3)
		 self.资源组[3]:更新(x,y,self.材料[1].物品 ~= nil and self.材料[2].物品 ~= nil)
		 if self.状态 =="淬灵石" then
		 	 self.资源组[3]:显示(self.x+210,self.y+430)
		 else 
		 	 self.资源组[3]:显示(self.x+120,self.y+415)
		 end


		    local xx = 0
			local yy = 0
			for i=1,20 do
				local jx = xx * 56 + 24
				local jy = yy * 56 + 37
				self.物品[i]:置坐标(self.x + jx,self.y + jy)
				self.物品[i]:显示(dt,x,y,self.鼠标)
				self.物品[i]:显示(dt,x,y,self.鼠标,{"手镯","佩饰","戒指","耳饰"},nil,nil,nil,{self.状态})
				if self.物品[i].焦点 and self.物品[i].物品 ~= nil then
					tp.提示:道具行囊(x,y,self.物品[i].物品)
					if self.物品[i].事件 and (self.物品[i].物品.类型 == "手镯"or self.物品[i].物品.名称 ==self.状态 or self.物品[i].物品.类型 == "佩饰"or self.物品[i].物品.类型 =="戒指"or self.物品[i].物品.类型 =="耳饰") then    
                        if self.物品[i].物品.名称 ==self.状态 then
                        	 self.材料[1]:置物品(self.物品[i].物品)
							 self.物品[i]:置物品(nil)
						     self.选中材料[1]=i
                        else 
                        	  self.材料[2]:置物品(self.物品[i].物品)
							 self.物品[i]:置物品(nil)
						     self.选中材料[2]=i
                        end
						 
				 
					end
				end

				xx = xx + 1
				if xx == 5 then
					xx = 0
					yy = yy + 1
				end
			end

			 for ns=1,2 do
			 	if self.状态 =="淬灵石" then
			 		self.材料[ns]:置坐标(self.x +35+ ((ns-1) * 203),self.y+313)
						if  self.材料[2].物品 ~= nil then
						zts:显示(self.x + 90,self.y + 400,self.材料[2].物品.等级*10000)
						zts:显示(self.x + 90,self.y + 433,self.材料[2].物品.等级*0.5)
						end
			 	else 
			 		 self.材料[ns]:置坐标(self.x +50+ ((ns-1) * 170),self.y+316)
			 	end
				
				self.材料[ns]:显示(dt,x,y,self.鼠标,false)
				if self.材料[ns].物品 ~= nil and self.材料[ns].焦点 then
					tp.提示:道具行囊(x,y,self.材料[ns].物品)
					if  self.材料[ns].事件 then
							self.物品[self.选中材料[ns]]:置物品(self.材料[ns].物品)
							self.材料[ns]:置物品(nil)
							self.选中材料[ns]=0
					end
				end
				if self.材料[ns].焦点 then
					self.焦点 = true
				end
			end
			
			



	else 
		
		Picture.标题:显示(self.x+self.资源组[1].宽度/2-90,self.y)
		zts1:置颜色(0xFFFFFFFF):显示(self.x+100,self.y+3,self.状态)
		Picture.物品格子背景:显示(self.x+9,self.y+31)
		zts:置颜色(4294967295)
		self.资源组[2]:显示(self.x+247,self.y+3)
		    local xx = 0
			local yy = 0
			for i=1,20 do
				local jx = xx * 51 + 10
				local jy = yy * 51 + 30
				self.物品[i]:置坐标(self.x + jx,self.y + jy)
				self.物品[i]:显示(dt,x,y,self.鼠标)
				if self.物品[i].焦点 and self.物品[i].物品 ~= nil then
					tp.提示:道具行囊(x,y,self.物品[i].物品)
					if self.物品[i].事件 then    
		               if self.状态 =="兵器谱" or self.状态 =="嗜血" or self.状态 =="神兵护法" or  self.状态 =="轻如鸿毛" or  self.状态 =="浩然正气"or
		                self.状态 == "魔王护持"or  self.状态 == "莲华妙法" or  self.状态 =="一气化三清" or  self.状态 == "盘丝舞" or  self.状态 == "神力无穷"or
		                 self.状态 ==  "尸气漫天"or  self.状态 == "拈花妙指"or  self.状态 == "龙附" or  self.状态 =="神木呓语" or  self.状态 =="穿云破空" or
		                 self.状态 =="元阳护体" or self.状态 == "堪察令" or self.状态 == "担山赶月"or self.状态 == "鬼斧神工"or self.状态 == "幽影灵魄" then
		                 客户端:发送数据(i,211,13,self.状态,self.格子)
		                elseif self.状态 =="选择装备" then
		                	 客户端:发送数据(i,93,13,self.状态,self.格子)
		                elseif self.状态 =="选择武器" then
		                	 客户端:发送数据(i,94,13,self.状态,self.格子)
		                else
		               	  客户端:发送数据(self.格子+self.数据格子,i+self.数据格子,16)
		               end
		               self:打开()
					end
				end

				xx = xx + 1
				if xx == 5 then
					xx = 0
					yy = yy + 1
				end
			end
	end


end




return 场景类_道具小窗口