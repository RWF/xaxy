--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:47
--======================================================================--
local 帮派成员类 = class()
local tp,zts,zts1
local insert = table.insert
function 帮派成员类:初始化(根)
	self.ID = 142
	self.x = 0+(全局游戏宽度-800)/2
	self.y = 0
	self.xx = 0
	self.yy = 0
	self.注释 = "帮派成员类"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.背景 = 根.资源:载入('JM.dll',"网易WDF动画",0x3346A898)
	self.取消 = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"取消")
	self.取消:绑定窗口_(142)
	self.增加 = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true)
	self.增加:绑定窗口_(142)
	self.减少 = 按钮.创建(自适应.创建(20,4,18,19,4,3),0,0,4,true,true)
	self.减少:绑定窗口_(142)
	self.允许加入 = 按钮.创建(自适应.创建(16,4,75,22,1,3),0,0,4,true,true,"允许加入")
	self.允许加入:绑定窗口_(142)
	self.拒绝申请 =  按钮.创建(自适应.创建(16,4,75,22,1,3),0,0,4,true,true,"拒绝申请")
	self.拒绝申请:绑定窗口_(142)
	self.清空列表 =  按钮.创建(自适应.创建(16,4,75,22,1,3),0,0,4,true,true,"清空列表")
	self.清空列表:绑定窗口_(142)
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.描边字体
end

function 帮派成员类:打开(数据)
	if  self.可视 then
		self.可视 = false
		self.显示数据 = {}
		self.显示序列 = 1
		self.选中编号 = 0
	else
						                    if  self.x > 全局游戏宽度 then
self.x = 0+(全局游戏宽度-800)/2
    end
	insert(tp.窗口_,self)
	self.显示数据 = {}
	self.显示数据 = {
		数据.名单[数据.id]
	}
	self.可视 = true
	self.显示序列 = 1
	self.选中编号 = 0
	self.成员显示 = {}
	for n, v in pairs(数据.名单) do
		if n ~= 数据.id then
			self.显示数据[#self.显示数据 + 1] = 数据.名单[n]
		end
	end

	for n = 1, #self.显示数据, 1 do
		if n <= 9 then
			self.显示位置 = n
		else
			self.显示位置 = n - math.floor(n / 10) * 10
		end

		self.显示数据[n].包围盒 =  require("gge包围盒")(400, 15, 64, 18)

		self.显示数据[n].包围盒:置坐标(self.x+170, self.y+140 + self.显示位置 * 23)
		--self.显示数据[n].包围盒:更新宽高(400, 15)
	end

		tp.运行时间 = tp.运行时间 + 3
		self.窗口时间 = tp.运行时间
		self.可视 = true
	end
end

function 帮派成员类:检查点(x,y)
	if self.背景:是否选中(x,y)  then
		return true
	end
end

function 帮派成员类:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	self.窗口时间 = tp.运行时间
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end


function 帮派成员类:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end



function 帮派成员类:显示(dt,x, y)
	self.减少:更新(x,y)
	self.增加:更新(x,y)
	self.允许加入:更新(x,y)
	self.拒绝申请:更新(x,y)
	self.清空列表:更新(x,y)
	self.取消:更新(x,y)
	self.焦点=false
	if self.减少:事件判断() then
		if self.显示序列 <= 1 then
			-- Nothing
		else
			self.显示序列 = self.显示序列 - 1
		end
	elseif self.增加:事件判断() then
		if self.显示序列 * 9 > #self.显示数据 then
			-- Nothing
		else
			self.显示序列 = self.显示序列 + 1
		end
	elseif self.清空列表:事件判断() then
		客户端:发送数据(1, 66, 13, "66")
	elseif self.拒绝申请:事件判断() then
		if self.选中编号 == 0 then
			tp.提示:写入("#y/请先选中一个玩家")
        else
			客户端:发送数据(self.显示数据[self.选中编号].id, 67, 13, "16")
		end


	elseif self.允许加入:事件判断() then
		if self.选中编号 == 0 then
			tp.提示:写入("#y/请先选中一个玩家")

			return 0
		end

		客户端:发送数据(self.显示数据[self.选中编号].id, 68, 13, "16")
	elseif self.取消:事件判断() then
		self.本类开关 = false
	end

	self.背景:显示(self.x+150, self.y+100)
	zts1:显示(self.x+355, self.y+104, "帮派成员名单")
	self.减少:显示(self.x+640,self.y+150)
	self.增加:显示(self.x+640,self.y+345)
	self.取消:显示(self.x+335,self.y+360,true)
	zts:置颜色(黑色)
	zts:显示(self.x+220, self.y+155, "离线时间")
	zts:显示(self.x+360, self.y+155, "名称")
	zts:显示(self.x+450, self.y+155, "职务")
	zts:显示(self.x+520, self.y+155, "帮贡")

	for n = 1, 9, 1 do
		self.显示编号 = (self.显示序列 - 1) * 9 + n

		if self.显示数据[self.显示编号] ~= nil then
			if self.选中编号 == self.显示编号 then
				zts:置颜色(0xFF00FF80)
			elseif self.显示编号 == 1 then
				zts:置颜色(0xFFFFFF80)
			else
				zts:置颜色(0xFF0080FF)
			end
			zts:显示(self.x+170, self.y+155 + n * 23, 测试时间(self.显示数据[self.显示编号].离线时间))
			zts:显示(self.x+360, self.y+155 + n * 23, self.显示数据[self.显示编号].名称)
			zts:显示(self.x+450, self.y+155 + n * 23, self.显示数据[self.显示编号].职务)
			zts:显示(self.x+520, self.y+155 + n * 23, self.显示数据[self.显示编号].帮贡.当前 .. "/" .. self.显示数据[self.显示编号].帮贡.获得)

			if self.显示数据[self.显示编号].包围盒:检查点(x,y) and 引擎.鼠标弹起(0) then
				if self.选中编号 == self.显示编号 then
					self.选中编号 = 0
				else
					self.选中编号 = self.显示编号
				end
			end
		end
	end
end

return 帮派成员类
