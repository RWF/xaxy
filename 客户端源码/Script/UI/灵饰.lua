--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--
local 场景类_灵饰 = class()

local floor = math.floor
local tp,zts1
local insert = table.insert


function 场景类_灵饰:初始化(根)
	self.ID = 36
	self.xx = 0
	self.x= 312
	self.y=1
	self.yy = 0
	self.注释 = "灵饰"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 自适应.创建(0,1,129,201,3,9),
		[2] = 按钮(资源:载入('JM.dll',"网易WDF动画",0xF11233BB),0,0,4,true,true),
		[3] = 自适应.创建(1,1,99,18,1,3,nil,18),
		[4] = 资源:载入('JM.dll',"网易WDF动画",0x841CBD61),
		[5] = 资源:载入('JM.dll',"网易WDF动画",0x9912E8B9),
		[6] = 资源:载入('JM.dll',"网易WDF动画",0x5795605E),
		[7] = 资源:载入('JM.dll',"网易WDF动画",0xEBD8985D),
		[8] = 资源:载入('JM.dll',"网易WDF动画",0x21246323),
	}
	self.资源组[2]:绑定窗口_(36)
	self.物品 = {}
	local 格子 = 根._物品格子
	for i=31,35 do
		self.物品[i] = 格子(0,0,i,"灵饰",根.底图)
	end
	tp = 根
	zts1 = tp.字体表.描边字体
	self.窗口时间 = 0
end

function 场景类_灵饰:打开(数据)
	if self.可视 then
		for i=31,35 do
			self.物品[i]:置物品(nil)
		end
		self.可视 = false
		self.鼠标=false
	else
		insert(tp.窗口_,self)
		self.数据 = 数据
		for i=31,35 do
			if self.数据[i] ~= nil and self.数据[i] ~= 0 then
				self.物品[i]:置物品(self.数据[i])
			end
		end
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end



function 场景类_灵饰:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	if self.资源组[2]:事件判断() then
		self:打开()
		return false
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[3]:显示(self.x+6,self.y+3)
	self.资源组[2]:显示(self.x+106,self.y+6)
	Picture.窗口标题背景_:置区域(0,0,40,16)
	Picture.窗口标题背景_:显示(self.x+37,self.y+3)
	zts1:置字间距(3)
	zts1:显示(self.x+41,self.y+3,"灵饰")
	zts1:置字间距(0)
	local xx = 0
	local yy = 1
	local xs = 0
	for i=31,35 do
		if i == 5 then
			xs = 27
		end
		xx = xx + 1
		self.资源组[3+i-30]:显示(self.x + xx * 58-49+xs,self.y + yy * 57-31)
		self.物品[i]:置坐标(self.x + xx * 58-47+xs,self.y + yy * 57-31,nil,nil,4,-1)
		self.物品[i]:显示(dt,x,y,self.鼠标,nil,3)
		if self.物品[i].物品 ~= nil and self.物品[i].焦点 then
			tp.提示:道具行囊(x,y,self.物品[i].物品,true)
		end
		if xx >= 2 then
			xx = 0
			yy = yy + 1
		end
		if self.物品[i].事件 then
			if tp.抓取物品 ~= nil  and self.物品[i].焦点 then
				if tp.抓取物品注释 =="灵饰" then
							self.物品[tp.抓取物品ID].确定 = false
							tp.抓取物品 = nil
							tp.抓取物品ID = nil
							tp.抓取物品注释 = nil
					客户端:发送数据(2, 74, 13, "9", 1)
				else
					if self:可装备(tp.抓取物品,i) then
					客户端:发送数据(tp.抓取物品ID+(tp.窗口.道具行囊.数据.Pages-1)*20,75,13,tp.窗口.道具行囊.更新)
					self.物品[i].确定 = false
					tp.窗口.道具行囊.物品[tp.抓取物品ID].确定 = false
					tp.抓取物品 = nil
					tp.抓取物品ID = nil
					tp.抓取物品注释 = nil
					end
				end

			elseif tp.抓取物品 == nil and self.物品[i].物品 ~= nil and self.物品[i].焦点 then
				tp.抓取物品 = self.物品[i].物品
				tp.抓取物品ID = i
				tp.抓取物品注释 = self.物品[i].注释
				self.物品[i].确定 = true
				self.物品[i]:置物品(nil)
			end
		elseif self.物品[i].右键 then
			if  self.物品[i].物品 ~= nil then

                   客户端:发送数据(i,59,13,tp.窗口.道具行囊.更新)
			end
		end
	end
end

function 场景类_灵饰:可装备(wp,i)
	if wp.类型 == "耳饰" and i == 31 then
		return true
    elseif wp.类型 =="佩饰" and i == 32 then
    return true
    elseif wp.类型 =="戒指" and i == 33 then
     return true
    elseif wp.类型 =="手镯" and i == 34 then
     return true
    elseif wp.类型 =="定制锦衣" and i == 35 then
     return true
    else
		tp.提示:写入("#Y/这个物品不可以装备")
		return false
	end

end

function 场景类_灵饰:检查点(x,y)
	if self.资源组[1]:是否选中(x,y)  then
		return true
	end
end

function 场景类_灵饰:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_灵饰:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_灵饰