--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--
local 场景类_符石 = class()
local tp,zts,zts1
local insert = table.insert
local xxx = 0
local yyy = 0
local sts = {"单价","数量","总额","现金"}
local tos = 引擎.取金钱颜色
local mouseb = 引擎.鼠标按下
function 场景类_符石:初始化(根)
	self.ID = 57
	self.x = 224+(全局游戏宽度-800)/2
	self.y = 80
	self.xx = 0
	self.yy = 0
	self.注释 = "商店"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.焦点1 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[0] = Picture.符石镶嵌界面,--背景
		[1] = 资源:载入('JM.dll',"网易WDF动画",0x0B9E4143),--背景1
		[2] = 资源:载入('JM.dll',"网易WDF动画",0xB0C4E507),--背景2
		[3] = 资源:载入('JM.dll',"网易WDF动画",0x685ABA69),--背景3
		[4] = 资源:载入('JM.dll',"网易WDF动画",0x431EEEC5),--背景4
		[5] = 资源:载入('JM.dll',"网易WDF动画",0xC53B87D1),--背景5
		[6] = 按钮.创建(自适应.创建(103,4,55,22,1,3),0,0,4,true,true,"镶嵌"),
		[7] = 按钮.创建(自适应.创建(103,4,55,22,1,3),0,0,4,true,true,"还原"),
		[8] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
		[9] = 按钮.创建(自适应.创建(105,4,27,20,1,3),0,0,4,true,true,"详")

	}
	for n=6,9 do
	   self.资源组[n]:绑定窗口_(57)
	end

	self.窗口时间 = 0
	tp = 根
	zts = 根.字体表.普通字体
	zts1 = 根.字体表.道具字体
    ff = 根._丰富文本
	self.介绍文本 = ff(187,480,根.字体表.普通字体)
  self.选中符石 ={}
	local 格子 = 根._物品格子
	self.物品 = {}
	for i=1,5 do
		self.物品[i] = 格子(0,0,i1,"打造")
		self.选中符石[i] = 0
	end
	self.符石数量 =0
	self.编号=0
end
function 场景类_符石:打开(数据,编号)
	if self.可视 then
       self.介绍文本:清空()
		self.可视 = false
    	if tp.窗口.道具行囊.可视 then
    		客户端:发送数据(1, 1, 13, "9", 1)
    	end
       	for i=1,5 do
		self.物品[i]:置物品(nil)
		self.选中符石[i] = 0
		end
       self.符石数量 =0
       self.编号=0
	else
	    if  self.x > 全局游戏宽度 then
	        self.x = 224+(全局游戏宽度-800)/2
	    end
		self.数据=数据
        self.编号=编号
		self.符石数量 =#self.数据.符石
		self.介绍文本:添加文本("")
		for i=1,5 do
			self.物品[i]:置物品(self.数据.符石[i])
		end
        self:刷新属性(self.物品)
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end
function 场景类_符石:显示(dt,x,y)
	self.资源组[9]:更新(x,y)
	self.资源组[8]:更新(x,y)
	self.资源组[6]:更新(x,y,self.符石数量 ~= 0)
	self.资源组[7]:更新(x,y,self.符石数量 ~= 0)
	if self.资源组[8]:事件判断() then
		self:打开()
	elseif self.资源组[6]:事件判断() then
		 客户端:发送数据(self.编号,self.编号,106,self.选中符石[1].."*-*"..self.选中符石[2].."*-*"..self.选中符石[3].."*-*"..self.选中符石[4].."*-*"..self.选中符石[5])
		self:打开()
	elseif self.资源组[7]:事件判断() then
		for i=1,5 do
            if self.物品[i].物品 ~= nil then
			  客户端:发送数据(1, 1, 13, "9", 1)
			  self.物品[i]:置物品(self.数据.符石[i])
		  	 self.选中符石[i]=0
		   end
		end
		self.符石数量 =#self.数据
		self:刷新属性(self.物品)
	end
	self.焦点 = false
	self.焦点1 = false
	self.资源组[0]:显示(self.x,self.y)
	self.资源组[6]:显示(self.x+109,self.y+445,true,1)
	self.资源组[7]:显示(self.x+200,self.y+445,true,1)
	self.资源组[8]:显示(self.x+331,self.y+3)
	self.资源组[9]:显示(self.x+60,self.y+220)
    self.数据.大模型:显示(self.x+30,self.y+80)
    --zts1:置颜色()
    zts1:显示(self.x+148,self.y+50,self.数据.名称)
    self.介绍文本:显示(self.x+148,self.y+75)

 if self.数据.符石.最小孔数 == 1 then
	 	self.资源组[1]:显示(self.x+158-18,self.y+342)
	 	if self.物品[1].物品 ~=nil then
	     self.物品[1]:置坐标(self.x+166-18,self.y+346)

		 self.物品[1]:显示(dt,x,y,self.鼠标,"符石显示",nil,nil,nil,nil,self.符石数量)
	 	end
  elseif self.数据.符石.最小孔数 == 2 then
	  	self.资源组[1]:显示(self.x+108-18,self.y+342)
	  	if self.物品[1].物品 ~=nil then
	     self.物品[1]:置坐标(self.x+114-18,self.y+346)
		 self.物品[1]:显示(dt,x,y,self.鼠标,"符石显示",nil,nil,nil,nil,self.符石数量)
	 	end
	  	self.资源组[2]:显示(self.x+208-18,self.y+342)
	  	if self.物品[2].物品 ~=nil then
	     self.物品[2]:置坐标(self.x+214-18,self.y+346)
		 self.物品[2]:显示(dt,x,y,self.鼠标,"符石显示",nil,nil,nil,nil,self.符石数量)
	 	end
  elseif self.数据.符石.最小孔数 == 3 then
	  	self.资源组[1]:显示(self.x+58-18,self.y+342)
	  	 if self.物品[1].物品 ~=nil then
	     self.物品[1]:置坐标(self.x+64-18,self.y+346)
		 self.物品[1]:显示(dt,x,y,self.鼠标,"符石显示",nil,nil,nil,nil,self.符石数量)
	 	end
	  	self.资源组[2]:显示(self.x+158-18,self.y+342)
	  	 	if self.物品[2].物品 ~=nil then
	     self.物品[2]:置坐标(self.x+164-18,self.y+346)
		 self.物品[2]:显示(dt,x,y,self.鼠标,"符石显示",nil,nil,nil,nil,self.符石数量)
	 	end
	  	self.资源组[3]:显示(self.x+258-18,self.y+342)
	  	 	if self.物品[3].物品 ~=nil then
	     self.物品[3]:置坐标(self.x+264-18,self.y+346)
		 self.物品[3]:显示(dt,x,y,self.鼠标,"符石显示",nil,nil,nil,nil,self.符石数量)
	 	end
  elseif self.数据.符石.最小孔数 == 4 then
	  	self.资源组[1]:显示(self.x+48-18,self.y+342)
	  	 	if self.物品[1].物品 ~=nil then
	     self.物品[1]:置坐标(self.x+54-18,self.y+346)
		 self.物品[1]:显示(dt,x,y,self.鼠标,"符石显示",nil,nil,nil,nil,self.符石数量)
	 	end
	  	self.资源组[2]:显示(self.x+122-18,self.y+342)
	  	 	if self.物品[2].物品 ~=nil then
	     self.物品[2]:置坐标(self.x+128-18,self.y+346)
		 self.物品[2]:显示(dt,x,y,self.鼠标,"符石显示",nil,nil,nil,nil,self.符石数量)
	 	end
	  	self.资源组[3]:显示(self.x+197-18,self.y+342)
	  	 if self.物品[3].物品 ~=nil then
	     self.物品[3]:置坐标(self.x+203-18,self.y+346)
		 self.物品[3]:显示(dt,x,y,self.鼠标,"符石显示",nil,nil,nil,nil,self.符石数量)
	 	end
	  	self.资源组[4]:显示(self.x+268-18,self.y+342)
	  	 if self.物品[4].物品 ~=nil then
	     self.物品[4]:置坐标(self.x+274-18,self.y+346)
		 self.物品[4]:显示(dt,x,y,self.鼠标,"符石显示",nil,nil,nil,nil,self.符石数量)
	 	end
  elseif self.数据.符石.最小孔数 == 5 then
	  	self.资源组[1]:显示(self.x+38-18,self.y+342)
	  	 	if self.物品[1].物品 ~=nil then
	     self.物品[1]:置坐标(self.x+44-18,self.y+346)
		 self.物品[1]:显示(dt,x,y,self.鼠标,"符石显示",nil,nil,nil,nil,self.符石数量)
	 	end
	  	self.资源组[2]:显示(self.x+98-18,self.y+342)
	  	 	if self.物品[2].物品 ~=nil then
	     self.物品[2]:置坐标(self.x+104-18,self.y+346)
		 self.物品[2]:显示(dt,x,y,self.鼠标,"符石显示",nil,nil,nil,nil,self.符石数量)
	 	end
	  	self.资源组[3]:显示(self.x+158-18,self.y+342)
	  	 if self.物品[3].物品 ~=nil then
	     self.物品[3]:置坐标(self.x+164-18,self.y+346)
		 self.物品[3]:显示(dt,x,y,self.鼠标,"符石显示",nil,nil,nil,nil,self.符石数量)
	 	end
	  	self.资源组[4]:显示(self.x+218-18,self.y+342)
	  	 	if self.物品[4].物品 ~=nil then
	     self.物品[4]:置坐标(self.x+224-18,self.y+346)
		 self.物品[4]:显示(dt,x,y,self.鼠标,"符石显示",nil,nil,nil,nil,self.符石数量)
	 	end
	  	self.资源组[5]:显示(self.x+278-18,self.y+342)
	  	 	if self.物品[5].物品 ~=nil then
	     self.物品[5]:置坐标(self.x+284-18,self.y+346)
		 self.物品[5]:显示(dt,x,y,self.鼠标,"符石显示",nil,nil,nil,nil,self.符石数量)
	 	end
  end
  for i=1,5 do
    if self.资源组[i]:是否选中(x,y)  then
    	self.焦点1 = true
    	if self.物品[i].物品 ~= nil and self.物品[i].焦点 then
				tp.提示:道具行囊(x,y,self.物品[i].物品)
		end
 	    if tp.抓取物品 ~=nil and mouseb(0) and self.鼠标 and tp.抓取物品.类型 =="符石" then
		      self.物品[i]:置物品(tp.抓取物品)
		        引擎.场景.窗口.道具行囊.物品[tp.抓取物品ID].确定 = false
		        self.选中符石[i] = tp.抓取物品ID
    			tp.抓取物品 = nil
				tp.抓取物品注释 = nil
                 tp.抓取物品ID =nil
                 self.符石数量 =self.符石数量+1
                 self:刷新属性(self.物品)
		  -- elseif tp.抓取物品 ==nil and mouseb(1) and self.鼠标 and self.物品[i].物品~= nil then
    --         引擎.场景.窗口.道具行囊.物品[self.选中符石[i]]:置物品(self.物品[i].物品)
		  -- 	self.物品[i]:置物品(nil)
		  -- 	self.选中符石[i]=0
		  -- 	self.符石数量 =self.符石数量-1
		  -- 	self:刷新属性(self.物品,rea)

 	    end
 	end
 end
end
function 场景类_符石:刷新属性(物品数据)
	self.介绍文本:清空()
	self.介绍文本:添加文本("#W/"..ItemData[self.数据.名称].说明)  
		self.介绍文本:添加文本("#W/【装备条件】等级"..self.数据.等级)
         if self.数据.符石.最小孔数>0 then
		self.介绍文本:添加文本("#G/开运孔数："..self.数据.符石.最小孔数.."/"..self.数据.符石.最大孔数)
	    else
	    self.介绍文本:添加文本("#G/开运孔数：当前装备还未开孔。")
	    end
	    self.数据.符石.组合=nil
   for i=1,5 do
            if  物品数据~=nil and (物品数据[i].物品 ~= nil or self.数据.符石[i] ~= nil)  then
	 	    	local sx ={}
	 	    	 sx[i] = ""
				local 气血={}     
				气血[i] = ItemData[物品数据[i].物品.名称].属性.气血
				local 魔法={}
				魔法[i] = ItemData[物品数据[i].物品.名称].属性.魔法
				local 命中={}
				命中[i] = ItemData[物品数据[i].物品.名称].属性.命中
				local 伤害={}
				伤害[i] = ItemData[物品数据[i].物品.名称].属性.伤害
				local 防御={}
				防御[i] = ItemData[物品数据[i].物品.名称].属性.防御
				local 速度={}
				速度[i] = ItemData[物品数据[i].物品.名称].属性.速度
				local 躲避={}
				躲避[i] = ItemData[物品数据[i].物品.名称].属性.躲避
				local 灵力={}
				灵力[i] = ItemData[物品数据[i].物品.名称].属性.灵力
				local 敏捷={}
				敏捷[i] = ItemData[物品数据[i].物品.名称].属性.敏捷
				local 体质={}
				体质[i] = ItemData[物品数据[i].物品.名称].属性.体质
				local 耐力={}
				耐力[i] = ItemData[物品数据[i].物品.名称].属性.耐力
				local 法术防御={}
				法术防御[i] = ItemData[物品数据[i].物品.名称].属性.法术防御
				local 魔力={}
				魔力[i] = ItemData[物品数据[i].物品.名称].属性.魔力
				local 力量={}
				力量[i] = ItemData[物品数据[i].物品.名称].属性.力量
				local 法术伤害={}
				法术伤害[i] = ItemData[物品数据[i].物品.名称].属性.法术伤害
				local 固定伤害={}
				固定伤害[i] = ItemData[物品数据[i].物品.名称].属性.固定伤害

				if 体质[i] ~= 0 and 体质[i] ~= nil  then
					sx[i] = sx[i].."体质 +"..体质[i].." "
				end
				if 耐力[i] ~= 0 and 耐力[i] ~= nil  then
					sx[i] = sx[i].."耐力 +"..耐力[i].." "
				end
				if 法术防御[i] ~= 0 and 法术防御[i] ~= nil  then
					sx[i] = sx[i].."法术防御 +"..法术防御[i].." "
				end
				if 魔力[i] ~= 0 and 魔力[i] ~= nil  then
					sx[i] = sx[i].."魔力 +"..魔力[i].." "
				end
				if 力量[i] ~= 0 and 力量[i] ~= nil  then
					sx[i] = sx[i].."力量 +"..力量[i].." "
				end
				if 法术伤害[i] ~= 0 and 法术伤害[i] ~= nil  then
					sx[i] = sx[i].."法术伤害 +"..法术伤害[i].." "
				end
				if 固定伤害[i] ~= 0 and 固定伤害[i] ~= nil  then
					sx[i] = sx[i].."固定伤害 +"..固定伤害[i].." "
				end
				if 伤害[i] ~= 0 and 伤害[i] ~= nil  then
					sx[i] = sx[i].."伤害 +"..伤害[i].." "
				end
				if 命中[i] ~= 0  and 命中[i] ~= nil then
					sx[i] = sx[i].."命中 +"..命中[i].." "
				end
				if 防御[i] ~= 0 and 防御[i] ~= nil  then
					sx[i] = sx[i].."防御 +"..防御[i].." "
				end
				if 气血[i] ~= 0 and 气血[i] ~= nil then
					sx[i] = sx[i].."气血 +"..气血[i].." "
				end
				if 魔法[i] ~= 0 and 魔法[i] ~= nil  then
					sx[i] = sx[i].."魔法 +"..魔法[i].." "
				end
				if 速度[i] ~= 0 and 速度[i] ~= nil  then
					sx[i] = sx[i].."速度 +"..速度[i].." "
				end
				if 躲避[i] ~= 0 and 躲避[i] ~= nil  then
					sx[i] = sx[i].."躲避 +"..躲避[i].." "
				end
				if 灵力[i] ~= 0 and 灵力[i] ~= nil  then
					sx[i] = sx[i].."灵力 +"..灵力[i].." "
				end
				if 敏捷[i] ~= 0 and 敏捷[i] ~= nil  then
					sx[i] = sx[i].."敏捷 +"..敏捷[i].." "
				end
				if sx[i] ~= "" then
					self.介绍文本:添加文本("#G/符石："..sx[i])
				end
            end
    end




		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="绿色" then     
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="紫色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="白色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="红色" then
		         if 物品数据[5].物品~=nil  and ItemData[物品数据[5].物品.名称].属性.颜色 =="黄色" then
		           self.数据.符石.组合={名称="万丈霞光",等级=4}
		         else
		           self.数据.符石.组合={名称="万丈霞光",等级=3}
		         end
		       else
		          self.数据.符石.组合={名称="万丈霞光",等级=2}
		       end
		     else
		        self.数据.符石.组合={名称="万丈霞光",等级=1}
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="黑色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="紫色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="蓝色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="黄色" then
		         if 物品数据[5].物品~=nil  and ItemData[物品数据[5].物品.名称].属性.颜色 =="绿色" then
		           self.数据.符石.组合={名称="百步穿杨",等级=4}
		         else
		           self.数据.符石.组合={名称="百步穿杨",等级=3}
		         end
		       else
		          self.数据.符石.组合={名称="百步穿杨",等级=2}
		       end
		     else
		        self.数据.符石.组合={名称="百步穿杨",等级=1}
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="白色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="红色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="紫色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="蓝色" then
		         if 物品数据[5].物品~=nil  and ItemData[物品数据[5].物品.名称].属性.颜色 =="黄色" then
		           self.数据.符石.组合={名称="隔山打牛",等级=4}
		         else
		           self.数据.符石.组合={名称="隔山打牛",等级=3}
		         end
		       else
		          self.数据.符石.组合={名称="隔山打牛",等级=2}
		       end
		     else
		        self.数据.符石.组合={名称="隔山打牛",等级=1}
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="黑色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="蓝色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="红色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="绿色" then
		         if 物品数据[5].物品~=nil  and ItemData[物品数据[5].物品.名称].属性.颜色 =="白色" then
		           self.数据.符石.组合={名称="心随我动",等级=4}
		         else
		           self.数据.符石.组合={名称="心随我动",等级=3}
		         end
		       else
		          self.数据.符石.组合={名称="心随我动",等级=2}
		       end
		     else
		        self.数据.符石.组合={名称="心随我动",等级=1}
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="白色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="黄色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="红色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="绿色" then
		         if 物品数据[5].物品~=nil  and ItemData[物品数据[5].物品.名称].属性.颜色 =="黑色" then
		           self.数据.符石.组合={名称="云随风舞",等级=4}
		         else
		           self.数据.符石.组合={名称="云随风舞",等级=3}
		         end
		       else
		          self.数据.符石.组合={名称="云随风舞",等级=2}
		       end
		     else
		        self.数据.符石.组合={名称="云随风舞",等级=1}
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="蓝色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="白色" then
		        self.数据.符石.组合={名称="望穿秋水",等级=1}
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="黄色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="黑色" then
		        self.数据.符石.组合={名称="万里横行",等级=1}
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="红色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="紫色" then
		        self.数据.符石.组合={名称="日落西山",等级=1}
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="黄色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="绿色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="黄色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="黑色" then
		           self.数据.符石.组合={名称="网罗乾坤",等级=3}
		       else
		          self.数据.符石.组合={名称="网罗乾坤",等级=2}
		       end
		     else
		        self.数据.符石.组合={名称="网罗乾坤",等级=1}
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="绿色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="红色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="红色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="白色" then
		           self.数据.符石.组合={名称="石破天惊",等级=3}
		       else
		          self.数据.符石.组合={名称="石破天惊",等级=2}
		       end
		     else
		        self.数据.符石.组合={名称="石破天惊",等级=1}
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="红色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="红色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="蓝色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="紫色" then
		           self.数据.符石.组合={名称="天雷地火",等级=3}
		       else
		          self.数据.符石.组合={名称="天雷地火",等级=2}
		       end
		     else
		        self.数据.符石.组合={名称="天雷地火",等级=1}
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="蓝色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="黄色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="黄色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="紫色" then
		           self.数据.符石.组合={名称="烟雨飘摇",等级=3}
		       else
		          self.数据.符石.组合={名称="烟雨飘摇",等级=2}
		       end
		     else
		        self.数据.符石.组合={名称="烟雨飘摇",等级=1}
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="黄色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="蓝色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="黄色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="白色" then
		           self.数据.符石.组合={名称="索命无常",等级=3}
		       else
		          self.数据.符石.组合={名称="索命无常",等级=2}
		       end
		     else
		        self.数据.符石.组合={名称="索命无常",等级=1}
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="绿色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="黄色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="红色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="紫色" then
		           self.数据.符石.组合={名称="行云流水",等级=3}
		       else
		          self.数据.符石.组合={名称="行云流水",等级=2}
		       end
		     else
		        self.数据.符石.组合={名称="行云流水",等级=1}
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="黄色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="红色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="蓝色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="白色" then
		           self.数据.符石.组合={名称="福泽天下",等级=3}
		       else
		          self.数据.符石.组合={名称="福泽天下",等级=2}
		       end
		     else
		        self.数据.符石.组合={名称="福泽天下",等级=1}
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="蓝色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="蓝色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="黄色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="黑色" then
		         if 物品数据[5].物品~=nil  and ItemData[物品数据[5].物品.名称].属性.颜色 =="黑色" then
		           self.数据.符石.组合={名称="无心插柳",等级=4}
		         else
		           self.数据.符石.组合={名称="无心插柳",等级=3}
		         end
		       else
		          self.数据.符石.组合={名称="无心插柳",等级=2}
		       end
		     else
		        self.数据.符石.组合={名称="无心插柳",等级=1}
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="白色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="紫色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="蓝色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="白色" then
		         if 物品数据[5].物品~=nil  and ItemData[物品数据[5].物品.名称].属性.颜色 =="红色" then
		           self.数据.符石.组合={名称="高山流水",等级=3}
		         else
		           self.数据.符石.组合={名称="高山流水",等级=2}
		         end
		       else
		          self.数据.符石.组合={名称="高山流水",等级=1}
		       end
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="黑色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="白色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="紫色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="绿色" then
		         if 物品数据[5].物品~=nil  and ItemData[物品数据[5].物品.名称].属性.颜色 =="红色" then
		           self.数据.符石.组合={名称="百无禁忌",等级=3}
		         else
		           self.数据.符石.组合={名称="百无禁忌",等级=2}
		         end
		       else
		          self.数据.符石.组合={名称="百无禁忌",等级=1}
		       end
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="蓝色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="红色" then
		        self.数据.符石.组合={名称="无懈可击",等级=1}

		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="白色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="蓝色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="黄色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="白色" then
		         if 物品数据[5].物品~=nil  and ItemData[物品数据[5].物品.名称].属性.颜色 =="紫色" then
		           self.数据.符石.组合={名称="暗度陈仓",等级=3}
		         end
		       end
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="红色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="黄色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="紫色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="紫色" then
		         if 物品数据[5].物品~=nil  and ItemData[物品数据[5].物品.名称].属性.颜色 =="白色" then
		           self.数据.符石.组合={名称="化敌为友",等级=3}
		         end
		       end
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="紫色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="绿色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="黑色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="红色" then
		           self.数据.符石.组合={名称="点石成金",等级=3}

		       end
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="蓝色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="蓝色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="蓝色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="蓝色" then
		         if 物品数据[5].物品~=nil  and ItemData[物品数据[5].物品.名称].属性.颜色 =="蓝色" then
		           self.数据.符石.组合={名称="不动明王",等级=3}
		         end
		       end
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="紫色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="紫色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="紫色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="紫色" then
		         if 物品数据[5].物品~=nil  and ItemData[物品数据[5].物品.名称].属性.颜色 =="紫色" then
		           self.数据.符石.组合={名称="势如破竹",等级=3}
		         end
		       end
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="黑色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="黑色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="黑色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="黑色" then
		         if 物品数据[5].物品~=nil  and ItemData[物品数据[5].物品.名称].属性.颜色 =="黑色" then
		           self.数据.符石.组合={名称="君临天下",等级=3}
		         end
		       end
		     end
		  end
		end
		if 物品数据[1].物品~=nil  and ItemData[物品数据[1].物品.名称].属性.颜色=="白色" then
		  if 物品数据[2].物品~=nil  and ItemData[物品数据[2].物品.名称].属性.颜色 =="白色" then
		     if 物品数据[3].物品~=nil  and ItemData[物品数据[3].物品.名称].属性.颜色 =="白色" then
		       if 物品数据[4].物品~=nil  and ItemData[物品数据[4].物品.名称].属性.颜色 =="白色" then
		         if 物品数据[5].物品~=nil  and ItemData[物品数据[5].物品.名称].属性.颜色 =="白色" then
		           self.数据.符石.组合={名称="凤舞九天",等级=3}
		         end
		       end
		     end
		  end
		end
		if self.数据.符石~=nil and self.数据.符石.组合 ~=nil then
		 	self.介绍文本:添加文本("#F/符石组合："..self.数据.符石.组合.名称)
		 	self.介绍文本:添加文本("#F/门派条件：无")
            self.介绍文本:添加文本("#F/部位条件：无")
            self.介绍文本:添加文本("#F/"..取符石介绍(self.数据.符石.组合))
	    end
end
function 场景类_符石:检查点(x,y)
	if self.资源组[0]:是否选中(x,y)  then
		return true
	end
end

function 场景类_符石:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_符石:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_符石