--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--
local 场景类_道具竞拍 = class(窗口逻辑)

local tp,zts,zts1
local floor = math.floor
local tonumber = tonumber
local tostring = tostring
local insert = table.insert
local xs = {[0]="当前价格",[1]="竞拍价格"}
local jqr = 引擎.取金钱颜色

function 场景类_道具竞拍:初始化(根)
	self.ID = 120
	self.x = 280
	self.y = 86
	self.xx = 0
	self.yy = 0
	self.注释 = "道具竞拍"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 根.资源:载入('JM.dll',"网易WDF动画",0x30F6DB85),
		[2] = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true),
		[3] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"竞拍"),
		[4] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"取消"),
		[5] = 根.资源:载入('JM.dll',"网易WDF动画",0x284B91F0),
		[6] = 自适应.创建(3,1,115,22,1,3),
        [7] = 根.资源:载入('JM.dll',"网易WDF动画",0xD2FFEE78),


		--0B9E4143  284B91F0 底图 边框   22D22D6D D2FFEE78 B345B07B 23485855 291A331E  7FFB866A 4A04B5AC  7885B3E4
	}
	for n=2,4 do
	   self.资源组[n]:绑定窗口_(120)
	end
	local 格子 = 根._物品格子
	self.物品 = 格子(0,0,i,"道具竞拍")
	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('道具竞拍总控件')
	总控件:置可视(true,true)
	self.输入框 = 总控件:创建输入("数量输入",self.x-45 ,self.y-38,100,14)
	self.输入框:置可视(false,false)
	self.输入框:置数字模式()
	self.输入框:置光标颜色(-16777216)
	self.输入框:置文字颜色(0xFF000000)
	self.输入框:置文本(111111)
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.描边字体_
end

function 场景类_道具竞拍:刷新(数据)
	self.物品:置物品(数据.物品)
	self.数据=数据
	self.数据.剩余时间=60-self.数据.剩余时间
end
function 场景类_道具竞拍:打开(数据)
	if self.可视 then
         tp.窗口.文本栏:载入("#H/是否关闭拍卖信息,当前物品的拍卖信息将不在更新","道具拍卖",true)

	else
		if  self.x > 全局游戏宽度 then
		self.x = 280
		end
        self.输入框:置可视(true,true)
		self.物品:置物品(数据.物品)
		self.数据=数据
		self.数据.剩余时间=60-self.数据.剩余时间
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_道具竞拍:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y,self.输入框:取文本() ~="" )
	self.资源组[4]:更新(x,y)
    self.资源组[7]:更新(dt)
	if self.鼠标 then
		if self.资源组[2]:事件判断() then
			self:打开()
		elseif self.资源组[3]:事件判断() then
          客户端:发送数据(1,204,13,self.输入框:取文本())
		elseif self.资源组[4]:事件判断() then
           self:打开()

 
			
		end
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[5]:显示(self.x+35,self.y+35)

	 zts1:显示(self.x+235,self.y+27+2,"请输入你的价格")

	self.资源组[6]:显示(self.x+230,self.y+46)
	self.资源组[2]:显示(self.x+337,self.y+17)
	self.资源组[3]:显示(self.x+225,self.y+68,true)
	self.资源组[4]:显示(self.x+290,self.y+68,true)
		self.物品:置坐标(self.x+35 ,self.y+33)
		self.物品:显示(dt,x,y,self.鼠标)
		if self.物品.焦点 and self.物品.物品 ~= nil then
			tp.提示:道具行囊(x,y,self.物品.物品)
		end
self.资源组[7]:显示(self.x+60,self.y+60)
   zts:置颜色(0xFF000000)
    zts1:显示(self.x + 89,self.y + 25+2,"剩余时间:")
    zts1:显示(self.x + 89,self.y + 41+2,"当前价格:")
    zts1:显示(self.x + 95,self.y + 57+2,"玩家ID :")
    zts1:显示(self.x + 89,self.y + 73+2,"玩家名称:")
  if self.数据.竞拍id then
  	 zts:显示(self.x + 153,self.y + 57,self.数据.竞拍id)
    zts:显示(self.x + 153,self.y + 73,self.数据.竞拍名称)
    zts:置颜色(引擎.取金钱颜色(self.数据.竞拍价格))
    zts:显示(self.x + 153,self.y + 41,self.数据.竞拍价格)
    zts:置颜色(0xFFFF0000)
    zts:显示(self.x + 153,self.y + 25,self.数据.剩余时间)
     zts:置颜色(0xFFFFFFFF)
   else
    zts:显示(self.x + 153,self.y + 57,self.数据.id)
    zts:显示(self.x + 153,self.y + 73,self.数据.名称)
    zts:置颜色(引擎.取金钱颜色(self.数据.价格))
    zts:显示(self.x + 153,self.y + 41,self.数据.价格)
    zts:置颜色(0xFFFF0000)
    zts:显示(self.x + 153,self.y + 25,self.数据.剩余时间)
     zts:置颜色(0xFFFFFFFF)
  end

	self.输入框:置坐标(self.x,self.y)



	self.控件类:更新(dt,x,y)
	self.控件类:显示(x,y)
	if self.输入框._已碰撞  then
		self.焦点 = true
	end
end



return 场景类_道具竞拍