--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:46
--======================================================================--
local 修炼 = class()
local bw = require("gge包围盒")(0,0,390,20)
local sts = {"攻击修炼     等级：" ,"法术修炼     等级：" ,"防御修炼     等级：","法抗修炼     等级："}
local sts1 = {"所需资金" ,"所需经验"}
local sts2= {"当前资金","当前经验"}
local box = 引擎.画矩形
local tp
local floor = math.floor
local jqr = 引擎.取金钱颜色
local tonumber = tonumber
local tostring = tostring
local format = string.format
local insert = table.insert
function 修炼:初始化(根)

	self.ID = 130
	self.x =200+(全局游戏宽度-800)/2
	self.y = 200
	self.xx = 0
	self.yy = 0
	self.注释 = "队伍栏"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 资源 = 根.资源
	local 自适应 =根._自适应
		self.资源组 = {
		[0] = 自适应.创建(99,1,421,245,3,9),
		[1] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
		[2] = 按钮.创建(自适应.创建(103,4,75,22,1,3),0,0,4,true,true,"  修 炼 "),
		[3] = 按钮.创建(自适应.创建(103,4,75,22,1,3),0,0,4,true,true,"  取 消 "),
	
		[5] = 自适应.创建(93,1,94,22,1,3),
		
		[7] = 自适应.创建(70,1,385,102,3,9),
		[8] = 按钮.创建(自适应.创建(103,4,75,22,1,3),0,0,4,true,true,"修炼10次"),
	}
	self.资源组[1]:绑定窗口_(130)
	self.资源组[2]:绑定窗口_(130)
	self.资源组[3]:绑定窗口_(130)
	self.资源组[8]:绑定窗口_(130)
		tp = 根
	self.窗口时间 = 0
	self.选中 = 0
	self.加入 = 0


end
function 修炼:刷新(数据)
	  self.数据 = 数据
end
function 修炼:打开(数据)
  if self.可视 then
	self.选中 = 0
	self.加入 = 0
		self.可视 = false
  else
  		self.x =200+(全局游戏宽度-800)/2
     self.数据 = 数据
		insert(tp.窗口_,self)
		tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.可视 = true
  end
 end


function 修炼:显示(dt,x, y)

		self.焦点 = false
		self.资源组[0]:显示(self.x,self.y)
		
        Picture.标题:显示(self.x+self.资源组[0].宽度/2-70,self.y)
		tp.字体表.华康字体:置颜色(0xFFFFFFFF):显示(self.x+190,self.y+2," 人物修炼 ")
        self.资源组[1]:显示(self.x+400,self.y+3)
        self.资源组[2]:显示(self.x+170,self.y+208,true,1)
        self.资源组[3]:显示(self.x+270,self.y+208,true,1)
        self.资源组[7]:显示(self.x+16,self.y+30)
        self.资源组[8]:显示(self.x+70,self.y+208,true,1)
        self.资源组[1]:更新(x,y)
        self.资源组[2]:更新(x,y,self.选中~=0)
        self.资源组[8]:更新(x,y,self.选中~=0)
        self.资源组[3]:更新(x,y)
        for i=1,2 do
               self.资源组[5]:显示(self.x-80+i*175,self.y+145)
               tp.字体表.华康字体:置颜色(0xFFFFFFFF):显示(self.x-150+i*175,self.y+147,sts1[i])
        end
         for i=1,2 do
               self.资源组[5]:显示(self.x-80+i*175,self.y+175)
                 tp.字体表.华康字体:置颜色(0xFFFFFFFF):显示(self.x-150+i*175,self.y+177,sts2[i])
        end
       if  self.资源组[2]:事件判断()  then
	       	if self.选中 == 0 then
				tp.提示:写入("#y/请先选中一个修炼")
				return 0
			else
				客户端:发送数据(self.选中,8, 5, "1")
			end
       elseif   self.资源组[3]:事件判断() or  self.资源组[1]:事件判断() then
        self:打开()
        elseif  self.资源组[8]:事件判断()  then
	       	if self.选中 == 0 then
				tp.提示:写入("#y/请先选中一个修炼")
				return 0
			else
				客户端:发送数据(self.选中,9, 5, "1")
			end
       end
		for m=1,4 do
			if self.数据[m + self.加入] ~= nil then
				bw:置坐标(self.x + 22,self.y +14+ m * 23)
				if self.选中~= m + self.加入 then
					if bw:检查点(x,y) then
						box(self.x + 22,self.y +14+ m * 23,self.x + 390,self.y + m * 22 + 38,-3551379)
						if 引擎.鼠标弹起(0) and self.鼠标 then
							self.选中 = m + self.加入
						end
						self.焦点 = true
					end
				else
					local ys = -10790181
					if bw:检查点(x,y) then
						ys = -9670988
						self.焦点 = true
					end
					box(self.x + 22,self.y +14+ m * 23,self.x + 390,self.y + m * 22 + 38,ys)
				end

			end
	  		 tp.字体表.华康字体:置颜色(0xFF000000):显示(self.x + 25,self.y +16+ m * 23,sts[m + self.加入]..self.数据[m + self.加入][1].."/"..self.数据[m + self.加入][4].."    修炼经验："..self.数据[m + self.加入][2].."/"..self.数据[m + self.加入][3])
		end

	  tp.字体表.华康字体:置颜色(0xFF000000):显示(self.x + 102,self.y +147 ,200000)
	  tp.字体表.华康字体:置颜色(0xFF000000):显示(self.x + 278,self.y +147 ,1000000)
	  tp.字体表.华康字体:置颜色(jqr(self.数据.银子+0)):显示(self.x + 102,self.y +177 ,self.数据.银子)
	  tp.字体表.华康字体:置颜色(0xFF000000):显示(self.x + 278,self.y +177 ,self.数据.当前经验)

end
function 修炼:检查点(x,y)
	if self.可视 and self.资源组[0]:是否选中(x,y)  then
		return true
	else
		return false
	end
end

function 修炼:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.可视 and self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 修炼:开始移动(x,y)
	if self.可视 and self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end
return 修炼









