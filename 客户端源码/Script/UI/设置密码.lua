--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--

local 场景类_设置密码 = class(窗口逻辑)
local insert = table.insert
local remove = table.remove
local floor = math.floor
local tp,zts1

function 场景类_设置密码:初始化(根)
	self.ID = 117
	self.x = 82
	self.y = 129
	self.xx = 0
	self.yy = 0
	self.注释 = "队伍栏"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 资源 = 根.资源
	local 自适应 =根._自适应
	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('创建控件')
	总控件:置可视(true,true)
	self.密码输入 = 总控件:创建输入("设置密码密码",109,134,120,14,根)
	self.密码输入:置可视(false,false)
	self.密码输入:置限制字数(10)
	self.密码输入:置密码模式()
    self.密码输入:置文字颜色(0xFF000000)
	self.确认输入 = 总控件:创建输入("确认密码",109,159,120,14,根)
	self.确认输入:置可视(false,false)
	self.确认输入:置限制字数(10)
	self.确认输入:置密码模式()
    self.确认输入:置文字颜色(0xFF000000)
		self.资源组 = {
		[1] = 根.资源:载入('JM.dll',"网易WDF动画",0x598DDA5A),
		[0] = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true),
		[2] = 按钮.创建(自适应.创建(16,4,43,22,1,3),0,0,4,true,true,"设置"),
		[3] = 按钮.创建(自适应.创建(16,4,43,22,1,3),0,0,4,true,true,"取消"),

	}
	self.资源组[0]:绑定窗口_(117)
	self.资源组[2]:绑定窗口_(117)
	self.资源组[3]:绑定窗口_(117)
		tp = 根
	self.窗口时间 = 0
self.介绍文本 = 根._丰富文本(230,149,根.字体表.普通字体)
    	 for n=0,119 do
    self.介绍文本:添加元素(n,根.包子表情动画[n])
    end
self.介绍文本:添加文本("#r/请牢记的解锁密码，建议将字母和数字的组合形式来设置，设置成功后请勿随意告知别人以防止盗号，安全游戏你我做起。#23#23")


end


function 场景类_设置密码:打开(内容)
  if self.可视 then
		self.密码输入:置可视(false,false)
		self.确认输入:置可视(false,false)
		self.密码输入:置焦点(false)
		self.确认输入:置焦点(false)
		self.可视 = false
  else
		if  self.x > 全局游戏宽度 then
		self.x = 82
		end
		insert(tp.窗口_,self)
		tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.可视 = true
  end
 end

function 场景类_设置密码:显示(dt,x,y)
		self.焦点 = false
		self.资源组[1]:显示(self.x,self.y)
	self.介绍文本:显示(self.x+10,self.y+213)
        self.资源组[0]:显示(self.x+230,self.y+3)
        self.资源组[2]:显示(self.x+60,self.y+340,true)
        self.资源组[3]:显示(self.x+160,self.y+340,true)
        self.资源组[0]:更新(x,y)
        self.资源组[2]:更新(x,y)
        self.资源组[3]:更新(x,y)
		self.控件类:显示(x,y)
		self.控件类:更新(dt,x,y)
		self.密码输入:置可视(true,true)
		self.确认输入:置可视(true,true)
       if  self.资源组[2]:事件判断()  then
        	if self.密码输入:取文本() ~= self.确认输入:取文本() then
        	    tp.提示:写入("#Y/请确认密码重新输入")
        	 elseif self.密码输入:取文本() == "" or self.确认输入:取文本() == "" then
        	     tp.提示:写入("#Y/请输入密码或者确认密码")
            else
            	客户端:发送数据(1,1,56,self.密码输入:取文本())
            	 self:打开()
            end
       elseif  self.资源组[0]:事件判断() or self.资源组[3]:事件判断() then
        self:打开()
       end
       self.密码输入:置坐标(self.x,self.y)
       self.确认输入:置坐标(self.x,self.y)
    if self.密码输入._已碰撞 then
		self.焦点 = true
	end
	  if self.确认输入._已碰撞 then
		self.焦点 = true
	end
end




return 场景类_设置密码










