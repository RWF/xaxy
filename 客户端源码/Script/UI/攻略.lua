-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-12 02:03:53




local 场景类_攻略 = class(窗口逻辑)
local insert = table.insert
local remove = table.remove
local floor = math.floor
local tp,zts,zts1

function 场景类_攻略:初始化(根)
    self.ID = 314
    self.x = 290+(全局游戏宽度-800)/2
    self.y = 65
    self.xx = 0
    self.yy = 0
    self.注释 = "攻略"
    self.可视 = false
    self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    local 按钮 = 根._按钮
    local 自适应 = 根._自适应
    local  资源= 根.资源
    self.资源组 = {
        [1] = 根.资源:载入("攻略背景.png","加密图片"),

    }


    self.介绍文本 = 根._丰富文本(240,500,根.字体表.普通字体)
    self.介绍文本:置默认颜色(0xFF000000)
        for n=0,119 do
        self.介绍文本:添加元素(n,根.包子表情动画[n])
     end

    self.翻页 =false
    self.窗口时间 = 0
    tp = 根
    zts = tp.字体表.普通字体
    zts1 = tp.字体表.描边字体

end




function 场景类_攻略:打开(内容)
  if self.可视 then
        self.可视 = false
        self.介绍文本:清空()
  else
       self.数据=内容
       self.介绍文本:清空()
        self.介绍文本:添加文本("#Y/             "..内容.."攻略")
        self.介绍文本:添加文本("#23/#23/#23/#23/#23/#23/#23/#23/#23/#23/#23/#23/")
        if f函数.文件是否存在(程序目录.."/Data/desc/"..内容..".txt")~=true then
          self.介绍文本:添加文本("#W/管理未设置攻略内容")
        else
            local file =  io.open(程序目录.."/Data/desc/"..内容..".txt", "r")
            self.介绍文本:添加文本(file:read("*a"))
            file:close() 
        end

        if  self.x > 全局游戏宽度 then
        self.x = 82+(全局游戏宽度-800)/2
        end
        insert(tp.窗口_,self)
        tp.运行时间 = tp.运行时间 + 1
        self.窗口时间 = tp.运行时间
        self.可视 = true
  end
 end
function 场景类_攻略:显示(dt,x,y)
    self.焦点 = false

    self.资源组[1]:显示(self.x,self.y)
self.介绍文本:显示(self.x+22,self.y+40)

end



return 场景类_攻略



