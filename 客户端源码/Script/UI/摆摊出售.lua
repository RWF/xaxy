--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:48
--======================================================================--
local 摆摊出售 = class(窗口逻辑)
local tp,zts,zts1,zts2
local insert = table.insert
local xxx = 0
local yyy = 0
local bw = require("gge包围盒")(0,0,230,20)
local box = 引擎.画矩形
local sts = {"单价","数量","总额","现金"}
local tos = 引擎.取金钱颜色
local mouseb = 引擎.鼠标弹起
function 摆摊出售:初始化(根)
	self.ID = 66
	self.x = 224
	self.y = 100
	self.xx = 0
	self.yy = 0
	self.注释 = "商店"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 资源:载入('JM.dll',"网易WDF动画",0x0967049D),
		
		[3] = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true),
		[4] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"上架"),
		[5] = 按钮.创建(自适应.创建(16,4,72,22,1,3),0,0,4,true,true,"物品类"),
		[6] = 按钮.创建(自适应.创建(16,4,72,22,1,3),0,0,4,true,true,"召唤兽类"),
		[7] = 按钮.创建(自适应.创建(16,4,72,22,1,3),0,0,4,true,true,"制造类"),
        [8] = 按钮.创建(自适应.创建(16,4,72,22,1,3),0,0,4,true,true,"更改招牌"),
        [9] = 按钮.创建(自适应.创建(16,4,45,22,1,3),0,0,4,true,true," <<"),
        [10] = 按钮.创建(自适应.创建(16,4,92,22,1,3),0,0,4,true,true,"摊位聊天室"),
        [11] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"收摊"),
        [12] = 按钮.创建(自适应.创建(16,4,72,22,1,3),0,0,4,true,true,"替身货郎"),
        [13] = 按钮.创建(自适应.创建(20,4,18,19,4,3),0,0,4,true,true),
		[14] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),
		[19] = 资源:载入('JM.dll',"网易WDF动画",0x783C4453),
        [20] = 自适应.创建(2,1,241,209,3,9),
	}
    self.资源组[5]:置偏移(6,0)
    self.资源组[7]:置偏移(6,0)
	for n=3,14 do
	   self.资源组[n]:绑定窗口_(66)
	end
	self.商品 = {}
	local 物品格子 = 根._物品格子
	for i=1,20 do
		self.商品[i] = 物品格子(0,i,n,"商店")
	end
	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('商店总控件')
	总控件:置可视(true,true)
	self.输入框 = 总控件:创建输入("数量输入",self.x + 220,self.y + 345,100,14)
	self.输入框:置可视(false,false)
	self.输入框:置限制字数(11)
	self.输入框:置数字模式()
	self.输入框:置光标颜色(-16777216)
	self.输入框:置文字颜色(-16777216)
	self.名称输入框 = 总控件:创建输入("数量输入0",self.x + 60,self.y + 40,120,14)
	self.名称输入框:置可视(false,false)
	self.名称输入框:置限制字数(12)
	self.名称输入框:置光标颜色(-16777216)
	self.名称输入框:置文字颜色(-16777216)
	self.上一次 = 1
	self.选中道具=0
	self.窗口时间 = 0
	tp = 根
	zts = 根.字体表.普通字体
	zts2 = 根.字体表.普通字体_
	zts1 = 根.字体表.描边字体
	self.物品列表 = {}
	self.bb列表 = {}
	self.制造列表 = {}
end

function 摆摊出售:打开(数据)
	if self.可视 then
		for i=1,20 do
			self.商品[i]:置物品(nil)
		end
		self.可视 = false
		self.商品[self.上一次].确定 = false
		self.上一次 = 1
		self.选中道具= 0
		self.输入框:置焦点(false)
		self.输入框:置可视(false,false)
		self.名称输入框:置焦点(false)
		self.名称输入框:置可视(false,false)
		self.召唤兽数据={}
		self.选中召唤兽 = 0
	else
			if  self.x > 全局游戏宽度 then
			self.x = 224
			end
		self.数据=数据.道具
        self.摊位名称 = 数据.名称
		insert(tp.窗口_,self)
	  	self.名称输入框:置可视(true,true)
	  	self.名称输入框:置文本(self.摊位名称)
	    tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end
function 摆摊出售:刷新宝宝(数据)
	   self.状态="召唤兽类"
	  	for i=1,20 do
			self.商品[i]:置物品(nil)
		end
	    self.选中道具= 0
		self.选中召唤兽 = 0
		self.制造数据={}
		self.选中制造 = 0
		self.道具 = nil
		self.商品[self.上一次].确定 = false
		self.上一次 = 1
		self.输入框:置焦点(false)
		self.输入框:置可视(false,false)
		self.召唤兽数据=数据
end
function 摆摊出售:刷新道具(数据)
		self.状态 = "物品类"
		self.商品[self.上一次].确定 = false
		self.上一次 = 1
		self.选中道具= 0
		self.制造数据={}
		self.选中制造 = 0
		self.输入框:置焦点(false)
		self.输入框:置可视(false,false)
		self.召唤兽数据={}
		self.选中召唤兽 = 0
		for n=1,20 do
			if 数据~=nil then
			   self.商品[n]:置物品(nil)
			   self.商品[n]:置物品(数据[n])
			end

	  	end
end
function 摆摊出售:刷新制造(数据)
		self.状态 = "制造类"
		self.商品[self.上一次].确定 = false
		self.上一次 = 1
		self.选中道具= 0
		self.输入框:置焦点(false)
		self.输入框:置可视(false,false)
		self.召唤兽数据={}
		self.选中召唤兽 = 0
		self.制造数据=数据
		self.选中制造 = 0
		for n=1,20 do
			   self.商品[n]:置物品(nil)
	  	end

end
function 摆摊出售:取重复bb(id)
	for n = 1, 10, 1 do
		if self.bb列表[n] ~= nil and self.bb列表[n].id == id then
			return n
		end
	end
	return 0
end
function 摆摊出售:取重复制造(id)
	for n = 1, 10, 1 do
		if self.制造列表[n] ~= nil and self.制造列表[n].id == id then
			return n
		end
	end
	return 0
end
function 摆摊出售:bb选中事件(id)
	local 操作id = self:取重复bb(id)
	if 操作id ~= 0 then
		self.输入框:置文本(self.bb列表[操作id].价格)
		self.资源组[4]:置文字("下架")
	else
		if  self.输入框:取文本()=="" then
		    self.输入框:置文本(1)
		end
		self.资源组[4]:置文字("上架")
	end
	self.输入框:置坐标(self.x-350,self.y-105)
	self.输入框:置文字颜色(tos(self.输入框:取文本()+0))
end
function 摆摊出售:制造选中事件(id)
	local 操作id = self:取重复制造(id)
	if 操作id ~= 0 then
		self.输入框:置文本(self.制造列表[操作id].价格)
		self.资源组[4]:置文字("下架")
	else
		if  self.输入框:取文本()=="" then
		    self.输入框:置文本(1)
		end
		self.资源组[4]:置文字("上架")
	end
	self.输入框:置坐标(self.x-350,self.y-105)
	self.输入框:置文字颜色(tos(self.输入框:取文本()+0))
end
function 摆摊出售:取重复物品(id)
	for n = 1, 20, 1 do
		if self.物品列表[n] ~= nil and self.物品列表[n].id == id then
			return n
		end
	end
	return 0
end

function 摆摊出售:刷新道具状态(内容)
	self.物品列表[内容.id + 0] = {
		价格 = 内容.价格,
		id = 内容.id
	}
end

function 摆摊出售:刷新bb状态(内容)
	self.bb列表[内容.id + 0] = {
		价格 = 内容.价格,
		id = 内容.id
	}
end
function 摆摊出售:移除道具(内容)
	self.物品列表[内容 + 0] = nil
end
function 摆摊出售:移除bb(内容)
	self.bb列表[内容 + 0] = nil
end
function 摆摊出售:刷新制造状态(内容)
	self.制造列表[内容.id + 0] = {
		价格 = 内容.价格,
		id = 内容.id
	}
end
function 摆摊出售:移除制造(内容)
	self.制造列表[内容 + 0] = nil
end
function 摆摊出售:道具选中事件(id)
	local 操作id = self:取重复物品(id)
	if 操作id ~= 0 then
		self.输入框:置文本(self.物品列表[操作id].价格)
		self.资源组[4]:置文字("下架")
	else
		if  self.输入框:取文本()=="" then
		    self.输入框:置文本(1)
		end
		self.资源组[4]:置文字("上架")
	end
	self.输入框:置坐标(self.x-350,self.y-105)
	self.输入框:置文字颜色(tos(self.输入框:取文本()+0))
end
function 摆摊出售:显示(dt,x,y)
	self.焦点1 = false
	self.资源组[3]:更新(x,y)
	self.资源组[8]:更新(x,y)
	self.资源组[10]:更新(x,y)
	self.资源组[11]:更新(x,y)
	self.资源组[12]:更新(x,y)
	self.资源组[4]:更新(x,y,self.选中道具 ~= 0 or self.选中召唤兽 ~= 0 or self.选中制造 ~= 0  )
	for i=5,7 do
		self.资源组[i]:更新(x,y,self.状态~=self.资源组[i]:取文字())
	end
	if self.资源组[3]:事件判断() then
		self:打开()
	elseif self.资源组[4]:事件判断() then
		if self.状态 == "物品类" then
			if self.选中道具 == 0 then
				tp.提示:写入("#Y/请先选中一个要出售的道具")
				return 0
			else
				客户端:发送数据(self.选中道具, 2, 44, self.输入框:取文本()+0, 1)
			end
		elseif self.状态 == "召唤兽类" then
			if self.选中召唤兽 == 0 then
				tp.提示:写入("#Y/请先选中一只召唤兽")
				return 0
			else
				客户端:发送数据(self.选中召唤兽, 6, 44, self.输入框:取文本()+0, 1)
			end
		elseif self.状态 == "制造类" then
			if self.选中制造 == 0 then
				tp.提示:写入("#Y/请先选中一项技能")
				return 0
			else
				客户端:发送数据(self.选中制造, 15, 44, self.输入框:取文本()+0,self.制造数据[self.选中制造].主技能id.."*-*".. self.制造数据[self.选中制造].包含技能id)
			end
		end
	elseif self.资源组[5]:事件判断() then
		客户端:发送数据(42, 4, 44, "1", 1)
	elseif self.资源组[6]:事件判断() then
		客户端:发送数据(98, 5, 44, "5", 1)
	elseif self.资源组[7]:事件判断() then
		客户端:发送数据(98, 14, 44, "5", 1)
	elseif self.资源组[8]:事件判断() then
		客户端:发送数据(51, 3, 44, self.名称输入框:取文本(), 1)
    elseif self.资源组[11]:事件判断() then
    	self:打开()
        self.物品列表 = {}
	   self.bb列表 = {}
    	客户端:发送数据(98, 12, 44, "5", 1)
	end
	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[3]:显示(self.x+270,self.y+5)
	self.资源组[4]:显示(self.x+175,self.y+334,true)
	self.资源组[8]:显示(self.x+170,self.y+32,true)
	self.资源组[10]:显示(self.x+20,self.y+370,true)
	self.资源组[11]:显示(self.x+120,self.y+370,true)
	self.资源组[12]:显示(self.x+210,self.y+370,true)
	self.名称输入框:置坐标(self.x-225,self.y-105)
	zts:置颜色(0xFF000000)
	zts:显示(self.x+61,self.y+65,窗口标题.名称)
	zts:显示(self.x+204,self.y+65,窗口标题.id)
	for i=5,7 do
		self.资源组[i]:显示(self.x+20+(i-5)*90,self.y+88,true,nil,nil,self.状态==self.资源组[i]:取文字(),2)
	end
	local xx = 0
	local yy = 0
	if self.状态== "物品类" then
	   Picture.物品格子背景:显示(self.x+9+9,self.y+29+90)
	   self.资源组[9]:更新(x,y)
	   self.资源组[9]:显示(self.x+235,self.y+334)
		for i=1,20 do
			if self:取重复物品(i) ~= 0 and self.商品[i].物品 ~= nil then
                self.商品[i].物品.小模型:灰度级()
			end
			self.商品[i]:置坐标(self.x + xx * 51 + 19,self.y + yy * 51 + 28+90)
			self.商品[i]:显示(dt,x,y,self.鼠标)
			if self.商品[i].物品 ~= nil and self.商品[i].焦点 then
				tp.提示:道具行囊(x,y,self.商品[i].物品,true)
				if mouseb(0) then
						self.商品[self.上一次].确定 = false
						self.商品[i].确定 = true
						self.上一次 = i
						self.选中道具=i
						self.输入框:置可视(true,true)
				end
		       if mouseb(1) then
		       	 	if self:取重复物品(i) ~= 0  then
                      客户端:发送数据(i, 2, 44, self.输入框:取文本(), 1)
			        end
			   end
			end
			if self:取重复物品(i) ~= 0 and self.商品[i].物品 ~= nil then
                zts2:置颜色(0xFFFF0000):显示(self.x + xx * 51 + 23,self.y + yy * 51 + 65+90, "出售中")
			end
			xx = xx + 1
			if xx == 5 then
				xx = 0
				yy = yy + 1
			end
		end
		if self.选中道具 ~= 0 then
			self:道具选中事件(self.选中道具)
	    end
	elseif self.状态== "召唤兽类" then
		self.资源组[13]:更新(x,y)
		self.资源组[14]:更新(x,y)
		self.资源组[20]:显示(self.x+9+9,self.y+118)
		tp.竖排花纹背景_:置区域(0,0,18,209)
		tp.竖排花纹背景_:显示(self.x+258,self.y+118)
		self.资源组[13]:显示(self.x+258,self.y+118)
		self.资源组[14]:显示(self.x+258,self.y+310)
        local 序号=0
	    for i=1,#self.召唤兽数据 do
	        序号 =序号 +1
	     	if self.召唤兽数据 [序号]~=nil then
				zts:置颜色(-16777216)
				local jx = self.x+28
				local jy = self.y+111+i*20
				bw:置坐标(jx-4,jy-3)
				local xz = bw:检查点(x,y)
				if self.选中召唤兽 ~= 序号 then
					if  xz and not tp.消息栏焦点 and self.鼠标 then
						box(jx-5,jy-6,jx+230,jy+15,-3551379)
						self.焦点 = true
						self.焦点1 = true
						if mouseb(0) then
			                 self.选中召唤兽 = 序号
                            self.输入框:置可视(true,true)
                            self.输入框:置文本(0)
						elseif mouseb(1)  then
							tp.窗口.召唤兽查看栏:打开(self.召唤兽数据[序号])
						end
					end
                else
					if  xz and not tp.消息栏焦点 and self.鼠标 then
						self.焦点 = true
						self.焦点1 = true
						if mouseb(1)  then
							tp.窗口.召唤兽查看栏:打开(self.召唤兽数据[序号])
						end
					end
						box(jx-5,jy-6,jx+230,jy+15,-10790181)
				end
				 if self:取重复bb(i) ~= 0 then
					zts:置颜色(0xFF00FF40):显示(jx,jy-2,self.召唤兽数据[i].名称)
				else
					zts:置颜色(0xFF000000):显示(jx,jy-2,self.召唤兽数据[i].名称)
				end
			end
		end
		if self.选中召唤兽 ~= 0 then
			self:bb选中事件(self.选中召唤兽)
	    end
    elseif self.状态== "制造类" then
    	self.资源组[19]:显示(self.x+9+9,self.y+118)
    	 local 序号=0
	    for i=1,#self.制造数据 do
	        序号 =序号 +1
	     	if self.制造数据[序号]~=nil then
				zts:置颜色(-16777216)
				local jx = self.x+28
				local jy = self.y+136+i*20
				bw:置坐标(jx-4,jy-3)
				local xz = bw:检查点(x,y)
				if self.选中制造 ~= 序号 then
					if  xz and not tp.消息栏焦点 and self.鼠标 then
						box(jx-5,jy-6,jx+230,jy+15,-3551379)
						self.焦点 = true
						self.焦点1 = true
						if mouseb(0) then
			                 self.选中制造 = 序号
                            self.输入框:置可视(true,true)
                            self.输入框:置文本(0)

						end
					end
                else
					if  xz and not tp.消息栏焦点 and self.鼠标 then
						self.焦点 = true
						self.焦点1 = true
					end
						box(jx-5,jy-6,jx+230,jy+15,-10790181)
				end
				 if self:取重复制造(i) ~= 0 then
					zts:置颜色(0xFF00FF40):显示(jx,jy-2,self.制造数据[i].名称)
					zts:置颜色(0xFF00FF40):显示(jx+110,jy-2,self.制造数据[i].熟练度 or 0)
					zts:置颜色(0xFF00FF40):显示(jx+200,jy-2,self.制造数据[i].等级)
				else
					zts:置颜色(0xFF000000):显示(jx,jy-2,self.制造数据[i].名称)
					zts:置颜色(0xFF000000):显示(jx+110,jy-2,self.制造数据[i].熟练度 or 0)
					zts:置颜色(0xFF000000):显示(jx+200,jy-2,self.制造数据[i].等级)
				end
			end
		end
		if self.选中制造 ~= 0 then
			self:制造选中事件(self.选中制造)
				tp.提示:自定义(self.x-50,self.y+50,SkillData[self.制造数据[self.选中制造].名称].介绍)
	    end
    end
	self.控件类:更新(dt,x,y)
	if self.输入框._已碰撞 or self.名称输入框._已碰撞 then
		self.焦点 = true
	end
	self.控件类:显示(x,y)
end



return 摆摊出售











-- function 摆摊出售类:刷新道具状态(内容)
-- 	self.物品列表[内容.文本.id + 0] = {
-- 		价格 = 内容.文本.价格,
-- 		id = 内容.文本.id
-- 	}
-- end
-- function 摆摊出售类:刷新bb状态(内容)
-- 	self.bb列表[内容.文本.id + 0] = {
-- 		价格 = 内容.文本.价格,
-- 		id = 内容.文本.id
-- 	}
-- end
-- function 摆摊出售类:移除道具(内容)
-- 	self.物品列表[内容.文本 + 0] = nil
-- end
-- function 摆摊出售类:移除bb(内容)
-- 	self.bb列表[内容.文本 + 0] = nil
-- end



-- function 摆摊出售类:更新bb状态(数据)
-- 	self.bb列表 = 数据.文本
-- end








