--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:46
--======================================================================--
local 场景类_召唤兽仓库 = class(窗口逻辑)

local floor = math.floor
local tp,zts,zts1,zts2
local bw = require("gge包围盒")(0,0,138,20)
local box = 引擎.画矩形
local mouseb = 引擎.鼠标弹起
local move = table.move
local insert = table.insert

function 场景类_召唤兽仓库:初始化(根)
	self.ID = 38
	self.x = 214+(全局游戏宽度-800)/2
	self.y = 158
	self.xx = 0
	self.yy = 0
	self.注释 = "召唤兽仓库"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.焦点1 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 自适应.创建(99,1,382,305,3,9),

		[3] = 自适应.创建(4,1,155,220,3,nil),
		[4] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
		[5] = 自适应.创建(93,1,47,22,1,3),
		[6] = 按钮.创建(自适应.创建(103,4,55,22,1,3),0,0,4,true,true,"取出"),
		[7] = 按钮.创建(自适应.创建(103,4,55,22,1,3),0,0,4,true,true,"存入"),
	}
	self.资源组[4]:绑定窗口_(38)
	self.资源组[6]:绑定窗口_(38)
	self.资源组[7]:绑定窗口_(38)
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.华康字体
	zts1 = tp.字体表.华康字体
	zts2 = tp.字体表.华康字体

end

function 场景类_召唤兽仓库:打开(数据)
	if self.可视 then
		self.可视 = false
	else
				  			if  self.x > 全局游戏宽度 then
			self.x = 214+(全局游戏宽度-800)/2
		end
		self.数据 = 数据
		insert(tp.窗口_,self)
		self.选中 = 0
		self.选中1 = 0
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_召唤兽仓库:显示(dt,x,y)
	local bbc = self.数据
	self.资源组[4]:更新(x,y)
	self.资源组[6]:更新(x,y,self.选中1 ~= 0)
	self.资源组[7]:更新(x,y,self.选中 ~= 0 and #bbc > 0)
	if self.资源组[4]:事件判断() then
		self:打开()
		return
	elseif self.资源组[6]:事件判断() then
		客户端:发送数据(self.选中1,6,23,"取出")
		self.选中1 = 0
	elseif self.资源组[7]:事件判断() then
		local bb = self.数据[self.选中]
		if bb == self.数据[self.数据.参战] then
			self.数据.参战 = nil
		end
		if tp.窗口.召唤兽属性栏.可视 then
			if self.数据[tp.窗口.召唤兽属性栏.选中+tp.窗口.召唤兽属性栏.加入] == bb then
				tp.窗口.召唤兽属性栏.名称输入框:置可视(false,false)
				tp.窗口.召唤兽属性栏.加入 = math.max(tp.窗口.召唤兽属性栏.加入 - 1,0)
				tp.窗口.召唤兽属性栏:置形象()
				-- if tp.窗口.道具行囊.可视 and tp.窗口.道具行囊.窗口 == "召唤兽" then
				-- 		tp.窗口.道具行囊.选中召唤兽 = 0
				-- 		tp.窗口.道具行囊.资源组[4] = nil
				-- 		for s=1,3 do
				-- 			tp.窗口.道具行囊.召唤兽装备[s]:置物品(nil)
				-- 		end
				-- end
			end
			if tp.窗口.召唤兽资质栏.可视 then
				tp.窗口.召唤兽资质栏:打开()
			end
			tp.窗口.召唤兽属性栏.选中 = tp.窗口.召唤兽属性栏.选中 - 1
		end
		客户端:发送数据(self.选中,6,23,"存入")

		self.选中 = 0
	end
	self.焦点 = false
	self.焦点1 = nil
	self.资源组[1]:显示(self.x,self.y)
	Picture.标题:显示(self.x+100,self.y)


	zts1:置字间距(1)
	zts1:置颜色(0xFFFFFFFF):显示(self.x+150,self.y+3,"召唤兽仓库")
	zts1:置字间距(0)
	self.资源组[3]:显示(self.x+22,self.y+39)
	self.资源组[3]:显示(self.x+202,self.y+39)
	self.资源组[4]:显示(self.x+359,self.y+3)
	self.资源组[5]:显示(self.x+15,self.y+271)
	self.资源组[6]:显示(self.x+110,self.y+270,true,1)
	self.资源组[7]:显示(self.x+291,self.y+270,true,1)

	for i=1,#self.数据.仓库 do
		zts:置颜色(-16777216)
		local jx = self.x+34
		local jy = self.y+55+i*22
		bw:置坐标(jx-4,jy-3)
		if i ~= self.选中1 then
			if bw:检查点(x,y) then
				box(jx-5,jy-6,jx+138,jy+15,-3551379)
				self.焦点 = true
				if mouseb(0) and self.鼠标 and not tp.消息栏焦点 then
					self.选中1 = i
				end
			end
		else
			local ys = -10790181
			if bw:检查点(x,y) then
				ys = -9670988
				self.焦点1 = true
				self.焦点 = true
				if mouseb(1) and self.鼠标 and not tp.消息栏焦点 then
					tp.窗口.召唤兽查看栏:打开(self.数据.仓库[i])
				end
			end
			box(jx-5,jy-6,jx+138,jy+15,ys)
		end
		zts:显示(jx,jy-2,self.数据.仓库[i].名称)
	end

	for i=1,#bbc do
		if bbc[i] ==  bbc[bbc.参战] then
			zts:置颜色(-65536)
		else
		    zts:置颜色(-16777216)
		end
		local jx = self.x+213
		local jy = self.y+55+i*22
		bw:置坐标(jx-4,jy-3)
		if i ~= self.选中 then
			if bw:检查点(x,y) then
				box(jx-5,jy-6,jx+138,jy+15,-3551379)
				self.焦点 = true
				if mouseb(0) and self.鼠标 and not tp.消息栏焦点 then
					self.选中 = i
				end
			end
		else
			local ys = -10790181
			if bw:检查点(x,y) then
				ys = -9670988
				self.焦点 = true
				self.焦点1 = true
				if mouseb(1) and self.鼠标 and not tp.消息栏焦点 then
					tp.窗口.召唤兽查看栏:打开(bbc[i])
				end
			end
			box(jx-5,jy-6,jx+138,jy+15,ys)
		end
		zts:显示(jx,jy-2,bbc[i].名称)
	end
	zts2:置颜色(-16777216)
	zts2:显示(self.x+24,self.y+274,1)
end



return 场景类_召唤兽仓库