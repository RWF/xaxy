--======================================================================--
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-03-03 18:23:04
--======================================================================--
local 商会购买类 = class()
local tp,zts,zts1
local insert = table.insert
local tos = 引擎.取金钱颜色
local mouseb = 引擎.鼠标弹起
local bw = require("gge包围盒")(0,0,225,170)
function 商会购买类:初始化(根)
  self.ID = 145
  self.x = 0
  self.y = 0
  self.xx = 0
  self.yy = 0
  self.注释 = "商会购买"
  self.可视 = false
  self.鼠标 = false
  self.焦点 = false
  self.可移动 = true
  local 资源 = 根.资源
  local 按钮 = 根._按钮
  local 自适应 = 根._自适应
  local 小型选项栏 = 根._小型选项栏
  self.窗口时间 = 0
  tp = 根
  zts = tp.字体表.普通字体
  zts1 = tp.字体表.描边字体
  self.背景=资源:载入('JM.dll',"网易WDF动画",0xFBCBB632)
  self.关闭=按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true)
  self.上页 = 按钮.创建(自适应.创建(20,4,18,19,4,3),0,0,4,true,true)
  self.上页:绑定窗口_(145)
  self.下页 = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true)
  self.下页:绑定窗口_(145)
  self.确定 = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"确定")
  self.确定:绑定窗口_(145)
  self.离开 = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"离开")
  self.离开:绑定窗口_(145)
  self.分类 = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true)
  self.分类:绑定窗口_(145)
  self.选项 = 小型选项栏.创建(自适应.创建(6,1,45,75,3,9),"商会购买_店铺分类")
end


function 商会购买类:打开(数据)
	if  self.可视 then
	self.可视 = false
	self.显示序列 = 1
	self.坐标重置 = 0
	self.商品选中 = 0
    self.上一次 = 0
	else

	insert(tp.窗口_,self)
	self.可视 = true
	self.店铺信息 = 数据
	self.显示序列 = 1
	self.坐标重置 = 0
	self.商品选中 = 0
    self.上一次 = 0
	for n = 1, #self.店铺信息, 1 do
		self.坐标重置 = self.坐标重置 + 1

		if self.坐标重置 > 8 then
			self.坐标重置 = 1
		end
		self.店铺信息[n].包围盒 = require("gge包围盒")(self.x+150,self.y+192, 222, 20)
		self.店铺信息[n].包围盒:置坐标(self.x+280, self.y+130 + self.坐标重置 * 20)

	 end
     if self.店铺信息.店类 then
        self.当前类型=self.店铺信息.店类
     else
        self.当前类型="全部"
     end
		tp.运行时间 = tp.运行时间 + 3
		self.窗口时间 = tp.运行时间
		self.可视 = true
	end
end

function 商会购买类:显示(dt,x, y)
	self.上页:更新(x,y)
	self.下页:更新(x,y)
	self.确定:更新(x,y)
	self.离开:更新(x,y)
    self.关闭:更新(x,y)
    self.分类:更新(x,y)
    
	self.焦点=false

	if self.上页:事件判断() then
		if self.显示序列 <= 1 then
			tp.提示:写入("#y/当前已经是第一页了")
		else
			self.显示序列 = self.显示序列 - 1
		end
	elseif self.下页:事件判断() then
		if self.显示序列 * 8 + 1 > #self.店铺信息 then
			tp.提示:写入("#y/当前已经是最后一页了")
		else
			self.显示序列 = self.显示序列 + 1
		end
	elseif self.离开:事件判断() or self.关闭:事件判断() then
		self:打开()
    elseif self.分类:事件判断() then
        self.选项:打开({"全部","物品","唤兽"})
        self.选项:置选中(self.当前类型)
    elseif self.选项:事件判断() then
        self.选项:置选中(self.选项.弹出事件)
        self.当前类型=self.选项.弹出事件
        self.选项.弹出事件 = nil
        self:打开()
        if self.当前类型 == "唤兽" then
          客户端:发送数据(2, 141, 36)
        elseif self.当前类型 == "物品" then
          客户端:发送数据(1, 141, 36)
        else 
          客户端:发送数据(3, 141, 36)
        end
	elseif self.确定:事件判断() then
		if self.商品选中 == 0 then
			tp.提示:写入("#y/请先选中一个商店")
		else
            if self.店铺信息[self.商品选中].种类 == "物品" and self.上一次 ~= self.商品选中 then
			     客户端:发送数据(1, 130, 36, self.店铺信息[self.商品选中].商铺id, 1)
                 self.上一次 = self.商品选中
            elseif self.店铺信息[self.商品选中].种类 == "唤兽" and self.上一次 ~= self.商品选中 then
                 客户端:发送数据(2, 130, 36, self.店铺信息[self.商品选中].商铺id, 1)
                 self.上一次 = self.商品选中
            end
		end
	end

	self.背景:显示(self.x+250, self.y+50,true)
    self.关闭:显示(self.x+529,self.y+62,true)
	self.上页:显示(self.x+516, self.y+143,true)
	self.下页:显示(self.x+516, self.y+313,true)
	self.确定:显示(self.x+340, self.y+444,true)
	self.离开:显示(self.x+420, self.y+444,true)


	for n = 1, 8, 1 do
		self.显示起始 = (self.显示序列 - 1) * 8 + n

		if self.店铺信息[self.显示起始] ~= nil then
			if self.商品选中 == self.显示起始 then
				zts:置颜色(0xFFFF0000):显示(self.x+280, self.y+135 + n * 20, self.店铺信息[self.显示起始].店名)
				zts:置颜色(0xFFFF0000):显示(self.x+410, self.y+135 + n * 20, self.店铺信息[self.显示起始].店面 .. "/" .. self.店铺信息[self.显示起始].店面)
				zts:置颜色(0xFFFF0000):显示(self.x+460, self.y+135 + n * 20, self.店铺信息[self.显示起始].种类.."店")
			else
				zts:置颜色(0xFF000000):显示(self.x+280, self.y+135 + n * 20, self.店铺信息[self.显示起始].店名)
				zts:置颜色(0xFF000000):显示(self.x+410, self.y+135 + n * 20, self.店铺信息[self.显示起始].店面 .. "/" .. self.店铺信息[self.显示起始].店面)
				zts:置颜色(0xFF000000):显示(self.x+460, self.y+135 + n * 20, self.店铺信息[self.显示起始].种类.."店")
			end

				if self.店铺信息[self.显示起始].包围盒:检查点(x, y) and mouseb(0) and self.鼠标 then
				   self.商品选中 = self.显示起始
			    end

		end
	end

	if self.商品选中 ~= 0 then
		zts:置颜色(0xFF000000):显示(self.x+280, self.y+348, "本店创始于" .. self.店铺信息[self.商品选中].创店日期 .. "  名店折扣:  0%")
		zts:置颜色(0xFF000000):显示(self.x+280, self.y+365, "店主：" .. self.店铺信息[self.商品选中].店主名称 .. "  ID:" .. self.店铺信息[self.商品选中].店主id)
		zts:置颜色(0xFF000000):显示(self.x+280, self.y+382, self.店铺信息[self.商品选中].宗旨)
        if mouseb(1) and self.店铺信息[self.商品选中].包围盒:检查点(x, y) then

            if self.店铺信息[self.商品选中].种类 == "物品" and self.上一次 ~= self.商品选中 then
                 客户端:发送数据(1, 130, 36, self.店铺信息[self.商品选中].商铺id, 1)
                 self.上一次 = self.商品选中
            elseif self.店铺信息[self.商品选中].种类 == "唤兽" and self.上一次 ~= self.商品选中 then
                 客户端:发送数据(2, 130, 36, self.店铺信息[self.商品选中].商铺id, 1)
                 self.上一次 = self.商品选中
            end

        end
	end
        zts:置颜色(0xFF000000):显示(self.x+465, self.y+123, self.当前类型)




    if bw:检查点(x, y) then
        self.焦点1=true
    else
        self.焦点1=nil
    end
    self.分类:显示(self.x+502, self.y+120,true)
    self.选项:显示(self.x+457,self.y+139,x,y,self.鼠标)
end


function 商会购买类:检查点(x,y)
  if self.背景:是否选中(x,y) then
    return true
  end
end

function 商会购买类:初始移动(x,y)
  tp.运行时间 = tp.运行时间 + 1
  self.窗口时间 = tp.运行时间
  if not self.焦点 then
    tp.移动窗口 = true
  end
  if self.鼠标 and  not self.焦点 then
    self.xx = x - self.x
    self.yy = y - self.y
  end
end

function 商会购买类:开始移动(x,y)
    if self.鼠标 then
        self.x = x - self.xx
        self.y = y - self.yy
    end

    local 临时坐标 = 0
    for n = 1, #self.店铺信息, 1 do
        临时坐标 = 临时坐标 + 1

        if 临时坐标 > 8 then
            临时坐标 = 1
        end
        self.店铺信息[n].包围盒:置坐标(self.x+280, self.y+130 + 临时坐标 * 20)
        bw:置坐标(self.x+280,self.y+155)
     end
end


return 商会购买类
