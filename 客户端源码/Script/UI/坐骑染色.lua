--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:47
--======================================================================--
local 场景类_坐骑染色 = class(窗口逻辑)
local tp,zts,zts1
local insert = table.insert
function 场景类_坐骑染色:初始化(根)
	self.ID = 224
	self.x = 235+(全局游戏宽度-800)/2
	self.y = 158
	self.xx = 0
	self.yy = 0
	self.注释 = "染色"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	local 资源 = 根.资源
	self.资源组 = {
		[1] = 资源:载入('JM.dll',"网易WDF动画",0xD7A969C2),
		[2] = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true),
		[3] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFAB3913C),0,0,4,true,true),
		[4] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x92ABEFD3),0,0,4,true,true),
		[5] = 按钮.创建(自适应.创建(19,4,22,23,4,3),0,0,4,true,true,"1"),
		[6] = 按钮.创建(自适应.创建(19,4,22,23,4,3),0,0,4,true,true,"2"),
		[7] = 按钮.创建(自适应.创建(19,4,22,23,4,3),0,0,4,true,true,"3"),
		[8] = 按钮.创建(自适应.创建(16,4,43,22,1,3),0,0,4,true,true,"确定"),
		[9] = 自适应.创建(3,1,40,22,1,3)
	}
	for i=2,8 do
	  	self.资源组[i]:绑定窗口_(224)
	end
	self.资源组[8]:置偏移(-1,0)
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.描边字体
	self.状态 =1
	self.颜灵果= 0
end

function 场景类_坐骑染色:打开(数据,类型) -- 服饰染色 召唤兽染色
	if self.可视 then
		self.可视 = false
		self.颜灵果= 0
		self.状态 =1
		self.人物动画= nil
		self.坐骑动画= nil
		self.坐骑饰品= nil
	else
		self.状态 =1
		self.类型 = 类型
		if  self.x > 全局游戏宽度 then
		self.x = 235+(全局游戏宽度-800)/2
		end
		self.数据=数据
		insert(tp.窗口_,self)
		self.方向 = 4
        local 资源组 = 引擎.坐骑库(tp.场景.人物.数据.造型,数据.类型,数据.饰品 or "空")

   		self.坐骑动画=  tp.资源:载入(资源组.坐骑资源,"网易WDF动画",资源组.坐骑站立)
   		if 资源组.坐骑饰品站立 ~= nil then
		self.坐骑饰品 = tp.资源:载入(资源组.坐骑饰品资源,"网易WDF动画",资源组.坐骑饰品站立)
		self.染色组2   = self.数据.装备.染色组 or {0,0,0}
		 self.初始染色组2  = {0,0,0}
		 if 数据.装备.染色组  then

			self.初始染色组2[1] = 数据.装备.染色组[1]
			self.初始染色组2[2] = 数据.装备.染色组[2]
			self.初始染色组2[3] = 数据.装备.染色组[3]
		 end
        self.染色方案2 = self.数据.装备.名称
		end

		self.染色方案1 = self.数据.类型
		 self.染色组1   = self.数据.染色组 or {0,0,0}

		 self.初始染色组={0,0,0}
		 if 数据.染色组 then
		 self.初始染色组[1] = 数据.染色组[1]
		self.初始染色组[2] = 数据.染色组[2]
		self.初始染色组[3] = 数据.染色组[3]

      end
		self:置染色()
		self:置方向()
	    tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_坐骑染色:显示(dt,x,y)
	self.焦点 = false
	self.坐骑动画:更新(dt)
	self.资源组[2]:更新(x,y)
	self.资源组[5]:更新(x,y,self.状态~=1)
	self.资源组[3]:更新(x,y)
	self.资源组[4]:更新(x,y)
	self.资源组[8]:更新(x,y)
	if self.资源组[2]:事件判断() then
		self:打开()
		return false
	elseif self.资源组[3]:事件判断() then
		if self.类型== "坐骑" then
			 if self.状态 == 1 then
	              self.染色组1[1] = self.染色组1[1]+1
	        elseif self.状态 == 2 then
	              self.染色组1[2] = self.染色组1[2]+1
	        elseif self.状态 == 3 then
	              self.染色组1[3] = self.染色组1[3]+1
		    end
		    if self.染色组1[1] > 2 then
		        self.染色组1[1] =0
		    end
		    if self.染色组1[2] > 2 then
		        self.染色组1[2] =0
		    end
		    if self.染色组1[3] > 2 then
		        self.染色组1[3] =0
		    end
		else
		    if self.状态 == 1 then
	              self.染色组2[1] = self.染色组2[1]+1
	        elseif self.状态 == 2 then
	              self.染色组2[2] = self.染色组2[2]+1
	        elseif self.状态 == 3 then
	              self.染色组2[3] = self.染色组2[3]+1
		    end
		    if self.染色组2[1] > 2 then
		        self.染色组2[1] =0
		    end
		    if self.染色组2[2] > 2 then
		        self.染色组2[2] =0
		    end
		    if self.染色组2[3] > 2 then
		        self.染色组2[3] =0
		    end
		end
		self:置染色()
		self:价格计算()
	elseif self.资源组[4]:事件判断() then
		self.方向 = self.方向 - 1
		if self.方向 < 0 then
			self.方向 = 7
		end
		self:置方向()
	elseif self.资源组[5]:事件判断() then
       self.状态 = 1
	elseif self.资源组[6]:事件判断() then
	   self.状态 = 2
	elseif self.资源组[7]:事件判断() then
       self.状态 = 3
	elseif self.资源组[8]:事件判断() then
		if self.类型 == "坐骑" then
      	 客户端:发送数据(self.染色组1[1], self.染色组1[2], 60, self.染色组1[3])
      	else
      		客户端:发送数据(self.染色组2[1], self.染色组2[2], 61, self.染色组2[3])
      	end
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x+149,self.y+3)
	self.资源组[5]:显示(self.x+18,self.y+200,true,nil,nil,self.状态==1,2)
	if  (self.类型 == "饰品") or (self.类型 == "坐骑" and self.数据.类型~= "神奇小龟" and self.数据.类型~= "宝贝葫芦" ) then
	self.资源组[7]:更新(x,y,self.状态~=3)
	self.资源组[7]:显示(self.x+74,self.y+200,true,nil,nil,self.状态==3,2)
	end
	self.资源组[6]:显示(self.x+46,self.y+200,true,nil,nil,self.状态==2,2)
	self.资源组[6]:更新(x,y,self.状态~=2)
	self.资源组[3]:显示(self.x+103,self.y+200,nil,nil,nil,nil,nil,2,0,1)
	self.资源组[4]:显示(self.x+130,self.y+200,nil,nil,nil,nil,nil,2,0,1)
	self.资源组[8]:显示(self.x+119,self.y+231)

    zts:置颜色(4294967295)
	zts:显示(self.x+14,self.y+235,"所需颜灵果:")
	zts:显示(self.x+101,self.y+236,self.颜灵果)
	zts1:置字间距(2.5)
	if self.类型 == "坐骑" then
	zts1:显示(self.x+54,self.y+3,"坐骑染色")
	else
	   zts1:显示(self.x+54,self.y+3,"饰品染色")
	end
	zts1:置字间距(0)
	tp.影子:显示(self.x+85,self.y+164)
	self.坐骑动画:显示(self.x+85,self.y+174)
		if self.坐骑饰品 then
			self.坐骑饰品:更新(dt)
	self.坐骑饰品:显示(self.x+85,self.y+174)
    end




end
function 场景类_坐骑染色:价格计算()
	self.颜灵果=0
 if self.类型 =="坐骑" then
	if self.染色组1[1]~=self.初始染色组[1] then
       self.颜灵果= self.颜灵果+10
	end
	if self.染色组1[2] ~=self.初始染色组[2] then
      self.颜灵果= self.颜灵果+10
	end
	if self.染色组1[3] ~=self.初始染色组[3] then
         self.颜灵果= self.颜灵果+10
	end
 else
	if self.染色组2[1]~=self.初始染色组2[1] then
      self.颜灵果= self.颜灵果+10
	end
	if self.染色组2[2] ~=self.初始染色组2[2] then
      self.颜灵果= self.颜灵果+10
	end
	if self.染色组2[3] ~=self.初始染色组2[3] then
         self.颜灵果= self.颜灵果+10
	end
 end

end

function 场景类_坐骑染色:置方向()
	self.坐骑动画:置方向(self.方向)
	if self.坐骑饰品 then
		self.坐骑饰品:置方向(self.方向)
	end
end

function 场景类_坐骑染色:置染色()
	self.坐骑动画:置染色(self.染色方案1,self.染色组1[1],self.染色组1[2],self.染色组1[3])
	if self.坐骑饰品 then
	   self.坐骑饰品:置染色(self.染色方案2,self.染色组2[1],self.染色组2[2],self.染色组2[3])
	end
	self:置方向()
end

return 场景类_坐骑染色