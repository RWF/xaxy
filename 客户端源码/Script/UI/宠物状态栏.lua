-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-12-09 01:58:13

local 场景类_宠物状态栏 = class(窗口逻辑)

local UIfont={"名称","等级","耐力","经验"}
local zts,zts1,tp
local insert = table.insert
function 场景类_宠物状态栏:初始化(根)
	self.ID = 14
	self.x = 260+(全局游戏宽度-800)/2
	self.y = 200
	self.xx = 0
	self.yy = 0
	self.注释 = "宠物状态栏"
		tp = 根
end
function 场景类_宠物状态栏:加载(根)

	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 自适应.创建(99,1,319,191,3,9),
		[2] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
		[3] = 按钮.创建(自适应.创建(103,4,55,22,1,3),0,0,4,true,true,"召唤兽"),
		[4] = 按钮.创建(自适应.创建(103,4,55,22,1,3),0,0,4,true,true,"炼妖"),
		[5] = 按钮.创建(自适应.创建(103,4,55,22,1,3),0,0,4,true,true,"仓库"),
		[7] = 自适应.创建(2,1,97,97,3,9),

		[9] = 自适应.创建(93,1,120,22,1,3),
	}
	for n=2,5 do
	    self.资源组[n]:绑定窗口_(14)
	end
	self.资源组[3]:置偏移(-1,0)
	self.窗口时间 = 0

	zts = tp.字体表.华康字体
	zts1 = tp.字体表.华康字体
end

function 场景类_宠物状态栏:打开(数据)
	print("打开了宠物：")
	if self.可视 then
		self.可视 = false
	else if  self.x > 全局游戏宽度 then
		self.x = 260+(全局游戏宽度-800)/2
    end
    self:加载(tp)
		self.宠物数据=数据
		insert(tp.窗口_,self)
		if self.宠物数据.模型 ~= nil then
			local n = 取模型(self.宠物数据.模型)
			self.资源组[6] = tp.资源:载入(n.资源,"网易WDF动画",n.静立)
			self.资源组[6]:置方向(4)
		end
	    tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_宠物状态栏:显示(dt,x,y)
	local cw = self.宠物数据
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y,cw.模型 ~= nil)
	self.资源组[4]:更新(x,y,cw.模型 ~= nil)
	self.资源组[5]:更新(x,y,cw.模型 ~= nil)
	if cw.模型 ~= nil then
		self.资源组[6]:更新(dt)
	end
	if self.资源组[2]:事件判断() then
		self:打开()
		return false
	elseif self.资源组[3]:事件判断() then
		客户端:发送数据(6, 1, 22, "6A", 1)
	elseif self.资源组[4]:事件判断() then
		if tp.窗口.宠物炼妖栏.可视 then
			tp.窗口.宠物炼妖栏:打开()
		else
		客户端:发送数据(6321, 22, 13, "包裹", 1) --炼妖
	    end
	elseif self.资源组[5]:事件判断() then
		客户端:发送数据(1,5,23,"66")
	end
	self.资源组[1]:显示(self.x,self.y)
	Picture.标题:显示(self.x+70,self.y)

 zts1:置字间距(3)
	zts1:置颜色(0xFFFFFFFF):显示(self.x+136,self.y+3,"宠物")
	self.资源组[2]:显示(self.x + 293,self.y + 6)
	self.资源组[3]:显示(self.x + 30,self.y + 155,true,1)
	self.资源组[4]:显示(self.x + 131,self.y + 155,true,1)
	self.资源组[5]:显示(self.x + 232,self.y + 155,true,1)

   
	
	for i=0,3 do
		self.资源组[9]:显示(self.x+175,self.y+36+i*30)
		zts1:显示(self.x +134,self.y + 41+i*30,UIfont[i+1])
	end
	zts1:置字间距(0)
	self.资源组[7]:显示(self.x + 30,self.y + 42)
	if cw.模型 ~= nil then
		if self.资源组[6] == nil and self.资源组[7] == nil then
			local n = 取模型(cw.模型)
			self.资源组[6] = tp.资源:载入(n.资源,"网易WDF动画",n.静立)
			self.资源组[6]:置方向(4)
		end
		tp.影子:显示(self.x + 78,self.y + 113)
		self.资源组[6]:显示(self.x + 78,self.y + 113)
		zts:置颜色(-16777216)
		zts:显示(self.x + 183,self.y + 40,cw.名称)
		zts:显示(self.x + 183,self.y + 70,cw.等级.."/"..cw.最大等级)
		zts:显示(self.x + 183,self.y + 100,cw.耐力.."/"..cw.最大耐力)
		zts:显示(self.x + 183,self.y + 130,cw.经验.."/"..cw.最大经验)
	end
end



return 场景类_宠物状态栏