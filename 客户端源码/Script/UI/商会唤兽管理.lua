--======================================================================--
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-03-03 18:22:15
--======================================================================--
local 商会唤兽管理 = class()
local tp,zts,zts1
local insert = table.insert
local tos = 引擎.取金钱颜色
local mouseb = 引擎.鼠标弹起
local bw = require("gge包围盒")(0,0,230,20)
local bw1 = require("gge包围盒")(0,0,230,20)
local box = 引擎.画矩形
function 商会唤兽管理:初始化(根)
  self.ID = 144
  self.x = 0
  self.y = 0
  self.xx = 0
  self.yy = 0
  self.注释 = "商会唤兽管理"
  self.可视 = false
  self.鼠标 = false
  self.焦点 = false
  self.可移动 = true
  local 资源 = 根.资源
  local 按钮 = 根._按钮
  local 自适应 = 根._自适应
  self.窗口时间 = 0
  tp = 根
  zts = tp.字体表.普通字体
  zts1 = tp.字体表.普通字体__
  self.背景=资源:载入('JM.dll',"网易WDF动画",0x57CBA2BE)
  self.改名 = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"改名")
  self.改名:绑定窗口_(144)
  self.上间 = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"上间")
  self.上间:绑定窗口_(144)
  self.下间 = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"下间")
  self.下间:绑定窗口_(144)
  self.离开 = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"离开")
  self.离开:绑定窗口_(144)
  self.上柜 = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"上柜")
  self.上柜:绑定窗口_(144)
  self.更改宗旨 =  按钮.创建(自适应.创建(12,4,75,22,1,3),0,0,4,true,true,"更改宗旨")
  self.更改宗旨:绑定窗口_(144)
  self.更改费用 =  按钮.创建(自适应.创建(12,4,75,22,1,3),0,0,4,true,true,"更改费用")
  self.更改费用:绑定窗口_(144)
  self.投入资金 =  按钮.创建(自适应.创建(12,4,75,22,1,3),0,0,4,true,true,"投入资金")
  self.投入资金:绑定窗口_(144)
  self.取出资金 =  按钮.创建(自适应.创建(12,4,75,22,1,3),0,0,4,true,true,"取出资金")
  self.取出资金:绑定窗口_(144)
  self.查看账单 =  按钮.创建(自适应.创建(12,4,75,22,1,3),0,0,4,true,true,"查看账单")
  self.查看账单:绑定窗口_(144)
  self.出售登记 =  按钮.创建(自适应.创建(12,4,75,22,1,3),0,0,4,true,true,"出售登记")
  self.出售登记:绑定窗口_(144)
  self.扩张柜台 =  按钮.创建(自适应.创建(12,4,75,22,1,3),0,0,4,true,true,"扩张柜台")
  self.扩张柜台:绑定窗口_(144)
  self.削减柜台 =  按钮.创建(自适应.创建(12,4,75,22,1,3),0,0,4,true,true,"削减柜台")
  self.削减柜台:绑定窗口_(144)
  self.暂停营业 =  按钮.创建(自适应.创建(12,4,75,22,1,3),0,0,4,true,true,"暂停营业")
  self.暂停营业:绑定窗口_(144)
  self.放回身上 =  按钮.创建(自适应.创建(12,4,75,22,1,3),0,0,4,true,true,"放回身上")
  self.放回身上:绑定窗口_(144)
  self.玩家背景=自适应.创建(2,1,241,209,3,9)
  self.商会背景=自适应.创建(2,1,241,209,3,9)
 self.玩家道具={}
 self.玩家选中=0
 self.商会道具={}
 self.商会选中=0
  self.控件类 = require("ggeui/加载类")()
  local 总控件 = self.控件类:创建控件('商店总控件')
  总控件:置可视(true,true)
  self.上柜银两输入=总控件:创建输入("数量输入11",self.x + 190,self.y + 378,200,14)
  self.上柜银两输入:置数字模式()
  self.上柜银两输入:置限制字数(9)
  self.上柜银两输入:置文字颜色(0xFF000000)
  self.上柜银两输入:置可视(false,false)
  self.商铺宗旨输入=总控件:创建输入("数量输入22",self.x +429,self.y + 92,280,16)
  self.商铺宗旨输入:置可视(false,false)
  self.商铺宗旨输入:置限制字数(34)
  self.商铺宗旨输入:置文字颜色(0xFF000000)
  self.商铺改名输入=总控件:创建输入("数量输入33",self.x +112,self.y + 85,220,16)
  self.商铺改名输入:置可视(false,false)
  self.商铺改名输入:置限制字数(14)
  self.商铺改名输入:置文字颜色(0xFF000000)
end

function 商会唤兽管理:打开(数据)
  if  self.可视 then
    self.可视 = false
    self.玩家道具={}
    self.玩家选中=0
    self.商会道具={}
    self.商会选中=0

  else

  if  self.x > 全局游戏宽度 then
  self.x = 0
  end
  self.玩家道具={}
  self.玩家选中=0
  self.商会道具={}
  self.商会选中=0
  insert(tp.窗口_,self)
  self.店铺信息=数据
  self.当前店面=1
  self.物品数据={}
  self.物品数据1={}
  self.商铺改名输入:置文本(self.店铺信息.店名)
  self.商铺改名输入:置可视(true,"商铺改名输入")
  self.商铺宗旨输入:置文本(self.店铺信息.宣言)
  self.商铺宗旨输入:置可视(true,"商铺改名输入")
  if self.店铺信息.营业==false then
  self.暂停营业:置文字("开始营业")
  else
  self.暂停营业:置文字("暂停营业")
  end

    tp.运行时间 = tp.运行时间 + 3
    self.窗口时间 = tp.运行时间
    self.可视 = true
  end
end
function 商会唤兽管理:刷新柜台道具(数据)
    self.当前店面=数据.类型
    self.商会选中=0
    self.物品数据1={}
    self.物品数据1=数据
    self.物品数据1.类型=nil
 end
function 商会唤兽管理:刷新玩家道具(数据)
    self.玩家选中=0
    self.物品数据={}
    self.物品数据=数据
 end
function 商会唤兽管理:显示(dt,x,y)
  self.焦点=false
  self.商铺改名输入:置坐标(self.x,self.y)
  self.上柜银两输入:置坐标(self.x,self.y)
  self.商铺宗旨输入:置坐标(self.x,self.y)
  self.改名:更新(x,y)
  self.上间:更新(x,y)
  self.下间:更新(x,y)
  self.上柜:更新(x,y)
  self.离开:更新(x,y)
 self.查看账单:更新(x,y)
 self.出售登记:更新(x,y)
 self.扩张柜台:更新(x,y)
 self.削减柜台:更新(x,y)
 self.放回身上:更新(x,y)
 self.暂停营业:更新(x,y)
 self.更改宗旨:更新(x,y)
 self.更改费用:更新(x,y)
 self.取出资金:更新(x,y)
 self.投入资金:更新(x,y)
  if self.离开:事件判断() then
    self:打开()
  elseif self.改名:事件判断() then
    if self.商铺改名输入:取文本()=="" then
    tp.提示:写入("#y/请先取个店名")
    else
    客户端:发送数据(983,134,36,self.商铺改名输入:取文本(),1)
    end
  elseif self.更改宗旨:事件判断() then
    if self.商铺宗旨输入:取文本()=="" then
    tp.提示:写入("#y/宗旨总不能什么话都没有吧")
    else
    客户端:发送数据(761,135,36,self.商铺宗旨输入:取文本(),1)
    end
  elseif self.扩张柜台:事件判断() then
    客户端:发送数据(275,136,36,"8A",1)
     self:打开()
  elseif self.出售登记:事件判断() then
    if self.玩家选中==0 then
    tp.提示:写入("#y/请先选中一个召唤兽")
    else
    客户端:发送数据(self.玩家选中,133,36,self.当前店面,1)
    end
  elseif self.上柜:事件判断() then
      if self.商会选中==0 then
      tp.提示:写入("#y/请先选中一个召唤兽")
      elseif self.上柜银两输入:取文本()=="" or self.上柜银两输入:取文本()+0<1 then
      tp.提示:写入("#y/请先输入价格")
      else
      客户端:发送数据(self.商会选中,self.当前店面,37,self.上柜银两输入:取文本(),2)
      end
  elseif self.放回身上:事件判断() then
      if self.商会选中==0 then
       tp.提示:写入("#y/请先选中一个召唤兽")
      else
      客户端:发送数据(self.商会选中,137,36,self.当前店面,1)
      end
  elseif self.暂停营业:事件判断() then
      客户端:发送数据(87842,138,36,"1",1)
  elseif self.上间:事件判断() then
      客户端:发送数据(2,127,36,self.当前店面,1)
  elseif self.下间:事件判断() then
      客户端:发送数据(2,128,36,self.当前店面,1)
  elseif self.取出资金:事件判断() then
      客户端:发送数据(2,129,36,"1",1)
  end
 self.背景:显示(self.x+100,self.y+50)
 self.玩家背景:显示(self.x+115,self.y+161)
 self.商会背景:显示(self.x+420,self.y+205)
  if self.玩家背景:是否选中(x,y) or self.商会背景:是否选中(x,y) then
     self.焦点1=true
  else
     self.焦点1=nil
  end
 zts:置颜色(0xFFFFFFFF):显示(self.x+189,self.y+110,self.店铺信息.创店日期)
 zts:置颜色(tos(self.店铺信息.现金+0)):显示(self.x+454,self.y+181,self.店铺信息.现金)
 zts:置颜色(0xFFFFFFFF):显示(self.x+189,self.y+403,"0%")
 zts:置颜色(0xFFFFFFFF):显示(self.x+189,self.y+423,"10%")
 zts:置颜色(0xFF000000):显示(self.x+306,self.y+85,self.店铺信息.类型.."店")
 zts:置颜色(0xFF000000):显示(self.x+186,self.y+137,self.店铺信息.店主名称)
 zts:置颜色(0xFF000000):显示(self.x+334,self.y+138,self.店铺信息.店主id)
 zts:置颜色(0xFF000000):显示(self.x+523,self.y+134,self.店铺信息.基础运营资金)
 zts:置颜色(0xFF000000):显示(self.x+523,self.y+159,self.店铺信息.日常运营资金)
 zts:置颜色(0xFF000000):显示(self.x+157,self.y+446,self.当前店面.."/"..self.店铺信息.店面)
 self.改名:显示(self.x+215,self.y+83,true)
 self.上间:显示(self.x+196,self.y+443,true)
 self.下间:显示(self.x+240,self.y+443,true)
 self.上柜:显示(self.x+284,self.y+375,true)
 self.离开:显示(self.x+637,self.y+443,true)
 self.查看账单:显示(self.x+215,self.y+413,true)
 self.放回身上:显示(self.x+295,self.y+413,true)
 self.扩张柜台:显示(self.x+284,self.y+443,true)
 self.削减柜台:显示(self.x+360,self.y+443,true)
 self.出售登记:显示(self.x+436,self.y+443,true)
 self.暂停营业:显示(self.x+556,self.y+443,true)
 self.更改宗旨:显示(self.x+605,self.y+130,true)
 self.更改费用:显示(self.x+605,self.y+155,true)
 self.取出资金:显示(self.x+605,self.y+180,true)
 self.投入资金:显示(self.x+530,self.y+180,true)
self.控件类:更新(dt,x,y)
self.控件类:显示(x,y)
  if self.商铺改名输入._已碰撞 or self.上柜银两输入._已碰撞 or self.商铺宗旨输入._已碰撞 then
    self.焦点 = true
  end

  self:玩家道具显示(dt,x,y)
  self:柜台道具显示(dt,x,y)
end

function 商会唤兽管理:玩家道具显示(dt,x,y)
      for i=1,#self.物品数据 do
        if self.物品数据[i]~=nil then
            zts:置颜色(-16777216)
            local jx = self.x+431
            local jy = self.y+195+i*20
            bw:置坐标(jx-4,jy-3)
            local xz = bw:检查点(x,y)
            if self.玩家选中 ~= i then
                if  xz and not tp.消息栏焦点 and self.鼠标 then
                   box(jx-5,jy-6,jx+226,jy+15,-3551379)
                    if mouseb(0) then
                        self.玩家选中 = i
                    end
                end
            else
                if  xz and not tp.消息栏焦点 and self.鼠标 then
                    if mouseb(0)  then
                      客户端:发送数据(self.玩家选中,133,36,self.当前店面,1)
                    elseif mouseb(1)  then
                      tp.窗口.召唤兽查看栏:打开(self.物品数据[i])
                    end
                end
                box(jx-5,jy-6,jx+226,jy+15,-10790181)
            end
            zts:置颜色(0xFF000000):显示(jx,jy-2,self.物品数据[i].名称)
       end
    end
 end

function 商会唤兽管理:柜台道具显示(dt,x,y)
      for i=1,#self.物品数据1 do
        if self.物品数据1[i].道具~=nil then
            zts:置颜色(-16777216)
            local jx = self.x+126
            local jy = self.y+151+i*20
            bw1:置坐标(jx-4,jy-3)
            local xz = bw1:检查点(x,y)
            if self.商会选中 ~= i then
                if  xz and not tp.消息栏焦点 and self.鼠标 then
                  box(jx-5,jy-6,jx+226,jy+15,-3551379)
                    if mouseb(0) then
                      self.商会选中=i
                      self.上柜银两输入:置可视(true,true)
                      self.上柜银两输入:置文本(self.物品数据1[i].价格)
                        if self.物品数据1[i].状态 then
                          self.上柜:置文字("下柜")
                        else
                          self.上柜:置文字("上柜")
                        end
                    end
                end
            else
                if  xz and not tp.消息栏焦点 and self.鼠标 then
                    if mouseb(0) then
                      客户端:发送数据(self.商会选中,137,36,self.当前店面,1)
                    elseif mouseb(1)  then
                      tp.窗口.召唤兽查看栏:打开(self.物品数据1[i].道具)    
                    end
                end
                box(jx-5,jy-6,jx+226,jy+15,-10790181)
            end
            if self.物品数据1[i].状态 then
                zts:置颜色(0xFF00FF40):显示(jx,jy-2,self.物品数据1[i].道具.名称)
            else
                zts:置颜色(0xFF000000):显示(jx,jy-2,self.物品数据1[i].道具.名称)
            end
       end
    end
 end


function 商会唤兽管理:检查点(x,y)
  if self.背景:是否选中(x,y)  then
    return true
  end
end

function 商会唤兽管理:初始移动(x,y)
  tp.运行时间 = tp.运行时间 + 1
  self.窗口时间 = tp.运行时间
  if not self.焦点 then
    tp.移动窗口 = true
  end
  if self.鼠标 and  not self.焦点 then
    self.xx = x - self.x
    self.yy = y - self.y
  end
end

function 商会唤兽管理:开始移动(x,y)
  if self.鼠标 then
    self.x = x - self.xx
    self.y = y - self.yy
  end
end



return 商会唤兽管理