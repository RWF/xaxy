--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:47
--======================================================================--

local 合成旗类 = class()
local insert = table.insert
local tp,zts1,zts
local mousea = 引擎.鼠标弹起
local fgz = 分割字符
local number = tonumber
local string = tostring
function 合成旗类:初始化(根)
	self.ID = 65
	self.xx = 0
	self.yy = 0
	self.注释 = "合成旗类"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true

	self.窗口时间 = 0
	tp = 根
	self.背景窗口 = tp._自适应.创建(99,1,0,0,3,9)
	zts = 根.字体表.普通字体
	zts1 = 根.字体表.描边字体
	zts1:置颜色(0xFFFFFFFF)
self.坐标数据 = {}
self.地图数据={
[1001]={x=11000,y=5600}
,[1501]={x=5760,y=2880}
,[1070]={x=3200,y=4200}
,[1092]={x=4480,y=3020}
,[1208]={x=190*20,y=120*20}
,[1173]={x=633*20,y=112*20}
,[1114]={x=128*20,y=96*20}
,[1174]={x=228*20,y=170*20}
,[1110]={x=352*20,y=336*20}
,[1111]={x=253*20,y=168*20}
}
end

function 合成旗类:打开(数据,格子)
	if self.可视 then
		self.可视 = false
		self.坐标数据 = {}
	else
  
    self.数据=数据
    self.格子 = 格子
    self.资源组=tp.资源:载入( 'smap.dll',"网易WDF动画",MapData[数据.地图编号].小地图)
	self.高度 = self.资源组:取高度()
	self.宽度 = self.资源组:取宽度()
	self.x = math.floor((800 - self.宽度) /2)
	self.y = math.floor((600 - self.高度) /2)
	local fg = {}
	fgz(string(self.高度),fg)
	fg = number(fg[3])
	local v = 27
	if fg == 1 or fg == 3 or fg == 5 or fg == 7 or fg == 9 then
	v = 26
	end
    		self.x = self.x or floor((全局游戏宽度 - self.宽度) /2)
    		self.y = self.y or floor((全局游戏高度 - self.高度) /2)
	self.背景窗口:置宽高(self.宽度+27,self.高度+v+15)
	for n = 1, #数据.坐标 do
		self.坐标数据[n] = {
			图片 =tp._按钮(tp.资源:载入('JM.dll',"网易WDF动画",0x146BB550)),
			x = 数据.坐标[n].x*20,
			y = 数据.坐标[n].y*20
		}
		self.坐标数据[n].图片:绑定窗口_(65)
	end
		self.偏移X = (self.宽度 - 20) / self.地图数据[数据.地图编号].x
		self.偏移Y = (self.高度 - 20)/ self.地图数据[数据.地图编号].y
		insert(tp.窗口_,self)
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
		self.可视 = true
	end
end



function 合成旗类:显示(dt,x,y)
 self.焦点 = false

for n = 1, #self.坐标数据, 1 do
	self.坐标数据[n].图片:更新(x,y)
	if self.坐标数据[n].图片:事件判断() then
		-- 客户端:发送数据(4061,{类型=self.数据.类型,格子=self.数据.格子,参数 = n})

		客户端:发送数据(self.格子, n, 16)
		self:打开()
		break
	end
end
self.背景窗口:显示(self.x-16,self.y-32)
self.资源组:显示(self.x,self.y)

for n = 1, #self.坐标数据, 1 do
	self.坐标数据[n].图片:显示(self.x+self.坐标数据[n].x*self.偏移X+13,self.y+8+self.坐标数据[n].y*self.偏移Y)
end

end
function 合成旗类:检查点(x,y)
	if self.资源组:是否选中(x,y) then
		return true
	end
end

function 合成旗类:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 合成旗类:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 合成旗类





