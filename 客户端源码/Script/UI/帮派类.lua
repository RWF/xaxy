--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:48
--======================================================================--
local 帮派类 = class()
local tp,zts,zts1
local insert = table.insert
local bw = require("gge包围盒")(0,0,40,22)
local box = 引擎.画矩形
function 帮派类:初始化(根)
	self.ID = 140
	self.x = 200+(全局游戏宽度-800)/2
	self.y = 70
	self.xx = 0
	self.yy = 0
	self.注释 = "帮派类"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.背景 = 资源:载入('JM.dll',"网易WDF动画",0xEA1D3980)
	self.加入 = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"加入")
	self.加入:绑定窗口_(140)
	self.取消 = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"取消")
	self.取消:绑定窗口_(140)
	self.关闭 = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true)
	self.关闭:绑定窗口_(140)
	self.增加 = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true)
	self.增加:绑定窗口_(140)
	self.减少 = 按钮.创建(自适应.创建(20,4,18,19,4,3),0,0,4,true,true)
	self.减少:绑定窗口_(140)
	self.更改职务 = 按钮.创建(自适应.创建(16,4,75,22,1,3),0,0,4,true,true,"更改职务")
	self.更改职务:绑定窗口_(140)
	self.申请列表 =  按钮.创建(自适应.创建(16,4,75,22,1,3),0,0,4,true,true,"申请名单")
	self.申请列表:绑定窗口_(140)
	self.踢出帮派 =  按钮.创建(自适应.创建(16,4,75,22,1,3),0,0,4,true,true,"请离帮派")
	self.踢出帮派:绑定窗口_(140)
	self.脱离帮派 =  按钮.创建(自适应.创建(16,4,75,22,1,3),0,0,4,true,true,"脱离帮派")
	self.脱离帮派:绑定窗口_(140)

	--self.选中背景 = 图像类("imge/001/lsbj.png")


	self.超丰富文本 = 根._丰富文本(142,127,根.字体表.普通字体)
	self.超丰富文本:添加元素("h", 0xFF000000)
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.普通字体
end


function 帮派类:打开(数据)
	if  self.可视 then
		self.超丰富文本:清空()
		self.可视 = false
		self.显示序列 = 1
		self.选中编号 = 0
	else
								                    if  self.x > 全局游戏宽度 then
self.x = 200+(全局游戏宽度-800)/2
    end
		insert(tp.窗口_,self)
		self.数据 = 数据
		self.可视 = true
	self.显示序列 = 1
	self.选中编号 = 0
	self.超丰富文本:清空()
	self.超丰富文本:添加文本("#h/" .. self.数据.宗旨)
	self.成员显示 = {}
	for n, v in pairs(self.数据.成员名单) do
		if self.数据.成员名单[n].职务 == "帮主" then
			self.成员显示[#self.成员显示 + 1] = table.loadstring(table.tostring(self.数据.成员名单[n]))
			self.数据.成员名单[n].显示状态 = 1
		end
	end
	for n, v in pairs(self.数据.成员名单) do
		if self.数据.成员名单[n].职务 == "副帮主" then
			self.成员显示[#self.成员显示 + 1] = table.loadstring(table.tostring(self.数据.成员名单[n]))
			self.数据.成员名单[n].显示状态 = 1
		end
	end
	for n, v in pairs(self.数据.成员名单) do
		if self.数据.成员名单[n].职务 == "左护法" then
			self.成员显示[#self.成员显示 + 1] = table.loadstring(table.tostring(self.数据.成员名单[n]))
			self.数据.成员名单[n].显示状态 = 1
		end
	end
	for n, v in pairs(self.数据.成员名单) do
		if self.数据.成员名单[n].职务 == "右护法" then
			self.成员显示[#self.成员显示 + 1] = table.loadstring(table.tostring(self.数据.成员名单[n]))
			self.数据.成员名单[n].显示状态 = 1
		end
	end
	for n, v in pairs(self.数据.成员名单) do
		if self.数据.成员名单[n].职务 == "长老" then
			self.成员显示[#self.成员显示 + 1] = table.loadstring(table.tostring(self.数据.成员名单[n]))
			self.数据.成员名单[n].显示状态 = 1
		end
	end
	for n, v in pairs(self.数据.成员名单) do
		if self.数据.成员名单[n].在线 and self.数据.成员名单[n].显示状态 == nil then
			self.成员显示[#self.成员显示 + 1] = table.loadstring(table.tostring(self.数据.成员名单[n]))
			self.数据.成员名单[n].显示状态 = 1
		end
	end
	for n, v in pairs(self.数据.成员名单) do
		if self.数据.成员名单[n].在线 == false and self.数据.成员名单[n].显示状态 == nil then
			self.成员显示[#self.成员显示 + 1] = table.loadstring(table.tostring(self.数据.成员名单[n]))
			self.数据.成员名单[n].显示状态 = 1
		end
	end
	for n = 1, #self.成员显示, 1 do
		self.成员显示[n].包围盒 = require("gge包围盒")(150, 192, 140, 15)
		--self.成员显示[n].包围盒:更新宽高(140, 15)
	end

		tp.运行时间 = tp.运行时间 + 3
		self.窗口时间 = tp.运行时间
		self.可视 = true
	end
end



function 帮派类:检查点(x,y)
	if self.背景:是否选中(x,y)  then
		return true
	end
end

function 帮派类:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	self.窗口时间 = tp.运行时间
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end


function 帮派类:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

function 帮派类:显示(dt,x,y)
	self.关闭:更新(x,y)
	self.减少:更新(x,y)
	self.增加:更新(x,y)
	self.更改职务:更新(x,y)
	self.申请列表:更新(x,y)
	self.踢出帮派:更新(x,y)
	self.脱离帮派:更新(x,y)
	self.焦点=false
	if self.关闭:事件判断() then
		self:打开()
	elseif self.减少:事件判断() then
		if self.显示序列 <= 1 then
			self.显示序列 = 1
		else
			self.显示序列 = self.显示序列 - 1
		end
	elseif self.增加:事件判断() then
		if self.显示序列 + 5 > #self.成员显示 then
			tp.提示:写入("#y/没有更多的帮派成员了")
		else
			self.显示序列 = self.显示序列 + 1
		end
	elseif self.申请列表:事件判断() then
		客户端:发送数据(6, 65, 13, "5")
	elseif self.脱离帮派:事件判断() then
		客户端:发送数据(6, 69, 13, "5")
	elseif self.踢出帮派:事件判断() then
		if self.选中编号 == 0 then
			tp.提示:写入("#y/请先选中一个帮派成员")
        else
        	客户端:发送数据(self.成员显示[self.选中编号].id, 70, 13, "5")
		end

	elseif self.更改职务:事件判断() then
		if self.选中编号 == 0 then
			tp.提示:写入("#y/请先选中一个帮派成员")
         else
         	客户端:发送数据(self.成员显示[self.选中编号].id, 71, 13, "5")
		end


	end
	self.背景:显示(self.x, self.y)
	self.关闭:显示(self.x+382, self.y+6)
	self.减少:显示(self.x+178, self.y+210)
	self.增加:显示(self.x+178, self.y+358)
	self.更改职务:显示(self.x+15, self.y+387,true)
	self.申请列表:显示(self.x+115, self.y+387,true)
	self.踢出帮派:显示(self.x+215, self.y+387,true)
	self.脱离帮派:显示(self.x+315, self.y+387,true)
	zts:置颜色(0xFF000000)
	zts:显示(self.x+94, self.y+34, self.数据.名称)
	zts:显示(self.x+94, self.y+56, self.数据.创始人.名称)
	zts:显示(self.x+94, self.y+78, self.数据.现任帮主.名称)
	zts:显示(self.x+94, self.y+100, self.数据.敌对.名称)
	zts:显示(self.x+94, self.y+122, self.数据.掌控区域.名称)
	zts:显示(self.x+94, self.y+144, self.数据.当前内政.名称)
	zts:显示(self.x+94, self.y+166, self.数据.同盟.名称)
	zts:显示(self.x+114, self.y+188, self.数据.行动力)
	zts:显示(self.x+284, self.y+34, self.数据.当前人数 .. "/" .. self.数据.人数上限)
	zts:显示(self.x+284, self.y+56, self.数据.资金)
	zts:显示(self.x+284, self.y+78, self.数据.安定)
	zts:显示(self.x+284, self.y+100, self.数据.人气)
	zts:显示(self.x+284, self.y+122, self.数据.繁荣)
	zts:显示(self.x+284, self.y+144, self.数据.规模)
	zts:显示(self.x+284, self.y+166, self.数据.编号)
	zts:显示(self.x+324, self.y+188, self.数据.敌对.编号)
	self.超丰富文本:显示(self.x+217, self.y+243)
	self.显示位置 = 0
	for n = 1, #self.成员显示, 1 do
		if self.显示序列 <= n and n <= self.显示序列 + 5 then
			self.显示位置 = n - self.显示序列 + 1

			 if self.选中编号 == n then
			 	--self.选中背景:显示(225, 317 + self.显示位置 * 20 - 20)


              box(self.x+25,self.y +245 + self.显示位置 * 20 ,self.x + 165,self.y +245 + self.显示位置 * 20 - 20,-9670988)
			 end

			if self.成员显示[n].在线 then
				zts1:置颜色(0xFF00FF00):显示(self.x+29, self.y+247 + self.显示位置 * 20 - 20, "[" .. self.成员显示[n].职务 .. "]")
				zts1:置颜色(0xFF00FF00):显示(self.x+89, self.y+247 + self.显示位置 * 20 - 20, self.成员显示[n].名称)
			else
				zts1:置颜色(0xFF000000):显示(self.x+29, self.y+247 + self.显示位置 * 20 - 20, "[" .. self.成员显示[n].职务 .. "]")
				zts1:置颜色(0xFF000000):显示(self.x+89, self.y+247 + self.显示位置 * 20 - 20, self.成员显示[n].名称)
			end

			self.成员显示[n].包围盒:置坐标(self.x+29, self.y+246 + self.显示位置 * 20 - 20)

			if self.成员显示[n].包围盒:检查点(x,y) and 引擎.鼠标弹起(0) then
				self.选中编号 = n
			end
		end
	end
end

return 帮派类
