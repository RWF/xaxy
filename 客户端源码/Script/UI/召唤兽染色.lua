-- @Author: 作者QQ381990860
-- @Date:   2021-08-13 19:47:23
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-12 01:31:47
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2020-11-17 01:27:01
--======================================================================--
local 场景类_召唤兽染色 = class(窗口逻辑)
local min = math.min
local floor = math.floor
local tp,zts
local mouseb = 引擎.鼠标弹起
local box = 引擎.画矩形
local insert = table.insert

function 场景类_召唤兽染色:初始化(根)
	self.ID = 151
	self.x = 376
	self.y = 41
	self.xx = 0
	self.yy = 0
	self.注释 = "召唤兽染色"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.焦点1 =false
	self.状态 = "原"
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 自适应.创建(99,1,305,451,3,9),

		[3] = 资源:载入('JM.dll',"网易WDF动画",0xA19838E8),
		[4] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
		[5] = 自适应.创建(93,1,140,22,1,3),
		[6] = 按钮.创建(自适应.创建(105,4,21,20,1,3),0,0,4,true,true,"原"),

		[100] = 按钮.创建(自适应.创建(103,4,43,22,1,3),0,0,4,true,true,"染色"),
	}
	self.资源组[4]:绑定窗口_(151)
		self.资源组[100]:绑定窗口_(151)
		self.资源组[6]:置偏移(-5,-1)
		self.资源组[6]:绑定窗口_(151)
	for n=7,87 do
		self.资源组[n]=按钮.创建(自适应.创建(105,4,21,20,1,3),0,0,4,true,true,""..n-6)
	    if n >= 16 then
	     self.资源组[n]:置偏移(-4,-1)

	   	
	   end
	   self.资源组[n]:绑定窗口_(151)
   end

    self.介绍文本 = 根._丰富文本(115,280,根.字体表.普通字体)
     	for n=0,119 do
   		self.介绍文本:添加元素(n,根.包子表情动画[n])
     end
    self.介绍文本:添加文本("#G/点击数字按钮可预览对象的变色效果,每次扣除银子#Y/1亿#G/两,本服支持#R/81#G/种染色效果,染色以后包括召唤兽武器和饰品同时染色,每种效果都不一样/n#91#91")

	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.华康字体

	self.选中 = 0
	self.加入 = 0
end

function 场景类_召唤兽染色:打开(数据,编号)
	self.资源组[101]=nil
	self.资源组[102]=nil
	if self.可视 then
		self.可视 = false
	else
		self.数据 =数据
		self.编号=编号
      self.染色 = nil
		local n = 取模型(数据.造型)
		self.资源组[101] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
		if 数据.饰品  then
			n = 取模型("饰品_"..数据.造型)
			self.资源组[102] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
			self.资源组[102]:置方向(0)
		elseif  取召唤兽武器(数据.造型)  then
             local ns=取模型("武器_"..数据.造型)
			  self.资源组[102] = tp.资源:载入(ns.战斗资源,"网易WDF动画",ns.待战)
		     self.资源组[102]:置方向(0)
		end


		if 数据.染色方案 ~= nil then
			self.资源组[101]:置染色(数据.染色方案,数据.染色组[1],数据.染色组[2],数据.染色组[3])
			if self.资源组[102] then
			 self.资源组[102]:置染色(数据.染色方案,数据.染色组[1],数据.染色组[2],数据.染色组[3])
			end
			self.资源组[101]:置方向(0)
		end

	     self.状态 = "原"
		insert(tp.窗口_,self)
		self.头像组 = {}
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_召唤兽染色:显示(dt,x,y)
	self.资源组[4]:更新(x,y)
	self.资源组[100]:更新(x,y,self.状态~="原")
	if self.资源组[4]:事件判断() then
		self:打开()
		return false
	end
	self.焦点1 =false
	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
	Picture.标题:显示(self.x+self.资源组[1].宽度/2-90,self.y)

	
	zts:置颜色(0xFFFFFFFF):显示(self.x+103,self.y+3,"召唤兽染色")
	
	self.资源组[3]:显示(self.x+15,self.y+66)
	self.资源组[4]:显示(self.x+283,self.y+6,true)
	self.资源组[5]:显示(self.x+25,self.y+37,true)
	zts:置颜色(0xFF000000):显示(self.x+35,self.y+40,self.数据.名称)
	self.介绍文本:显示(self.x+180,self.y+36)
    local xx,yy =0,0
	for i=6,87 do
		local jx = xx * 24+ 10
		local jy = yy *  33+ 224
		self.资源组[i]:更新(x,y,self.状态~=self.资源组[i]:取文字())
		self.资源组[i]:显示(self.x+jx,self.y+ jy,true,1,nil,self.状态==self.资源组[i]:取文字(),2)
	    if self.资源组[i]:事件判断()then
		    	if i == 6 then
					if self.数据.染色方案 ~= nil then
					self.资源组[101]:置染色(self.数据.染色方案,self.数据.染色组[1],self.数据.染色组[2],self.数据.染色组[3])
					else
						local n = 取模型(self.数据.造型)
						self.资源组[101] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
					end
					self.染色 =nil
		    	else
		    	  self:取染色(i-6)
		    	end
		    	self.状态=self.资源组[i]:取文字()
	    end
	    xx = xx + 1
		if xx == 12 then
			xx = 0
			yy = yy + 1
		end
	end
	if self.资源组[100]:事件判断() and self.染色 then
	    客户端:发送数据(self.染色[1],self.染色[2],107,self.染色[3],self.编号)
	end
	tp.影子:显示(self.x+100,self.y+176)
		self.资源组[101]:更新(dt)
		self.资源组[101]:显示(self.x+100,self.y+176)
		if self.资源组[102] ~= nil then
		self.资源组[102]:更新(dt)
		self.资源组[102]:显示(self.x+100,self.y+176)
		end
		self.资源组[100]:显示(self.x+258,self.y+422,true,1)
	zts:置颜色(-16777216)

end
function 场景类_召唤兽染色:取染色(id)
self.染色={}
local dsad={
	[1]={119,0,0},
	[2]={119,1,0},
	[3]={119,2,0},
	[4]={2000,1,0},
	[5]={2000,2,0},
	[6]={2000,3,0},
	[7]={2010,1,0},
	[8]={2010,2,0},
	[9]={2010,4,0},
	[10]={2010,5,0},
	[11]={2010,6,0},
	[12]={20113,1,0},
	[13]={20113,3,0},
	[14]={20113,4,0},
	[15]={20113,5,0},
	[16]={20113,6,0},
	[17]={2042,1,0},
	[18]={2077,1,0},
	[19]={2077,2,0},
	[20]={2077,3,0},
	[21]={2078,1,0},
	[22]={2078,2,0},
	[23]={2078,4,0},
	[24]={2078,5,0},
	[25]={2078,6,0},
	[26]={2079,1,0},
	[27]={2079,2,0},
	[28]={2079,4,0},
	[29]={2079,5,0},
	[30]={2079,6,0},
	[31]={23,1,0},
	[32]={23,2,0},
	[33]={23,3,0},
	[34]={24,1,0},
	[35]={24,2,0},
	[36]={24,3,0},
	[37]={31,1,0},
	[38]={31,2,0},
	[39]={31,3,0},
	[40]={32,1,0},
	[41]={32,2,0},
	[42]={32,3,0},
	[43]={33,1,0},
	[44]={33,2,0},
	[45]={33,3,0},
	[46]={39,1,0},
	[47]={39,2,0},
	[48]={39,3,0},
	[49]={40,1,0},
	[50]={40,2,0},
	[51]={40,3,0},
	[52]={44,1,0},
	[53]={44,2,0},
	[54]={44,3,0},
	[55]={45,1,0},
	[56]={45,2,0},
	[57]={45,3,0},
	[58]={47,1,0},
	[59]={47,2,0},
	[60]={47,3,0},
	[61]={48,1,0},
	[62]={48,2,0},
	[63]={48,3,0},
	[64]={49,1,0},
	[65]={49,2,0},
	[66]={49,3,0},
	[67]={52,1,0},
	[68]={62,1,0},
	[69]={64,1,0},
	[70]={66,1,0},
	[71]={67,1,0},
	[72]={68,1,0},
	[73]={72,1,0},
	[74]={73,1,0},
	[75]={76,1,0},
	[76]={77,1,0},
	[77]={78,1,0},
	[78]={88,1,0},
	[79]={95,1,0},
	[80]={96,1,0},
	[81]={97,1,0}}


 self.染色=dsad[id]
  self.资源组[101]:置染色(self.染色[1],self.染色[2],self.染色[3],self.染色[4])
 if self.资源组[102] then
    self.资源组[102]:置染色(self.染色[1],self.染色[2],self.染色[3],self.染色[4])
 end


end


return 场景类_召唤兽染色


