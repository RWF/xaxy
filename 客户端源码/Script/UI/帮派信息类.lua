--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:47
--======================================================================--
local 帮派信息类 = class()
local tp,zts,zts1
local insert = table.insert
function 帮派信息类:初始化(根)
	self.ID = 144
	self.x = 0+(全局游戏宽度-800)/2
	self.y = 0
	self.xx = 0
	self.yy = 0
	self.注释 = "帮派信息类"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.背景 = 根.资源:载入('JM.dll',"网易WDF动画",0x3125AB48)
	self.关闭 = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true)
	self.关闭:绑定窗口_(144)
	self.金库 = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"金库")
	self.金库:绑定窗口_(144)
	self.厢房 = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"厢房")
	self.厢房:绑定窗口_(144)
	self.药房 = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"药房")
	self.药房:绑定窗口_(144)
	self.书院 = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"书院")
	self.书院:绑定窗口_(144)
	self.仓库 = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"仓库")
	self.仓库:绑定窗口_(144)
	self.兽室 = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"兽室")
	self.兽室:绑定窗口_(144)

	self.设置内政 = 按钮.创建(自适应.创建(16,4,75,22,1,3),0,0,4,true,true,"设置内政")
	self.设置内政:绑定窗口_(144)

	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.描边字体

end

function 帮派信息类:打开(数据)
	if  self.可视 then
		self.可视 = false
	 self.所需进度=0
	 self.损耗资金=0
	 self.损耗人气=0
	 self.损耗繁荣=0
	 self.选中类型=""
	else
						                    if  self.x > 全局游戏宽度 then
self.x = 0+(全局游戏宽度-800)/2
    end
	insert(tp.窗口_,self)

	self.可视 = true
		 self.数据=数据
		 	 self.所需进度=0
	 self.损耗资金=0
	 self.损耗人气=0
	 self.损耗繁荣=0
	 self.选中类型=""
		tp.运行时间 = tp.运行时间 + 3
		self.窗口时间 = tp.运行时间
		self.可视 = true
	end
end



function 帮派信息类:设置消耗(类型)
	self.选中类型 = 类型
	if 类型 == "金库" then
		self.所需进度 = 500
		self.损耗资金 = 5000000
		self.损耗繁荣 = 300
		self.损耗人气 = 200
	elseif 类型 == "书院" then
		self.所需进度 = 500
		self.损耗资金 = 5000000
		self.损耗繁荣 = 300
		self.损耗人气 = 200
	elseif 类型 == "聚义厅" then
		self.所需进度 = self.数据.规模 * 500 + 500
		self.损耗资金 = self.数据.规模 * 5000000 + 5000000
		self.损耗繁荣 = 500
		self.损耗人气 = 500
	else
		self.所需进度 = 200
		self.损耗资金 = 3000000
		self.损耗繁荣 = 100
		self.损耗人气 = 100
	end
end



function 帮派信息类:检查点(x,y)
	if self.背景:是否选中(x,y)  then
		return true
	end
end

function 帮派信息类:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	self.窗口时间 = tp.运行时间
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 帮派信息类:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end


function 帮派信息类:显示(dt,x, y)
	self.关闭:更新(x,y)
	self.金库:更新(x,y)
	self.厢房:更新(x,y)
	self.书院:更新(x,y)
	self.仓库:更新(x,y)
	self.药房:更新(x,y)
	self.兽室:更新(x,y)
	self.设置内政:更新(x,y)
   self.焦点=false
	if self.关闭:事件判断() then
		self.本类开关 = false
	elseif self.金库:事件判断() then
		self:设置消耗("金库")
	elseif self.厢房:事件判断() then
		self:设置消耗("厢房")
	elseif self.书院:事件判断() then
		self:设置消耗("书院")
	elseif self.仓库:事件判断() then
		self:设置消耗("仓库")
	elseif self.药房:事件判断() then
		self:设置消耗("聚义厅")
	elseif self.兽室:事件判断() then
		self:设置消耗("兽室")
	elseif self.设置内政:事件判断() then
		if self.选中类型 == "" then
			tp.提示:写入("#y/请先选择要升级的建筑物")
		else
			客户端:发送数据(12, 72, 13, self.选中类型)
		end
	end

	self.背景:显示(self.x+220,self.y+ 60)
	self.关闭:显示(self.x+603, self.y+66)

	zts1:显示(self.x+445, self.y+224, "请选择要升级的建筑：")
	self.金库:显示(self.x+447, self.y+250,true)
	self.厢房:显示(self.x+527, self.y+250,true)
	self.书院:显示(self.x+447, self.y+285,true)
	self.仓库:显示(self.x+527, self.y+285,true)
	self.药房:显示(self.x+447, self.y+320,true)
	self.兽室:显示(self.x+527, self.y+320,true)
	self.设置内政:显示(self.x+475, self.y+175,true)
	zts:置颜色(0xFF000000)
	zts:显示(self.x+360, self.y+95, self.数据.金库)
	zts:显示(self.x+360, self.y+117, self.数据.书院)
	zts:显示(self.x+360, self.y+139, self.数据.兽室)
	zts:显示(self.x+360, self.y+161, self.数据.厢房)
	zts:显示(self.x+360, self.y+183, self.数据.药房)
	zts:显示(self.x+360, self.y+205, self.数据.仓库)
	zts:显示(self.x+336, self.y+227, self.数据.资金)
	zts:显示(self.x+336, self.y+249, self.数据.金库 * 5000000 + 3000000)
	zts:显示(self.x+360,self.y+ 271, self.数据.书院 * 2)
	zts:显示(self.x+360, self.y+293, 0)
	zts:显示(self.x+360, self.y+315, self.数据.守护兽等级)
	zts:显示(self.x+360, self.y+337, self.数据.兽室 * 5)
	zts:显示(self.x+360, self.y+359, self.数据.当前人数)
	zts:显示(self.x+360, self.y+381, self.数据.人数上限)
	zts:显示(self.x+336, self.y+403, 0)
	zts:显示(self.x+336, self.y+425, self.数据.资材)
	zts:显示(self.x+336, self.y+447, self.数据.仓库 * 100 + 100)
	zts:显示(self.x+557, self.y+95, self.数据.学费指数)
	zts:显示(self.x+557, self.y+117, self.数据.物价指数)
	zts:显示(self.x+557, self.y+139, self.数据.修理指数)
	zts:显示(self.x+533, self.y+381, self.损耗资金)
	zts:显示(self.x+533, self.y+403, self.所需进度)
	zts:显示(self.x+533, self.y+425, self.损耗繁荣)
	zts:显示(self.x+533, self.y+447, self.损耗人气)
	zts:置颜色(0xFFFFFFFF)
end

return 帮派信息类
