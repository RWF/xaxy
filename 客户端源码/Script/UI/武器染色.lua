--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:48
--======================================================================--
local 场景类_武器染色 = class(窗口逻辑)
local min = math.min
local floor = math.floor
local tp,zts
local mouseb = 引擎.鼠标弹起
local box = 引擎.画矩形
local insert = table.insert
function 场景类_武器染色:初始化(根)
    self.ID = 151
    self.x = 376
    self.y = 41
    self.xx = 0
    self.yy = 0
    self.注释 = "武器染色"
    self.可视 = false
    self.鼠标 = false
    self.焦点 = false
    self.焦点1 =false
    self.状态 = "原"
    self.可移动 = true
    local 资源 = 根.资源
    local 按钮 = 根._按钮
    local 自适应 = 根._自适应
    self.资源组 = {
        [1] = 自适应.创建(99,1,305,451,3,9),

        [3] = 资源:载入('JM.dll',"网易WDF动画",0xA19838E8),
        [4] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
        [5] = 自适应.创建(93,1,140,22,1,3),
        [6] = 按钮.创建(自适应.创建(101,4,21,20,1,3),0,0,4,true,true,"原"),

        [100] = 按钮.创建(自适应.创建(102,4,43,22,1,3),0,0,4,true,true,"染色"),
    }
    self.资源组[4]:绑定窗口_(151)
        self.资源组[100]:绑定窗口_(151)
        self.资源组[6]:置偏移(-5,-1)
        self.资源组[6]:绑定窗口_(151)
    for n=7,87 do
        self.资源组[n]=按钮.创建(自适应.创建(101,4,21,20,1,3),0,0,4,true,true,""..n-6)
        if n >= 16 then
         self.资源组[n]:置偏移(-4,-1)
       else
        self.资源组[n]:置偏移(0,-1)
       end
       self.资源组[n]:绑定窗口_(151)
   end

    self.介绍文本 = 根._丰富文本(115,280,根.字体表.普通字体)
        for n=0,119 do
        self.介绍文本:添加元素(n,根.包子表情动画[n])
     end
    self.介绍文本:添加文本("#G/点击数字按钮可预览对象的变色效果,每种效果都不一样/n#91")

    self.窗口时间 = 0
    tp = 根
    zts = tp.字体表.华康字体

    self.选中 = 0
    self.加入 = 0
end

function 场景类_武器染色:打开(类型,格子)
    if self.可视 then
        self.可视 = false
    else
        self.数据 =数据
      self.染色 = nil
     self.类型 = 类型
     self.格子 = 格子
   -- {角色=user.角色.造型,武器=user.角色.武器数据,格子=参数,类型=内容}

    


        local n = 取模型(tp.场景.人物.数据.造型)
        self.资源组[101] = tp.资源:载入(n.资源,"网易WDF动画",n.静立)

        local m = tp:qfjmc(tp.场景.人物.数据.武器数据.类型,tp.场景.人物.数据.武器数据.等级,tp.场景.人物.数据.武器数据.名称)
        local nm = 取模型(m.."_"..tp.场景.人物.数据.造型)
        self.资源组[102] = tp.资源:载入(nm.资源,"网易WDF动画",nm.静立)
        self.资源组[102]:置方向(0)
        self.资源组[101]:置方向(0)
            if tp.场景.人物.数据.武器数据.染色 ~= nil then
                self.资源组[102]:置染色(tp.场景.人物.数据.武器数据.染色.染色方案,tp.场景.人物.数据.武器数据.染色.染色组.a,tp.场景.人物.数据.武器数据.染色.染色组.b)
            end
         self.状态 = "原"
        insert(tp.窗口_,self)
        self.头像组 = {}
        tp.运行时间 = tp.运行时间 + 1
        self.窗口时间 = tp.运行时间
        self.可视 = true
    end
end

function 场景类_武器染色:显示(dt,x,y)
    self.资源组[4]:更新(x,y)
    self.资源组[100]:更新(x,y,self.状态~="原")
    if self.资源组[4]:事件判断() then
        self:打开()
        return false
    end
    self.焦点1 =false
    self.焦点 = false
    self.资源组[1]:显示(self.x,self.y)
    Picture.标题:显示(self.x+self.资源组[1].宽度/2-90,self.y)

 
    zts:置颜色(0xFFFFFFFF):显示(self.x+103,self.y+3,"武器染色")
    
    self.资源组[3]:显示(self.x+15,self.y+66)
    self.资源组[4]:显示(self.x+283,self.y+6,true)
    self.资源组[5]:显示(self.x+25,self.y+37,true)
    zts:置颜色(0xFF000000):显示(self.x+35,self.y+40,tp.场景.人物.数据.武器数据.名称)
    self.介绍文本:显示(self.x+180,self.y+36)
    local xx,yy =0,0
    for i=6,87 do
        local jx = xx * 24+ 10
        local jy = yy *  33+ 224
        self.资源组[i]:更新(x,y,self.状态~=self.资源组[i]:取文字())
        self.资源组[i]:显示(self.x+jx,self.y+ jy,true,1,nil,self.状态==self.资源组[i]:取文字(),2)
        if self.资源组[i]:事件判断()then
                if i == 6 then
                    if tp.场景.人物.数据.武器数据.染色 ~= nil then
                        self.资源组[102]:置染色(tp.场景.人物.数据.武器数据.染色.染色方案,tp.场景.人物.数据.武器数据.染色.染色组.a,tp.场景.人物.数据.武器数据.染色.染色组.b,tp.场景.人物.数据.武器数据.染色.染色组.c)
                    else
                                local m = tp:qfjmc(tp.场景.人物.数据.武器数据.类型,tp.场景.人物.数据.武器数据.等级,tp.场景.人物.数据.武器数据.名称)
                                local nm = 取模型(m.."_"..tp.场景.人物.数据.造型)
                                self.资源组[102] = tp.资源:载入(nm.资源,"网易WDF动画",nm.静立)

                    end
                    self.染色 =nil
                else
                  self:取染色(i-6)
                end
                self.状态=self.资源组[i]:取文字()
        end
        xx = xx + 1
        if xx == 12 then
            xx = 0
            yy = yy + 1
        end
    end
    if self.资源组[100]:事件判断() and self.染色 then
        客户端:发送数据(self.格子,0,16,self.染色[1].."*-*"..self.染色[2].."*-*"..self.染色[3])
        self:打开()
    end
    tp.影子:显示(self.x+100,self.y+176)
        self.资源组[101]:更新(dt)
        self.资源组[101]:显示(self.x+100,self.y+176)
        if self.资源组[102] ~= nil then
        self.资源组[102]:更新(dt)
        self.资源组[102]:显示(self.x+100,self.y+176)
        end
        self.资源组[100]:显示(self.x+258,self.y+422,true,1)
    zts:置颜色(-16777216)

end
function 场景类_武器染色:取染色(id)
 self.染色 = {}
    if id ==1 then
        self.染色[1]=119
        self.染色[2]=0
        self.染色[3]=0
    elseif id ==2 then
        self.染色[1]=119
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==3 then
        self.染色[1]=119
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==4 then
        self.染色[1]=2000
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==5 then
        self.染色[1]=2000
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==6 then
        self.染色[1]=2000
        self.染色[2]=3
        self.染色[3]=0
    elseif id ==7 then
        self.染色[1]=2010
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==8 then
        self.染色[1]=2010
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==9 then
        self.染色[1]=2010
        self.染色[2]=4
        self.染色[3]=0
    elseif id ==10 then
        self.染色[1]=2010
        self.染色[2]=5
        self.染色[3]=0
    elseif id ==11 then
        self.染色[1]=2010
        self.染色[2]=6
        self.染色[3]=0
    elseif id ==12 then
        self.染色[1]=20113
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==13 then
        self.染色[1]=20113
        self.染色[2]=3
        self.染色[3]=0
    elseif id ==14 then
        self.染色[1]=20113
        self.染色[2]=4
        self.染色[3]=0
    elseif id ==15 then
        self.染色[1]=20113
        self.染色[2]=5
        self.染色[3]=0
    elseif id ==16 then
        self.染色[1]=20113
        self.染色[2]=6
        self.染色[3]=0
    elseif id ==17 then
        self.染色[1]=2042
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==18 then
        self.染色[1]=2077
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==19 then
        self.染色[1]=2077
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==20 then
        self.染色[1]=2077
        self.染色[2]=3
        self.染色[3]=0
    elseif id ==21 then
        self.染色[1]=2078
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==22 then
        self.染色[1]=2078
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==23 then
        self.染色[1]=2078
        self.染色[2]=4
        self.染色[3]=0
    elseif id ==24 then
        self.染色[1]=2078
        self.染色[2]=5
        self.染色[3]=0
    elseif id ==25 then
        self.染色[1]=2078
        self.染色[2]=6
        self.染色[3]=0
    elseif id ==26 then
        self.染色[1]=2079
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==27 then
        self.染色[1]=2079
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==28 then
        self.染色[1]=2079
        self.染色[2]=4
        self.染色[3]=0
    elseif id ==29 then
        self.染色[1]=2079
        self.染色[2]=5
        self.染色[3]=0
    elseif id ==30 then
        self.染色[1]=2079
        self.染色[2]=6
        self.染色[3]=0
    elseif id ==31 then
        self.染色[1]=23
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==32 then
        self.染色[1]=23
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==33 then
        self.染色[1]=23
        self.染色[2]=3
        self.染色[3]=0
    elseif id ==34 then
        self.染色[1]=24
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==35 then
        self.染色[1]=24
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==36 then
        self.染色[1]=24
        self.染色[2]=3
        self.染色[3]=0
    elseif id ==37 then
        self.染色[1]=31
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==38 then
        self.染色[1]=31
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==39 then
        self.染色[1]=31
        self.染色[2]=3
        self.染色[3]=0
    elseif id ==40 then
        self.染色[1]=32
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==41 then
        self.染色[1]=32
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==42 then
        self.染色[1]=32
        self.染色[2]=3
        self.染色[3]=0
    elseif id ==43 then
        self.染色[1]=33
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==44 then
        self.染色[1]=33
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==45 then
        self.染色[1]=33
        self.染色[2]=3
        self.染色[3]=0
    elseif id ==46 then
        self.染色[1]=39
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==47 then
        self.染色[1]=39
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==48 then
        self.染色[1]=39
        self.染色[2]=3
        self.染色[3]=0
    elseif id ==49 then
        self.染色[1]=40
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==50 then
        self.染色[1]=40
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==51 then
        self.染色[1]=40
        self.染色[2]=3
        self.染色[3]=0
    elseif id ==52 then
        self.染色[1]=44
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==53 then
        self.染色[1]=44
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==54 then
        self.染色[1]=44
        self.染色[2]=3
        self.染色[3]=0
    elseif id ==55 then
        self.染色[1]=45
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==56 then
        self.染色[1]=45
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==57 then
        self.染色[1]=45
        self.染色[2]=3
        self.染色[3]=0
    elseif id ==58 then
        self.染色[1]=47
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==59 then
        self.染色[1]=47
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==60 then
        self.染色[1]=47
        self.染色[2]=3
        self.染色[3]=0
    elseif id ==61 then
        self.染色[1]=48
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==62 then
        self.染色[1]=48
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==63 then
        self.染色[1]=48
        self.染色[2]=3
        self.染色[3]=0
    elseif id ==64 then
        self.染色[1]=49
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==65 then
        self.染色[1]=49
        self.染色[2]=2
        self.染色[3]=0
    elseif id ==66 then
        self.染色[1]=49
        self.染色[2]=3
        self.染色[3]=0
    elseif id ==67 then
        self.染色[1]=52
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==68 then
        self.染色[1]=62
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==69 then
        self.染色[1]=64
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==70 then
        self.染色[1]=66
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==71 then
        self.染色[1]=67
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==72 then
        self.染色[1]=68
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==73 then
        self.染色[1]=72
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==74 then
        self.染色[1]=73
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==75 then
        self.染色[1]=76
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==76 then
        self.染色[1]=77
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==77 then
        self.染色[1]=78
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==78 then
        self.染色[1]=88
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==79 then
        self.染色[1]=95
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==80 then
        self.染色[1]=96
        self.染色[2]=1
        self.染色[3]=0
    elseif id ==81 then
        self.染色[1]=97
        self.染色[2]=1
        self.染色[3]=0
 end

 if self.资源组[102] then
    self.资源组[102]:置染色(self.染色[1],self.染色[2],self.染色[3])
 end


end


return 场景类_武器染色


