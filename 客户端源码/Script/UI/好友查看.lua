--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:47
--======================================================================--
local 好友查看 = class()
local insert = table.insert
local tp,zts1,zts
local mouseb = 引擎.鼠标弹起

function 好友查看:初始化(根)
	self.ID = 61
	self.x = 210+(全局游戏宽度-800)/2
	self.y = 165
	self.xx = 0
	self.yy = 0
	self.注释 = "玩家给与"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
	    [0] = 资源:载入('JM.dll',"网易WDF动画",0x3D5D95E9), --420FF401
	    [1] = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true),
		[2] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"观察"),
	    [3] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"入队"),
	    [4] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"更新"),
	    [5] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"给予"),
	    [6] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"断交"),
	 }

	for n=1,5 do
	   self.资源组[n]:绑定窗口_(61)
	end
	self.窗口时间 = 0
	tp = 根
	zts = 根.字体表.普通字体
end

function 好友查看:打开(数据,编号)
	if self.可视 then
		self.可视 = false
	else
						                    if  self.x > 全局游戏宽度 then
self.x = 210+(全局游戏宽度-800)/2
    end
       self.数据 = 数据
       local n =  ModelData[self.数据.造型]
        self.头像 = tp.资源:载入(n.头像资源,"网易WDF动画",n.头像组.道具)
		insert(tp.窗口_,self)
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
		self.可视 = true
	end
end

function 好友查看:显示(dt,x,y)
	self.焦点 = false
  self.资源组[0]:显示(self.x,self.y)
 self.资源组[1]:显示(self.x+324,self.y+5)
 self.资源组[1]:更新(x,y)
 self.资源组[2]:显示(self.x+20,self.y+230,true)
 self.资源组[2]:更新(x,y)
 self.资源组[3]:显示(self.x+82,self.y+230,true)
 self.资源组[3]:更新(x,y)
 self.资源组[4]:显示(self.x+144,self.y+230,true)
 self.资源组[4]:更新(x,y)
 self.资源组[5]:显示(self.x+206,self.y+230,true)
 self.资源组[5]:更新(x,y)

  self.资源组[6]:显示(self.x+268,self.y+230,true)
 self.资源组[6]:更新(x,y)
	 if self.资源组[1]:事件判断() then
	 self:打开()
	 elseif self.资源组[2]:事件判断() then
	 	客户端:发送数据(self.数据.id+0,2,62,"69@-@79",1)
	 elseif self.资源组[3]:事件判断() then
	 	客户端:发送数据(self.数据.id+0,6,11,"69@-@79",1)

	 elseif self.资源组[4]:事件判断() then
      客户端:发送数据(self.数据.id+0,23,5,"69@-@79")
	 elseif self.资源组[5]:事件判断() then
		 客户端:发送数据(self.数据.id+0,2,26,"9A@-@14",1)
	 elseif self.资源组[6]:事件判断() then
		 客户端:发送数据(self.数据.id+0,1,62)
		 self:打开()
	end
	zts:置颜色(0xFF000000)
	zts:显示(self.x+225,self.y+40,self.数据.名称)
	zts:显示(self.x+225,self.y+64,self.数据.id)
	zts:显示(self.x+225,self.y+88,self.数据.等级)
	zts:显示(self.x+225,self.y+112,self.数据.门派)
	zts:显示(self.x+225,self.y+136,self.数据.名称)

	zts:显示(self.x+70,self.y+170,self.数据.帮派)
	zts:显示(self.x+275,self.y+170,self.数据.关系)
	zts:显示(self.x+70,self.y+192,self.数据.当前称谓)
	zts:显示(self.x+275,self.y+192,self.数据.好友度)
	zts:置颜色(0xFFFFFFFF)
    self.头像:显示(self.x+17,self.y+35)
end

function 好友查看:检查点(x,y)
	if self.资源组[0]:是否选中(x,y)  then
		return true
	end
end

function 好友查看:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 好友查看:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 好友查看