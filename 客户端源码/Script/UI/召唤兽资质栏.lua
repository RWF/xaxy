--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:47
--======================================================================--
local 场景类_召唤兽资质栏 = class(窗口逻辑)
local zts,bb,tp,zts1
local yx = {{68,4},{25,28},{111,28},{24,75},{111,75},{68,97}}
local insert = table.insert
local bds = {"攻击资质","防御资质","体力资质","法力资质","速度资质","躲闪资质"}
local bds1 = {"寿命","成长","五行"}

function 场景类_召唤兽资质栏:初始化(根)
	self.ID = 8
	self.xx = 0
	self.yy = 0

	self.注释 = "召唤兽资质栏"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 自适应.创建(0,1,242,453,3,9),
		[2] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),
		[3] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x1F996671),0,0,4,true,true),
		[4] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x9C24F376),0,0,4,true,true),
		[5] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xCD999F0B),0,0,4,true,true),
		[6] = 资源:载入('JM.dll',"网易WDF动画",0x68D384BD),
		[7] = 资源:载入('JM.dll',"网易WDF动画",0x7367031D),
		[8] = 资源:载入('JM.dll',"网易WDF动画",0x1E714129),
		[9] = 资源:载入('JM.dll',"网易WDF动画",0xF2FC2425),
		[10] = 资源:载入('JM.dll',"网易WDF动画",0x10E2B4A7),
		[11] = 资源:载入('JM.dll',"网易WDF动画",0xC361C087),
		[12] = 资源:载入("qy.png","加密图片"),
		[13] = 自适应.创建(3,1,95,19,1,3),
		[14] = 自适应.创建(1,1,204,18,1,3,nil,18),


	}
    self.上页 = 根._按钮(资源:载入('JM.dll',"网易WDF动画",0x7AB5584C),0,0,3,true,true)
	self.下页 = 根._按钮(资源:载入('JM.dll',"网易WDF动画",0xCB50AB1D),0,0,3,true,true)
	self.进阶 ={
        [16] = 资源:载入('JM.dll',"网易WDF动画",0x1094AD16),----进阶界面
 	    [1] = 资源:载入('JM.dll',"网易WDF动画",0x4536A03D),--进阶1
		[2] = 资源:载入('JM.dll',"网易WDF动画",0x714C3706),--2
		[3] = 资源:载入('JM.dll',"网易WDF动画",0xD60014B8),-----3
		[4] = 资源:载入('JM.dll',"网易WDF动画",0xF7EBF987), ----4
		[5] = 资源:载入('JM.dll',"网易WDF动画",0x11963488),----5
		[6] = 资源:载入('JM.dll',"网易WDF动画",0x9A4F1961),--6
		[7] = 资源:载入('JM.dll',"网易WDF动画",0x1E7ABB94), --7
		[8] = 资源:载入('JM.dll',"网易WDF动画",0xA6C9A76A),--8
		[9] = 资源:载入('JM.dll',"网易WDF动画",0x2982E3F7),---9
		[10] = 资源:载入('JM.dll',"网易WDF动画",0x1D0717D7),---91
		[11] = 资源:载入('JM.dll',"网易WDF动画",0xC44F0602),--94
		[12] = 资源:载入('JM.dll',"网易WDF动画",0x9765D0B3),--97
		[13] = 资源:载入('JM.dll',"网易WDF动画",0x36A2C1A6),--100
		[14] = 资源:载入('JM.dll',"网易WDF动画",0xAFC2E161), --未完成内框
		[15] = 资源:载入('JM.dll',"网易WDF动画",0x27E24CFA), --完成
   }
	self.物品 = {}
	local wp = 根._物品格子
		local 底图 = {根.资源:载入("护手.png","加密图片"),根.资源:载入("底图.png","加密图片"),根.资源:载入("铠甲.png","加密图片")}

	for i=1,3 do
	    self.物品[i] = wp(0,0,i,"召唤兽资质物品",底图[i])
	end
	self.技能 = {}
	local jn = 根._技能格子
	for i=1,36 do
	    self.技能[i] = jn(0,0,i,"召唤兽资质技能")
	end
	for i=2,5 do
	    self.资源组[i]:绑定窗口_(8)
	end
	self.内丹={}
	local nd = 根._内丹格子
		for i=1,12 do
	    self.内丹[i] = nd(0,0,i,"召唤兽内丹")
	end
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.描边字体
	self.状态 = 1
	self.窗口时间 = 0


	self.显示编号 = 0
		    self.道具={}
		self.技能数量 =0
		self.技能页数 =0
end

function 场景类_召唤兽资质栏:打开(b,编号)
	if self.可视 then
		if b ~= nil and  b ~= bb then
			bb = b
			for i=1,3 do
			    self.物品[i]:置物品(bb.道具数据[i])
			end

			for i=1,36 do
			    self.技能[i]:置技能(bb.技能[i])
			end
			self.技能数量 = #bb.技能
			self.技能页数 =0
			for i=1,6 do
			    self.内丹[i]:置内丹(bb.内丹[i])
			end
			return
		end
		self.状态 = nil
		self.可视 = false
	else
		insert(tp.窗口_,self)
		bb = b
		for i=1,3 do
		    self.物品[i]:置物品(bb.道具数据[i])
		end

		for i=1,36 do
		    self.技能[i]:置技能(bb.技能[i])
		end
		self.技能数量 = #bb.技能
       	for i=1,6 do
		    self.内丹[i]:置内丹(bb.内丹[i])
		end

		self.技能页数 =0
		self.状态 = 1
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true

	end
end

function 场景类_召唤兽资质栏:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y,self.状态 ~= 1)
	self.资源组[4]:更新(x,y,self.状态 ~= 2)
	self.资源组[5]:更新(x,y,self.状态 ~= 3 and (bb.参战等级>= 45 or bb.神兽 ) )
	     self.显示编号=0
              for i=1,3 do
           if self.道具[i]~=nil then
              if self.道具[i].名称=="七星宝甲" then
               self.装备坐标={x=self.x+157,y=self.y+31}
               self.背景坐标={x=self.x+157,y=self.y+31}
              elseif self.道具[i].名称=="嵌宝金腕" then
               self.装备坐标={x=self.x+35,y=self.y+32}
               self.背景坐标={x=self.x+35,y=self.y+32}
              elseif self.道具[i].名称=="冰蚕丝圈" then
               self.装备坐标={x=self.x+96,y=self.y+31}
               self.背景坐标={x=self.x+96,y=self.y+31}
               end

              self.道具[i]:显示(self.装备坐标.x,self.装备坐标.y)

              if self.道具[i]:获取焦点()  then

               self.显示编号=i
                end

             end
           end
           if self.显示编号~=0 then
             self.道具[self.显示编号]:显示事件()
            end



	if self.资源组[2]:事件判断() then
		self:打开()
		return false
	elseif self.资源组[3]:事件判断() then
		self.状态 = 1
	elseif self.资源组[4]:事件判断() then
		self.状态 = 2
	elseif self.资源组[5]:事件判断() then
		self.状态 = 3
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[14]:显示(self.x+6,self.y+3)
	Picture.窗口标题背景_:置区域(0,0,84,16)
	Picture.窗口标题背景_:显示(self.x+71,self.y+3)
	zts1:置字间距(1)
	zts1:显示(self.x+76,self.y+3,"召唤兽资质")
	zts1:置字间距(0)
	self.资源组[12]:显示(self.x+30,self.y+27)
	zts1:置字间距(10)
	for i=0,5 do
		zts1:显示(self.x+23,self.y+98+i*23,bds[i+1])
		self.资源组[13]:显示(self.x+124,self.y+96+i*23)
	end
	zts1:置字间距(58)
	zts1:置颜色(-1404907)
	for i=0,2 do
		zts1:显示(self.x+23,self.y+236+i*23,bds1[i+1])
		self.资源组[13]:显示(self.x+124,self.y+234+i*23)
	end
	zts1:置颜色(4294967295)
	zts1:置字间距(0)
	self.资源组[2]:显示(self.x+216,self.y+5)
	self.资源组[3]:显示(self.x+208,self.y+305,true,nil,nil,self.状态 == 1,2)
	self.资源组[4]:显示(self.x+208,self.y+351,true,nil,nil,self.状态 == 2,2)
	self.资源组[5]:显示(self.x+208,self.y+397,true,nil,nil,self.状态 == 3,2)


	local xx = 0
	local yy = 0
	if self.状态 == 1 then
		
       if  self.技能数量> 12*(self.技能页数+1) then
		
				self.下页:更新(x,y)
				self.下页:显示(self.x+192,self.y+406)
				if self.下页:事件判断() then
				self.技能页数=self.技能页数+1
				end

		 end
		 if self.技能页数 > 0 then
		 				self.上页:更新(x,y)
				self.上页:显示(self.x+192,self.y+326)
				if self.上页:事件判断() then
				self.技能页数=self.技能页数-1
				end
		 end

		for i=1+self.技能页数*12,12+self.技能页数*12 do
			local jx = self.x+20+(xx*41)
			local jy = self.y+309+(yy*41)
			self.资源组[6]:显示(jx,jy)
		    self.技能[i]:置坐标(jx+3,jy+2)
	   	    self.技能[i]:显示(x,y,self.鼠标)
	   	    if self.技能[i].焦点 and self.技能[i].技能 ~= nil then

	   	    	tp.提示:BB技能(x,y,self.技能[i].技能)
	   	    end
			xx = xx + 1
			if xx > 3 then
				xx = 0
				yy = yy + 1
			end
		end





	elseif self.状态 == 2 then

		self.资源组[7]:显示(self.x+20,self.y+309)
		local v1 = bb.内丹.总数
		for i=1,6 do
			local jxx = self.x+20 + yx[i][1]
			local jxy = self.y+309 + yx[i][2]

			if i <= v1 and   self.内丹[i].内丹 == nil  then
					self.资源组[9]:显示(jxx,jxy)
		        if self.鼠标 and self.资源组[9]:是否选中(x,y) then
		        tp.提示:自定义(x,y,"可以学习的内丹技能格")
		        self.焦点 = true
		       end
			elseif i > v1 and   self.内丹[i].内丹 == nil  then
				self.资源组[8]:显示(jxx,jxy)
				if self.鼠标 and self.资源组[8]:是否选中(x,y) then
					tp.提示:自定义(x,y,"不可用内丹技能格,召唤兽可用内丹格数量与其参战等级相关")
					self.焦点 = true
				end
			end
			self.内丹[i]:置坐标(jxx,jxy)
	   	    self.内丹[i]:显示(x,y,self.鼠标)
	   	     if  self.内丹[i].内丹 ~= nil and self.内丹[i].焦点 then
			    tp.提示:内丹提示(x,y,self.内丹[i].内丹)
			 end
		end
	elseif self.状态 == 3 then
		local jx = self.x+20
		local jy = self.y+309
		self.资源组[11]:显示(jx,jy)
		self.进阶[16]:显示(jx+40,jy+7)
		if self.进阶[16]:是否选中(x,y) then
			tp.提示:自定义(x,y,"#W/使用#Y/易经丹#W/可以提升该召唤兽灵性,当灵性到达到50可以获得新的造型")
		end
		if bb.灵性>0 and bb.灵性<0 then
	    elseif bb.灵性>0 and bb.灵性<= 10 then
	    	self.进阶[1]:显示(jx+39,jy+6)
	    elseif bb.灵性>10 and bb.灵性<= 20 then
	    	self.进阶[2]:显示(jx+39,jy+6)
	    elseif bb.灵性>20 and bb.灵性<= 30 then
	    	self.进阶[3]:显示(jx+39,jy+6)
	    elseif bb.灵性>30 and bb.灵性<= 40 then
	    	self.进阶[4]:显示(jx+39,jy+6)
	    elseif bb.灵性>40 and bb.灵性<= 50 then
	    	self.进阶[5]:显示(jx+39,jy+6)
	    elseif bb.灵性>50 and bb.灵性<= 60 then
	    	self.进阶[6]:显示(jx+39,jy+6)
	    elseif bb.灵性>60 and bb.灵性<= 70 then
	    	self.进阶[7]:显示(jx+39,jy+6)
	    elseif bb.灵性>70 and bb.灵性<= 80 then
	    	self.进阶[8]:显示(jx+39,jy+6)
	    elseif bb.灵性>80 and bb.灵性<= 90 then
	        self.进阶[9]:显示(jx+39,jy+6)
	    elseif bb.灵性>90 and bb.灵性<= 91 then
	    	self.进阶[10]:显示(jx+39,jy+6)
	    elseif bb.灵性>91 and bb.灵性<= 93 then
	    	self.进阶[11]:显示(jx+39,jy+6)
	    elseif bb.灵性>93 and bb.灵性<= 97 then
	        self.进阶[12]:显示(jx+39,jy+6)
	    elseif bb.灵性>97  then
	        self.进阶[13]:显示(jx+39,jy+6)
		end


		if bb.灵性>80 and bb.特性=="无" then
		self.进阶[14]:显示(jx+51,jy+30)
		elseif bb.灵性>80 and bb.特性~="无" then
		self.进阶[15]:显示(jx+67,jy+36)
		end

		if bb.特性 ~="无" then
		zts1:置颜色(0xFFFFFFA4)
		zts1:显示(jx+74,jy+45,bb.特性)
		zts1:置颜色(0xFFFFFFFF)
	    end
	    if (self.进阶[14]:是否选中(x,y) or self.进阶[15]:是否选中(x,y)) and bb.特性几率~=0 and bb.特性~= "无" then
			tp.提示:特性(x,y,bb.特性,bb.等级,bb.特性几率)
		end
		if bb.最高灵性显示 then
			zts1:置颜色(0xFFFFFFFF)
		  zts1:显示(jx+13,jy+115,"灵性:"..bb.灵性)
			zts1:置颜色(0xFFFFFFFF)
		 zts1:显示(jx+67,jy+115,"(历史最高值"..bb.最高灵性..")")
		elseif  bb.灵性>0 then
				zts1:置颜色(0xFFFFFFFF)
		zts1:显示(jx+65,jy+115,"灵性:"..bb.灵性)
		end
	end
	for i=1,3 do
	    self.物品[i]:置坐标(self.x + 37 + (i-1) *60,self.y + 31)
	    self.物品[i]:显示(dt,x,y,self.鼠标,dt)
	    if self.物品[i].物品 ~= nil and self.物品[i].焦点 then
				tp.提示:道具行囊(x,y,self.物品[i].物品)
			end
	end
	zts:置颜色(-16777216)
	zts:显示(self.x+131,self.y+100,bb.攻资)
	zts:显示(self.x+131,self.y+123,bb.防资)
	zts:显示(self.x+131,self.y+146,bb.体资)
	zts:显示(self.x+131,self.y+169,bb.法资)
	zts:显示(self.x+131,self.y+192,bb.速资)
	zts:显示(self.x+131,self.y+215,bb.躲资)

   if bb.特殊资质.攻资> 0 then
	zts:置颜色(0xFF8000FF)
	zts:显示(self.x+181,self.y+100,"+"..bb.特殊资质.攻资)
	zts:显示(self.x+181,self.y+123,"+"..bb.特殊资质.防资)
	zts:显示(self.x+181,self.y+146,"+"..bb.特殊资质.体资)
	zts:显示(self.x+181,self.y+169,"+"..bb.特殊资质.法资)
	zts:显示(self.x+181,self.y+192,"+"..bb.特殊资质.速资)
	zts:显示(self.x+181,self.y+215,"+"..bb.特殊资质.躲资)
    zts:置颜色(-16777216)
    end

	if bb.类型=="神兽" then
	zts:显示(self.x+131,self.y+238,"★永生★")
	else
	zts:显示(self.x+131,self.y+238,math.floor(bb.寿命))
    end
	zts:显示(self.x+131,self.y+261,bb.成长)
	zts:显示(self.x+131,self.y+284,bb.五行)
end



return 场景类_召唤兽资质栏