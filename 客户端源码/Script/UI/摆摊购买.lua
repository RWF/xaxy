--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:48
--======================================================================--
local 摆摊购买 = class(窗口逻辑)
local tp,zts,zts1,zts2
local insert = table.insert
local xxx = 0
local yyy = 0
local bw = require("gge包围盒")(0,0,230,20)
local box = 引擎.画矩形
local sts = {"单价","数量","总额","现金"}
local tos = 引擎.取金钱颜色
local mouseb = 引擎.鼠标弹起
function 摆摊购买:初始化(根)
	self.ID = 67
	self.x = 224+(全局游戏宽度-800)/2
	self.y = 100
	self.xx = 0
	self.yy = 0
	self.注释 = "商店"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 资源:载入('JM.dll',"网易WDF动画",0x9F9301F5),
		
		[3] = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true),
		[4] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"购买"),
		[5] = 按钮.创建(自适应.创建(16,4,72,22,1,3),0,0,4,true,true,"物品类"),
		[6] = 按钮.创建(自适应.创建(16,4,72,22,1,3),0,0,4,true,true,"召唤兽类"),
		[7] = 按钮.创建(自适应.创建(16,4,72,22,1,3),0,0,4,true,true,"制造类"),
        [8] = 按钮.创建(自适应.创建(16,4,92,22,1,3),0,0,4,true,true,"摊位聊天室"),
        [9] = 按钮.创建(自适应.创建(20,4,18,19,4,3),0,0,4,true,true),
		[10] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),
		[19] = 资源:载入('JM.dll',"网易WDF动画",0x839F03F8),
        [20] = 自适应.创建(2,1,241,209,3,9),

	}
    self.资源组[5]:置偏移(6,0)
    self.资源组[7]:置偏移(6,0)
	for n=3,10 do
	   self.资源组[n]:绑定窗口_(67)
	end

	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('摆摊购买')
	总控件:置可视(true,true)
	self.输入框 = 总控件:创建输入("购买数量", 194,335,100,14)
	
	self.输入框:置可视(false,false)
	self.输入框:置限制字数(3)
	self.输入框:置数字模式()
	self.输入框:置光标颜色(-16777216)
	self.输入框:置文字颜色(0xFF000000)


	self.商品 = {}
	local 物品格子 = 根._物品格子
	for i=1,20 do
		self.商品[i] = 物品格子(0,i,n,"商店")
	end
	self.数量=1
	self.上一次 = 1
	self.选中道具=0
	self.窗口时间 = 0
	tp = 根
	zts = 根.字体表.普通字体
	zts2 = 根.字体表.普通字体_
	zts1 = 根.字体表.描边字体
	self.关注 = 根._按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFF205590),0,0,1,true,true)
	self.关注:置根(根)
	self.关注:置偏移(-3,2)
	self.关注显示 = false
end

function 摆摊购买:打开(数据)
	if self.可视 then
				self.输入框:置焦点(false)
		self.输入框:置可视(false,false)
		for i=1,20 do
			self.商品[i]:置物品(nil)
		end
		self.可视 = false
		self.商品[self.上一次].确定 = false
		self.上一次 = 1
		self.选中道具= 0
		self.选中召唤兽 = 0
		self.选中制造 = 0
		self.数量=1
	else
								                    if  self.x > 全局游戏宽度 then
self.x = 224+(全局游戏宽度-800)/2
    end
		self.数据=数据.道具
		insert(tp.窗口_,self)
		self.招牌名称 = 数据.名称
		self.招牌id = 数据.id
		self.招牌角色 = 数据.角色
		self.当前银子 = 数据.银子
		self.关注显示 = tp.场景.玩家[数据.id].摊位关注
		self.关注:置打勾框(self.关注显示)
	    tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 摆摊购买:刷新道具(数据)
	self.状态= "物品类"
	self.商品[self.上一次].确定 = false
	self.上一次 = 1
	self.选中道具= 0
	self.召唤兽数据={}
	self.选中召唤兽 = 0
	self.选中制造 = 0
	self.制造 = {}
    self.数量=1
    		self.输入框:置焦点(false)
		self.输入框:置可视(false,false)
	for n=1,20 do
		self.商品[n]:置物品(nil)
		if 数据[n] ~= nil then
			self.商品[n]:置物品(数据[n].道具)
			self.商品[n].物品.单价=数据[n].单价+0
			--self.物品数据[n].编号 = 数据[n].编号
		end
	  end

end

function 摆摊购买:刷新宝宝(数据)
	   self.状态="召唤兽类"
	   		self.输入框:置焦点(false)
		self.输入框:置可视(false,false)
	  	for i=1,20 do
			self.商品[i]:置物品(nil)
		end
		self.选中召唤兽 = 0
		self.选中制造 = 0
		self.道具 = nil
		self.商品[self.上一次].确定 = false
		self.上一次 = 1
		self.制造 = {}
	for n = 1,12 do
		self.召唤兽数据[n] =nil
		if 数据[n] ~= nil and 数据[n].bb ~= nil then
			self.召唤兽数据[n] = 数据[n].bb
			self.召唤兽数据[n].单价 = 数据[n].单价+0
		end
	end
end
function 摆摊购买:刷新制造(数据)
	   self.状态="制造类"
	  	for i=1,20 do
			self.商品[i]:置物品(nil)
		end
		self.选中召唤兽 = 0
		self.选中制造 = 0
		self.道具 = nil
		self.商品[self.上一次].确定 = false
		self.上一次 = 1
		self.召唤兽数据={}
        self.制造 = 数据
end

function 摆摊购买:显示(dt,x,y)
	self.资源组[3]:更新(x,y)
	self.资源组[8]:更新(x,y)
	self.关注:更新(x,y)
	self.资源组[4]:更新(x,y,self.选中道具 ~= 0 or self.选中召唤兽 ~= 0 or self.选中制造 ~= 0 )
	for i=5,7 do
		self.资源组[i]:更新(x,y,self.状态~=self.资源组[i]:取文字())
	end
	if self.资源组[3]:事件判断() then
		self:打开()
	elseif self.关注:事件判断() then
		self.关注:置打勾框(not self.关注显示)
		self.关注显示 = not self.关注显示
		tp.场景.玩家[self.招牌id].摊位关注 = self.关注显示
	elseif self.资源组[4]:事件判断() then
		if self.状态 == "物品类" then
			if self.选中道具 == 0 then
				tp.提示:写入("#Y/请先选中一个要出售的道具")
			else
				客户端:发送数据(self.选中道具, 8, 44, self.输入框:取文本()+0, 1)
			end
		elseif self.状态 == "召唤兽类" then
			if self.选中召唤兽 == 0 then
				tp.提示:写入("#Y/请先选中一只召唤兽")
			else
				客户端:发送数据(self.选中召唤兽, 11, 44,1)
			end
		elseif self.状态 == "制造类" then
			if self.选中制造 == 0 then
				tp.提示:写入("#Y/请先选中一项技能")
			else
				客户端:发送数据(self.选中制造, 16, 44, 1)
			end
		end
	elseif self.资源组[5]:事件判断() then
		客户端:发送数据(2647, 9, 44, "1", 1)
	elseif self.资源组[6]:事件判断() then
		客户端:发送数据(4512, 10, 44, "2", 1)
	elseif self.资源组[7]:事件判断() then
		客户端:发送数据(4512, 13, 44, "2", 1)
	elseif self.资源组[8]:事件判断() then

	end
	self.焦点1 = false
	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[3]:显示(self.x+270,self.y+5)
	self.资源组[4]:显示(self.x+200,self.y+390,true)
	self.资源组[8]:显示(self.x+30,self.y+390,true)
	zts:置颜色(0xFFFFFF00):显示(self.x+111,self.y+33,self.招牌名称)
	zts:置颜色(0xFF000000):显示(self.x+61,self.y+60,self.招牌角色)
	zts:置颜色(0xFF000000):显示(self.x+204,self.y+60,self.招牌id)
	zts:置颜色(tos(self.当前银子+0)):显示(self.x+195,self.y+365,self.当前银子)
	zts1:显示(self.x+183,self.y + 31,"关注摊位")
	self.关注:显示(self.x+243,self.y + 25)
	for i=5,7 do
		self.资源组[i]:显示(self.x+20+(i-5)*90,self.y+88,true,nil,nil,self.状态==self.资源组[i]:取文字(),2)
	end
	local xx = 0
	local yy = 0
	if self.状态== "物品类" then
	   Picture.物品格子背景:显示(self.x+9+9,self.y+29+90)
		for i=1,20 do
			self.商品[i]:置坐标(self.x + xx * 51 + 19,self.y + yy * 51 + 28+90)
			self.商品[i]:显示(dt,x,y,self.鼠标)
			if self.商品[i].物品 ~= nil and self.商品[i].焦点 then
				tp.提示:道具行囊(x,y,self.商品[i].物品,true)
				if mouseb(0) then
					self.选中道具=i
					if self.上一次 ==self.选中道具   then
						if self.商品[self.选中道具].物品.数量~=nil  and self.数量<self.商品[self.选中道具].物品.数量 then
                        self.数量=self.数量+1
                        end
					else
						self.商品[self.上一次].确定 = false
						self.商品[i].确定 = true
						self.上一次 = i
						self.数量=1
					end
						self.输入框:置可视(true,true)
						self.输入框:置文本(self.数量)
				elseif 	mouseb(1) then
					if self.上一次 ==self.选中道具 and self.数量 >1 then
                        self.数量=self.数量-1
                    end
					self.输入框:置可视(true,true)
					self.输入框:置文本(self.数量)
				end

			end


			xx = xx + 1
			if xx == 5 then
				xx = 0
				yy = yy + 1
			end
		end
			if self.选中道具~=0 then

				zts:置颜色(tos(self.商品[self.选中道具].物品.单价)):显示(self.x+65,self.y+335,self.商品[self.选中道具].物品.单价)
				
				self.输入框:置坐标(self.x,self.y)
				-- zts:置颜色(0xFF000000):显示(self.x+195,self.y+335,self.数量)
				zts:置颜色(tos(self.商品[self.选中道具].物品.单价*self.数量)):显示(self.x+65,self.y+365,self.商品[self.选中道具].物品.单价*self.数量)
			end
	elseif self.状态== "召唤兽类" then
		self.资源组[9]:更新(x,y)
		self.资源组[10]:更新(x,y)
		self.资源组[20]:显示(self.x+9+9,self.y+118)
		tp.竖排花纹背景_:置区域(0,0,18,209)
		tp.竖排花纹背景_:显示(self.x+258,self.y+118)
		self.资源组[9]:显示(self.x+258,self.y+118)
		self.资源组[10]:显示(self.x+258,self.y+310)
        local 序号=0
	    for i=1,12 do
	     	if self.召唤兽数据 [i]~=nil then
	     		序号 =序号 +1
				zts:置颜色(-16777216)
				local jx = self.x+28
				local jy = self.y+111+序号*20
				bw:置坐标(jx-4,jy-3)
				local xz = bw:检查点(x,y)
				if self.选中召唤兽 ~= i then
					if  xz and not tp.消息栏焦点 and self.鼠标 then
						box(jx-5,jy-6,jx+230,jy+15,-3551379)
						self.焦点 = true
						self.焦点1 = true
						if mouseb(0) then
			                 self.选中召唤兽 = i
						elseif mouseb(1)  then
							tp.窗口.召唤兽查看栏:打开(self.召唤兽数据[i])
						end
					end
                else
					if  xz and not tp.消息栏焦点 and self.鼠标 then
						self.焦点 = true
						self.焦点1 = true
						if mouseb(1)  then
							tp.窗口.召唤兽查看栏:打开(self.召唤兽数据[i])
						end
					end
						box(jx-5,jy-6,jx+230,jy+15,-10790181)
				end
				zts:置颜色(0xFF000000):显示(jx,jy-2,self.召唤兽数据[i].名称)
			end
		end
		if self.选中召唤兽 ~= 0 then
			zts:置颜色(tos(self.召唤兽数据[self.选中召唤兽].单价)):显示(self.x+65,self.y+335,self.召唤兽数据[self.选中召唤兽].单价)
			zts:置颜色(0xFF000000):显示(self.x+195,self.y+335,1)
			zts:置颜色(tos(self.召唤兽数据[self.选中召唤兽].单价)):显示(self.x+65,self.y+365,self.召唤兽数据[self.选中召唤兽].单价)
	    end
    elseif self.状态== "制造类" then
    	self.资源组[19]:显示(self.x+9+9,self.y+118)
        local 序号=0
	    for i=1,5 do
	     	if self.制造[i]~=nil then
	     		序号 =序号 +1
				zts:置颜色(-16777216)
				local jx = self.x+28
				local jy = self.y+136+序号*20
				bw:置坐标(jx-4,jy-3)
				local xz = bw:检查点(x,y)
				if self.选中制造 ~= i then
					if  xz and not tp.消息栏焦点 and self.鼠标 then
						box(jx-5,jy-6,jx+230,jy+15,-3551379)
						self.焦点 = true
						self.焦点1 = true
						if mouseb(0) then
			                 self.选中制造 = i
						elseif mouseb(1)  then
								local xx = SkillData[self.制造[i].制造.名称] 
							tp.提示:自定义(x,y+40,xx.介绍)
						end
					end
                else
					if  xz and not tp.消息栏焦点 and self.鼠标 then
						self.焦点 = true
						self.焦点1 = true
						if mouseb(1)  then
								local xx = SkillData[self.制造[i].制造.名称]
							tp.提示:自定义(x,y+40,xx.介绍)
						end
					end
						box(jx-5,jy-6,jx+230,jy+15,-10790181)
				end
				zts:置颜色(0xFF000000):显示(jx,jy-2,self.制造[i].制造.名称)
				zts:置颜色(0xFF000000):显示(jx+110,jy-2,self.制造[i].制造.等级)
				zts:置颜色(tos(self.制造[i].单价)):显示(jx+180,jy-2,self.制造[i].单价)
			end
		end
		if self.选中制造 ~= 0 then
			zts:置颜色(tos(self.制造[self.选中制造].单价)):显示(self.x+65,self.y+335,self.制造[self.选中制造].单价)
			zts:置颜色(0xFF000000):显示(self.x+195,self.y+335,1)
			zts:置颜色(tos(self.制造[self.选中制造].单价)):显示(self.x+65,self.y+365,self.制造[self.选中制造].单价)
				tp.提示:自定义(self.x-50,self.y+50,SkillData[self.制造[self.选中制造].制造.名称].介绍)
	    end
    end
	if self.输入框._已碰撞 then
	self.焦点 = true
	end
	self.控件类:更新(dt,x,y)
	 self.控件类:显示(x,y)
end



return 摆摊购买
