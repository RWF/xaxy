--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:47
--======================================================================--
local 坐骑类 = class()
local bd ={"体质","魔力","力量","耐力","敏捷"}

local tp,zts,zts1
local insert = table.insert
local mouseb = 引擎.鼠标弹起

local box = 引擎.画矩形
function 坐骑类:初始化(根)
  self.ID = 148
  self.x = 0
  self.y = 0
  self.xx = 0
  self.yy = 0
  self.注释 = "坐骑"
  self.可视 = false
  self.鼠标 = false
  self.焦点 = false
  self.可移动 = true
  local 资源 = 根.资源
  local 按钮 = 根._按钮
  local 自适应 = 根._自适应
  self.窗口时间 = 0
  tp = 根
  self.临时潜力 = {体质=0,魔力=0,力量=0,耐力=0,敏捷=0}
  zts = tp.字体表.普通字体
  zts1 = tp.字体表.描边字体
	self.背景 =  资源:载入('JM.dll',"网易WDF动画",0x14ADA146)
	--self.选中背景 = 图像类("imge/005/2.jpg")
	self.骑乘 = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"骑乘")
	self.骑乘:绑定窗口_(148)
	self.装饰 = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"装饰")
	self.装饰:绑定窗口_(148)
	self.观看 = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"观看")
	self.观看:绑定窗口_(148)
	self.修炼 = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"修炼")
	self.修炼:绑定窗口_(148)
	self.驯养 = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"驯养")
	self.驯养:绑定窗口_(148)
    self.关闭 = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true)
	self.关闭:绑定窗口_(148)
	self.喂养 = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"喂养")
	self.喂养:绑定窗口_(148)
	self.放生 = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"放生")
	self.放生:绑定窗口_(148)
  	self.属性按钮 =  按钮.创建(自适应.创建(16,4,75,22,1,3),0,0,4,true,true,"更改属性")
  	self.属性按钮:绑定窗口_(148)
  	  self.查看技能 =  按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xB15C5678),0,0,4,true,true,"查看技能")
  	self.查看技能:绑定窗口_(148)
	self.查看技能:置偏移(0,3)
		self.坐骑上 = 按钮.创建(自适应.创建(20,4,25,19,4,3),0,0,4,true,true) --上
		self.坐骑上:绑定窗口_(148)
		self.坐骑下 = 按钮.创建(自适应.创建(21,4,25,19,4,3),0,0,4,true,true)--下
		self.坐骑下:绑定窗口_(148)
		self.召唤兽上 = 按钮.创建(自适应.创建(20,4,25,19,4,3),0,0,4,true,true) --上
		self.召唤兽上:绑定窗口_(148)
		self.召唤兽下 = 按钮.创建(自适应.创建(21,4,25,19,4,3),0,0,4,true,true)--下
		self.召唤兽下:绑定窗口_(148)
	self.改名 = 按钮.创建(自适应.创建(16,4,40,22,1,3),0,0,4,true,true,"改名")
	self.改名:绑定窗口_(148)
   	self.选中编号 = 0
   	self.加点按钮={}
   	self.减点按钮={}
	for n = 1, 5, 1 do
		self.加点按钮[n] = 按钮.创建(自适应.创建(25,4,19,19,4,3),0,0,4,true,true)
		self.减点按钮[n] =  按钮.创建(自适应.创建(26,4,19,19,4,3),0,0,4,true,true)
	end
	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('商店总控件')
	总控件:置可视(true,true)
	self.名称输入框 = 总控件:创建输入("数量输入016",self.x + 333,self.y + 82,120,14)
	self.名称输入框:置可视(false,false)
	self.名称输入框:置限制字数(16)
	self.名称输入框:置光标颜色(-16777216)
	self.名称输入框:置文字颜色(-16777216)
end
function 坐骑类:打开(数据)
	if self.可视 then
		self.可视 = false
		 self.选中编号 = 0
		 self.人物动画=nil
		 self.名称输入框:置焦点(false)
		self.名称输入框:置可视(false,false)
		self.坐骑动画=nil
		self.坐骑饰品=nil
		self.临时潜力 = {体质=0,魔力=0,力量=0,耐力=0,敏捷=0}
	else
		self.坐骑饰品=nil
		if  self.x > 全局游戏宽度 then
		self.x = 0
		end
		self.名称输入框:置可视(true,true)
		self.人物动画=nil
		self.坐骑动画=nil
		self.临时潜力 = {体质=0,魔力=0,力量=0,耐力=0,敏捷=0}
		 self.选中编号 = 0
        self.坐骑= 数据.坐骑
        self.坐骑数据 = 数据.坐骑数据
        for n = 1, #self.坐骑数据, 1 do
		self.坐骑数据[n].包围盒 = require("gge包围盒")(320, 240 + n * 20,string.len(self.坐骑数据[n].名称) * 9, 18)
		end

		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end
function 坐骑类:刷新(数据)
		self.临时潜力 = {体质=0,魔力=0,力量=0,耐力=0,敏捷=0}
        self.坐骑= 数据.坐骑
        self.坐骑数据 = 数据.坐骑数据
        for n = 1, #self.坐骑数据, 1 do
		self.坐骑数据[n].包围盒 = require("gge包围盒")(320, 240 + n * 20,string.len(self.坐骑数据[n].名称) * 9, 18)
		end
		if self.选中编号~=0 then
		  self:创建动画()
		end
end
function 坐骑类:创建动画()

	self.坐骑饰品=nil
	if self.选中编号~= 0 then
	
	   	local 资源组 = 引擎.坐骑库(tp.场景.人物.数据.造型,self.坐骑数据[self.选中编号].类型,self.坐骑数据[self.选中编号].饰品 or "空")
		self.人物动画=tp.资源:载入(资源组.人物资源,"网易WDF动画",资源组.人物站立)
		self.坐骑动画=  tp.资源:载入(资源组.坐骑资源,"网易WDF动画",资源组.坐骑站立)
   		if self.坐骑数据[self.选中编号].染色组 then
   			self.坐骑动画:置染色(self.坐骑数据[self.选中编号].类型,self.坐骑数据[self.选中编号].染色组[1],self.坐骑数据[self.选中编号].染色组[2],self.坐骑数据[self.选中编号].染色组[3])
   		end

		if 资源组.坐骑饰品站立 ~= nil then
		self.坐骑饰品 = tp.资源:载入(资源组.坐骑饰品资源,"网易WDF动画",资源组.坐骑饰品站立)
			 if self.坐骑数据[self.选中编号].装备.染色组 then
			    self.坐骑饰品:置染色(self.坐骑数据[self.选中编号].装备.名称,self.坐骑数据[self.选中编号].装备.染色组[1],self.坐骑数据[self.选中编号].装备.染色组[2],self.坐骑数据[self.选中编号].装备.染色组[3])
			 end
    	 self.坐骑饰品:置方向(0)
		end
		
		self.人物动画:置染色(DyeData[tp.场景.人物.数据.造型][1],tp.场景.人物.数据.染色.a,tp.场景.人物.数据.染色.b,tp.场景.人物.数据.染色.c,0)
		self.人物动画:置方向(0)
		self.坐骑动画:置方向(0)
		if self.坐骑.编号 ~= self.选中编号 then
			self.人物动画 = nil
		end
	end

end

function 坐骑类:显示(dt,x, y)
	for n = 1, #self.坐骑数据, 1 do
		if self.坐骑数据[n].包围盒:检查点(x, y) and 引擎.鼠标弹起(0) then
			self.选中编号 = n
			self.名称输入框:置文本( self.坐骑数据[n].名称)
			self:创建动画()

		end
	end

	if self.坐骑动画 ~= nil then
		self.坐骑动画:更新(dt)
	end

	if self.人物动画 ~= nil then
		self.人物动画:更新(dt)
	end
		if self.坐骑饰品 ~= nil then
		self.坐骑饰品:更新(dt)
	end

	self.骑乘:更新(x,y,self.选中编号~=0)
	self.装饰:更新(x,y,self.选中编号~=0)
	self.观看:更新(x,y,self.选中编号~=0)
	self.修炼:更新(x,y,self.选中编号~=0)
	self.驯养:更新(x,y,self.选中编号~=0)
	self.喂养:更新(x,y,self.选中编号~=0)
	self.放生:更新(x,y,self.选中编号~=0)
	self.改名:更新(x,y,self.选中编号~=0)
	self.坐骑上:更新(x,y)
	self.坐骑下:更新(x,y)
	self.召唤兽上:更新(x,y)
	self.召唤兽下:更新(x,y)
	self.查看技能:更新(x,y,self.选中编号~=0)
	self.关闭:更新(x,y)
	self.属性按钮:更新(x,y,self.选中编号~=0)
	self.焦点=false
	if self.骑乘:事件判断() then
		if self.选中编号 == 0 then
			tp.提示:写入("#y/请先选择一只坐骑")
		else
			客户端:发送数据(self.选中编号, 6, 48, "9", 1)
		end
	elseif self.驯养:事件判断() then
		if self.选中编号 == 0 then
			tp.提示:写入("#y/请先选择一只坐骑")
		else
			客户端:发送数据(self.选中编号, 6, 49, "9", 1)
		end
	elseif self.改名:事件判断() then
		if self.选中编号 == 0 then
			tp.提示:写入("#y/请先选择一只坐骑")
		else
			客户端:发送数据(self.选中编号, 6, 58, self.名称输入框:取文本())
		end
	elseif self.放生:事件判断() then
		if self.选中编号 == 0 then
			tp.提示:写入("#y/请先选择一只坐骑")
		else
		tp.窗口.文本栏:载入("#H/放生以后这个坐骑将会消失,你确定要执行?","坐骑",true)
		end
	elseif self.查看技能:事件判断() then
		tp.窗口.坐骑技能:打开(self.坐骑数据[self.选中编号])

	elseif self.属性按钮:事件判断() then
		    客户端:发送数据(self.选中编号, 3, 50, self.临时潜力.力量.."*-*"..self.临时潜力.体质.."*-*"..self.临时潜力.魔力.."*-*"..self.临时潜力.耐力.."*-*"..self.临时潜力.敏捷, 1)
	elseif self.关闭:事件判断() then
		self:打开()
	end



	self.背景:显示(self.x+100, self.y+50)
	self.骑乘:显示(self.x+105, self.y+230)
	self.装饰:显示(self.x+165, self.y+230)
	self.观看:显示(self.x+225, self.y+230)
	self.修炼:显示(self.x+290, self.y+440)
	self.驯养:显示(self.x+350, self.y+440)
	self.喂养:显示(self.x+410, self.y+440)
	self.放生:显示(self.x+315, self.y+473)
	self.关闭:显示(self.x+452, self.y+54)
	self.属性按钮:显示(self.x+390, self.y+253)
	self.查看技能:显示(self.x+380, self.y+470)
	self.坐骑上:显示(self.x+257, self.y+255)
	self.坐骑下:显示(self.x+257, self.y+340)
	self.召唤兽上:显示(self.x+257, self.y+370)
	self.召唤兽下:显示(self.x+257, self.y+478)
	self.改名:显示(self.x+430, self.y+80)
	if self.选中编号~=0 then
			if self.选中编号 == self.坐骑.编号 then
				self.骑乘:置文字("下骑")
			else
				self.骑乘:置文字("骑乘")
			end
		for n = 1, 5, 1 do
			self.加点按钮[n]:更新(x,y,self.坐骑数据[self.选中编号].潜能>0,1)
			self.减点按钮[n]:更新(x,y,self.临时潜力[bd[n]] > 0,1)

			if self.加点按钮[n]:事件判断() then
				self.临时潜力[bd[n]] = self.临时潜力[bd[n]] + 1
				self.坐骑数据[self.选中编号].潜能 =self.坐骑数据[self.选中编号].潜能 - 1
			elseif self.减点按钮[n]:事件判断() then
				self.临时潜力[bd[n]] = self.临时潜力[bd[n]] - 1
				self.坐骑数据[self.选中编号].潜能 = self.坐骑数据[self.选中编号].潜能 + 1
			end
		self.加点按钮[n]:显示(self.x+430, self.y+107 + n * 24)
		self.减点按钮[n]:显示(self.x+453, self.y+107 + n * 24)
		end
	end

	self.名称输入框:置坐标(self.x,self.y)
	for n = 1, #self.坐骑数据, 1 do
		if self.选中编号 == n then
			box(self.x+117,self.y +240 + n* 20 ,self.x + 250,self.y +240 +n * 20 + 20,-9670988)


			zts:置颜色(0xFF000000):显示(self.x+333, self.y+107, self.坐骑数据[n].等级)
			for i=1,5 do
				if self.临时潜力[bd[i]] > 0 then
					zts:置颜色(-65536)
				else
					zts:置颜色(-16777216)
				end
				zts:显示(self.x + 333,self.y + 132 + ((i-1)*25) ,self.坐骑数据[n][bd[i]]+self.临时潜力[bd[i]])
			end
			zts:置颜色(0xFF000000):显示(self.x+333, self.y+257, self.坐骑数据[n].潜能)
			zts:置颜色(0xFF000000):显示(self.x+353, self.y+282, self.坐骑数据[n].成长)
			zts:置颜色(0xFF000000):显示(self.x+353, self.y+307, self.坐骑数据[n].环境度)
			zts:置颜色(0xFF000000):显示(self.x+353, self.y+332, self.坐骑数据[n].好感度)
			zts:置颜色(0xFF000000):显示(self.x+353, self.y+357, self.坐骑数据[n].饱食度)
			zts:置颜色(0xFF000000):显示(self.x+363, self.y+382, self.坐骑数据[n].当前经验)
			zts:置颜色(0xFF000000):显示(self.x+363, self.y+407, self.坐骑数据[n].升级经验)
		end
		if self.坐骑.编号 == n then
			zts:置颜色(0xFFFFFF00):显示(self.x+120, self.y+243 + n * 20, self.坐骑数据[n].名称)
		else
		 	zts:置颜色(0xFF000000):显示(self.x+120, self.y+243 + n * 20, self.坐骑数据[n].名称)
		end

		self.坐骑数据[n].包围盒:置坐标(self.x+120, self.y+240 + n * 20)
	end

	if self.坐骑动画 ~= nil then
		tp.影子:显示(self.x+190, self.y+200)
		self.坐骑动画:显示(self.x+190, self.y+200)
		if self.坐骑饰品 ~= nil then
			self.坐骑饰品:显示(self.x+190, self.y+200)
		end
		if self.人物动画 ~= nil then
			self.人物动画:显示(self.x+190, self.y+200)
		end

	end
	self.控件类:更新(dt,x,y)
	if  self.名称输入框._已碰撞 then
		self.焦点 = true
	end
	self.控件类:显示(x,y)
end

function 坐骑类:检查点(x,y)
  if self.背景:是否选中(x,y)  then
    return true
  end
end

function 坐骑类:初始移动(x,y)
  tp.运行时间 = tp.运行时间 + 1
  self.窗口时间 = tp.运行时间
  if not self.焦点 then
    tp.移动窗口 = true
  end
  if self.鼠标 and  not self.焦点 then
    self.xx = x - self.x
    self.yy = y - self.y
  end
end

function 坐骑类:开始移动(x,y)
  if self.鼠标 then
    self.x = x - self.xx
    self.y = y - self.yy
  end
end
return 坐骑类
