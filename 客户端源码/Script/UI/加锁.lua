--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:46
--======================================================================--
local 场景类_加锁 = class(窗口逻辑)
local bw = require("gge包围盒")(0,0,80,22)
local box = 引擎.画矩形
local tp,zts,zts1
local floor = math.floor
local tonumber = tonumber
local tostring = tostring
local format = string.format
local insert = table.insert

function 场景类_加锁:初始化(根)
	self.ID = 116
	self.x = 300+(全局游戏宽度-800)/2
	self.y = 100
	self.xx = 0
	self.yy = 0
	self.注释 = "加锁"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 根.资源:载入('JM.dll',"网易WDF动画",0xA86FECEB),
		[2] = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true),
		[3] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"加锁"),
		[4] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"解锁"),
		[5] = 按钮.创建(自适应.创建(16,4,75,22,1,3),0,0,4,true,true,"修改密码"),

	}
	for n=2,5 do
	   self.资源组[n]:绑定窗口_(116)
	end
	self.物品 = {}
	local 格子 = 根._物品格子
	for i=1,20 do
		self.物品[i] = 格子(0,0,i,"加锁")
	end
  self.上一次 = 0
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.描边字体
	self.选中召唤兽 = 0
	self.加入=0
end

function 场景类_加锁:打开(数据)
	if self.可视 then
		self.道具 = nil
		self.选中召唤兽 = 0
		self.加入=0
		self.可视 = false
		if self.上一次 ~= 0 then
			self.物品[self.上一次].确定 = false
		end
		self.上一次 = 0

	else
		  			if  self.x > 全局游戏宽度 then
		self.x = 300+(全局游戏宽度-800)/2
		end
		self.数据=数据
		self.资源组[7]=nil
		for i=1,20 do
		self.物品[i]:置物品(self.数据[i])
		end

		if self.上一次 ~= 0 then
			self.物品[self.上一次].确定 = false
		end
		-- self:置形象()
		self.上一次 = 0
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间

	    self.可视 = true
	end
end
function 场景类_加锁:置形象()
		if self.选中召唤兽 ~= 0 and self.数据.宝宝列表[self.选中召唤兽]  ~= nil then
			local n = 取模型(self.数据.宝宝列表[self.选中召唤兽].造型)
			self.资源组[7] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
			if self.数据.宝宝列表[self.选中召唤兽].染色方案 ~= nil then
				self.资源组[7]:置染色(self.数据.宝宝列表[self.选中召唤兽].染色方案,self.数据.宝宝列表[self.选中召唤兽].染色组[1],self.数据.宝宝列表[self.选中召唤兽].染色组[2],self.数据.宝宝列表[self.选中召唤兽].染色组[3])
				self.资源组[7]:置方向(0)
			end
		end

end

function 场景类_加锁:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y)
	self.资源组[4]:更新(x,y)
	self.资源组[5]:更新(x,y)
	if self.鼠标 then
		if self.资源组[2]:事件判断() then
			self:打开()
		elseif self.资源组[3]:事件判断() then --加锁
			if self.上一次==0 and self.选中召唤兽==0 then
			    tp.提示:写入("#Y/请选择你所需要加锁的召唤或者物品!")
			else
				客户端:发送数据(self.上一次,199,13,self.选中召唤兽)
				self:打开()
			end

		elseif self.资源组[4]:事件判断() then --解锁
          			if self.上一次==0 and self.选中召唤兽==0 then
			    tp.提示:写入("#Y/请选择你所需要加锁的召唤或者物品!")
			else
				客户端:发送数据(self.上一次,200,13,self.选中召唤兽)
				self:打开()
			end
		end
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x+250,self.y+6)
	self.资源组[3]:显示(self.x+30,self.y+420,true)
	self.资源组[4]:显示(self.x+100,self.y+420,true)
	self.资源组[5]:显示(self.x+170,self.y+420,true)
	local xx = 0
	local yy = 0
	for i=1,20 do
		local jx = xx * 51 + 10
		local jy = yy * 51 + 183
		self.物品[i]:置坐标(self.x + jx,self.y + jy)
		self.物品[i]:显示(dt,x,y,self.鼠标)
		if self.物品[i].焦点 and self.物品[i].物品 ~= nil then
			tp.提示:道具行囊(x,y,self.物品[i].物品)
		  if self.物品[i].事件  and self.鼠标 then
				if  self.上一次 ~= 0 then
					self.物品[self.上一次].确定 = false
				end
				self.物品[i].确定 = true
				self.道具 = self.物品[i].物品
				self.上一次 = i

		end
	end
		xx = xx + 1
		if xx == 5 then
			xx = 0
			yy = yy + 1
		end
	end

  zts:置颜色(0xFF000000)
		for m=1,4 do
			if self.数据.宝宝列表[m + self.加入] ~= nil then
				bw:置坐标(self.x + 162,self.y + 24 + m * 23)
				if self.选中召唤兽 ~= m + self.加入 then
					if bw:检查点(x,y) then
						box(self.x + 159,self.y + 24 + m * 22,self.x + 243,self.y + 24 + m * 22 + 22,-3551379)
						if 引擎.鼠标弹起(0) and self.鼠标 then
							self.选中召唤兽 = m + self.加入
							self:置形象()
						end
						self.焦点 = true
					end
				else
					local ys = -10790181
					if bw:检查点(x,y) then
						ys = -9670988
						self.焦点 = true
					end
					box(self.x + 159,self.y + 24 + m * 22,self.x + 243,self.y + 24 + m * 22 + 22,ys)
				end
				zts:显示(self.x + 162,self.y + 27 + m * 23,self.数据.宝宝列表[m + self.加入].名称)
			end
		end
		if  self.数据.宝宝列表[self.选中召唤兽] ~= nil then
			zts:显示(self.x + 69,self.y + 145 ,format("%s/%s",self.数据.宝宝列表[self.选中召唤兽].当前气血,self.数据.宝宝列表[self.选中召唤兽].气血上限))
			zts:显示(self.x + 69,self.y + 166 ,format("%s/%s",self.数据.宝宝列表[self.选中召唤兽].当前魔法,self.数据.宝宝列表[self.选中召唤兽].魔法上限))
		end
		if self.资源组[7] ~= nil then
			tp.影子:显示(self.x + 88,self.y + 115)
			self.资源组[7]:显示(self.x + 88,self.y + 115)
			self.资源组[7]:更新(dt)
		end
zts:置颜色(0xFFFFFFFF)



end



return 场景类_加锁