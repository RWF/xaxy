



local 场景类_活跃兑换 = class(窗口逻辑)
local insert = table.insert
local remove = table.remove
local floor = math.floor
local tp,zts
local tos = 引擎.取金钱颜色
local mousea = 引擎.鼠标按住
local mouseb = 引擎.鼠标弹起

function 场景类_活跃兑换:初始化(根)
    self.ID = 313
    self.x = 190+(全局游戏宽度-800)/2
    self.y = 65
    self.xx = 0
    self.yy = 0
    self.注释 = "活跃兑换"

    tp = 根
        self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    local 按钮 = 根._按钮
    local 自适应 = 根._自适应
    local  资源= 根.资源
    self.资源组 = {
        [1] = 根.资源:载入("活跃度兑换.png","加密图片"),
        [2] =  按钮.创建(资源:载入('新关闭按钮',"动画"),0,0,4,true,true), 
        [3] = 按钮.创建(资源:载入('MAX.7z',"网易WDF动画",0),0,0,4,true,true," 兑  换"),

    }

    self.商品 = {}
    local 物品格子 = 根._物品格子
    for i=1,48 do
        self.商品[i] = 物品格子(0,i,n,"商店1")
    end

    self.翻页 =false
    self.窗口时间 = 0



    self.控件类 = require("ggeui/加载类")()
    local 总控件 = self.控件类:创建控件('商店总控件')
    总控件:置可视(true,true)
    self.输入框 = 总控件:创建输入("数量输入",self.x + 259-190,self.y +374-65,100,14)
    self.输入框:置可视(false,false)
    self.输入框:置限制字数(3)
    self.输入框:置数字模式()
    self.输入框:置光标颜色(-16777216)
    self.输入框:置文字颜色(0xFFFFFFFF)
    self.单价 = 0
    self.数量 = 0
    self.上一次 = 1

    zts = 根.字体表.华康字体

    self.焦点1 = false

end





function 场景类_活跃兑换:打开(内容)
  if self.可视 then

        self.可视 = false
        for n=1,48 do
            self.商品[n]:置物品(nil)
            self.商品[n].确定 = false
        end
    self.数量 = 0
    self.单价 = 0
    self.上一次 = 1
    self.输入框:置可视(false,false)


  else
        if  self.x > 全局游戏宽度 then
        self.x = 100
        end

       self.数据 =内容
        for n=1,48 do
            self.商品[n]:置物品(self.数据[n])

        end

        if  self.x > 全局游戏宽度 then
        self.x = 82+(全局游戏宽度-800)/2
        end
        insert(tp.窗口_,self)
        tp.运行时间 = tp.运行时间 + 1
        self.窗口时间 = tp.运行时间
        self.可视 = true
  end
 end
function 场景类_活跃兑换:显示(dt,x,y)
    self.焦点 = false
    self.资源组[2]:更新(x,y)
    self.资源组[3]:更新(x,y,self.道具 ~= nil)

    self.资源组[1]:显示(self.x,self.y)
    self.资源组[2]:显示(self.x+403,self.y+3)
     self.资源组[3]:显示(self.x+345,self.y+366,true,1)
    if self.资源组[2]:事件判断() then
      self:打开()
    elseif self.资源组[3]:事件判断() then
        客户端:发送数据(self.商品[self.上一次].物品.number,2,14,self.数量)
    end
        local xx = 0
            local yy = 0

    for i=1,48 do
                self.商品[i]:置坐标(self.x + xx * 51+13,self.y + yy * 51 +34)
                self.商品[i]:显示(dt,x,y,self.鼠标,nil,nil,nil,nil,nil,true)
                if self.商品[i].物品 ~= nil and self.商品[i].焦点 then
                    tp.提示:道具行囊(x,y,self.商品[i].物品,true)
                    if mouseb(0) then
                        if self.道具 == nil then
                            self.商品[self.上一次].确定 = false
                            self.商品[i].确定 = true
                            self.上一次 = i
                            self.道具 = self.商品[i].物品
                            self.单价 = tostring(self.道具.价格)
                            self.数量 = 1
                            self.输入框:置可视(true,true)
                        else
                            if self.上一次 == i then
                                self.数量 = self.数量 + 1
                            else
                                self.商品[self.上一次].确定 = false
                                self.商品[i].确定 = true
                                self.上一次 = i
                                self.道具 = self.商品[i].物品
                                self.单价 = tostring(self.道具.价格)
                                self.数量 = 1
                                self.输入框:置可视(true,true)
                            end
                        end
                        self.输入框:置文本(self.数量)
                    end
                   if mouseb(1) then
                        if self.道具~= nil then
                            if self.上一次 == i   then
                                self.数量  = self.数量 - 1
                            else
                                self.商品[self.上一次].确定 = false
                                self.商品[i].确定 = true
                                self.上一次 = i
                                self.道具 = self.商品[i].物品
                                self.单价 = tostring(self.道具.价格)
                                self.数量 = 1
                                self.输入框:置可视(true,true)
                            end
                        end
                        self.输入框:置文本(self.数量)
                    end
                end
                xx = xx + 1
                if xx == 8 then
                    xx = 0
                    yy = yy + 1
                end
            end
            if self.道具 ~= nil then


                self.输入框:置坐标(self.x,self.y)
                if self.输入框:取文本() == "" then
                    self.输入框:置文本(1)
                end
                if self.输入框:取文本()+0 > 99 then
                    self.输入框:置文本(99)
                end
                if self.输入框:取文本()+0 < 0 then
                    self.输入框:置文本(0)
                end
                self.数量 = tonumber(self.输入框:取文本())
                zts:置颜色(0xFFFFFFFF):显示(self.x + 115+40,self.y + 370,(self.数量 * self.单价))
            end

          zts:置颜色(0xFFFFFFFF):显示(self.x + 27,self.y + 347,"   活跃度           总价          数量")


            zts:置颜色(0xFFFFFFFF):显示(self.x + 23,self.y + 370,self.数据.银子)
            self.控件类:更新(dt,x,y)
            if self.输入框._已碰撞 then
                self.焦点 = true
            end
             self.控件类:显示(x,y)
end




return 场景类_活跃兑换



