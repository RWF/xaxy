-- @Author: 作者QQ414628710
-- @Date:   2022-01-15 20:06:44
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-21 22:44:37
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--
local 场景类_组合输入框 = class()

local floor = math.floor
local zts,tp
local insert = table.insert
function 场景类_组合输入框:初始化(根)
	self.ID = 38
	self.x = 150
	self.y = 246
	self.xx = 0
	self.yy = 0
	self.注释 = "组合输入框"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 资源:载入('JM.dll',"网易WDF动画",0x675760A4),
		[2] = 按钮(自适应.创建(12,4,55,22,1,3),0,0,4,true,true,"确定"),
		[3] = 按钮(自适应.创建(12,4,55,22,1,3),0,0,4,true,true,"取消"),
	}
	-- for i=2,3 do
	--     self.资源组[i]:绑定窗口_(38)
	-- end
	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('改名总控件')
	总控件:置可视(true,true)
	self.输入框 = 总控件:创建输入("输入",0,0,400,14)
	self.输入框:置可视(false,false)
    self.输入框:置光标颜色(-16777216)
	self.输入框:置文字颜色(-16777216)
	self.窗口时间 = 0
	tp = 根
	zts = 根.字体表.普通字体
end

function 场景类_组合输入框:打开(类型,附加,ID)
	if self.可视 then
		self.回调事件 = nil
		self.类型事件 = nil
		self.输入框:置可视(false,false)
		self.可视 = false
	else
		if  self.x > 全局游戏宽度 then
		self.x = 150
		end
		self.输入框:置文本("")
		insert(tp.窗口_,self)
		if 类型 == "改名" then
			self.输入框:置限制字数(12)
	    elseif 类型 == "帮派名称" then
	    	self.输入框:置限制字数(12)
	    elseif 类型 == "大雁塔" then
	    	self.输入框:置限制字数(3)
	    	self.输入框:置数字模式()
	    elseif 类型 == "拆分" then
	    	self.输入框:置限制字数(3)
	    	self.输入框:置数字模式()
	    elseif 类型 == "帮派宗旨" or 类型=="传音"then
	    	self.输入框:置限制字数(60)
	   	elseif 类型 == "影蛊" or 类型 == "删除角色" then
			self.输入框:置限制字数(12)
			self.输入框:置数字模式()
		end
		self.输入框:置可视(true,true)
		self.回调事件 = 附加
		self.类型事件 = 类型
		self.ID=ID
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end


function 场景类_组合输入框:显示(dt,x,y)
	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
	zts:置颜色(0xFFFFFFFF)
	zts:显示(self.x+36,self.y+38,self.回调事件)
	self.控件类:更新(dt,x,y)
	self.控件类:显示(dt,x,y)
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y)
	if self.资源组[2]:事件判断() then
		if self.类型事件 == "改名" then
            客户端:发送数据(51,51,51,self.输入框:取文本())
			self:打开()
		elseif self.类型事件 == "帮派名称" then
			客户端:发送数据(1, 62, 13,self.输入框:取文本())
			self:打开()
	    elseif self.类型事件 == "帮派宗旨" then
			客户端:发送数据(1, 63, 13, self.输入框:取文本())
			self:打开()
		elseif self.类型事件 == "删除角色" then

			客户端:发送数据(20, 20, 5, self.输入框:取文本())
			self:打开()
		elseif self.类型事件 == "传音" then

		
			客户端:发送数据(self.ID,0,16,self.输入框:取文本())
			self:打开()
		elseif self.类型事件 == "大雁塔" then
			if self.输入框:取文本() ~= nil and self.输入框:取文本() ~= "" then
				客户端:发送数据(self.输入框:取文本()+0 ,13, 6)
				self:打开()
			end
		elseif self.类型事件 == "影蛊" then

			客户端:发送数据(1, 243, 13, tonumber(self.输入框:取文本()))
			self:打开()
		elseif self.类型事件 == "拆分" then
	
			客户端:发送数据(tonumber(self.ID), 247, 13, tonumber(self.输入框:取文本()))
			self:打开()
		end
	elseif self.资源组[3]:事件判断() then
		self:打开()
		return false
	end
	self.资源组[2]:显示(self.x+355,self.y+97)
	self.资源组[3]:显示(self.x+425,self.y+97)
	self.输入框:置坐标(self.x + 47,self.y + 71)
	if self.输入框._已碰撞 then
		self.焦点 = true
	end
end

function 场景类_组合输入框:检查点(x,y)
	if self.资源组[1]:是否选中(x,y)  then
		return true
	end
end

function 场景类_组合输入框:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_组合输入框:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_组合输入框