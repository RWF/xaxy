



local 场景类_好友查找 = class()
local insert = table.insert
local remove = table.remove
local min = math.min
local ceil = math.ceil
local floor = math.floor
local tp,zts,zts1
-- local bw = require("gge包围盒")(0,0,270,53)
function 场景类_好友查找:初始化(根)
    self.ID = 319
    self.x = 0
    self.y = 0
    self.xx = 0
    self.yy = 0
    self.注释 = "好友查找"
    self.可视 = false
    self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    local 按钮 = 根._按钮
    local 自适应 = 根._自适应
    local  资源= 根.资源

    self.资源组 = {
    [8] = 资源:载入('JM.dll',"网易WDF动画",0X60D3F930),
    [9] = 按钮.创建(自适应.创建(16,4,43,22,1,3),0,0,4,true,true,"查找"),
    [10] = 按钮.创建(自适应.创建(16,4,72,22,1,3),0,0,4,true,true,"添加好友"),
    [11] = 按钮.创建(自适应.创建(16,4,72,22,1,3),0,0,4,true,true,"临时好友"),
    [12] = 按钮.创建(自适应.创建(16,3,72,22,1,3),0,0,4,true,true,"黑名单"),
    [13] = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true),
    }



    self.控件类 = require("ggeui/加载类")()
    local 总控件 = self.控件类:创建控件('给予总控件')
    总控件:置可视(true,true)
    self.输入框 = 总控件:创建输入("查找名称输入",self.x+250 + 53,self.y+150 + 40,100,14)
    self.输入框:置可视(false,false)
    self.输入框:置限制字数(12)
    self.输入框:置文本("")
    self.输入框:屏蔽快捷键(true)
    self.输入框:置光标颜色(-16777216)
    self.输入框:置文字颜色(-16777216)
  self.输入框1 = 总控件:创建输入("查找id输入",self.x+250 + 222,self.y+150 + 40,100,14)
  self.输入框1:置数字模式()
    self.输入框1:置可视(false,false)
    self.输入框1:置限制字数(11)
    self.输入框1:屏蔽快捷键(true)
    self.输入框1:置光标颜色(-16777216)
    self.输入框1:置文字颜色(-16777216)

    self.窗口时间 = 0
    tp = 根
    zts = tp.字体表.普通字体
    zts1 = tp.字体表.描边字体

end

function 场景类_好友查找:打开()
 self.数据=nil
  if self.可视 then

        self.可视 = false
        self.输入框:置焦点(false)
        self.输入框:置可视(false,false)
              self.输入框1:置焦点(false)
        self.输入框1:置可视(false,false)
  else
        self.输入框:置焦点(true)
        self.输入框:置可视(true,true)
       self.输入框1:置焦点(true)
        self.输入框1:置可视(true,true)
        if  self.x > 全局游戏宽度 then
        self.x = 82+(全局游戏宽度-800)/2
        end
        insert(tp.窗口_,self)
        tp.运行时间 = tp.运行时间 + 1
        self.窗口时间 = tp.运行时间
        self.可视 = true
  end
 end



function 场景类_好友查找:显示(dt,x,y)
  self.资源组[8]:显示(self.x+250,self.y+150)
  self.资源组[9]:更新(x,y)
  self.资源组[10]:更新(x,y)
  self.资源组[11]:更新(x,y)
  self.资源组[12]:更新(x,y)
  self.资源组[13]:更新(x,y)
  self.资源组[9]:显示(self.x+270,self.y+376)
  self.资源组[10]:显示(self.x+320,self.y+376)
  self.资源组[11]:显示(self.x+400,self.y+376)
  self.资源组[12]:显示(self.x+480,self.y+376)
  self.资源组[13]:显示(self.x+540,self.y+153)


 self.焦点=false
  if self.资源组[13]:事件判断() then
    self:打开()
  elseif self.资源组[9]:事件判断() then
    if self.输入框:取文本()=="" and self.输入框1:取文本()==""  then
      tp.提示:写入("#Y/请先输入要查找的角色名称或ID")
      return
    else
        客户端:发送数据(1,27,5,self.输入框:取文本(),self.输入框1:取文本())
    end
 elseif self.资源组[10]:事件判断() then
      if self.数据 then
        客户端:发送数据(self.数据.id+0,2, 54, "9s")
      else
         tp.提示:写入("#Y/请先搜索要查找的角色名称或ID")
      end
 elseif self.资源组[11]:事件判断() then
      if self.数据 then
      else
         tp.提示:写入("#Y/请先搜索要查找的角色名称或ID")
      end
  elseif self.资源组[12]:事件判断() then
      if self.数据 then
      else
         tp.提示:写入("#Y/请先搜索要查找的角色名称或ID")
      end
  end
  if self.数据 then
    zts:置颜色(0xFF000000):显示(self.x+250+42,self.y+150+110,"昵称："..self.数据.名称)
    zts:置颜色(0xFF000000):显示(self.x+250+42,self.y+150+130,"I  D："..self.数据.id)
    zts:置颜色(0xFF000000):显示(self.x+250+42,self.y+150+150,"等级："..self.数据.等级)
    zts:置颜色(0xFF000000):显示(self.x+250+42,self.y+150+170,"门派："..self.数据.门派)
  end


    if self.输入框._已碰撞 or self.输入框1._已碰撞 then
        self.焦点 = true
    end
  self.输入框:置坐标(self.x,self.y)
  self.输入框1:置坐标(self.x,self.y)
   self.控件类:显示(self.x,self.y)
    self.控件类:更新(dt,x,y)
 end


function 场景类_好友查找:检查点(x,y)
    if self.可视 and self.资源组[8]:是否选中(x,y)  then
        return true
    else
        return false
    end
end

function 场景类_好友查找:初始移动(x,y)
    tp.运行时间 = tp.运行时间 + 1
    if not tp.消息栏焦点 then
        self.窗口时间 = tp.运行时间
    end
    if not self.焦点 then
        tp.移动窗口 = true
    end
    if self.可视 and self.鼠标 and  not self.焦点 then
        self.xx = x - self.x
        self.yy = y - self.y
    end
end

function 场景类_好友查找:开始移动(x,y)
    if self.可视 and self.鼠标 then
        self.x = x - self.xx
        self.y = y - self.yy
    end
end

return 场景类_好友查找



