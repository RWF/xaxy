--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--
local 场景类_系统设置 = class()

local floor = math.floor
local ceil = math.ceil
local tp,zts,zts1
local insert = table.insert
local format = string.format


function 场景类_系统设置:初始化(根)
	self.ID = 26
	self.x = 175+(全局游戏宽度-800)/2
	self.y = 97
	self.xx = 0
	self.yy = 0
	self.注释 = "系统设置"
	tp = 根
		self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	local 小型选项栏 = 根._小型选项栏
	local 滑块 = 根._滑块

	self.资源组 = {
		[1] = 自适应.创建(0,1,455,427,3,9),
		[2] = 自适应.创建(1,1,417,18,1,3,nil,18),
		[3] = 自适应.创建(5,1,306,319,3,9),
		[4] = 自适应.创建(72,1,165,23,1,3,nil,18),
		[5] = 按钮.创建(自适应.创建(18,4,16,16,4,1),0,0,4,true,true),
		[6] = 按钮.创建(自适应.创建(12,4,72,22,1,1),0,0,4,true,true,"系统设置"),
		[7] = 按钮.创建(自适应.创建(12,4,72,22,1,1),0,0,4,true,true,"音频设置"),
		[8] = 按钮.创建(自适应.创建(12,4,72,22,1,1),0,0,4,true,true,"战斗设置"),
		[9] = 按钮.创建(自适应.创建(30,4,26,26,4,1),0,0,5,true,true),
		[10] = 按钮.创建(自适应.创建(30,4,26,26,4,1),0,0,5,true,true),
		[11] = 按钮.创建(自适应.创建(30,4,26,26,4,1),0,0,5,true,true),
		[12] = 按钮.创建(自适应.创建(30,4,26,26,4,1),0,0,5,true,true),
		[13] = 滑块.创建(资源:载入('JM.dll',"网易WDF动画",0x8D4BBC26),1,160,16,1),
		[14] = 滑块.创建(资源:载入('JM.dll',"网易WDF动画",0x8D4BBC26),1,160,16,1),
		[15] = 自适应.创建(3,1,80,19,1,3,nil,18),
		[16] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),
		[17] = 小型选项栏.创建(自适应.创建(6,1,80,93,3,9),"系统设置_战斗设置_药品设置"),
		[18] = 滑块.创建(资源:载入('JM.dll',"网易WDF动画",0x8D4BBC26),1,160,16,1),
		[19] = 滑块.创建(资源:载入('JM.dll',"网易WDF动画",0x8D4BBC26),1,160,16,1),
		[20] = 小型选项栏.创建(自适应.创建(6,1,100,116,3,9),"系统设置_战斗设置_技能设置"),
		[21] = 自适应.创建(3,1,100,19,1,3,nil,18),
		[22] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),
		[23] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFF205590),0,0,1,true,true), --屏蔽玩家
		[24] = 自适应.创建(3,1,80,19,1,3,nil,18),--下拉框开始
		[25] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),
		[26] = 小型选项栏.创建(自适应.创建(6,1,80,110,3,9),"self.游戏窗口设置"),--下拉框结束
		[27] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFF205590),0,0,1,true,true), --动态地图特效
		[28] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFF205590),0,0,1,true,true), --连点模式
		[29] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFF205590),0,0,1,true,true), --屏蔽变身造型
		[30] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),
		[31] = 小型选项栏.创建(自适应.创建(6,1,80,110,3,9),"self.背景"),--下拉框结束
		[32] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFF205590),0,0,1,true,true),--下拉框开始
		[33] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFF205590),0,0,1,true,true),--下拉框开始
		[34] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFF205590),0,0,1,true,true),--下拉框开始
		-- [35] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFF205590),0,0,1,true,true),--下拉框开始
		[36] = 小型选项栏.创建(自适应.创建(6,1,100,116,3,9),"系统设置_宝宝战斗"),
		[37] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),

[38] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFF205590),0,0,1,true,true), --屏蔽变身造型
[39] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFF205590),0,0,1,true,true), --屏蔽变身造型
[40] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFF205590),0,0,1,true,true), --屏蔽变身造型

	}

	for i=5,12 do
	   self.资源组[i]:绑定窗口_(26)
	end
    self.资源组[16]:绑定窗口_(26)
    self.资源组[22]:绑定窗口_(26)
    self.资源组[37]:绑定窗口_(26)
    self.资源组[23]:绑定窗口_(26)
    self.资源组[23]:置偏移(-3,2)

    self.资源组[27]:绑定窗口_(26)
    self.资源组[27]:置偏移(-3,2)
    self.资源组[28]:绑定窗口_(26)
    self.资源组[28]:置偏移(-3,2)
     self.资源组[29]:绑定窗口_(26)
    self.资源组[29]:置偏移(-3,2)
    self.资源组[25]:绑定窗口_(26)
    self.资源组[30]:绑定窗口_(26)
         self.资源组[38]:绑定窗口_(26)
    self.资源组[38]:置偏移(-3,2)
         self.资源组[39]:绑定窗口_(26)
    self.资源组[39]:置偏移(-3,2)
         self.资源组[40]:绑定窗口_(26)
    self.资源组[40]:置偏移(-3,2)
	for i=9,12 do
		self.资源组[i].允许再次点击 = true
	end
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.描边字体
	self.窗口时间 = 0
			local XXX=读配置(程序目录.."配置.ini","环境配置","宽度") or "800"
	local YYY= 读配置(程序目录.."配置.ini","环境配置","高度") or "600"
	self.游戏窗口设置= XXX.."*"..YYY
end


function 场景类_系统设置:打开()
	if self.可视 then

		self.可视 = false
		self.分类 = nil
	else
		if  self.x > 全局游戏宽度 then
		self.x = 175+(全局游戏宽度-800)/2
		end

	self.分类 = 1
		self.资源组[18]:置起始点(self.资源组[18]:取百分比转换(tp.系统设置.战斗设置[2],0,100))
		self.资源组[19]:置起始点(self.资源组[19]:取百分比转换(tp.系统设置.战斗设置[3],0,100))

	  	self.资源组[23]:置打勾框(玩家屏蔽)
	  	self.资源组[27]:置打勾框(地图特效)
	    self.资源组[28]:置打勾框(连点模式)
	    self.资源组[29]:置打勾框(屏蔽变身)
	    self.资源组[32]:置打勾框(摊位屏蔽)
	    self.资源组[34]:置打勾框(屏蔽给予)
	    -- self.资源组[35]:置打勾框(迭代)
	    self.资源组[33]:置打勾框(低配模式)
	    self.资源组[38]:置打勾框(屏蔽查看)
	    self.资源组[39]:置打勾框(屏蔽定制)
	    self.资源组[40]:置打勾框(屏蔽交易)
	  	if 引擎.是否全屏 then
			self.游戏窗口设置 = "全屏窗口"
		else
			self.游戏窗口设置 = 全局游戏宽度.."*"..全局游戏高度
		end
		insert(tp.窗口_,self)
		    tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间

	    self.可视 = true
	end
end

function 场景类_系统设置:显示(dt,x,y)
	self.焦点 = false
	self.资源组[5]:更新(x,y)


	self.资源组[6]:更新(x,y,self.分类 ~= 1)
	self.资源组[7]:更新(x,y,self.分类 ~= 2)
	self.资源组[8]:更新(x,y,self.分类 ~= 3)

	if 引擎.是否全屏 then
		self.游戏窗口设置 = "全屏窗口"
	else
		self.游戏窗口设置 = 全局游戏宽度.."*"..全局游戏高度
	end
    if  self.资源组[5]:事件判断() then
    	self:打开()
	elseif self.资源组[6]:事件判断() then
		self.分类 = 1
	elseif self.资源组[7]:事件判断() then
		self.分类 = 2
		self.资源组[13]:置起始点(self.资源组[13]:取百分比转换(tp.系统设置.声音设置[1],0,140))
		self.资源组[14]:置起始点(self.资源组[14]:取百分比转换(tp.系统设置.声音设置[2],0,140))
	elseif self.资源组[8]:事件判断() then
		self.分类 = 3
		self.资源组[18]:置起始点(self.资源组[18]:取百分比转换(tp.系统设置.战斗设置[2],0,100))
		self.资源组[19]:置起始点(self.资源组[19]:取百分比转换(tp.系统设置.战斗设置[3],0,100))
	elseif self.资源组[20]:事件判断() then
			tp.系统设置.战斗设置[7] = self.资源组[20].弹出事件
			self.资源组[20].弹出事件 = nil
	elseif self.资源组[36]:事件判断() then
			tp.系统设置.战斗设置[9] = self.资源组[36].弹出事件
			self.资源组[36].弹出事件 = nil
	elseif self.资源组[9]:事件判断() then
		tp.系统设置.声音设置[3] = not tp.系统设置.声音设置[3]
		tp.音乐:暂停()
	elseif self.资源组[10]:事件判断() then
		tp.系统设置.声音设置[4] = not tp.系统设置.声音设置[4]
	elseif self.资源组[11]:事件判断() then
		tp.系统设置.战斗设置[4] = not tp.系统设置.战斗设置[4]
	elseif self.资源组[12]:事件判断() then
		tp.系统设置.战斗设置[5] = not tp.系统设置.战斗设置[5]
	elseif self.资源组[16]:事件判断() then
		self.资源组[17]:打开({"一级药","二级药","三级药","无限制"})
		-- self.资源组[17]:置选中(tp.系统设置.战斗设置[1])
	elseif self.资源组[22]:事件判断() then
		local tb = {"普通攻击","防御","默认法术"}
		-- for i=1,#tp.场景.人物.数据.人物技能 do
		-- 	if tp.场景.人物.数据.人物技能[i].种类 ~= 0 then
		-- 		insert(tb,tp.场景.人物.数据.人物技能[i].名称)
		-- 	end
		-- end
		 self.资源组[20]:打开(tb)
		 self.资源组[20]:置选中(tp.系统设置.战斗设置[7])

	elseif self.资源组[37]:事件判断() then
		local tb = {"普通攻击","防御","默认法术"}
		 self.资源组[36]:打开(tb)
		 self.资源组[36]:置选中(tp.系统设置.战斗设置[9])
	elseif self.资源组[23]:事件判断() then
		玩家屏蔽 = not 玩家屏蔽
		self.资源组[23]:置打勾框(玩家屏蔽)
	elseif self.资源组[32]:事件判断() then
		摊位屏蔽 = not 摊位屏蔽
		self.资源组[32]:置打勾框(摊位屏蔽)
	elseif self.资源组[34]:事件判断() then
		屏蔽给予 = not 屏蔽给予
		self.资源组[34]:置打勾框(屏蔽给予)
		客户端:发送数据(63,63,63,63)

	elseif self.资源组[33]:事件判断() then
		低配模式 = not 低配模式
		self.资源组[33]:置打勾框(低配模式)
		if 低配模式 then
            地图特效 = false
			tp.提示:写入("#Y/你已经开启了低配模式,开启后降低内存使用,关闭各种特效,需要重新加载地图后生效")
		else
            地图特效 = true
             tp.提示:写入("#Y/你已经开启了高配模式,所有特效开启,体验最优质的画面,需要重新加载地图后生效")
		end
	 elseif self.资源组[25]:事件判断() then
		local tbt = {"800*600","1024*768","1280*720","1280*768","1280*800","全屏窗口"}--"640*480","800*600","1024*768","1280*720","1280*768","1280*800","全屏窗口"
		self.资源组[26]:打开(tbt)
		self.资源组[26]:置选中(self.游戏窗口设置)
	 elseif self.资源组[30]:事件判断() then
		local tbt = {"方案1","方案2","方案3","方案4","方案5","方案6","方案7","方案8","方案9","方案10","方案11"}--"640*480","800*600","1024*768","1280*720","1280*768","1280*800","全屏窗口"
		self.资源组[31]:打开(tbt)
		self.资源组[31]:置选中("方案"..tp.系统设置.战斗设置[8])
	elseif self.资源组[27]:事件判断() then
		地图特效 = not 地图特效
		self.资源组[27]:置打勾框(地图特效)
	elseif self.资源组[29]:事件判断() then
		屏蔽变身 = not 屏蔽变身
		self.资源组[29]:置打勾框(屏蔽变身)
		客户端:发送数据(4,4,59,59)

	elseif self.资源组[38]:事件判断() then
		屏蔽查看 = not 屏蔽查看
		self.资源组[38]:置打勾框(屏蔽查看)
		客户端:发送数据(1,1,59,59)
	elseif self.资源组[39]:事件判断() then
		屏蔽定制 = not 屏蔽定制
		self.资源组[39]:置打勾框(屏蔽定制)
		客户端:发送数据(2,2,59,59)
	elseif self.资源组[40]:事件判断() then
		屏蔽交易 = not 屏蔽交易
		self.资源组[40]:置打勾框(屏蔽交易)
		客户端:发送数据(3,3,59,59)

	elseif self.资源组[28]:事件判断() then
		 if 连点模式 then
 			连点模式=false
 		else
			连点模式=true
 			tp.提示:写入("#Y你开启了连点模式，长按鼠标3秒后可持续给人物或召唤兽分配属性点")
  		end
  		self.资源组[28]:置打勾框(连点模式)
	end
	--
		self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x+6,self.y+3)
	self.资源组[3]:显示(self.x+137,self.y+32)
	Picture.窗口标题背景_:置区域(0,0,78,16)
	Picture.窗口标题背景_:显示(self.x+180,self.y+3)
	zts1:置字间距(3)
	zts1:显示(self.x+185,self.y+3,"系统设置")
	zts1:置字间距(0)

	

	self.资源组[5]:显示(self.x+430,self.y+3)
	self.资源组[6]:显示(self.x+21,self.y+40,true,nil,nil,self.分类 == 1,2)
	self.资源组[7]:显示(self.x+21,self.y+80,true,nil,nil,self.分类 == 2,2)
	self.资源组[8]:显示(self.x+21,self.y+120,true,nil,nil,self.分类 == 3,2)
	tp.竖排花纹背景1_:置区域(0,0,18,393)
	tp.竖排花纹背景1_:显示(self.x+111,self.y+27)
    if self.分类 == 1 then
    		self.资源组[33]:更新(x,y)
    		-- self.资源组[35]:更新(x,y)
		self.资源组[25]:更新(x,y)
		self.资源组[30]:更新(x,y)

		self.资源组[23]:更新(x,y)
		self.资源组[27]:更新(x,y)
		self.资源组[28]:更新(x,y)
		self.资源组[29]:更新(x,y)
		self.资源组[32]:更新(x,y)
		self.资源组[34]:更新(x,y)
		self.资源组[38]:更新(x,y)
		self.资源组[39]:更新(x,y)
		self.资源组[40]:更新(x,y)

    	zts:置颜色(-16777216):显示(self.x+205,self.y+50,"游戏分辨率")
    	zts:置颜色(-16777216):显示(self.x+205,self.y+80,"外置聊天背景")

		zts:置颜色(-16777216):显示(self.x+155,self.y+120,"屏蔽玩家(F9)")
		zts:置颜色(-16777216):显示(self.x+155,self.y+160,"动态地图特效")
		zts:置颜色(-16777216):显示(self.x+155,self.y+200,"屏蔽摊位(F10)")
		zts:置颜色(-16777216):显示(self.x+155,self.y+240,"屏蔽给予")
			zts:置颜色(-16777216):显示(self.x+155,self.y+280,"屏蔽查看")

		zts:置颜色(-16777216):显示(self.x+285,self.y+120,"连点模式")
		zts:置颜色(-16777216):显示(self.x+285,self.y+160,"屏蔽变身造型")
		zts:置颜色(-16777216):显示(self.x+285,self.y+200,"低配模式")
		zts:置颜色(-16777216):显示(self.x+285,self.y+240,"屏蔽交易")
		zts:置颜色(-16777216):显示(self.x+285,self.y+280,"屏蔽定制")
		self.资源组[23]:显示(self.x+250,self.y+115,true)
		self.资源组[27]:显示(self.x+250,self.y+155,true)
		self.资源组[32]:显示(self.x+250,self.y+195,true)
		self.资源组[34]:显示(self.x+250,self.y+235,true)
        self.资源组[39]:显示(self.x+250,self.y+275,true)

		self.资源组[28]:显示(self.x+380,self.y+115,true)
		self.资源组[29]:显示(self.x+380,self.y+155,true)
		self.资源组[33]:显示(self.x+380,self.y+195,true)
		self.资源组[38]:显示(self.x+380,self.y+235,true)
		self.资源组[40]:显示(self.x+380,self.y+275,true)

		self.资源组[24]:显示(self.x+300,self.y+50)
		self.资源组[24]:显示(self.x+300,self.y+80)
		self.资源组[25]:显示(self.x+300+66,self.y+50-1)
		self.资源组[30]:显示(self.x+300+66,self.y+80-1)
		self.资源组[26]:显示(self.x+300,self.y+50+18,x,y,self.鼠标)
		self.资源组[31]:显示(self.x+300,self.y+80+18,x,y,self.鼠标)
		zts:置颜色(-16777216):显示(self.x+300+6,self.y+50+2,self.游戏窗口设置)
		zts:置颜色(-16777216):显示(self.x+300+6,self.y+80+2,"方案"..tp.系统设置.战斗设置[8])
		if self.资源组[26]:事件判断() then
			local 宽高s =self.资源组[26].弹出事件
			if 宽高s ~= "全屏窗口" then
				if 引擎.是否全屏 then
				    引擎.置全屏()
				end
				local 宽高XY =分割文本2(宽高s,"*")
				if not 判断是否数组(宽高XY) then
				    return false
				end
				local 宽高X,宽高Y=math.ceil(宽高XY[1]) or 850,math.ceil(宽高XY[2]) or 620
				引擎.置宽高(宽高X,宽高Y)
				全局游戏宽度 = 宽高X
				全局游戏高度 = 宽高Y
				withs = 全局游戏宽度
				hegts = 全局游戏高度
				with = 全局游戏宽度/2
				hegt = 全局游戏高度/2
				写配置(程序目录.."配置.ini","环境配置","宽度",全局游戏宽度)
				写配置(程序目录.."配置.ini","环境配置","高度",全局游戏高度)
				self.游戏窗口设置 = 宽高s
			else
				if (全局游戏宽度==1024 and 全局游戏高度==768 )or(全局游戏宽度==1280 and 全局游戏高度==720 )or(全局游戏宽度==1280 and 全局游戏高度==768 )or(全局游戏宽度==1280 and 全局游戏高度==800 )  then
				    引擎.置全屏()
				    self.游戏窗口设置 = 宽高s
					if not 引擎.是否全屏 then
					    self.游戏窗口设置 = 全局游戏宽度.."*"..全局游戏高度
					end
				end
			end
			self.资源组[26].弹出事件 = nil
	 elseif  self.资源组[31]:事件判断() then
	 		if self.资源组[31].弹出事件=="方案1"  then
	 			tp.系统设置.战斗设置[8]=1
			elseif self.资源组[31].弹出事件=="方案2"  then
			tp.系统设置.战斗设置[8]=2
			elseif self.资源组[31].弹出事件=="方案3"  then
			tp.系统设置.战斗设置[8]=3
			elseif self.资源组[31].弹出事件=="方案4"  then
			tp.系统设置.战斗设置[8]=4
			elseif self.资源组[31].弹出事件=="方案5"  then
			tp.系统设置.战斗设置[8]=5
			elseif self.资源组[31].弹出事件=="方案6"  then
			tp.系统设置.战斗设置[8]=6
			elseif self.资源组[31].弹出事件=="方案7"  then
			tp.系统设置.战斗设置[8]=7
			elseif self.资源组[31].弹出事件=="方案8"  then
			tp.系统设置.战斗设置[8]=8
			elseif self.资源组[31].弹出事件=="方案9"  then
			tp.系统设置.战斗设置[8]=9
			elseif self.资源组[31].弹出事件=="方案10"  then
			tp.系统设置.战斗设置[8]=10
			elseif self.资源组[31].弹出事件=="方案11"  then
			tp.系统设置.战斗设置[8]=11
			end
			self.资源组[26].弹出事件 = nil
	  end
	elseif self.分类 == 2 then
		self.资源组[9]:更新(x,y,tp.系统设置.声音设置[3] ~= true)
		self.资源组[10]:更新(x,y,tp.系统设置.声音设置[4] ~= true)
		zts:置颜色(-16777216)
		zts:显示(self.x+165,self.y+70,"音频")
		zts:显示(self.x+195,self.y+101,"游戏音乐")
		zts:显示(self.x+195,self.y+141,"游戏音效")
		self.资源组[9]:显示(self.x+166,self.y+95,true,nil,nil,tp.系统设置.声音设置[3] == true,4)
		self.资源组[10]:显示(self.x+166,self.y+135,true,nil,nil,tp.系统设置.声音设置[4] == true,4)
		for i=1,2 do
			self.资源组[4]:显示(self.x+257,self.y+56+i*40)
		end
		self.资源组[13]:显示(self.x+258,self.y+98,x,y,self.鼠标)
		self.资源组[14]:显示(self.x+258,self.y+138,x,y,self.鼠标)
		if self.资源组[13].接触 then
			tp.系统设置.声音设置[1] = ceil(140*self.资源组[13]:取百分比())
			tp.音乐:置音量(tp.系统设置.声音设置[1])
            写配置(程序目录.."配置.ini","环境配置","音量",tp.系统设置.声音设置[1])
			self.焦点 = true
		elseif self.资源组[14].接触 then
			tp.系统设置.声音设置[2] = ceil(140*self.资源组[14]:取百分比())
			写配置(程序目录.."配置.ini","环境配置","音效",tp.系统设置.声音设置[2])
			self.焦点 = true
		end
	elseif self.分类 == 3 then
		self.资源组[16]:更新(x,y)
		self.资源组[11]:更新(x,y,tp.系统设置.战斗设置[4] ~= true)
		self.资源组[12]:更新(x,y,tp.系统设置.战斗设置[5] ~= true)
		self.资源组[22]:更新(x,y)
		self.资源组[37]:更新(x,y)
		zts:置颜色(-16777216)
		zts:显示(self.x+165,self.y+70,"药品")
		zts:显示(self.x+195,self.y+101,"自动吃药")
		self.资源组[11]:显示(self.x+166,self.y+95,true,nil,nil,tp.系统设置.战斗设置[4] == true,4)
		self.资源组[15]:显示(self.x+256,self.y+99)
		zts:置颜色(-16777216)
		zts:显示(self.x+262,self.y+101,tp.系统设置.战斗设置[1])

		--
		zts:显示(self.x+150,self.y+140,format("血量低于%d%s吃药",tp.系统设置.战斗设置[2],"%"))
		zts:显示(self.x+150,self.y+180,format("蓝量低于%d%s吃药",tp.系统设置.战斗设置[3],"%"))
		for i=1,2 do
			self.资源组[4]:显示(self.x+262,self.y+95+i*40)
		end
		self.资源组[18]:显示(self.x+263,self.y+138,x,y,self.鼠标)
		self.资源组[19]:显示(self.x+263,self.y+178,x,y,self.鼠标)
		if self.资源组[18].接触 then
			tp.系统设置.战斗设置[2] = ceil(100*self.资源组[18]:取百分比())
			self.焦点 = true
		elseif self.资源组[19].接触 then
			tp.系统设置.战斗设置[3] = ceil(100*self.资源组[19]:取百分比())
			self.焦点 = true
		end
		zts:显示(self.x+165,self.y+220,"技能")
		zts:显示(self.x+195,self.y+251,"人物自动设置")
		zts:显示(self.x+195,self.y+281,"BB自动设置")

		self.资源组[12]:显示(self.x+166,self.y+275,true,nil,nil,tp.系统设置.战斗设置[5] == true,4)
		self.资源组[21]:显示(self.x+285,self.y+279)

		self.资源组[12]:显示(self.x+166,self.y+245,true,nil,nil,tp.系统设置.战斗设置[5] == true,4)
		self.资源组[21]:显示(self.x+285,self.y+249)
		zts:置颜色(-16777216)
		zts:显示(self.x+290,self.y+251,tp.系统设置.战斗设置[7])
		zts:显示(self.x+290,self.y+281,tp.系统设置.战斗设置[9])
		--
		self.资源组[16]:显示(self.x+322,self.y+97)
		self.资源组[17]:显示(self.x+256,self.y+117,x,y,self.鼠标)
		self.资源组[22]:显示(self.x+371,self.y+247)
		self.资源组[20]:显示(self.x+285,self.y+266,x,y,self.鼠标)
		self.资源组[37]:显示(self.x+371,self.y+277)
		self.资源组[36]:显示(self.x+285,self.y+296,x,y,self.鼠标)
		if self.资源组[17]:事件判断() then
			tp.系统设置.战斗设置[1] = self.资源组[17].弹出事件
			self.资源组[17].弹出事件 = nil
		elseif self.资源组[20]:事件判断() then
			tp.系统设置.战斗设置[7] = self.资源组[20].弹出事件
			self.资源组[20].弹出事件 = nil
		elseif self.资源组[36]:事件判断() then
			tp.系统设置.战斗设置[9] = self.资源组[36].弹出事件
			self.资源组[36].弹出事件 = nil
		end
	end
end


function 场景类_系统设置:检查点(x,y)
	if self.资源组[1]:是否选中(x,y)  then
		return true
	end
end

function 场景类_系统设置:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not 引擎.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_系统设置:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_系统设置