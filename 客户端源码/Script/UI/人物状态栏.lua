--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:46
--======================================================================--
local 场景类_人物状态栏 = class()

local bwh = require("gge包围盒")
local bw = bwh(0,0,25,276)
local bw1 = bwh(0,0,120,36)
local bw2 = bwh(0,0,244,24)
local floor = math.floor
local format = string.format
local min = math.min
local zt = {"人\n物\n属\n性","师\n门\n技\n能","辅\n助\n技\n能","修\n炼\n技\n能"}
local fl = {"人物属性","师门技能","辅助技能","修炼技能"}
local bd ={"体质","魔力","力量","耐力","敏捷"}
local bd1 = {"命中","伤害","防御","速度","灵力","躲避"}
local box = 引擎.画矩形
local as = {"攻击修炼","防御修炼","法术修炼","法抗修炼","猎术修炼"}
local as11 = {"攻击","防御","法术","法抗","猎术"}
local as1 = {"攻击控制力","防御控制力","法术控制力","法抗控制力","  育兽术  "}
local wz = {"名称","","帮派","门派","等级","人气","帮贡","门贡","气血","魔法","愤怒","活力","体力","命中","伤害","防御","速度","灵力","躲避","体质","魔力","力量","耐力","敏捷","潜力"}
--
local tp,zts,zts1,rwa,zts2,zts3
local mousea = 引擎.鼠标弹起
local insert = table.insert

function 场景类_人物状态栏:初始化(根)
	self.ID = 4
	self.x = 全局游戏宽度-300
	self.y = 75
	self.xx = 0
	self.yy = 0
	self.注释 = "人物状态栏"
	self.可视 = false
	self.鼠标 = true
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 自适应.创建(0,1,293,413,3,9),
		[2] = 自适应.创建(2,1,130,117,3,9),
		[5] = 按钮.创建(自适应.创建(18,4,16,16,4),0,0,4,true,true),
		[6] = 按钮.创建(自适应.创建(27,4,25,69,2),0,0,5,true,true),
		[7] = 按钮.创建(自适应.创建(27,4,25,69,2),0,0,5,true,true),
		[8] = 按钮.创建(自适应.创建(27,4,25,69,2),0,0,5,true,true),
		[9] = 按钮.创建(自适应.创建(27,4,25,69,2),0,0,5,true,true),
		[10] =  按钮.创建(自适应.创建(12,4,43,22,1,3),0,0,4,true,true,"称谓"),
		[11] =  按钮.创建(自适应.创建(25,4,19,19,4,3),0,0,4,true,true),
		[12] =  按钮.创建(自适应.创建(25,4,19,19,4,3),0,0,4,true,true),
		[13] =  按钮.创建(自适应.创建(25,4,19,19,4,3),0,0,4,true,true),
		[14] =  按钮.创建(自适应.创建(25,4,19,19,4,3),0,0,4,true,true),
		[15] =  按钮.创建(自适应.创建(25,4,19,19,4,3),0,0,4,true,true),
		[16] =  按钮.创建(自适应.创建(26,4,19,19,4,3),0,0,4,true,true),
		[17] =  按钮.创建(自适应.创建(26,4,19,19,4,3),0,0,4,true,true),
		[18] =  按钮.创建(自适应.创建(26,4,19,19,4,3),0,0,4,true,true),
		[19] =  按钮.创建(自适应.创建(26,4,19,19,4,3),0,0,4,true,true),
		[20] =  按钮.创建(自适应.创建(26,4,19,19,4,3),0,0,4,true,true),
		[21] =  按钮.创建(自适应.创建(12,4,43,22,1,3),0,0,4,true,true,"预览"),
		[22] =  按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"更多属性"),
		[23] =  按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"推荐加点"),
		[24] =  按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"确认加点"),
		[25] =  按钮.创建(自适应.创建(12,4,43,22,1,3),0,0,4,true,true,"升级"),
		[26] =  按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"奇经八脉"),
		[27] =  按钮.创建(自适应.创建(12,4,43,22,1,3),0,0,4,true,true,"使用"),
		[28] =  按钮.创建(自适应.创建(12,4,43,22,1,3),0,0,4,true,true,"自动"),
		[29] = 按钮.创建(自适应.创建(13,4,72,20,1,3),0,0,4,true,true,"生活技能"),
		[30] = 按钮.创建(自适应.创建(13,4,72,20,1,3),0,0,4,true,true,"剧情技能"),
		[31] = 按钮.创建(自适应.创建(13,4,72,20,1,3),0,0,4,true,true,"其他技能"),
		[32] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"  烹饪"),
		[33] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"  炼药"),
		[34] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"  打造"),
		[35] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"  摆摊"),
		[36] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"  拍卖"),
		[37] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"学剧情点"),
		[38] = 自适应.创建(3,1,40,19,1,3),
		[39] =  资源:载入('JM.dll',"网易WDF动画",0x68D384BD),
		[40] =  资源:载入('JM.dll',"网易WDF动画",0x68D384BD),
		[41] =  资源:载入('JM.dll',"网易WDF动画",0x3906F9F1),
		[42] =  资源:载入('JM.dll',"网易WDF动画",0xC28E86D1),
		[43] = 自适应.创建(1,1,255,18,1,3,nil,18),
		[44] = 自适应.创建(4,1,155,220,3,nil),
		[45] =  按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"设为当前"),
 		[46] =	按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x49D09C8B),0,0,4,true,true),
 		[47] = 按钮.创建(自适应.创建(13,4,72,20,1,3),0,0,4,true,true,"  学习"),
 		[48] = 按钮.创建(自适应.创建(13,4,72,20,1,3),0,0,4,true,true,"使用技能"),

	}
	self.bd = {"体质","魔力","力量","耐力","敏捷"}



	for i=5,37 do
		self.资源组[i]:绑定窗口_(4)
		if i >= 29 and i <= 31 then
			self.资源组[i]:置偏移(0,-1)
		end
	end
	self.资源组[47]:绑定窗口_(4)
	self.资源组[48]:绑定窗口_(4)
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.普通字体__
	zts2 = tp.字体表.描边字体
	zts3 = tp.字体表.普通字体__
	zts1:置行间距(2.7)
	self.状态 = 1
	self.子类状态 = 1
	self.选中师门技能 = 0
	self.选中师门法术 = 0
	self.临时潜力 = {体质=0,魔力=0,力量=0,耐力=0,敏捷=0}
	self.预览属性 = {气血=0,魔法=0,命中=0,伤害=0,速度=0,灵力=0,防御=0,躲避=0}
	self.窗口时间 = 0
		self.辅助技能 = {}
    self.强化技能= {}
    self.剧情技能 = {}
    self.师门技能 = {}
	self.特技数据 = {}
			self.修炼选中1 = 0
		self.修炼选中2 = 0
		self.选中剧情 = 0
		self.上一次 = 0
end




function 场景类_人物状态栏:打开(数据)
	if self.可视 then
		self.状态 = 1
		self.子类状态 = 1
	    self.强化技能= {}
	    self.剧情技能 = {}
	    self.师门技能 = {}
		self.特技数据 = {}
				self.修炼选中1 = 0
		self.修炼选中2 = 0
		self.选中师门技能 = 0
		self.选中师门法术 = 0
		self.上一次 = 0
		self.选中剧情 = 0
		self.临时潜力 = {体质=0,魔力=0,力量=0,耐力=0,敏捷=0}
        self.预览属性 = {气血=0,魔法=0,命中=0,伤害=0,速度=0,灵力=0,防御=0,躲避=0}
		self.可视 = false
	else
			self.x = 全局游戏宽度-300
       self.数据 =数据

       	local jn = tp._技能格子
	for i=1,12 do
	    self.辅助技能[i] = jn(0,0,i,"辅助技能")
	    self.强化技能[i] = jn(0,0,i,"强化技能")
	    self.剧情技能[i] = jn(0,0,i,"剧情技能")
	end
    for i=1,#self.数据.师门技能 do
	    self.师门技能[i] = jn(0,0,i,"师门技能")
	end

		self.修炼选中1 = 0
		self.修炼选中2 = 0
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_人物状态栏:刷新(数据)
    self.数据 =数据
	for i=1,#self.师门技能 do
	self.师门技能[i]:置技能(self.数据.师门技能[i])
		if self.师门技能[i].包含技能~=nil then
			for v=1,#self.师门技能[i].包含技能 do
			self.师门技能[i].包含技能[v]:置技能(self.数据.师门技能[i].包含技能[v])
			end
		end
	end
end
function 场景类_人物状态栏:取差异属性(sxb)
	local sx1 = self.数据.最大气血
	local sx2 = self.数据.魔法上限
	local sx3 = self.数据.伤害
	local sx4 = self.数据.防御
	local sx5 = self.数据.速度
	local sx6 = self.数据.灵力
	local sx7 =self.数据.躲避
	local sx8 = self.数据.命中
	local 体质 = self.数据.体质+sxb.体质
	local 魔力 = self.数据.魔力+sxb.魔力
	local 力量 = self.数据.力量+sxb.力量
	local 耐力 = self.数据.耐力+sxb.耐力
	local 敏捷 = self.数据.敏捷+ sxb.敏捷

 if self.数据.种族=="人" then
     伤害1=math.floor(力量*0.77)+20
     命中1=math.floor(力量*2.31)+20
     防御1=math.floor(耐力*1.4)
     灵力1=math.floor(力量*0.2+魔力*0.9+体质*0.3+耐力*0.2)
     速度1=math.floor((敏捷)*0.7+力量*0.1+体质*0.1)
     躲避1=math.floor(敏捷)
     魔法上限=math.floor(魔力*2.5)
     最大气血=math.floor(体质*6)
   elseif self.数据.种族=="魔" then
     伤害1=math.floor(力量*0.77)+20
     命中1=math.floor(力量*2.31)+20
     防御1=math.floor(耐力*1.4)
     灵力1=math.floor(力量*0.2+魔力*0.9+体质*0.3+耐力*0.2)
     速度1=math.floor((敏捷)*0.7+力量*0.1+体质*0.1)
     躲避1=math.floor(敏捷)
     魔法上限=math.floor(魔力*2.5)
     最大气血=math.floor(体质*6)
   elseif self.数据.种族=="仙" then
     伤害1=math.floor(力量*0.77)+20
     命中1=math.floor(力量*2.31)+20
     防御1=math.floor(耐力*1.4)
     灵力1=math.floor(力量*0.2+魔力*0.9+体质*0.3+耐力*0.2)
     速度1=math.floor((敏捷)*0.7+力量*0.1+体质*0.1)
     躲避1=math.floor(敏捷)
     魔法上限=math.floor(魔力*2.5)
     最大气血=math.floor(体质*6)
     end
 伤害1=伤害1+self.数据.装备属性.伤害+self.数据.技能属性.伤害
 命中1=命中1+self.数据.装备属性.命中+self.数据.技能属性.命中
 防御1=防御1+self.数据.装备属性.防御+5+self.数据.技能属性.防御
 灵力1=灵力1+self.数据.装备属性.灵力+self.数据.技能属性.灵力
 速度1=速度1+self.数据.装备属性.速度+self.数据.技能属性.速度
 伤害1=math.floor(伤害1+命中1*0.3)
 躲避1 = 躲避1+self.数据.技能属性.躲避
 魔法上限=魔法上限+self.数据.装备属性.魔法+self.数据.技能属性.魔法
 最大气血=最大气血+self.数据.装备属性.气血+self.数据.技能属性.气血
 -- 最大气血=math.floor(最大气血*(1+0.01*self.数据.辅助技能[1].等级))
 -- 魔法上限=math.floor(魔法上限*(1+0.01*self.数据.辅助技能[2].等级))



	return {气血=最大气血-sx1,魔法=魔法上限-sx2,伤害=伤害1-sx3,防御=防御1-sx4,速度=速度1-sx5,灵力=灵力1-sx6,躲避=躲避1-sx7,命中=命中1-sx8}
end
function 场景类_人物状态栏:更新气血数据(数据)
 self.数据.当前气血=数据.当前气血
 self.数据.气血上限=数据.气血上限
 self.数据.最大气血=数据.最大气血
 self.数据.当前魔法=数据.当前魔法
 self.数据.魔法上限=数据.魔法上限
 self.数据.当前经验=数据.当前经验
 self.数据.升级经验=数据.升级经验
 self.数据.愤怒=数据.愤怒
end

function 场景类_人物状态栏:显示(dt,x,y)

	self.焦点 = false
	-- 变量集合
	local rw = self.数据
	-- 显示集合
	self.资源组[5]:更新(x,y)
	if self.资源组[5]:事件判断() then
		self:打开()
		return false
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[43]:显示(self.x+6,self.y+3)
	Picture.窗口标题背景_:置区域(0,0,76,16)
	Picture.窗口标题背景_:显示(self.x+99,self.y+3)
	zts2:置字间距(2)
	zts2:显示(self.x+105,self.y+3,fl[self.状态])
	zts2:置字间距(0)
	self.资源组[5]:显示(self.x+266,self.y+6)
	self.资源组[46]:显示(self.x+5,self.y+5)
	self.资源组[46]:更新(x,y)
	local ms = nil
	zts1:置颜色(4294967295)
	for i=1,4 do
		self.资源组[i+5]:更新(x,y,self.状态 ~= i)
		if self.资源组[i+5]:事件判断() then
			self.状态 = i
			self.选中师门技能 = 0
			self.选中师门法术 = 0
           if self.状态 == 2 then
				for i=1,#self.师门技能 do
				self.师门技能[i]:置技能(self.数据.师门技能[i])
				if self.师门技能[i].包含技能~=nil then
				for v=1,#self.师门技能[i].包含技能 do
				self.师门技能[i].包含技能[v]:置技能(self.数据.师门技能[i].包含技能[v])
				end
				end
				end
          end
			if self.状态 == 3 then
				for n=1,#self.数据.辅助技能 do
					self.辅助技能[n]:置技能(self.数据.辅助技能[n])
				end
	           for n=1,#self.数据.强化技能 do
					self.强化技能[n]:置技能(self.数据.强化技能[n])
				end
				 for n=1,#self.数据.剧情技能 do
					self.剧情技能[n]:置技能(self.数据.剧情技能[n])
				end
			end
		end
			if self.状态 == 4 then


			    end
		if self.资源组[i+5].按住 then
			ms = i
		end
		local jx = self.x-23
		local jy = self.y+41+(i-1)*68
		self.资源组[i+5]:显示(jx,jy,nil,nil,nil,self.状态 == i,2)
		zts1:显示(jx+7,jy+6,zt[i])
	end
	local jx = self.x-23
	local jy = self.y+41+(self.状态-1)*68
	self.资源组[self.状态+5]:显示(jx,jy,nil,nil,nil,true,2)
	zts1:显示(jx+8,jy+7,zt[self.状态])
	if ms ~= nil then
		local jx = self.x-23
		local jy = self.y+41+(ms-1)*68
		self.资源组[ms+5]:显示(jx,jy,nil,nil,nil,true,2)
		zts1:显示(jx+8,jy+7,zt[ms])
	end
	-- 层次逻辑
	if self.状态 == 1 then
		self.资源组[38]:置宽高1(131,19)
		tp.字体表.普通字体:置颜色(0xFFFFFFFF)
		for i=0,3 do
			self.资源组[38]:显示(self.x+48,self.y+30+i*23)
			tp.字体表.普通字体:显示(self.x+15,self.y+34+i*23,wz[i+1])
		end
		self.资源组[38]:置宽高1(46,19)
		for i=0,3 do
			self.资源组[38]:显示(self.x+227,self.y+30+i*23)
			tp.字体表.普通字体:显示(self.x+190,self.y+34+i*23,wz[i+5])
		end
		self.资源组[38]:置宽高1(226,19)
		self.资源组[38]:显示(self.x+49,self.y+129)
		tp.字体表.普通字体:显示(self.x+15,self.y+133,wz[9])
		self.资源组[38]:置宽高1(97,19)
		for i=0,1 do
			self.资源组[38]:显示(self.x+48,self.y+153+i*23)
			tp.字体表.普通字体:显示(self.x+15,self.y+157+i*23,wz[10+i])
		end
		for i=0,5 do
			self.资源组[38]:显示(self.x+49,self.y+204+i*24)
			tp.字体表.普通字体:显示(self.x+15,self.y+207+i*24,wz[14+i])
		end
		self.资源组[38]:置宽高1(84,19)
		for i=0,1 do
			self.资源组[38]:显示(self.x+191,self.y+153+i*23)
			tp.字体表.普通字体:显示(self.x+159,self.y+157+i*23,wz[12+i])
		end
		self.资源组[38]:置宽高1(42,19)
		for i=0,5 do
			self.资源组[38]:显示(self.x+192,self.y+204+i*24)
			tp.字体表.普通字体:显示(self.x+159,self.y+207+i*24,wz[20+i])
		end
		tp.字体表.普通字体:显示(self.x+25,self.y+383,"经验")
		tp.字体表.普通字体:置颜色(0xFFFFFFFF)
		tp.经验背景_:置宽高1(171,19)
		tp.经验背景_:显示(self.x+58,self.y+379)
		tp.经验背景_:置宽高1(186,19)
		self.资源组[10]:更新(x,y)
		self.资源组[21]:更新(x,y)
		self.资源组[25]:更新(x,y)
		self.资源组[10]:显示(self.x+4,self.y+52,true)
		for i=1,5 do
			self.资源组[10+i]:更新(x,y,self.数据.潜能 > 0,1)
			if self.资源组[10+i]:事件判断() then
				self.临时潜力[self.bd[i]] = self.临时潜力[self.bd[i]] + 1
				self.数据.潜能 =self.数据.潜能 - 1
			end
			self.资源组[10+i]:显示(self.x+237,self.y+204+((i-1)*24))
			--
			self.资源组[15+i]:更新(x,y,self.临时潜力[self.bd[i]] > 0,1)
			if self.资源组[15+i]:事件判断() then
				self.临时潜力[self.bd[i]] = self.临时潜力[self.bd[i]] - 1
				rw.潜能 = rw.潜能 + 1
			end
			self.资源组[15+i]:显示(self.x+259,self.y+204+((i-1)*24))
		end
		self.资源组[21]:显示(self.x+236,self.y+325,true)
		for i=1,3 do
			self.资源组[21+i]:更新(x,y)
			self.资源组[21+i]:显示(self.x+14+(i-1)*97,self.y+351,true)
		end
		self.资源组[25]:显示(self.x+236,self.y+380,true)
		if self.资源组[10]:事件判断() then
			客户端:发送数据(9, 53, 13, "97", 1)
		elseif self.资源组[21]:事件判断() then
			self.预览属性 = self:取差异属性(self.临时潜力)
		elseif self.资源组[22]:事件判断() then
		 tp.窗口.幻化属性:打开(self.数据.等级,self.数据.装备属性,self.数据.躲避)
		 elseif self.资源组[23]:事件判断() and self.数据.潜能>0 then
		 	if self.数据.门派=="大唐官府"or self.数据.门派=="无" or self.数据.门派=="花果山" or self.数据.门派=="天宫"or self.数据.门派=="五庄观"or self.数据.门派=="凌波城" or self.数据.门派=="狮驼岭" then
		 	   	self.临时潜力.力量 = self.数据.潜能
			elseif self.数据.门派=="神木林" or self.数据.门派=="龙宫"or self.数据.门派=="女魃墓"or self.数据.门派=="魔王寨"  then
			self.临时潜力.魔力 = self.数据.潜能
           elseif self.数据.门派=="盘丝洞"or self.数据.门派=="无底洞" or self.数据.门派=="方寸山" or self.数据.门派=="阴曹地府"or self.数据.门派=="女儿村"  then
           	self.临时潜力.敏捷 = self.数据.潜能
           	elseif self.数据.门派=="普陀山" or self.数据.门派=="无底洞" or self.数据.门派=="化生寺" or self.数据.门派=="天机城" then
           		self.临时潜力.耐力 = self.数据.潜能
		 	end
		 	self.数据.潜能 =0
		elseif self.资源组[24]:事件判断() then
        客户端:发送数据(0, 3, 5, self.临时潜力.力量.."*-*"..self.临时潜力.体质.."*-*"..self.临时潜力.魔力.."*-*"..self.临时潜力.耐力.."*-*"..self.临时潜力.敏捷, 1)
			self.临时潜力 = {体质=0,魔力=0,力量=0,耐力=0,敏捷=0}
			self.预览属性 = {气血=0,魔法=0,命中=0,伤害=0,速度=0,灵力=0,防御=0,法伤=0,躲避=0}
		elseif self.资源组[25]:事件判断() then
			if self.数据.等级 == 69 or self.数据.等级 == 89 or self.数据.等级 == 109 or self.数据.等级 == 129 or self.数据.等级 == 159 then
				tp.窗口.文本栏:载入("#H/你确定要升级吗？","升级",true)

		    else
			  	客户端:发送数据(0, 2, 5, "0", 1)
				self.预览属性 = {气血=0,魔法=0,命中=0,伤害=0,速度=0,灵力=0,防御=0,躲避=0}
				self.临时潜力 = {体质=0,魔力=0,力量=0,耐力=0,敏捷=0}
			end


		end
		-- 文字
		zts:置颜色(-16777216)
		zts:显示(self.x + 56,self.y + 33,rw.名称)
		zts:显示(self.x + 56,self.y + 56,self.数据.称谓 or "无")
		zts:显示(self.x + 56,self.y + 79,self.数据.帮派 or "无帮派")
		zts:显示(self.x + 56,self.y + 102,rw.门派)
		zts:显示(self.x + 234,self.y + 33,rw.等级)
		zts:显示(self.x + 234,self.y + 56,rw.人气)
		zts:显示(self.x + 234,self.y + 79,self.数据.帮贡 or 0)
		zts:显示(self.x + 234,self.y + 102,self.数据.门贡 or 0)
		zts:显示(self.x + 56,self.y + 133,format("%d/%d/%d",rw.当前气血,rw.气血上限,rw.最大气血))
		zts:显示(self.x + 56,self.y + 157,format("%d/%d",rw.当前魔法,rw.魔法上限))
		zts:显示(self.x + 56,self.y + 180,format("%d/150",rw.愤怒))
		for i=1,6 do
			if self.预览属性[bd1[i]] > 0 then
				zts:置颜色(-65536)
			else
				zts:置颜色(-16777216)
			end
			zts:显示(self.x + 56,self.y + 208 + ((i-1)*24) + 0.3,rw[bd1[i]])
		end

		-----------
		if self.预览属性.气血 > 0 then
			zts:置颜色(-65536)
			zts:显示(self.x + 160,self.y + 132,"+"..self.预览属性.气血)
		end
		if self.预览属性.魔法 > 0 then
			zts:置颜色(-65536)
			zts:显示(self.x + 120,self.y + 156.3,"+"..self.预览属性.魔法)
		end
		if self.预览属性.伤害 > 0 then
			zts:置颜色(-65536)
			zts:显示(self.x + 100,self.y + 232.3,"+"..self.预览属性.伤害)
		end
		if self.预览属性.防御 > 0 then
			zts:置颜色(-65536)
			zts:显示(self.x + 100,self.y + 256.3,"+"..self.预览属性.防御)
		end
		if self.预览属性.速度 > 0 then
			zts:置颜色(-65536)
			zts:显示(self.x + 100,self.y + 280.3,"+"..self.预览属性.速度)
		end
		if self.预览属性.灵力 > 0 then
			zts:置颜色(-65536)
			zts:显示(self.x + 100,self.y + 304.3,"+"..self.预览属性.灵力)
		end
		if self.预览属性.躲避 > 0 then
			zts:置颜色(-65536)
			zts:显示(self.x + 100,self.y + 328.3,"+"..self.预览属性.躲避)
		end
        if self.预览属性.命中 > 0 then
			zts:置颜色(-65536)
			zts:显示(self.x + 100,self.y + 208.3,"+"..self.预览属性.命中)
		end
         ------------
		zts:置颜色(-16777216)
		zts:显示(self.x + 198,self.y + 155,format("%d/%d",rw.当前活力,rw.活力上限))
		zts:显示(self.x + 198,self.y + 180,format("%d/%d",rw.当前体力,rw.体力上限))
		for i=1,5 do
			if self.临时潜力[self.bd[i]] > 0 then
				zts:置颜色(-65536)
			else
				zts:置颜色(-16777216)
			end
			zts:显示(self.x + 198,self.y + 208 + ((i-1)*24) + 0.3,rw[self.bd[i]]+self.临时潜力[self.bd[i]])
		end
		zts:置颜色(-16777216)
		zts:显示(self.x + 198,self.y + 328.3,self.数据.潜能)
		self.资源组[41]:置区域(0,0,min(floor(self.数据.当前经验 / self.数据.升级经验 * 161),161),self.资源组[41].高度)
		self.资源组[41]:显示(self.x+63,self.y+382)
		local ts = format("%d/%d",self.数据.当前经验,self.数据.升级经验)
	     
				zts2:置描边颜色(-16240640)
		zts2:置颜色(4294967295)
		zts2:显示(self.x + ((272 - zts2:取宽度(ts))/2)+8,self.y + 380.5,self.数据.当前经验.."/"..self.数据.升级经验)
		zts2:置描边颜色(-16777216)
	elseif self.状态 == 2 then
		self.资源组[44]:置宽高3(131,302)
		self.资源组[44]:显示(self.x+14,self.y+37)
		self.资源组[44]:显示(self.x+150,self.y+37)
		
		zts2:置字间距(2)
		zts2:显示(self.x+48,self.y+42,"师门技能")
		zts2:显示(self.x+184,self.y+42,"师门法术")
		zts2:显示(self.x+17,self.y+348,"特技")
		zts2:置字间距(0)
		
		self.资源组[26]:更新(x,y)
		self.资源组[27]:更新(x,y,self.选中师门法术 ~= 0 and (self.数据.师门技能[self.选中师门技能].包含技能[self.选中师门法术].种类 == 0 or self.数据.师门技能[self.选中师门技能].包含技能[self.选中师门法术].种类 == 12 ))
		if self.资源组[26]:事件判断() then --经脉
			客户端:发送数据(1,4, 5, "0")
		elseif self.资源组[27]:事件判断() then

			if self.数据.师门技能[self.选中师门技能].包含技能[self.选中师门法术].种类 == 0  then
			    客户端:发送数据(6321, 57, 40, "61", 1)
			elseif self.数据.师门技能[self.选中师门技能].包含技能[self.选中师门法术].种类 == 12  then
				客户端:发送数据(self.选中师门技能,11,5,self.选中师门法术)
			end
            self:打开()
			return false
		end
		self.资源组[28]:更新(x,y)
		self.资源组[26]:显示(self.x+165,self.y+346,true)
		self.资源组[27]:显示(self.x+240,self.y+347,true)
		self.资源组[28]:显示(self.x+235,self.y+378,true)
		zts:置颜色(-16777216)
		if self.数据.门派 ~= "无" then
			for n=1,7 do
				local jx = self.x + 24
				local jy = self.y + (n+1) * 38 - 5
				bw1:置坐标(jx-5,jy-4)
				if self.选中师门技能 ~= n then
					if bw1:检查点(x,y) and self.鼠标 then
						引擎.画矩形(jx-5,jy-4,jx+117,jy+34,-3551379)
						if mousea(0) and not tp.消息栏焦点 then
							self.选中师门技能 = n
							self.技能={}
							self.选中师门法术 = 0
						end
						self.焦点 = true
					end
				else
					local ys = -10790181
					if bw1:检查点(x,y) then
						ys = -9670988
						self.焦点 = true
					end
					引擎.画矩形(jx-5,jy-4,jx+117,jy+34,ys)
				end
				self.师门技能[n]:置坐标(jx,jy)
				self.师门技能[n]:显示(x,y,self.鼠标,1)
				if self.师门技能[n].焦点  then
					tp.提示:技能(x,y,self.师门技能[n].技能,self.师门技能[n].技能.剩余冷却回合)
				end
				zts:显示(jx+35,jy,self.师门技能[n].技能.名称)
				zts:显示(jx+35,jy+15,self.师门技能[n].技能.等级)
			end
		end
		if self.选中师门技能 ~= 0 then
			local bh = rw.师门技能[self.选中师门技能].包含技能

			for n=1,#bh do
				if self.技能[n] == nil then
					local xsd =SkillData[bh[n].名称]
			
					self.技能[n] = tp.资源:载入(xsd.文件,"网易WDF动画",xsd.小图标)
					bh[n].剩余冷却回合 = xsd.冷却
					bh[n].介绍=xsd.介绍
					bh[n].消耗说明=xsd.消耗
					bh[n].使用条件=xsd.条件
					bh[n].小模型=self.技能[n]
					bh[n].选中技能 = self.选中师门技能
					if not bh[n].学会 then
						self.技能[n]:灰度级()
					end
				end
				local jx = self.x + 160
				local jy = self.y + (n+1) * 38 - 5
				bw1:置坐标(jx-5,jy-4)
				if self.选中师门法术 ~= n then
					if bw1:检查点(x,y) and self.鼠标 and not tp.消息栏焦点 then
						引擎.画矩形(jx-5,jy-4,jx+117,jy+34,-3551379)
						if mousea(0) and not tp.消息栏焦点 and self.鼠标 and bh[n].学会   then
							self.选中师门法术 = n
						end
						self.焦点 = true
					end
				else
					if bw1:检查点(x,y) and self.鼠标 and not tp.消息栏焦点 then
						引擎.画矩形(jx-5,jy-4,jx+117,jy+34,-3551379)
						if mousea(0) and not tp.消息栏焦点 and self.鼠标 and bh[n].学会   then
							tp.抓取技能 = bh[n]
							tp.抓取技能ID = n
						end
						self.焦点 = true
					end


					local ys = -10790181
					if bw1:检查点(x,y) then
						ys = -9670988
						self.焦点 = true
					end
					引擎.画矩形(jx-5,jy-4,jx+117,jy+34,ys)
				end
				self.技能[n]:显示(jx,jy+2)
				if self.技能[n]:是否选中(x,y) and self.鼠标 then
					tp.提示:技能(x,y,bh[n],bh[n].剩余冷却回合)
				end
				zts:显示(jx+34,jy+4,bh[n].名称)
			end
		end
		for n=1,6 do
			local ts = self.数据.特技数据[n]
			local jx = self.x + n * 36 - 18
			local jy = self.y + 374
			self.资源组[42]:显示(jx,jy)
			if ts ~= nil and ts.名称 ~= nil then
				if self.特技数据[n] == nil then
					local ts1 = SkillData[ts.名称]
					ts.介绍 =ts1.介绍
					self.特技数据[n] = tp.资源:载入(ts1.文件,"网易WDF动画",ts1.小图标)
				end
				self.特技数据[n]:显示(jx-1,jy-1)
				if self.特技数据[n]:是否选中(x,y) and self.鼠标 then
					self.焦点 = true
					tp.提示:技能(x,y,ts)
				end
			end
		end
	elseif self.状态 == 3 then
		for i=1,3 do
			self.资源组[28+i]:更新(x,y,self.子类状态 ~= i)
			if self.资源组[28+i]:事件判断() then
				self.子类状态 = i
			end
			self.资源组[28+i]:显示(self.x + 25 + (i-1)*85,self.y + 40,true,nil,nil,self.子类状态 == i,2)
		end
		local xx = 0
		local yy = 0
		for i=1,6 do
			self.资源组[31+i]:更新(x,y)
			self.资源组[31+i]:显示(self.x + 18 + xx * 93,self.y + 345 + yy * 29,true)
			xx = xx + 1
			if xx > 2 then
				xx = 0
				yy = yy + 1
			end
		end
		if self.资源组[33]:事件判断()then
			  客户端:发送数据(65,28,13,"97",1)---炼药
			elseif self.资源组[32]:事件判断()then --烹饪
			客户端:发送数据(56,29,13,"97",1)
			elseif self.资源组[34]:事件判断()then --打造
			  客户端:发送数据(2, 6, 13, "9", 1)
			elseif self.资源组[35]:事件判断()then --摆摊
				tp.场景.人物:停止移动()
			  客户端:发送数据(2647,1,44,"1",1)
			  elseif self.资源组[36]:事件判断() then
	       tp.窗口.文本栏:载入("#H/在这里可以选择需要拍卖的道具或者召唤兽60秒内无人出价商品将自动退还,最后竞拍给出价最高的,竞拍成功扣除10%的手续费,拍卖商品需要消耗500仙玉,请选择你需要竞拍的类型","拍卖",true)
 		elseif self.资源组[37]:事件判断() then
            
 			tp.窗口.文本栏:载入("#H/一亿经验加上1000万银子可以换取1点技能点，你确定需要学习吗？","剧情点",true)
		end
		xx = 0
		yy = 0
		for n=1,12 do
			local jx = self.x+xx*60+31
			local jy = self.y+yy*80+90
			self.资源组[40]:显示(jx,jy)
			self.资源组[38]:显示(jx+4,jy+49)
			if self.子类状态 == 1 then
				self.辅助技能[n]:置坐标(jx+3,jy+2)
				self.辅助技能[n]:显示(x,y,self.鼠标)
				if self.辅助技能[n].技能 ~= nil then
					zts:置颜色(-16777216)
					zts:显示(jx+11,jy+53,self.辅助技能[n].技能.等级)
					if self.辅助技能[n].焦点 then
						tp.提示:技能(x,y,self.辅助技能[n].技能)
					end
				end
			elseif self.子类状态 == 3 then
					self.强化技能[n]:置坐标(jx+3,jy+2)
					self.强化技能[n]:显示(x,y,self.鼠标)
					if self.强化技能[n].技能 ~= nil then
					zts:置颜色(-16777216)
					zts:显示(jx+11,jy+53,self.强化技能[n].技能.等级)
					if self.强化技能[n].焦点 then
					tp.提示:技能(x,y,self.强化技能[n].技能)
					end
					end
			elseif self.子类状态 == 2 then
					self.剧情技能[n]:置坐标(jx+3,jy+2)
					self.剧情技能[n]:显示(x,y,self.鼠标,nil,true)

					if self.剧情技能[n].技能 ~= nil then
					zts:置颜色(-16777216)
					zts:显示(jx+11,jy+53,self.剧情技能[n].技能.等级)
						if self.剧情技能[n].焦点 then
						tp.提示:技能(x,y,self.剧情技能[n].技能)
						end

						if self.剧情技能[n].事件 then

							if self.上一次 ~= 0 then
							self.剧情技能[self.上一次].技能.认证=false
						end
							self.选中剧情=n
							self.上一次=n
						end
					end

			end
			xx = xx + 1
			if xx > 3 then
				xx = 0
				yy = yy + 1
			end
		end
		if self.子类状态 == 2 then
				self.资源组[38]:显示(self.x+140,self.y+65)
					self.资源组[47]:显示(self.x+195,self.y+65)
					self.资源组[47]:更新(x,y,self.选中剧情~=0)
					self.资源组[48]:显示(self.x+195,self.y+321)
					self.资源组[48]:更新(x,y,self.选中剧情==3 and  self.剧情技能[3].技能.等级>0)
					zts:置颜色(0xFFFFFFFF):显示(self.x+35,self.y+68,"剩余剧情技能点：")
					zts:置颜色(-16777216):显示(self.x+153,self.y+68,self.数据.剧情技能点)
		end
		if self.选中剧情~=0 then
		self.剧情技能[self.选中剧情].技能.认证=true
		end
		if  self.资源组[47]:事件判断() then
			客户端:发送数据(19,19,5,self.选中剧情)
		elseif  self.资源组[48]:事件判断()  then
            	 tp.鼠标.置鼠标("道具")
		     tp.变身开关 = true
		end
	elseif self.状态 == 4 then
		zts2:置字间距(3)
		zts2:显示(self.x+27,self.y+40,"角色自身修炼  当前:"..self.数据.人物修炼.当前)
		zts2:显示(self.x+27,self.y+221,"召唤兽控制修炼 当前:"..self.数据.召唤兽修炼.当前)
		zts2:置字间距(0)
		self.资源组[2]:置宽高(255,115)
		self.资源组[2]:显示(self.x+19,self.y+77)
		self.资源组[2]:显示(self.x+19,self.y+258)
		
		for i=0,4 do
			local jx = self.x + 34
			local jy = self.y + 85 + i * 22
			bw2:置坐标(jx-9,jy-7)
			local xz = bw2:检查点(x,y)
			if self.修炼选中1 ~= i+1 then
				if xz then
					box(jx-9,jy-4,jx+236,jy+14,-3551379)
					if mousea(0) then
						self.修炼选中1 = i+1
					end
					self.焦点 = true
				end
			else
				local ys = -10790181
				if xz then
					ys = -9670988
					self.焦点 = true
				end
			    box(jx-9,jy-4,jx+236,jy+14,ys)
			end--计算修炼等级经验(等级,上限)
		end
		for i=0,3 do
			local jx = self.x + 34
			local jy = self.y + 275 + i * 22
			bw2:置坐标(jx-9,jy-7)
			local xz = bw2:检查点(x,y)
			if self.修炼选中2 ~= i+1 then
				if xz then
					box(jx-9,jy-4,jx+236,jy+14,-3551379)
					if mousea(0) then
						self.修炼选中2 = i+1
					end
					self.焦点 = true
				end
			else
				local ys = -10790181
				if xz then
					ys = -9670988
					self.焦点 = true
				end
			    box(jx-9,jy-4,jx+236,jy+14,ys)
			end--计算修炼等级经验(等级,上限)
		end
		self.资源组[45]:更新(x,y,self.修炼选中1~=0 or self.修炼选中2~=0 )
		if  self.资源组[45]:事件判断() then
			local 人物修 = "无"
			local bb修 = "无"
			if self.修炼选中1~=0 then
				人物修=as11[self.修炼选中1]
			end
			if self.修炼选中2~=0 then
				bb修=as11[self.修炼选中2]
			end
			客户端:发送数据(12,12,5,人物修,bb修)
		end
		self.资源组[45]:显示(self.x+180,self.y+380)




        zts3:置颜色(-16777216)

		zts3:显示(self.x + 34,self.y + 85 ,as[1].." 等级:"..self.数据.人物修炼.攻击.等级.."/"..self.数据.人物修炼.攻击.上限.." 修炼经验:"..self.数据.人物修炼.攻击.经验.."/"..self.数据.人物修炼经验.攻击)
		zts3:显示(self.x + 34,self.y + 85 + 1 * 22,as[2].." 等级:"..self.数据.人物修炼.防御.等级.."/"..self.数据.人物修炼.防御.上限.." 修炼经验:"..self.数据.人物修炼.防御.经验.."/"..self.数据.人物修炼经验.防御)
		zts3:显示(self.x + 34,self.y + 85 + 2 * 22,as[3].." 等级:"..self.数据.人物修炼.法术.等级.."/"..self.数据.人物修炼.法术.上限.." 修炼经验:"..self.数据.人物修炼.法术.经验.."/"..self.数据.人物修炼经验.法术)
		zts3:显示(self.x + 34,self.y + 85 + 3 * 22,as[4].." 等级:"..self.数据.人物修炼.法抗.等级.."/"..self.数据.人物修炼.法抗.上限.." 修炼经验:"..self.数据.人物修炼.法抗.经验.."/"..self.数据.人物修炼经验.法抗)
		zts3:显示(self.x + 34,self.y + 85 + 4 * 22,as[5].." 等级:25/25 修炼经验:9999999")

		zts3:显示(self.x + 34,self.y + 275 ,as1[1].." 等级:"..self.数据.召唤兽修炼.攻击.等级.."/"..self.数据.召唤兽修炼.攻击.上限.." 修炼经验:"..self.数据.召唤兽修炼.攻击.经验.."/"..self.数据.召唤兽修炼经验.攻击)
		zts3:显示(self.x + 34,self.y + 275 + 1 * 22,as1[2].." 等级:"..self.数据.召唤兽修炼.防御.等级.."/"..self.数据.召唤兽修炼.防御.上限.." 修炼经验:"..self.数据.召唤兽修炼.防御.经验.."/"..self.数据.召唤兽修炼经验.防御)
		zts3:显示(self.x + 34,self.y + 275 + 2 * 22,as1[3].." 等级:"..self.数据.召唤兽修炼.法术.等级.."/"..self.数据.召唤兽修炼.法术.上限.." 修炼经验:"..self.数据.召唤兽修炼.法术.经验.."/"..self.数据.召唤兽修炼经验.法术)
		zts3:显示(self.x + 34,self.y + 275 + 3 * 22,as1[4].." 等级:"..self.数据.召唤兽修炼.法抗.等级.."/"..self.数据.召唤兽修炼.法抗.上限.." 修炼经验:"..self.数据.召唤兽修炼.法抗.经验.."/"..self.数据.召唤兽修炼经验.法抗)
		--zts3:显示(self.x + 34,self.y + 275 + 4 * 22,as1[5].." 等级:"..self.数据.召唤兽修炼.猎术.等级.."/"..self.数据.召唤兽修炼.猎术.上限.." 修炼经验:"..self.数据.召唤兽修炼.猎术.经验.."/"..self.数据.召唤兽修炼经验.猎术)
			end
	--逻辑集合
	bw:置坐标(self.x-24,self.y+36)
end

function 场景类_人物状态栏:检查点(x,y)
	if self.资源组[1]:是否选中(x,y) or bw:检查点(x,y)  then
		return true
	end
end

function 场景类_人物状态栏:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_人物状态栏:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end


return 场景类_人物状态栏