--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:47
--======================================================================--
local 场景类_宠物炼妖栏 = class()
local bw = require("gge包围盒")(0,0,120,37)
local box = 引擎.画矩形
local remove = table.remove
local insert = table.insert
local random = 引擎.取随机整数
local min = math.min
local cfs = 删除重复
local tss = 引擎.取天生
local qmxs = 取模型
local mouseb = 引擎.鼠标弹起

local zts,tp,zts1
local insert = table.insert
local qmx = 引擎.取模型
function 场景类_宠物炼妖栏:初始化(根)
	self.ID = 16
	self.x = 189+(全局游戏宽度-800)/2
	self.y = 119
	self.xx = 0
	self.yy = 0
	self.注释 = "宠物炼妖栏"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	tp = 根
	local 按钮 = tp._按钮
	local 自适应 = 根._自适应
	local 物品 = tp._物品格子
	self.资源组 = {
		[0] = 自适应.创建(1,1,403,18,1,3,nil,18),
		[1] = 自适应.创建(0,1,441,405,3,9),
		[2] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),
		[3] = 按钮.创建(自适应.创建(12,4,55,22,1,3),0,0,4,true,true,"炼妖"),
		[4] = 按钮.创建(自适应.创建(12,4,55,22,1,3),0,0,4,true,true,"洗炼"),
		[5] = 按钮.创建(自适应.创建(12,4,55,22,1,3),0,0,4,true,true,"炼化"),
		[6] = 按钮.创建(自适应.创建(12,4,55,22,1,3),0,0,4,true,true,"提取"),
		[7] = 按钮.创建(自适应.创建(20,4,18,19,4,3),0,0,4,true,true),
		[8] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),
		[9] = 按钮.创建(自适应.创建(22,4,27,20,4,3),0,0,4,true,true),
		[10] = 按钮.创建(自适应.创建(23,4,27,20,4,3),0,0,4,true,true),
		[11] = 自适应.创建(2,1,130,117,3,9),
		[12] = 根.资源:载入('JM.dll',"网易WDF动画",0x62F1C735),
		[13]= 根.资源:载入('JM.dll',"网易WDF动画",0x4EE0010A),
		[14]= 根.资源:载入('JM.dll',"网易WDF动画",0x4EE0010A),
	}
	 self.资源组[13].当前帧=取随机数(1,self.资源组[13].数量)
	  self.资源组[14].当前帧=取随机数(1,self.资源组[14].数量)
	self.物品 = {}
	for i=1,180 do
		self.物品[i] = 物品.创建(0,0,i,"炼妖物品")
	end
	for n=2,10 do
		self.资源组[n]:绑定窗口_(16)
	end
	self.头像组 = {}
	self.炼妖物品 = 物品.创建(0,0,0,"炼妖_物品")
	self.加入 = 0
	self.开始 = 1
	self.结束 = 20
	self.窗口时间 = 0
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.描边字体
	self.炼化结束=0
	self.召唤兽 ={}
	self.道具 = {}
	self.编号 = 0
	self.类型 = "包裹"
end
function 场景类_宠物炼妖栏:添加召唤兽(数据)
	self.炼化结束=0
self.召唤兽 = 数据
end
function 场景类_宠物炼妖栏:添加道具(数据)
	self.炼化结束=0
	self.道具= 数据
		for n=1,20 do
			self.物品[n]:置物品(数据[n])
		end
end
function 场景类_宠物炼妖栏:打开()
	if self.可视 then
		self.可视 = false
		self.主动 = nil
		self.副动 = nil
		self.主召唤兽 = nil
		self.炼妖物品:置物品(nil)
		self.副召唤兽 = nil
		self.主点击 = nil
		self.副点击 = nil
	else
		if  self.x > 全局游戏宽度 then
		self.x = 189+(全局游戏宽度-800)/2
		end
		self.炼化结束=0
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end





function 场景类_宠物炼妖栏:主置形象()
	if self.召唤兽[self.主召唤兽]  ~= nil and self.主召唤兽 ~= 0 then
		local n = 取模型(self.召唤兽[self.主召唤兽].造型)
		self.主动 = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
		if self.召唤兽[self.主召唤兽].饰品  then
			n = 取模型("饰品_"..self.召唤兽[self.主召唤兽].造型)
			self.资源组[24] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
		end
		if self.召唤兽[self.主召唤兽].染色方案 then
			 self.主动:置染色(self.召唤兽[self.主召唤兽].染色方案,self.召唤兽[self.主召唤兽].染色组[1],self.召唤兽[self.主召唤兽].染色组[2],self.召唤兽[self.主召唤兽].染色组[3],self.召唤兽[self.主召唤兽].染色组[4])
			 self.主动:置方向(0)
		end
	end
end


function 场景类_宠物炼妖栏:副置形象()
	if self.召唤兽[self.副召唤兽]  ~= nil and self.副召唤兽 ~= 0 then
		local n = 取模型(self.召唤兽[self.副召唤兽].造型)
		self.副动 = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
		if self.召唤兽[self.副召唤兽].饰品  then
			n = 取模型("饰品_"..self.召唤兽[self.副召唤兽].造型)
			self.资源组[24] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
		end
		if self.召唤兽[self.副召唤兽].染色方案 then
			 self.副动:置染色(self.召唤兽[self.副召唤兽].染色方案,self.召唤兽[self.副召唤兽].染色组[1],self.召唤兽[self.副召唤兽].染色组[2],self.召唤兽[self.副召唤兽].染色组[3],self.召唤兽[self.副召唤兽].染色组[4])
			 self.副动:置方向(0)
		end
	end
end


function 场景类_宠物炼妖栏:显示(dt,x,y)
	local bbs = self.召唤兽
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y)
	self.资源组[4]:更新(x,y)
	self.资源组[5]:更新(x,y)
	self.资源组[6]:更新(x,y)
	self.资源组[7]:更新(x,y)
	self.资源组[8]:更新(x,y)
	-- self.资源组[9]:更新(x,y)
	-- self.资源组[10]:更新(x,y)
	self.资源组[3]:更新(x,y,self.主召唤兽 ~= nil and self.副召唤兽 ~= nil)
	self.资源组[4]:更新(x,y,((self.主召唤兽 ~= nil and self.副召唤兽 == nil) or (self.副召唤兽 ~= nil and self.主召唤兽 == nil)))
	self.资源组[5]:更新(x,y,((self.主召唤兽 ~= nil and self.副召唤兽 == nil) or (self.副召唤兽 ~= nil and self.主召唤兽 == nil)))
	self.资源组[6]:更新(x,y,((self.主召唤兽 ~= nil and self.副召唤兽 == nil) or (self.副召唤兽 ~= nil and self.主召唤兽 == nil)))
	self.资源组[7]:更新(x,y,self.加入 > 0)
	self.资源组[8]:更新(x,y,self.加入 < #self.召唤兽 - 7)
	-- self.资源组[9]:更新(x,y,self.道具.类型 == "行囊")
	-- self.资源组[10]:更新(x,y,self.道具.类型 == "包裹")
	if self.主动 ~= nil then
		self.主动:更新(dt)
	end
	if self.副动 ~= nil then
		self.副动:更新(dt)
	end
	if self.资源组[3]:事件判断() then
			if self.炼妖物品.物品 ~=nil then
			tp.提示:写入("#Y/请将炼妖物品栏清空")
			return 0
			 end

			if not (self.主召唤兽 ==self.召唤兽.参战 or self.副召唤兽 == self.召唤兽.参战) then
		            客户端:发送数据(self.主召唤兽, 23, 13,self.副召唤兽)
					self.主召唤兽 = nil
					self.副召唤兽 = nil
					if tp.窗口.召唤兽资质栏.可视 then
						tp.窗口.召唤兽资质栏:打开(v,true)
					end
			else
				tp.提示:写入("#Y/炼妖前请取消参战状态")
			end

	elseif self.资源组[4]:事件判断() then
		if self.副召唤兽 ~=nil then
			tp.提示:写入("#Y/打书或者内丹请一只一只来")
			return 0
			 end
		if self.炼妖物品.物品 ~= nil then
			if self.主召唤兽 ~= nil then
				if self.炼妖物品.物品.类型 == "炼妖" and self.炼妖物品.物品.名称~="圣兽丹" and self.炼妖物品.物品.名称~="吸附石" and self.炼妖物品.物品.名称~="炼妖石"then

		              客户端:发送数据(self.主召唤兽, 24, 13, self.编号,self.类型)
		              self.炼妖物品:置物品(nil)
				else
					if self.炼妖物品.物品.名称=="圣兽丹" or self.炼妖物品.物品.名称=="吸附石" then
					tp.提示:写入("#Y/圣兽丹或者吸附石请点击提取按钮提取召唤兽饰品")
				    elseif self.炼妖物品.物品.名称=="炼妖石" then
				    	tp.提示:写入("#Y/炼妖石请点击炼化按钮提取召唤兽饰品")
					else
					tp.提示:写入("#Y/请放入正确物品")
					end
				end
			else

				tp.提示:写入("#Y/请选择一只召唤兽")
			end
		else
				tp.提示:写入("#Y/请放入物品")

		end
    elseif self.资源组[5]:事件判断() then
	  if self.副召唤兽 ~=nil then
			tp.提示:写入("#Y/提取召唤兽请一只一只来")
			return 0
		end
		if self.炼妖物品.物品 ~= nil then
			if self.主召唤兽 ~= nil then
				if self.炼妖物品.物品.名称 == "炼妖石"  then

					    tp.窗口.文本栏:载入("与“炼妖石”炼妖合成的召唤兽会被消耗，炼妖石的等级必须要大于召唤兽的参战等级。#W，你是否确定使用炼妖石：","炼妖石",true)
				else
					tp.提示:写入("#Y/请放入正确物品")
				end
			else
				tp.提示:写入("#Y/请选择一只召唤兽")
			end
		else
			tp.提示:写入("#Y/请放入物品")
		end
	elseif self.资源组[6]:事件判断() then
		if self.副召唤兽 ~=nil then
			tp.提示:写入("#Y/提取召唤兽请一只一只来")
			return 0
		end
		if self.炼妖物品.物品 ~= nil then
			if self.主召唤兽 ~= nil then
				if self.炼妖物品.物品.名称 == "圣兽丹"  then
					    tp.窗口.文本栏:载入("与“圣兽丹”炼妖合成的召唤兽会被消耗，每只召唤兽只能佩戴一个召唤兽饰品。#W，你是否确定要提取：","提取",true)
                elseif self.炼妖物品.物品.名称 == "吸附石"  then
					    tp.窗口.文本栏:载入("与“吸附石”炼妖合成的召唤兽会被消耗，随机几率提取召唤兽随机技能。#W，你是否确定要吸取：","吸附",true)
				else
					tp.提示:写入("#Y/请放入正确物品")
				end
			else
				tp.提示:写入("#Y/请选择一只召唤兽")
			end
		else
			tp.提示:写入("#Y/请放入物品")
		end
	elseif self.资源组[7]:事件判断() then
		self.加入 = self.加入 - 1
	elseif self.资源组[8]:事件判断() then
		self.加入 = self.加入 + 1
	-- elseif self.资源组[9]:事件判断() then
 --        -- 客户端:发送数据(6321, 22, 13, "包裹", 1) --炼妖
	-- elseif self.资源组[10]:事件判断() then
	-- 	客户端:发送数据(6321, 22, 13, "行囊", 1) --炼妖
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[0]:显示(self.x+6,self.y+3)
	Picture.窗口标题背景_:置区域(0,0,74,16)
	Picture.窗口标题背景_:显示(self.x+178,self.y+3)
	zts1:显示(self.x+192,self.y+3,"炼  妖")
	self.资源组[11]:置宽高(130,117)
	for i=1,2 do
		self.资源组[11]:显示(self.x+i*261-236,self.y+36)
	end
	self.资源组[11]:置宽高(150,228)
	self.资源组[11]:显示(self.x+13,self.y+164)
	self.资源组[2]:显示(self.x + 416,self.y + 6)
	self.资源组[3]:显示(self.x + 165,self.y + 103,true)
	self.资源组[4]:显示(self.x + 224,self.y + 103,true)
	self.资源组[5]:显示(self.x + 165,self.y + 129,true)
	self.资源组[6]:显示(self.x + 224,self.y + 129,true)
	self.资源组[7]:显示(self.x + 148,self.y + 166)
	self.资源组[8]:显示(self.x + 148,self.y + 374)
	-- self.资源组[9]:显示(self.x + 173,self.y + 377)
	-- self.资源组[10]:显示(self.x + 204,self.y + 377)
    if  self.炼化结束 >0   then

        self.资源组[13]:显示(self.x + 300,self.y + 70)

	    self.资源组[14]:显示(self.x + 350,self.y + 70)
	   self.资源组[13]:更新(dt)
	   self.资源组[14]:更新(dt)
	   self.炼化开始 =nil
	   self.炼化结束=self.炼化结束+1
	end
	if self.炼化结束 ==120 then
		客户端:发送数据(self.主召唤兽, 88, 13, self.编号,self.类型)
		   self.炼妖物品:置物品(nil)
	        self.主召唤兽 = nil
	   self.炼化结束=0
	end
	tp.画线:置区域(0,0,15,189)
	tp.画线:显示(self.x+152,self.y+185)
	Picture.物品界面背景_:显示(self.x+174,self.y+164)
	tp.横排花纹背景_:置区域(0,0,192,18)
	tp.横排花纹背景_:显示(self.x+237,self.y+376)
	Picture.物品格子背景_:显示(self.x+196,self.y+39)
	local p = self.开始 - 1
	for h=1,4 do
		for l=1,5 do
			p = p + 1
			self.物品[p]:置坐标(l * 51 + self.x+124,h * 51+self.y+114)
			self.物品[p]:显示(dt,x,y,self.鼠标,{"炼妖"})
			 if self.物品[p].焦点 then
					if self.物品[p].物品 ~= nil then
					tp.提示:道具行囊(x,y,self.物品[p].物品)
					if mouseb(0) and (self.物品[p].物品.类型 == "炼妖" ) then
				    	if self.炼妖物品.物品 == nil then
				    		self.炼妖物品:置物品(self.物品[p].物品)
				    		self.编号 = p
				    		self.类型= self.道具.类型
				    		self.物品[p]:置物品(nil)
				    	elseif self.炼妖物品.物品 ~= nil then
				    		local jy1 = self.炼妖物品.物品
				    		self.编号 = p
				    		self.类型= self.道具.类型
				    		客户端:发送数据(6321, 22, 13, self.道具.类型, 1)
				    		self.炼妖物品:置物品(self.物品[p].物品)
				    		self.物品[p]:置物品(nil)
				    	end
				   	end
				end
			end
		end
	end
	if self.主动 ~= nil and self.主召唤兽 ~= nil then
		tp.影子:显示(self.x + 85,self.y + 130)
		self.主动:显示(self.x + 85,self.y + 130)
		if self.资源组[24] ~= nil then
			self.资源组[24]:更新(dt)
			self.资源组[24]:显示(self.x + 85,self.y + 130)
		end
	end
	if self.副动 ~= nil and self.副召唤兽 ~= nil then
		tp.影子:显示(self.x + 345,self.y + 130)
		self.副动:显示(self.x + 345,self.y + 130)
		if self.资源组[25] ~= nil then
			self.资源组[25]:更新(dt)
			self.资源组[25]:显示(self.x + 345,self.y + 130)
		end
	end
	self.炼妖物品:置坐标(self.x+199,self.y+40)
	self.炼妖物品:显示(dt,x,y,self.鼠标)
	if self.炼妖物品.物品 ~= nil and self.炼妖物品.焦点 then
		tp.提示:道具行囊(x,y,self.炼妖物品.物品)
		if mouseb(0) then
				local wp = self.炼妖物品.物品 -- 主物品
				self.炼妖物品:置物品(nil)
				客户端:发送数据(6321, 22, 13, self.道具.类型, 1)
		end
	end
	for i=1,#bbs do
		if bbs[i+self.加入] ~= nil  then
			local jx = self.x+19
			local jy = self.y+(i*43)+129
			bw:置坐标(jx,jy+1)
			if self.头像组[i+self.加入] == nil or self.头像组[i+self.加入][1] ~= bbs[i+self.加入].造型 then
				self.头像组[i+self.加入] = {bbs[i+self.加入].造型}
				local n =  ModelData[self.头像组[i+self.加入][1]]
				self.头像组[i+self.加入][2] = tp.资源:载入(n.头像资源,"网易WDF动画",n.小头像)
			end
			if bw:检查点(x,y) then
				if mouseb(0) and self.鼠标 and not tp.消息栏焦点 then
					if self.主召唤兽 ~= i + self.加入 and self.副召唤兽 ~= i + self.加入 then
						if self.主召唤兽 == nil then
							self.主召唤兽 = i + self.加入
							self:主置形象()
						elseif self.副召唤兽 == nil then
							self.副召唤兽 = i + self.加入
							self:副置形象()
						elseif self.主召唤兽 ~= nil and self.副召唤兽 ~= nil then
							self.主召唤兽 = i + self.加入
							self:主置形象()
						end
					else
						if self.主召唤兽 == i + self.加入 then
							self.主召唤兽 = nil
							self:主置形象()
						elseif self.副召唤兽 == i + self.加入 then
							self.副召唤兽 = nil
							self:副置形象()
						end
					end
				end
				self.焦点 = true
			end
			if self.主召唤兽 == i + self.加入 or self.副召唤兽 == i + self.加入 then
				box(jx-2,jy-3,jx+128,jy+40,-2097481216)
			end
			tp.宠物头像背景_:显示(jx+1,jy)
			self.头像组[i][2]:显示(jx+4,jy+4)
			if self.主召唤兽 == i + self.加入 or self.副召唤兽 == i + self.加入 then
				self.资源组[12]:显示(jx+92,jy+3)
			end
			if bbs[i+self.加入].参战信息 ~= nil then
				zts:置颜色(-65536)
			else
				zts:置颜色(-16777216)
			end
			zts:显示(jx+41,jy+4,bbs[i+self.加入].名称)
			zts:显示(jx+41,jy+21,bbs[i+self.加入].等级.."级")
		end
	end
end

function 场景类_宠物炼妖栏:检查点(x,y)
	if self.资源组[1]:是否选中(x,y)  then
		return true
	end
end

function 场景类_宠物炼妖栏:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if  self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_宠物炼妖栏:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_宠物炼妖栏








