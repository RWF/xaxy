-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-29 00:11:55

local 圣旨 = class()
local insert = table.insert
local remove = table.remove
local tp,zts1,zts
local mouseb = 引擎.鼠标弹起
function 圣旨:初始化(根)
    self.ID = 260
    self.x = 210
    self.y = 185
    self.xx = 0
    self.yy = 0
    self.注释 = "圣旨"
    self.可视 = false
    self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    local 资源 = 根.资源
    local 按钮 = 根._按钮
    local 自适应 = 根._自适应
    self.资源组 = {
        [0] = 资源:载入('MAX.7z',"网易WDF动画",110),
         [1] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"传音分享"),
        [2] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"空间分享"),
     }
     self.资源组[1]:绑定窗口_(260)
     self.资源组[2]:绑定窗口_(260)
    self.窗口时间 = 0
    tp = 根
    zts = 根.字体表.普通字体1
end

function 圣旨:打开(数据)
    if self.可视 then
        self.可视 = false

self.大模型=nil
    else
        if  self.x > 全局游戏宽度 then
        self.x = 310
        end

local item =ItemData[数据.名称]

self.大模型 = tp.资源:载入(item.文件 ,"网易WDF动画",item.大图标)

       self.数据 = 数据
        insert(tp.窗口_,self)
        tp.运行时间 = tp.运行时间 + 1
        self.窗口时间 = tp.运行时间
        self.可视 = true
    end
end

function 圣旨:显示(dt,x,y)
    self.焦点 = false
  self.资源组[0]:显示(self.x,self.y)
    self.资源组[1]:更新(x,y)
    self.资源组[2]:更新(x,y)
  self.资源组[1]:显示(self.x+190,self.y+129)
    self.资源组[2]:显示(self.x+280,self.y+129)
self.大模型:显示(self.x+55,self.y+30)
 zts:置颜色(0xFF703B3B):显示(self.x+220,self.y+60,self.数据.名称)
if self.数据.技能 then

    zts:置颜色(0xFF703B3B):显示(self.x+190,self.y+88,"所带技能："..self.数据.技能)
elseif self.数据.等级 then
   zts:置颜色(0xFF703B3B):显示(self.x+190,self.y+88,"所带等级："..self.数据.等级)
end

 zts:置颜色(0xFFFFFFFF)



end

function 圣旨:检查点(x,y)
    if self.资源组[0]:是否选中(x,y)  then
        return true
    end
end

function 圣旨:初始移动(x,y)
    tp.运行时间 = tp.运行时间 + 1
    if not tp.消息栏焦点 then
        self.窗口时间 = tp.运行时间
    end
    if not self.焦点 then
        tp.移动窗口 = true
    end
    if self.鼠标 and not self.焦点 then
        self.xx = x - self.x
        self.yy = y - self.y
    end
end

function 圣旨:开始移动(x,y)
    if self.鼠标 then
        self.x = x - self.xx
        self.y = y - self.yy
    end
end

return 圣旨