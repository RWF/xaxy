-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-24 02:34:38
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-03-16 19:45:24
--======================================================================--
-- 972CA1E4 背景



-- EE991CA7 总良师值
-- DECF7EA1 总积分
-- DC7797A6 徒弟数量
-- C7636E9B 房屋类型
-- BD49F124 规模
-- 46E51276 店名
-- 2E2ABAB5 价值
-- 107F0617 分数
-- 012CAF64 名次
-- 040F53E6 得分
-- 19EA9000 帮派
-- 1F6A1A2E 魅力值
-- 2002BD19 店主
-- 4C83D1D6 发现人
-- 51111704 古玩名
-- 67EFB10F 积分

-- 83179BB2 服务器
-- 862D226A 真品数
-- 87401A04 成就点数
-- 8BC27028 领悟阶段
-- 8E2096D4 昵称
-- A27C95F0 帮派人数



local 场景类_排行榜 = class(窗口逻辑)
local insert = table.insert
local remove = table.remove
local floor = math.floor
local tp,zts,zts1
local 属性排行 ={"气血排行","防御排行","伤害排行","速度排行","命中排行","灵力排行","魔法排行","躲避排行"}
local 积分排行 ={"门贡排行","知了积分","长安保卫","活动积分","比武积分","副本积分","成就积分","单人积分"}
local 数据排行 ={"银子排行","储备排行","积累经验","经验排行","仙玉排行","累冲排行","积累银子","人物等级"}
function 场景类_排行榜:初始化(根)
	self.ID = 200
	self.x = 82+(全局游戏宽度-800)/2
	self.y = 20
	self.xx = 0
	self.yy = 0
	self.注释 = "排行榜"
	tp = 根
		self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
    local  资源= 根.资源
	self.资源组 = {
		[1] = 根.资源:载入('JM.dll',"网易WDF动画",0xFBF970A7),
		[2] = 根.资源:载入('JM.dll',"网易WDF动画",0x75B260A7),
		[3] = 根.资源:载入('JM.dll',"网易WDF动画",0x972CA1E4),
		[4] = 根.资源:载入('JM.dll',"网易WDF动画",0x98EA209E),--1
		[5] = 根.资源:载入('JM.dll',"网易WDF动画",0xAD1D064F),--2
		[6] = 根.资源:载入('JM.dll',"网易WDF动画",0xF31B90F1),--3
		[7] = 根.资源:载入('JM.dll',"网易WDF动画",0x6E62E765),--3
		[8] = 根.资源:载入('JM.dll',"网易WDF动画",0x8269DBB8),--3
		[9] = 根.资源:载入('JM.dll',"网易WDF动画",0x107F0617),
		[10]=按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x20F3E242),0,0,4),--??
		[11]=按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xCAABDBB8),0,0,4),--??
		[12]=按钮.创建(资源:载入('MAX.7z',"网易WDF动画",3),0,0,4,true,true,"银子排行"),
		[13]=按钮.创建(资源:载入('MAX.7z',"网易WDF动画",3),0,0,4,true,true,"储备排行"),
		[14]=按钮.创建(资源:载入('MAX.7z',"网易WDF动画",3),0,0,4,true,true,"积累经验"),
		[15]=按钮.创建(资源:载入('MAX.7z',"网易WDF动画",3),0,0,4,true,true,"经验排行"),
		[16]=按钮.创建(资源:载入('MAX.7z',"网易WDF动画",3),0,0,4,true,true,"仙玉排行"),
		[17]=按钮.创建(资源:载入('MAX.7z',"网易WDF动画",3),0,0,4,true,true,"累冲排行"),
		[18]=按钮.创建(资源:载入('MAX.7z',"网易WDF动画",3),0,0,4,true,true,"积累银子"),
		[19]=按钮.创建(资源:载入('MAX.7z',"网易WDF动画",3),0,0,4,true,true,"人物等级"),
		[20]=按钮.创建(资源:载入('MAX.7z',"网易WDF动画",8),0,0,4,true,true,"数据排行"),
		[21]=按钮.创建(资源:载入('MAX.7z',"网易WDF动画",8),0,0,4,true,true,"积分排行"),
		[22]=按钮.创建(资源:载入('MAX.7z',"网易WDF动画",8),0,0,4,true,true,"属性排行"),

		[23]=按钮.创建(资源:载入('MAX.7z',"网易WDF动画",8),0,0,4,true,true,"帮派排行"),
		[24]=按钮.创建(资源:载入('MAX.7z',"网易WDF动画",8),0,0,4,true,true,"休闲排行"),
		[25]=按钮.创建(资源:载入('MAX.7z',"网易WDF动画",8),0,0,4,true,true,"门派排行"),

		[26]=按钮.创建(根.资源:载入('MAX.7z',"网易WDF动画",9),0,0,3),
	}
	self.状态= "数据排行"
	self.分类状态= "银子排行"
	for n=10,11 do
		self.资源组[n]:绑定窗口_(200)
	end
 	for i=12,19 do
		self.资源组[i]:绑定窗口_(200)
		self.资源组[i]:置文字颜色(0xFFD4BFBF)
		self.资源组[i]:置偏移(25,2)

	end
 	for i=20,25 do
		self.资源组[i]:绑定窗口_(200)
		self.资源组[i]:置文字颜色(0xFF703B3B)
		self.资源组[i]:置偏移(1,4)

	end
	self.资源组[26]:绑定窗口_(200)
	self.翻页 =false
	self.窗口时间 = 0

	zts = tp.字体表.华康字体
	zts1 = tp.字体表.描边字体

	end


function 场景类_排行榜:刷新(内容)
	self.数据 = 内容.数据
		self.状态= 内容.类型
	if self.状态 == "数据排行" then
	    for i=1,8 do
			self.资源组[i+11]:置文字(数据排行[i])
		end
		self.分类状态= 数据排行[1]
	elseif self.状态== "积分排行" then
		for i=1,8 do
			self.资源组[i+11]:置文字(积分排行[i])
		end
		self.分类状态=积分排行[1]
	elseif self.状态== "属性排行" then
		 for i=1,8 do
			self.资源组[i+11]:置文字(属性排行[i])
		end
		self.分类状态= 属性排行[1]
	end
end


function 场景类_排行榜:打开(内容)
  if self.可视 then

		self.可视 = false
      	self.状态= "数据排行"
	  self.分类状态= "银子排行"
		self.可视 = false
		self.翻页 =false
		for i=1,8 do
			self.资源组[i+11]:置文字(数据排行[i])
		end
		self.分类状态= 数据排行[1]

  else

  		    for i=1,8 do
			self.资源组[i+11]:置文字(数据排行[i])
		end
		self.分类状态= 数据排行[1]
  		self.翻页 =false
  	    self.数据 = 内容
  		self.状态= "数据排行"
		self.分类状态= "银子排行"
		if  self.x > 全局游戏宽度 then
		self.x = 82+(全局游戏宽度-800)/2
		end
		insert(tp.窗口_,self)

		tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.可视 = true
  end
 end
function 场景类_排行榜:显示(dt,x,y)
	self.焦点 = false
	for i=10,19 do
		  self.资源组[i]:更新(x,y,self.分类状态~=self.资源组[i]:取文字())
	end
   for i=20,25 do
   	 self.资源组[i]:更新(x,y,self.状态~=self.资源组[i]:取文字())
   end
	for i=12,19 do
		if self.资源组[i]:事件判断() then
		  self.分类状态= self.资源组[i]:取文字()
		end
	end
	 self.资源组[26]:更新(x,y)
	if self.资源组[20]:事件判断() then

		客户端:发送数据(4, 4, 43, "P7")
	elseif self.资源组[21]:事件判断() then

		客户端:发送数据(2, 2, 43, "P7")
   	elseif self.资源组[22]:事件判断() then
		客户端:发送数据(3, 3, 43, "P7")
	elseif self.资源组[11]:事件判断() then
		self.翻页 =not self.翻页
	elseif self.资源组[26]:事件判断() then
		self.可视=false
	end

	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x + 231,self.y + 26)
	self.资源组[3]:显示(self.x + 232,self.y + 120)
	self.资源组[26]:显示(self.x+600,self.y+75)

	self.资源组[10]:显示(self.x + 200,self.y + 420)
	self.资源组[11]:显示(self.x + 420,self.y + 460)
   zts:置颜色(0xFF703B3B):显示(self.x + 450,self.y + 463,"翻页")

	for i=12,19 do
		self.资源组[i]:显示(self.x + 103,self.y +125+(i-12)*35,true,1,nil,self.分类状态==self.资源组[i]:取文字(),2)
	end
	for i=1,6 do
		self.资源组[19+i]:显示(self.x+20+i*80,self.y +80,true,1,nil,self.状态==self.资源组[19+i]:取文字(),2)
	end
 	if self.翻页 then
 		for o=1,10 do
			if self.数据[self.分类状态][o+10] ~= nil then
				引擎.画线(self.x+240,self.y+120+29*o,self.x+583,self.y+120+29*o,0xFFC19B84)
				zts:置颜色(0xFF703B3B):显示(self.x+310,self.y+137+28*o,o+10)
				zts:置颜色(0xFF703B3B):显示(self.x+300+100,self.y+137+28*o,self.数据[self.分类状态][o+10].名称)
				zts:置颜色(0xFF703B3B):显示(self.x+310+180,self.y+137+28*o,self.数据[self.分类状态][o+10].数值)
			end
		end
 	else
 		for o=1,10 do
			if self.数据[self.分类状态][o] ~= nil then
				引擎.画线(self.x+240,self.y+120+29*o,self.x+583,self.y+120+29*o,0xFFC19B84)
				if o > 3 then
					zts:置颜色(0xFF703B3B):显示(self.x+310,self.y+137+28*o,o)
				end
				if self.数据[self.分类状态][o].名称 ==tp.场景.人物.数据.名称  then
					zts:置颜色(0xFFFF0000):显示(self.x+300+100,self.y+137+28*o,self.数据[self.分类状态][o].名称)
					zts:置颜色(0xFFFF0000):显示(self.x+310+180,self.y+137+28*o,self.数据[self.分类状态][o].数值)
				else
					zts:置颜色(0xFF703B3B):显示(self.x+300+100,self.y+137+28*o,self.数据[self.分类状态][o].名称)
					zts:置颜色(0xFF703B3B):显示(self.x+310+180,self.y+137+28*o,self.数据[self.分类状态][o].数值)
				end
			end
		end
	   	self.资源组[4]:显示(self.x+286,self.y+130+15)
		self.资源组[5]:显示(self.x+293,self.y+123+28*2)
		self.资源组[6]:显示(self.x+302,self.y+125+28*3)
 	end


	self.资源组[7]:显示(self.x+295,self.y+125)
	self.资源组[8]:显示(self.x+395,self.y+125)
    self.资源组[9]:显示(self.x +495,self.y + 125)
end




return 场景类_排行榜



