-- @Author: 作者QQ414628710
-- @Date:   2022-01-15 20:06:44
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-24 01:57:37
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:45
--======================================================================--
local CDK = class(窗口逻辑)

function CDK:初始化(根)
    self.ID = 414
    self.x = 224
    self.y = 100
    self.xx = 0
    self.yy = 0
    self.注释 = "成长之路"
    self.可视 = false
    self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    local 资源 = 根.资源
    local 按钮 = 根._按钮
    local 自适应 = 根._自适应
    self.资源组 = {
        [1] = 自适应.创建(99,1,352,100,3,9),
        [2] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
        [3] = 根._按钮(根._自适应(15,4,72,20,1),0,0,4,true,true,"  确定"),
        [4] = 自适应.创建(8,1,220,19,1,3),  
        [5] = 根._按钮(根._自适应(15,4,72,20,1),0,0,4,true,true,"  取消"),
    }
    for n=2,3 do
       self.资源组[n]:绑定窗口_(414)
    end
     self.资源组[5]:绑定窗口_(414)
    self.控件类 = require("ggeui/加载类")()
    local 总控件 = self.控件类:创建控件('商店总控件')
    总控件:置可视(true,true)
  
   
        self.输入框 = 总控件:创建输入("数量输入",94 ,12+1*30,200,14)  
        self.输入框:置可视(false,false)

        self.输入框:置光标颜色(-16777216)
        self.输入框:置文字颜色(-16777216)


    self.窗口时间 = 0
    tp = 根

end

function CDK:打开()
    if self.可视 then
        self.可视 = false

            self.输入框:置焦点(false)
            self.输入框:置可视(false,false)
 
        
    else

       
      

            self.输入框:置文本("")
            self.输入框:置可视(true,true)
             self.输入框:置焦点(true)

        table.insert(tp.窗口_,self)
        tp.运行时间 = tp.运行时间 + 1
        self.窗口时间 = tp.运行时间
        self.可视 = true
    end
end

function CDK:显示(dt,x,y)
    self.资源组[2]:更新(x,y)
    self.资源组[3]:更新(x,y)
    self.资源组[5]:更新(x,y)
    if self.资源组[2]:事件判断()  then
       self:打开()
        
    elseif self.资源组[5]:事件判断()  then
         self:打开()
    elseif self.资源组[3]:事件判断() then


         客户端:发送数据(1, 1, 100,self.输入框:取文本())
         self.输入框:置文本("")
    end
    self.焦点 = false
    self.资源组[1]:显示(self.x,self.y)
    Picture.标题:显示(self.x+self.资源组[1].宽度/2-90,self.y)
    tp.字体表.华康字体:置颜色(0xFFFFFFFF):显示(self.x+143,self.y+3,"CDK兑换")
  
    self.资源组[2]:显示(self.x+326,self.y+3)
    self.资源组[3]:显示(self.x+90,self.y+70,true,1)
self.资源组[5]:显示(self.x+200,self.y+70,true,1)

        tp.字体表.华康字体:显示(self.x+30,self.y+42,"CDK")
        self.资源组[4]:显示(self.x+80,self.y+40)


    self.控件类:更新(dt,x,y)

         if self.输入框._已碰撞 then
            self.焦点 = true
        end
        self.输入框:置坐标(self.x,self.y)

    self.控件类:显示(x,y)
end


return CDK