local 场景类_物品店铺 = class(窗口逻辑)
local insert = table.insert
local tp,zts,zts1
local tos = 引擎.取金钱颜色
local mouseb = 引擎.鼠标弹起
function 场景类_物品店铺:初始化(根)
    self.ID = 328
    self.x = 280+(全局游戏宽度-800)/2
    self.y = 100
    self.xx = 0
    self.yy = 0
    self.注释 = "物品店铺"
    tp = 根
    self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    local 按钮 = 根._按钮
    local 自适应 = 根._自适应
    local 资源= 根.资源
    self.资源组 = {
        [1]= 资源:载入('JM.dll',"网易WDF动画",0xD7ACF355),
        [2] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),
        [3] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"上间"),
        [4] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"下间"),
        [5] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"取消"),
        [6] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"购买"),

    }
    zts = tp.字体表.普通字体
    zts1 = tp.字体表.描边字体

    self.控件类 = require("ggeui/加载类")()
    local 总控件 = self.控件类:创建控件('物品店铺')
    总控件:置可视(true,true)
    self.物品数量 = 总控件:创建输入("购买数量",self.x,self.y,100,14)
    self.物品数量:置可视(false,false)
    self.物品数量:置数字模式()
    self.物品数量:置限制字数(3)
    self.物品数量:置光标颜色(-16777216)
    self.物品数量:置文字颜色(-16777216)

    for i=2,6 do
        self.资源组[i]:绑定窗口_(328)
    end
    self.窗口时间 = 0
end


function 场景类_物品店铺:打开(数据)
  if self.可视 then
    self.商会选中 = 0
    self.上一次 = 1
    self.可视 = false
    self.选择物品数量 = 0
    self.物品数据=nil
    self.购买数据=nil
    self.物品数量:置可视(false,false)
    if 数据 ~= 1 then
        tp.窗口.商会购买类.上一次=0
    end
  else
    if tp.窗口.唤兽店铺.可视 then
       tp.窗口.唤兽店铺:打开(1)
    end
    if tp.窗口.唤兽店铺.x ~= 280+(全局游戏宽度-800)/2 or tp.窗口.唤兽店铺.x~= 100 then
        self.x=tp.窗口.唤兽店铺.x
        self.y=tp.窗口.唤兽店铺.y
    end
    if  self.x > 全局游戏宽度 then
        self.x = 82+(全局游戏宽度-800)/2
    end
    self.购买数据 = 数据
    self.商会选中 = 0
    self.物品数据 = {}
    self.上一次 = 1
    local 物品格子 = tp._物品格子

      for n=1,20 do
        self.物品数据[n] = 物品格子(0,n,n,"道具店铺")
        self.物品数据[n]:置物品(nil)
        self.物品数据[n].价格=nil
        self.物品数据[n].状态=nil
            if 数据[n]~=nil then
                self.物品数据[n]:置物品(数据[n].道具)
                self.物品数据[n].价格=数据[n].价格
                self.物品数据[n].状态=数据[n].状态
            end
     end
        self.选择物品数量 = 0
        self.物品数量:置可视(false,false)
        insert(tp.窗口_,self)
        tp.运行时间 = tp.运行时间 + 1
        self.窗口时间 = tp.运行时间
        self.可视 = true
   end
 end

function 场景类_物品店铺:刷新(数据)
    self.购买数据 = 数据
    self.商会选中 = 0
    self.物品数据 = {}
    self.上一次 = 1
    local 物品格子 = tp._物品格子
      for n=1,20 do
        self.物品数据[n] = 物品格子(0,n,n,"道具店铺")
        self.物品数据[n]:置物品(nil)
        self.物品数据[n].价格=nil
        self.物品数据[n].状态=nil
            if 数据[n]~=nil then
                self.物品数据[n]:置物品(数据[n].道具)
                self.物品数据[n].价格=数据[n].价格
                self.物品数据[n].状态=数据[n].状态
            end
        end
        self.选择物品数量 = 0
        self.物品数量:置可视(false,false)
 end


function 场景类_物品店铺:显示(dt,x,y)
    self.焦点 = false
    for i=2,6 do
       self.资源组[i]:更新(x,y)
    end

    self.资源组[1]:显示(self.x,self.y)
    self.资源组[2]:显示(self.x+270,self.y+6)
    zts:置颜色(0xFFFFFFFF):显示(self.x+120, self.y+59, self.购买数据.创店日期)
    zts:置颜色(0xFFFFFFFF):显示(self.x+238, self.y+37, "0%")
    zts:置颜色(0xFF000000):显示(self.x+59, self.y+83, self.购买数据.店主名称)
    zts:置颜色(0xFF000000):显示(self.x+203, self.y+83, self.购买数据.店主id)
    zts:置颜色(0xFF000000):显示(self.x+56, self.y+395, self.购买数据.类型 .. "/" .. self.购买数据.店面)
    zts1:置颜色(4294967040):显示(self.x+15, self.y+35, self.购买数据.店名)
    zts:置颜色(tos(self.购买数据.现金)):显示(self.x+196, self.y+361, self.购买数据.现金)
    self.资源组[3]:显示(self.x+100, self.y+392,true)
    self.资源组[4]:显示(self.x+145, self.y+392,true)
    self.资源组[5]:显示(self.x+190, self.y+392,true)
    self.资源组[6]:显示(self.x+235, self.y+392,true)


      local xx = 0
      local yy = 0
        for i=1,20 do
          self.物品数据[i]:置坐标(self.x + xx * 51 + 19, self.y + yy * 51 + 105)
          self.物品数据[i]:显示(dt,x,y,self.鼠标)
          if self.物品数据[i].物品 ~= nil and self.物品数据[i].焦点 then
            tp.提示:道具行囊(x,y,self.物品数据[i].物品,true)
                if mouseb(0) then
                    self.商会选中=i
                    if self.上一次 ==self.商会选中   then
                        if self.物品数据[self.商会选中].物品.数量~=nil  and self.选择物品数量<self.物品数据[self.商会选中].物品.数量 then
                        self.选择物品数量=self.选择物品数量+1
                        else
                            self.选择物品数量=1
                            self.物品数据[self.商会选中].确定 = true
                        end
                    else
                        self.物品数据[self.上一次].确定 = false
                        self.物品数据[i].确定 = true
                        self.上一次 = i
                        self.选择物品数量=1
                    end
                    self.物品数量:置文本(self.选择物品数量)
                    self.物品数量:置可视(true,true)
                elseif  mouseb(1) then
                    if self.上一次 ==self.商会选中 and self.选择物品数量 >1 then
                        self.选择物品数量=self.选择物品数量-1
                    end
                    self.物品数量:置文本(self.选择物品数量)
                    self.物品数量:置可视(true,true)
                end

          end
          xx = xx + 1
          if xx == 5 then
            xx = 0
            yy = yy + 1
          end
        end

        if self.商会选中 ~= 0 then
            local s = tonumber(self.物品数量:取文本())
            local v
            if self.物品数据[self.商会选中].物品 and not self.物品数据[self.商会选中].物品.数量 then
                v=1
            elseif self.物品数据[self.商会选中].物品 then
                v= tonumber(self.物品数据[self.商会选中].物品.数量)
            end
            if s == nil or s <= 0 then
                s = 1
            end
            if v == nil then
                v = 1
            end
            if s > v then
                if self.物品数据[self.商会选中].物品.数量 == 0 then
                    self.物品数量:置文本(1)
                else
                    self.物品数量:置文本(v)
                end
            end
            if type(tonumber(self.物品数量:取文本()))~= "number" then
                self.物品数量:置文本(1)
            end
            self.物品数量:置坐标(self.x-82,self.y+232)
            zts:置颜色(tos(self.物品数据[self.商会选中].价格)):显示(self.x + 66, self.y + 332,self.物品数据[self.商会选中].价格)
            zts:置颜色(tos(self.物品数据[self.商会选中].价格)):显示(self.x + 66, self.y + 361,self.物品数据[self.商会选中].价格*self.物品数量:取文本())
        end

        if self.资源组[2]:事件判断() or self.资源组[5]:事件判断() then
           self:打开()
        elseif self.资源组[3]:事件判断() then
            客户端:发送数据(self.购买数据.类型, 131, 36, self.购买数据.商铺id, 1)
        elseif self.资源组[4]:事件判断() then
            客户端:发送数据(self.购买数据.类型, 132, 36, self.购买数据.商铺id, 1)
        elseif self.资源组[6]:事件判断() then
            if self.商会选中 == 0 then
                tp.提示:写入("#y/请先选中要购买的商品")
            else
                客户端:发送数据(self.购买数据.类型, self.商会选中, 38, self.购买数据.商铺id, self.物品数量:取文本())
            end
        end

    self.控件类:更新(dt,x,y)
    self.控件类:显示(x,y)
    if  self.物品数量._已碰撞 then
        self.焦点 = true
    end
end




return 场景类_物品店铺



