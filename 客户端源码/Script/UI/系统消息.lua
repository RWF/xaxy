--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--

local 场景类_系统消息 = class()
local tp,zts
local insert = table.insert

function 场景类_系统消息:初始化(根)
	self.ID = 115
	self.x = 211+(全局游戏宽度-800)/2
	self.y = 154
	self.xx = 0
	self.yy = 0
	self.注释 = "系统消息"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 资源:载入('JM.dll',"网易WDF动画",0x6209D9CA),
		[2] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),

	}
	self.介绍文本 = 根._丰富文本(305,90,根.字体表.普通字体)
	   self.资源组[2]:绑定窗口_(115)
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体
end

function 场景类_系统消息:打开(数据)
	if  self.可视 then
		self.介绍文本:清空()
		self.可视 = false
	else
								                    if  self.x > 全局游戏宽度 then
self.x = 211+(全局游戏宽度-800)/2
    end
		insert(tp.窗口_,self)
		self.数据 = 数据
		self.介绍文本:清空()
		self.介绍文本:添加文本(self.数据.内容)
		tp.运行时间 = tp.运行时间 + 3
		self.窗口时间 = tp.运行时间
		self.可视 = true
	end
end

function 场景类_系统消息:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	if self.资源组[2]:事件判断() then
	   self:打开()
	end

	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x+341,self.y+3)
	self.介绍文本:显示(self.x+30,self.y+85)
  	zts:置颜色(0xFF000000)
  	zts:显示(self.x+88,self.y+43,self.数据.时间)
    zts:置颜色(0xFFFFFFFF)

end


function 场景类_系统消息:检查点(x,y)
	if self.资源组[1]:是否选中(x,y)  then
		return true
	end
end

function 场景类_系统消息:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	self.窗口时间 = tp.运行时间
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end


function 场景类_系统消息:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end


return 场景类_系统消息