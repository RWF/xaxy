--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:48
--======================================================================--
local 场景类_技能学习 = class(窗口逻辑)

local floor = math.floor
local bw = require("gge包围盒")(0,0,164,37)
local box = 引擎.画矩形
local tp,zys
local ARGB = ARGB
local insert = table.insert

function 场景类_技能学习:初始化(根)
	self.ID = 22
	self.x = 218+(全局游戏宽度-800)/2
	self.y = 83
	self.xx = 0
	self.yy = 0
	self.注释 = "技能学习"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 资源 = 根.资源
		local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 资源:载入('JM.dll',"网易WDF动画",0x9ED74AA6),
		[2] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xF11233BB),0,0,4,true,true),
		[3] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFD3D61F2),0,0,4,true,true),
		[4] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x09217E13),0,0,4,true,true),
		[5] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFD3D61F2),0,0,4,true,true),
		[6] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x09217E13),0,0,4,true,true),
		[7] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x2BD1DEF7),0,0,4,true,true,"学习"),
		[8] = 按钮.创建(自适应.创建(12,4,72,20,1,3),0,0,4,true,true,"学习10次"),
	}
	for n=2,8 do
		self.资源组[n]:绑定窗口_(22)
	end
	self.介绍文本 = 根._丰富文本(150,150,根.字体表.普通字体)
	self.窗口时间 = 0
	self.选中 = nil
	self.选中1 = nil
	self.选中包含技能加入 = nil
	self.师门技能 = nil
	self.包含技能 = nil
	self.本次需求 = nil
	self.刷新文本 = false
	tp = 根
	zys = 资源
end



function 场景类_技能学习:打开(数据)
	if self.可视 then
		self.介绍文本:清空()
		self.选中师门技能 = nil
		self.选中包含技能 = nil
		self.选中包含技能加入 = nil
		self.师门技能 = nil
		self.包含技能 = nil
		self.刷新文本 = false
		self.可视 = false
	else
								                    if  self.x > 全局游戏宽度 then
self.x = 218+(全局游戏宽度-800)/2
    end
		self.数据 = 数据
		self.数据.本次需求 = nil
		insert(tp.窗口_,self)
		self.师门技能 = {}
		self.包含技能 = {}
		self.选中 = 0
		self.加入 = 0
		tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
		self.可视 = true
	end
end


function 场景类_技能学习:刷新(数据)
 self.数据 = 数据
 if self.数据.师门技能[self.选中] then
 self.数据.本次需求 =计算技能数据(self.数据.师门技能[self.选中].等级+1)
 end
end
function 场景类_技能学习:显示(dt,x,y)
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y,self.选中 ~= 0 and self.加入 > 0)
	self.资源组[4]:更新(x,y,self.选中 ~= 0 and self.加入 < #self.数据.师门技能[self.选中].包含技能 - 4)
	self.资源组[5]:更新(x,y,false)
	self.资源组[6]:更新(x,y,false)
	self.资源组[7]:更新(x,y,self.选中 ~= 0 and self.数据.师门技能[self.选中].等级 < 180)
	self.资源组[8]:更新(x,y,self.选中 ~= 0 and self.数据.师门技能[self.选中].等级 < 170)
	self.焦点 = false
	if self.鼠标 then
		if self.资源组[2]:事件判断() then
			self:打开()
			return false
		elseif self.资源组[3]:事件判断() then
			self.加入 = self.加入 - 1
		elseif self.资源组[4]:事件判断() then
			self.加入 = self.加入 + 1
		elseif self.资源组[7]:事件判断() then
           客户端:发送数据(self.选中,1,7,1)
        elseif self.资源组[8]:事件判断() then
           客户端:发送数据(self.选中,1,72,1)
		end
	end
	-- 显示
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x + 364,self.y + 6)
	self.资源组[3]:显示(self.x + 355,self.y + 40)
	self.资源组[4]:显示(self.x + 355,self.y + 168)
	self.资源组[5]:显示(self.x + 354,self.y + 202)
	self.资源组[6]:显示(self.x + 354,self.y + 330)
	self.资源组[7]:显示(self.x + 124,self.y + 440,true)
	self.资源组[8]:显示(self.x + 224,self.y + 440,true)
	local fonts = tp.字体表.普通字体
	fonts:置颜色(ARGB(255,0,0,0))
	if self.数据.门派 ~= "无" then
		for n=1,7 do
			bw:置坐标(self.x+18,self.y+35+n*40-5)
			if self.选中 ~= n then
				local x2 =SkillData[self.数据.师门技能[n].名称]
				self.数据.师门技能[n].介绍 = x2.介绍
				if self.师门技能[n] == nil then
					self.师门技能[n] = zys:载入(x2.文件,"网易WDF动画",x2.小图标)
				end
				if bw:检查点(x,y) and not tp.消息栏焦点 and self.鼠标 then
					box(self.x+16,self.y+35+n*40-9,self.x+181,self.y+72+n*40-9,ARGB(255,201,207,109))
					if 引擎.鼠标弹起(0) then
						self.选中 = n
						self.加入 = 0
						self.包含技能 = {}

						if #self.数据.师门技能[self.选中].包含技能 > 0 then
							for i=1,#self.数据.师门技能[self.选中].包含技能 do
								local x1 =   SkillData[self.数据.师门技能[self.选中].包含技能[i].名称]

                                 self.包含技能[i] =zys:载入(x1.文件,"网易WDF动画",x1.小图标)
							end
						end
						self.介绍文本:清空()
						self.介绍文本:添加文本("#N/【介绍】"..self.数据.师门技能[self.选中].介绍)
						self.介绍文本:添加文本("#N/【等级】"..self.数据.师门技能[self.选中].等级)
						self.数据.本次需求 =计算技能数据(self.数据.师门技能[self.选中].等级+1)
					end
					self.焦点 = true
				end
			else
				local ys = ARGB(255,91,90,219)
				if bw:检查点(x,y) then
					ys = ARGB(255,108,110,180)
					self.焦点 = true
				end
				box(self.x+16,self.y+35+n*40-9,self.x+181,self.y+72+n*40-9,ys)
			end

			self.师门技能[n]:显示(self.x+18,self.y+35+n*40-5)
			fonts:显示(self.x + 18+35,self.y + 35 +n*40+2,self.数据.师门技能[n].名称)
			fonts:显示(self.x + 18+112,self.y + 35 +n*40+2 ,self.数据.师门技能[n].等级.."/180")
		end
		-- 技能信息类
		if self.选中 ~= nil and self.选中 ~= 0 then
			if #self.数据.师门技能[self.选中].包含技能 > 0 then
				for i=1,4 do
					if self.包含技能[i] ~= nil then
						self.包含技能[i+self.加入]:显示(self.x+208,self.y+i*30+37)
						fonts:显示(self.x+235,self.y+i*30+42,self.数据.师门技能[self.选中].包含技能[i+self.加入].名称)
					end
				end
			end
			fonts:显示(self.x + 83,self.y + 362,self.数据.当前经验)
			fonts:显示(self.x + 83,self.y + 385,self.数据.金钱)
			fonts:显示(self.x + 83,self.y + 407,self.数据.存银)
			if self.数据.本次需求 ~= nil then
				fonts:显示(self.x + 290,self.y + 362,self.数据.本次需求.经验)
				fonts:显示(self.x + 290,self.y + 385,self.数据.本次需求.金钱)
			else
				fonts:显示(self.x + 290,self.y + 362,"--------")
				fonts:显示(self.x + 290,self.y + 385,"--------")
			end
			fonts:显示(self.x + 290,self.y + 407,self.数据.储备)
		end
	end
	self.介绍文本:显示(self.x+205,self.y+205)
end




return 场景类_技能学习