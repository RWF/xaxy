--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:48
--======================================================================--
local 成长之路 = class(窗口逻辑)
local text  ={"体质","魔力","力量","耐力","敏捷"}
function 成长之路:初始化(根)
    self.ID = 400
    self.x = 224
    self.y = 100
    self.xx = 0
    self.yy = 0
    self.注释 = "成长之路"
    self.可视 = false
    self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    local 资源 = 根.资源
    local 按钮 = 根._按钮
    local 自适应 = 根._自适应
    self.资源组 = {
        [1] = 自适应.创建(99,1,252,216,3,9),
        [2] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
        [3] = 根._按钮(根._自适应(15,4,72,20,1),0,0,4,true,true,"  确定"),
        [4] = 自适应.创建(8,1,80,19,1,3),  
    }
    for n=2,3 do
       self.资源组[n]:绑定窗口_(400)
    end
    
    self.控件类 = require("ggeui/加载类")()
    local 总控件 = self.控件类:创建控件('商店总控件')
    总控件:置可视(true,true)
    self.输入框={}
    for i=1,5 do
        self.输入框[i] = 总控件:创建输入("数量输入",104 ,12+i*30,86,14)  
        self.输入框[i]:置可视(false,false)
        self.输入框[i]:置限制字数(3)
        self.输入框[i]:置数字模式()
        self.输入框[i]:置光标颜色(-16777216)
        self.输入框[i]:置文字颜色(-16777216)
    end

    self.窗口时间 = 0
    tp = 根

end

function 成长之路:打开(data)
    if self.可视 then
        self.可视 = false
        for i=1,5 do
            self.输入框[i]:置焦点(false)
            self.输入框[i]:置可视(false,false)
        end
        
    else

        self.number  = data[2]
        self.name=data[3]
         for k,v in ipairs(text) do
            self.输入框[k]:置文本(data[1][v])
            self.输入框[k]:置可视(true,true)
             self.输入框[k]:置焦点(true)
         end
        table.insert(tp.窗口_,self)
        tp.运行时间 = tp.运行时间 + 1
        self.窗口时间 = tp.运行时间
        self.可视 = true
    end
end

function 成长之路:显示(dt,x,y)
    self.资源组[2]:更新(x,y)
    self.资源组[3]:更新(x,y)
    if self.资源组[2]:事件判断() then
        self:打开()
    elseif self.资源组[3]:事件判断() then
        local TempText=""
        for i=1,5 do
           if self.输入框[i]:取文本() == "" then
               tp.提示:写入("#Y/错误的数值,请重新输入")
                return 
           end
           TempText =  i==5 and  TempText..self.输入框[i]:取文本() or TempText..self.输入框[i]:取文本().."*"      
        end

        客户端:发送数据(self.number, 31, 5,TempText)
    end
    self.焦点 = false
    self.资源组[1]:显示(self.x,self.y)
    Picture.标题:显示(self.x+self.资源组[1].宽度/2-90,self.y)
    tp.字体表.华康字体:置颜色(0xFFFFFFFF):显示(self.x+88,self.y+3,"名称："..self.name)
  
    self.资源组[2]:显示(self.x+226,self.y+3)
    self.资源组[3]:显示(self.x+100,self.y+187,true,1)
    for k,v in ipairs(text) do
        tp.字体表.华康字体:显示(self.x+50,self.y+10+k*30,v)
        self.资源组[4]:显示(self.x+100,self.y+10+k*30,v)
    end

    self.控件类:更新(dt,x,y)
    for i=1,5 do
         if self.输入框[i]._已碰撞 then
            self.焦点 = true
        end
        self.输入框[i]:置坐标(self.x,self.y)
    end
    self.控件类:显示(x,y)
end


return 成长之路