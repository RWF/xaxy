--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:48
--======================================================================--
local 查看装备 = class(窗口逻辑)

local floor = math.floor
local tp,zts,zts1
local insert = table.insert


function 查看装备:初始化(根)
    self.ID = 236
    self.xx = 0
    self.x= 312
    self.y=150
    self.yy = 0
    self.注释 = "查看装备"
    self.可视 = false
    self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    local 资源 = 根.资源
    local 按钮 = 根._按钮
    local 自适应 = 根._自适应
    self.资源组 = {
        [1] = 资源:载入('JM.dll',"网易WDF动画",0x6951A4F4),

        [2] = 按钮(资源:载入('JM.dll',"网易WDF动画",0xF11233BB),0,0,4,true,true),
        [4] = 资源:载入('JM.dll',"网易WDF动画",0x841CBD61),
        [5] = 资源:载入('JM.dll',"网易WDF动画",0x9912E8B9),
        [6] = 资源:载入('JM.dll',"网易WDF动画",0x5795605E),
        [7] = 资源:载入('JM.dll',"网易WDF动画",0xEBD8985D),
        [8] = 资源:载入('JM.dll',"网易WDF动画",0x21246323),
        [9] = 资源:载入('JM.dll',"网易WDF动画",0x21246323),
    }
    self.资源组[2]:绑定窗口_(236)
    self.物品 = {}
    local 格子 = 根._物品格子
    for i=21,26 do
        self.物品[i] = 格子(0,0,i,"查看装备",根.底图)
    end
    tp = 根
    zts = tp.字体表.普通字体
    self.窗口时间 = 0
end

function 查看装备:打开(数据)
    if self.可视 then
        for i=21,26 do
            self.物品[i]:置物品(nil)
        end
        self.可视 = false
        self.资源组[99]=nil
        self.资源组[100]=nil
    else
        insert(tp.窗口_,self)
        self.数据 = 数据
        for i=21,26 do
            if self.数据[i] ~= nil and self.数据[i] ~= 0 then
                self.物品[i]:置物品(self.数据[i])
            end
        end



        if self.数据.武器数据.类别 ~= 0 and self.数据.武器数据.类别 ~= "" and self.数据.武器数据.类别 ~= nil then
                local v = self.数据.武器数据.类别
                if self.数据.武器数据.名称 == "龙鸣寒水" or self.数据.武器数据.名称 == "非攻" then
                    v = "弓弩1"
                end
                local 资源 = 取模型(self.数据.造型,v)
                self.资源组[99] = tp.资源:载入(资源.资源,"网易WDF动画",资源.静立)
                local m = tp:qfjmc(self.数据.武器数据.类别,self.数据.武器数据.等级,self.数据.武器数据.名称)
                local n = 取模型(m.."_"..self.数据.造型)
                self.资源组[100] = tp.资源:载入(n.资源,"网易WDF动画",n.静立)
                self.资源组[100]:置方向(1)
            else
                    local n = 取模型(self.数据.造型)
                    self.资源组[99] = tp.资源:载入(n.资源,"网易WDF动画",n.静立)
            end
            self.资源组[99]:置方向(1)


        tp.运行时间 = tp.运行时间 + 1
        self.窗口时间 = tp.运行时间
        self.可视 = true
    end
end



function 查看装备:显示(dt,x,y)
    self.焦点 = false
    self.资源组[2]:更新(x,y)
    if self.资源组[2]:事件判断() then
        self:打开()
        return false
    end
    self.资源组[1]:显示(self.x,self.y)
    self.资源组[2]:显示(self.x+227,self.y+3)
    local xx = 0
    local yy = 1
    local xs = 0
    for i=21,26 do
        if i == 5 then
            xs = 27
        end
        xx = xx + 1
        self.物品[i]:置坐标(self.x + xx * 59*3-163+xs,self.y + yy * 57-5)
        self.物品[i]:显示(dt,x,y,self.鼠标,nil)
        if self.物品[i].物品 ~= nil and self.物品[i].焦点 then
            tp.提示:道具行囊(x,y,self.物品[i].物品,true)
        end
        if xx >= 2 then
            xx = 0
            yy = yy + 1
        end
    end
    zts:置颜色(0xFF000000):显示(self.x+110,self.y+266,self.数据.名称)
    zts:置颜色(0xFF000000):显示(self.x+110,self.y+266+22,self.数据.id)
    zts:置颜色(0xFF000000):显示(self.x+110,self.y+266+44,self.数据.等级)
        tp.影子:显示(self.x + 132,self.y + 180)
        self.资源组[99]:更新(dt)
        self.资源组[99]:显示(self.x + 132,self.y + 180)

        if self.资源组[100] ~= nil then
        self.资源组[100]:更新(dt)
        self.资源组[100]:显示(self.x + 132,self.y + 180)
        end
end




return 查看装备