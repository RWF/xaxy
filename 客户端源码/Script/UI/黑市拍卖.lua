-- @Author: 作者QQ414628710
-- @Date:   2022-01-15 20:06:44
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-05-19 01:21:39

local 古董拍卖类 = class()

local tp,zts,zts1
local insert = table.insert
local xxx = 0
local yyy = 0
local sts = {"单价","数量","总额","现金"}
local tos = 引擎.取金钱颜色
local mouseb = 引擎.鼠标弹起
local bw = require("gge包围盒")(0,0,120,20)
local tp
local 对象组={}
local 字体
function 古董拍卖类:初始化(根)
    self.ID = 422
    self.x = 166
    self.y = 100
    self.xx = 0
    self.yy = 0
    self.注释 = "古董拍卖类"
    self.可视 = false
    self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    self.右键关闭=1
    local 按钮 = 根._按钮
    local 自适应 = 根._自适应
    self.资源组 = {
        [2] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"开始拍卖"),
        [1] = Picture.黑市拍卖,
        [3] = Picture.黑市底图,
        [4] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),
    }
    tp=根

 self.古董盘子=tp.资源:载入("MAX.7z","网易WDF动画","F5EE7A01")
 self.绿色流动=tp.资源:载入("MAX.7z","网易WDF动画","23485855")
 self.绿色闪烁=tp.资源:载入("MAX.7z","网易WDF动画","2E7810F8")
self:重置对象()
  字体=tp.字体表.描边字体 --tp.字体表.普通字体
end

function 古董拍卖类:打开(内容)


    if self.可视 then

        self.选中人物 = 0
        self.可视 = false

    else
        

        tp.运行时间 = tp.运行时间 + 1
        self.发言记录={}
        self.中奖记录=0
        self.窗口时间 = tp.运行时间
        self.流程开关=false
        self.可视 = true
    
      -- self.data = 内容
      -- print_r(内容)

        -- local s =  ModelData[内容[1]]
        -- self.人物头像 = tp.资源:载入(s.头像资源,"网易WDF动画",s.中头像)
        
        tp.提示:写入("#Y请先选择一位竞拍者作为专家评判")
        self.专家数据={}
        self.专家选择=false

 
    self:置随机对象()

insert(tp.窗口_,self)
    end
end

function 古董拍卖类:置随机对象()
    self:重置对象()
 self.拍卖组={}
 for n=1,#对象组 do

     if self.专家数据~=nil and self.专家数据.名称~=nil and 对象组[n].名称==self.专家数据.名称 then
         table.remove(对象组,n)
    end


     end

 for n=1,12 do
  
     local 序列=取随机数(1,#对象组)

     self.拍卖组[n]={名称=对象组[序列][1],
     头像=tp.资源:载入("MAX.7z","网易WDF动画",对象组[序列][2]),
     资源=对象组[序列][2],
     喊话=require("script/System/喊话").创建(tp)}
     table.remove(对象组,序列)

     end
end
function 古董拍卖类:重置对象()
 对象组={
{"江湖奸商","4EE3D815"},
{"古董商人","4EE3D815"},
{"吹牛王","5669B9CB"},
{"毛毛虫","5786AD5A"},
{"沙僧","5CF6E3DB"},
{"大宝","6E68B9A5"},
{"黑熊怪","79CEA1EA"},
{"罗道人","809D9F14"},
{"牛大胆","809D9F14"},
{"大蝙蝠","90ECA422"},
{"陈妈妈","9A2DA270"},
{"袁天罡","A2F065F2"},
{"马面","A5C7D24B"},
{"雷鸟精","A7EE2B7B"},
{"玉面狐狸","A971F674"},
{"龟丞相","AA3785D1"},
{"王母娘娘","C2C20ABC"},
{"徒弟公公","C86B52A8"},
{"牛头","CDD130C4"},
{"文老伯","D204CB7C"},
{"太白金星","D7E7B76B"},
{"孙悟空","DDEEE075"},
{"郑镖头","E23A99ED"},
{"唐僧","E7E0C5B5"},
{"至尊宝","EE123BF0"},
{"猪八戒","EF9CEB2F"},
{"虾兵","FF844C07"},
{"酒肉和尚","01265B89"},



}
end

function 古董拍卖类:执行流程(内容)

 --table.print(内容)
 self.流程来源=内容
 self.流程数据={发言={},价格={},起始=全局时间,次数=0,上限=1,中奖=0,最后=0}
 if 内容.类型==1 then

     self.专家数据.喊话:写入1(self.专家数据.x,self.专家数据.y,"#Y假的，一看就知道是假的，看我一锤下去让它早登极乐")
      self.流程开关=false
         self.专家选择=false
    elseif 内容.类型==2 then
     tp.提示:写入("#Y遇到强盗,损失古董")
      self.流程开关=false
         self.专家选择=false
    else

     if 内容.类型==3 then

          self.流程数据.发言[1]=取随机数(1,12)
          self.拍卖组[self.流程数据.发言[1]].喊话:写入1(self.拍卖组[self.流程数据.发言[1]].x,self.拍卖组[self.流程数据.发言[1]].y,"#Y我出价8888",200)
          self.流程开关=true
          self.发言记录[self.流程数据.发言[1]]=1
          self.流程数据.中奖=self.流程数据.发言[1]
        elseif 内容.类型==4 then
         local 组={}
         for n=1,12 do

             组[n]=n

             end

         for n=1,#内容.动作 do

             local 序列=取随机数(1,#组)
             self.流程数据.发言[n]=组[序列]
             table.remove(组,序列)


             end
          self.拍卖组[self.流程数据.发言[1]].喊话:写入1(self.拍卖组[self.流程数据.发言[1]].x,self.拍卖组[self.流程数据.发言[1]].y,"#Y我出价"..内容.动作[1],200)
          self.流程开关=true
          self.发言记录[self.流程数据.发言[1]]=1
          self.流程数据.中奖=self.流程数据.发言[#self.流程数据.发言]
          self.流程数据.上限=#内容.动作


         end



     end


end
function 古董拍卖类:流程更新(dt,x,y)

 if 全局时间-self.流程数据.起始>=3 then
     self.流程数据.次数=self.流程数据.次数+1
     if self.流程数据.次数==self.流程数据.上限 and self.流程数据.最后==0 then


          self.专家数据.喊话:写入1(self.专家数据.x,self.专家数据.y,"#Y哇塞，你可是捡到宝贝了，这件宝贝的价值可是高达"..取随机数(9999999,99999999).."两呢！")
          self.流程数据.最后=1
          self.流程数据.起始=全局时间
          self.中奖记录=self.流程数据.中奖
        elseif  self.流程数据.最后==1 then
         self.拍卖组[self.流程数据.中奖].喊话:写入1(self.拍卖组[self.流程数据.中奖].x,self.拍卖组[self.流程数据.中奖].y,"#Y哈哈，天上掉馅饼了，今晚天香阁我请客咯！")
         self.流程数据.最后=2
         self.流程数据.起始=全局时间
       elseif self.流程数据.最后==2 then
         self.流程开关=false
         self.专家选择=false
         tp.提示:写入("#Y你获得了"..self.流程来源.银子.."两银子")

         tp.窗口.聊天框类:添加文本("#xt/#W/你获得了"..self.流程来源.银子.."两银子")
        
         --
        elseif self.流程数据.次数<self.流程数据.上限 then
         local 序列=self.流程数据.发言[self.流程数据.次数+1]
         self.拍卖组[序列].喊话:写入1(self.拍卖组[序列].x,self.拍卖组[序列].y,"#Y我出价"..self.流程来源.动作[self.流程数据.次数+1],200)
         self.流程数据.起始=全局时间
         self.发言记录[序列]=1




         end



     end
 end
function 古董拍卖类:显示(dt,x,y)
 self.资源组[1]:显示(self.x,self.y)
 self.资源组[2]:显示(self.x+160,self.y+334)
 self.资源组[2]:更新(x,y,self.专家数据.头像~=nil and self.流程开关==false)
 self.资源组[4]:显示(self.x+359,self.y+5)
 self.资源组[4]:更新(x,y)
 self.绿色流动:更新(dt)
 self.绿色闪烁:更新(dt)
 if self.流程开关 then

     self:流程更新(dt,x,y)

     end
 if self.资源组[4]:事件判断() then

      self:打开()
  elseif self.资源组[2]:事件判断() then

       客户端:发送数据(5,37,5)
      
        self.发言记录={}
        self.中奖记录=0
        
        self:置随机对象()


     end
  
 字体:置颜色(0xFFFFFFFF)
 字体:显示(self.x+160,self.y+2,"古玩拍卖")
 local 画线 = 引擎.画线
 local c = -1
    画线(self.x+20,self.y+28,self.x+360,self.y+28,c)--上
    画线(self.x+20,self.y+28,self.x+20,self.y+230)--下
    画线(self.x+360,self.y+28,self.x+360,self.y+230,c)--左
    画线(self.x+20,self.y+230,self.x+360,self.y+230,c)


    画线(self.x+20,self.y+260,self.x+360,self.y+260,c)--上
    画线(self.x+20,self.y+260,self.x+20,self.y+360)--下
    画线(self.x+360,self.y+260,self.x+360,self.y+360,c)--左
    画线(self.x+20,self.y+360,self.x+360,self.y+360,c)





 local 字体=tp.字体表.普通字体 --tp.字体表.普通字体
 字体:置颜色(0xFFFFFFFF)

    --画线(self.x2,self.y,self.x2,self.y2,c)
 local 计数=0
 for n=1,3 do

      for i=1,4 do
         计数=计数+1
         self.资源组[3]:显示(self.x+80*i-30,self.y+65*n-30)
         self.拍卖组[计数].头像:显示(self.x+80*i-30+2,self.y+65*n-30+2)
         self.拍卖组[计数].x= self.x+80*i-30
         self.拍卖组[计数].y=self.y+65*n-30
         字体:显示(self.x+80*i-30,self.y+65*n-30+50,self.拍卖组[计数].名称)
         if self.发言记录[计数]~=nil then

             self.绿色流动:显示(self.x+80*i-5,self.y+65*n-5)
             end
         if self.中奖记录==计数 then


             self.绿色闪烁:显示(self.x+80*i-5,self.y+65*n-5)
             end


         if self.拍卖组[计数].头像:是否选中(x,y) and 引擎.鼠标弹起(左键) and self.专家选择==false and self.流程开关==false then

             self.专家数据.头像=tp.资源:载入("MAX.7z","网易WDF动画",self.拍卖组[计数].资源)
             self.专家数据.名称=self.拍卖组[计数].名称
             self.专家数据.喊话=require("script/System/喊话").创建(tp)
             self.专家选择=true


             end


         end


     end
 local 计数=0
 for n=1,3 do

      for i=1,4 do
         计数=计数+1

         self.拍卖组[计数].喊话:显示(self.x+80*i-30+2,self.y+65*n-30+2)




         end


     end

 self.资源组[3]:显示(self.x+50,self.y+280)
 -- self.人物头像:显示(self.x+52,self.y+282)
 字体:显示(self.x+51,self.y+333,self.data[2])
 self.资源组[3]:显示(self.x+170,self.y+280)
 --if self.流程开关 then

     self.古董盘子:显示(self.x+166,self.y+282)

    -- end
 self.资源组[3]:显示(self.x+290,self.y+280)
 if self.专家数据.头像~=nil then

     self.专家数据.头像:显示(self.x+292,self.y+282)
     字体:显示(self.x+291,self.y+333,self.专家数据.名称)
     self.专家数据.喊话:显示(self.x+292,self.y+282)
     self.专家数据.x= self.x+292
     self.专家数据.y=self.y+282


     end
end


function 古董拍卖类:检查点(x,y)
    if self.可视 and self.资源组[1]:是否选中(x,y)  then
        return true
    else
        return false
    end
end
function 古董拍卖类:初始移动(x,y)
    tp.运行时间 = tp.运行时间 + 1
    if not tp.消息栏焦点 then
        self.窗口时间 = tp.运行时间
    end
    if not self.焦点 then
        tp.移动窗口 = true
    end
    if self.鼠标 and  not self.焦点 then
        self.xx = x - self.x
        self.yy = y - self.y
    end
end

function 古董拍卖类:开始移动(x,y)
    if self.鼠标 then
        self.x = x - self.xx
        self.y = y - self.yy

    end
end



return 古董拍卖类