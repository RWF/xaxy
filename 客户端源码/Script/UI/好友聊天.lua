-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-29 00:42:36

local 场景类_好友聊天 = class(窗口逻辑)
local insert = table.insert
local remove = table.remove
local min = math.min
local ceil = math.ceil
local floor = math.floor
local tp,zts,zts1
-- local bw = require("gge包围盒")(0,0,270,53)
function 场景类_好友聊天:初始化(根)
    self.ID = 318
    self.x = 100+(全局游戏宽度-800)/2
    self.y = 65
    self.xx = 0
    self.yy = 0
    self.注释 = "好友聊天"
    self.可视 = false
    self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    local 按钮 = 根._按钮
    local 自适应 = 根._自适应
    local  资源= 根.资源
    self.资源组 = {
        [1] = 根.资源:载入("好友聊天.png","加密图片"),
    [4] = 按钮.创建(自适应.创建(16,4,72,22,1,3),0,0,4,true,true,"发送消息"),
    [5] = 按钮.创建(自适应.创建(16,4,60,22,1,3),0,0,4,true,true,"下一条"),
    [6] = 按钮.创建(自适应.创建(20,4,18,19,4,3),0,0,4,true,true),
    [7] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),
    [8] = 根._滑块.创建(自适应.创建(11,4,15,40,2,3,nil),4,14,168,2),
     [9] = 按钮.创建(自适应.创建(16,4,72,22,1,3),0,0,4,true,true,"加为好友"),
    }
    for i=4,7 do
          self.资源组[i]:绑定窗口_(318)
    end

  self.控件类 = require("ggeui/加载类")()
  local 总控件 = self.控件类:创建控件('创建控件')
  总控件:置可视(true,true)
  self.输入 =  总控件:创建输入("数量输入",self.x,self.y ,270,14)
  self.输入:置可视(true,true)
  self.输入:置限制字数(120)
  self.输入:置光标颜色(0xFFFF0000)
  self.输入:置文字颜色(0xFF000000)
    self.输入:屏蔽快捷键(true)
    self.丰富文本 = 根._丰富文本(261,192,根.字体表.普通字体)
    self.丰富文本:置默认颜色(0xFF000000)
        for n=0,119 do
        self.丰富文本:添加元素(n,根.包子表情动画[n])
     end

    self.翻页 =false
    self.窗口时间 = 0
    tp = 根
    zts = tp.字体表.普通字体
    zts1 = tp.字体表.描边字体

end




function 场景类_好友聊天:打开(内容)
    self.介绍加入=0
  if self.可视 then

        self.可视 = false
          self.丰富文本:清空()
                self.输入:置焦点(false)
        self.输入:置可视(false,false)
  else
            self.输入:置焦点(true)
        self.输入:置可视(true,true)
       self.数据=内容
       self.丰富文本:清空()
        if f函数.文件是否存在(程序目录..[[data/]]..tp.场景.人物.数据.id) then
        if f函数.文件是否存在(程序目录..[[data/]]..tp.场景.人物.数据.id..[[/]]..内容.id) then
        self.丰富文本:添加文本(__gge.readfile(程序目录..[[data\]]..tp.场景.人物.数据.id..[[\]]..内容.id..[[\记录.txt]]))
        else
        os.execute("md "..程序目录..[[data\]]..tp.场景.人物.数据.id..[[\]]..内容.id)
        local file =io.open(程序目录..[[data\]]..tp.场景.人物.数据.id..[[\]]..内容.id..[[\记录.txt]],"w")
        file:write("")
        file:close()
        end
        else
        os.execute("md "..程序目录..[[data\]]..tp.场景.人物.数据.id)
        os.execute("md "..程序目录..[[data\]]..tp.场景.人物.数据.id..[[\]]..内容.id)
        local file =io.open(程序目录..[[data\]]..tp.场景.人物.数据.id..[[\]]..内容.id..[[\记录.txt]],"w")
        file:write("")
        file:close()
        end
      self.资源组[2]=tp.资源:载入(self.数据.造型.."头像.png","加密图片")
        for i=1,#self.丰富文本.显示表 - 12 do
    self.丰富文本:滚动(1)
  end
        if  self.x > 全局游戏宽度 then
        self.x = 82+(全局游戏宽度-800)/2
        end
        self.资源组[8]:置起始点(0)
        insert(tp.窗口_,self)
        tp.运行时间 = tp.运行时间 + 1
        self.窗口时间 = tp.运行时间
        self.可视 = true
  end
 end
 function 场景类_好友聊天:刷新(内容)

      self.丰富文本:添加文本("#R"..内容.信息)
      self.丰富文本:添加文本("#H"..内容.内容)
      local 临时内容=__gge.readfile(程序目录..[[data\]]..tp.场景.人物.数据.id..[[\]]..内容.id..[[\记录.txt]])
      临时内容=临时内容.."\n".."#R"..内容.信息.."\n".."#H"..内容.内容
      local file =io.open(程序目录..[[data\]]..tp.场景.人物.数据.id..[[\]]..内容.id..[[\记录.txt]],"w")
      file:write(临时内容)
      file:close()


  end
function 场景类_好友聊天:显示(dt,x,y)
    self.焦点 = false

    self.资源组[1]:显示(self.x,self.y)
   self.资源组[2]:显示(self.x+354,self.y+103)
   self.资源组[4]:显示(self.x+215,self.y+413)
    self.资源组[4]:更新(x,y)
self.资源组[9]:更新(x,y)
  self.资源组[5]:显示(self.x+235,self.y+305)
  self.资源组[9]:显示(self.x+165,self.y+305)
  self.资源组[6]:显示(self.x+280,self.y+86)
  self.资源组[7]:显示(self.x+280,self.y+273)

  self.资源组[5]:更新(x,y,消息闪烁)
  self.资源组[6]:更新(x,y,self.介绍加入 > 0)
  self.资源组[7]:更新(x,y,self.介绍加入 < #self.丰富文本.显示表 - 12)

    zts:置颜色(0xFF000000):显示(self.x+60,self.y+33,self.数据.名称)
    zts:置颜色(0xFF000000):显示(self.x+237,self.y+33,self.数据.等级)
    zts:置颜色(0xFF000000):显示(self.x+60,self.y+65,self.数据.id)
    zts:置颜色(0xFF000000):显示(self.x+237,self.y+65,self.数据.好友度)



 if self.资源组[4]:事件判断()  then
    if self.输入:取文本()=="" then
      tp.提示:写入("#Y您想说点什么给对方听呢？")
    else
      客户端:发送数据(self.数据.id,24,5,self.输入:取文本())
      self.输入:置文本("")
    end
  elseif self.资源组[5]:事件判断() and 消息闪烁 then
    客户端:发送数据(self.数据.id,25,5,1)
  elseif self.资源组[9]:事件判断()  then
   客户端:发送数据(self.数据.id+0,2, 54, "9s")
  elseif self.资源组[6]:事件判断() then

    self.资源组[8]:置起始点(self.资源组[8]:取百分比转换(self.介绍加入-1,12,#self.丰富文本.显示表))
  elseif self.资源组[7]:事件判断() then

    self.资源组[8]:置起始点(self.资源组[8]:取百分比转换(self.介绍加入+1,12,#self.丰富文本.显示表))
  end

  if #self.丰富文本.显示表 > 12 then
    self.介绍加入 = min(ceil((#self.丰富文本.显示表-12)*self.资源组[8]:取百分比()),#self.丰富文本.显示表-12)
     self.丰富文本.加入 = self.介绍加入
    self.资源组[8]:显示(self.x+280,self.y+106,x,y,self.鼠标)
  end

    self.丰富文本:显示(self.x+20,self.y+95)




      if self.资源组[8].接触 then
        self.焦点 = true
    end
    if self.输入._已碰撞 then
        self.焦点 = true
    end

  self.输入:置坐标(self.x-85,self.y+275)
   self.控件类:显示(self.x,self.y)
    self.控件类:更新(dt,x,y)
 end




return 场景类_好友聊天



