--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:46
--======================================================================--
local 场景类_分解 = class(窗口逻辑)

local tp,zts,zts1
local floor = math.floor
local tonumber = tonumber
local tostring = tostring
local insert = table.insert
local xs = {[0]="单 价",[1]="数 量",[2]="总 额",[3]="现 金"}
local jqr = 引擎.取金钱颜色

function 场景类_分解:初始化(根)
	self.ID = 112
	self.x = 300+(全局游戏宽度-800)/2
	self.y = 100
	self.xx = 0
	self.yy = 0
	self.注释 = "分解"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 根.资源:载入('JM.dll',"网易WDF动画",0xBB983DBB),
		[2] = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true),
		[3] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"分解"),
		[4] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"取消"),

	}
	for n=2,4 do
	   self.资源组[n]:绑定窗口_(112)
	end
	self.物品 = {}
	local 格子 = 根._物品格子
	for i=1,20 do
		self.物品[i] = 格子(0,0,i,"分解")
	end

	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.描边字体
end

function 场景类_分解:打开(数据)
	if self.可视 then
		self.道具 = nil
		self.可视 = false
		if self.上一次 ~= nil then
			self.物品[self.上一次].确定 = false
		end
		self.上一次 = nil

	else
		if  self.x > 全局游戏宽度 then
		   		self.x = 300+(全局游戏宽度-800)/2
		end
		for i=1,20 do
		self.物品[i]:置物品(数据[i])
		end
		self.数据=数据
		if self.上一次 ~= nil then
			self.物品[self.上一次].确定 = false
		end
		self.上一次 = nil
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_分解:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y,self.道具 ~= nil )
	self.资源组[4]:更新(x,y)
	if self.鼠标 then
		if self.资源组[2]:事件判断() then
			self:打开()
		elseif self.资源组[3]:事件判断() then
           客户端:发送数据(self.上一次, 205, 13,"包裹")
           self:打开()
		elseif self.资源组[4]:事件判断() then
			self:打开()
		end
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x+247,self.y+6)
	self.资源组[3]:显示(self.x+65,self.y+352,true)
	self.资源组[4]:显示(self.x+170,self.y+352,true)
	local xx = 0
	local yy = 0
	for i=1,20 do
		local jx = xx * 51 + 10
		local jy = yy * 51 + 33
		self.物品[i]:置坐标(self.x + jx,self.y + jy)
		self.物品[i]:显示(dt,x,y,self.鼠标,{"武器","衣服","项链","头盔","鞋子","腰带"})
		if self.物品[i].焦点 and self.物品[i].物品 ~= nil then
			tp.提示:道具行囊(x,y,self.物品[i].物品)
		  if self.物品[i].事件 and (self.物品[i].物品.类型 == "武器" or self.物品[i].物品.类型 == "头盔"or self.物品[i].物品.类型 == "衣服"or self.物品[i].物品.类型 == "项链" or self.物品[i].物品.类型 == "鞋子" or self.物品[i].物品.类型 == "腰带" or self.物品[i].物品.类型 == "衣服" )  and self.鼠标 then
				if self.上一次 ~= nil and self.上一次 ~= 0 then
					self.物品[self.上一次].确定 = false
				end
				self.物品[i].确定 = true
				self.道具 = self.物品[i].物品
				self.上一次 = i

		end
	end
		xx = xx + 1
		if xx == 5 then
			xx = 0
			yy = yy + 1
		end
	end
	zts:置颜色(-16777216)
	if self.道具 ~= nil then
		zts:显示(self.x + 115,self.y + 260,self:计算(self.道具.等级,"sl"))
        zts:显示(self.x + 115,self.y + 288,self:计算(self.道具.等级,"tl"))
	end
	zts:置颜色(0xFFFFFFFF)
   zts:显示(self.x + 100,self.y + 315,"剩余分解符："..self.数据.分解符)
   zts:显示(self.x + 100,self.y + 335,"当前体力："..self.数据.体力)
end

function 场景类_分解:计算(等级,lx)
   local fj = {sl= math.floor(等级/10*4),tl= math.floor(等级/10*7)}
	return fj[lx]
end


return 场景类_分解