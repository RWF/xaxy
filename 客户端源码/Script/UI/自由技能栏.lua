--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--
local 场景类_自由技能栏 = class(窗口逻辑)

local floor = math.floor
local bw = require("gge包围盒")(0,0,164,39)
local box = 引擎.画矩形
local tp
local mouseb = 引擎.鼠标弹起
local insert = table.insert
function 场景类_自由技能栏:初始化(根)
	self.ID = 32
	self.x = 238+(全局游戏宽度-800)/2
	self.y = 83
	self.xx = 0
	self.yy = 0
	self.注释 = "自由技能栏"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
		local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 资源:载入('JM.dll',"网易WDF动画",0x9ED74AA6),
		[2] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xF11233BB),0,0,4,true,true),
		[3] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFD3D61F2),0,0,4,true,true),
		[4] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x09217E13),0,0,4,true,true),
		[5] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFD3D61F2),0,0,4,true,true),
		[6] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x09217E13),0,0,4,true,true),
		[7] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x2BD1DEF7),0,0,4,true,true,"学习"),
		[8] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"学习10次"),
	}
	for n=2,8 do
		self.资源组[n]:绑定窗口_(32)
	end
	self.介绍文本 = 根._丰富文本(150,150,根.字体表.普通字体)
	self.窗口时间 = 0
	self.刷新文本 = false
	self.学习格子 = {}
	self.选中 = 0
	self.加入 = 0
	self.上限 = 0
	tp = 根
end

function 场景类_自由技能栏:打开(数据)
	if self.可视 then
		self.介绍文本:清空()
		self.刷新文本 = false
		self.可视 = false
		self.本次需求 = nil
		self.数据 ={}
		 self.加入=0
		 self.长度=0
	else
								                    if  self.x > 全局游戏宽度 then
self.x = 238+(全局游戏宽度-800)/2
    end
		self.数据=数据
		if self.数据.类别 == "强化技能" then
			for i=1,#self.数据 do
				local nx= SkillData[self.数据[i].名称]   
				self.数据[i].介绍=nx.介绍
			 	self.学习格子[i] = tp.资源:载入(nx.文件,"网易WDF动画",nx.小图标)
			 end
			 self.长度=7
		elseif self.数据.类别=="辅助技能" then
			for i=1,6 do
				local nx= SkillData[self.数据[i].名称]
				self.数据[i].介绍=nx.介绍
			 	self.学习格子[i] = tp.资源:载入(nx.文件,"网易WDF动画",nx.小图标)
			 end
			 self.长度=6
		elseif self.数据.类别=="生活技能" then
			for i=1,12 do
				local nx= SkillData[self.数据[i].名称]
				self.数据[i].介绍=nx.介绍
			 	self.学习格子[i] = tp.资源:载入(nx.文件,"网易WDF动画",nx.小图标)
			 end
			  self.加入=6
			  self.长度=6
		end
		self.上限 = 180
		insert(tp.窗口_,self)
		tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
		self.可视 = true
	end
end
function 场景类_自由技能栏:刷新(数据)
	self.数据 ={}
		self.数据=数据
		if self.数据.类别 == "强化技能" then
			for i=1,#self.数据 do
				local nx= SkillData[self.数据[i].名称]
				self.数据[i].介绍=nx.介绍
			 	self.学习格子[i] = tp.资源:载入(nx.文件,"网易WDF动画",nx.小图标)
			 end
			 self.长度=7
		elseif self.数据.类别=="辅助技能" then
			for i=1,6 do
				local nx= SkillData[self.数据[i].名称]
				self.数据[i].介绍=nx.介绍
			 	self.学习格子[i] = tp.资源:载入(nx.文件,"网易WDF动画",nx.小图标)
			 end
			 self.长度=6
		elseif self.数据.类别=="生活技能" then
			for i=1,12 do
				local nx= SkillData[self.数据[i].名称]
				self.数据[i].介绍=nx.介绍
			 	self.学习格子[i] = tp.资源:载入(nx.文件,"网易WDF动画",nx.小图标)
			 end
			 self.加入=6
			 self.长度=6
		end
		self.上限 = 180
		self.本次需求 =计算技能数据(self.数据[self.选中].等级+1)
end

function 场景类_自由技能栏:显示(dt,x,y)
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y,false)
	self.资源组[4]:更新(x,y,false)
	self.资源组[5]:更新(x,y,false)
	self.资源组[6]:更新(x,y,false)
	self.资源组[7]:更新(x,y,self.选中 ~= 0)
	self.资源组[8]:更新(x,y,self.选中 ~= 0)
	local font = tp.字体表.普通字体
	local xx = 0
	local yy = 0
	if self.资源组[2]:事件判断() then
		self:打开()
		return false
	elseif self.资源组[7]:事件判断() then
		if self.数据.类别 == "强化技能" then
          客户端:发送数据(self.选中,1,10,1)
      elseif self.数据.类别 == "辅助技能" then
          客户端:发送数据(self.选中,2,10,1)
        elseif self.数据.类别 == "生活技能" then
          客户端:发送数据(self.选中,2,10,1)
      end
    elseif self.资源组[8]:事件判断() then
		if self.数据.类别 == "强化技能" then
          客户端:发送数据(self.选中,3,10,1)
      elseif self.数据.类别 == "辅助技能" then
          客户端:发送数据(self.选中,4,10,1)
        elseif self.数据.类别 == "生活技能" then
          客户端:发送数据(self.选中,4,10,1)
      end
	end

	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x + 364,self.y + 6)
	self.资源组[3]:显示(self.x + 355,self.y + 40)
	self.资源组[4]:显示(self.x + 355,self.y + 168)
	self.资源组[5]:显示(self.x + 354,self.y + 202)
	self.资源组[6]:显示(self.x + 354,self.y + 330)
	self.资源组[7]:显示(self.x + 124,self.y + 440,true)
self.资源组[8]:显示(self.x + 244,self.y + 440,true)
		for i=1,self.长度 do
			xx = self.x + 18
			yy = self.y + i * 40
			bw:置坐标(xx,yy + 30)
			if self.选中 ~= i + self.加入 then
				if bw:检查点(x,y) then
					box(xx-2,yy+25,xx+163,yy+39+25,-3551379)
					if self.鼠标 and mouseb(0) then
						self.选中 = i + self.加入
						self.介绍文本:清空()
						self.介绍文本:添加文本("#N/【介绍】"..self.数据[self.选中].介绍)
						self.介绍文本:添加文本("#N/【等级】"..self.数据[self.选中].等级)
						self.介绍文本:添加文本("#N/【所需帮派贡献】"..self:学习技能(self.数据[self.选中].等级))
						self.本次需求 =计算技能数据(self.数据[self.选中].等级+1)
					end
					self.焦点 = true
				end
			else
				local ys = -10790181
				if bw:检查点(x,y) then
					ys = -9670988
					self.焦点 = true
				end
				box(xx-2,yy+25,xx+163,yy+39+25,ys)
			end
			self.学习格子[i+self.加入]:显示(self.x + 18,yy + 24)
			font:置颜色(-16777216)
			font:显示(xx + 42,yy + 37,self.数据[i+self.加入].名称)
			font:显示(xx + 112,yy + 37 ,self.数据[i+self.加入].等级.."/"..self.上限)
		end

	if self.选中 ~= 0 then
		font:显示(self.x + 83,self.y + 362,self.数据.经验)
		font:显示(self.x + 83,self.y + 385,self.数据.银两)
		font:显示(self.x + 83,self.y + 407,self.数据.存银)
		if self.本次需求 ~= nil then
			font:显示(self.x + 290,self.y + 362,self.本次需求.经验)
			font:显示(self.x + 290,self.y + 385,self.本次需求.金钱)
		else
				font:显示(self.x + 290,self.y + 362,"--------")
				font:显示(self.x + 290,self.y + 385,"--------")
		end
		font:显示(self.x + 290,self.y + 407,self.数据.储备)
	end
	self.介绍文本:显示(self.x+205,self.y+205)
end


function 场景类_自由技能栏:学习技能(等级)
	if 等级< 100 then
		return 0
	else
	return floor(0.7*等级)
	end
end
return 场景类_自由技能栏