--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--
local 场景类_法宝合成 = class(窗口逻辑)

local tp,zts,zts1
local floor = math.floor
local tonumber = tonumber
local tostring = tostring
local insert = table.insert
local xs = {"金","木","水","火","土"}
local jqr = 引擎.取金钱颜色

function 场景类_法宝合成:初始化(根)
	self.ID = 112
	self.x = 100+(全局游戏宽度-800)/2
	self.y = 150
	self.xx = 0
	self.yy = 0
	self.注释 = "法宝合成"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 根.资源:载入('JM.dll',"网易WDF动画",0xDEA18D0A),
		[2] = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true),
		[3] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"开始"),
		[4] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"取消"),
		[5] = 根.资源:载入('JM.dll',"网易WDF动画",0xFF5EF52B),
		[6] = 根.资源:载入('JM.dll',"网易WDF动画",0xBB25066F),

	}
	for n=2,4 do
	   self.资源组[n]:绑定窗口_(112)
	end
	self.物品 = {}
	local 格子 = 根._物品格子
	for i=1,20 do
		self.物品[i] = 格子(0,0,i,"法宝合成")
	end

	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.描边字体
 	 self.材料 = {}
	self.选中材料={}
	for i=1,5 do
		self.材料[i] = 格子(self.x+4,self.y,1,"打造材料")
		self.选中材料[i]=0
	end
	self.材料数量 = 0
end

function 场景类_法宝合成:打开(数据)
	if self.可视 then
		self.可视 = false
		self.材料数量 = 0
	else
								                    if  self.x > 全局游戏宽度 then
self.x = 100+(全局游戏宽度-800)/2
    end
		for i=1,20 do
		self.物品[i]:置物品(数据[i])
		end
		for i=1,5 do
		self.选中材料[i]=0
		self.材料[i]:置物品(nil)
		 end
		self.数据=数据
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end
function 场景类_法宝合成:刷新(数据)
		self.材料数量 = 0
		for i=1,20 do
		self.物品[i]:置物品(数据[i])
		end
		for i=1,5 do
		self.选中材料[i]=0
		self.材料[i]:置物品(nil)
		 end
		self.数据=数据
end

function 场景类_法宝合成:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y,self.材料数量 >= 5)
	self.资源组[4]:更新(x,y)
	if self.鼠标 then
		if self.资源组[2]:事件判断() then
			self:打开()
		elseif self.资源组[3]:事件判断() then
			self.转盘 = 0

		elseif self.资源组[4]:事件判断() then
			self:打开()
		end
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x+560,self.y+3)
	self.资源组[3]:显示(self.x+365,self.y+262,true)
	self.资源组[4]:显示(self.x+470,self.y+262,true)
	self.资源组[5]:显示(self.x+430,self.y+144)
	self.资源组[6]:显示(self.x+430,self.y+144)
	if self.转盘  then
	    self.转盘 = self.转盘 +0.1
	    if self.转盘>20 then
	        self.转盘=nil
	        客户端:发送数据(1,216,13,self.选中材料[1].."*-*"..self.选中材料[2].."*-*"..self.选中材料[3].."*-*"..self.选中材料[4].."*-*"..self.选中材料[5])
			      for i=1,5 do
				self.选中材料[i]=0
				self.材料[i]:置物品(nil)
				 end
	    end
      	self.资源组[6]:更新(dt)
	end
	local is = 0
	for h=1,4 do
		for l=1,5 do
			is = is + 1
			self.物品[is]:置坐标(l * 51 -29 + self.x,h * 51 + self.y -16)
			self.物品[is]:显示(dt,x,y,self.鼠标,"法宝合成")
			if self.物品[is].物品 ~= nil and self.物品[is].焦点 then
				tp.提示:道具行囊(x,y,self.物品[is].物品)
				if self.物品[is].事件 and self.鼠标 then
                	if  self.物品[is].物品.名称 == "内丹" or self.物品[is].物品.名称 == "千年阴沉木"or self.物品[is].物品.名称 == "麒麟血" or self.物品[is].物品.名称 == "玄龟板"or self.物品[is].物品.名称 == "金凤羽" then
					  if self.材料数量 < 5 then
							if self.物品[is].物品.名称 == "内丹" and self.材料[5].物品 == nil then
								self.选中 =is
			               		self.材料[5]:置物品(self.物品[is].物品)
								self.物品[is]:置物品(nil)
								self.材料数量 = self.材料数量 + 1
								self.选中材料[5]=is
							elseif self.物品[is].物品.名称 == "千年阴沉木"and self.材料[2].物品 == nil then
								self.选中 =is
			               		self.材料[2]:置物品(self.物品[is].物品)
								self.物品[is]:置物品(nil)
								self.材料数量 = self.材料数量 + 1
								self.选中材料[2]=is
							elseif self.物品[is].物品.名称 == "麒麟血" and self.材料[3].物品 == nil then
								self.选中 =is
			               		self.材料[3]:置物品(self.物品[is].物品)
								self.物品[is]:置物品(nil)
								self.材料数量 = self.材料数量 + 1
								self.选中材料[3]=is
							elseif self.物品[is].物品.名称 == "玄龟板"and self.材料[1].物品 == nil then
								self.选中 =is
			               		self.材料[1]:置物品(self.物品[is].物品)
								self.物品[is]:置物品(nil)
								self.材料数量 = self.材料数量 + 1
								self.选中材料[1]=is
							elseif self.物品[is].物品.名称 == "金凤羽"and self.材料[4].物品 == nil then
								self.选中 =is
			               		self.材料[4]:置物品(self.物品[is].物品)
								self.物品[is]:置物品(nil)
								self.材料数量 = self.材料数量 + 1
								self.选中材料[4]=is
							end
					   else
		                    tp.提示:写入("#Y/放入的物品太多了，不允许再放入了！")
		               end
		           else
		              tp.提示:写入("#Y/只有法宝材料才能合成！")
		           end
				end
			end
		end
	end





	zts:置颜色(-16777216)
	local is = 0
	for h=1,2 do
		for l=1,2 do
			is = is + 1
			 Picture.物品格子:显示(l * 205 +98 + self.x,h * 170 + self.y-140 )

			self.材料[is]:置坐标(l * 205 +100 + self.x,h * 170 + self.y-140 )
			self.材料[is]:显示(dt,x,y,self.鼠标)
			 zts1:显示(l * 205 +115 + self.x,h * 170 + self.y-125 ,xs[is])
			if self.材料[is].物品 ~= nil and self.材料[is].焦点 then
				tp.提示:道具行囊(x,y,self.材料[is].物品)
				if self.材料[is].事件 and self.鼠标 then
					self.材料数量 = self.材料数量 - 1
                    self.物品[self.选中材料[is]]:置物品(self.数据[self.选中材料[is]])
					self.材料[is]:置物品(nil)
				end
			end
		end
	end
    Picture.物品格子:显示(self.x+401, self.y+120)

	self.材料[5]:置坐标( self.x+403, self.y+120)
	self.材料[5]:显示(dt,x,y,self.鼠标)
	     zts1:显示(self.x+418, self.y+135,xs[5])
	if self.材料[5].物品 ~= nil and self.材料[5].焦点 then
		tp.提示:道具行囊(x,y,self.材料[5].物品)
		if self.材料[5].事件 and self.鼠标 then
			self.材料数量 = self.材料数量 - 1
            self.物品[self.选中材料[5]]:置物品(self.数据[self.选中材料[5]])
			self.材料[5]:置物品(nil)
		end
	end



end




return 场景类_法宝合成