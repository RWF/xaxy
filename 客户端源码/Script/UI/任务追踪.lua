-- @Author: 作者QQ414628710
-- @Date:   2022-01-15 20:06:44
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-23 18:12:48
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:46
--======================================================================--
local 场景类_任务追踪栏 = class()

local floor = math.floor
local format = string.format
local tp
local xxx = 0
local yyy = 0
local box = 引擎.画矩形
local min = math.min
local max = math.max
local ceil = math.ceil
local mousea = 引擎.鼠标按下
local mouseb = 引擎.鼠标弹起

function 场景类_任务追踪栏:初始化(根)
	self.x = 全局游戏宽度-170
	self.y = 100
	self.xx = 0
	self.yy = 0

	self.焦点 = false
	self.移动窗口 = false
	self.可移动窗口 = false
	self.数据={}
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	tp = 根
	self.资源组 = {
		[1] = 根._自适应.创建(71,1,1,1,3,9),

		[2] = 按钮(资源:载入('MAX.7z',"网易WDF动画",4),0,0,4),
		[3] = 按钮(资源:载入('MAX.7z',"网易WDF动画",6),0,0,4),
		[4] = 按钮(资源:载入('MAX.7z',"网易WDF动画",5),0,0,4),
		[5] = 按钮(资源:载入('JM.dll',"网易WDF动画",0x335CECBC),0,0,4),
		[6] = tp._滑块(资源:载入('JM.dll',"网易WDF动画",0x7F027E7B),4,10,139,2),
		[7] = 按钮(资源:载入('JM.dll',"网易WDF动画",0xB7F2FF5E),0,0,4),
		[8] = 按钮(资源:载入('JM.dll',"网易WDF动画",0x36DDB607),0,0,4),
		[9] = 根._自适应.创建(9,1,1,1,3,9),
		[10] = 按钮(资源:载入('MAX.7z',"网易WDF动画",7),0,0,4),
	}
	self.资源组[1]:置宽高(170,25)
	self.资源组[9]:置宽高(170,177)
	self.wz文字 = tp.字体表.普通字体

	self.状态 = 2
	self.介绍加入 = 0
	self.窗口时间 = 0
	self.介绍文本 =根._丰富文本(160,170):添加元素("L",-16776961)

end

function 场景类_任务追踪栏:加载(数据)
	self.介绍文本:清空()
	self.数据 = 数据
	for i=1,#self.数据 do
		if self.数据[i].名称~="双倍经验" and self.数据[i].名称~="摄妖香" and self.数据[i].名称~="秘制红罗羹" then

		    self.介绍文本:添加文本(format("#G/%s\n◆%s",self.数据[i].名称,self.数据[i].说明))
		    if i < #self.数据 then
			    self.介绍文本:添加文本("")
			end

		end
	end
	for i=1,#self.介绍文本.显示表 - 10 do
		self.介绍文本:滚动(1)
	end
	if #self.介绍文本.显示表 > 10 and self.介绍加入 ~= 0 then
		self.资源组[6]:置起始点(self.资源组[6]:取百分比转换(self.介绍加入+1,10,#self.介绍文本.显示表))
		self.介绍加入 = min(ceil((#self.介绍文本.显示表-10)*self.资源组[6]:取百分比()),#self.介绍文本.显示表-10)
		self.介绍文本.加入 = self.介绍加入
	end
end

function 场景类_任务追踪栏:显示(dt,x,y)
	if not tp.任务追踪 then
		return
	end
	self.x = 全局游戏宽度-170
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y)
	self.资源组[4]:更新(x,y)
	self.资源组[10]:更新(x,y)
	if self.资源组[2]:事件判断(x,y) then
		if self.状态 == 2 then
			self.状态 = 8
			self.可移动窗口 = true
			tp.提示:写入("#Y/你现在可以上下移动你的任务追踪栏")
		else
		    self.状态 = 2
		    self.可移动窗口 = false
		    tp.提示:写入("#Y/锁定了任务追踪栏")
		end
	elseif self.资源组[4]:事件判断(x,y) then
		tp.任务追踪 = false
	end
	self.资源组[1]:显示(self.x,self.y-5)
	self.资源组[9]:显示(self.x,self.y+25)

	self.wz文字:置颜色(0xFFFFFFFF)
	self.wz文字:显示(self.x+58,self.y,"任务追踪")
	self.资源组[2]:显示(self.x+28,self.y-2,true,nil,nil,self.状态==2,0)
	self.资源组[3]:显示(self.x+5,self.y-2)
	self.资源组[4]:显示(self.x+155,self.y-2)
	self.资源组[10]:显示(self.x+130,self.y-2)

	if #self.介绍文本.显示表 > 10 then
		self.资源组[5]:更新(x,y,self.介绍加入 > 0)
		self.资源组[7]:更新(x,y,self.介绍加入 < #self.介绍文本.显示表 - 10)
		if self.资源组[5]:事件判断() then
			self.资源组[6]:置起始点(self.资源组[6]:取百分比转换(self.介绍加入-1,10,#self.介绍文本.显示表))
		elseif self.资源组[7]:事件判断() then
			self.资源组[6]:置起始点(self.资源组[6]:取百分比转换(self.介绍加入+1,10,#self.介绍文本.显示表))
		end
		self.资源组[5]:显示(self.x,self.y+31)
		--box(self.x,self.y+44,self.x+195,self.y+183,ARGB(160,30,30,30))
		self.资源组[6]:显示(self.x,self.y+44,x,y,true)
		self.资源组[7]:显示(self.x,self.y+183)
		self.介绍加入 = min(ceil((#self.介绍文本.显示表-10)*self.资源组[6]:取百分比()),#self.介绍文本.显示表-10)
		self.介绍文本.加入 = self.介绍加入
	end
	self.介绍文本:显示(self.x+10,self.y+29,4294967295)
	if self.资源组[6].接触 or tp.按钮焦点 then
		self.焦点 = true
	end
	if self.可移动窗口 and self:检查点(x,y) then
		if mousea(0) then
			self:初始移动(x,y)
		end
		if self.移动窗口 and not tp.隐藏UI and not tp.消息栏焦点 then
			self:开始移动(x,y)
		end
	end
	if self.移动窗口 and (mouseb(0) or tp.隐藏UI or tp.消息栏焦点) then
		self.移动窗口 = false
	end
end



function 场景类_任务追踪栏:检查点(x,y)
	-- if not tp.战斗中  and tp.任务追踪 and ((self.可移动窗口 and self.资源组[1]:是否选中(x,y)) or self.焦点)  then
	if not tp.战斗中  and tp.任务追踪 and (self.资源组[1]:是否选中(x,y) or self.资源组[9]:是否选中(x,y))  then
		return true
	end
end

function 场景类_任务追踪栏:初始移动(x,y)
	if tp.消息栏焦点 then
  		return
 	end
	if not self.焦点 then
		self.移动窗口 = true
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_任务追踪栏:开始移动(x,y)
	self.x = x - self.xx
	self.y = y - self.yy
end

return 场景类_任务追踪栏