--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--
local 场景类_消息框窗口 = class(窗口逻辑)

local floor = math.floor
local tp
local insert = table.insert
local mousea = 引擎.鼠标按下

function 场景类_消息框窗口:初始化(根)
	self.ID = 41
	self.x = 100+(全局游戏宽度-800)/2
	self.y = 80
	self.xx = 0
	self.yy = 0
	self.注释 = "消息框窗口"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = false
	local 资源 = 根.资源
    self.上翻 = 根._按钮(资源:载入('JM.dll',"网易WDF动画",0x7E6D27EE),0,0,3,true)
	self.下翻 = 根._按钮(资源:载入('JM.dll',"网易WDF动画",0x1E08B148),0,0,3,true)
	self.资源组 = {
		[1] = 根._自适应.创建(71,1,579,469,3,9)
	}
	self.上翻:绑定窗口_(41)
	self.下翻:绑定窗口_(41)
	self.窗口时间 = 0
	tp = 根
	self.页数= 1
end

function 场景类_消息框窗口:打开()
	if self.可视 then
		self.可视 = false
		tp.下一次确定 = true
	else
		self.页数= 1
		if  self.x > 全局游戏宽度 then
		self.x = 100+(全局游戏宽度-800)/2
		end
		insert(tp.窗口_,self)
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end
function 场景类_消息框窗口:更新(dt,x,y)
end

function 场景类_消息框窗口:显示(dt,x,y)
	if not self.可视 then
	    return
	end

	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
    self.上翻:更新(x,y,self.页数>1)
	self.下翻:更新(x,y,self.页数<4)
  	self.上翻:显示(self.x+500,self.y+443)
	self.下翻:显示(self.x+530,self.y+443)
	if self.页数== 1 then
		local v = 0
		for i=0,17 do
			tp.包子表情动画[i]:显示(self.x+6+i*32,self.y+32)
			if tp.包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..i)
					break
				end
			end
		end
		for i=18,35 do
			tp.包子表情动画[i]:显示(self.x+6+(i-18)*32,self.y+82)
			if tp.包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..i)
					break
				end
			end
		end
		for i=36,49 do
			tp.包子表情动画[i]:显示(self.x+6+(i-36)*41,self.y+132)
			if tp.包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..i)
					break
				end
			end
		end
		for i=50,63 do
			tp.包子表情动画[i]:显示(self.x+6+(i-50)*42,self.y+182)
			if tp.包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..i)
					break
				end
			end
		end
		for i=64,77 do
			tp.包子表情动画[i]:显示(floor(self.x+6+(i-64)*41.5),self.y+232)
			if tp.包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..i)
					break
				end
			end
		end
		for i=78,89 do
			tp.包子表情动画[i]:显示(self.x+6+(i-78)*47,self.y+282)
			if tp.包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..i)
					break
				end
			end
		end
		for i=90,100 do
			tp.包子表情动画[i]:显示(self.x+6+(i-90)*51,self.y+342)
			if tp.包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..i)
					break
				end
			end
		end
		for i=101,111 do
			tp.包子表情动画[i]:显示(self.x+8+(i-101)*53,self.y+402)
			if tp.包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..i)
					break
				end
			end
		end
		for i=112,119 do
			tp.包子表情动画[i]:显示(self.x+6+(i-112)*58,self.y+462)
			if tp.包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..i)
					break
				end
			end
		end
	elseif  self.页数== 2 then
		local v = 0
		for i=0,5 do
			tp.大包子表情动画[i]:显示(self.x+6+i*95,self.y+105)
			if tp.大包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..400+i)
					break
				end
			end
		end
		for i=6,11 do
			tp.大包子表情动画[i]:显示(self.x+6+(i-6)*95,self.y+210)
			if tp.大包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..400+i)
					break
				end
			end
		end
		for i=12,17 do
			tp.大包子表情动画[i]:显示(self.x+6+(i-12)*95,self.y+315)
			if tp.大包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..400+i)
					break
				end
			end
		end
		for i=18,23 do
			tp.大包子表情动画[i]:显示(self.x+6+(i-18)*95,self.y+420)
			if tp.大包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..400+i)
					break
				end
			end
		end
	elseif  self.页数== 3 then
		local v = 0
		for i=24,29 do
			tp.大包子表情动画[i]:显示(self.x+6+(i-24)*95,self.y+105)
			if tp.大包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..400+i)
					break
				end
			end
		end
		for i=30,35 do
			tp.大包子表情动画[i]:显示(self.x+6+(i-30)*95,self.y+210)
			if tp.大包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..400+i)
					break
				end
			end
		end
		for i=36,41 do
			tp.大包子表情动画[i]:显示(self.x+6+(i-36)*95,self.y+315)
			if tp.大包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..400+i)
					break
				end
			end
		end
		for i=42,47 do
			tp.大包子表情动画[i]:显示(self.x+6+(i-42)*95,self.y+420)
			if tp.大包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..400+i)
					break
				end
			end
		end
	elseif  self.页数== 4 then
		local v = 0
		for i=48,53 do
			tp.大包子表情动画[i]:显示(self.x+6+(i-48)*95,self.y+105)
			if tp.大包子表情动画[i]:是否选中(x,y) and self.鼠标 then
				if mousea(0) then
					self:打开()
					tp.窗口.聊天框类.输入:添加文本("#"..400+i)
					break
				end
			end
		end
	end
	if self.上翻:事件判断() then
		self.页数 = self.页数 -1
	elseif self.下翻:事件判断() then
		self.页数 = self.页数 +1
	end

end




return 场景类_消息框窗口