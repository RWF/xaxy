--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:46
--======================================================================--
local 场景类_人物称谓栏 = class(窗口逻辑)
local bw = require("gge包围盒")(0,0,190,18)
local box = 引擎.画矩形
local format = string.format
local mouse = 引擎.鼠标弹起
local fonts
local names,tp
local move = table.move
local insert = table.insert

function 场景类_人物称谓栏:初始化(根)
	self.ID = 5
	self.x = 175+(全局游戏宽度-800)/2
	self.y = 110
	self.xx = 0
	self.yy = 0
	self.注释 = "人物称谓栏"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = Picture.称谓特效界面,
		[2] = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true),
		[3] = 按钮.创建(自适应.创建(20,4,18,19,4,3),0,0,4,true,true),
		[4] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),
		[5] = 按钮.创建(自适应.创建(16,4,43,22,1,3),0,0,4,true,true,"改变"),
		[6] = 按钮.创建(自适应.创建(16,4,43,22,1,3),0,0,4,true,true,"隐藏"),
		[7] = 按钮.创建(自适应.创建(16,4,43,22,1,3),0,0,4,true,true,"删除"),
        [8] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),
		[9] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),
		[10] = 按钮.创建(自适应.创建(16,4,43,22,1,3),0,0,4,true,true,"购买"),
		[11] = 按钮.创建(自适应.创建(16,4,43,22,1,3),0,0,4,true,true,"取消"),
		[12] = 根._小型选项栏.创建(自适应.创建(6,1,125,175,3,9),"选择特效"),
        [13] = 根._小型选项栏.创建(自适应.创建(6,1,85,125,3,9),"购买特效"),
	}
	self.加入 = 0
	self.选中 = 0
	for n=2,11 do
	   self.资源组[n]:绑定窗口_(5)
	end
	self.介绍文本 = 根._丰富文本(190,139,根.字体表.普通字体)

	       	for n=0,119 do
     		self.介绍文本:添加元素(n,根.包子表情动画[n])
         end
	tp = 根

	fonts = tp.字体表.普通字体
	self.窗口时间 = 0
	self.数据={}
	self.选择拥有="不显示特效"
	self.选择购买=""
	self.选择价格=0
	self.称谓偏移=生成XY(0,0)
end


function 场景类_人物称谓栏:打开(数据)
	if self.可视 then
		self.介绍文本:清空()
		self.可视 = false
		self.选择拥有="不显示特效"
		self.选择购买=""
		self.选择价格=0
		self.称谓动画 =nil
		-- self.资源组[12].可视 = false
		-- self.资源组[13].可视 = false
	else
		if  self.x > 全局游戏宽度 then
		   	self.x = 175+(全局游戏宽度-800)/2
		end
       self:置人物形象()
		self.数据 = 数据[1]
		self.仙玉 = 数据[2]
		self.特效 = 数据[3]
		table.insert(self.特效,"不显示特效")
		insert(tp.窗口_,self)
		self.加入 = 0
		self.选中 = 0
	    tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_人物称谓栏:置人物形象()
	self.资源组[99] = nil
	self.资源组[100] = nil
 			if tp.场景.人物.数据.武器数据.类别 ~= 0 and tp.场景.人物.数据.武器数据.类别 ~= "" and tp.场景.人物.数据.武器数据.类别 ~= nil then
				local v = tp.场景.人物.数据.武器数据.类别
				if tp.场景.人物.数据.武器数据.名称 == "龙鸣寒水" or tp.场景.人物.数据.武器数据.名称 == "非攻" then
					v = "弓弩1"
				end
				local 资源 = 取模型(tp.场景.人物.数据.造型,v)
				self.资源组[99] = tp.资源:载入(资源.资源,"网易WDF动画",资源["静立"])
				local m = tp:qfjmc(tp.场景.人物.数据.武器数据.类别,tp.场景.人物.数据.武器数据.等级,tp.场景.人物.数据.武器数据.名称)
				local n = 取模型(m.."_"..tp.场景.人物.数据.造型)
				self.资源组[100] = tp.资源:载入(n.资源,"网易WDF动画",n["静立"])
				if tp.场景.人物.数据.武器数据.染色 then
					self.资源组[100]:置染色(tp.场景.人物.数据.武器数据.染色.染色方案,tp.场景.人物.数据.武器数据.染色.染色组.a,tp.场景.人物.数据.武器数据.染色.染色组.b)
				end
				self.资源组[100]:置方向(4)
			else
					local n = 取模型(tp.场景.人物.数据.造型)
					self.资源组[99] = tp.资源:载入(n.资源,"网易WDF动画",n["静立"])
			end
        	self.资源组[99]:置染色(DyeData[tp.场景.人物.数据.造型][1],tp.场景.人物.数据.染色.a,tp.场景.人物.数据.染色.b,tp.场景.人物.数据.染色.c) 
			self.资源组[99]:置方向(4)

end
function 场景类_人物称谓栏:刷新(数据)
  self.数据 =数据
end
function 场景类_人物称谓栏:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y,self.加入 > 0)
	self.资源组[4]:更新(x,y,self.加入 < #self.数据 - 5)
	self.资源组[5]:更新(x,y,self.选中 ~= 0)
	self.资源组[6]:更新(x,y,self.选中 ~= 0)
	self.资源组[7]:更新(x,y,self.选中 ~= 0)
	self.资源组[8]:更新(x,y)
	self.资源组[9]:更新(x,y)

	self.资源组[10]:更新(x,y,self.选择购买 ~= "")
	self.资源组[11]:更新(x,y)
	if self.资源组[2]:事件判断()   then
		self:打开()
		return false
	elseif self.资源组[10]:事件判断()   then

		客户端:发送数据(9,57,13,self.选择购买)

	elseif self.资源组[3]:事件判断() then
		self.加入 = self.加入 -5
	elseif self.资源组[4]:事件判断() then
		self.加入 = self.加入 +5
	elseif self.资源组[5]:事件判断() then
      	客户端:发送数据(9,56,13,self.数据[self.选中],self.选择拥有)
		self.数据.当前 = self.数据[self.选中]
        -- tp.场景.人物.称谓偏移 = 生成XY(tp.字体表.人物字体:取宽度(self.数据.当前) / 2,-15)
	elseif self.资源组[6]:事件判断() then
		self.数据.当前 = nil
		客户端:发送数据(9,55,13,self.数据[self.选中],1)
	elseif self.资源组[7]:事件判断() then
		--if self.数据[self.选中] == self.数据.当前 then
		  客户端:发送数据(9,54,13,self.数据[self.选中],1)
		--end
		self.介绍文本:清空()
		table.remove(self.数据,self.选中)
		self.选中 = 0
	elseif self.资源组[8]:事件判断() then
		 self.资源组[12]:打开(self.特效)
         self.资源组[12]:置选中(self.选择拥有)
	elseif self.资源组[9]:事件判断() then
		local tb={"彩虹云朵","绿叶飘零","枫叶竹亭","白色气泡","碧绿挂帘","恶魔飘带","向日葵","天使翅膀左","蓝色求脱单","万圣节南瓜","粉色热恋中","天使翅膀右","竹林情缘","血色桃花",
		"心形曲带","月色佳人","满天星","蓝色刺绣","极寒雪花","深海双鱼","示爱","火","绿色围栏","喜结连理","示爱蓝色","寒风刺骨","旋转星星","沙滩景色","绿叶桃花","泡泡心电图",
		"秀恩爱","喜结良缘","火光四射","卡通飘带","南天门","心形挂件","绿色竹林","黑红双煞","蓝色条形","示爱心形粉","恶魔南瓜","心形音浪","恋爱百分百","粉绿竹子","户太8号",
		"红色枫叶","紫色求脱单","卡通绸带","深海双鱼称","悠闲熊猫","花木条","雪里透红","示爱气泡","热恋中","水果篮","阳光明媚","秀恩爱粉色","阳光海滩","恶魔飘带称","金丝贡缎"}
		  self.资源组[13]:打开(tb)
		self.资源组[13]:置选中(self.选择购买)
	elseif self.资源组[12]:事件判断() then
            self.资源组[12]:置选中(self.资源组[12].弹出事件)
            self.选择拥有=self.资源组[12].弹出事件
            self.资源组[12].弹出事件 = nil
			

	elseif self.资源组[13]:事件判断() then
            self.资源组[13]:置选中(self.资源组[13].弹出事件)
            	self.选择购买=self.资源组[13].弹出事件
            	self.称谓动画=tp.资源:载入(LabelData[self.选择购买].资源,"网易WDF动画",LabelData[self.选择购买].动画)
            	self.称谓动画偏移=LabelData[self.选择购买].偏移
            	self.选择价格=LabelData[self.选择购买].无文字
            self.资源组[13].弹出事件 = nil
	end
	self.资源组[1]:显示(self.x,self.y)

	fonts:置颜色(0xFF000000):显示(self.x + 102, self.y + 36,self.数据.当前)
   fonts:置颜色(0xFF000000):显示(self.x + 336, self.y + 349,self.仙玉)
     fonts:置颜色(0xFF000000):显示(self.x + 100, self.y + 62,self.选择拥有)
      fonts:置颜色(0xFF000000):显示(self.x + 336, self.y + 267,self.选择购买)
     fonts:置颜色(0xFF000000):显示(self.x + 100, self.y + 88,"永久")
     fonts:置颜色(0xFF000000):显示(self.x + 336, self.y + 294,"永久")
     fonts:置颜色(0xFF000000):显示(self.x + 336, self.y + 321,self.选择价格)



	self.资源组[2]:显示(self.x + 428,self.y + 6)
	self.资源组[3]:显示(self.x + 215,self.y + 138)
	self.资源组[4]:显示(self.x + 215,self.y + 240)
	self.资源组[5]:显示(self.x + 40,self.y + 378,true)
	self.资源组[6]:显示(self.x + 100,self.y + 378,true)
	self.资源组[7]:显示(self.x + 160,self.y + 378,true)

	self.资源组[8]:显示(self.x + 220,self.y + 60)
	self.资源组[9]:显示(self.x + 411,self.y + 263)

	self.资源组[10]:显示(self.x + 256,self.y + 378)
	self.资源组[11]:显示(self.x + 371,self.y + 378)


	self.资源组[99]:更新(dt)
	self.资源组[99]:显示(self.x + 346,self.y + 175)
	if self.资源组[100] ~= nil then
		self.资源组[100]:更新(dt)
		self.资源组[100]:显示(self.x + 346,self.y + 175)
	end
	tp.影子:显示(self.x + 346,self.y + 175)	

	fonts:置颜色(-16777216)
	for m=1,5 do
		if self.数据[m+self.加入] ~= nil then
			bw:置坐标(self.x + 23,self.y + 124 + m * 19)
			if self.选中 ~= m + self.加入 then
				if bw:检查点(x,y) then
					box(self.x + 20,self.y + 120 + m * 19,self.x + 211,self.y + 142 + m * 19,-3551379)
					if mouse(0) then
						self.称谓偏移=生成XY(math.floor(tp.字体表.人物字体:取宽度(self.数据[m+self.加入]) / 2),-15)
						self.选中 = m + self.加入
						self.介绍文本:清空()
					    
						self.介绍文本:添加文本(string.format("#N/%s",LabelData[self.数据[self.选中]] and  LabelData[self.数据[self.选中]].介绍 or "暂无此称谓介绍"))
					end
					self.焦点 = true
				end
			else
				local ys = -10790181
				if bw:检查点(x,y) then
					ys = -9670988
					self.焦点 = true
				end
				box(self.x + 20,self.y + 120 + m * 19,self.x + 211,self.y + 142 + m * 19,ys)
			end
			fonts:显示(self.x + 23,self.y + 124 + m * 19,self.数据[m+self.加入])
		end
	end
	   self.资源组[12]:显示(self.x + 93,self.y + 79,x,y,self.鼠标)
   self.资源组[13]:显示(self.x + 331,self.y + 285,x,y,self.鼠标)

		if self.称谓动画 then
			    self.称谓动画:显示(生成XY(self.x + 345,self.y + 185) -self.称谓动画偏移-self.称谓偏移)
             self.称谓动画:更新(dt)
		end
	tp.字体表.人物字体:置颜色(0xFF70FC70):显示(self.x + 320,self.y + 230,tp.场景.人物.数据.名称)
	if self.选中~=0 then
		tp.字体表.人物字体:置颜色(0xFF80BFFF):显示x(生成XY(self.x + 345,self.y + 185)- self.称谓偏移,self.数据[self.选中])

	end


	self.介绍文本:显示(self.x + 20,self.y + 302)
end


return 场景类_人物称谓栏