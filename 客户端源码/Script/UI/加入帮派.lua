--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:46
--======================================================================--
local 加入帮派类 = class()
local bw = require("gge包围盒")(0,0,220,22)
local box = 引擎.画矩形
local tp,zts,zts1
local floor = math.floor
local tonumber = tonumber
local tostring = tostring
local format = string.format
local insert = table.insert
function 加入帮派类:初始化(根)

	self.ID = 119
	self.x = 250+(全局游戏宽度-800)/2
	self.y = 60
	self.xx = 0
	self.yy = 0
	self.注释 = "队伍栏"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 资源 = 根.资源
	local 自适应 =根._自适应
		self.资源组 = {
		[0] = 根.资源:载入('JM.dll',"网易WDF动画",0xF221A4EF),
		[1] = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true),
		[2] = 按钮.创建(自适应.创建(16,4,43,22,1,3),0,0,4,true,true,"加入"),
		[3] = 按钮.创建(自适应.创建(16,4,43,22,1,3),0,0,4,true,true,"取消"),
		[4] = 按钮.创建(自适应.创建(20,4,18,19,4,3),0,0,4,true,true),
		[5] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),

	}
		self.资源组[1]:绑定窗口_(119)
		self.资源组[2]:绑定窗口_(119)
		self.资源组[3]:绑定窗口_(119)
		self.资源组[4]:绑定窗口_(119)
		self.资源组[5]:绑定窗口_(119)
		tp = 根
	self.窗口时间 = 0
	self.介绍文本 = 根._丰富文本(230,280,根.字体表.普通字体)
	 for n=0,119 do
    self.介绍文本:添加元素(n,根.包子表情动画[n])
    end
    zts=tp.字体表.普通字体
	self.选中 = 0
	self.加入 = 0

end

function 加入帮派类:打开(数据)
  if self.可视 then
	self.选中 = 0
	self.加入 = 0
		self.可视 = false
  else
  			if  self.x > 全局游戏宽度 then
		   			self.x = 250+(全局游戏宽度-800)/2
		end
     self.数据 = 数据
		insert(tp.窗口_,self)
		tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.可视 = true
  end
 end



function 加入帮派类:检查点(x,y)
	if self.可视 and self.资源组[0]:是否选中(x,y)  then
		return true
	else
		return false
	end
end

function 加入帮派类:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.可视 and self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 加入帮派类:开始移动(x,y)
	if self.可视 and self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

function 加入帮派类:显示(dt,x, y)

		self.焦点 = false
		self.资源组[0]:显示(self.x,self.y)
        self.资源组[1]:显示(self.x+265,self.y+6)
        self.资源组[2]:显示(self.x+70,self.y+360,true)
        self.资源组[3]:显示(self.x+170,self.y+360,true)

		self.资源组[4]:显示(self.x+250,self.y+60,true)
		self.资源组[5]:显示(self.x+250,self.y+165,true)

        self.资源组[1]:更新(x,y)
        self.资源组[2]:更新(x,y)
        self.资源组[3]:更新(x,y)
        self.资源组[4]:更新(x,y,self.加入>0)
        self.资源组[5]:更新(x,y,#self.数据>5 and #self.数据> self.加入+5 )


       if  self.资源组[2]:事件判断()  then
	       	if self.选中 == 0 then
				tp.提示:写入("#y/请先选中一个帮派")
				return 0
			else
				客户端:发送数据(self.数据[self.选中].编号, 64, 13, "1")
			end

		elseif   self.资源组[4]:事件判断()  then
			self.加入=self.加入-5
		elseif   self.资源组[5]:事件判断()  then
	self.加入=self.加入+5
       elseif  self.资源组[1]:事件判断() or self.资源组[3]:事件判断() then
        self:打开()
       end
     zts:置颜色(0xFF000000)
		for m=1,5 do
			if self.数据[m + self.加入] ~= nil then
				bw:置坐标(self.x + 25,self.y +40+ m * 23)
				if self.选中~= m + self.加入 then
					if bw:检查点(x,y) then
						box(self.x + 23,self.y +40+ m * 23,self.x + 243,self.y + m * 22 + 62,-3551379)
						if 引擎.鼠标弹起(0) and self.鼠标 then
							self.选中 = m + self.加入
						end
						self.焦点 = true
					end
				else
					local ys = -10790181
					if bw:检查点(x,y) then
						ys = -9670988
						self.焦点 = true
					end
					box(self.x + 23,self.y +40+ m * 23,self.x + 243,self.y + m * 22 + 62,ys)
				end
				zts:显示(self.x + 25,self.y +42 + m * 23,self.数据[m + self.加入].名称)
				zts:显示(self.x + 125,self.y +42 + m * 23,self.数据[m + self.加入].编号)
				zts:显示(self.x + 170,self.y +42 + m * 23,self.数据[m + self.加入].帮主)
			end
		end
		if  self.数据[self.选中] ~= nil then
			self.介绍文本:清空()
           self.介绍文本:添加文本("#H/"..self.数据[self.选中].宗旨)
			self.介绍文本:显示(self.x + 30,self.y + 230)
		end

    zts:置颜色(0xFFFFFFFF)


end

return 加入帮派类









