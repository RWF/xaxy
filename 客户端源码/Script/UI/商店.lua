--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:47
--======================================================================--
local 场景类_商店 = class(窗口逻辑)
local tp,zts,zts1
local insert = table.insert
local xxx = 0
local yyy = 0
local sts = {"单价","数量","总额","现金"}
local tos = 引擎.取金钱颜色
local mouseb = 引擎.鼠标弹起
function 场景类_商店:初始化(根)
	self.ID = 17
	self.x = 224
	self.y = 100
	self.xx = 0
	self.yy = 0
	self.注释 = "商店"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 自适应.创建(99,1,272,401,3,9),
		[2] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
		[3] = 根._按钮(根._自适应(15,4,72,20,1),0,0,4,true,true,"  确定"),
		[5] = 自适应.创建(93,1,110,22,1,3),
		
	}
	for n=2,3 do
	   self.资源组[n]:绑定窗口_(17)
	end
	self.商品 = {}
	local 物品格子 = 根._物品格子
	for i=1,25 do
		self.商品[i] = 物品格子(0,i,n,"商店")
	end
	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('商店总控件')
	总控件:置可视(true,true)
	self.输入框 = 总控件:创建输入("数量输入",self.x + 220-326,self.y + 180,100,14)
	self.输入框:置可视(false,false)
	self.输入框:置限制字数(3)
	self.输入框:置数字模式()
	self.输入框:置光标颜色(-16777216)
	self.输入框:置文字颜色(-16777216)
	self.单价 = 0
	self.数量 = 0
	self.上一次 = 1
	self.窗口时间 = 0
	tp = 根
	zts = 根.字体表.华康字体
	zts1 = 根.字体表.华康字体
end
function 场景类_商店:刷新(数据)
		self.数据=数据
		self.组号=数据.组号
end
function 场景类_商店:打开(数据)
	if self.可视 then
		for i=1,25 do
			self.商品[i]:置物品(nil)
		end
		self.道具 = nil
		self.单价 = 0
		self.数量 = 0
		self.可视 = false
		self.商品[self.上一次].确定 = false
		self.上一次 = 1
		self.输入框:置焦点(false)
		self.输入框:置可视(false,false)
	else
		if  self.x > 全局游戏宽度 then
		self.x = 224
		end
		self.数据=数据
		self.组号=数据.组号
		insert(tp.窗口_,self)
		for n=1,#self.数据 do
			self.商品[n]:置物品(self.数据[n])
	  	end
	    tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_商店:显示(dt,x,y)
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y,self.道具 ~= nil)
	if self.资源组[2]:事件判断() then
		self:打开()
	elseif self.资源组[3]:事件判断() then


         客户端:发送数据(self.上一次, 3, 14, self.数量,self.组号)
	end
	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
	Picture.标题:显示(self.x+self.资源组[1].宽度/2-90,self.y)


	zts1:置颜色(0xFFFFFFFF):显示(self.x+108,self.y+3,"购  买")
	zts:置颜色(4294967295)
	zts:置字间距(15)
	for i=0,3 do
		self.资源组[5]:显示(self.x+110,self.y+247+i*29)
		zts:显示(self.x+57,self.y+251+i*29,sts[i+1])
	end
	zts:置字间距(0)
	Picture.物品格子背景:显示(self.x+9,self.y+29)
	self.资源组[2]:显示(self.x+246,self.y+3)
	self.资源组[3]:显示(self.x+113,self.y+370,true,1)
	local xx = 0
	local yy = 0
	for i=1,20 do
		self.商品[i]:置坐标(self.x + xx * 51 + 10,self.y + yy * 51 + 28)
		self.商品[i]:显示(dt,x,y,self.鼠标)
		if self.商品[i].物品 ~= nil and self.商品[i].焦点 then
			tp.提示:道具行囊(x,y,self.商品[i].物品,true)
			if mouseb(0) then
				if self.道具 == nil then
					self.商品[self.上一次].确定 = false
					self.商品[i].确定 = true
					self.上一次 = i
					self.道具 = self.商品[i].物品
					self.单价 = tostring(self.道具.价格)
					self.数量 = 1
					self.输入框:置可视(true,true)
				else
					if self.上一次 == i then
						self.数量 = self.数量 + 1
					else
						self.商品[self.上一次].确定 = false
						self.商品[i].确定 = true
						self.上一次 = i
						self.道具 = self.商品[i].物品
						self.单价 = tostring(self.道具.价格)
						self.数量 = 1
						self.输入框:置可视(true,true)
					end
				end
				self.输入框:置文本(self.数量)
			end
	       if mouseb(1) then
				if self.道具~= nil then
					if self.上一次 == i   then
						self.数量  = self.数量 - 1
					else
						self.商品[self.上一次].确定 = false
						self.商品[i].确定 = true
						self.上一次 = i
						self.道具 = self.商品[i].物品
						self.单价 = tostring(self.道具.价格)
						self.数量 = 1
						self.输入框:置可视(true,true)
					end
				end
				self.输入框:置文本(self.数量)
			end
		end
		xx = xx + 1
		if xx == 5 then
			xx = 0
			yy = yy + 1
		end
	end
	if self.道具 ~= nil then
		zts:置颜色(-16777216)
		zts:显示(self.x + 119,self.y + 252,self.单价)
		self.输入框:置坐标(self.x,self.y)
		if self.输入框:取文本() == "" then
			self.输入框:置文本(1)
		end
		if self.输入框:取文本()+0 > 99 then
			self.输入框:置文本(99)
		end
		if self.输入框:取文本()+0 < 0 then
			self.输入框:置文本(0)
		end
		self.数量 = tonumber(self.输入框:取文本())
		zts:显示(self.x + 118,self.y + 309,(self.数量 * self.单价))
	end
	zts:置颜色(tos(self.数据.银子))
	zts:显示(self.x + 118,self.y + 338,self.数据.银子)
	self.控件类:更新(dt,x,y)
	if self.输入框._已碰撞 then
		self.焦点 = true
	end
	self.控件类:显示(x,y)
end


return 场景类_商店