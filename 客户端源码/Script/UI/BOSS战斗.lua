--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:45
--======================================================================--
local 场景类_BOSS战斗 = class(窗口逻辑)
local tp,zts,zts1
local floor = math.floor
local tonumber = tonumber
local tostring = tostring
local insert = table.insert
	local sl = 0
	local js= 1
	local sjs =0
function 场景类_BOSS战斗:初始化(根)
	self.ID = 130
	self.x = 150+(全局游戏宽度-800)/2
	self.y = 180
	self.xx = 0
	self.yy = 0
	self.注释 = "BOSS战斗"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	local 资源 = 根.资源
	self.资源组 = {
	    [1] = 资源:载入('JM.dll',"网易WDF动画",0xA9A0E875),
	    [0] = 资源:载入('JM.dll',"网易WDF动画",0x24D08F02),
	    [2] = 资源:载入('JM.dll',"网易WDF动画",0x2436C9A1),
		[3] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),
       	[4] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x1A2D2115),0,0,4),

	}


	for i=3,4 do
		self.资源组[i]:绑定窗口_(130)
	end

  self.介绍文本 = 根._丰富文本(240,280,根.字体表.普通字体)
  self.介绍文本:添加元素(109,根.包子表情动画[109])
  self.介绍文本:添加文本("#G/点击#Y/“我要挑战”#W/你将挑战整个游戏里面难度最大怪物，点击#Y/“关闭”#W/将退出挑战，#Y/想到对应的难度也会获得相对应的奖励哦#W/快来试一下运气吧！#109")


	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.描边字体
end

function 场景类_BOSS战斗:打开(数据)
	if self.可视 then
		self.可视 = false

	else
		if  self.x > 全局游戏宽度 then
		self.x = 150+(全局游戏宽度-800)/2
		end
		self.数据 =数据
		self.开始 =nil
		self.结束 =nil
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_BOSS战斗:显示(dt,x,y)
	self.焦点 = false
   	self.资源组[1]:显示(self.x+0,self.y+0)

   	self.资源组[0]:显示(self.x+130,self.y-220)
   	self.资源组[2]:显示(self.x+70,self.y-40)
   	zts1:置颜色(0xFFFFFFFF):显示(self.x+160,self.y-25," 挑 战 世 界 B O S S ")
	self.资源组[3]:显示(self.x+445,self.y+0)
    self.资源组[4]:显示(self.x+160,self.y+10)
    zts1:置颜色(0xFFFFFFFF):显示(self.x+210,self.y+25," 我 要 挑 战 ")
	self.资源组[3]:更新(x,y)
    self.资源组[4]:更新(x,y)

	self.介绍文本:显示(self.x+80,self.y+189)

	if self.鼠标 then
		if self.资源组[3]:事件判断() then
          self:打开()
    	elseif self.资源组[4]:事件判断() then


           客户端:发送数据(self.数据[1],4,6,self.数据[2])

		end
	end



end




return 场景类_BOSS战斗