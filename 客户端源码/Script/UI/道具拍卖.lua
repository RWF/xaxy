--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--
local 场景类_道具拍卖 = class(窗口逻辑)

local tp,zts,zts1
local floor = math.floor
local tonumber = tonumber
local tostring = tostring
local insert = table.insert
local xs = {[0]="当前银两",[1]="竞拍价格"}
local jqr = 引擎.取金钱颜色

function 场景类_道具拍卖:初始化(根)
	self.ID = 120
	self.x = 280
	self.y = 86
	self.xx = 0
	self.yy = 0
	self.注释 = "道具拍卖"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 自适应.创建(0,1,272,319,3,9),
		[2] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),
		[3] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true," 竞 拍 "),
		[4] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true," 取 消 "),
		[5] = 自适应.创建(1,1,236,18,1,3,nil,18),
		[6] = 自适应.创建(3,1,90,19,1,3)
	}
	for n=2,4 do
	   self.资源组[n]:绑定窗口_(120)
	end
	self.物品 = {}
	local 格子 = 根._物品格子
	for i=1,20 do
		self.物品[i] = 格子(0,0,i,"道具拍卖")
	end
	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('道具拍卖总控件')
	总控件:置可视(true,true)
	self.输入框 = 总控件:创建输入("数量输入",self.x + 110,self.y + 304,100,14)
	self.输入框:置可视(false,false)
	self.输入框:置数字模式()
	self.输入框:置光标颜色(-16777216)
	self.输入框:置文字颜色(0xFF000000)
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.描边字体
end
function 场景类_道具拍卖:刷新(数据)
		for i=1,20 do
		self.物品[i]:置物品(数据[i])
		end
		self.数据=数据
		self.道具 = nil
		if self.上一次 ~= nil then
			self.物品[self.上一次].确定 = false
		end
		self.上一次 = nil
end
function 场景类_道具拍卖:打开(数据)
	if self.可视 then
		self.道具 = nil
		self.可视 = false
		if self.上一次 ~= nil then
			self.物品[self.上一次].确定 = false
		end
		self.上一次 = nil
		self.输入框:置可视(false,false)
	else
		if  self.x > 全局游戏宽度 then
		self.x = 280
		end
		for i=1,20 do
		self.物品[i]:置物品(数据[i])
		end
		self.数据=数据
		if self.上一次 ~= nil then
			self.物品[self.上一次].确定 = false
		end
		self.上一次 = nil
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_道具拍卖:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y,self.道具 ~= nil )
	self.资源组[4]:更新(x,y)

	if self.鼠标 then
		if self.资源组[2]:事件判断() then
			self:打开()
		elseif self.资源组[3]:事件判断() then
          客户端:发送数据(self.上一次,203,13,self.输入框:取文本())
		elseif self.资源组[4]:事件判断() then
			self:打开()
		end
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[5]:显示(self.x+6,self.y+3)
	Picture.窗口标题背景_:置区域(0,0,70,16)
	Picture.窗口标题背景_:显示(self.x+97,self.y+3)
	zts1:显示(self.x+109,self.y+3,"拍  卖")
	Picture.物品界面背景_:显示(self.x+9,self.y+30)
	tp.横排花纹背景_:置区域(3,0,267,18)
	tp.横排花纹背景_:显示(self.x+2,self.y+242)
	zts:置颜色(4294967295)
	for i=0,1 do
		zts:显示(self.x+18,self.y+272+i*24,xs[i])
		self.资源组[6]:显示(self.x+78,self.y+268+i*24)
	end
	self.资源组[2]:显示(self.x+247,self.y+6)
	self.资源组[3]:显示(self.x+188,self.y+262,true)
	self.资源组[4]:显示(self.x+188,self.y+290,true)
	local xx = 0
	local yy = 0
	for i=1,20 do
		local jx = xx * 51 + 10
		local jy = yy * 51 + 30
		self.物品[i]:置坐标(self.x + jx,self.y + jy)
		self.物品[i]:显示(dt,x,y,self.鼠标)
		if self.物品[i].焦点 and self.物品[i].物品 ~= nil then
			tp.提示:道具行囊(x,y,self.物品[i].物品)
			if self.物品[i].事件 then
				if self.上一次 ~= nil and self.上一次 ~= 0 then
					self.物品[self.上一次].确定 = false
				end
				self.物品[i].确定 = true
				self.道具 = self.物品[i].物品
				self.输入框:置文本(1)
				self.输入框:置可视(true,true)
				self.上一次 = i
			end
		end

		xx = xx + 1
		if xx == 5 then
			xx = 0
			yy = yy + 1
		end
	end
	zts:置颜色(-16777216)

	if self.道具 ~= nil  then
		self.输入框:置坐标(self.x-300,self.y-95)
		-- if self.输入框:取文本()~=""and  self.输入框:取文本()+0 > self.数据.银子 then
		--    self.输入框:置文本(self.数据.银子)
		-- end

	end
	zts:置颜色(jqr(self.数据.银子))
	zts:显示(self.x + 90,self.y + 272,self.数据.银子)
	self.控件类:更新(dt,x,y)
	self.控件类:显示(x,y)
	if self.输入框._已碰撞  then
		self.焦点 = true
	end
end




return 场景类_道具拍卖