--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:48
--======================================================================--
local 场景类_成就弹窗 = class(窗口逻辑)
local tp,zts,zts1
local insert = table.insert
function 场景类_成就弹窗:初始化(根)
    self.ID = 378
    self.x = 247
    self.y = 446
    self.xx = 0
    self.yy = 0
    self.注释 = "成就弹窗"
    self.可视 = false
    self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    local 按钮 = 根._按钮
    local 自适应 = 根._自适应
    self.资源组 = {

        [1] = 根.资源:载入('JM.dll',"网易WDF动画",0x30F6DB85),
        [2] = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true),

        [3] = 根.资源:载入('AF83732B',"动画"),
        [4] = 根.资源:载入('E6490543',"动画"),
        [5] = 根.资源:载入('FC43594C',"动画"),
        [6] = 根.资源:载入('F64ACC8C',"动画"),
    }

--     self.表情框={
--     无级别={[1]=根.资源:载入('808383AA',"动画"),[2]="",[3]=""},
-- }

       self.资源组[2]:绑定窗口_(378)
    self.窗口时间 = 0
    tp = 根
    zts = tp.字体表.窗口字体
    zts1 = tp.字体表.楷体
end

function 场景类_成就弹窗:打开(内容)
    if self.可视 then
       
        self.可视 = false
       
    else
        self.data =内容
        if  self.x > 全局游戏宽度 then
        self.x = 247
        end
        insert(tp.窗口_,self)
        tp.运行时间 = tp.运行时间 + 1
        self.窗口时间 = tp.运行时间
        self.可视 = true
    end
end

function 场景类_成就弹窗:显示(dt,x,y)
    self.焦点 = false
   
    if self.鼠标 then
        if self.资源组[2]:事件判断() then
            self:打开()
        end
    end
    self.资源组[1]:显示(self.x,self.y)
    self.资源组[2]:显示(self.x+337,self.y+17)
    self.资源组[2]:更新(x,y)
    self.资源组[4]:显示(self.x+40,self.y+35)--表情框
    self.资源组[5]:显示(self.x+40,self.y+35)--无级别表情
    zts1:置颜色(0xFF000000):显示(self.x+100,self.y+35,self.data.标题)--"牛了！无级别啊"
    zts:置颜色(0xFF000000):显示(self.x+100,self.y+65,self.data.文本)--"鉴定至少80级装备出现特效无级别"
    
    
    self.资源组[6]:显示(self.x,self.y)--特效
    self.资源组[6]:更新(dt)
    self.资源组[3]:显示(self.x,self.y)--烟花
    self.资源组[3]:更新(dt)


end




return 场景类_成就弹窗