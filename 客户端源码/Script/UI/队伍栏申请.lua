--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--
local 场景类_队伍栏申请 = class(窗口逻辑)
local insert = table.insert
local remove = table.remove
local floor = math.floor
local tp,zts1

function 场景类_队伍栏申请:初始化(根)
	self.ID = 55
	self.x = 82+(全局游戏宽度-800)/2
	self.y = 169
	self.xx = 0
	self.yy = 0
	self.注释 = "队伍栏"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 根.资源:载入('JM.dll',"网易WDF动画",0x8FCD5E87),
		[2] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),
        [3] = 按钮.创建(自适应.创建(12,4,55,22,1,3),0,0,4,true,true,"同意"),
        [4] = 按钮.创建(自适应.创建(12,4,55,22,1,3),0,0,4,true,true,"拒绝"),

	    ----- 申请列表  --B4917324红色令牌---8072710D阵法选界面
	}
		self.资源组[2]:绑定窗口_(12)
	self.队伍坐标 = {14,133,252,371,490}
	self.队伍格子 = {}
	self.格子 = require("script/System/队伍_格子")
	for i=1,5 do
		self.队伍格子[i] = self.格子.创建(0,0,i,根)
	end
	self.选中人物 = 0
	self.窗口时间 = 0
	tp = 根

   zts1 = tp.字体表.描边字体
end
function 场景类_队伍栏申请:刷新(内容)
		if 内容~=nil then
		self.临时文本=内容
		for i=1,5 do
		self.队伍格子[i] = self.格子.创建(0,0,i,根)
	   end
			for n=1,#self.临时文本 do
			self.队伍数据[n]=self.临时文本[n]
			self.队伍格子[n]:置人物(self.队伍数据[n])
			end
		end
	end
function 场景类_队伍栏申请:打开(内容)
   self.队伍数据={}
  if self.可视 then
		for i=1,5 do
			self.队伍格子[i].禁止 = false
		end
		self.选中人物 = 0
		self.可视 = false
  else
  							                    if  self.x > 全局游戏宽度 then
self.x = 82+(全局游戏宽度-800)/2
    end
  	   队伍消息 =false
		insert(tp.窗口_,self)
		if 内容~=nil then
		self.临时文本=内容
			for n=1,#self.临时文本 do
			self.队伍数据[n]=self.临时文本[n]
			self.队伍格子[n]:置人物(self.队伍数据[n])
			end
		end
		tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.可视 = true
  end
 end
function 场景类_队伍栏申请:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y,self.选中人物 ~= 0 )
	self.资源组[4]:更新(x,y,self.选中人物 ~= 0 )
	if self.资源组[2]:事件判断() then
		self:打开()
		return false
	elseif self.资源组[3]:事件判断() then
客户端:发送数据(self.选中人物, 8, 11, "66", 1)
	elseif self.资源组[4]:事件判断() then
客户端:发送数据(self.选中人物, 9, 11, "66", 1)
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x + 593,self.y + 5)
	self.资源组[3]:显示(self.x + 440,self.y + 260,true)
	self.资源组[4]:显示(self.x + 531,self.y + 260,true)

	for i=0,4 do
		local jx = 11+i*119
		i = i + 1
		self.队伍格子[i]:置坐标(self.x+ jx,self.y + 37)
		self.队伍格子[i]:显示(dt,x,y,self.鼠标,1)
		if self.队伍格子[i].事件 then
			if self.选中人物 ~= 0 and self.选中人物 ~= i then
			   self.队伍格子[self.选中人物].禁止 = false
		  		self.选中人物 = 0
			else
				self.选中人物 = i
				self.队伍格子[self.选中人物].禁止 = true
			end
	   end
		zts1:显示(self.x+jx+92,self.y+155,i)
	end



end





return 场景类_队伍栏申请