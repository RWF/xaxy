--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--

local 场景类_任务栏 = class()
local ceil = math.ceil
local bw = require("gge包围盒")(0,0,148,18)
local box = 引擎.画矩形
local insert = table.insert
local remove = table.remove

local xys = 生成XY
local floor = math.floor
local format = string.format



local tp,font,fonts1
local min = math.min
local max = math.max
local mouseb = 引擎.鼠标弹起

function 场景类_任务栏:初始化(根)
  self.ID = 129
  self.x = 213+(全局游戏宽度-800)/2
  self.y = 125
  self.xx = 0
  self.yy = 0
  self.注释 = "任务栏"
  self.可视 = false
  self.鼠标 = false
  self.焦点 = false
  self.可移动 = true
  local 资源 = 根.资源
  local 按钮 = 根._按钮
  local 滑块 = 根._滑块
  local 自适应 = 根._自适应
  self.资源组 = {
    [1] = 自适应.创建(99,1,430,353,3,9),
    [2] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
    [3] = 按钮.创建(自适应.创建(20,4,18,19,4,3),0,0,4,true,true),
    [4] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),
    [5] = 按钮.创建(自适应.创建(20,4,18,19,4,3),0,0,4,true,true),
    [6] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),
    [7] =  滑块.创建(自适应.创建(11,4,15,40,2,3,nil),4,14,210,2),
    [10] = 滑块.创建(自适应.创建(11,4,15,40,2,3,nil),4,14,213,2),
    [11] = 按钮.创建(自适应.创建(103,4,72,22,1,3),0,0,4,true,true,"自动寻路"),

    [15] = 自适应.创建(4,1,1,1,3,nil),



  }
  for n=2,6 do
      self.资源组[n]:绑定窗口_(129)
  end

      self.资源组[11]:绑定窗口_(129)

  self.介绍文本 = 根._丰富文本(174,215)
  self.介绍文本:添加元素("ms",require("gge精灵类")(require("gge纹理类")(程序目录.."Data/pic/0073.png")))
    self.介绍文本:添加元素("xxx",0xFFFF8000)
     self.介绍文本:添加元素("yyy",0xFF8080FF)
  self.选中 = 0
  self.加入 = 0
  self.介绍加入 = 0
  self.窗口时间 = 0
  self.状态 = 1
  font = 根.字体表.华康字体
  fonts1 = 根.字体表.华康字体
  tp = 根
end

function 场景类_任务栏:打开()
  if self.可视 then
    self.选中 = 0
    self.加入 = 0
    self.介绍加入 = 0
    self.介绍文本:清空()
    self.可视 = false
  else
                                if  self.x > 全局游戏宽度 then
self.x = 213+(全局游戏宽度-800)/2
    end
    insert(tp.窗口_,self)
    self.状态 = 1
      tp.运行时间 = tp.运行时间 + 1
      self.窗口时间 = tp.运行时间
      self.可视 = true
  end
end




function 场景类_任务栏:显示(dt,x,y)
  self.焦点 = false
  self.资源组[2]:更新(x,y)
  self.资源组[11]:更新(x,y,self.选中~=0)
  self.资源组[3]:更新(x,y,self.加入 > 0)
  self.资源组[4]:更新(x,y,self.加入 < #tp.场景.假人 - 11)
  self.资源组[5]:更新(x,y,self.介绍加入 > 0)
  self.资源组[6]:更新(x,y,self.介绍加入 < #self.介绍文本.显示表 - 13)
  if self.资源组[2]:事件判断() then
    self:打开()
    return false
  elseif self.资源组[3]:事件判断() then
    self.资源组[7]:置起始点(self.资源组[7]:取百分比转换(self.加入-1,11,#tp.场景.假人))
  elseif self.资源组[4]:事件判断() then
    self.资源组[7]:置起始点(self.资源组[7]:取百分比转换(self.加入+1,11,#tp.场景.假人))
  elseif self.资源组[5]:事件判断() then
    self.资源组[10]:置起始点(self.资源组[10]:取百分比转换(self.介绍加入-1,13,#self.介绍文本.显示表))
  elseif self.资源组[6]:事件判断() then
    self.资源组[10]:置起始点(self.资源组[10]:取百分比转换(self.介绍加入+1,13,#self.介绍文本.显示表))
  elseif self.资源组[11]:事件判断()   then


        local a = xys(floor(tp.角色坐标.x / 20),floor(tp.角色坐标.y / 20))
        local b = xys(tp.场景.假人[self.选中].X,tp.场景.假人[self.选中].Y)
        local wb = tp.场景.地图.寻路:寻路(a,b,true)
        tp.场景.人物.路径组 = wb
        -- local 发送数据 =""
        -- for i=1,#tp.场景.人物.路径组 do
        --   发送数据=发送数据..tp.场景.人物.路径组[i].x.."*-*"..tp.场景.人物.路径组[i].y.."@-@"
        -- end
        --  客户端:发送数据(0,1,4,发送数据,1)
   客户端:发送数据(0,1,4,tp.场景.假人[self.选中].X.."*-*"..tp.场景.假人[self.选中].Y,1)
    self.选中 = 0
    self.加入 = 0
    self.介绍加入 = 0
    self.介绍文本:清空()
    self.资源组[10]:置起始点(0)
    self:打开()
  end
  self.资源组[1]:显示(self.x,self.y)
  Picture.标题:显示(self.x+self.资源组[1].宽度/2-90,self.y)
  self.资源组[2]:显示(self.x + 403,self.y + 6)


  fonts1:置颜色(0xFFFFFFFF):显示(self.x+176,self.y+3,"自动寻路")

  self.资源组[11]:显示(self.x + 320,self.y + 30,true,1)

    self.资源组[15]:置宽高(159,251)
    self.资源组[15]:显示(self.x+15,self.y+63)
    self.资源组[15]:置宽高(190,251)
    self.资源组[15]:显示(self.x+204,self.y+63)
    tp.字体表.描边字体_:置字间距(5)
    tp.字体表.描边字体_:显示(self.x+52,self.y+68,"NPC列表")
    tp.字体表.描边字体_:显示(self.x+259,self.y+68,"NPC详细")
    tp.字体表.描边字体_:置字间距(0)
    tp.画线:置区域(0,0,15,209)
    tp.画线:显示(self.x+179,self.y+85)
    tp.画线:显示(self.x+399,self.y+85)
    self.资源组[3]:显示(self.x + 176,self.y + 66)
    self.资源组[4]:显示(self.x + 176,self.y + 294)
    self.资源组[5]:显示(self.x + 396,self.y + 66)
    self.资源组[6]:显示(self.x + 396,self.y + 294)
    if #tp.场景.假人 > 11 then
      self.加入 = min(ceil((#tp.场景.假人-11)*self.资源组[7]:取百分比()),#tp.场景.假人-11)
      self.资源组[7]:显示(self.x + 176,self.y + 84,x,y,self.鼠标)
    end
    if #self.介绍文本.显示表 > 13 then
      self.介绍加入 = min(ceil((#self.介绍文本.显示表-13)*self.资源组[10]:取百分比()),#self.介绍文本.显示表-13)
      self.介绍文本.加入 = self.介绍加入
      self.资源组[10]:显示(self.x + 395,self.y + 84,x,y,self.鼠标)
    end
    font:置颜色(-16777216)
    font:置阴影颜色(nil)
    for n=1,11 do
      if((n + self.加入) <= (11 + self.加入)) and tp.场景.假人[n + self.加入]~=nil then
        bw:置坐标(self.x + 17,self.y + 89 + (n-1)*19.5)
        if self.选中 ~= self.加入 + n then
          if bw:检查点(x,y) then
            box(self.x + 21,self.y + 93 + (n-1)*19.5,self.x + 169,self.y + 93 + (n-1)*19.5+18,-3551379)
            if mouseb(0) and self.鼠标 and not tp.消息栏焦点 then
              self.选中 = n + self.加入
              self.介绍文本:清空()
              self.介绍文本:添加文本("#L/◆名称："..tp.场景.假人[self.选中].名称)
              self.介绍文本:添加文本("#xxx/◆X坐标："..math.floor(tp.场景.假人[self.选中].X))
              self.介绍文本:添加文本("#xxx/◆Y坐标："..math.floor(tp.场景.假人[self.选中].Y))
             self.介绍文本:添加文本("")
                self.介绍文本:添加文本("")
                  self.介绍文本:添加文本("#ms")
              if tp.场景.假人[self.选中].执行事件  then
                  self.介绍文本:添加文本("#yyy/"..tp.场景.假人[self.选中].事件)
               else
                self.介绍文本:添加文本("#yyy/这个NPC暂时无描述！")
              end
              for i=1,#self.介绍文本.显示表 - 13 do
                self.介绍文本:滚动(1)
              end
              self.资源组[10]:置起始点(0)
              self.介绍加入 = 0
            end
            self.焦点 = true
          end
        else
          local ys = -10790181
          if bw:检查点(x,y) then
            ys = -9670988
            self.焦点 = true
          end
          box(self.x + 21,self.y + 93 + (n-1)*19.5,self.x + 169,self.y + 93 + (n-1)*19.5+18,ys)
        end
        font:显示(self.x+30,self.y+95+(n-1)*19.5,tp.场景.假人[n + self.加入].名称)
      end
    end
    self.介绍文本:显示(self.x + 208,self.y + 95.5)
  
  if self.资源组[7].接触 or self.资源组[10].接触 then
    self.焦点 = true
  end
end

function 场景类_任务栏:检查点(x,y)
  if self.资源组[1]:是否选中(x,y)  then
    return true
  end
end

function 场景类_任务栏:初始移动(x,y)
  tp.运行时间 = tp.运行时间 + 1
  if not tp.消息栏焦点 then
      self.窗口时间 = tp.运行时间
  end
  if not self.焦点 then
    tp.移动窗口 = true
  end
  if self.鼠标 and  not self.焦点 then
    self.xx = x - self.x
    self.yy = y - self.y
  end
end

function 场景类_任务栏:开始移动(x,y)
  if self.鼠标 then
    self.x = x - self.xx
    self.y = y - self.yy
  end
end

return 场景类_任务栏