--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:47
--======================================================================--
local 场景类_好友列表 = class(窗口逻辑)
local min = math.min
local floor = math.floor
local tp,zts,zts1

local bw = require("gge包围盒")(0,0,143,55)
local mouseb = 引擎.鼠标弹起
local box = 引擎.画矩形
local insert = table.insert

function 场景类_好友列表:初始化(根)
	self.ID = 28
	self.x = 610+(全局游戏宽度-800)/2
	self.y = 26
	self.xx = 0
	self.yy = 0
	self.注释 = "好友列表"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.焦点1 =false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应

-- 	团团 22:27:43
--    放大镜 ，

--  A7C69CF6  不知道什么



	self.资源组 = {
		[1] = 根.资源:载入("好友栏.png","加密图片"),
		[2]=按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x2E6DCEC8)),
		[3]=按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFCD5C71C)),
		[4] = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true),
		[5] = 按钮.创建(自适应.创建(16,4,72,22,1,3),0,0,4,true,true,"快速添加"),
		[6] = 按钮.创建(自适应.创建(20,4,15,19,4,3),0,0,4,true,true), --上
		[7] = 按钮.创建(自适应.创建(21,4,15,19,4,3),0,0,4,true,true),--下
		[8] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xC47885C6)),--加好友
		[9] =  根._滑块.创建(自适应.创建(11,4,15,40,2,3,nil),4,14,365,2),
		[10] = 按钮.创建(自适应.创建(16,4,72,22,1,3),0,0,4,true,true,"查找好友"),

	}
	for n=2,8 do
	   self.资源组[n]:绑定窗口_(28)
	end
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.描边字体
	self.选中 = 0
	self.加入 = 0
end

function 场景类_好友列表:打开(数据)
	if self.可视 then
		self.可视 = false
		self.资源组[9]:置起始点(0)
		self.选中 = 0
		self.加入 = 0
	else
				                    if  self.x > 全局游戏宽度 then
self.x = 612+(全局游戏宽度-800)/2
    end
		self.数据 =数据
		-- for i=1,#self.数据 do
		-- 		if i > 8 then
		-- 			self.加入 = i-8
		-- 			self.资源组[9]:置起始点(self.资源组[9]:取百分比转换(self.加入,8,#self.数据))
		-- 		end
		-- 		self.选中 = i
		-- end
		self.资源组[9]:置起始点(0)
		insert(tp.窗口_,self)
		self.头像组 = {}
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_好友列表:显示(dt,x,y)
	self.资源组[4]:更新(x,y)
	self.资源组[2]:更新(x,y)
    self.资源组[3]:更新(x,y)
	self.资源组[5]:更新(x,y)
	self.资源组[6]:更新(x,y,self.加入 > 0)
	self.资源组[7]:更新(x,y,self.加入 < #self.数据 - 8)
	self.资源组[8]:更新(x,y)
	self.资源组[10]:更新(x,y)
	if self.资源组[4]:事件判断() then
		self:打开()
		return false
	elseif self.资源组[5]:事件判断() or self.资源组[8]:事件判断() then

     	tp.好友开关 =true
	tp.鼠标.置鼠标("好友")
	elseif self.资源组[6]:事件判断() then
		--self.加入 = self.加入 - 4
		self.资源组[9]:置起始点(self.资源组[9]:取百分比转换(self.加入-4,4,#self.数据))
	elseif self.资源组[7]:事件判断() then
		--self.加入 = self.加入 + 4
		self.资源组[9]:置起始点(self.资源组[9]:取百分比转换(self.加入+4,4,#self.数据))
	elseif self.资源组[10]:事件判断() or self.资源组[2]:事件判断() then
      tp.窗口.好友查找:打开()
	end
	self.焦点1 =false
	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)




	self.资源组[4]:显示(self.x+167,self.y+4,true)
	self.资源组[5]:显示(self.x+16,self.y+55)
	self.资源组[10]:显示(self.x+94,self.y+55,true)


	self.资源组[6]:显示(self.x+166,self.y+84)
	self.资源组[7]:显示(self.x+166,self.y+492)

	self.资源组[8]:显示(self.x+35,self.y+517)
	self.资源组[2]:显示(self.x+75,self.y+517)
	self.资源组[3]:显示(self.x+115,self.y+517)
	zts:置颜色(-16777216)
	if #self.数据 > 8 then
		self.资源组[9]:置高度(min(floor(390/(#self.数据-8)),374))
		self.加入 = min(math.ceil((#self.数据-8)*self.资源组[9]:取百分比()),#self.数据-8)
        self.资源组[9]:显示(self.x+168,self.y+125,x,y,self.鼠标)
	end

	for i=1,8 do
		if self.数据[i+self.加入] ~= nil then
			local jx = self.x+19
			local jy = self.y+35+i*53
			bw:置坐标(jx-1,jy-5)
			if self.头像组[i+self.加入] == nil or self.头像组[i+self.加入][1] ~= self.数据[i+self.加入].造型 then
				self.头像组[i+self.加入] = {self.数据[i+self.加入].造型}
				local n =  ModelData[self.头像组[i+self.加入][1]]
				self.头像组[i+self.加入][2] = tp.资源:载入(n.头像资源,"网易WDF动画",n.中头像)
			end
			local xz
			if bw:检查点(x,y) then
				self.焦点1 =true
				xz = true
			  if 引擎.鼠标按下(1) and self.鼠标 and not tp.消息栏焦点 then
			  引擎.场景.窗口.好友查看:打开(self.数据[i+self.加入],i+self.加入)
		       end
			end
			if i+self.加入 ~= self.选中 then
				if xz then
					if mouseb(0) and self.鼠标 and not tp.消息栏焦点 then
						self.选中 = i+self.加入
					end
					self.焦点 = true
				end
			else
 				if bw:检查点(x,y)  and 引擎.鼠标按下(0) and self.鼠标 and not tp.消息栏焦点 then
				 tp.窗口.好友聊天:打开(self.数据[i+self.加入])
		       end

				if xz and self.鼠标 and not tp.消息栏焦点 then
					self.焦点 = true
				end
				box(jx-1,jy-3,jx+142,jy+53,-10790181)
			end
		    tp.人物头像背景_:显示(jx,jy)
		    zts:显示(jx+52,jy+4,self.数据[i+self.加入].名称)
		    zts:显示(jx+52,jy+19,self.数据[i+self.加入].等级.."级")
		    self.头像组[i+self.加入][2]:显示(jx+3,jy+3)
		end
	end
	if self.资源组[9].接触 then
		self.焦点 = true
	end
end


return 场景类_好友列表