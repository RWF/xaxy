



local 光武拓印 = class(窗口逻辑)
local insert = table.insert
local remove = table.remove
local floor = math.floor
local tp,zts,zts1
function 光武拓印:初始化(根)
    self.ID = 317
    self.x = 297+(全局游戏宽度-800)/2
    self.y = 157
    self.xx = 0
    self.yy = 0
    self.注释 = "成就"
    tp = 根
        self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    local 按钮 = 根._按钮
    local 自适应 = 根._自适应
    local  资源= 根.资源
    self.资源组 = {
        [1] = Picture.改变造型,
        [2] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
        [3]=按钮.创建(自适应.创建(101,4,72,22,1,3),0,0,4,true,true," 取 消"),
        [4]=按钮.创建(自适应.创建(15,4,72,20,1,3),0,0,4,true,true," 确 定"),
        [5]=按钮.创建(自适应.创建(15,4,72,20,1,3),0,0,4,true,true,"选择武器"),
        [6]=按钮.创建(自适应.创建(15,4,45,20,1,3),0,0,4,true,true,"兑换"),
    }
    self.选择物品 = {}
    local 格子 = 根._物品格子
    for i=1,3 do
        self.选择物品[i] = 格子(0,0,i,"选择物品",底图)
    end

    for i=2,6 do
      self.资源组[i]:绑定窗口_(317)

    end
    self.选择物品ID =0
    self.物品ID =0
    self.翻页 =false
    self.窗口时间 = 0
    zts1 = tp.字体表.华康字体

end


function 光武拓印:刷新(内容)
        self.仙玉=内容.仙玉

        self.物品ID=内容.格子+0
        local Name= 内容.物品.名称
        if WeaponModel[Name] then
            
             local 物品1=table.copy(内容.物品)
             local 物品2=table.copy(内容.物品)
             物品1.名称 =WeaponModel[Name]["武器类型1"]
             物品2.名称 =WeaponModel[Name]["武器类型2"]
             self.选择物品[1]:置物品(内容.物品)
            self.选择物品[2]:置物品(物品1)

           self.选择物品[3]:置物品(物品2)
      
           

             self.选择物品[1].确定=false
             self.选择物品[2].确定=false
             self.选择物品[3].确定=false
             self.选择物品ID =0
        else 
            tp.提示:写入("#Y/"..Name.."没有这个造型的拓印数据")
        end
    
end
function 光武拓印:打开(内容)
  if self.可视 then
        self.可视 = false
  else
        self.选择物品ID =0
        self.物品ID =0
        for i=1,3 do
         self.选择物品[i]:置物品(nil)
         self.选择物品[i].确定=false
        end
        if  self.x > 全局游戏宽度 then
        self.x = 82+(全局游戏宽度-800)/2
        end
        insert(tp.窗口_,self)
        tp.运行时间 = tp.运行时间 + 1
        self.窗口时间 = tp.运行时间
        self.可视 = true
  end
 end
function 光武拓印:显示(dt,x,y)
    self.焦点 = false

    self.资源组[1]:显示(self.x,self.y)
    zts1:显示(self.x+176,self.y+54,"选择可变造型")
    self.资源组[2]:显示(self.x+287,self.y+3,true,1)
    self.资源组[3]:显示(self.x+220,self.y+172,true,1)
    self.资源组[4]:显示(self.x+220,self.y+202,true,1)
    self.资源组[5]:显示(self.x+38,self.y+58,true,1)
    self.资源组[6]:显示(self.x+168,self.y+172,true,1)
    for i=2,6 do
       if i==4 then
        
        self.资源组[i]:更新(x,y,self.选择物品ID ~=0)
       else
        self.资源组[i]:更新(x,y)
       end
       if self.资源组[i]:事件判断() then
           if i==2 or i==3 then 
                self:打开()
           elseif i==5 then
             客户端:发送数据(91,91,13)  
           elseif i==4 then 
              if self.仙玉 < 3000 then
                    tp.提示:写入("#Y/当前仙玉不足无法进行拓印")
              else 


                 客户端:发送数据(self.物品ID,self.选择物品ID,70)  
                self:打开()
              end 
           end
       end
    end
   self.选择物品[1]:置坐标(self.x +49,self.y +82)
   self.选择物品[2]:置坐标(self.x +162,self.y +82)
   self.选择物品[3]:置坐标(self.x +226,self.y +82)
   if self.物品ID~=0 then
      
      zts1:显示(self.x+85,self.y+172,self.仙玉)
      zts1:显示(self.x+85,self.y+202,3000)
   end
   for i=1,3 do
       self.选择物品[i]:显示(dt,x,y,self.鼠标,nil,0.7)
        if self.选择物品[i].物品 ~= nil and self.选择物品[i].焦点 then
            tp.提示:道具行囊(x,y,self.选择物品[i].物品,true)
        end
        if i~=1 and self.选择物品ID ==0 and self.物品ID~=0 and self.选择物品[i].事件  then
            self.选择物品[i].确定=true
            self.选择物品ID=i-1
        end
   end
end




return 光武拓印



