-- @Author: 作者QQ414628710
-- @Date:   2022-01-15 20:06:44
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-03-22 02:53:32
--======================================================================--
-- 召唤兽查看栏
--======================================================================--
local 场景类_召唤兽查看栏 = class(窗口逻辑)
local yx = {{68,4},{25,28},{111,28},{24,75},{111,75},{68,97}}
local floor = math.floor
local xslb,lb,tp,zts1
local format = string.format
local insert = table.insert

function 场景类_召唤兽查看栏:初始化(根)
	self.ID = 35
	self.x = 211+(全局游戏宽度-800)/2
	self.y = 117
	self.xx = 0
	self.yy = 0
	self.注释 = "召唤兽查看栏"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	self.资源组 = {
		[1] = Picture.召唤兽查看界面,
		[2] = 按钮.创建(根.资源:载入('JM.dll',"网易WDF动画",0xF11233BB),0,0,4,true,true),
		[3] =  按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x86D66B9A),0,0,4,true,true,"显示类型"),
		[4] =  按钮.创建(根.资源:载入('JM.dll',"网易WDF动画",0x7AB5584C),0,0,3,true,true),
		[5] =  按钮.创建(根.资源:载入('JM.dll',"网易WDF动画",0xCB50AB1D),0,0,3,true,true),
		[6] = 资源:载入('JM.dll',"网易WDF动画",0x68D384BD),
		[7] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x1F996671),0,0,4,true,true),
		[8] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x9C24F376),0,0,4,true,true),
		[9] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xCD999F0B),0,0,4,true,true),



		-- [10] = 资源:载入('JM.dll',"网易WDF动画",0x10E2B4A7),
		[11] = 资源:载入('JM.dll',"网易WDF动画",0xC361C087),
		[12] = 资源:载入('JM.dll',"网易WDF动画",0x7367031D),
		[13] = 资源:载入('JM.dll',"网易WDF动画",0x1E714129),
		[14] = 资源:载入('JM.dll',"网易WDF动画",0xF2FC2425),
	}
self.进阶 ={
        [16] = 资源:载入('JM.dll',"网易WDF动画",0x1094AD16),----进阶界面
 	    [1] = 资源:载入('JM.dll',"网易WDF动画",0x4536A03D),--进阶1
		[2] = 资源:载入('JM.dll',"网易WDF动画",0x714C3706),--2
		[3] = 资源:载入('JM.dll',"网易WDF动画",0xD60014B8),-----3
		[4] = 资源:载入('JM.dll',"网易WDF动画",0xF7EBF987), ----4
		[5] = 资源:载入('JM.dll',"网易WDF动画",0x11963488),----5
		[6] = 资源:载入('JM.dll',"网易WDF动画",0x9A4F1961),--6
		[7] = 资源:载入('JM.dll',"网易WDF动画",0x1E7ABB94), --7
		[8] = 资源:载入('JM.dll',"网易WDF动画",0xA6C9A76A),--8
		[9] = 资源:载入('JM.dll',"网易WDF动画",0x2982E3F7),---9
		[10] = 资源:载入('JM.dll',"网易WDF动画",0x1D0717D7),---91
		[11] = 资源:载入('JM.dll',"网易WDF动画",0xC44F0602),--94
		[12] = 资源:载入('JM.dll',"网易WDF动画",0x9765D0B3),--97
		[13] = 资源:载入('JM.dll',"网易WDF动画",0x36A2C1A6),--100
		[14] = 资源:载入('JM.dll',"网易WDF动画",0xAFC2E161), --未完成内框
		[15] = 资源:载入('JM.dll',"网易WDF动画",0x27E24CFA), --完成
   }
   	self.内丹={}
	local nd = 根._内丹格子
		for i=1,12 do
	    self.内丹[i] = nd(0,0,i,"召唤兽内丹")
	end
	self.技能 = {}
	self.状态 = 1
	for n=2,5 do
	    self.资源组[n]:绑定窗口_(35)
	end
	for n=7,9 do
	    self.资源组[n]:绑定窗口_(35)
	end
	for n=1,36 do
	  	self.技能[n] = 根._技能格子(0,0,n,"技能查看")
	end
	self.窗口时间 = 0
	tp = 根
	self.圣兽丹动画 =根.资源:载入('JM.dll',"网易WDF动画",0xA4F1E391)
				self.技能数量 = 0
			self.技能页数 =0
	zts1 = tp.字体表.普通字体
end
function 场景类_召唤兽查看栏:刷新(b,l)
		self.data = b
		lb = l
		if self.data.类型 =="孩子" then
			self.资源组[3] =  tp._按钮(tp.资源:载入('JM.dll',"网易WDF动画",0x86D66B9A),0,0,4,true,true,"设置参战")
		else
			self.资源组[3] =  tp._按钮(tp.资源:载入('JM.dll',"网易WDF动画",0x86D66B9A),0,0,4,true,true,"显示类型")
		end
		for n=1,36 do
		  	self.技能[n]:置技能(self.data.技能[n],self.data.类型)
		end
		self.技能数量 = #self.data.技能
		self.技能页数 =0
		self:置形象()

end
function 场景类_召唤兽查看栏:打开(b,l)

	if self.可视 then
		if b ~= nil and self.data ~= b then
			self.data = b
			for n=1,12 do
			  	self.技能[n]:置技能(self.data.技能[n],2)
			end
			lb = l
			tp.运行时间 = tp.运行时间 + 1
		    self.窗口时间 = tp.运行时间
		    return false
		end
		self.data = nil
		xslb = nil
		self.可视 = false
	else
	    if  self.x > 全局游戏宽度 then
			self.x = 211+(全局游戏宽度-800)/2
		end
		self.data = b
		lb = l
		self.状态 = 1
		insert(tp.窗口_,self)
		if self.data.类型 =="孩子" then
			self.资源组[3] =  tp._按钮(tp.资源:载入('JM.dll',"网易WDF动画",0x86D66B9A),0,0,4,true,true,"设置参战")

		else
			self.资源组[3] =  tp._按钮(tp.资源:载入('JM.dll',"网易WDF动画",0x86D66B9A),0,0,4,true,true,"显示类型")
			for i=1,6 do
				    self.内丹[i]:置内丹(self.data.内丹[i])
			end
		end
		for n=1,36 do
		  	self.技能[n]:置技能(self.data.技能[n],self.data.类型)
		end

			self.技能数量 = #self.data.技能
			self.技能页数 =0
		self:置形象()
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_召唤兽查看栏:置形象()
	if self.data ~= nil then
		self.资源组[100] = nil
		self.资源组[101] = nil
		local n = 取模型(self.data.造型)
		self.资源组[100] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)

		if self.data.饰品  then
			if 取召唤兽饰品(self.data.造型) then
			local n = 取模型("饰品_"..self.data.造型)
			self.资源组[101] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
			end
		elseif 取召唤兽武器(self.data.造型) then
			local n = 取模型("武器_"..self.data.造型)
			self.资源组[101] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
		end		

		if self.data.染色方案 ~= nil then
			if self.data.染色方案 == "黑白" then
				self.资源组[100]:置染色("黑白",ARGB(255,235,235,235))
				self.资源组[100]:置方向(0)
				if self.data.饰品 ~= nil then
					self.资源组[101]:置染色("黑白",ARGB(255,185,185,185))
					self.资源组[101]:置方向(0)
				end
			else
				self.资源组[100]:置染色(self.data.染色方案,self.data.染色组[1],self.data.染色组[2],self.data.染色组[3])
				self.资源组[100]:置方向(0)
			end
		end
	end
end

function 场景类_召唤兽查看栏:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x + 461,self.y + 6)


	self.资源组[7]:更新(x,y,self.状态 ~= 1)
	self.资源组[8]:更新(x,y,self.状态 ~= 2 and self.data.类型 ~="孩子")
	self.资源组[9]:更新(x,y,self.状态 ~= 3 and  self.data.类型 ~="孩子" and(self.data.参战等级>= 45 or self.data.神兽 )  )

	self.资源组[7]:显示(self.x+454,self.y+305-69,true,nil,nil,self.状态 == 1,2)
	self.资源组[8]:显示(self.x+454,self.y+351-69,true,nil,nil,self.状态 == 2,2)
	self.资源组[9]:显示(self.x+454,self.y+397-69,true,nil,nil,self.状态 == 3,2)




	
	self.资源组[3]:更新(x,y)
	self.资源组[3]:显示(self.x + 6,self.y+2,true)
	
	if self.鼠标 then
		if self.资源组[2]:事件判断() then
			self:打开()
			return false
		elseif self.资源组[7]:事件判断() then
			self.状态 = 1
		elseif self.资源组[8]:事件判断() then
			self.状态 = 2
		elseif self.资源组[9]:事件判断() then
			self.状态 = 3
	   elseif self.圣兽丹动画:是否选中(x,y) then
      tp.提示:自定义(x+40,y,"#y/该召唤兽拥有饰品可以提高#g/10%#y/的资质属性")

		elseif self.资源组[3]:事件判断() then
			if self.data.类型 =="孩子" then
				客户端:发送数据(lb,32,5)
			else 
				if xslb == nil then
					xslb = true
				else
					xslb = nil
				end
			end

		end
	end
	if self.资源组[100] ~= nil then
		tp.影子:显示(self.x + 116,self.y+152)
		self.资源组[100]:更新(dt)
		self.资源组[100]:显示(self.x + 116,self.y+152)
		if self.资源组[101] ~= nil then
			self.资源组[101]:更新(dt)
			self.资源组[101]:显示(self.x + 116,self.y+152)
		end
	end
	
	zts1:置颜色(ARGB(255,255,255,255))
	local v = ""
	if xslb then
	    v = self.data.类型
	end
	zts1:显示(self.x + 50,self.y + 35,self.data.名称)
	zts1:置颜色(-16711936)
	zts1:显示(self.x + 50 + #self.data.名称*7.2,self.y + 35,v)
	zts1:置颜色(-16777216)
	zts1:显示(self.x + 71,self.y + 180,self.data.名称)
	zts1:显示(self.x + 95,self.y + 209,self.data.参战等级)
	zts1:显示(self.x + 219,self.y + 209,self.data.等级)
	zts1:显示(self.x + 62,self.y + 233,format("%d/%d",self.data.当前气血,self.data.气血上限))
	zts1:显示(self.x + 62,self.y + 256,format("%d/%d",self.data.当前魔法,self.data.魔法上限))
	zts1:显示(self.x + 62,self.y + 280,self.data.伤害)
	zts1:显示(self.x + 62,self.y + 305,self.data.防御)
	zts1:显示(self.x + 62,self.y + 328,self.data.速度)
	zts1:显示(self.x + 62,self.y + 353,self.data.灵力)
	zts1:显示(self.x + 206,self.y + 232,self.data.体质)
	zts1:显示(self.x + 206,self.y + 257,self.data.魔力)
	zts1:显示(self.x + 206,self.y + 280,self.data.力量)
	zts1:显示(self.x + 206,self.y + 305,self.data.耐力)
	zts1:显示(self.x + 206,self.y + 328,self.data.敏捷)
	zts1:显示(self.x + 206,self.y + 352,self.data.潜能)
	zts1:显示(self.x + 384,self.y + 33,self.data.攻资)
	zts1:显示(self.x + 384,self.y + 55,self.data.防资)
	zts1:显示(self.x + 384,self.y + 77,self.data.体资)
	zts1:显示(self.x + 384,self.y + 99,self.data.法资)
	zts1:显示(self.x + 384,self.y + 122,self.data.速资)
	zts1:显示(self.x + 384,self.y + 144,self.data.躲资)
	-- if self.data.进阶属性.力量 > 0 then
 --      zts1:置颜色(0xFFFF00FF)
 --      zts1:显示(self.x + 272,self.y + 223+1*24,"+"..self.data.进阶属性.力量)
 --      zts1:置颜色(-16777216)
 --     end

 --      if self.data.进阶属性.敏捷 > 0 then
 --      zts1:置颜色(0xFFFF00FF)
 --      zts1:显示(self.x + 272,self.y + 223+2*24,"+"..self.data.进阶属性.敏捷)
 --      zts1:置颜色(-16777216)
 --     end
 --          if self.data.进阶属性.耐力 > 0 then
 --      zts1:置颜色(0xFFFF00FF)
 --      zts1:显示(self.x + 272,self.y + 223+3*24,"+"..self.data.进阶属性.耐力)
 --      zts1:置颜色(-16777216)
 --     end
 --          if self.data.进阶属性.体质 > 0 then
 --      zts1:置颜色(0xFFFF00FF)
 --      zts1:显示(self.x + 272,self.y + 223+4*24,"+"..self.data.进阶属性.体质)
 --      zts1:置颜色(-16777216)
 --     end
 --          if self.data.进阶属性.魔力 > 0 then
 --      zts1:置颜色(0xFFFF00FF)
 --      zts1:显示(self.x + 272,self.y + 223+5*24,"+"..self.data.进阶属性.魔力)
 --      zts1:置颜色(-16777216)
 --     end
    if self.data.特殊资质.攻资> 0 then
	zts1:置颜色(0xFF8000FF)
	zts1:显示(self.x+414,self.y+33,"+"..self.data.特殊资质.攻资)
	zts1:显示(self.x+414,self.y+55,"+"..self.data.特殊资质.防资)
	zts1:显示(self.x+414,self.y+77,"+"..self.data.特殊资质.体资)
	zts1:显示(self.x+414,self.y+99,"+"..self.data.特殊资质.法资)
	zts1:显示(self.x+414,self.y+122,"+"..self.data.特殊资质.速资)
	zts1:显示(self.x+414,self.y+144,"+"..self.data.特殊资质.躲资)
    zts1:置颜色(-16777216)
    end
 	 if self.data.饰品 then
     self.圣兽丹动画:显示(self.x+58,self.y+145)
     end

	if self.data.类型=="神兽" or self.data.类型=="孩子" then
		zts1:显示(self.x + 384,self.y + 168,"★永生★")
	else
	zts1:显示(self.x + 384,self.y + 168,math.floor(self.data.寿命))
    end
	zts1:显示(self.x + 384,self.y + 192,self.data.成长)
	zts1:显示(self.x + 384,self.y + 215,self.data.五行)




	if self.状态 == 1 then
			local xx = 0
			local yy = 0
         if self.技能数量>12*(self.技能页数+1) then
		
				self.资源组[5]:更新(x,y)
				self.资源组[5]:显示(self.x + 436,self.y + 312)
				if self.资源组[5]:事件判断() then
				self.技能页数=self.技能页数+1
				end
 		end
		  if self.技能页数 > 0 then
				self.资源组[4]:更新(x,y)
				self.资源组[4]:显示(self.x + 436,self.y + 278)
				if self.资源组[4]:事件判断() then
				self.技能页数=self.技能页数-1
				end
			
          end

		for i=1+self.技能页数*12,12+self.技能页数*12 do
			local jx = self.x+266+(xx*41) --20  246
			local jy = self.y+240+(yy*41)--309  -69 
			self.资源组[6]:显示(jx,jy)
		    self.技能[i]:置坐标(jx+3,jy+2)
	   	    self.技能[i]:显示(x,y,self.鼠标)
	   	    if self.技能[i].焦点 and self.技能[i].技能 ~= nil then

	   	    	tp.提示:BB技能(x,y,self.技能[i].技能)
	   	    end
			xx = xx + 1
			if xx > 3 then
				xx = 0
				yy = yy + 1
			end
		end


	elseif self.状态 == 2 then

		self.资源组[12]:显示(self.x+266,self.y+240)
		local v1 = self.data.内丹.总数
		for i=1,6 do
			local jxx = self.x+266 + yx[i][1]
			local jxy = self.y+240 + yx[i][2]

			if i <= v1 and   self.内丹[i].内丹 == nil  then
					self.资源组[14]:显示(jxx,jxy)
		        if self.鼠标 and self.资源组[14]:是否选中(x,y) then
		        tp.提示:自定义(x,y,"可以学习的内丹技能格")
		        self.焦点 = true
		       end
			elseif i > v1 and   self.内丹[i].内丹 == nil  then
				self.资源组[13]:显示(jxx,jxy)
				if self.鼠标 and self.资源组[13]:是否选中(x,y) then
					tp.提示:自定义(x,y,"不可用内丹技能格,召唤兽可用内丹格数量与其参战等级相关")
					self.焦点 = true
				end
			end
			self.内丹[i]:置坐标(jxx,jxy)
	   	    self.内丹[i]:显示(x,y,self.鼠标)
	   	     if  self.内丹[i].内丹 ~= nil and self.内丹[i].焦点 then
			    tp.提示:内丹提示(x,y,self.内丹[i].内丹)
			 end
		end
	elseif self.状态 == 3 then
		local jx = self.x+266
		local jy = self.y+240
		self.资源组[11]:显示(jx,jy)
		self.进阶[16]:显示(jx+40,jy+7)
		if self.进阶[16]:是否选中(x,y) then
			tp.提示:自定义(x,y,"#W/使用#Y/易经丹#W/可以提升该召唤兽灵性,当灵性到达到50可以获得新的造型")
		end
		if self.data.灵性>0 and self.data.灵性<0 then
	    elseif self.data.灵性>0 and self.data.灵性<= 10 then
	    	self.进阶[1]:显示(jx+39,jy+6)
	    elseif self.data.灵性>10 and self.data.灵性<= 20 then
	    	self.进阶[2]:显示(jx+39,jy+6)
	    elseif self.data.灵性>20 and self.data.灵性<= 30 then
	    	self.进阶[3]:显示(jx+39,jy+6)
	    elseif self.data.灵性>30 and self.data.灵性<= 40 then
	    	self.进阶[4]:显示(jx+39,jy+6)
	    elseif self.data.灵性>40 and self.data.灵性<= 50 then
	    	self.进阶[5]:显示(jx+39,jy+6)
	    elseif self.data.灵性>50 and self.data.灵性<= 60 then
	    	self.进阶[6]:显示(jx+39,jy+6)
	    elseif self.data.灵性>60 and self.data.灵性<= 70 then
	    	self.进阶[7]:显示(jx+39,jy+6)
	    elseif self.data.灵性>70 and self.data.灵性<= 80 then
	    	self.进阶[8]:显示(jx+39,jy+6)
	    elseif self.data.灵性>80 and self.data.灵性<= 90 then
	        self.进阶[9]:显示(jx+39,jy+6)
	    elseif self.data.灵性>90 and self.data.灵性<= 91 then
	    	self.进阶[10]:显示(jx+39,jy+6)
	    elseif self.data.灵性>91 and self.data.灵性<= 93 then
	    	self.进阶[11]:显示(jx+39,jy+6)
	    elseif self.data.灵性>93 and self.data.灵性<= 97 then
	        self.进阶[12]:显示(jx+39,jy+6)
	    elseif self.data.灵性>97  then
	        self.进阶[13]:显示(jx+39,jy+6)
		end


		if self.data.灵性>80 and self.data.特性=="无" then
		self.进阶[14]:显示(jx+51,jy+30)
		elseif self.data.灵性>80 and self.data.特性~="无" then
		self.进阶[15]:显示(jx+67,jy+36)
		end

		if self.data.特性 ~="无" then
		zts1:置颜色(0xFFFFFFA4)
		zts1:显示(jx+74,jy+45,self.data.特性)
		zts1:置颜色(0xFFFFFFFF)
	    end
	    if (self.进阶[14]:是否选中(x,y) or self.进阶[15]:是否选中(x,y)) and self.data.特性几率~=0 and self.data.特性~= "无" then
			tp.提示:特性(x,y,self.data.特性,self.data.等级,self.data.特性几率)
		end
		if self.data.最高灵性显示 then
			zts1:置颜色(0xFFFFFFFF)
		  zts1:显示(jx+13,jy+115,"灵性:"..self.data.灵性)
			zts1:置颜色(0xFFFFFFFF)
		 zts1:显示(jx+67,jy+115,"(历史最高值"..self.data.最高灵性..")")
		elseif  self.data.灵性>0 then
				zts1:置颜色(0xFFFFFFFF)
		zts1:显示(jx+65,jy+115,"灵性:"..self.data.灵性)
		end
	end

end


return 场景类_召唤兽查看栏