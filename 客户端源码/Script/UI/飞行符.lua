--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--
local 场景类_飞行符 = class(窗口逻辑)

local tp
local insert = table.insert

function 场景类_飞行符:初始化(根)
	self.ID = 25
	self.x = 90+(全局游戏宽度-800)/2
	self.y = 130
	self.xx = 0
	self.yy = 0
	self.注释 = "飞行符"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	self.资源组 = {
		[1] = 资源:载入('JM.dll',"网易WDF动画",0x516749F4),
		[2] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xDB61AD29),0,0,1,true,true),
		[3] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x3D561594),0,0,1,true,true),
		[4] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x6BD8F1C8),0,0,1,true,true),
		[5] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xC5C5FE46),0,0,1,true,true),
		[6] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x29622DDB),0,0,1,true,true),
		[7] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x491E6B4C),0,0,1,true,true),
		[8] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x549B2B83),0,0,1,true,true),
	}
	for i=2,8 do
		self.资源组[i]:绑定窗口_(25)
	end
	tp = 根
	self.窗口时间 = 0
	self.坐标 = {[1]={x=70,y=140},[2]={x=195,y=140},[3]={x=320,y=140},[4]={x=70,y=290},[5]={x=195,y=290},[6]={x=320,y=290}}
end

function 场景类_飞行符:打开(类型,格子)
	if self.可视 then
		self.可视 = false
	else
								                    if  self.x > 全局游戏宽度 then
self.x = 90+(全局游戏宽度-800)/2
    end
		 self.类型=类型
        self.格子=格子
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_飞行符:显示(dt,x,y)
	for i=2,8 do
	   self.资源组[i]:更新(x,y)
	end
	self.焦点 = false
	if self.鼠标 then
		for i=1,7 do

			if self.资源组[i+1]:事件判断() then
			客户端:发送数据(self.格子,i,16)
			self:打开()
         end
		end
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x + 378,self.y + 83,true)
	self.资源组[3]:显示(self.x + 425,self.y + 191,true)
	self.资源组[4]:显示(self.x + 574,self.y + 244,true)
	self.资源组[5]:显示(self.x + 208,self.y + 17,true)
	self.资源组[6]:显示(self.x + 203,self.y + 85,true)
	self.资源组[7]:显示(self.x + 186,self.y + 161,true)
	self.资源组[8]:显示(self.x + 249,self.y + 224,true)
	--if tp.按钮
end



return 场景类_飞行符