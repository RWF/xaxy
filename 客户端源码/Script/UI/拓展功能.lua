



local 场景类_成就 = class(窗口逻辑)
local insert = table.insert
local remove = table.remove
local floor = math.floor
local tp,zts,zts1
function 场景类_成就:初始化(根)
    self.ID = 315
    self.x = 90+(全局游戏宽度-800)/2
    self.y = 65
    self.xx = 0
    self.yy = 0
    self.注释 = "成就"
    tp = 根
        self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    local 按钮 = 根._按钮
    local 自适应 = 根._自适应
    local  资源= 根.资源
    self.资源组 = {
        [1] = Picture.拓展功能,
        [2] = 按钮.创建(资源:载入('新关闭按钮',"动画"),0,0,4,true,true),
        [3]=按钮.创建(自适应.创建(100,4,75,26,1,3),0,0,4,true,true,"光武拓印"),
        [4]=按钮.创建(自适应.创建(100,4,75,26,1,3),0,0,4,true,true,"改变双加"),
        [5]=按钮.创建(自适应.创建(100,4,75,26,1,3),0,0,4,true,true,"赞助系统"),
        [6]=按钮.创建(自适应.创建(100,4,75,26,1,3),0,0,4,true,true,"首服奖励"),
        [20] = tp._自适应(70,1,533,221,3,9),
        [21]=资源:载入('7A47CB79',"动画").精灵,
        [22]=资源:载入('FE277252',"动画").精灵,
    }


    for i=2,6 do
      self.资源组[i]:绑定窗口_(315)

    end


    self.翻页 =false
    self.窗口时间 = 0
    zts1 = tp.字体表.华康字体

end




function 场景类_成就:打开(内容)
  if self.可视 then
        self.可视 = false
  else

        if  self.x > 全局游戏宽度 then
        self.x = 82+(全局游戏宽度-800)/2
        end
        insert(tp.窗口_,self)
        tp.运行时间 = tp.运行时间 + 1
        self.窗口时间 = tp.运行时间
        self.可视 = true
  end
 end
function 场景类_成就:显示(dt,x,y)
    self.焦点 = false

    self.资源组[1]:显示(self.x,self.y)
    zts1:置颜色(0xFFFFFFFF):显示(self.x+302,self.y+25,"拓 展 功 能")
    self.资源组[2]:显示(self.x+620,self.y+20,true,1)
    self.资源组[3]:显示(self.x+63,self.y+335,true,1)
    self.资源组[4]:显示(self.x+153,self.y+335,true,1)
    self.资源组[5]:显示(self.x+243,self.y+335,true,1)
    self.资源组[6]:显示(self.x+243+90,self.y+335,true,1)
    self.资源组[20]:显示(self.x+71,self.y+89,true,1)
    self.资源组[21]:显示(self.x+204,self.y+57)
    -- self.资源组[41]:置区域(0,0,min(floor(self.数据.当前经验 / self.数据.升级经验 * 161),161),self.资源组[41].高度)
    self.资源组[22]:显示(self.x+209,self.y+61)
    for i=2,6 do
       self.资源组[i]:更新(x,y)
       if self.资源组[i]:事件判断() then
           if i==2 then 
                self:打开()
           elseif i==3 then
             tp.窗口.光武拓印:打开()

           elseif i==4 then 
           tp.窗口.改变双加:打开() 
           elseif i==5 then 
           tp.窗口.CDK:打开()
           elseif i==6 then 
            客户端:发送数据(76,76,76)
           end
       end
    end

end




return 场景类_成就



