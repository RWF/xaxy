--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:46
--======================================================================--
local 场景类_召唤兽属性栏 = class()
local bw = require("gge包围盒")(0,0,140,34)
local box = 引擎.画矩形

local format = string.format
local floor = math.floor
local min = math.min
local max = math.max
local tp,zts,zts1,zts2
local ceil = math.ceil
local tostring = tostring
local bd0 = {"气血","魔法","攻击","防御","速度","灵力"}
local bd = {"体质","魔力","力量","耐力","敏捷"}
local mousea = 引擎.鼠标按住
local mouseb = 引擎.鼠标弹起
local insert = table.insert

function 场景类_召唤兽属性栏:初始化(根)
	self.ID = 7
	self.x = 0
	self.y = 35
	self.xx = 0
	self.yy = 0
	self.注释 = "召唤兽属性栏"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[0] = 自适应.创建(1,1,332,18,1,3,nil,18),
		[1] = 自适应.创建(0,1,370,453,3,9),
		[2] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),
		[3] = 按钮.创建(自适应.创建(12,4,43,22,1,3),0,0,4,true,true,"参战"),
		[4] = 按钮.创建(自适应.创建(12,4,43,22,1,3),0,0,4,true,true,"改名"),
		[5] = 按钮.创建(自适应.创建(25,4,19,19,4,3),0,0,4,true,true),
		[6] = 按钮.创建(自适应.创建(25,4,19,19,4,3),0,0,4,true,true),
		[7] = 按钮.创建(自适应.创建(25,4,19,19,4,3),0,0,4,true,true),
		[8] = 按钮.创建(自适应.创建(25,4,19,19,4,3),0,0,4,true,true),
		[9] = 按钮.创建(自适应.创建(25,4,19,19,4,3),0,0,4,true,true),
		[10] = 按钮.创建(自适应.创建(26,4,19,19,4,3),0,0,4,true,true),
		[11] = 按钮.创建(自适应.创建(26,4,19,19,4,3),0,0,4,true,true),
		[12] = 按钮.创建(自适应.创建(26,4,19,19,4,3),0,0,4,true,true),
		[13] = 按钮.创建(自适应.创建(26,4,19,19,4,3),0,0,4,true,true),
		[14] = 按钮.创建(自适应.创建(26,4,19,19,4,3),0,0,4,true,true),
		[15] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"推荐加点"),
		[16] = 按钮.创建(自适应.创建(12,4,43,22,1,3),0,0,4,true,true,"预览"),
		[17] = 按钮.创建(自适应.创建(12,4,43,22,1,3),0,0,4,true,true,"驯养"),
		[18] = 按钮.创建(自适应.创建(12,4,43,22,1,3),0,0,4,true,true,"放生"),
		[19] = 按钮.创建(自适应.创建(12,4,43,22,1,3),0,0,4,true,true,"观看"),
		[20] = 按钮.创建(自适应.创建(12,4,43,22,1,3),0,0,4,true,true,"鉴定"),
		[21] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"确认加点"),
		[22] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xB15C5678),0,0,4,true,true,"查看资质"),
		[23] = 资源:载入('JM.dll',"网易WDF动画",0x363AAF1B),
		[26] = 资源:载入('JM.dll',"网易WDF动画",0x3906F9F1),
		[27] = 按钮.创建(自适应.创建(20,4,18,19,4,3),0,0,4,true,true),
		[28] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),
		[29] = 自适应.创建(2,1,171,182,3,9),
		[30] = 自适应.创建(2,1,158,153,3,9),
		[31] = 自适应.创建(3,1,109,19,1,3),
		[32] = 自适应.创建(3,1,97,19,1,3),
		[33] = 自适应.创建(3,1,40,19,1,3),
		[34] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x288FC5D3),0,0,1), --288FC5D3 --0x22E95BB5大
		[35] =  根._滑块.创建(自适应.创建(11,4,15,40,2,3,nil),4,14,143,2),
		[36] = 按钮.创建(自适应.创建(25,4,19,19,4,3),0,0,4,true,true),
		[37] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x289B7143),0,0,4),--289B7143
	}
	   self.圣兽丹动画 =根.资源:载入('JM.dll',"网易WDF动画",0xA4F1E391)
	self.资源组[22]:置偏移(0,3)
	for i=2,28 do
		if i ~= 23 and i ~= 24 and i ~= 25 and i ~= 26 then
			self.资源组[i]:绑定窗口_(7)
		end
	end
	self.资源组[37]:绑定窗口_(7)

	self.资源组[34]:绑定窗口_(7)
	self.资源组[36]:绑定窗口_(7)
	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('召唤兽总控件')
	总控件:置可视(true,true)
	self.名称输入框 = 总控件:创建输入("名称输入",0,0,100,14)
	self.名称输入框:置可视(false,false)
	self.名称输入框:置限制字数(12)
	self.名称输入框:置光标颜色(-16777216)
	self.名称输入框:置文字颜色(-16777216)
	self.临时潜能 = {}
	self.预览属性 = {}
	for i=0,12 do
		self.临时潜能[i] = {体质=0,魔力=0,力量=0,耐力=0,敏捷=0}
	end
	for i=0,12 do
		self.预览属性[i] = {气血=0,魔法=0,伤害=0,速度=0,灵力=0,防御=0}
	end
	self.加入 = 0
	self.选中 = 0
	self.拽拖计次 = 0
	self.拽拖对象 = 0
	self.拽拖事件 = 0
	self.插入选区 = 0
	self.操作 = nil
	self.头像组 = {}
	self.窗口时间 = 0
	self.鉴定 = false

	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.普通字体__
	zts2 = tp.字体表.描边字体
end


function 场景类_召唤兽属性栏:打开(数据)
	if self.可视 then
		for i=0,8 do
			if self.数据[i] ~= nil then
				self.数据[i].潜能 = self.数据[i].潜能 + (self.临时潜能[i].体质+self.临时潜能[i].魔力+self.临时潜能[i].力量+self.临时潜能[i].耐力+self.临时潜能[i].敏捷)
			end
			self.临时潜能[i] = {体质=0,魔力=0,力量=0,耐力=0,敏捷=0}
		end
		for i=0,8 do
			self.预览属性[i] = {气血=0,魔法=0,伤害=0,速度=0,灵力=0,防御=0}
		end
		self.加入 = 0
		self.选中 = 0
		self.鉴定 = false
		self.拽拖计次 = 0
		self.拽拖对象 = 0
		self.拽拖事件 = 0
		self.插入选区 = 0
		self.头像组 = {}
		self.窗口时间 = 0
		self.名称输入框:置可视(false,false)
		self.可视 = false
		self.资源组[35]:置起始点(0)
		if tp.窗口.召唤兽资质栏.可视 then
			tp.窗口.召唤兽资质栏:打开()
		end
	else
		self.数据 = 数据

		insert(tp.窗口_,self)
		for i=1,#self.数据 do
			if self.数据[i].参战 ~= 0 then
				if i > 4 then
					self.加入 = i-4
					self.资源组[35]:置起始点(self.资源组[35]:取百分比转换(self.加入,4,#self.数据))
				end
				self.选中 = i
				self.名称输入框:置文本(self.数据[self.选中].名称)
				self.名称输入框:置可视(true,true)
				if tp.窗口.召唤兽资质栏.可视 then
					tp.窗口.召唤兽资质栏:打开(self.数据[self.选中])
				end
				self:置形象()
				self.拽拖计次 = 0
				break
			end
		end
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end
function 场景类_召唤兽属性栏:刷新(数据)
	self.鉴定 = false
	self.头像组={}
	if 数据 ~= nil then
   self.数据 = 数据
   end
   	if tp.窗口.召唤兽资质栏.可视 then
	tp.窗口.召唤兽资质栏:打开(self.数据[self.选中],self.选中)
	end
  self:置形象()

end
function 场景类_召唤兽属性栏:置形象()
	self.资源组[24] = nil
	self.资源组[25] = nil
	if self.数据[self.选中]  ~= nil then
		local n = 取模型(self.数据[self.选中].造型)
		self.资源组[24] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
		if self.数据[self.选中].饰品  then
			if 取召唤兽饰品(self.数据[self.选中].造型) then
			n = 取模型("饰品_"..self.数据[self.选中].造型)
			self.资源组[25] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
			end
		elseif 取召唤兽武器(self.数据[self.选中].造型) then
			n = 取模型("武器_"..self.数据[self.选中].造型)
			self.资源组[25] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
		end

		if self.数据[self.选中].染色方案 ~= nil then
			self.资源组[24]:置染色(self.数据[self.选中].染色方案,self.数据[self.选中].染色组[1],self.数据[self.选中].染色组[2],self.数据[self.选中].染色组[3])
			if self.资源组[25] then
			   self.资源组[25]:置染色(self.数据[self.选中].染色方案,self.数据[self.选中].染色组[1],self.数据[self.选中].染色组[2],self.数据[self.选中].染色组[3])
			end
			self.资源组[24]:置方向(0)
		end
	end
end


function 场景类_召唤兽属性栏:更改参战(编号,状态)
  if self.可视==false then return 0 end
 if 状态 then
   self.数据.参战=编号
   tp.提示:写入("#Y/你的这只召唤兽已经设置为出战状态")
  else
   self.数据.参战=0
   tp.提示:写入("#Y/你的这只召唤兽已经设置为休息状态")
   end
 end

function 场景类_召唤兽属性栏:显示(dt,x,y)
	-- 变量

	local bbs = self.数据
	local bbsa = #bbs
	local bb,ls,yl
	if self.选中 ~= 0 then
		bb = bbs[self.选中]
		ls = self.临时潜能[self.选中]
		yl = self.预览属性[self.选中]
		if self.数据[self.选中] ~= nil and self.数据.参战 ==self.选中 then
			self.资源组[3]:置文字("休息")
		else
			self.资源组[3]:置文字("参战")
		end
		if self.数据[self.选中] ~= nil and self.数据.观看 ==self.选中 then
			self.资源组[19]:置文字("取消")
		else
			self.资源组[19]:置文字("观看")
		end
	end
	-- 更新
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y,bb ~= nil)
	self.资源组[4]:更新(x,y,bb ~= nil)
	for i=5,9 do
	   self.资源组[i]:更新(x,y,bb ~= nil and bb.潜能 > 0,1)
	   if self.资源组[i]:事件判断() then
	   		bb.潜能 = bb.潜能 - 1
	   		ls[bd[i-4]] = ls[bd[i-4]] + 1
	   end
	end
	for i=10,14 do
	   self.资源组[i]:更新(x,y,bb ~= nil and ls[bd[i-9]] > 0,1)
	   if self.资源组[i]:事件判断() then
	   		bb.潜能 = bb.潜能 + 1
	   		ls[bd[i-9]] = ls[bd[i-9]] - 1
	   end
	end
	self.资源组[15]:更新(x,y,bb ~= nil and bb.潜能 > 0)
	self.资源组[16]:更新(x,y,bb ~= nil)
	self.资源组[17]:更新(x,y,bb ~= nil)
	self.资源组[18]:更新(x,y,bb ~= nil)
	self.资源组[19]:更新(x,y,bb ~= nil)
	self.资源组[20]:更新(x,y,bb ~= nil)
	self.资源组[21]:更新(x,y,bb ~= nil)
	self.资源组[22]:更新(x,y,bb ~= nil)
	self.资源组[34]:更新(x,y,bb ~= nil)
	self.资源组[37]:更新(x,y,bb ~= nil)
	self.资源组[36]:更新(x,y)
	-- 显示
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[0]:显示(self.x+6,self.y+3)
	Picture.窗口标题背景_:置区域(0,0,92,16)
	Picture.窗口标题背景_:显示(self.x+132,self.y+3)
	zts2:置字间距(2)
	zts2:显示(self.x+138,self.y+3,"召唤兽属性")
	zts2:置字间距(0)
	self.资源组[29]:显示(self.x+12,self.y+29)
	self.资源组[30]:显示(self.x+197,self.y+28)
	self.资源组[31]:显示(self.x+197,self.y+217)

	self.资源组[36]:显示(self.x+168,self.y+216)

	tp.经验背景_:显示(self.x+55,self.y+423)
	zts:置颜色(4294967295)

	tp.经验背景_:显示(self.x+55,self.y+423)
	tp.字体表.普通字体:置颜色(4294967295)

	tp.字体表.普通字体:显示(self.x+19,self.y+222,"召唤兽携带数量："..bbsa.."/"..bbs.召唤兽上限)


	tp.字体表.普通字体:显示(self.x+21,self.y+427,"经验")
	if bb == nil then
		tp.字体表.普通字体:显示(self.x+200,self.y+194,"参战等级：--")
	else
		tp.字体表.普通字体:显示(self.x+200,self.y+194,"参战等级："..bb.参战等级)

	end
   if bb ~= nil and self.鉴定 == true then

   	   tp.字体表.普通字体:置颜色(0xFF000000):显示(self.x+250,self.y+40,"类型："..bb.类型)


   end
	for i=0,5 do
		self.资源组[32]:显示(self.x+54,self.y+243+i*24)
		tp.字体表.普通字体:置颜色(0xFFFFFFFF):显示(self.x+20,self.y+247+i*24,bd0[i+1])
	end
	for i=0,4 do
		self.资源组[31]:显示(self.x+197,self.y+243+i*24)
		tp.字体表.普通字体:显示(self.x+163,self.y+247+i*24,bd[i+1])
	end
	tp.字体表.普通字体:置颜色(-1404907)
	tp.字体表.普通字体:显示(self.x+20,self.y+397,"忠诚")
	tp.字体表.普通字体:显示(self.x+163,self.y+367,"潜能")
	tp.字体表.普通字体:置颜色(-16777216)
	self.资源组[33]:显示(self.x+53,self.y+393)
	self.资源组[33]:显示(self.x+193,self.y+363)
	self.资源组[2]:显示(self.x+344,self.y+3)
	self.资源组[3]:显示(self.x+310,self.y+186,true)
	self.资源组[4]:显示(self.x+310,self.y+215,true)
	for i=5,9 do
		self.资源组[i]:显示(self.x+309,self.y+242+((i-5)*25))
	end
	for i=10,14 do
		self.资源组[i]:显示(self.x+330,self.y+242+((i-10)*25))
	end
	self.资源组[15]:显示(self.x+242,self.y+365,true)
	self.资源组[16]:显示(self.x+315,self.y+366,true)
	self.资源组[17]:显示(self.x+100,self.y+393,true)
	self.资源组[18]:显示(self.x+147,self.y+393,true)
	self.资源组[19]:显示(self.x+194,self.y+393,true)
	self.资源组[20]:显示(self.x+241,self.y+393,true)
	self.资源组[21]:显示(self.x+288,self.y+393,true)
	self.资源组[22]:显示(self.x+270,self.y+418,true)
	tp.画线:置区域(0,0,15,159)
	tp.画线:显示(self.x+171,self.y+34)
	if bbsa > 4 then
		self.资源组[35]:置高度(min(floor(143/(bbsa-4)),104))
		self.加入 = min(ceil((bbsa-4)*self.资源组[35]:取百分比()),bbsa-4)
	end
	self.资源组[27]:更新(x,y,self.加入 > 0)
	self.资源组[28]:更新(x,y,self.加入 < bbsa - 4)
	self.资源组[27]:显示(self.x+167,self.y+31)
	self.资源组[28]:显示(self.x+167,self.y+193)
	if bbsa > 4 then
		self.资源组[35]:显示(self.x+168,self.y+50,x,y,self.鼠标)
	end
	-- 真
	
	for i=1,4 do
		if bbs[i+self.加入] ~= nil then
			local jx = self.x+18
			local jy = self.y+(i*43)-6
			bw:置坐标(jx,jy+1)
			if self.头像组[i+self.加入] == nil then
				local n = ModelData[bbs[i+self.加入].造型]
				self.头像组[i+self.加入] = tp.资源:载入(n.头像资源,"网易WDF动画",n.小头像)
			end
			-- 拽拖
			local xz = bw:检查点(x,y)
			if not self.资源组[35].接触 and xz and self.鼠标 and not tp.消息栏焦点 and self.拽拖对象 == 0 then
				if mousea(0) then
					self.拽拖计次 = self.拽拖计次 + 1
					if self.拽拖计次 >= 28 then
						self.拽拖对象 = i+self.加入
						self.拽拖事件 = {self.头像组[i+self.加入]}
						self.拽拖计次 = 0
					end
				end
			end
			-- 其他
			if self.选中 ~= i+self.加入 then
				if not self.资源组[35].接触 and xz and not tp.消息栏焦点 and self.鼠标 then
					if self.拽拖对象 ~= 0 then
						box(jx+70,jy+34,jx+125,jy+39,-16777216)
						self.插入选区 = i+self.加入
					end
					if mouseb(0) and self.拽拖对象 == 0 then

						self.选中 = i+self.加入
						self.名称输入框:置文本(bbs[self.选中].名称)
						self.名称输入框:置可视(true,true)
						self.鉴定=false
						if tp.窗口.召唤兽资质栏.可视 then
							tp.窗口.召唤兽资质栏:打开(bbs[self.选中])
						end
						self:置形象()
						self.拽拖计次 = 0
					end
					self.焦点 = true
				end
			else
				if not self.资源组[35].接触 and xz and not tp.消息栏焦点 and self.鼠标 then
					self.焦点 = true
				end
				if self.拽拖对象 ~= i+self.加入 then
					box(jx-1,jy-3,jx+142,jy+41,-10790181)
				end
			end
			if not self.资源组[35].接触 and xz and not tp.消息栏焦点 and self.鼠标 and mouseb(0) and 引擎.按键按住(0x10) then
							
								tp.窗口.聊天框类:添加数据(self.选中,bbs[self.选中].名称,2) 
			end
			self.资源组[23]:显示(jx+1,jy)
			self.头像组[i+self.加入]:显示(jx+4,jy+4)
			if bbs[i+self.加入] ==bbs[bbs.参战] then
				zts:置颜色(-256)
			else
				zts:置颜色(-16777216)
			end
			zts:显示(jx+41,jy+3,bbs[i+self.加入].名称)
			if bbs[i+self.加入].等级 then
			 zts:显示(jx+41,jy+21,bbs[i+self.加入].等级.."级")
			end

		end
	end
	if bb ~= nil then
		local jx = self.x + 280
		local jy = self.y + 145
		tp.影子:显示(jx,jy)
		self.资源组[24]:更新(dt)
		self.资源组[24]:显示(jx,jy)
		if self.资源组[25] ~= nil then
			self.资源组[25]:更新(dt)
			self.资源组[25]:显示(jx,jy)
		end
		-- 文字
		self.名称输入框:置坐标(self.x + 204,self.y + 220)
		self.控件类:更新(dt,x,y)
		self.控件类:显示(dt,x,y)
		if self.名称输入框._已碰撞 then
			self.焦点 = true
		end
		if bb.饰品 then
		self.圣兽丹动画:显示(self.x+200,self.y+155)
		end
		if bb.加锁 then
		tp.加锁图标1:显示(self.x+330,self.y+39)
		end
     self.资源组[34]:显示(self.x+205,self.y+38)
     self.资源组[37]:显示(self.x+220,self.y+35)
		zts:置颜色(-16777216)
		zts:显示(self.x + 60,self.y + 247,format("%d/%d",bb.当前气血,bb.气血上限))
		if yl.气血 > 0 then
			zts1:置颜色(-65536)
			zts1:显示(self.x + 126,self.y + 246,"+"..yl.气血)
		end
		zts:显示(self.x + 60,self.y + 271,format("%d/%d",bb.当前魔法,bb.魔法上限))
		if yl.魔法 > 0 then
			zts1:置颜色(-65536)
			zts1:显示(self.x + 126,self.y + 270,"+"..yl.魔法)
		end
		zts:显示(self.x + 60,self.y + 295,bb.伤害)
		if yl.伤害 > 0 then
			zts1:置颜色(-65536)
			zts1:显示(self.x + 126,self.y + 295,"+"..yl.伤害)
		end
		zts:显示(self.x + 60,self.y + 319,bb.防御)
		if yl.防御 > 0 then
			zts1:置颜色(-65536)
			zts1:显示(self.x + 126,self.y + 319,"+"..yl.防御)
		end
		zts:显示(self.x + 60,self.y + 343,bb.速度)
		if yl.速度 > 0 then
			zts1:置颜色(-65536)
			zts1:显示(self.x + 126,self.y + 343,"+"..yl.速度)
		end
		zts:显示(self.x + 60,self.y + 367,bb.灵力)
		if yl.灵力 > 0 then
			zts1:置颜色(-65536)
			zts1:显示(self.x + 126,self.y + 367,"+"..yl.灵力)
		end
		zts:显示(self.x + 60,self.y + 397,math.floor(bb.忠诚))
		-- ls
		zts:显示(self.x + 204,self.y + 247,bb.体质)
		if ls.体质 > 0 then
			zts:置颜色(-65536)
			zts:显示(self.x + 204 + (#tostring(bb.体质)*7) + 1,self.y + 247,"+"..ls.体质)
			zts:置颜色(-16777216)
		end
		zts:显示(self.x + 204,self.y + 271,bb.魔力)
		if ls.魔力 > 0 then
			zts:置颜色(-65536)
			zts:显示(self.x + 204 + (#tostring(bb.魔力)*7) + 1,self.y + 271,"+"..ls.魔力)
			zts:置颜色(-16777216)
		end
		zts:显示(self.x + 204,self.y + 296,bb.力量)
		if ls.力量 > 0 then
			zts:置颜色(-65536)
			zts:显示(self.x + 204 + (#tostring(bb.力量)*7) + 1,self.y + 296,"+"..ls.力量)
			zts:置颜色(-16777216)
		end
		zts:显示(self.x + 204,self.y + 320,bb.耐力)
		if ls.耐力 > 0 then
			zts:置颜色(-65536)
			zts:显示(self.x + 204 + (#tostring(bb.耐力)*7) + 1,self.y + 320,"+"..ls.耐力)
			zts:置颜色(-16777216)
		end
		zts:显示(self.x + 204,self.y + 344,bb.敏捷)
		if ls.敏捷 > 0 then
			zts:置颜色(-65536)
			zts:显示(self.x + 204 + (#tostring(bb.敏捷)*7) + 1,self.y + 344,"+"..ls.敏捷)
			zts:置颜色(-16777216)
		end


  if self.数据[self.选中].进阶属性.力量 > 0 then
      zts:置颜色(0xFFFF00FF)
      zts:显示(self.x + 272,self.y + 223+3*24,"+"..self.数据[self.选中].进阶属性.力量)
      zts:置颜色(-16777216)
     end

      if self.数据[self.选中].进阶属性.敏捷 > 0 then
      zts:置颜色(0xFFFF00FF)
      zts:显示(self.x + 272,self.y + 223+5*24,"+"..self.数据[self.选中].进阶属性.敏捷)
      zts:置颜色(-16777216)
     end
          if self.数据[self.选中].进阶属性.耐力 > 0 then
      zts:置颜色(0xFFFF00FF)
      zts:显示(self.x + 272,self.y + 223+4*24,"+"..self.数据[self.选中].进阶属性.耐力)
      zts:置颜色(-16777216)
     end
          if self.数据[self.选中].进阶属性.体质 > 0 then
      zts:置颜色(0xFFFF00FF)
      zts:显示(self.x + 272,self.y + 223+1*24,"+"..self.数据[self.选中].进阶属性.体质)
      zts:置颜色(-16777216)
     end
          if self.数据[self.选中].进阶属性.魔力 > 0 then
      zts:置颜色(0xFFFF00FF)
      zts:显示(self.x + 272,self.y + 223+2*24,"+"..self.数据[self.选中].进阶属性.魔力)
      zts:置颜色(-16777216)
     end

		zts:显示(self.x + 200,self.y + 367,bb.潜能)
		self.资源组[26]:置区域(0,0,min(floor(bb.当前经验 / bb.升级经验 * 176),176),16)
		self.资源组[26]:显示(self.x + 60,self.y + 426)
		-- zts2:置描边颜色(-16240640)
		zts2:置颜色(4294967295)
		-- zts2:置描边颜色(-16777216)
		local ts = format("%d/%d",bb.当前经验,bb.升级经验)
		tp.字体表.描边字体_:显示(self.x + ((275 - zts2:取宽度(ts))/2)+8,self.y + 427,ts)
	end
	if self.资源组[2]:事件判断() then
		self:打开()
		return false
	elseif self.资源组[3]:事件判断() then
		 客户端:发送数据(self.选中,2,23,"51",1)
	elseif self.资源组[4]:事件判断() then
       客户端:发送数据(self.选中,3,23,self.名称输入框:取文本(),1)
	elseif self.资源组[15]:事件判断() then
		-- 力耐加点
		-- local zt = 1
		-- while true do
		-- 	if zt == 1 then
		-- 		bb.潜能 = bb.潜能 - 1
		-- 		ls.体质 = ls.体质 + 1
		-- 		zt = 2
		-- 	elseif zt == 2 then
		-- 		bb.潜能 = bb.潜能 - 1
		-- 		ls.耐力 = ls.耐力 + 1
		-- 		zt = 1
		-- 	end
		-- 	if bb.潜能 <= 0 then
		-- 		break
		-- 	end
		-- end
		if bb.魔力 > bb.力量 then
			ls.魔力=bb.潜能
			bb.潜能 = 0
		 else
		 	ls.力量=bb.潜能
			bb.潜能 = 0
		end
	elseif self.资源组[16]:事件判断() then
		self.预览属性[self.选中] = self:取差异属性(ls)

	elseif self.资源组[17]:事件判断() then
		客户端:发送数据(self.选中,7,23,"yx")
	elseif self.资源组[18]:事件判断() then
		local 事件 = function()
			self.临时潜能[self.选中] = {体质=0,魔力=0,力量=0,耐力=0,敏捷=0}
			self.预览属性[self.选中] = {气血=0,魔法=0,伤害=0,速度=0,灵力=0,防御=0}
			if bb == self.数据[self.数据.参战] then
				self.数据[self.数据.参战] = {}
			end
			if tp.窗口.道具行囊.可视 and tp.窗口.道具行囊.窗口 == "召唤兽" then
				if bb == self.数据[self.数据.参战] then
					tp.窗口.道具行囊.选中召唤兽 = 0
					tp.窗口.道具行囊.资源组[4] = nil
					for s=1,3 do
						tp.窗口.道具行囊.召唤兽装备[s]:置物品(nil)
					end
				end
			end

            客户端:发送数据(self.选中,1,23,"9B",1)
            self.选中 = 0
			 self.加入 = max(self.加入 - 1,0)
			self.名称输入框:置可视(false,false)
			if tp.窗口.召唤兽资质栏.可视 then
				tp.窗口.召唤兽资质栏:打开()
			end

			self:置形象()
		end
	    tp.窗口.文本栏:载入("#H/真的要放生"..bb.等级.."级的#R/"..bb.名称.."#H/吗?",nil,true,事件)
	elseif self.资源组[19]:事件判断() then
		客户端:发送数据(self.选中,8,23,"66")
	elseif self.资源组[20]:事件判断() then
		if self.鉴定  then
        self.鉴定 = false
        else
      	self.鉴定 = true
       end
	elseif self.资源组[21]:事件判断() then


		  客户端:发送数据(self.选中,4,23,ls.力量.."*-*"..ls.体质.."*-*"..ls.魔力.."*-*"..ls.耐力.."*-*"..ls.敏捷,1)

		self.临时潜能[self.选中] = {体质=0,魔力=0,力量=0,耐力=0,敏捷=0}
		self.预览属性[self.选中] = {气血=0,魔法=0,伤害=0,速度=0,灵力=0,防御=0}
	elseif self.资源组[22]:事件判断() then
		tp.窗口.召唤兽资质栏:打开(self.数据[self.选中])
		tp.窗口.召唤兽资质栏.x = self.x + 370
		tp.窗口.召唤兽资质栏.y = self.y
	elseif self.资源组[27]:事件判断() then
		self.资源组[35]:置起始点(self.资源组[35]:取百分比转换(self.加入-1,4,bbsa))
		self.头像组 = {}
	elseif self.资源组[28]:事件判断() then
		self.资源组[35]:置起始点(self.资源组[35]:取百分比转换(self.加入+1,4,bbsa))
		self.头像组 = {}
	    elseif self.圣兽丹动画:是否选中(x,y) then
      tp.提示:自定义(x+40,y,"#y/该召唤兽拥有饰品可以提高#g/10%#y/的资质属性")
    elseif self.资源组[34]:事件判断() then --召唤兽染色
    	tp.窗口.召唤兽染色:打开(self.数据[self.选中],self.选中)
    elseif self.资源组[36]:事件判断() then 
          客户端:发送数据(1,15,6,"扩充召唤兽")  
     elseif self.资源组[37]:事件判断() then --召唤兽染色
    	tp.窗口.召唤兽换造型:打开(self.数据[self.选中],self.选中)
	end
	-- 拖拽事件


	if self.拽拖对象 ~= 0 then
		box(x-70,y-25,x+73,y+19,-849650981)
		self.资源组[23]:显示(x-67,y-22)
		self.拽拖事件[1]:显示(x-63,y-18)
		if bbs[self.拽拖对象].参战信息 ~= nil then
			zts:置颜色(-256)
		end
		zts:显示(x-27,y-19,bbs[self.拽拖对象].名称)
		zts:显示(x-27,y,bbs[self.拽拖对象].等级.."级")
		if mouseb(0) then
			if self.鼠标 then
				if self.插入选区 == 0 then
					self.拽拖计次 = 0
					self.拽拖对象 = 0
					self.拽拖事件 = 0
					self.插入选区 = 0
					tp.禁止关闭 = false
				else
					self:排列(self.拽拖对象,self.插入选区)
					self.拽拖计次 = 0
					self.拽拖对象 = 0
					self.拽拖事件 = 0
					self.插入选区 = 0
					tp.禁止关闭 = false
				end
			else
				self.拽拖计次 = 0
				self.拽拖对象 = 0
				self.拽拖事件 = 0
				self.插入选区 = 0
				tp.禁止关闭 = false
			end
		end
		tp.禁止关闭 = true
		self.焦点 = true
	end
	if self.资源组[35].接触 then
		self.焦点 = true
	end
end
function 场景类_召唤兽属性栏:取差异属性(sxb)
	local sx1 = self.数据[self.选中].气血上限
	local sx2 = self.数据[self.选中].魔法上限
	local sx3 = self.数据[self.选中].伤害
	local sx4 = self.数据[self.选中].防御
	local sx5 = self.数据[self.选中].速度
	local sx6 = self.数据[self.选中].灵力
	local 体质 = self.数据[self.选中].体质 + self.数据[self.选中].装备属性.体质 + sxb.体质
	local 魔力 = self.数据[self.选中].魔力 + self.数据[self.选中].装备属性.魔力 + sxb.魔力
	local 力量 = self.数据[self.选中].力量 + self.数据[self.选中].装备属性.力量 + sxb.力量
	local 耐力 = self.数据[self.选中].耐力 + self.数据[self.选中].装备属性.耐力 + sxb.耐力
	local 敏捷 = self.数据[self.选中].敏捷 + self.数据[self.选中].装备属性.敏捷 + sxb.敏捷




	local 伤害1=math.floor(self.数据[self.选中].等级*(self.数据[self.选中].攻资+self.数据[self.选中].特殊资质.攻资)*(14+10*self.数据[self.选中].成长)/7500+self.数据[self.选中].成长*(力量+self.数据[self.选中].进阶属性.力量)+20)+self.数据[self.选中].装备属性.伤害
	local 灵力1=math.floor(self.数据[self.选中].等级*(self.数据[self.选中].法资+self.数据[self.选中].特殊资质.法资+1640)*(self.数据[self.选中].成长+1)/7500+0.7*(魔力+self.数据[self.选中].进阶属性.魔力)+20+(力量+self.数据[self.选中].进阶属性.力量)*0.4+(耐力+self.数据[self.选中].进阶属性.耐力)*0.2+(体质+self.数据[self.选中].进阶属性.体质)*0.3)+self.数据[self.选中].装备属性.灵力
	local 防御1=math.floor(self.数据[self.选中].等级*(self.数据[self.选中].防资+self.数据[self.选中].特殊资质.防资)/433+self.数据[self.选中].成长*(耐力+self.数据[self.选中].进阶属性.耐力)*4/3)+self.数据[self.选中].装备属性.防御
	local 速度1=math.floor((敏捷+self.数据[self.选中].进阶属性.敏捷)*self.数据[self.选中].速资/1000)+self.数据[self.选中].装备属性.速度
	local 气血上限=math.floor(self.数据[self.选中].等级*(self.数据[self.选中].体资+self.数据[self.选中].特殊资质.体资)/1000+self.数据[self.选中].成长*(体质+self.数据[self.选中].进阶属性.体质)*6)+10+self.数据[self.选中].装备属性.气血
	local 最大魔法=math.floor(self.数据[self.选中].等级*(self.数据[self.选中].法资+self.数据[self.选中].特殊资质.法资)/2000+self.数据[self.选中].成长*(魔力+self.数据[self.选中].进阶属性.魔力)*2.5)+self.数据[self.选中].装备属性.魔法


	return {气血=气血上限-sx1,魔法=最大魔法-sx2,伤害=伤害1-sx3,防御=防御1-sx4,速度=速度1-sx5,灵力=灵力1-sx6}
end
function 场景类_召唤兽属性栏:排列(a,b)
	local bbx = self.数据
	local abs = bbx[a]
	local bbs = bbx[b]
	local aba = self.临时潜能[a]
	local bba = self.临时潜能[b]
	local abc = self.预览属性[a]
	local bbc = self.预览属性[b]
	bbx[a] = bbs
	self.临时潜能[a] = bba
	self.预览属性[a] = bbc
	bbx[b] = abs
	self.临时潜能[b] = aba
	self.预览属性[b] = abc
	if self.选中 ~= 0 then
		self.选中 = b
		self:置形象()
		self.名称输入框:置文本(bbx[self.选中].名称)
		self.名称输入框:置可视(true,true)
		if tp.窗口.召唤兽资质栏.可视 then
			tp.窗口.召唤兽资质栏:打开(bbx[self.选中])
		end
	end
	self.头像组 = {}
end



function 场景类_召唤兽属性栏:检查点(x,y)
	if self.资源组[1]:是否选中(x,y)  then
		return true
	end
end

function 场景类_召唤兽属性栏:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_召唤兽属性栏:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_召唤兽属性栏