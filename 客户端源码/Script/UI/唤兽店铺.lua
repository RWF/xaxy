local 场景类_唤兽店铺 = class(窗口逻辑)
local insert = table.insert
local tp,zts,zts1
local tos = 引擎.取金钱颜色
local mouseb = 引擎.鼠标弹起
local bw = require("gge包围盒")(0,0,244,20)
local box = 引擎.画矩形
function 场景类_唤兽店铺:初始化(根)
    self.ID = 328
    self.x = 280+(全局游戏宽度-800)/2
    self.y = 100
    self.xx = 0
    self.yy = 0
    self.注释 = "唤兽店铺"
    tp = 根
    self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    local 按钮 = 根._按钮
    local 自适应 = 根._自适应
    local 资源= 根.资源
    self.资源组 = {
        [1]= 资源:载入('JM.dll',"网易WDF动画",0xD7ACF355),
        [2] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),
        [3] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"上间"),
        [4] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"下间"),
        [5] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"取消"),
        [6] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"购买"),
        [7] = 自适应.创建(2,1,258,209,3,9),

    }
    zts = tp.字体表.普通字体
    zts1 = tp.字体表.描边字体

    for i=2,6 do
        self.资源组[i]:绑定窗口_(328)
    end
    self.窗口时间 = 0
end


function 场景类_唤兽店铺:打开(数据)
  if self.可视 then
    self.物品数据 = nil
    self.购买数据 = nil
    self.商会选中 = 0
    self.可视 = false
    if 数据 ~= 1 then
        tp.窗口.商会购买类.上一次=0
    end
  else
    if tp.窗口.物品店铺.可视 then
       tp.窗口.物品店铺:打开(1)
    end
    if tp.窗口.物品店铺.x ~= 280+(全局游戏宽度-800)/2 or tp.窗口.物品店铺.x~= 100 then
        self.x=tp.窗口.物品店铺.x
        self.y=tp.窗口.物品店铺.y
    end
    if  self.x > 全局游戏宽度 then
        self.x = 82+(全局游戏宽度-800)/2
    end
    self.物品数据 = {}
    self.购买数据 = 数据
    self.物品数据 =self.购买数据.列表
    self.购买数据.列表=nil
    self.商会选中 = 0
    insert(tp.窗口_,self)
    tp.运行时间 = tp.运行时间 + 1
    self.窗口时间 = tp.运行时间
    self.可视 = true
   end
 end

function 场景类_唤兽店铺:刷新(数据)
    self.物品数据 = {}
    self.购买数据 = 数据
    self.物品数据 =self.购买数据.列表
    self.购买数据.列表=nil
    self.商会选中 = 0
 end

function 场景类_唤兽店铺:显示(dt,x,y)
    self.焦点 = false
    for i=2,6 do
       self.资源组[i]:更新(x,y)
    end

    self.资源组[1]:显示(self.x,self.y)
    self.资源组[2]:显示(self.x+270,self.y+6)
    self.资源组[7]:显示(self.x+17,self.y+106)
    if self.资源组[7]:是否选中(x,y) then
     self.焦点1=true
    else
     self.焦点1=nil
    end
    zts:置颜色(0xFFFFFFFF):显示(self.x+120, self.y+59, self.购买数据.创店日期)
    zts:置颜色(0xFFFFFFFF):显示(self.x+238, self.y+37, "0%")
    zts:置颜色(0xFF000000):显示(self.x+59, self.y+83, self.购买数据.店主名称,true)
    zts:置颜色(0xFF000000):显示(self.x+203, self.y+83, self.购买数据.店主id)
    zts:置颜色(0xFF000000):显示(self.x+56, self.y+395, self.购买数据.类型 .. "/" .. self.购买数据.店面)
    zts1:置颜色(4294967040):显示(self.x+15, self.y+35, self.购买数据.店名)
    zts:置颜色(tos(self.购买数据.现金)):显示(self.x+196, self.y+361, self.购买数据.现金)
    self.资源组[3]:显示(self.x+100, self.y+392,true)
    self.资源组[4]:显示(self.x+145, self.y+392,true)
    self.资源组[5]:显示(self.x+190, self.y+392,true)
    self.资源组[6]:显示(self.x+235, self.y+392,true)
    local 序号 = 0
    for i=1,#self.物品数据 do
        if self.物品数据[i] and self.物品数据[i].道具~=nil then
            序号 = 序号 + 1
            zts:置颜色(-16777216)
            local jx = self.x+28
            local jy = self.y+96+序号*20
            bw:置坐标(jx-4,jy-3)
            local xz = bw:检查点(x,y)
            if self.商会选中 ~= i then
                if  xz and not tp.消息栏焦点 and self.鼠标 then
                  box(jx-5,jy-6,jx+240,jy+15,-3551379)
                    if mouseb(0) then
                      self.商会选中=i
                    elseif mouseb(1)  then
                      tp.窗口.召唤兽查看栏:打开(self.物品数据[i].道具)
                    end
                end
            else
                if  xz and not tp.消息栏焦点 and self.鼠标 then
                    if mouseb(1)  then
                      tp.窗口.召唤兽查看栏:打开(self.物品数据[i].道具)
                    end
                end
                box(jx-5,jy-6,jx+240,jy+15,-10790181)
            end
            zts:置颜色(0xFF000000):显示(jx,jy-2,self.物品数据[i].道具.名称)
       end
    end
    if self.商会选中 ~= 0 then
        zts:置颜色(tos(self.物品数据[self.商会选中].价格)):显示(self.x + 66, self.y + 332,self.物品数据[self.商会选中].价格)
        zts:置颜色(tos(self.物品数据[self.商会选中].价格)):显示(self.x + 66, self.y + 361,self.物品数据[self.商会选中].价格)
    end

        if self.资源组[2]:事件判断() or self.资源组[5]:事件判断() then
           self:打开()
        elseif self.资源组[3]:事件判断() then
            客户端:发送数据(self.购买数据.类型, 139, 36, self.购买数据.商铺id, 1)
        elseif self.资源组[4]:事件判断() then
            客户端:发送数据(self.购买数据.类型, 140, 36, self.购买数据.商铺id, 1)
        elseif self.资源组[6]:事件判断() then
            if self.商会选中 == 0 then
                tp.提示:写入("#y/请先选中要购买的召唤兽")
            else
                客户端:发送数据(self.购买数据.类型, self.商会选中, 75, self.购买数据.商铺id)
            end
        end

end




return 场景类_唤兽店铺



