-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-12-19 01:32:04
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--
local 场景类_玩家小窗口 = class()
local tp,zts1,zts
local insert = table.insert

function 场景类_玩家小窗口:初始化(根)
	self.ID = 133
	self.x = 420+(全局游戏宽度-800)/2
	self.y = 100
	self.xx = 0
	self.yy = 0
	self.注释 = "玩家小窗口"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = false
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 资源:载入("JM.dll","网易WDF动画",0x770F5F96),
		[2] = 资源:载入("JM.dll","网易WDF动画",0xA0D00989),
		[3] =  按钮.创建(资源:载入("JM.dll","网易WDF动画",0x91D4E204),0,0,4,true,true,"给予"),
		[4] =  按钮.创建(资源:载入("JM.dll","网易WDF动画",0x91D4E204),0,0,4,true,true,"交易"),
		[5] =  按钮.创建(资源:载入("JM.dll","网易WDF动画",0x91D4E204),0,0,4,true,true,"组队"),
		[6] =  按钮.创建(资源:载入("JM.dll","网易WDF动画",0x91D4E204),0,0,4,true,true,"装备"),
		[7] =  按钮.创建(资源:载入("JM.dll","网易WDF动画",0x91D4E204),0,0,4,true,true,"好友"),
		[8] =  按钮.创建(资源:载入("JM.dll","网易WDF动画",0x91D4E204),0,0,4,true,true,"攻击"),
		[9] =  按钮.创建(资源:载入("JM.dll","网易WDF动画",0x91D4E204),0,0,4,true,true,"团队"),
		[10] =  按钮.创建(资源:载入("JM.dll","网易WDF动画",0x91D4E204),0,0,4,true,true,"信息"),
		[11] =  按钮.创建(资源:载入("JM.dll","网易WDF动画",0x91D4E204),0,0,4,true,true,"观察"),
		[12] =  按钮.创建(资源:载入("JM.dll","网易WDF动画",0x91D4E204),0,0,4,true,true,"临时"),

	}
   self.人物头像背景={}
 	for i=3,12 do
		self.资源组[i]:绑定窗口_(133)
		self.资源组[i]:置文字颜色(0xFF000000)
		self.资源组[i]:置偏移(-2,0)
	end
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体_
	zts1 = tp.字体表.描边字体
end

function 场景类_玩家小窗口:打开(数据,x,y)
	if  self.可视 then
		self.可视 = false
	else
	if  self.x > 全局游戏宽度 then
		self.x = 420+(全局游戏宽度-800)/2
   end
		insert(tp.窗口_,self)
		self.数据 = 数据
		tp.运行时间 = tp.运行时间 + 3
		self.窗口时间 = tp.运行时间
		self.可视 = true
       local x 
		  if self.数据.变身  then
			 x =   ModelData[self.数据.变身]
					
		  else
			x =  ModelData[self.数据.造型]
					
		  end
		  self.人物头像背景 = tp.资源:载入(x.头像资源,"网易WDF动画",x.中头像)
	end
end

function 场景类_玩家小窗口:显示(dt,x,y)
	self.焦点 = false
	for i=3,12 do
		self.资源组[i]:更新(x,y)
	end

	if self.资源组[3]:事件判断() then
		客户端:发送数据(self.数据.id+0,2,26,"9A@-@14",1)
	elseif  self.资源组[4]:事件判断() then
		客户端:发送数据(self.数据.id+0,6,27,"HY@-@S1Q1",1)
	elseif  self.资源组[5]:事件判断() then
			客户端:发送数据(self.数据.id+0,6,11,"69@-@79",1)
	elseif  self.资源组[6]:事件判断() then
		  客户端:发送数据(self.数据.id+0,62,62,"69@-@79",1)
	elseif  self.资源组[7]:事件判断() then
			客户端:发送数据(self.数据.id+0,2, 54, "9s")
	elseif  self.资源组[8]:事件判断() then
          客户端:发送数据(self.数据.id+0,6,28,"P7@-@Q4",1)
	elseif  self.资源组[9]:事件判断() then
	elseif  self.资源组[10]:事件判断() then
	elseif  self.资源组[11]:事件判断() then
	elseif  self.资源组[12]:事件判断() then
	end
	tp.人物头像背景_:显示(self.x+50,self.y+0)
    self.人物头像背景:显示(self.x+53,self.y+3)
      self.资源组[1]:显示(self.x+100,self.y+0)
       self.资源组[2]:显示(self.x+100,self.y+52)

		self.资源组[3]:显示(self.x+110,self.y+55)
		self.资源组[4]:显示(self.x+158,self.y+55)
		self.资源组[5]:显示(self.x+110,self.y+80)
		self.资源组[6]:显示(self.x+158,self.y+80)
		self.资源组[7]:显示(self.x+110,self.y+105)
		self.资源组[8]:显示(self.x+158,self.y+105)
		self.资源组[9]:显示(self.x+110,self.y+130)
		self.资源组[10]:显示(self.x+158,self.y+130)
		self.资源组[11]:显示(self.x+110,self.y+155)
		self.资源组[12]:显示(self.x+158,self.y+155)
             zts:置颜色(0xFFFFFF00):显示(self.x+103,self.y+1,self.数据.名称)
       zts:置颜色(0xFFFFFF00):显示(self.x+103,self.y+18,self.数据.id)
       if self.数据.门派 then
       	 zts:置颜色(0xFFFFFF00):显示(self.x+103,self.y+36,self.数据.门派)
       else
         zts:置颜色(0xFFFFFF00):显示(self.x+103,self.y+36,"无门派")
       end
end



function 场景类_玩家小窗口:检查点(x,y)
	if self.资源组[1]:是否选中(x,y) or  self.资源组[2]:是否选中(x,y) or  tp.人物头像背景_:是否选中(x,y)  then
		return true
	end
end

function 场景类_玩家小窗口:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	self.窗口时间 = tp.运行时间
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end


function 场景类_玩家小窗口:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end


return 场景类_玩家小窗口