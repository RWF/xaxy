--======================================================================--
-- @作者: QQ381990860
-- @创建时间:	2019-12-03 02:17:19
-- @Last Modified time: 2021-06-28 22:32:20
--======================================================================--
local 场景类_召唤兽换形 = class(窗口逻辑)
local min = math.min
local floor = math.floor
local tp,zts
local mouseb = 引擎.鼠标弹起
local box = 引擎.画矩形
local insert = table.insert

function 场景类_召唤兽换形:初始化(根)
	self.ID = 154
	self.x = 210
	self.y = 41
	self.xx = 0
	self.yy = 0
	self.注释 = "召唤兽换形"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.焦点1 =false
	self.状态 = " 0 - 15级"
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 自适应.创建(99,1,440,481,3,9),

		[3] = 资源:载入('JM.dll',"网易WDF动画",0xA19838E8),
		[4] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
		[5] = 自适应.创建(93,1,140,22,1,3),
		[93] =按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFF205590),0,0,1,true,true),
		[94] = 按钮.创建(自适应.创建(103,4,43,22,1,3),0,0,4,true,true,"取消"),
		[95] = 按钮.创建(自适应.创建(103,4,80,22,1,3),0,0,4,true,true," 0 - 15级"),
		[96] = 按钮.创建(自适应.创建(103,4,80,22,1,3),0,0,4,true,true,"25 - 65级"),
		[97] = 按钮.创建(自适应.创建(103,4,80,22,1,3),0,0,4,true,true,"75 -105级"),
		[98] = 按钮.创建(自适应.创建(103,4,80,22,1,3),0,0,4,true,true,"115-145级"),
		[99] = 按钮.创建(自适应.创建(103,4,80,22,1,3),0,0,4,true,true,"155-175级"),
		[100] = 按钮.创建(自适应.创建(103,4,43,22,1,3),0,0,4,true,true,"改变"),
		
	}
	self.资源组[4]:绑定窗口_(154)
	for i=93,100 do
		self.资源组[i]:绑定窗口_(154)
	end
		self.资源组[93]:置偏移(-3,2) 
	

	self.介绍文本 = 根._丰富文本(100,280,根.字体表.普通字体)

	self.介绍文本:添加文本("#G/1.转换造型后自动清空染色效果\n#R/2.自动清空饰品\n#Y/3.未进阶的宝宝转换后不允许进阶\n#W/4.保留之前原有的一切属性\n#B/某些进阶宝宝无法转换比如进阶大海龟")

	self.窗口时间 = 0
	self.进阶模式=false
	tp = 根
	zts = tp.字体表.华康字体
	self.选中状态 ="无"
end

function 场景类_召唤兽换形:打开(数据,编号)
	if self.可视 then
		self.可视 = false
		self.Button={}
		self.选中状态 ="无"
		self.进阶模式=false
		self.资源组[106]=nil
		self.资源组[107]=nil
		self.资源组[101]=nil
		self.资源组[102]=nil
		self.资源组[93]:置打勾框(self.进阶模式)
	else
		self.数据 =数据
		self.编号=编号
		self.染色 = nil
		local n = 取模型(数据.造型)
		self.资源组[101] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
		if 数据.饰品 then
			n = 取模型("饰品_"..数据.造型)
			self.资源组[102] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
			self.资源组[102]:置方向(0)
		elseif 取召唤兽武器(数据.造型)	then
			local ns=取模型("武器_"..数据.造型)
			self.资源组[102] = tp.资源:载入(ns.战斗资源,"网易WDF动画",ns.待战)
			self.资源组[102]:置方向(0)
		end
		if 数据.染色方案 ~= nil then
			self.资源组[101]:置染色(数据.染色方案,数据.染色组[1],数据.染色组[2],数据.染色组[3])
			if self.资源组[102] then
				self.资源组[102]:置染色(数据.染色方案,数据.染色组[1],数据.染色组[2],数据.染色组[3])
			end
			self.资源组[101]:置方向(0)
		end
		self.状态 = " 0 - 15级"
		self:InitializeButton(1)
		insert(tp.窗口_,self)
		self.头像组 = {}
		tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
		self.可视 = true
		self.选中状态 ="无"
	end
end

function 场景类_召唤兽换形:InitializeButton(number)
	self.Button={}
	for k,v in pairs(PetDataN[number]) do
		if self.进阶模式 then
			self.Button[k]=tp._按钮.创建(tp._自适应.创建(105,4,80,20,1,3),0,0,4,true,true,"进阶"..v)
		else 
			self.Button[k]=tp._按钮.创建(tp._自适应.创建(105,4,80,20,1,3),0,0,4,true,true,v)
		end
		self.Button[k]:绑定窗口_(154)
	end
	self.选中状态="无"
end

function 场景类_召唤兽换形:显示(dt,x,y)
	self.资源组[4]:更新(x,y)
	self.资源组[100]:更新(x,y,self.选中状态~="无")
	self.资源组[94]:更新(x,y)
	self.资源组[93]:更新(x,y)
	if self.资源组[4]:事件判断() or self.资源组[94]:事件判断() then
		self:打开()
		return false
	elseif self.资源组[93]:事件判断() then
		self.进阶模式 = not self.进阶模式
		self.资源组[93]:置打勾框(self.进阶模式)
		self.状态 = " 0 - 15级"
		self:InitializeButton(1)
	end
	self.焦点1 =false
	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
	Picture.标题:显示(self.x+self.资源组[1].宽度/2-90,self.y)
	zts:置颜色(0xFFFFFFFF):显示(self.x+170,self.y+3,"召唤兽换造型")
	for i=95,99 do
		self.资源组[i]:更新(x,y,self.状态~=self.资源组[i]:取文字())
		self.资源组[i]:显示(self.x+(i-95)*85+10,self.y+36,true,1,nil,self.状态==self.资源组[i]:取文字(),2)
		if self.资源组[i]:事件判断()then
			self.状态=self.资源组[i]:取文字()
			self:InitializeButton(i-94)
		end 
	end
	self.资源组[4]:显示(self.x+283+135,self.y+6,true)

	self.资源组[3]:显示(self.x+10,self.y+95)
	self.资源组[3]:显示(self.x+273,self.y+95)
	self.资源组[5]:显示(self.x+22,self.y+66,true)
	self.资源组[5]:显示(self.x+285,self.y+66,true)
	zts:置颜色(0xFF000000):显示(self.x+55,self.y+69,self.数据.名称)
	tp.影子:显示(self.x+90,self.y+216)
	self.资源组[101]:更新(dt)
	self.资源组[101]:显示(self.x+90,self.y+216)
	if self.资源组[102] ~= nil then
		self.资源组[102]:更新(dt)
		self.资源组[102]:显示(self.x+90,self.y+216)
	end
	self.介绍文本:显示(self.x+170,self.y+66)
	local xx,yy =0,0
	for i=1,#self.Button do
		local jx = xx * 85+ 10
		local jy = yy *	33+ 254
		self.Button[i]:更新(x,y,self.选中状态~=self.Button[i]:取文字())
		self.Button[i]:显示(self.x+jx,self.y+ jy,true,1,nil,self.选中状态==self.Button[i]:取文字(),2)
		if self.Button[i]:事件判断()then
			local 造型 =self.Button[i]:取文字()
			self.资源组[107]=nil
			if type(无错误取模型(造型)) =="table" then 
				local n = 取模型(造型)
				self.资源组[106] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
				if	取召唤兽武器(造型)	then
					local ns=取模型("武器_"..造型)
					self.资源组[107] = tp.资源:载入(ns.战斗资源,"网易WDF动画",ns.待战)
					self.资源组[107]:置方向(0)
				end
				self.选中状态=造型
			else 
				self.选中状态="无"
				tp.提示:写入("#Y/此召唤兽不能改变造型！")
			end
		end
		xx = xx + 1
		if xx == 5 then
			xx = 0
			yy = yy + 1
		end
	end
	self.资源组[5]:显示(self.x+145,self.y+447,true)
	if self.选中状态~="无" then 
		if self.进阶模式	then 
			zts:置颜色(0xFF0FFF49):显示(self.x+22,self.y+452,"改变造型仙玉需要：	"..PetData[string.sub(self.选中状态,5,string.len(self.选中状态) )].换造型)
		else
			zts:置颜色(0xFF0FFF49):显示(self.x+22,self.y+452,"改变造型仙玉需要：	"..PetData[self.选中状态].换造型)
		end 
		if self.资源组[100]:事件判断() and self.选中状态~="无" then
			客户端:发送数据(self.编号,9,23,self.选中状态,self.进阶模式)
		end
		tp.影子:显示(self.x+352,self.y+216)
		self.资源组[106]:更新(dt)
		self.资源组[106]:显示(self.x+352,self.y+216)
		if self.资源组[107] ~= nil then
			self.资源组[107]:更新(dt)
			self.资源组[107]:显示(self.x+352,self.y+216)
		end
		zts:置颜色(0xFF000000):显示(self.x+325,self.y+69,self.选中状态)
	else 
		zts:置颜色(0xFF0FFF49):显示(self.x+22,self.y+452,"改变造型仙玉需要：")
	end
	zts:置颜色(0xFFFFE61A):显示(self.x+330,self.y+423,"是否进阶")
	self.资源组[93]:显示(self.x+390,self.y+418)
	self.资源组[100]:显示(self.x+310,self.y+447,true,1)
	self.资源组[94]:显示(self.x+380,self.y+447,true,1)
	zts:置颜色(-16777216)
end




return 场景类_召唤兽换形


