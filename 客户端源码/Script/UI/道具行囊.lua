--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--
local bw = require("gge包围盒")(0,0,100,22)
local bw1= require("gge包围盒")(0,0,100,17)
local box = 引擎.画矩形
local floor = math.floor
local tp
local format = string.format


local xys = 生成XY
local insert = table.insert
local type = type
local min = math.min
local qjq = 引擎.取金钱颜色
local zqj = 引擎.坐骑库

local 场景类_道具行囊 = class(窗口逻辑)

function 场景类_道具行囊:初始化(根)
	self.ID = 9
	self.x = 0
	self.y = 0
	self.xx = 0
	self.yy = 0
	self.注释 = "道具行囊"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	self.窗口 = "主人公"
	self.召唤兽 = 0
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 资源:载入('JM.dll',"网易WDF动画",0x83084DEB),
		[2] = 资源:载入('JM.dll',"网易WDF动画",0x4B2AFA64),
		[3] = 资源:载入('JM.dll',"网易WDF动画",0x9B1DB10D),
		[5] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xF11233BB),0,0,4,true,true),
		[6] = 按钮.创建(自适应.创建(10,4,37,20,1,3),0,0,4,true,true,"人物"),
		[7] = 按钮.创建(自适应.创建(10,4,37,20,1,3),0,0,4,true,true,"召唤"),
		[8] = 按钮.创建(自适应.创建(16,4,43,22,1,3),0,0,4,true,true,"现金"),
		[9] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFD3D61F2),0,0,4,true,true),
		[10] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x09217E13),0,0,4,true,true),
		[11] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x63CC8A23),0,0,4,true,true),
		[12] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x00D13BBF),0,0,4,true,true),
        [13] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x18C30C41),0,0,4,true,true), ---法宝按钮
      
        [14] =  按钮.创建(资源:载入('MAX.7z',"网易WDF动画","72116F63"),0,0,4,true,true,"1"),
        [15] =  按钮.创建(资源:载入('MAX.7z',"网易WDF动画","72116F63"),0,0,4,true,true,"2"),
        [16] =  按钮.创建(资源:载入('MAX.7z',"网易WDF动画","72116F63"),0,0,4,true,true,"3"),
        [17] =  按钮.创建(资源:载入('MAX.7z',"网易WDF动画","72116F63"),0,0,4,true,true,"4"),
        [18] =  按钮.创建(自适应.创建(25,4,19,19,4,3),0,0,4,true,true),
		[28] =  按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"使用"),
		[29] =  按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"修炼"),
		[30] = 按钮.创建(自适应.创建(10,4,37,20,1,3),0,0,4,true,true,"坐骑"),
		[31] = 按钮.创建(自适应.创建(10,4,37,20,1,3),0,0,4,true,true,"子女"),
		[34] = 资源:载入('JM.dll',"网易WDF动画",0xA7CE2F61),
		[35] = 资源:载入('JM.dll',"网易WDF动画",0x115E9954),
		[36] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"查看"),
		[37] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"抛弃"),
		[38] = 按钮.创建(自适应.创建(10,4,24,20,1,3),0,0,4,true,true,"1"),
		[39] = 按钮.创建(自适应.创建(10,4,24,20,1,3),0,0,4,true,true,"2"),
		[40] = 按钮.创建(自适应.创建(10,4,24,20,1,3),0,0,4,true,true,"饰"),
		[41] =  按钮.创建(自适应.创建(16,4,43,22,1,3),0,0,4,true,true,"加锁"),
		[42] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"骑乘"),
		[43] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"详细"),
		[44] =  按钮.创建(自适应.创建(16,4,43,22,1,3),0,0,4,true,true,"锦衣"),
		[45] =  按钮.创建(自适应.创建(16,4,43,22,1,3),0,0,4,true,true,"出售"),
		[46] =  按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xEF45CDC6)),
		[47] = 按钮.创建(自适应.创建(10,4,24,20,1,3),0,0,4,true,true,"3"),
		[48] =  按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x74545C3F),0,0,3,true,true), --
		[49] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x92ABEFD3),0,0,4,true,true),
		[50] =	按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x49D09C8B),0,0,4,true,true),
        [51]  = 资源:载入('JM.dll',"网易WDF动画",0x676FB611),
        [52] = 按钮.创建(自适应.创建(10,4,24,20,1,3),0,0,4,true,true,"仓"),
        [53] =  资源:载入('JM.dll',"网易WDF动画",0x3906F9F1),
        [54] = 按钮.创建(自适应.创建(10,4,24,20,1,3),0,0,4,true,true,"拆"),

        -- 72116F63

	}
	self.装备坐标 = {x={195,249,195,249,195,249},y={24,24,79,79,133,133},xx={10,63,115},yy={144,144,144}}
	self.人物装备 = {}
	self.召唤兽装备 = {}
	tp = 根
	local 格子 = 根._物品格子
	local 底图 = {根.资源:载入("帽子.png","加密图片"),根.资源:载入("项链.png","加密图片"),根.资源:载入("底图.png","加密图片"),根.资源:载入("底图.png","加密图片"),根.资源:载入("腰带.png","加密图片"),根.资源:载入("鞋子.png","加密图片")}
	for i=1,6 do
		self.人物装备[i] = 格子(0,0,i,"道具行囊_人物装备",底图[i])
	end
	self.人物装备[1]:置根(根)
	local 底图 = {根.资源:载入("护手.png","加密图片"),根.资源:载入("底图.png","加密图片"),根.资源:载入("铠甲.png","加密图片")}
	for i=1,3 do
		self.召唤兽装备[i] = 格子(0,0,i,"道具行囊_召唤兽装备",底图[i])
	end
	self.坐骑饰品= 格子(0,0,1,"道具行囊_坐骑饰品",底图[1])
	self.物品 = {}
		local 底图 = 根.资源:载入("底图2.png","加密图片")

	self.锦衣 = {}
	for i=1,6 do
		self.锦衣[i] = 格子(0,0,i,"道具行囊_锦衣",底图)
	end
	self.方向 =0
	self.选中召唤兽 = 0
	self.加入 = 0
	for n=5,13 do
		if self.资源组[n] ~= nil  then
			self.资源组[n]:绑定窗口_(9)
		end
	end
	for n=14,17 do

			self.资源组[n]:绑定窗口_(9)
			self.资源组[n]:置偏移(-2,4)
	end
	self.资源组[18]:绑定窗口_(9)
	for n=28,49 do
		if self.资源组[n] ~= nil  and n~=34 and n~=35 then
		self.资源组[n]:绑定窗口_(9)
	end
	end
	self.资源组[52]:绑定窗口_(9)
	self.资源组[38]:置偏移(0,-1)
	self.资源组[39]:置偏移(0,-1)
	self.资源组[47]:置偏移(0,-1)
	self.资源组[40]:置偏移(-2,-1)
	self.资源组[52]:置偏移(-2,-1)
	self.资源组[54]:绑定窗口_(9)
	self.资源组[54]:置偏移(-2,-1)
	self.窗口时间 = 0
	self.提示文字 = "Shift+鼠标右键可以打开符石镶嵌界面。"
	self.当前银行 = "现金"
	self.记录_ = 0
end
function 场景类_道具行囊:刷新(数据)
   self.数据=数据
   	 self.数据.子女列表={}
   self.数据.宝宝列表 = {}
   self.更新  =self.数据.类型
   	local 格子 = tp._物品格子
	for i=1,20 do
		self.物品[i] = 格子.创建(0,0,i,self.数据.类型)
	end
        for s=1,3 do
			self.召唤兽装备[s]:置物品(nil)
		end
		self.坐骑饰品:置物品(nil)

		for i=1,6 do
			self.人物装备[i]:置物品(self.数据[i+20])
		end
		for q=1,20 do
			self.物品[q]:置物品(self.数据[q])
		end

		for i=1,6 do
		 self.锦衣[i]:置物品(self.数据[i+39])
		end

		if  self.更新=="锦衣" then
			self.窗口 ="锦衣"
			self:置形象()
		else
			if self.窗口 ~="召唤兽" and self.窗口 ~="坐骑"  and self.窗口 ~="子女"  then
			 self.选中召唤兽 = 0
			self.加入 = 0
			self.窗口 ="主人公"

		  	self.资源组[4] = tp.资源:载入(ModelData[tp.场景.人物.数据.造型].头像资源,"网易WDF动画",ModelData[tp.场景.人物.数据.造型].头像组.道具)
			end


		end
       if self.窗口 =="召唤兽" then
          客户端:发送数据(19,19,19)
       elseif self.窗口 =="坐骑"then
       	客户端:发送数据(28,28,5)
       elseif self.窗口 =="子女"then
       	客户端:发送数据(29,29,5)
       end
		--
		self.当前银行 = "现金"
end

function 场景类_道具行囊:打开(数据)
	if self.可视 then
		self.可视 = false
		self.选中召唤兽 = nil
		self.加入 = nil
		self.当前银行 = nil
		self.窗口 = nil
        
		self.方向=0
		for s=1,3 do
			self.召唤兽装备[s]:置物品(nil)
		end
		self.坐骑饰品:置物品(nil)
		for i=1,6 do
			self.人物装备[i]:置物品(nil)
		end
	
		for i=1,6 do
			self.锦衣[i]:置物品(nil)
		end
						tp.抓取物品 = nil
						tp.抓取物品ID = nil
						tp.抓物品注释 = nil
		self.更新 = nil


		if tp.窗口.灵饰.可视 then
			tp.窗口.灵饰:打开()
		end
	else


		self.数据=数据
		self.数据.子女列表={}
		self.数据.宝宝列表 = {}
	   self.更新  =self.数据.类型
	   	local 格子 = tp._物品格子
		for i=1,20 do
			self.物品[i] = 格子.创建(0,0,i,self.数据.类型)
		end

        for s=1,3 do
			self.召唤兽装备[s]:置物品(nil)
		end
		self.坐骑饰品:置物品(nil)
       self:置形象()
       local 底图 = {tp.资源:载入("帽子.png","加密图片"),tp.资源:载入("项链.png","加密图片"),tp.资源:载入("底图.png","加密图片"),tp.资源:载入("底图.png","加密图片"),tp.资源:载入("腰带.png","加密图片"),tp.资源:载入("鞋子.png","加密图片")}
		for i=1,6 do
			self.人物装备[i] = 格子(0,0,i,"道具行囊_人物装备",底图[i])
			self.人物装备[i]:置物品(self.数据[i+20])
		end

		for q=1,20 do
			self.物品[q]:置物品(self.数据[q])
		end

		for i=1,6 do
		 self.锦衣[i]:置物品(self.数据[i+39])
		end
		insert(tp.窗口_,self)
		self.选中召唤兽 = 0
		self.加入 = 0
		self.当前银行 = "现金"
		self.窗口 = "主人公"
		self.资源组[4] = tp.资源:载入(ModelData[tp.场景.人物.数据.造型].头像资源,"网易WDF动画",ModelData[tp.场景.人物.数据.造型].头像组.道具)
	    tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_道具行囊:置形象()
	self.资源组[4] = nil
    self.资源组[98] = nil
	self.资源组[99] = nil
	self.资源组[100] = nil
	self.资源组[101] = nil
	if self.窗口 == "召唤兽" then
		if self.选中召唤兽 ~= 0 and self.数据.宝宝列表[self.选中召唤兽]  ~= nil then
			local n = 取模型(self.数据.宝宝列表[self.选中召唤兽].造型)
			self.资源组[4] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)

			if self.数据.宝宝列表[self.选中召唤兽].饰品  then
				if 取召唤兽饰品(self.数据.宝宝列表[self.选中召唤兽].造型) then
				 local n = 取模型("饰品_"..self.数据.宝宝列表[self.选中召唤兽].造型)
				self.资源组[98] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
				end
			elseif 取召唤兽武器(self.数据.宝宝列表[self.选中召唤兽].造型) then
				 local n = 取模型("武器_"..self.数据.宝宝列表[self.选中召唤兽].造型)
				self.资源组[98] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
			end
			if self.数据.宝宝列表[self.选中召唤兽].染色组 ~= nil and DyeData[self.数据.宝宝列表[self.选中召唤兽].造型] then  
				self.资源组[4]:置染色(self.数据.宝宝列表[self.选中召唤兽].染色方案,self.数据.宝宝列表[self.选中召唤兽].染色组[1],self.数据.宝宝列表[self.选中召唤兽].染色组[2],self.数据.宝宝列表[self.选中召唤兽].染色组[3])
				self.资源组[4]:置方向(0)
				if self.资源组[98] then
					
					self.资源组[98]:置方向(0)
				end
			end
			for i=1,3 do
				self.召唤兽装备[i]:置物品(self.数据.宝宝列表[self.选中召唤兽].道具数据[i])
			end



		end
	 elseif self.窗口 == "锦衣" then
				if tp.场景.人物.数据.武器数据.类别 ~= 0 and tp.场景.人物.数据.武器数据.类别 ~= "" and tp.场景.人物.数据.武器数据.类别 ~= nil then
					local v = tp.场景.人物.数据.武器数据.类别
					if tp.场景.人物.数据.武器数据.名称 == "龙鸣寒水" or tp.场景.人物.数据.武器数据.名称 == "非攻" then
						v = "弓弩1"
					end
					local 资源 = 取模型(tp.场景.人物.数据.造型,v)
					self.资源组[4] = tp.资源:载入(资源.资源,"网易WDF动画",资源.静立)
					local m = tp:qfjmc(tp.场景.人物.数据.武器数据.类别,tp.场景.人物.数据.武器数据.等级,tp.场景.人物.数据.武器数据.名称)
					local n = 取模型(m.."_"..tp.场景.人物.数据.造型)
					self.资源组[99] = tp.资源:载入(n.资源,"网易WDF动画",n.静立)
					if tp.场景.人物.数据.武器数据.染色 then
					self.资源组[99]:置染色(tp.场景.人物.数据.武器数据.染色.染色方案,tp.场景.人物.数据.武器数据.染色.染色组.a,tp.场景.人物.数据.武器数据.染色.染色组.b)
				end
					self.资源组[99]:置方向(0)
				else
						local n = 取模型(tp.场景.人物.数据.造型)
						self.资源组[4] = tp.资源:载入(n.资源,"网易WDF动画",n.静立)
				end



			   	if tp.场景.人物.数据.锦衣数据.定制 then
    				local n = 取模型(tp.场景.人物.数据.锦衣数据.定制)
					self.资源组[4] = tp.资源:载入(n.资源,"网易WDF动画",n.静立)

		         elseif tp.场景.人物.数据.锦衣数据.战斗锦衣 then
						local n = 取模型(tp.场景.人物.数据.锦衣数据.战斗锦衣)
						self.资源组[4] = tp.资源:载入(n.资源,"网易WDF动画",n.静立)

				elseif tp.场景.人物.数据.锦衣数据.锦衣 then
						local n = 取模型(tp.场景.人物.数据.锦衣数据.锦衣.."_"..tp.场景.人物.数据.造型)
						self.资源组[4] = tp.资源:载入(n.资源,"网易WDF动画",n.静立)

				end
				if tp.场景.人物.数据.锦衣数据.光环 then
				   	local n = 引擎.取光环(tp.场景.人物.数据.锦衣数据.光环)
					self.资源组[100] = tp.资源:载入(n[4],"网易WDF动画",n[1])
				end
				if tp.场景.人物.数据.锦衣数据.脚印 then
				   	local n = 引擎.取脚印(tp.场景.人物.数据.锦衣数据.脚印)
					self.资源组[101] = tp.资源:载入(n[2],"网易WDF动画",n[1])
					self.资源组[101]:置方向(0)
				end
				if tp.场景.人物.数据.锦衣数据.锦衣==nil and tp.场景.人物.数据.锦衣数据.定制==nil   and tp.场景.人物.数据.锦衣数据.战斗锦衣==nil then
					self.资源组[4]:置染色(DyeData[tp.场景.人物.数据.造型][1],tp.场景.人物.数据.染色.a,tp.场景.人物.数据.染色.b,tp.场景.人物.数据.染色.c) 
				end
			self.资源组[4]:置方向(0)
	elseif self.窗口 == "子女" then
		if self.选中召唤兽 ~= 0 and self.数据.子女列表[self.选中召唤兽]  ~= nil then
			local n = 取模型(self.数据.子女列表[self.选中召唤兽].造型)
			self.资源组[4] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
			if self.数据.子女列表[self.选中召唤兽].饰品  then
				if 取召唤兽饰品(self.数据.子女列表[self.选中召唤兽].造型) then
				 local n = 取模型("饰品_"..self.数据.子女列表[self.选中召唤兽].造型)
				self.资源组[98] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
				end
			elseif 取召唤兽武器(self.数据.子女列表[self.选中召唤兽].造型) then
				 local n = 取模型("武器_"..self.数据.子女列表[self.选中召唤兽].造型)
				self.资源组[98] = tp.资源:载入(n.战斗资源,"网易WDF动画",n.待战)
			end
			if self.数据.子女列表[self.选中召唤兽].染色方案 ~= nil then
				self.资源组[4]:置染色(self.数据.子女列表[self.选中召唤兽].染色方案,self.数据.子女列表[self.选中召唤兽].染色组[1],self.数据.子女列表[self.选中召唤兽].染色组[2],self.数据.子女列表[self.选中召唤兽].染色组[3])
				self.资源组[4]:置方向(0)
				if self.资源组[98] then
					self.资源组[98]:置染色(self.数据.子女列表[self.选中召唤兽].染色方案,self.数据.子女列表[self.选中召唤兽].染色组[1],self.数据.子女列表[self.选中召唤兽].染色组[2],self.数据.子女列表[self.选中召唤兽].染色组[3])
					self.资源组[98]:置方向(0)
				end
			end
			for i=1,3 do
				self.召唤兽装备[i]:置物品(self.数据.子女列表[self.选中召唤兽].道具数据[i])
			end
			tp.窗口.召唤兽查看栏:刷新(self.数据.子女列表[self.选中召唤兽],self.选中召唤兽)
		end
	elseif self.窗口 == "坐骑" then
		if self.选中召唤兽 ~= 0 and self.数据.坐骑数据[self.选中召唤兽]  ~= nil then
			local n = 引擎.坐骑库(tp.场景.人物.数据.造型,self.数据.坐骑数据[self.选中召唤兽].类型,self.数据.坐骑数据[self.选中召唤兽].饰品 or "空")
			self.资源组[4] = tp.资源:载入(n.坐骑资源,"网易WDF动画",n.坐骑站立)


		if self.数据.坐骑数据[self.选中召唤兽].染色组 then
   			self.资源组[4]:置染色(self.数据.坐骑数据[self.选中召唤兽].类型,self.数据.坐骑数据[self.选中召唤兽].染色组[1],self.数据.坐骑数据[self.选中召唤兽].染色组[2],self.数据.坐骑数据[self.选中召唤兽].染色组[3])
   		end


			if self.数据.坐骑数据[self.选中召唤兽].饰品  ~= nil then
				self.资源组[99] = tp.资源:载入(n.坐骑饰品资源,"网易WDF动画",n.坐骑饰品站立)
					if self.数据.坐骑数据[self.选中召唤兽].装备.染色组 then
					self.资源组[99]:置染色(self.数据.坐骑数据[self.选中召唤兽].装备.名称,self.数据.坐骑数据[self.选中召唤兽].装备.染色组[1],self.数据.坐骑数据[self.选中召唤兽].装备.染色组[2],self.数据.坐骑数据[self.选中召唤兽].装备.染色组[3])
					end
			end

			self.坐骑饰品:置物品(self.数据.坐骑数据[self.选中召唤兽].装备)
		end
	end
end

function 场景类_道具行囊:显示(dt,x,y)
	self.焦点 = false
	if (self.窗口 == "召唤兽" or self.窗口 == "子女") and self.资源组[4] ~= nil then
		self.资源组[4]:更新(dt)
	end
	self.资源组[5]:更新(x,y)
	self.资源组[8]:更新(x,y,self.窗口 == "主人公")
	self.资源组[9]:更新(x,y,self.加入 > 0)
	self.资源组[10]:更新(x,y,(self.窗口 == "召唤兽" and self.加入 <  #self.数据.宝宝列表 - 4) or (self.窗口 == "子女" and self.加入 <  #self.数据.子女列表 - 4) or (self.窗口 == "坐骑" and self.加入 <  #self.数据.坐骑数据 - 4))
	self.资源组[11]:更新(x,y,self.更新~="包裹")
	self.资源组[12]:更新(x,y,self.更新~="行囊" )
    self.资源组[13]:更新(x,y )
    self.资源组[44]:更新(x,y,self.更新~="锦衣" )
    self.资源组[41]:更新(x,y )
    self.资源组[45]:更新(x,y )
    self.资源组[48]:更新(x,y )

	self.资源组[50]:更新(x,y )
	local 字体 = tp.字体表.普通字体
	if self.鼠标 then
		if self.资源组[5]:事件判断() then
			self:打开()
			return false
		elseif self.资源组[8]:事件判断() then
			if self.当前银行 == "现金" then
				self.当前银行 = "储备"
			elseif self.当前银行 == "储备" then
				self.当前银行 = "仙玉"
			elseif self.当前银行 == "仙玉" then
				self.当前银行 = "现金"
			end
		elseif self.资源组[9]:事件判断() then
			self.加入 = self.加入 - 1
		elseif self.资源组[10]:事件判断() then
			self.加入 = self.加入 + 1
        elseif self.资源组[41]:事件判断() then
			客户端:发送数据(1, 198, 13,"包裹")
        elseif self.资源组[45]:事件判断() then
		客户端:发送数据(1, 87, 13,"包裹")
        elseif self.资源组[48]:事件判断() then
         客户端:发送数据(1, 198, 13,"包裹")
		end

        if tp.抓取物品 == nil then
			if self.资源组[11]:事件判断() then --道具
			客户端:发送数据(1, 1, 13, "9", 1)
			elseif self.资源组[12]:事件判断() then--行囊
			客户端:发送数据(2, 1, 13, "9", 1)
			elseif self.资源组[13]:事件判断() then
			    客户端:发送数据(3, 1, 13)
              
			elseif self.资源组[44]:事件判断() then
			客户端:发送数据(4, 1, 13, "9")
			end
		 elseif tp.抓取物品  then
		    if self.资源组[11]:事件判断() then --道具
				客户端:发送数据(tp.抓取物品ID, 14, 13, self.更新, "包裹")
				tp.抓取物品 = nil
				tp.抓取物品ID = nil
				tp.抓取物品注释 = nil

			elseif self.资源组[12]:事件判断()  then--行囊
				客户端:发送数据(tp.抓取物品ID, 14, 13, self.更新, "行囊")
				tp.抓取物品 = nil
				tp.抓取物品ID = nil
				tp.抓取物品注释 = nil
			elseif self.资源组[13]:事件判断()  then
				if tp.抓取物品.类型 == "法宝" then
					客户端:发送数据(tp.抓取物品ID, 14, 13, self.更新,"法宝")
				else
					tp.提示:写入("#Y/只有法宝才能放入法宝栏!")
							self.物品[tp.抓取物品ID].确定 = false
					self.物品[tp.抓取物品ID]:置物品(tp.抓取物品)
				end

					tp.抓取物品 = nil
					tp.抓取物品ID = nil
					tp.抓取物品注释 = nil
			elseif self.资源组[44]:事件判断() then
				if tp.抓取物品.类型 == "锦衣" then
					客户端:发送数据(tp.抓取物品ID, 14, 13, self.更新,"锦衣")

				else
					tp.提示:写入("#Y/只有锦衣才能放入锦衣栏!")
					self.物品[tp.抓取物品ID].确定 = false
					self.物品[tp.抓取物品ID]:置物品(tp.抓取物品)
				end
					tp.抓取物品 = nil
					tp.抓取物品ID = nil
					tp.抓取物品注释 = nil
			end
         end


	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[11]:显示(self.x + 18,self.y + 410,true,nil,nil,self.更新 == "包裹",2)
	self.资源组[12]:显示(self.x + 63,self.y + 410,true,nil,nil,self.更新 == "行囊",2)
    self.资源组[13]:显示(self.x + 108,self.y + 410,true,nil,nil)
    self.资源组[44]:显示(self.x + 153,self.y + 410,true,true,nil,self.更新 == "锦衣",2)
    self.资源组[45]:显示(self.x + 198,self.y + 410,true,true)
    self.资源组[41]:显示(self.x + 243,self.y + 410,true,true)
    self.资源组[48]:显示(self.x + 287,self.y + 410)
    self.资源组[50]:显示(self.x + 5,self.y + 5)
	if self.窗口 == "主人公" then
		self.资源组[2]:显示(self.x-1,self.y + 22)
		if self.资源组[4] then 
		self.资源组[4]:显示(self.x + 38,self.y + 21)
	   end
		self.资源组[8]:置文字(self.当前银行)
		self.资源组[8]:显示(self.x + 5,self.y + 147,nil,true)
		self.资源组[40]:更新(x,y)
		self.资源组[52]:更新(x,y)
		self.资源组[54]:更新(x,y)
		  self.资源组[46]:更新(x,y,#self.数据.追加技能+#self.数据.附加技能+#self.数据.变身技能>0 )
		 if self.更新=="包裹" then
            local sdafwq= 0
		 	for i=1,self.数据.MaxPages do
			 	self.资源组[13+i]:更新(x,y,self.数据.Pages~=i)
			 	sdafwq=(i-1)*50
				self.资源组[13+i]:显示(self.x+283,self.y+200+sdafwq,true,1,nil,self.数据.Pages==i,2)
				if self.资源组[13+i]:事件判断() then
					if tp.抓取物品  then
						if   tp.抓取物品注释 == "包裹"  then
						 客户端:发送数据(tp.抓取物品ID+(self.数据.Pages-1)*20, i, 69)
						tp.抓取物品 = nil
						tp.抓取物品ID = nil
						tp.抓取物品注释 = nil
					   end
					else 
						客户端:发送数据(i, 30, 13)
					end
				end
		 	end
		 	if self.数据.MaxPages < 4 then
		 		self.资源组[18]:更新(x,y)
				self.资源组[18]:显示(self.x+283,self.y+200+sdafwq+60)
				if self.资源组[18]:事件判断() then
		 		tp.窗口.文本栏:载入("#H/此次扩充包裹栏需要扣除"..5000*self.数据.MaxPages.."仙玉,是否确认扩充","扩充包裹",true)
		 		end 
		 	end
		 	

		end
		if self.资源组[40]:事件判断() then
          客户端:发送数据(2, 74, 13, "9", 1)
        elseif self.资源组[52]:事件判断() then
          客户端:发送数据(2, 246, 13, "9", 1)
        elseif self.资源组[54]:事件判断() then
        	 if tp.抓取物品 == nil  then 
         tp.拆分开关 = true
         tp.提示:写入("#y/您现在可以点击包裹内的物品进行拆分")
    	 else 
    	 	tp.提示:写入("#y/请取消现在所抓取的物品")
         end
        elseif self.资源组[46]:是否选中(x,y) then
		    local tzzs = "#y/"
		    if #self.数据.追加技能 >0 then
		      for i=1,#self.数据.追加技能 do
		       tzzs=tzzs.."追加技能:#g/"..self.数据.追加技能[i].." #y/"
		      end
		    end
		     if #self.数据.变身技能 >0 then
		      for i=1,#self.数据.变身技能 do
		       tzzs=tzzs.."变身套装:#g/"..self.数据.变身技能[i].." #y/"
		      end
		    end
		    if #self.数据.附加技能 >0 then
		      for i=1,#self.数据.附加技能 do
		       tzzs=tzzs.."附加技能:#g/"..self.数据.附加技能[i].." #y/"
		      end
		    end
		    if tzzs ~="#y/" then
		     tp.提示:自定义(x,y+40,tzzs)
		    end
		elseif self.资源组[38]:事件判断() then
			tp.窗口.换装:打开()
		elseif self.资源组[39]:事件判断() then
			tp.窗口.换装:打开()
		elseif self.资源组[47]:事件判断() then
			tp.窗口.换装:打开()
		end
		for s=1,2 do
			self.资源组[37+s]:更新(x,y)
			self.资源组[37+s]:显示(self.x + 170,floor(self.y + s * 25),true)
		end
		self.资源组[47]:更新(x,y)
		self.资源组[47]:显示(self.x + 170,floor(self.y + 3 * 25),true)
		self.资源组[40]:显示(self.x + 170,floor(self.y + 4 * 25),true)
		self.资源组[52]:显示(self.x + 170,floor(self.y + 5 * 25),true)
		self.资源组[54]:显示(self.x + 170,floor(self.y + 6 * 25),true)
       self.资源组[46]:显示(self.x + 170,self.y + 170)


		for i=1,6 do
			self.人物装备[i]:置坐标(self.x+self.装备坐标.x[i]+5,self.y+self.装备坐标.y[i]-1)
			self.人物装备[i]:显示(dt,x,y,self.鼠标)
			if self.人物装备[i].物品 ~= nil and self.人物装备[i].焦点 then
				tp.提示:道具行囊(x,y,self.人物装备[i].物品)
			end
			if self.人物装备[i].焦点 then
				self.焦点 = true
			end
			if not tp.消息栏焦点 then
			if tp.抓取物品 == nil and self.人物装备[i].物品 ~= nil and self.人物装备[i].焦点   then
				if 引擎.鼠标弹起(1) then

                  客户端:发送数据(i+20,58,13,self.更新)
				end
			end
			-- 事件开始
			if self.人物装备[i].事件 then

				if tp.抓取物品 ~= nil and self.人物装备[i].物品 == nil and self.人物装备[i].焦点  then
					if self:可装备(tp.抓取物品,i) then
						if tp.抓取物品注释 == "道具行囊_人物装备" then
							客户端:发送数据(1, 1, 13, "9", 1)
							self.人物装备[i].确定 = false
						 tp.抓取物品 = nil
						tp.抓取物品ID = nil
						tp.抓物品注释 = nil
						else

                        客户端:发送数据(tp.抓取物品ID+(self.数据.Pages-1)*20,2,13,self.更新)
					 	self.人物装备[i].确定 = false
						self.物品[tp.抓取物品ID].确定 = false
						tp.抓取物品 = nil
						tp.抓取物品ID = nil
						tp.抓取物品注释 = nil
					end
					end
				elseif self.人物装备[i].焦点 and tp.抓取物品 == nil and self.人物装备[i].物品 ~= nil then

					tp.抓取物品 = self.人物装备[i].物品
					tp.抓取物品ID = i+20
					tp.抓取物品注释 = self.人物装备[i].注释
				
					self.人物装备[i].确定 = true
					self.人物装备[i]:置物品(nil)

				elseif  tp.抓取物品 ~= nil and self.人物装备[i].物品 ~= nil and self.人物装备[i].焦点 then
					if self:可装备(tp.抓取物品,i) then
                        客户端:发送数据(self.物品[tp.抓取物品ID].ID,30,55,self.更新)
						self.物品[tp.抓取物品ID].确定 = false
						tp.抓取物品 = nil
						tp.抓取物品ID = nil
						tp.抓取物品注释 = nil
					end
				end
			end
			-- 事件结束
		end
	end
		local jq
		if self.当前银行 == "现金" then
			jq = self.数据.银两
		elseif self.当前银行 == "储备" then
			jq = self.数据.储备
		elseif self.当前银行 == "仙玉" then
			jq = self.数据.仙玉
		end
	   	字体:置颜色(qjq(jq))
	   	字体:显示(self.x + 65,self.y + 153,jq)
	   	字体:置颜色(qjq(self.数据.存银))
	    字体:显示(self.x + 65,self.y + 174,self.数据.存银)

	elseif self.窗口 == "召唤兽" or self.窗口 == "子女" then
		if self.窗口 == "召唤兽" then
			self.资源组[3]:显示(self.x,self.y+22)
		elseif self.窗口 == "子女" then
			self.资源组[35]:显示(self.x,self.y+22)
			self.资源组[36]:更新(x,y,self.数据.子女列表[self.选中召唤兽] ~= nil)
			self.资源组[37]:更新(x,y,self.数据.子女列表[self.选中召唤兽] ~= nil)
			self.资源组[36]:显示(self.x+180,self.y+145)
			self.资源组[37]:显示(self.x+240,self.y+145)
			if self.资源组[36]:事件判断() then
				tp.窗口.召唤兽查看栏:打开(self.数据.子女列表[self.选中召唤兽],self.选中召唤兽)
			elseif 	self.资源组[37]:事件判断() then
				 tp.窗口.文本栏:载入(string.format("#H/你确定要抛弃%s吗？#R/注意抛弃以后再也无法找回！",self.数据.子女列表[self.选中召唤兽].名称),"孩子抛弃",true,nil,self.选中召唤兽)
			end
		end
	    --do
		self.资源组[9]:显示(self.x + 289,self.y + 23,true)
		self.资源组[10]:显示(self.x + 285,self.y + 115,true)
		local 列表 = 0
		字体:置颜色(-16777216)
		local dxs = nil
		if self.窗口 == "召唤兽" then
			dxs = self.数据.宝宝列表
		else
			dxs = self.数据.子女列表
		end
		for m=1,4 do
			if dxs[m + self.加入] ~= nil then
				bw:置坐标(self.x + 182,self.y + 24 + m * 23)
				if self.选中召唤兽 ~= m + self.加入 then
					if bw:检查点(x,y) then
						box(self.x + 179,self.y + 24 + m * 22,self.x + 283,self.y + 24 + m * 22 + 22,-3551379)
						if 引擎.鼠标弹起(0) and self.鼠标 then
							self.选中召唤兽 = m + self.加入
							self:置形象()
						end
						self.焦点 = true
					end
				else
					local ys = -10790181
					if bw:检查点(x,y) then
						ys = -9670988
						self.焦点 = true
					end
					box(self.x + 179,self.y + 24 + m * 22,self.x + 283,self.y + 24 + m * 22 + 22,ys)
				end
				
					if dxs.参战 ==  m + self.加入 then
						字体:置颜色(0xFFF8E04D):显示(self.x + 182,self.y + 27 + m * 23,dxs[m + self.加入].名称.."(参战)")
						字体:置颜色(0xFF000000)
					else 
						字体:显示(self.x + 182,self.y + 27 + m * 23,dxs[m + self.加入].名称)
					end
					
			
				
			end
		end
		if self.窗口 == "召唤兽" and dxs[self.选中召唤兽] ~= nil then
			字体:显示(self.x + 209,self.y + 152 ,format("%s/%s",self.数据.宝宝列表[self.选中召唤兽].当前气血,self.数据.宝宝列表[self.选中召唤兽].气血上限))
			字体:显示(self.x + 209,self.y + 172 ,format("%s/%s",self.数据.宝宝列表[self.选中召唤兽].当前魔法,self.数据.宝宝列表[self.选中召唤兽].魔法上限))
		elseif self.窗口 == "子女" and dxs[self.选中召唤兽] ~= nil then
				tp.经验背景_:置宽高1(131,19)
				tp.经验背景_:显示(self.x+173,self.y+170)
				tp.经验背景_:置宽高1(186,19)
				self.资源组[53]:置区域(0,0,math.min(floor(dxs[self.选中召唤兽].当前经验 / dxs[self.选中召唤兽].升级经验 * 121),121),self.资源组[53].高度)
				self.资源组[53]:显示(self.x+178,self.y+173)
                tp.字体表.描边字体_:显示(self.x+230,self.y+174,math.floor(dxs[self.选中召唤兽].当前经验 / dxs[self.选中召唤兽].升级经验*100).."%")	
		end
		if self.资源组[4] ~= nil then
			tp.影子:显示(self.x + 98,self.y + 115)
			self.资源组[4]:显示(self.x + 98,self.y + 115)
			if self.资源组[98] then
				self.资源组[98]:显示(self.x + 98,self.y + 115)
				self.资源组[98]:更新(dt)
			end
		end
		for i=1,3 do
			self.召唤兽装备[i]:置坐标(self.x+self.装备坐标.xx[i]+3,self.y+self.装备坐标.yy[i]-2)
			self.召唤兽装备[i]:显示(dt,x,y,self.鼠标)
			if not tp.消息栏焦点 then
				if self.召唤兽装备[i].物品 ~= nil and self.召唤兽装备[i].焦点 then
					tp.提示:道具行囊(x,y,self.召唤兽装备[i].物品)
				end
				if self.数据.宝宝列表[self.选中召唤兽] ~= nil then
					if tp.抓取物品 == nil and self.召唤兽装备[i].物品 ~= nil and self.召唤兽装备[i].焦点  then
						if 引擎.鼠标弹起(1) then
							客户端:发送数据(i,27,13,self.更新,self.选中召唤兽)
						end
					end
					if self.召唤兽装备[i].事件 then
						if tp.抓取物品 ~= nil  and self:召唤兽可装备(tp.抓取物品,i) then
							if tp.抓取物品注释=="道具行囊_召唤兽装备" then
									self.召唤兽装备[i]:置物品(tp.抓取物品)
							 else

							   	客户端:发送数据(tp.抓取物品ID,26,13,self.更新,self.选中召唤兽)
							end

							self.召唤兽装备[i].确定 = false
							self.物品[tp.抓取物品ID].确定 = false
							tp.抓取物品 = nil
							tp.抓取物品ID = nil
							tp.抓取物品注释 = nil

						elseif tp.抓取物品 == nil and self.召唤兽装备[i].物品 ~= nil  then
							tp.抓取物品 = self.召唤兽装备[i].物品
							tp.抓取物品ID = i
						
							tp.抓取物品注释 = self.召唤兽装备[i].注释
							self.召唤兽装备[i].确定 = true
							self.召唤兽装备[i]:置物品(nil)
						end
					end
				elseif self.数据.子女列表[self.选中召唤兽] ~= nil then
					if tp.抓取物品 == nil and self.召唤兽装备[i].物品 ~= nil and self.召唤兽装备[i].焦点  then
						if 引擎.鼠标弹起(1) then
							客户端:发送数据(i,self.选中召唤兽,68,"脱")
						end
					end
					
				end
				
				if self.召唤兽装备[i].焦点 then
					self.焦点 = true
				end
			end
		end
	elseif self.窗口 == "坐骑" then
		self.资源组[34]:显示(self.x,self.y + 25)
		self.资源组[42]:更新(x,y,self.选中召唤兽 ~= 0)
		self.资源组[43]:更新(x,y,self.选中召唤兽 ~= 0)
		self.资源组[9]:显示(self.x + 289,self.y + 23,true)
		self.资源组[10]:显示(self.x + 285,self.y + 115,true)
		if self.选中召唤兽 ~= 0 and self.数据.坐骑数据[self.选中召唤兽] ~= nil  then
            if self.数据.坐骑.编号 ==self.选中召唤兽 then
               self.资源组[42]:置文字("下骑")
            else
            	self.资源组[42]:置文字("骑乘")
            end
		end
		if self.资源组[42]:事件判断() then
			if self.选中召唤兽==0 then
			tp.提示:写入("#y/请先选择一只坐骑")
			else
			客户端:发送数据(self.选中召唤兽,6,48,"9",1)
			end
		elseif self.资源组[43]:事件判断() then
			   客户端:发送数据(2,6,47,"9",1)
		end
		self.资源组[42]:显示(self.x+240,self.y+131)
		self.资源组[43]:显示(self.x+240,self.y+161)

		for m=1,4 do
			if self.数据.坐骑数据[m + self.加入] ~= nil then
				bw1:置坐标(self.x + 184,self.y + 37 + m * 18)
				if self.选中召唤兽 ~= m + self.加入 then
					if bw1:检查点(x,y) then
						box(self.x + 184,self.y + 37 + m * 18,self.x + 285,self.y + 37 + m * 18 + 17,-3551379)
						if 引擎.鼠标弹起(0) and self.鼠标 then
							self.选中召唤兽 = m + self.加入
							self:置形象()
						end
						self.焦点 = true
					end
				else
					local ys = -10790181
					if bw1:检查点(x,y) then
						ys = -9670988
						self.焦点 = true
					end
					box(self.x + 184,self.y + 37 + m * 18,self.x + 285,self.y + 37 + m * 18 + 17,ys)
				end
				if self.数据.坐骑 ~= nil and self.数据.坐骑.编号 == self.数据.坐骑数据[m + self.加入].编号 then
					字体:置颜色(-256)
				else
					字体:置颜色(-16777216)
				end
				字体:显示(self.x + 189,self.y + 38 + m * 18,self.数据.坐骑数据[m + self.加入].名称)
			end
		end

			self.坐骑饰品:置坐标(self.x+185,self.y+130)
			self.坐骑饰品:显示(dt,x,y,self.鼠标)
			if not tp.消息栏焦点 then
				if self.坐骑饰品.物品 ~= nil and self.坐骑饰品.焦点 then
					tp.提示:道具行囊(x,y,self.坐骑饰品.物品)
				end
				if self.数据.坐骑数据[self.选中召唤兽] ~= nil then
					if tp.抓取物品 == nil and self.坐骑饰品.物品 ~= nil and self.坐骑饰品.焦点  then
						if 引擎.鼠标弹起(1) then
							客户端:发送数据(1,229,13,self.更新,self.选中召唤兽)
						end
					end
					if self.坐骑饰品.事件 then
						if tp.抓取物品 ~= nil  and tp.抓取物品.类型=="坐骑道具" and tp.抓取物品.名称~= "摄灵珠" then
							if tp.抓取物品注释=="道具行囊_坐骑饰品" then
									self.坐骑饰品:置物品(tp.抓取物品)
							 else

							   	-- 客户端:发送数据(tp.抓取物品ID,228,13,self.更新,self.选中召唤兽)

							   	客户端:发送数据(tp.抓取物品ID, (self.窗口 == "召唤兽" or self.窗口 == "坐骑" ) and self.选中召唤兽 or 0,16)
							end

							self.坐骑饰品.确定 = false
							self.物品[tp.抓取物品ID].确定 = false
							tp.抓取物品 = nil
							tp.抓取物品ID = nil
							tp.抓取物品注释 = nil

						end
					end
				end
				if self.坐骑饰品.焦点 then
					self.焦点 = true
				end
			end

		if self.资源组[4] ~= nil then
			tp.影子:显示(self.x + 112,self.y + 158)
			self.资源组[4]:更新(dt)
			self.资源组[4]:显示(self.x + 112,self.y + 158)
			if self.资源组[99] ~= nil then
				self.资源组[99]:更新(dt)
				self.资源组[99]:显示(self.x + 112,self.y + 158)
			end
		end
	elseif self.窗口 == "锦衣" then
		self.资源组[51]:显示(self.x,self.y+24)
  		tp.竖排花纹背景_:置区域(0,0,18,150)
  		tp.竖排花纹背景_:显示(self.x+165,self.y+20)
  		self.资源组[49]:显示(self.x+80,self.y+165)
  		self.资源组[49]:更新(x,y)
  		if self.资源组[4] ~= nil then
  			if self.资源组[100] ~= nil then
				self.资源组[100]:更新(dt)
				self.资源组[100]:显示(self.x + 92,self.y + 138)
			end
			 if self.资源组[101] ~= nil then
				self.资源组[101]:更新(dt)
				self.资源组[101]:显示(self.x + 52,self.y + 138)
			end
			tp.影子:显示(self.x + 92,self.y + 138)

			self.资源组[4]:更新(dt)
			self.资源组[4]:显示(self.x + 92,self.y + 138)
			if self.资源组[99] ~= nil then
				self.资源组[99]:更新(dt)
				self.资源组[99]:显示(self.x + 92,self.y + 138)
			end

			if self.资源组[49]:事件判断() then
				self.方向 = self.方向 - 1
				if self.方向 < 0 then
					self.方向 = 3
				end
			   self.资源组[4]:置方向(self.方向)
			   if self.资源组[99] then
			       self.资源组[99]:置方向(self.方向)
			   end
			    if self.资源组[101] then
			       self.资源组[101]:置方向(self.方向)
			   end
			end
		end

		local xx = 0
	    local yy = 1
		for i=1,6 do
			xx = xx + 1
			self.锦衣[i]:置坐标(self.x + xx * 58+132,self.y + yy * 57-29)
			self.锦衣[i]:显示(dt,x,y,self.鼠标,nil,0.7)
			if self.锦衣[i].物品 ~= nil and self.锦衣[i].焦点 then
				tp.提示:道具行囊(x,y,self.锦衣[i].物品,true)
			end
			if xx >= 2 then
				xx = 0
				yy = yy + 1
			end
		    if self.锦衣[i].事件 then
					if tp.抓取物品 ~= nil and self.锦衣[i].物品 == nil and self.锦衣[i].焦点  then
						if self:可装备锦衣(tp.抓取物品,i) then
							if tp.抓取物品注释 == "道具行囊_锦衣" then
							客户端:发送数据(4, 1, 13, "9", 1)
							self.锦衣[i].确定 = false
							 tp.抓取物品 = nil
							tp.抓取物品ID = nil
							tp.抓物品注释 = nil
							else

	                        客户端:发送数据(tp.抓取物品ID,184,13,self.更新)
						 	self.锦衣[i].确定 = false
							self.物品[tp.抓取物品ID].确定 = false
							tp.抓取物品 = nil
							tp.抓取物品ID = nil
							tp.抓取物品注释 = nil
						end
						end
					elseif self.锦衣[i].焦点 and tp.抓取物品 == nil and self.锦衣[i].物品 ~= nil then
						tp.抓取物品 = self.锦衣[i].物品
						tp.抓取物品ID = i+39
						tp.抓取物品注释 = self.锦衣[i].注释
						self.锦衣[i].确定 = true
						self.锦衣[i]:置物品(nil)
					elseif  tp.抓取物品 ~= nil and self.锦衣[i].物品 ~= nil and self.锦衣[i].焦点 then
						if self:可装备锦衣(tp.抓取物品,i) then
	                        客户端:发送数据(tp.抓取物品ID,184,13,self.更新)
							self.物品[tp.抓取物品ID].确定 = false
							tp.抓取物品 = nil
							tp.抓取物品ID = nil
							tp.抓取物品注释 = nil
						end
					end
			elseif self.锦衣[i].右键 then
	        	客户端:发送数据(i+39,184,13,self.更新)
			end
	    end
	end
	if  self.窗口 ~= "锦衣"  then
		self.资源组[6]:更新(x,y,self.窗口 ~= "主人公")
		self.资源组[7]:更新(x,y,self.窗口 ~= "召唤兽")
		self.资源组[30]:更新(x,y,self.窗口 ~= "坐骑")
		self.资源组[31]:更新(x,y,self.窗口 ~= "子女")
		self.资源组[6]:显示(self.x+2,self.y + 28,true,nil,nil,self.窗口 == "主人公",2)
		self.资源组[7]:显示(self.x+2,self.y + 57,true,nil,nil,self.窗口 == "召唤兽",2)
		self.资源组[30]:显示(self.x+2,self.y + 84,true,nil,nil,self.窗口 == "坐骑",2)
		self.资源组[31]:显示(self.x+2,self.y + 111,true,nil,nil,self.窗口 == "子女",2)
	end

	if self.鼠标 then
		if self.资源组[6]:事件判断() then
			self.资源组[4] = tp.资源:载入(ModelData[tp.场景.人物.数据.造型].头像资源,"网易WDF动画",ModelData[tp.场景.人物.数据.造型].头像组.道具)
			for s=1,3 do
				self.召唤兽装备[s]:置物品(nil)
			end
			self.坐骑饰品:置物品(nil)
			for n=1,6 do
				self.人物装备[n]:置物品(self.数据[n+20])
			end
			self.当前银行 = "现金"
			self.窗口 = "主人公"
			self.选中召唤兽 = 0
			self.加入 = 0
		elseif self.资源组[7]:事件判断() then
			for s=1,3 do
				self.召唤兽装备[s]:置物品(nil)
			end
			self.坐骑饰品:置物品(nil)
			客户端:发送数据(19,19,19)
			self.当前银行 = "现金"
			self.窗口 = "召唤兽"
			self.选中召唤兽 = 0
			self.加入 = 0
		elseif self.资源组[30]:事件判断() then
			self.选中召唤兽=0
			客户端:发送数据(28,28,5)
			self.窗口 = "坐骑"
		elseif self.资源组[31]:事件判断() then
			for s=1,3 do
				self.召唤兽装备[s]:置物品(nil)
			end
			self.坐骑饰品:置物品(nil)
			self.选中召唤兽 = 0
			self.加入 = 0
			客户端:发送数据(29,29,5)
			self.窗口 = "子女"
		end
	end

	self.资源组[5]:显示(self.x + 291,self.y + 6,true)
	local o = 0
	for h=1,4 do
		for l=1,5 do
			o = o + 1
			self.物品[o]:置坐标(l * 51 - 26 + self.x+4,h * 51 + 146 + self.y)
			self.物品[o]:显示(dt,x,y,self.鼠标)
			if self.物品[o].焦点 and not tp.消息栏焦点 then
				if self.物品[o].物品 ~= nil then
					tp.提示:道具行囊(x,y,self.物品[o].物品)
				end
				self.焦点 = true
			end

			if tp.抓取物品 == nil and self.物品[o].焦点 and self.物品[o].物品 ~= nil  then
				if 引擎.按键按住(0x10) and 引擎.鼠标弹起(0) then
					if #tp.窗口.聊天框类.道具召唤兽 > 1 then
						tp.提示:写入("#Y/只能发送1个道具或者召唤兽")
					elseif self.更新 ~="包裹" then
						tp.提示:写入("#Y/只能在发送包裹中的道具")
					elseif  tp.窗口.聊天框类.序列==3  or tp.窗口.聊天框类.序列==2  then

						tp.窗口.聊天框类:添加数据(o,self.物品[o].物品.名称,1) 

						-- local lssj = {名称=self.物品[o].物品.名称,编号=o,内容=self.更新,类型="道具"}
						-- table.insert(tp.窗口.聊天框类.道具召唤兽,lssj)
						-- tp.窗口.聊天框类.输入:添加文本("["..self.物品[o].物品.名称.."]")
					else
						tp.提示:写入("#Y/只能在世界频道或者队伍频道发送")
					end
					return
				elseif  引擎.按键按住(0x10) and 引擎.鼠标弹起(1) and (self.物品[o].物品.类型 == "鞋子" or self.物品[o].物品.类型 == "腰带"  or self.物品[o].物品.类型 == "项链"  or self.物品[o].物品.类型 =="头盔"  or self.物品[o].物品.类型 == "衣服" or self.物品[o].物品.类型 == "武器")then
                         tp.窗口.符石:打开(self.物品[o].物品,o)
                         return 
				end
			end
			if tp.抓取物品 == nil and self.物品[o].焦点 and self.物品[o].物品 ~= nil and not tp.消息栏焦点 then
				if self.物品[o].右键 then
					if self.窗口 == "召唤兽" and self.选中召唤兽 ~= 0 and  self.物品[o].物品.类型=="召唤兽装备" then

                             客户端:发送数据(o,26,13,self.更新,self.选中召唤兽)
                    elseif self.窗口 == "子女"  and self.选中召唤兽 ~= 0  and (self.物品[o].物品.类型 == "儿童用品" or self.物品[o].物品.类型 =="炼妖" or self.物品[o].物品.类型=="召唤兽装备")  then 
                    	 if self.物品[o].物品.类型 == "儿童用品" or self.物品[o].物品.类型 =="炼妖"then 
                    	 	客户端:发送数据(o,self.选中召唤兽,67)
                    	 elseif  self.物品[o].物品.类型=="召唤兽装备" then
								客户端:发送数据(o,self.选中召唤兽,68,"穿")
                    	 end
     
				    elseif  self.物品[o].物品.类型 == "坐骑道具" and  self.窗口 ~= "坐骑"then
		                          	tp.提示:写入("#Y/请点击坐骑栏给坐骑使用！")
		                          	 return 	 
					elseif self.窗口 == "锦衣"   and self.物品[o].物品.类型 == "锦衣" then
	
	                          客户端:发送数据(o,184,13,self.更新)
	                else  
	                	
							if self.更新=="包裹" then
								if self.物品[o].物品.名称 =="飞行符" then          
			                          tp.窗口.飞行符:打开(self.更新,(self.数据.Pages-1)*20+o)
			                    elseif self.窗口 == "主人公" and (self.物品[o].物品.类型 == "鞋子" or self.物品[o].物品.类型 == "腰带"  or self.物品[o].物品.类型 == "项链"  or self.物品[o].物品.类型 =="头盔"  or self.物品[o].物品.类型 == "衣服" or self.物品[o].物品.类型 == "武器")then
	                                 客户端:发送数据((self.数据.Pages-1)*20+o,2,13,self.更新) 
								 elseif self.窗口 == "主人公" and (self.物品[o].物品.类型 == "手镯" or self.物品[o].物品.类型 == "定制锦衣" or self.物品[o].物品.类型 == "耳饰" or self.物品[o].物品.类型 == "佩饰" or self.物品[o].物品.类型 == "戒指" ) then
		                          客户端:发送数据((self.数据.Pages-1)*20+o,75,13,self.更新)

		                         elseif  self.物品[o].物品.类型 =="传音" then
		                         	 tp.窗口.组合输入框:打开("传音","请输入你要发送传音的内容",(self.数据.Pages-1)*20+o)
			                    elseif  self.物品[o].物品.名称 =="武器幻色丹" then 
										if tp.场景.人物.数据.武器数据.名称 == "" and  tp.场景.人物.数据.武器数据.类别 == "" then
											tp.提示:写入("#Y/请佩戴武器后在使用。")
											return 
										end
			                           tp.窗口.武器染色:打开(self.更新,(self.数据.Pages-1)*20+o)
			                    elseif self.物品[o].物品.类型 == "合成旗" and #self.物品[o].物品.坐标>=7  then 
			                    	 tp.窗口.合成旗类:打开(self.物品[o].物品,(self.数据.Pages-1)*20+o)
			                 elseif self.物品[o].物品.类型 == "超级合成旗" and #self.物品[o].物品.坐标>=21  then 
			                    	 tp.窗口.合成旗类:打开(self.物品[o].物品,(self.数据.Pages-1)*20+o)
			                    elseif self.物品[o].物品.名称 =="陨铁" then 
		    						 tp.窗口.幻化界面:打开(self.物品,o,(self.数据.Pages-1)*20)
			                    elseif self.物品[o].物品.类型 == "窗口道具"  then
		                             tp.窗口.道具小窗口:打开(self.物品,o,(self.数据.Pages-1)*20)
			                    else 
			
			                    	
			                    	
			                    	客户端:发送数据((self.数据.Pages-1)*20+o, (self.窗口 == "召唤兽" or self.窗口 == "坐骑" ) and self.选中召唤兽 or 0,16)
								end	 
							elseif self.更新=="行囊" then
								tp.提示:写入("#Y/行囊内无法使用道具。")
							end
					end
				end
			end

			-- 点击物品逻辑
			if self.物品[o].事件 then
				if tp.拆分开关 and self.更新=="包裹" and self.物品[o].物品 ~= nil  then
					if self.物品[o].物品.数量 then
					tp.拆分开关=false
           		 tp.鼠标:还原鼠标()
					   tp.窗口.组合输入框:打开("拆分","当前物品的数量为:"..self.物品[o].物品.数量,(self.数据.Pages-1)*20+o)
					else 
						tp.提示:写入("#y/只能拆分叠加的物品")
					end
					return
			
				elseif tp.抓取物品 == nil and self.物品[o].物品 ~= nil  then
					tp.抓取物品 = self.物品[o].物品
					tp.抓取物品ID = o
					tp.抓取物品注释 = self.物品[o].注释
					self.物品[tp.抓取物品ID].确定 = true
					self.物品[o]:置物品(nil)
				elseif tp.抓取物品 ~= nil  and tp.抓取物品ID <21   then
                    客户端:发送数据((self.数据.Pages-1)*20+tp.抓取物品ID,(self.数据.Pages-1)*20+o, 15, self.更新)
					if tp.抓取物品注释 == self.数据.类型 then 
						self.物品[tp.抓取物品ID].确定 = false
					 end
					tp.抓取物品 = nil
					tp.抓取物品ID = nil
					tp.抓取物品注释 = nil
				elseif tp.抓取物品 ~= nil and self.物品[o].物品 == nil and tp.抓取物品注释 == "道具行囊_人物装备"  then
					self.人物装备[tp.抓取物品ID-20]:置物品(nil)
                     客户端:发送数据(tp.抓取物品ID,58,13,self.更新,(self.数据.Pages-1)*20+o)
					self.人物装备[tp.抓取物品ID-20].确定 = false
					tp.抓取物品 = nil
					tp.抓取物品ID = nil
					tp.抓取物品注释 = nil

				elseif tp.抓取物品 ~= nil and self.物品[o].物品 == nil and tp.抓取物品注释 == "道具行囊_锦衣"  then
					self.锦衣[tp.抓取物品ID-39]:置物品(nil)
                     客户端:发送数据(tp.抓取物品ID,184,13,self.更新,o)
					self.锦衣[tp.抓取物品ID-39].确定 = false
					tp.抓取物品 = nil
					tp.抓取物品ID = nil
					tp.抓取物品注释 = nil
				elseif tp.抓取物品 ~= nil and self.物品[o].物品 == nil and tp.抓取物品注释 == "道具行囊_召唤兽装备" then

                   客户端:发送数据(tp.抓取物品ID,27,13,self.更新,self.选中召唤兽)

					self.召唤兽装备[tp.抓取物品ID].确定 = false
					self.召唤兽装备[tp.抓取物品ID]:置物品(nil)
					self.物品[o].确定 = true
					tp.抓取物品 = nil
					tp.抓取物品ID = nil
					tp.抓取物品注释 = nil
				elseif tp.抓取物品 ~= nil and self.物品[o].物品 ~= nil and (tp.抓取物品注释 == "道具行囊_人物装备"  or  tp.抓取物品注释 == "道具行囊_召唤兽装备"  or  tp.抓取物品注释 == "道具行囊_坐骑饰品"or tp.抓取物品注释 == "道具行囊_锦衣" ) then
					local jy = self.物品[o].物品
					local jy1 = tp.抓取物品
					local jy2 = tp.抓取物品ID-20
					if tp.抓取物品注释 == "道具行囊_人物装备" then
						if self:可装备(jy,jy2) then
                          客户端:发送数据(o,2,13,self.更新)
							self.人物装备[jy2].确定 = false
							tp.抓取物品 = nil
							tp.抓取物品ID = nil
							tp.抓取物品注释 = nil

						end

					 elseif tp.抓取物品注释 == "道具行囊_锦衣" then
						if self:可装备(jy,jy2-19) then
                          客户端:发送数据(o,184,13,self.更新)
							self.锦衣[jy2].确定 = false
							tp.抓取物品 = nil
							tp.抓取物品ID = nil
							tp.抓取物品注释 = nil

						end
					elseif tp.抓取物品注释 == "道具行囊_召唤兽装备" then
						if self:召唤兽可装备(self.物品[o].物品,tp.抓取物品ID) then
							self.召唤兽装备[tp.抓取物品ID].确定 = false
							tp.抓取物品 = nil
							tp.抓取物品ID = nil
							tp.抓取物品注释 = nil
							self.召唤兽装备[jy2]:置物品(jy)
							self.物品[o]:置物品(jy1)
							self.数据.宝宝列表[self.选中召唤兽]:穿戴装备(self.召唤兽装备[jy2].物品,jy2)
						end
					end
                elseif tp.抓取物品 ~= nil and tp.抓取物品注释 == "灵饰"  then
                	if self.物品[o].物品 ~= nil then
							if tp.窗口.灵饰:可装备(self.物品[o].物品,tp.抓取物品ID)   then

						        客户端:发送数据(tp.抓取物品ID,75,13,self.更新,o)
							end
					 elseif self.物品[o].物品 ==nil then
							--客户端:发送数据(2, 74, 13, "9", 1)
							客户端:发送数据(tp.抓取物品ID,59,13,self.更新,(self.数据.Pages-1)*20+o)
							tp.窗口.灵饰.物品[tp.抓取物品ID].确定 = false
							tp.抓取物品 = nil
							tp.抓取物品ID = nil
							tp.抓取物品注释 = nil
					end
				end
			end
		end
	end
	字体:置颜色(-256)
	字体:显示(self.x + 20,self.y + 435,self.提示文字)
	if not self.鼠标 and not tp.窗口.灵饰.鼠标 and  not tp.窗口.法宝神器.鼠标 then
		if 引擎.鼠标弹起(0)  then
			if tp.抓取物品 ~= nil and not tp.窗口.文本栏.可视 and tp.抓取物品ID<= 20  then
				tp.窗口.文本栏:载入("#H/真的要#Y/摧毁#R/"..tp.抓取物品.名称.."#W/吗?","丢弃物品",true)
			end
		end
	end
end


function 场景类_道具行囊:召唤兽可装备(i1,i2)

	 --1=项圈 2=铠甲 3=项圈
	 local 格子 = 0
	if i1 ~= nil and   i1.类型 =="召唤兽装备"  then
	        if i1.种类 == "护腕" then
	            格子= 1
	        elseif i1.种类 == "铠甲" then
	        	 格子= 3
	        elseif i1.种类 == "项圈" then
	            格子= 2
	        end
	        if 格子 == i2 then
	            return true
	        end
	end
	return false
end


function 场景类_道具行囊:可装备(i1,i2)
if  self.窗口 == "主人公" then
	if i1.类型=="头盔" and i2==1 then
	 return true
    elseif i1.类型=="项链" and i2==2 then
     return true
    elseif i1.类型=="武器" and i2==3 then
    	return true
    elseif i1.类型=="衣服" and i2==4 then
    	return true
    elseif i1.类型=="腰带" and i2==5 then
    	return true
    elseif i1.类型=="鞋子" and i2==6 then
    	return true
    else
    	tp.提示:写入("#Y/该装备类型不符")
    	return false
   end
end
	return false
end


function 场景类_道具行囊:可装备锦衣(i1,i2)
if  self.窗口 == "锦衣" then
	if i1.部位=="头盔" and i2==1 then
	 return true
    elseif i1.部位=="项链" and i2==2 then
     return true
    elseif i1.部位=="衣服" and i2==3 then
    	return true
    elseif i1.部位=="脚印" and i2==4 then
    	return true
    elseif i1.部位=="武器" and i2==5 then
    	return true
     elseif i1.部位=="光环" and i2==6 then
    	return true
    else
    	tp.提示:写入("#Y/该锦衣类型不符")
    	return false
   end
end
	return false
end

return 场景类_道具行囊