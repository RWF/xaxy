--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:47
--======================================================================--
local 坐骑技能 = class()


local tp,zts,zts1
local insert = table.insert
local mouseb = 引擎.鼠标弹起

local box = 引擎.画矩形
function 坐骑技能:初始化(根)
  self.ID = 149
  self.x =480+(全局游戏宽度-800)/2
  self.y = 75
  self.xx = 0
  self.yy = 0
  self.注释 = "坐骑"
  self.可视 = false
  self.鼠标 = false
  self.焦点 = false
  self.可移动 = true
  local 资源 = 根.资源
  local 按钮 = 根._按钮
  local 自适应 = 根._自适应
  self.窗口时间 = 0
  tp = 根
  zts = tp.字体表.普通字体
  zts1 = tp.字体表.描边字体
	self.背景 =  资源:载入('JM.dll',"网易WDF动画",0x5A8AA272)
    self.关闭 = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true)
	self.关闭:绑定窗口_(149)

end
function 坐骑技能:打开(数据)
	if self.可视 then
		self.可视 = false



	else
                                if  self.x > 全局游戏宽度 then
self.x = 480+(全局游戏宽度-800)/2
    end
       self.数据=数据

		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end


function 坐骑技能:显示(dt,x, y)




	self.关闭:更新(x,y)

	self.焦点=false
	if self.关闭:事件判断() then
		self:打开()
	end



	self.背景:显示(self.x, self.y)
	self.关闭:显示(self.x+245, self.y+6)



end

function 坐骑技能:检查点(x,y)
  if self.背景:是否选中(x,y)  then
    return true
  end
end

function 坐骑技能:初始移动(x,y)
  tp.运行时间 = tp.运行时间 + 1
  self.窗口时间 = tp.运行时间
  if not self.焦点 then
    tp.移动窗口 = true
  end
  if self.鼠标 and  not self.焦点 then
    self.xx = x - self.x
    self.yy = y - self.y
  end
end

function 坐骑技能:开始移动(x,y)
  if self.鼠标 then
    self.x = x - self.xx
    self.y = y - self.yy
  end
end
return 坐骑技能
