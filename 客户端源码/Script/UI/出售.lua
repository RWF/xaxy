--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:46
--======================================================================--

local 场景类_出售 = class(窗口逻辑)

local tp,zts,zts1
local floor = math.floor
local tonumber = tonumber
local tostring = tostring
local insert = table.insert
local xs = {[0]="单 价",[1]="数 量",[2]="总 额",[3]="现 金"}
local jqr = 引擎.取金钱颜色

function 场景类_出售:初始化(根)
	self.ID = 21
	self.x = 280
	self.y = 86
	self.xx = 0
	self.yy = 0
	self.注释 = "出售"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 自适应.创建(0,1,272,419,3,9),
		[2] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),
		[3] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true," 出  售"),
		[4] = 按钮.创建(自适应.创建(12,4,72,22,1,3),0,0,4,true,true,"一键出售"),
		[5] = 按钮.创建(自适应.创建(22,4,27,20,4,3),0,0,4,true,true),
		[6] = 按钮.创建(自适应.创建(23,4,27,20,4,3),0,0,4,true,true),
		[7] = 自适应.创建(1,1,236,18,1,3,nil,18),
		[8] = 自适应.创建(3,1,90,19,1,3)
	}
	for n=2,6 do
	   self.资源组[n]:绑定窗口_(21)
	end
	self.物品 = {}
	local 格子 = 根._物品格子
	for i=1,20 do
		self.物品[i] = 格子(0,0,i,"出售")
	end
	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('出售总控件')
	总控件:置可视(true,true)
	self.输入框 = 总控件:创建输入("数量输入",self.x + 133,self.y + 310,100,14)
	self.输入框:置可视(false,false)
	self.输入框:置限制字数(3)
	self.输入框:置数字模式()
	self.输入框:置光标颜色(-16777216)
	self.输入框:置文字颜色(-16777216)
	self.窗口时间 = 0
    self.类型="包裹"
	self.收购价格 = 1
	self.回收分类 = nil
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.描边字体
end
function 场景类_出售:刷新(数据)
		for i=1,20 do
		self.物品[i]:置物品(数据[i])
		end
		self.数据=数据
		self.道具 = nil
		self.单价 = nil
		self.数量 = nil
		if self.上一次 ~= nil then
			self.物品[self.上一次].确定 = false
		end
		self.上一次 = nil
end
function 场景类_出售:打开(数据)
	if self.可视 then
		self.道具 = nil
		self.单价 = nil
		self.数量 = nil
		self.可视 = false
		if self.上一次 ~= nil then
			self.物品[self.上一次].确定 = false
		end
		self.上一次 = nil
		self.输入框:置可视(false,false)
	else
		if  self.x > 全局游戏宽度 then
		   			self.x = 280
		end

		for i=1,20 do
		self.物品[i]:置物品(数据[i])
		end
		self.数据=数据
		if self.上一次 ~= nil then
			self.物品[self.上一次].确定 = false
		end
		self.上一次 = nil
         self.类型="包裹"
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_出售:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y,self.道具 ~= nil and self.单价~=0 )
	self.资源组[4]:更新(x,y)
	self.资源组[5]:更新(x,y,self.类型 == "行囊" )
	self.资源组[6]:更新(x,y,self.类型 == "包裹")
	if self.鼠标 then
		if self.资源组[2]:事件判断() then
			self:打开()
		elseif self.资源组[3]:事件判断() then
          客户端:发送数据(self.上一次, 4, 13, self.类型,self.数量)
		elseif self.资源组[4]:事件判断() then
			客户端:发送数据(1, 13, 13, self.类型,1)
		elseif self.资源组[5]:事件判断() then
            	客户端:发送数据(1, 87, 13,"包裹")
              self.类型="包裹"
		elseif self.资源组[6]:事件判断() then
              客户端:发送数据(1, 87, 13,"行囊")
           self.类型="行囊"
		end
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[7]:显示(self.x+6,self.y+3)
	Picture.窗口标题背景_:置区域(0,0,70,16)
	Picture.窗口标题背景_:显示(self.x+97,self.y+3)
	zts1:显示(self.x+109,self.y+3,"出  售")
	Picture.物品界面背景_:显示(self.x+9,self.y+30)
	tp.横排花纹背景_:置区域(3,0,147,18)
	tp.横排花纹背景_:显示(self.x+116,self.y+242)
	zts:置颜色(4294967295)
	for i=0,3 do
		zts:显示(self.x+68,self.y+277+i*24,xs[i])
		self.资源组[8]:显示(self.x+108,self.y+273+i*24)
	end
	self.资源组[2]:显示(self.x+247,self.y+6)
	self.资源组[3]:显示(self.x+100,self.y+369,true)
	self.资源组[4]:显示(self.x+100,self.y+393,true)
	self.资源组[5]:显示(self.x+55,self.y+244)
	self.资源组[6]:显示(self.x+86,self.y+244)
	local xx = 0
	local yy = 0
	for i=1,20 do
		local jx = xx * 51 + 10
		local jy = yy * 51 + 30
		self.物品[i]:置坐标(self.x + jx,self.y + jy)
		self.物品[i]:显示(dt,x,y,self.鼠标)
		if self.物品[i].焦点 and self.物品[i].物品 ~= nil then
			tp.提示:道具行囊(x,y,self.物品[i].物品)
			if self.物品[i].事件 then
				if self.上一次 ~= nil and self.上一次 ~= 0 then
					self.物品[self.上一次].确定 = false
				end
				self.物品[i].确定 = true
				self.道具 = self.物品[i].物品
				if self.道具.数量 == nil then
				    self.道具.数量 = 1
				end
				self.数量 = 1
			    if self.物品[i].物品.类型=="武器" or self.物品[i].物品.类型=="衣服" or self.物品[i].物品.类型=="头盔" or self.物品[i].物品.类型=="项链" or self.物品[i].物品.类型=="腰带" or self.物品[i].物品.类型=="鞋子" or self.物品[i].物品.类型=="手镯" or self.物品[i].物品.类型=="耳饰" or self.物品[i].物品.类型=="戒指" or self.物品[i].物品.类型=="佩饰"  then
			     		local 临时差价= self.物品[i].物品.等级*40
			     		self.单价=0
   				 		if  self.物品[i].物品.等级 > 40 then
   				 			 临时差价= self.物品[i].物品.等级*1000
							if self.物品[i].物品.类型=="武器" then
							self.单价=( self.物品[i].物品.命中+ self.物品[i].物品.伤害)*5
							elseif self.物品[i].物品.类型=="衣服" then
							self.单价=( self.物品[i].物品.防御)*5
							elseif self.物品[i].物品.类型=="项链" then
							self.单价=( self.物品[i].物品.灵力)*30
							elseif self.物品[i].物品.类型=="腰带" then
							self.单价=( self.物品[i].物品.防御+ self.物品[i].物品.气血)*12
							elseif self.物品[i].物品.类型=="鞋子" then
							self.单价=( self.物品[i].物品.敏捷+ self.物品[i].物品.防御)*29
							elseif self.物品[i].物品.类型=="头盔" then
							self.单价=( self.物品[i].物品.魔法+ self.物品[i].物品.防御)*22
							elseif self.物品[i].物品.类型=="手镯" then
							self.单价=20000
							elseif self.物品[i].物品.类型=="耳饰" then
							self.单价=20000
							elseif self.物品[i].物品.类型=="戒指" then
							self.单价=20000
							elseif self.物品[i].物品.类型=="佩饰" then
							self.单价=20000
							end
						end
					        self.单价= self.单价 + 临时差价
							self.输入框:置文本(1)
							self.输入框:置可视(true,true)
							self.上一次 = i
			    else
			    	     local jg  = 0

                            jg = ItemData[self.物品[i].物品.名称].出售 or 0
							self.单价 = jg
							self.输入框:置文本(1)
							self.输入框:置可视(true,true)
							self.上一次 = i
							jg = nil
				end
			elseif  self.物品[i].右键 then
			  客户端:发送数据(i, 4, 13, self.类型,1)
			end
		end
		xx = xx + 1
		if xx == 5 then
			xx = 0
			yy = yy + 1
		end
	end
	zts:置颜色(-16777216)
	if self.道具 ~= nil then
		zts:显示(self.x + 115,self.y + 277,self.单价)
		local s = tonumber(self.输入框:取文本())
		if s == nil or s <= 0 or s == ""  then
		s = 1
	end
	if self.道具 ~= nil and s > tonumber(self.道具.数量) then
		if tonumber(self.道具.数量) == 0 then
			self.输入框:置文本(1)
		else
			self.输入框:置文本(tonumber(self.道具.数量))
		end
		end
		self.数量 = tonumber(self.输入框:取文本())
		if self.数量 == nil then
			self.数量 = 1
		end
		zts:显示(self.x + 115,self.y + 325,self.数量 * self.单价)
		self.输入框:置坐标(self.x-300,self.y-95)
		s = nil
	end
	zts:置颜色(jqr(self.数据.银两))
	zts:显示(self.x + 115,self.y + 349,self.数据.银两)
	self.控件类:更新(dt,x,y)
	self.控件类:显示(x,y)
	if self.输入框._已碰撞  then
		self.焦点 = true
	end
end



return 场景类_出售