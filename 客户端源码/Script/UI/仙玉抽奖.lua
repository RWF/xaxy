-- @Author: 作者QQ414628710
-- @Date:   2022-01-15 20:06:44
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-17 23:30:45
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:46
--======================================================================--
local 场景类_仙玉抽奖 = class(窗口逻辑)
local sd =0
local sd1 =0
local tp,zts,zts1
local floor = math.floor
local tonumber = tonumber
local tostring = tostring
local insert = table.insert
local xs = {[0]="单 价",[1]="数 量",[2]="总 额",[3]="现 金"}
local jqr = 引擎.取金钱颜色

function 场景类_仙玉抽奖:初始化(根)
	self.ID = 162
	self.x = 150+(全局游戏宽度-800)/2
	self.y = 100
	self.xx = 0
	self.yy = 0
	self.注释 = "仙玉抽奖"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 根.资源:载入('JM.dll',"网易WDF动画",0xE5B48C60),
		[2] = 按钮.创建(根.资源:载入('JM.dll',"网易WDF动画",0x50CE0A28),0,0,4,true,true),
		[3] = 按钮.创建(自适应.创建(12,4,55,22,1,3),0,0,4,true,true,"抽奖"),
		[4] = 按钮.创建(自适应.创建(12,4,55,22,1,3),0,0,4,true,true,"停止"),

	}
	for n=2,4 do
	   self.资源组[n]:绑定窗口_(162)
	end
	self.物品 = {}
	local 格子 = 根._物品格子
	for i=1,24 do
		self.物品[i] = 格子(0,0,i,"仙玉抽奖")
	end

	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体__
	zts1 = tp.字体表.描边字体
	self.抽奖开始  =false
	self.抽奖停止 = false
	self.结束抽奖=false
	self.春节=0
end

function 场景类_仙玉抽奖:打开(数据)
	if self.可视 then
		self.可视 = false
		self.抽奖开始  =false
		self.抽奖停止 = false
	else
		self.春节=0
		self.抽奖开始  =false
		self.抽奖停止 = false
		self.结束抽奖=false
		if  self.x > 全局游戏宽度 then
		   		self.x = 150+(全局游戏宽度-800)/2
		end
		for i=1,24 do
		self.物品[i]:置物品(数据[i])
		end
		self.数据=数据
		self.仙玉 =数据.仙玉 +0
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end
function 场景类_仙玉抽奖:停止(数据)
	  self.结束抽奖 = 数据.编号
	  self.抽奖物品 = 数据.名称
	  if 数据.抽奖 then
	  self.春节=1
	  end
end
function 场景类_仙玉抽奖:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x+450,self.y+6)
	if self.资源组[2]:事件判断() then
		self:打开()


	end
	if self.结束抽奖 and self.结束抽奖 == sd and self.抽奖物品 then
	     if sd >= 1 then
        	self.物品[sd].确定 =true
        	if sd == 1 then
        		 self.物品[24].确定 =false
        	 else
        	 	self.物品[sd-1].确定 =false
            end
        end
        self.结束抽奖 =false

        self.抽奖开始,self.抽奖停止=false,false
        tp.提示:写入("#Y/恭喜你获得了"..self.抽奖物品)
         客户端:发送数据(self.数据.组号,225,13,self.抽奖物品,self.春节)
        self.春节=0
        self.抽奖物品 =nil
        if  tp.窗口.道具行囊.可视 then
			local sdf
			if tp.窗口.道具行囊.更新 =="包裹" then
				sdf =1
			elseif tp.窗口.道具行囊.更新 =="法宝" then
				sdf =3
			elseif tp.窗口.道具行囊.更新 =="锦衣" then
				sdf =4
			else
				sdf =2
			end
			客户端:发送数据(sdf, 1, 13, "9", 1)
		end
	end
	if self.抽奖开始 or self.抽奖停止  then
		if self.抽奖开始 then
		  sd1 =sd1 +1
		 else
		 	 sd1 =sd1 +0.2
		 end
      if sd1 >5 then
          sd =sd+ 1
          sd1 =0
      end
		 if sd > 24 then
		 	sd = 1
		 end
        if sd >= 1 then
        	self.物品[sd].确定 =true
        	if sd == 1 then
        		 self.物品[24].确定 =false
        	 else
        	 	self.物品[sd-1].确定 =false
            end
        end
        if self.抽奖开始 then
			self.资源组[4]:更新(x,y)
			self.资源组[4]:显示(self.x+212,self.y+339,true)
			if self.资源组[4]:事件判断() then
				self.抽奖开始 = false
				self.抽奖停止 = true
				客户端:发送数据(self.数据.组号,224,13,"抽奖")
			end
		end
	else
		self.资源组[3]:更新(x,y)
		self.资源组[3]:显示(self.x+212,self.y+339,true)
		 if self.资源组[3]:事件判断() then
			if self.仙玉 < 200 and self.数据.抽奖==nil then
				tp.提示:写入("#Y/你的仙玉不足"..self.数据.扣除.."，无法进行抽奖")
			else
	           self.抽奖开始 = true
			end
		end
	end





	local xx = 0
	local yy = 0
	for i=1,24 do
		local jx = xx * 69 + 41
		local jy = yy * 73 + 40
		self.物品[i]:置坐标(self.x + jx,self.y + jy)
		self.物品[i]:显示(dt,x,y,self.鼠标,nil,nil,nil,nil,nil,true)
		zts:显示(self.x + jx+24-zts:取宽度(self.物品[i].物品.名称) / 2,
			self.y + jy+57,self.物品[i].物品.名称)
		if self.物品[i].焦点 and self.物品[i].物品 ~= nil then
			tp.提示:道具行囊(x,y,self.物品[i].物品)
		end
		xx = xx + 1
		if xx == 6 then
			xx = 0
			yy = yy + 1
		end
	end

end




return 场景类_仙玉抽奖