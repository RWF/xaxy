--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:46
--======================================================================--
local 场景类_兑换界面 = class(窗口逻辑)
local tp
local insert = table.insert
local zts,zts1
function 场景类_兑换界面:初始化(根)
	self.ID = 111
	self.x = 211+(全局游戏宽度-800)/2
	self.y = 154
	self.xx = 0
	self.yy = 0
	self.注释 = "兑换界面"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 自适应.创建(99,1,400,301,3,9),
		[2] = 按钮.创建(自适应.创建(103,4,75,22,1,3),0,0,4,true,true,"  兑 换"),
		[3] = 按钮.创建(自适应.创建(103,4,75,22,1,3),0,0,4,true,true,"  取 消"),
		[4] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),

		[6] = 自适应.创建(70,1,370,110,3,9),

	}
	self.介绍文本 = 根._丰富文本(360,275,根.字体表.普通字体)
	for n=2,4 do
	   self.资源组[n]:绑定窗口_(111)
	end
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.华康字体
	zts1 = tp.字体表.华康字体
end

function 场景类_兑换界面:打开(数据)
	if  self.可视 then
		self.介绍文本:清空()
		self.可视 = false
	else
								if  self.x > 全局游戏宽度 then
		   							self.x = 211+(全局游戏宽度-800)/2
		end

		insert(tp.窗口_,self)
		self.数据 = 数据
		self.状态=self.数据.状态
		self.介绍文本:清空()
		if self.状态=="乾元丹" then
		    self.介绍文本:添加文本("#h/≥69级,在本门派师傅处学习。人物等级≥69级可兑换1个乾元丹，人物等级≥89级可兑换2个乾元丹，人物等级≥109级可兑换3个乾元丹，人物等级≥129级可兑换4个乾元丹，人物等级≥155级可兑换5个乾元丹，人物等级≥159级可兑换6个乾元丹，人物等级≥164级可兑换7个乾元丹，人物等级≥168级可兑换8个乾元丹，人物等级≥171级可兑换9个乾元丹")
		 elseif self.状态=="潜能果" then
		    self.介绍文本:添加文本("#h/潜能果是一种把经验转化为潜力的果子。90级>人物等级≥60级，可食用50个；渡劫155级≥人物等级≥90级，可食用100个；渡劫170级≥人物等级≥渡劫≥155级，可食用150个；人物等级≥渡劫等级≥170级，可食用200个。")
		end
		tp.运行时间 = tp.运行时间 + 3
		self.窗口时间 = tp.运行时间
		self.可视 = true
	end
end

function 场景类_兑换界面:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y)
	self.资源组[4]:更新(x,y)
	if self.资源组[2]:事件判断() then
    if self.状态 =="乾元丹" then
    	客户端:发送数据(7,7,5,2)
     elseif self.状态 == "潜能果" then
     	客户端:发送数据(10,10,5,2)
    end

	elseif self.资源组[3]:事件判断() then
	   self:打开()
	elseif self.资源组[4]:事件判断() then
	 self:打开()
	end

	self.资源组[1]:显示(self.x,self.y)
	Picture.标题:显示(self.x+self.资源组[1].宽度/2-90,self.y)

	
	if self.状态 == "乾元丹" then
			zts1:置颜色(0xFFFFFFFF):显示(self.x+173,self.y+3,"乾元丹")
	elseif self.状态 == "潜能果" then
			zts1:置颜色(0xFFFFFFFF):显示(self.x+173,self.y+3,"潜能果")
	end
	
	self.资源组[6]:显示(self.x +15,self.y +30)
	self.介绍文本:显示(self.x+15,self.y+35)
	self.资源组[2]:显示(self.x +85,self.y +260,true,1)
	self.资源组[3]:显示(self.x + 285,self.y +260,true,1)
  	self.资源组[4]:显示(self.x +379,self.y +6)
	if self.状态 == "乾元丹" then
		zts:显示(self.x+20,self.y+160,"乾元丹：")
		zts:显示(self.x+210,self.y+160,"可换乾元丹：")
		zts:显示(self.x+20,self.y+190,"当前经验：")
		zts:显示(self.x+210,self.y+190,"当前银两：")
		zts:显示(self.x+20,self.y+220,"需要经验：")
		zts:显示(self.x+210,self.y+220,"需要金钱：")
		zts:置颜色(0xFFFFFF00)
		zts:显示(self.x+90,self.y+160,self.数据.乾元丹.乾元丹)
		zts:显示(self.x+290,self.y+160,self.数据.乾元丹.可换乾元丹)
		zts:显示(self.x+90,self.y+190,self.数据.当前经验)
		zts:显示(self.x+280,self.y+190,self.数据.当前银子)
		zts:显示(self.x+90,self.y+220,self:计算消耗(self.数据.乾元丹.乾元丹+1,"jy"))
		zts:显示(self.x+280,self.y+220,self:计算消耗(self.数据.乾元丹.乾元丹+1,"jq"))
		zts:置颜色(0xFFFFFFFF)
	elseif self.状态 == "潜能果" then
       		zts:显示(self.x+20,self.y+160,"当前已食用的潜能果：")
		zts:显示(self.x+210,self.y+160,"还能再食用：")
		zts:显示(self.x+20,self.y+190,"当前可用经验点数：")
		zts:显示(self.x+20,self.y+220,"下一个果所需经验：")
		zts:置颜色(0xFFFFFF00)
		zts:显示(self.x+156+14,self.y+160,self.数据.潜能果.潜能果)
		zts:显示(self.x+290+14,self.y+160,self.数据.潜能果.可换潜能果)
		zts:显示(self.x+142+14,self.y+190,self.数据.当前经验)
		zts:显示(self.x+142+14,self.y+220,self:计算潜能果消耗(self.数据.潜能果.潜能果+1))

		zts:置颜色(0xFFFFFFFF)
	end

end

function 场景类_兑换界面:计算潜能果消耗(等级)
  local js =100000000+等级*15000000
return js

end
function 场景类_兑换界面:计算消耗(等级,类型)
	local js = {}
	if 等级 == 1 then
	    js={jy=220000000,jq=40000000}
	elseif 等级 == 2 then
	      js={jy=270000000,jq=55000000}
	elseif 等级 == 3 then
	      js={jy=340000000,jq=70000000}
	elseif 等级 == 4 then
	  js={jy=420000000,jq=80000000}
	elseif 等级 == 5 then
	  js={jy=510000000,jq=100000000}
	elseif 等级 == 6 then
	  js={jy=620000000,jq=120000000}
	elseif 等级 == 7 then
	  js={jy=750000000,jq=150000000}
	elseif 等级 == 8 then
	  js={jy=750000000,jq=150000000}
	elseif 等级 == 9 then
	  js={jy=750000000,jq=150000000}
	else
	js={jy="无法提升",jq="无法提升"}
	end
return js[类型]

end


return 场景类_兑换界面