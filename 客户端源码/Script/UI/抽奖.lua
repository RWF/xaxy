-- @Author: 作者QQ414628710
-- @Date:   2022-01-15 20:06:44
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-17 23:28:38
--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:48
--======================================================================--
local 场景类_抽奖 = class()
local tp,zts,zts1
local floor = math.floor
local tonumber = tonumber
local tostring = tostring
local insert = table.insert
	local sl = 0
	local js= 1
	local sjs =0
function 场景类_抽奖:初始化(根)
	self.ID = 127
	self.x = 220+(全局游戏宽度-800)/2
	self.y = 56
	self.xx = 0
	self.yy = 0
	self.注释 = "抽奖"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	local 资源 = 根.资源
	self.资源组 = {
	    [0] = 资源:载入('JM.dll',"网易WDF动画",0x2788C04C),
	    [1] = 资源:载入('JM.dll',"网易WDF动画",0x65C595A9),
	    [2] = 资源:载入('JM.dll',"网易WDF动画",0x28A3B224),
		[3] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),
       	[4] = 按钮.创建(自适应.创建(12,4,55,22,1,3),0,0,4,true,true,"开始"),
		[5] = 按钮.创建(自适应.创建(12,4,55,22,1,3),0,0,4,true,true,"停止"),
	}
	self.箭头 = {
	[1] = 资源:载入('JM.dll',"网易WDF动画",0xE6ADCF01),
	[2] = 资源:载入('JM.dll',"网易WDF动画",0x8F61ED91),
	[3] = 资源:载入('JM.dll',"网易WDF动画",0xE3292A10),
	[4] = 资源:载入('JM.dll',"网易WDF动画",0xD4A5BD23),
	[5] = 资源:载入('JM.dll',"网易WDF动画",0x8DC9DB6E),
	[6] = 资源:载入('JM.dll',"网易WDF动画",0x51A0CA20),
	[7] = 资源:载入('JM.dll',"网易WDF动画",0xE2A0C942),
	[8] = 资源:载入('JM.dll',"网易WDF动画",0x302E5C54),
	[9] = 资源:载入('JM.dll',"网易WDF动画",0xDC6A040F),
	[10] = 资源:载入('JM.dll',"网易WDF动画",0xA1D5A3E7),
	[11] = 资源:载入('JM.dll',"网易WDF动画",0xD2BAC037),
	[12] = 资源:载入('JM.dll',"网易WDF动画",0x73304A08),
	[13] = 资源:载入('JM.dll',"网易WDF动画",0x26262F11),
	[14] = 资源:载入('JM.dll',"网易WDF动画",0xE25DB7FE),
     }

	for i=3,5 do
		self.资源组[i]:绑定窗口_(127)
	end

  self.介绍文本 = 根._丰富文本(450,280,根.字体表.普通字体)
  self.介绍文本:添加文本("#W/点击#Y/“开始”#W/启动指针转动，点击#Y/“停止”#W/停止指针转动，#Y/指针所指的物品就是亲爱的玩家朋友所获得的奖励哦。#W/快来试一下运气吧！(#R每次抽奖消耗20点活动积分#Y/)")


	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.描边字体
end

function 场景类_抽奖:打开(数据,类型)
	if self.可视 then
		self.可视 = false

	else
	if  self.x > 全局游戏宽度 then
self.x = 220+(全局游戏宽度-800)/2
    end
		self.开始 =nil
		self.结束 =nil
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_抽奖:显示(dt,x,y)
	self.焦点 = false
   	self.资源组[0]:显示(self.x+0,self.y+0)
   	self.资源组[1]:显示(self.x+0,self.y+0)
   	self.资源组[2]:显示(self.x-50,self.y+370)
	self.资源组[3]:显示(self.x+395,self.y+374)
	self.资源组[4]:显示(self.x+110,self.y+424)
	self.资源组[5]:显示(self.x+200,self.y+424)
	self.资源组[3]:更新(x,y)
	self.资源组[4]:更新(x,y,self.开始==nil and self.结束==nil)
	self.资源组[5]:更新(x,y,self.开始~=nil)
	self.介绍文本:显示(self.x-45,self.y+389)
    if self.开始 then
      sl=sl+1
      if sl>2 then
    	 self.开始 =self.开始 +1
    	 sl=0
       end
       if self.开始 >= 14 then
           self.开始 = 1
       end
        self.箭头[self.开始]:显示(self.x+0,self.y+0)
     elseif  self.结束 then
			sl=sl+1
			if sl>5 then
			self.结束 =self.结束 +1
			sl=0
			end


			if self.结束 >= 14 then
			self.结束 = 1
			end
			sjs =sjs +1
			self.箭头[self.结束]:显示(self.x+0,self.y+0)
			if sjs > 取随机数(50,100) then
			   js =self.结束
			   self.结束 =nil
			   sjs= 0
			   客户端:发送数据(js,206,13,"抽奖")
			end
     else
     	self.箭头[js]:显示(self.x+0,self.y+0)
    end
	if self.鼠标 then
		if self.资源组[3]:事件判断() then
			self:打开()
    	elseif self.资源组[4]:事件判断() then
			self.开始 = 1
		 elseif self.资源组[5]:事件判断() then
		 	self.结束 =self.开始
			self.开始 = nil

		end
	end



end


function 场景类_抽奖:检查点(x,y)
	if self.资源组[0]:是否选中(x,y) or self.资源组[2]:是否选中(x,y)  then
		return true
	end
end

function 场景类_抽奖:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_抽奖:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_抽奖