--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--
local 场景类_队标栏 = class(窗口逻辑)

local tp
local insert = table.insert
function 场景类_队标栏:初始化(根)
	self.ID = 152
	self.x = 200
	self.y = 118
	self.xx = 0
	self.yy = 0
	self.注释 = "队标栏"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = 自适应.创建(99,1,400,413,3,9),
		[2] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),

		[98] = 自适应.创建(70,1,360,323,3,9),
		[99] = 按钮.创建(自适应.创建(103,4,72,22,1,3),0,0,4,true,true,"选定队标"),
	
	}
--D2FFEE78 选择框
	   self.资源组[99]:绑定窗口_(152)
  	   self.资源组[2]:绑定窗口_(152)


	local 格子 = 根._队标格子
	self.物品 = {}
	for i=1,30 do
		self.物品[i] = 格子(0,0,"队标")
	end
	self.物品[1]:置根(根)
	self.窗口时间 = 0
	tp = 根

	


end


function 场景类_队标栏:打开(内容)
	if self.可视 then
		self.可视 = false
		if self.上一次 ~= nil and self.上一次 ~= 0 then
			self.物品[self.上一次].确定 = false
		end
		self.上一次=nil
	else
		self.上一次=nil
		for i=1,#内容 do
		self.物品[i]:置物品(内容[i])
		end
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end



function 场景类_队标栏:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[99]:更新(x,y,self.上一次 ~= nil and self.上一次 ~= 0)
	if self.资源组[2]:事件判断() then
		self:打开()
		return false
	elseif self.资源组[99]:事件判断() then
		客户端:发送数据(self.上一次,16, 11, "77")
	end
	self.资源组[1]:显示(self.x,self.y)
	Picture.标题:显示(self.x+self.资源组[1].宽度/2-90,self.y)
	 tp.字体表.华康字体:置颜色(0xFFFFFFFF):显示(self.x+175,self.y+3,"队标选择")

	self.资源组[2]:显示(self.x + 373,self.y + 6,true)
	self.资源组[98]:显示(self.x + 20,self.y + 35)
	self.资源组[99]:显示(self.x + 160,self.y + 378,true,1)
	for i=1,6 do
		引擎.画线(self.x+20+i*60,self.y+35,self.x+20+i*60,self.y+360,0xFF000053)
	end
	for o=1,4 do
		引擎.画线(self.x+20,self.y+35+65*o,self.x+380,self.y+35+65*o,0xFF000053)
	end
	local xx = 0
	local yy = 0
	for i=1,30 do
		local jx = xx * 60 + 20
		local jy = yy * 65 + 35

		self.物品[i]:置坐标(self.x + jx,self.y + jy)
		self.物品[i]:显示(dt,x,y)
		if self.物品[i].焦点 and self.物品[i].物品 ~= nil then
			if self.物品[i].事件 then
				if self.上一次 ~= nil and self.上一次 ~= 0 then
					self.物品[self.上一次].确定 = false
				end
				self.物品[i].确定 = true
				self.上一次=i
			end
		end
		xx = xx + 1
		if xx == 6 then
			xx = 0
			yy = yy + 1
		end
	end



end



return 场景类_队标栏