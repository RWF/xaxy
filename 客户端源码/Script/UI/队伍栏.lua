--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--
local 场景类_队伍栏 = class(窗口逻辑)
local insert = table.insert
local remove = table.remove
local floor = math.floor
local tp,zts,zts1

function 场景类_队伍栏:初始化(根)
	self.ID = 12
	self.x = 82+(全局游戏宽度-800)/2
	self.y = 169
	self.xx = 0
	self.yy = 0
	self.注释 = "队伍栏"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应

	self.资源组 = {
		[1] = 自适应.创建(99,1,617,291+20,3,9),
		[2] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
		[3] = 按钮.创建(自适应.创建(103,4,55,22,1,3),0,0,4,true,true,"阵型"),
		[4] = 按钮.创建(自适应.创建(103,4,72,22,1,3),0,0,4,true,true,"踢出队伍"),
		[5] = 按钮.创建(自适应.创建(103,4,72,22,1,3),0,0,4,true,true,"申请列表"),

		[7] = 根.资源:载入('ZY.dll',"网易WDF动画",0x2231EBB4),
		[8] = 自适应.创建(8,1,93,19,1,3),
		[9] = 自适应.创建(8,1,36,19,1,3),
		[10] = 按钮.创建(自适应.创建(103,4,72,22,1,3),0,0,4,true,true,"离队队伍"),
		[11] = 自适应.创建(2,1,116,141,3,9),
		[12] = 自适应.创建(8,1,117,19,1,3),
		[13] = 按钮.创建(自适应.创建(103,4,72,22,1,3),0,0,4,true,true,"提升队长"),
		[14] = 按钮.创建(自适应.创建(103,4,55,22,1,3),0,0,4,true,true,"等级"),
		[15] = 按钮.创建(自适应.创建(103,4,55,22,1,3),0,0,4,true,true,"查看"),
	}
	for n=2,4 do
		self.资源组[n]:绑定窗口_(12)
	end
	self.队伍坐标 = {14,133,252,371,490}
	self.队伍格子 = {}
	local 格子 = require("script/System/队伍_格子")
	for i=1,5 do
		self.队伍格子[i] = 格子.创建(0,0,i,根)
	end
	self.选中人物 = 0
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.描边字体
  	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('给予总控件')
	总控件:置可视(true,true)
	self.输入框 = 总控件:创建输入("最低等级输入",300,47,36,14)
	self.输入框:置可视(false,false)
	self.输入框:置限制字数(3)
	self.输入框:屏蔽快捷键(true)
	self.输入框:置光标颜色(-16777216)
	self.输入框:置文字颜色(-16777216)
   	self.输入框1 = 总控件:创建输入("最高等级输入",360,47,36,14)
   	self.输入框1:置数字模式()
	self.输入框1:置可视(false,false)
	self.输入框1:置限制字数(3)
	self.输入框1:屏蔽快捷键(true)
	self.输入框1:置光标颜色(-16777216)
	self.输入框1:置文字颜色(-16777216)
	for n=2,4 do
		self.资源组[n]:绑定窗口_(12)
	end
	self.队伍坐标 = {14,133,252,371,490}
	self.队伍格子 = {}
		local 格子 = require("script/System/队伍_格子")
	for i=1,5 do
		self.队伍格子[i] = 格子.创建(0,0,i,根)
	end
	self.选中人物 = 0
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.华康字体
	zts1 = tp.字体表.华康字体
	self.当前阵法="普通"
	self.队伍数据={}
end
function 场景类_队伍栏:刷新阵法(内容)
  self.当前阵法=内容
 end
function 场景类_队伍栏:刷新(内容)
	self.队伍格子 = {}
	self.队伍数据={}
	self.选中人物 = 0
	local 格子 = require("script/System/队伍_格子")
	for i=1,5 do
	self.队伍格子[i] = 格子.创建(0,0,i,根)
	end
	if 内容~=nil then
		for n=1,#内容 do
		self.队伍数据[n]=内容[n]
		self.队伍格子[n]:置人物(self.队伍数据[n])
		end
	end
	self.输入框:置文本(内容.最低等级)
	self.输入框1:置文本(内容.最高等级)
 end
function 场景类_队伍栏:打开(内容)
   self.申请开关=false
  if self.可视 then
  		self.队伍数据={}
		local 格子 = require("script/System/队伍_格子")
		for i=1,5 do
		self.队伍格子[i]:置人物(nil)
		end
		self.选中人物 = 0
		self.可视 = false
  else
		if  self.x > 全局游戏宽度 then
		self.x = 82+(全局游戏宽度-800)/2
		end
		insert(tp.窗口_,self)
		if 内容~=nil then
			for n=1,#内容 do
			self.队伍数据[n]=内容[n]
			self.队伍格子[n]:置人物(self.队伍数据[n])
			end
		end
	    self.输入框:置可视(true,true)
	    self.输入框:置文本(0)
	    self.输入框1:置可视(true,true)
	    self.输入框1:置文本(0)
	    self.输入框:置文本(内容.最低等级)
	    self.输入框1:置文本(内容.最高等级)
       if 内容.队标 == "普通" then
          self.资源组[7] = tp.资源:载入('ZY.dll',"网易WDF动画",0x2231EBB4)
         elseif 内容.队标=="玫瑰" then
			self.资源组[7]= tp.资源:载入('ZY.dll',"网易WDF动画",0xF8107DB0)
         elseif 内容.队标=="扇子" then
			self.资源组[7]= tp.资源:载入('ZY.dll',"网易WDF动画",0x1B25FA29)
		elseif 内容.队标=="音符队标" then
		self.资源组[7] =tp.资源:载入('ZY.dll',"网易WDF动画",0x25D20174)
		elseif 内容.队标=="鸭梨队标" then
		self.资源组[7] =tp.资源:载入('ZY.dll',"网易WDF动画",0x7AEF08A1)
		elseif 内容.队标=="心飞翔队标" then
		self.资源组[7] =tp.资源:载入('ZY.dll',"网易WDF动画",0x9BEF1016)

       end

		tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.可视 = true
  end
 end
function 场景类_队伍栏:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y)
	self.资源组[4]:更新(x,y,self.选中人物 ~= 0 and #self.队伍数据 > 1)
	self.资源组[13]:更新(x,y,self.选中人物 ~= 0 and #self.队伍数据 > 1)
	self.资源组[5]:更新(x,y)
	self.资源组[7]:更新(dt)
	self.资源组[10]:更新(x,y)
	self.资源组[14]:更新(x,y)
	self.资源组[15]:更新(x,y,self.选中人物 ~= 0)

	if self.资源组[2]:事件判断() then
		self:打开()
		return false
	elseif self.资源组[3]:事件判断() then
		  客户端:发送数据(4, 3, 11, "7", 1)
	elseif self.资源组[4]:事件判断() then
		tp.窗口.文本栏:载入("#H/真的要踢出等级为#G"..self.队伍数据[self.选中人物].等级.."#H/的队员#R"..self.队伍数据[self.选中人物].名称.."#H/吗？","踢出人物",true)
	elseif self.资源组[13]:事件判断() then
     tp.窗口.文本栏:载入("#H/真的要将#G"..self.队伍数据[self.选中人物].等级.."#H/的队员#R"..self.队伍数据[self.选中人物].名称.."#H/更换为队长吗？","更换队长",true)
    elseif self.资源组[5]:事件判断() then
       客户端:发送数据(8, 7, 11, "66", 1)
	elseif self.资源组[10]:事件判断() then
		客户端:发送数据(4, 11, 11, "77", 1)
    elseif self.资源组[14]:事件判断() then
    	客户端:发送数据(self.输入框:取文本()+0, 14, 11,self.输入框1:取文本()+0)
    elseif self.资源组[7]:是否选中(x,y) and 引擎.鼠标按下(0) then
    	客户端:发送数据(15, 15, 11, "77")
	end

	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x + 591,self.y + 6)
	Picture.标题:显示(self.x+self.资源组[1].宽度/2-90,self.y)
	
	self.资源组[7]:显示(self.x + 63,self.y + 63)
	self.资源组[8]:显示(self.x+154,self.y+33+10)
	self.资源组[14]:显示(self.x + 395,self.y + 28,true,1)
	self.资源组[15]:显示(self.x + 395,self.y + 55,true,1)
	self.资源组[4]:显示(self.x + 455,self.y + 28,true,1)
	self.资源组[5]:显示(self.x + 455,self.y + 55,true,1)
	self.资源组[13]:显示(self.x + 530,self.y + 55,true,1)
	self.资源组[10]:显示(self.x + 530,self.y + 28,true,1)

	zts1:显示(self.x+270,self.y+3,"队伍列表")

	zts1:显示(self.x+15,self.y+35+10,"令牌")
	zts1:显示(self.x+252,self.y+35+10,"等级：")
	zts1:显示(self.x+334,self.y+35+10,"至")
	self.资源组[3]:显示(self.x + 89,self.y + 32+10,true,1)
	self.资源组[9]:显示(self.x+292,self.y+33+10)
	self.资源组[9]:显示(self.x+354,self.y+33+10)
	zts:置颜色(-16777216)
	zts:显示(self.x + 164,self.y + 37+10,self.当前阵法)
	self.输入框:置坐标(self.x,self.y )
	self.输入框1:置坐标(self.x ,self.y )
	for i=0,4 do
		local jx = 11+i*120
		self.资源组[11]:显示(self.x+jx,self.y+63+20)
		for n=0,2 do
			self.资源组[12]:显示(self.x+jx,self.y+211+n*24+20)
		end

		i = i + 1
		self.队伍格子[i]:置坐标(self.x + jx,self.y + 63+20)
		self.队伍格子[i]:显示(dt,x,y+20,self.鼠标)
		if self.队伍格子[i].事件 then
			if self.选中人物 ~= 0 and self.选中人物 ~= i then
				客户端:发送数据(i, 13, 11,self.选中人物, 1)
			else
				self.选中人物 = i
				self.队伍格子[self.选中人物].禁止 = true
			end
		end
		zts1:置颜色(0xFF000000):显示(self.x+jx+88,self.y+182+20,i)
		zts1:置颜色(0xFFFFFFFF)
	end
	self.控件类:更新(dt,x,y)
	if self.输入框._已碰撞 then
		self.焦点 = true
	end
	if self.输入框1._已碰撞 then
		self.焦点 = true
	end
	self.控件类:显示(x,y)
end




return 场景类_队伍栏


