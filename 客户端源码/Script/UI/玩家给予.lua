--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--
local 玩家给予 = class()
local insert = table.insert
local tp,zts1,zts
local mouseb = 引擎.鼠标弹起
function 玩家给予:初始化(根)
	self.ID = 54
	self.x = 30
	self.y = 35
	self.xx = 0
	self.yy = 0
	self.注释 = "玩家给与"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = Picture.给予界面,
		[2] =  根._按钮(根._自适应(15,4,75,20,1),0,0,4,true,true," 给  予"),
		[4] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
	 }
	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('创建控件')
	总控件:置可视(true,true)
	self.输入框 = 总控件:创建输入("银两给予",self.x+437,self.y+386-18,80,14,根)
	self.输入框:置限制字数(8)
	self.输入框:置数字模式()
    self.输入框:置光标颜色(0xFFFF0000)
	self.数量输入 ={}
    self.给予物品={}
    self.物品数据={}
	local 格子 = 根._物品格子
	for i=1,20 do
	  self.物品数据[i] = 格子.创建(0,0,i,"给予物品")
	end
	for i=1,3 do
		self.数量输入[i] = 总控件:创建输入("数量输入"..i,self.x+216+i*80,self.y+350-20,25,14,根)
		self.数量输入[i]:置可视(false,false)
		self.数量输入[i]:置限制字数(3)
		self.数量输入[i]:置数字模式()
		self.数量输入[i]:置文字颜色(-16777216)
		self.给予物品[i] = 格子.创建(0,0,i,"选中物品")
	end
	self.窗口时间 = 0
	tp = 根
	zts = 根.字体表.普通字体
	zts1 = 根.字体表.描边字体
	zts1:置颜色(0xFFFFFFFF)

 self.选中编号 ={[1]=0,[2]=0,[3]=0}
end

function 玩家给予:打开(数据)
	if self.可视 then
		self.可视 = false
		self.输入框:置焦点(false)
		self.输入框:置可视(false,false)
		self.输入框:置文本(0)
		for i=1,3 do
		self.数量输入[i]:置焦点(false)
		self.数量输入[i]:置可视(false,false)
		self.数量输入[i]:置文本(1)
		self.给予物品[i]:置物品(nil)
		self.选中编号[i] = 0
		end
		for i=1,20 do
			self.物品数据[i]:置物品(nil)
		end
	else
		if  self.x > 全局游戏宽度 then
		self.x = 30
		end
        self.本类数据=数据
        self.本类数据.id=self.本类数据.id+0
        self.给予名称=self.本类数据.名称
	    self.给予等级=self.本类数据.等级
	    self.给予id=self.本类数据.id


		for i=1,20 do
			if self.本类数据.道具[i]~=nil then
			self.物品数据[i]:置物品(self.本类数据.道具[i])
		    end
         end
		insert(tp.窗口_,self)
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
		self.可视 = true
	end
end

function 玩家给予:显示(dt,x,y)
	self.焦点 = false
  self.资源组[1]:显示(self.x+250,self.y+20)
 self.资源组[2]:显示(self.x+350,self.y+420,true,1)
 self.资源组[2]:更新(x,y)

 self.资源组[4]:显示(self.x+500,self.y+24)
 self.资源组[4]:更新(x,y)
	 if self.资源组[4]:事件判断()  then
	 self:打开()
	 elseif self.资源组[2]:事件判断() then
     	self:交易物品()
	 end
	local xx = 0
	local yy = 0
	for i=1,20 do
		self.物品数据[i]:置坐标(self.x + xx * 51 + 260,self.y + yy * 51 + 47)
		self.物品数据[i]:显示(dt,x,y,self.鼠标)
		if self.物品数据[i].物品 ~= nil and self.物品数据[i].焦点 then
			tp.提示:道具行囊(x,y,self.物品数据[i].物品,true)
			if self.物品数据[i].事件 then
				for o=1,3 do
				  if self.给予物品[o].物品 == nil then
				  self.给予物品[o]:置物品(self.本类数据.道具[i])
				  self.物品数据[i]:置物品(nil)
				  self.数量输入[o]:置可视(true,true)

				  self.选中编号[o] = i
					break
				  end
				end
             end
		end
		xx = xx + 1
		if xx == 5 then
			xx = 0
			yy = yy + 1
		end
	end
	for i=1,3 do
		self.数量输入[i]:置坐标(self.x+i*1-28,self.y-35)
		self.给予物品[i]:置坐标(self.x  +i *78+ 203,self.y +292-25)
		self.给予物品[i]:显示(dt,x,y,self.鼠标)
		if self.数量输入[i]._已碰撞 then
		self.焦点 = true
		end
		if self.给予物品[i].物品 ~= nil then
	        if self.数量输入[i]:取文本() == "" then
			self.数量输入[i]:置文本(1)
		    end
			if  self.本类数据.道具[self.选中编号[i]]  and self.本类数据.道具[self.选中编号[i]].数量 ~= nil then
	            --if tonumber(self.数量输入[i]:取文本()) > self.本类数据.道具[self.选中编号[i]].数量 then
		        self.数量输入[i]:置文本(self.本类数据.道具[self.选中编号[i]].数量)
		       -- elseif tonumber(self.数量输入[i]:取文本()) > 99 then
				--self.数量输入[i]:置文本(99)
		       -- end
		   else
		   	 self.数量输入[i]:置文本(1)
            end
			if self.给予物品[i].焦点 then
			tp.提示:道具行囊(x,y,self.给予物品[i].物品,true)
				if  self.给予物品[i].事件 or self.给予物品[i].右键 then
					self.物品数据[self.选中编号[i]]:置物品(self.本类数据.道具[self.选中编号[i]])
					self.选中编号[i] = 0
					self.给予物品[i]:置物品(nil)
					self.数量输入[i]:置可视(false,false)
				end
		     end
		 end
	end

 zts1:显示(self.x+258,self.y+410-20,"对象:")
 zts:置颜色(0xFFFFFF00)
 zts:显示(self.x+435,self.y+414-20,self.给予等级)
 zts:显示(self.x+295,self.y+386-20,self.本类数据.道具.银两)
 zts:显示(self.x+297,self.y+412-20,self.给予名称)
 self.输入框:置坐标(self.x-30,self.y-35)
 self.输入框:置可视(true,true)
	if self.输入框:取文本() == "" then
	 self.输入框:置文本(0)
	end
	 self.输入框:置文字颜色(引擎.取金钱颜色(tonumber(self.输入框:取文本())))
	if assert(tonumber(self.输入框:取文本()), "银子这只能输入数字")  > self.本类数据.道具.银两 then
	 self.输入框:置文本(self.本类数据.道具.银两)
	end
	if self.输入框._已碰撞 then
	 self.焦点 = true
	end
 self.控件类:更新(dt,x,y)
 self.控件类:显示(x,y)
end
function 玩家给予:交易物品()
	local 临时数量={}
 self.物品存在=0
	for n=1,3 do
		临时数量[n]= tonumber(self.数量输入[n ]:取文本())
	if self.选中编号[n]==0 then
	 self.物品存在=self.物品存在+1
	end
	end
	if self.物品存在==3 and tonumber(self.输入框:取文本())==0 then
	tp.提示:写入("#Y/你要给予对方什么？")
	else

		客户端:发送数据(self.本类数据.id,self.输入框:取文本()+0, 29, self.选中编号[1].."*-*"..self.选中编号[2].."*-*"..self.选中编号[3])



	self:打开()
	end
 end
function 玩家给予:检查点(x,y)
	if self.资源组[1]:是否选中(x,y)  then
		return true
	end
end

function 玩家给予:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 玩家给予:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 玩家给予