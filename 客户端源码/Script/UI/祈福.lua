--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:49
--======================================================================--

local 场景类_祈福 = class()
local insert = table.insert
local remove = table.remove
local floor = math.floor
local tp,zts1

function 场景类_祈福:初始化(根)
	self.ID = 201
	self.x = 82+(全局游戏宽度-800)/2
	self.y = 20
	self.xx = 0
	self.yy = 0
	self.注释 = "祈福"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
    local  资源= 根.资源
	self.资源组 = {
		[1] = 根.资源:载入('JM.dll',"网易WDF动画",0x48AF84E5),
		[2] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x66914CF5),0,0,4),
		--[3] = 根.资源:载入('JM.dll',"网易WDF动画",0x972CA1E4),
		-- [8] = 根.资源:载入('JM.dll',"网易WDF动画",0x8269DBB8),--3
		-- [9] = 根.资源:载入('JM.dll',"网易WDF动画",0x107F0617),
		-- [10]=按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x20F3E242),0,0,4),--??
		-- [11]=按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xCAABDBB8),0,0,4),--??
		-- [12]=按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x219A1626),0,0,4,true,true,"银子排行"),

	}

self.资源组[2]:绑定窗口_(201)
	self.窗口时间 = 0
	tp = 根
	zts1 = tp.字体表.华康字体

end
function 场景类_祈福:打开(内容)

  if self.可视 then
		self.可视 = false
  else
  	    self.数据 = 内容
		if  self.x > 全局游戏宽度 then
		self.x = 82+(全局游戏宽度-800)/2
		end
		insert(tp.窗口_,self)
		tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.可视 = true
  end
 end
function 场景类_祈福:显示(dt,x,y)
	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:更新(x,y)
	self.资源组[2]:显示(self.x+205,self.y+130)
	if self.资源组[2]:事件判断() then
	   客户端:发送数据(14,14,5,self.数据.类型)
	end
	zts1:置颜色(0xFFFFFFFF):显示(self.x+185,self.y+405,"你当前的祈福次数为："..self.数据.祈福)
end


function 场景类_祈福:检查点(x,y)
	if self.可视 and self.资源组[1]:是否选中(x,y)  then
		return true
	else
		return false
	end
end

function 场景类_祈福:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.可视 and self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_祈福:开始移动(x,y)
	if self.可视 and self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_祈福



