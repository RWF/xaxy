
local 场景类_法宝神器 = class(窗口逻辑)
local hint = {[6]="包裹",[7]="行囊"}
function 场景类_法宝神器:初始化(根)
    self.ID = 502
    self.x = 156
    self.y = 132
    self.xx = 0
    self.yy = 0
    self.注释 = "法宝神器"
    self.可视 = false
    self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    self.窗口 = "法宝"
    local 资源 = 根.资源
    local 按钮 = 根._按钮
    local 自适应 = 根._自适应
    self.资源组 = {
        [1] = Picture.法宝界面,
        [2]=按钮.创建(自适应.创建(105,4,32,20,1,3),0,0,4,true,true," 1"),
        [3]=按钮.创建(自适应.创建(105,4,32,20,1,3),0,0,4,true,true," 2"),
        [4]=按钮.创建(自适应.创建(105,4,32,20,1,3),0,0,4,true,true," 3"),
        [5]=按钮.创建(自适应.创建(105,4,32,20,1,3),0,0,4,true,true,"看"),
        [6]=按钮.创建(自适应.创建(105,4,75,20,1,3),0,0,4,true,true,"  道具"),
        [7]=按钮.创建(自适应.创建(105,4,75,20,1,3),0,0,4,true,true,"  行囊"),  
        [8]=按钮.创建(自适应.创建(102,4,72,20,1,3),0,0,4,true,true,"  使用"),  
        [9]=按钮.创建(自适应.创建(102,4,72,20,1,3),0,0,4,true,true,"  修炼"),  
        [10]=按钮.创建(自适应.创建(102,4,72,20,1,3),0,0,4,true,true,"补充灵气"),
        [11]=按钮.创建(资源:载入('新关闭按钮',"动画"),0,0,4,true,true), 
        [12]=按钮.创建(自适应.创建(100,4,75,26,1,3),0,0,4,true,true,"法宝"),
        [13]=按钮.创建(自适应.创建(100,4,75,26,1,3),0,0,4,true,true,"神器"),
        [14]=按钮.创建(自适应.创建(100,4,75,26,1,3),0,0,4,true,true,"灵宝"), 
        [15]=按钮.创建(自适应.创建(102,4,75,20,1,3),0,0,4,true,true,"洗炼技能"),   
        [16]=按钮.创建(自适应.创建(102,4,75,20,1,3),0,0,4,true,true,"洗炼属性"),  
        [17]=按钮.创建(自适应.创建(102,4,75,20,1,3),0,0,4,true,true,"补充灵气"),
        [18]=按钮.创建(自适应.创建(102,4,75,20,1,3),0,0,4,true,true,"提取灵气"),    
    }
    tp = 根
    local 格子 = 根._物品格子

    self.物品 = {}
    local 底图 = 根.资源:载入("底图2.png","加密图片")
    self.法宝 = {}
    for i=1,4 do
        self.法宝[i] = 格子(0,0,i,"法宝",底图)
    end
    for i=2,11 do
        self.资源组[i]:绑定窗口_(502)
    end
    for i=15,18 do
        self.资源组[i]:绑定窗口_(502)
    end
    for i=12,14 do
        self.资源组[i]:绑定窗口_(502)
         self.资源组[i]:置偏移(15,2)
    end
    for i=1,20 do
            self.物品[i] = 格子(0,0,i,"法宝神器物品")
    end
    self.加入 = 0
    self.窗口时间 = 0
    self.法宝数量=0
    self.介绍文本 = 根._丰富文本(92,144)
    self.介绍文本:置默认颜色(0xFFFFFFFF)
 
end
function 场景类_法宝神器:刷新(数据)
   self.数据=数据
   self.更新  =self.数据.类型
   self.介绍文本:清空()
         if  self.更新=="法宝"then
             self.资源组[1] = Picture.法宝界面
            local 格子 = tp._物品格子
            for q=1,20 do
            self.物品[q]:置物品(self.数据[q])
            end
            for i=1,4 do
            self.法宝[i]:置物品(self.数据[i+34])
            end
             if  self.选中法宝~=nil then
                    local is = self.选中法宝.id
                    self.物品[self.选中法宝.id].确定 = false
                    self.选中法宝 = self.物品[self.选中法宝.id].物品 
                    if  self.选中法宝 then

                      self.选中法宝.id=is
                      self.物品[self.选中法宝.id].确定 = true
 
                    end
                   
                    
                    
                    
             end
        else
            local 神器介绍 ={
                藏锋敛锐="破釜沉舟额外增加四个单位输出，有一定几率不休息",
                惊锋="增加横扫千军百分之20的伤害，同时气血消耗增加",
                泪光盈盈="增加雨落寒沙20%的伤害",
                璇华 ="增加五行法术伤害效果30%", 
                玉魄="杨柳甘露有几率满状态复活",
                沧浪赋="龙卷雨击增加法术30%伤害",
                澄明="天罗地网额外增加4个单位，伤害结果增加15%",
                相思="地涌金莲额外增加3个单位",
                凭虚御风="落叶萧萧增加额外2个单位，技能伤害增加5%",
                弦外之音="雷霆万钧增加额外3个目标",
                千机巧变="针锋相对技能效果增加50%",
                情思悠悠="地涌金莲的目标获得治疗量增加20%",
                定风波="额外增加百分之15%法术暴击几率",
                酣战="进入战斗额外增加3点战意，每消耗一点战意增加1%的伤害（最高增加5%）",
                泪雨零铃="增加唤魔·毒魅20%伤害，有几率获得重生",
                裂帛="伤害性法术伤害增加20%",
                狂战="提高自身百分之15物理伤害降低5%自己防御",
                魂魇="气血小于30%时，增加尸腐毒50%伤害结果",
                金汤之固="气血小于30%时，增加百分之50的法术伤害",
                披坚执锐="遭受攻击时，有8%几率免受90%的伤害。",
                斗转参横="带有状态生命之泉时，日月乾坤命中率提升10%",
                镜花水月="封印命中率增加10%，降低自身15%气血和防御",
                威服天下="攻击时无视目标的“幸运”、“高级幸运”，且暴击伤害增加26%",
                挥毫="治疗效果增加百分之35，同时蓝耗翻倍",
                风起云墨="受到你治疗的目标本回合内受到的所有伤害降低8%。",
                迭锁="偃甲状态下，每个攻之械提升你百分之10的伤害",
                静笃="每次击杀敌方单位，增加一定伤害最高增加200点",
                蛮血="增加15%鹰击伤害，有额外几率增加3个单位",
                业焰明光="你的单体法术有50%的几率造成额外40%的伤害",
                亡灵泣语="在夜晚状态额外增加50%技能伤害结果",
                流火="飞砂走石有30%几率增加30%的伤害，飞砂走石有30%几率增加额外增加3个单位",
                惊梦="焚魔烈焰技能效果增加20%，额外增加2个单位",
                钟灵="蜜润效果增加50%，额外增加5个单位",
                开辟="当头一棒效果提升15%，双倍伤害范围调整为60%-80%",
                万物滋长="战斗进场有20%变身齐天大圣美猴王提高30%的物理伤害",
                盏中晴雪="进入战斗时有10%率提升速度20%，同时降低自身10%防御和10%的灵力" ,
                
          }
             self.资源组[1] = Picture.神器界面
             self.选中法宝=nil
             self.介绍文本:添加文本(神器介绍[self.数据.数据.技能])
             for q=1,20 do
                self.物品[q]:置物品(nil)
                self.物品[q].确定=false
            end
        end
end

function 场景类_法宝神器:打开(数据)
    if self.可视 then
        self.可视 = false
        self.加入 = nil
        self.窗口 = nil
        self.鼠标 = false
        self.选中法宝=nil
        for i=1,4 do
            self.法宝[i]:置物品(nil)
        end
        self.介绍文本:清空()
        tp.抓取物品 = nil
        tp.抓取物品ID = nil
        tp.抓物品注释 = nil
        self.更新 = nil
        self.资源组[1] = Picture.法宝界面
       
    else
         self.法宝数量 =0
         self.选中法宝=nil
       self.数据=数据
       self.更新  =self.数据.类型
        for q=1,20 do
            self.物品[q]:置物品(self.数据[q])
            self.物品[q].确定=false
            if self.数据[q] then
               self.法宝数量=self.法宝数量+1
            end
        end
        for i=1,4 do
         self.法宝[i]:置物品(self.数据[i+34])
        end
        table.insert(tp.窗口_,self)
        self.加入 = 0
        tp.运行时间 = tp.运行时间 + 1
        self.窗口时间 = tp.运行时间
        self.可视 = true
    end
end
function 场景类_法宝神器:显示(dt,x,y)
    self.焦点 = false


    self.资源组[1]:显示(self.x,self.y)
    self.资源组[12]:显示(self.x+260,self.y+30,true,1,nil,self.更新=="法宝",2)
    self.资源组[13]:显示(self.x+350,self.y+30,true,1,nil,self.更新=="神器",2)
    self.资源组[14]:显示(self.x+440,self.y+30,true,1,nil,self.更新=="灵宝",2)
    self.资源组[11]:显示(self.x+516,self.y+3)
        self.资源组[2]:显示(self.x+92,self.y+33,true,1)
        self.资源组[3]:显示(self.x+132,self.y+33,true,1)
        self.资源组[4]:显示(self.x+172,self.y+33,true,1)
        self.资源组[5]:显示(self.x+212,self.y+33,true,1)
              for i=2,5 do
        self.资源组[i]:更新(x,y)
        end 
    for i=11,14 do
        self.资源组[i]:更新(x,y)
    end 
      if self.资源组[11]:事件判断()  then
          self:打开()
      elseif self.资源组[12]:事件判断()  then
          客户端:发送数据(3, 1, 13)
      elseif self.资源组[13]:事件判断()  then
        客户端:发送数据(33, 33, 5)
      elseif self.资源组[14]:事件判断()  then
         tp.提示:写入("#Y/功能还未开发,请留意公告")
      end
     for i=1,4 do
            self.法宝[i]:置坐标(self.x +i * 57-37,self.y +62)
            self.法宝[i]:显示(dt,x,y,self.鼠标,nil,0.7)
            if self.法宝[i].物品 ~= nil and self.法宝[i].焦点 then
                tp.提示:道具行囊(x,y,self.法宝[i].物品,true)
            end
            if self.法宝[i].事件 and self.更新 =="法宝" then
                    if tp.抓取物品  and self.法宝[i].物品 == nil and self.法宝[i].焦点  then
                        if self:可装备(tp.抓取物品,i) then
                            if tp.抓取物品注释 == "法宝神器" then
                                客户端:发送数据(3, 1, 13, "9", 1)
                                self.法宝[i].确定 = false
                            elseif tp.抓取物品注释 == "法宝" then
          
                                        self.法宝[tp.抓取物品ID-34].确定 = false
                                        self.法宝[tp.抓取物品ID-34]:置物品(tp.抓取物品)

                     
                            else
                            客户端:发送数据(tp.抓取物品ID,196,13,self.更新)
                            self.法宝[i].确定 = false
                            self.物品[tp.抓取物品ID].确定 = false
                            end
                            tp.抓取物品 = nil
                            tp.抓取物品ID = nil
                            tp.抓取物品注释 = nil
                         end
                    elseif self.法宝[i].焦点 and tp.抓取物品 == nil and self.法宝[i].物品 ~= nil then
                        tp.抓取物品 = self.法宝[i].物品
                        tp.抓取物品ID = i+34
                        tp.抓取物品注释 = self.法宝[i].注释
                        self.法宝[i].确定 = true
                        self.法宝[i]:置物品(nil)
                    elseif  tp.抓取物品 ~= nil and self.法宝[i].物品 ~= nil and self.法宝[i].焦点 and tp.抓取物品注释 == "法宝神器" then
                        if self:可装备(tp.抓取物品,i) then
                            客户端:发送数据(tp.抓取物品ID,196,13,self.更新)
                        
                            self.物品[tp.抓取物品ID].确定 = false
                            tp.抓取物品 = nil
                            tp.抓取物品ID = nil
                            tp.抓取物品注释 = nil
                        end
                    end
            elseif self.法宝[i].右键  and self.更新 =="法宝" then
                客户端:发送数据(i+34,196,13,self.更新)
            end
        end
    if self.更新 =="法宝" then
        if self.鼠标 then
          for i=6,7 do
             if self.资源组[i]:事件判断() then
                 if tp.抓取物品 then
                     客户端:发送数据(tp.抓取物品ID, 14,13, self.更新, hint[i])
                     self.物品[tp.抓取物品ID].确定 = false
                    tp.抓取物品 = nil
                    tp.抓取物品ID = nil
                    tp.抓取物品注释 = nil
                 else
                    tp.提示:写入("#Y/少侠请你抓取一个法宝")       
                 end
                 break
             end
          end
            for i=8,10 do
                 if self.资源组[i]:事件判断() then
                     if self.选中法宝 then
                        if  i==8 then
                            if self.选中法宝.名称 =="月光宝盒" then
                            客户端:发送数据(self.选中法宝.id,207,13,"定标")
                            elseif self.选中法宝.名称 =="影蛊" then
                            客户端:发送数据(self.选中法宝.id,242,13,"影蛊")
                           else
                            tp.提示:写入("#Y该法宝无法主动使用!")
                            end
                        
                        elseif i==9 then
                          客户端:发送数据(self.选中法宝.id,195,13,self.更新)
                        elseif i==10 then--补充灵气
                             tp.窗口.文本栏:载入("#H/最低消耗120点剩余灵气,给对应的法宝补充（120/法宝等级）的灵气","补充法宝灵气",true)
                          
                        end
                     else
                        tp.提示:写入("#Y/少侠请右键选中一个法宝")       
                     end
                     break
                 end
            end
            if self.资源组[18]:事件判断() then
                 if self.选中法宝 then --提取灵气
                    tp.窗口.文本栏:载入("#H/提取法宝灵气将补充（等级*法宝灵气）点剩余灵气，法宝将会消失，确定提取吗？","提取法宝灵气",true)
                      
                 else
                    tp.提示:写入("#Y/少侠请右键选中一个法宝")       
                 end
            end
        end
        for i=6,10 do
        self.资源组[i]:更新(x,y)
        end 
        self.资源组[18]:更新(x,y)
        self.资源组[6]:显示(self.x+354,self.y+287,true,1)
        self.资源组[7]:显示(self.x+448,self.y+287,true,1)
        self.资源组[8]:显示(self.x+15,self.y+287,true,1)
        self.资源组[9]:显示(self.x+95,self.y+287,true,1)
        self.资源组[10]:显示(self.x+175,self.y+287,true,1)
        self.资源组[18]:显示(self.x+15,self.y+317,true,1)
        tp.字体表.华康字体:置颜色(-256):显示(self.x+95,self.y+317,"剩余灵气："..self.数据.剩余灵气)
        local o = 0
        for h=1,4 do
            for l=1,5 do
                o = o + 1
                self.物品[o]:置坐标(l * 51+209 + self.x,h * 51 +16 + self.y)
                self.物品[o]:显示(dt,x,y,self.鼠标)
                if self.物品[o].焦点 and not tp.消息栏焦点 then
                    if self.物品[o].物品 ~= nil then
                        tp.提示:道具行囊(x,y,self.物品[o].物品)
                    end
                    self.焦点 = true
                end

                if tp.抓取物品 == nil and self.物品[o].焦点 and self.物品[o].物品 ~= nil and not tp.消息栏焦点 then
                    if self.物品[o].右键  then
                        if self.更新 == "法宝" and self.物品[o].物品.类型 == "法宝" then
                            if self.选中法宝 then
                                self.物品[self.选中法宝.id].确定 = false
                            end
                            self.选中法宝=self.物品[o].物品
                            self.选中法宝.id=o
                            self.物品[o].确定 = true
                        end
                    elseif  self.物品[o].事件 then
                        if self.选中法宝 then
                            self.物品[self.选中法宝.id].确定=false
                            self.选中法宝 =nil
                        end
                        tp.抓取物品 = self.物品[o].物品
                        tp.抓取物品ID = o
                        tp.抓取物品注释 = self.物品[o].注释
                        self.物品[tp.抓取物品ID].确定 = true
                        self.物品[o]:置物品(nil)
                        return
                    end
                end

                if self.物品[o].事件 and tp.抓取物品  then
                        if tp.抓取物品注释 == "法宝神器物品" then
                             客户端:发送数据(tp.抓取物品ID,o, 15, self.更新)

                                self.物品[tp.抓取物品ID].确定 = false

                        elseif tp.抓取物品注释 == "法宝" then
                            if self.物品[o].物品 == nil  then
                                self.法宝[tp.抓取物品ID-34]:置物品(nil)
                                 客户端:发送数据(tp.抓取物品ID,196,13,self.更新,o)
                                self.法宝[tp.抓取物品ID-34].确定 = false
                            else

                                if self:可装备(self.物品[o].物品,tp.抓取物品ID-34) then

                                    客户端:发送数据(o,196,13,self.更新)
                                    self.法宝[tp.抓取物品ID-34].确定 = false
                                end
                            end
                         end
                    tp.抓取物品 = nil
                    tp.抓取物品ID = nil
                    tp.抓取物品注释 = nil
                 end
            end
        end
        tp.字体表.华康字体:置颜色(0xFF19C93D):显示(self.x + 502,self.y + 320,self.法宝数量)
        if self.选中法宝  then
             tp.字体表.华康字体:置颜色(0xFFFFFFFF):显示(self.x + 87,self.y + 159,self.选中法宝.名称)
            tp.字体表.华康字体:置颜色(0xFFFFFFFF):显示(self.x + 87,self.y + 199,self.选中法宝.当前经验.." / "..self.选中法宝.最大经验)
            tp.字体表.华康字体:置颜色(0xFFFFFFFF):显示(self.x + 87,self.y + 239,(self.选中法宝.层数+1)*(self.选中法宝.层数+1)*ItemData[self.选中法宝.名称].等级*tp.场景.人物.数据.等级*1000)
          
        end
    elseif self.更新 =="神器" then
        Picture[self.数据.数据.名称]:显示(self.x+300,self.y+90)
        Picture[self.数据.数据.名称]:更新(dt)
         for i=15,17 do
            self.资源组[i]:更新(x,y)
              self.资源组[i]:显示(self.x+263+(i-15)*90,self.y+315,true,1)
        end 

        if self.资源组[15]:事件判断()  then
          tp.窗口.文本栏:载入("#H/每次消耗1万仙玉，你确定需要更换吗？","洗炼技能",true)
        elseif self.资源组[16]:事件判断() then
            tp.窗口.文本栏:载入("#H/每次消耗1000万银币，你确定需要更换吗？","洗炼属性",true)
         elseif self.资源组[17]:事件判断() then
            tp.窗口.文本栏:载入("#H/消耗1亿银币补满200点灵气，你确定需要补充吗？","补充灵气",true)
        end 

        for i=1,5 do
            tp.字体表.华康字体:置颜色(0xFFFFFFFF):显示(self.x+15,self.y+140+i*25,self.数据.数据.属性[i].类型.."+"..self.数据.数据.属性[i].数额)
        end
        tp.字体表.华康字体:显示(self.x+410,self.y+277,"剩余灵气："..self.数据.数据.灵气)
        tp.字体表.描边字体:显示(self.x+148,self.y+161,self.数据.数据.技能)
        self.介绍文本:显示(self.x+138,self.y+188)
       tp.字体表.华康字体:置颜色(-256):显示(self.x+10,self.y+318,"神器技能分2种可以洗炼,属性也可以洗炼")
    end

    


end
function 场景类_法宝神器:可装备(i1,i2)
if  self.更新 == "法宝" then
    if ItemData[i1.名称].等级==1 and (i2==1 or i2==2)  then
     return true
    elseif ItemData[i1.名称].等级==2 and i2==2 then
     return true
    elseif ItemData[i1.名称].等级==3 and i2==3 then
        return true
    elseif ItemData[i1.名称].等级==4 and i2==4 then
        return true
    else
        local 消息 =(ItemData[i1.名称].等级==1) and "1或者2" or ItemData[i1.名称].等级
        tp.提示:写入("#Y/该法宝等级或者类型不符,应该放入第"..消息.."格子")
        return false
   end
end
    return false
end

return 场景类_法宝神器