-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-29 00:08:41
local 场景类_签到 = class()
local insert = table.insert
local remove = table.remove
local floor = math.floor
local tp,zts
function 场景类_签到:初始化(根)
    self.ID = 312
    self.x = 90+(全局游戏宽度-800)/2
    self.y = 65
    self.xx = 0
    self.yy = 0
    self.注释 = "签到"
    tp = 根

    self.可视 = false
    self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    local 按钮 = 根._按钮
    local 自适应 = 根._自适应
    local  资源= 根.资源
    self.资源组 = {
        [1] = 根.资源:载入("签到.png","加密图片"),
        [2] = 按钮.创建(根.资源:载入('MAX.7z',"网易WDF动画",2),0,0,3),
        [3] = 按钮.创建(根.资源:载入('MAX.7z',"网易WDF动画",205),0,0,3),
        [4] = 根.资源:载入("已签到.png","加密图片"),
        [5] = 根.资源:载入("未签到.png","加密图片"),
    }

    for n=2,3 do
        self.资源组[n]:绑定窗口_(312)
    end

    self.翻页 =false
    self.窗口时间 = 0

    zts = tp.字体表.华康字体

end




function 场景类_签到:打开(内容)
  if self.可视 then

        self.可视 = false


  else
        if  self.x > 全局游戏宽度 then
        self.x = 82+(全局游戏宽度-800)/2
        end

       self.数据=内容
        insert(tp.窗口_,self)
        tp.运行时间 = tp.运行时间 + 1
        self.窗口时间 = tp.运行时间
        self.可视 = true
  end
 end
function 场景类_签到:显示(dt,x,y)
    self.焦点 = false

    self.资源组[1]:显示(self.x,self.y)
    for i=2,3 do
         self.资源组[i]:更新(x,y)
    end
   if self.资源组[2]:事件判断() then
     self:打开()
   elseif self.资源组[3]:事件判断() then
      客户端:发送数据(1, 22,65, "93")
   end
    self.资源组[2]:显示(self.x+603,self.y+2)
    self.资源组[3]:显示(self.x+508,self.y+102)
    zts:显示(self.x+482,self.y+68,os.date("%Y年%m月%d日"))
    zts:显示(self.x+512,self.y+88,os.date("%H:%M:%S"))
    zts:显示(self.x+325,self.y+356,"(当前累计天数):"..self.数据.累计签到)
     for i=1,4 do

        if self.数据[n] then
             self.资源组[4]:显示(self.x+14+(i+2)*62,self.y+75)
        else
             self.资源组[5]:显示(self.x+14+(i+2)*62,self.y+75)
        end
    end
    local jx=0
    local jy=0

    for n=5,31 do
        if  n==12  or n==19 or n==26 then
            jy=jy+1
            jx=0
        end
        if self.数据[n] then
             self.资源组[4]:显示(self.x+14+jx*62,self.y+118+jy*44)
        else
             self.资源组[5]:显示(self.x+14+jx*62,self.y+118+jy*44)
        end

        jx=jx+1
    end

end


function 场景类_签到:检查点(x,y)
    if self.可视 and self.资源组[1]:是否选中(x,y)  then
        return true
    else
        return false
    end
end

function 场景类_签到:初始移动(x,y)
    tp.运行时间 = tp.运行时间 + 1
    if not tp.消息栏焦点 then
        self.窗口时间 = tp.运行时间
    end
    if not self.焦点 then
        tp.移动窗口 = true
    end
    if self.可视 and self.鼠标 and  not self.焦点 then
        self.xx = x - self.x
        self.yy = y - self.y
    end
end

function 场景类_签到:开始移动(x,y)
    if self.可视 and self.鼠标 then
        self.x = x - self.xx
        self.y = y - self.yy
    end
end

return 场景类_签到



