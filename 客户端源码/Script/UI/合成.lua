--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:47
--======================================================================--
local 场景类_合成 = class(窗口逻辑)

local floor = math.floor
local tp,zts
local insert = table.insert
local tos = 引擎.取金钱颜色
function 场景类_合成:初始化(根)
	self.ID = 34
	self.x = 226+(全局游戏宽度-800)/2
	self.y = 110
	self.xx = 0
	self.yy = 0
	self.注释 = "合成"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	self.资源组 = {
		[1] = Picture.合成界面,
		[2] = 按钮.创建(根._自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
		[3] =  根._按钮(根._自适应(15,4,72,20,1),0,0,4,true,true,"  合成"),
	}
	local 格子 = 根._物品格子
	self.物品 = {}
	for i=1,20 do
		self.物品[i] = 格子(0,0,"合成")
	end
	self.材料 = {}
	for i=1,4 do
		self.材料[i] = 格子(0,0,"合成材料")
	end
	for n=2,3 do
		self.资源组[n]:绑定窗口_(34)
	end
	self.材料数量 = 0
	self.选中 =0
	tp = 根
	zts = tp.字体表.华康字体
  self.选中材料= {}
	self.窗口时间 = 0

end
function 场景类_合成:刷新(数据)
		self.数据=数据
		for i=1,20 do
		self.物品[i]:置物品(数据[i])
		end
		for i=1,4 do
		self.材料[i]:置物品(nil)
	   end
	end
function 场景类_合成:打开(数据)
	if self.可视 then
		self.可视 = false
		self.选中 =0
	   self.材料数量 = 0
	   self.选中材料= {}
	else
				  			if  self.x > 全局游戏宽度 then
	self.x = 226+(全局游戏宽度-800)/2
		end
		insert(tp.窗口_,self)
		for i=1,20 do
			self.物品[i]:置物品(数据[i])
		end
		for i=1,4 do
		self.材料[i]:置物品(nil)
	   end
		self.数据=数据
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_合成:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y,self.材料数量 >= 4)
	if self.资源组[2]:事件判断() then
		self:打开()
		return false
	elseif self.资源组[3]:事件判断() then
     客户端:发送数据(1,185,13,self.选中材料[1].."*-*"..self.选中材料[2].."*-*"..self.选中材料[3].."*-*"..self.选中材料[4])
     for i=1,4 do
		self.材料[i]:置物品(nil)
	   end
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x+272,self.y+6)
	self.资源组[3]:显示(self.x+175,self.y+160,true,1,nil,self.材料数量 < 4,3)
	zts:置颜色(引擎.取金钱颜色(self.数据.银子))
    zts:显示(self.x+193,self.y+70,self.数据.银子)
    zts:置颜色(0xFFFFFF00)
    zts:显示(self.x+193,self.y+127,self.数据.体力)
    zts:置颜色(0xFFFFFFFF)
	local is = 0
	for h=1,4 do
		for l=1,5 do
			is = is + 1
			self.物品[is]:置坐标(l * 51 - 30 + self.x,h * 50 + self.y + 139)
			self.物品[is]:显示(dt,x,y,self.鼠标,"符石")
			if self.物品[is].物品 ~= nil and self.物品[is].焦点 then
				tp.提示:道具行囊(x,y,self.物品[is].物品)
				if self.物品[is].事件 and self.鼠标 then
               		if self.物品[is].物品.类型 == "符石" or self.物品[is].物品.名称 == "符石卷轴" then
					  if self.材料数量 < 4 then
							for i=1,4 do
								if self.材料[i].物品 == nil then
									self.选中 =is
				               		self.材料[i]:置物品(self.物品[is].物品)
									self.物品[is]:置物品(nil)
									self.材料数量 = self.材料数量 + 1
									self.选中材料[i]=is
									break
								end
							end
					   else
		                    tp.提示:写入("#Y/放入的物品太多了，不允许再放入了！")
		               end
		           else
		              tp.提示:写入("#Y/只有符石和符石卷轴才能合成！")
		           end
				end
			end
		end
	end
	is = 0
	for h=1,2 do
		for l=1,2 do
			is = is + 1
			self.材料[is]:置坐标(l * 56 - 43 + self.x,h * 55 + self.y + 8)
			self.材料[is]:显示(dt,x,y,self.鼠标)
			if self.材料[is].物品 ~= nil and self.材料[is].焦点 then
				tp.提示:道具行囊(x,y,self.材料[is].物品)
				if self.材料[is].事件 and self.鼠标 then
					self.材料数量 = self.材料数量 - 1
                    self.物品[self.选中]:置物品(self.数据[self.选中])
					self.材料[is]:置物品(nil)
				end
			end
		end
	end
	if self.材料数量 >= 4 then
	zts:置颜色(引擎.取金钱颜色(500000))
	zts:显示(self.x + 193,self.y + 41,500000)
	zts:置颜色(0xFFFFFF00)
	zts:显示(self.x + 193,self.y + 97,30)
	zts:置颜色(0xFFFFFFFF)
	end
end



return 场景类_合成

