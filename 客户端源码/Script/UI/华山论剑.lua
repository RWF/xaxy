--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:46
--======================================================================--
local 场景类_抽奖 = class()
local tp,zts,zts1
local floor = math.floor
local tonumber = tonumber
local tostring = tostring
local insert = table.insert
	local sl = 0
	local js= 1
	local sjs =0
function 场景类_抽奖:初始化(根)
	self.ID = 127
	self.x = 220+(全局游戏宽度-800)/2
	self.y = 160
	self.xx = 0
	self.yy = 0
	self.注释 = "抽奖"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	local 资源 = 根.资源
	self.资源组 = {
	    [0] = 资源:载入('JM.dll',"网易WDF动画",0x1732C1EF), --背景
	  --  [1] = 资源:载入('JM.dll',"网易WDF动画",0x65C595A9),
	   -- [2] = 资源:载入('JM.dll',"网易WDF动画",0x28A3B224),
		[3] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true), --关闭
       	[4] = 按钮.创建(自适应.创建(12,4,55,22,1,3),0,0,4,true,true,"备战"),
		[5] = 按钮.创建(自适应.创建(12,4,55,22,1,3),0,0,4,true,true,"取消"),
	}
	

	for i=3,5 do
		self.资源组[i]:绑定窗口_(127)
	end

  self.介绍文本 = 根._丰富文本(self.x+12,self.y+29,根.字体表.普通字体)
  self.介绍文本:添加文本("#W/点击#Y/“备战”#W/进入备战状态，点击#Y/“取消”#W/结束备战，#Y/备战时间最慢为2分钟#W/轮空将直接进入下一轮！(#R胜利奖励积分，失败则无积分奖励#Y/)")
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.描边字体
end

function 场景类_抽奖:打开(数据,类型)
	if self.可视 then
		self.可视 = false
		local 匹配 =0
		   self.进程 = 1
		 	客户端:发送数据(匹配,248,13)

	else
	if  self.x > 全局游戏宽度 then
      self.x = 220+(全局游戏宽度-800)/2
    end
		self.进程 = 1
		self.倒计时 = 1
		self.起始 = 0
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_抽奖:显示(dt,x,y)
	self.焦点 = false
   	self.资源组[0]:显示(self.x+0,self.y+0)
	self.资源组[3]:显示(self.x+300,self.y+7)
	self.资源组[4]:显示(self.x+20,self.y+110)
	self.资源组[5]:显示(self.x+90,self.y+110)
	self.资源组[3]:更新(x,y)
	self.资源组[4]:更新(x,y)
	self.资源组[5]:更新(x,y)
	self.介绍文本:显示(self.x+12,self.y+29)
	
	
     if self.进程 == 2 then
	 self.倒计时 = 120- (os.time()-self.起始)
	 tp.字体表.普通字体:置颜色(0xFFFFFFFF):显示(self.x+165,self.y+114,"(备战中 "..self.倒计时.." 秒)")
	  if self.倒计时 == 0 then 
	    local 匹配 =0
		客户端:发送数据(匹配,248,13,"11")
	    self.进程 = 1
		
	  end
	 
     end	 
   
	if self.鼠标 then
		if self.资源组[3]:事件判断() then
		 
			self:打开()
    	elseif self.资源组[4]:事件判断() then
		     if self.进程 ~= 2 then
			 self.起始 = os.time()
			 end
			 local 匹配 =1
			 客户端:发送数据(匹配,248,13) ---内容.参数，内容.序号，序号，内容.文本
			 

		 elseif self.资源组[5]:事件判断() then
		   local 匹配 =0
		   self.进程 = 1
		 	客户端:发送数据(匹配,248,13)
			

		end
	end
end





function 场景类_抽奖:检查点(x,y)
	if self.资源组[0]:是否选中(x,y)  then
		return true
	end
end

function 场景类_抽奖:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_抽奖:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_抽奖