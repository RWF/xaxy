--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:47
--======================================================================--
local 场景类_对话栏 = class()
local bw = require("gge包围盒")
local floor = math.floor
local ceil = math.ceil
local insert = table.insert
local remove = table.remove
local format = string.format
local random = 引擎.取随机整数
local tp
local max = math.max

local sort = table.sort
local mousea = 引擎.鼠标按住
local mouseb = 引擎.鼠标弹起
local insert = insert
local qbb = 引擎.取宝宝
local zts
function 场景类_对话栏:初始化(根)
	self.ID = 2
	self.x = 120+(全局游戏宽度-800)/2
	self.y = 260
	self.xx = 0
	self.yy = 0
	self.注释 = "对话栏"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	self.下一页 = {}
	tp = 根
	local 资源 = tp.资源
	local 自适应 = tp._自适应
	self.资源组 = {
		[1] = 资源:载入('JM.dll',"网易WDF动画",0x260BE57C),
		--[2] = ,


		[2] = 自适应.创建(28,1,40,28,1,3,nil,18),
		[3] =tp._按钮(资源:载入('对话关闭',"动画"),0,0,4)
	}
	--self.背景窗口 = tp._自适应.创建(0,1,538,189,3,9)
	self.背景窗口 =资源:载入('MAX.7z',"网易WDF动画","对话框")
    

	self.头像 = ""
	self.名称 = ""
	self.头像宽度 = 0
	self.头像高度 = 0
	self.文本高度 = 0
	self.选项 = {}
	self.记录文本 = {}
	self.丰富文本 = 根._丰富文本(570,287)
	for n=0,119 do
		self.丰富文本:添加元素(n,根.包子表情动画[n])
	end
	self.窗口时间 = 0
	self.坐标 = 0
	self.换向=true
	self.事件 = nil
	self.接触按钮 = false
	self.缓冲时间 = 0 -- 都有缓冲时间，避免重复点击
	self.模型头像 = ""
	zts = tp.字体表.华康字体
end

function 场景类_对话栏:打开()
	if self.可视 then
		self.丰富文本:清空()
		self.选项 = {}
		self.记录文本 = {}
		self.可视 = false
		self.换向 =false
		self.模型头像 = nil
		self.第二列 = nil
		self.第三列 = nil
		self.第四列 = nil
		self.第五列 = nil
		self.文本内容 = nil
		tp.场景.事件选中 = false
	else
		if  self.x > 全局游戏宽度 then
		self.x = 160+(全局游戏宽度-800)/2
		end
		self.可视 = true
	    tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	end
end

function 场景类_对话栏:显示(dt,x,y)
	if self.缓冲时间 > 0  then
	  	self.缓冲时间 = self.缓冲时间 - 0.2
	end
	self.接触按钮 = false
	if self.头像 ~= "" then
		-- local v = self.头像宽度
		-- if v > 50 then
		-- 	v = self.x + 13
		-- end
		 self.头像:显示(self.x+50+self.头像.信息组[0].Key_Y,self.y-self.头像高度+2,1)
	end
	if self.名称 ~= nil and self.名称 ~= "" then
		self.资源组[1]:显示(self.x,self.y-27)
		zts:置颜色(4294967295):显示(self.x+10 + (78 - #self.名称*3.72),self.y-20,self.名称)
	end
	self.背景窗口:显示(self.x,self.y)
	self.资源组[3]:更新(x,y)
	self.资源组[3]:显示(self.x+self.背景窗口.宽度-20,self.y+3)
	self.丰富文本:显示(self.x+17,self.y+17)
	self:选项解析(x,y)
	self.焦点 = false
	if self.鼠标 then
		if not self.接触按钮 and not (self.头像 ~= "" and self.头像:是否选中(x,y)) and self.缓冲时间 <= 0 then
			if mouseb(0) then

					if self.选项 == nil or (self.选项 ~= nil and self.选项[1] == nil) then
						if self.事件 == "车迟国" then
							if 1~=2 then--tp.剧情开关.副本[2] == 2 then
							--	tp.剧情开关.副本[2] = 3
								tp.假人库:生成副本Npc()
								tp.窗口.任务栏:删除("副本任务")
								tp.窗口.任务栏:添加("副本任务",format("“灭佛兴道”之气不能忍，吃光场内所有供品。\n#L/完成度：%d/10",tp.剧情开关.副本[4][3]))
							elseif tp.剧情开关.副本[2] == 4 then
							--	tp.剧情开关.副本[2] = 5
								tp.场景:传送至(1137,41,35)
								tp.假人库:生成副本Npc()
								tp.窗口.任务栏:删除("副本任务")
								tp.窗口.任务栏:添加("副本任务",format("这三座道士像，定是那妖道所砌！为解心头之气，只好冒犯神像了！\n#L/完成度：%d/3",tp.剧情开关.副本[4][4]))
							elseif tp.剧情开关.副本[2] == 5 then
							--	tp.剧情开关.副本[2] = 6
								tp.场景:传送至(1111,31,149)
								tp.假人库:生成副本Npc()
								tp.窗口.任务栏:删除("副本任务")
								tp.窗口.任务栏:添加("副本任务","前方似乎有个神仙，问问他关于妖道的消息")
							elseif tp.剧情开关.副本[2] == 6 then
							--	tp.剧情开关.副本[4][5] = 1
							--	tp.剧情开关.副本[4][6] = 0
								tp.假人库:生成副本Npc()
								tp.窗口.任务栏:删除("副本任务")
								tp.窗口.任务栏:添加("副本任务",format("火速阻止风婆、雨师、雷公的施法\n#L/完成度：%d/3",tp.剧情开关.副本[4][6]))
							end
						elseif self.事件 ~= nil and type(self.事件) =="table" then
							for i=1,#self.事件 do
								loadstring(self.事件[i])()
							end
							for i=1,#(self.结束事件 or {}) do
								loadstring(self.结束事件[i])()
							end
						end
						if self.结束事件 == nil then
							self:打开()
						else
							self.结束事件 = nil
						end
					end
					return false
			end
			if mouseb(1) then
				self:打开()
			end
		end
	end
end

function 场景类_对话栏:选项解析(x,y)
	if #self.选项 > 0 then
		for n=1, #self.选项 do
			local xx = 0
			local yy = 0
			if n<=5 then
				self.选项[n].选中判断:置坐标(self.x+19,self.y + self.文本高度+((n-1) * 22) + 5)
			elseif n <= 10  and self.第二列 then
					self.选项[n].选中判断:置坐标(self.x+ self.第二列,self.y + self.文本高度+((n-6) * 22))
			elseif n <= 15 and self.第三列 then
				self.选项[n].选中判断:置坐标(self.x+self.第三列,self.y + self.文本高度+((n-11) * 22))
			elseif n <= 20 and self.第四列 then
				self.选项[n].选中判断:置坐标(self.x+self.第四列 ,self.y + self.文本高度+((n-16) * 22))
			elseif n <= 25 and self.第五列 then
				self.选项[n].选中判断:置坐标(self.x+self.第五列 ,self.y + self.文本高度+((n-21) * 22))
			elseif n>25 and self.第五列 then
				self.下一页.选中判断:置坐标(self.x+self.第五列+100,self.y + self.文本高度+(4 * 22))
			end
			
			if self.鼠标 and self.选项[n].选中判断:检查点(x,y) and not self.焦点 then
				self.接触按钮 = true
				self.焦点 = true
				if mousea(0) then
					xx = 1
					yy = 1
				end
				self.资源组[2]:置宽高(zts:取宽度(self.选项[n].基本内容)+12,28)
				self.资源组[2]:显示(self.选项[n].选中判断.x-7+xx,self.选项[n].选中判断.y-6+yy)
				if mouseb(0) then
					if self.选项[n].跳转链接 ~= nil then
						local 当前地图 = tp.场景.数据.编号
						if 日常任务(self.事件)  or self.事件=="门派闯关开始" or self.事件=="门派首席"  or self.事件== "摇钱树"or self.事件== "梦幻瓜子"then
							客户端:发送数据(self.NPC编号,9,6,self.选项[n].跳转链接)
						elseif self.事件 == "车迟1" then	
							客户端:发送数据(self.NPC编号,8,6,self.选项[n].跳转链接)
					   elseif self.事件 == "设置孩子成长方向" then	
							客户端:发送数据(1,11,6,self.选项[n].跳转链接)
						elseif self.事件 == "兑换神兽" then	
							客户端:发送数据(1,14,6,self.选项[n].跳转链接)
						elseif self.事件 == "召唤兽改造型" then	
							客户端:发送数据(1,17,6,self.选项[n].跳转链接)
						elseif self.事件 == "召唤兽改造型2" then
							客户端:发送数据(1,18,6,self.选项[n].跳转链接)
						elseif self.事件 == "召唤兽改造型3" then
							客户端:发送数据(1,19,6,self.选项[n].跳转链接)	
					    elseif self.事件 == "重置孩子属性" then	
							客户端:发送数据(1,12,6,self.选项[n].跳转链接)
						elseif self.事件 == "门派师傅" then
							客户端:发送数据(self.NPC编号,6,6,self.选项[n].跳转链接)
						elseif self.名称=="月光宝盒" then
							客户端:发送数据(6,208,13,self.选项[n].跳转链接)
						elseif self.事件 == "门派传送" then
							客户端:发送数据(1,5,6,self.选项[n].跳转链接)
						elseif self.事件== "刷新" then
							客户端:发送数据(self.NPC编号,4,6,self.选项[n].跳转链接)
						else
                            客户端:发送数据(2,3,6,self.选项[n].跳转链接,self.名称)  --事件解析
						end
						self.可视 = false
					    self.换向 =false
						break
					end
				end
	        end
	        if self.选项[n].基本内容 == "红尘试炼" then
	        	zts:置颜色(0xFFFFFF00)
	        else
	        	zts:置颜色(-65536)
	        end
			if n<=5 then
				zts:显示(self.x+19 + xx,self.y  + yy + self.文本高度+((n-1) * 22) + 5,self.选项[n].基本内容)
			elseif n <= 10 and self.第二列 then
					zts:显示(self.x+ self.第二列+ xx,self.y  + yy + self.文本高度+((n-6) * 22),self.选项[n].基本内容)
			elseif n <= 15 and self.第三列 then
				zts:显示(self.x+self.第三列 + xx,self.y  + yy + self.文本高度+((n-11) * 22),self.选项[n].基本内容)
			elseif n <= 20 and self.第四列 then
				zts:显示(self.x+self.第四列 + xx,self.y  + yy + self.文本高度+((n-16) * 22),self.选项[n].基本内容)
			elseif n <= 25 and self.第五列 then
				zts:显示(self.x+self.第五列 + xx,self.y  + yy + self.文本高度+((n-21) * 22),self.选项[n].基本内容)
			elseif n>25 and self.第五列 then
				zts:显示(self.x+self.第五列 +100+ xx,self.y  + yy + self.文本高度+(4 * 22),self.下一页.基本内容)
			end
		end
		if self.鼠标 and self.下一页.选中判断:检查点(x,y) and not self.焦点 then
			    local xx = 0
			    local yy = 0
				self.接触按钮 = true
				self.焦点 = true
				if mousea(0) then
					xx = 1
					yy = 1
				end
				local length = #self.选项
				if length > 25 and self.第五列 then
				   self.资源组[2]:置宽高(zts:取宽度(self.下一页.基本内容)+12,28)
				   self.资源组[2]:显示(self.下一页.选中判断.x-7+xx,self.下一页.选中判断.y-6+yy)
			    
				if mouseb(0) then
					for n=1,25 do
                        table.remove(self.选项,1)
                    end
				end
			    end
			end
	end
end

local 大小排序 = function(a,b)
	return a < b
end

function 场景类_对话栏:文本(头像,名称,内容,选项,事件,NPC编号)
	for i=1,#tp.窗口_ do
		if tp.窗口_[i] == self then
			remove(tp.窗口_,i)
			break
		end
	end
	insert(tp.窗口_,self)
	self.第二列 = nil
	self.第三列 = nil
	self.第四列 = nil
	self.第五列 = nil
	if 内容 then
	    self.文本内容 = 内容
	else
		self.文本内容="此NPC未加入对话"
	end
	self.事件 = 事件
	self.NPC编号 = NPC编号
	if not self.可视 then
		self:打开()
		self.选项 = {}
		self.名称 = 名称
		self.丰富文本:清空()
	

		local ab = self.丰富文本:添加文本(self.文本内容)

		if  ModelData[头像] and ModelData[头像].大头像  then
			self.头像 = tp.资源:载入( ModelData[头像].头像资源,"网易WDF动画",ModelData[头像].大头像)
			self.头像高度 = self.头像.高度 + 取y偏移(头像)
			self.模型头像 = 头像
		else
			self.头像 = ""
			self.模型头像 = ""
		end
		self.文本高度 = ab+23
		if 选项 ~= nil and 选项 ~= "" and 选项[1] ~= nil then
			local jc = nil
			if #选项 > 5 then
				jc = {}
			end
			for i=1,#选项 do
				if jc ~= nil then
					insert(jc,zts:取宽度(选项[i]))
					if #jc >= 5 then
						sort(jc,大小排序)
						if self.第二列 == nil then
							self.第二列 = jc[#jc] + 24 + 35
						elseif self.第三列 == nil then
							self.第三列 = self.第二列 + jc[#jc] + 35
						elseif self.第四列 == nil then
							self.第四列 = self.第三列 + jc[#jc] + 35
						elseif self.第五列 == nil then
							self.第五列 = self.第四列 + jc[#jc] + 35
						end
						jc = {}
					end
				end
				self.选项[i] = {
					基本内容 = 选项[i],
					跳转链接 = 选项[i],
					选中判断 = bw(0,0,zts:取宽度(选项[i]),14)
				}

			end
			self.下一页 = {
	               基本内容 = "下一页",
	               选中判断 = bw(0,0,zts:取宽度("下一页"),14)}
		end
		self.缓冲时间 = 2
	else
		local 记录 = {
			头像_ = 头像,
			名称_ = 名称,
			内容_ = 内容,
			选项_ = 选项,
			事件_ = 事件,
	
		}
		insert(self.记录文本,记录)
	end
end



function 场景类_对话栏:检查点(x,y)
	if self.可视 and (self.背景窗口:是否选中(x,y)  or (self.头像 ~= nil and self.头像 ~= "" and self.头像:是否选中(x,y))) then
		return true
	else
		return false
	end
end

function 场景类_对话栏:初始移动()
	tp.运行时间 = tp.运行时间 + 1
  	self.窗口时间 = tp.运行时间
end

function 场景类_对话栏:开始移动()
end
function 场景类_对话栏:傲来国渔夫(页数)
	local nc = {}
	nc[1] = {"对虾","河虾","泥鳅","沙丁鱼"}
	nc[2] = {"草鱼","大黄鱼","河蟹","毛蟹","小黄鱼","鲫鱼"}
	nc[3] = {"大闸蟹","海马","海星","河豚","甲鱼","金枪鱼","鲤鱼","娃娃鱼"}
	local dh = false
	local nb = 0
	local nms = 0
	local nm = 0
	if 页数 == "随机二级鱼类" then
		-- for i=1,#nc[1] do
		-- 	if 物品判断(nc[1][i],10,true) then
		-- 		nb = 2
		-- 		nm = i
		-- 		dh = true
		-- 		break
		-- 	end
		-- end
		if dh then
			local item = nc[nb][引擎.取随机整数(1,#nc[nb])]
			--增加物品(item)
			引擎.场景.窗口.对话栏:文本("捕鱼人","渔夫","消耗10只"..nc[1][nm].."兑换了一只"..item)
		else
			引擎.场景.窗口.对话栏:文本("捕鱼人","渔夫","必需要有任意10只一级鱼类才可以兑换，可是你身上没有我要的东西。")
		end
	elseif 页数 == "随机三级鱼类" then
		-- for i=1,#nc[2] do
		-- 	if 物品判断(nc[2][i],20,true) then
		-- 		nb = 3
		-- 		nm = i
		-- 		dh = true
		-- 		break
		-- 	end
		-- end
		if dh then
			local item = nc[nb][引擎.取随机整数(1,#nc[nb])]
			增加物品(item)
			引擎.场景.窗口.对话栏:文本("捕鱼人","渔夫","消耗20只"..nc[2][nm].."兑换了一只"..item)
		else
			引擎.场景.窗口.对话栏:文本("捕鱼人","渔夫","必需要有任意20只二级鱼类才可以兑换，可是你身上没有我要的东西。")
		end
	elseif 页数 == "随机属性人参果" then
		引擎.场景.窗口.对话栏:文本("捕鱼人","渔夫","还没有开放兑换该物品")
	elseif 页数 == "祥瑞腾蛇" then
		-- for i=1,#nc[3] do
		-- 	if 物品判断(nc[3][i],100,true) then
		-- 		nm = i
		-- 		dh = true
		-- 		break
		-- 	end
		-- end
		if dh then
			-- 引擎.场景:获取宝宝("祥瑞腾蛇")
			引擎.场景.窗口.对话栏:文本("捕鱼人","渔夫","消耗100只"..nc[3][nm].."兑换了一只祥瑞腾蛇")
		else
			引擎.场景.窗口.对话栏:文本("捕鱼人","渔夫","必需要有任意100只三级鱼类才可以兑换，可是你身上没有我要的东西。")
		end
	end
end

return 场景类_对话栏