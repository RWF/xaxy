--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:50
--======================================================================--
local 场景类_首服奖励 = class(窗口逻辑)
local tp,zts
local insert = table.insert

function 场景类_首服奖励:初始化(根)
    self.ID = 333
    self.x = 204+(全局游戏宽度-800)/2
    self.y = 10
    self.xx = 0
    self.yy = 0
    self.注释 = "文本栏"
    self.可视 = false
    self.鼠标 = false
    self.焦点 = false
    self.可移动 = true
    tp = 根

    self.资源组 = {
        [1] = tp._自适应(99,1,350+100,171+360,3,9),

        [2] = tp._按钮(tp._自适应(101,4,72,22,1,3),0,0,4,true,true," 上一页"),
        [3] = tp._按钮(tp._自适应(101,4,72,22,1,3),0,0,4,true,true," 下一页"),
        [4] = tp._按钮(tp._自适应(104,4,16,16,4,3),0,0,4,true,true),

        [5] =  tp._自适应(9,1,410,75,3,9),
        [6] =  tp._自适应(9,1,300,40,3,9),
        [7] = tp.资源:载入('JM.dll',"网易WDF动画",0x2e0ba3e2),
        [8] = tp._自适应(70,1,330+100,95+360,3,9),
        [9] = tp.资源:载入('JM.dll',"网易WDF动画",0xE6490543),
        [10] = tp.资源:载入('JM.dll',"网易WDF动画",0x22D22D6D), --22D22D6D   7FFB866A
        
    }
    self.resour={}
    for n=2,4 do
       self.资源组[n]:绑定窗口_(333)
       
    end
    self.窗口时间 = 0
    self.加入=0
    zts =根.字体表.华康字体
end

function 场景类_首服奖励:打开(内容)
    if  self.可视 then
        self.resour={}
         self.加入=0
        self.可视 = false
        self.data=nil
    else
        self.x = 245+(全局游戏宽度-800)/2
        insert(tp.窗口_,self)

        local xxx=1
        for k,v in pairs(Reward) do


            self.resour[xxx]={
            Icon= tp.资源:载入(ItemData[v.名称].文件 ,"网易WDF动画",ItemData[v.名称].小图标),
            explain=v.介绍,
            Name =k,
            偏移 =0
           }
             if 内容[k] then
                self.resour[xxx].Icon=  tp.资源:载入(ModelData[内容[k][3]].头像资源,"网易WDF动画",ModelData[内容[k][3]].中头像)
                self.resour[xxx].偏移=3

             end 
        xxx=xxx+1
        end

        self.data = 内容
       
        tp.运行时间 = tp.运行时间 + 3
        self.窗口时间 = tp.运行时间
        self.可视 = true
    end
end

function 场景类_首服奖励:显示(dt,x,y)
    self.焦点 = false
    self.资源组[3]:更新(x,y,self.加入+5 < #self.resour)
    self.资源组[2]:更新(x,y,self.加入 > 0)
    self.资源组[10]:更新(dt)
     self.资源组[4]:更新(x,y)
    if  self.资源组[4]:事件判断() then
            self:打开(内容)
    elseif  self.资源组[2]:事件判断() then
        self.加入= self.加入-5
    elseif  self.资源组[3]:事件判断() then -- +

         self.加入= self.加入+5
    end
    self.资源组[1]:显示(self.x,self.y)
    self.资源组[8]:显示(self.x+10,self.y+30)
    Picture.标题:显示(self.x+self.资源组[1].宽度/2-90,self.y)
    zts:置颜色(0xFFFFFFFF):显示(self.x+self.资源组[1].宽度/2-30,self.y+3,"首服奖励")
    self.资源组[4]:显示(self.x+420,self.y+6,true)
    self.资源组[2]:显示(self.x + 75,self.y + 500,true,1)
    self.资源组[3]:显示(self.x + 275,self.y + 500,true,1)

            local yy = 0
            local jx = self.x+20
            local jy = self.y+40
            for n=1,5 do
                if self.resour[n+self.加入] then
                    self.资源组[5]:显示(jx,jy+yy*90)

                    self.资源组[6]:显示(jx+105,jy+5+yy*90)
                    self.资源组[9]:显示(jx+15,jy+14+yy*90)

                    if  self.resour[n+self.加入].Icon then
                    self.resour[n+self.加入].Icon:显示(jx+15+self.resour[n+self.加入].偏移,jy+14+yy*90+self.resour[n+self.加入].偏移)
                       if self.resour[n+self.加入].Icon:是否选中(x,y) and self.鼠标 and not tp.消息栏焦点  then 
                          tp.提示:首服奖励(x,y,Reward[self.resour[n+self.加入].Name].奖励)
                       end
                    end
                    zts:置颜色(0xFF000000):显示(jx+110,jy+51+yy*90,self.resour[n+self.加入].explain)
                    if self.data[self.resour[n+self.加入].Name] then
                        local msg =self.data[self.resour[n+self.加入].Name]

                        zts:置颜色(0xFFFFFFFF):显示(jx+110,jy+17+yy*90,string.format("名称：%s ID：%s 时间：%s",msg[2],msg[1],os.date("%m月%d日",msg[5]) ))
                        self.资源组[7]:显示(jx+335,jy+14+yy*90)
                        self.资源组[10]:显示(jx+11,jy+10+yy*90)

                    else 
                    zts:置颜色(0xFFFFFFFF):显示(jx+110,jy+17+yy*90,"当前奖励还未有人达成")
                    end
                    yy = yy + 1
                end


            
            end

    
end
return 场景类_首服奖励