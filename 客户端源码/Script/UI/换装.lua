--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:48
--======================================================================--
local 场景类_换装 = class(窗口逻辑)

local floor = math.floor
local tp,zts1
local insert = table.insert


function 场景类_换装:初始化(根)
	self.ID = 36
	self.xx = 0
	self.x= 312
	self.y=1
	self.yy = 0
	self.注释 = "换装"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] =资源:载入('JM.dll',"网易WDF动画",0x8855CB43),
		[2] = 按钮(资源:载入('JM.dll',"网易WDF动画",0xF11233BB),0,0,4,true,true),

	}
	self.资源组[2]:绑定窗口_(36)
	self.物品 = {}
	local 格子 = 根._物品格子
	-- for i=31,35 do
	-- 	self.物品[i] = 格子(0,0,i,"道具行囊_换装",根.底图)
	-- end
	tp = 根
	zts1 = tp.字体表.描边字体
	self.窗口时间 = 0
end

function 场景类_换装:打开(数据)
	if self.可视 then
		-- for i=31,35 do
		-- 	self.物品[i]:置物品(nil)
		-- end
		self.可视 = false
	else
		insert(tp.窗口_,self)
		self.数据 = 数据
		-- for i=31,35 do
		-- 	if self.数据[i] ~= nil and self.数据[i] ~= 0 then
		-- 		self.物品[i]:置物品(self.数据[i])
		-- 	end
		-- end
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end



function 场景类_换装:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	if self.资源组[2]:事件判断() then
		self:打开()
		return false
	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x+116,self.y+14)

	zts1:显示(self.x+53,self.y+13,"换 装")



end




return 场景类_换装