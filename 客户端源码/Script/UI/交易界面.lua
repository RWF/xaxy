--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:46
--======================================================================--
local 交易界面 = class()
local bw = require("gge包围盒")(0,0,173,20)
local box = 引擎.画矩形
local insert = table.insert
local tp,zts1,zts
local mouseb = 引擎.鼠标弹起
function 交易界面:初始化(根)
	self.ID = 55
	self.x = 150
	self.y = 70
	self.xx = 0
	self.yy = 0
	self.注释 = "玩家给与"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.焦点1 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = Picture.交易物品界面,
		[2] = 按钮.创建(自适应.创建(103,4,72,22,1,3),0,0,4,true,true," 交 易"),
		[3] = 按钮.创建(自适应.创建(103,4,72,22,1,3),0,0,4,true,true," 取 消"),
		[4] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
		[5] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFF205590),0,0,1,true,true),
		[6] = 按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFF205590),0,0,1,true,true),
		[7] = 按钮.创建(自适应.创建(103,4,72,22,1,3),0,0,4,true,true," 物 品"),
		[8] = 按钮.创建(自适应.创建(103,4,72,22,1,3),0,0,4,true,true," 召唤兽"),
		[9] =Picture.召唤兽交易界面
	 }


	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('创建控件')
	总控件:置可视(true,true)
	self.输入框 = 总控件:创建输入("银两给予",self.x+128,self.y+38,80,14,根)
	self.输入框:置限制字数(8)
	self.输入框:置数字模式()
    self.输入框:置光标颜色(0xFFFF0000)
	self.数量输入 ={}
    self.给予物品={}
    self.物品数据={}
    self.对方物品={}
    self.选中编号={}
    self.选中召唤兽={}
	local 格子 = 根._物品格子
	for i=1,20 do
	  self.物品数据[i] = 格子.创建(0,0,i,"给予物品")
	end
	for i=1,3 do
		self.数量输入[i] = 总控件:创建输入("数量输入"..i,self.x+i*54-20,self.y+127,25,14,根)
		self.数量输入[i]:置可视(false,false)
		self.数量输入[i]:置限制字数(3)
		self.数量输入[i]:置数字模式()
		self.数量输入[i]:置文字颜色(-16777216)
		self.给予物品[i] = 格子.创建(0,0,i,"选中物品")
		self.对方物品[i] = 格子.创建(0,0,i,"对方物品")
	    self.选中编号[i] = 0
		self.选中召唤兽[i] = 0
	end
	self.窗口时间 = 0
	tp = 根
	zts = 根.字体表.普通字体
	zts1 = 根.字体表.描边字体
	zts1:置颜色(0xFFFFFFFF)
  self.状态=1

 self.交易数据={}
 self.锁定 = false
self.自我锁定 = false
end
function 交易界面:刷新道具(数据)
	self.本类数据.道具=数据.道具

	self.状态=1
end
function 交易界面:刷新召唤兽(数据)
 self.召唤兽数据=数据
	self.状态=9
end
function 交易界面:打开(数据)
	if self.可视 then
		客户端:发送数据(self.本类数据.id, 6, 35, "P7", 1)
		self.可视 = false
		self.状态=1
		self.输入框:置焦点(false)
		self.输入框:置可视(false,false)
		self.输入框:置文本(0)
		self.锁定 = false
		self.自我锁定 = false
		for i=1,3 do
		self.数量输入[i]:置焦点(false)
		self.数量输入[i]:置可视(false,false)
		self.数量输入[i]:置文本(1)
		self.给予物品[i]:置物品(nil)
		self.对方物品[i]:置物品(nil)
		self.选中编号[i] = 0
		self.选中召唤兽[i] = 0
		end
		for i=1,20 do
			self.物品数据[i]:置物品(nil)
		end
	else
		if  self.x > 全局游戏宽度 then
		self.x = 150
		end

		self.状态=1
		self.输入框:置焦点(false)
		self.输入框:置可视(false,false)
		self.输入框:置文本(0)
		self.锁定 = false
		self.自我锁定 = false
		for i=1,3 do
		self.数量输入[i]:置焦点(false)
		self.数量输入[i]:置可视(false,false)
		self.数量输入[i]:置文本(1)
		self.给予物品[i]:置物品(nil)
		self.对方物品[i]:置物品(nil)
		self.选中编号[i] = 0
		self.选中召唤兽[i] = 0
		end
        self.本类数据=数据

        self.本类数据.id=self.本类数据.id+0
		for i=1,20 do
			if self.本类数据.道具[i]~=nil then
			self.物品数据[i]:置物品(self.本类数据.道具[i])
		    end
         end
		insert(tp.窗口_,self)
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
		self.可视 = true
	end
end
function 交易界面:刷新交易数据(数据)

    self.交易数据=数据
    for i=1,3 do
    self.对方物品[i]:置物品(self.交易数据.道具[i])
    end
	self.锁定 = true
end



function 交易界面:显示(dt,x,y)

	self.焦点 = false
	self.焦点1 = nil
	self.资源组[self.状态]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x+260,self.y+322,true,1)
	self.资源组[2]:更新(x,y)
	self.资源组[3]:显示(self.x+370,self.y+322,true,1)
	self.资源组[3]:更新(x,y)
	self.资源组[4]:显示(self.x+476,self.y+5)
	self.资源组[4]:更新(x,y)
	self.资源组[5]:更新(x,y)
	--self.资源组[6]:更新(x,y)
	self.资源组[7]:显示(self.x+260,self.y+40,true,1,nil,self.状态==1,2)
    self.资源组[7]:更新(x,y)
	self.资源组[8]:显示(self.x+370,self.y+40,true,1,nil,self.状态==9,2)
	self.资源组[8]:更新(x,y)
	self.资源组[6]:置打勾框(self.锁定)
	self.资源组[5]:置打勾框(self.自我锁定)
	if self.资源组[4]:事件判断() or self.资源组[3]:事件判断() then
		self:打开()
	elseif self.资源组[2]:事件判断() then
		客户端:发送数据(self.本类数据.id, 6, 34, "P7", 1)

	elseif self.资源组[5]:事件判断() then
		-- self.发送信息={id=self.本类数据.id,银两=self.输入框:取文本()+0,

		-- 道具=self.选中编号,召唤兽=self.选中召唤兽}

		-- 	self.文本信息 = self.文本信息 .. "@-@"

   self.文本信息 = ""

	for n = 1, #self.选中编号, 1 do
		self.文本信息 = self.文本信息 .. self.选中编号[n] .. "*-*"
	end

	self.文本信息 = self.文本信息 .. "@-@"

	for n = 1, #self.选中召唤兽, 1 do
		self.文本信息 = self.文本信息 .. self.选中召唤兽[n] .. "*-*"
	end

	self.文本信息 = self.文本信息 .. "@-@"


		客户端:发送数据(self.本类数据.id, self.输入框:取文本()+0, 33, self.文本信息, 7)
		self.自我锁定 =true
	elseif self.资源组[7]:事件判断() then
		客户端:发送数据(self.本类数据.id, 14, 31, "P7", 1)
	elseif self.资源组[8]:事件判断() then
		客户端:发送数据(self.本类数据.id, 6, 32, "P7", 1)
	end
	if self.状态==1 then
		local xx = 0
		local yy = 0
		for i=1,20 do
		self.物品数据[i]:置坐标(self.x + xx * 51 + 231,self.y + yy * 51 + 85)
		self.物品数据[i]:显示(dt,x,y,self.鼠标)
		if self.物品数据[i].物品 ~= nil and self.物品数据[i].焦点 then
			tp.提示:道具行囊(x,y,self.物品数据[i].物品,true)
			if self.物品数据[i].事件 and self.自我锁定 ==false then
				for o=1,3 do
				  if self.给予物品[o].物品 == nil  then
				  self.给予物品[o]:置物品(self.本类数据.道具[i])
				  self.物品数据[i]:置物品(nil)
				  self.数量输入[o]:置可视(true,true)
				  self.选中编号[o] = i
					break
				  end
				end
		     end
		end
		xx = xx + 1
		if xx == 5 then
			xx = 0
			yy = yy + 1
		end
		end
		for i=1,3 do
		self.数量输入[i]:置坐标(self.x+i*1-152,self.y-70)
		self.给予物品[i]:置坐标(self.x  +i*56-36,self.y +63)
		self.给予物品[i]:显示(dt,x,y,self.鼠标)
		if self.数量输入[i]._已碰撞 then
		self.焦点 = true
		end
		if self.给予物品[i].物品 ~= nil then
		    if self.数量输入[i]:取文本() == "" then
			self.数量输入[i]:置文本(1)
		    end
			if  self.本类数据.道具[self.选中编号[i]].数量 ~= nil then
		        --if tonumber(self.数量输入[i]:取文本()) > self.本类数据.道具[self.选中编号[i]].数量 then
		        self.数量输入[i]:置文本(self.本类数据.道具[self.选中编号[i]].数量)
		         else
		   	   self.数量输入[i]:置文本(1)
		       -- elseif tonumber(self.数量输入[i]:取文本()) > 99 then
				--self.数量输入[i]:置文本(99)
		       -- end
		    end
			if self.给予物品[i].焦点 then
			tp.提示:道具行囊(x,y,self.给予物品[i].物品,true)
				if  (self.给予物品[i].事件 or self.给予物品[i].右键 )and self.自我锁定 == false then
					self.物品数据[self.选中编号[i]]:置物品(self.本类数据.道具[self.选中编号[i]])
					self.选中编号[i] = 0
					self.给予物品[i]:置物品(nil)
					self.数量输入[i]:置可视(false,false)
				end
		     end
		 end
		end
		self.资源组[5]:显示(self.x+190,self.y+64)
		self.资源组[6]:显示(self.x+190,self.y+259)
		if self.锁定 then
			for i=1,3 do
			self.对方物品[i]:置坐标(self.x  +i *56-37,self.y +259)
			self.对方物品[i]:显示(dt,x,y,self.鼠标)
			zts:置颜色(引擎.取金钱颜色(self.交易数据.银两))
			zts:显示(self.x+128,self.y+234,self.交易数据.银两)
			zts:置颜色(0xFFFFFF00)
				if self.对方物品[i].焦点 then
				tp.提示:道具行囊(x,y,self.对方物品[i].物品,true)
				end
			end
	    end
	elseif self.状态 == 9 then
		local 序号 = 0
		for i=1,3 do
		self.数量输入[i]:置可视(false,false)
		end
	for i=1,#self.召唤兽数据 do
		if  i~=self.选中召唤兽[1] and i~=self.选中召唤兽[2] and i~=self.选中召唤兽[3] then
		序号 = 	序号+1
		zts:置颜色(-16777216)
		local jx = self.x+260
		local jy = self.y+64+序号*20
		bw:置坐标(jx-4,jy-3)
		if bw:检查点(x,y) then
			box(jx-5,jy-6,jx+173,jy+15,-3551379)
			self.焦点 = true
			self.焦点1 = true
			if mouseb(0) and self.鼠标 and not tp.消息栏焦点 and self.自我锁定 == false  then
				  for o=1,3 do
				  if self.选中召唤兽[o] == 0 then
				 self.选中召唤兽[o] = i
				  break
				  end
				  end
				  -- self.选中召唤兽[1] = i
			elseif mouseb(1) and self.鼠标 and not tp.消息栏焦点 then
					self.焦点 = true
			        self.焦点1 = true
				tp.窗口.召唤兽查看栏:打开(self.召唤兽数据[i])
			end
		end
		zts:显示(jx,jy-2,self.召唤兽数据[i].名称)
	 end
	end
	    local 显示序列 = 0
    for n=1,3 do
      if self.选中召唤兽[n]~=0 then
      显示序列=显示序列+1
      zts:置颜色(-16777216)
      zts:显示(self.x+43,self.y+73+显示序列*28-33,self.召唤兽数据[self.选中召唤兽[n]].名称)
      bw:置坐标(self.x+38,self.y+67+显示序列*28-33)
        if bw:检查点(x,y) then
          if 引擎.鼠标弹起(1)  then
          	self.焦点 = true
			 self.焦点1 = true
          	tp.窗口.召唤兽查看栏:打开(self.召唤兽数据[self.选中召唤兽[n]])
          elseif 引擎.鼠标弹起(0) and  self.自我锁定==false then
          self.选中召唤兽[n]=0
          end
        end
      end
    end
       	self.资源组[5]:显示(self.x+218,self.y+34)
		self.资源组[6]:显示(self.x+218,self.y+229)
		if self.锁定 then
			zts:置颜色(引擎.取金钱颜色(self.交易数据.银两))
			zts:显示(self.x+128,self.y+234,self.交易数据.银两)
			zts:置颜色(0xFFFFFF00)
			if self.交易数据.召唤兽~= nil then
				for i=1,#self.交易数据.召唤兽 do
					zts:置颜色(-16777216)
					zts:显示(self.x+43,self.y+291+i*28-55,self.交易数据.召唤兽[i].名称)
					bw:置坐标(self.x+38,self.y+285+i*28-55)
					if bw:检查点(x,y) then
					if 引擎.鼠标弹起(1)  then
					self.焦点 = true
			        self.焦点1 = true
					tp.窗口.召唤兽查看栏:打开(self.交易数据.召唤兽[i])
					end
				end
			 end
			end

	    end
	end
	 zts:置颜色(0xFFFFFF00)
	 zts:显示(self.x+65,self.y+198,self.本类数据.等级)
	 zts:显示(self.x+145,self.y+198,"陌生人")
	 zts:显示(self.x+87,self.y+170,self.本类数据.名称)
	 self.输入框:置坐标(self.x-150,self.y-70)
	 self.输入框:置可视(true,true)
	if self.输入框:取文本() == "" then
	 self.输入框:置文本(0)
	end
	 self.输入框:置文字颜色(引擎.取金钱颜色(tonumber(self.输入框:取文本())))
	if self.输入框:取文本()+0 > self.本类数据.道具.银两 then
	 self.输入框:置文本(self.本类数据.道具.银两)
	end
	if self.输入框._已碰撞 then
	 self.焦点 = true
	end
	 self.控件类:更新(dt,x,y)
	 self.控件类:显示(x,y)
end

function 交易界面:检查点(x,y)
	if self.资源组[self.状态]:是否选中(x,y) then
		return true
	end
end

function 交易界面:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 交易界面:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 交易界面
