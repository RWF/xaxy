--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:46
--======================================================================--

local 存钱界面 = class()
local insert = table.insert
local tp,zts1,zts
local mouseb = 引擎.鼠标弹起
function 存钱界面:初始化(根)
	self.ID = 60
	self.x = 310
	self.y = 185
	self.xx = 0
	self.yy = 0
	self.注释 = "玩家给与"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
	    [0] = 资源:载入('JM.dll',"网易WDF动画",0x420FF401), --420FF401
	    [1] = 按钮.创建(自适应.创建(17,4,16,16,4,3),0,0,4,true,true),
		[2] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"取钱"),
		[3] = 按钮.创建(自适应.创建(16,4,55,22,1,3),0,0,4,true,true,"取消"),
	 }
	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('创建控件')
	总控件:置可视(true,true)
	self.输入框 = 总控件:创建输入("银两给予",self.x+100,self.y+110,80,14,根)
	self.输入框:置限制字数(8)
	self.输入框:置数字模式()
    self.输入框:置光标颜色(0xFFFF0000)
	self.窗口时间 = 0
	tp = 根
	zts = 根.字体表.普通字体
end

function 存钱界面:打开(数据)
	if self.可视 then
		self.可视 = false
		self.输入框:置焦点(false)
		self.输入框:置可视(false,false)
		self.输入框:置文本(0)

	else
		if  self.x > 全局游戏宽度 then
		self.x = 310
		end
       self.数据 = 数据
       self.输入框:置文本(0)
		insert(tp.窗口_,self)
		tp.运行时间 = tp.运行时间 + 1
	    self.窗口时间 = tp.运行时间
		self.可视 = true
	end
end

function 存钱界面:显示(dt,x,y)
	self.焦点 = false
  self.资源组[0]:显示(self.x,self.y)
 self.资源组[1]:显示(self.x+172,self.y+6)
 self.资源组[1]:更新(x,y)
 self.资源组[2]:显示(self.x+30,self.y+150,true)
 self.资源组[2]:更新(x,y)
 self.资源组[3]:显示(self.x+110,self.y+150,true)
 self.资源组[3]:更新(x,y)
	 if self.资源组[1]:事件判断() or self.资源组[3]:事件判断() then
	 self:打开()
	 elseif self.资源组[2]:事件判断() then
		  客户端:发送数据(tonumber(self.输入框:取文本()), 49, 13, 取随机数(1, 99999) .. "", 1)
		  self.输入框:置文本(0)
	end

 zts:置颜色(引擎.取金钱颜色(self.数据.银子))
 zts:显示(self.x+100,self.y+46,self.数据.银子)
  zts:置颜色(引擎.取金钱颜色(self.数据.存银))
 zts:显示(self.x+100,self.y+79,self.数据.存银)
 zts:置颜色(0xFFFFFFFF)
 self.输入框:置坐标(self.x-311,self.y-185)
 self.输入框:置可视(true,true)
 self.输入框:置文字颜色(引擎.取金钱颜色(tonumber(self.输入框:取文本())))


	if self.输入框._已碰撞 then
	 self.焦点 = true
	end
 self.控件类:更新(dt,x,y)
 self.控件类:显示(x,y)
end

function 存钱界面:检查点(x,y)
	if self.资源组[0]:是否选中(x,y)  then
		return true
	end
end

function 存钱界面:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 存钱界面:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 存钱界面