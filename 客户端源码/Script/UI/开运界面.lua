--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:48
--======================================================================--
local 场景类_开运 = class(窗口逻辑)
local tp,zts,zts1
local insert = table.insert
local xxx = 0
local yyy = 0
local sts = {"单价","数量","总额","现金"}
local tos = 引擎.取金钱颜色
local mouseb = 引擎.鼠标弹起
function 场景类_开运:初始化(根)
	self.ID = 59
	self.x = 224+(全局游戏宽度-800)/2
	self.y = 80
	self.xx = 0
	self.yy = 0
	self.注释 = "商店"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[1] = Picture.开运界面,--背景
        [2] = 按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true),
		[3] = 根._按钮(根._自适应(15,4,55,20,1),0,0,4,true,true,"开运")

	}
	local 格子 = 根._物品格子
	self.物品 = {}
	for i=1,20 do
		self.物品[i] = 格子(0,0,i1,"打造")
	end
	for n=2,3 do
	   self.资源组[n]:绑定窗口_(59)
	end
	self.窗口时间 = 0
	tp = 根
	zts = 根.字体表.华康字体
	zts1 = 根.字体表.华康字体
	self.材料 = 格子(0,0,i1,"材料")
	self.选中 =0
end
function 场景类_开运:刷新(数据,类型)
		self.数据=数据
		self.类型 =类型
		for i=1,20 do
		self.物品[i]:置物品(数据[i])
		end
	end
function 场景类_开运:打开(数据,类型)
	if self.可视 then
		self.可视 = false
		self.选中 =0
		self.材料:置物品(nil)
		for i=1,20 do
		self.物品[i]:置物品(nil)
		end
	else
								                    if  self.x > 全局游戏宽度 then
self.x = 224+(全局游戏宽度-800)/2
    end
		self.数据=数据
		self.类型=类型
		self.资源组[3]:置文字(类型)
		for i=1,20 do
		self.物品[i]:置物品(数据[i])
		end
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
	    self.可视 = true
	end
end

function 场景类_开运:显示(dt,x,y)
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y,self.材料.物品 ~= nil)
	if self.资源组[2]:事件判断() then
		self:打开()
	elseif self.资源组[3]:事件判断() then
		  客户端:发送数据(self.选中,89,13,self.类型)
    self.材料:置物品(nil)
	end
	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[2]:显示(self.x+257,self.y+3)
	self.资源组[3]:显示(self.x+30,self.y+121,true,1,nil,self.材料.物品 == nil,3)
	if self.类型 == "开运" then
		  zts1:显示(self.x+108,self.y+2,"装备开运")
	elseif self.类型 == "修理" then
	     zts1:显示(self.x+108,self.y+2,"装备修理")
	 elseif self.类型 == "鉴定" then
	     zts1:显示(self.x+108,self.y+2,"装备鉴定")
	end

    zts:置颜色(引擎.取金钱颜色(self.数据.银子))
    zts:显示(self.x+178,self.y+65,self.数据.银子)
    zts:置颜色(0xFFFFFF00)
    zts:显示(self.x+178,self.y+120,self.数据.体力)
    zts:置颜色(0xFFFFFFFF)
    local is = 0
	for h=1,4 do
		for l=1,5 do
			is = is + 1
			self.物品[is]:置坐标(l * 51 -38 + self.x,h * 51 + self.y + 102)
			self.物品[is]:显示(dt,x,y,self.鼠标,{"武器","衣服","项链","头盔","鞋子","腰带"})
			if self.物品[is].物品 ~= nil and self.物品[is].焦点 then
				tp.提示:道具行囊(x,y,self.物品[is].物品)
				if self.物品[is].事件 and (self.物品[is].物品.类型 == "武器" or self.物品[is].物品.类型 == "头盔"or self.物品[is].物品.类型 == "衣服"or self.物品[is].物品.类型 == "项链" or self.物品[is].物品.类型 == "鞋子" or self.物品[is].物品.类型 == "腰带" or self.物品[is].物品.类型 == "衣服"   )  and self.鼠标 then
					if self.材料.物品 ~= nil then
					  self.物品[self.选中]:置物品(self.材料.物品)
					   self.选中 =is
	               		self.材料:置物品(self.物品[is].物品)
						self.物品[is]:置物品(nil)
					else
						 self.选中 =is
	               		self.材料:置物品(self.物品[is].物品)
						self.物品[is]:置物品(nil)
                    end
				end
			end
			if self.物品[is].焦点 then
				self.焦点 = true
			end
		end
	end
	if self.材料.物品 ~= nil then

	    self.材料:置坐标(self.x+32,self.y+56)
		self.材料:显示(dt,x,y,self.鼠标,false)
		if self.类型 =="开运" then
				zts:置颜色(引擎.取金钱颜色((self.材料.物品.符石.最小孔数+1)*5000000))
				zts:显示(self.x + 178,self.y + 38,(self.材料.物品.符石.最小孔数+1)*5000000)
				zts:置颜色(0xFFFFFF00)
				zts:显示(self.x + 178,self.y + 94,(self.材料.物品.符石.最小孔数+1)*30)
				zts:置颜色(0xFFFFFFFF)
		elseif self.类型 == "修理" then
			 self.花费金钱 = math.floor(self.材料.物品.等级 / 500 * 1500 * (500 - self.材料.物品.耐久度))
			 zts:置颜色(引擎.取金钱颜色(self.花费金钱))
				zts:显示(self.x + 178,self.y + 38,self.花费金钱)
             zts:置颜色(0xFFFFFFFF)
         elseif self.类型 == "鉴定" then
			 self.花费金钱 = math.floor(self.材料.物品.等级 * 150)
			 zts:置颜色(引擎.取金钱颜色(self.花费金钱))
				zts:显示(self.x + 178,self.y + 38,self.花费金钱)
             zts:置颜色(0xFFFFFFFF)
		end

		if self.材料.焦点 then
			self.焦点 = true
			tp.提示:道具行囊(x,y,self.材料.物品)
			if mouseb(0) and self.鼠标 then
					self.物品[self.选中]:置物品(self.材料.物品)
					self.材料:置物品(nil)
					self.选中=0
			end
	    end
   end
end



return 场景类_开运