--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:48
--======================================================================--
local 场景类_文本栏 = class(窗口逻辑)
local tp
local insert = table.insert

function 场景类_文本栏:初始化(根)
	self.ID = 3
	self.x = 245+(全局游戏宽度-800)/2
	self.y = 232
	self.xx = 0
	self.yy = 0
	self.注释 = "文本栏"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	tp = 根

	self.资源组 = {
		[1] = tp._自适应(99,1,350,171,3,9),
		[2] = tp._按钮(tp._自适应(15,4,55,20,1),0,0,4,true,true,"确定"),
		[3] = tp._按钮(tp._自适应(15,4,55,20,1),0,0,4,true,true,"取消"),
		[4] = tp._按钮(tp._自适应(15,4,72,20,1),0,0,4,true,true,"强化打造"),
		[5] = tp._按钮(tp._自适应(15,4,72,20,1),0,0,4,true,true,"普通打造"),
		[6] = tp._按钮(tp._自适应(15,4,72,20,1),0,0,4,true,true,"装备道具"),
		[7] = tp._按钮(tp._自适应(15,4,72,20,1),0,0,4,true,true,"召唤兽"),
		[8] = tp._自适应(70,1,330,95,3,9),
	}
	self.介绍文本 = 根._丰富文本(305,75,根.字体表.普通字体)
	for n=2,7 do
	   self.资源组[n]:绑定窗口_(3)
	   self.资源组[n]:文本栏按钮_(true)
	end
	self.窗口时间 = 0
	
end

function 场景类_文本栏:载入(内容,回调,按钮,事件,number)
	if  self.可视 then
		self.文本 = nil
		self.介绍文本:清空()
		self.可视 = false
	else
		self.x = 245+(全局游戏宽度-800)/2
		insert(tp.窗口_,self)
		self.文本 = {}
		self.文本.内容 = 内容
		self.文本.回调 = 回调
		self.文本.按钮 = 按钮
		self.文本.事件 = 事件
		self.文本.number=number
		self.介绍文本:清空()
		self.介绍文本:添加文本(内容)
		tp.运行时间 = tp.运行时间 + 3
		self.窗口时间 = tp.运行时间
		self.可视 = true
	end
end

function 场景类_文本栏:显示(dt,x,y)
	self.焦点 = false
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y)
	if self.资源组[2]:事件判断() then
		if self.文本.事件 ~= nil then
			self.文本.事件()
			self:载入()
			return false
		end
		self:事件逻辑(self.文本.回调,self.文本.number)
		self.可视 = false
	elseif self.资源组[3]:事件判断() then
		if self.文本.回调 == "丢弃物品" then
			if tp.抓取物品注释 == "包裹" or tp.抓取物品注释 == "行囊" then
				tp.窗口.道具行囊.物品[tp.抓取物品ID].确定 = false
					if  tp.窗口.道具行囊.可视 then
					local sdf
					if tp.抓取物品注释 =="包裹" then
					sdf =1
					else
					sdf =2
					end
					客户端:发送数据(sdf, 1, 13, "9", 1)
					end
			elseif tp.抓取物品注释 == "道具行囊_人物装备" then

				tp.窗口.道具行囊.人物装备[tp.抓取物品ID].确定 = false

			elseif tp.抓取物品注释 == "道具行囊_召唤兽装备" then
				tp.窗口.道具行囊.召唤兽装备[tp.抓取物品ID]:置物品(tp.抓取物品)
				tp.窗口.道具行囊.召唤兽装备[tp.抓取物品ID].确定 = false
				tp.队伍[1].宝宝列表[tp.窗口.道具行囊.选中召唤兽]:穿戴装备(tp.抓取物品,tp.抓取物品ID)
			end
			tp.抓取物品 = nil
			tp.抓取物品ID = nil
			tp.抓取物品注释 = nil
		end
		self.可视 = false
	elseif self.资源组[4]:事件判断() then
		客户端:发送数据(tp.窗口.打造.选中材料[1],tp.窗口.打造.选中材料[2],17,"强化打造","打造")
		for i=1,2 do
			tp.窗口.打造.选中材料[i] =0
		end
		tp.窗口.打造:打开()
		self.可视 = false
	elseif self.资源组[5]:事件判断() then
		客户端:发送数据(tp.窗口.打造.选中材料[1],tp.窗口.打造.选中材料[2],17,"普通打造","打造")
		for i=1,2 do
			tp.窗口.打造.选中材料[i] =0
		end
		tp.窗口.打造:打开()
		self.可视 = false
	elseif self.资源组[6]:事件判断() then
	    客户端:发送数据(0, 201, 13, "包裹", 1)
		self.可视 = false
	elseif self.资源组[7]:事件判断() then
		--客户端:发送数据(0, 202, 13, "包裹", 1)
		tp.提示:写入("#Y/召唤兽目前不能拍卖,还在开发中")
		self.可视 = false

	end
	self.资源组[1]:显示(self.x,self.y)
	self.资源组[8]:显示(self.x+10,self.y+30)
	if self.文本.回调 == "打造" then
	self.资源组[4]:显示(self.x + 75,self.y + 135,true,1)
	self.资源组[5]:显示(self.x + 205,self.y + 135,true,1)
	self.资源组[4]:更新(x,y)
	self.资源组[5]:更新(x,y)
	elseif self.文本.回调 == "拍卖" then
	self.资源组[6]:显示(self.x + 35,self.y + 135,true,1)
	self.资源组[7]:显示(self.x + 135,self.y + 135,true,1)
	self.资源组[6]:更新(x,y)
	self.资源组[7]:更新(x,y)
	self.资源组[3]:显示(self.x + 235,self.y + 135,true,1)
	else
	self.资源组[2]:显示(self.x + 75,self.y + 135,true,1)
	self.资源组[3]:显示(self.x + 205,self.y + 135,true,1)
    end
	self.介绍文本:显示(self.x  + 25,self.y + 35)
end


function 场景类_文本栏:事件逻辑(回调,number)
	if 回调 == "丢弃物品" then
	       客户端:发送数据(tp.窗口.道具行囊.物品[tp.抓取物品ID].ID+(tp.窗口.道具行囊.数据.Pages-1)*20, 3, 13,tp.窗口.道具行囊.更新)
		if tp.抓取物品注释 == "行囊" or tp.抓取物品注释 == "包裹"  or tp.抓取物品注释 == "锦衣"  then

			tp.窗口.道具行囊.物品[tp.抓取物品ID].确定 = false
		elseif tp.抓取物品注释 == "道具行囊_人物装备" then
			tp.窗口.道具行囊.人物装备[tp.抓取物品ID-20].确定 = false
		elseif tp.抓取物品注释 == "道具行囊_召唤兽装备" then
			tp.窗口.道具行囊.召唤兽装备[tp.抓取物品ID].确定 = false
		elseif tp.抓取物品注释 == "道具行囊_灵饰" then
			tp.窗口.灵饰.物品[tp.抓取物品ID].确定 = false
		end
		tp.抓取物品 = nil
		tp.抓取物品ID = nil
		tp.抓取物品注释 = nil
	elseif 回调 == "洗炼属性" then
		客户端:发送数据(34, 34, 5)
	elseif 回调 == "洗炼技能" then
		客户端:发送数据(35, 35, 5)
	elseif 回调 == "补充灵气" then
		客户端:发送数据(36, 36, 5)
	elseif 回调 == "经脉" then
		客户端:发送数据(17, 17, 5, 1)
	elseif 回调 == "补充法宝灵气" then
		客户端:发送数据(tp.窗口.法宝神器.选中法宝.id,209,13,tp.窗口.法宝神器.更新)
	elseif 回调 == "提取法宝灵气" then
		客户端:发送数据(tp.窗口.法宝神器.选中法宝.id,210,13,tp.窗口.法宝神器.更新)
	elseif 回调 == "道具拍卖" then
	   if tp.窗口.道具竞拍.可视 then

		tp.窗口.道具竞拍.可视 = false
        tp.窗口.道具竞拍.输入框:置可视(false,false)
	   end
	elseif 回调 == "孩子抛弃" then
		客户端:发送数据(number, 30, 5)
	elseif 回调 == "踢出人物" then
       客户端:发送数据(tp.窗口.队伍栏.选中人物, 10, 11, "66", 1)
	elseif 回调 == "储存召唤兽" then
		tp.窗口.召唤兽属性栏:存储()
	elseif 回调 == "更换队长" then
		客户端:发送数据(tp.窗口.队伍栏.选中人物, 12, 11, "66", 1)
	elseif 回调 == "提取" then
		客户端:发送数据(tp.窗口.宠物炼妖栏.主召唤兽, 88, 13, tp.窗口.宠物炼妖栏.编号,tp.窗口.宠物炼妖栏.类型)
		   tp.窗口.宠物炼妖栏.炼妖物品:置物品(nil)
	        tp.窗口.宠物炼妖栏.主召唤兽 = nil
	elseif 	  回调 == "吸附" then
			客户端:发送数据(tp.窗口.宠物炼妖栏.主召唤兽, 88, 13, tp.窗口.宠物炼妖栏.编号,tp.窗口.宠物炼妖栏.类型)
		   tp.窗口.宠物炼妖栏.炼妖物品:置物品(nil)
	        tp.窗口.宠物炼妖栏.主召唤兽 = nil
	elseif 	  回调 == "炼妖石" then
	        tp.窗口.宠物炼妖栏.炼化结束 =1
	elseif 	  回调 == "坐骑" then
		 客户端:发送数据(tp.窗口.坐骑.选中编号, 6, 42, "9", 1)
	        tp.窗口.坐骑.选中编号 =0
	elseif 	  回调 == "剧情点" then
		客户端:发送数据(18, 18, 5, 1)
	elseif 	  回调 == "扩充包裹" then
		客户端:发送数据(90, 90, 13)
	elseif 	  回调 == "升级" then
	        客户端:发送数据(0, 2, 5, "0", 1)
			tp.窗口.人物状态栏.预览属性 = {气血=0,魔法=0,命中=0,伤害=0,速度=0,灵力=0,防御=0,躲避=0}
			tp.窗口.人物状态栏.临时潜力 = {体质=0,魔力=0,力量=0,耐力=0,敏捷=0}
	end
end

return 场景类_文本栏