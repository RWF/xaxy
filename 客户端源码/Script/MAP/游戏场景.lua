-- @Author: 作者QQ414628710
-- @Date:   2022-01-15 20:06:44
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-03-03 01:50:33
--======================================================================--
local 场景假人 = require("script/Role/假人")
local jl = require("gge精灵类")

local tp = nil
local floor = math.floor
local lsph = nil
local insert = table.insert
local zt,lsph

local 场景类_地图 = class()
function 场景类_地图:初始化(根)
	tp = 根
	zt = tp.字体表.描边字体
	 self.传送点 = require("script/MAP/传送点")
	self.战斗 = require("script/Fight/战斗1")(根)
	self.地图 = require("script/MAP/地图类")(根,self)
	 self.人物 = require("script/Role/人物")(根)

	self.传送圈数据={}
	self.战斗提示 = ""
	self.战斗提示时间 = 0
	
	-- self.过度纹理 = require("gge纹理类")():渲染目标(全局游戏宽度+50,全局游戏高度+50)
	-- self.过度进度 = 155
	-- self.过度时间 = 30
	self.渐变动画 = require("Script/MAP/渐变动画")
	self.开启场景加载 = false
	self.屏幕坐标 = 生成XY()
	self.滑屏 = 生成XY()
	self.自由事件 = nil
    self.队长开关= false
	self.传送时间 = 0
	self.当前刷新 = nil
	self.传送 = {}
	self.临时NPC={}
	self.场景人物 = {}
	self.假人 = {}

end

function 场景类_地图:转移(数据)
	if 低配模式 ==false  then
self.渐变动画:初始化()
	tp.隐藏UI = true
	tp.恢复UI = true
end
	 self.人物.脚印动画组=nil
	 self.鼠标点击动画组=nil
	 self.鼠标点击动画组={}
 self.人物.脚印动画组={}
	self.场景人物 = {}
	self.假人 = {}
	self.传送 = {}
	self.临时NPC={}
	self.数据 =数据
    self.玩家 ={}
	self.地图名称 = 数据.名称
	tp.窗口.时辰.地图 = 数据.名称
	self.传送圈数据 = 数据.传送圈
	 if self.数据.编号==5135 or self.数据.编号==6001 or self.数据.编号==6002 or self.数据.编号==6003 then
         self.数据.编号=5131
	 end

	local yu = 取Bgm(self.数据.编号)
	if tp.音乐.文件 ~= 程序目录.."Audio/"..yu..".mp3" then
		if 引擎.文件是否存在(程序目录.."Audio/"..yu..".mp3") then
			tp.音乐:停止()
			tp.音乐 = nil
			tp.音乐 = tp.资源:载入(程序目录.."Audio/"..yu..".mp3","音乐",nil)
			if tp.音乐开启 then
				tp.音乐:播放(true)
			end
			tp.音乐:置音量(tp.系统设置.声音设置[1])
		end
	end
	insert(self.场景人物,self.人物)
	if self.数据.传送圈 then
		for n=1,#self.数据.传送圈 do
		 	self.传送[n] = self.传送点(unpack(self.数据.传送圈[n]))
		 	q = nil
		end
	end

    self:传送至(self.数据.编号,self.数据.地图数据.x,self.数据.地图数据.y)
end



function 场景类_地图:传送至(地图,坐标x,坐标y)
	if tp.窗口.玩家给予.可视 or  tp.窗口.NPC给予.可视 or tp.窗口.对话栏.可视 then
		tp.窗口.玩家给予.可视 =false
		tp.窗口.NPC给予.可视 =false
		tp.窗口.对话栏.可视=false
	end
         
		 -- 引擎.截图到纹理(self.过度纹理)
		 -- self.过度精灵 =jl(self.过度纹理 )
		 

		 -- self.过度进度 = 255
		 --  self.过度时间 = 3
		  if 地图==11351 or 地图==11352 or 地图==11353 or 地图==11354 or 地图==11355 then
		     地图=1876
		  elseif 地图>=7000 and 地图<=7092 then
		  	地图=1009
		  end
		local 地图等级 = 取场景等级(地图)
		if 地图等级 ~= nil then
			self.场景最低等级,self.场景最高等级 = 地图等级,地图等级
		else
			self.场景最低等级 = nil
			self.场景最高等级 = nil
		end
       self.地图:加载(程序目录.."Data/Dt/381990860"..地图..".dll",地图)
		tp.角色坐标.x,tp.角色坐标.y = 坐标x,坐标y
		tp.召唤兽坐标.x,tp.召唤兽坐标.y = 坐标x-20,坐标y-20
		self.滑屏.x,self.滑屏.y = tp.角色坐标.x,tp.角色坐标.y
	    collectgarbage("collect")
end
function 场景类_地图:显示(dt,x,y)
	if self.数据  then
		self.地图:显示(tp.角色坐标,self.屏幕坐标,x,y,dt)
    end
	if tp.战斗中 then
		self.战斗:显示(dt,x,y)
		self.战斗:更新(dt,x,y)
		if self.战斗提示 ~= "" then
			tp.字体表.人物字体:置颜色(-459738)
			tp.字体表.人物字体:置阴影颜色(nil)
			tp.字体表.人物字体:显示((全局游戏宽度-tp.字体表.普通字体:取宽度(self.战斗提示)) / 2,全局游戏高度-100,self.战斗提示)
			self.战斗提示时间 = self.战斗提示时间 + 1
			if self.战斗提示时间 >= 80 then
				self.战斗提示 = ""
				self.战斗提示时间 = 0
			end
		end
	end
	for n=1,#self.传送 do
		if self.传送[n] ~= nil then
			self.传送[n]:更新(dt,xx,yy,偏移)
		end
	end
 --   	if self.过度精灵 ~= nil then
	-- 	self.过度时间 = self.过度时间 - 0.55
	-- 	if self.过度时间 <= 0 then
	-- 		self.过度进度 = self.过度进度 - (self.过度减少 or 20)
	-- 		self.过度时间 = 0
	-- 		if self.过度进度 <= 0 then
	-- 			self.过度进度 = 0
	-- 		end
	-- 	end
	-- 	if self.过度进度 <= 240 then
	-- 		if tp.恢复UI then
	-- 			tp.隐藏UI = false
	-- 			tp.恢复UI = false
	-- 		end
	-- 	end
	-- 	self.过度精灵:置颜色(ARGB(self.过度进度,255,255,255))
	-- 	self.过度精灵:显示()
	-- 	if self.过度进度 <= 0 then
	-- 		self.过度精灵:释放()
	-- 		self.过度精灵 = nil
	-- 		self.过度减少 = nil
	-- 	end
	-- end
	if 低配模式 ==false then
	self.渐变动画:显示(0,0)
	self.渐变动画:更新(dt)
    end
	
end

return 场景类_地图