--======================================================================--
-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-05-18 00:26:14
--======================================================================--
local 地图类 = class()
local tp,tps,hc
local 精灵类 = require("gge精灵类")()
local 路径类 = require("script/MAP/路径类")
local map = require("script/MAP/MAP")
local dtx = require("script/Resource/地图特效")
local function 排序(a,b)
    if a.坐标.y == b.坐标.y  then
       return a.id > b.id 
    end
	return a.坐标.y < b.坐标.y 

    
end
local h,l

function 地图类:初始化(根,根1)
	tp = 根
	tps = 根1
    self.开始位置   = 生成XY(1,1) --生成XY() 是GGE函数，生成的{x=0,y=0},而且重载了相加和相减
    self.结束位置   = 生成XY(1,1)
end

function 地图类:加载(文件,编号)
        for i,v in ipairs{"resource","JM.dll","ZY.dll","ZHS.dll",'WP.dll','WP1.dll',"smap.dll","JS.dll","sound.dll","sound1.dll","MAX.7z"} do
            CacheFile[v] ={}
        end
		self.mapzz = {}
		self.增加 = {x=0,y=0,z=0}
	
		self.db = {}
		 self.map = map(文件)
		self.宽度,self.高度,self.行数,self.列数 = self.map.Width,self.map.Height,self.map.MapRowNum,self.map.MapColNum

		
		self.寻路 = 路径类.创建(hc,self.列数*16,self.行数*12,self.map:取障碍())

		self.传送tx = {}
		self.特效cw={}
		if  地图特效 then
		self.特效cw = 取传特效表(编号)
        end
		if #self.特效cw > 0   then
			for nw=1,#self.特效cw do
				if self.特效cw[nw]~=nil then
				    self.传送tx[nw] = dtx(self.特效cw[nw].x,self.特效cw[nw].y,self.特效cw[nw].切换,self.特效cw[nw].资源)
				end
			end
		end
end

function 地图类:显示(pos,偏移,xx,yy,dt,pys1)
	local mp={}

    local 主角位置  = 生成XY(math.ceil(pos.x/320), math.ceil(pos.y/240))
    local 需要块数  = 生成XY(math.ceil(全局游戏宽度/320), math.ceil(全局游戏高度/240))
    local 视口范围 = 生成XY(math.ceil(全局游戏宽度/640)+1, math.ceil(全局游戏高度/480)+1)

    if(主角位置.x == 1)then
        self.开始位置.x   = 1
        self.结束位置.x   = 需要块数.x
    elseif(主角位置.x >= self.列数)then
        self.开始位置.x   = 主角位置.x - 需要块数.x
        self.结束位置.x   = 主角位置.x
    else
        self.开始位置.x   = 主角位置.x - 视口范围.x
        self.结束位置.x   = 主角位置.x + 视口范围.x
        if(self.开始位置.x <=0)then
            self.开始位置.x =1
        end
        if(self.结束位置.x >self.列数)then
            self.结束位置.x =self.列数
        end
    end
    if(主角位置.y == 1)then
        self.开始位置.y   = 1
        self.结束位置.y   = 需要块数.y
    elseif(主角位置.y >= self.行数)then
        self.开始位置.y   = 主角位置.y - 需要块数.y
        self.结束位置.y   = 主角位置.y
    else
        self.开始位置.y   = 主角位置.y - 视口范围.y
        self.结束位置.y   = 主角位置.y + 视口范围.y
        if(self.结束位置.y>self.行数)then
            self.结束位置.y=self.行数
        end
        if(self.开始位置.y<=0)then
            self.开始位置.y=1
        end
    end
    if self.开始位置.x <=0 then self.开始位置.x = 1 end
    if self.开始位置.y <=0 then self.开始位置.y = 1 end
    if self.结束位置.x <=0 then self.结束位置.x = 1 end
    if self.结束位置.y <=0 then self.结束位置.y = 1 end

    -- print("地图列行",self.列数,self.行数)
    -- print("需要块数",需要块数.x,需要块数.y)
    -- print("主角位置",主角位置.x,主角位置.y)
    -- print("开始位置",self.开始位置.x,self.开始位置.y)
    -- print("结束位置",self.结束位置.x,self.结束位置.y)

    local id
    local zz

    -- zz 遮罩

    for h = self.开始位置.y, self.结束位置.y do
        for  l = self.开始位置.x, self.结束位置.x do
            id = ((h-1)*self.列数+l)-1
            if self.db[id] == nil then
                self.db[id] = {{}}
                -- 遮罩
                zz = self.map:取附近遮罩(id)
                -- table.print(zz)
                for i=1,#zz do
                    if self.mapzz[zz[i]] == nil then
                        self.mapzz[zz[i]] = 1
                        table.insert(self.db[id][1],{self.map:取遮罩(zz[i])})              
                    end
                end
                -- 地图
                self.db[id][2] = {(l-1)*320,(h-1)*240}
                self.db[id][3] = #self.db[id][1]
                self.map:取纹理(id)
                break
            end
        end
    end
 
    for h = self.开始位置.y, self.结束位置.y do
        for  l = self.开始位置.x, self.结束位置.x do
            id = ((h-1)*self.列数+l)-1
            if self.db[id] ~= nil then
                for i=1,#self.db[id][1] do
                    table.insert(mp,self.db[id][1][i])
                end
                精灵类:置纹理(self.map.缓存[id])
                -- 地图
                精灵类:显示(self.db[id][2][1]+偏移.x,self.db[id][2][2]+偏移.y)

            end
        end
    end

    -- local mp = {}

    -- local 主角位置1 = math.ceil(pos.x/320)
    -- local 主角位置2 = math.ceil(pos.y/240)
    --  self.开始位置.x = 主角位置1 - 4
    --  self.结束位置.x = 主角位置1 + 4
    --  self.开始位置.y = 主角位置2 - 4
    --  self.结束位置.y = 主角位置2 + 4
    -- if self.结束位置.x > self.列数 then
    --     self.结束位置.x = self.列数
    -- end
    -- if self.开始位置.x < 1 then
    --     self.开始位置.x = 1
    --     self.结束位置.x = self.结束位置.x + 1
    -- end
    -- if self.结束位置.y > self.行数 then
    --     self.结束位置.y = self.行数
    -- end
    -- if self.开始位置.y < 1 then
    --     self.开始位置.y = 1
    -- end

    -- local id
    -- local zz

    -- for h = self.开始位置.y, self.结束位置.y do
    --     for  l = self.开始位置.x, self.结束位置.x do
    --         id = ((h-1)*self.列数+l)-1
    --         if self.db[id] == nil then
    --             self.db[id] = {{}}
    --             zz = self.map:取附近遮罩(id)
    --             for i=1,#zz do
    --                 if self.mapzz[zz[i]] == nil then
    --                     self.mapzz[zz[i]] = 1
    --                     table.insert(self.db[id][1],{self.map:取遮罩(zz[i])})              
    --                 end
    --             end
    --             self.db[id][2] = {(l-1)*320,(h-1)*240}
    --             self.db[id][3] = #self.db[id][1]
    --             self.map:取纹理(id)
    --             break
    --         end
    --     end
    -- end
 
    -- for h = self.开始位置.y, self.结束位置.y do
    --     for  l = self.开始位置.x, self.结束位置.x do
    --         id = ((h-1)*self.列数+l)-1
    --         if self.db[id] ~= nil then
    --             for i=1,#self.db[id][1] do
    --                 table.insert(mp,self.db[id][1][i])
    --             end
    --             精灵类:置纹理(self.map.缓存[id])
    --             精灵类:显示(self.db[id][2][1]+偏移.x,self.db[id][2][2]+偏移.y)

    --         end
    --     end
    -- end





	if not tp.战斗中 then
		local cs
		if tp.第二场景开启 == false then
			tp.选中假人 = false
			table.sort(tps.场景人物,排序)
			if tp.窗口.快捷技能栏:检查点(xx,yy) or tp.窗口.底图框:检查点(xx,yy) or tp.窗口.人物框:检查点(xx,yy) or tp.窗口.时辰:检查点(xx,yy) or tp.窗口.任务追踪:检查点(xx,yy)  then
				tp.禁止通行 = true
			end
			for n=1,#tps.传送 do
				tps.传送[n]:显示(dt,xx,yy,偏移)
			end
			for n=1,#tps.假人 do
				if tps.假人[n] ~= nil then
					tps.假人[n]:更新(dt,xx,yy,偏移)
				end
			end

			for n=1,#tps.临时NPC do
				if tps.临时NPC[n] ~= nil then
					tps.临时NPC[n]:更新(dt,xx,yy,偏移)
				end
			 end
			if self.传送tx~=nil and 地图特效 then
				for n=1,#self.传送tx do
					if self.特效cw[n]~=nil and self.特效cw[n].资源是否最上面 ~= 1  then
					    self.传送tx[n]:显示(dt,xx,yy,偏移)
					end
				end
			end
			 for n=1,#tps.场景人物 do
			-- 	--  if  tps.场景人物[n].标识 or 取两点距离(tp.角色坐标,tps.场景人物[n].坐标)<=全局游戏宽度/2+80  then
					tps.场景人物[n]:显示(dt,xx,yy,偏移)
			-- 	-- end
			 end
		else
			--if tp.剧情处理.可视 and tp.剧情处理.显示方式 == 0 then
				if tp.第二场景.人物组 ~= nil then
				 	table.sort(tp.第二场景.人物组,排序)
				 	for i=1,#tp.第二场景.人物组 do
				 		local v = tp.第二场景.人物组[i]
				 		if v.出现特效 == nil then
				 			tp.影子:显示(v.坐标 + tps.屏幕坐标)
				 		end
				 		v:显示(dt,xx,yy,tps.屏幕坐标)
				 	end
				end
				if self.传送tx~=nil then
					for n=1,#self.传送tx do
						if self.特效cw[n]~=nil and self.特效cw[n].资源是否最上面 ~= 1 then
						    self.传送tx[n]:显示(dt,xx,yy,偏移)
						end
					end
				end
			--end
		end

		for i=1,#mp do
			精灵类:置纹理(mp[i][1])
			精灵类:显示(mp[i][2]+偏移.x,mp[i][3]+偏移.y)
		end
		if tp.第二场景.人物组 ~= nil then
			for i=1,#tp.第二场景.人物组 do
				local v = tp.第二场景.人物组[i]
				v.喊话:显示(v.坐标.x + tps.屏幕坐标.x,v.坐标.y + tps.屏幕坐标.y)
			end
		end
		if self.传送tx~=nil and 地图特效 then
			for n=1,#self.传送tx do
				if self.特效cw[n]~=nil and self.特效cw[n].资源是否最上面 == 1 then
				    self.传送tx[n]:显示(dt,xx,yy,偏移)
				end
			end
		end
        
		 tps.屏幕坐标 = 取画面坐标(tp.角色坐标.x,tp.角色坐标.y,self.宽度,self.高度)

	 end
end
return 地图类;
