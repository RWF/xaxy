--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:52
--======================================================================--
local 场景类_小地图 = class()
local tp
local xys = 生成XY
local floor = math.floor
local format = string.format
local mousea = 引擎.鼠标按住
local mouseb = 引擎.鼠标弹起
local wns = 引擎.文件是否存在
local insert = table.insert
local fgz = 分割字符
local number = tonumber
local string = tostring

local pt
local bw = require("gge包围盒")(0,0,0,16)


function 场景类_小地图:初始化(根)
	self.ID = 1
	self.注释 = "小地图"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	self.记忆地图 = 0
	self.窗口时间 = 0
	self.目标格子 = xys()
	local 资源 = 根.资源
	local 自适应 = 根._自适应
	tp = 根
	self.标记 = 资源:载入('JM.dll',"网易WDF动画",0x393947EB)
	self.终点 = 资源:载入('JM.dll',"网易WDF动画",0xDEE57252)
	self.小点=  资源:载入('JM.dll',"网易WDF动画",0xF792E03C)
	self.自动寻路=根._按钮.创建(资源:载入('JM.dll',"网易WDF动画",0x0BC4D521),0,0,4,true,true,"")
	self.世界 = 根._按钮.创建(自适应.创建(103,4,55,22,1,3),0,0,4,true,true,"世界")
	self.关闭 =根._按钮.创建(自适应.创建(104,4,16,16,4,3),0,0,4,true,true)
	self.普通 = 根._按钮.创建(资源:载入('MAX.7z',"网易WDF动画","11F8B522"),0,0,5,true,true)
	self.普通:置根(根)
	self.普通:置偏移(-3,2)
	self.传送 = 根._按钮.创建(资源:载入('MAX.7z',"网易WDF动画","11F8B522"),0,0,5,true,true)
	self.传送:置根(根)
	self.传送:置偏移(-3,2)
	self.传送圈 = 根._按钮.创建(资源:载入('MAX.7z',"网易WDF动画","11F8B522"),0,0,5,true,true)
	self.传送圈:置根(根)
	self.传送圈:置偏移(-3,2)
	self.特殊 = 根._按钮.创建(资源:载入('MAX.7z',"网易WDF动画","11F8B522"),0,0,5,true,true)
	self.特殊:置根(根)
	self.特殊:置偏移(-3,2)
	self.商店 = 根._按钮.创建(资源:载入('MAX.7z',"网易WDF动画","11F8B522"),0,0,5,true,true)
	self.商店:置根(根)
	self.商店:置偏移(-3,2)
	self.任务 = 根._按钮.创建(资源:载入('MAX.7z',"网易WDF动画","11F8B522"),0,0,5,true,true)
	self.任务:置根(根)
	self.任务:置偏移(-3,2)
	self.全部 = 根._按钮.创建(资源:载入('MAX.7z',"网易WDF动画","11F8B522"),0,0,5,true,true)
	self.全部:置根(根)
	self.全部:置偏移(-3,2)
	self.普通显示 = false
	self.全部显示 = false
	self.传送显示 = false
	self.传送圈显示 = false
	self.特殊显示 = false
	self.间隔计次=0
	self.商店显示 = false
	self.任务显示 = false

	self.偏移X = 0
	self.偏移Y = 0
	self.背景窗口 = tp._自适应.创建(99,1,0,0,3,9)
	self.窗口时间 = 0
	self.目标格子 = nil
	self.xx = 0
	self.yy = 0
	self.my = 0
	self.传送圈动画 = 根.资源:载入("JM.dll","网易WDF动画",0x558897FF)

	pt = 根.字体表.华康字体
	self.间隔 =0
end

function 场景类_小地图:打开(map)
 
	if MapData[map] ==nil or MapData[map].小地图 == nil then
		if self.可视 then
			self.qx = nil
			self.qy = nil
			self.可视 = false
			self.普通显示 = false
			-- self.普通:置打勾框(self.普通显示)
			self.传送显示 = false
			-- self.传送:置打勾框(self.传送显示)
			self.传送圈显示 = false
			-- self.传送圈:置打勾框(self.传送圈显示)
			self.特殊显示 = false
			-- self.特殊:置打勾框(self.特殊显示)
			self.商店显示 = false
			-- self.商店:置打勾框(self.商店显示)
			self.任务显示 = false
			-- self.任务:置打勾框(self.任务显示)
			self.全部显示 = false
			-- self.全部:置打勾框(self.全部显示)
		else
			tp.窗口.聊天框类:添加文本("此场景无法查看小地图")
		end
	else
		if self.记忆地图 ~= map then
			local ids = format(程序目录.."Data/pic/%d.png",map)
			if wns(ids) and MapData[map].小地图 == nil then
				self.小地图 = tp.资源:载入(ids,"图片")
				self.my = 2
			else
			    self.小地图 = tp.资源:载入('smap.dll',"网易WDF动画",MapData[map].小地图)
			end
			self.宽度 = self.小地图:取宽度()
			self.高度 = self.小地图:取高度()
			local fg = {}
			fgz(string(self.高度),fg)
			fg = number(fg[3])
			local v = 27
			if fg == 1 or fg == 3 or fg == 5 or fg == 7 or fg == 9 then
				v = 26
			end
			self.x = self.x or floor((全局游戏宽度 - self.宽度) /2)
			self.y = self.y or floor((全局游戏高度 - self.高度) /2)
			self.qx = nil
			self.qy = nil
			if self.宽度>= 500 then
				self.背景窗口:置宽高(self.宽度+v+2,self.高度+v+28+13)
			else
			    self.背景窗口:置宽高(self.宽度+84,self.高度+v+13)
			end
			self.偏移X = (self.宽度 - 20) / tp.场景.地图.宽度
			self.偏移Y = (self.高度 - 20)/ tp.场景.地图.高度
			self.记忆地图 = map
			self.可视 = true
			tp.运行时间 = tp.运行时间 + 1
			self.窗口时间 = tp.运行时间
			insert(tp.窗口_,self)
		elseif self.记忆地图 == map and self.可视 then
			self.qx = nil
			self.qy = nil
		    self.可视 = false
		 --    self.普通显示 = false
			-- self.普通:置打勾框(self.普通显示)
		 --    self.传送显示 = false
			-- self.传送:置打勾框(self.传送显示)
			-- self.传送圈显示 = false
			-- self.传送圈:置打勾框(self.传送圈显示)
			-- self.特殊显示 = false
			-- self.特殊:置打勾框(self.特殊显示)
			-- self.商店显示 = false
			-- self.商店:置打勾框(self.商店显示)
			-- self.任务显示 = false
			-- self.任务:置打勾框(self.任务显示)
			-- self.全部显示 = false
			-- self.全部:置打勾框(self.全部显示)
		elseif self.可视 == false and self.记忆地图 == map then
			self.qx = nil
			self.qy = nil
			self.可视 = true
			insert(tp.窗口_,self)
			tp.运行时间 = tp.运行时间 + 1
			self.窗口时间 = tp.运行时间
		end
	end
	return false
end

function 场景类_小地图:清空()
	self.目标格子 = nil
end

function 场景类_小地图:显示(dt,x,y)

	if self.记忆地图 ~= tp.场景.数据.编号 then

		if  MapData[map]==nil or MapData[map].小地图 == nil then
			self.qx = nil
			self.qy = nil
			self.可视 = false
			self.普通显示 = false
			-- self.普通:置打勾框(self.普通显示)
			self.传送显示 = false
			-- self.传送:置打勾框(self.传送显示)
			self.传送圈显示 = false
			-- self.传送圈:置打勾框(self.传送圈显示)
			self.特殊显示 = false
			-- self.特殊:置打勾框(self.特殊显示)
			self.商店显示 = false
			-- self.商店:置打勾框(self.商店显示)
			self.任务显示 = false
			-- self.任务:置打勾框(self.任务显示)
			self.全部显示 = false
			-- self.全部:置打勾框(self.全部显示)
			return false
		end
		self.普通显示 = false
		-- self.普通:置打勾框(false)
		self.传送显示 = false
		-- self.传送:置打勾框(false)
		self.传送圈显示 = false
		-- self.传送圈:置打勾框(false)
		self.特殊显示 = false
		-- self.特殊:置打勾框(false)
		self.商店显示 = false
		-- self.商店:置打勾框(false)
		self.任务显示 = false
		-- self.任务:置打勾框(false)
		self.全部显示 = false
		-- self.全部:置打勾框(false)
		local ids = format(程序目录.."Data/pic/%d.png",tp.场景.数据.编号)
		if  wns(ids) then
			self.小地图 = tp.资源:载入(ids,"图片")
			self.my = 2
		else
			self.小地图 = tp.资源:载入('smap.dll',"网易WDF动画",MapData[map].小地图)
		end
		self.宽度 = self.小地图:取宽度()
		self.高度 = self.小地图:取高度()
		local fg = {}
		fgz(string(self.高度),fg)
		fg = number(fg[3])
		local v = 27
		if fg == 1 or fg == 3 or fg == 5 or fg == 7 or fg == 9 then
			v = 26
		end
		self.qx = nil self.qy = nil
		if self.宽度>= 500 then
			self.背景窗口:置宽高(self.宽度+v+2,self.高度+v+28)
		else
			self.背景窗口:置宽高(self.宽度+84,self.高度+v)
		end
		self.偏移X = (self.宽度 - 20) / tp.场景.地图.宽度
		self.偏移Y = (self.高度 - 20)/ tp.场景.地图.高度
		self.记忆地图 = tp.场景.地图数据.编号

	end
	self.选中人物 = false
	self.焦点 = false
	self.普通:更新(x,y)
	self.传送圈:更新(x,y)
	self.传送:更新(x,y)
	self.特殊:更新(x,y)
	self.商店:更新(x,y)
	self.任务:更新(x,y)
    self.全部:更新(x,y)
	self.自动寻路:更新(x,y)
	if self.普通:事件判断() then
		-- self.普通:置打勾框(not self.普通显示)
		self.普通显示 = not self.普通显示
	elseif self.传送圈:事件判断() then
		-- self.传送圈:置打勾框(not self.传送圈显示)
		self.传送圈显示 = not self.传送圈显示
	elseif self.传送:事件判断() then
		-- self.传送:置打勾框(not self.传送显示)
		self.传送显示 = not self.传送显示
	elseif self.特殊:事件判断() then
		-- self.特殊:置打勾框(not self.特殊显示)
		self.特殊显示 = not self.特殊显示
   	elseif self.任务:事件判断() then
		-- self.任务:置打勾框(not self.任务显示)
		self.任务显示 = not self.任务显示
	 elseif self.商店:事件判断() then
		-- self.商店:置打勾框(not self.商店显示)
		self.商店显示 = not self.商店显示
	elseif self.全部:事件判断() then

		-- self.全部:置打勾框(not self.全部显示)
		self.全部显示 = not self.全部显示
		-- self.普通:置打勾框(self.全部显示)
		self.普通显示 = self.全部显示
		-- self.传送圈:置打勾框(self.全部显示)
		self.传送圈显示 = self.全部显示
		-- self.传送:置打勾框(self.全部显示)
		self.传送显示 = self.全部显示
		-- self.特殊:置打勾框(self.全部显示)
		self.特殊显示 = self.全部显示
		-- self.任务:置打勾框(self.全部显示)
		self.任务显示 = self.全部显示
		-- self.商店:置打勾框(self.全部显示)
		self.商店显示 = self.全部显示
	elseif self.自动寻路:事件判断() then
        tp.窗口.自动寻路:打开()
	end

	if self.鼠标 then
		if self.世界:事件判断() then
			self:打开()
			tp.窗口.世界大地图:打开()
		elseif self.关闭:事件判断() then
			self:打开()
		end
	end

	self.背景窗口:显示(self.x-16,self.y-30)
	self.自动寻路:显示(self.x-16,self.y-60)
	Picture.标题:显示(self.x+self.背景窗口.宽度/2-90,self.y-30)


    tp.字体表.华康字体:置颜色(0xFFFFFFFF):显示(self.x+self.背景窗口.宽度/2-90+Picture.标题.宽度/2-tp.字体表.描边字体:取宽度(MapData[tp.场景.数据.编号].名称)/2,
    	self.y-27,MapData[tp.场景.数据.编号].名称)

	if self.普通显示 or self.传送圈显示 or self.特殊显示 or self.传送显示 or self.任务显示 or  self.商店显示 or  self.全部显示 then
		self.小地图:置颜色(-6579301)
	else
		self.小地图:置颜色(4294967295)
	end
	self.小地图:显示(self.x,self.y-self.my)
	local jyjr = tp.场景.假人
	local rwjr = {}
	local csdz = tp.场景.传送
	local csqsj=tp.场景.传送圈数据

	--[[for i=1,#tp.任务Npc do
		if tp.任务Npc[i][1] == self.地图名称 then
			insert(rwjr,tp.任务Npc[i])
		end
	end]]
	if (self.传送圈显示 or self.全部显示) and  csqsj then
	    for i=1,#csqsj do
		 self.传送圈动画:显示(floor(csqsj[i][1]*20* self.偏移X + self.x),floor(csqsj[i][2] *20* self.偏移Y + 10 + self.y))

	   end
	end

  self.传送圈动画:更新(dt)


	pt:置颜色(4294967295)
	for n=1,#jyjr do
	
			jyjr[n].地图不显示 = nil
			jyjr[n].地图坐标 = {x=(floor(jyjr[n].坐标.x * self.偏移X + self.x-(pt:取宽度(jyjr[n].名称)/2)+6)),y=floor(jyjr[n].坐标.y * self.偏移Y + 4 + self.y)}
			-- for m=1,#rwjr do
			-- 	if rwjr[m][2] == jyjr[n].名称 and jyjr[n].事件~="神兽" then
			-- 		rwjr[m][5] = jyjr[n]
			-- 		rwjr[m][6] = jyjr[n].地图坐标.x
			-- 		rwjr[m][7] = jyjr[n].地图坐标.y
			-- 		jyjr[n].地图不显示 = 1
			-- 	end
			-- end
			if jyjr[n].事件=="神兽" then
			 	jyjr[n].地图不显示 = 1
			end 
			if (self.特殊显示 and jyjr[n].地图不显示 == nil and jyjr[n].事件 == "特殊" and jyjr[n].事件 ~= "传送" and jyjr[n].事件 ~= "任务" and jyjr[n].事件 ~= "商店")  then
				pt:置颜色(0xFF00FF00):显示(jyjr[n].地图坐标.x,jyjr[n].地图坐标.y,jyjr[n].名称)
			end
			if (self.传送显示 and jyjr[n].地图不显示 == nil and jyjr[n].事件 ~= "特殊" and jyjr[n].事件 == "传送" and jyjr[n].事件 ~= "任务"and jyjr[n].事件 ~= "商店")  then
				pt:置颜色(0xFFFFFF00):显示(jyjr[n].地图坐标.x,jyjr[n].地图坐标.y,jyjr[n].名称)
			end
			if (self.任务显示 and jyjr[n].地图不显示 == nil and jyjr[n].事件 ~= "特殊" and jyjr[n].事件 ~= "传送" and jyjr[n].事件 == "任务" and jyjr[n].事件 ~= "商店")  then
				pt:置颜色(0xFFFF0000):显示(jyjr[n].地图坐标.x,jyjr[n].地图坐标.y,jyjr[n].名称)
			end
			if (self.商店显示 and jyjr[n].地图不显示 == nil and jyjr[n].事件 ~= "特殊" and jyjr[n].事件 ~= "传送" and jyjr[n].事件 ~= "任务" and jyjr[n].事件 == "商店")  then
				pt:置颜色(0xFFFF8000):显示(jyjr[n].地图坐标.x,jyjr[n].地图坐标.y,jyjr[n].名称)
			end
			if (self.普通显示 and jyjr[n].地图不显示 == nil and jyjr[n].事件 ~= "特殊" and jyjr[n].事件 ~= "传送" and jyjr[n].事件 ~= "任务" and jyjr[n].事件 ~= "商店")  then
				pt:置颜色(0xFFFFFFFF):显示(jyjr[n].地图坐标.x,jyjr[n].地图坐标.y,jyjr[n].名称)
			end

	end
	pt:置颜色(0xFFFFFFFF)
	
	local v = self.x + self.背景窗口.宽度 - 48
	local v2 = self.y + self.背景窗口.高度 - 61

	self.关闭:显示(v+16,self.y-27)
	if self.宽度>= 500 then
		tp.字体表.普通字体:置颜色(0xFF80FFFF):显示(self.x +23,v2+ 2,"全部")
		tp.字体表.普通字体:置颜色(0xFFFFFFFF):显示(self.x +60+23,v2+ 2,"普通")
		tp.字体表.普通字体:置颜色(0xFF3737FF):显示(self.x +120+23,v2+ 2,"出口")
		tp.字体表.普通字体:置颜色(0xFFFFFF00):显示(self.x +180+23,v2+ 2,"传送")
		tp.字体表.普通字体:置颜色(0xFF00FF00):显示(self.x +240+23,v2+ 2,"特殊")
		tp.字体表.普通字体:置颜色(0xFFFF0000):显示(self.x +300+23,v2+ 2,"任务")
		tp.字体表.普通字体:置颜色(0xFFFF8000):显示(self.x +360+23,v2+ 2,"商店")
		self.世界:显示(v-34,v2,true,1)
		self.全部:显示(self.x-5+0*60,v2-3 ,true,1,nil,self.全部显示,2)
		self.普通:显示(self.x-5+1*60,v2-3,true,1,nil,self.普通显示,2)
		self.传送圈:显示(self.x-5+2*60,v2-3,true,1,self.传送圈显示,2)
		self.传送:显示(self.x-5+3*60,v2-3,true,1,nil,self.传送显示,2)
		self.特殊:显示(self.x-5+4*60,v2-3,true,1,nil,self.特殊显示,2)
	   	self.任务:显示(self.x-5+5*60,v2-3,true,1,nil,self.任务显示,2)
	   	self.商店:显示(self.x-5+6*60,v2-3,true,1,nil,self.商店显示,2)
	else
		tp.字体表.普通字体:置颜色(0xFF80FFFF):显示(v-34,self.y + 10,"全部")
		tp.字体表.普通字体:置颜色(0xFFFFFFFF):显示(v-34,self.y + 45,"普通")
		tp.字体表.普通字体:置颜色(0xFF3737FF):显示(v-34,self.y + 80,"出口")
		tp.字体表.普通字体:置颜色(0xFFFFFF00):显示(v-34,self.y + 115,"传送")
		tp.字体表.普通字体:置颜色(0xFF00FF00):显示(v-34,self.y + 150,"特殊")
		tp.字体表.普通字体:置颜色(0xFFFF0000):显示(v-34,self.y + 185,"任务")
		tp.字体表.普通字体:置颜色(0xFFFF8000):显示(v-34,self.y + 215,"商店")
		self.世界:显示(v-34,self.y + 250,true,1) 
		self.全部:显示(v-3,self.y + 3 ,true,1,nil,self.全部显示,2)
		self.普通:显示(v-3,self.y + 38 ,true,1,nil,self.普通显示,2)
		self.传送圈:显示(v-3,self.y + 73,true,1,nil,self.传送圈显示,2)
		self.传送:显示(v-3,self.y + 108,true,1,nil,self.传送显示,2)
		self.特殊:显示(v-3,self.y + 143,true,1,nil,self.特殊显示,2)
	   	self.任务:显示(v-3,self.y + 178,true,1,nil,self.任务显示,2)
	   	self.商店:显示(v-3,self.y + 213,true,1,nil,self.商店显示,2)
	end
		self.世界:更新(x,y)
		self.关闭:更新(x,y)

	if self.目标格子 ~= nil then
		self.终点:显示(self.x + self.目标格子.x,self.y + self.目标格子.y)

	end



	local 角色坐标x = floor(tp.角色坐标.x * self.偏移X + 13 + self.x)
	local 角色坐标y = floor(tp.角色坐标.y * self.偏移Y + 8 + self.y)
	local 鼠标坐标 = xys(floor((x - self.x)/(self.宽度/(tp.场景.地图.宽度/20))),floor((y - self.y)/(self.高度/(tp.场景.地图.高度/20))))
	self.间隔计次=0
	if tp.场景.人物.路径组 then
	    			for n=1,#tp.场景.人物.路径组 do
				self.间隔计次=self.间隔计次+1
				if self.间隔计次>=15 and n~=#tp.场景.人物.路径组 then
					self.临时坐标x=floor(tp.场景.人物.路径组[n].x*20 * self.偏移X  + 13 + self.x)
					self.临时坐标y=floor(tp.场景.人物.路径组[n].y*20 * self.偏移Y  -8 + self.y)
					self.小点:显示(self.临时坐标x,self.临时坐标y)
				self.间隔计次=0
				elseif n==#tp.场景.人物.路径组 then
					self.临时坐标x=floor(tp.场景.人物.路径组[n].x*20 * self.偏移X  + 13 + self.x)
					self.临时坐标y=floor(tp.场景.人物.路径组[n].y*20 * self.偏移Y -8 + self.y)
				end
			end
	end




	self.标记:显示(角色坐标x,角色坐标y)

	if  self.小地图:是否选中(x,y) and self.鼠标 and not tp.消息栏焦点 and (self.目标格子 == nil or (self.目标格子.x ~= x-self.x and self.目标格子.y ~= y-self.y))  then
		tp.提示:自定义(x+6,y+6,format("#Y/%d，%d", 鼠标坐标.x,鼠标坐标.y))
		if mouseb(0) and not tp.战斗中 and self.鼠标 and not self.选中人物 then
			tp.运行时间 = tp.运行时间 + 1
			self.窗口时间 = tp.运行时间
			local a = xys(floor(tp.角色坐标.x / 20),floor(tp.角色坐标.y / 20))
			local wb = tp.场景.地图.寻路:寻路(a,鼠标坐标,true)
			local bts = {x,y}
			tp.场景.跟随坐标 = {wb}
			self.目标格子 = {x=bts[1]-self.x,y=bts[2]-self.y}


			 客户端:发送数据(0,1,4,鼠标坐标.x.."*-*"..鼠标坐标.y,1)

		end
	end



end

function 场景类_小地图:检查点(x,y)
	if self.背景窗口:是否选中(x,y) or self.自动寻路:是否选中(x,y) then
		return true
	end
end

function 场景类_小地图:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_小地图:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_小地图




