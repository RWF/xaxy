--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:52
--======================================================================--
local 场景类_世界地图分类小地图 = class()
local tp,pt
local xys = 生成XY
local floor = math.floor
local format = string.format
local mouseb = 引擎.鼠标弹起
local wns = 引擎.文件是否存在
local insert = table.insert
local fgz = 分割字符
local tonumber = tonumber
local 当前地图12 = 0
local jrj = require("script/Role/假人")


--======================================================================--
function 场景类_世界地图分类小地图:初始化(根)
	self.ID = 1
	self.注释 = "世界地图分类小地图"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	self.记忆地图 = 0
	self.窗口时间 = 0
	self.目标格子 = xys()
	local 资源 = 根.资源
	local 按钮 = 根._按钮
	tp = 根
	self.迷你传送阵 = 资源:载入('JM.dll',"网易WDF动画",0x558897FF)
	self.普通 = 根._按钮.创建(资源:载入('JM.dll',"网易WDF动画",0xFF205590),0,0,1,true,true)
	self.普通:置根(根)
	self.普通:置偏移(-3,2)
	self.普通显示 = false
	self.偏移X = 0
	self.偏移Y = 0
	self.背景窗口 = tp._自适应.创建(0,1,0,0,3,9)
	self.窗口时间 = 0
	self.目标格子 = nil
	self.xx = 0
	self.yy = 0
	self.my = 0
	pt = tp.字体表.普通字体
	local 按钮 = 根._按钮
	local 自适应 = 根._自适应
	self.资源组 = {
		[2] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),--关闭按钮
	}
		self.资源组[2]:绑定窗口_(1)
end

function 场景类_世界地图分类小地图:打开(map)
	self.当前地图12 = map
	
	if not map or MapData[map].小地图 == nil then
		if self.可视 then
			self.qx = nil
			self.qy = nil
			self.可视 = false
		else
			tp.窗口.消息框:添加文本("此场景无法查看小地图")
		end
	else
		if self.记忆地图 ~= map then
			local 小地图宽度,小地图高度 = MapData[map].宽*20,MapData[map].高*20
			local ids = format("./img/%d.png",self.当前地图12)
			if wns(ids) and MapData[map].小地图 == nil then
				self.小地图 = tp.资源:载入(ids,"图片")
				self.my = 2
			else
			    self.小地图 = tp.资源:载入('smap.dll',"网易WDF动画",MapData[map].小地图)
			end
			self.宽度 = self.小地图:取宽度()

			self.高度 = self.小地图:取高度()
			local fg = {}
			fgz(tostring(self.高度),fg)
			fg = tonumber(fg[3])
			local v = 27
			if fg == 1 or fg == 3 or fg == 5 or fg == 7 or fg == 9 then
				v = 26
			end
			self.x = self.x or floor((全局游戏宽度 - self.宽度) /2)
			self.y = self.y or floor((全局游戏高度 - self.高度) /2)
			self.qx = nil
			self.qy = nil
			if self.宽度>= 500 then
				self.背景窗口:置宽高(self.宽度+v+2,self.高度+v+28)
			else
			    self.背景窗口:置宽高(self.宽度+84,self.高度+v)
			end
			self.偏移X = (self.宽度 - 20) / 小地图宽度
			self.偏移Y = (self.高度 - 20)/ 小地图高度
			self.记忆地图 = map
			self.可视 = true
			tp.运行时间 = tp.运行时间 + 1
			self.窗口时间 = tp.运行时间
			insert(tp.窗口_,self)
		elseif self.记忆地图 == map and self.可视 then
			self.qx = nil
			self.qy = nil
		    self.可视 = false
		elseif self.可视 == false and self.记忆地图 == map then
			self.qx = nil
			self.qy = nil
			self.可视 = true
			insert(tp.窗口_,self)
			tp.运行时间 = tp.运行时间 + 1
			self.窗口时间 = tp.运行时间
		end
	end
---===========
	self.假人QQ = {}


	self.普通显示 = false
	self.普通:置打勾框(false)
	return false
end

function 场景类_世界地图分类小地图:显示(dt,x,y)
	   self.资源组[2]:更新(x,y)
	if self.鼠标 then
		if self.资源组[2]:事件判断() then
			self:打开()
		end
	end
	self.焦点 = false
	self.普通:更新(x,y)
	if self.普通:事件判断() then
		self.普通:置打勾框(not self.普通显示)
		self.普通显示 = not self.普通显示
	end
	self.背景窗口:显示(self.x-16,self.y-17)
	if self.普通显示  then
		self.小地图:置颜色(-6579301)
	else
		self.小地图:置颜色(4294967295)
	end
	self.小地图:显示(self.x,self.y-self.my)
--########################################################?自己修改?##########################################
	if self.普通显示 then--########################################################?自己修改?##########################################
		for i=1,#self.假人QQ do

                 pt:置颜色(0xFFFFFFFF)--白色文字 闲人类
                 pt:显示(floor(self.假人QQ[i].X*20 * self.偏移X-11 + self.x),floor(self.假人QQ[i].Y *20* self.偏移Y+3 + self.y),self.假人QQ[i].名称)


		end
	end
--########################################################?自己修改?########以下是鼠标点击活动##################################
	local v = self.x + self.背景窗口.宽度 - 48
	local v2 = self.y + self.背景窗口.高度 - 48
	self.资源组[2]:显示(v+16,self.y-16)--关闭按钮
	if self.宽度>= 500 then
		self.普通:显示(self.x-5,v2-3,true)--全部选择框
		tp.字体表.普通字体:置颜色(0xFF03A89E):显示(self.x +23,v2+2 ,"全部")
	else
		self.普通:显示(v-32,self.y + 13,true)--全部选择框
		tp.字体表.普通字体:置颜色(0xFF03A89E):显示(v-5,self.y + 20,"全部")
	end
end

function 场景类_世界地图分类小地图:检查点(x,y)
	if self.背景窗口:是否选中(x,y) then
		return true
	end
end

function 场景类_世界地图分类小地图:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 场景类_世界地图分类小地图:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 场景类_世界地图分类小地图