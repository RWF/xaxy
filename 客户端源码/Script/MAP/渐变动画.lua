-- @Author: Free
-- @Date:   2019-02-23 20:42:28
-- @Last Modified by:   Free
-- @Last Modified time: 2020-11-21 17:37:42

local 渐变动画 = {}
local 渲染目标 = require "gge纹理类"():渲染目标(全局游戏宽度,全局游戏高度)

local 精灵 = require "gge精灵类"(渲染目标)

local 可见 = false
local 透明度=255
function 渐变动画:初始化()
    引擎.截图到纹理(渲染目标)
    可见=true
    透明度=255
    return self
end

function 渐变动画:更新(dt)
    if 可见 then
        if 透明度>=5 then
            透明度 = 透明度 -dt*170
        else
             引擎.场景.隐藏UI = false
             引擎.场景.恢复UI = false
            可见 = false

        end
    end
end

function 渐变动画:显示(x,y)
    if 可见 then
        精灵:置颜色(bit.lshift(透明度, 24)+0xFFFFFF)
            :显示(0,0)
    end
end

return 渐变动画