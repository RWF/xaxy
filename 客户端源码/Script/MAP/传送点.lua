--======================================================================--
-- @作者: QQ381990860
-- @创建时间:   2019-12-03 02:17:19
-- @Last Modified time: 2021-11-12 01:31:52
--======================================================================--
local tp


local 场景类_传送点 = class()

function 场景类_传送点:初始化(x,y,切换)
	if tp == nil then
		tp = 引擎.场景
	end
	self.坐标 = 生成XY(x*20,y*20)
	self.切换 = 切换
	 self.发送时间=0
end

function 场景类_传送点:更新(dt,x,y,pys)
	if 取两点距离(tp.角色坐标,self.坐标) < 800 and (tp.角色坐标.x >= self.坐标.x-26 and tp.角色坐标.x <= self.坐标.x + 44 and tp.角色坐标.y >= self.坐标.y-9 and tp.角色坐标.y <= self.坐标.y + 45) then
		local a = self.切换
		if a ~= nil then
			local kcs = true
			local sl = #tp.场景.人物.路径组 or 0
			if sl > 0 then
				local dz = 生成XY(tp.场景.人物.路径组[sl].x*20,tp.场景.人物.路径组[sl].y*20)
				if 取两点距离(tp.角色坐标,dz) >= 800 then -- 若人物走到传送点阵时，人物与寻路目标点大于屏幕范围则不进行传送
					kcs = false
				end
			end
			if kcs then
				 if 全局时间-self.发送时间>=4 then
					self.发送时间=全局时间

					客户端:发送数据(self.切换,3,4,"")
				end
			end
		end
		return
	end
end

function 场景类_传送点:显示(dt,x,y,pys)
	if 取两点距离(tp.角色坐标,self.坐标) < 800 then
		tp.传送点:显示(self.坐标 + pys)
	end
end

return 场景类_传送点

