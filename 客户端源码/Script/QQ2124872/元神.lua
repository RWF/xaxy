-- @Author: 80后痴汉
-- @Date:	2021-10-25 00:54:26
-- @Last Modified by:	作者QQ2124872
-- @Last Modified time: 2021-10-25 10:34:10
-- @GGELUA游戏爱好者基地 https://www.ggelua.cc
-- @GGELUA游戏爱好者基地 https://bbs.ggelua.cc
-- @GGELUA游戏爱好者交流Q群 560529474
-- @GGELUA声明：编写此脚本只因个人兴趣爱好，如若放出，仅供个人用于学习、研究;不得用于商业用途。





local 系统类_元神 = class()
local floor = math.floor
local tp,zts,zt
local format = string.format
local insert = table.insert

function 系统类_元神:初始化(根)
	self.ID = 10003
	self.x = 120
	self.y = 120
	self.xx = 0
	self.yy = 0
	self.注释 = "元 神 突 破"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	tp = 根
	self.窗口时间 = 0
	zts = tp.字体表.道具字体
	zt = tp.字体表.描边字体

	self.显示状态=1
	self.突破=1
	self.控件类 = require("ggeui/加载类")()
	local 总控件 = self.控件类:创建控件('序号控件')
	总控件:置可视(true,true)
	self.输入框 = 总控件:创建输入("卡号输入",0,0,180,14)
	self.输入框:置可视(false,false)
	self.输入框:置限制字数(30)
	self.输入框:屏蔽快捷键(true)
	self.输入框:置光标颜色(-16777216)
	self.输入框:置文字颜色(-16777216)

	self.数量框 = 总控件:创建输入("数量输入",0,0,180,14)
	self.数量框:置可视(false,false)
	self.数量框:置限制字数(10)
	self.数量框:置数字模式()
	self.数量框:屏蔽快捷键(true)
	self.数量框:置光标颜色(-16777216)
	self.数量框:置文字颜色(-16777216)
	self.门派 = {"方寸山","龙宫","女儿村","神木林","大唐官府","化生寺","阴曹地府","盘丝洞","狮驼岭","魔王寨","普陀山","天宫","凌波城","五庄观","天机城","女魃墓","花果山","无底洞"}
	
	
	local wz = require("gge文字类")
	self.腾祥铭宋简小 = wz.创建(程序目录.."Data/pic/txmsj.ttf",14,false,true,true)
	self.腾祥铭宋简小:置描边颜色(0xFFFFFFFF)
	self.选择门派 = ""
end

function 系统类_元神:打开(数据)
	if self.可视 then
		self.可视 = false
		self.资源组=nil
		return
	else
		insert(tp.窗口_,self)
		local 资源 = tp.资源
		local 按钮 = tp._按钮
		local 自适应 = tp._自适应
		self.动态1 = 资源:载入('80wl.dll',"网易WDF动画",0x103FA0FF)
		self.动态2 = 资源:载入('80wl.dll',"网易WDF动画",0x8E28D6F3)
		self.动态3 = 资源:载入('80wl.dll',"网易WDF动画",0x76F0D6C6)
		self.动态4 = 资源:载入('80wl.dll',"网易WDF动画",0x7F01F6E7)
		self.动态5 = 资源:载入('80wl.dll',"网易WDF动画",0x71321C77)
		self.聊天 = 资源:载入('80wl.dll',"网易WDF动画",0x98AAC807)
		self.资源组 = {
			[1] = 资源:载入('80wl.dll',"网易WDF动画",0x10001001),
			[2] = 资源:载入('80wl.dll',"网易WDF动画",0x10005000),
			[3] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10002000),0,0,4,true,true,"个人元神突破"),
			[4] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10002000),0,0,4,true,true,"所有门派特性"),
			[7] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10003000),0,0,4,true,true),
			 -----------门派 -----------
			[9]  = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  方寸山"
			[10] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  龙宫"
			[11] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  女儿村"
			[12] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  神木林"
			[13] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  大唐"
			[14] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  化生寺"
			[15] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  地府"
			[16] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  盘丝洞"
			[17] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  狮驼岭"
			[18] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  魔王寨"
			[19] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  普陀山"
			[20] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  天宫"
			[21] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  凌波城"
			[22] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  五庄观"
			[23] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  天机城"
			[24] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  女魃墓"
			[25] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  花果山"
			[26] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004000),0,0,4,true,true),--,"  无底洞"
			
			[27] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004001),0,0,4,true,true),--,"  无底洞"
			[28] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004001),0,0,4,true,true),--,"  无底洞"
			[29] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004001),0,0,4,true,true),--,"  无底洞"
			[30] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004001),0,0,4,true,true),--,"  无底洞"
			[31] = 按钮.创建(tp.资源:载入('80wl.dll',"网易WDF动画",0x10004001),0,0,4,true,true),--,"  无底洞"
			


		}
		self.方寸山 = 资源:载入(程序目录..'Data/pic/fc.png',"图片")
		self.龙宫 = 资源:载入(程序目录..'Data/pic/lg.png',"图片")
		self.女儿村 = 资源:载入(程序目录..'Data/pic/ne.png',"图片")
		self.神木林 = 资源:载入(程序目录..'Data/pic/sml.png',"图片")
		self.大唐官府 = 资源:载入(程序目录..'Data/pic/dt.png',"图片")
		self.化生寺 = 资源:载入(程序目录..'Data/pic/hss.png',"图片")
		self.阴曹地府 = 资源:载入(程序目录..'Data/pic/df.png',"图片")
		self.盘丝洞 = 资源:载入(程序目录..'Data/pic/psd.png',"图片")
		self.狮驼岭 = 资源:载入(程序目录..'Data/pic/stl.png',"图片")
		self.魔王寨 = 资源:载入(程序目录..'Data/pic/mw.png',"图片")
		self.普陀山 = 资源:载入(程序目录..'Data/pic/pts.png',"图片")
		self.天宫 = 资源:载入(程序目录..'Data/pic/tg.png',"图片")
		self.凌波城 = 资源:载入(程序目录..'Data/pic/lbc.png',"图片")
		self.五庄观 = 资源:载入(程序目录..'Data/pic/wzg.png',"图片")
		self.天机城 = 资源:载入(程序目录..'Data/pic/tjc.png',"图片")
		self.女魃墓 = 资源:载入(程序目录..'Data/pic/nbm.png',"图片")
		self.花果山 = 资源:载入(程序目录..'Data/pic/hgs.png',"图片")
		self.无底洞 = 资源:载入(程序目录..'Data/pic/wdd.png',"图片")


		self.数据=数据
		tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
		self.可视 = true
		self.显示状态 = 1
		self.查看阶段 = 1
		self.随机动画 = 取随机数(1,5)
	end
end

function 系统类_元神:刷新(数据)
	self.数据 = 数据
	self.随机动画 = 取随机数(1,5)
end


function 系统类_元神:显示(dt,x,y)
	self.焦点 = false
	self.动态1:更新(dt)
	self.动态2:更新(dt)
	self.动态3:更新(dt)
	self.动态4:更新(dt)
	self.动态5:更新(dt)
	self.资源组[3]:更新(x,y,self.显示状态~=1)
	self.资源组[4]:更新(x,y,self.显示状态~=2)
	self.资源组[9]:更新(x,y,self.显示状态~=3)
	self.资源组[10]:更新(x,y,self.显示状态~=4)
	self.资源组[11]:更新(x,y,self.显示状态~=5)
	self.资源组[12]:更新(x,y,self.显示状态~=6)
	self.资源组[13]:更新(x,y,self.显示状态~=7)
	self.资源组[14]:更新(x,y,self.显示状态~=8)
	self.资源组[15]:更新(x,y,self.显示状态~=9)
	self.资源组[16]:更新(x,y,self.显示状态~=10)
	self.资源组[17]:更新(x,y,self.显示状态~=11)
	self.资源组[18]:更新(x,y,self.显示状态~=12)
	self.资源组[19]:更新(x,y,self.显示状态~=13)
	self.资源组[20]:更新(x,y,self.显示状态~=14)
	self.资源组[21]:更新(x,y,self.显示状态~=15)
	self.资源组[22]:更新(x,y,self.显示状态~=16)
	self.资源组[23]:更新(x,y,self.显示状态~=17)
	self.资源组[24]:更新(x,y,self.显示状态~=18)
	self.资源组[25]:更新(x,y,self.显示状态~=19)
	self.资源组[26]:更新(x,y,self.显示状态~=20)
	
	self.资源组[27]:更新(x,y,self.查看阶段~=1)
	self.资源组[28]:更新(x,y,self.查看阶段~=2)
	self.资源组[29]:更新(x,y,self.查看阶段~=3)
	self.资源组[30]:更新(x,y,self.查看阶段~=4)
	self.资源组[31]:更新(x,y,self.查看阶段~=5)


	self.资源组[1]:显示(self.x,self.y)
	self.资源组[3]:显示(self.x + 28,self.y + 28)
	self.资源组[4]:显示(self.x + 128,self.y + 28)

	zt:显示(self.x+269,self.y+4,self.注释)
	
	if self.资源组[3]:事件判断() then
		self.显示状态=1
		self.选择门派 = ""
		self.资源组[3]:显示(self.x + 28,self.y + 28,nil,nil,nil,self.显示状态==1,3)
	elseif self.资源组[4]:事件判断() then
		self.显示状态=2
		self.选择门派 = ""
		self.资源组[4]:显示(self.x + 128,self.y + 28,nil,nil,nil,self.显示状态==2,3)
	elseif self.资源组[7]:事件判断() then
		发送数据(112)
	end
	
	if self.显示状态 == 1 then
		self.资源组[2]:显示(self.x + 81,self.y + 63)
		self[self.数据.门派]:显示(self.x+349,self.y+62)
		if self.数据.层数 == 0 then
			tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+260,self.y+170,"尚未突破元神")
			local 突破消耗 = "本次突破需消耗：\n\n"
			for n = 1,3 do
				if SpiritData[self.数据.门派]["突破"..(self.数据.层数+1).."介消耗类型"..n] ~= 0 then
					突破消耗 = 突破消耗 .. SpiritData[self.数据.门派]["突破"..(self.数据.层数+1).."介消耗名称"..n] .." X " .. SpiritData[self.数据.门派]["突破"..(self.数据.层数+1).."介消耗数量"..n] .."\n\n"
				end
			end
			tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+260,self.y+200,突破消耗)
		else
			local 当前元神 = "当前元神 "..self.数据.层数.."阶   \n\n附加效果：\n\n"
			for n =1, 4 do
				if SpiritData[self.数据.门派]["效果"..n.."说明"] ~= "无" then
					if SpiritData[self.数据.门派][self.数据.层数.."介效果"..n.."下限"] > 0 or SpiritData[self.数据.门派][self.数据.层数.."介效果"..n.."上限"] > 0 then
						当前元神 = 当前元神 .. SpiritData[self.数据.门派]["效果"..n.."说明"] .. " "..SpiritData[self.数据.门派][self.数据.层数.."介效果"..n.."下限"] .. " ~ "..SpiritData[self.数据.门派][self.数据.层数.."介效果"..n.."上限"] .. "\n\n"
					end
				end
			end
			self.腾祥铭宋简小:置颜色(0xFF5e2612):显示(self.x+260,self.y+170,当前元神)
			if self.数据.层数 < 5 then
				local 突破消耗 = "本次突破需消耗：\n\n"
				for n = 1,3 do
					if SpiritData[self.数据.门派]["突破"..(self.数据.层数+1).."介消耗类型"..n] ~= 0 then
						突破消耗 = 突破消耗 .. SpiritData[self.数据.门派]["突破"..(self.数据.层数+1).."介消耗名称"..n] .." X " .. SpiritData[self.数据.门派]["突破"..(self.数据.层数+1).."介消耗数量"..n] .."\n\n"
					end
				end
				self.腾祥铭宋简小:置颜色(0xFF5e2612):显示(self.x+450,self.y+205,突破消耗)
			end
		end
		if self.数据.层数 < 5 then
			self.资源组[7]:更新(x,y)
			self.资源组[7]:显示(self.x + 179,self.y + 72+self.数据.层数*41)
		end
		local 阶段 = {"一阶","二阶","三阶","四阶","五阶"}
		for i =1,5 do
			local 是否突破 = "未突破"
			if self.数据.层数 + 1 == i then
				是否突破 = "可突破"
			elseif self.数据.层数 + 1 > i then
				是否突破 = "已突破"
			end
			tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+128,self.y+70+(i-1)*41,阶段[i])
			tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+128,self.y+87+(i-1)*41,是否突破)
		end
	else
		self.xxx=0
		self.yyy=0
		for i=9,26 do
			if self.yyy>=18 then
			  self.yyy=0
			end
			self.资源组[i]:显示(self.x+85+self.xxx,self.y+67+self.yyy*16)
			self.yyy=self.yyy+1
			if self.资源组[i]:事件判断() then
				self.查看阶段 = 1
				self.显示状态=i-6
				self.选择门派 = self.门派[i-8]
				self.资源组[i]:显示(self.x + 85,self.y + 67+16*(i-9),nil,nil,nil,self.显示状态==i-6,3)
			end
		end
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68,"方  寸  山")
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68+16*1,"龙      宫")
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68+16*2,"女  儿  村")
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68+16*3,"神  木  林")
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68+16*4,"大      唐")
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68+16*5,"化  生  寺")
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68+16*6,"地      府")
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68+16*7,"盘  丝  洞")
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68+16*8,"狮  驼  岭")
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68+16*9,"魔  王  寨")
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68+16*10,"普  陀  山")
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68+16*11,"天      宫")
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68+16*12,"凌  波  城")
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68+16*13,"五  庄  观")
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68+16*14,"天  机  城")
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68+16*15,"女  魃  墓")
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68+16*16,"花  果  山")
		tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+118,self.y+68+16*14,"无  底  洞")
	
		if self.选择门派 ~= "" then
			for n = 1,5 do
				self.资源组[n+26]:显示(self.x + 260 + (n-1)*71,self.y+170)
				tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+265 + (n-1)*71,self.y+172,n.."介效果")
				if self.资源组[n+26]:事件判断() then
					self.查看阶段 = n
				end
			end
			local 显示文本 = self.选择门派..self.查看阶段.."介元神效果：\n\n"
			for n =1, 4 do
				if SpiritData[self.选择门派] ~= nil and SpiritData[self.选择门派]["效果"..n.."说明"] ~= "无" then
					if SpiritData[self.选择门派][self.查看阶段.."介效果"..n.."下限"] > 0 or SpiritData[self.选择门派][self.查看阶段.."介效果"..n.."上限"] > 0 then
						显示文本 = 显示文本 .. SpiritData[self.选择门派]["效果"..n.."说明"] .. " "..SpiritData[self.选择门派][self.查看阶段.."介效果"..n.."下限"] .. " ~ "..SpiritData[self.选择门派][self.查看阶段.."介效果"..n.."上限"] .. "\n\n"
					end
				end
			end
			tp.字体表.普通字体:置颜色(0xFF5e2612):显示(self.x+260,self.y+200,显示文本)
			if self[self.选择门派] ~= nil then
				self[self.选择门派]:显示(self.x+340,self.y+65)
			end
		end
		self["动态"..self.随机动画]:显示(self.x+500,self.y+97+(5-1)*41)
	end
end

function 系统类_元神:检查点(x,y)
	if self.资源组 ~= nil and self.资源组[1]:是否选中(x,y)  then
		return true
	end
end

function 系统类_元神:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 系统类_元神:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 系统类_元神