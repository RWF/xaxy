-- @Author: 作者QQ414628710
-- @Date:   2021-11-24 01:43:03
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2021-11-24 23:43:28
-- @Author: 80后痴汉
-- @Date:	2021-10-25 00:54:26
-- @Last Modified by:	作者QQ2124872
-- @Last Modified time: 2021-10-25 10:34:10
-- @GGELUA游戏爱好者基地 https://www.ggelua.cc
-- @GGELUA游戏爱好者基地 https://bbs.ggelua.cc
-- @GGELUA游戏爱好者交流Q群 560529474
-- @GGELUA声明：编写此脚本只因个人兴趣爱好，如若放出，仅供个人用于学习、研究;不得用于商业用途。





local 挂机系统 = class()
qz = math.floor
local tp,zts,zts1
local tonumbe = tonumber
local insert = table.insert
local floor = math.floor
local box = 引擎.画矩形
local mouse = 引擎.鼠标弹起
local xys = 生成XY

function 挂机系统:初始化(根)
	self.ID = 1002
	self.xx = 0
	self.yy = 0
	self.注释 = "挂机系统"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	self.触发延时 = os.time()
	tp = 根
	local 资源 = tp.资源
	local 按钮 =  tp._按钮
	local 自适应 = tp._自适应
	
	self.背景 = 资源:载入(程序目录..'Data/pic/挂机背景.png',"图片")
		
	self.x = qz(全局游戏宽度/2-self.背景:取宽度()/2)
	self.y = qz(全局游戏高度/2-self.背景:取高度()/2)
	
	local 小型选项栏 = 根._小型选项栏
	self.下拉框 = {
		[1] = 自适应.创建(3,1,80,19,1,3,nil,18),--下拉框开始
		[2] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),
		[3] = 小型选项栏.创建(自适应.创建(6,1,80,66,3,9),"self.人物自动"),--下拉框结束
		
		[4] = 自适应.创建(3,1,80,19,1,3,nil,18),--下拉框开始
		[5] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),
		[6] = 小型选项栏.创建(自适应.创建(6,1,80,66,3,9),"self.宠物自动"),--下拉框结束
	}
	
	self.按钮组 = {}
	self.按钮组.开启 = 按钮.创建(自适应.创建(101,4,72,22,1,3),0,0,4,true,true,"开启功能")
	self.按钮组.购买 = 按钮.创建(自适应.创建(101,4,72,22,1,3),0,0,4,true,true,"兑换时间")
	local wz = require("gge文字类")
	self.菜单文字 = wz.创建(程序目录.."Data/pic/txmsj.ttf",15,false,false,true)
	self.介绍文字 = wz.创建(程序目录.."Data/pic/txmsj.ttf",14,false,false,true)
	
	self.介绍文本 = {}
	self.托管数据 = {}
	
	self.系统数据 = {}
end


function 挂机系统:打开(内容)
	if self.可视 then
		self.可视 = false
	else
		if 内容 == nil then
			return
		end
		insert(tp.窗口_,self)
	    tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
	    self.可视 = true
		self.x = qz(全局游戏宽度/2-self.背景:取宽度()/2)
		self.y = qz(全局游戏高度/2-self.背景:取高度()/2)
		self.系统数据 = 内容
		self.菜单 = {}
		self.菜单包围盒 = {}
		self.选中菜单 = 0
		self.介绍文本 = {}
		if self.托管数据 == nil then
			self.托管数据 = {}
		end
		for n,v in pairs(self.系统数据) do
			self.菜单[#self.菜单+1] = n
			self.菜单包围盒[#self.菜单] = require("gge包围盒")(0,0,76,25)
			-- self.菜单包围盒[#self.菜单]:绑定窗口_(self.ID)
			self.托管数据[n] = v.状态.开启
			
			self.介绍文本[#self.菜单] = tp._丰富文本(340,250,tp.字体表.普通字体)
			self.介绍文本[#self.菜单]:清空()
			self.介绍文本[#self.菜单]:添加文本("#W/【说明】"..v.功能介绍)
		end
		self.选中菜单 = 1
		self.介绍行数 = 0
		self:加载按钮()
	end
end

function 挂机系统:刷新开关(内容)

	if self.托管数据 == nil then
		self.托管数据 = {}
	end
	for n,v in pairs(内容) do
		self.托管数据[n] = v.开启
	end
end

function 挂机系统:更新(内容)
	self.系统数据 = 内容
	if self.托管数据 == nil then
		self.托管数据 = {}
	end
	for n,v in pairs(self.系统数据) do
		self.托管数据[n] = v.状态.开启
	end
	self:加载按钮()
end


function 挂机系统:加载按钮()
	if #self.菜单 > 0 then
		if self.系统数据[self.菜单[self.选中菜单]] ~= nil then
			if self.系统数据[self.菜单[self.选中菜单]].状态.开启 then
				self.按钮组.开启:置文字("关闭功能")
			else
				self.按钮组.开启:置文字("开启功能")
			end
			if self.系统数据[self.菜单[self.选中菜单]].开启消耗 <= 0 then
				self.按钮组.购买:置文字("兑换时间")
			else
				self.按钮组.购买:置文字("兑换时间")
			end
		end
	end
end

function 挂机系统:是否更新购买按钮()
	if self.系统数据[self.菜单[self.选中菜单]].开启消耗 > 0 and (self.系统数据[self.菜单[self.选中菜单]].状态.时间 < os.time() or self.系统数据[self.菜单[self.选中菜单]].重复购买) then
		return true
	end
	return false
end

function 挂机系统:显示(dt,x,y)
	self.焦点 = false
	self.背景:显示(self.x,self.y)
	if self.按钮组 ~= nil and self.菜单[self.选中菜单] ~= nil and self.系统数据[self.菜单[self.选中菜单]] ~= nil then
		if self.按钮组.购买 ~= nil then
			self.按钮组.购买:更新(x,y,self:是否更新购买按钮())
		end
		self.按钮组.开启:更新(x,y)
	end
	if #self.菜单 > 0 then
		local 行 = 0
		for i = 1,#self.菜单 do
			if self.菜单包围盒[i] ~= nil then
				self.菜单包围盒[i]:置坐标(self.x+1,self.y+35+行*30)
			end
			if self.选中菜单 == i then
				box(self.x+1,self.y+35+行*30,self.x+1+71,self.y+35 + 25+ 行*30,-10790181)
			end
			self.菜单文字:置颜色(0xffffffff):显示(self.x+5,self.y+38+行*30,self.菜单[i])
			self.菜单文字:置颜色(0xffffffff):显示(self.x+5,self.y+50+行*30,"--------")
			if self.菜单包围盒[i]:检查点(x,y) then
				if mouse(0) and self.选中菜单 ~= i then
					self.选中菜单 = i
					self:加载按钮()
				end
			end
			行 = 行 + 1
		end
	end
	self.介绍行数 = 0
	if self.菜单[self.选中菜单] ~= nil and self.系统数据[self.菜单[self.选中菜单]] ~= nil then
		self.介绍文本[self.选中菜单]:显示(self.x+85,self.y+38)
		local 介绍长度 = string.len(self.系统数据[self.菜单[self.选中菜单]].功能介绍)
		self.介绍行数 = 1 + self.介绍行数 + qz(介绍长度 / 48)
		self.介绍文字:置颜色(0xff00FF00):显示(self.x+85,self.y+38+self.介绍行数*22,self:购买介绍(self.菜单[self.选中菜单]))
		if self.系统数据[self.菜单[self.选中菜单]].开启消耗 > 0 then
			self.介绍文字:置颜色(0xffffffff):显示(self.x+85,self.y+38+self.介绍行数*22,self:剩余时间(self.菜单[self.选中菜单]))
			if self.系统数据[self.菜单[self.选中菜单]].当前货币 ~= nil then
				self.介绍文字:置颜色(0xff00FF00):显示(self.x+85,self.y+38+self.介绍行数*22,"当前剩余"..self.系统数据[self.菜单[self.选中菜单]].消耗类型.."："..self.系统数据[self.菜单[self.选中菜单]].当前货币)
				self.介绍行数 = self.介绍行数 + 1
			end
		end
		self.按钮组.购买:显示(self.x+165,self.y+38+self.介绍行数*22)
		self.按钮组.开启:显示(self.x+285,self.y+38+self.介绍行数*22)
		if self.按钮组.购买:事件判断() then
			发送数据(110,{动作=2,功能=self.菜单[self.选中菜单]})
		elseif self.按钮组.开启:事件判断() then
			if self.菜单[self.选中菜单] == "自动巡逻" and tp.场景.场景最低等级 == nil then
            	tp.提示:写入("#Y/自动巡逻开启失败，当前场景不是野外地图。")
				return
			end
			发送数据(110,{动作=3,功能=self.菜单[self.选中菜单]})
		end
	end
end

function 挂机系统:剩余时间(名称)
	local 介绍 = ""
	if self.系统数据[名称] ~= nil and self.系统数据[名称].开启消耗 > 0 and self.系统数据[名称].状态 ~= nil then
		local 时间 = qz((self.系统数据[名称].状态.时间-os.time())/60)
		if 时间 < 0 then
			时间 = 0
		end
		介绍 = "剩余时间：本功能剩余使用时间为"..时间.."分钟。"
		self.介绍行数 = self.介绍行数 + 2
	else
		self.介绍行数 = self.介绍行数 + 2
	end
	return 介绍
end

function 挂机系统:购买介绍(名称)
	local 介绍 = ""
	if self.系统数据[名称] ~= nil and self.系统数据[名称].开启消耗 > 0 then
		local 时间 = qz(self.系统数据[名称].持续时间/60)
		介绍 = "收费说明：每消耗"..self.系统数据[名称].开启消耗..self.系统数据[名称].消耗类型.."可兑换"..时间.."分钟时间.\n从兑换起开始计时。"
		self.介绍行数 = self.介绍行数 + 2
	else
		介绍 = "收费说明：本功能免费开启，无需另外购买时间。"
		self.介绍行数 = self.介绍行数 + 2
	end
	return 介绍
end


function 挂机系统:自动巡逻处理()
	if not tp.战斗中 then
		if tp.场景.人物.移动 then
			tp.场景.人物:开始移动()
		else
			local 地图宽度 = qz(tp.场景.地图.宽度/20)
			local 地图高度 = qz(tp.场景.地图.高度/20)
			local 路径 = {}
			for i = 1,地图宽度 do
				for n = 1,地图高度 do
					local xy = {x=i,y=n}
					local 允许=true
					if xy.x<20 or xy.y<20 or xy.x>=tp.场景.地图.宽度-20 or xy.y>=tp.场景.地图.高度-20 then
						允许=false
					end
					if 允许 and tp.场景.地图.寻路:检查点(xy.x,xy.y) then
						路径[#路径+1] = {x=xy.x,y=xy.y}
					end
				end
			end
			local 随机点 = 取随机数(1,#路径)
			local 格子 = 路径[随机点]
			self:申请移动(格子)
		end
	end
end


function 挂机系统:刷新处理(功能)
	if 功能 == "自动巡逻" then
		self:自动巡逻处理()
	elseif 功能 == "自动抓鬼" then
		self:自动抓鬼处理()
	end
end



function 挂机系统:自动抓鬼处理()
	if tp.战斗中 == false then
		if tp.场景.人物.移动 then
			tp.场景.人物:开始移动()
		else
			if self.触发延时 <= os.time() then
				发送数据(110,{动作=4})
			end
		end
	end
end


function 挂机系统:申请移动(内容)
	local 格子 = 内容
	local a = xys(floor(tp.角色坐标.x / 20),floor(tp.角色坐标.y / 20))
	tp.场景.人物.路径组 = tp.场景.地图.寻路:寻路(a,格子)
	tp.场景.跟随坐标 = {tp.场景.人物.路径组}
	if tp.场景.人物.路径组 ~= nil and tp.场景.人物.路径组[1] ~= nil then
		tp.场景.人物.延时 = 20
		--local v = {
		--	x = floor(x - tp.场景.人物.pys.x),
		--	y = floor(y - tp.场景.人物.pys.y),
		--	ani = tp.资源:载入('JM.dll',"网易WDF动画",0x0D98AC0A)}
		--	insert(tp.场景.人物.鼠标点击动画组,v)
		客户端:发送数据(0,1,4,格子.x.."*-*"..格子.y,1)
	else
		tp.场景.人物.路径组 = {}
	end
end

















function 挂机系统:检查点(x,y)
	if self.可视 and self.背景:是否选中(x,y)  then
		return true
	end
end

function 挂机系统:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 挂机系统:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy

	end
end


return 挂机系统