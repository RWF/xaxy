-- @Author: 80后痴汉
-- @Date:	2021-10-25 00:54:26
-- @Last Modified by:	作者QQ2124872
-- @Last Modified time: 2021-10-25 10:34:10
-- @GGELUA游戏爱好者基地 https://www.ggelua.cc
-- @GGELUA游戏爱好者基地 https://bbs.ggelua.cc
-- @GGELUA游戏爱好者交流Q群 560529474
-- @GGELUA声明：编写此脚本只因个人兴趣爱好，如若放出，仅供个人用于学习、研究;不得用于商业用途。





local 助战技能学习 = class()

local floor = math.floor
local bw = require("gge包围盒")(0,0,164,37)
local box = 引擎.画矩形
local tp,zys,zts,zts1,zts2
local ARGB = ARGB
local insert = table.insert

function 助战技能学习:初始化(根)
	self.ID = 22
	self.x = 372
	self.y = 105
	self.xx = 0
	self.yy = 0
	self.注释 = "技能学习"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	self.介绍文本 = 根._丰富文本(150,150,根.字体表.普通字体)
	self.介绍文本1 = 根._丰富文本(150,150,根.字体表.普通字体)
	tp = 根
	zts = tp.字体表.普通字体
	zts1 = tp.字体表.普通字体__
	zts2 = tp.字体表.描边字体
	self.窗口时间 = 0
	self.选中 = nil
	self.选中1 = nil
	self.选中包含技能加入 = nil
	self.师门技能 = nil
	self.包含技能 = nil
	self.本次需求 = nil
	self.刷新文本 = false
	zys = tp.资源
end

function 助战技能学习:打开(内容)
	if self.可视 then
		self.介绍文本:清空()
		self.选中师门技能 = nil
		self.选中包含技能 = nil
		self.选中包含技能加入 = nil
		self.包含技能 = nil
		self.刷新文本 = false
		self.可视 = false
		self.资源组=nil
		return
	else
		insert(tp.窗口_,self)
		self.助战 = 内容
		self.助战.师门技能 = 内容.师门技能
		if self.助战.门派 == "无门派" then
		    tp.提示:写入("#Y/拜入门派才可以学习师门技能")
		    return false
		end
		self:加载数据()
		self.师门技能 = {}
		self.包含技能 = {}
		self.选中 = 0
		self.加入 = 0
		tp.运行时间 = tp.运行时间 + 1
	  	self.窗口时间 = tp.运行时间
		self.可视 = true
		self.本次需求 = {金钱=0,经验=0}
	end
end
function 助战技能学习:刷新(内容)
	self.助战 = 内容
	self.助战.师门技能 = 内容.师门技能
end
function 助战技能学习:加载数据()
	local 按钮 = tp._按钮
	local 资源 = tp.资源
	local 自适应 = tp._自适应
	self.资源组 = {
		[0] = 自适应.创建(1,1,372,18,1,3,nil,18), --横条
		[1] = 自适应.创建(0,1,385,473,3,9),
		[2] = 按钮.创建(自适应.创建(18,4,16,16,4,3),0,0,4,true,true),
		[3] = 按钮.创建(自适应.创建(20,4,18,19,4,3),0,0,4,true,true),--上滚动
		[4] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),--下滚动
		[5] = 按钮.创建(自适应.创建(20,4,18,19,4,3),0,0,4,true,true),--上滚动
		[6] = 按钮.创建(自适应.创建(21,4,18,19,4,3),0,0,4,true,true),--下滚动
		[7] = 按钮.创建(自适应.创建(12,4,42,22,1,3),0,0,4,true,true,"学习"),
		[8] = 自适应.创建(1,1,352,18,1,3,nil,18), --横条
		[51] = 自适应.创建(4,1,170,215,3,nil),
		[52] = 自适应.创建(2,1,156,148,3,9),
	}
	for n=2,7 do
		self.资源组[n]:绑定窗口_(22)
	end
end

function 助战技能学习:显示(dt,x,y)
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y,self.选中 ~= 0 and self.加入 > 0)
	self.资源组[4]:更新(x,y,self.选中 ~= 0 and self.加入 < #self.助战.师门技能[self.选中].包含技能 - 4)
	self.资源组[5]:更新(x,y,false)
	self.资源组[6]:更新(x,y,false)
	self.资源组[7]:更新(x,y,self.选中 ~= 0 and self.助战.师门技能[self.选中].等级 < 180,1)
	self.焦点 = false
	if self.鼠标 then
		if self.资源组[2]:事件判断() then
			self:打开()
			return false
		elseif self.资源组[3]:事件判断() then
			self.加入 = self.加入 - 1
		elseif self.资源组[4]:事件判断() then
			self.加入 = self.加入 + 1
		elseif self.资源组[7]:事件判断() then
			发送数据(108,{识别码=self.助战.识别码,序列=self.选中},3,17)
			if self.助战.当前经验 >= self.本次需求.经验 and (self.助战.银子 + self.助战.储备 >= self.本次需求.金钱) then
				if self.助战.等级+10 > self.助战.师门技能[self.选中].等级 then
					for n=1,#self.助战.师门技能[self.选中].包含技能 do
					 	if self.助战.师门技能[self.选中].包含技能[n].学会 then
						 	self.助战.师门技能[self.选中].包含技能[n].等级 = self.助战.师门技能[self.选中].等级
						end
					end
					self.助战.师门技能[self.选中].等级 = self.助战.师门技能[self.选中].等级 + 1
					self.介绍文本:清空()
					self.介绍文本:添加文本("#N/【等级】"..self.助战.师门技能[self.选中].等级)
					self.介绍文本1:清空()
					local 介绍 = SkillData[self.助战.师门技能[self.选中].名称].介绍
					if 介绍 == nil then
						介绍 = "无描述"
					end
					self.介绍文本1:添加文本("#N/【描述】"..介绍)
					local 本次需求 = 计算技能数据(self.助战.师门技能[self.选中].等级+1)
					self.本次需求 = {经验= 本次需求.经验,金钱 = 本次需求.金钱}
				end
			end
		end
	end
	-- 显示
	self.资源组[1]:显示(self.x,self.y)
	if 界面风格=="经典" then
	self.资源组[0]:显示(self.x+6,self.y-1)
	elseif 界面风格=="暖风" then
	self.资源组[0]:显示(self.x+106,self.y-1)
	end
	self.资源组[2]:显示(self.x + 364,self.y + 6)
	self.资源组[3]:显示(self.x + 355,self.y + 40)
	self.资源组[4]:显示(self.x + 355,self.y + 168)
	self.资源组[5]:显示(self.x + 354,self.y + 202)
	self.资源组[6]:显示(self.x + 354,self.y + 330)
	self.资源组[7]:显示(self.x + 154,self.y + 440,true)
	-- tp.窗口标题背景_:置区域(0,0,80,16)
	-- tp.窗口标题背景_:显示(self.x+139,self.y+2)
			self.资源组[51]:置宽高3(181,312)
		self.资源组[51]:显示(self.x+11,self.y+37)
		self.资源组[51]:置宽高3(159,153)
		self.资源组[51]:显示(self.x+195,self.y+37)
		self.资源组[52]:显示(self.x+195,self.y+200)
		zts1:置颜色(4294967295)
		zts1:置描边颜色(-16777216)
		zts1:置字间距(2)
		zts1:显示(self.x+48,self.y+42,"师门技能")
		zts1:显示(self.x+249,self.y+42,"师门法术")
		zts1:显示(self.x+135,self.y+42,"等级")

	zts1:置字间距(1)
	zts1:显示(self.x+150,self.y+10,"助战技能")
	--zts:置颜色(ARGB(255,0,0,0))
	if self.助战.门派 ~= "无门派" then
		for n=1,7 do
			bw:置坐标(self.x+18,self.y+35+n*40-5)
			if self.选中 ~= n then
				if bw:检查点(x,y) and not tp.消息栏焦点 and self.鼠标 then
					box(self.x+16,self.y+35+n*40-9,self.x+181,self.y+72+n*40-9,ARGB(255,201,207,109))
					if 引擎.鼠标弹起(0) then
						self.选中 = n
						self.加入 = 0
						self.包含技能 = {}
						self.介绍文本:清空()
						self.介绍文本:添加文本("#N/【等级】"..self.助战.师门技能[self.选中].等级)
						self.介绍文本1:清空()
						local 介绍 = SkillData[self.助战.师门技能[self.选中].名称].介绍
						if 介绍 == nil then
							介绍 = "无描述"
						end
						self.介绍文本1:添加文本("#N/【描述】"..介绍)
						local 本次需求 = 计算技能数据(self.助战.师门技能[self.选中].等级+1)
						self.本次需求 = {经验= 本次需求.经验,金钱 = 本次需求.金钱}
					end
					self.焦点 = true
				end
			else
				local ys = ARGB(255,91,90,219)
				if bw:检查点(x,y) then
					ys = ARGB(255,108,110,180)
					self.焦点 = true
				end
				box(self.x+16,self.y+35+n*40-9,self.x+181,self.y+72+n*40-9,ys)
			end
			local sm = self.助战.师门技能[n]
			local ski = SkillData[sm.名称]   --1介绍 6资源 7小图标 8大图标
			if self.师门技能[n] == nil then
				self.师门技能[n] = tp.资源:载入(ski.文件,"网易WDF动画",ski.大图标)
			end
			self.师门技能[n]:显示(self.x+18,self.y+25+n*40-5)
			zts:显示(self.x + 18+50,self.y + 35 +n*40+2,self.助战.师门技能[n].名称)
			zts:显示(self.x + 18+112,self.y + 35 +n*40+2 ,self.助战.师门技能[n].等级.."/180")
		end

    zts2:置颜色(0xffFFFFFF)
	tp.经验背景_:置宽高1(97,22)
	zts2:显示(self.x+8,self.y+361,"可用经验")
	tp.经验背景_:显示(self.x+76,self.y+358)
	tp.经验背景_:置宽高1(97,22)

	tp.经验背景_:置宽高1(97,22)
	zts2:显示(self.x+178,self.y+361,"升级所需经验")
	tp.经验背景_:显示(self.x+268,self.y+358)
	tp.经验背景_:置宽高1(97,22)

		tp.经验背景_:置宽高1(97,22)
	zts2:显示(self.x+8,self.y+386,"可用金钱")
	tp.经验背景_:显示(self.x+76,self.y+383)
	tp.经验背景_:置宽高1(97,22)

		tp.经验背景_:置宽高1(97,22)
	zts2:显示(self.x+178,self.y+386,"升级所需金钱")
	tp.经验背景_:显示(self.x+268,self.y+383)
	tp.经验背景_:置宽高1(97,22)

		tp.经验背景_:置宽高1(97,22)
	zts2:显示(self.x+8,self.y+412,"存 款 ")
	tp.经验背景_:显示(self.x+76,self.y+410)
	tp.经验背景_:置宽高1(97,22)

		tp.经验背景_:置宽高1(97,22)
	zts2:显示(self.x+178,self.y+412,"储 备 金 ")
	tp.经验背景_:显示(self.x+268,self.y+410)
	tp.经验背景_:置宽高1(97,22)



		-- 技能信息类
		if self.选中 ~= nil and self.选中 ~= 0 then
			--if #self.助战.师门技能[self.选中].包含技能 > 0 then
				--end
			if #self.助战.师门技能[self.选中].包含技能 > 0 then
				for i=1,#self.助战.师门技能[self.选中].包含技能 do
				local ski1 = SkillData[self.助战.师门技能[self.选中].包含技能[i].名称]
					self.包含技能[i] =  tp.资源:载入(ski1.文件,"网易WDF动画",ski1.小图标)
				end
				for i=1,4 do
					if self.包含技能[i] ~= nil then
						if not self.助战.师门技能[self.选中].包含技能[i].学会 then
							self.包含技能[i+self.加入]:灰度级()
					    end
						self.包含技能[i+self.加入]:显示(self.x+208,self.y+i*30+37)
						zts:显示(self.x+235,self.y+i*30+42,self.助战.师门技能[self.选中].包含技能[i+self.加入].名称)
						if self.包含技能[i]:是否选中(x,y) and self.鼠标 then
							tp.提示:技能(x,y,self.助战.师门技能[self.选中].包含技能[i],self.助战.师门技能[self.选中].包含技能[i].剩余冷却回合)
			            end
					end
				end
			end
			zts:显示(self.x + 83,self.y + 362,self.助战.当前经验)
			zts:显示(self.x + 83,self.y + 387,self.助战.银子)
			zts:显示(self.x + 83,self.y + 415,self.助战.存银)
			if self.本次需求 ~= nil and self.本次需求.经验 ~= nil and self.本次需求.金钱 ~= nil then
				zts:显示(self.x + 290,self.y + 362,self.本次需求.经验)
				zts:显示(self.x + 290,self.y + 387,self.本次需求.金钱)
			else
				zts:显示(self.x + 290,self.y + 362,"未学会技能")
				zts:显示(self.x + 290,self.y + 387,"未学会技能")
			end
			zts:显示(self.x + 290,self.y + 415,self.助战.储备)
		end
	end
	self.介绍文本:显示(self.x+205,self.y+205)
	self.介绍文本1:显示(self.x+205,self.y+225)
end

function 助战技能学习:检查点(x,y)
	if self.资源组~=nil and self.资源组[1]:是否选中(x,y)  then
		 return true
	end
end

function 助战技能学习:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
  		self.窗口时间 = tp.运行时间
 	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if  self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 助战技能学习:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 助战技能学习