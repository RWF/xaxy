-- @Author: 80后痴汉
-- @Date:	2021-10-25 00:54:26
-- @Last Modified by:	作者QQ2124872
-- @Last Modified time: 2021-10-25 10:34:10
-- @GGELUA游戏爱好者基地 https://www.ggelua.cc
-- @GGELUA游戏爱好者基地 https://bbs.ggelua.cc
-- @GGELUA游戏爱好者交流Q群 560529474
-- @GGELUA声明：编写此脚本只因个人兴趣爱好，如若放出，仅供个人用于学习、研究;不得用于商业用途。





local 助战辅助技能 = class()

local floor = math.floor
local min = math.min
local tp,zts1,zts2
local xxx = 0
local yyy = 0
local max = 1
local insert = table.insert
local mouseb = 引擎.鼠标弹起

function 助战辅助技能:初始化(根)
	self.ID = 41
	--宽高 549 431
	self.x = 372
	self.y = 105
	self.xx = 0
	self.yy = 0
	self.注释 = "助战辅助技能"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	self.状态 = 1
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体
	zta = tp.字体表.普通字体
	zts1 = tp.字体表.普通字体
	zts2 = tp.字体表.普通字体__
	self.分类选中=""
	self.子类选中=""
	self.玩法介绍内容=""
	self.丰富文本说明 = 根._丰富文本(476,265)

end

function 助战辅助技能:打开(内容)
	if self.可视 then
		self.可视 = false
		self.资源组 = nil
		return
	else
		insert(tp.窗口_,self)
		local 资源 = tp.资源
		local 自适应 = tp._自适应
		local 按钮 = tp._按钮
		self.资源组 = {
			[1] = 自适应.创建(0,1,315,421,3,9),
			--[2] = 按钮.创建(自适应.创建(12,4,36,22,1,3),0,0,4,true,true,"学习"),
			[16] = 自适应.创建(3,1,40,19,1,3),
		}
		for q = 1,11 do
			self.资源组[1+q] = 按钮.创建(自适应.创建(12,4,40,22,1,3),0,0,4,true,true,"学习")
		end
		self.状态 = 1
		self.加入 = 0
		self.选中 = 0
		tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
		self.可视 = true
		self.识别码= 内容.识别码
		local 辅助技能 = {"强身术","冥想","强壮","暗器技巧",}
		self.辅助技能 = {}
		self.本次消耗 = {}
		for n,v in pairs(辅助技能) do
			if 内容.辅助技能[v] ~= nil then
				local ski = SkillData[v]
				if ski.文件 ~= nil and ski.大图标 ~= nil then
					self.辅助技能[#self.辅助技能+1] = tp.资源:载入(ski.文件,"网易WDF动画",ski.大图标)
					self.辅助技能[#self.辅助技能].等级 = 内容.辅助技能[v]
					self.辅助技能[#self.辅助技能].jn = ski
					self.本次消耗[#self.本次消耗+1] = 计算技能数据(self.辅助技能[#self.辅助技能].等级+1)
					if self.辅助技能[#self.辅助技能].等级 >= 40 then
						self.本次消耗[#self.本次消耗].帮贡 = floor(0.5 * self.辅助技能[#self.辅助技能].等级)
					end
				end
			end
		end
		local 强化技能 = {"命中强化","伤害强化","防御强化","灵力强化","速度强化","固伤强化","治疗强化"}
		self.强化技能 = {}
		for n,v in pairs(强化技能) do
			if 内容.强化技能[v] ~= nil then
				local ski1 = SkillData[v]
				if ski1.文件 ~= nil and ski1.大图标 ~= nil then
					self.强化技能[#self.强化技能+1] = tp.资源:载入(ski1.文件,"网易WDF动画",ski1.大图标)
					self.强化技能[#self.强化技能].等级 = 内容.强化技能[v]
					self.强化技能[#self.强化技能].jn = ski1
					self.本次消耗[#self.本次消耗+1] = 计算技能数据(self.强化技能[#self.强化技能].等级+1)
					if self.强化技能[#self.强化技能].等级 >= 40 then
						self.本次消耗[#self.本次消耗].帮贡 = floor(0.5 * self.强化技能[#self.强化技能].等级)
					end
				end
			end
		end
	end
end

function 助战辅助技能:刷新(内容)
	self.识别码= 内容.识别码
	local 辅助技能 = {"强身术","冥想","强壮","暗器技巧",}
	self.辅助技能 = {}
	self.本次消耗 = {}
	for n,v in pairs(辅助技能) do
		if 内容.辅助技能[v] ~= nil then
			local ski = SkillData[v]
			if ski.文件 ~= nil and ski.大图标 ~= nil then
				self.辅助技能[#self.辅助技能+1] = tp.资源:载入(ski.文件,"网易WDF动画",ski.大图标)
				self.辅助技能[#self.辅助技能].等级 = 内容.辅助技能[v]
				self.辅助技能[#self.辅助技能].jn = ski
				self.本次消耗[#self.本次消耗+1] = 计算技能数据(self.辅助技能[#self.辅助技能].等级+1)
				if self.辅助技能[#self.辅助技能].等级 >= 40 then
					self.本次消耗[#self.本次消耗].帮贡 = floor(0.5 * self.辅助技能[#self.辅助技能].等级)
				end
			end
		end
	end
	local 强化技能 = {"命中强化","伤害强化","防御强化","灵力强化","速度强化","固伤强化","治疗强化"}
	self.强化技能 = {}
	for n,v in pairs(强化技能) do
		if 内容.强化技能[v] ~= nil then
			local ski1 = SkillData[v]
			if ski1.文件 ~= nil and ski1.大图标 ~= nil then
				self.强化技能[#self.强化技能+1] = tp.资源:载入(ski1.文件,"网易WDF动画",ski1.大图标)
				self.强化技能[#self.强化技能].等级 = 内容.强化技能[v]
				self.强化技能[#self.强化技能].jn = ski1
				self.本次消耗[#self.本次消耗+1] = 计算技能数据(self.强化技能[#self.强化技能].等级+1)
				if self.强化技能[#self.强化技能].等级 >= 40 then
					self.本次消耗[#self.本次消耗].帮贡 = floor(0.5 * self.强化技能[#self.强化技能].等级)
				end
			end
		end
	end
end


function 助战辅助技能:显示(dt,x,y)
	for n = 1,11 do
		self.资源组[1+n]:更新(x,y)
	end
	for n = 1,11 do
		if self.资源组[1+n]:是否选中(x,y) then
			local 消耗提示 = "#W升级消耗\n经验：#G"..self.本次消耗[n].经验.."#W\n银子：#G"..self.本次消耗[n].金钱
			if self.本次消耗[n].帮贡 ~= nil and self.本次消耗[n].帮贡 > 0 then
				消耗提示 = 消耗提示 .. "#W\n帮贡：#G"..self.本次消耗[n].帮贡
			end
			tp.提示:自定义(x+40,y+27,消耗提示)
		end
		if self.资源组[1+n]:事件判断() then
			local 技能名 = {"强身术","冥想","强壮","暗器技巧","命中强化","伤害强化","防御强化","灵力强化","速度强化","固伤强化","治疗强化"}
			发送数据(108,{识别码=self.识别码,学习号=技能名[n]},3,18)
		end
	end
	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
	for n = 1,11 do
		if n < 5 then
			self.资源组[1+n]:显示(self.x+30+n*70-70,self.y+118)
		elseif n < 9 then
			self.资源组[1+n]:显示(self.x+30+(n-4)*70-70,self.y+120+118)
		else
			self.资源组[1+n]:显示(self.x+30+(n-8)*70-70,self.y+120+118+120)
		end
	end
	self.资源组[16]:显示(self.x+30,self.y+97)
	self.资源组[16]:显示(self.x+100,self.y+97)
	self.资源组[16]:显示(self.x+170,self.y+97)
	self.资源组[16]:显示(self.x+240,self.y+97)
	self.资源组[16]:显示(self.x+30,self.y+97+120)
	self.资源组[16]:显示(self.x+100,self.y+97+120)
	self.资源组[16]:显示(self.x+170,self.y+97+120)
	self.资源组[16]:显示(self.x+240,self.y+97+120)
	self.资源组[16]:显示(self.x+30,self.y+97+120+120)
	self.资源组[16]:显示(self.x+100,self.y+97+120+120)
	self.资源组[16]:显示(self.x+170,self.y+97+120+120)
	zts:置颜色(0xffffffff)
	zts:显示(self.x+30+1*70-70,self.y+81,"强身术")
	zts:显示(self.x+30+2*70-70,self.y+81,"冥想")
	zts:显示(self.x+30+3*70-70,self.y+81,"强壮")
	zts:显示(self.x+30+4*70-70,self.y+81,"暗器技巧")
	zts:显示(self.x+30+1*70-70,self.y+81+120,"命中强化")
	zts:显示(self.x+30+2*70-70,self.y+81+120,"伤害强化")
	zts:显示(self.x+30+3*70-70,self.y+81+120,"防御强化")
	zts:显示(self.x+30+4*70-70,self.y+81+120,"灵力强化")
	zts:显示(self.x+30+1*70-70,self.y+81+120+120,"速度强化")
	zts:显示(self.x+30+2*70-70,self.y+81+120+120,"固伤强化")
	zts:显示(self.x+30+3*70-70,self.y+81+120+120,"治疗强化")
	zts:置颜色(-16777216)
	zts:显示(self.x+30+1*70-55,self.y+81+20,self.辅助技能[1].等级)
	zts:显示(self.x+30+2*70-55,self.y+81+20,self.辅助技能[2].等级)
	zts:显示(self.x+30+3*70-55,self.y+81+20,self.辅助技能[3].等级)
	zts:显示(self.x+30+4*70-55,self.y+81+20,self.辅助技能[4].等级)
	zts:显示(self.x+30+1*70-55,self.y+81+20+120,self.强化技能[1].等级)
	zts:显示(self.x+30+2*70-55,self.y+81+20+120,self.强化技能[2].等级)
	zts:显示(self.x+30+3*70-55,self.y+81+20+120,self.强化技能[3].等级)
	zts:显示(self.x+30+4*70-55,self.y+81+20+120,self.强化技能[4].等级)
	zts:显示(self.x+30+1*70-55,self.y+81+20+120+120,self.强化技能[5].等级)
	zts:显示(self.x+30+2*70-55,self.y+81+20+120+120,self.强化技能[6].等级)
	zts:显示(self.x+30+3*70-55,self.y+81+20+120+120,self.强化技能[7].等级)
	for n = 1,#self.辅助技能 do
		if self.辅助技能[n] ~= nil then
			self.辅助技能[n]:显示(self.x+30+n*70-70,self.y+35)
			if self.辅助技能[n]:是否选中(x,y) and self.鼠标 then
				tp.提示:技能(x,y,self.辅助技能[n].jn)
			end
		end
	end
	for i = 1,#self.强化技能 do
		if i < 5 then
			self.强化技能[i]:显示(self.x+30+i*70-70,self.y+35+120)
		elseif i < 9 then
			self.强化技能[i]:显示(self.x+30+(i-4)*70-70,self.y+35+120+120)
		else
			self.强化技能[i]:显示(self.x+30+(i-8)*70-70,self.y+35+120+120+120)
		end
		if self.强化技能[i]:是否选中(x,y) and self.鼠标 then
			tp.提示:技能(x,y,self.强化技能[i].jn)
		end
	end
	local xx = 0
	local yy = 0
end

function 助战辅助技能:检查点(x,y)
	if self.资源组[1] ~= nil  then
		return true
	end
end
function 助战辅助技能:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
		self.窗口时间 = tp.运行时间
	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 助战辅助技能:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 助战辅助技能