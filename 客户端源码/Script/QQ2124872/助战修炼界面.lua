-- @Author: 作者QQ414628710
-- @Date:   2022-01-15 20:06:44
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2023-08-06 18:22:18
-- @Author: 80后痴汉
-- @Date:	2021-10-25 00:54:26

local 助战修炼界面 = class()

local floor = math.floor
local min = math.min
local zts,tp,zts1,zts2
local xxx = 0
local yyy = 0
local max = 1
local insert = table.insert
local mouseb = 引擎.鼠标弹起

function 助战修炼界面:初始化(根)
	self.ID = 41
	--宽高 549 431
	self.x = 372
	self.y = 105
	self.xx = 0
	self.yy = 0
	self.注释 = "助战修炼界面"
	self.可视 = false
	self.鼠标 = false
	self.焦点 = false
	self.可移动 = true
	self.状态 = 1
	self.窗口时间 = 0
	tp = 根
	zts = tp.字体表.普通字体
	zta = tp.字体表.普通字体
	zts1 = tp.字体表.普通字体
	zts2 = tp.字体表.描边字体
	self.分类选中=""
	self.子类选中=""
	self.玩法介绍内容=""
	self.丰富文本说明 = 根._丰富文本(476,265)

end

function 助战修炼界面:打开(内容)
	if self.可视 then
		self.可视 = false
		self.资源组 = nil
		return
	else
		insert(tp.窗口_,self)
		local 资源 = tp.资源
		local 自适应 = tp._自适应
		local 按钮 = tp._按钮
		self.资源组 = {
			[1] = 自适应.创建(0,1,315,251,3,9),
			[2] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"十次"),
			[3] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"十次"),
			[4] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"十次"),
			[5] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"十次"),
			
			[6] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"一次"),
			[7] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"一次"),
			[8] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"一次"),
			[9] = 按钮.创建(自适应.创建(12,4,45,22,1,3),0,0,4,true,true,"一次"),
		}
		self.状态 = 1
		self.加入 = 0
		tp.运行时间 = tp.运行时间 + 1
		self.窗口时间 = tp.运行时间
		self.可视 = true
		self.助战修炼 = 内容.助战修炼
		self.识别码 = 内容.识别码
	end
end

function 助战修炼界面:刷新(内容)
	self.助战修炼 = 内容.助战修炼
	self.识别码 = 内容.识别码
end


function 助战修炼界面:显示(dt,x,y)
	self.焦点 = false
	self.资源组[1]:显示(self.x,self.y)
	
	self.资源组[2]:更新(x,y)
	self.资源组[3]:更新(x,y)
	self.资源组[4]:更新(x,y)
	self.资源组[5]:更新(x,y)
	
	self.资源组[6]:更新(x,y)
	self.资源组[7]:更新(x,y)
	self.资源组[8]:更新(x,y)
	self.资源组[9]:更新(x,y)
	
	self.资源组[2]:显示(self.x+265,self.y+60-2)
	self.资源组[3]:显示(self.x+265,self.y+100-2)
	self.资源组[4]:显示(self.x+265,self.y+140-2)
	self.资源组[5]:显示(self.x+265,self.y+180-2)
	
	self.资源组[6]:显示(self.x+215,self.y+60-2)
	self.资源组[7]:显示(self.x+215,self.y+100-2)
	self.资源组[8]:显示(self.x+215,self.y+140-2)
	self.资源组[9]:显示(self.x+215,self.y+180-2)
	
	zts:显示(self.x+110,self.y+20,"助战修炼界面")
	zts:显示(self.x+230,self.y+35,"提升次数")
	zts2:显示(self.x+10,self.y+60,"攻击修炼 等级:"..self.助战修炼.攻击修炼.当前等级.."/"..self.助战修炼.攻击修炼.上限等级.." 经验"..self.助战修炼.攻击修炼.当前经验.."/"..self.助战修炼.攻击修炼.升级经验)
	zts2:显示(self.x+10,self.y+100,"法术修炼 等级:"..self.助战修炼.法术修炼.当前等级.."/"..self.助战修炼.法术修炼.上限等级.." 经验"..self.助战修炼.法术修炼.当前经验.."/"..self.助战修炼.法术修炼.升级经验)
	zts2:显示(self.x+10,self.y+140,"防御修炼 等级:"..self.助战修炼.防御修炼.当前等级.."/"..self.助战修炼.防御修炼.上限等级.." 经验"..self.助战修炼.防御修炼.当前经验.."/"..self.助战修炼.防御修炼.升级经验)
	zts2:显示(self.x+10,self.y+180,"抗法修炼 等级:"..self.助战修炼.抗法修炼.当前等级.."/"..self.助战修炼.抗法修炼.上限等级.." 经验"..self.助战修炼.抗法修炼.当前经验.."/"..self.助战修炼.抗法修炼.升级经验)
	zts2:置颜色(0xff00FFFF):显示(self.x+10,self.y+220,"攻击修炼万银子/1次,防御修炼1.5万银子/1次")
	if self.资源组[2]:事件判断() then
		发送数据(108,{识别码=self.识别码,修炼类型="攻击修炼",次数=10},3,16)
	elseif self.资源组[3]:事件判断() then
		发送数据(108,{识别码=self.识别码,修炼类型="法术修炼",次数=10},3,16)
	elseif self.资源组[4]:事件判断() then
		发送数据(108,{识别码=self.识别码,修炼类型="防御修炼",次数=10},3,16)
	elseif self.资源组[5]:事件判断() then
		发送数据(108,{识别码=self.识别码,修炼类型="抗法修炼",次数=10},3,16)
	elseif self.资源组[6]:事件判断() then
		发送数据(108,{识别码=self.识别码,修炼类型="攻击修炼",次数=1},3,16)
	elseif self.资源组[7]:事件判断() then
		发送数据(108,{识别码=self.识别码,修炼类型="法术修炼",次数=1},3,16)
	elseif self.资源组[8]:事件判断() then
		发送数据(108,{识别码=self.识别码,修炼类型="防御修炼",次数=1},3,16)
	elseif self.资源组[9]:事件判断() then
		发送数据(108,{识别码=self.识别码,修炼类型="抗法修炼",次数=1},3,16)
	end
end

function 助战修炼界面:检查点(x,y)
	if self.资源组[1] ~= nil  then
		return true
	end
end

function 助战修炼界面:初始移动(x,y)
	tp.运行时间 = tp.运行时间 + 1
	if not tp.消息栏焦点 then
		self.窗口时间 = tp.运行时间
	end
	if not self.焦点 then
		tp.移动窗口 = true
	end
	if self.鼠标 and  not self.焦点 then
		self.xx = x - self.x
		self.yy = y - self.y
	end
end

function 助战修炼界面:开始移动(x,y)
	if self.鼠标 then
		self.x = x - self.xx
		self.y = y - self.yy
	end
end

return 助战修炼界面