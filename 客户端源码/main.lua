-- @Author: 作者QQ414628710
-- @Date:   2021-11-11 01:02:19
-- @Last Modified by:   作者QQ414628710
-- @Last Modified time: 2022-04-25 01:11:41
-- 入口
require("gge函数")

require("Script/ComomFunc/ComomFunc")


窗口逻辑= require("script/UI/窗口逻辑")
ffi = require("ffi")
f函数 = require("ffi函数")
cjson = require("cjson")

require("lfs")
-- local lfs  = require("lfs")
程序目录= __gge.isdebug and string.match(lfs.currentdir(), "^(.*)\\") ..[[\客户端\]] or lfs.currentdir()..[[\]]
全局游戏宽度 =math.min(math.ceil(读配置(程序目录.."配置.ini","环境配置","宽度")) or 800,1280)       
全局游戏高度 = math.min(math.ceil(读配置(程序目录.."配置.ini","环境配置","高度")) or 600,820)
窗口标题 = {服务器 = "",时间 = "",id = ""}
全局时间=0
-------------------
游戏公告="获取公告失败，请关闭游戏重新打开或者检查您的网络连接"
全局游戏标题="文和西游"
引擎(全局游戏标题, 全局游戏宽度,全局游戏高度, 60, true)
scale_w = (全局游戏宽度/800);
scale_h = (全局游戏高度/600);
require("Script/Resource/音效库")
require("Script/Resource/特效库")
require("Script/Resource/坐骑库")
角色动画类=require ("script/Role/角色动画类")
-- 授权=读配置(程序目录.."配置.ini","环境配置","FT")
 -- 授权="Y4MBVyrYZJJfEQLnUax+AA=="
-- Config=ReadExcel("Config",授权)[1]

-- 全局游戏标题=Config.标题
-- 全局IP =  Config.ip
-- 版本号=   Config.版本号
-- 全局端口 = Config.端口

-- 全局IP =  "127.0.0.1"
-- 全局端口 = 9097
全局IP =  "111.230.192.48"
全局端口 = 40000
版本号=   1.01


CacheFile ={}
同步时间 = os.time()
屏蔽查看 =false
屏蔽定制 =false 
屏蔽交易=false

玩家屏蔽 = false
管理模式 =false
摊位屏蔽 = false
地图特效 = true
低配模式 = false
连点模式 = false
角色移动 = 0
消息开关 = false
队伍消息 =false
屏蔽变身= false
屏蔽给予= false
游戏时间 = 0
系统处理类 = {数字id = 0}
重连状态 = false
连接状态 = false
连接状态 = true
自动回合 = 100
禁止重连 = false
使用重连 = false
消息闪烁=false
自动战斗 = false
-- 迭代=true
local yq = 引擎
yq.场景 = require("script/Control/Control")()
yq.垂直同步(true)
local tp = yq.场景
local jc = false
xtjc=os.time()
function 更新函数(dt,x,y)
	if os.time() - 数据包时间 >= 1 then
		数据包流 = 0
		数据包流1 = 0
		数据包时间 = os.time()
	end
	 -- if math.floor(引擎.取游戏时间()/1000)>os.time()-xtjc then
	 --    引擎.关闭()
	 --    __gge.messagebox("游戏数据异常，请勿使用任何加速工具","加速检测")
  --   end
	if 使用重连 and 禁止重连 == false and 停止连接 == false then
		停止连接 = true
		客户端:重新连接()
	end
	随机序列 = 随机序列 + 1
	if 随机序列 >= 1000 then
		随机序列 = 0
	end
end
function 渲染函数(dt,x,y)--鼠标x,鼠标y
  
	local wbgb
	local wbgb2
	if yq.外部 ~= nil then
		if yq.外部.渲染开始() then
			yq.场景.外部聊天框[tp.系统设置.战斗设置[8]]:显示(0,0)
			local xxx,xxy = 10,0
			local kg = false
			local xx,yy = yq.外部.取鼠标坐标()
			local xxk = yq.场景.窗口.聊天框类
			xxk.按钮_左拉:更新(xx,yy)
			xxk.按钮_上拉:更新(xx,yy)
			xxk.按钮_下拉:更新(xx,yy)
			xxk.按钮_移动:更新(xx,yy)
			xxk.按钮_查询:更新(xx,yy)
			xxk.按钮_禁止:更新(xx,yy)
			xxk.按钮_锁定:更新(xx,yy)
			xxk.按钮_清屏:更新(xx,yy)

			  xxk.按钮_门 :更新(xx,yy)
			  xxk.按钮_系:更新(xx,yy)
			  xxk.按钮_全:更新(xx,yy)
			  xxk.按钮_闻:更新(xx,yy)
			  xxk.按钮_当:更新(xx,yy)
			  xxk.按钮_经:更新(xx,yy)
			  xxk.按钮_队:更新(xx,yy)
			  xxk.按钮_音:更新(xx,yy)
			if xxk.按钮_左拉:事件判断() then
				wbgb2 = true
			elseif xxk.按钮_下拉:事件判断() then
				if xxk.丰富文本外框.滚动值 > 0 then
					xxk.丰富文本外框:滚动(-1)
				end
			elseif xxk.按钮_上拉:事件判断() then
				if xxk.丰富文本外框.滚动值 < #xxk.丰富文本外框.显示表 - 24 then
					xxk.丰富文本外框:滚动(1)
				end
			elseif xxk.按钮_禁止:事件判断() then
				xxk.禁止 = xxk.禁止 == false
			elseif xxk.按钮_清屏:事件判断() then
				xxk:清空内容()
			end
			xxk.丰富文本外框:显示(xxx-10,xxy + 2)
			xxk.按钮_左拉:显示(xxx + 3,xxy +全局游戏高度-20,true)
			xxk.按钮_上拉:显示(xxx + 26,xxy + 全局游戏高度-20,true)
			xxk.按钮_下拉:显示(xxx + 49,xxy + 全局游戏高度-20,true)
			xxk.按钮_移动:显示(xxx + 73,xxy + 全局游戏高度-20,true)
			xxk.按钮_查询:显示(xxx + 96,xxy +全局游戏高度-20,true)
			xxk.按钮_禁止:显示(xxx + 119,xxy + 全局游戏高度-20,true)
			xxk.按钮_锁定:显示(xxx + 143,xxy +全局游戏高度-20,true)
			xxk.按钮_清屏:显示(xxx + 165,xxy + 全局游戏高度-20,true)
			xxk.按钮_信号:显示(xxx + 188,xxy + 全局游戏高度-20,true)

			  xxk.按钮_全:显示(xxx + 3,xxy +全局游戏高度-40,true)
			  xxk.按钮_当:显示(xxx + 26,xxy +全局游戏高度-40,true)
			  xxk.按钮_队:显示(xxx + 49,xxy +全局游戏高度-40,true)
			  xxk.按钮_门:显示(xxx + 73,xxy +全局游戏高度-40,true)
			  xxk.按钮_系:显示(xxx + 96,xxy +全局游戏高度-40,true)
			  xxk.按钮_闻:显示(xxx + 119,xxy +全局游戏高度-40,true)
			  xxk.按钮_经:显示(xxx + 143,xxy +全局游戏高度-40,true)
			  xxk.按钮_音:显示(xxx + 165,xxy +全局游戏高度-40,true)

	        yq.外部.渲染结束()
	        if 引擎.是否在窗口内() then
		        yq.外部.置鼠标按住(false)
	        end
	    end
	    if yq.外部.是否在窗口内()  then
	        引擎.在外部 = true
	    else
	        引擎.在外部 = nil
	    end
    end
	引擎.渲染开始()
	-- print("开始","-当前内存",math.floor(collectgarbage("count")/1024).."MB")
	引擎.渲染清除()
	tp:显示(dt,x,y)
	引擎.渲染结束()

	if wbgb then
		临时高度=1
	end
	if wbgb2 then
		外部聊天框窗口关闭(wbgb2)
	end
	
end
function 外部聊天框窗口关闭(wbgb)
	if yq.外部 ~= nil and wbgb then
		local xxk = yq.场景.窗口.聊天框类
		xxk.外部聊天 = xxk.外部聊天 == false
		yq.外部.关闭()
		yq.外部 = nil
		-- xxk.丰富文本:置宽度(450)
		xxk.丰富文本:置高度(xxk:取高度())
		if xxk.丰富文本.滚动值 > 0 then
			xxk.丰富文本:滚动(-xxk.丰富文本.滚动值)
		end
		if xxk.丰富文本.滚动值 < #xxk.丰富文本.显示表 - 24 then
			xxk.丰富文本:滚动(xxk.丰富文本.滚动值)
		end
		xxk.按钮_左拉.外部按钮 = nil
		xxk.按钮_上拉.外部按钮 = nil
		xxk.按钮_下拉.外部按钮 = nil
		xxk.按钮_移动.外部按钮 = nil
		xxk.按钮_查询.外部按钮 = nil
		xxk.按钮_禁止.外部按钮 = nil
		xxk.按钮_锁定.外部按钮 = nil
		xxk.按钮_清屏.外部按钮 = nil
		xxk.按钮_移动.确定按下 = false
		引擎.在外部 = nil
		collectgarbage("collect")
	end
end
local function 退出函数()

	停止连接=true
	客户端:断开连接()
	return true
end
引擎.置退出函数(退出函数)
function 客户端连接断开()
	连接状态 = false
	使用重连 = true
end







